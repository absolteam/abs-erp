<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var actionname="";
var paymentDetails="";
var tempid="";
$(function(){  
	$('#chequeNumber').attr('autocomplete','off');
	$("#paymentValidation").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	$('#paymentDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	$('#chequeDate').datepick();
	
	if(Number($('#paymentId').val())>0){ 
		$('#paymentMode option').each(function(){
			var thisval = $(this).val();
			var thisarr = thisval.split('@@');
			if(Number($('#temppaymode_'+rowId).val()) == Number(thisarr[1]))
				$('#paymode_'+rowId+' option[value='+$(this).val()+']').attr("selected","selected");  
		}); 
		$('#currencyId').val($('#temp_currency').val());
		if(Number($('#accountId').val()>0)){ 
			$('#cash_combination').hide();
			$('#account_no').show();
		}else{
			$('#cash_combination').show();
			$('#account_no').hide();
		}
	}else{
		var default_currency=$('#default_currency').val(); 
		$('#currencyId option').each(function(){  
			var curval=$(this).val();
			if(curval==default_currency){   
				$('#currencyId option[value='+$(this).val()+']').attr("selected","selected"); 
				return false;
			}
		});
	} 
	$('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
		actionname=""; 
		actionname=$(this).attr('id');  
		if(actionname=='receive'){ 
			actionname="show_receive_list";
		} 
		else if(actionname=="accountid"){
	    	   bankId=Number(0);
	    	   actionname="getbankaccount_details";
	    }
		else{ 
			actionname="show_paymentterm_details";
		} 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+actionname+".action", 
		 	async: false,  
		 	data : {showPage : "invPayment"},  
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
			return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.pdiscard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$('#codecombination-popup').dialog('destroy');		
					$('#codecombination-popup').remove();  
					$("#main-wrapper").html(result);  
				}
			});
	 });

	 $('#paymentMode').change(function(){ 
		 if($('#paymentMode option:selected').text()=="Cash"){
			 $('#accountNumber').removeClass('validate[required]');
			 $('#cashCombination').addClass('validate[required]');
			 $('#accountNumber').val('');
			 $('#account_no').hide();
			 $('#chequeDate').val('');
			 $('#cash_combination').show();
			 $('#cashCombinationId').val('');
			 $('#cashCombination').val('');
		 }if($('#paymentMode option:selected').text()=="Bank"){
			 $('#cashCombination').removeClass('validate[required]');
			 $('#accountNumber').addClass('validate[required]');
			 $('#accountNumber').val('');
			 $('#chequeDate').val('');
			 $('#account_no').show();
			 $('#cash_combination').hide();
			 $('#cashCombinationId').val('');
			 $('#cashCombination').val('');
		 }
	 });

	//Combination pop-up config
	    $('.codecombination-popup').click(function(){
	        tempid=$(this).parent().get(0); 
	        $('.ui-dialog-titlebar').remove(); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/combination_treeview.action",
	             async: false, 
	             dataType: "html",
	             cache: false,
	             success:function(result){
	                  $('.codecombination-result').html(result); 
	             },
	             error:function(result){
	                  $('.codecombination-result').html(result);
	             }
	         });
	         return false;
	    }); 

	    $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
		});

	$('.psave').click(function(){   
		var paymentId=Number($('#paymentId').val());
		var paymentNumber=$('#paymentNumber').val();
		var paymentDate=$('#paymentDate').val();
		var receiveId=Number($('#receiveId').val());
		var paymentTermId=Number($('#paymentTermId').val());  
		var invoiceNumber=$('#invoiceNumber').val();
		var loanId=Number(0);
		var currencyId=Number($('#currencyId').val()); 
		var accountId=Number($('#accountId').val()); 
		var description=$('#description').val();
		var paymentMode=Number($('#paymentMode').val());
		var cashCombinationId = Number($('#cashCombinationId').val()); 
		var chequeNumber=$('#chequeNumber').val();
		var chequeDate = $('#chequeDate').val(); 
		if($("#paymentValidation").validationEngine({returnIsValid:true})){   
		 $.ajax({
				type:"POST",
				url:'<%=request.getContextPath()%>/payment_inventory_save.action',
			 	async: false,
			 	data:{	
				 		paymentId:paymentId, paymentNumber:paymentNumber, paymentDate:paymentDate, receiveId:receiveId, paymentTermId:paymentTermId,
				 		invoiceNumber:invoiceNumber,paymentDetails:paymentDetails, loanId:loanId, currencyId:currencyId, bankAccountId:accountId,
				 		description:description, paymentMode:paymentMode, cashCombinationId: cashCombinationId, chequeNumber:chequeNumber, chequeDate:chequeDate
				 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();   
									$('#codecombination-popup').dialog('destroy');		
									$('#codecombination-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(message).slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				}
			 });
		} 
		else{
		 return false;
	 	}
	 });

	 var getPaymentDetails=function(){
		 var receiveLines=new Array(); 
		 var productLines=new Array(); 
		 var unitLines=new Array();  
		 var loanCharges=new Array();
 		 var combination=new Array();
 		 var lineDescriptions=new Array();
 		 var detailId=new Array();
		 var productId="";
		 var receiveQty=""; 
		 var unitrateQty=""; 
		 paymentDetails="";  
		 $('.rowid').each(function(){
		 	 var rowId=getRowId($(this).attr('id')); 
			 productId=$('#productid_'+rowId).val();
			 receiveQty=$('#receiveqty_'+rowId).text(); 
			 unitrateQty=$('#unitrateval_'+rowId).val(); 
			 var paymentDetailId = Number($('#detailid_'+rowId).val()); 
			 var linedescription=$('#lineDescription_'+rowId).val();
			 if(linedescription==null || linedescription=='')
				 linedescription="###";
			 if(typeof productId!='undefined' &&
					typeof receiveQty!='undefined' && productId!='' && receiveQty!=''
					&& typeof unitrateQty!='undefined' && unitrateQty!=''){ 
				receiveLines.push(receiveQty); 
				productLines.push(productId);
				unitLines.push(unitrateQty);  
				loanCharges.push(0);
 				combination.push(0);
 				detailId.push(paymentDetailId);
 				lineDescriptions.push(linedescription);
			 }  
		  }); 
		  for(var j=0;j<receiveLines.length;j++){ 
			  paymentDetails+=productLines[j]+"__"+receiveLines[j]+"__"+unitLines[j]+"__"+loanCharges[j]+"__"+combination[j]+"__"+lineDescriptions[j]+"__"+detailId[j];
			if(j==receiveLines.length-1){   
			} 
			else{
				paymentDetails+="#@";
			}
		 }  
		 return paymentDetails;
	 };
	 $('#amount').val($("#netAmount").val());
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function setCombination(combinationTreeId, combinationTree){  
  	$('#cashCombinationId').val(combinationTreeId); 
    $('#cashCombination').val(combinationTree);  
} 
function processDiscount(paymentTermId){
	if(paymentTermId!=0){ 
	 var amountRegx = new RegExp(',', 'g'); 
	 var amount=Number($('#amount').val().replace(amountRegx,''));   
	 var paymentDate = $('#paymentDate').val();
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_discount_details.action", 
		 	async: false,
		 	data:{totalAmount: amount, paymentTermId: paymentTermId, paymentDate:paymentDate},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$("#dicount_details").html(result);  
			}
	  });
	}
	return false;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="account.payment.info" /></div>
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.generalInfo" /></div>
			<form name="paymentValidation" id="paymentValidation"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">   
					<div class="width50 float-left"> 
						<fieldset> 
							<div>
								<label class="width30" for="paymentNumber"><fmt:message key="account.payment.paymentNumber" /></label> 
								<input type="hidden" id="paymentId" name="paymentId" value="${PAYMENT_INFO.paymentId}"/>
								<c:choose>
									<c:when test="${PAYMENT_INFO.paymentId ne null && PAYMENT_INFO.paymentId ne ''}"> 
										<input type="text" readonly="readonly" name="paymentNumber" id="paymentNumber" class="width50 validate[required]" value="${PAYMENT_INFO.paymentNumber}"/> 
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="paymentNumber" id="paymentNumber" class="width50 validate[required]" value="${PAYMENT_NUMBER}"/>
									</c:otherwise>
								</c:choose>  
							</div> 
							<div>
								<label class="width30" for="paymentDate"><fmt:message key="account.payment.paymentDate" /></label> 
								<c:choose>
									<c:when test="${PAYMENT_INFO.date ne null && PAYMENT_INFO.date ne ''}">
										<c:set var="date" value="${PAYMENT_INFO.date}"/>  
										<% 
											String date=DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
										%>
										<input type="text" readonly="readonly" name="paymentDate" id="paymentDate" class="width50 validate[required]" value="<%=date%>"/> 
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="paymentDate" id="paymentDate" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div>   
							<div>
								<label class="width30" for="invoiceNumber"><fmt:message key="account.payment.invoiceNumber" /></label> 
								<input type="text" name="invoiceNumber" id="invoiceNumber" class="width50" value="${PAYMENT_INFO.invoiceNumber}"/>
							</div> 
							<div>
								<label class="width30"><fmt:message key="account.payment.currency" /></label>
								<select name="currencyId" class="width50 validate[required]" id="currencyId">
									<option value="">Select</option> 
									<c:choose>
										<c:when test="${CURRENCY_LIST ne null && CURRENCY_LIST ne ''}">
											<c:forEach var="currency" items="${CURRENCY_LIST}">
												<option value="${currency.currencyId}">${currency.currencyPool.code}</option> 
											</c:forEach>
										</c:when>
									</c:choose> 
								</select> 
								<input type="hidden" id="default_currency" name="default_currency" value="${DEFAULT_CURRENCY}"/>
								<input type="hidden" id="temp_currency" name="temp_currency" value="${PAYMENT_INFO.currency.currencyId}"/>
							</div>	
							<div>
								<label class="width30"><fmt:message key="account.payment.paymentMode" /></label>
								<select name="paymentMode" class="width50 validate[required]" id="paymentMode">
									<option value="">Select</option>  
									<c:choose>
										<c:when test="${PAYMENT_MODE ne null && PAYMENT_MODE ne ''}">
											<c:forEach var="MODE" items="${PAYMENT_MODE}">
												<option value="${MODE.accessCode}@@${MODE.lookupDetailId}">${MODE.displayName}</option> 
											</c:forEach>
										</c:when>
									</c:choose> 
								</select> 
								<input type="hidden" id="tempModeId" name="tempModeId" value="${PAYMENT_INFO.lookupDetail.lookupDetailId}"/>
							</div>	
							<div id="cash_combination" style="display:none;">
								<label class="width30"><fmt:message key="account.payment.cashcombiantion" /></label>
								<c:choose>
									<c:when test="${PAYMENT_INFO.combination ne null && PAYMENT_INFO.combination ne ''}">
										<c:choose>
											<c:when test="${PAYMENT_INFO.combination.accountByAnalysisAccountId ne null && PAYMENT_INFO.combination.accountByAnalysisAccountId ne ''}">
												<input type="text" readonly="readonly" name="cashCombination" tabindex="5"  id="cashCombination" 
														class="width50" value="${PAYMENT_INFO.combination.accountByAnalysisAccountId.account}"/>
											</c:when>
											<c:otherwise>
												<input type="text" readonly="readonly" name="cashCombination" tabindex="5"  id="cashCombination" 
														class="width50" value="${PAYMENT_INFO.combination.accountByNaturalAccountId.account}"/>
											</c:otherwise>
										</c:choose>  
									</c:when>
									<c:otherwise>
										 <span class="button">
											<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span> 
									</c:otherwise>
								</c:choose> 					
								<input type="hidden" readonly="readonly" name="cashCombinationId" id="cashCombinationId"
									value="${PAYMENT_INFO.combination.combinationId}"/>
							</div>
							<div id="account_no" style="display:none;">
								<label class="width30" for="accountNumber"><fmt:message key="account.payment.accountNo" /></label>
								<input type="text" readonly="readonly" name="accountNumber" tabindex="5"  id="accountNumber" 
									class="width50" value="${PAYMENT_INFO.bankAccount.bank.bankName} [${PAYMENT_INFO.bankAccount.accountNumber}]"/>
								<span class="button" style="position: relative!important;top:5px!important; right:45px!important; float: right;">
									<a style="cursor: pointer;" id="accountid" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
								<input type="hidden" readonly="readonly" name="accountId" id="accountId"
									value="${PAYMENT_INFO.bankAccount.bankAccountId}"/>
								<label class="width30" for="chequeNumber"><fmt:message key="account.payment.chequeNumber" /></label>
								<input type="text" name="chequeNumber" tabindex="5"  id="chequeNumber" 
									class="width50" value="${PAYMENT_INFO.chequeNumber}"/>
								<label class="width30" for="chequeDate"><fmt:message key="account.payment.chequedate" /></label>
								<c:choose>
									<c:when test="${PAYMENT_INFO.chequeDate ne null && PAYMENT_INFO ne ''}">
										<c:set var="chequeDate" value="${PAYMENT_INFO.chequeDate}"/>
										<input type="text" name="chequeDate" tabindex="5" readonly="readonly"  id="chequeDate" 
											class="width50" value="<%=DateFormat.convertDateToString(pageContext.getAttribute("chequeDate").toString())%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" name="chequeDate" tabindex="5"  readonly="readonly" id="chequeDate" class="width50"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div>
								<label class="width30" for="description"><fmt:message key="accounts.quotation.description" /></label>
								<textarea rows="2" id="description" class="width50 float-left">${PAYMENT_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width48 float-left"> 
						<fieldset style="min-height:183px;"> 
							<div>
								<label class="width30" for="receiveNumber"><fmt:message key="account.payment.receiveNumber" /></label>
								<input type="hidden" id="receiveId" name="receiveId" value="${PAYMENT_INFO.receive.receiveId}"/>
								<input type="text" name="receiveNumber" readonly="readonly" id="receiveNumber" class="width50 validate[required]" value="${PAYMENT_INFO.receive.receiveNumber}"/>
								<span class="button">
									<a style="cursor: pointer;" id="receive" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div> 
							<div>
								<label class="width30" for="supplierNumber"><fmt:message key="hr.supplier.supplierNumber" /></label> 
								<input type="text" readonly="readonly" name="supplierNumber" id="supplierNumber" class="width50" value="${PAYMENT_INFO.receive.purchase.supplier.supplierNumber}"/> 
							</div>
							<div>
								<label class="width30" for="supplierName"><fmt:message key="hr.supplier.name" /></label> 
								<c:choose>
									<c:when test="${PAYMENT_INFO.receive.purchase.supplier.person ne null && PAYMENT_INFO.receive.purchase.supplier.person ne ''}">
										<input type="text" readonly="readonly" name="supplierName" id="supplierName" class="width50" value=" ${PAYMENT_INFO.receive.purchase.supplier.person.firstName} ${PAYMENT_INFO.receive.purchase.supplier.person.lastName}"/> 
									</c:when>
									<c:otherwise> 
										<input type="text" readonly="readonly" name="supplierName" id="supplierName" class="width50" value="${PAYMENT_INFO.receive.purchase.supplier.cmpDeptLocation.company.companyName}"/> 
									</c:otherwise> 
								</c:choose>
							</div> 
							<div>
								<label class="width30" for="paymenttermname"><fmt:message key="accounts.supplier.paymentTerms" /></label>
								<input type="hidden" name="paymentTermId" id="paymentTermId" class="width50" value="${PAYMENT_INFO.paymentTerm.paymentTermId}"/>
								<input type="text" name="paymenttermname" readonly="readonly" id="paymenttermname" class="width50" value="${PAYMENT_INFO.paymentTerm.name}"/>
								<span class="button">
									<a style="cursor: pointer;" id="paymentterm" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div> 
							<div>
								<label class="width30" for="discountDays"><fmt:message key="account.payment.discountDays" /></label> 
								<input type="text" readonly="readonly" name="discountDays" id="discountDays" class="width50" value="${PAYMENT_INFO.paymentTerm.discountDays}"/> 
							</div> 
							<div>
								<label class="width30" for="discount"><fmt:message key="account.payment.discount" /></label> 
								<input type="text" readonly="readonly" name="discount" id="discount" class="width50" value="${PAYMENT_INFO.paymentTerm.discount}"/> 
							</div>
							<div>
								<label class="width30" for="amount"><fmt:message key="account.payment.amount" /></label> 
								<input type="text" readonly="readonly" name="amount" id="amount" class="width50" value=""/> 
							</div>
						</fieldset>
					</div>  
			</div>  
		</div>    
		<div class="clearfix"></div> 
		<div class="portlet-content" style="margin-top:10px;" id="hrm"> 
 			<fieldset>
 				<legend><fmt:message key="accounts.quotation.productInfo" /></legend>
	 			<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100" >   
						 <table id="hastab" class="width100"> 
							<thead>
							   <tr>  
							   		<th style="width:5%"><fmt:message key="accounts.quotation.productCode" /></th>  
								    <th style="width:5%"><fmt:message key="accounts.quotation.product" /></th>  
								    <th style="width:5%">UOM</th>  
								    <th style="width:5%"><fmt:message key="accounts.quotation.quantity" /></th> 
								    <th style="width:5%"><fmt:message key="accounts.quotation.unitRate" /></th>  
								    <th style="width:5%"><fmt:message key="accounts.quotation.totalAmount" /></th>  
								    <th style="width:5%"><fmt:message key="accounts.quotation.description" /></th>  
							  </tr>
							</thead> 
							<tbody class="tab">
								 <%double totalAmount=0; %> 
								<c:choose>
									<c:when test="${PAYMENT_INFO.paymentDetails ne null && PAYMENT_INFO.paymentDetails ne ''
										 && fn:length(PAYMENT_INFO.paymentDetails) > 0}"> 
										 <c:forEach var="PAYMENT_DETAIL" items="${PAYMENT_INFO.paymentDetails}" varStatus="status"> 
										 	<tr class="rowid" id="fieldrow_${status.index+1}"> 
										 		<td id="productcode_${status_index+1}">${PAYMENT_DETAIL.product.code}</td>
										 		<td id="productname_${status_index+1}">${PAYMENT_DETAIL.product.productName}</td>
										 		<td id="uom_${status_index+1}">${PAYMENT_DETAIL.product.lookupDetailByProductUnit.displayName}</td>
										 		<td id="receiveqty_${status_index+1}">${PAYMENT_DETAIL.quantity}</td>
										 		<td id="unitrate_${status_index+1}">
										 			<c:set var="unitRate" value="${PAYMENT_DETAIL.unitRate}"/>
										 			<%=AIOSCommons.formatAmount(pageContext.getAttribute("unitRate"))%> 
										 			<input type="hidden" id="unitrateval_${status.index+1}" value="${PAYMENT_DETAIL.unitRate}"/>
										 		</td>
										 		<td id="total_${status_index+1}">
										 			<c:set var="totalAmount" value="${PAYMENT_DETAIL.quantity * PAYMENT_DETAIL.unitRate}"/>
										 			<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalAmount"))%> 
										 			<%totalAmount+= Double.parseDouble(pageContext.getAttribute("totalAmount").toString());%>
										 		</td>
										 		<td style="display: none;">
										 			<input type="hidden" value="${PAYMENT_DETAIL.product.productId}" id="productid_${status_index+1}">
										 			<input type="hidden" value="${PAYMENT_DETAIL.paymentDetailId}" id="detailid_${status_index+1}">
										 		</td>
										 		<td>
										 			<input type="text" id="lineDescription_${status_index+1}" value="${PAYMENT_DETAIL.description}"/>
										 		</td>
										 	</tr>
										 </c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach var="i" begin="1" end="2" step="1" varStatus="status1"> 
											<tr class="rowid" id="fieldrow_${status1.index+1}" style="height:23px;"> 
												<c:forEach var="j" begin="1" end="7" step="1" varStatus="status1"> 
													<td></td>
												 </c:forEach>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose> 
							</tbody>
					</table>
				</div> 
			</fieldset>
		</div>
		<div class="clearfix"></div> 
		<div class="portlet-content" style="margin-top:10px;" id="hrm"> 
			<div class="width100 float-left" id="dicount_details">
				<c:if test="${PAYMENT_INFO.paymentTerm ne null && PAYMENT_INFO.paymentTerm ne ''}">
					<fieldset>
						<legend><fmt:message key="accounts.discount.discountdetails" /></legend>
						<div class="width100 float-left">
							<label><fmt:message key="accounts.quotation.totalAmount" /></label> 
							<span style="text-align: right;"><%=AIOSCommons.formatAmount(totalAmount) %></span>
						</div>
						<div class="width100 float-left">
							<label><fmt:message key="accounts.discount.discountreceived" /></label>
							<c:choose>
								<c:when test="${PAYMENT_INFO.discountReceived ne null && PAYMENT_INFO.discountReceived ne 0}">
									<c:set var="discountReceived" value="${PAYMENT_INFO.discountReceived}"/>
								</c:when>
								<c:otherwise>
									<c:set var="discountReceived" value="0.00"/>
								</c:otherwise>
							</c:choose> 
							<span style="text-align: right;">
								<c:choose>
									<c:when test="${PAYMENT_INFO.discountReceived ne null && PAYMENT_INFO.discountReceived ne 0}">
										<%=AIOSCommons.formatAmount(pageContext.getAttribute("discountReceived"))%>
									</c:when>
									<c:otherwise>
										<c:out value="${discountReceived}" />
									</c:otherwise>
								</c:choose> 
							</span>
						</div> 
						<div class="width100 float-left">
							<label><fmt:message key="accounts.discount.netamount" /></label> 
							<span style="text-align: right;">
								<%=AIOSCommons.formatAmount(totalAmount - Double.parseDouble(pageContext.getAttribute("discountReceived").toString()))%>
								<input id="netAmount" type="hidden" value="<%=AIOSCommons.formatAmount(totalAmount - Double.parseDouble(pageContext.getAttribute("discountReceived").toString()))%>"/>
							</span>
						</div>
					</fieldset>
				</c:if>
			</div> 
		</div> 
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right pdiscard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right psave" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
  </form>
  </div> 
</div>