<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td{
	width:6%;
}
.calendarflag{
	display: none;font-size: small;color: red;
}
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
} 
.ui-autocomplete-input {min-height:27px!important;}
.ui-combobox-button {padding-bottom:  5px !important;}
</style> 
 <script type="text/javascript">
 var startpick;
 var slidetab;
 var availflag=1;
 var calendarRecordsAdded = 0;
 var periodStatusFlag=false;
 $(function(){   

	 if(Number($('#calHeaderId').val())>0){
			$('#status').val($('#tempstatus').val().trim());
			calendarRecordsAdded=1;
			if(Number($('.tab tr').length>2)){
				$('#startPicker').attr('disabled',true);
				$('.delete_calendar').remove();
			} 
			var calendarstatus=$('#financial-status').val().trim();	 
			if(calendarstatus=="false" && Number($('#status').val())==2){
				$('#status').attr('disabled',true);
			} 	
			if(Number($('#status').val())==2){
				$('.optionclass').hide();
				$('#calendarName').attr('disabled',true);
				$('#startPicker').attr('disabled',true);
				$('#endPicker').attr('disabled',true);
				$('.delete_calendar').remove(); 
				$('.save_calendar').remove(); 
			}
		}else{
			$('.delete_calendar').remove();
		}

	  
		
	 if($('#page-val').val()!="add")
		$('.skip').show(); 
	 else
		$('.skip').remove();  

	 $('.financialYear').combobox({ 
       selected: function(event, ui){   
          var temparray=$(this).val().split('#@');
          var calendarId=Number(temparray[0]);
          var showPage=$('#page-val').val();
          if(calendarId>0){
        	  $.ajax({
	  				type:"POST",
	  				url:"<%=request.getContextPath()%>/show_updatecalendar.action",
	  				data:{calendarId:calendarId, showPage:showPage},
	  			 	async: false,
	  			    dataType: "html",
	  			    cache: false,
	  				success:function(result){
	  					$("#main-wrapper").html(result);
	  				}
  		  	   });
           }else{return false;} 
       }
	}); 
	
		
		
	$('#tempcalendarName').val($('#calendarName').val().trim());	
	 
	var endDate=$('#endPicker').val();
	if(endDate=="31-Dec-9999"){
		$('#endPicker').val(""); 
	} 
	$jquery("#editCalendarVal").validationEngine('attach');
	 	
	 $('.formError').remove();
	 $('.disableedit').hide();

	 
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'}); 
	 var tempvar=$('.tab>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#DeleteImage_'+rowid).hide(); 
	
	 $(".addData").click(function(){ 
		 var checkflag=validateName($('#calendarName').val().trim().toLowerCase());  
		 if(checkflag==true){
		 if($('#openFlag').val() == 0) {
			 slidetab=$(this).parent().parent().get(0);   
			 //Find the Id for fetch the value from Id
			 var tempvar=$(this).parent().attr("id");
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]); 
			 if($jquery("#editCalendarVal").validationEngine('validate')){  
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/gl_calendar_period_add_action.action",
						data:{rowId:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				}); 
			 }
			 else{
				return false;
			 }	
			}else{
				return false;
			}
		}else{
			$(".calendarflag").animate({"color": "blue"}, 1000);
			$(".calendarflag").animate({"color": "red"}, 1000); 
			return false;
		}
	});

	$(".editData").click(function(){ 
		 var checkflag=validateName($('#calendarName').val().trim().toLowerCase());
		 if(checkflag==true){
			 if($('#openFlag').val() == 0) { 
			 slidetab=$(this).parent().parent().get(0);  
			 
			 if($jquery("#editCalendarVal").validationEngine('validate')){  
				 if($('#availflag').val()==1){
					 
					//Find the Id for fetch the value from Id
				var tempvar=$(this).attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				$("#AddImage_"+rowid).hide();
				$("#EditImage_"+rowid).hide();
				$("#DeleteImage_"+rowid).hide();
				$("#WorkingImage_"+rowid).show();   
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/gl_calendar_period_edit_action.action",
					 data:{rowId:rowid,addEditFlag:"E"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){
					
				   $(result).insertAfter(slidetab);
	
				 //Bussiness parameter
		   			var name=$('#name_'+rowid).text();
		   			var startTime=$('#startTime_'+rowid).text(); 
		   			var endTime=$('#endTime_'+rowid).text(); 
		   			var extention=$('#extention_'+rowid).text();
					var periodstatus=$('#statusline_'+rowid).val();
		   			$('#name').val(name);
		   			$('#extention').val(extention);
		   			$('#startPicker_').val(startTime);
		   			$('#endPicker_').val(endTime);
		   		 	$('.periodStatus').val(periodstatus);
				    $('#openFlag').val(1);
				   
				},
				error:function(result){
				//	alert("wee"+result);
				}
				
				});
				 }
					else{
						$('#temperror').hide().html("Calendar name <span style='font-weight:bold; display: inline;'>\""+name+ "\"</span> is already is use !!!").slideDown(1000);
					}			
			 }
			 else{
				 return false;
			 }	
			 }else{
				 return false;
			 }
		 } 
		 else{
				$(".calendarflag").animate({"color": "blue"}, 1000);
				$(".calendarflag").animate({"color": "red"}, 1000); 
				return false;
			}
	});
	 
	 $(".delrow").live('click',function(){ 
		 var checkflag=validateName($('#calendarName').val().trim().toLowerCase());
		 if(checkflag==true){
		 slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		var tempvar=$(slidetab).attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		var lineId=$('#lineId_'+rowid).text(); 
		var flag=false; 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/gl_calendar_period_delete.action", 
				 data:{rowId:lineId},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
					 $('.formError').hide(); 
                     $('#temperror').hide();
                     $('#wrgNotice').hide(); 
					 $('.tempresult').html(result);   
					 if(result!=null){
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
                              flag = true;
					    	else{	
						    	flag = false; 
						    	$("#temperror").html($('#returnMsg').html());
						    	 $("#temperror").hide().slideDown(); 
					    	} 
					     }  
				},
				error:function(result){
					 $("#temperror").html($('#returnMsg').html());
					 $('.tempresult').html(result);   
					 return false;
				}
					
			 });
			 if(flag==true){  
	        		 var childCount=Number($('#childCount').val());
	        		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount); 
	        		 }  
	        		if(childCount==0){
	        			 $('#calendarName').attr("disabled",false);
	 					$('#startPicker').attr("disabled",false);
	 					$('#endPicker').attr("disabled",false);  
	        		 } 
	        		 $(this).parent().parent('tr').remove(); 
	        		//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){   
	     				i=i+1;
	     				$($(this).children().get(5)).text(i);
   					 });  
	     			return false; 
			}  
			 else{
			 $("#wrgNotice").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"});   
			 $("#wrgNotice").hide().html("Please insert record of delete.").slideDown(1000); 
			 return false; 
		 }
	 }	 else{
					$(".calendarflag").animate({"color": "blue"}, 1000);
					$(".calendarflag").animate({"color": "red"}, 1000); 
					return false;
				}	 
		return false; 
	 });

	 $('.save_calendar').click(function(){ 
		 var checkflag=validateName($('#calendarName').val().trim().toLowerCase());
		 if(checkflag==true){
		 var calHeaderId=$("#calHeaderId").val(); 
		 var calName=$("#calendarName").val();
		 var frmDate=$("#startPicker").val();
		 var toDate=$("#endPicker").val();
		 var status=$('#status').val();
		
		 if($jquery("#editCalendarVal").validationEngine('validate')){  
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/gl_calendar_add_save_action.action", 
				 data:{calendarId:calHeaderId, name:calName, startTime:frmDate, endTime:toDate, status:status},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){  
					$('.formError').hide();  
					$(".tempresult").html(result); 
					var message=$('.tempresult').html().trim(); 
					var id=$('#page-val').val();
					if(id!="" && id=="add-customise" && message!=""){
						var showPage="add-customise";
						 $.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/chart_of_accounts_coaaddentry.action", 
					     	async: false,
					     	data:{showPage:showPage},
							dataType: "html",
							cache: false,
							success: function(result){  
								$("#DOMWindow").html(result); 
								$.scrollTo(0,300);
							} 		
						}); 
					 } 
					 else{
						var tempmessage=$('.tempresult').html().trim(); 
						var temparray=tempmessage.split("(");
						message=temparray[0];
						tempmessage = temparray[1].substring(0, temparray[1].length-1).toLowerCase(); 
						if(tempmessage=="success"){
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_calendarentry.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(message).slideDown(1000); 
									$('#success_message').delay(2000).slideUp(1000);
									return false;
								}
						 	});
						}else{
							$('#error_message').hide().html(message).slideDown(1000);
							$('#error_message').delay(2000).slideUp(1000);
							return false;
						}
				   }
			    } 
		   }); 
		 }
		 else{
			 return false;
		 }	
		 }	 else{
				$(".calendarflag").animate({"color": "blue"}, 1000);
				$(".calendarflag").animate({"color": "red"}, 1000); 
				return false;
			}	 	  
	 });

	 $('.delete_calendar').click(function(){
		 var cnfrm = confirm("Warning: Delete Financial Year?");
		 var calHeaderId=$("#calHeaderId").val(); 
			if(!cnfrm) {
				return false;
			}else{
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/gl_calendar_delete.action", 
				 	async: false,
				 	data:{calendarId:calHeaderId},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$(".tempresult").html(result); 
						var message=$('.tempresult').html().trim(); 
						var tempmessage=message; 
						var temparray=tempmessage.split("(");
						message=temparray[0];
						tempmessage = temparray[1].substring(0, temparray[1].length-1).toLowerCase(); 
						if(tempmessage=="success"){
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_calendarentry.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(message).slideDown(1000); 
									$('#success_message').delay(2000).slideUp(1000);  
								}
						 	});
						}else{
							$('#error_message').hide().html(message).slideDown(1000);
							$('#error_message').delay(2000).slideUp(1000);  
						}
					}
			 	}); 
			} 
	 });
      
	 $(".cancel_calendar").click(function(){
		 if($('#page-val').val()!=null && $('#page-val').val()!="" && $('#page-val').val()=="add-customise"){
			 $.ajax({
				 type:"POST",
				 url:"<%=request.getContextPath()%>/show_setup_wizard.action", 
				 async: false,
				 dataType: "html",
				 cache: false,
				 success:function(result){  
					 $('.formError').remove(); 
					 $('#DOMWindow').remove();
					 $('#DOMWindowOverlay').remove();
					 $('#main-wrapper').html(result);  
				 },
			     error:function(result){
					 $("#main-wrapper").html(result);
				 }
			 });
		}else{
				window.location.reload();
			} 
	 });

	 $('.addrows').click(function(){
		  var i=Number(0); 
		  var id=Number(0);
		  $('.rowid').each(function(){
			  id=0;
			  var temparray=$(this).attr('id');
			  var idarray=temparray.split('_');
			  var rowid=Number(idarray[1]);   
			  id=rowid+1;
		  }); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/gl_createcalendar_addrows.action",
				data:{rowId:id}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('.tab tr:last').before(result);
				 if($(".tab").height()>255)
					 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
				//To reset sequence 
				 $('.rowid').each(function(){  
						i=i+1;
						var rowid=Number($(this).attr('id').split('_')[1]);
						$('#lineId_'+rowid).text(i);
					});  
				} 
 			
			});
		}); 
	 
	 $('.skip-cal').click(function(){ 
			var showPage="add-customise";
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/chart_of_accounts_coaaddentry.action", 
		     	async: false,
		     	data:{showPage:showPage},
				dataType: "html",
				cache: false,
				success: function(result){  
					$("#DOMWindow").html(result); 
					$.scrollTo(0,300);
				} 		
			});  
		 });
	 $('#calendarName').change(function(){  
		 var flag=validateName($('#calendarName').val().toLowerCase().trim()); 
		 if(flag==false){
			 $('.calendarflag').hide().html("Already exits.").slideDown(1);
		 }else{
			 $('.calendarflag').hide();
		 }
	 });
	
 });
function validateName(name){
	var checkflag=true;
	$('.financialYear option').each(function(){ 
		var tempname=$(this).val().trim().toLowerCase();
		var arrayval=tempname.split('#@');
		var financialname=arrayval[1];
		var tempcalendarName=$('#tempcalendarName').val().trim().toLowerCase();
		if(tempcalendarName!=name){
			if(checkflag==true && name==financialname){
				checkflag=false;
			} 
		}
	}); 
	return checkflag;
} 
//Prevent selection of invalid dates through the select controls
 function checkLinkedDays() {
     var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
     $('#selectedDay option:gt(27)').attr('disabled', false);
     $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
     if ($('#selectedDay').val() > daysInMonth){
         $('#selectedDay').val(daysInMonth);
     }
 } 
 function customRange(dates) {
 	if (this.id == 'startPicker') {
 	 	if($('#startPicker').val()!=""){
 	 	 	$('.startPickerformError').hide();
 	 		addPeriods(); 
 	 	}
 		$('#endPicker').datepick('option', 'minDate', dates[0] || null); 
 		
 	}
 	else{
 		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);  
 	} 
 }
function addPeriods(){
	var date = new Date($('#startPicker').datepick('getDate')[0].getTime());  
	$.datepick.add(date, parseInt(364, 10), 'd');
	$('#endPicker').val($.datepick.formatDate(date)); 
}
</script>
<div id="main-content">
<div style="display:none;" class="tempresult"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			financial year
		</div>
	<form name="newFields" id="editCalendarVal"> 
	<input type="hidden" name="page" id="page-val" value="${showPage}"/>
	<input type="hidden" name="tempcalendarName" id="tempcalendarName" value="${CALENDAR_DETAILS.name}"/>
	<input type="hidden" name="financial-status" id="financial-status" value="${FINANCIAL_STATUS}"/>
	 	  <div class="portlet-content width100"> 
	 	   <div id="success_message" class="response-msg success ui-corner-all width90" style="display:none;"></div> 
	 	   <div id="error_message" class="response-msg error ui-corner-all width90" style="display:none;"></div> 
	 	   	<input type="hidden" name="childCount" id="childCount"  value="${fn:length(PERIOD_DETAILS)}"/> 
	 		<div class="width100" id="hrm">  
				<div class="width48 float-right">
					<fieldset> 		
						<div>
							<label class="width30">Show All Financial Year</label>  
							<select name="financialYear" id="financialYear" class="financialYear">
								<option value="">Search</option>
								<c:choose>
									<c:when test="${FINANCIAL_YEAR ne null && FINANCIAL_YEAR ne ''}">
										<c:forEach var="bean" items="${FINANCIAL_YEAR}">
											<c:choose>
												<c:when test="${bean.status ==1}">
													<option value="${bean.calendarId}#@${bean.name}">${bean.name} (Active Year)</option>
												</c:when>
												<c:otherwise>
													<option value="${bean.calendarId}#@${bean.name}">${bean.name}</option>
												</c:otherwise>
											</c:choose> 
										</c:forEach> 
									</c:when>
								</c:choose>
							</select>
						</div>				 
					</fieldset> 
				</div>
			<div class="width50 float-left">
				<fieldset> 
					<div>
						<label class="width30">Financial Name<span style="color:red;">*</span></label>  
							<input type="text" tabindex="1" name="calendarName" value="${CALENDAR_DETAILS.name}" id="calendarName"
								 class="width50 validate[required]">
							<input type="hidden" name="editCalHederId" value="${CALENDAR_DETAILS.calendarId}" id="calHeaderId"/> 
							<span class="calendarflag"></span>
					</div>  
					<div>
						<label class="width30">From Date<span style="color:red;">*</span></label>  
						<input type="text" tabindex="2" name="startDate" readonly="readonly" value="${CALENDAR_DETAILS.startTime}"
							 id="startPicker" class="width50 validate[required]">
					</div>
					<div>
						<label class="width30">To Date</label>  
						<input id="endPicker" type="text" class="width50" readonly="readonly" name="endDate" 
							tabindex="3" value="${CALENDAR_DETAILS.endTime}">
					</div>
					<div>
						<label class="width30">Year Status<span style="color:red;">*</span></label>
						<select name="status" id="status" class="width50 validate[required]"> 
							<option value="1" selected="selected">Active</option>
							<option value="2">Inactive</option>
						</select>
						<input type="hidden" name="tempstatus" id="tempstatus" value="${CALENDAR_DETAILS.status}"/>
					</div>
				</fieldset>	
			</div> 
		</div>
	</div>	
	<div class="clearfix"></div> 
	<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>   
	<div class="portlet-content">  
	<div id="hrm" class="hastable width100">  
	<fieldset>
		<legend>Period Details<span
									class="mandatory">*</span></legend> 
	 	<div  class="childCountErr response-msg error ui-corner-all" style="width:80%; display:none;"></div> 
		<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>
		<input type="hidden" name="trnValue" value="" readonly="readonly" id="transURN"/> 
		<input type="hidden" name="openFlag" id="openFlag"  value="0"/>  
		<table id="hastab" class="width100"> 
			<thead>
				<tr>   
					<th class="width20"><fmt:message key="accounts.calendar.label.periodname"/></th>
					<th class="width5"><fmt:message key="accounts.calendar.label.fromdate"/></th>
					<th class="width5"><fmt:message key="accounts.calendar.label.todate"/></th>
					<th class="width5"><fmt:message key="accounts.calendar.label.adjustingperiod"/></th>
					<th class="width5">Period Status</th>
					<th style="width:2%;" class="optionclass"><fmt:message key="accounts.calendar.label.options"/></th>
				</tr>
			</thead>
			<tbody class="tab">
			 <c:forEach items="${requestScope.PERIOD_DETAILS}" var="result" varStatus="status">                         
		        <tr id="fieldrow_${status.index+1}" class="rowid">
		            <td class="width20" id="name_${status.index+1}">${result.name}</td>
		            <td class="width5" id="startTime_${status.index+1}">${result.startTime}</td>
		           	<td class="width5" id="endTime_${status.index+1}">${result.endTime}</td>
		            <td class="width5" id="extention_${status.index+1}">${result.extention}</td>
		            <c:choose>
		            	<c:when test="${result.status eq 1}">
		            		<td class="width5" id="periodstatus_${status.index+1}">Open</td>
		            	</c:when>
		            	<c:when test="${result.status eq 2}">
		            		<td class="width5" id="periodstatus_${status.index+1}">Close</td>
		            	</c:when>
		            	<c:when test="${result.status eq 3}">
		            		<td class="width5" id="periodstatus_${status.index+1}">Inactive</td>
		            	</c:when>
		            	<c:otherwise>
		            		<td class="width5" id="periodstatus_${status.index+1}">Temporary Closed</td>
		            	</c:otherwise>
		            </c:choose> 
		            <td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
		            <td style="display:none;">
		            	<input type="hidden" name="periodId_${status.index+1}" id="periodId_${status.index+1}"  value="${result.periodId}"/> 
				        <input type="hidden" name="statusline_${status.index+1}" id="statusline_${status.index+1}"  value="${result.status}"/>  
		            </td>
		            <c:choose>
		            	<c:when test="${result.status ne 2}">
				             <td style="width:2%;" class="optionclass">  
	                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
							 	 <span class="ui-icon ui-icon-plus"></span>
							   </a>
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
									<span class="ui-icon ui-icon-wrench"></span>
							   </a> 
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span>
							   </a>
							    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="WorkingImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
									<span class="processing"></span>
							   </a> 
							</td> 
					</c:when>
					<c:otherwise>
						<td style="width:2%;" class="optionclass"> </td>
					</c:otherwise>
					</c:choose>
		        </tr> 
		    </c:forEach> 
		    <c:choose>
		    	<c:when test="${CALENDAR_DETAILS.status ne 2}">
		    		 <c:forEach var="i" begin="${fn:length(PERIOD_DETAILS)+1}" end="${fn:length(PERIOD_DETAILS)+2}" step="1" varStatus ="status">  
						<tr id="fieldrow_${i}" class="rowid"> 
							<td class="width20" id="name_${i}"></td>
				            <td class="width5" id="startTime_${i}"></td>
				           	<td class="width5" id="endTime_${i}"></td>
				            <td class="width5" id="extention_${i}"></td>
				            <td class="width5" id="periodstatus_${i}"></td>
				            <td style="display:none;" id="lineId_${i}">${i}</td> 
				            <td style="display:none;">
				            	<input type="hidden" name="periodId_${i}" id="periodId_${i}"  value="0"/>   
					 			<input type="hidden" name="statusline" id="statusline_${i}"/>  
				            </td>
					 		<td  style="width:2%;" id="option_${i}"> 
							  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}"  title="Add Record">
							 	 <span class="ui-icon ui-icon-plus"></span>
							   </a>	
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  id="EditImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
									<span class="ui-icon ui-icon-wrench"></span>
								</a> 
								<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  id="DeleteImage_${i}" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span>
								</a>
								<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
									<span class="processing"></span>
								</a>
							</td>  
						</tr>
		 			</c:forEach>
		    	</c:when>
		    </c:choose> 
		 </tbody>
	   </table>
   </fieldset>
  </div>  
 </div>  
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
<div class="portlet-header ui-widget-header float-right cancel_calendar" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>
<div class="portlet-header ui-widget-header float-right delete_calendar" style="cursor:pointer;"><fmt:message key="accounts.common.button.delete"/></div> 
<div class="portlet-header ui-widget-header float-right save_calendar" style="cursor:pointer;"><fmt:message key="accounts.common.button.update"/></div>
</div>
<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
</div>
<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
		<div class="portlet-header ui-widget-header float-left skip-cal" style="cursor:pointer;">skip</div> 
</div>
<input type="hidden" name="availflag" id="availflag" value="1"/>
</form>
</div>
</div>