<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />

<style type="text/css">
#purchase_print {
	overflow: hidden;
} 
.portlet-content{padding:1px;}
.buttons { margin:4px;}

.ui-widget-header{
	padding:4px;
}

	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}

.buttons {
	margin: 4px;
}

#filter-form td {
	padding: 5px!important;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 90%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript"> 

var oTable; var selectRow=""; var aSelected = []; var aData="";
 var supplierId = 0; var productId = 0; var datefrom = ""; var dateto ="";
 var selectedDepartmentId = 0;
 var selectedSectionId = 0;
 var selectedPersonId = 0;
$(function(){  

	$('#sections').combobox({
		selected : function(event, ui) {
			selectedSectionId = $('#sections :selected').val(); 
		}
	});
	
		
	$('#departments').combobox({
		selected : function(event, ui) {
			selectedDepartmentId = $('#departments :selected').val(); 
		}
	});

	$('#persons').combobox({
		selected : function(event, ui) {
			selectedPersonId = $('#persons :selected').val(); 
		}
	});
	
	
	
	 $("#productId").multiselect();
		
		 $(".print-call").click(function(){
			var actionUrl= $('input[name=actionRadio]:checked', '#filter-form').val();
			var actionId= $('input[name=actionRadio]:checked', '#filter-form').attr("id");
			 datefrom=$('#fromDate').val();
			 dateto=$('#endDate').val();   
			 var otherInfo=$('#otherInfo').val();
			 var products = generateDataToSendToDB("productId"); 
				$.ajax({
					type: "POST", 
					url: '<%=request.getContextPath()%>/'+actionUrl+'.action', 
			     	async: false,
			     	data:{fromDate:datefrom,toDate: dateto,
			     		products:products,departmentId:selectedDepartmentId,
			     		personId:selectedPersonId,sectionId:selectedSectionId,
			     		otherInfo:otherInfo
			     		},
					dataType: "html",
					cache: false,
					success: function(output){ 
						var myWindow=window.open('printing/sys_empty_template.jsp','_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
						myWindow.document.write(output);
						myWindow.focus();
						myWindow.location.reload();
					} 		
				}); 
			 }); 
		  
		  
		  
		  $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
			$('#selectedMonth,#linkedMonth').change();
			$('#l10nLanguage,#rtlLanguage').change();
			if ($.browser.msie) {
			        $('#themeRollerSelect option:not(:selected)').remove();
			}
			$('#fromDate,#endDate').datepick({
			 onSelect: customRange, showTrigger: '#calImg'}); 
			
			/*  $(".actionRadio").click(function(){
				 if($(this).attr("id")=='periodicStock'){
					 $('#fromDate').val("");
					 $('#endDate').val("");
					 $('#fromDate').attr("disabled", "disabled"); 
					 $('#endDate').attr("disabled", "disabled"); 
				 }else{
					 $('#fromDate').removeAttr("disabled"); 
					 $('#endDate').removeAttr("disabled"); 
				 }
			 }); */
}); 
	
function printCal(myWindow){
	myWindow.focus();
	myWindow.print();
}
function generateDataToSendToDB(source){
	var typeList=new Array();
	var ids="";
	$('#'+source+' option:selected').each(function(){
		typeList.push($(this).val());
	});
	for(var i=0; i<typeList.length;i++){
		if(i==(typeList.length)-1)
			ids+=typeList[i];
		else
			ids+=typeList[i]+",";
	}
	
	return ids;
}
	
		
		//Prevent selection of invalid dates through the select controls
		function checkLinkedDays() {
			var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
					'#selectedMonth').val());
			$('#selectedDay option:gt(27)').attr('disabled', false);
			$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
					true);
			if ($('#selectedDay').val() > daysInMonth) {
				$('#selectedDay').val(daysInMonth);
			}
		}
		function customRange(dates) {
			if (this.id == 'fromDate') {
				$('#endDate').datepick('option', 'minDate', dates[0] || null); 
			} else {
				$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
			}
		}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Receive & Issuance
		</div>
		<div class="portlet-content">
			<form id="filter-form">
			<div class="width100 float-left">
				<table width="90%" >
					<tr>
						<td colspan="2">
							<div>
								<input type="radio" name="actionRadio" class="width5 actionRadio" checked="checked" id="issueVsReceive" value="receive_vs_issuance_report_printout"/> 
								<label class="width20">Receive Vs Issuance</label>
								
								<input type="radio" name="actionRadio" class="width5 actionRadio" id="sectionIssues" value="issuance_by_section_report_printout"/> 
								<label class="width20">Material Issuance</label>
								
								<input type="radio" name="actionRadio" class="width5 actionRadio" id="monthlyReport" value="receive_vs_issuance_monthly_report_printout"/> 
								<label class="width10">Monthly Report</label>
								
								<input type="radio" name="actionRadio" class="width5 actionRadio" id="periodicStock" value="periodic_stock_report_printout"/> 
								<label class="width10">Stock Report</label>
							</div>
						</td>
						<td>Issuance Section :</td>
						<td>
							<div>
								<select id="sections">
									<option value="">Select</option>
									<c:forEach var="sec" items="${SECTIONS}">
										<option value="${sec.lookupDetailId}">${sec.displayName}</option>						
									</c:forEach>
								</select>
							</div>
						</td> 
					</tr>
					<tr>
						
						<td>From Date :</td>
						<td>
							<div>
								<input type="text"
								name="fromDate" class="fromDate" id="fromDate"
								readonly="readonly">
							</div>
						</td>
						<td>Deaprtment :</td>
						<td>
							<div>
								<select id="departments">
									<option value="">Select</option>
									<c:forEach var="location" items="${DEPARTMENT_LIST}">
										<option value="${location.departmentId}">${location.departmentName}</option>						
									</c:forEach>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>To Date :</td>
						<td>
							<div>
								 <input type="text"
							name="toDate" class="toDate" id="endDate"
							readonly="readonly">
							</div>
						</td>
						<td>Requisition Person :</td>
						<td>
							<div>
								<select id="persons">
									<option value="">Select</option>
									<c:forEach var="person" items="${PERSON_LIST}">
										<option value="${person.personId}">${person.firstName} ${person.lastName}</option>						
									</c:forEach>
								</select>
							</div>
						</td> 
					</tr>
					<tr>
						<td>Print Note's :</td>
						<td>
							<div>
								 <input type="text"
							name="otherInfo" class="otherInfo" id="otherInfo"
							>
							</div>
						</td>
						
					</tr>
					
				</table>

			</div>
			</form>
			<div class="width100 float-left" style="padding: 5px;"> 
				<div>
					<label class="width100" for="productId">Product</label>
					<select id="productId" class="product width100" name="product" multiple="multiple">
						<c:forEach var="product" items="${PRODUCT_INFO}">
							<option value="${product.productId}">${product.code} -- ${product.productName}</option>						
						</c:forEach> 
					</select>	
					
				</div>  
				
			</div> 
 			
			
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>