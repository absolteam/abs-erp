<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
.inner-pos-div>div>span {
	font-size: 14px !important;
}

.c_c_c_c>div {
	font-size: 14px !important;
	font-weight: bold;
}

.c_c_c_c>div>span {
	font-size: 14px !important;
}

.pos-right-content>div>label {
	font-size: 14px !important;
}

.pos-right-content>div>span {
	font-size: 14px !important;
}

#transaction-block>label {
	font-size: 14px !important;
}

#amount-block>label {
	font-size: 14px !important;
}

#coupon-block>label {
	font-size: 14px !important;
}

#exchange-block>label {
	font-size: 14px !important;
}

#tabs-1{padding:2px!important;}
</style>
<script type="text/javascript">
	var slidetab = "";
	var salesOrderDetails = "";
	var chargesDetails = "";
	var tempid = "";
	var additionalCharges = []; 
	var posSessionId=null;
	
	var closeWin = 1;
	var printer = [];
	var posPrintReciptContent = "";
	
	$(function() { 
		
		$jquery('#inlineTargetKeypad').keypad({ keypadOnly: false,target: $jquery('.inlineTarget:first'),closeText: 'Done',
				separator: '|', prompt: '', 
		    layout: ['6|7|8|9|' + $jquery.keypad.BACK, 
		        '2|3|4|5|' + $jquery.keypad.CLEAR, 
		        '1|0|.|00|' + $jquery.keypad.CLOSE,  
		        '10|20|50|100',
		        '200|500|1000'],onClose: function(value, inst) { 
		        	$(".inlineTarget").trigger( "change" );
			    }, beforeShow: function(div, inst) { 
			    	$(div).find(".keypad-key").css( "width", "17%" );
			    	$(div).find(".keypad-special").css( "width", "23%" );
		    	}}); 
		 
		
		var keypadTarget = null; 
		$jquery('.inlineTarget').focus(function() { 
		    if (keypadTarget != this) { 
		        keypadTarget = this; 
		        $jquery('#inlineTargetKeypad').keypad('option', {target: this}); 
		    } 
		});
		
		
		$jquery('.discount,.charges').keypad({ keypadOnly: false,closeText: 'Done',
			separator: '|', prompt: '', 
	    layout: ['7|8|9|' + $jquery.keypad.BACK, 
	        '4|5|6|' + $jquery.keypad.CLEAR, 
	        '1|2|3|' + $jquery.keypad.CLOSE, 
	        '.|0|00'],onClose: function(value, inst) { 
	        	$(this).trigger( "change" );
		    }, beforeShow: function(div, inst) { 
		        
	    	}}
		
		);  
		 
 		 $jquery("#pointOfSaleValidation").validationEngine('attach');  

		$('.pos-invoice-discard').click(function(){  
			var pointOfSaleId = Number($('#pointOfSaleId').val());
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/delete_saved_pos_order.action",
				data:{pointOfSaleId: pointOfSaleId},
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove(); 
					$('#pos-invoice-popup').dialog('destroy');		
					$('#pos-invoice-popup').remove(); 
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove();  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$("#main-wrapper").html(result);
				}
			});
		});  
		
		$('.pos-invoice-close').click(function(){   
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/discard_pointofsales.action", 
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove(); 
					$('#pos-invoice-popup').dialog('destroy');		
					$('#pos-invoice-popup').remove(); 
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove();  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$("#main-wrapper").html(result);
				}
			});
		});  
		
		$('.pos-receipt-copy').click(function(){  
			printHTML();
			return false;
		});
		
		$('.secret-pin-validate').click(function(){
			$('.ui-dialog-titlebar').remove();  
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_pointofsale_secretpin.action", 
				async: false,
				dataType: "html",
				cache: false,
				success:function(result){  
					 $('#temp-result').html(result); 
					 $('.callJq').trigger('click');  
					 $('#employeeId').focus();
				}
			}); 
		});
		
		$('.callJq').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#temp-result', 
			loaderHeight:16, 
			loaderWidth:17 
		}); 
		
		$('.pos-receipt').click(function(){  
			var pointOfSaleId = Number($('#pointOfSaleId').val());
			var personId = Number($('#personId').val());
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_dispatch_pointofsale.action", 
				async: false,
			    dataType: "json",
			    data: {pointOfSaleId: pointOfSaleId, personId: personId},
			    cache: false,
			    success:function(response){   
					if (response.returnMessage == "SUCCESS"){ 
						$('.pos-invoice-discard').remove();
						$('.pos-invoice-reset').remove();
						$('.pos-receipt-print').remove();
						$('.pos-receipt-save').remove();
						$('.pos-receipt-dispatch').remove();
						$('.pos-back').remove();
						$('#common-popup-new').dialog('destroy');		
						$('#common-popup-new').remove(); 
						$('.pos-receipt-copy').show();
						$('.pos-receipt').remove();
						$('#pos-invoice-popup').dialog('destroy');		
						$('#pos-invoice-popup').remove();  
						$('#common-popup-new').dialog('destroy');		
						$('#common-popup-new').remove();  
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						printHTML();
					} 
				} 
			}); 
 			return false;
		});
		
		$('#pos-invoice-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
		
		$('#pos-customerorder-discard').live('click',function(){ 
			$('#pos-invoice-popup').dialog('close'); 
		});  
		
		$('.paymentMode').click(function(){   
			$('#coupon-block').hide();
			$('#exchange-block').hide();
			$('#transaction-block').hide();
			var paymentMode = getRowId($(this).attr('id'));
			$('.paymentMode').each(function(){ 
 				if($($($(this).parent()).get(0)).hasClass('selected')){
					$($($(this).parent()).get(0)).removeClass('selected');
				}
			});
			$($($(this).parent()).get(0)).addClass('selected');
			$('#receiveAmount').focus();
			if(paymentMode == 4){
				$('#amount-block').hide();
				$('#coupon-block').show();
				$('#transaction-block').hide();
			}else if(paymentMode == 5){
				$('#exchange-block').show();
				$('#amount-block').hide();
				$('#transaction-block').hide();
			}else if(paymentMode == 2 || paymentMode == 3 ){ 
				$('#amount-block').show();
				$('#transaction-block').show();
			}else{ 
				var receiveCashAmount = convertStringToDouble($.trim($('#outStanding').text())); 
				if(receiveCashAmount != 0 && receiveCashAmount != ''){
					var receiveAmount = Number($.trim($('#receiveAmount').val()));
					if(receiveAmount == 0)
						$('#receiveAmount').val(receiveCashAmount);
					$('#amount-block').show();
				} 
			} 
  			showPaymentSummary(getRowId($(this).attr('id'))); 
		}); 

		$('#receiveAmount,#couponNumber,#exchangeReference').change(function(){   
			var paymentMode = Number(0);
			$('.paymentMode').each(function(){ 
				if($($($(this).parent()).get(0)).hasClass('selected')){
					paymentMode = getRowId($(this).attr('id'));
				}
			}); 
			showPaymentSummary(paymentMode); 
		}); 
		
		 
		posSessionId='${requestScope.posSessionId}';  
		
		$('#common-popup-new').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
	});

	function getRowId(id){   
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	 

	function showPaymentSummary(paymentMode) {  
 		var receiveAmount = Number($('#receiveAmount').val());  
 		var couponNumber =  Number($('#couponNumber').val());  
 		var exchangeReference = $.trim($('#exchangeReference').val());
 		var customerId = Number($('#customerId').val());   
 		var transactionNumber = $('#transactionNumber').val();
 		var dispatchDiscount =  Number($('#dispatchDiscount').val());  
 		var dispatchCost =  Number($('#dispatchCost').val());   
		if(paymentMode > 0 && (receiveAmount > 0 || couponNumber > 0 || exchangeReference != '')){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_pos_payment_summary.action",
						async : false,
						data : { 
							paymentMode : paymentMode,
							receiveAmount : receiveAmount,
							couponNumber : couponNumber,
							exchangeReference : exchangeReference,
							customerId : customerId,
							transactionNumber : transactionNumber,
							dispatchDiscount: dispatchDiscount,
							dispatchCost: dispatchCost
						},
						dataType : "json",
						cache : false,
						success : function(response) {
							$('.c_c_c_c').show();
							if (response.pointOfSaleVO != null) {
								$('#salesAmount').text(
										response.pointOfSaleVO.salesAmount);
								$('#additionalCost').text(
										response.pointOfSaleVO.additionalCost);
								$('#totalDue').text(
										response.pointOfSaleVO.totalDue);
								if (response.pointOfSaleVO.customerDiscount != null
										&& response.pointOfSaleVO.customerDiscount != '')
									$('#customerDiscount')
											.text(
													response.pointOfSaleVO.customerDiscount);
								if (response.pointOfSaleVO.cashReceived != null
										&& response.pointOfSaleVO.cashReceived != '') {
									$('#cashReceived')
											.text(
													response.pointOfSaleVO.cashReceived);
									$('#ch').show();
								}
								if (response.pointOfSaleVO.cardReceived != null
										&& response.pointOfSaleVO.cardReceived != '') {
									$('#cardReceived')
											.text(
													response.pointOfSaleVO.cardReceived);
									$('#cd').show();
								}
								if (response.pointOfSaleVO.chequeReceived != null
										&& response.pointOfSaleVO.chequeReceived != '') {
									$('#chequeReceived')
											.text(
													response.pointOfSaleVO.chequeReceived);
									$('#cq').show();
								}
								if (response.pointOfSaleVO.couponReceived != null
										&& response.pointOfSaleVO.couponReceived != '') {
									$('#couponReceived')
											.text(
													response.pointOfSaleVO.couponReceived);
									$('#cp').show();
								}
								if (response.pointOfSaleVO.exchangeReceived != null
										&& response.pointOfSaleVO.exchangeReceived != '') {
									$('#exchangeReceived')
											.text(
													response.pointOfSaleVO.exchangeReceived);
									$('#er').show();
								}
								$('#receivedTotal').text(
										response.pointOfSaleVO.receivedTotal);
								if (response.pointOfSaleVO.outStanding != null
										&& response.pointOfSaleVO.outStanding != '') {
									$('#outStanding').text(
											response.pointOfSaleVO.outStanding);
									$('.outstanding').show();
								} else
									$('.outstanding').hide();

								if (response.pointOfSaleVO.balanceDue != null
										&& response.pointOfSaleVO.balanceDue != '') {
									$('#balanceDue').text(
											response.pointOfSaleVO.balanceDue);
									$('.balancedue').show();
								} else
									$('.balancedue').hide();
							} else {
								$('#receipt-error').hide().html(
										response.returnMessage).slideDown();
								$('#receipt-error').delay(2000).slideUp();
							}
						}
					});
			$(
					'#receiveAmount,#couponNumber,#exchangeReference,#transactionNumber')
					.val('');
		}
		return false;
	}
	function checkModificationDeleteApprover(){
		var pointOfSaleId = Number($('#pointOfSaleId').val());
		var ordPrinted =false;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/check_pos_printorder.action",
			data:{pointOfSaleId:pointOfSaleId},
			async : false,
			dataType : "json",
			cache : false,
			success : function(response) {
				ordPrinted=response.printerFlag;
			}
		});
		if(ordPrinted){
			 $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup-new').dialog('open');
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/validate_approver.action",
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('.common-result-new').html(result);
				}
			});
			return false;
		}else{
			return true;
		}
	
	} 
</script>
<div id="main-content" style="overflow: hidden;">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"
			style="padding: 8px 4px 4px 8px !important; width: auto;">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> POS Sales
			Invoice
		</div>
		<form id="pointOfSaleValidation" method="POST"
			style="position: relative;">
			<div id="applet-result"></div>
			<div id="tabs"
				class="ui-tabs ui-widget ui-widget-content ui-corner-all width99"
				style="float: left; *float: none;">
				<input type="hidden" id="customerId"
					value="${requestScope.customerId}" />
				<input type="hidden" id="personId"/>
				<input type="hidden" id="dispatchCost"
					value="${requestScope.additionalCost}" />
				<input type="hidden" id="dispatchDiscount"
					value="${requestScope.customerDiscount}" /> <input type="hidden"
					id="shippingId" value="${requestScope.shippingId}" /> <input
					type="hidden" id="deliveryOption"
					value="${requestScope.deliveryOption}" />
					<input type="hidden" id="pointOfSaleId"
							value="${requestScope.pointOfSaleTempId}" /> 
				<div id="hrm" class="float-left width98">
					<fieldset style="margin: 0px;" class="width99">
						<ul
							class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99"
							style="margin: 0px;">
							<li
								class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a
								href="#tabs-1">Receipt Details </a></li>
							<li class="ui-state-default ui-corner-top ui-tabs-selected"><a
								href="#tabs-2" style="display: none;">Product Details </a></li>
							<li class="ui-state-default ui-corner-top ui-tabs-selected"><a
								href="#tabs-3" style="display: none;">Additional Charges </a></li>
						</ul>
						<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
							id="tabs-1">
							<div class="portlet-content">
								<div id="page-error"
									class="response-msg error ui-corner-all width90"
									style="display: none;"></div>
								<div class="tempresult" style="display: none;"></div>
								<div class="width100 float-left" id="hrm">
									<div class="float-left  width48">
										<fieldset style="min-height: 85px;">
											<table class="pos-item-table width100">
												<tr>
													<c:forEach var="paymentType" items="${PAYMENT_TYPE}"
														varStatus="status">
														<c:if test="${status.index % 2 eq 0}">
															<td>
																<div class="box-model"
																	style="background-color: #A9D0F5;">
																	<span style="font-size: 16px !important;"
																		class="paymentMode ${paymentType.key == 1 ? ' coins' : paymentType.key == 3 ? 'card_visa' : 'exchange'}"
																		id="paymentMode_${paymentType.key}">${paymentType.value}
																	</span>
																</div>
															</td>
														</c:if>
													</c:forEach>
												</tr>
												<tr>
													<c:forEach var="paymentType" items="${PAYMENT_TYPE}"
														varStatus="status">
														<c:if test="${status.index % 2 ne 0}">
															<td>
																<div class="box-model"
																	style="background-color: #A9D0F5;">
																	<span style="font-size: 16px !important;"
																		class="paymentMode ${paymentType.key == 2 ? 'bank_pencil' : paymentType.key == 4 ? 'coupon' : 'voucher'}"
																		id="paymentMode_${paymentType.key}">${paymentType.value}</span>
																</div>
															</td>
														</c:if>
													</c:forEach>
												</tr>
											</table>
										</fieldset>
									</div>
									<div class="float-right width48 pos-right-content">
										<div>
											<label class="width30"> Reference No</label> <span style="position: relative; top: 8px;"
												id="referenceNumber">${requestScope.salesRefNumber} </span>
										</div>
										<div>
											<label class="width30"> Sales Date </label> <span style="position: relative; top: 8px;"
												id="salesDate"> ${requestScope.salesDate} </span>
										</div> 
										<c:if test="${requestScope.customerId gt 0}">
											<div>
												<label class="width30"> Customer </label> <span style="position: relative; top: 8px;">
													${requestScope.customerName}</span>
											</div>
											<c:if test="${requestScope.shippingId ne null && requestScope.shippingId ne '' && 
												requestScope.shippingId gt 0 && requestScope.shippingId > 0}">
												<div>
													<label class="width30"> Shipping Address </label> <span style="position: relative; top: 8px;">
														${requestScope.shippingAddress} </span>
												</div>
											</c:if>
											<c:if test="${requestScope.customerCoupon ne null && requestScope.customerCoupon ne '' && 
												requestScope.customerCoupon gt 0}">
												<div>
													<label class="width30"> Coupon </label> <span style="position: relative; top: 8px;"
														id="issueCoupon">${requestScope.customerCoupon}</span>
												</div>
											</c:if>
										</c:if>
									</div>
								</div>
								<div class="clearfix"></div>
								<fieldset style="max-height: 385px; height: 380px; margin:3px;">
									<div class="pos-summary width100">
										<div class="width40 float-right">
											<div class="width80 float-left inner-pos-div">
												<div class="width50 float-left">
													<span>Sales Amount</span>
												</div>
												<div class="width30 float-right">
													<span id="salesAmount" class="right-align float-right">${requestScope.productTotalPrice}</span>
												</div>
												<div class="width50 float-left">
													<span>Additional Cost</span>
												</div>
												<div class="width30 float-right">
													<span id="additionalCost" class="right-align float-right">${requestScope.additionalCost}</span>
												</div>
												<c:if
													test="${requestScope.customerDiscount ne null && requestScope.customerDiscount ne ''
														&& requestScope.customerDiscount ne '0.00'}">
													<div class="width50 float-left">
														<span>Discount</span>
													</div>
													<div class="width30 float-right">
														<span id="customerDiscount"
															class="right-align float-right">${requestScope.customerDiscount}</span>
													</div>
												</c:if>
												<div class="width50 float-left">
													<span>Total Due</span>
												</div>
												<div class="width30 float-right">
													<span id="totalDue" class="right-align float-right">${requestScope.totalDues}</span>
												</div>
												<div class="width80 float-left">
													<span>Received</span>
													<div style="margin: 3px; padding: 0px 15px;"
														class="c_c_c_c">
														<div id="ch">
															Cash : <span id="cashReceived"
																class="right-align float-right"></span>
														</div>
														<div id="cd">
															Card : <span id="cardReceived"
																class="right-align float-right"></span>
														</div>
														<div id="cp">
															Coupon : <span id="couponReceived"
																class="right-align float-right"></span>
														</div>
														<div id="cq">
															Cheque : <span id="chequeReceived"
																class="right-align float-right"></span>
														</div>
														<div id="er">
															Exchange : <span id="exchangeReceived"
																class="right-align float-right"></span>
														</div>
													</div>
												</div>
												<div class="width30 float-right">
													<span id="receivedTotal" class="right-align float-right"></span>
												</div>
												<div class="clearfix"></div>
												<div class="pos-seperator float-right width30 outstanding"></div>
												<div class="clearfix"></div>
												<div class="width50 float-left outstanding">
													<span>Outstanding</span>
												</div>
												<div class="width30 float-right outstanding">
													<span id="outStanding" class="right-align float-right">${requestScope.totalDues}</span>
												</div>
												<div class="clearfix"></div>
												<div class="pos-seperator float-right width30"></div>
												<div class="clearfix"></div>
												<div class="width50 float-left balancedue"
													style="display: none;">
													<span>Balance Due</span>
												</div>
												<div class="width30 float-right">
													<span id="balanceDue" class="right-align float-right"></span>
												</div>
											</div>
										</div>
										<div class="width58 float-left">
											<div class="portlet-content">
												<div id="receipt-error"
													class="response-msg error ui-corner-all"
													style="display: none;"></div>
												<div id="transaction-block" style="display: none;">
													<label>Transaction Number</label> <input type="text"
														class="width40 validate[optional] inlineTarget"
														id="transactionNumber" />
												</div>
												<div id="amount-block">
													<label>Enter Amount</label> <input type="text"
														class="width40 validate[optional,custom[number]] right-align inlineTarget"
														id="receiveAmount" />
												</div>
												<div id="coupon-block" style="display: none;">
													<label>Coupon Number</label> <input type="text"
														class="width40 validate[optional,custom[onlyNumber]] inlineTarget"
														id="couponNumber" />
												</div>
												<div id="exchange-block" style="display: none;">
													<label>Exchange Number</label> <input type="text"
														class="width40 validate[optional] inlineTarget"
														id="exchangeReference" />
												</div>
											</div>
											<span id="inlineTargetKeypad" style="width: 100% !important;"></span>
										</div>
									</div>
									<div class="clearfix"></div>
									<div
										class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left "
										style="margin: 10px;"> 
										<div
											class="portlet-header ui-widget-header float-right pos-receipt-save"
											style="cursor: pointer; display: none; background: #0B610B; color: #fff; padding: 10px;">
											Save Order</div> 
										<div
											class="portlet-header ui-widget-header float-right pos-receipt-print"
											style="cursor: pointer; display: none; background: #01DF01; color: #fff; padding: 10px;">
											Print Order</div> 
									</div>
									<div
										class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
										style="margin: 10px;"> 
										<div
											class="portlet-header ui-widget-header float-right pos-invoice-discard"
											style="cursor: pointer; background: #DF0101; color: #fff; padding: 10px;">
											<fmt:message key="accounts.common.button.cancel" />
										</div> 
										<div
											class="portlet-header ui-widget-header float-right pos-invoice-close"
											style="cursor: pointer; background: #610B21; color: #fff; padding: 10px;">
											close
										</div>
										<div
											class="portlet-header ui-widget-header float-right pos-invoice-reset"
											style="cursor: pointer; display: none; background: #610B21; color: #fff; padding: 10px;">
											<fmt:message key="accounts.common.button.reset" />
										</div>
										<c:set var="display" value="block"/>
										<c:if test="${requestScope.showPinNumber eq true}">
											<c:set var="display" value="none"/>
												<div class="portlet-header ui-widget-header float-right secret-pin-validate"
															id="secret-pin-validate" style="cursor: pointer; background: #0B3B0B; color: #fff; padding: 10px;">Print
														Receipt
												</div> 
										</c:if>
										<div class="portlet-header ui-widget-header float-right pos-receipt"
											id="pos-receipt" 
												style="display: ${display}; cursor: pointer; background: #0B3B0B; color: #fff; padding: 10px;">Print
												Receipt
										</div>   
										<div
											class="portlet-header ui-widget-header float-right pos-receipt-copy"
											id="pos-receipt-copy"
											style="cursor: pointer; display: none; background: #0B3B0B; color: #fff; padding: 10px;">Print
											Receipt</div> 
										<div
											class="portlet-header ui-widget-header float-right pos-back"
											style="cursor: pointer; display: none; background: #AEB404; color: #fff; padding: 10px;">
											Back</div>

									</div>
								</fieldset>
							</div>
						</div>
						<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
							id="tabs-2" style="display: none;">
							<div class="portlet-content width100" id="hrm"
								style="margin-top: 10px;">
								<div id="hrm" class="hastable width100">
									<table id="hastab" class="width100">
										<thead>
											<tr>
												<th style="width: 8%;">Product</th>
												<th style="width: 3%;">Quantity</th>
												<th style="width: 3%;">Price</th>
												<th style="width: 2%;">Points</th>
												<th style="width: 3%;">Discount</th>
												<th style="width: 3%;">Mode</th>
												<th style="width: 3%;">Total</th>
												<th style="width: 1%;" class="options"><fmt:message
														key="accounts.common.label.options" />
												</th>
											</tr>
										</thead>
										<tbody class="tab">
											<c:forEach var="salesInvoice" items="${SALES_INVOICE}"
												varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
													<td><input type="hidden" name="productId"
														id="productid_${status.index+1}"
														value="${salesInvoice.productId}" /> <span
														class="width98" id="product_${status.index+1}">${salesInvoice.productName}</span>
													</td>
													<td id="productQty_${status.index+1}" class="productQty">
														${salesInvoice.quantity}</td>
													<td id="amount_${status.index+1}"
														class="amount right-align">${salesInvoice.displayUnitPrice}</td>
													<td id="productPoint_${status.index+1}"
														class="productPoint"><c:if
															test="${salesInvoice.totalPoints gt 0}">
												${salesInvoice.totalPoints}</c:if>
													</td>
													<td><input type="text" name="discount"
														id="discount_${status.index+1}" class="discount width98" />
													</td>
													<td><select name="discountmode"
														id="discountmode_${status.index+1}"
														class="discountmode width98">
															<option value="-1">Select</option>
															<option value="1">Percentage</option>
															<option value="0">Amount</option>
													</select></td>
													<td class="right-align" id="totalAmount_${status.index+1}">${salesInvoice.displayPrice}</td>
													<td style="width: 1%;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status.index+1}"
														style="cursor: pointer; display: none;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status.index+1}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}"
														style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
							id="tabs-3" style="display: none;">
							<div class="hastable width100 float-left"
								style="margin-top: 10px;">
								<table id="hastab_charges" class="width100">
									<thead>
										<tr>
											<th class="width10">Charges Type</th>
											<th style="width: 5%;">Charges</th>
											<th style="width: 1%;" class="options"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tabcharges">
										<c:forEach begin="0" end="1" step="1" varStatus="status1">
											<tr class="rowidcharges"
												id="fieldrowcharges_${status1.index+1}">
												<td id="chargesLineId_${status1.index+1}"
													style="display: none;">${status1.index+1}</td>
												<td><select name="chargesTypeId"
													id="chargesTypeId_${status1.index+1}"
													class="chargestypeid width90">
														<option value="">Select</option>
														<c:forEach var="TYPE" items="${CHARGES_TYPE}">
															<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
														</c:forEach>
												</select>
												</td>
												<td><input type="text" name="charges"
													id="charges_${status1.index+1}" class="charges width98"
													style="text-align: right;"></td>
												<td style="width: 1%;" class="opn_td"
													id="optioncharges_${status1.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
													id="AddImagecharges_${status1.index+1}"
													style="cursor: pointer; display: none;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
													id="EditImagecharges_${status1.index+1}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
													id="DeleteImagecharges_${status1.index+1}"
													style="cursor: pointer; display: none;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImagecharges_${status1.index+1}"
													style="display: none;" title="Working"> <span
														class="processing"></span> </a> <input type="hidden"
													name="posChargeId" id="posChargeId_${status1.index+1}" />
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="temp-result" style="display: none;"></div>
			<span class="callJq"></span>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div
					class="portlet-header ui-widget-header float-left addrowscharges"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="pos-invoice-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="pos-invoice-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div align="center" id="content">
				<h1 id="title" style="display: none;"></h1> 
				<applet id="qz" code="qz.PrintApplet.class"
					archive="${pageContext.request.contextPath}/printing/applet/qz-print.jar" 
					width="0" height="0" align="left"> 
				</applet>
			</div>
		</form>
	</div>
	<div id="common-popup-new" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result-new width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
</div>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jZebra.js"></script>