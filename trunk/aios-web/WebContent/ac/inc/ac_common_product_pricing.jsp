<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
		Pricing Information
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="customerlist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${PRODUCT_PRICING_INFO}" var="bean" varStatus="status">  
	     		 <option value="${bean.productPricingCalcId}|${bean.salePriceAmount}">${bean.pricingType}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	
 	<div class="float-left width100 pricing_list" style="padding:2px;">  
     	<ul> 
			<li class="width30 float-left"><span>Pricing Type</span></li>  
			<li class="width30 float-left"><span>Calculation Type</span></li>  
 			<li class="width30 float-left"><span>Sale Amount</span></li>   
			<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
     		<c:forEach items="${PRODUCT_PRICING_INFO}" var="bean" varStatus="status">
	     		<li id="pricing_${status.index+1}" class="pricing_list_li width100 float-left" style="height: 20px;">
	     			<input type="hidden" value="${bean.productPricingCalcId}" id="pricingbyid_${status.index+1}">  
	     			<input type="hidden" value="${bean.salePriceAmount}" id="salePriceAmountid_${status.index+1}"> 
 	     			<span id="pricingtype_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.pricingType}</span>
 	     			<span id="calctype_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.calculationTypeCode}</span>
 	     			<span id="saleamount_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.salePriceAmount}
	     				<span id="pricingNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
	     			</span>  
	     		</li>
	     	</c:forEach> 
     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 	<input type="hidden" id="pageRowId" value="${requestScope.rowId}"/>
 	<input type="hidden" id="pageInfo" value="${requestScope.pageInfo}"/>
 </div> 
 <script type="text/javascript"> 
 var pageInfo = '';
 $(function(){
 	 pageInfo = $('#pageInfo').val();
	 if(null!=pageInfo && pageInfo=="sales_invoice"){ 
		 $($($('#price-common-popup').parent()).get(0)).css('width',800);
		 $($($('#price-common-popup').parent()).get(0)).css('height',250);
		 $($($('#price-common-popup').parent()).get(0)).css('padding',0);
		 $($($('#price-common-popup').parent()).get(0)).css('left',293);
		 $($($('#price-common-popup').parent()).get(0)).css('top',100);
		 $($($('#price-common-popup').parent()).get(0)).css('overflow','hidden');
  		 $('#price-common-popup').dialog('open'); 
	 }else{
		 $($($('#common-popup').parent()).get(0)).css('width',800);
		 $($($('#common-popup').parent()).get(0)).css('height',250);
		 $($($('#common-popup').parent()).get(0)).css('padding',0);
		 $($($('#common-popup').parent()).get(0)).css('left',293);
		 $($($('#common-popup').parent()).get(0)).css('top',100);
		 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
		 $('#common-popup').dialog('open'); 
	}
	 
	 $('.autoComplete').combobox({ 
	      selected: function(event, ui){   
    	   var pricingdetails = $(this).val();
	       var pricingarray = pricingdetails.split("|"); 
 	       var pageRowId = $('#pageRowId').val(); 
	       $('#productPricingDetailId_'+pageRowId).val(pricingarray[0]); 
	       $('#amount_'+pageRowId).val(pricingarray[1]);  
	       triggerPriceCalc(pageRowId);
	       if(null!=pageInfo && pageInfo=="sales_invoice")
	      	 $('#price-common-popup').dialog("close");   
	      else
	      	$('#common-popup').dialog("close");   
	   } 
	}); 
	 
	 $('.pricing_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.pricing_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#pricingNameselect_'+rowid).hasClass('selected')){ 
					 $('#pricingNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#pricingNameselect_'+rowid).addClass('selected');
			return false;
		});
	 
	 $('.pricing_list_li').live('dblclick',function(){  
		  var tempvar="";var idarray = ""; var rowid="";
		  var pageRowId = $('#pageRowId').val(); 
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#productPricingDetailId_'+pageRowId).val($('#pricingbyid_'+rowid).val()); 
	      $('#amount_'+pageRowId).val($('#salePriceAmountid_'+rowid).val());   
	      triggerPriceCalc(pageRowId);
	      if(null!=pageInfo && pageInfo=="sales_invoice")
	      	 $('#price-common-popup').dialog("close");   
	      else
	      	$('#common-popup').dialog("close"); 
		  return false;
	  });
	 $('#close').click(function(){
		 if(null!=pageInfo && pageInfo=="sales_invoice")
	      	 $('#price-common-popup').dialog("close");   
	      else
	      	$('#common-popup').dialog("close"); 
	 });  
 }); 
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.pricing_list ul li{
		padding:3px;
	}
	.pricing_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>