<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		var tdsize = $($("#definition-table>tbody>tr:first").children()).size();
		if (tdsize == 1) {
			$($("#definition-table>tbody>tr:first").children()).each(
					function() {
						$(this).addClass('float-left width20');
					});
		}

		$('.definition').click(
				function() {
					var definitionName = $.trim($(this).text()); 
					var productDefinitionId = getRowId($(this).find('input')
							.attr('id'));
					var productId = Number($('#specialProductId').val());
					showProductDefinitionFilter(productDefinitionId,productId, definitionName);
					return false;
				}); 
		
		$('.subCategoryDef').click(function(){ 
			var categoryId = getRowId($(this).find('input').attr('id'));
			var specialProductId = Number($('#specialProductId').val()); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_categorydef_product.action",
										async : false,
										data : {
											categoryId : categoryId, productId: specialProductId
										},
										dataType : "html",
										cache : false,
										success : function(result) {
											$("#filter-product-result").html(
													result);
											$(".product-listing").show();
										}
									});
							return false;
						});
	});
</script>
<fieldset class="splproductset" style="max-height: 95px;">
	<legend>
		<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sub
		Category
	</legend>
	<div class="width100 content-item-table"
		style="max-height: 80px; overflow-y: auto; overflow-x: hidden;">
		<table id="definition-table" class="pos-item-table width100">
			<c:set var="definitionRow" value="0" />
			<c:set var="subCategoryLoop" value="true" />
			<c:choose>
				<c:when
					test="${PRODUCT_DEFINITIONS ne null && fn:length(PRODUCT_DEFINITIONS) > 0}">
					<c:forEach begin="0" step="5" items="${PRODUCT_DEFINITIONS}">
						<tr>
							<c:if
								test="${PRODUCT_SUBCATEGORY ne null && PRODUCT_SUBCATEGORY ne '' && subCategoryLoop eq true}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="subCategoryDef">${PRODUCT_SUBCATEGORY.categoryName}
											<input type="hidden"
											id="categoryId_${PRODUCT_SUBCATEGORY.productCategoryId}"
											class="categoryId" /> </span>
									</div>
								</td>
								<c:set var="subCategoryLoop" value="false" />
							</c:if>
							<!-- TD 1 -->
							<td>
								<div class="box-model" style="background: #9FF781;">
									<span class="definition">${PRODUCT_DEFINITIONS[definitionRow+0].definitionLabel}
										<input type="hidden"
										id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+0].productDefinitionId}"
										class="definitionId" /> </span>
								</div>
							</td>
							<!-- TD 2 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+1]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+1].definitionLabel ne null}">
								<td> 
									<div class="box-model" style="background: #81F7D8;">
										<span class="definition">${PRODUCT_DEFINITIONS[definitionRow+1].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+1].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 3 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+2]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+2].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="definition">${PRODUCT_DEFINITIONS[definitionRow+2].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+2].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 4 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+3]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+3].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="definition">${PRODUCT_DEFINITIONS[definitionRow+3].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+3].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 5 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+4] ne  null &&
								PRODUCT_DEFINITIONS[definitionRow+4].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="definition">${PRODUCT_DEFINITIONS[definitionRow+4].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+4].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<c:set var="definitionRow" value="${definitionRow + 5}" />
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<c:if
							test="${PRODUCT_SUBCATEGORY ne null && PRODUCT_SUBCATEGORY ne ''}">
							<td>
								<div class="box-model" style="background: #9FF781;">
									<span class="subCategoryDef">${PRODUCT_SUBCATEGORY.categoryName}
										<input type="hidden"
										id="categoryId_${PRODUCT_SUBCATEGORY.productCategoryId}"
										class="categoryId" /> </span>
								</div>
							</td>
						</c:if>
					</tr>
				</c:otherwise>
			</c:choose>

		</table>
		<input type="hidden" id="specialProductId"
			value="${requestScope.productId}" /> 
	</div>
</fieldset>