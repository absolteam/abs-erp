<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var slidetab="";
var commissionMethods = "";
var personObject = [];
	$(function(){  
		 
		$('input,select').attr('disabled', true);
	 
		 
		$('.common-popup').click(function(){  
		    $('.ui-dialog-titlebar').remove();   
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/common_multiselect_person_list.action", 
				 	async: false,  
				 	data : {personTypes : "1"}, 
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);   
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0); 
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				return false;
			});

		$('#person-list-close').live('click',function(){
			 $('#common-popup').dialog('close');
		 }); 

		$('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		$('.targetsale').live('change',function(){
			var rowId = getRowId($(this).attr('id'));
			triggerAddRow(rowId);
			return false;
		});

		$('.isPercentage').live('change',function(){
			var rowId = getRowId($(this).attr('id'));
			triggerAddRow(rowId);
			return false;
		});

		$('.commissionvalue').live('change',function(){
			var rowId = getRowId($(this).attr('id'));
			triggerAddRow(rowId);
			return false;
		});

		if($('.commissionvalue').val() >0) {
			$('#commissionBase').val($('#tempcommissionBase').val());
			$('#calculationMethod').val($('#tempcalculationMethod').val());
			
			if ('${COMMISSION_RULE.isCreditNote}' == "true")
				$('#isCreditNote').attr('checked', true);
			else
				$('#isCreditNote').attr('checked', false);

			if ('${COMMISSION_RULE.isDebitNote}' == "true")
				$('#isDebitNote').attr('checked', true);
			else
				$('#isDebitNote').attr('checked', false);
			
			$('.rowid').each(function(){   
	    		 var rowId=getRowId($(this).attr('id')); 
	    		 if ($('#tempisPercentage_'+rowId).val() == "true")
	    			 $('#isPercentage_'+rowId).val(1); 
	    		 else if ($('#tempisPercentage_'+rowId).val() == "false")
	    			 $('#isPercentage_'+rowId).val(0); 
	 		 }); 
	 		 var jsonData = [];
			$('.person_details').each(function(){   
	    		 var rowId=getRowId($(this).attr('id')); 
	    		 jsonData.push({
						"personId" : Number($('#personId_'+rowId).val()),
						"personName" : $('#personName_'+rowId).val()
					});  
	 		 }); 
 			personObject = {
				"title" : "Persons",
				"record" : jsonData
			};
 	 		$('#view_option').show();
	 		$('.commission_person_list').remove();
		}

		 $('.addrows').click(function(){ 
			  var i=Number(1); 
			  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
			  id = id+1;  
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/commission_rule_addrow.action", 
				 	async: false,
				 	data:{rowId: id},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tab tr:last').before(result);
						 if($(".tab").height()>255)
							 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
						 $('.rowid').each(function(){   
				    		 var rowId=getRowId($(this).attr('id')); 
				    		 $('#lineId_'+rowId).html(i);
							 i=i+1; 
				 		 }); 
					}
				});
			  return false;
		 }); 

		 $('.delrow').live('click',function(){ 
			 slidetab = $(this).parent().parent().get(0);  
	       	 $(slidetab).remove();  
	       	 var i=1;
	    	 $('.rowid').each(function(){   
	    		 var rowId = getRowId($(this).attr('id')); 
	    		 $('#lineId_'+rowId).html(i);
				 i=i+1; 
	 		 });   
		 });

		 $('#person_multiselect_popup').dialog({
		 		autoOpen: false,
		 		width: 800,
		 		height: 400,
		 		bgiframe: true,
		 		modal: true,
		 		buttons: {
		 			"OK": function(){  
		 				$(this).dialog("close"); 
		 			} 
		 		}
		 	});

		 $('#view_option').click(function(){   
			  $('.ui-dialog-titlebar').remove();
				var htmlString = "";
							htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>"
									+ personObject.title + "</div>";
									var count = 0;
							
				$(personObject.record)
									.each(
											function(index) {
												count += 1;
												htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
														+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
														+ count
														+ ".</span>"
														+ "<span class='width40 float-left'>"
														+ personObject.record[index].personName
														+ "</span><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
											});

							$('#person_multiselect_popup').dialog('open');

							$('#person_multiselect_popup').html(htmlString);
						});

		 $('.deleteoption')
			.live('click',
					function() {
				var rowId = getRowId($(this).attr('id')); 
				var count_index =  $('#countindex_'+rowId).attr('id'); 
				$('#div_'+rowId).remove(); 
				personObject.record.splice(count_index.split('.')[0], 1); 
				var i = 0;
				$('.count_index')
				.each(
						function() {
							i = i +1;
					$(this).text(i+".");		
				});
				if(personObject.record.length == 0){
					$('#view_option').hide();
					$('#person_multiselect_popup').dialog("close");
				} 
				return false;
		 });

		 
	});

	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function triggerAddRow(rowId) {
		var targetsale = $('#targetsale_' + rowId).val();
		var isPercentage = $('#isPercentage_' + rowId).val();
		var commissionvalue = $('#commissionvalue_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (targetsale != null && targetsale != "" && targetsale != null
				&& isPercentage != "" && isPercentage != null
				&& commissionvalue != null && commissionvalue != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}

	function selectedPersonList(jsonData) {
		personObject = [];
		personObject = {
			"title" : "Persons",
			"record" : jsonData
		};
 		$('#view_option').show();
	}
	
	 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>commission
			rule
		</div> 
		<form name="commissionrule_entry_details"
			id="commissionrule_entry_details" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="commissionRuleId" name="commissionRuleId"
					value="${COMMISSION_RULE.commissionRuleId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 145px;">
							<div>
								<label class="width30" for="isCreditNote">Credit Note </label> <input
									type="checkbox" name="isCreditNote" id="isCreditNote"
									style="width: 5%" />
							</div>
							<div>
								<label class="width30" for="isDebitNote">Debit Note </label> <input
									type="checkbox" name="isDebitNote" id="isDebitNote"
									style="width: 5%" />
							</div>
							<div>
								<label class="width30" for="calculationMethod">Method
									  </label> <select
									name="calculationMethod" id="calculationMethod"
									class="width50 validate[required]">
									<option value="">Select</option>
									<c:forEach var="calculationMethod"
										items="${CALCULATION_METHOD}">
										<option value="${calculationMethod.key}">${calculationMethod.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempcalculationMethod"
									value="${COMMISSION_RULE.calculationMethod}" />
							</div>
							<div>
								<label class="width30">Description</label>
								<textarea id="description" name="description" class="width50">${COMMISSION_RULE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 145px;">
							<div>
								<label class="width30" for="ruleName">Rule Name   </label> <input type="text" name="ruleName"
									id="ruleName" class="width50 validate[required]"
									value="${COMMISSION_RULE.ruleName}" />
							</div>
							<div>
								<label class="width30" for="commissionBase">Rule Base</label> <select
									name="commissionBase" id="commissionBase"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="commissionBase" items="${COMMISSION_BASE}">
										<option value="${commissionBase.key}">${commissionBase.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempcommissionBase"
									value="${COMMISSION_RULE.commissionBase}">
							</div>
							<div>
								<label class="width30" for="validFrom">Valid From </label>
								<c:choose>
									<c:when
										test="${COMMISSION_RULE.validFrom ne null && COMMISSION_RULE.validFrom ne ''}">
										<c:set var="validFrom" value="${COMMISSION_RULE.validFrom}" />
										<%
											String validFrom = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"validFrom").toString());
										%>
										<input type="text" readonly="readonly" name="validFrom"
											id="validFrom" class="width50 validate[required]"
											value="<%=validFrom%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="validFrom"
											id="validFrom" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="validTo">Valid To</label>
								<c:choose>
									<c:when
										test="${COMMISSION_RULE.validTo ne null && COMMISSION_RULE.validTo ne ''}">
										<c:set var="validTo" value="${COMMISSION_RULE.validTo}" />
										<%
											String validTo = DateFormat.convertDateToString(pageContext
															.getAttribute("validTo").toString());
										%>
										<input type="text" readonly="readonly" name="validTo"
											id="validTo" class="width50 validate[required]"
											value="<%=validTo%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="validTo"
											id="validTo" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="commissionPerson">Person(s)<span
									class="mandatory">*</span> </label>   
								<div class="float-left width60">
									<c:forEach var="commissionPerson" items="${COMMISSION_RULE.commissionPersonCombinations}" varStatus="personStatus">
									 <span class="float-left width80"><span
									class="mandatory">*</span> ${commissionPerson.person.firstName} ${commissionPerson.person.lastName}[${commissionPerson.person.personNumber}]</span>
								 	</c:forEach> 
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Commission method<span class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<input type="hidden" name="childCount" id="childCount"
							value="${fn:length(COMMISSION_RULE.commissionMethods)}" />
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Flat(%)</th>
									<th style="width: 5%">Target Sale</th>
									<th style="width: 5%">Commission Type</th>
									<th style="width: 5%">Commission</th>
									<th style="width: 5%">Description</th>
									<th style="width: 0.01%; display: none;"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="commissionMethod"
									items="${COMMISSION_RULE.commissionMethods}" varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
										<td><input type="text"
											class="width96 flatcommission validate[optional,custom[number]]"
											id="flatcommission_${status.index+1}"
											value="${commissionMethod.flatCommission}"
											 />
										</td>
										<td><input type="text"
											class="width96 targetsale validate[optional,custom[number]]"
											id="targetsale_${status.index+1}"
											value="${commissionMethod.targetSale}"
											style=" text-align: right;" />
										</td>
										<td><select name="isPercentage"
											id="isPercentage_${status.index+1}" class="width96">
												<option value="">Select</option>
												<option value="1">Percentage</option>
												<option value="0">Amount</option>
										</select> <input type="hidden" value="${commissionMethod.isPercentage}"
											id="tempisPercentage_${status.index+1}" /></td>
										<td><input type="text"
											class="width96 commissionvalue validate[optional,custom[number]]"
											id="commissionvalue_${status.index+1}"
											value="${commissionMethod.commission}"  />
										</td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status.index+1}" class="width96"
											value="${commissionMethod.description}" />
										</td>
										<td style="width: 0.01%; display: none;" class="opn_td"
											id="option_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a> <input
											type="hidden" id="commissionMethodId_${status.index+1}"
											value="${commissionMethod.commissionMethodId}" />
										</td>
									</tr>
								</c:forEach>
								 
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div> 
			<div class="commission_person_list" style="display: none;">
				<c:forEach var="commissionPerson" items="${COMMISSION_RULE.commissionPersonCombinations}" varStatus="personStatus">
					<input class="person_details" type="hidden" id="personId_${personStatus.index+1}" value="${commissionPerson.person.personId}"/>
					<input type="hidden" id="personName_${personStatus.index+1}" value="${commissionPerson.person.firstName} ${commissionPerson.person.lastName}[${commissionPerson.person.personNumber}]"/>
				</c:forEach>
			</div>
			 
		</form>
	</div>
</div>
<div class="clearfix"></div>