<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
	var slidetab = "";
	var storeObject = [];
	var tempid = "";
	var eodBalancing = [];
 	$(function() { 
		 
		$jquery("#eodBalancingValidation").validationEngine('attach'); 
		$('#salesDate').datepick({ 
			 onSelect: getStorePOS, defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
		
		
		$("#sales-print-call").click(function(){
			var selectedUserTillId=$('#posUserTillId').val();
			var selectedStoreId=Number(0);
			if (typeof selectedUserTillId != 'undefined'){
				selectedUserTillId=Number(selectedUserTillId);
			}else{
				selectedStoreId=Number($('#posStoreId').val());
				selectedUserTillId=Number(0);
			}
			
			var selectedPersonId=Number(0);
			var fromDate=$('#salesDate').val();
			var toDate=$('#salesDate').val();
 			window.open("<%=request.getContextPath()%>/get_daily_sales_report_printout.action?userTillId="+selectedUserTillId
 					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML&reportType=pos",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
			return false;
			
		});
	    
		$('.actualamount').live('change',
				function() {  
			var rowId = getRowId($(this).attr('id'));
			var actualAmount = Number($('#actualamount_' + rowId).val());
			$('#variance_' + rowId).text('');
			if (actualAmount > 0) {
				var tillAmount = Number(convertToDouble($.trim($('#tillamount_' + rowId)
						.text())));
				
				var variance = Number(tillAmount - actualAmount); 
				if (variance > 0)
					$('#variance_' + rowId).text(convertToAmount(variance.toFixed(2)));
			}
			return false;
		});

		$('.discard').click(function(){  
			eodBalancingDiscard("");
			 return false;
		 });
		 
		
		 $('.eod_save').click(function(){ 
			 eodDetails = getEODDetails();  
			 var posStoreId = Number(0);
 			 var actionName = "";
 			 var currentDate = "";
 			 posStoreId = $('#posStoreId').val();
			 currentDate= $('#salesDate').val();
			 if (typeof posUserTillId != 'undefined'){
				 actionName = "process_eod_balancing";
				 posStoreId = $('#posStoreId').val();
				 currentDate= $('#salesDate').val(); 
			 } 
			 else{
				 actionName = "save_eod_balancing";  
				 posStoreId = $('#posStoreId').val();
				 currentDate= $('#salesDate').val(); 
			 } 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/"+actionName+".action", 
				 	async: false,
				    dataType: "json",
				    data:{eodDetails : JSON.stringify(eodBalancing), posStoreId: posStoreId, currentDate: currentDate},
				    cache: false,
					success:function(response){ 
						 if(($.trim(response.returnMessage)=="SUCCESS")) 
							 eodBalancingDiscard("Record created.");
 						 else{
 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
 							 $('#page-error').delay(2000).slideUp();
 							 return false;
 						 } 
 					}
			 });
		 });  

		 $('#store-common-popup').live('click',function(){  
			   $('#common-popup').dialog('close'); 
		   });

		 $('.eod-store-popup').click(function(){  
			 $('.ui-dialog-titlebar').remove();  
				$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_multi_popup.action", 
			 	async: false,  
			 	data:{rowId : Number(0)},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);  
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0); 
					return false;
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

		 $('#store_multiselect_popup').dialog({
		 		autoOpen: false,
		 		width: 800,
		 		height: 400,
		 		bgiframe: true,
		 		modal: true,
		 		buttons: {
		 			"OK": function(){  
		 				$(this).dialog("close"); 
		 			} 
		 		}
		 	});
		 	
		 $('#view_option').click(function(){   
			  $('.ui-dialog-titlebar').remove();
				var htmlString = "";
							htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>"
									+ storeObject.title + "</div>";
									var count = 0;
							
				$(storeObject.record)
									.each(
											function(index) {
												count += 1;
												htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
														+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
														+ count
														+ ".</span>"
														+ "<span class='width40 float-left'>"
														+ storeObject.record[index].storeName
														+ "</span><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
											});

							$('#store_multiselect_popup').dialog('open');

							$('#store_multiselect_popup').html(htmlString);
						});

		 $('.deleteoption')
			.live('click',
					function() {
				var rowId = getRowId($(this).attr('id')); 
				var count_index =  $('#countindex_'+rowId).attr('id'); 
				$('#div_'+rowId).remove(); 
				storeObject.record.splice(count_index.split('.')[0], 1); 
				var i = 0;
				$('.count_index')
				.each(
						function() {
							i = i +1;
					$(this).text(i+".");		
				});
				if(storeObject.record.length == 0){
					$('#view_option').hide();
					$('#store_multiselect_popup').dialog("close");
				} 
				getStorePOS();
				return false;
		 });

		 $('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
		 
		 $('#posStoreId').change(
					function() {  
				 var posStoreId = Number($('#posStoreId').val());
				 var salesDate= $('#salesDate').val();
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/show_storeeod_details.action",
						async : false,
						data : {
							salesDate : salesDate, posStoreId: posStoreId
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.pos-eod-details').html(result);
						}
					}); 	
			});
	});

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function multiStorePopup(jsonData) {
		storeObject = [];
		storeObject = {
			"title" : "Stores",
			"record" : jsonData
		};
 		$('#view_option').show();
 		getStorePOS();
 		return false;
	}

	function getStorePOS() {  
		var posUserTillId = $('#posUserTillId').val(); 
		if (typeof posUserTillId != 'undefined'){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_eodpos_details.action",
				async : false,
				data : {
					salesDate : $('#salesDate').val(), posUserTillId: posUserTillId
				},
				dataType : "html",
				cache : false,
				success : function(result) {
					$('.pos-eod-details').html(result);
				}
			}); 	
		}
		return false;
	}

	function getPOSReceiptDetails(currencyMode, rowId) {
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pos_receipts_fetch.action",
			async : false,
			data : {
				paymentMode : currencyMode
			},
			dataType : "json",
			cache : false,
			success : function(response) {
				if (null != response && response != '') {
					$('#tillBasicAmount_' + rowId).val(
							response.pointOfSaleVO.balanceDue);
					$('#quantity_' + rowId).text(
							response.pointOfSaleVO.totalDue);
					$('#tillamount_' + rowId).text(
							response.pointOfSaleVO.salesAmount);
				}
			}
		});
		return false;
	}

	function manupulateLastRow() {
		var hiddenSize = 0;
		var tdSize = 0;
		var actualSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden'))
				++hiddenSize;
		});
		tdSize = $($(".tab>tr:first").children()).size();
		actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++)
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}

	function getEODDetails() { 
		var eodDetails = [];
		$('.rowid')
				.each(
						function() {
							var rowid = getRowId($(this).attr('id'));
							var currencyMode = Number($(
									'#currencymode_' + rowid).val());
							var actualAmount = Number($(
									'#actualamount_' + rowid).val());
							var tillAmount = Number(convertToDouble($(
									'#tillamount_' + rowid).text()));
							if (currencyMode > 0 && actualAmount > 0) {
								eodDetails.push({
									"tillAmount" : tillAmount,
									"currencyMode" : currencyMode,
									"actualAmount" : actualAmount
								});
							}
						});
			eodBalancing = {
				description : $('#description').val(),
				currentDate : $('#salesDate').val(),
				posUserTillId : Number($('#posUserTillId').val()),
				implementationId : Number($('#implementationId').val()),
				eodDetails: eodDetails
			};
		return eodBalancing;
	}
	function eodBalancingDiscard(message){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_eod_balancing.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();
				$('#store_multiselect_popup').dialog('destroy');
				$('#store_multiselect_popup').remove();
				$("#main-wrapper").html(result);
				if (message != null && message != '') {
					$('#success_message').hide().html(message).slideDown(1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> EOD
			Balancing Entry
		</div>
		<form id="eodBalancingValidation" method="POST">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right width48">
						<fieldset style="min-height: 70px;">
							<div>
								<label class="width30">Description<span
									class="mandatory">*</span> </label>
								<textarea class="width50" id="description"></textarea>
							</div>
							<div
								class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left "
								style="margin: 10px;">
								<div class="portlet-header ui-widget-header float-right sales-print-call"
									style="cursor: pointer;" id="sales-print-call">
									Sales Report
								</div>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 70px;">
							<div>
								<label class="width30">Reference No<span
									class="mandatory">*</span> </label> <input type="text"
									id="referenceNumber" name="referenceNumber"
									class="width50 validate[required]"
									value="${requestScope.referenceNumber}" />
							</div>
							<div>
								<label class="width30">Date<span class="mandatory">*</span>
								</label> <input type="text" id="salesDate" name="salesDate"
									class="width50 validate[required]" />
							</div>
							<c:choose>
								<c:when test="${requestScope.STORE_DETAILS ne null && requestScope.STORE_DETAILS ne ''}">
									<div>
										<label class="width30">Store<span class="mandatory">*</span>
										</label> 
										<select name="stores" id="posStoreId" class="width50 validate[required]">
											<option value="">Select</option>
											<c:forEach var="storeDetail" items="${STORE_DETAILS}">
												<option value="${storeDetail.storeId}">${storeDetail.storeName}</option>
											</c:forEach>
										</select>
									</div>
								</c:when>
								<c:otherwise>
									<div style="display: none;">
										<label class="width30">Store<span class="mandatory">*</span>
										</label> <span class="button float-left"
											style="position: absolute; margin-top: 5px;"> <a
											style="cursor: pointer;"
											class="btn ui-state-default ui-corner-all eod-store-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <span
											style="cursor: pointer; position: relative; top: 8px; float: left; display: none;"
											id="view_option">View</span> <input type="hidden"
											id="posUserTillId" name="posUserTillId"
											value="${requestScope.posUserTillId}" /> <input type="hidden"
											id="implementationId" name="implementationId"
											value="${requestScope.implementationId}" />
									</div>
								</c:otherwise>
							</c:choose> 
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content class90 pos-eod-details" id="hrm"
				style="margin-top: 10px;">
				
			</div>
			<div class="clearfix"></div> 
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right eod_save"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div style="display: none;" id="store_multiselect_popup"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>