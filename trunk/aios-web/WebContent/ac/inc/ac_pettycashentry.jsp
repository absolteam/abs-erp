<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var forwardFlag = false; var slidetab ="";
var tempid = "";
var pettyCashDetails = [];
$(function(){
	 
	 $jquery("#pettycashentry_details").validationEngine('attach'); 
	 $jquery('.cashIn,.cashOut,.expenseAmount,.linesExpenseAmount,.allowMaxAmount').number(true, 2);
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	    tempid=""; 
	    $('.ui-dialog-titlebar').remove();   
	 	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_treeview.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.codecombination-result').html(result);  
			},
			error:function(result){ 
				 $('.codecombination-result').html(result); 
			}
		});  
	});
	
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pettycash_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					if($(".tab").height()>255)
						$(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});  
					$('.rowid').each(function(){   
			    		var rowId=getRowId($(this).attr('id')); 
			    		$('#lineId_'+rowId).html(i);
						i=i+1; 
			 		}); 
				}
			});
		  return false;
	 });
	
	$('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
     	 $(slidetab).remove();  
     	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});
	
	$('.codecombination-popup-transaction').live('click',function(){ 
 	    tempid=$(this).parent().get(0);  
 	    $('.ui-dialog-titlebar').remove();   
 	 	$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/combination_treeview.action", 
 		 	async: false, 
 		    dataType: "html",
 		    cache: false,
 			success:function(result){   
 				 $('.codecombination-result').html(result);  
 			},
 			error:function(result){ 
 				 $('.codecombination-result').html(result); 
 			}
 		});  
 		return false;
 	});
	
	 $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true
	 });

	 $('.common-popup-person').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data : {personTypes: "1"}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);   
				$('#common-popup').dialog('open'); 
				$($($('#common-popup').parent()).get(0)).css('top',0); 
			},
			error:function(result){  
				 $('.common-result').html(result);  
			}
		});
		return false;   
	});
	 
	 $('.forward-common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();    
	    var pettyCashId = Number($('#settlementVoucherId').val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_all_cashin_forwardvoucher.action", 
		 	async: false,  
		 	data : {pettyCashId: pettyCashId}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);   
				$('#common-popup').dialog('open'); 
				$($($('#common-popup').parent()).get(0)).css('top',0); 
			} 
		});
		return false; 
	});
	 
	 $('.common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
	    var carryForwardVoucherId = Number(0);
        var pettyCashType = $('#pettyCashType').val(); 
        var pettyCashCode = $.trim(pettyCashType.split('@@')[1]);
        var actionname = "";
        $('#forwardVoucherId').val('');
		$('#forwardVoucher').val('');
        if(pettyCashCode == "CTPC"){
        	actionname="show_receipt_reference"; 
        } else if(pettyCashCode=="AIPC"){ 
	    	actionname="show_all_cashin_voucher"; 
		} else if(pettyCashCode=="SFAI"){
			actionname="show_all_advanceissued_voucher"; 
		} else if(pettyCashCode=="CRFS" || pettyCashCode=="CGFS"){
			actionname="show_all_nonclosed_advancedvoucher"; 
		} else if(pettyCashCode=="CFRD"){
			actionname="show_all_cashin_voucher"; 
		} else if(pettyCashCode=="RFPC"){
			actionname="show_all_advanceissued_voucher"; 
		} 
        carryForwardVoucherId = Number($('#settlementVoucherId').val());
 		if(pettyCashType!=null && pettyCashType!=""){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+actionname+".action", 
			 	async: false,  
			 	data : {pettyCashType: pettyCashType, carryForwardVoucherId: carryForwardVoucherId}, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);   
					$('#common-popup').dialog('open'); 
 					$($($('#common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});
			return false;  
		} 
		else {
			return false;
		}
	});
	 
	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.voucher_log').click(function(){
		var settlementVoucherId = $('#settlementVoucherId').val();
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_pettycash_log.action", 
		 	async: false,  
		 	data : {settlementVoucherId: settlementVoucherId}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		});
	});

	$('.petty_log_discard').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
		 return false;
	});

	$('.forward_voucher_log').click(function(){
		var settlementVoucherId = $('#forwardVoucherId').val();
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_pettycash_log.action", 
		 	async: false,  
		 	data : {settlementVoucherId: settlementVoucherId}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		});
	});
	
	$('.petty_save').click(function(){
		if($jquery("#pettycashentry_details").validationEngine('validate')){
 			var pettyCashId = Number($('#pettyCashId').val());
			var pettyCashNo = $('#pettyCashNo').val();
			var pettyCashType = $('#pettyCashType').val();
			var receiptRefId = Number($('#receiptRefId').val());
			var receiptRefType = $('#receiptRefType').val();
			var settlementRequired = $('#settlementReq').val();
			if(settlementRequired == 1)
				settlementRequired = true;
			else
				settlementRequired = false;
			var againstInvoice = $('#againstInvoice').val();
			if(againstInvoice == 1)
				againstInvoice = true;
			else
				againstInvoice = false;
			var personId = Number($('#personId').val());
			var cashIn = $('#cashIn').val();
			var cashOut = $('#cashOut').val();
			var expenseAmount = $('#expenseAmount').val();
			var invoiceNo = $('#invoiceNo').val();
			var combinationId = Number($('#combinationId').val());
			var description = $('#description').val();
			var settlementVoucherId = Number($('#settlementVoucherId').val());
			var carryForwardVoucherId = Number($('#forwardVoucherId').val());
			var voucherDate=$('#voucherDate').val();
			var receiverOther = $('#personName').val();
			var pettycashtype = pettyCashType.split('@@')[0];
			var savePettyCash  = true;
			var pettyCashDetailStr = "";
			if(pettycashtype == 3){
				savePettyCash = false;
				pettyCashDetails = getPettyCashDetails();
				if(null != pettyCashDetails && pettyCashDetails.length > 0){ 
					savePettyCash = true;
					pettyCashDetailStr = JSON.stringify(pettyCashDetails); 
				}
			} 
			if(savePettyCash ==  true){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_pettycash.action", 
				 	async: false, 
				 	data:{	pettyCashId: pettyCashId, pettyCashNo: pettyCashNo, pettyCashType: pettyCashType, description: description,
				 			receiptRefId: receiptRefId, receiptRefType: receiptRefType, settlementRequired: settlementRequired, personId: personId,
				 			cashIn: cashIn, cashOut: cashOut, expenseAmount: expenseAmount, invoiceNo: invoiceNo, pettyCashDetailStr: pettyCashDetailStr,
				 			combinationId: combinationId, settlementVoucherId: settlementVoucherId, againstInvoice: againstInvoice,
				 			carryForwardVoucherId: carryForwardVoucherId, voucherDate: voucherDate, receiverOther: receiverOther
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
	 					var returnMessage = $.trim(response.returnMessage);   
						var message="";
						if(pettyCashId == 0)
							message = "Record created.";
						else
							message = "Record updated.";
						if(returnMessage=="SUCCESS"){
							 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_all_petty_cash.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#DOMWindow').remove();
									$('#DOMWindowOverlay').remove();
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove();  
				  					$('#codecombination-popup').dialog('destroy');		
				  					$('#codecombination-popup').remove(); 
									$("#main-wrapper").html(result);  
									$('#success_message').hide().html(message).slideDown();
									$('#success_message').delay(2000).slideUp();
									return false;
								} 
							 });
						 }
						 else{
							 $('#page-error').hide().html(returnMessage).slideDown();
							 $('#page-error').delay(2000).slideUp(); 
							 return false;
						 }  
					} 
				});
			}else{
				$('#page-error').hide().html("Please enter pettycash detail").slideDown();
				$('#page-error').delay(2000).slideUp();
			} 
		} else {
			return false;
		} 
	});
	
	var getPettyCashDetails = function(){
	  	pettyCashDetails = []; 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var personId = Number($('#personId_'+rowId).val());  
			var personName = $('#personName_'+rowId).val();  
			var combinationId = Number($('#combinationId_'+rowId).val());  
			var expenseAmount = Number($jquery('#linesExpenseAmount_'+rowId).val()); 
			var billNumber = $('#billno_'+rowId).val(); 
			var description = $('#linesDescription_'+rowId).val();  
			var pettyCashDetailId =  Number($('#pettyCashDetailId_'+rowId).val()); 
			if(typeof combinationId != 'undefined' && combinationId > 0 && expenseAmount > 0 && (personId > 0 || personName != '')){ 
				pettyCashDetails.push({
					"personId" : personId,
					"personName" : personName,
					"combinationId" : combinationId,
					"expenseAmount" : expenseAmount,
					"billNumber" : billNumber,
					"description" : description, 
					"pettyCashDetailId": pettyCashDetailId
				});
			} 
		});  
		return pettyCashDetails;
	 }; 

	$('#pettycashvoucher-close').live('click',function(){
		$('#common-popup').dialog('close');  
		return false;
	}); 

	$('.petty_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_all_petty_cash.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#codecombination-popup').dialog('destroy');		
				$('#codecombination-popup').remove(); 
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
				$("#main-wrapper").html(result);   
			}
		 }); 
		 return false;
	}); 
	
	$('#pettycash-common-popup').live('click',function(){ 
		$('#common-popup').dialog('close');	
		return false;
	});
	
	 $('#advissueVoucher-close').live('click',function(){
		 $('#common-popup').dialog('close');  
		 return false;
	 });
	 $('#advissueVoucherforward-close').live('click',function(){
		 $('#common-popup').dialog('close');  
		 return false;
	 });
	 $('#spendVoucher-close').live('click',function(){
		 $('#common-popup').dialog('close');  
		 return false;
	 }); 
	
	$('#pettyCashType').change(function(){ 
		$('.maxValue').html('');
		$('#settlementVoucher').val('');
		$('#cashOut').val('');
		$('#cashIn').val('');
		$('#expenseAmount').val('');
		$('#codeCombination').val('');
		$('#receiptRefNumber').val('');
		$('#personId').val('');
		$('#personName').val('');
		$('#settlementVoucherId').val('');
		$('#receiptRefId').val('');
		$('#receiptRefType').val('');
		$('#receiptRefNumber').val('');
		$('#forwardVoucherId').val('');
		$('#forwardVoucher').val('');
		$('#personName').attr('readonly', true);
		if(null!= $(this).val() && $(this).val()!='') {
			var typearr = $(this).val().split('@@'); 
			if(typearr[1]=="CTPC") {
				$('.receiptdiv').show();
				$('.cashindiv').show();
				$('.cashoutdiv').hide();
				$('.billdiv').hide();
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').hide(); 
				$('.againstbilldiv').hide();
				$('.expensediv').hide();
				$('.expense_combi_div').hide();
				$('.persondiv').hide(); 
				$('.forwardvoucherdiv').hide(); 
				$('.pettycash_detaildiv').hide();  
				$('#settlementReq').val(0);   
 				$('.cashIn').addClass('validate[required]');
				$('.receiptRefNumber').addClass('validate[required]'); 
				$('.cashOut').removeClass('validate[required]'); 
				$('.expenseAmount').removeClass('validate[required]');
				$('.settlementVoucher').removeClass('validate[required]');  
				$('.codeCombination').removeClass('validate[required]');
				$('.personName').removeClass('validate[required]');
			} else if(typearr[1]=='AIPC'){
				$('.receiptdiv').hide();
				$('.cashindiv').hide();
				$('.expensediv').hide(); 
				$('.expense_combi_div').hide();
				$('.billdiv').hide();
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').show(); 
				$('.againstbilldiv').show();
				$('.cashoutdiv').show(); 
				$('.persondiv').show();
				$('#person').show();
				$('.forwardvoucherdiv').hide(); 
				$('.pettycash_detaildiv').hide();  
				$('#settlementReq').val(1);  
				$('#againstInvoice').val(0);  
 				$('.cashOut').addClass('validate[required]');
				$('.settlementVoucher').addClass('validate[required]');    
				$('.expenseAmount').removeClass('validate[required]'); 
				$('.receiptRefNumber').removeClass('validate[required]');  
				$('.cashIn').removeClass('validate[required]');  
				$('.codeCombination').removeClass('validate[required]');
			}else if(typearr[1]=='SFAI'){
				$('.receiptdiv').hide();
				$('.cashindiv').hide();
				$('.cashoutdiv').hide(); 
				$('.expensediv').hide();
				$('.persondiv').hide();
				$('.expense_combi_div').hide();
				$('.billdiv').hide();
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').show(); 
				$('.againstbilldiv').show(); 
				$('.forwardvoucherdiv').hide();  
				$('.pettycash_detaildiv').show(); 
				$('#settlementReq').val(1);  
				$('#againstInvoice').val(0);  
  				$('.receiptRefNumber').removeClass('validate[required]');  
				$('.cashIn').removeClass('validate[required]');  
				$('.cashOut').removeClass('validate[required]');
				$('.settlementVoucher').addClass('validate[required]');  
				$('.expenseAmount').removeClass('validate[required]');
				$('.codeCombination').removeClass('validate[required]');
			}else if(typearr[1]=='CRFS'){ 
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').show(); 
				$('.cashindiv').show();
				$('.againstbilldiv').show();
				$('.pettycash_detaildiv').hide();  
				$('.receiptdiv').hide();
				$('.cashoutdiv').hide();
				$('.expensediv').hide();
				$('.expense_combi_div').hide();
				$('.billdiv').hide();  
				$('.persondiv').show();
				$('.forwardvoucherdiv').hide();  
				$('#settlementReq').val(1);  
				$('#againstInvoice').val(1);  
 				$('.cashIn').addClass('validate[required]');  
				$('.settlementVoucher').addClass('validate[required]');  
				$('.receiptRefNumber').removeClass('validate[required]');  
				$('.cashOut').removeClass('validate[required]');
				$('.expenseAmount').removeClass('validate[required]');
				$('.codeCombination').removeClass('validate[required]'); 
			}else if(typearr[1]=='CGFS'){ 
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').show(); 
				$('.cashoutdiv').show();
				$('.againstbilldiv').show();
				$('.receiptdiv').hide();
				$('.cashindiv').hide();
				$('.expensediv').hide();
				$('.expense_combi_div').hide();
				$('.billdiv').hide(); 
				$('.persondiv').show(); 
				$('.pettycash_detaildiv').hide();  
				$('.forwardvoucherdiv').hide();  
				$('#settlementReq').val(1);  
				$('#againstInvoice').val(1);  
 				$('.cashIn').removeClass('validate[required]');   
				$('.receiptRefNumber').removeClass('validate[required]');   
				$('.expenseAmount').removeClass('validate[required]');
				$('.settlementVoucher').addClass('validate[required]');  
				$('.cashOut').addClass('validate[required]');
				$('.codeCombination').removeClass('validate[required]');
			}else if(typearr[1]=='RFPC'){ 
				$('.cashoutdiv').hide(); 
				$('.expense_combi_div').show(); 
				$('.againstbilldiv').show(); 
				$('.billdiv').show();
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').show();  
				$('.receiptdiv').hide();
				$('.cashindiv').hide(); 
				$('.expensediv').show();
				$('.persondiv').show();
				$('.forwardvoucherdiv').hide();  
				$('.pettycash_detaildiv').hide();  
				$('#settlementReq').val(1);  
				$('#againstInvoice').val(0);  
 				$('.cashIn').removeClass('validate[required]');   
				$('.receiptRefNumber').removeClass('validate[required]');   
				$('.expenseAmount').addClass('validate[required]');
				$('.settlementVoucher').addClass('validate[required]');  
				$('.codeCombination').removeClass('validate[required]');
				$('.cashOut').removeClass('validate[required]');
			}else if(typearr[1]=='CFRD'){ 
				$('.cashoutdiv').hide(); 
				$('.expense_combi_div').hide(); 
				$('.pettycash_detaildiv').hide();  
				$('.againstbilldiv').hide(); 
				$('.billdiv').hide();
				$('.voucherDateDiv').show(); 
				$('.voucherdiv').show();  
				$('.forwardvoucherdiv').show();  
 				$('.receiptdiv').hide();
				$('.cashindiv').show(); 
				$('.expensediv').hide();
				$('.persondiv').hide(); 
				$('#settlementReq').val(0);   
				$('.cashIn').addClass('validate[required]');   
				$('.receiptRefNumber').removeClass('validate[required]');   
				$('.expenseAmount').removeClass('validate[required]');
				$('.settlementVoucher').removeClass('validate[required]');  
				$('.codeCombination').removeClass('validate[required]');
				$('.cashOut').removeClass('validate[required]');
			}
		}  
	}); 
	
	$('.show_expense_person_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
		var tableRowId = getRowId($(this).attr('id'));
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list_detail.action", 
		 	async: false,  
		 	data: {personTypes: personTypes, id: tableRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	$('.personid').live('change', function(){
		triggerAddRow(getRowId($(this).attr('id')));
		return false;
	});
	
	$('.linesExpenseAmount').live('change', function(){ 
		var expenseTotal = Number(0);
		var pendingTotal = Number(0);
		var maximumExpense = Number($('#maximumExpense').text());
		$('.rowid').each(function(){   
			var rowId=getRowId($(this).attr('id')); 
			expenseTotal = Number(expenseTotal) + Number($jquery('#linesExpenseAmount_'+rowId).val()); 
		});
 		pendingTotal = Number(maximumExpense) - Number(expenseTotal);
		if(pendingTotal >= 0){
			$('#maximumExpense').text(maximumExpense);
			$('#totalSpendExpense').text(expenseTotal);
			$('#pendingExpenseAmount').text(pendingTotal);
			triggerAddRow(getRowId($(this).attr('id')));
		}
		return false;
	});
	
	if('${PETTYCASH_INFO.voucherDate}' == null || '${PETTYCASH_INFO.voucherDate}' == '')
 		$('#voucherDate').datepick({ 
	   	 	defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
		else
			$('#voucherDate').datepick();
	
	if(Number($('#pettyCashId').val())>0){
		$('#pettyCashType').val($('#temppettyCashType').val());
		var settlementReq = $('#tempsettlementReq').val();
		var againstInvoice = $('#tempagainstInvoice').val();
		if(settlementReq == "true")
			$('#settlementReq').val(1);
		else
			$('#settlementReq').val(0);
		if(againstInvoice == "true")
			$('#againstInvoice').val(1);
		else
			$('#againstInvoice').val(0);
		pettytypeAction($('#temppettyCashType').val()); 
		showHidePettyCashFields();
	} 
	
	manupulateLastRow();
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function setCombination(combinationTreeId, combinationTree) { 
	if(tempid != null && tempid != ''){
		var idVals = $(tempid).attr('id').split("_");
		var rowids = Number(idVals[1]);
		$('#combinationId_' + rowids).val(combinationTreeId);
		$('#codeCombination_' + rowids).val(combinationTree);
		$('#codeCombination_' + rowids).val().replace(/[\s\n\r]+/g, ' ').trim();
		triggerAddRow(rowids);
	}else{
		$('#combinationId').val(combinationTreeId);
		$('#codeCombination').val(combinationTree);
		$('#codeCombination').val().replace(/[\s\n\r]+/g, ' ').trim();
	} 
	return false;
}
function triggerAddRow(rowId){   
	var personName = $('#personName_'+rowId).val();  
	var combinationId = Number($('#combinationId_'+rowId).val());  
	var linesExpenseAmount = $jquery('#linesExpenseAmount_'+rowId).val();   
 	var nexttab=$('#fieldrow_'+rowId).next();   
	if(combinationId > 0 && combinationId > 0  
			&& linesExpenseAmount > 0 && personName != ''
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show(); 
		$('.addrows').trigger('click');
	} 
}
function personDetailPopupResult(personid, personname, commonParam, id){
	$('#personName_'+id).val(personname); 
	$('#personId_'+id).val(personid); 
	$('#common-popup').dialog("close"); 
}
function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	$('.pettycash_detaildiv').hide();
	var pettyCashType = $('#temppettyCashType').val();
	if(pettyCashType!= null && pettyCashType!= ''){
		var typeval = Number(pettyCashType.split('@@')[0]);  
		if(typeval == 3){
			 $('.pettycash_detaildiv').show();
			 $('.spend_detail_div').show();
		}
	} 
}

function showHidePettyCashFields(){
	var pettytype = ($('#temppettyCashType').val()); 
	if(null!= pettytype && pettytype !='') {
		var typearr = pettytype.split('@@'); 
		if(typearr[1]=="CTPC") {
			$('.receiptdiv').show();
			$('.cashindiv').show();
			$('.cashoutdiv').hide();
			$('.billdiv').hide();
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').hide(); 
			$('.againstbilldiv').hide();
			$('.expensediv').hide();
			$('.expense_combi_div').hide();
			$('.persondiv').hide(); 
			$('.forwardvoucherdiv').hide();  
			$('.pettycash_detaildiv').hide();  
			$('.cashIn').addClass('validate[required]');
			$('.receiptRefNumber').addClass('validate[required]'); 
			$('.cashOut').removeClass('validate[required]'); 
			$('.expenseAmount').removeClass('validate[required]');
			$('.settlementVoucher').removeClass('validate[required]');  
			$('.codeCombination').removeClass('validate[required]');
			$('.personName').removeClass('validate[required]');
		} else if(typearr[1]=='AIPC'){
			$('.receiptdiv').hide();
			$('.cashindiv').hide();
			$('.expensediv').hide(); 
			$('.expense_combi_div').hide();
			$('.billdiv').hide();
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').show(); 
			$('.againstbilldiv').show();
			$('.cashoutdiv').show(); 
			$('.pettycash_detaildiv').hide();  
			$('.persondiv').show();
			$('#person').show();
			$('.forwardvoucherdiv').hide();  
			$('.cashOut').addClass('validate[required]');
			$('.settlementVoucher').addClass('validate[required]');    
			$('.expenseAmount').removeClass('validate[required]'); 
			$('.receiptRefNumber').removeClass('validate[required]');  
			$('.cashIn').removeClass('validate[required]');  
			$('.codeCombination').removeClass('validate[required]');
		}else if(typearr[1]=='SFAI'){
			$('.receiptdiv').hide();
			$('.cashindiv').hide();
			$('.cashoutdiv').hide();
			$('#personName').attr('readonly', false);
			$('.expensediv').hide();
			$('.expense_combi_div').hide();
			$('.billdiv').hide();
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').show(); 
			$('.againstbilldiv').show();
			$('.persondiv').hide();
			$('.forwardvoucherdiv').hide();  
			$('.receiptRefNumber').removeClass('validate[required]');  
			$('.cashIn').removeClass('validate[required]');  
			$('.cashOut').removeClass('validate[required]');
			$('.settlementVoucher').removeClass('validate[required]');  
			$('.expenseAmount').removeClass('validate[required]');
			$('.codeCombination').removeClass('validate[required]');
		}else if(typearr[1]=='CRFS'){ 
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').show(); 
			$('.cashindiv').show();
			$('.againstbilldiv').show();
			$('.receiptdiv').hide();
			$('.cashoutdiv').hide();
			$('.expensediv').hide();
			$('.pettycash_detaildiv').hide();  
			$('.expense_combi_div').hide();
			$('.billdiv').hide();  
			$('.persondiv').show();
			$('.forwardvoucherdiv').hide();  
			$('.cashIn').addClass('validate[required]');  
			$('.settlementVoucher').addClass('validate[required]');  
			$('.receiptRefNumber').removeClass('validate[required]');  
			$('.cashOut').removeClass('validate[required]');
			$('.expenseAmount').removeClass('validate[required]');
			$('.codeCombination').removeClass('validate[required]'); 
		}else if(typearr[1]=='CGFS'){ 
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').show(); 
			$('.cashoutdiv').show();
			$('.againstbilldiv').show();
			$('.receiptdiv').hide();
			$('.cashindiv').hide();
			$('.expensediv').hide();
			$('.expense_combi_div').hide();
			$('.billdiv').show(); 
			$('.pettycash_detaildiv').hide();  
			$('.persondiv').show(); 
			$('.forwardvoucherdiv').hide();  
			$('.cashIn').removeClass('validate[required]');   
			$('.receiptRefNumber').removeClass('validate[required]');   
			$('.expenseAmount').removeClass('validate[required]');
			$('.settlementVoucher').addClass('validate[required]');  
			$('.cashOut').addClass('validate[required]');
			$('.codeCombination').removeClass('validate[required]');
		}else if(typearr[1]=='RFPC'){ 
			$('.cashoutdiv').hide(); 
			$('.expense_combi_div').show(); 
			$('.againstbilldiv').show(); 
			$('.billdiv').show();
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').show();  
			$('.receiptdiv').hide();
			$('.cashindiv').hide(); 
			$('.expensediv').show();
			$('.persondiv').show();
			$('.pettycash_detaildiv').hide();  
			$('.forwardvoucherdiv').hide();  
			$('.cashIn').removeClass('validate[required]');   
			$('.receiptRefNumber').removeClass('validate[required]');   
			$('.expenseAmount').addClass('validate[required]');
			$('.settlementVoucher').addClass('validate[required]');  
			$('.codeCombination').removeClass('validate[required]');
			$('.cashOut').removeClass('validate[required]');
		}else if(typearr[1]=='CFRD'){ 
			$('.cashoutdiv').hide(); 
			$('.expense_combi_div').hide(); 
			$('.againstbilldiv').hide(); 
			$('.billdiv').hide();
			$('.voucherDateDiv').show(); 
			$('.voucherdiv').show();  
			$('.forwardvoucherdiv').show();  
			$('.receiptdiv').hide();
			$('.cashindiv').show(); 
			$('.expensediv').hide();
			$('.pettycash_detaildiv').hide();  
			$('.persondiv').hide(); 
			$('.cashIn').addClass('validate[required]');   
			$('.receiptRefNumber').removeClass('validate[required]');   
			$('.expenseAmount').removeClass('validate[required]');
			$('.settlementVoucher').removeClass('validate[required]');  
			$('.codeCombination').removeClass('validate[required]');
			$('.cashOut').removeClass('validate[required]');
		}
	} 
}
function pettytypeAction(pettytypeName){ 
	var typearr = pettytypeName.split('@@'); 
	if(typearr[1]=="CTPC") {
		$('.receiptdiv').show();
		$('.cashindiv').show();
		$('.cashoutdiv').hide();
		$('.billdiv').hide();
		$('.voucherdiv').hide(); 
		$('.againstbilldiv').hide();
		$('.expensediv').hide();
		$('.pettycash_detaildiv').hide();  
		$('.expense_combi_div').hide();
		$('.persondiv').hide(); 
		$('.forwardvoucherdiv').hide();  
		$('.cashIn').addClass('validate[required]');
		$('.receiptRefNumber').addClass('validate[required]'); 
		$('.cashOut').removeClass('validate[required]'); 
		$('.expenseAmount').removeClass('validate[required]');
		$('.settlementVoucher').removeClass('validate[required]');  
		$('.codeCombination').removeClass('validate[required]');
		$('.personName').removeClass('validate[required]');
	} else if(typearr[1]=='AIPC'){
		$('.receiptdiv').hide();
		$('.cashindiv').hide();
		$('.expensediv').hide(); 
		$('.expense_combi_div').hide();
		$('.billdiv').hide();
		$('.voucherdiv').show(); 
		$('.voucher_log').show(); 
		$('.againstbilldiv').show();
		$('.cashoutdiv').show(); 
		$('.persondiv').show();
		$('.pettycash_detaildiv').hide();  
		$('#person').show();
		$('.forwardvoucherdiv').hide();  
		$('.cashOut').addClass('validate[required]');
		$('.settlementVoucher').addClass('validate[required]');    
		$('.expenseAmount').removeClass('validate[required]'); 
		$('.receiptRefNumber').removeClass('validate[required]');  
		$('.cashIn').removeClass('validate[required]');  
		$('.codeCombination').removeClass('validate[required]');
	}else if(typearr[1]=='SFAI'){
		$('.receiptdiv').hide();
		$('.cashindiv').hide();
		$('.cashoutdiv').hide();
		$('#person').hide();
		$('.expensediv').hide();
		$('.expense_combi_div').hide();
		$('.billdiv').hide();
		$('.voucherdiv').show(); 
		$('.voucher_log').show(); 
		$('.againstbilldiv').hide();
		$('.persondiv').hide();
		$('.forwardvoucherdiv').hide();  
		$('.receiptRefNumber').removeClass('validate[required]');  
		$('.cashIn').removeClass('validate[required]');  
		$('.cashOut').removeClass('validate[required]');
		$('.settlementVoucher').addClass('validate[required]');  
		$('.expenseAmount').removeClass('validate[required]');
		$('.codeCombination').removeClass('validate[required]');
	}else if(typearr[1]=='CRFS'){ 
		$('.voucherdiv').show(); 
		$('.cashindiv').show();
		$('.againstbilldiv').show();
		$('.receiptdiv').hide();
		$('.cashoutdiv').hide();
		$('.expensediv').hide();
		$('.expense_combi_div').hide();
		$('.billdiv').hide();  
		$('.persondiv').show();
		$('.voucher_log').show();
		$('.pettycash_detaildiv').hide();  
		$('.forwardvoucherdiv').hide();  
		$('.cashIn').addClass('validate[required]');  
		$('.settlementVoucher').addClass('validate[required]');  
		$('.receiptRefNumber').removeClass('validate[required]');  
		$('.cashOut').removeClass('validate[required]');
		$('.expenseAmount').removeClass('validate[required]');
		$('.codeCombination').removeClass('validate[required]'); 
	}else if(typearr[1]=='CGFS'){ 
		$('.voucherdiv').show(); 
		$('.cashoutdiv').show();
		$('.againstbilldiv').show();
		$('.receiptdiv').hide();
		$('.cashindiv').hide();
		$('.expensediv').hide();
		$('.expense_combi_div').hide();
		$('.billdiv').show(); 
		$('.persondiv').show(); 
		$('.voucher_log').show(); 
		$('.pettycash_detaildiv').hide();  
		$('.forwardvoucherdiv').hide();  
		$('.cashIn').removeClass('validate[required]');   
		$('.receiptRefNumber').removeClass('validate[required]');   
		$('.expenseAmount').removeClass('validate[required]');
		$('.settlementVoucher').addClass('validate[required]');  
		$('.cashOut').addClass('validate[required]');
		$('.codeCombination').removeClass('validate[required]');
	}else if(typearr[1]=='RFPC'){ 
		$('.cashoutdiv').hide(); 
		$('.expense_combi_div').show(); 
		$('.againstbilldiv').show(); 
		$('.billdiv').show();
		$('.voucherdiv').show();  
		$('.receiptdiv').hide();
		$('.cashindiv').hide(); 
		$('.expensediv').show();
		$('.voucher_log').show(); 
		$('.persondiv').show();
		$('.forwardvoucherdiv').hide();  
		$('.pettycash_detaildiv').hide();  
		$('.cashIn').removeClass('validate[required]');   
		$('.receiptRefNumber').removeClass('validate[required]');   
		$('.expenseAmount').addClass('validate[required]');
		$('.settlementVoucher').addClass('validate[required]');  
		$('.codeCombination').removeClass('validate[required]');
		$('.cashOut').removeClass('validate[required]');
	}else if(typearr[1]=='CFRD'){ 
		$('.cashoutdiv').hide(); 
		$('.expense_combi_div').hide(); 
		$('.againstbilldiv').hide(); 
		$('.billdiv').hide();
		$('.voucherdiv').show();  
		$('.forwardvoucherdiv').show();  
		$('.receiptdiv').hide();
		$('.cashindiv').show(); 
		$('.expensediv').hide();
		$('.voucher_log').show(); 
		$('.pettycash_detaildiv').hide();  
		$('.persondiv').hide(); 
		$('.cashIn').addClass('validate[required]');   
		$('.receiptRefNumber').removeClass('validate[required]');   
		$('.expenseAmount').removeClass('validate[required]');
		$('.settlementVoucher').removeClass('validate[required]');  
		$('.codeCombination').removeClass('validate[required]');
		$('.cashOut').removeClass('validate[required]');
	} 
} 
function personPopupResult(personid, personname, commonParam){
	$('#personId').val(personid);
	$('#personName').val(personname);
	$('#common-popup').dialog("close");   
}
function callReceiptReference(param1, param2, param3, param4){ 
	 $('#receiptRefId').val(param1);
	 $('#receiptRefType').val(param2);
	 $('#receiptRefNumber').val(param3);
	 $('#cashIn').val(param4);
	 $('.cashInMaxValue').html("Max("+param4+")"); 
	 $jquery('.allowMaxAmount').val(param4);
	 $('#common-popup').dialog("close");   
}
function getMaximumPettyCash(settlementVoucherId) {  
	var typearr = $('#pettyCashType').val().split('@@'); 
	if(typearr[1]!="CTPC" && typearr[1]!="CRFS" && forwardFlag == false) {
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_maximum_pettycash.action", 
		 	async: false,  
		 	data : {settlementVoucherId: settlementVoucherId}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $(".tempresult").html(result);
				 $('.maxValue').html("Max("+result+")");
			} 
		});
	} else {
		return false;
	}
	return false;
}
function commonPettyCashDetail(pettyCashId, personId, expenseAmount,  voucherNumber, employee, invoiceNumber,cashOut){
	if(forwardFlag == false){
		  $('#settlementVoucherId').val(pettyCashId); 
		  $('#settlementVoucher').val(voucherNumber);  
		  var typearrs = $('#pettyCashType').val().split('@@'); 
		  if(typearrs[1]=="CRFS" || typearrs[1]=="CFRD")
			$('#cashIn').val(expenseAmount);
		  else if(typearrs[1]=="SFAI") 
			  $('#expenseAmount').val(expenseAmount);  
		  getMaximumPettyCash(pettyCashId);
	} else{
		  $('#forwardVoucherId').val(pettyCashId); 
		  $('#forwardVoucher').val(voucherNumber); 
		  $('.forward_voucher_log').show();  
	}
	$('.voucher_log').show(); 
	$('#pettycashvoucher-close').trigger('click');
	return false;
 }
function commonPettyCashInVouchers(pettyCashId, voucherNumber, cashIn){ 
	$('#settlementVoucherId').val(pettyCashId); 
	$('#settlementVoucher').val(voucherNumber); 
	var pettyCashType = $('#pettyCashType').val(); 
    var pettyCashCode = $.trim(pettyCashType.split('@@')[1]);
    if(pettyCashCode=="CFRD"){
    	$('#cashIn').val(cashIn);  
	}else{
		$('#cashOut').val(cashIn);  
	} 
	$('.cashOutMaxValue').html("Max("+cashIn+")"); 
	$('.voucher_log').show(); 
	$('#pettycashvoucher-close').trigger('click');
	return false;
}
function commonPettyCashInForwardVouchers(pettyCashId, voucherNumber, cashIn){ 
	$('#forwardVoucherId').val(pettyCashId); 
	$('#forwardVoucher').val(voucherNumber); 
	$('.voucher_log').show(); 
	$('#pettycashvoucher-close').trigger('click');
	return false;
}
function commonPettyAdvIssueVouchers(pettyCashId, voucherNumber, cashOut){
	$('#settlementVoucherId').val(pettyCashId); 
	$('#settlementVoucher').val(voucherNumber); 
	var pettyCashType = $('#pettyCashType').val().split('@@')[1];  
	if(pettyCashType == "CRFS"){
		$('#cashIn').val(cashOut);
		$('.cashInMaxValue').html("Max("+cashOut+")"); 
	}else if(pettyCashType == "SFAI"){
		$('#maximumExpense').html(cashOut);
		$('.spend_detail_div').show();
	}else if(pettyCashType == "RFPC"){
		$('.expenseMaxValue').html("Max("+cashOut+")"); 
	} else{
		getCashTransferPettyCashAmount(pettyCashId);
	}
	$('.voucher_log').show(); 
	$('#advissueVoucher-close').trigger('click');
 	return false;
}
function commonPettyAdvIssueForward(pettyCashId, voucherNumber, cashOut){
	$('#settlementVoucherId').val(pettyCashId); 
	$('#settlementVoucher').val(voucherNumber); 
	$('.cashInMaxValue').html("Max("+cashOut+")"); 
	$('.voucher_log').show(); 
	$('#advissueVoucherforward-close').trigger('click');
 	return false;
}
function commonPettySpendVouchers(pettyCashId, voucherNumber, expenseAmount){
	$('#settlementVoucherId').val(pettyCashId); 
	$('#settlementVoucher').val(voucherNumber); 
	$('.cashInMaxValue').html("Max("+expenseAmount+")"); 
	$('.voucher_log').show(); 
	 var pettyCashType = $('#pettyCashType').val(); 
     var pettyCashCode = $.trim(pettyCashType.split('@@')[1]);
     if(pettyCashCode=="CGFS"){
    	 getAdvanceIssuePettyCashAmount(pettyCashId);
	 }
	$('#spendVoucher-close').trigger('click'); 
 	return false;
}
function getAdvanceIssuePettyCashAmount(pettyCashId) {  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/get_advanceissue_pettycash.action", 
	 	async: false,  
	 	data : {pettyCashId: pettyCashId}, 
	    dataType: "json",
	    cache: false,
		success:function(response){   
 			 $('.cashOutMaxValue').html("Max("+response.cashOut+")");
		}  
	}); 
	return false;
}
function getCashTransferPettyCashAmount(pettyCashId) {  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/get_cashtransfer_pettycash.action", 
	 	async: false,  
	 	data : {pettyCashId: pettyCashId}, 
	    dataType: "json",
	    cache: false,
		success:function(response){   
			$('#cashOut').val(response.cashIn);
 			$('.cashOutMaxValue').html("Max("+response.cashIn+")");
		}  
	}); 
	return false;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>petty cash</div>		 	 
		  <form name="pettycashentry_details" id="pettycashentry_details" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
				<div id="page-success" class="response-msg success ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="pettyCashId" name="pettyCashId" value="${PETTYCASH_INFO.pettyCashId}"/>
			  	<input type="text" class="allowMaxAmount" style="display: none;" readonly="readonly"/>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="min-height: 145px;"> 
							<div class="persondiv">
								<label class="width30" for="personName">Employee<span class="mandatory">*</span></label> 
								<input type="hidden" id="personId" name="personId" value="${PETTYCASH_INFO.personByPersonId.personId}"/>
								<input type="text" readonly="readonly" name="personName" id="personName" 
									class="width50 validate[required] personName" value="${PETTYCASH_INFO.employeeName}"/>
								 <span class="button">
									<a style="cursor: pointer;" id="person" class="btn ui-state-default ui-corner-all common-popup-person width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
							</div>  
							<div class="cashindiv">
								<label class="width30" for="cashIn">Cash In<span class="mandatory">*</span></label>
								<input type="text" name="cashIn" id="cashIn" class="cashIn width50" value="${PETTYCASH_INFO.cashIn}">
								<span class="cashInMaxValue" style="margin-top:5px;"></span>
							</div>  
							<div class="cashoutdiv" style="display:none;">
								<label class="width30" for="cashOut">Cash Out<span class="mandatory">*</span></label>
								<input type="text" name="cashOut" id="cashOut" class="width50 cashOut" value="${PETTYCASH_INFO.cashOut}">
								<span class="cashOutMaxValue"></span>
							</div>  
							<div style="display: none;" class="billdiv">
								<label class="width30" for="invoiceNo">Bill No</label>  
								<input type="text" name="invoiceNo" id="invoiceNo" class="width50" value="${PETTYCASH_INFO.invoiceNumber}"/> 
							</div>  
							<div class="expensediv" style="display:none;">
								<label class="width30" for="expenseAmount">Expense Amount<span class="mandatory">*</span></label>
								<input type="text" name="expenseAmount" id="expenseAmount" class="width50 expenseAmount" value="${PETTYCASH_INFO.expenseAmount}">
								<span class="expenseMaxValue">${PETTYCASH_INFO.cashOut}</span>
							</div>  
							<div class="expense_combi_div" style="display:none;">
								<label class="width30" for="expenseAmount">Expense COA<span class="mandatory">*</span></label>
								<input type="hidden" name="combinationId" id="combinationId" value="${PETTYCASH_INFO.combination.combinationId}">
								<input type="text" name="codeCombination" id="codeCombination" class="width50 codeCombination"
									value="${PETTYCASH_INFO.combinationStr}">
								<span class="button">
									<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div>  
							<div>
								<label class="width30 tooltip">Description</label>
								<textarea rows="2" id="description" class="width51 float-left">${PETTYCASH_INFO.description}</textarea> 
							</div>
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="min-height: 145px;"> 
							<div>
								<label class="width30" for="pettyCashNo">Petty Cash No<span class="mandatory">*</span></label> 
								<c:choose>
									<c:when test="${PETTYCASH_INFO.pettyCashId ne null && PETTYCASH_INFO.pettyCashId ne '' && PETTYCASH_INFO.pettyCashId ne 0}">
										<input type="text" readonly="readonly" name="pettyCashNo" id="pettyCashNo" 
											class="width50 validate[required]" value="${PETTYCASH_INFO.pettyCashNo}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="pettyCashNo" id="pettyCashNo" 
											class="width50 validate[required]" value="${PETTY_CASH_NUMBER}"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div class="voucherDateDiv">
								<label class="width30" for="voucherDate">Date<span
									style="color: red;">*</span> </label>  
								<input type="text" id="voucherDate" name="voucherDate" value="${PETTYCASH_INFO.voucherDateView}"
									class="voucherDate width50" readonly="readonly"/>
							</div>
							<div>
								<label class="width30" for="pettyCashType">Type<span class="mandatory">*</span></label> 
								<input type="hidden" id="temppettyCashType" name="temppettyCashType" 
									value="${PETTYCASH_INFO.pettyCashTypeName}"/>
								<select name="pettyCashType" id="pettyCashType" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="PETTY_TYPES" items="${PETTY_CASH_TYPES}">
										<option value="${PETTY_TYPES.key}">${PETTY_TYPES.value}</option>
									</c:forEach>
								</select> 
							</div> 
							<div class="receiptdiv">
								<label class="width30" for="receiptRefNumber">Receipt Ref<span class="mandatory">*</span></label> 
								<input type="hidden" id="receiptRefId" name="receiptRefId" value="${PETTYCASH_INFO.receiptVoucherId}"/>
								<input type="hidden" id="receiptRefType" name="receiptRefType" value="${PETTYCASH_INFO.receiptReference}"/>
								<input type="text" readonly="readonly" name="receiptRefNumber" id="receiptRefNumber" 
									class="width50 receiptRefNumber validate[required]" value="${PETTYCASH_INFO.receiptVoucher}"/> 
								<span class="button">
									<a style="cursor: pointer;" id="receiptref" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>  
							</div> 
							
							<div style="display: none;" class="voucherdiv">
								<label class="width30" for="settlementVoucher">Voucher No<span class="mandatory">*</span></label> 
								<input type="hidden" id="settlementVoucherId" name="settlementVoucherId" value="${PETTYCASH_INFO.parentPettyCashId}"/>
								<input type="text" readonly="readonly" name="settlementVoucher" id="settlementVoucher" 
									class="width50 settlementVoucher" value="${PETTYCASH_INFO.parentReference}"/> 
								<span class="button">
									<a style="cursor: pointer;" id="settlvoucher" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
								<span style="display :none; text-decoration: underline; cursor: pointer;" class="voucher_log">Log</span> 
							</div>  
							<div style="display: none;" class="forwardvoucherdiv">
								<label class="width30" for="forwardVoucher">Receipt Voucher<span class="mandatory">*</span></label> 
								<input type="hidden" id="forwardVoucherId" name="forwardVoucherId" value=""/>
								<input type="text" readonly="readonly" name="forwardVoucher" id="forwardVoucher" class="width50 forwardVoucher" value=""/> 
								<span class="button">
									<a style="cursor: pointer;" id="forwardVoucherref" class="btn ui-state-default ui-corner-all forward-common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
								<span style="display :none; text-decoration: underline; cursor: pointer;" class="forward_voucher_log">Log</span> 
							</div>  
							<div>
								<label class="width30" for="settlementReq">Settlement Required</label>
								<select name="status" id="settlementReq" class="validate[required]" style="width:51%;" disabled="disabled"> 
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
								<input type="hidden" id="tempsettlementReq" name="tempsettelmentReq" value="${PETTYCASH_INFO.settelmentFlag}"/>
							</div> 
							<div style="display: none;" class="againstbilldiv">
								<label class="width30" for="againstInvoice">Settled Against Bill</label>
								<select name="status" id="againstInvoice" class="validate[required]" style="width:51%;" disabled="disabled"> 
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
								<input type="hidden" id="tempagainstInvoice" name="tempagainstInvoice" value="${PETTYCASH_INFO.againstInvoice}"/>
							</div> 
						</fieldset>
					</div> 
			</div>  
			<div class="clearfix"></div> 
			<div class="portlet-content class90 pettycash_detaildiv" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							PettyCash Detail<span
										style="color: red;">*</span>
						</legend>
						<div id="page-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="warning_message"
							class="response-msg notice ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100"> 
							<table id="hastab" class="width100">
								<thead>
									<tr> 
										<th style="width: 5%">Person</th>
										<th style="width: 8%">Combination</th>
										<th style="width: 2%">Amount</th>
										<th style="width: 2%">Bill</th>
										<th style="width: 5%">Description</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab"> 
									<c:choose>
										<c:when
											test="${PETTYCASH_DETAIL ne null && fn:length(PETTYCASH_DETAIL) > 0}">
											<c:forEach var="bean" items="${PETTYCASH_DETAIL}"
												varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
													<td>
														<input type="text" id="personName_${status.index+1}" class="personid width80" value="${bean.personName}"/>
														<input type="hidden" id="personId_${status.index+1}" value="${bean.person.personId}"/>
														<span class="button float-right"> <a
															style="cursor: pointer; position: relative; top:5px;" id="personID_${status.index+1}"
															class="btn ui-state-default ui-corner-all show_expense_person_list width100">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
													</td>
													<td><input type="hidden" value="${bean.combination.combinationId}"
														name="combinationId_${status.index+1}" 
														id="combinationId_${status.index+1}" />  
														<input type="text" id="codeCombination_${status.index+1}" readonly="readonly" 
															class="width80" value="${bean.combinationStr}"/>
														<span class="button" id="codeID_${status.index+1}"> 
															<a style="cursor: pointer;"
															class="btn ui-state-default ui-corner-all codecombination-popup-transaction width100">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
													</td> 
													<td>
														<input type="text" name="linesExpenseAmount" id="linesExpenseAmount_${status.index+1}" style="text-align: right;"
															value="${bean.expenseAmount}" class="linesExpenseAmount width98 right-align validate[optional,custom[number]]">
													</td>
													<td>
														<input type="text" name="billno" value="${bean.billNo}" id="billno_${status.index+1}" class="billno width98">
													</td>
													<td>
														<input type="text" name="linesDescription" value="${bean.description}" id="linesDescription_${status.index+1}" class="width98">
													</td> 
													<td style="width: 1%;" class="opn_td"
														id="option_${status.index+1}"> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}"
														style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> 
														<input type="hidden" id="pettyCashDetailId_${status.index+1}" value="${bean.pettyCashDetailId}"/>
													</td> 
												</tr>
											</c:forEach>
										</c:when>
									</c:choose> 
									<c:forEach var="i" begin="${fn:length(PETTYCASH_DETAIL)+1}" end="${fn:length(PETTYCASH_DETAIL)+2}"
										step="1" varStatus="status1">
										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td>
												<input type="text" id="personName_${i}" class="personid width80"/>
												<input type="hidden" id="personId_${i}"/>
												<span class="button float-right"> <a
													style="cursor: pointer; position: relative; top:5px;" id="personID_${i}"
													class="btn ui-state-default ui-corner-all show_expense_person_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="hidden"
												name="combinationId_${i}" 
												id="combinationId_${i}" />  
												<input type="text" id="codeCombination_${i}" readonly="readonly" class="width80"/>
												<span class="button" id="codeID_${i}">
													<a style="cursor: pointer;"
													class="btn ui-state-default ui-corner-all codecombination-popup-transaction width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td>
												<input type="text" name="linesExpenseAmount" id="linesExpenseAmount_${i}" style="text-align: right;"
													 class="linesExpenseAmount width98 right-align validate[optional,custom[number]]">
											</td>
											<td>
												<input type="text" name="billno" id="billno_${i}" class="billno width98">
											</td>
											<td>
												<input type="text" name="linesDescription" id="linesDescription_${i}" class="width98">
											</td> 
											<td style="width: 1%;" class="opn_td"
												id="option_${i}"> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer;" title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> 
												<input type="hidden" id="pettyCashDetailId_${i}"/>
											</td> 
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div id="hrm" class="hastable width40 spend_detail_div" style="display: none; margin-top: 10px;"> 
							<table id="hastab" class="width100">
								<thead>
									<tr> 
										<th>Maxium Amount</th>
										<th>Total Expense</th>
										<th>Pending Amount</th>  
									</tr>
								</thead>
								<tr>
									<td id="maximumExpense">${PETTYCASH_INFO.cashOut}</td>
									<td id="totalSpendExpense">${PETTYCASH_INFO.expenseAmount}</td>
									<td id="pendingExpenseAmount">${PETTYCASH_INFO.remainingBalance}</td>
								</tr>  
							</table>
						</div>
						<div class="clearfix"></div> 
					</fieldset>
				</div>
			<div class="clearfix"></div>  
			<div id="temp-result" style="display:none;"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right petty_discard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
				<div class="portlet-header ui-widget-header float-right petty_save" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
			</div> 
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>   
		</div> 
		
		<span class="callJq"></span>
		<div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>  
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		 </div>
 	 </form>
  </div> 
</div>