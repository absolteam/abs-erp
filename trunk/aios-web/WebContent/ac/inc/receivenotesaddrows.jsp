<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td style="display:none;" id="lineId_${rowId}">${rowId}</td>
	<td id="productcode_${rowId}">
		<select id="productCode_${rowId}" class="autocompletecode productcodeclass_${rowId}" name="productCode">
			<option value="">Select</option>
			<c:forEach items="${PRODUCT_INFO}" var="bean">
				<option value="${bean.productId}">${bean.code}</option>
			</c:forEach>
		</select>
	</td>
	<td id="productname_${rowId}">
		<select id="productName_${rowId}" class="autocomplete productnameclass_${rowId}" name="productName">
			<option value="">Select</option>
			<c:forEach items="${PRODUCT_INFO}" var="bean">
				<option value="${bean.productId}@@${bean.lookupDetailByProductUnit.displayName}">${bean.productName}</option>
			</c:forEach>
		</select>
	</td>
	<td id="uom_${rowId}"></td>
	<td id="receiveqty_${rowId}">
		<input type="text" class="width60 receiveQty" name="receiveQty" id="receiveQty_${rowId}"/>
	</td>  
	<td style="display:none">
		<input type="hidden" name="receivelineid" id="receivelineid_${rowId}"/>
		<input type="hidden" name="productid" id="productid_${rowId}"/> 
	</td> 
	 <td style="width:1%;" class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataRow" id="AddImage_${requestScope.rowId}" style="cursor:pointer;display:none;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataRow" id="EditImage_${requestScope.rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowRow" id="DeleteImage_${requestScope.rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${requestScope.rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>
<script type="text/javascript">
$(function(){ 
	 $(".autocomplete").combobox({ 
	       selected: function(event, ui){
		      	var productVal=$(this).val();
		      	proArray=productVal.split('@@');
		        var unitName=proArray[1];
		        slidetab=$(this).attr('id'); 
		  		var idarray =slidetab.split('_');
		  		var rowid=Number(idarray[1]);  
		  		$('#uom_'+rowid).text(unitName);
		  		var receiveQty=$('#receiveQty_'+rowid).val();
		  		 var nexttab=slidetab=$(this).parent().parent().get(0);  
			  	 nexttab=$(nexttab).next();
		  		triggerAddRow(receiveQty,unitName,nexttab);  
		  		//change product code
		  		var productName=$(this).val(); 
		  		$('#productCode_'+rowid+' option').each(function(){
			 		 var productId=$(this).val(); 
			 		 var splitval=productName.split("@@");
			 		 productName=splitval[0]; 
			 		 if(productId==productName){
			 			productName=$(this).text();   
			 			$('.productcodeclass_'+rowid).combobox('autocomplete', productName);
			 			$('#productid_'+rowid).val(productId);
			 		 }
			 	 });
	       }
	    }); 
		 
		 $('.autocompletecode').combobox({  
			 selected: function(event, ui){
				 var elVal=$(this).val(); 
				 slidetab=$(this).attr('id'); 
			  	 var idarray =slidetab.split('_');
			  	 var rowid=Number(idarray[1]);  
			  	 var nexttab=slidetab=$(this).parent().parent().get(0);  
			  	 nexttab=$(nexttab).next();
			  	 $('#productName_'+rowid+' option').each(function(){
			 		 var productId=$(this).val(); 
			 		 var splitval=productId.split("@@");
			 		 productId=splitval[0]; 
			 		 if(elVal==productId){
			 			var productName=$(this).text();  
			 			$('.productnameclass_'+rowid).combobox('autocomplete', productName); 
			 			$('#uom_'+rowid).text(splitval[1]);
			 			$('#productid_'+rowid).val(productId);
			 		 }
			 	 });
			  	var receiveQty=$('#receiveQty_'+rowid).val();
			  	var unitVal=$('#uom_'+rowid).text();
			  	triggerAddRow(receiveQty,unitVal,nexttab); 
	         } 
		 });
	
$('.addDataRow').click(function(){
		if($("#receivenotesValidation").validationEngine({returnIsValid:true})){   
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]); 
		 
		 var showPage="";
		 var receiveId=$('#receiveId').val();
		 if(receiveId !=null && receiveId!="" && receiveId>0)
			 showPage="editadd";
		 else
			 showPage="addadd";
		 
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/receivenotes_lineentry.action",
			data:{id:rowid, showPage:showPage},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			 	$('#lineNumber').val($('#lineId_'+rowid).text());
			}
		 });
		}
		else{
			return false;
		}		
	});
	
	$('.editDataRow').click(function(){
		if($("#receivenotesValidation").validationEngine({returnIsValid:true})){   
			 $('.error').hide(); 
			 slidetab=$(this).parent().parent().get(0);   
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			  
			 var showPage="";
			 var receiveId=$('#receiveId').val();
			 if(receiveId !=null && receiveId!="" && receiveId>0)
				 showPage="editedit";
			 else
				 showPage="addedit";
			 
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).hide();
			 $("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/receivenotes_lineentry.action",
				data:{id:rowid,showPage:showPage},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 	$(result).insertAfter(slidetab); 
				 	$('#lineNumber').val($('#lineId_'+rowid).text()); 
				 	
				 	//Bussiness parameter  
		        	$('#itemNature').val($('#itemnature_'+rowid).text().substring(0,1).toUpperCase());  
				 
		        	$('#productId').val($('#productid_'+rowid).val()); 
		        	$('#productName').val($('#productname_'+rowid).text());  
		        	$('#uom').val($('#uom_'+rowid).text());
	        		$('#receiveQty').val($('#receiveqty_'+rowid).text());  
				}
			 });
			}
			else{
				return false;
			}		
	 });
	
	$(".delrowRow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var receiveQty=$('#receiveQty_'+rowid).val().trim();
		 if(receiveQty!=null && receiveQty!=""){
			 var flag=true; 
			 <%-- 
			 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/receive_deleteline.action", 
					 data:{id:lineId},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.formError').hide();  
						 $('.tempresult').html(result);   
						 if(result!=null){
						    	if($('#returnMsg').html() != '' || $('#returnMsg').html() == "Success")
	                              flag = true;
						    	else{	
							    	flag = false; 
							    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						    	} 
						     }  
					},
					error:function(result){
						$("#page-error").hide().html($('#returnMsg').html().slideDown()); 
						$('.tempresult').html(result);   
						return false;
					}
						
				 });  --%>
				 if(flag==true){ 
		        		 var childCount=Number($('#childCount').val());
		        		 if(childCount > 0){
							childCount=childCount-1;
							$('#childCount').val(childCount); 
		        		 } 
		        		 $(slidetab).remove(); 
		        		//To reset sequence 
		        		var i=0;
		     			$('.rowid').each(function(){  
							i=i+1; 
	   					 });  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }  
	 });
});
</script>