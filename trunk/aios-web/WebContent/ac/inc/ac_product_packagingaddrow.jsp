<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><select name="productUnitId" id="productUnitId_${requestScope.rowId}"
		class="width70">
			<option value="">Select</option>
			<c:forEach var="productUnit" items="${PRODUCT_UNITS}">
				<option value="${productUnit.lookupDetailId}">${productUnit.displayName}</option>
			</c:forEach>
	</select> <span class="button float-right"> <a style="cursor: pointer;"
			id="PRODUCT_UNITNAME_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all product-unitlookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" name="quantity" id="quantity_${requestScope.rowId}"
		class="quantity validate[optional,custom[number]] width80"></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;" title="Working"> <span
			class="processing"></span> </a> <input type="hidden"
		name="productPackageDetailId" id="productPackageDetailId_${requestScope.rowId}" /></td>
</tr>