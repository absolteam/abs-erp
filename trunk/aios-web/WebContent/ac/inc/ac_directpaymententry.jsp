<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname="";
var accountTypeId=0;
var paymentLineDetails="";
var getPaymentDetails =null;
$(function(){
	$jquery(".amount,.totalPaymentAmountH").number(true, 2); 
	if(Number($('#directPaymentId').val()) > 0){
		calculateTotalPayment();
		$('#paymentrequest').remove();
		$('#paymentCurrency').val("${DIRECT_PAYMENT.currency.currencyId}");
		var payMode = "${DIRECT_PAYMENT.paymentMode}";  
		if("${DIRECT_PAYMENT.paymentMode}" == 6) {
			$('#accountId').val('');
			$('#cardAccountId').val('');
			$('.interaccount').show(); 
			$('#paymentMode option[value='+payMode+'@@IAT]').attr("selected","selected");  
		 } else  if("${DIRECT_PAYMENT.paymentMode}" == 2) {
			$('#transferAccountId').val('');
			$('#cashCombinationId').val('');
			$('#codeCombination').val('');
			$('#cardAccountId').val('');
			$('#paymentMode option[value='+payMode+'@@CHQ]').attr("selected","selected");  
			$('.bank').show();
		 } else  if("${DIRECT_PAYMENT.paymentMode}" == 3 || "${DIRECT_PAYMENT.paymentMode}" == 4) { 
			$('#accountId').val('');
			$('#transferAccountId').val('');
			 if("${DIRECT_PAYMENT.paymentMode}" == 3)
				$('#paymentMode option[value='+payMode+'@@DCD]').attr("selected","selected");  
			 else
				 $('#paymentMode option[value='+payMode+'@@CCD]').attr("selected","selected");  
			$('.cards').show();
		 } else  if("${DIRECT_PAYMENT.paymentMode}" == 1 || "${DIRECT_PAYMENT.paymentMode}" == 5) { 
			 $('#accountId').val('');
			 $('#transferAccountId').val('');
			 $('#cardAccountId').val('');
			 if("${DIRECT_PAYMENT.paymentMode}" == 1)
				$('#paymentMode option[value='+payMode+'@@CSH]').attr("selected","selected");  
			 else
				 $('#paymentMode option[value='+payMode+'@@WRT]').attr("selected","selected");  
			$('.cash').show();
		 } 
		var payeetype = "";  
		$('#supplier').show();
		if("${DIRECT_PAYMENT.supplier}" != null && "${DIRECT_PAYMENT.supplier}" !=""){
			payeetype = "SPL"; 
			$('#supplierId').val("${DIRECT_PAYMENT.supplier.supplierId}"); 
		} else if("${DIRECT_PAYMENT.customer}" != null && "${DIRECT_PAYMENT.customer}" !=""){
			payeetype = "CST"; 
			$('#supplierId').val("${DIRECT_PAYMENT.customer.customerId}");
		}else if("${DIRECT_PAYMENT.personByPersonId}" != null && "${DIRECT_PAYMENT.personByPersonId}" !=""){
			payeetype = "EMP"; 
			$('#supplierId').val("${DIRECT_PAYMENT.personByPersonId.personId}");
		}else if("${DIRECT_PAYMENT.others}" != null && "${DIRECT_PAYMENT.others}" !=""){
			payeetype = "OT"; 
			$('#supplierName').attr('readOnly',false);
			$('#supplier').hide();
		} 
		$('#payeeType option').each(function(){
			var thisval = $(this).val();
			var thisarray = thisval.split('@@'); 
			if(thisarray[1]==payeetype){
				$('#payeeType option[value='+$(this).val()+']').attr("selected","selected"); 
				return false; 
			}
		}); 
		$('#directPaymentDate').datepick(); 
	}
	else{
		$('#paymentCurrency').val($('#default_currency').val()); 
		$('#directPaymentDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
	} 
	if(Number($('#messageId').val()) > 0 || Number($('#alertId').val()) > 0){
		var payeetype = "";  
		$('#supplier').show();
		//$('.codecombination-popup').remove();
		var payMode = "${DIRECT_PAYMENT.paymentMode}";  
		if("${DIRECT_PAYMENT.paymentMode}" == 6) {
			$('#accountId').val('');
			$('#cardAccountId').val('');
			$('.interaccount').show(); 
			$('#paymentMode option[value='+payMode+'@@IAT]').attr("selected","selected");  
		 } else  if("${DIRECT_PAYMENT.paymentMode}" == 2) {
			$('#transferAccountId').val('');
			$('#cashCombinationId').val('');
			$('#codeCombination').val('');
			$('#cardAccountId').val('');
			$('#paymentMode option[value='+payMode+'@@CHQ]').attr("selected","selected");  
			$('.bank').show();
		 } else  if("${DIRECT_PAYMENT.paymentMode}" == 3 || "${DIRECT_PAYMENT.paymentMode}" == 4) { 
			$('#accountId').val('');
			$('#transferAccountId').val('');
			 if("${DIRECT_PAYMENT.paymentMode}" == 3)
				$('#paymentMode option[value='+payMode+'@@DCD]').attr("selected","selected");  
			 else
				 $('#paymentMode option[value='+payMode+'@@CCD]').attr("selected","selected");  
			$('.cards').show();
		 } else  if("${DIRECT_PAYMENT.paymentMode}" == 1 || "${DIRECT_PAYMENT.paymentMode}" == 5) { 
			 $('#accountId').val('');
			 $('#transferAccountId').val('');
			 $('#cardAccountId').val('');
			 if("${DIRECT_PAYMENT.paymentMode}" == 1)
				$('#paymentMode option[value='+payMode+'@@CSH]').attr("selected","selected");  
			 else
				 $('#paymentMode option[value='+payMode+'@@WRT]').attr("selected","selected");  
			$('.cash').show();
		 } 
		
		
		if("${DIRECT_PAYMENT.supplier}" != null && "${DIRECT_PAYMENT.supplier}" !=""){
			payeetype = "SPL"; 
			$('#supplierId').val("${DIRECT_PAYMENT.supplier.supplierId}"); 
		} else if("${DIRECT_PAYMENT.customer}" != null && "${DIRECT_PAYMENT.customer}" !=""){
			payeetype = "CST"; 
			$('#supplierId').val("${DIRECT_PAYMENT.customer.customerId}");
		}else if("${DIRECT_PAYMENT.personByPersonId}" != null && "${DIRECT_PAYMENT.personByPersonId}" !=""){
			payeetype = "EMP"; 
			$('#supplierId').val("${DIRECT_PAYMENT.personByPersonId.personId}");
		}else if("${DIRECT_PAYMENT.others}" != null && "${DIRECT_PAYMENT.others}" !=""){
			payeetype = "OT"; 
			$('#supplierName').attr('readOnly',false);
			$('#supplier').hide();
		}else{
			payeetype = "OT"; 
			$('#supplierName').attr('readOnly',false);
			$('#supplier').hide();
		} 
		$('#payeeType option').each(function(){
			var thisval = $(this).val();
			var thisarray = thisval.split('@@'); 
			if(thisarray[1]==payeetype){
				$('#payeeType option[value='+$(this).val()+']').attr("selected","selected"); 
				return false; 
			}
		}); 
		calculateTotalPayment();
	}
	$('#chequeDate').datepick();
 
	$jquery("#directPaymentCreation").validationEngine('attach');

	manupulateLastRow();
	
	$('.common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();  
	    var idname= "";
	    idname = $(this).attr('id'); 
	    var rowId=-1; 
        var personTypes="1"; 
        var cardTypeInfo = "";
        var paymentModeId = 0;
 	    if(idname == "bankaccountid"){
	    	actionname = "getbankaccountwithcheque_details";  
		}else if(idname == "transferAccountid"){
			actionname = "getbankaccount_details";
		}else if(idname == "cardaccountid"){
			var cardtypes = $('#paymentMode').val();
			var cardTypeArray = cardtypes.split('@@'); 
			cardTypeInfo = cardTypeArray[1]; 
			$('#cardNumber').val('');
			$('#cardAccountId').val('');
 			actionname = "get_creditdebitcard_details"; 
		}  
		else if(idname == "paymentrequest"){
			var paymode = $('#paymentMode').val();
			var payModeArray = paymode.split('@@'); 
			paymentModeId = payModeArray[0];
			if(paymentModeId==null || paymentModeId!='')
				actionname ="show_allapproved_payment_request"; 
			else
				return false;
		}  
		else if(idname == "supplier"){ 
			var payeetypes = $('#payeeType').val();
			var payTypeArray = payeetypes.split('@@'); 
			if(payTypeArray[1]=="CST")  
				actionname="show_customer_common_popup";
			else if(payTypeArray[1]=="SPL") 
				actionname="show_common_supplier_popup";
			else if(payTypeArray[1]=="EMP"){
				actionname="common_person_list"; 
			} else
				return false;
		}  
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+actionname+".action", 
			 	async: false,  
			 	data : {pageInfo : "directPayment", id: rowId, personTypes: personTypes,
			 			 cardTypeInfo: cardTypeInfo, paymentModeId: paymentModeId}, 
			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.common-result').html(result);   
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0); 
					return false;
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});  
			return false;
		});
		$('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		$('#supplier-common-popup').live('click',function(){  
			   $('#common-popup').dialog('close'); 
		   });

		 $('#customer-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });

		 $('#paymentrequest-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });

		$('#payeeType').change(function(){
			$('#supplierName').val(''); 
			if($(this).val()!=null && $(this).val()!=''){
				var payeetypes = $('#payeeType').val();
				var payTypeArray = payeetypes.split('@@');  
				if(payTypeArray[1]=="OT"){
					$('#supplier').hide();
					$('#supplierName').attr('readOnly',false);
				} else {
					$('#supplier').show();
					$('#supplierName').attr('readOnly',true);
				}
			}
		});

		//Combination pop-up config
	    $('.codecombination-popup').live('click',function(){
	        tempid=$(this).parent().get(0);
	        accountTypeId=0; 
	        actionname="";
	        $('.codeCombinationformError').remove();
	        $('.ui-dialog-titlebar').remove();  
	        actionname=$(tempid).attr('id');
	        var idVal=actionname.split('_');
	        if(typeof(idVal[1]!='undefined') && idVal[1]!=null && idVal[1]!=""){
	      	  actionname=idVal[0];
	        }
	        else{
	      	  actionname=$(tempid).attr('id');
	        }  
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/combination_treeview.action",
	             async: false, 
	             dataType: "html",
	             cache: false,
	             success:function(result){
	                  $('.codecombination-result').html(result); 
	             },
	             error:function(result){
	                  $('.codecombination-result').html(result);
	             }
	         });
	         return false;
	    }); 
	     
	     $('#codecombination-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800, 
				bgiframe: false,
				modal: true 
			});
		
	$('.modeOfPay').change(function(){
		$('#cashCombinationId').val(''); 
	    $('.cashCodeCombination').val(''); 
	    $('#accountNumber').val(''); 
	    $('.chequeNumber').val(''); 
	    $('#chequeDate').val(''); 
	    $('#accountId').val('');
	    $('#transferAccountId').val('');
	    $('#cardNumber').val();
	    $('#cardAccountId').val('');
		if($('.modeOfPay').val()!=null && $('.modeOfPay').val()!=""){
			 var arrayVal = $(this).val().split('@@'); 
			 if(arrayVal[1]=="CHQ") {
				 $('#accountNumber').addClass('validate[required]'); 
				 $('#chequeDate').addClass('validate[required]'); 
				 $('#codeCombination').removeClass('validate[required]');  
				 $('#cardNumber').removeClass('validate[required]'); 
				 $('.cardNumberformError').remove();
				 $('.codeCombinationformError').remove();
				 $('.bank').show(); 
				 $('.cash').hide();
				 $('.cards').hide(); 
				 $('.interaccount').hide(); 
			 } 
			 else if(arrayVal[1]=="CSH" || arrayVal[1]=="WRT"){
				$('.cash').show(); 
				$('.bank').hide(); 
				$('.interaccount').hide(); 
				$('.cards').hide();
				$('#accountNumber').removeClass('validate[required]'); 
				$('#chequeDate').removeClass('validate[required]'); 
				$('#cardNumber').removeClass('validate[required]'); 
				$('#codeCombination').addClass('validate[required]'); 
				$('.accountNumberformError').remove();
				$('.chequeDateformError').remove();
				$('.cardNumberformError').remove(); 
				$('#cashCombinationId').val($('#generalClearingAccountId').val()); 
			    $('.cashCodeCombination').val($('#generalClearingAccount').val()); 
			}  else if(arrayVal[1]=="CCD" || arrayVal[1]=="DCD"){
				$('.bank').hide();
				$('.cash').hide(); 
				$('.interaccount').hide(); 
				$('.cards').show();
				$('.accountNumberformError').remove();
				$('.chequeDateformError').remove(); 
				$('.codeCombinationformError').remove();
				$('#accountNumber').removeClass('validate[required]'); 
				$('#chequeDate').removeClass('validate[required]');  
				$('#codeCombination').removeClass('validate[required]');  
				$('#cardNumber').addClass('validate[required]'); 
			} else if(arrayVal[1]=="IAT"){
				$('.bank').hide();
				$('.cash').hide(); 
				$('.cards').hide();
				$('.interaccount').show(); 
				$('.chequeDateformError').remove(); 
				$('.codeCombinationformError').remove();
				$('.cardNumberformError').remove();
				$('#accountNumber').addClass('validate[required]'); 
				$('#chequeDate').removeClass('validate[required]');  
				$('#codeCombination').removeClass('validate[required]');  
				$('#cardNumber').removeClass('validate[required]'); 
			}
		}else{
			$('.bank').hide();
			$('.cash').hide(); 
			$('.cards').hide();
			$('.interaccount').hide(); 
		} 
	});

	$('.delrow').live('click',function(){ 
		var tempid = getRowId($(this).attr('id')); 
		var recordId = Number($('#record_'+tempid).val());
		 slidetab="";
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
   	 	 $('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });   
   	 	calculateTotalPayment();  
   	 	if(recordId > 0){
 			$('.activeInvoice').each(function(){   
				var tempinvoiceid = getRowId($(this).attr('id')); 
				if(tempinvoiceid == recordId){
					$('#activeInvoice_'+tempinvoiceid).attr('checked', false);
				}
   	 		});
   	 	} 
	 });

	$('#pay_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_direct_payments_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#codecombination-popup').dialog('destroy');		
				$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	});
	$('#pay_save_print').click(function(){
		paymentSave(true);
	});
	$('#pay_save').click(function(){
		paymentSave(false);
	});

	getPaymentDetails = function(){
		var combinationarray=new Array();
		var amountarr=new Array();
		var descarr=new Array();
		var detailId=new Array();
		var invoiceNumbers=new Array();
		var paymentRequestLines = new Array();
		var recordIdArray=new Array();
		var useCaseArray=new Array();
		var paymentDetail="";
		$('.rowid').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			 var combination=$('#combinationId_'+rowId).val();   
			 var amount=$jquery('#amount_'+rowId).val();   
			 var lineDescription=$('#linedescription_'+rowId).val();
			 var invoiceNumber=$('#invoiceNumber_'+rowId).val();
			 var paymentRequestLineId = Number($('#paymentRequestLineId_'+rowId).val());
			 var recordId = Number($('#record_'+rowId).val());
			 var useCase = $('#useCase_'+rowId).val();
			 if(typeof invoiceNumber == 'undefined' || invoiceNumber==null || invoiceNumber=="")
				 invoiceNumber="##";
			 var paymentDetailId=Number($('#directPaymentLineId_'+rowId).val()); 
			 if(typeof amount != 'undefined' && amount!=null && 
					 amount!="" && combination!=null && combination!=null){
				 combinationarray.push(combination); 
				 amountarr.push(amount);
				 detailId.push(paymentDetailId);
				 if(lineDescription!=null && lineDescription!="")
					 descarr.push(lineDescription);
				 else
					 descarr.push("##");
				 
				 invoiceNumbers.push(invoiceNumber);
				 recordIdArray.push(recordId);
				 if(useCase!=null && useCase!="")
					 useCaseArray.push(useCase);
				 else
					 useCaseArray.push("##");
				 paymentRequestLines.push(paymentRequestLineId);
			 } 
		});
		for(var j=0;j<combinationarray.length;j++){ 
			paymentDetail += combinationarray[j]+"__"+amountarr[j]+"__"+descarr[j]+"__"+detailId[j]+"__"+invoiceNumbers[j]+"__"+paymentRequestLines[j]+"__"+useCaseArray[j]+"__"+recordIdArray[j];
			if(j==combinationarray.length-1){   
			} 
			else{
				paymentDetail+="@@";
			}
		}  
		 
		return paymentDetail;
	}; 

	$('.amount').live('change',function(){
		calculateTotalPayment();
		triggerAddRow(getRowId($(this).attr('id')));
		return false;
	});

	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/directpayment_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 
	
	$('.activeInvoice').live('change',function(){
		var disabledflag = false;
		$('.activeInvoice').each(function(){
			 if($(this).attr('checked') == false && disabledflag != true){
				 disabledflag = false; 
			 } 
			 else{
				 disabledflag = true;
			 } 
		});
		$('#paymentCurrency').attr('disabled', disabledflag);
		var invoiceId = getRowId($(this).attr('id'));
		
		if($(this).attr('checked') == true){
			var supplierId = Number($('#supplierId').val());
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_invoicedata_payment.action", 
			 	async: false,
			 	data:{invoiceId: invoiceId, supplierId: supplierId},
			    dataType: "json",
			    cache: false,
				success:function(response){ 
					 if(response != null && response.invoiceVO != null){
						 var rowId = Number(getRowId($(".tab>.rowid:last").attr('id'))); 
						 $('#amount_'+rowId).attr('readonly',true);
						 $('#codeID_'+rowId).hide();
						 $('#invoiceNumber_'+rowId).val(response.invoiceVO.invoiceNumber);
						 $('#amount_'+rowId).val(response.invoiceVO.invoiceAmount);
						 $('#linedescription_'+rowId).val(response.invoiceVO.description);
						 $('#combinationId_'+rowId).val(response.invoiceVO.combinationId);
						 $('#codeCombination_'+rowId).val(response.invoiceVO.combinationStr);
						 $('#useCase_'+rowId).val(response.invoiceVO.useCase);
						 $('#record_'+rowId).val(response.invoiceVO.invoiceId);
						 calculateTotalPayment();
						 triggerAddRow(rowId);
					 }
				}
			});
			return false;
		} else{
			var deleteRowId = Number(0);
			$('.rowid').each(function(){   
	  		 	var rowId=getRowId($(this).attr('id')); 
	  		 	var recordId = Number($('#record_'+rowId).val()); 
	  		 	if(recordId == invoiceId){
	  		 		deleteRowId = rowId; 
	  		 	}
			}); 
	     	$('#fieldrow_'+deleteRowId).remove();  
	     	var i=1;
	   	 	$('.rowid').each(function(){   
	   		 	var rowId=getRowId($(this).attr('id')); 
	   		 	$('#lineId_'+rowId).html(i);
				 i=i+1; 
			});   
	   	 	calculateTotalPayment();
	     	return false;
		} 
	}); 
});

function paymentSave(printFlag){
	paymentLineDetails="";
	 if($jquery("#directPaymentCreation").validationEngine('validate')){
		var directPaymentId = Number($('#directPaymentId').val());
		var paymentNumber = $('#directPaymentNumber').val();
		var directPaymentDate = $('#directPaymentDate').val();
		var paymentCurrency=$('#paymentCurrency').val();
		var supplierId = Number($('#supplierId').val());
		var description=$('#description').val();
		var paymentMode = $('#paymentMode').val();
		var payModeArray = paymentMode.split('@@');
		paymentMode = payModeArray[0];
		var accountId = Number($('#accountId').val());
		var chequeNumber = Number($('#chequeNumber').val());
		var chequeDate = $('#chequeDate').val(); 
		var cardAccountId = Number($('#cardAccountId').val());
		var chequeId = Number($('#chequeId').val());
		var cashCombinationId = Number($('#cashCombinationId').val());
		var paymentRequestId = Number($('#paymentRequestId').val());
		var vendorName = $('#supplierName').val();
		var payeeType = $('#payeeType').val();
		var payTypeArray = payeeType.split('@@');
		payeeType = payTypeArray[1];
		var transferAccountId = Number($('#transferAccountId').val());
		var alertId = Number($('#alertId').val());
		var recordId = Number($('#recordId').val());
		var useCase = $('#useCase').val();
		var messageId=Number($('#messageId').val());
		var processType=Number($('#processType').val());
		paymentLineDetails = getPaymentDetails();  
		if(paymentLineDetails!=null && paymentLineDetails!=""){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/savedirect_paymentinfo.action", 
			 	async: false, 
			 	data:{	directPaymentId: directPaymentId, paymentNumber: paymentNumber, currencyId: paymentCurrency, description: description,
				 		vendorName: vendorName, chequeBookId: chequeId, supplierId: supplierId, paymentDate: directPaymentDate, 
				 		recordId: recordId, useCase: useCase, paymentMode:paymentMode, bankAccountId: accountId, chequeDate: chequeDate,
				 		payeeType: payeeType, cashCombinationId: cashCombinationId, cardAccountId: cardAccountId,
				 		paymentRequestId: paymentRequestId, alertId: alertId, transferAccountId: transferAccountId, chequeNumber:chequeNumber,
				 		paymentLineDetails: paymentLineDetails,messageId:messageId,processType:processType
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){    
					 if(response.returnMessage == "SUCCESS"){ 
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_direct_payments_list.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove();  
				  					$('#codecombination-popup').dialog('destroy');		
				  					$('#codecombination-popup').remove(); 
									$("#main-wrapper").html(result); 
									if(directPaymentId == 0)
										$('#success_message').hide().html("Record created.").slideDown(1000);
									else
										$('#success_message').hide().html("Record updated.").slideDown(1000);
									$('#success_message').delay(2000).slideUp();
								}
						 });  
					 } 
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(3000).slideUp();
				}
			}); 
		}else{
			$('#page-error').hide().html("Please enter payment detail info.").slideDown(1000);
			 $('#page-error').delay(3000).slideUp();
			return false;
		}
	}else{
		return false;
	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var code=$('#codeCombination_'+rowId).val();
	 var amt=$('#amount_'+rowId).val();  
	 var nexttab=$('#fieldrow_'+rowId).next();   
	 if(code!=null && code!=""
			&& amt!=null && amt!="" && $(nexttab).hasClass('lastrow')){
		 $('#DeleteImage_'+rowId).show();
		 $('.addrows').trigger('click');
	 } 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function setCombination(combinationTreeId,combinationTree){ 
  if(actionname=="cash"){  
	  $('#cashCombinationId').val(combinationTreeId); 
      $('.cashCodeCombination').val(combinationTree); 
  }
  else if(actionname=="codeID"){
	  var tempvar =$(tempid).attr('id');	
      var idVal=tempvar.split("_");
      var rowid=Number(idVal[1]); 
	  $('#combinationId_'+rowid).val(combinationTreeId); 
      $('#codeCombination_'+rowid).val(combinationTree); 
      triggerAddRow(rowid);
	  return false;
  }
  actionname="";
}
function calculateTotalPayment(){
	var totalPay=0;
	$('.rowid').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var totalamount=Number($.trim($jquery('#amount_'+rowId).val()));
		totalPay=Number(totalPay+totalamount);
	}); 
	$jquery('.totalPaymentAmountH').val(totalPay);
	$('#totalPaymentAmount').text($('.totalPaymentAmountH').val());
}
function personPopupResult(personid, personname, commonParam){
	$('#supplierId').val(personid);
	$('#supplierName').val(personname);
	$('#common-popup').dialog("close");   
}
function showPaymentRequestDetail(paymentRequestId) { 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_payment_request_detail.action",
					async : false,
					data : {
						paymentRequestId : paymentRequestId
					},
					dataType : "html",
					cache : false,
					success : function(result) {
						$(".tab").html(result);
						return false;
					}
				});
	return false;
	}
	function commonSupplierPopup(supplierId, supplierName, combinationId,
			accountCode, param) {
		var currencyId = Number($('#paymentCurrency').val());
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_supplier_activeinvoices.action",
						async : false,
						data : {
							supplierId : supplierId, currencyId: currencyId
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$(".supplierinvoice").html(result);
							return false;
						}
					});
		return false;
	}
	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#supplierId').val(customerId);
		$('#supplierName').val(customerName);
	}
	function paymentRequestPopup(paymentRequestId, paymentRequestNo, date){
		$('#paymentRequestId').val(paymentRequestId);
		$('#paymentRequestNo').val(paymentRequestNo);
		showPaymentRequestDetail(paymentRequestId); 
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Direct
			Payments
		</div> 
		<div>
			<form id="directPaymentCreation" style="position: relative;">
				<div class="portlet-content">
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
 						<div class="tempresult" style="display: none;"></div>
						<div id="page-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div class="width48 float-left" id="hrm">
							<fieldset style="min-height: 150px;">
								<input type="hidden" id="directPaymentId"
									value="${DIRECT_PAYMENT.directPaymentId}"> <input
									type="hidden" id="recordId"
									value="${DIRECT_PAYMENT.recordId}"> <input
									type="hidden" name="useCase" id="useCase"
									value="${DIRECT_PAYMENT.useCase}" /> <input
									type="hidden" name="alertId" id="alertId" value="${alertId}" />
									<input
									type="hidden" name="messageId" id="messageId" value="${messageId}" />
									<input
									type="hidden" name="processType" id="processType" value="${processType}" />
								<div>
									<label class="width30">Payment Voucher<span
										class="mandatory">*</span>
									</label>
									<c:choose>
										<c:when test="${paymentNumber ne null && paymentNumber ne ''}">
											<input type="text" name="paymentNumber"
												value="${paymentNumber}" id="directPaymentNumber"
												readonly="readonly" class="width50 validate[required]"
												TABINDEX=1>
										</c:when>
										<c:otherwise>
											<input type="text" name="directPaymentNumber"
												value="${DIRECT_PAYMENT.paymentNumber}"
												id="directPaymentNumber" readonly="readonly"
												class="width50 validate[required]" TABINDEX=1>
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30">Payment Date<span
										class="mandatory">*</span>
									</label>
									<c:choose>
										<c:when
											test="${DIRECT_PAYMENT.paymentDate ne null && DIRECT_PAYMENT.paymentDate ne ''}">
											<c:set var="payDate" value="${DIRECT_PAYMENT.paymentDate}" />
											<%
												String date = DateFormat.convertDateToString(pageContext
																.getAttribute("payDate").toString());
											%>
											<input type="text" name="directPaymentDate" value="<%=date%>"
												id="directPaymentDate" readonly="readonly"
												class="width50 validate[required]" TABINDEX=2>
										</c:when>
										<c:otherwise>
											<input type="text" name="directPaymentDate"
												id="directPaymentDate" readonly="readonly"
												class="width50 validate[required]" TABINDEX=2>
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30">Payment Mode<span
										class="mandatory">*</span>
									</label> <select name="paymentMode" class="width51 modeOfPay validate[required]"
										id="paymentMode">
										<option value="">Select</option>
										<c:choose>
											<c:when test="${PAYMENT_MODE ne null && PAYMENT_MODE ne ''}">
												<c:forEach items="${PAYMENT_MODE}" var="MODE">
													<option value="${MODE.key}">${MODE.value}</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
									<input type="hidden" id="paymentModeTemp" value="${DIRECT_PAYMENT.paymentMode}">
								</div>
								<div class="cash" style="display: none;">
									<label class="width30">Cash Account<span
										class="mandatory">*</span>
									</label> <input type="hidden" name="combinationId"
										id="cashCombinationId"
										value="${DIRECT_PAYMENT.combination.combinationId}" />
										<c:choose>
											<c:when
											test="${CASH_ACCOUNTS.combinationId ne null && CASH_ACCOUNTS.combinationId ne ''}">
												<input type="hidden" name="generalClearingAccountId"
										id="generalClearingAccountId"
										value="${CASH_ACCOUNTS.combinationId}" />
										<input type="hidden" name="generalClearingAccount"
										id="generalClearingAccount"
											value="${CASH_ACCOUNTS.accountByNaturalAccountId.code}[${CASH_ACCOUNTS.accountByNaturalAccountId.account}]" />
											</c:when> 
											<c:otherwise>
											<input type="hidden" name="generalClearingAccountId"
												id="generalClearingAccountId"/>
											<input type="hidden" name="generalClearingAccount"
												id="generalClearingAccount"/></c:otherwise>
											</c:choose>
									<c:choose>
										<c:when
											test="${DIRECT_PAYMENT.combination.combinationId ne null && DIRECT_PAYMENT.combination.combinationId ne ''}">
											<input type="text" name="codeCombination" readonly="readonly"
												id="codeCombination"
												value="${DIRECT_PAYMENT.combination.accountByNaturalAccountId.code}[${DIRECT_PAYMENT.combination.accountByNaturalAccountId.account}]"
												class="cashCodeCombination width50">
										</c:when>
										<c:otherwise>
											<input type="text" name="codeCombination" readonly="readonly"
												id="codeCombination" class="cashCodeCombination width50">
										</c:otherwise>
									</c:choose>
									<span class="button width10" id="cash_combi">
										<a style="cursor: pointer;" id="cashcombination"
										class="btn ui-state-default ui-corner-all codecombination-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div class="bank" style="display: none;">
									<label class="width30">Account No<span
										class="mandatory">*</span>
									</label> <input type="text" id="accountNumber"
										class="width50 accountNumber" readonly="readonly"
										name="accountNumber"
										value="${DIRECT_PAYMENT.bankAccount.bank.bankName} [${DIRECT_PAYMENT.bankAccount.accountNumber}]" />
									<span class="button width10">
										<a style="cursor: pointer;" id="bankaccountid"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" readonly="readonly" name="accountId"
										id="accountId"
										value="${DIRECT_PAYMENT.bankAccount.bankAccountId}" />
								</div>
								<div class="bank" style="display: none;">
									<label class="width30">Cheque No</label> <input type="text"
										id="chequeNumber" class="width50 chequeNumber" name="chequeNumber"
										value="${DIRECT_PAYMENT.chequeNumber}" /> <input type="hidden"
										id="chequeId"
										value="${DIRECT_PAYMENT.chequeBook.chequeBookId}" />
								</div>
								<div class="bank" style="display: none;">
									<label class="width30">Cheque Date<span
										class="mandatory">*</span>
									</label>
									<c:choose>
										<c:when
											test="${DIRECT_PAYMENT.chequeDate ne null && DIRECT_PAYMENT.chequeDate ne ''}">
											<c:set var="cheqDate" value="${DIRECT_PAYMENT.chequeDate}" />
											<%
												String cheqDate = DateFormat
																.convertDateToString(pageContext.getAttribute(
																		"cheqDate").toString());
											%>
											<input type="text" name="chequeDate" value="<%=cheqDate%>"
												id="chequeDate" readonly="readonly" class="width50">
										</c:when>
										<c:otherwise>
											<input type="text" name="chequeDate" id="chequeDate"
												readonly="readonly" class="width50">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="cards" style="display: none;">
									<label class="width30">Card No<span class="mandatory">*</span>
									</label> <input type="text" id="cardNumber" class="width50 cardNumber"
										name="cardNumber"
										value="${DIRECT_PAYMENT.bankAccount.cardNumber}" /> <span
										class="button width10">
										<a style="cursor: pointer;" id="cardaccountid"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" readonly="readonly" name="cardAccountId"
										id="cardAccountId"
										value="${DIRECT_PAYMENT.bankAccount.bankAccountId}" />
								</div>
								<div class="interaccount" style="display: none;">
									<label class="width30">Transfer From<span
										class="mandatory">*</span>
									</label> <input type="text" id="transferAccount"
										class="width50 cardNumber" name="cardNumber"
										value="${DIRECT_PAYMENT.bankAccount.accountNumber}" /> <span
										class="button width10">
										<a style="cursor: pointer;" id="transferAccountid"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" readonly="readonly" name="transferAccountId"
										id="transferAccountId"
										value="${DIRECT_PAYMENT.bankAccount.bankAccountId}" />
								</div>
								<div class="paymentReqDiv">
									<label class="width30">Payment Request</label> <input
										type="text" id="paymentRequestNo" class="width50"
										name="paymentRequestNo"
										value="${DIRECT_PAYMENT.paymentRequest.paymentReqNo}" /> <input
										type="hidden" id="paymentRequestId" name="paymentRequestId"
										value="${DIRECT_PAYMENT.paymentRequest.paymentRequestId}" /> <span
										class="button width10">
										<a style="cursor: pointer;" id="paymentrequest"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-right" id="hrm">
							<fieldset style="min-height: 150px;">
								<div>
									<label class="width30 tooltip">Currency<span
										class="mandatory">*</span>
									</label> <select name="paymentCurrency" id="paymentCurrency"
										class="width51 validate[required]">
										<option value="">Select</option>
										<c:choose>
											<c:when
												test="${CURRENCY_INFO ne null and CURRENCY_INFO ne ''}">
												<c:forEach items="${CURRENCY_INFO}" var="CURRENCY">
													<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</select> <input type="hidden" id="default_currency"
										value="${DEFAULT_CURRENCY}" />
								</div>
								<div>
									<label class="width30">Payee Type<span
										class="mandatory">*</span>
									</label> <select name="payeeType" id="payeeType"
										class="width51 validate[required]">
										<option value="">Select</option>
										<c:forEach items="${PAYEE_TYPES}" var="PAYEE">
											<option value="${PAYEE.key}">${PAYEE.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label class="width30">Payable<span class="mandatory">*</span>
									</label>
									<c:choose>
										<c:when
											test="${DIRECT_PAYMENT.supplier.person ne null && DIRECT_PAYMENT.supplier.person ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.supplier.person.firstName} ${ DIRECT_PAYMENT.supplier.person.lastName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.supplier.company ne null && DIRECT_PAYMENT.supplier.company ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.supplier.company.companyName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.supplier.cmpDeptLocation ne null && DIRECT_PAYMENT.supplier.cmpDeptLocation ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.supplier.cmpDeptLocation.company.companyName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.customer.personByPersonId ne null && DIRECT_PAYMENT.customer.personByPersonId ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.customer.personByPersonId.firstName} ${DIRECT_PAYMENT.customer.personByPersonId.lastName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.customer.company ne null && DIRECT_PAYMENT.customer.company ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.customer.company.companyName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.customer.cmpDeptLocation ne null && DIRECT_PAYMENT.customer.cmpDeptLocation ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.customer.cmpDeptLocation.company.companyName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.personByPersonId ne null && DIRECT_PAYMENT.personByPersonId ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.personByPersonId.firstName} ${DIRECT_PAYMENT.personByPersonId.lastName}"
												id="supplierName" class="width50 validate[required]"
												TABINDEX=3 readonly="readonly" />
										</c:when>
										<c:when
											test="${DIRECT_PAYMENT.others ne null && DIRECT_PAYMENT.others ne '' }">
											<input type="text" name="supplierName"
												value="${DIRECT_PAYMENT.others}" id="supplierName"
												class="width50 validate[required]" TABINDEX=3
												readonly="readonly" />
										</c:when>
										<c:otherwise>
											<input type="text" name="supplierName" id="supplierName"
												class="width50 validate[required]" TABINDEX=3
												readonly="readonly" />
										</c:otherwise>
									</c:choose>
									<span class="button width10 supplier-popup-span">
										<a style="cursor: pointer;" id="supplier"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" readonly="readonly" name="supplierId"
										id="supplierId" />
								</div>
								<div>
									<label class="width30 tooltip">Description</label>
									<textarea rows="2" id="description" class="width51">${DIRECT_PAYMENT.description}</textarea>
								</div>
							</fieldset>
						</div>
						<div class="width100 float-right payment-request" id="hrm"
							style="display: none;"></div>
					
					<div class="width100 float-left supplierinvoice" id="hrm"></div>	
 					<div id="hrm" class="hastable width100 float-left"
						style="margin-top: 10px;">
						<fieldset>
							<legend>Payment Information<span
									class="mandatory">*</span></legend>
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 1%">L.No</th>
										<th style="width: 2%">Invoice No.</th>
										<th style="width: 10%">Account Code</th>
										<th style="width: 2%">Amount</th>
										<th style="width: 5%">Description</th>
										<th style="width: 0.01%;"><fmt:message
												key="accounts.common.label.options" />
										</th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:choose>
										<c:when
											test="${DIRECT_PAYMENT_DETAIL ne null && DIRECT_PAYMENT_DETAIL ne ''}">
											<c:forEach var="PAYMENT_DETAIL"
												items="${DIRECT_PAYMENT_DETAIL}" varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}">${status.index+1}</td>
													<td>
														<c:choose>
															<c:when test="${viewFromDirectPayment eq true}">
															<input type="text" class="width98 invoiceNumber"
															id="invoiceNumber_${status.index+1}"
															value="${PAYMENT_DETAIL.invoiceNumber}" />
														</c:when>
														<c:otherwise>
															<span>${PAYMENT_DETAIL.invoiceNumber}</span>
															<input type="hidden" class="width98 invoiceNumber"
															id="invoiceNumber_${status.index+1}"
															value="${PAYMENT_DETAIL.invoiceNumber}" />
														</c:otherwise>
														</c:choose>
													</td>
													<td class="codecombination_info"
														id="codecombination_${status.index+1}"
														style="cursor: pointer;"><input type="hidden"
														name="combinationId_${status.index+1}"
														value="${PAYMENT_DETAIL.combination.combinationId}"
														id="combinationId_${status.index+1}" /> 
														<c:choose>
															<c:when test="${viewFromDirectPayment eq true}">
																<c:choose>
																	<c:when test="${PAYMENT_DETAIL.combination.accountByAnalysisAccountId.account ne null}">
																		<input type="text"
																			name="codeCombination_${status.index+1}"
																			readonly="readonly" id="codeCombination_${status.index+1}"
																			value="${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]-[${PAYMENT_DETAIL.combination.accountByAnalysisAccountId.account}]"
																			class="codeComb width80">
																	</c:when>
																	<c:otherwise>
																		<input type="text"
																			name="codeCombination_${status.index+1}"
																			readonly="readonly" id="codeCombination_${status.index+1}"
																			value="${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]"
																			class="codeComb width80">
																	</c:otherwise>
																</c:choose>
																 <span class="button"
																id="codeID_${status.index+1}"> <a
																	style="cursor: pointer;"
																	class="btn ui-state-default ui-corner-all codecombination-popup spl_codecombination-popup width100">
																		<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${PAYMENT_DETAIL.combination.accountByAnalysisAccountId.account ne null}">
																		<span>${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]-[${PAYMENT_DETAIL.combination.accountByAnalysisAccountId.account}]</span>
																		<input type="hidden"
																			name="codeCombination_${status.index+1}"
																			readonly="readonly" id="codeCombination_${status.index+1}"
																			value="${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]-[${PAYMENT_DETAIL.combination.accountByAnalysisAccountId.account}]"
																			class="codeComb width80">
																	</c:when>
																	<c:otherwise>
																		<span>${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]</span>
																		<input type="hidden"
																			name="codeCombination_${status.index+1}"
																			readonly="readonly" id="codeCombination_${status.index+1}"
																			value="${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]"
																			class="codeComb width80">
																	</c:otherwise>
																</c:choose>
															</c:otherwise>
														</c:choose>	 
														</td>
													<td>
													<c:choose>
														<c:when test="${viewFromDirectPayment eq true}">
														<input type="text" 
															class="width98 validate[optional,custom[number]] amount"
															id="amount_${status.index+1}"
															value="${PAYMENT_DETAIL.amount}"
															style="text-align: right;" />
														</c:when>
														<c:otherwise>
															<span class="float-right">${PAYMENT_DETAIL.amount}</span>
														<input type="hidden" disabled="disabled"
															class="width98 validate[optional,custom[number]] amount"
															id="amount_${status.index+1}"
															value="${PAYMENT_DETAIL.amount}"
															style="text-align: right;" />
														</c:otherwise>
													</c:choose>
													</td>
													<td><input type="text" name="linedescription"
														id="linedescription_${status.index+1}"
														value="${PAYMENT_DETAIL.description}" /></td>
													<td style="display: none;"><input type="hidden"
														id="directPaymentLineId_${status.index+1}"
														value="${PAYMENT_DETAIL.directPaymentDetailId}" /> <input
														type="hidden" id="paymentRequestLineId_${status.index+1}"
														value="${PAYMENT_DETAIL.paymentRequestDetail.paymentRequestDetailId}" />
														<input value="${PAYMENT_DETAIL.useCase}"
														type="hidden" id="useCase_${status.index+1}" />
														<input  value="${PAYMENT_DETAIL.recordId}"
														type="hidden" id="record_${status.index+1}" />
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status.index+1}">
														<c:if test="${viewFromDirectPayment eq true}">
															<a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
															id="AddImage_${status.index+1}"
															style="display: none; cursor: pointer;" title="Add Record">
																<span class="ui-icon ui-icon-plus"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
															id="EditImage_${status.index+1}"
															style="display: none; cursor: pointer;"
															title="Edit Record"> <span
																class="ui-icon ui-icon-wrench"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
															id="DeleteImage_${status.index+1}"
															style="cursor: pointer;" title="Delete Record"> <span
																class="ui-icon ui-icon-circle-close"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
															id="WorkingImage_${status.index+1}" style="display: none;"
															title="Working"> <span class="processing"></span> </a>
														</c:if>
														</td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose> 
									<c:choose>
										<c:when test="${requestScope.messageId eq null || requestScope.messageId eq 0 &&
												requestScope.alertId eq null || requestScope.alertId eq 0}">
											<c:forEach var="i"
												begin="${fn:length(DIRECT_PAYMENT_DETAIL)+0}"
												end="${fn:length(DIRECT_PAYMENT_DETAIL)+1}" step="1"
												varStatus="status1">
												<tr class="rowid" id="fieldrow_${status1.index+1}">
													<td id="lineId_${status1.index+1}">${status1.index+1}</td>
													<td><input type="text" class="width98 invoiceNumber"
														id="invoiceNumber_${status1.index+1}" value="" /></td>
													<td class="codecombination_info"
														id="codecombination_${status1.index+1}"
														style="cursor: pointer;"><input type="hidden"
														name="combinationId_${status1.index+1}"
														id="combinationId_${status1.index+1}" /> <input
														type="text" name="codeCombination_${status1.index+1}"
														readonly="readonly"
														id="codeCombination_${status1.index+1}"
														class="codeComb width80"> 
															<span class="button"
															id="codeID_${status1.index+1}"> <a
																style="cursor: pointer;"
																class="btn ui-state-default ui-corner-all codecombination-popup width100">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
														</td>
													<td><input type="text"
														class="width98 amount validate[optional,custom[number]]"
														id="amount_${status1.index+1}" style="text-align: right;" />
													</td>
													<td><input type="text" name="linedescription"
														id="linedescription_${status1.index+1}" /></td>
													<td style="display: none;"><input type="hidden"
														id="directPaymentLineId_${status1.index+1}" /> <input
														type="hidden" id="paymentRequestLineId_${status1.index+1}" />
														<input
														type="hidden" id="useCase_${status1.index+1}" />
														<input
														type="hidden" id="record_${status1.index+1}" />
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status1.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status1.index+1}"
														style="display: none; cursor: pointer;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status1.index+1}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status1.index+1}"
														style="display: none; cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status1.index+1}"
														style="display: none;" title="Working"> <span
															class="processing"></span> </a></td>
												</tr>
											</c:forEach>
										</c:when> 
										<c:otherwise>
											<tr class="rowid">
												<c:forEach var="i"
													begin="0" end="5" step="1">
													<td></td>
												</c:forEach>
											</tr>
										</c:otherwise>
									</c:choose> 
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>
			</form>
		</div>
		<div class="clearfix"></div>
		<div class="width40 float-right">
			<span style="font-weight: bold">Total :</span> <span
				style="font-weight: bold" id="totalPaymentAmount"></span>
			<input type="text" class="totalPaymentAmountH" style="display: none;" readonly="readonly"/>	
		</div>
		<div class="clearfix"></div>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div class="portlet-header ui-widget-header float-right"
				id="pay_discard" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div> 
			<div class="portlet-header ui-widget-header float-right"
				id="pay_save" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
		</div>
		<div style="display: none;"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="portlet-header ui-widget-header float-left addrows"
				style="cursor: pointer;">
				<fmt:message key="accounts.common.button.addrow" />
			</div>
		</div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span>
				</a>
			</div>
			<div id="common-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span>
				</a>
			</div>
			<div id="codecombination-popup"
				class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="codecombination-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>