<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript">
var currentId=""; var tempvar=""; var idarray=""; var rowid="";
$(function(){ 
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',0);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	$("#tree_bank").treeview({
		collapsed: true,
		animated: "medium",
		control:"#sidetreecontrol",
		persist: "location"
	}); 
	$('.accountid').live('click',function(){ 
		$('.accountid').each(function(){  
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			 if($('#bankaccountselect_'+rowid).hasClass('selected')){ 
				 $('#bankaccountselect_'+rowid).removeClass('selected');
			 }
			 currentId=0;
		});
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#bankaccountselect_'+rowid).addClass('selected');
	});
	
	$('.accountid').live('dblclick',function(){  
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		var lineno=$('.accountline_no').attr('id'); 
		$('#accountNumber_'+lineno).val($('#bankname_'+rowid).html()+"( "+ $('#accountnumber_'+rowid).html()+")");
		$('#accountId_'+lineno).val($('#accountid_'+rowid).html());   
		$('#common-popup').dialog('close');  
	});
	
	$('.close').click(function(){
		 $('#common-popup').dialog('close'); 
	});
});
</script> 
<style>
.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>
<div class="portlet-content">  
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Bank Account Details
</div>
<input type="hidden" id="${ROW_ID}" class="accountline_no"/>
<ul id="tree_bank">
	<li><strong>Banks</strong></li> 
	<c:choose>
		<c:when test="${requestScope.BANK_DETAILS ne null && requestScope.BANK_DETAILS ne''}">
			<c:forEach items="${BANK_DETAILS}" var="bank" varStatus="status1"> 
				<li id="bank_${status1.index+1}">
					<span style="display: none;" id="bankid_${status1.index+1}">${bank.bankId}</span>
					<span id="bankname_${status1.index+1}" class="bankid">${bank.bankName} </span>
					<ul> 
						<c:forEach items="${BANK_ACCOUNT_DETAILS}" var="account" varStatus="status"> 
							<c:choose>
								<c:when test="${bank.bankId eq account.bank.bankId}"> 
									<span style="display: none;" id="accountid_${status.index+1}">${account.bankAccountId}</span>
									<li id="accountnumber_${status.index+1}" class="accountid width30" style="cursor: pointer;">${account.accountNumber}</li> 
									<span id="bankaccountselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; position: relative ;right:600px; top:-24px; "></span>
								</c:when>
							</c:choose> 
						</c:forEach> 
					</ul>
				</li>	
			</c:forEach> 
		</c:when>
	</c:choose>		
</ul>  
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
	<div class="portlet-header ui-widget-header float-right close" style="cursor:pointer;">close</div>  
</div>  
</div>  
	