<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:forEach var="detail" items="${REQUISITION_DETAIL}"
	varStatus="status">
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
		<td id="itemtype_${status.index+1}">
			<span>${detail.itemType}</span>
		</td>
		<td><span class="float-left width60"
			id="product_${status.index+1}">
				${detail.product.code} -
				${detail.product.productName}</span>
				<span class="width10 float-right" id="unitCode_${status.index+1}"
						style="position: relative;">${detail.product.lookupDetailByProductUnit.accessCode}</span>
			<input
			type="hidden" id="productid_${status.index+1}"
			value="${detail.product.productId}" /></td>
		<td><span class="float-left width60"
			id="storeName_${status.index+1}">${detail.storeName}</span> <input
			type="hidden" name="storeId" id="storeId_${status.index+1}" value="" />
			<input type="hidden" name="shelfId" id="shelfId_${status.index+1}" value="${detail.shelfId}"/> <span class="button float-right">
				<a style="cursor: pointer;" id="prodIDStr_${status.index+1}"
				class="btn ui-state-default ui-corner-all purchase-store-popup width100">
					<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
		<td id="uom_${status.index+1}" class="uom" style="display: none;"></td>
		<td>
			<select name="packageType" id="packageType_${status.index+1}" class="packageType">
				<option value="-1">Select</option>
				<c:forEach var="packageType" items="${detail.productPackageVOs}">
					<c:choose>
						<c:when test="${packageType.productPackageId gt 0}">
							<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
							 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
							 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
							 	</c:forEach> 
							 </optgroup>
						</c:when> 
					</c:choose> 
				</c:forEach>
			</select>
			<input type="hidden" id="temppackageType_${status.index+1}" value="-1"/>
		</td>
		<td><input type="text"
			class="width96 packageUnit validate[optional,custom[number]]"
			id="packageUnit_${status.index+1}" value="${detail.packageUnit}" /> 
		</td> 
		<td><input type="hidden"
			class="width96 quantity validate[optional,custom[number]]"
			id="quantity_${status.index+1}" value="${detail.quantity}" />
			<span id="baseDisplayQty_${status.index+1}">${detail.quantity}</span>
		</td>
		<td><input type="text"
			class="width96 unitrate validate[optional,custom[number]]"
			id="unitrate_${status.index+1}" value="${detail.unitRate}"
			style="text-align: right;" />
			<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" id="totalAmountH_${status.index+1}" 
			class="totalAmountH" value="${detail.totalAmount}"/> 
			<input type="hidden" id="basePurchasePrice_${status.index+1}" value="${detail.unitRate}"/>
		</td> 
		<td class="totalamount" id="totalamount_${status.index+1}"
			style="text-align: right;"> 
		</td>
		<td><input type="text" name="linedescription"
			id="linedescription_${status.index+1}" value="${detail.description}" />
		</td>
		<td>
			<div id="moreoption_${status.index+1}" style="cursor: pointer; text-align: center; background-color: #3ADF00;" 
				class="portlet-header ui-widget-header float-left moreoption">
				MORE OPTION
			</div>
		</td>
		<td style="display: none;"><input type="hidden"
			id="purchaseLineId_${status.index+1}" /> <input type="hidden"
			id="requisitionLineId_${status.index+1}"
			value="${detail.requisitionDetailId}" />
			<input type="text"
				class="width96 batchNumber" id="batchNumber_${status.index+1}" /> <input
				type="text" readonly="readonly" class="width96 expiryBatchDate"
				id="expiryBatchDate_${status.index+1}"/>
		</td>
		<td style="width: 0.01%;" class="opn_td" id="option_${status.index+1}"><a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
			id="AddImage_${status.index+1}"
			style="display: none; cursor: pointer;" title="Add Record"> <span
				class="ui-icon ui-icon-plus"></span> </a> <a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
			id="EditImage_${status.index+1}"
			style="display: none; cursor: pointer;" title="Edit Record"> <span
				class="ui-icon ui-icon-wrench"></span> </a> <a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
			id="DeleteImage_${status.index+1}" style="cursor: pointer;"
			title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
		</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
			id="WorkingImage_${status.index+1}" style="display: none;"
			title="Working"> <span class="processing"></span> </a>
		</td>
	</tr>
</c:forEach>
<script type="text/javascript">
$(function(){
	$jquery(".unitrate,.totalAmountH").number(true,3); 
	var totalPurchase = 0;
	$('.rowid').each(function(){
		var rowId = getRowId($(this).attr('id'));
		$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
		$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
		var totalamount=Number($jquery('#totalAmountH_'+rowId).val());
 		totalPurchase = Number(totalPurchase+totalamount);
	});
	$jquery('.totalPurchaseAmountH').val(totalPurchase);
	$('#totalPurchaseAmount').text($('.totalPurchaseAmountH').val());
});
</script>