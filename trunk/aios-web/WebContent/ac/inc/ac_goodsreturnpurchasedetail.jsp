<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>  
<script type="text/javascript"> 
var purchaseReturns="";
$(function(){ 
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open');  
	 
	$(".low input[type='button']").click(function(){
		var arr = $(this).attr("name").split("2");
		var from = arr[0];
		var to = arr[1];  
		$("#" + from + " option:selected").each(function(){   
	   		if(from=="left"){
	   			var thisval=$(this).val();
		   		var splitval=thisval.split("&&");
		   		var purchaseQty=splitval[2];
		   		$('#temppurchase').val(purchaseQty);
		   		$('#tempreturns').val("");
		   		$('.ui-dialog-titlebar').remove(); 
		   		$('#temp-error').hide();
		   		$('.otherx-popup').dialog('open'); 
		   	}  
	   		else if(from=="right"){
	   			var thisval=$(this).val();
	   			var splitval=thisval.split("&&");
		   		var purchaseQty=splitval[2];
		   		var index=$(this).clone().val().lastIndexOf("&&");  
		   		var splittext=$(this).clone().text(); 
	 			splittext=splittext.split("("); 
	   			$("#" + to).append("<option value="+$(this).clone().val().substring(0,index)+">"+
	   					splittext[0]+"("+purchaseQty+")</option>");
	   			$(this).remove();
	   			getCounts();  
		   	} 
		});  
	});

	   
	 
	// management Dialog			
	 	$('.otherx-popup').dialog({
	 		autoOpen: false,
	 		width: 500, 
	 		bgiframe: true,
	 		modal: false,
	 		buttons: {
	 			"Ok": function(){    
	 				if($('#tempreturns').val()=="" || $('#tempreturns').val()==null || $('#tempreturns').val()<=0) {
		 				$('#temp-error').hide().html("Return qty should not be empty").slideDown();
			 		}
		 			else if(Number($('#tempreturns').val())>Number($('#temppurchase').val())){  
		 				$('#temp-error').hide().html("Return qty should not be greater than purchase qty.").slideDown();
			 		}else{
			 			$("#left option:selected").each(function(){  
				 			var slitvalue=$(this).clone().val(); 
				 			slitvalue=slitvalue.split("&&"); 
				 			var splittext=$(this).clone().text(); 
				 			splittext=splittext.split("(");
				 			$("#right").append("<option value="+slitvalue[0]+"&&"+slitvalue[1]+"&&"+slitvalue[2]+"&&"+$('#tempreturns').val()+">"+
				 					splittext[0]+"("+$('#tempreturns').val()+")</option>");
			 			});
			 			$('#left option:selected').remove();
			 			getCounts();  
			 			$(this).dialog("close"); 
				 	} 
	 			}
	 		}
	 	});
	 	$('.noreturn_close-pops').click(function(){
			 $('#common-popup').dialog('close'); 
			 var purchaseId=Number($('#purchaseId').val()); 
			 if(purchaseId>0){
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_purchase_detail.action", 
				 	async: false, 
				 	data:{purchaseId:purchaseId},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $('.otherx-popup').dialog('destroy');		
						 $('.otherx-popup').remove();  
				 		$(".tab").html(result); 
					}
				});
			} 
		});  
	 	$('.return_close-pops').click(function(){
			 $('#common-popup').dialog('close'); 
			 var purchaseId=Number($('#purchaseId').val()); 
			 purchaseReturns=getPurchaseReturns();
			 if(purchaseReturns!=null && purchaseReturns!=""){ 
				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_purchase_return_detail.action", 
				 	async: false, 
				 	data:{purchaseId:purchaseId, purchaseReturns:purchaseReturns},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $('.otherx-popup').dialog('destroy');		
						 $('.otherx-popup').remove();  
				 		$(".tab").html(result); 
					}
				});
			 }
	 	});

	 	var getPurchaseReturns =function(){ 
	 		purchaseReturns="";
	 		var optionsize=$('#right option').length;
	 		var i=1;
	 		$('#right option').each(function(){  
	 			purchaseReturns+=$(this).val();
		 		if(optionsize!=i){
		 			purchaseReturns+="@@";
			 	} 
			 	i++;
		 	});  
	 		return purchaseReturns;
		};
});
function getCounts(){
	$('#totalleft').html($('#left option').length);  
	$('#totalright').html($('#right option').length); 
}
</script>
<style>
.container {float: left;padding: 3px;}
.container select{width: 20em;}
input[type="button"]{width: 8em;}
.low{position: relative; top: 35px;} 
#purchase_detail{height:250px!important;}
</style> 
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:250px;">
	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Goods Return Entry</div>	
	<div class="width40 float-left">  
		<div>Total product(s):<span id="totalleft" style="font-weight: bold;margin-left:3px;">${fn:length(PURCHASE_DETAIL_INFO)}</span></div>
		  <select name="itemsToChoose" class="width98" id="left" size="8"> 
	 		<c:forEach var="bean" items="${PURCHASE_DETAIL_INFO}">
	         	<option value="${bean.purchaseDetailId}&&${bean.product.productId}&&${bean.quantity}">${bean.product.productName}(${bean.quantity})</option>
	         </c:forEach>
	      </select> 
	</div> 
	<div class="low container">
	    <input name="left2right" id="left2right" class="left2right" value="add" type="button"><br>
	    <input name="right2left" id="right2left" class="right2left" value="remove" type="button">
	</div>
	
	<div class="width40 float-left">
		<div>Total Return(s):<span id="totalright" style="font-weight: bold;margin-left:3px;"></span></div>
	  <select name="itemsToAdd" id="right" size="8" class="width98" multiple="multiple">
	  </select>
	</div>
	<div class="otherx-popup" id="hrm" class="width100 float-left">
	<div id="temp-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
		<div>
			<label for="purchaseQty" class="width15">Purchase Qty</label>
			<input type="text" name="temppurchase" readonly="readonly" id="temppurchase" class="width50"/>
		</div> 
		<div style="margin-top: 3px;">
			<label for="returnQty" class="width15" title="Please enter no.of qty to return">Return Qty</label>
			<input type="text" name="tempreturns"  id="tempreturns" class="width50" title="Please enter no.of qty to return"/>
		</div> 
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;">  
		<div class="portlet-header ui-widget-header float-right return_close-pops" style="cursor:pointer;">Close</div>   
	 </div>
</div>