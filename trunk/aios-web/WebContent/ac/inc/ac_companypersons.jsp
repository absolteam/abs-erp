<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			account holder details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="componentstypes" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${COMPANY_PERSON}" var="bean" varStatus="status">
	     		<option value="${bean.companyPersonId}">${bean.companyName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 tenant_list" style="padding:2px;">  
	     	<ul> 
	     		<li class="width40 float-left"><span>Name</span></li>  
	     		<li class="width20 float-left"><span>Type</span></li>
	     		<li class="width20 float-left"><span>Number</span></li>   
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${COMPANY_PERSON}" var="bean" varStatus="status">
		     		<li id="tenant_${status.index+1}" class="tenant_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.companyPersonId}" id="tenantbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.companyName}" id="tenatnamebynameid_${status.index+1}">  
		     			<span style="cursor: pointer; visibility: visible;" class="float-left width40">${bean.companyName}</span> 
		     			<span id="tenantNamebyname_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.companyTypeName} </span>  
		     			<span style="cursor: pointer;" class="float-left width20">${bean.tradeNo}
		     				<span id="tenantNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){ 
	$($($('#holder-popup').parent()).get(0)).css('width',500);
	$($($('#holder-popup').parent()).get(0)).css('height',250);
	$($($('#holder-popup').parent()).get(0)).css('padding',0);
	$($($('#holder-popup').parent()).get(0)).css('left',0);
	$($($('#holder-popup').parent()).get(0)).css('top',100);
	$($($('#holder-popup').parent()).get(0)).css('overflow','hidden');
	$('#holder-popup').dialog('open'); 
	$('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   $('#accountHolderId').val($(this).val());  
		       $('#holderName').val($(".autoComplete option[value="+$(this).val()+"]").text());
		       $('#holder-popup').dialog("close"); 
	   } 
	}); 
	$('.tenant_list_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.tenant_list_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#tenantNameselect_'+rowid).hasClass('selected')){ 
				 $('#tenantNameselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#tenantNameselect_'+rowid).addClass('selected');
	});
	
	$('.tenant_list_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#accountHolderId').val($('#tenantbyid_'+rowid).val());
		 $('#holderName').val($('#tenatnamebynameid_'+rowid).val());  
		 $('#holder-popup').dialog('close'); 
	});
	$('#close').click(function(){
		 $('#holder-popup').dialog('close'); 
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
		position: absolute!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.tenant_list ul li{
		padding:3px;
	}
	.tenant_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>