<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
#asset-summary>div {
	padding: 3px;
}
</style>
<script type="text/javascript">
var idname="";
var slidetab="";
var popval=""; 
var assetObject = [];
$(function(){    
	$jquery("#asset_revaluation").validationEngine('attach'); 

	$('#revaluationDate').datepick();

	 $('#revaluation_discard').click(function(){ 
		 showAssetRevaluation("");
		 return false; 
	 });

	
	$('#revaluationSource').change(function() {
		$('#sourceDetail').html('');
		$('#sourceDetail')
		.append(
				'<option value="">Select</option>');
		if(Number($(this).val())>0) {
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_revaluation_source_detail.action",
											async : false,
											dataType : "json",
											data : {
												source : $(this).val()
											},
											cache : false,
											success : function(response) {
												$
														.each(
																response.sourceDetails,
																function(key,
																		value) {
																	$(
																			'#sourceDetail')
																			.append(
																					'<option value='
				+ key
				+ '>'
																							+ value
																							+ '</option>');
																});
											}
										});
							}
						});

		$('#fairMarketValue')
				.change(
						function() {
							var surplus = ((Number($(this).val()) - Number(assetObject.bookValue)));
							var reserve = surplus
									+ (Number(assetObject.revaluationReserve));
							$('#surplus').html(
									convertToAmount(Math
											.abs(surplus.toFixed(2))));
							$('#reserve').html(
									reserve >= 0 ? convertToAmount(Math
											.abs(reserve.toFixed(2))) : "-"
											+ convertToAmount(Math.abs(reserve
													.toFixed(2))));
						});

		$('#revaluation_save')
				.click(
						function() {
							if($jquery("#asset_revaluation").validationEngine('validate')){
								var assetRevaluationId = Number($(
										'#assetRevaluationId').val());
								var referenceNumber = $('#referenceNumber')
										.val();
								var assetId = Number($('#assetCreationId')
										.val());
								var valuationType = Number($('#valuationType')
										.val());
								var revaluationType = Number($(
										'#revaluationType').val());
								var fairMarketValue = Number($(
										'#fairMarketValue').val());
								var revaluationMethod = Number($(
										'#revaluationMethod').val());
								var revaluationDate = $('#revaluationDate')
										.val();
								var description = $('#description').val();
								var revaluationSource = Number($(
										'#revaluationSource').val());
								var sourceDetail = Number($('#sourceDetail')
										.val());
								var revaluationApproach = Number($(
										'#revaluationApproach').val());
								$
										.ajax({
											type : "POST",
											url : "<%=request.getContextPath()%>/save_asset_revaluation.action", 
									 	async: false,
									 	data:{ 
												assetId : assetId,
												assetRevaluationId : assetRevaluationId,
												referenceNumber : referenceNumber,
												description : description,
												valuationType : valuationType,
												revaluationType : revaluationType,
												fairMarketValue : fairMarketValue,
												revaluationMethod : revaluationMethod,
												source : revaluationSource,
												sourceDetail : sourceDetail,
												apporach : revaluationApproach,
												revaluationDate : revaluationDate,
												assetDetails : JSON
														.stringify(assetObject)
											},
											dataType : "json",
											cache : false,
											success : function(response) {
												if (response.returnMessage == "SUCCESS") {
													showAssetRevaluation(assetRevaluationId > 0 ? "Record updated."
															: "Record created.");
												} else {
													$('#page-error')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													return false;
												}
											}
										});
							} else {
								return false;
							}
						});

		$('.common-popup').click(
				function() {
					$('.ui-dialog-titlebar').remove();
					popval = $(this).attr('id');
					var actionValue = "";
					if (popval == "asset")
						actionValue = "show_common_asset_popup";
					else
						actionValue = "show_customer";
					$.ajax({
						type : "POST",
						url : "<%=request.getContextPath()%>/"+actionValue+".action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	 $('#asset-common-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 top: 0,
		 modal: true 
	}); 
});
function showAssetRevaluation(message){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_revaluation.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();
				$("#main-wrapper").html(result);
				if (message != null && message != '') {
					$('#success_message').hide().html(message).slideDown(1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
function commonAssetPopup(assetId, assetName){
	$('#assetCreationId').val(assetId);
	$('#assetCreationName').val(assetName); 
	var revaluationDate = $('#revaluationDate').val();
	$('#asset-summary').hide();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_asset_value_detail.action",
					async : false,
					dataType : "json",
					data : {
						assetCreationId : assetId,
						commenceDate : revaluationDate
					},
					cache : false,
					success : function(response) {
						assetObject = {
							"assetCost" : response.assetValueDetail.assetCost,
							"depreciation" : response.assetValueDetail.depreciationExpenses,
							"bookValue" : response.assetValueDetail.bookValue,
							"revaluationReserve" : null != response.assetValueDetail.accumulatedSurplus ? response.assetValueDetail.accumulatedSurplus
									: 0
						};
						$('#assetCost').html(
								response.assetValueDetail.showAssetCost);
						$('#accumualtedDepreciation')
								.html(
										response.assetValueDetail.showAccumulatedDepreciation);
						$('#bookValue').html(
								response.assetValueDetail.showBookValue);
						$('#reserve')
								.html(
										response.assetValueDetail.showAccumulatedSurplus);
						$('#asset-summary').show();
					}
				});
		return false;
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			revaluation
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general
			information
		</div>
		<form name="asset_revaluation" id="asset_revaluation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetRevaluationId"
					name="assetRevaluationId"
					value="${ASSET_REVALUATION.assetRevaluationId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 175px;">
							<div style="display: none;">
								<label class="width30" for="revaluationMethod">Revaluation
									Method<span class="mandatory">*</span> </label> <select
									name="revaluationMethod" id="revaluationMethod"
									class="width51">
									<option value="">Select</option>
									<c:forEach var="revaluationMethod"
										items="${REVALUATION_METHOD}">
										<option value="${revaluationMethod.key}">${revaluationMethod.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="revaluationSource">Revaluation
									Source<span class="mandatory">*</span> </label> <select
									name="revaluationApproach" id="revaluationSource"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="revaluationSource"
										items="${REVALUATION_SOURCE}">
										<option value="${revaluationSource.key}">${revaluationSource.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="sourceDetail">Source Detail<span
									class="mandatory">*</span> </label> <select name="sourceDetail"
									id="sourceDetail" class="width51 validate[required]">
									<option value="">Select</option>
								</select>
							</div>
							<div>
								<label class="width30" for="revaluationApproach">Revaluation
									Approach </label> <select name="revaluationApproach"
									id="revaluationApproach" class="width51">
									<option value="">Select</option>
									<c:forEach var="revaluationApproach"
										items="${REVALUATION_APPROACH}">
										<option value="${revaluationApproach.key}">${revaluationApproach.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_REVALUATION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 175px;">
							<div>
								<label class="width30" for="referenceNumber">Reference <span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_REVALUATION.referenceNumber ne null && ASSET_REVALUATION.referenceNumber ne ''}">
										<input type="text" readonly="readonly" name="referenceNumber"
											id="referenceNumber" class="width50 validate[required]"
											value="${ASSET_REVALUATION.referenceNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="referenceNumber"
											id="referenceNumber" class="width50 validate[required]"
											value="${requestScope.referenceNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="revaluationDate">Revaluation
									Date<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_REVALUATION.revaluationDate ne null && ASSET_REVALUATION.revaluationDate ne ''}">
										<c:set var="revaluationDate"
											value="${ASSET_REVALUATION.revaluationDate}" />
										<%
											String revaluationDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"revaluationDate").toString());
										%>
										<input type="text" readonly="readonly" name="revaluationDate"
											id="revaluationDate" class="width50 validate[required]"
											value="<%=revaluationDate%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="revaluationDate"
											name="revaluationDate" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetCreationName">Asset<span
									class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" id="assetCreationName"
									value="${ASSET_REVALUATION.assetCreation.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="assetCreationId"
									value="${ASSET_REVALUATION.assetCreation.assetCreationId}" />
								<span class="button"> <a style="cursor: pointer;"
									id="asset"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="valuationType">Valuation
									Type<span class="mandatory">*</span> </label> <select
									name="valuationType" id="valuationType"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="valuationType" items="${VALUATION_TYPE}">
										<option value="${valuationType.key}">${valuationType.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="revaluationType">Revaluation
									Type<span class="mandatory">*</span> </label> <select
									name="revaluationType" id="revaluationType"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="revaluationType" items="${REVALUATION_TYPE}">
										<option value="${revaluationType.key}">${revaluationType.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="fairMarketValue">Fair Market
									Value<span class="mandatory">*</span> </label> <input type="text"
									id="fairMarketValue"
									value="${ASSET_REVALUATION.fairMarketValue}"
									class="width50 validate[required,custom[number]]" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content" id="hrm">
				<div class="width50 float-left" id="asset-summary"
					style="display: none;">
					<div class="clearfix">
						<span class="width25 float-left">Asset Cost </span><span>:</span>
						<span class="width30" id="assetCost"></span>
					</div>
					<div class="clearfix">
						<span class="width25 float-left">Accumualted Dep </span> <span>:</span>
						<span class="width30" id="accumualtedDepreciation"></span>
					</div>
					<div class="clearfix">
						<span class="width25 float-left">Book Value </span> <span>:</span>
						<span class="width30" id="bookValue"></span>
					</div>
					<div class="clearfix">
						<span class="width25 float-left">Surplus </span> <span>:</span> <span
							class="width30" id="surplus"></span>
					</div>
					<div class="clearfix">
						<span class="width25 float-left">Reserve </span> <span>:</span> <span
							class="width30" id="reserve"></span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="revaluation_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="revaluation_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>