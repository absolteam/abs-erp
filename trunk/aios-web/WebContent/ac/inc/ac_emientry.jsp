<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
#common-popup{
	overflow: hidden;
} 
.width95{
	width:95% !important;
}
.sys_var{
	display: none;
}
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var slidetab="";  var lineDetails = "";
var collateralLineDetail="";
var showPage="emi";
$(function(){ 
	$('.loan-popup').click(function(){ 
	       tempid=$(this).attr('id'); 
	       $('.ui-dialog-titlebar').remove();  
	       var accountId=Number(0); 
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getloanpopup_details.action", 
			 	async: false, 
			 	data:{accountId:accountId, showPage:showPage},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.emilinedetails').html("");
					$('#savediscard').hide();
					$('.calculate').hide(); 
					$('.build_schedule').show();
					$('.loanpop-result').html(result);  
				},
				error:function(result){  
					 $('.loanpop-result').html(result); 
				}
			});  
	 });
	 $('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
	 $("#loanentry_details").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	 
	 $('.build_schedule').live('click',function(){
		 if($('#loanentry_details').validationEngine({returnIsValid:true})){ 
			 var loanId=Number($('#loanId').val()); 
			 var sheduleBy="S";  
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/build_loanschedule.action", 
			 	async: false,  
			 	data:{loanId:loanId, sheduleBy:sheduleBy},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.emilinedetails').html(result);    
					 $('#savediscard').show();   
					 $('.build_schedule').hide();
					 return false;
				},
				error:function(result){  
					 $('.emilinedetails').html(result); 
				} 
			});  
		 }else{
			 return false;
		}
		return false;
	 });
	 $('.other-popup').click(function(){
		$('.ui-dialog-titlebar').remove();  
		$('.formError').hide(); 
		$('#savediscard').hide();
		$('.calculate').hide();
		$('.build_schedule').show();
		$('.otherx-popup').dialog('open'); 
	 });
	 
	// management Dialog			
	 	$('.otherx-popup').dialog({
	 		autoOpen: false,
	 		width: 500, 
	 		bgiframe: true,
	 		modal: true,
	 		buttons: {
	 			"Close": function(){   
	 				if($('#varianceDays').val()<=31 && $('#yearlyNoDate').val()<=365 && $('#addCharge').val()<=100){
	 					if($('#varianceBy').val()!=null && $('#varianceBy').val()!="")
		 					$('#otherDetails').val($('#varianceBy  option:selected').text()+" ,"+$('#yearlyDays  option:selected').text()+" ,"+$('#addCharge').val());
		 				else if($('#sheduleBy').val()!=null && $('#sheduleBy').val()!="")
		 					$('#otherDetails').val("Schedule By User");
		 				$(this).dialog("close"); 
	 				}
	 				else{
	 					$('#pop-error').hide().html("couldn't process your request.").slideDown();
	 				}
	 			}, 
			 	"Clear": function() {  
					$('#varianceBy').val("");
					$('#varianceDays').val("");
					$('#yearlyDays').val("");
					$('#yearlyNoDate').val(""); 
					$('#addCharge').val(""); 
					$('#otherDetails').val("");
					$('#sheduleBy').val("");
					$(this).dialog("close");
				} 
	 		}
	 	});
	
	 	
	/* $('#sheduleBy').change(function(){ 
		 if($('#sheduleBy').val()!=null && $('#sheduleBy').val()!=""){ 
			 if($('#sheduleBy').val()=="U"){ 
				 $('.sys_var').slideUp();
				 $('#varianceBy').val("");
				 $('#varianceDays').val("");
				 $('#yearlyDays').val("");
				 $('#yearlyNoDate').val(""); 
				 $('#addCharge').val(""); 
				 $('#otherDetails').val("");
			 }
			 else{ 
				 $('.sys_var').slideDown();
			 }
		 }
	 });*/
	 
	$('#varianceBy').change(function(){
		if($('#varianceBy').val()!=null && $('#varianceBy').val()!=""){
			if($('#varianceBy').val()=='U'){
				$('#varianceDays').show();
			}
			else{
				$('#varianceDays').val("").hide();
			}
		}
		else{
			$('#varianceDays').val("").hide();
		}
	});
	
	$('#yearlyDays').change(function(){
		if($('#yearlyDays').val()!=null && $('#yearlyDays').val()!=""){
			if($('#yearlyDays').val()=='U'){
				$('#yearlyNoDate').show();
			}
			else{
				$('#yearlyNoDate').val("").hide();
			}
		}
		else{
			$('#yearlyNoDate').val("").hide();
		}
	});
	
	$('.discard_emi').click(function(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getemi_details.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove(); 
				$('.otherx-popup').dialog('destroy');		
				$('.otherx-popup').remove();  
				$("#main-wrapper").html(result); 
				return false;
			} 
		});  
		return false;
	});
	
	$('.save').click(function(){
		var loanId=$('#loanId').val();
		lineDetails="";
		lineDetails = getEMILineDetails();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/saveemi_details.action", 
		 	async: false,   
		 	data:{loanId:loanId, emiLines:lineDetails},
		    dataType: "html",
		    cache: false,
		    success:function(result){   
				 $(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/getemi_details.action", 
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){
								$('#common-popup').dialog('destroy');		
								$('#common-popup').remove(); 
								$('.otherx-popup').dialog('destroy');		
								$('.otherx-popup').remove();  
								$("#main-wrapper").html(result); 
								$('#success_message').hide().html(message).slideDown(1000);
							}
					 });
				 }
				 else{
					 $('#page-error').hide().html(message).slideDown(1000);
					 return false;
				 }
			},
			error:function(result){  
				$('#page-error').hide().html("Server goes wrong").slideDown(1000);
			}
		});  
	});
	
	$('.calculate').click(function(){
		lineDetails="";
		lineDetails = getEMILineDetails();  
		var loanId=$('#loanId').val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/calculate_emi.action", 
		 	async: false,   
		 	data:{loanId:loanId, emiLines:lineDetails},
		    dataType: "html",
		    cache: false,
		    success:function(result){   
		    	$('.emilinedetails').html(result);    
		    	 $('.calculate').show(); 
				 $('#savediscard').show();
		    }
		});
	});
	
	var getEMILineDetails=function(){
		$('.rowid').each(function(){
			var rowId=getRowId($(this).attr('id'));  
			var lineDate=$('#dueDate_'+rowId).val();
			var instalment=$('#instalment_'+rowId).val();
			var interest=$('#interest_'+rowId).val();
			var insurance=$('#insurance_'+rowId).val();
			var otherCharges=$('#addtionalcharges_'+rowId).val(); 
			var eiborAmount=$('#eiborAmount_'+rowId).val();
			var description=$('#description_'+rowId).val(); 
			if(typeof lineDate != 'undefined' && lineDate!=null && lineDate!="" && instalment!=null && instalment!=""){
				instalment=instalment.replace(/,/gi,''); 
				if(typeof interest== 'undefined' || interest== null  || interest== ''){
					interest=0;
				}else{
					interest=interest.replace(/,/gi,'');
				}
				if(typeof eiborAmount=='undefined' || eiborAmount== null  || eiborAmount== ''){
					eiborAmount=0;
				}else{
					eiborAmount=eiborAmount.replace(/,/gi,'');
				}
				if(typeof insurance== 'undefined' || insurance== null  || insurance== ''){
					insurance=0;
				}else{
					insurance=insurance.replace(/,/gi,'');
				}
				if(typeof otherCharges== 'undefined' || otherCharges== null  ||otherCharges== ''){
					otherCharges=0;
				}else{
					otherCharges=otherCharges.replace(/,/gi,'');
				}
				if(typeof description== 'undefined' || description== ''){
					description=null;
				}  
				lineDetails+=lineDate+"↕"+instalment+"↕"+interest+"↕"+insurance+"↕"+otherCharges+"↕"+eiborAmount+"↕"+description+"↕↕";
			}
		});
		return lineDetails;
	};
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>emi information</div>	
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general information</div>	
		  <form name="loanentry_details" id="loanentry_details"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="emiId" name="emiId" value=""/>
				<div class="width100 float-left" id="hrm"> 
					<fieldset> 
						<div class="width100 float-left">
							<div class="width48 float-right"> 
								<div>
									<label class="width30" for="rateType">Rate Type</label>
									<input type="text" readonly="readonly" name="rateType" id="rateType" class="width50 validate[required]"/>
								</div> 
								<div>
									<label class="width30" for="rateType">Rate Variance</label>
									<input type="text" readonly="readonly" name="rateVariance" id="rateVariance" class="width50 validate[required]"/>
								</div> 
								<div>
									<label class="width30" for="emiFrequency">EMI Frequency</label>
									<input type="text" readonly="readonly" name="emiFrequency" id="emiFrequency" class="width50 validate[required]"/>
								</div> 
								<div>
									<label class="width30" for="emiDueDate">First Due</label>
									<input type="text" readonly="readonly" name="emiDueDate" id="emiDueDate" tabindex="5" class="width50 validate[required]"/>
								</div> 
								<div style="display:none;">
									<label class="width30" for="leadDay">Lead Day</label>
									<input type="text" readonly="readonly" name="leadDay" id="leadDay" tabindex="6" class="width50 validate[required]"/>
								</div> 
							</div>
							<div class="width50 float-left">
								<div>
									<label class="width30" for="loanNumber">Loan Number<span style="color: red;">*</span></label>
									<input type="text" readonly="readonly" name="loanNumber" id="loanNumber" tabindex="1" class="width50 validate[required]"/>
									<span class="button">
										<a style="cursor: pointer;" id="accountid" class="btn ui-state-default ui-corner-all loan-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>
									<input type="hidden" readonly="readonly" name="loanId" id="loanId"/>
								</div> 
								<div style="display:none;">
									<label class="width30" for="currency">Currency</label>
									<input type="text" readonly="readonly" name="currency" id="currency" class="width50 validate[required]"/>
								</div> 
								<div>
									<label class="width30" for="loanType">Loan Type</label>
									<input type="text" readonly="readonly" name="loanType" id="loanType" tabindex="2" class="width50 validate[required]"/>
								</div>
								<div>
									<label class="width30" for="approvalAmount">Approval Amount</label>
									<input type="text" readonly="readonly" name="approvalAmount" id="approvalAmount" tabindex="3" class="width50 validate[required]"/>
								</div> 
								<div>
									<label class="width30" for="accountNumber">Account Number</label>
									<input type="text" readonly="readonly" name="accountNumber" id="accountNumber" tabindex="4" class="width50 validate[required]"/>
								</div> 
								<div style="display: none;">
									<label for="sheduleBy" class="width30">Schedule By</label>
									<select name="sheduleBy" id="sheduleBy" class="width50">
										<option value="">Select</option>
										<option value="U">User</option>
										<option value="S">System</option>
									</select> 
								</div>
								<div style="display:none;">
									<label class="width30" for="otherDetails">Other Details</label>
									<input type="text" readonly="readonly" name="otherDetails" id="otherDetails" tabindex="5" class="width50 validate[required]"/>
									<span class="button">
										<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all other-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>
								</div> 
							</div>
						</div>  
					</fieldset>
				</div>  
				<div class="otherx-popup" id="hrm" class="width100 float-left">
					<div id="pop-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
					<div style="margin-top: 3px;">
						<label for="sheduleBy" class="width15">Schedule By</label>
						<select name="sheduleBy" id="sheduleBy" class="width50">
							<option value="">Select</option>
							<option value="U">User</option>
							<option value="S">System</option>
						</select> 
					</div>
					<div class="sys_var">
						<label for="varianceBy" class="width15">Month Variance</label>
						<select name="varianceBy" id="varianceBy" class="width50">
							<option value="">Select</option>
							<option value="U">User</option>
							<option value="S">System</option>
						</select>
						<input style="display:none;" type="text" name="varianceDays" id="varianceDays" class="width20"/>
					</div>
					<div style="margin-top: 3px;" class="sys_var">
						<label for="yearlyDays" class="width15">Yearly Days</label>
						<select name="yearlyDays" id="yearlyDays" class="width50">
							<option value="">Select</option>
							<option value="U">User</option>
							<option value="S">System</option>
						</select>
						<input style="display:none;" type="text" name="yearlyNoDate" id="yearlyNoDate" class="width20"/>
					</div> 
					<div style="display:none;">
						<label for="addCharge" class="width15">Addtional Charges</label>
						<input type="text" name="addCharge" id="addCharge" class="width50"/>
					</div> 
					<div class="insterestrate" style="margin-top: 3px; display: none;">
						<label for="interestRate" class="width15">Interest</label>
						<input type="text" name="interestRate"  id="interestRate" class="width50"/>
					</div> 
				</div>
				<div class="clearfix"></div> 
 				<div class="portlet-content emilinedetails" id="hrm"></div>  
		</div>  
		<div class="clearfix"></div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;" > 
			<div class="portlet-header ui-widget-header float-right build_schedule" style="cursor:pointer;display:none;">Build Schedule</div>   
			<div class="portlet-header ui-widget-header float-right discard_emi" style="cursor:pointer;display:none;"><fmt:message key="accounts.common.button.discard"/></div>  
	 	</div>  
		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div style="display: none;" class="portlet-header ui-widget-header float-left calculate" style="cursor:pointer;">calculate</div> 
		</div>  
		<div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="loanpop-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>  
  </form>
  </div> 
</div>