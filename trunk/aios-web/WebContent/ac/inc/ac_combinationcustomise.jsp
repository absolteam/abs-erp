<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.ui-dialog{
	z-index: 99999!important;
}
.ui-autocomplete-input {
	min-height: 27px !important;
}
.ui-widget-overlay{
	z-index: 99999!important; 
}
 
</style>
<script type="text/javascript">
var combinationId=0;
var segmentId=0; 
$(function(){
	if($('.skip').attr('id')=="add-template" || $('.skip').attr('id')==''){
		$('.skip').html('Ok');
	}else{
		$('.skip').html('Skip');
	}
	$("#tree").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	}); 
	$('.autoComplete').combobox({ 
       selected: function(event, ui){  
           var combtext=$(".autoComplete option:selected").text().trim(); 
           var result = combtext.lastIndexOf("[")+1;
           var result1= combtext.lastIndexOf("]");
           result=combtext.substring(result,result1);
           var combarray=result.split(".");
           segmentId=combarray.length; 
           combinationId=$(this).val();
     	   $('#add').trigger('click');
       }
	});

	$('.createcombi').mouseenter(function() { 
		 combinationId=0;
		 segmentId=0; 
	});

	$('.companys').live('mouseenter',function(){ 
		combinationId=$($($(this).children()).get(0)).val(); 
		segmentId=1;
	});

	$('.costscenter').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val(); 
		segmentId=2;
	});

	$('.naturals').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val(); 
		segmentId=3;
	});
	
	$('.analysis').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=4;
	});
	
	$('.buffer1s').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=5;
	});

	$('.buffer2s').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=6;
	});

	$('.skip').click(function(){  
		 var id=$('.skip').attr('id'); 
		 var url_action=""; 
		 if(id=="add-template" || id==""){
			url_action="chart_of_accounts_retrieve";
		 }
		 else{
			url_action="customise_unearnedrevenue"; 
		 }
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/"+url_action+".action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){   
				if(url_action=="chart_of_accounts_retrieve"){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove(); 
					$("#main-wrapper").html(result);
				}else{
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$("#DOMWindow").html(result);  
				} 
			} 		
		});  
  	});

	$('#add').click(function(){
		$('.success,.error').hide();
		$('.ui-dialog-titlebar').remove();  
		segmentId=segmentId+1;
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getcombination_account_codes.action", 
		 	async: false, 
		 	data:{segmentId:segmentId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.popup-result').html(result);  
			},
			error:function(result){  
				 $('.popup-result').html(result); 
			}
		});
		 return false;
	});

	 $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:400,
		height:200,
		zIndex:99999,
		bgiframe: true,
		modal: true 
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>combination tree entry</div>	
		  <form name="combinationview" id="combinationviewValidation"> 
			<div class="portlet-content">  
			 <div id="temp-result" style="display: none;"></div> 
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
				<div id="page-success" class="response-msg success ui-corner-all width90" style="display:none;"></div>  
			  	<div class="width100 float-left" id="hrm">  
			  		<div class="float-right  width30">  
			  			<select name="comb_tree" id="combinationSelectTree" class="autoComplete">
			  				<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}" varStatus="status1">
			  				<option value=""></option>
			  					<option value="${bean.combinationId}">${bean.accountByCompanyAccountId.account} [${bean.accountByCompanyAccountId.code}]
			  							<c:choose>
											<c:when test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
												<c:forEach var="costbean" items="${bean.costVos}" varStatus="cstatus1">
													<option value="${costbean.combinationId}">${costbean.accountByCostcenterAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
														<c:choose>
															<c:when test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
																<c:forEach var="naturalbean" items="${costbean.naturalVos}" varStatus="nstatus1">
																	<option value="${naturalbean.combinationId}">
																		${naturalbean.accountByNaturalAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}]
																			<c:choose>
																				<c:when test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
																					<c:forEach var="analysisbean" items="${naturalbean.analysisVos}" varStatus="astatus1">
																						<option value="${analysisbean.combinationId}">
																							${analysisbean.accountByAnalysisAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}]
																							<c:choose>
																								<c:when test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0}">
																									<c:forEach var="buffer1bean" items="${analysisbean.buffer1Vos}" varStatus="b1status1">
																										<option value="${buffer1bean.combinationId}">
																											${buffer1bean.accountByBuffer1AccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}]
																												<c:choose>
														   															<c:when test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
														   																<c:forEach var="buffer2bean" items="${buffer1bean.buffer2Vos}" varStatus="b2status">
														   																	<option value="${buffer2bean.combinationId}">${buffer2bean.accountByBuffer2AccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}]
														   																	</option>
														   																</c:forEach>
														   															</c:when>
														   														</c:choose>	
																										</option>		
																									</c:forEach>
																								</c:when>
																							</c:choose>
																						</option>		
																					</c:forEach>
																				</c:when>
																			</c:choose>
																	</option>		
																</c:forEach>
															</c:when>
														</c:choose>
													</option>
												</c:forEach> 
											</c:when>
										</c:choose>	
			  					</option>
			  				</c:forEach>
			  			</select>
			  		</div>
					<div class="float-left  width50">   
						<div id="main">
							<div id="sidetree">
								<div id="sidetreecontrol"></div> 
									<c:choose>
								<c:when
									test="${COMBINATION_TREE_VIEW ne null && COMBINATION_TREE_VIEW ne ''}">
									<div id="rightclickarea">
										<ul id="tree">
											<li><a style="font-weight: bold;" class="createcombi">Combination</a>
												<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}"
													varStatus="status">
													<li><a
														id="company_${status.index+1}_${status.index+1}"
														class="combination_ companys"> <input type="hidden"
															id="companyId_${status.index+1}_${status.index+1}"
															value="${bean.combinationId}" />
															${bean.accountByCompanyAccountId.account}
															[${bean.accountByCompanyAccountId.code}]</a> <c:choose>
															<c:when
																test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
																<ul>
																	<c:forEach var="costbean" items="${bean.costVos}"
																		varStatus="cstatus">

																		<li><a class="combination_ costscenter"
																			id="costcenter_${status.index+1}_${cstatus.index+1}">
																				<input type="hidden"
																				id="costcenterId_${status.index+1}_${cstatus.index+1}"
																				value="${costbean.combinationId}" />
																				${costbean.accountByCostcenterAccountId.account}
																				[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
																		</a> <c:choose>
																				<c:when
																					test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
																					<ul>
																						<c:forEach var="naturalbean"
																							items="${costbean.naturalVos}"
																							varStatus="nstatus">
																							<li><a class="combination_ naturals"
																								id="natural_${cstatus.index+1}_${nstatus.index+1}">
																									<input type="hidden"
																									id="naturalId_${cstatus.index+1}_${nstatus.index+1}"
																									value="${naturalbean.combinationId}" />
																									${naturalbean.accountByNaturalAccountId.account}
																									[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}]</a>
																								<c:choose>
																									<c:when
																										test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
																										<ul>
																											<c:forEach var="analysisbean"
																												items="${naturalbean.analysisVos}"
																												varStatus="astatus">
																												<c:if
																													test="${naturalbean.accountByNaturalAccountId eq analysisbean.accountByNaturalAccountId}">
																													<li><a class="combination_ analysis"
																														id="analysis_${nstatus.index+1}_${astatus.index+1}">
																															<input type="hidden"
																															id="analysisId_${nstatus.index+1}_${astatus.index+1}"
																															value="${analysisbean.combinationId}" />
																															${analysisbean.accountByAnalysisAccountId.account}
																															[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}]</a>
																														<c:choose>
																															<c:when
																																test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0 &&
														   																		naturalbean.accountByNaturalAccountId.accountId eq analysisbean.accountByNaturalAccountId.accountId}">
																																<ul>
																																	<c:forEach var="buffer1bean"
																																		items="${analysisbean.buffer1Vos}"
																																		varStatus="b1status">
																																		<li><a
																																			class="combination_ buffer1s"
																																			id="buffer1_${astatus.index+1}_${b1status.index+1}">
																																				<input type="hidden"
																																				id="buffer1Id_${astatus.index+1}_${b1status.index+1}"
																																				value="${buffer1bean.combinationId}" />
																																				${buffer1bean.accountByBuffer1AccountId.account}
																																				[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}]</a>
																																			<c:choose>
																																				<c:when
																																					test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
																																					<ul>
																																						<c:forEach var="buffer2bean"
																																							items="${buffer1bean.buffer2Vos}"
																																							varStatus="b2status">
																																							<li><a
																																								class="combination_ buffer2s"
																																								id="buffer2_${b1status.index+1}_${b2status.index+1}">
																																									<input type="hidden"
																																									id="buffer2Id_${b1status.index+1}_${b2status.index+1}"
																																									value="${buffer2bean.combinationId}" />
																																									${buffer2bean.accountByBuffer2AccountId.account}
																																									[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}]</a>
																																								<span style="cursor: pointer;"
																																								id="deleteoption_${buffer2bean.combinationId}"
																																								class="deleteoption"><img
																																									width="10" height="10"
																																									src="./images/cancel.png">
																																							</span>
																																							</li>
																																						</c:forEach>
																																					</ul>
																																				</c:when>
																																				<c:otherwise>
																																					<span style="cursor: pointer;"
																																						id="deleteoption_${buffer1bean.combinationId}"
																																						class="deleteoption"><img
																																						width="10" height="10"
																																						src="./images/cancel.png"> </span>
																																				</c:otherwise>
																																			</c:choose></li>
																																	</c:forEach>
																																</ul>
																															</c:when>
																															<c:otherwise>
																																<span style="cursor: pointer;"
																																	id="deleteoption_${analysisbean.combinationId}"
																																	class="deleteoption"><img
																																	width="10" height="10"
																																	src="./images/cancel.png"> </span>
																															</c:otherwise>
																														</c:choose></li>
																												</c:if>
																											</c:forEach>
																										</ul>
																									</c:when>
																									<c:otherwise>
																										<span style="cursor: pointer;"
																											id="deleteoption_${naturalbean.combinationId}"
																											class="deleteoption"><img width="10"
																											height="10" src="./images/cancel.png">
																										</span>
																									</c:otherwise>
																								</c:choose></li>
																						</c:forEach>
																					</ul>
																				</c:when>
																				<c:otherwise>
																					<span style="cursor: pointer;"
																						id="deleteoption_${costbean.combinationId}"
																						class="deleteoption"><img width="10"
																						height="10" src="./images/cancel.png"> </span>
																				</c:otherwise>
																			</c:choose></li>
																	</c:forEach>
																</ul>
															</c:when>
															<c:otherwise>
																<span style="cursor: pointer;"
																	id="deleteoption_${bean.combinationId}"
																	class="deleteoption"><img width="10" height="10"
																	src="./images/cancel.png"> </span>
															</c:otherwise>
														</c:choose></li>
												</c:forEach></li>
										</ul>
									</div>
									<div class="vmenu">
										<div class="first_li">
											<span>Add</span>
										</div>
										<div class="sep_li"></div>
										<div class="first_li">
											<span>Close</span>
										</div>
									</div>
								</c:when>
							</c:choose>
								</div> 
							</div> 
						</div> 
				</div>  
			</div> 
			<div style="display: none;" class="portlet-header ui-widget-header float-right" id="add">
				add
			</div> 
			<div style="display: none;"
			class="portlet-header ui-widget-header float-right"
			id="account-popup-close">close</div>
			<div class="clearfix"></div>  
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a  class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="popup-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 	
				<div class="portlet-header ui-widget-header float-right skip" id="${showPage}" style="cursor:pointer;"></div> 
			</div>
		  </form>
  	</div> 
</div>