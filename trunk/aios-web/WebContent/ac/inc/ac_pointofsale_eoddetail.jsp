<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$(function(){
	$('#salesDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, onSelect: showPointOfSaleEOD, showTrigger: '#calImg'});
	$('#discard').click(function(){ 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_eod_balance.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $("#main-wrapper").html(result);  
			}
		});
	 });
	
	$('#storeId').change(function(){ 
		showPointOfSaleEOD();
	});
	
	$(".pos-eod-print").click(function(){
		var selectedStoreId = Number($('#storeId').val());
		var fromDate = $('#salesDate').val(); 
		var toDate = $('#salesDate').val(); 
		window.open("<%=request.getContextPath()%>/show_eodsales_printout.action?selectedStoreId="+selectedStoreId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
			'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	}); 
	showPointOfSaleEOD();
});
function showPointOfSaleEOD(){
	var selectedStoreId = Number($('#storeId').val());
	var fromDate = $('#salesDate').val();
	var toDate = $('#salesDate').val(); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_daily_sales_reportsummary.action", 
	 	async: false, 
	 	data: {selectedStoreId: selectedStoreId, fromDate: fromDate, toDate: toDate},
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			 $(".poseod_detail").html(result);  
		}
	}); 
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>end of day sales summary
		</div>
		<form name="pos_eoddetails" id="pos_eoddetails"
			style="position: relative;">
			<div class="portlet-content">
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-left">
						<fieldset style="min-height: 50px;">
							<div>
								<label class="width30" for="termId">Store</label> 
								<c:choose>
									<c:when test="${requestScope.STORE_ID ne null && requestScope.STORE_ID gt 0}">
										<c:forEach var="store" items="${STORE_DETAIL}">
											<c:set var ="storeNm" value="${store.storeName}"/>
											<c:set var ="storeID" value="${store.storeId}"/>
										</c:forEach>
										<input type="hidden" id="storeId" value="${storeID}"/>
										<input type="text" id="storeName" class="width50" value="${storeNm}" readonly="readonly"/>
									</c:when>
									<c:otherwise>
										<select
											name="storeId" id="storeId" style="width: 51%;">
											<option value="-1">--All--</option>
											<c:choose>
												<c:when test="${STORE_DETAIL ne null && STORE_DETAIL ne ''}">
													<c:forEach var="store" items="${STORE_DETAIL}">
														<option value="${store.storeId}">${store.storeName}</option>
													</c:forEach>
												</c:when>
											</c:choose>
										</select>
									</c:otherwise>
								</c:choose> 
							</div>
							<div>
								<label class="width30">Date</label> <input type="text"
									id="salesDate" class="width50" />
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content poseod_detail"
					style="margin-top: 10px;" id="hrm"> 
					 
				</div>
				<div class="clearfix"></div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div class="portlet-header ui-widget-header float-right pdiscard"
						id="discard">
						<fmt:message key="accounts.common.button.cancel" />
					</div>
					<div
						class="portlet-header ui-widget-header float-right pos-eod-print"
						id="pos-eod-print">print</div>
				</div>
			</div>
		</form>
	</div>
</div>