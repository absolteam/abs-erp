<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
 	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	$(function() {
		oTable = $('#SupplierCommonPopup')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_common_supplier_jsonlist.action?pageInfo"
									+ $('#pageInfo').val(),

							"aoColumns" : [ {
								"mDataProp" : "supplierNumber"
							}, {
								"mDataProp" : "supplierName"
							}, {
								"mDataProp" : "type"
							} ],
						});

		/* Click event handler */
		$('#SupplierCommonPopup tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					commonSupplierPopup(aData.supplierId, aData.supplierName,
							aData.combinationId, aData.accountCode, aData);
					$('#common-popup').dialog("close");
					return false;
				});
		
		$('#supplier-common-popup').click(function () { 
			$('#common-popup').dialog("close");
			return false;
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>supplier
		</div>
		<div class="portlet-content">
			<div id="product_json_list">
				<table id="SupplierCommonPopup" class="display">
					<thead>
						<tr>
							<th>Reference</th>
							<th>Supplier</th>
							<th>Type</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="supplier-common-popup">close</div>
			</div>
			<input type="hidden" id="pageInfo" value="${requestScope.pageInfo}" />
		</div>
	</div>
</div>