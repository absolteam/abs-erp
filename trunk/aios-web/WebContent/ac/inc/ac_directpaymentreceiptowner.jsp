<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Direct Payment</title>
</head>
<body>
	<div style="height: 420px; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;">
		<div align="center"> 
			<% 
				type = "";
				if(SecondCopy) {
					type = "(Client Copy)";
				} else {
					type = "(Owner Copy)";
					SecondCopy = true;
				}
			%> 
			<h3>Payment<%= type %></h2>	
			<hr/>
			<h5 style="display: inline;">P.O. Box: 5929 - (9712) 6671191 أبو ظبي - الامارات العربية المتحدة - ص. : 6662800 - فاكس</h5>
			<br/>
			<h5 style="display: inline;">P.O. Box: 5929 - Abu Dhabi - United Arab Emirates - Tel. : (9712) 6662800 - Fax: (9712) 6671191</h5>
			<hr/>
		</div>
		
		<div style="margin-bottom: 110px;">
			<div style="width: 33%;float:left;" align="center">
				<p>
					<span style="width: 68px; display: table-cell; border-style:solid; border: 1px;">Dhs. &nbsp&nbsp&nbsp درهم</span>
					<!--<span style="width: 50px; display: table-cell;  border-style:solid; border: 1px;">Fils  ???</span>
				--></p>
				<p>
					<span style="width: 68px; display: table-cell; border: thin; border-style:solid;">
					<% 
					amount=0d;
						for(DirectPaymentDetail directPaymentDetail : directPayment.getDirectPaymentDetails()) {
							amount+=directPaymentDetail.getAmount();
						}
					%>
					<%= amount%>
					</span>
					<!--<span style="width: 50px; display: table-cell;  border-style:solid; border: 1px;">00</span>
				--></p>
			</div>
			
			
			<div class="float_left bold" style="width: 33%; margin-top: 20px;" align="center">
				<p style="display: table-cell;">
					دفع قسيمة
				</p>
				<hr/>
				<p style="display: table-cell;">
					PAYMENT VOUCHER
				</p>
			</div> 
			<div class="float_right" style="width: 33%;" align="center">
				<p style="color: red;">
					No. <%= directPayment.getPaymentNumber()%>
				</p>
				<p>
					<% 
					payDate=DateFormat.convertDateToString(directPayment.getPaymentDate().toString()); 
					%>
					Date <u><%= payDate %></u> التاريخ
				</p>
			</div>
		</div> 
		<div>
			<div>
			<%
				name="";
				if(directPayment.getSupplier().getPerson()!=null){
					name=directPayment.getSupplier().getPerson().getFirstName()+" "+directPayment.getSupplier().getPerson().getLastName();
				}else{
					name=directPayment.getSupplier().getCmpDeptLocation().getCompany().getCompanyName();
				}
			%>
				<span class="detail_text left_align" style="width: 140px;">Paid To Mr/M/s.</span>			
				<span class="detail_text center_align span_border" style="width: 492px;"><%= name%></span>
				<span class="detail_text right_align" style="width: 115px;">وردت من السيد / السيدة</span>
			</div>
			<div>
				<% temp = AIOSCommons.convertAmountToWords(amount); %>
				<span class="detail_text left_align" style="width: 100px;">Sum of Dhs.</span>			
				<span class="detail_text center_align span_border" style="width: 595px;"><%= temp %></span>
				<span class="detail_text right_align" style="width: 78px;">مجموع درهم</span>
			</div>
			<div>
				<% temp="";
					temp = (Constants.Accounts.PaymentMode.get(directPayment.getPaymentMode())).toString()+" "; 
					if(directPayment.getBankAccount()!=null){
						temp+=""+directPayment.getBankAccount().getBank().getBankName()
						+" ("+directPayment.getBankAccount().getAccountNumber()+" )"+ directPayment.getChequeNumber();
					}
						
				%>
				<span class="detail_text left_align" style="width: 89px;">Payment Mode.</span>			
				<span class="detail_text center_align span_border" style="width: 598px;"><%= temp %></span>
				<span class="detail_text right_align" style="width: 59px;">على حساب</span>
			</div>
			<div>
				<%
					invNo = "";
					account = "";
					amounts = "";
					for(DirectPaymentDetail directPaymentDetail : directPayment.getDirectPaymentDetails()) {
						invNo += (directPaymentDetail.getInvoiceNumber() != null 
								&& directPaymentDetail.getInvoiceNumber() != "")? directPaymentDetail.getInvoiceNumber() 
										+ " , " : "Nil , "; 
						if(null!=directPaymentDetail.getCombination().getAccountByAnalysisAccountId()){
							account +=
								directPaymentDetail.getCombination().getAccountByNaturalAccountId().getAccount()
									+"."+directPaymentDetail.getCombination().getAccountByAnalysisAccountId().getAccount() + " , ";
						}else{
							account += 
								directPaymentDetail.getCombination().getAccountByNaturalAccountId().getAccount() + " , ";
						}
						amounts += (directPaymentDetail.getAmount() != null)? directPaymentDetail.getAmount() 
										+ " , " : "Nil , ";
					}
					/* for(ReContractAgreementTO payment : agreementTO.getContractAgreementList()) {
						
						chqNo += (payment.getChequeNumber() != null 
								&& payment.getChequeNumber() != "")? payment.getChequeNumber() 
										+ " (" + payment.getAttribute1() + ")" + " , " : "Cash (" + payment.getAttribute1() + ") , ";
						
						banks += (payment.getBankName() != null 
								&& payment.getBankName() != "")? payment.getBankName() + " , " : "";
								
						dates += (payment.getChequeNumber() != null 
								&& payment.getChequeNumber() != "")? payment.getChequeDate() + " , " : "";
					} */
					
					
				%>
				<span class="detail_text left_align" style="width: 110px;">Invoice No.</span>			
				<span class="detail_text center_align span_border" style="width: 563px;"><%= invNo %></span>
					<span class="detail_text right_align" style="width: 72px;">الفاتورة رقم</span>
			</div>
			<div>
				<span class="detail_text left_align" style="width: 300px;">Account. 
					<span class="span_border center_align" style="display: -moz-inline-stack; width: 204px;"><%= account %></span>
					<span class="right_align"> مجموعة </span> 
				 </span>			
				<span class="detail_text" style="width: 450px;">Amount.
					<span class="span_border center_align" style="display: -moz-inline-stack; width: 350px;"><%= amounts %></span>
					<span class="right_align"> كمية </span> 
				</span>				
			</div>
			<%-- <div>
				<%
					postChqNo = "";  
					for(ReContractAgreementTO post : agreementTO.getPostDetails()) {
							postChqNo += post.getChequeNumber() + " (" + post.getAttribute1() + ")" + " , ";
					}
				%>
				<span class="detail_text left_align" style="width: 110px;">PDC Details</span>	
				<span class="detail_text center_align span_border" style="width: 563px;"><%= postChqNo %></span>
				<span class="detail_text right_align" style="width: 72px;">شيكات مؤجلة</span>
			</div> --%>
			<div style="margin-top: 40px; padding-left: 10px; padding-right: 10px;">
				<div style="width: 220px; text-align: center; display: -moz-inline-stack;">
					<hr/>
					<span style="padding-bottom: 80px;">Receiver's Sign. / المتلقي في التوقيع</span>				
				</div>
				<div style="float: right; width: 220px; text-align: center;">
					<hr/>
					<span style="padding-bottom: 80px;">Accountant's Sign. / المحاسب التوقيع</span>
				</div>
			</div>
		</div>
		
		
	</div>
</body>
</html>