<tr id="fieldrow_${rowId}" class="rowid"> 	
	<td id="code_${rowId}"></td>
	<td id="description_${rowId}"></td>
	<td id="accounttype_${rowId}"></td> 
	<td id="subTypeName_${rowId}"></td>
	<td style="display:none;" id="accounttypeid_${rowId}"></td>
	<td style="display:none;" id="accountid_${rowId}"></td>  
	<td style="display:none;" id="lineId_${rowId}">${rowId}</td>
	<td class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataRow" id="AddImage_${rowId}" style="cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataRow" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowRow" id="DeleteImage_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
		  <input type="hidden" name="subTypeId" id="subTypeId_${rowId}" 
													  value="">
	</td>  
</tr>
<script type="text/javascript">
$(function(){  
	$('#segmentId').val($('#tempSegmentId').val());
	$('.addDataRow').click(function(){
		if($jquery("#coavalidate-form").validationEngine({returnIsValid:true})){
 		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addadd";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/chart_of_account_addentry.action",
			data:{id:rowid, showPage:showPage},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			}
		 });
		}
		else{
			return false;
		}		
	});

	$('.editDataRow').click(function(){
 		if($jquery("#coavalidate-form").validationEngine({returnIsValid:true})){   
			 $('.error').hide(); 
			 slidetab=$(this).parent().parent().get(0);   
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var showPage="addedit";
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).hide();
			 $("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/chart_of_account_addentry.action",
				data:{id:rowid,showPage:showPage},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 	$(result).insertAfter(slidetab); 
				 	$('#accountCode').val($('#code_'+rowid).text());
				 	$('#accountDescription').val($('#description_'+rowid).text()); 
				 	$('#accountTypeId').val($('#accounttypeid_'+rowid).text());
				 	return false;	
				}
			 });
			}
			else{
				return false;
			}
		event.preventDefault();	
	 });

	 $(".delrowRow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var code=$('#code_'+rowid).text().trim();
		 if(code!=null && code!=""){
			 var flag=false; 
			 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/chart_of_account_deletelineentry.action", 
					 data:{id:lineId},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.formError').hide();  
						 $('.tempresult').html(result);   
						 if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
						    	else{	
							    	flag = false; 
							    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						    	} 
						     }  
					},
					error:function(result){
						$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 });
				 if(flag==true){ 
		        		 var childCount=Number($('#childCount').val());
		        		 if(childCount > 0){
							childCount=childCount-1;
							$('#childCount').val(childCount); 
		        		 } 
		        		 $(this).parent().parent('tr').remove(); 
		        		//To reset sequence 
		        		var i=0;
		     			$('.rowid').each(function(){  
							i=i+1;
							$($(this).children().get(5)).text(i); 
	   					 });  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }  
	 });
});
</script>