<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<tr style="background-color: #F8F8F8;"
	id="childRowId_${requestScope.rowid}">
	<td colspan="11" class="tdidentity"
		id="childRowTD_${requestScope.rowid}">
		<div id="errMsg"></div>
		<form name="periodform" id="credit_debit_validate"
			style="position: relative;">
			<input type="hidden" name="addEditFlag" id="addEditFlag"
				value="${requestScope.AddEditFlag}" />
			<div class="float-left width50" id="hrm">
				<fieldset style="min-height: 185px;">
					<div>
						<label class="width30">Account Number<span
							style="color: red">*</span> </label> <input type="text" tabindex="1"
							name="accountNumber" id="accountNumber"
							class="width50 validate[required]">
					</div>
					<div>
						<label class="width30">Card Type<span style="color: red">*</span>
						</label> <select tabindex="2" name="cardType" id="cardType"
							class="width51 validate[required]">
							<option value="">Select</option>
							<c:forEach var="cardType" items="${BANK_CARD_TYPES}">
								<option value="${cardType.key}">${cardType.value}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width30">Card Number<span style="color: red">*</span>
						</label> <input type="text" tabindex="3" name="cardNumber" id="cardNumber"
							class="width50 validate[required]">
					</div>
					<div>
						<label class="width30">Account Holder</label> <input type="text"
							tabindex="4" name="holderName" id="holderName" class="width50">
						<span class="button"> <a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all holder-popup width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
							type="hidden" id="accountHolderId" />
					</div>
					<div>
						<label class="width30">Issuing Entity<span
							style="color: red">*</span> </label> <select tabindex="5"
							name="issuingEntity" id="issuingEntity"
							class="width51 validate[required]">
							<option value="">Select</option>
							<c:forEach var="ISSUES" items="${CARD_ISSUING_ENTITIES}">
								<option value="${ISSUES.lookupDetailId}">${ISSUES.displayName}</option>
							</c:forEach>
						</select> <span class="button" style="position: relative;"> <a
							style="cursor: pointer;" id="CARD_ISSUING_ENTITIES"
							class="btn ui-state-default ui-corner-all issusing-entity-lookup width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div>
					<div>
						<label class="width30">Valid From</label> <input type="text"
							tabindex="6" name="validFrom" id="validFrom" class="width50">
					</div>
					<div>
						<label class="width30">Valid To</label> <input type="text"
							tabindex="7" name="validTo" id="validTo" class="width50">
					</div>
				</fieldset>
			</div>
			<div class="width48 float-left" id="hrm">
				<fieldset style="min-height: 201px;">
					<div>
						<label class="width30">Limit</label> <input type="text"
							tabindex="8" name="limit" id="limit" class="width50">
					</div>
					<div>
						<label class="width30">CSC</label> <input type="text" tabindex="9"
							name="cscNumber" id="cscNumber" class="width50">
					</div>
					<div>
						<label class="width30">PIN</label> <input type="text"
							tabindex="10" name="pinNumber" id="pinNumber" class="width50">
					</div>
					<div>
						<label class="width30">Purpose</label> <input type="text"
							tabindex="11" name="accountPurpose" id="accountPurpose"
							class="width50">
					</div>
					<div>
						<label class="width30">Contact Number</label> <input type="text"
							tabindex="12" name="branchContact" id="branchContact"
							class="width50 validate[optional[custom[onlyNumber]]]">
					</div>
					<div>
						<label class="width30">Contact Person</label> <input type="text"
							tabindex="13" name="contactPerson" id="contactPerson"
							class="width50">
					</div>
					<div>
						<label class="width30">Description</label> <input type="text"
							tabindex="14" name="description" id="description" class="width50">
					</div>
				</fieldset>
				<div id="othererror" class="response-msg error ui-corner-all"
					style="width: 60%; display: none;"></div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
				style="margin: 20px;">
				<div
					class="portlet-header ui-widget-header float-right cancel_account"
					id="cancel_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right update_account"
					id="update_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="holder-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="holder-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</td>
</tr>
<script type="text/javascript"> 
 var accessCode = "";
 $(function(){   
	 $('#validFrom,#validTo').datepick(); 
		  $('.cancel_account').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
					$("#AddImage_"+rowid).show();
				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
				 	$("#EditImage_"+rowid).show();
				}
				 $("#childRowId_"+rowid).remove();	
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 $('#holder-popup').dialog('destroy');		
			  	 $('#holder-popup').remove(); 
				 $('#openFlag').val(0);
			    
		  });

		  $('.discard-lookup').live('click',function(){ 
			  $('#holder-popup').dialog('close');
			  return false;
		  });

		  $('.issusing-entity-lookup').click(function(){ 
		        $('.ui-dialog-titlebar').remove(); 
		        accessCode = $(this).attr("id");  
		          $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.holder-result').html(result); 
		                  $('#holder-popup').dialog('open');
		                  return false;
		             },
		             error:function(result){
		                  $('.holder-result').html(result);
		             }
		         });
		          return false;
		 	});	


		//Lookup Data Roload call
		 	$('#save-lookup').live('click',function(){  
				if(accessCode=="CARD_ISSUING_ENTITIES"){
					$('#issuingEntity').html("");
					$('#issuingEntity').append("<option value=''>Select</option>");
					loadLookupList("issuingEntity"); 
				}  
			}); 
			
		 	 $jquery("#credit_debit_validate").validationEngine('attach'); 
		 	 
		  $('.update_account').click(function(){ 
			 //Find the Id for fetch the value from Id
			 var tempvar=$('.tdidentity').attr('id');
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var accountNumber = $('#accountNumber').val();
			 var bankAccountId=Number($('#bankAccountId_'+rowid).val()); 
			 var cardNumber=$('#cardNumber').val();
			 var cardType=Number($('#cardType').val());
			 var accountPurpose=$('#accountPurpose').val();
			 var issuingEntity=Number($('#issuingEntity').val());
			 var validFrom=$('#validFrom').val();
			 var validTo=$("#validTo").val();
			 var branchContact=$('#branchContact').val();
			 var contactPerson=$('#contactPerson').val();  
			 var limit=$("#limit").val();
			 var holderId = $('#accountHolderId').val(); 
			 var cscNumber = $('#cscNumber').val();
			 var pinNumber = $('#pinNumber').val();
			 var description = $('#description').val(); 
			 var lineId=$('#lineId_'+rowid).val();
			 var cardTypeName = $('#cardType option:selected').text();
			 var addEditFlag=$('#addEditFlag').val(); 
			 if($jquery("#credit_debit_validate").validationEngine('validate')){   
		        var flag=false;
		        $.ajax({
						type:"POST", 
						url:"<%=request.getContextPath()%>/bank_creditdebit_save.action",
					 	async: false, 
					 	data:{
					 		bankAccountId: bankAccountId, cardNumber: cardNumber, cardType: cardType, accountPurpose: accountPurpose,
					 		issuingEntity: issuingEntity, validFrom: validFrom, validTo: validTo, contactPerson: contactPerson, id: lineId,
					 		cardLimit: limit, csvNumber: cscNumber, addEditFlag: addEditFlag, holderId: holderId, accountNumber: accountNumber,
					 		pinNumber: pinNumber, description: description, cardTypeName: cardTypeName
					 		},
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('#holder-popup').dialog('destroy');		
					  		$('#holder-popup').remove(); 
							$('.tempresult').html(result); 
						     if(result!=null) {
                                  flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                          $("#othererror").html($('#returnMsg').html()); 
                          return false;
					 	} 
		        }); 
		        if(flag==true){ 
	        		//Bussiness parameter
        			$('#accountNumber_'+rowid).text(accountNumber); 
        			$('#cardType_'+rowid).text($('#cardType option:selected').text()); 
        			$('#cardNumber_'+rowid).text(cardNumber); 
        			$('#purpose_'+rowid).text(accountPurpose); 
        			$('#accountHolder_'+rowid).text($('#holderName').val()); 
        			$('#issuingEntity_'+rowid).text($('#issuingEntity option:selected').text());
        			$('#validFrom_'+rowid).text(validFrom);
        			$('#validTo_'+rowid).text(validTo);
        			$('#limit_'+rowid).text(limit);
        			$('#csc_'+rowid).val(cscNumber); 
        			$('#pin_'+rowid).val(pinNumber);
        			$('#holderidval_'+rowid).val(holderId); 
        			$('#issuingid_'+rowid).val(issuingEntity);  
        			$('#typeidval_'+rowid).val(cardType);
        			$('#description_'+rowid).val(description); 
        			$('#branchContact_'+rowid).val(branchContact); 
        			$('#contactPerson_'+rowid).val(contactPerson);
        			 
					//Button(action) hide & show 
    				 $("#AddImage_"+rowid).hide();
    				 $("#EditImage_"+rowid).show();
    				 $("#DeleteImage_"+rowid).show();
    				 $("#WorkingImage_"+rowid).hide(); 
        			 $("#childRowId_"+rowid).remove();	 

				  	var childCount=Number($('#childCount').val());
					childCount=childCount+1;
					$('#childCount').val(childCount); 

					 triggerAddRow(rowid);
        		}		  	 
		      } 
		      else{return false;}
		  });

		  $('#person-list-close').live('click',function(){
				 $('#holder-popup').dialog('close');
			 });

		  $('.holder-popup').click(function(){ 
		       tempid=$(this).parent().get(0);   
		       $('.ui-dialog-titlebar').remove(); 
			 	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/common_person_list.action",
										async : false,
										data : {
											personTypes : "ALL"
										},
										dataType : "html",
										cache : false,
										success : function(result) {
											$('.holder-result').html(result);
											$('#holder-popup').dialog('open');
											$(
													$(
															$('#holder-popup')
																	.parent())
															.get(0)).css('top',
													0);
										},
										error : function(result) {
											$('.holder-result').html(result);
										}
									});
						});
		$('#holder-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			modal : true
		});
	});
	function personPopupResult(personid, personname, commonParam) {
		$('#accountHolderId').val(personid);
		$('#holderName').val(personname);
		$('#holder-popup').dialog("close");
	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
