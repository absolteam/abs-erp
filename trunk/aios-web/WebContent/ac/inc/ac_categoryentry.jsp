<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accountTypeId=0;
$(function(){
	$jquery("#category_details").validationEngine('attach');
	 
	$('#categoryName').focus(); 
	$('#status').val($('#tempstatusId').val());
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	}
	$('#fromDate,#toDate').datepick({
	 onSelect: customRange, showTrigger: '#calImg'}); 
	 
     $('.categorydiscard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getall_categorylist.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
	 });

     $('.categorysave').click(function(){ 
		 var categoryId=Number($('#categoryId').val());
		 var name=$('#categoryName').val();
		 var fromDate=$('#fromDate').val();
		 var toDate=$('#toDate').val();
		 var status=$('#status').val(); 
		 if($jquery("#category_details").validationEngine('validate')){
 			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/savecategory.action", 
				 	async: false,
				 	data:{	categoryId:categoryId, name:name, fromDate:fromDate, toDate:toDate, status:status},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$(".tempresult").html(result); 
						var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/getall_categorylist.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$("#main-wrapper").html(result); 
										if(categoryId > 0)
											$('#success_message').hide().html("Record updated.").slideDown(1000);
										else
											$('#success_message').hide().html("Record created.").slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Sorry!Internal error.").slideDown(1000);
					}
			 });
		 }
		 else{
			 return false;}
	 });
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth){
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		if($('#fromDate').val()!=""){ 
 	 		addPeriods(); 
 	 	}
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}
function addPeriods(){
	var date = new Date($('#fromDate').datepick('getDate')[0].getTime());  
	$.datepick.add(date, parseInt(364, 10), 'd');
	$('#toDate').val($.datepick.formatDate(date)); 
}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>category</div> 
		  <form name="category_details" id="category_details" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="height:60px;"> 
							<div>
								<label class="width30" for="fromDate">From Date<span style="color: red;">*</span></label> 
								<c:choose>
									<c:when test="${CATEGORY_INFO.fromDate ne null && CATEGORY_INFO.fromDate ne ''}">
										<c:set var="fromdate" value="${CATEGORY_INFO.fromDate}"/>  
										<%String date=DateFormat.convertDateToString(pageContext.getAttribute("fromdate").toString());%>
										<input type="text" id="fromDate" readonly="readonly" class="width50 validate[required]" value="<%=date%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" id="fromDate" readonly="readonly" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div>
								<label class="width30" for="toDate">To Date<span style="color: red;">*</span></label> 
								<c:choose>
									<c:when test="${CATEGORY_INFO.toDate ne null && CATEGORY_INFO.toDate ne ''}">
										<c:set var="todate" value="${CATEGORY_INFO.toDate}"/>  
										<%String todates=DateFormat.convertDateToString(pageContext.getAttribute("todate").toString());%>
										<input type="text" id="toDate" readonly="readonly" class="width50 validate[required]" value="<%=todates%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" id="toDate" readonly="readonly" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose>  
							</div>  
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset> 
							<div>
								<label class="width30" for="categoryName">Category Name<span style="color: red;">*</span></label> 
								<input type="hidden" id="categoryId" name="categoryId" value="${CATEGORY_INFO.categoryId}"/>
								<input type="text" name="categoryName" id="categoryName" maxlength="30" class="width50 validate[required]" value="${CATEGORY_INFO.name}"/>
							</div> 
							<div>
								<label class="width30" for="noOfDays">Status<span style="color: red;">*</span></label> 
								<select name="status" id="status" class="width51 validate[required]">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
								<input type="hidden" id="tempstatusId" name="tempstatusId" value="${CATEGORY_INFO.status}"/>
							</div>  
						</fieldset>
					</div> 
			</div>  
		</div>    
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right categorydiscard" id="categorydiscard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right categorysave" id="categorysave" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
  </form>
  </div> 
</div>