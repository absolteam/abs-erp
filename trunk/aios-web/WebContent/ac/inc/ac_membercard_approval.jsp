<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
$(function(){
 	  
	 $('input, select').attr('disabled', true); 
	 
	 { 
		  $.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_member_card_entry.action", 
		     	async: false, 
		     	data:{couponId: Number($('#memberCardId').val())},
				dataType: "json",
				cache: false,
				success: function(response){ 
					 $('#cardType').val(response.memberCardVO.cardType);
					 $('#startDigit').val(response.memberCardVO.startDigit);
					 $('#endDigit').val(response.memberCardVO.endDigit);
					 $('#currentDigit').val(null!=response.memberCardVO.currentDigit ? response.memberCardVO.currentDigit :
						 response.memberCardVO.startDigit);
					 $('#statusPoints').val(response.memberCardVO.statusPoints);
 					 $('#description').val(response.memberCardVO.description);
				} 		
			}); 
		  return false;
	  } 
	});
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>member
			card
		</div>
		<div class="portlet-content"> 
		 	<input type="hidden" id="memberCardId" value="${requestScope.memberCardId}"/>
			<div class="clearfix"></div>
			<form id="member_card_validation" style="position: relative;">
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 100px;">
								<div>
									<label for="status">Status Points<span
										style="color: red;">*</span> </label> <input type="text"
										name="statusPoints" id="statusPoints" tabindex="5"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="6" id="description"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label for="cardType">Card Type<span
										style="color: red;">*</span> </label> <select name="cardType"
										id="cardType" class="width51 validate[required]" tabindex="1">
										<option value="">Select</option>
										<c:forEach var="cardType" items="${CARD_TYPES}">
											<option value="${cardType.key}">${cardType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label for="startDigit">Start Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="startDigit" id="startDigit" tabindex="2"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="endDigit">End Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="endDigit" id="endDigit" tabindex="3"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="currentDigit">Current Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="currentDigit" id="currentDigit" tabindex="4"
										class="width50 validate[required,custom[onlyNumber]]"
										readonly="readonly" />
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div> 
				</div>
			</form> 
		</div>
	</div>
</div>
<div class="clearfix"></div> 