<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
select.width51 {
	width: 51%;
}
</style>
<script type="text/javascript">
var idname="";
var slidetab="";
var popval=""; 
$(function(){   
	 
	$('input, select').attr('disabled', true);
});

 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>check out
		</div> 
		<form name="check_outs" id="check_outs" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetCheckOutId" name="assetCheckOutId"
					value="${CHECK_OUT.assetCheckOutId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 120px;">  
							<div>
								<label class="width30" for="checkOutTo">CheckOut By<span
									class="mandatory">*</span></label> 
									<c:choose>
										<c:when test="${CHECK_OUT.personByCheckOutBy ne null && CHECK_OUT.personByCheckOutBy ne ''}">
											<input type="text" readonly="readonly" id="checkByPersonName" class="width50 validate[required]"
												value="${CHECK_OUT.personByCheckOutBy.firstName} ${CHECK_OUT.personByCheckOutBy.lastName}"/>
											<input type="hidden" readonly="readonly" id="checkOutby" value="${CHECK_OUT.personByCheckOutBy.personId}"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" id="checkByPersonName" class="width50 validate[required]"
												value="${requestScope.personName}"/>
											<input type="hidden" readonly="readonly" id="checkOutby" value="${requestScope.checkOutBy}"/>
										</c:otherwise>
									</c:choose>  
 								 
							</div>
							<div>
								<label class="width30" for="checkOutTo">CheckOut To<span
									class="mandatory">*</span></label> 
									<input type="text" readonly="readonly" id="checkToPersonName" class="width50 validate[required]"
										value="${CHECK_OUT.personByCheckOutTo.firstName} ${CHECK_OUT.personByCheckOutTo.lastName}"/>
									<input type="hidden" readonly="readonly" id="checkOutTo" value="${CHECK_OUT.personByCheckOutTo.personId}"/>
 									 
							</div> 
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${CHECK_OUT.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30" for="checkOutNumber">CheckOut Number<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${CHECK_OUT.assetCheckOutId ne null && CHECK_OUT.assetCheckOutId ne ''}">
										<input type="text" readonly="readonly" name="checkOutNumber"
											id="checkOutNumber" class="width50 validate[required]"
											value="${CHECK_OUT.checkOutNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="checkOutNumber"
											id="checkOutNumber" class="width50 validate[required]"
											value="${requestScope.checkOutNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetName">Asset Name<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" id="assetCreationName"
										value="${CHECK_OUT.assetCreation.assetName}" class="width50 validate[required]"/>
									<input type="hidden" id="assetCreationId" value="${CHECK_OUT.assetCreation.assetCreationId}"/>
									 
							</div>
							<div style="display: none;">
								<label class="width30" for="checkOutDue">CheckOut Due<span
									class="mandatory">*</span> </label> <select name="checkOutDue"
									id="checkOutDue" class="width51">
									<option value="">Select</option>
									<c:forEach var="checkOutDue" items="${CHECK_OUT_DUE}">
										<option value="${checkOutDue.key}">${checkOutDue.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempCheckOutDue"
									value="${CHECK_OUT.checkOutDue}" />
							</div>
							<div>
								<label class="width30" for="checkOutDate">CheckOut Date<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" id="checkOutDate" 
									 value="${CHECK_OUT.outDate}" class="width50 validate[required]"/>
							</div>  
							<div>
								<label class="width30" for="location">Location<span
									class="mandatory">*</span></label> 
									<c:choose>
										<c:when test="${CHECK_OUT.assetCheckOutId ne null && CHECK_OUT.assetCheckOutId ne '' && CHECK_OUT.assetCheckOutId gt 0}">
											<input type="text" readonly="readonly" id="cmpDeptLocationName" class="width50 validate[required]"
												value="${CHECK_OUT.cmpDeptLocation.company.companyName} &gt;&gt; ${CHECK_OUT.cmpDeptLocation.department.departmentName} &gt;&gt; ${CHECK_OUT.cmpDeptLocation.location.locationName}" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" id="cmpDeptLocationName" class="width50 validate[required]"/>
										</c:otherwise>
									</c:choose> 
									<input type="hidden" readonly="readonly" id="cmpDeptLocationId" value="${CHECK_OUT.cmpDeptLocation.cmpDeptLocId}"/>
 									 
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>  
		</form>
	</div>
</div>
<div class="clearfix"></div>