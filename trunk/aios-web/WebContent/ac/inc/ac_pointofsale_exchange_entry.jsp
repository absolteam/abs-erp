<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
	var slidetab = "";
	var tempid = "";
	var exchangeDetails = [];
	$(function() {

		$('.pos-exchange-discard').click(function(){  
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_merchandise_exchange.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result); 
				} 
			});  
		}); 
		
		$('#pos-exchange-save').click(function(){  
  			additionalCharges = getPOSExchangeDetails(); 
  			if(additionalCharges.length > 0){
 				$.ajax({
 					type:"POST",
 					url:"<%=request.getContextPath()%>/save_merchandise_exchange.action",
											async : false,
											dataType : "json",
											data : {
 												exchangeDetails : JSON
														.stringify(exchangeDetails)
											},
											cache : false,
											success : function(response) {
												if (response.returnMessage == "SUCCESS")
													$('.pos-exchange-discard')
															.trigger('click');
											}
										});
							} else {
								$('#page-error').hide().html(
										"Please enter return details")
										.slideDown();
								$('#page-error').delay(2000).slideUp();
							}
						});
	});
	function getPOSExchangeDetails() {
		exchangeDetails = [];
		$('.rowid').each(
				function() {
					var rowid = getRowId($(this).attr('id'));
					var returnQuantity = Number($('#returnQuantity_' + rowid)
							.val());
					var pointOfSaleDetailId = Number($(
							'#pointOfSaleDetailId_' + rowid).val());
					if (returnQuantity > 0 && pointOfSaleDetailId > 0) {
						exchangeDetails.push({
							"returnQuantity" : returnQuantity,
							"pointOfSaleDetailId" : pointOfSaleDetailId
						});
					}
				});
		return exchangeDetails;
	}

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
</script>
<div class="portlet-content">
	<div id="page-error" class="response-msg error ui-corner-all width90"
		style="display: none;"></div>
	<div class="tempresult" style="display: none;"></div>
	<div class="width100 float-left" id="hrm">
		<div class="float-right width48 pos-right-content">
			<fieldset>
				<div>
					<label class="width30"> POS Reference</label> <span>${POINT_OF_SALE.referenceNumber}
					</span>  
				</div>
				<div>
					<label class="width30"> Sales Date </label> <span>
						${POINT_OF_SALE.date} </span>
				</div>
				<c:if test="${POINT_OF_SALE.customerId gt 0}">
					<div>
						<label class="width30"> Customer </label> <span>
							${POINT_OF_SALE.customerName} </span>
					</div>
				</c:if>
			</fieldset>
		</div>
		<div class="float-left width48 pos-right-content">
			<fieldset>
				<div>
					<label class="width30"> Reference No</label> <span>${requestScope.salesRefNumber}
					</span>  
				</div>
				<div>
					<label class="width30"> Exchange Date </label> <span>
						${requestScope.salesDate} </span>
				</div> 
			</fieldset>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="portlet-content width100" id="hrm"
		style="margin-top: 10px;">
		<div id="hrm" class="hastable width100">
			<table id="hastab" class="width100">
				<thead>
					<tr>
						<th style="width: 8%;">Product</th>
						<th style="width: 3%;">Quantity</th>
						<th style="width: 3%;">Price</th>
						<th style="width: 3%;">Discount</th>
						<th style="width: 3%;">Total</th>
						<th style="width: 3%;">Returns</th>
					</tr>
				</thead>
				<tbody class="tab">
					<c:forEach var="salesInvoice"
						items="${POINT_OF_SALE.pointOfSaleDetailVOs}" varStatus="status">
						<tr class="rowid" id="fieldrow_${status.index+1}">
							<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
							<td>${salesInvoice.productName}</td>
							<td id="productQty_${status.index+1}" class="productQty">
								${salesInvoice.quantity}</td>
							<td id="amount_${status.index+1}" class="amount right-align">${salesInvoice.productUnitPrice}</td>
							<td class="right-align">${salesInvoice.discountValue}</td>
							<td class="right-align" id="totalAmount_${status.index+1}">${salesInvoice.totalPrice}</td>
							<td><input type="text" name="returnQuantity"
								id="returnQuantity_${status.index+1}"
								class="returnQuantity width98" /> <input type="hidden"
								name="pointOfSaleDetailId"
								id="pointOfSaleDetailId_${status.index+1}"
								value="${salesInvoice.pointOfSaleDetailId}" />
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="margin: 10px;">
		<div
			class="portlet-header ui-widget-header float-right pos-exchange-discard"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.discard" />
		</div>
		<div
			class="portlet-header ui-widget-header float-right pos-exchange-save"
			id="pos-exchange-save" style="cursor: pointer;">
			<fmt:message key="accounts.common.button.save" />
		</div>
	</div>
</div>
