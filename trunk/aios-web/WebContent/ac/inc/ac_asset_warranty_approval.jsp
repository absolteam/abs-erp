<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$(function(){ 
	 
	$('input,select').attr('disabled', true);
	   
			if(Number($('#assetWarrantyId').val())> 0){
				$('#warrantyType').val($('#tempwarrantyType').val());
			}
}); 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			warranty
		</div> 
		<form id="asset_warranty_validation" style="position: relative;">
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 120px;">
								<div>
									<label for="assetName">Asset<span style="color: red;">*</span>
									</label> <input type="text" name="assetCreationName" value="${ASSET_WARRANTY.assetName}"
										id="assetCreationName" tabindex="5"
										class="width50 validate[required]" readonly="readonly" /> <input value="${ASSET_WARRANTY.assetId}"
										type="hidden" name="assetCreationId" id="assetCreationId" />
										<input value="${ASSET_WARRANTY.assetWarrantyId}"
										type="hidden" name="assetWarrantyId" id="assetWarrantyId" /> 
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="6" id="description">${ASSET_WARRANTY.description}</textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset style="min-height: 120px;">
								<div>
									<label for="warrantyNumber">Warranty Number<span
										style="color: red;">*</span> </label> <input type="text" value="${ASSET_WARRANTY.warrantyNumber}"
										name="warrantyNumber" id="warrantyNumber" tabindex="1"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="warrantyType">Warranty Type<span
										style="color: red;">*</span> </label> <select name="warrantyType"
										id="warrantyType" class="width51 validate[required]"
										tabindex="2">
										<option value="">Select</option>
										<c:forEach var="warrantyType" items="${WARRANTY_TYPE}">
											<option value="${warrantyType.lookupDetailId}">${warrantyType.displayName}</option>
										</c:forEach>
									</select>
									<input type="hidden" id="tempwarrantyType" value="${ASSET_WARRANTY.warrantyTypeId}"/> 
								</div>
								<div>
									<label for="fromDate">From Date<span
										style="color: red;">*</span> </label> <input type="text" value="${ASSET_WARRANTY.fromDate}"
										name="fromDate" id="fromDate" tabindex="3"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="endDate">End Date<span style="color: red;">*</span>
									</label> <input type="text" name="endDate" id="endDate" tabindex="4" value="${ASSET_WARRANTY.toDate}"
										class="width50 validate[required]" readonly="readonly" />
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div> 
				</div>
			</form> 
		</div></div>
<div class="clearfix"></div>