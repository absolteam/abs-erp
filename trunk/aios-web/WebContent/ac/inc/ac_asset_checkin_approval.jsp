<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
select.width51 {
	width: 51%;
}
</style>
<script type="text/javascript">
var idname="";
var slidetab="";
var popval=""; 
$(function(){  
	$('input,select').attr('disabled', true);
});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>check in
		</div> 
		<form name="check_ins" id="check_ins" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetCheckInId" name="assetCheckInId"
					value="${CHECK_IN.assetCheckInId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 120px;">  
							<div>
								<label class="width30" for="checkOutTo">CheckOut by<span
									class="mandatory">*</span></label> 
									<c:choose>
										<c:when test="${CHECK_IN.person ne null && CHECK_IN.person ne ''}">
											<input type="text" readonly="readonly" id="checkByPersonName" class="width50 validate[required]"
												value="${CHECK_IN.person.firstName} ${CHECK_IN.person.lastName}"/>
											<input type="hidden" readonly="readonly" id="checkOutby" value="${CHECK_IN.person.personId}"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" id="checkByPersonName" class="width50 validate[required]"
												value="${requestScope.personName}"/>
											<input type="hidden" readonly="readonly" id="checkOutby" value="${requestScope.checkOutBy}"/>
										</c:otherwise>
									</c:choose>   
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${CHECK_IN.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30" for="checkInNumber">CheckIn Number<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${CHECK_IN.assetCheckInId ne null && CHECK_IN.assetCheckInId ne ''}">
										<input type="text" readonly="readonly" name="checkInNumber"
											id="checkInNumber" class="width50 validate[required]"
											value="${CHECK_IN.checkInNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="checkInNumber"
											id="checkInNumber" class="width50 validate[required]"
											value="${requestScope.checkInNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetName">Asset Name<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" id="assetName"
										value="${CHECK_IN.assetCheckOut.assetCreation.assetName}" class="width50 validate[required]"/>
									<input type="hidden" id="assetCheckOutId" value="${CHECK_IN.assetCheckOut.assetCheckOutId}"/> 
							</div> 
							<div>
								<label class="width30" for="checkOutTo">CheckIn<span
									class="mandatory">*</span></label> 
									<input type="text" readonly="readonly" id="checkToPersonName" class="width50 validate[required]"
										value="${CHECK_IN.assetCheckOut.personByCheckOutTo.firstName} ${CHECK_IN.assetCheckOut.personByCheckOutTo.lastName}" />
									<input type="hidden" readonly="readonly" id="checkOutTo" value="${CHECK_IN.assetCheckOut.personByCheckOutTo.personId}"/> 
							</div>
							<div>
								<label class="width30" for="checkInDate">CheckIn Date<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" id="checkInDate" 
									 value="${CHECK_IN.inDate}" class="width50 validate[required]"/>
							</div>  
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>  
		</form>
	</div>
</div>
<div class="clearfix"></div> 