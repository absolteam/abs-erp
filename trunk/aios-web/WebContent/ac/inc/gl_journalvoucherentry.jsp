<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td {
	width: 6%;
}

.hastable tr td {
	padding: 5px !important;
}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var addedVouchers = 0;
var transactionDetail="";
$(function(){  
	
	if(Number($('#alertId').val()) > 0){ 
		$('.rowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var combinationValue = $('#codeCombination_'+rowId).val();
			combinationValue=combinationValue.replace(/[\s\n\r]+/g, ' ' ).trim(); 
			$('#codeCombination_'+rowId).val(combinationValue); 
		});
		$('.codecombination-popup').remove();
		$('.delrow').remove();
		
		callDebitCreditTotal(); 
	} else {
		manupulateLastRow();
	}  

	$jquery("#addMasterJournalValidation").validationEngine('attach');
	
	$jquery(".debitAMOUNT,.linesAMOUNT,.debitTotalH,.creditTotalH").number( true, 3 );
	
	$('#journalDate').datepick({
		onSelect: customRange, showTrigger: '#calImg'}); 
	 var tempvar=$('.tab>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#DeleteImage_'+rowid).hide();  
	
	if($('#tempcategoryId').val()!=null && $('#tempcategoryId').val()!=""){ 
		$('.rowid').each(function(){
			var idvals=getRowId($(this).attr('id'));
			if(Number($('#journalid_'+idvals).val()>0)){
				var combinationValue = $('#codeCombination_'+idvals).val();
				combinationValue=$.trim(combinationValue.replace(/[\s\n\r]+/g, ' ' )); 
				$('#codeCombination_'+idvals).val(combinationValue); 
			} 
		}); 
		
		$('.codeComb').each(function(){
			var combinationDesc = $(this).val();
			combinationDesc=combinationDesc.replace(/[\s\n\r]+/g, ' ' ).trim(); 
			$(this).val(combinationDesc);
			
		});
		$('#periodId').val('${JOURNAL_INFO.period.periodId}');
		$('.debitTotal').text($('.debitTotalH').val());
		$('.creditTotal').text($('.creditTotalH').val());
	} 
		
	$('.save').click(function(){  
		var id=$('.save').attr('id'); 
		var journalDate=$('#journalDate').val();
		var periodId=$('#periodId').val();
		var currencyId=$('#currencyId').val();
		var decription=$('#decription').val();
		var journalId=Number($('#journalId').val());
		var categoryId=Number($('#categoryId').val());
		var journalNumber=$('#journalNumber').text().trim(); 
		var debitAmount=0;var creditAmount=0;var dlinesamount=0;var clinesamount=0;
		var totalAmount=callJournalTotal(); 
		var alertId = Number($('#alertId').val());
		transactionDetail=""; 
		 if(id=="add"){
			url_action="journal_voucher_save";
		 }
		 else{
			url_action="journal_voucher_update";
		 }
		 $('.rowid').each(function(){   
			 var idvals=getRowId($(this).attr('id')); 
			 if(Number($jquery('#debitAmount_'+idvals).val())>0){ 
				dlinesamount = $jquery('#debitAmount_'+idvals).val();  
				debitAmount = Number(Number(debitAmount)+Number(dlinesamount)); 
				debitAmount = Number(debitAmount).toFixed(3);
			 } 
			 if(Number($jquery('#linesAmount_'+idvals).val())>0){
				clinesamount = $jquery('#linesAmount_'+idvals).val(); 
				creditAmount = Number(Number(creditAmount)+Number(clinesamount)); 
				creditAmount = Number(creditAmount).toFixed(3);
			 } 
		}); 
		transactionDetail=getTransactionDetails();  
		if($jquery("#addMasterJournalValidation").validationEngine('validate')){
 			if(transactionDetail!=null && transactionDetail!=""){
				if(debitAmount==creditAmount){
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/"+url_action+".action", 
						 	async: false,
						 	data:{ journalDate: journalDate, periodId: periodId, currencyId: currencyId, description: decription, 
							 	   transactionDetail: transactionDetail, journalId: journalId,
							 	   alertId: alertId, totalAmount: totalAmount, categoryId: categoryId, journalNumber: journalNumber
							 	 },
						    dataType: "html",
						    cache: false,
							success:function(result){
								 $(".tempresult").html(result);
								 var message=$('#returnMsg').html(); 
								 if(message.trim()=="Success"){
									 $.ajax({
											type:"POST",
											url:"<%=request.getContextPath()%>/journal_voucher_retrive.action", 
										 	async: false,
										    dataType: "html",
										    cache: false,
											success:function(result){
												$('#codecombination-popup').dialog('destroy');		
							  					$('#codecombination-popup').remove(); 
												$("#main-wrapper").html(result); 
												if(journalId > 0)
													$('#success_message').hide().html("Record updated.").slideDown(1000);
												else
													$('#success_message').hide().html("Record created.").slideDown(1000);
											}
									 });
								 }
								 else{
									 $('#journal-error').hide().html(message).slideDown(1000);
									 return false;
								 }
							}
						 });
					}
					else{
						$('#journal-error').hide().html("Debit side & Credit side should be equal.").slideDown(1000); 
						return false;
					}
			}else{
				$('#journal-error').hide().html("Please enter transaction details").slideDown(1000); 
				return false;
			} 
		 }
		 else{
			 return false;
		 }
		 return false;
	 });

	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/journalvoucher_discard.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
			 		$("#main-wrapper").html(result); 
			 		$('#codecombination-popup').dialog('destroy');		
  					$('#codecombination-popup').remove(); 
				}
			});
			return false;
	 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/journal_voucher_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
	   	 callDebitCreditTotal(); 
		 return false; 
	});

	 var getTransactionDetails = function(){
			var transactionDetails = "";
			var combinationArray=new Array();
			var transactionFlagArray=new Array();
			var amountArray=new Array();
			var lineDescriptionArray=new Array(); 
			var transactionLineIdArray=new Array();  
			$('.rowid').each(function(){ 
				var rowId=getRowId($(this).attr('id'));  
				var codeCombinationID=$('#combinationId_'+rowId).val();
				var transactionFLAG=Number(1);
				linesAmount = $jquery('#linesAmount_'+rowId).val();
				if(Number($jquery('#debitAmount_'+rowId).val())>0 ){
					linesAmount = $jquery('#debitAmount_'+rowId).val();
				}else{
					transactionFLAG  =Number(0);
					linesAmount = $jquery('#linesAmount_'+rowId).val();
				} 
				var linesDescription=$('#linesDescription_'+rowId).val(); 
				var transactionDetailId =  Number($('#transactionDetailId_'+rowId).val());
				if(typeof codeCombinationID != 'undefined' && codeCombinationID!=null 
						&& transactionFLAG>=0 && linesAmount!="" && linesAmount>0) {
					combinationArray.push(codeCombinationID);
					transactionFlagArray.push(transactionFLAG);
					amountArray.push(linesAmount); 
					transactionLineIdArray.push(transactionDetailId);
					if(linesDescription!=null && linesDescription!="")
						lineDescriptionArray.push(linesDescription);
					else
						lineDescriptionArray.push("##");   
				} 
			});
			for(var j=0;j<combinationArray.length;j++){  
				transactionDetails+=combinationArray[j]+"__"+transactionFlagArray[j]+"__"+amountArray[j]+"__"+transactionLineIdArray[j]+"__"+lineDescriptionArray[j];
				if(j==combinationArray.length-1){   
				} 
				else{
					transactionDetails+="#@";
				}
			}  
			return transactionDetails;
		 };

     $('.codecombination_info').live('click',function(){
    	 var codecombinationdesc="";
    	 var tempvar=$(this).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 codecombinationdesc= $('#codecombination_desc_'+rowid).text().replace(/[\s\n\r]+/g, ' '); 
		 if(codecombinationdesc!=null && codecombinationdesc!='')
		 	$('#codecombinationbox').hide().html(codecombinationdesc).slideDown(800);
		 else
			 $('#codecombinationbox').hide();
		 return false;
      });

   //Combination pop-up config
 	$('.codecombination-popup-transaction').live('click',function(){ 
 	    tempid=$(this).parent().get(0);  
 	    $('.ui-dialog-titlebar').remove();   
 	 	$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/combination_treeview.action", 
 		 	async: false, 
 		    dataType: "html",
 		    cache: false,
 			success:function(result){   
 				 $('.codecombination-result').html(result);  
 			},
 			error:function(result){ 
 				 $('.codecombination-result').html(result); 
 			}
 		});  
 		return false;
 	});
 	
 	 $('#codecombination-popup').dialog({
 			autoOpen: false,
 			minwidth: 'auto',
 			width:800, 
 			bgiframe: false,
 			modal: true
 	 });

 	 $('.codeComb').live('change',function(){  
 	 	 triggerAddRow(getRowId($(this).attr('id')));
 	 });

 	 $('.debitAMOUNT').live('change',function(){
 		callDebitCreditTotal(); 
 		$('#linesAmount_'+getRowId($(this).attr('id'))).val("");
 	 	 triggerAddRow(getRowId($(this).attr('id')));
 	 });

 	 $('.linesAMOUNT').live('change',function(){  
 		 callDebitCreditTotal(); 
 		$('#debitAmount_'+getRowId($(this).attr('id'))).val("");
 	 	 triggerAddRow(getRowId($(this).attr('id')));
 	 });

 	if(Number($('#tempcurrencyId').val()) == 0){ 
 		$('#currencyId').val($('#defaultcurrencyId').val()); 
	}else{
		$('#categoryId').val($('#tempcategoryId').val());
		$('#currencyId').val($('#tempcurrencyId').val()); 
	}
});
function callJournalTotal(){ 
	var totalAmount=0.0;
	var linesAmount=0.0;
	$('.rowid').each(function(){  
		var idvals=getRowId($(this).attr('id'));
		if($('#debitAmount_'+idvals).val().trim()>0){
			linesAmount=$('#debitAmount_'+idvals).val(); 
			linesAmount=linesAmount.split(',').join(''); 
			totalAmount=Number(Number(totalAmount)+Number(linesAmount)); 
		}  
	});  
	return totalAmount;
} 
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function customRange(field, rules, i, options){
	$('#p_info_flag').hide(); 
	$('#periodId').html('');
	$('#periodId')
	.append(
			'<option value="">Select</option>'); 
	if($('#journalDate').val()!="" && $('#journalDate').val()!=null){ 
		$('.journalDateformError').remove();  
		var periodDate=$('#journalDate').val();
		 $.ajax({
			 type : "POST",
			 url:"<%=request.getContextPath()%>/show_selected_dateperiod.action",
						data : {
							periodDate : periodDate
						},
						async : false,
						dataType : "json",
						cache : false,
						success : function(response) { 
							if (response.returnMessage != null
									&& response.returnMessage != '') { 
								$('#journalDate').removeClass('validate[required]').addClass('validate[required,custom[period]]');
 								$jquery("#journalDate").validationEngine('validate');
 							} else {
								$('#journalDate').removeClass('validate[required,custom[period]]').addClass('validate[required]');
								
								$(response.aaData)
										.each(
												function(index) {
													$('#periodId')
															.append(
																	'<option value='
												+ response.aaData[index].periodId
												+ '>'
																			+ response.aaData[index].name
																			+ '</option>'); 
														$('#periodId').val(response.aaData[index].periodId);
												});
 							}
						}
					});
		}
		return false;
	}
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function setCombination(combinationTreeId, combinationTree) {
		var idVals = $(tempid).attr('id').split("_");
		var rowids = Number(idVals[1]);
		$('#combinationId_' + rowids).val(combinationTreeId);
		$('#codeCombination_' + rowids).val(combinationTree);
		$('#codeCombination_' + rowids).val().replace(/[\s\n\r]+/g, ' ').trim();
		triggerAddRow(rowids);
	}
	function triggerAddRow(rowId) {
		var codeCombination = $('#codeCombination_' + rowId).val();
		var linesAmount = $('#linesAmount_' + rowId).val();
		var debitAmount = $('#debitAmount_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (codeCombination != null && codeCombination != ""
				&& ((linesAmount != null && linesAmount != "") 
						|| (debitAmount!=null && debitAmount!=""))
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
			callDebitCreditTotal();
		}
	}
	function callDebitCreditTotal() {
		var debitTotal = Number(0);
		$('.rowid').each(
				function() {
					var ids = getRowId($(this).attr('id')); 
					if (Number($jquery('#debitAmount_' + ids).val()) > 0) {
						var tempdebitTotal = $jquery('#debitAmount_' + ids).val();
						debitTotal = Number(debitTotal)
								+ Number(tempdebitTotal);
					}
				});
		$jquery('.debitTotalH').val(debitTotal);
 		$('.debitTotal').text($('.debitTotalH').val());
		var creditTotal = Number(0);
		$('.rowid').each(
				function() {
					var ids = getRowId($(this).attr('id'));
					if (Number($jquery('#linesAmount_' + ids).val()) > 0) {
						var tempCreditTotal = $jquery('#linesAmount_' + ids).val();
						creditTotal = Number(creditTotal)
								+ Number(tempCreditTotal);
					}
				});
		$jquery('.creditTotalH').val(creditTotal);
		$('.creditTotal').text($('.creditTotalH').val());
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when
			test="${COMMENT_INFO ne null && COMMENT_INFO ne ''
							&& COMMENT_INFO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From :<span>
							${COMMENT_INFO.name}</span>
					</label> <label class="width70">${COMMENT_INFO.comment}</label>
				</fieldset>
			</div>
		</c:when>
	</c:choose>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.journalvoucher" />
		</div>  
		<form name="addMasterJournal" id="addMasterJournalValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="journal-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="journalId" name="journalId"
					value="${JOURNAL_INFO.transactionId}" />
				<input type="hidden" id="alertId" name="alertId"
					value="${alertId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 85px;">
							<div>
								<label class="width20"><fmt:message
										key="accounts.jv.label.jvcurrency" /><span style="color: red;">*</span> </label> <select
									name="currencyId" class="width51 validate[required]"
									style="margin-bottom: 5px;" id="currencyId">
									<option value="">Select</option>
									<c:choose>
										<c:when test="${CURRENCY_LIST ne null && CURRENCY_LIST ne ''}">
											<c:forEach items="${CURRENCY_LIST}" var="BEANS"
												varStatus="status">
												<option value="${BEANS.currencyId}">${BEANS.currencyPool.code}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="tempcurrencyId" name="tempcurrencyId"
									value="${JOURNAL_INFO.currency.currencyId}" /> <input
									type="hidden" id="defaultcurrencyId" name="defaultcurrencyId"
									value="${DEFAULT_CURRENCY}" />
							</div>
							<div>
								<label class="width20"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="decription" name="decription" class="width51">${JOURNAL_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 85px;">
							<c:if
								test="${JOURNAL_INFO.journalNumber ne null && JOURNAL_INFO.journalNumber ne ''}">
								<div style="padding-bottom: 7px;">
									<label class="width20"> <fmt:message
											key="accounts.jv.label.vouchernumber" /><span
										style="color: red;">*</span> </label> <span
										style="font-weight: normal;" id="journalNumber">${JOURNAL_INFO.journalNumber}</span>
								</div>
							</c:if>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> <fmt:message
										key="accounts.jv.label.jvgldate" /><span style="color: red;">*</span>
								</label>
								<c:choose>
									<c:when
										test="${JOURNAL_INFO.transactionTime ne null && JOURNAL_INFO.transactionTime ne ''}">
										<c:set var="journalDate"
											value="${JOURNAL_INFO.transactionTime}" />
										<input name="journalDate" type="text" readonly="readonly"
											id="journalDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("journalDate").toString())%>"
											class="journalDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="journalDate" type="text" readonly="readonly"
											id="journalDate" 
											class="journalDate validate[required] width50">
									</c:otherwise>
								</c:choose>
								<span
									style="display: none; font-size: 10px; position: absolute; margin-top: 10px;"
									id="p_info_flag"></span>
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.period" /><span style="color: red;">*</span>
								</label> <select name="periodId" id="periodId"
									class="validate[required] width51">
									<option value="">Select</option>
									<c:forEach var="period" items="${PERIOD_LIST}">
										<option value="${period.periodId}">${period.name}</option>
									</c:forEach>
								</select> 
 							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.category" /><span style="color: red;">*</span>
								</label> <select name="category" id="categoryId"
									class="validate[required] width51">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${CATEGORY_INFO ne null &&  CATEGORY_INFO ne ''}">
											<c:forEach var="category" items="${CATEGORY_INFO}">
												<option value="${category.categoryId}">${category.name}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" name="tempcategory" id="tempcategoryId"
									value="${JOURNAL_INFO.category.categoryId}" />
							</div>
						</fieldset>
					</div>

				</div>

				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							<fmt:message key="accounts.jv.label.transactiondetail" /><span
										style="color: red;">*</span>
						</legend>
						<div id="page-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="warning_message"
							class="response-msg notice ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<input type="hidden" readonly="readonly" name="trnValue"
								id="trnValue" /> <input type="hidden" name="childCount"
								id="childCount" value="${JOURNAL_LINE_SIZE}" />
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 1%"><fmt:message
												key="accounts.jv.label.jvlinno" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.jv.label.jvcc" /></th>
										<th style="width: 2%">Debit</th>
										<th style="width: 2%">Credit</th>
										<th style="width: 5%"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">

									<c:choose>
										<c:when
											test="${requestScope.JOURNAL_LINE_INFO ne null && requestScope.JOURNAL_LINE_INFO ne ''}">
											<c:forEach var="bean" items="${JOURNAL_LINE_INFO}"
												varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}">${status.index+1}</td>
													<td class="codecombination_info"
														id="codecombination_${status.index+1}"
														style="cursor: pointer;"><input type="hidden"
														name="combinationId_${status.index+1}"
														value="${bean.combination.combinationId}"
														id="combinationId_${status.index+1}" /> <input
														type="text" name="codeCombination_${status.index+1}"
														readonly="readonly" id="codeCombination_${status.index+1}"
														class="codeComb width80"
														value="${bean.combination.accountByNaturalAccountId.code} 
											<c:if test="${bean.combination.accountByAnalysisAccountId ne null && bean.combination.accountByAnalysisAccountId ne ''}">
												.${bean.combination.accountByAnalysisAccountId.code}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer1AccountId ne null && bean.combination.accountByBuffer1AccountId ne ''}">
												.${bean.combination.accountByBuffer1AccountId.code}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer2AccountId ne null && bean.combination.accountByBuffer2AccountId ne ''}">
												.${bean.combination.accountByBuffer2AccountId.code}
											</c:if>  
											( ${bean.combination.accountByNaturalAccountId.account} 
											<c:if test="${bean.combination.accountByAnalysisAccountId ne null && bean.combination.accountByAnalysisAccountId ne ''}">
												.${bean.combination.accountByBuffer1AccountId.account}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer1AccountId ne null && bean.combination.accountByBuffer1AccountId ne ''}">
												.${bean.combination.accountByBuffer1AccountId.account}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer2AccountId ne null && bean.combination.accountByBuffer2AccountId ne ''}">
												.${bean.combination.accountByBuffer2AccountId.account}
											</c:if> )">
														<span class="button" id="codeID_${status.index+1}">
															<a style="cursor: pointer;"
															class="btn ui-state-default ui-corner-all codecombination-popup-transaction width100">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
													</td>
													<td id="transactionflag_${status.index+1}"> 
														<c:choose>
															<c:when test="${bean.isDebit eq true}">
																<input type="text"
																	name="debitAmount" id="debitAmount_${status.index+1}"
																	style="text-align: right;" value="${bean.amount}"
																	class="debitAMOUNT width97 right-align validate[optional,custom[number]]">
															</c:when>
															<c:otherwise>
																<input type="text"
																	name="debitAmount" id="debitAmount_${status.index+1}"
																	style="text-align: right;"
																	class="debitAMOUNT width97 right-align validate[optional,custom[number]]">
															</c:otherwise>
														</c:choose>
													</td>
													<td id="amount_${status.index+1}" style="">
													<c:choose>
														<c:when test="${bean.isDebit ne true}">
															<input
																type="text" name="linesAmount"
																id="linesAmount_${status.index+1}" value="${bean.amount}"
																style="text-align: right;"
																class="linesAMOUNT width97 right-align validate[optional,custom[number]]">
														</c:when>
														<c:otherwise>
															<input
																type="text" name="linesAmount"
																id="linesAmount_${status.index+1}"
																style="text-align: right;"
																class="linesAMOUNT width97 right-align validate[optional,custom[number]]">
														</c:otherwise>
													</c:choose>
													</td>
													<td id="linedecription_${status.index+1}"><input
														type="text" name="linesDescription"
														id="linesDescription_${status.index+1}"
														value="${bean.description}" class="width98"
														maxlength="150">
													</td>
													<td style="display: none"><input type="hidden"
														name="transactionDetailId" value="${bean.transactionDetailId}"
														id="transactionDetailId_${status.index+1}" />
													</td>
													<td style="width: 1%;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status.index+1}"
														style="cursor: pointer; display: none;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status.index+1}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}"
														style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a>
													</td>
													<td style="display: none;" class="codecombination_desc"
														id="codecombination_desc_${status.index+1}">
														${bean.combination.accountByNaturalAccountId.account} <c:if
															test="${bean.combination.accountByAnalysisAccountId ne null && bean.combination.accountByAnalysisAccountId ne ''}">
												.${bean.combination.accountByAnalysisAccountId.account}
											</c:if> <c:if
															test="${bean.combination.accountByBuffer1AccountId ne null && bean.combination.accountByBuffer1AccountId ne ''}">
												.${bean.combination.accountByBuffer1AccountId.account}
											</c:if> <c:if
															test="${bean.combination.accountByBuffer2AccountId ne null && bean.combination.accountByBuffer2AccountId ne ''}">
												.${bean.combination.accountByBuffer2AccountId.account}
											</c:if>
													</td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose>
									<c:if
										test="${requestScope.alertId eq null ||  requestScope.alertId eq '' || requestScope.alertId le 0}">
										<c:forEach var="i" begin="${requestScope.JOURNAL_LINE_SIZE+1}"
											end="${requestScope.JOURNAL_LINE_SIZE+2}" step="1"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${i}">
												<td id="lineId_${i}">${i}</td>
												<td class="codecombination_info" id="codecombination_${i}"
													style="cursor: pointer;"><input type="hidden"
													name="combinationId_${i}" id="combinationId_${i}" /> <input
													type="text" name="codeCombination_${i}" readonly="readonly"
													id="codeCombination_${i}" class="codeComb width80">
													<span class="button" id="codeID_${i}"> <a
														style="cursor: pointer;"
														class="btn ui-state-default ui-corner-all codecombination-popup-transaction width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td id="transactionflag_${i}"> 
												<input type="text"
													name="debitAmount" id="debitAmount_${i}"
													style="text-align: right;"
													class="debitAMOUNT width97 right-align validate[optional,custom[number]]">
												</td>
												<td id="amount_${i}" style=""><input type="text"
													name="linesAmount" id="linesAmount_${i}"
													style="text-align: right;"
													class="linesAMOUNT width97 right-align validate[optional,custom[number]]">
												</td>
												<td id="linedecription_${i}"><input type="text"
													name="linesDescription" id="linesDescription_${i}"
													class="width98" maxlength="150">
												</td>
												<td style="display: none"><input type="hidden"
														name="transactionDetailId" 
														id="transactionDetailId_${i}" />
												</td>
												<td style="width: 1%;" class="opn_td" id="option_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}" style="cursor: pointer; display: none;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}" style="display: none; cursor: pointer;"
													title="Edit Record"> <span
														class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}"
													style="cursor: pointer; display: none;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> 
												</td>
												<td style="display: none;" id="codecombination_desc_${i}"></td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
						<div class="clearfix"></div>
						<table class="width50 float-right" id="hrm">
							<tr>
								<td>
									<div>
										<span class="width10 float-left">Debit</span> <span
											class="debitTotal width16 right-align"></span> 
											<input style="display: none;" readonly="readonly" type="text" 
												class="debitTotalH" value="${requestScope.totalAmount}"/>
									</div></td>
							</tr>
							<tr>
								<td>
									<div>
										<span class="width10 float-left">Credit</span> <span
											class="creditTotal width16 right-align"></span>
											<input style="display: none;" readonly="readonly" type="text" 
												class="creditTotalH" value="${requestScope.totalAmount}"/>
									</div></td>
								<td style="display: none;"><span
									class="convertedDebitTotal width16 right-align"
									style="display: block; float: left; width: 12%;"></span> <span
									class="convertedCreditTotal width16 right-align"
									style="display: block; float: left; width: 12%;"></span>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				

				<div class="clearfix"></div>
				<div id="codecombinationbox" style="display: none;"
					class="float-left"></div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="${showPage}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>