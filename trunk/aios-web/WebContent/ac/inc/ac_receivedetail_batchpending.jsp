<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowidpo" id="fieldrowpo_${requestScope.rowId}">
	<td style="display: none;" id="lineIdpo_${requestScope.rowId}">${requestScope.rowId}</td>
	<td id="productcode_${requestScope.rowId}">${RECEIVE_DETAILPENDING.product.code}</td>
	<td id="productname_${requestScope.rowId}">
		${RECEIVE_DETAILPENDING.product.productName}</td>
	<td style="display: none;" id="uom_${requestScope.rowId}"></td>
	<td id="purchaseqty_${requestScope.rowId}">${RECEIVE_DETAILPENDING.purchaseQty}</td>
	<td></td>
	<td></td>
	<td> 
		<select name="grnpackageType" id="grnpackageType_${requestScope.rowId}" class="grnpackageType"> 
			<option value="-1">Select</option>
			<c:forEach var="packageType" items="${RECEIVE_DETAILPENDING.productPackageVOs}">
				<c:choose>
					<c:when test="${packageType.productPackageId gt 0}">
						<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
						 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
						 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
						 	</c:forEach> 
						 </optgroup>
					</c:when> 
				</c:choose> 
			</c:forEach>
		</select>  
		<input type="hidden" name="tempgrnpackageType" id="tempgrnpackageType_${requestScope.rowId}" value="${RECEIVE_DETAILPENDING.packageDetailId}"/>
	</td>
	<td><input type="text" value="${RECEIVE_DETAILPENDING.quantity}"
		class="width90 grnPackageUnit validate[optional,custom[number]]" name="grnPackageUnit"
		id="grnPackageUnit_${requestScope.rowId}" />  
	</td>
	<td><input type="hidden"
		class="width90 receiveQtybatch validate[optional,custom[number]]"
		value="${RECEIVE_DETAILPENDING.quantity}" name="receiveQty"
		id="receiveQty_${requestScope.rowId}" /> <input type="hidden"
		value="${RECEIVE_DETAILPENDING.quantity}" name="receiveHiddenQty"
		id="receiveHiddenQty_${requestScope.rowId}" />
		<span id="grnbaseUnitConversion_${requestScope.rowId}" class="width15" 
			style="display: none;">${RECEIVE_DETAILPENDING.baseUnitName}</span>
		<span id="baseDisplayQty_${requestScope.rowId}">${RECEIVE_DETAILPENDING.baseQuantity}</span>
		<c:choose>
			<c:when test="${RECEIVE_DETAILPENDING.baseQuantity ne null && RECEIVE_DETAILPENDING.baseQuantity ne ''}">
				<input type="hidden" value="${RECEIVE_DETAILPENDING.baseQuantity}" name="receiveMaxQty"
					id="receiveMaxQty_${requestScope.rowId}" />
			</c:when>
			<c:otherwise>
				<input type="hidden" value="${RECEIVE_DETAILPENDING.quantity}" name="receiveMaxQty"
					id="receiveMaxQty_${requestScope.rowId}" />
			</c:otherwise>
		</c:choose> 
	</td>
	<td><input type="text" style="text-align: right;"
		class="width90 unitRate validate[optional,custom[number]]"
		value="${RECEIVE_DETAILPENDING.unitRate}" name="unitRate"
		id="unitRate_${requestScope.rowId}" />
		<input type="text" style="display: none;" class="totalAmount" id="totalAmount_${requestScope.rowId}"
				value="${RECEIVE_DETAILPENDING.quantity * RECEIVE_DETAILPENDING.unitRate}" name="totalAmount"/>
	</td>
	<td><input type="text" class="width96 batchNumber"
		id="batchNumber_${requestScope.rowId}" maxlength="40" /></td>
	<td><input type="text" readonly="readonly"
		class="width96 expiryBatchDate"
		id="expiryBatchDate_${requestScope.rowId}" /></td>
	<td id="total_${requestScope.rowId}" style="text-align: right;"></td>
	<td style="display: none"><input type="hidden"
		name="receivelineid" id="receivelineid_${requestScope.rowId}" /> <input
		type="hidden" name="purchasePOId"
		id="purchasePOId_${requestScope.rowId}"
		value="${requestScope.purchaseId}" /> <input type="hidden"
		name="productid" id="productid_${requestScope.rowId}"
		value="${RECEIVE_DETAILPENDING.product.productId}" /> <input
		type="hidden" name="purchaseLineId"
		id="purchaseLineId_${requestScope.rowId}"
		value="${RECEIVE_DETAILPENDING.purchaseDetailId}" /> </td>
	<td><input type="hidden" name="storeDetail"
		value="${RECEIVE_DETAILPENDING.shelfId}"
		id="storeDetail_${requestScope.rowId}" /> <span
		id="rackname_${requestScope.rowId}">${RECEIVE_DETAILPENDING.storeName}</span>
		<span class="button float-right"> <a style="cursor: pointer;"
			id="${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all store-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td class="returnDiv" style="display: none;"><span
		id="returnQty1_${requestScope.rowId}">${RECEIVE_DETAILPENDING.returnQty}</span>
		<input type="hidden" name="returnQty"
		id="returnQty_${requestScope.rowId}" class="returnQty"
		value="${RECEIVE_DETAILPENDING.returnQty}" />
	</td>
	<td><input type="text" name="linedescription"
		id="linedescription_${requestScope.rowId}"
		value="${RECEIVE_DETAILPENDING.description}" /> 
	</td>
	<td style="width: 0.01%;" class="opn_td"
		id="optionpo_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_batchpo"
		id="DeleteCImage_${requestScope.rowId}" style="cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a>
	</td>
</tr>
<script type="text/javascript">
	$(function() { 
		$('.expiryBatchDate').datepick(); 
		$jquery(".unitRate,.totalAmount").number(true,3); 
		$('#total_${requestScope.rowId}').text($('#totalAmount_${requestScope.rowId}').val());
		$('#grnpackageType_${requestScope.rowId}').val($('#tempgrnpackageType_${requestScope.rowId}').val());
	});
</script>