<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fieldset>
		<legend>
			<fmt:message key="accounts.discount.discountdetails" />
		</legend>
		<div class="width100 float-left">
			<label><fmt:message key="accounts.quotation.totalAmount" /></label>
			<c:set var="totalPay" value="${totalAmount}"/>
			<span style="text-align: right;"><%=AIOSCommons.formatAmount(pageContext.getAttribute("totalPay")) %></span>
		</div>
		<div class="width100 float-left">
			<label><fmt:message key="accounts.discount.discountreceived" /></label>
			<c:set var="discountReceives" value="${discountReceived}"/>
			<span style="text-align: right;"><%=AIOSCommons.formatAmount(pageContext.getAttribute("discountReceives")) %></span>
		</div> 
		<div class="width100 float-left">
			<label><fmt:message key="accounts.discount.netamount" /></label>
			<c:set var="netAmounts" value="${netAmount}"/>
			<span style="text-align: right;"><%=AIOSCommons.formatAmount(pageContext.getAttribute("netAmounts")) %></span>
		</div>
</fieldset>