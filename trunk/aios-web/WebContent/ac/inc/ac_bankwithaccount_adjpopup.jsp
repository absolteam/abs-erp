<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type="text/javascript">
var currentId=""; var tempvar=""; var idarray=""; var rowid="";
$(function(){ 
	$('#DOMWindow').css('z-index',999);
	$('#DOMWindowOverlay').css('z-index',999);
	$($($('#common-popup-adjpopup').parent()).get(0)).css('width',500);
	$($($('#common-popup-adjpopup').parent()).get(0)).css('height',250);
	$($($('#common-popup-adjpopup').parent()).get(0)).css('padding',0);
	$($($('#common-popup-adjpopup').parent()).get(0)).css('left',0);
	$($($('#common-popup-adjpopup').parent()).get(0)).css('top',100);
	$($($('#common-popup-adjpopup').parent()).get(0)).css('overflow','hidden');  
	$('#common-popup-adjpopup').dialog('open'); 
	
	$("#tree_bank_adjpopup").treeview({
		collapsed: true,
		animated: "medium",
		control:"#sidetreecontrol",
		persist: "location"
	}); 
	$('.accountadjid').live('click',function(){ 
		$('.accountadjid').each(function(){  
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			 if($('#bankaccountadjselect_'+rowid).hasClass('selected')){ 
				 $('#bankaccountadjselect_'+rowid).removeClass('selected');
			 }
			 currentId=0;
		});
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#bankaccountadjselect_'+rowid).addClass('selected');
		return false;
	});
	
	$('.accountadjid').live('dblclick',function(){  
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#accountadjNumber').val($('#accountadjnumber_'+rowid).html());
		$('#accountIdadj').val($('#accountadjid_'+rowid).html());   
		currentId=$(this).parent().parent().get(0);
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#bankaccountadjselect_'+rowid).removeClass('selected');
		$('#common-popup-adjpopup').dialog('close');  
		return false;
	});
	
	$('.close-bkacc-adjpopup').click(function(){
		 $('#common-popup-adjpopup').dialog('close'); 
	});
});
</script> 
<style>
.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>
<div class="portlet-content">  
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Bank Account Details
</div>
<ul id="tree_bank_adjpopup">
	<li><strong>Banks</strong></li> 
	<c:choose>
		<c:when test="${requestScope.BANK_DETAILS ne null && requestScope.BANK_DETAILS ne''}">
			<c:forEach items="${BANK_DETAILS}" var="bank" varStatus="status1"> 
				<li id="bank_${status1.index+1}">
					<span style="display: none;" id="bankadjid_${status1.index+1}">${bank.bankId}</span>
					<span id="bankadjname_${status1.index+1}" class="bankid">${bank.bankName} </span>
					<ul> 
						<c:forEach items="${BANK_ACCOUNT_DETAILS}" var="account" varStatus="status"> 
							<c:choose>
								<c:when test="${bank.bankId eq account.bank.bankId}"> 
									<span style="display: none;" id="accountadjid_${status.index+1}">${account.bankAccountId}</span>
									<span style="display: none;" id="accountadjcombinationid_${status.index+1}">${account.combination.combinationId}</span>
									<li id="accountadjnumber_${status.index+1}" class="accountadjid width30" style="cursor: pointer;">${account.accountNumber}</li> 
									<span id="bankaccountadjselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; position: relative ;right:600px; top:-24px; "></span>
								</c:when>
							</c:choose> 
						</c:forEach> 
					</ul>
				</li>	
			</c:forEach> 
		</c:when>
	</c:choose>		
</ul>  
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
	<div class="portlet-header ui-widget-header float-right close-bkacc-adjpopup" style="cursor:pointer;">close</div>  
</div>  
</div>   