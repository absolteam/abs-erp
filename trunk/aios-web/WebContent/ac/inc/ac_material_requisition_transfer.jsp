<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td id="lineId_${requestScope.rowId}" style="display: none;">${requestScope.rowId}</td>
	<td>${REQUISITION_SESSION.referenceNumber}</td>
	<td>${REQUISITION_SESSION.date}</td>
	<td>${REQUISITION_SESSION.totalRequisitionQty}</td>
	<td>${REQUISITION_SESSION.description}</td>
	<td style="display: none;"><input type="hidden"
		id="materialRequisitionId_${requestScope.rowId}"
		value="${REQUISITION_SESSION.materialRequisitionId}" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataRQ"
		id="EditImage_${requestScope.rowId}" style="cursor: pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowRQ"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a></td>
</tr>