<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:forEach var="REQUEST_DETAILS" items="${PAYMENT_REQUEST_DETAILS}" varStatus="status"> 
	<tr class="rowid" id="fieldrow_${status.index+1}"> 
		<td id="lineId_${status.index+1}">${status.index+1}</td> 
		<td>
			<input type="text" class="width98 invoiceNumber" id="invoiceNumber_${status.index+1}"/>
		</td>
		<td class="codecombination_info" id="codecombination_${status.index+1}" style="cursor: pointer;">
			<input type="hidden" name="combinationId_${status.index+1}" id="combinationId_${status.index+1}"/> 
			<input type="text" name="codeCombination_${status.index+1}" readonly="readonly" id="codeCombination_${status.index+1}" class="codeComb width80">
			<span class="button" id="codeID_${status.index+1}">
				<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
					<span class="ui-icon ui-icon-newwin"> 
					</span> 
				</a>
			</span>
		</td>
		<td>
			<input type="text" class="width98 amount validate[optional,custom[number]]" readonly="readonly" value="${REQUEST_DETAILS.amount}" id="amount_${status.index+1}" style="text-align: right;"/>
		</td> 
		<td>
		  <input type="text" name="linedescription" id="linedescription_${status.index+1}" value="${REQUEST_DETAILS.description}"/>
		</td>
		<td style="display:none;">
			<input type="hidden" id="directPaymentLineId_${status.index+1}"/>
			<input type="hidden" id="paymentRequestLineId_${status.index+1}" value="${REQUEST_DETAILS.paymentRequestDetailId}"/>
			<input
				type="hidden" id="useCase_${status.index+1}"/>
				<input
				type="hidden" id="record_${status.index+1}"/>
		</td> 
		 <td style="width:0.01%;" class="opn_td" id="option_${status.index+1}"> 
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
					<span class="ui-icon ui-icon-circle-close"></span>
			  </a> 
		</td>   
	</tr>
</c:forEach>
<tr class="lastrow" style="height:23px;">
	<c:forEach var="i" begin="0" end="5" varStatus="status"> 
		<td></td>
	</c:forEach>
</tr>
<script type="text/javascript">
$(function(){
	$jquery("#directPaymentCreation").validationEngine('attach'); 
});
</script>