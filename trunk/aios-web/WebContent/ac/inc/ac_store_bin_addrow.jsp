<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="binrowid" id="binfieldrow_${rowId}">
	<td style="display: none;" id="binlineId_${rowId}">${rowId}</td>
	<td><input type="text" class="width90 binSectionName"
		name="binSectionName" id="binSectionName_${rowId}" />
	</td>
	<td><select name="binSection"
		id="binSection_${rowId}" class="binSection"
		class="width90">
			<option value="">Select</option>
			<c:forEach var="binType" items="${BIN_TYPES}">
				<option value="${binType.key}">${binType.value}</option>
			</c:forEach>
	</select></td>
	<td><input type="text" class="width90 bindimension"
		style="border: 0px;" name="bindimension"
		id="bindimension_${rowId}" />
	</td>
	<td>
		<div class="portlet-header ui-widget-header float-right addRacks"
			id="addRack_${rowId}" style="cursor: pointer;"
			onclick="addRacks(${rowId});">Add Shelf</div>
	</td>
	<td style="width: 0.01%;" class="opn_td"
		id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip binaddData"
		id="BinAddImage_${rowId}"
		style="display: none; cursor: pointer;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bineditData"
		id="BinEditImage_${rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bindelrow"
		id="BinDeleteImage_${rowId}"
		style="display: none; cursor: pointer;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" name="shelfid" value="0"
		id="shelfid_${rowId}" /> <input type="hidden"
		name="confirmSave" value="false"
		id="confirmSave_${rowId}" />
		<input type="hidden" name="shelfaisleId" value="${requestScope.aisleId}" id="shelfaisleId_${rowId}" />
	</td>
</tr>