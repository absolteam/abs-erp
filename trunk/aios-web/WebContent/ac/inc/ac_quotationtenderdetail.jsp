<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:forEach var="bean" items="${TENDERS_DETAIL_LIST}" varStatus ="status">   
	<tr class="rowid" id="fieldrow_${status.index+1}"> 
		<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td>
		<td>
			<c:if test="${bean.product.itemType eq 'A'}">
				<c:out value="Asset"/>
			</c:if> 
			<c:if test="${bean.product.itemType eq 'E'}">
				<c:out value="Expense"/>
			</c:if> 
			<c:if test="${bean.product.itemType eq 'I'}">
				<c:out value="Inventory"/>
			</c:if> 
		</td>  
		<td class="product" id="product_${status.index+1}"> 
			${bean.product.productName}
			<input type="hidden" id="productid_${status.index+1}" style="border:0px;" value="${bean.product.productId}"/>
		</td> 
		<td id="uom_${status.index+1}" class="uom" style="display: none;"> 
			${bean.product.lookupDetailByProductUnit.displayName}
		</td> 
		<td>
			<input type="text" name="quantity" readonly="readonly" id="quantity_${status.index+1}" 
				class="width96 validate[required,custom[number]] quantity" value="${bean.quantity}"/> 
				<c:set var="totalUnit" value="0"/>
				<c:if test="${bean.unitRate ne null && bean.unitRate ne ''}">
					<c:set var="totalUnit" value="${bean.unitRate * bean.quantity}"/>
				</c:if> 
			<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" id="totalAmountH_${status.index+1}" 
				class="totalAmountH" value="${bean.unitRate}"/> 
		</td> 
		<td>
			<c:choose>
				<c:when test="${bean.unitRate ne null && bean.unitRate ne ''}">
					<input type="text" name="unitrate" id="unitrate_${status.index+1}" 
						class="width96 validate[required,custom[number]] unitrate" value="${bean.unitRate}"/>
				</c:when>
				<c:otherwise>
					<input type="text" name="unitrate" id="unitrate_${status.index+1}" 
						class="width96 validate[required,custom[number]] unitrate"/>
				</c:otherwise>
			</c:choose> 
		</td>  
		<td class="totalamount" id="totalamount_${status.index+1}" style="text-align: right;">
		</td> 
		<td style="display:none;">
			<input type="hidden" id="quotationLineId_${status.index+1}"/>
			<input type="hidden" id="requisitionLineId_${status.index+1}"/>
		</td>
		<td>
			 <input type="text" name="linedescription" id="linedescription_${status.index+1}"/>
		</td> 
		 <td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
			  </a>	
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
					<span class="ui-icon ui-icon-wrench"></span>
			  </a>  
		  	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
				  <span class="ui-icon ui-icon-circle-close"></span>
		 	  </a> 
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
				  <span class="processing"></span>
			 </a>
		</td>   
	</tr>
</c:forEach>
<script type="text/javascript">
$(function(){
	$jquery("#quotationCreation").validationEngine('attach'); 
	$jquery(".unitrate,.totalAmountH").number(true,3); 
	$('.rowid').each(function(){
		var rowId = getRowId($(this).attr('id'));
		$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val()); 
	});  
});
</script>