<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div
	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<div class="mainhead portlet-header ui-widget-header">
		<span style="display: none;"
			class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Persisted
		Orders
	</div>
	<c:if
		test="${CONSOLIDATED_SESSION_LIST ne null && fn:length(CONSOLIDATED_SESSION_LIST) > 0}">
		<fieldset
			style="max-height: 150px; overflow-x: hidden; overflow-y: auto;">
			<div class="width100">
				<table class="width100" border="0">
					<c:set var="savedOrderRow" value="0" />
					<c:forEach begin="0" step="3" items="${CONSOLIDATED_SESSION_LIST}">
						<tr style="border: 0px;">
							<!-- TD 1 -->
							<td style="border: 0px; padding: 3px 6px 3px 3px !important;">
								<div style="padding: 4px; background: #9FF781;"
									class="width100 float-left">
									<span style="margin: 0 5px;" class="float-left count_index">${savedOrderRow+1}
										.</span> <span class="width80 float-left" style="cursor: pointer;"
										onclick="javascript:openSavedOrder('${CONSOLIDATED_SESSION_LIST[savedOrderRow+0].pointOfSaleId}')">${CONSOLIDATED_SESSION_LIST[savedOrderRow+0].deliveryOptionName}</span>
									<span style="cursor: pointer;"
										onclick="javascript:deleteSavedOrder('${CONSOLIDATED_SESSION_LIST[savedOrderRow+0].pointOfSaleId}')">
										<img width="10" height="10" src="./images/cancel.png"> </span>
								</div></td>
							<!-- TD 2 -->
							<td style="border: 0px; padding: 3px 6px 3px 3px !important;">
								<c:if
									test="${CONSOLIDATED_SESSION_LIST[savedOrderRow+1] ne null}">
									<div style="padding: 4px; background: #81F7D8;"
										class="width100 float-left">
										<span style="margin: 0 5px;" class="float-left count_index">${savedOrderRow+2}
											.</span> <span class="width80 float-left" style="cursor: pointer;"
											onclick="javascript:openSavedOrder('${CONSOLIDATED_SESSION_LIST[savedOrderRow+1].pointOfSaleId}')">${CONSOLIDATED_SESSION_LIST[savedOrderRow+1].deliveryOptionName}</span>
										<span style="cursor: pointer;"
											onclick="javascript:deleteSavedOrder('${CONSOLIDATED_SESSION_LIST[savedOrderRow+1].pointOfSaleId}')">
											<img width="10" height="10" src="./images/cancel.png">
										</span>
									</div>
								</c:if></td>
							<!-- TD 3 -->
							<td style="border: 0px; padding: 3px 6px 3px 3px !important;">
								<c:if
									test="${CONSOLIDATED_SESSION_LIST[savedOrderRow+2] ne null}">
									<div style="padding: 4px; background: #9FF781;"
										class="width100 float-left">
										<span style="margin: 0 5px;" class="float-left count_index">${savedOrderRow+3}
											.</span> <span class="width80 float-left" style="cursor: pointer;"
											onclick="javascript:openSavedOrder('${CONSOLIDATED_SESSION_LIST[savedOrderRow+2].pointOfSaleId}')">${CONSOLIDATED_SESSION_LIST[savedOrderRow+2].deliveryOptionName}</span>
										<span style="cursor: pointer;"
											onclick="javascript:deleteSavedOrder('${CONSOLIDATED_SESSION_LIST[savedOrderRow+2].pointOfSaleId}')">
											<img width="10" height="10" src="./images/cancel.png">
										</span>
									</div>
								</c:if></td>
							<c:set var="savedOrderRow" value="${savedOrderRow + 3}" />
						</tr>
					</c:forEach>
				</table>
			</div>
		</fieldset>
	</c:if>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
	<div class="portlet-header ui-widget-header float-right pos-discard"
			id="pos-session-discard"
			style="background: #DF0101; color: #fff; padding: 10px;">Close</div>
		<div class="portlet-header ui-widget-header float-right pos-discard"
			id="show-dispatch-orders"
			style="background:  #faac58; color: #fff; padding: 10px;">Show Delivery Orders</div>
	</div>
</div>