<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
input[disabled='disabled'] {
	background-color: #ffffff;
}
</style>
<script type="text/javascript"> 
var startpick="";
 var slidetab="";
 var actualID="";
 var addedCurrency = 0;
$(function() {

	$('.formError').remove();
	 var tempvar=$('.tab>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#DeleteImage_'+rowid).hide(); 
	 
 	$('.addData').live('click', function(){
		slidetab=$(this).parent().parent().get(0);   
		$.ajax({
			type:"POST",
			 url:"<%=request.getContextPath()%>/gl_currency_add_add.action", 
			 data:{manipulationFlag:"A"},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $(result).insertAfter(slidetab);
				 var rowid=$(slidetab).attr('id'); 
				 var idarray=rowid.split('_');
				 var idval=idarray[1];  
				 $('#pageId').val(idval);
				 $($($(slidetab).children().get(4)).children().get(0)).hide();
			     $($($(slidetab).children().get(4)).children().get(1)).hide();
			     $($($(slidetab).children().get(4)).children().get(2)).hide(); 
			     $($($(slidetab).children().get(4)).children().get(3)).show(); 
			}
		});
		return false;
	});
	
 	$('.editData').live('click',function(){ 
		slidetab=$(this).parent().parent().get(0);  
		var curId=$($($($($(slidetab).children()).get(3)).children()).get(1)).val(); 
		$.ajax({
			type:"POST",
			 url:"<%=request.getContextPath()%>/gl_currency_add_add.action",  
			data:{manipulationFlag:"E"},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $(result).insertAfter(slidetab); 
				 $('#selCode').val(curId);  
				 if($($(slidetab).children().get(2)).text()=="YES"){ 
					 $('#curEnable').attr('checked','checked');	
				 }  
				 else{
					 $('#curEnable').removeAttr('checked');	
				 } 
				 
				 if($($(slidetab).children().get(1)).text()=="YES"){ 
					 $('#defaultCurrency').attr('checked','checked');	
				 }  
				 else{
					 $('#defaultCurrency').removeAttr('checked');		
				 } 
				 var rowid=$(slidetab).attr('id');  
				 var idarray=rowid.split('_');
				 var idval=idarray[1];  
				 $('#pageId').val(idval);
				 $($($(slidetab).children().get(4)).children().get(0)).hide();
			     $($($(slidetab).children().get(4)).children().get(1)).hide();
			     $($($(slidetab).children().get(4)).children().get(2)).hide(); 
			     $($($(slidetab).children().get(4)).children().get(3)).show(); 
			     return false;
			}
		});
		return false;
	});

     $('.delrow').live('click',function(){ 
    	slidetab=$(this).parent().parent().get(0); 
    	var tempId= $($($(slidetab).children().get(3)).children().get(2)).val(); 
     	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/gl_currency_add_delete.action", 
		 	data:{manipulationFlag:"D",id:tempId},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('.tempresult').html(result); 
				var i=0;
	 			$('.rowid').each(function(){  
					i=i+1;
					$($($(this).children().get(3)).children().get(2)).val(i);
				});
	   			$(this).parent().parent('tr').remove();
	   			return false;
			},
			error:function(result){
				$("#temperror").html($('#returnMsg').html());
		    	$("#temperror").hide().slideDown(); 
			} 
    	}); 
	 	return false;
 	});	

 	$('.cancelrec').click(function(){ 
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/currency_master_code.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
		 		$("#main-wrapper").html(result); 
			},
			error:function(result){
				$("#main-wrapper").html(result);
				 
			} 
    	});
 	});

 	$('.headerData').click(function(){
 		$("#temperror").hide();
 		var flag=false;
 		$('.rowid').each(function(){  
 			var field=$(this).attr('id');
 			var idarray = field.split('_');
			var rowid=Number(idarray[1]);
			var code=$('#curcode_'+rowid).html();
			if(code !=null && code!='' || code!=0){
				flag=true;
				return false;
			}
		});
 		
 		if(flag){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/gl_currency_final_save.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){  
			 		 $("#main-wrapper").html(result);  
				},
				error:function(result){
					 $("#main-wrapper").html(result);  
				} 
	   		}); 
 		}else{
 			$("#temperror").html("There is no information to save!");
	    	$("#temperror").hide().slideDown(); 
 		}
	});
	
	$('.tooltips').tooltip({
		track: true,
		delay: 0,
		showURL: false,
		showBody: " - ",
		fade: 250
	});
	 
	 if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
			$('#themeRollerSelect option:not(:selected)').remove();
		}
		
		$('.startPicker').click(function(){
			startpick=($(this).attr('class'));
			$($($($(this).parent().parent()).children().get(3)).children().get(0)).val("");
		});
		 
		$('.startPicker,.endPicker').datepick({
		onSelect: customRange, showTrigger: '#calImg'});

		//add rows manipulations
		$('.addrows').click(function(){
			var i=0;
		 	var lastRowId=$('.tab>tr:last').attr('id');
			if(lastRowId!=null){
			var idarray = lastRowId.split('_');
			var rowid=Number(idarray[1]);
			rowid=Number(rowid+1);
			}else{
				rowid=Number(1);
			}
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/gl_currency_addrow.action", 
				data:{id:rowid},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tab").append(result);
					//if($(".tab").height()<255)
						// $(".tab").animate({height:'+=20',maxHeight:'255'},0);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});

					//To reset sequence 
			    		var i=0;
			 			$('.rowid').each(function(){  
							i=i+1;
							$($($(this).children().get(3)).children().get(2)).val(i);
						});
				}
			});
			return false;
		});
});


	//Prevent selection of invalid dates through the select controls 
	function checkLinkedDays() { 
	    var daysInMonth = $.datepick.daysInMonth(
			$('#selectedYear').val(), $v('#selectedMonth').val());
	    $('#selectedDay option:gt(27)').attr('disabled', false);
	    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	    if ($('#selectedDay').val() > daysInMonth) { 
	        $('#selectedDay').val(daysInMonth); 
	    } 
	}
	function customRange(dates) {
		 
if (($(this).attr('class')) == startpick) {
	$('.endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$(startpick).datepick('option', 'maxDate', dates[0] || null);
}
}
 
 
</script>

<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<form name="newFields">
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;"
					class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Currency
			</div> 
			<div id="temperror" class="response-msg error ui-corner-all"
				style="width: 80%; display: none"></div>
			<div class="childCountErr response-msg error ui-corner-all"
				style="width: 80%; display: none;"></div>
			<div id="wrgNotice" class="response-msg notice ui-corner-all"
				style="width: 80%; display: none;">${requestScope.wrgMsg}</div>
			<div class="portlet-content">
				<div style="display: none;" class="tempresult"></div>
				<input type="hidden" name="trnValue" id="trnValue"
					readonly="readonly" /> <input type="hidden" name="childCount"
					id="childCount" value="0" />
				<div id="hrm">
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead class="chrome_tab">
								<tr>
									<th class="width10"><fmt:message
											key="accounts.currencymaster.label.code" />
									</th>
									<th style="width: 8%;">Default Currency</th>
									<th class="width10"><fmt:message
											key="accounts.currencymaster.label.enable" />
									</th>
									<th style="width: 5%;"><fmt:message
											key="accounts.currencymaster.label.options" />
									</th>
								</tr>
							</thead>
							<tbody class="tab" style="">
								<%for(int i=1;i<=2;i++) { %>
								<tr class="rowid" id="fieldRow_<%=i%>">
									<td class="width10" id="curcode_<%=i%>"></td>
									<td id="defaultcurrencies_<%=i%>" class="width10"></td>
									<td class="width10"></td>
									<td style="display: none"><input type="hidden"
										name="tempId" value="" id="tempId" /> <input type="hidden"
										name="curIdVal" value="" id="curIdVal" /> <input type="hidden"
										name="lineId" value="<%=i%>" /></td>
									<td style="width: 5%;"><a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
										style="cursor: pointer;" title="Add Record"> <span
											class="ui-icon ui-icon-plus"></span> </a> <a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
										style="display: none; cursor: pointer;" title="Edit Record">
											<span class="ui-icon ui-icon-wrench"></span> </a> <a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
										id="DeleteImage_<%=i%>" style="cursor: pointer;"
										title="Delete Record"> <span
											class="ui-icon ui-icon-circle-close"></span> </a> <a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
										style="display: none; cursor: pointer;" title="Working"> <span
											class="processing"></span> </a></td>
									<td style="display: none;"></td>
								</tr>
								<%}%>
							</tbody>
						</table>
						<div class="iDiv" style="display: none;"></div>
					</div>
					<div class="vGrip">
						<span></span>
					</div>
				</div>
			</div>
		</form>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div class="portlet-header ui-widget-header float-right cancelrec"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right headerData"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.save" />
			</div>

		</div>
		<div style="display: none;"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="portlet-header ui-widget-header float-left addrows"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.addrow" />
			</div>
		</div>
	</div>


</div>