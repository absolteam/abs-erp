<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowidpacks" id="fieldrowpacks_${ROW_ID}">
	<td id="packingLineId_${ROW_ID}" style="display: none;">${ROW_ID}</td>  
	<td>
		<input type="text" name="packReferenceNumber" id="referenceNumber_${ROW_ID}" class="packReferenceNumber width98"> 
	</td> 
	<td> 
		 <select name="packingType" id="packingType_${ROW_ID}" class="packingType width90">
			<option value="">Select</option>
				<c:forEach var="TYPE" items="${PRODUCT_UNIT}">
					<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
				</c:forEach>
		</select> 
	</td>  
	<td>
		<input type="text" name="noOfPacks" id="noOfPacks_${ROW_ID}" class="noOfPacks width98"> 
	</td>  
	<td>
		<input type="text" name="grossWeight" id="grossWeight_${ROW_ID}" class="grossWeight width98"> 
	</td>  
	<td>
		<input type="text" name="dispatcher" id="dispatcher_${ROW_ID}" class="dispatcher width98"> 
	</td> 
	<td>
		<input type="text" name="description" id="description_${ROW_ID}" class="description width98"> 
	</td>  
	 <td style="width:1%;" class="opn_td" id="optioncharges_${ROW_ID}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatapacks" 
		  	id="AddImagecharges_${ROW_ID}" style="cursor:pointer;display:none;" title="Add Record">
				<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatapacks"
		  	 id="EditImagecharges_${ROW_ID}" style="display:none; cursor:pointer;" title="Edit Record">
			 <span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowpacks" 
		  	id="DeleteImagecharges_${ROW_ID}" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" 
		  	id="WorkingImagepacks_${ROW_ID}" style="display:none;" title="Working">
			<span class="processing"></span>
		  </a>
		  <input type="hidden" name="salesDeliveryPackId" id="salesDeliveryPackId_${ROW_ID}"/>
	</td>  
</tr> 