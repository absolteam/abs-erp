<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.ui-autocomplete-input {
	min-height: 27px !important;
	width: 70%!important;
}
.combination-error{
	color: #d80036;
	font-size: 9px !important;
}
</style>
<script type="text/javascript">
var combinationId=0;
var segmentId=0; 
var tree;
$(function(){  
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	
	$jquery("#combination_treeview").fancytree({ 
 		extensions: ['contextMenu','filter'],  
 		quicksearch: true, 
 		icons: true,  
 		minExpandLevel: 1,
		debugLevel: 0,
		selectMode: 1,
 		source: 
			{url: "<%=request.getContextPath()%>/show_combination_tree.action"}, 
		        
  		contextMenu: {
 	        menu: { 
 	          'add': { 'name': 'Add', 'icon': 'add' },	
 	          'copy': { 'name': 'Copy', 'icon': 'copy' }, 
 	          'paste': { 'name': 'Paste', 'icon': 'paste' }, 
 	          'delete': { 'name': 'Delete', 'icon': 'delete' },
 	          'sep1': '---------',
 	          'quit': { 'name': 'Quit', 'icon': 'quit' },
 	          'sep2': '---------',
 	        },
 	        actions: function(node, action, options) { 
 	        	combinationId = node.key;
  	        	segmentId = getRowId(node.refKey);
  	        	$("#"+action).trigger('click');
 	        }
 		},
 		filter: {
 	        autoApply: true,
  	        fuzzy: false,  
 	        highlight: true, 
 	        mode: "hide" 
 	    },postProcess: function(event, data) {  
 	    	tree = $jquery("#combination_treeview").fancytree("getTree");
 	    	$('.ui-autocomplete-input').attr('name', 'search'); 
 	    	callRecursiveData(data.response); 
 	  	},
	});   
	
	$jquery("input[name=search]").live('keyup',function(e){ 
		if(Number($(this).val().length) > 4){
			var n,
	        opts = {
	          autoExpand: true,
	          leavesOnly: false
	        },
	        match = $(this).val();

	      if(e && e.which === $jquery.ui.keyCode.ESCAPE || $jquery.trim(match) === ""){
	        $("button#btnResetSearch").click();
	        return;
	      }
		        // Pass function to perform match
		  n = tree.filterNodes(match, opts); 
	      
	      $("button#btnResetSearch").attr("disabled", false);
	    //  $jquery("span#matches").text("(" + n + " matches)");
		} 
		if(e && e.which === $jquery.ui.keyCode.ESCAPE || $jquery.trim($(this).val()) === ""){
	        $("button#btnResetSearch").click();
	        return;
	    }
    }).focus(); 
 	
	
	$('#add').click(function(){  
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_combination_entry.action", 
		 	async: false, 
		 	data:{combinationId: combinationId, segmentId: segmentId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#temp-result').html(result); 
				$('.callJq').trigger('click');  
			}
		}); 
		return false;
	}); 
	
	$('#delete').click(function(){   
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/delete_combination.action", 
		 	async: false, 
		 	data:{combinationId: combinationId},
		    dataType: "json",
		    cache: false,
		    success:function(response){  
 				if(response.sqlReturnMessage == "SUCCESS"){ 
					var node = $jquery("#combination_treeview").fancytree("getTree").getNodeByKey(combinationId);
					node.remove();
				    $('#success_message').hide().html("Record deleted.").slideDown(1000);
					$('#success_message').delay(2000).slideUp();
 				 }else{
					$('#error_message').hide().html(response.sqlReturnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
				}
			}
		});  
		return false;
	}); 
	
	$('#copy').click(function(){  
		$('#copyCombinationId').val(combinationId);
		$('#copySegmentId').val(segmentId);
		$('#success_message').hide().html("Record copied.").slideDown(1000);
		$('#success_message').delay(2000).slideUp();
		return false;
	});  
	
	$('#paste').click(function(){   
		var combinationCopyId = Number($('#copyCombinationId').val());
		var copySegmentId = Number($('#copySegmentId').val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_paste.action", 
		 	async: false, 
		 	data:{combinationCopyId: combinationCopyId, combinationId: combinationId, segmentId: segmentId, copySegmentId: copySegmentId},
		    dataType: "json",
		    cache: false,
			success:function(response){  
				if(response.sqlReturnMessage == "SUCCESS"){
					 callCombinationTree("Record pasted.");
				 }else{
					$('#error_message').hide().html(response.sqlReturnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
				}
			}
		});
		return false;
	}); 
	
	$('.combination_subadd').live('click',function(){  
		 var combinationId = Number($('#combinationId').val()); 
		 var segmentId = Number($('#segmentId').val());
 		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_combination_subaccounts.action", 
		 	async: false, 
		 	data:{segmentId: segmentId, combinationId: combinationId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				 if(null != response && null != response.accountList){
					 $('#segmentName').html("Sub "+$.trim($('#segmentNameCst').val()));
 					 $('#accountCode').html('');
					 $('#accountCode').append(
							'<option value="">Select</option>');
					 $('#accessId').val(response.accountList[0].accessId);
					 $(response.accountList)
						.each(
								function(index) { 
							$('#accountCode')
								.append('<option value='
										+ response.accountList[index].accountId +'_'+ response.accountList[index].segmentId
										+ '>' 
										+ response.accountList[index].accountDescription +' ['+ response.accountList[index].accountCode+']'
										+ '</option>');
						});
 					 $('.combination_subadd').hide(); 
				 }else{
					 $('#pageerror_message').hide().html("Chart of accounts not defined.").slideDown(1000);
					 $('#pageerror_message').delay(2000).slideUp();
				 }
			}
		}); 
 		return false;
	});  
 
	$('.combinationadd_discard').live('click', function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		return false;
	}); 

	 $('.deleteoption').click(function(){
          var combinationId = Number($(this).attr('id').split('_')[1]);
          var slidetab = $(this).parent(); 
          $('.combination-error').remove();
           if(combinationId > 0) {
        	  $.ajax({
  				type:"POST",
  				url:"<%=request.getContextPath()%>/combination_ledger_delete.action",
											async : false,
											dataType : "json",
											data: {combinationId: combinationId},
											cache : false,
											success : function(response) {
												if (response.sqlReturnMessage == "SUCCESS")
													callDiscard();
												else{
													var htmlString = "<span class='combination-error'></span>";
													$(slidetab).append(htmlString); 
													$('.combination-error').html(response.sqlReturnMessage);  
  												}
											}
										});
							}
							return false;
						});

		$('.code_close').live('click', function() {
			callDiscard();
			return false;
 		});
	 $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:400,
		height:200,
		bgiframe: false,
		modal: true 
	});

	$('.print_combination').click(function(){ 
 		window.open('get_combinationall.action'
 		 		+ "",'','width=1000,height=600,scrollbars=yes,left=100px'); 
	});	
	
	$("button#btnResetSearch").click(function(e){
      	$("input[name=search]").val("");
      	$("span#matches").text("");
      	tree.clearFilter();
    }).attr("disabled", true);
	
	$('.combinationSearch').comboboxpos({ 
	    selected: function(event, ui){   
	    	
       	}
	});
	$('.ui-autocomplete-input').next('button').remove();
}); 
function saveCombination(accountId, accessId, segmentId, combinationId){ 
 	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/save_combination.action", 
	 	async: false, 
	 	data:{combinationId: combinationId, segmentId: segmentId, accessId: accessId, accountId: accountId},
	    dataType: "json",
	    cache: false,
		success:function(response){  
				if(response.sqlReturnMessage == "SUCCESS"){
					 callCombinationTree("Record created.");
			 }else{
				$('#pageerror_message').hide().html(response.sqlReturnMessage).slideDown(1000);
				$('#pageerror_message').delay(2000).slideUp();
			}
		}
	});    
}
function getRowId(id) {
	if(typeof id != 'undefined'){
		var idval = id.split('_');
		var rowId = Number(idval[0]);
		return rowId;
	} else{
		return 0;
	}
}
function getLastId(id) {
	var idval = id.split('_');
	var rowId = Number(idval[1]);
	return rowId;
}
function callCombinationTree(message){
 	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/combination_tree.action", 
		async: false,
		dataType: "html", 
		cache: false,
		success:function(result){   
			$('#DOMWindow').remove();
			$('#DOMWindowOverlay').remove(); 
			$("#main-wrapper").html(result); 
 			$('#success_message').hide().html(message).slideDown(1000);
			$('#success_message').delay(2000).slideUp();
		}
	});	 
}
function callDiscard() {
	$.ajax({
		type : "POST",
		url : "<%=request.getContextPath()%>/combination_tree.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){  
			$('#common-popup').dialog('destroy');		
			$('#common-popup').remove();
			$("#main-wrapper").html(result); 
		}
	});
	return false;
} 

function callRecursiveData(data){ 
   	$.each(data, function(i, item) {  
  		$('#combinationSearch')
			.append(
					'<option value="'+data[i].key+'">'+data[i].title+'</option>');
  		callRecursiveData(data[i].children);
	});  
} 
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			combinations
			<div
				class="portlet-header ui-widget-header float-right print_combination"
				style="cursor: pointer; margin-top: -5px;">Print</div>
		</div>
		
		<div class="portlet-content width48 float-right" id="hrm">
			<div class="portlet-header float-right">Note: For Creation of
				combination, right click on the account code.</div>
			<div class="clearfix"></div> 
 			<div class="portlet-header float-right width58">
				<label class="width10" style="float: none; font-weight: bold;">Search:</label> 
				<select name="combinationSearch" id="combinationSearch" class="combinationSearch">
 				</select>
				<button id="btnResetSearch" style="float: none; position: relative; 
					right: 5px; min-height: 34px; font-weight: bold;">&times;</button>
				<span id="matches" style="display: none;"></span>
			</div> 
			
		</div>
		<div class="portlet-content width48 float-left">
 			<div id="temp-result" style="display: none;"></div>
			<div id="success_message" class="response-msg success ui-corner-all" 
				style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" 
				style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>  
			<div id="combination_treeview"></div> 
			<input type="hidden" id="copyCombinationId"/>
			<input type="hidden" id="copySegmentId"/>
			<div style="display: none;"
			class="portlet-header ui-widget-header float-right" id="add">
			add</div>
			<div style="display: none;"
				class="portlet-header ui-widget-header float-right" id="copy">
				copy</div>
			<div style="display: none;"
				class="portlet-header ui-widget-header float-right" id="paste">
				paste</div>
			<div style="display: none;"
				class="portlet-header ui-widget-header float-right" id="delete">
				delete</div>
			<div style="display: none;"
				class="portlet-header ui-widget-header float-right"
				id="account-popup-close">close</div>
			<div class="clearfix"></div> 
		</div> 
  	<div id="temp-result" style="display: none;"></div>
	<span class="callJq"></span>
	</div>
</div>