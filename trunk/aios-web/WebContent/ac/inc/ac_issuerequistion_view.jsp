<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
.quantityerr {
	position: absolute;
	display: block;
	font-size: x-small !important;
	color: #ff255c;
}
</style>
<script type="text/javascript">
var slidetab="";
var requistionDetails="";
var tempid = ""; var accessCode = "";
$(function(){    
	$jquery("#requistionValidation").validationEngine('attach'); 
	 
	
	 $('input,select').attr('disabled', true);

	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_issue_requistion.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$('#codecombination-popup').dialog('destroy');		
  					$('#codecombination-popup').remove(); 
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/issue_requistion_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('#requistion_save').click(function(){
		 requistionDetails = "";
		 if($jquery("#requistionValidation").validationEngine('validate')){
 		 		
	 			var issueRequistionId = Number($('#issueRequistionId').val()); 
	 			var referenceNumber = $('#tempreferenceNumber').val();
	 			var issueDate = $('#issueDate').val(); 
	 			var requistionDate = $('#requistionDate').val();  
	 			var locationId = $('#locationId').val();  
 	 			var shippingSource = $('#shippingSource').val();  
	 			var personId = $('#personId').val();  
 	 			var description=$('#description').val();  
	 			requistionDetails = getRequistionDetails();  
	 			 
	   			if(requistionDetails!=null && requistionDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/save_issue_requisition.action", 
	 				 	async: false, 
	 				 	data:{	issueRequistionId: issueRequistionId, referenceNumber: referenceNumber, issueDate: issueDate, locationId: locationId,
	 				 			requistionDate: requistionDate, personId: personId, shippingSourceId : shippingSource, 
	 				 			description: description, requistionDetail: requistionDetails
	 					 	 },
	 				    dataType: "html",
	 				    cache: false,
	 					success:function(result){   
	 						 $(".tempresult").html(result);
	 						 var message=$.trim($('.tempresult').html());  
	 						 if(message=="SUCCESS"){
	 							 $.ajax({
	 									type:"POST",
	 									url:"<%=request.getContextPath()%>/show_issue_requistion.action", 
	 								 	async: false,
	 								    dataType: "html",
	 								    cache: false,
	 									success:function(result){
	 										$('#common-popup').dialog('destroy');		
	 										$('#common-popup').remove();  
	 					  					$('#codecombination-popup').dialog('destroy');		
	 					  					$('#codecombination-popup').remove(); 
	 										$("#main-wrapper").html(result); 
	 										if(issueRequistionId==0)
	 											$('#success_message').hide().html("Record created.").slideDown(1000);
	 										else
	 											$('#success_message').hide().html("Record updated.").slideDown(1000);
	 										$('#success_message').delay(3000).slideUp();
	 									}
	 							 });
	 						 } 
	 						 else{
	 							 $('#page-error').hide().html(message).slideDown(1000);
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					},
	 					error:function(result){  
	 						$('#page-error').hide().html("Internal error.").slideDown(1000);
	 						$('#page-error').delay(2000).slideUp();
	 					}
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter requisition details.").slideDown(1000);
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	});

	 var getRequistionDetails = function(){ 
		 var productArray = new Array();
		 var quantityArray = new Array();
	 	 var amountArray = new Array(); 
	 	 var descriptionArray = new Array(); 
	 	 var issueRequistionDetailArray = new Array(); 
	 	 var shelfArray = new Array(); 
	 	 var requistionDetail = ""; 
	 		$('.rowid').each(function(){ 
		 		
	 			 var rowId = getRowId($(this).attr('id'));  
	 			 var productId = $('#productid_'+rowId).val();   
	 			 var quantity = $('#productQty_'+rowId).val();  
	 			 var amount = $('#amount_'+rowId).val();    
	 			 var lineDescription=$('#linesDescription_'+rowId).val();
	 			 var issueRequistionDetailId = Number($('#issueRequistionDetailId_'+rowId).val());
	 			 var shelfid = Number($('#shelfid_'+rowId).val());
	 			 if(typeof productId != 'undefined' && productId!=null && 
	 					productId!="" && quantity!='' && amount!=''){
	 				productArray.push(productId); 
	 				quantityArray.push(quantity); 
	 				amountArray.push(amount);
	  				if(lineDescription ==null || lineDescription=="")
	  					descriptionArray.push("##");
	 				else 
	 					descriptionArray.push(lineDescription); 
	  				issueRequistionDetailArray.push(issueRequistionDetailId); 
	  				shelfArray.push(shelfid);
	 			 } 
	 		});
	 		 
	  		for(var j=0;j<productArray.length;j++){ 
	  			requistionDetail += productArray[j]+"__"+quantityArray[j]+"__"+amountArray[j]+"__"+
	  						descriptionArray[j]+"__"+issueRequistionDetailArray[j]+"__"+shelfArray[j];
	 			if(j==productArray.length-1){   
	 			} 
	 			else{
	 				requistionDetail += "@#";
	 			}
	 		} 
	  		 
	 		return requistionDetail; 
	 	}; 

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

 	$('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

 	  $('#product-stock-popup').live('click',function(){  
 		   $('#common-popup').dialog('close'); 
 	   });

 	 $('#store-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });


 	$('.shipping-source-lookup').click(function(){
        $('.ui-dialog-titlebar').remove(); 
        accessCode = $(this).attr("id");
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingSource').html("");
			$('#shippingSource').append("<option value=''>Select</option>");
			loadLookupList("shippingSource"); 
		} 
	});


 	$('.common-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('.common-department-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_departmentbranch_list.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('.product-stock-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		var rowId = getRowId($(this).attr('id'));  
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_popup.action", 
		 	async: false,  
		 	data: {itemType: "E", rowId: rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
	
 	 $('.productQty').live('change',function(){  
 		$('.quantityerr').remove(); 
 	 	 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#amount_'+lineID).val());
 	 	 var availQty = Number($('#totalAvailQty_'+lineID).val());
 	 	 if(productQty > availQty) {
 	 		$(this).parent().append("<span class='quantityerr' id=inverror_"+lineID+">Issue Qty should be lt or eq stock qty.</span>"); 
 	 		$('#productQty_'+lineID).val('');
 	 	 } else {
 	 		 if(productQty >0 && amount > 0){ 
 	 	 		$('#totalAmount_'+lineID).text(Number(productQty) *  (amount));
 	 	 	 }
 	 		triggerAddRow(lineID);
 	 	 }  
 		 return false;
 	 });

 	$('.amount').live('change',function(){ 
 		 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#amount_'+lineID).val());
 	 	 
 	 	 if(productQty >0 && amount > 0){
 	 		$('#totalAmount_'+lineID).text(Number(productQty) *  (amount));
 	 	 }
 		triggerAddRow(lineID);
 		return false;
 	 });

 	if (Number($('#issueRequistionId').val()) > 0) {
 		$('#shippingSource').val($('#tempShippingSource').val());
		$('.rowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var treevalue = $('#codeCombination_'+rowId).val().replace(/[\s\n\r]+/g, ' ' ).trim();  
			$('#codeCombination_'+rowId).val(treevalue); 
		}); 
	}
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}

function triggerAddRow(rowId){  
	 
	var productid = $('#productid_'+rowId).val();  
	var productQty = $('#productQty_'+rowId).val(); 
	var amount = $('#amount_'+rowId).val();
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid!=null && productid!=""
			&& productQty!=null && productQty!=""  
			&& amount!=null && amount!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}
function departmentBranchPopupResult(locationId,locationName){
	$('#locationId').val(locationId);
	$('#locationName').val(locationName);
}
function getStockDetails(currentObj){
	var productId = $('#productid_'+currentObj).val();
	var storeId = $('#storeid_'+currentObj).val(); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_stock_details.action",
			async : false,
			data : {
				productId : productId,
				storeId : storeId
			},
			dataType : "json",
			cache : false,
			success : function(response) {
				if (response.stockVO.availableQuantity != null
						&& response.stockVO.availableQuantity != '') { 
					$('#totalAvailQty_' + currentObj).val(
							response.stockVO.availableQuantity);
					$('#amount_' + currentObj).val(response.stockVO.unitRate);
					$('#totalAmount_' + currentObj).text(
							response.stockVO.totalAmount);
					triggerAddRow(currentObj);
				} else {
					$('#line-error').hide().html(
							"For Product " + $('#product_' + currentObj).text()
									+ " in store "
									+ $('#store_' + currentObj).text()
									+ " is empty.").slideDown(2000);
					$('#line-error').delay().slideUp();
				}
				return false;
			},
			error : function(response) {
				$('#line-error').hide().html(
						"For Product " + $('#product_' + currentObj).text()
								+ " in store "
								+ $('#store_' + currentObj).text()
								+ " is empty.").slideDown(2000);
				$('#line-error').delay(3000).slideUp();
			}
		});
		return false;
	}
	function commonProductPopup(aData, rowId) {
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.productName);
		$('#store_' + rowId).html(aData.storeName);
		$('#totalAvailQty_' + rowId).val(aData.availableQuantity); 
		$('#amount_' + rowId).val(aData.unitRate); 
 		$('#shelfid_' + rowId).val(aData.shelfId);
		$('.amount').trigger('change');
	}
	function personPopupResult(personid, personname, commonParam) {
		$('#personId').val(personid);
		$('#personNumber').val(personname);
		$('#common-popup').dialog("close");
	}
	/*function commonStorePopup(storeId, storeName, rowId) { 
		$('#storeid_' + rowId).val(storeId);
		$('#store_' + rowId).html(storeName);
		getStockDetails(rowId);
		return false;
	}*/

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Issue
			Requisition
		</div> 
		<form name="requistionValidation" id="requistionValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="issueRequistionId" name="issueRequistionId"
					value="${ISSUE_REQUISTION.issueRequistionId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30">Location Name</label> <input type="text"
									readonly="readonly" name="locationName" id="locationName"
									class="width50 "
									value="${ISSUE_REQUISTION.cmpDeptLocation.location.locationName}" />
								 <input
									type="hidden" readonly="readonly" name="locationId"
									id="locationId"
									value="${ISSUE_REQUISTION.cmpDeptLocation.cmpDeptLocId}" />
							</div>
 							<div>
								<label class="width30">Requistion Person</label> <input type="text"
									readonly="readonly" name="requistionPerson" id="personNumber"
									class="width50 "
									value="${ISSUE_REQUISTION.person.firstName } ${ISSUE_REQUISTION.person.lastName }" />
							<input
									type="hidden" readonly="readonly" name="personId" id="personId"
									value="${ISSUE_REQUISTION.person.personId}" />
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${ISSUE_REQUISTION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 120px;">
							<div style="padding-bottom: 8px;">
								<label class="width30"> Reference No. </label> <span
									style="font-weight: normal;" id="referenceNumber"> <c:choose>
										<c:when
											test="${ISSUE_REQUISTION.referenceNumber ne null && ISSUE_REQUISTION.referenceNumber ne ''}">
													<input name="requistionDate" type="text" readonly="readonly"
											id="requistionDate" class="requistionDate  width50"
											value="${ISSUE_REQUISTION.referenceNumber}"/>
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${ISSUE_REQUISTION.referenceNumber}" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> Requisition Date </label>
								<c:choose>
									<c:when
										test="${ISSUE_REQUISTION.requistionDate ne null && ISSUE_REQUISTION.requistionDate ne ''}">
										<c:set var="requistionDate"
											value="${ISSUE_REQUISTION.requistionDate}" />
										<input name="requistionDate" type="text" readonly="readonly"
											id="requistionDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("requistionDate").toString())%>"
											class="requistionDate  width50">
									</c:when>
									<c:otherwise>
										<input name="requistionDate" type="text" readonly="readonly"
											id="requistionDate"
											class="requistionDate  width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Issue Date</label>
								<c:choose>
									<c:when
										test="${ISSUE_REQUISTION.issueDate ne null && ISSUE_REQUISTION.issueDate ne ''}">
										<c:set var="issueDate" value="${ISSUE_REQUISTION.issueDate}" />
										<input name="issueDate" type="text" readonly="readonly"
											id="issueDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("issueDate").toString())%>"
											class="issueDate  width50">
									</c:when>
									<c:otherwise>
										<input name="issueDate" type="text" readonly="readonly"
											id="issueDate" class="issueDate  width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Shipping Source</label> <select
									name="shippingSource" id="shippingSource" class="width51 ">
									<option value="">Select</option>
									<c:forEach var="SOURCE" items="${SHIPPING_SOURCE}">
										<option value="${SOURCE.lookupDetailId}">${SOURCE.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempShippingSource"
									value="${ISSUE_REQUISTION.lookupDetail.lookupDetailId}" />
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>Requisition Detail</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th style="display: none;">UOM</th>
										<th style="display: none;">Costing Type</th>
										<th style="display: none;">Store</th>
										<th style="width: 5%;">Quantity</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 5%;">Total</th>
										<th style="width: 10%;"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%; display: none;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${ISSUE_REQUISTION.issueRequistionDetails}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.productName}</span> 
											</td>
											<td id="uom_${status.index+1}" style="display: none;">${DETAIL.product.lookupDetailByProductUnit.displayName}</td>
											<td id="costingType_${status.index+1}" style="display: none;"></td>
											<td style="display: none;">
												<input type="hidden" name="shelfId"
												id="shelfid_${status.index+1}"
												value="${DETAIL.shelf.shelfId}" />
											 <span
												id="store_${status.index+1}"></span>
												<span class="button float-right"> <a
													style="cursor: pointer; display: none;" id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80"> <input type="hidden"
												name="totalAvailQty" id="totalAvailQty_${status.index+1}" />
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="amount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><c:set var="totalAmount"
													value="${DETAIL.quantity * DETAIL.unitRate}" /> <span
												id="totalAmount_${status.index+1}" style="float: right;">${totalAmount}</span>
											</td>
											<td><input type="text" name="linesDescription"
												id="linesDescription_${status.index+1}"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											<td style="width: 1%; display: none;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="issueRequistionDetailId"
												id="issueRequistionDetailId_${status.index+1}"
												value="${DETAIL.issueRequistionDetailId}" />
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div> 
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div> 