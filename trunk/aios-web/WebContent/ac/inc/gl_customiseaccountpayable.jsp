<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style> 
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
.ui-dialog{
	z-index: 99999!important;
}
</style>
<script type="text/javascript">
var accountTypeId=0;
var slidetab;
var tempid;
$(function(){
	 $jquery("#coavalidate-form").validationEngine('attach'); 
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0); 
	      $('.ui-dialog-titlebar').remove();   
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
	
	$('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		bgiframe: false,
		modal: false
	}); 
	 
	 $('.discard').click(function(){
		 $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);
			},
			error: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
			}
		});
		 return false;
	 });
	 
	 $(document).keyup(function(e) { 
		 var id=$('.save').attr('id'); 
		  if (e.keyCode == 27 && id=="add-customise"){ 
			  $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);
					},
					error: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);  
					}
				});
		  } 
	});  
	 
	 $('.skip').click(function(){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_setup_wizard.action", 
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){ 
					$('#codecombination-popup').dialog('destroy');		
			  		$('#codecombination-popup').remove(); 
					$("#DOMWindow").html(result); 
					$.scrollTo(0,300);
				} 		
			});  
	  });
	 
	 $('.save').click(function(){ 
		 $('.error').hide();
		 var ownersEquityId=Number($('#ownersEquityId').val());
		 var clearingAccountControlId = Number($('#clearingAccountControlId').val());
		 var accountsPayableControlId = Number($('#accountsPayableControlId').val());
		if(ownersEquityId==null || ownersEquityId=='' || ownersEquityId==0){
			$('.page-error').hide().html("Choose Owners Equity").slideDown(1000);
			return false;
		}
		if(clearingAccountControlId==null || clearingAccountControlId=='' || clearingAccountControlId==0){
			$('.page-error').hide().html("Choose Clearing Account").slideDown(1000);
			return false;
		}
		if(accountsPayableControlId==null || accountsPayableControlId=='' || accountsPayableControlId==0){
			$('.page-error').hide().html("Choose Accounts Payable Account").slideDown(1000);
			return false;
		}
		
		if($jquery("#coavalidate-form").validationEngine('validate')){

			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customise_accountpayblecontrol.action",
				data:{ownersEquityId: ownersEquityId, clearingAccount: clearingAccountControlId, accountsPayableId: accountsPayableControlId},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tempresult").html(result);
					var message=$('#returnMsg').html(); 
					if(message.trim()=="Success"){  
						 $.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_setup_wizard.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){  
								$('#DOMWindow').remove();
								$('#DOMWindowOverlay').remove();
								$('#codecombination-popup').dialog('destroy');		
						  		$('#codecombination-popup').remove(); 
								$("#main-wrapper").html(result);
								$('#success_message').hide().html(message).slideDown(1000);
								$.scrollTo(0,300);
							} 		
						});  
					 }
					else{
						$('.page-error').hide().html($('#returnMsg').html()).slideDown(1000);
						return false;
					}
				}
			 });
		}
		else{
			return false;
		}
	 });
	 return false;
});
function setCombination(combinationTreeId, combinationTree){  
	tempid=$(tempid).attr('id');  
	if(typeof tempid!='undefined'&& tempid=="ownersEquityCode"){
		$('#ownersEquityId').val(combinationTreeId); 
		$('#ownersEquity').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="clearingAccountControlCode"){
		$('#clearingAccountControlId').val(combinationTreeId); 
		$('#clearingAccountControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountsPayableControlCode"){
		$('#accountsPayableControlId').val(combinationTreeId); 
		$('#accountsPayableControl').val(combinationTree);
	} 
} 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Create Account Payble Control</div>
		<div class="portlet-content">
			<div class="tempresult" style="display:none;"></div>
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:80% !important; display:none;"></div> 	
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;"> 
				<div class="width60" id="hrm">
					<fieldset>
						<legend>Clearing/ Owners Equity</legend> 
						<div>
							<label class="width30">Clearing Account<span style="color: red;">*</span></label>
							<input type="hidden" name="clearingAccountControlId" id="clearingAccountControlId" value="${CLEARNING_ACCOUNT.combinationId}"/> 
							<c:choose>
								<c:when test="${CLEARNING_ACCOUNT.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="clearingAccountControl" id="clearingAccountControl" value="${CLEARNING_ACCOUNT.accountByCostcenterAccountId.account}.${CLEARNING_ACCOUNT.accountByNaturalAccountId.account}.${CLEARNING_ACCOUNT.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 validate[required]">
								</c:when>
								<c:otherwise>
									<input type="text" name="clearingAccountControl" id="clearingAccountControl" readonly="readonly" class="width48 validate[required]">
								</c:otherwise>
							</c:choose> 
							<span class="button" id="clearingAccountControlCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">Owners Equity<span style="color: red;">*</span></label>
							<input type="hidden" name="ownersEquityId" id="ownersEquityId" value="${OWNERS_EQUITY.combinationId}"/> 
							<c:choose>
								<c:when test="${OWNERS_EQUITY.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="ownersEquity" id="ownersEquity" value="${OWNERS_EQUITY.accountByCostcenterAccountId.account}.${OWNERS_EQUITY.accountByNaturalAccountId.account}.${OWNERS_EQUITY.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 validate[required]">
								</c:when>
								<c:otherwise>
									<input type="text" name="ownersEquity" id="ownersEquity" readonly="readonly" class="width48 validate[required]">
								</c:otherwise>
							</c:choose> 
							
							<span class="button" id="ownersEquityCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">Accounts Payable<span style="color: red;">*</span></label>
							<input type="hidden" name="accountsPayableControlId" id="accountsPayableControlId" value="${ACCOUNTS_PAYABLE.combinationId}"/> 
							<c:choose>
								<c:when test="${ACCOUNTS_PAYABLE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="accountsPayableControl" id="accountsPayableControl" value="${ACCOUNTS_PAYABLE.accountByCostcenterAccountId.account}.${ACCOUNTS_PAYABLE.accountByNaturalAccountId.account}.${ACCOUNTS_PAYABLE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 validate[required]">
								</c:when>
								<c:otherwise>
									<input type="text" name="accountsPayableControl" id="accountsPayableControl" readonly="readonly" class="width48 validate[required]">
								</c:otherwise>
							</c:choose> 
							
							<span class="button" id="accountsPayableControlCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="accountpayble_controlcode" style="font-weight:bold;"></div> 
				<div class="clearfix"></div>  
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.cancel"/></div> 
					<div class="portlet-header ui-widget-header float-right save" id="add"><fmt:message key="accounts.common.button.save"/></div>
				</div> 
				<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
					<div class="portlet-header ui-widget-header float-left skip" style="cursor:pointer;">skip</div> 
				</div>
			</form>
		</div>	
	</div>		
	<div style="display: none; position: absolute; overflow: auto; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div>
</div>