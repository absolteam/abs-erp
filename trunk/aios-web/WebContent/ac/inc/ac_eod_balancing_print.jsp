<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
.caption {
	padding: 2px;
	padding-top: 5px !important;
}

.data {
	padding: 2px;
	padding-top: 5px !important;
}

.data-container {
	padding: 5px;
}

td {
	padding-top: 8px !important;
}

th {
	font-weight: bold;
	padding-top: 8px !important;
}

@media print {
	.hideWhilePrint {
		display: none;
	}
}
</style>
<script type="text/javascript"> 
 
 </script>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading">EOD Balancing</span>
	</div>
	<div id="main-content" class="hr_main_content">
		<div id="hrm" class="width100">
			<div class="float-right width30">
				<span>
				Till Number : ${EOD_DETAILS.tillNumber} </span>
			<br>
			<span>
				EOD Person : ${EOD_DETAILS.posUserName} </span>
			</div> 
			<div class="float-left width30">
				<span>
				Reference NO : ${EOD_DETAILS.referenceNumber} </span>
			<br>
			<span>
				Date : ${EOD_DETAILS.eodDate} </span>
			<br> 
			<span>
				Store : ${EOD_DETAILS.storeName} </span>
			</div>   
		</div>
		<div class="width100 float-left">
			<div id="hrm" class=" hastable width100">
				<table class="width100">
					<thead>
						<tr>
							<th>Craft Product</th>
							<th>Quantity</th>  
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach var="eodDetail" items="${EOD_DETAILS.eodBalancingDetailVOs}"
							varStatus="status">
							<tr class="rowid">
								<td>${eodDetail.product.productName}</td>
								<td>${eodDetail.productQuantity}</td> 
							</tr>
							<c:if test="${eodDetail.eodProductionDetailVOs ne null && fn:length(eodDetail.eodProductionDetailVOs)>0}">
								<tr>
									<th>Production Material</th>
									<th>Quantity</th>  
									<th>Accepted Wastage</th>  
									<th>Production Wastage</th>  
								</tr>
								<c:forEach var="eodProduction" items="${eodDetail.eodProductionDetailVOs}">
									<tr class="rowid">
										<td>${eodProduction.product.productName}</td>
										<td>${eodProduction.productQuantity}</td>
										<td>${eodProduction.acceptedWastage}</td> 
										<td>${eodProduction.productionWastage}</td> 
									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
	</div> 
</body>