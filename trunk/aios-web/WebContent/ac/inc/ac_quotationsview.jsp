<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript">
	var quotationId = 0; var rowId = ""; 
	$(function() { 
		
		$jquery("#quotationCreation").validationEngine('attach'); 
		
		quotationId = Number($('#quotationId').val());   
		 $('#quotation_document_information').click(function(){
				if(quotationId>0){
					AIOS_Uploader.openFileUploader("doc","quotationDocs","Quotation",quotationId,"QuotationDocuments");
				}else{
					AIOS_Uploader.openFileUploader("doc","quotationDocs","Quotation","-1","QuotationDocuments");
				}
			});
		 
		 if(quotationId>0)
			populateUploadsPane("doc","quotationDocs","Quotation",quotationId);	
		 
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
		       $('#themeRollerSelect option:not(:selected)').remove();
		}
		$('#quotationDate,#quotationExpiry').datepick({
		onSelect: customRange, showTrigger: '#calImg'});
		
		if("${QUOTATION.tender.tenderNumber}" != "") {
			$("#quotationCurrency").val("${currencyId}");
			$('.rowid').each(function(){
				var rowId=getRowId($(this).attr('id')); 
				$('#quantity').attr('readonly', true);
			});
			$(".hastable").show();
		}
		
		if("${QUOTATION.requisition}" != null && "${QUOTATION.requisition}" != "") { 
			$(".hastable").show();
			
		}
				
		$("#quotation_save").click(function() {
			 if($jquery("#quotationCreation").validationEngine('validate')){
 				quotationId = Number($("#quotationId").val()); 
				var tenderId = Number($("#tenderId").val()); 
				var requisitionId = Number($("#requisitionId").val()); 
				var quotationCurrency = Number($("#quotationCurrency").val());
				var quotationDate = $("#quotationDate").val();
				var quotationExpiry = $("#quotationExpiry").val();
				var description = $("#description").val();
				var quotationDetails = getQuotationDetails();
				var supplierId=$('#supplierId').val();
				var quotationNumber=$('#quotationNumber').val();
				if(quotationDetails!=null && quotationDetails!=""){ 
 				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/save_quotation.action", 
			     	async: false,
			     	data: {
			     		quotationId: quotationId,
			     		tenderId: tenderId,
			     		quotationNumber: quotationNumber,
			     		currencyId: quotationCurrency,
			     		quotationDate: quotationDate,
			     		expiryDate: quotationExpiry,
			     		description: description, 
			     		supplierId : supplierId,
			     		requisitionId: requisitionId,
			     		quotationStringDetail: quotationDetails
			     	},
					dataType: "json",
					cache: false,
					success: function(response) { 
						var message = "Record Created.";
						if(response.returnMessage=="SUCCESS"){
							if(quotationId > 0)
								message = "Record Updated."; 
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/quotationListRedirect.action", 
						     	async: false, 
								dataType: "html",
								cache: false,
								success: function(result){ 
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();
									$("#main-wrapper").html(result);   
								} 		
							}); 
 							if(quotationId > 0)
								$('#success_message').hide().html(message).slideDown(1000); 
							else
								$('#success_message').hide().html(message).slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						}
						else{
							$('#page-error').hide().html(response.returnMessage).slideDown(1000);
							$('#page-error').delay(2000).slideUp();
							return false;
						}  
 					} 		
				}); 
 				}else{
					 $('#page-error').hide().html("Please enter Quotation Detail").slideDown(1000);
					 $('#page-error').delay(3000).slideUp();
					 return false;
				}
			} else {
				return false;
			}
			});
		
			$("#quotation_discard").click(function() { 
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/quotationListRedirect.action", 
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result) { 
						$('#common-popup').dialog('destroy');		
						$('#common-popup').remove();
						$("#main-wrapper").html(result); 
					} 		
				});  
				return false;
			});
			
			$('#common-popup').dialog({
				 autoOpen: false,
				 minwidth: 'auto',
				 width:800, 
				 bgiframe: false,
				 overflow:'hidden',
				 modal: true 
			});
			
			$('.delrow').live('click',function(){ 
				 slidetab=$(this).parent().parent().get(0);  
		       	 $(slidetab).remove();  
		       	 var i=1;
		    	 $('.rowid').each(function(){   
		    		 var rowId=getRowId($(this).attr('id')); 
		    		 $('#lineId_'+rowId).html(i);
					 i=i+1; 
		 		 }); 
		 		 return false;  
			});
			
			$('.quantity').live('change',function(){
				rowId=getRowId($(this).attr('id'));
				var quantity=Number($(this).val());
				var total=Number(0);
				var unitrate=Number($('#unitrate_'+rowId).val());
				if(unitrate!=null && quantity>0 && unitrate>0 ){
					total=Number(unitrate*quantity);
					$('#totalamount_'+rowId).text(total.toFixed(2));
					triggerAddRow(rowId);
				}
				 return false;
			});

			$('.unitrate').live('change',function(){
				rowId=getRowId($(this).attr('id'));
				var unitrate=Number($(this).val());
				var total=Number(0);
				var quantity=Number($('#quantity_'+rowId).val());
				if(unitrate!=null && quantity>0 && unitrate>0 ){
					total=Number(unitrate*quantity);
					$('#totalamount_'+rowId).text(total.toFixed(2));
					//triggerAddRow(rowId);
				}
				 return false;
			});

			 $('#product-common-popup').live('click',function(){  
				   $('#common-popup').dialog('close'); 
			   });
		 
			  $('#supplier-common-popup').live('click',function(){  
				   $('#common-popup').dialog('close'); 
			  });

			  $('#tender-common-popup').live('click',function(){  
				   $('#common-popup').dialog('close'); 
			  });
			  
			$('.tender-common-popup').click(function(){  
				$('.ui-dialog-titlebar').remove(); 
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_common_tender_popup.action", 
				 	async: false,   
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);  
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0);
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				 return false;
			});
			
			$('.requisition-common-popup').click(function(){  
				$('.ui-dialog-titlebar').remove(); 
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_quotation_requisition_popup.action", 
				 	async: false,   
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);  
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0);
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				 return false;
			});

			$('.supplierquotation-common-popup').click(function(){  
				$('.ui-dialog-titlebar').remove(); 
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
				 	async: false,   
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);  
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0);
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				 return false;
			});

			$('.productquotation-common-popup').live('click',function(){  
				$('.ui-dialog-titlebar').remove(); 
				var rowId=getRowId($(this).attr('id'));
				var itemType=$('#itemnature_'+rowId).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_purchase_product_common_popup.action", 
				 	async: false,   
					data:{itemType: itemType, rowId:rowId, pageInfo: "quotation"},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);  
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0);
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				 return false;
			});
			
			 $('.addrows').click(function(){ 
				  var i=Number(1); 
				  var id=Number(getRowId($(".tab>.rowid:last").attr('id')));
				  id=id+1;  
				  $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/purchase_addrow.action",
				async : false,
				data : {
					rowId : id
				},
				dataType : "html",
				cache : false,
				success : function(result) {
					$('.tab tr:last').before(result);
					if ($(".tab").height() > 255)
						$(".tab").css({
							"overflow-x" : "hidden",
							"overflow-y" : "auto"
						});
					$('.rowid').each(function() {
						var rowId = getRowId($(this).attr('id'));
						$('#lineId_' + rowId).html(i);
						i = i + 1;
					});
				}
			});
			return false;
		});

		var getQuotationDetails = function() {
			var quotationDetails = "";
			var productid = new Array();
			var unitrate = new Array();
			var totalunit = new Array();
			var lineDescriptions = new Array();
			var lineid = new Array();
			var requisitionDetail = new Array();
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var productId = $('#productid_' + rowId).val();
						var unitRate = $('#unitrate_' + rowId).val();
						var quantity = $('#quantity_' + rowId).val();
						var quotationLineId = Number($(
								'#quotationLineId_' + rowId).val());
						var linedescription = $('#linedescription_' + rowId)
								.val();
						 var requisitionDetailId = Number($(
									'#requisitionLineId_' + rowId).val());
						if (typeof productId != 'undefined'
								&& productId != null && productId != ""
								&& unitRate != null && unitRate != "") {
							productid.push(productId);
							unitrate.push(unitRate);
							totalunit.push(quantity);
							lineid.push(quotationLineId);
							requisitionDetail.push(requisitionDetailId);
							if (linedescription != null
									&& linedescription != "")
								lineDescriptions.push(linedescription);
							else
								lineDescriptions.push("##");
						}
					});
			for ( var j = 0; j < productid.length; j++) {
				quotationDetails += productid[j] + "__" + unitrate[j] + "__"
						+ totalunit[j] + "__" + lineid[j] + "__"
						+ lineDescriptions[j]+ "__"+requisitionDetail[j];
				if (j == productid.length - 1) {
				} else {
					quotationDetails += "#@";
				}
			}
			return quotationDetails;
		};
		
		{
			if(Number($('#quotationId').val()<=0)){ 
				manupulateLastRow();
			} 
		}
	});

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function checkLinkedDays() {

		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}

	function customRange(dates) {

		if (this.id == 'quotationDate') {
			$('#quotationExpiry').datepick('option', 'minDate',
					dates[0] || null);
			if ($('#quotationDate').val() != "") {
				$('.quotationDateformError').hide();
				addPeriods();
			}
		} else {
			$('#quotationDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	function addPeriods() {
		var date = new Date($('#quotationDate').datepick('getDate')[0]
				.getTime());
		$.datepick.add(date, parseInt(30, 10), 'd');
		$('#quotationExpiry').val($.datepick.formatDate(date));
	}
	function triggerAddRow(rowId) {
		var itemNature = $('#itemnature_' + rowId).val();
		var productid = $('#productid_' + rowId).val();
		var unitrate = $('#unitrate_' + rowId).val();
		var quantity = $('#quantity_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (itemNature != null && itemNature != "" && productid != null
				&& productid != "" && unitrate != null && unitrate != ""
				&& quantity != null && quantity != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}

	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('tr:last').removeAttr('id');
		$('tr:last').removeClass('rowid').addClass('lastrow');
		$($('tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('tr:last').append("<td style=\"\"></td>");
		}
	}

	function commonProductPopup(productId, productName, rowId) {
		$('#productid_' + rowId).val(productId);
		$('#product_' + rowId).html(productName);
	}
	function commonSupplierPopup(supplierId, supplierName, combinationId,
			accountCode, param) {
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
	}
	function commonTenderPopup(tenderId, tenderNumber, currencyId) {
		$('#tenderId').val(tenderId);
		$('#tenderNumber').val(tenderNumber);
		$('#quotationCurrency').val(currencyId);
		$('.requisition-common-popup').remove();
		callTendarDetails(tenderId);
	}
	function commonQuotationRequisitionPopup(aData){
		$('#requisitionNumber').val(aData.referenceNumber);
		$('#requisitionId').val(aData.requisitionId);
		$('.tender-common-popup').remove();
		callRequisitionDetails(aData.requisitionId);
	}
	function callRequisitionDetails(requisitionId){ 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getquotation_requisition_detail.action",
			async : false,
			data : {
				requisitionId : requisitionId
			},
			dataType : "html",
			cache : false,
			success : function(result) {
				$('.tab').html(result);
				$('.hastable').show();
			},
			error : function(result) {
				$('.tab').html(result);
			}
		});
	}
	function callTendarDetails(tenderId){ 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/gettenderdetail_list.action",
			async : false,
			data : {
				tenderId : tenderId
			},
			dataType : "html",
			cache : false,
			success : function(result) {
				$('.tab').html(result);
				$('.hastable').show();
			},
			error : function(result) {
				$('.tab').html(result);
			}
		});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			quotation
		</div>
		<div class="width98 float-left"> 
			<form id="quotationCreation" style="position: relative;">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div>
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
					<div class="width48 float-left" id="hrm">
						<fieldset style="min-height: 140px;">
							<input type="hidden" id="quotationId"
								value="${QUOTATION.quotationId}"> <input type="hidden"
								id="tenderId" value="${QUOTATION.tender.tenderId}">
							<div>
								<label class="width30">Reference Number<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${QUOTATION.quotationNumber ne null && QUOTATION.quotationNumber ne ''}">
										<input type="text" name="quotationNumber"
											value="${QUOTATION.quotationNumber}" id="quotationNumber"
											readonly="readonly" class="width50 validate[required]">
									</c:when>
									<c:otherwise>
										<input type="text" name="quotationNumber"
											value="${QUOTATION_NUMBER}" id="quotationNumber"
											readonly="readonly" class="width50 validate[required]">
									</c:otherwise>
								</c:choose>

							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.tender.list.tenderNumber" /> </label> <input type="text"
									name="tenderNumber" value="${QUOTATION.tender.tenderNumber}"
									id="tenderNumber" class="width50" readonly="readonly">
								<c:if
									test="${QUOTATION.quotationId eq null || QUOTATION.quotationId le 0}">
									<span class="button"> <a style="cursor: pointer;"
										id="tender"
										class="btn ui-state-default ui-corner-all tender-common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</c:if>
							</div> 
							<div>
								<label class="width30">Requisition</label> <input type="text"
									name="requisitionNumber" value="${QUOTATION.requisition.referenceNumber}"
									id="requisitionNumber" class="width50" readonly="readonly">
									<input type="hidden" id="requisitionId" value="${QUOTATION.requisition.requisitionId}"/>
								<c:if
									test="${QUOTATION.quotationId eq null || QUOTATION.quotationId le 0}">
									<span class="button"> <a style="cursor: pointer;"
										id="tender"
										class="btn ui-state-default ui-corner-all requisition-common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</c:if>
							</div>
							
							<div style="display: none;">
								<label class="width30"><fmt:message
										key="accounts.quotation.currency" /><span class="mandatory">*</span>
								</label> <select name="quotationCurrency" id="quotationCurrency"
									disabled="disabled" class="width50 validate[required]">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${QUOTATION_CURRENCY ne null and QUOTATION_CURRENCY ne ''}">
											<c:forEach items="${QUOTATION_CURRENCY}" var="CURRENCY">
												<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.quotation.quotationDate" /><span
									class="mandatory">*</span> </label> <input type="text"
									name="quotationDate" value="${QUOTATION_quotationDate}"
									readonly="readonly" id="quotationDate"
									class="width50 validate[required]" TABINDEX=4>
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.quotation.quotationExpiry" /><span
									class="mandatory">*</span> </label> <input type="text"
									name="quotationExpiry" value="${QUOTATION_expiryDate}"
									readonly="readonly" id="quotationExpiry"
									class="width50 validate[required]" TABINDEX=5>
							</div>
						</fieldset>
					</div>
					<div class="width48 float-right" id="hrm">
						<fieldset style="min-height: 140px;">
							<div style="display: none;">
								<label class="width30" for="supplierName">Supplier No<span
									class="mandatory">*</span> </label> <input type="hidden"
									id="supplierId" name="supplierId"
									value="${QUOTATION.supplier.supplierId}" /> <input type="text"
									readonly="readonly" name="supplierNumber" id="supplierNumber"
									class="width50"
									value="${QUOTATION.supplier.supplierNumber}" />
							</div>
							<div>
								<label class="width30 tooltip"><fmt:message
										key="hr.supplier.supplierName" /><span class="mandatory">*</span>
								</label> <input type="text" name="supplierName"
									value="${QUOTATION.supplier.person.firstName} ${QUOTATION.supplier.person.lastName}${QUOTATION.supplier.cmpDeptLocation.company.companyName}${QUOTATION.supplier.company.companyName}"
									id="supplierName" class="validate[required] width50" TABINDEX=6
									readonly="readonly"> <span class="button supp-pop">
									<a style="cursor: pointer;" id="supplier"
									class="btn ui-state-default ui-corner-all supplierquotation-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30 tooltip"><fmt:message
										key="accounts.quotation.description" /> </label>
								<textarea rows="2" class="float-left width51" id="description">${QUOTATION.description}</textarea>
							</div>
							<div class="width100 float-left">									
							
								<div id="quotation_document_information" class="width90" style="cursor: pointer; color: blue;">
									<u>Upload Documents Here</u>
								</div>
								<div style="padding-top: 10px; margin-top:3px;" class="width90">
									<span id="quotationDocs"></span>
								</div>
								
							</div>	
						</fieldset>
					</div>
				</div>

				<div id="hrm" class="hastable width100 float-left"
					style="margin-top: 10px; display: none;">
					<fieldset>
						<legend>
							<fmt:message key="accounts.quotation.productInfo" /><span
									class="mandatory">*</span>
						</legend>
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%"><fmt:message
											key="accounts.quotation.itemType" />
									</th>
									<th style="width: 5%"><fmt:message
											key="accounts.quotation.product" />
									</th>
									<th style="width: 5%; display: none;">UOM</th>
									<th style="width: 5%"><fmt:message
											key="accounts.quotation.unitRate" />
									</th>
									<th style="width: 5%"><fmt:message
											key="accounts.quotation.quantity" />
									</th>
									<th style="width: 5%"><fmt:message
											key="accounts.quotation.totalAmount" />
									</th>
									<th style="width: 5%"><fmt:message
											key="accounts.quotation.description" />
									</th>
									<th style="width: 0.01%;"><fmt:message
											key="accounts.common.label.options" />
									</th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:choose>
									<c:when
										test="${QUOTATION_DETAIL_INFO ne null && QUOTATION_DETAIL_INFO ne '' && fn:length(QUOTATION_DETAIL_INFO)>0}">
										<c:forEach var="detail" items="${QUOTATION_DETAIL_INFO}"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
												<td>
													${detail.itemTypeStr}
												</td>
												<td><span class="float-left width60"
													id="product_${status.index+1}">${detail.product.productName}</span>
													<span class="button float-right prod quoteprod"
													style="display: none;" style="height:16px;"> <a
														style="cursor: pointer; height: 100%;"
														id="productline_${status.index+1}"
														class="btn ui-state-default ui-corner-all productquotation-common-popup width100 float-right">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" id="productid_${status.index+1}"
													value="${detail.product.productId}"/>
												</td>
												<td id="uom_${status.index+1}" class="uom" style="display: none;">
													${detail.product.lookupDetailByProductUnit.displayName}</td>
												<td><input type="text" class="width96 unitrate"
													id="unitrate_${status.index+1}" value="${detail.unitRate}"
													style="text-align: right;" /></td>
												<td><input type="text" class="width96 quantity"
													id="quantity_${status.index+1}"
													value="${detail.totalUnits}"/></td>
												<td class="totalamount" id="totalamount_${status.index+1}"
													style="text-align: right;"><c:out
														value="${detail.unitRate * detail.totalUnits}" /></td>
												<td style="display: none;"><input type="hidden"
													id="quotationLineId_${status.index+1}" style="border: 0px;"
													value="${detail.quotationDetailId}" />
													<input type="hidden" id="requisitionLineId_${status.index+1}" 
														value="${detail.requisitionDetail.requisitionDetailId}"/>
												</td>
												<td><input type="text" name="linedescription"
													id="linedescription_${status.index+1}"
													value="${detail.description}" /></td>
												<td style="width: 0.01%;" class="opn_td"
													id="option_${status.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${status.index+1}"
													style="display: none; cursor: pointer;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${status.index+1}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${status.index+1}" style="cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${status.index+1}" style="display: none;"
													title="Working"> <span class="processing"></span> </a></td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach var="i" begin="1" end="2" step="1"
											varStatus="status1">
											<tr style="height: 23px;" class="rowid"
												id="fieldrow_${status1.index+1}">
												<c:forEach var="j" begin="1" end="8" step="1"
													varStatus="status1">
													<td></td>
												</c:forEach>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</fieldset>
				</div>
				<div class="clearfix"></div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div class="portlet-header ui-widget-header float-right qdiscard"
						id="quotation_discard">
						<fmt:message key="accounts.common.button.cancel" />
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div style="display: none;"
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
					<div class="portlet-header ui-widget-header float-left addrows"
						style="cursor: pointer;">
						<fmt:message key="accounts.common.button.addrow" />
					</div>
				</div>
			</form>
		</div>
		<div class="clearfix"></div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span> </a>
			</div>
			<div id="common-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>