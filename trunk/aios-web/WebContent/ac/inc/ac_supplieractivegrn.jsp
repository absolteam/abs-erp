<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend><fmt:message key="accounts.grn.label.activegrn"/></legend>
	<div>
		<c:forEach var="ACTIVE_GRN" items="${GOODS_RECEIVE_INFO}">
			<label for="activeGRNNumber_${ACTIVE_GRN.receiveId}" class="width10">
				<input type="checkbox" name="activeGRNNumber" class="activeGRNNumber" id="activeGRNNumber_${ACTIVE_GRN.receiveId}"/>
				${ACTIVE_GRN.receiveNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultGRNSelect($('.activeGRNNumber').attr('id'));
	$('.activeGRNNumber').click(function(){   
		validateGRNInfo($(this));   
	});
});
</script>