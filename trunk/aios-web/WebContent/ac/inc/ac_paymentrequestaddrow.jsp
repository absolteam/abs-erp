<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td id="lineId_${ROW_ID}" style="display: none;">${ROW_ID}</td>
	<td><select name="reference" id="reference_${ROW_ID}"
		class="reference width70">
			<option value="">Select</option>
			<c:forEach var="category" items="${REQUEST_CATEGORY}">
				<option value="${category.lookupDetailId}">${category.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;" id="REQUEST_CATEGORY_${ROW_ID}"
			class="btn ui-state-default ui-corner-all request_category-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td> 
	<td><select id="paymode_${ROW_ID}" class="paymode">
			<c:forEach var="MODE" items="${PAYMENT_MODE}">
				<option value="${MODE.key}">${MODE.value}</option>
			</c:forEach>
	</select>
	</td>
	<td><input type="text" id="amount_${ROW_ID}"
		class="amount validate[optional,custom[number]]"
		style="text-align: right;" />
	</td>
	<td><input type="text" id="description_${ROW_ID}"
		class="description" /> <input type="hidden"
		id="paymentRequestId_${ROW_ID}" />
	</td>
	<td style="width: 0.01%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a>
	</td>
</tr>
<script type="text/javascript">
$(function() {
	$jquery("#requestvalidation").validationEngine('attach'); 
	$jquery(".amount").number(true, 2); 
});
</script>