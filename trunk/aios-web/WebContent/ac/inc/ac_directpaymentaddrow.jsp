<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><input type="text" class="width98 invoiceNumber"
		id="invoiceNumber_${ROW_ID}" value="" /></td>
	<td class="codecombination_info" id="codecombination_${ROW_ID}"
		style="cursor: pointer;"><input type="hidden"
		name="combinationId_${ROW_ID}" id="combinationId_${ROW_ID}" /> <input
		type="text" name="codeCombination_${ROW_ID}" readonly="readonly"
		id="codeCombination_${ROW_ID}" class="codeComb width80"> <span
		class="button" id="codeID_${ROW_ID}"> <a
			style="cursor: pointer;"
			class="btn ui-state-default ui-corner-all codecombination-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text"
		class="width98 amount validate[optional,custom[number]]"
		id="amount_${ROW_ID}" style="text-align: right;" /></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${ROW_ID}" /></td>
	<td style="display: none;"><input type="hidden"
		id="directPaymentLineId_${ROW_ID}" /> <input type="hidden"
		id="paymentRequestLineId_${ROW_ID}" />
		<input type="hidden" id="useCase_${ROW_ID}" />
		<input  type="hidden" id="record_${ROW_ID}" />
	</td>
	<td style="width: 0.01%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working">
			<span class="processing"></span> </a></td>
</tr>
<script type="text/javascript">
	$(function() {
		$jquery("#directPaymentCreation").validationEngine('attach'); 
		$jquery("#amount_${ROW_ID}").number(true, 2); 
	});
</script>