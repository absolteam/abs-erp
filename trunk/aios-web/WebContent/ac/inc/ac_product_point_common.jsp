<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"
		class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> product
	points
</div>
<input type="hidden" id="params" value="${rowId}" />
<div id="hrm">
	<div class="float-right width50"
		style="position: relative; top: -29px;">
		<select name="productpointlist" class="autoComplete width50">
			<option value=""></option>
			<c:forEach items="${PRODUCT_POINTS}" var="bean" varStatus="status">
				<option value="${bean.productProductId}">${bean.statusPoints}</option>
			</c:forEach>
		</select> <span style="margin-top: 2px; cursor: pointer; position: absolute;"
			class="float-right"><img width="24" height="24"
			src="images/icons/Glass.png">
		</span>
	</div>
	<div class="float-left width100 product_point_list" style="padding: 2px;">
		<ul>
			<li class="width30 float-left"><span>Points</span>
			</li>
			<li class="width30 float-left"><span>Purchase Amount</span>
			</li>
			<li
				style="border-bottom: 1px solid #FFE5B1; margin-left: -4px; padding-right: 5px; margin-bottom: 5px;"
				class="width100 float-left"></li>
			<c:forEach items="${PRODUCT_POINTS}" var="bean" varStatus="status">
				 
			</c:forEach>
		</ul>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
		style="position: absolute; top: 86%; left: 92%;">
		<div class="portlet-header ui-widget-header float-right close"
			id="close" style="cursor: pointer;">close</div>
	</div>
</div>
<script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){ 
		       commonCouponMethod($(this).val(), $(".autoComplete option:selected").text(), $("#params").val()); 
 	   } 
	}); 
	 
	 $('.product_point_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.product_point_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#couponNameselect_'+rowid).hasClass('selected')){ 
					 $('#couponNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#couponNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.product_point_list_li').live('dblclick',function(){  
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
 		  commonCouponMethod($('#couponbyid_'+rowid).val(), $('#couponbyname_'+rowid).val(), $("#params").val()); 
 		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
 });
  
 </script>
<style>
.ui-autocomplete-input {
	padding: 0.48em 0 0.47em 0.45em;
	width: 80%;
	position: absolute;
	right: 3px;
}

.ui-button {
	margin: 0px;
}

.ui-autocomplete {
	width: 194px !important;
}

.product_point_list ul li {
	padding: 3px;
}

.product_point_list {
	border: 1px solid #E5E5E5;
	float: left;
	height: 175px;
	overflow-y: auto;
	overflow-x: hidden;
	padding: 4px;
	position: relative;
	top: -25px;
}

.selected {
	background: url("./images/checked.png");
	background-repeat: no-repeat;
}

.width38 {
	width: 38%;
}

.width28 {
	width: 28%;
}
</style>