<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">  
$(function(){
  
	{
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_point_entry.action", 
		     	async: false, 
		     	data:{productPointId: Number($('#productPointId').val())},
				dataType: "json",
				cache: false,
				success: function(response){ 
					$('#productCode').val(response.productPointVO.product.code);
					$('#productId').val(response.productPointVO.product.productId);
					$('#statusPoints').val(response.productPointVO.statusPoints);
					$('#purchaseAmount').val(response.productPointVO.purchaseAmount);
					$('#status').attr('checked',response.productPointVO.status ? true : false); 
					$('#fromDate').val(response.productPointVO.validFrom);
					$('#toDate').val(response.productPointVO.validTo);
					$('#description').val(response.productPointVO.description);
				} 		
			}); 
		} 
	 
});  
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
			points
		</div>
		<div class="portlet-content"> 
			<form id="product_point_validation" style="position: relative;">
				<input type="hidden" id="productPointId" value="${requestScope.productPointId}"/>
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset>
								<div>
									<label class="width30" for="fromDate">From Date<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" name="fromDate" id="fromDate"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="toDate">To Date<span class="mandatory">*</span>  </label> <input type="text"
										readonly="readonly" name="toDate" id="toDate"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30">Description</label>
									<textarea id="description" name="description" class="width51"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label class="width30" for="productCode">Product Name<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" name="productCode" id="productCode"
										class="width50 validate[required]" /> <input type="hidden"
										id="productId" name="productId" />  
								</div>

								<div>
									<label class="width30" for="statusPoints">Status Points<span
										class="mandatory">*</span> </label> <input type="text"
										id="statusPoints" name="statusPoints"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="purchaseAmount">Purchase
										Amount<span class="mandatory">*</span> </label> <input type="text"
										name="purchaseAmount" id="purchaseAmount"
										class="width50 validate[required]" />
								</div>

								<div>
									<label class="width30">Status</label> <select name="status"
										id="status" class="validate[required]" style="width: 51%;">
										<option value="true">Active</option>
										<option value="inactive">In active</option>
									</select>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div> 
				</div>
			</form> 
		</div>
	</div> 
</div>
<div class="clearfix"></div>