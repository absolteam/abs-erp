<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var couponId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 oTable = $('#CouponDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_all_coupons.action",

		"aoColumns" : [ { 
			"mDataProp" : "couponName"
		 }, {
			"mDataProp" : "couponTypeName"
		 }, {
			"mDataProp" : "startDigit"
		 },{
			"mDataProp" : "endDigit"
		 },{
			"mDataProp" : "validFrom"
		 },{
			"mDataProp" : "validTo"
		 },{
			"mDataProp" : "statusValue"
		 }], 
	});	 

	 $('#fromDate,#toDate').datepick({
		 onSelect: customRange, showTrigger: '#calImg'}); 
	 	
	 $jquery("#coupon_validation").validationEngine('attach');  
	 
	$('#edit').click(function(){   
 		if(couponId != null && couponId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_coupon_entry.action", 
		     	async: false, 
		     	data:{couponId: couponId},
				dataType: "json",
				cache: false,
				success: function(response){ 
					 $('#couponName').val(response.couponVO.couponName);
					 $('#couponType').val(response.couponVO.couponType);
					 $('#startDigit').val(response.couponVO.startDigit);
					 $('#endDigit').val(response.couponVO.endDigit);
 					 $('#status').val(response.couponVO.status);
					 $('#fromDate').val(response.couponVO.validFrom);
					 $('#toDate').val(response.couponVO.validTo);
					 $('#description').val(response.couponVO.description);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
 		return false;
	});  

	$('#delete').click(function(){  
		if(couponId != null && couponId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_coupon.action", 
	     	async: false,
	     	data:{couponId: couponId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_coupon.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted..")
																			.slideDown(
																					1000);
																	$(
																			'#success_message')
																			.delay(
																					3000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															3000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert(
										"Please select a record to delete.");
								return false;
							}
							return false;
						});

		/* Click event handler */
		$('#CouponDT tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				couponId = aData.couponId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				couponId = aData.couponId;
			}
		});

		/* Click event handler */
		$('#CouponDT tbody tr').live('dblclick', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				couponId = aData.couponId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				couponId = aData.couponId;
			}
			$('#edit').trigger('click');
		});

		$('.coupon_discard').click(function(){ 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_coupon.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) { 
												$("#main-wrapper").html(result);
											}
										});
							});

		 

		$('.coupon_save').click(function(){   
			if($jquery("#coupon_validation").validationEngine('validate')){
			var couponName = $('#couponName').val();
 			var startDigit = Number($('#startDigit').val()); 
 			var endDigit = Number($('#endDigit').val()); 
  			var couponType = Number($('#couponType').val()); 
 			var fromDate = $('#fromDate').val();
 			var toDate = $('#toDate').val();
 			var status =$('#status').val(); 
			var description =$('#description').val();  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/save_coupon.action", 
		     	async: false,
		     	data:{
		     			couponId : couponId, couponName: couponName, startDigit: startDigit, endDigit: endDigit,
		     			couponType: couponType, fromDate: fromDate, toDate: toDate, status: status, description: description,
		     			recordId: Number(0)
		     		 },
				dataType: "json",
				cache: false,
				success: function(response){  
					if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
						var message = "Record created.";
						if (couponId > 0)
							message = "Record updated.";
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_coupon.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			'#common-popup')
																			.dialog(
																					'destroy');
																	$(
																			'#common-popup')
																			.remove();
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																	'#success_message')
																	.hide()
																	.html(
																			message)
																	.slideDown();

																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else
								return false;
						});
		
		{ 
			couponId = Number($('#couponRecordId').val());
			if(couponId > 0){
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_coupon_entry.action", 
			     	async: false, 
			     	data:{couponId: Number($('#couponRecordId').val())},
					dataType: "json",
					cache: false,
					success: function(response){ 
						 $('#couponName').val(response.couponVO.couponName);
						 $('#couponType').val(response.couponVO.couponType);
						 $('#startDigit').val(response.couponVO.startDigit);
						 $('#endDigit').val(response.couponVO.endDigit);
						 $('#status').val(response.couponVO.status);
						 $('#fromDate').val(response.couponVO.validFrom);
						 $('#toDate').val(response.couponVO.validTo);
						 $('#description').val(response.couponVO.description);
					} 		
				}); 
			} 
		} 
	});

function customRange(dates) {
 	if (this.id == 'fromDate') {
 	 	if($('#fromDate').val()!="")
 	 	 	$('.fromDateformError').hide(); 
 		$('#toDate').datepick('option', 'minDate', dates[0] || null); 
 	}
 	else{
 		if($('#toDate').val()!="")
 	 	 	$('.toDateformError').hide(); 
 		$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
 	}  
 }
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>coupon
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div class="clearfix"></div>
			<form id="coupon_validation" style="position: relative;">
				<input type="hidden" id="couponRecordId" value="${requestScope.couponId}"/>
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 145px;">
								<div>
									<label for="fromDate">From Date<span
										style="color: red;">*</span> </label> <input type="text"
										name="fromDate" id="fromDate" tabindex="7"
										class="width50 validate[required]" readonly="readonly" />
								</div>
								<div>
									<label for="toDate">To Date<span
										style="color: red;">*</span> </label> <input type="text"
										name="toDate" id="toDate" tabindex="8" class="width50 validate[required]"
										readonly="readonly" />
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="9" id="description"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label for="couponName">Coupon Name<span
										style="color: red;">*</span> </label> <input type="text"
										name="couponName" id="couponName" tabindex="1"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="couponType">Coupon Type<span
										style="color: red;">*</span> </label> <select name="couponType"
										id="couponType" class="width51 validate[required]"
										tabindex="2">
										<option value="">Select</option>
										<c:forEach var="couponType" items="${COUPON_TYPES}">
											<option value="${couponType.key}">${couponType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label for="startDigit">Start Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="startDigit" id="startDigit" tabindex="3"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="endDigit">End Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="endDigit" id="endDigit" tabindex="4"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="status">Status<span style="color: red;">*</span>
									</label> <select name="status" id="status"
										class="width51 validate[required]" tabindex="6">
										<option value="true" selected="selected">Active</option>
										<option value="false">Inactive</option>
									</select>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						<div
							class="portlet-header ui-widget-header float-right coupon_discard">
							<fmt:message key="accounts.common.button.cancel" />
						</div>
						<div
							class="portlet-header ui-widget-header float-right coupon_save">
							<fmt:message key="accounts.common.button.save" />
						</div>
					</div>
				</div>
			</form>
			<div id="rightclickarea">
				<div id="coupon_list">
					<table id="CouponDT" class="display">
						<thead>
							<tr>
								<th>Coupon Name</th>
								<th>Coupon Type</th>
								<th>Start Digit</th>
								<th>End Digit</th>
								<th>From Date</th>
								<th>To Date</th>
								<th>Status</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="accounts.common.button.edit" />
				</div>
			</div>
		</div>
	</div>
</div>