<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var posUserTillId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var callType=null;
$(function(){
	$('.formError').remove();
	
	listCall();
	 
		
	 $('.pos-reset').click(function(){
			clear_form_elements();
	});
	
	 $('#employeepopup').click(function(){
		 callType="EMPLOYEE";
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var rowId=-1; 
         var personTypes="1";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 
	 $('#supervisorpopup').click(function(){
		 callType="SUPERVISOR";
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var rowId=-1; 
         var personTypes="1";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 
	 $('.show-store-list').live('click',function(){  
		    $('.ui-dialog-titlebar').remove();   
		    var rowId = 0;//$(this).attr('id').split("_")[1];  
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
				 	async: false,   
				    dataType: "html",
				    data: {rowId: rowId},
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);   
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0); 
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				return false;
			});

	 
	 $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
	 
	/* Click event handler */
	$('#POSUserTill tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  posUserTillId = aData.posUserTillId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          posUserTillId = aData.posUserTillId;
	      }
  	});
	
	/* Click event handler */
	$('#POSUserTill tbody tr').live('dblclick', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  posUserTillId = aData.posUserTillId;
			  editPOSUserTill();
	      }
 		 else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          posUserTillId = aData.posUserTillId;
	      }
  	});
	
	
	
	$jquery("#POSUserTillForm").validationEngine('attach');
	//Save & discard process
	$('.pos-save').click(function(){ 
		$('.error,.success').hide();
		if($jquery("#POSUserTillForm").validationEngine('validate')){  
			var posUserTillId=Number($('#posUserTillId').val());
			var personId=Number($('#personId').val());
			var storeId=Number($('#storeId').val());
			var tillNumber=$('#tillNumber').val();
			var isActive=$('#isActive').attr('checked');
			var supervisorId=Number($('#supervisorId').val());
			var kitchenPrinter= $('#kitchenPrinter').val();
			var barPrinter = $('#barPrinter').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_pos_user_till.action", 
			 	async: false, 
			 	data:{ 
			 		posUserTillId:posUserTillId,
			 		personId:personId,
			 		storeId:storeId,
			 		tillNumber:tillNumber,
			 		isActive:isActive,
			 		kitchenPrinter: kitchenPrinter,
			 		barPrinter: barPrinter,
			 		supervisorId:supervisorId
			 	},
			    dataType: "json",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $('.success').hide().html("Successfully Saved").slideDown(1000);
							listCall();
							clear_form_elements();
					 }else{
						 $('.error').hide().html("Save goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('.error').hide().html("Save goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	$('#delete').click(function(){
		$('.error,.success').hide();
			if(posUserTillId == null || posUserTillId == 0)
			{
				alert("Please select one row to delete");
				return false;
			}
			else
			{		
				var cnfrm = confirm('Selected Details will be deleted permanently');
				if(!cnfrm)
					return false;
				posUserTillId = Number(posUserTillId); 
		
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_pos_user_till.action",  
		     	data: {posUserTillId:posUserTillId},  
		     	async: false,
				dataType: "json",
				cache: false,
				success: function(result)
		     	{  
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listCall();
						 $('.success').hide().html("Information Deleted Successfully.").slideDown();
						 clear_form_elements();
					 }
					 else{
						 $('.error').hide().html(message).slideDown(1000);
						 return false;
					 }
		     	},
		     	error:function(result)
	            {
		     		$('.error').hide().html(message).slideDown(1000);
	            } 
			}); 
			return false;
			}
			return false; 
	});
	
	{
		posUserTillId = Number($('#posUserTillId').val());
		if(posUserTillId > 0) {
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/show_pos_user_till.action",  
		     	data: {posUserTillId: Number($('#posUserTillId').val())},  
		     	async: false,
				dataType: "json",
				cache: false,
				success: function(result){  
					pOSUserTillVOJson=result.pOSUserTillVOJson;
					$('#posUserTillId').val(pOSUserTillVOJson.posUserTillId);
					$('#employeeName').val(pOSUserTillVOJson.personName);
					$('#personId').val(pOSUserTillVOJson.personId);
					$('#storeName').val(pOSUserTillVOJson.storeName);
					$('#storeId').val(pOSUserTillVOJson.storeId);
					$('#tillNumber').val(pOSUserTillVOJson.tillNumber);
					$('#supervisorName').val(pOSUserTillVOJson.supervisorName);
					$('#supervisorId').val(pOSUserTillVOJson.supervisorId);
					$('#kitchenPrinter').val(pOSUserTillVOJson.kitchenPrinter);
					$('#barPrinter').val(pOSUserTillVOJson.barPrinter);
					if(pOSUserTillVOJson.isActive==1)
						$('#isActive').attr("checked",true);
					else
						$('#isActive').attr("checked",false);
						
		     	} 
			}); 
		} 
	 }
});
function editPOSUserTill(){
	$('.error,.success').hide();
		if(posUserTillId == null || posUserTillId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			posUserTillId = Number(posUserTillId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/show_pos_user_till.action",  
	     	data: {posUserTillId:posUserTillId},  
	     	async: false,
			dataType: "json",
			cache: false,
			success: function(result){  
				pOSUserTillVOJson=result.pOSUserTillVOJson;
				$('#posUserTillId').val(pOSUserTillVOJson.posUserTillId);
				$('#employeeName').val(pOSUserTillVOJson.personName);
				$('#personId').val(pOSUserTillVOJson.personId);
				$('#storeName').val(pOSUserTillVOJson.storeName);
				$('#storeId').val(pOSUserTillVOJson.storeId);
				$('#tillNumber').val(pOSUserTillVOJson.tillNumber);
				$('#supervisorName').val(pOSUserTillVOJson.supervisorName);
				$('#supervisorId').val(pOSUserTillVOJson.supervisorId);
				$('#kitchenPrinter').val(pOSUserTillVOJson.kitchenPrinter);
				$('#barPrinter').val(pOSUserTillVOJson.barPrinter);
				if(pOSUserTillVOJson.isActive==1)
					$('#isActive').attr("checked",true);
				else
					$('#isActive').attr("checked",false);
					
	     	},
	     	 error:function(result)
             {
                
             }
		}); 
		return false;
		}
		return false;  


}
function listCall(){
	$('#POSUserTill').dataTable().fnDestroy();
	oTable = $('#POSUserTill')
	.dataTable(
			{
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_pos_till_json.action",

		"aoColumns" : [ { 
			"mDataProp" : "personName"
		 }, {
			"mDataProp" : "storeName"
		 }, {
			"mDataProp" : "tillNumber"
		 }, {
			"mDataProp" : "supervisorName"
		 },{
			"mDataProp" : "statusView"
		 }],
	});	 
}
function personPopupResult(personId,personName,commonParam){
	if(callType=='SUPERVISOR'){
		$('#supervisorName').val(personName);
		$('#supervisorId').val(personId);
	}else{
		$('#employeeName').val(personName);
		$('#personId').val(personId);
	}
	return false;
}

function commonStorePopup(storeId,storeName,commonParam){
	$('#storeName').val(storeName);
	$('#storeId').val(storeId);
	$('#common-popup').dialog('close');
	return false;
}
function clear_form_elements() {
	posUserTillId=null;
	$('.error,.success').hide();
	$('#POSUserTillForm').each(function(){
	    this.reset();
	});
}
</script>
<div id="main-content">
	<c:choose> 
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>POS User</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    
	   	    <form id="POSUserTillForm" name="POSUserTillForm" style="position: relative;">
				<div id="hrm" class="float-left width70"  >
					<input type="hidden" id="posUserTillId"  value="${requestScope.posUserTillId}"/>
					<fieldset style="min-height: 170px;">
						  <legend>POS User Till</legend> 
					      
			      	  	<div>
			      	  		<label class="width30">Employee<span style="color:red">*</span></label>
							<input type="text" id="employeeName" readonly="readonly" class=" width50 validate[required]"
								value=""/>
							
							<span class="button" id="employee"  style="position: relative; top:0px;">
								<a style="cursor: pointer;" id="employeepopup" class="btn ui-state-default ui-corner-all width10"> 
									<span class="ui-icon ui-icon-newwin">
									</span> 
								</a>
							</span> 
							<input type="hidden" id="personId" value="" class="personId" />
						</div>
						<div>
			      	  		<label class="width30">Store<span style="color:red">*</span></label>
							<input type="text" id="storeName" readonly="readonly" class=" width50 validate[required]"
								value=""/>
							
							<span class="button" id="store"  style="position: relative; top:0px;">
								<a style="cursor: pointer;" id="show-store-list" class="btn ui-state-default ui-corner-all show-store-list width10"> 
									<span class="ui-icon ui-icon-newwin">
									</span> 
								</a>
							</span> 
							<input type="hidden" id="storeId" value="" class="storeId" />
						</div>	
										
						<div>
							<label class="width30">Till Number<span style="color:red">*</span></label>
							<input type="text" class="width50 tillNumber validate[required]" name="tillNumber" id="tillNumber"/>
						</div> 
						<div>
			      	  		<label class="width30">Supervisor<span style="color:red">*</span></label>
							<input type="text" id="supervisorName" readonly="readonly" class=" width50 validate[required]"/>
							
							<span class="button" id="supervisor"  style="position: relative; top:0px;">
								<a style="cursor: pointer;" id="supervisorpopup" class="btn ui-state-default ui-corner-all width10"> 
									<span class="ui-icon ui-icon-newwin">
									</span> 
								</a>
							</span> 
							<input type="hidden" id="supervisorId" class="supervisorId" />
						</div>
						<div>
							<label class="width30">Kitchen Printer</label>
							<input type="text" class="width50 kitchenPrinter" name="kitchenPrinter" id="kitchenPrinter" />
						</div> 
						<div>
							<label class="width30">Bar Printer</label>
							<input type="text" class="width50 barPrinter" name="barPrinter" id="barPrinter" />
						</div> 
						<div>
							<label class="width30">Active</label>
							<input type="checkbox" class="width5" id="isActive" />
						</div> 	
						<div class="clearfix"></div>
						<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right delete" id="delete">Delete</div>
								<div class="portlet-header ui-widget-header float-right pos-reset" id="pos-reset">Reset</div>
								<div class="portlet-header ui-widget-header float-right pos-save" id="pos-save">Save</div>
						</div>								
				 </fieldset>
			</div>
		</form>
		 	<div id="rightclickarea">
			 	<div id="promotions_list">
					<table id="POSUserTill" class="display">
						<thead>
							<tr>
								<th>Person</th> 
								<th>Store</th> 
								<th>Till Number</th> 
								<th>Supervisor</th>  
								<th>Status</th> 
 							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Delete</span></div>
				<div class="sep_li"></div>		 
 			</div>
			
		</div>
	</div>
	<div id="common-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
	</div>
</div>