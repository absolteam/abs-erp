<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var productId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	 if(typeof($('#codecombination-popup')!="undefined")){  
		 $('#codecombination-popup').dialog('destroy');		
		 $('#codecombination-popup').remove();  
	 } 
	$('.formError').remove();
	$('#example').dataTable({ 
		"sAjaxSource": "product_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Product_ID", "bVisible": false},
			{ "sTitle": "Product Code"},
			{ "sTitle": "Product Name"},
			{ "sTitle": 'Unit Name'}, 
			{ "sTitle": 'Item Type'},
			{ "sTitle": 'Item Nature'}, 
			{ "sTitle": 'Status'},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){  
		var showPage="add";
		productId: Number(0);
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/product_addentry.action", 
	     	async: false,
	     	data:{productId: productId, showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#edit').click(function(){   
		var showPage="edit";  
		if(productId>0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_addentry.action", 
		     	async: false,
		     	data:{productId: productId, showPage:showPage},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
					$.scrollTo(0,300);
				} 		
			}); 
		}else{
			$('#error_message').hide().html("Please select record to edit.").slideDown(1000);
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	}); 

	$('#delete').click(function(){  
		$('#success_message').hide();
		if(productId>0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_deletentry.action", 
		     	async: false,
		     	data:{productId:productId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);  
					 var message=$('#returnMsg').html(); 
					 if(message.trim()=="Success"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/product_retrieve.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Product Deleted.").slideDown(1000);
									$('#success_message').delay(3000).slideUp();
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(3000).slideUp();
						 return false;
					 } 
				} 		
			}); 
		}else{
			$('#error_message').hide().html("Please select record to delete.").slideDown(1000);
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	}); 

	$('#view').click(function(){  
		window.open('product_print_report.action?format=HTML','','width=1000,height=800,scrollbars=yes,left=100px');  
	});

	//init datatable
	oTable = $('#example').dataTable();
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          productId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          productId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Product Information</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="journal_accounts">
						<table class="display" id="example"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>View</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div>
 				<div class="portlet-header ui-widget-header float-right" id="edit"  ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>