<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var reconciliationId = 0;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 

$(function (){ 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('.formError').remove(); 
	$('#bank_reconciliation').dataTable({ 
 		"sAjaxSource": "bank_reconciliation_list.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	    "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Bank_Reconcile_ID', "bVisible": false,"bSearchable": false},
 			{ "sTitle": 'Reference'},
 			{ "sTitle": 'Account No.'}, 
 			{ "sTitle": 'Date'}, 
 			{ "sTitle": 'Statement Date',}, 
 			{ "sTitle": 'Statement Ref.'}, 
 			{ "sTitle": 'Account Balance'}, 
 			{ "sTitle": 'Statement Balance'}, 
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#bank_reconciliation').dataTable();

 	/* Click event handler */
 	$('#bank_reconciliation tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	       aData =oTable.fnGetData( this );
 	       reconciliationId=aData[0]; 
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	       aData =oTable.fnGetData( this );
 	       reconciliationId=aData[0];
 	       
 	    }
 	});
 	
 	$('#add').click(function(){
 		$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/bank_reconciliation_entry.action",
 			data: {reconciliationId : 0,recordId:Number(0)}, 
 			async:false,
 			dataType:"html",
 			cache:false,
 			success:function(result){
 				$("#main-wrapper").html(result);
 			}
 		});	
 	});
 	
 	$('#edit').click(function(){
 		if(reconciliationId!=null && reconciliationId!=0) {
	 		$.ajax({
	 			type:"POST",
	 			url:"<%=request.getContextPath()%>/bank_reconciliation_entry.action",
	 			data: {reconciliationId : reconciliationId, recordId:Number(0)}, 
	 			async:false,
	 			dataType:"html",
	 			cache:false,
	 			success:function(result){
	 				$("#main-wrapper").html(result);
	 			}
	 		});	
 		}else{
 			alert("Please select a record to delete.");
 			return false;
 		}
 	});

 	$('#view').click(function(){  
 		if(reconciliationId!=null && reconciliationId!=0) {
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/bank_reconciliation_view.action", 
		     	async: false,
		     	data:{reconciliationId: reconciliationId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				}
			 });
		} else{
			alert("Please select a record to edit.");
			 return false;
		 }
	}); 
 	
 	$("#print").click(function() {  
		if(reconciliationId>0){	 
			window.open('show_bankreconciliation_printout.action?reconciliationId='+ reconciliationId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});

 	$('#delete').click(function(){  
 		if(reconciliationId!=null && reconciliationId!=0) {
 			var cnfrm = confirm("Selected record will be permanently deleted.");
 			if(!cnfrm)
 				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_bank_reconciliation.action", 
		     	async: false,
		     	data:{reconciliationId: reconciliationId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_bank_reconciliation_list.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
									$('#success_message').delay(2000).slideUp();
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(2000).slideUp();
						 return false;
					 }
				}
			 });
		} else{
			 alert("Please select a record to delete.");
			 return false;
		 }
	}); 
}); 
</script> 
<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Bank Reconciliation</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<div class="tempresult" style="display:none;"></div> 
	    	<div id="rightclickarea">
				<div id="gridDiv">
					<table class="display" id="bank_reconciliation"></table>
				</div>	
			</div>		
			<div class="vmenu">
	 	      	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
 				<div class="first_li"><span>Delete</span></div>
			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="print"><fmt:message key="accounts.common.button.print"/></div>	
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>	  
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div> 