<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Income Statement</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td { 
	height: 28px;
}
th{
	border-bottom: 3px double #000;
}
.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
.align-left{text-align: left!important;}
.lstln td{  
	 font-weight: bold;
 }
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"
			style="font-family: book; font-style: italic; text-transform: inherit !important; 
				font-size: 28px !important; color: black;"><strong>${THIS.companyName}</strong>
		</span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading" style="font-family: book;"><span><u>INCOME STATEMENT REPORT <br/> 
		<c:if test="${INCOME_STATEMENT.fromDate ne null && INCOME_STATEMENT.fromDate ne ''}">
			 FROM ${INCOME_STATEMENT.fromDate}  
		</c:if>  
		<c:if test="${INCOME_STATEMENT.toDate ne null && INCOME_STATEMENT.toDate ne ''}">
			 TO ${INCOME_STATEMENT.toDate}  
		</c:if> 
		</u> </span> </span>
	</div>
	<div style="clear: both;"></div>
	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; 
			-moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
		class="width100 float_left">
		<table style="width: 99%; margin: 0 auto;">  
			<tr> 
				<th class="align-left width90 float_left">REVENUE</th> 
			</tr> 
		</table>
		<table style="width: 99%; margin: 0 auto;">  
			<c:forEach items="${INCOME_STATEMENT.transactionDetailVOs}"
				var="detail">
				<tr> 
					<td class="align-left width70 float_left">${detail.accountDescription}</td> 
  					<td class="align-right width20 float_left">${detail.revenueAmount}</td>
  				</tr>
			</c:forEach>  
			<tr class="lstln">
				<td class="align-left width70 float_left">TOTAL</td> 
				<td class="align-right width20 float_left">${INCOME_STATEMENT.revenueAmount}</td> 
 			</tr>
		</table>
	</div>
	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; 
			-moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
		class="width100 float_left">
		<table style="width: 99%; margin: 0 auto;">  
			<tr> 
				<th class="align-left width90 float_left">EXPENSE</th> 
			</tr> 
		</table>
		<table style="width: 99%; margin: 0 auto;">  
			<c:forEach items="${INCOME_STATEMENT.transactionDetailVOCs}"
				var="detail">
				<tr> 
					<td class="align-left width70 float_left">${detail.accountDescription}</td> 
  					<td class="align-right width20 float_left">${detail.expenseAmount}</td>
  				</tr>
			</c:forEach>  
			<tr class="lstln">
				<td class="align-left width70 float_left">TOTAL</td> 
				<td class="align-right width20 float_left">${INCOME_STATEMENT.expenseAmount}</td> 
 			</tr>
		</table>
		
		<table style="width: 99%; margin: 0 auto;">  
			<tr class="lstln"> 
				<td class="align-left width10 float_left">${INCOME_STATEMENT.label}</td> 
				<td class="align-left width20 float_left">${INCOME_STATEMENT.netAmount}</td> 
			</tr> 
		</table>
	</div>
	<div class="clear-fix"></div>
</body>
</html>