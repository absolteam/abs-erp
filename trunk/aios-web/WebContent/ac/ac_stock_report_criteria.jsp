<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete-input {
	min-height: 26px !important;
	width: 50% !important;
}

 
label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;
var selectedStoreId = 0;
var selectedProductId = 0;
var selectedSectionId = 0;
var selectedRackId = 0;
var selectedShelfId = 0;
populateDatatable();

 $('#stores').combobox({
	selected : function(event, ui) {
		selectedStoreId = $('#stores :selected').val(); 
		$('#sectiondiv').hide();
		$('#rackdiv').hide();
		$('#shelfdiv').hide();
		$('#productdiv').hide();
		$('#sections').html('');
		$('#sections')
			.append(
				'<option value="">Select</option>');
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_store_section.action",
				async : false,
				data: {storeId: selectedStoreId},
				dataType : "json",
				cache : false,
				success : function(response) {
					if(response.aaData != null){
						$('#sectiondiv').show();
						$(response.aaData)
						.each(
								function(index) {
							$('#sections')
								.append('<option value='
										+ response.aaData[index].aisleId
										+ '>' 
										+ response.aaData[index].sectionName
										+ '</option>');
						}); 
					}  
				}
		}); 
	}
}); 

 $('#sections').combobox({ 
		selected : function(event, ui) { 
			$('#rackdiv').hide();
			$('#shelfdiv').hide();
			$('#productdiv').hide();
			$('#racks').html('');
			$('#racks')
				.append(
					'<option value="">Select</option>');
			selectedSectionId = $('#sections :selected').val();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_store_rack.action",
					async : false,
					data: {sectionId: selectedSectionId},
					dataType : "json",
					cache : false,
					success : function(response) {
						if(response.aaData != null){
							$('#rackdiv').show();
							$(response.aaData)
							.each(
									function(index) {
								$('#racks')
									.append('<option value='
											+ response.aaData[index].shelfId
											+ '>' 
											+ response.aaData[index].name
											+ '</option>');
							}); 
						} 
					}
			});  
		}
	});
 
 $('#racks').combobox({
		selected : function(event, ui) { 
			$('#shelfdiv').hide();
			$('#productdiv').hide();
			$('#shelfs').html('');
			$('#shelfs')
				.append(
					'<option value="">Select</option>');
			selectedRackId = $('#racks :selected').val();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_store_shelf.action",
					async : false,
					data: {rackId: selectedRackId},
					dataType : "json",
					cache : false,
					success : function(response) {
						if(response.aaData != null){
							$('#shelfdiv').show();
							$(response.aaData)
							.each(
									function(index) {
								$('#shelfs')
									.append('<option value='
											+ response.aaData[index].shelfId
											+ '>' 
											+ response.aaData[index].name
											+ '</option>');
							}); 
						}  
					}
			});  
		}
	});
 
 $('#shelfs').combobox({
		selected : function(event, ui) {  
			$('#productdiv').hide();
			$('#products').html('');
			$('#products')
				.append(
					'<option value="">Select</option>');
			selectedShelfId = $('#shelfs :selected').val();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_store_shelf_product.action",
					async : false,
					data: {shelfId: selectedShelfId},
					dataType : "json",
					cache : false,
					success : function(response) {
						if(response.aaData != null){
							$('#productdiv').show();
							$(response.aaData)
							.each(
									function(index) {
								$('#products')
									.append('<option value='
											+ response.aaData[index].productId
											+ '>' 
											+ response.aaData[index].productName
											+ '</option>');
							}); 
						}  
					}
			}); 
		}
	});

	$('#list_grid').click(function(){
		populateDatatable();
	});
	
$('#products').combobox({
	selected : function(event, ui) {
		selectedProductId = $('#products :selected').val(); 
	}
});


function populateDatatable(){
	$('#StockReportDT').dataTable({ 
		"sAjaxSource": "get_stock_report_list_json.action?storeId="+selectedStoreId+"&sectionId="+selectedSectionId+"&rackId="+selectedRackId
						+"&shelfId="+selectedShelfId+"&productId="+selectedProductId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "Store Id", "bVisible": false},
			{ "sTitle": "Code"},
			{ "sTitle": "Product"},
			{ "sTitle": "Product Unit"},
			{ "sTitle": "Store"}, 
			{ "sTitle": "Shelf", "bVisible": false},
			{ "sTitle": "Quantity"},
	     ], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#StockReportDT').dataTable();
	selectedRowId = 0;
}

var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){ 
		window.open("<%=request.getContextPath()%>/get_stock_report_XLS.action?storeId="+selectedStoreId+"&sectionId="+selectedSectionId+"&rackId="+selectedRackId
				+"&shelfId="+selectedShelfId+"&productId="+selectedProductId,
			'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px'); 
		return false; 
	});
 	
 	$(".print-call").click(function(){  
			window.open("<%=request.getContextPath()%>/get_stock_report_printout.action?storeId="+selectedStoreId+"&sectionId="+selectedSectionId+"&rackId="+selectedRackId
				+"&shelfId="+selectedShelfId+"&productId="+selectedProductId+"&format=HTML",
			'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
 		 
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){  
 		window.open("<%=request.getContextPath()%>/get_stock_report_pdf.action?storeId="+selectedStoreId+"&sectionId="+selectedSectionId+"&rackId="+selectedRackId
				+"&shelfId="+selectedShelfId+"&productId="+selectedProductId
													+ "&format=PDF", '_blank',
											'width=800,height=700,scrollbars=yes,left=100px,top=2px'); 
						return false;

					});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Stock
			Report
		</div>
		<div class="portlet-content">
			<div id="error_message" class="response-msg error ui-corner-all"
				style="display: none;"></div>
			<div id="hrm" class="width100">
				<fieldset>
					<div class="width50 float-left">
						<div>
							<label class="width10" for="stores">Store</label> <select
								id="stores" class="width50">
								<option value="">Select</option>
								<c:forEach var="st" items="${STORE_LIST}">
									<option value="${st.storeId}">${st.storeName}</option>
								</c:forEach>
							</select>
						</div>
						<div id="sectiondiv" style="display: none;">
							<label class="width10" for="sections">Section</label> <select
								id="sections" class="width50">
								<option value="">Select</option>
							</select>
						</div>
						<div id="rackdiv" style="display: none;">
							<label class="width10" for="racks">Rack</label> <select
								id="racks" class="width50">
								<option value="">Select</option>
							</select>
						</div>
					</div>
					<div class="width48 float-left">
						<div id="shelfdiv" style="display: none;">
							<label class="width10" for="shelfs">Shelf</label> <select
								id="shelfs" class="width50">
								<option value="">Select</option>
							</select>
						</div>
						<div id="productdiv" style="display: none;">
							<label class="width10" for="products">Product</label> <select
								id="products" class="width50">
								<option value="">Select</option>
							</select>
						</div> 
					</div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
						style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
						<div class="portlet-header ui-widget-header float-right"
							id="list_grid" style="cursor: pointer; color: #fff;">
							list gird
						</div> 
					</div>
				</fieldset>
			</div>
			<div class="tempresult" style="display: none; width: 100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="StockReportDT">

					</table>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span> </a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>