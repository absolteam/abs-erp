<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css"">
.void_payment_view {
	display: block;
	height: 20px;
	width: 20px;
	background: url(images/notification-bg.png) no-repeat;
	background-position: center;
	float: right;
}
</style>
<script type="text/javascript"> 
var paymentId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
	$('#DOMWindow').remove();
	$('#DOMWindowOverlay').remove();
 	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#codecombination-popup').dialog('destroy');		
	$('#codecombination-popup').remove();  
	
	$('#InvoicePaymentList').dataTable({ 
		"sAjaxSource": "getpayment_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Payment_Id", "bVisible": false},
			{ "sTitle": '<fmt:message key="account.payment.paymentNumber" />'},
			{ "sTitle": '<fmt:message key="account.payment.paymentDate" />'},  
			{ "sTitle": '<fmt:message key="account.payment.accountNo" />'}, 
			{ "sTitle": '<fmt:message key="account.payment.chequedate" />'}, 
			{ "sTitle": '<fmt:message key="account.payment.chequeNumber" />'}, 
 			{ "sTitle": 'Supplier'}, 
		], 
		"sScrollY": $("#main-content").height() - 235, 
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){   
		paymentId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/payment_addentry.action", 
			data:{paymentId: paymentId},
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
			} 		
		}); 
	});  

	$('#edit').click(function(){    
		if(paymentId > 0) {
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/payment_addentry.action", 
				data:{paymentId:paymentId},
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result); 
				} 		
			}); 
		} else {
			 alert("Please select a record to edit.");
			 return false;
		} 
	});  

	$('#delete').click(function(){   
		if(paymentId!=null && paymentId!=0){
			 var cnfrm = confirm('Selected record will be deleted permanently');
				if(!cnfrm)
					return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/payment_deletentry.action", 
	     	async: false,
	     	data:{paymentId:paymentId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);   
				var message=$.trim($('.tempresult').html()); 
				if(message!=null && message=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/getpayment_detaillist.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown();  
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown();
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$('#print').click(function(){  
		if(paymentId>0){
			// HTML PRINT  
			window.open('payment_print.action?paymentId='+ paymentId +'&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px'); 
		}else{
			alert("Please select a record to print.");
			return false;
		} 
	}); 

	$('#chequePrint').click(function(){  
   		// HTML PRINT  
   		if(paymentId>0){
   			if('${THIS.templatePrint}' == null || '${THIS.templatePrint}' == "false" || '${THIS.templatePrint}'== '') {  
   	    		window.open('payment_print.action?paymentId='+ paymentId +'&format=HTML', '',
   							'width=800,height=800,scrollbars=yes,left=100px'); 
   	   		} 
   	   		// TEMPLATE PRINT
    	   	else if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true") { 
   	   			$.ajax({
   	   				type:"POST",
   	   				url: '<%=request.getContextPath()%>/payment_print.action?', 
   	   				async: false, 
   	   				data : {paymentId: paymentId},
   	   				dataType: "html",
   	   				cache: false,
   	   				success:function(result){   
   	   					window.open('<%=request.getContextPath()%>/printing/applet/ac_payment_cheque_print.html',
																	'_blank',
																	'width=450,height=150,scrollbars=yes,left=200px,top=200px');
												},
												error : function(result) {
													alert("Error");
												}
											});
									return false;
								}
							} else {
								alert(
										"Please select a record to cheque print.");
								return false;
							}
						});

		//init datatable
		oTable = $('#InvoicePaymentList').dataTable();

		/* Click event handler */
		$('#InvoicePaymentList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				paymentId = aData[0];
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				paymentId = aData[0];
			}
		});
		return false;
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Payments
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<c:if
				test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">
					<fmt:message key="${requestScope.success}" />
				</div>
			</c:if>
			<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">
					<fmt:message key="${requestScope.error}" />
				</div>
			</c:if>
			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="purchase_payment_list">
					<table class="display" id="InvoicePaymentList"></table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Add</span>
				</div>
				<div class="sep_li"></div>
				<div class="first_li" style="display: none;">
					<span>Edit</span>
				</div>
				<div class="first_li" style="display: none;">
					<span>View</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="chequePrint">Cheque Print</div>
				<div class="portlet-header ui-widget-header float-right" id="print">
					<fmt:message key="accounts.common.button.print" />
				</div>
				<!--<div class="portlet-header ui-widget-header float-right" id="view" style="display: none;"><fmt:message key="accounts.common.button.view"/></div>
				-->
				<div class="portlet-header ui-widget-header float-right" id="edit"
					style="display: none;">
					<fmt:message key="accounts.common.button.edit" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="add">
					<fmt:message key="accounts.common.button.add" />
				</div>
			</div>
		</div>
	</div>
</div>