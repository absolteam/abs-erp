<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
 .ui-autocomplete-input { width:43%!important; min-height: 25px!important;}
	  .ui-autocomplete{height:250px;
	  	overflow-y: auto;
	  	overflow-x: hidden;
	  	}
</style>
<script type="text/javascript"> 
var combinationAccounts = "";
var combinationId = Number(0);
var fromDate = ""; var toDate = "";
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){  
	$('.autoComplete option').each(function(){
		$(this).text($(this).text().replace(/[\s\n\r]+/g, ' ' ).trim()); 
	});
	
	//loadJsonTransaction(categoryId, combinationAccounts, fromDate, toDate);  
	
	$('.print-call').click(function(){ 
 		fromDate=$('#fromDate').val();
		toDate=$('#toDate').val();  
		window.open('show_ledger_html_report.action?combinationId='
							+ combinationId + '&fromDate=' + fromDate
							+ '&toDate=' + toDate + '&format=HTML', '',
							'width=800,height=800,scrollbars=yes,left=100px');  
		return false;
	}); 

	$(".xls-download-call").click(function(){   
		fromDate=$('#fromDate').val();
		toDate=$('#toDate').val();  
		window.open('show_ledger_excel_report.action?combinationId='
				+ combinationId + '&fromDate=' + fromDate
				+ '&toDate=' + toDate, '_blank',
				'width=0,height=0');  
	}); 
	
	$('.pdf-download-call').click(function(){
		if(transactionId==0) {
			categoryId=Number($('#categoryName').val());
			codeCombinationId= Number($('#combinationSelectTree').val());
			accountList= $('#selected-accounts-value').val(); 
			fromDate=$('#fromDate').val();
			toDate=$('#toDate').val();  
			window.open('show_ledger_report.action?combinationId='
							+ combinationId + '&fromDate=' + fromDate
							+ '&toDate=' + toDate + '&accountList='+ accountList+'&format=HTML', '',
							'width=800,height=800,scrollbars=yes,left=100px'); 
		} else {
			window.open('show_journal_report.action?journalId='+ transactionId + '&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');
		} 
	});  
	$('.autoComplete').comboboxpos({
		selected : function(event, ui) {
			$(".autoComplete option:selected").text().replace(/[\s\n\r]+/g, ' ').trim();
			var combtext = $(".autoComplete option:selected").text().replace(/[\s\n\r]+/g, ' ').trim();
			$('.ui-autocomplete-input').val(combtext);
			combinationId = Number($(this).val());  
		}
	});
	 
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
		$('#themeRollerSelect option:not(:selected)').remove();
	}
	$('#fromDate,#toDate').datepick({
		onSelect : customRange,
		showTrigger : '#calImg'
	});
	
	$('.reset-call').click(function(){
		transactionId = 0;
		clear_form_elements();
		checkDataTableExsist(categoryId, combinationAccounts, fromDate, toDate);
	});
});
function filterJournalVoucher(combinationAccounts){
	categoryId = $('#categoryName').val();
	fromDate =$('#fromDate').val();
	toDate =$('#toDate').val();
	if(categoryId!='' && fromDate!='' && toDate!='' && combinationAccounts!='')
		checkDataTableExsist(categoryId, combinationAccounts, fromDate, toDate); 
}
function checkDataTableExsist(categoryId, combinationAccounts, fromDate, toDate){
	oTable = $('#GeneralLedgerRDT').dataTable();
	oTable.fnDestroy();
	$('#GeneralLedgerRDT').remove(); 
	$('#transactions').html("<table class='display' id='GeneralLedgerRDT'></table>"); 
	loadJsonTransaction(categoryId, combinationAccounts, fromDate, toDate);
}
function loadJsonTransaction(categoryId, combinationAccounts, fromDate, toDate){ 
	$('#GeneralLedgerRDT').dataTable({ 
		"sAjaxSource": "general_ledger_jsonlist.action?categoryId="+categoryId+"&fromDate="+fromDate+"&toDate="+toDate
						+"&combinationAccountList="+combinationAccounts,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "JOURNAL_ID", "bVisible": false}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.vouchernumber"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.jvdate"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.periodname"/>'},
			{ "sTitle": '<fmt:message key="accounts.jv.label.currencycode"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.categoryname"/>'},
			{ "sTitle": '<fmt:message key="accounts.jv.label.entrydate"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.description"/>'},
		], 
		"sScrollY": $("#main-content").height() - 278,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']],  
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	//init datatable
	oTable = $('#GeneralLedgerRDT').dataTable();
}
function checkLinkedDays() {
	var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
			'#selectedMonth').val());
	$('#selectedDay option:gt(27)').attr('disabled', false);
	$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
			true);
	if ($('#selectedDay').val() > daysInMonth) {
		$('#selectedDay').val(daysInMonth);
	}
}
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	}
}
function clear_form_elements() {
	$('#page-error').hide();
	$('#category_details').each(function(){
	    this.reset();
	});
	$('#selected-accounts-text').html('');
	$('#selected-accounts-value').val('');
	$('#fromDate').val('');
	$('#toDate').val(''); 
}

</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general ledger report</div>	
			 <div class="portlet-content">
			 <form name="category_details" id="category_details"> 
			 	<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right" style="display:none;"> 
						<fieldset style="min-height: 50px;">  
							
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset> 
							<div class="float-left width98">  
								<label class="width30">Account Code<span class="mandatory">*</span></label>
					  			<select name="comb_tree" id="combinationSelectTree" class="autoComplete width60">
					  				<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}" varStatus="status1">
					  				<option value=""></option>
					  					<option value="${bean.combinationId}">${bean.accountByCompanyAccountId.account} [${bean.accountByCompanyAccountId.code}]
					  							<c:choose>
													<c:when test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
														<c:forEach var="costbean" items="${bean.costVos}" varStatus="cstatus1">
															<option value="${costbean.combinationId}">${costbean.accountByCostcenterAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
																<c:choose>
																	<c:when test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
																		<c:forEach var="naturalbean" items="${costbean.naturalVos}" varStatus="nstatus1">
																			<option value="${naturalbean.combinationId}">
																				${naturalbean.accountByNaturalAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}]
																					<c:choose>
																						<c:when test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
																							<c:forEach var="analysisbean" items="${naturalbean.analysisVos}" varStatus="astatus1">
																								<option value="${analysisbean.combinationId}">
																									${analysisbean.accountByAnalysisAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}]
																									<c:choose>
																										<c:when test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0}">
																											<c:forEach var="buffer1bean" items="${analysisbean.buffer1Vos}" varStatus="b1status1">
																												<option value="${buffer1bean.combinationId}">
																													${buffer1bean.accountByBuffer1AccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}]
																														<c:choose>
																   															<c:when test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
																   																<c:forEach var="buffer2bean" items="${buffer1bean.buffer2Vos}" varStatus="b2status">
																   																	<option value="${buffer2bean.combinationId}">${buffer2bean.accountByBuffer2AccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}]
																   																	</option>
																   																</c:forEach>
																   															</c:when>
																   														</c:choose>	
																												</option>		
																											</c:forEach>
																										</c:when>
																									</c:choose>
																								</option>		
																							</c:forEach>
																						</c:when>
																					</c:choose>
																			</option>		
																		</c:forEach>
																	</c:when>
																</c:choose>
															</option>
														</c:forEach> 
													</c:when>
												</c:choose>	
					  					</option>
					  				</c:forEach>
					  			</select> 
					  		</div>  
					  		<div class="float-left width98" style="display: none;">  
					  			<label class="width30">Period(s)</label>
					  			
					  		</div>
					  		<div>
								<label class="width30" for="fromDate">From Date</label>  
								<input type="text" id="fromDate" class="width50" readonly="readonly"/> 
							</div> 
				  			<div>
								<label class="width30" for="toDate">To Date</label>  
								<input type="text" id="toDate" class="width50" readonly="readonly"/> 
							</div>  
						</fieldset>
					</div> 
				</div> 
				<div id="transactions">
					<table class="display" id="GeneralLedgerRDT"></table>
				</div> 
	  		  </form> 
  		  </div>
 	</div> 
</div> 
<div class="process_buttons">
		<div id="hrm" class="width100 float-right ">
		  	<div class="width5 float-right height30" style="display: none;" title="Reset"><img width="30" height="30" src="images/reset.png" class="reset-call" style="cursor:pointer;"/></div>
		  	<div class="width5 float-right height30" title="Download as PDF" style="display: none;"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call"></div>
		 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
		  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
		 </div>
</div> 