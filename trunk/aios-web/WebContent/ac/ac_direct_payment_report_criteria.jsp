<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var directPaymentId =0;
var oTable;

var fromDate = null;
var toDate = null;
var payeeTypeId = 0;
var paymentModeId = 0;
var isBankAccount = false;
var isCustomer = false;
var isSupplier = false;
var isOthers = false;
var isPDC = false;


populateDatatable();
	
function populateDatatable(){
	
	$('#DirectPaymentRT').dataTable({ 
		"sAjaxSource": "get_direct_payment_list_json.action?paymentModeId="+paymentModeId+"&payeeTypeId="+payeeTypeId+"&toDate="+toDate+"&fromDate="+fromDate
				+"&isBankAccount="+isBankAccount+"&isCustomer="+isCustomer+"&isSupplier="+isSupplier+"&isOthers="+isOthers+"&isPDC="+isPDC,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Direct Payment", "bVisible": false},
			{ "sTitle": "Payment NO."},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Payment Mode"}, 
			{ "sTitle": "Amount"},
			{ "sTitle": "Payable"}, 
			{ "sTitle": "Bank Name"}, 
			{ "sTitle": "Account NO."},  
			{ "sTitle": "Cheque NO."},
			{ "sTitle": "Cheque Date"},
			{ "sTitle": "Flag", "bVisible": false},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
	});
	//Json Grid
	//init datatable
	oTable = $('#DirectPaymentRT').dataTable();
}


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		
		window.open("<%=request.getContextPath()%>/get_direct_payment_reportxls.action?paymentModeId="+paymentModeId+"&payeeTypeId="+payeeTypeId+"&toDate="+toDate+"&fromDate="+fromDate
				+"&isBankAccount="+isBankAccount+"&isCustomer="+isCustomer+"&isSupplier="+isSupplier+"&isOthers="+isOthers+"&isPDC="+isPDC+"&format=HTML",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px'); 
		return false; 
		
		
	});
 	
 	$(".print-call").click(function(){ 
 		window.open("<%=request.getContextPath()%>/get_direct_payment_printout.action?paymentModeId="+paymentModeId+"&payeeTypeId="+payeeTypeId+"&toDate="+toDate+"&fromDate="+fromDate
				+"&isBankAccount="+isBankAccount+"&isCustomer="+isCustomer+"&isSupplier="+isSupplier+"&isOthers="+isOthers+"&isPDC="+isPDC+"&format=HTML",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
 	

 	$(".pdf-download-call").click(function(){ 
	 	
 		window.open("<%=request.getContextPath()%>/get_direct_payment_PDF.action?paymentModeId="+paymentModeId+"&payeeTypeId="+payeeTypeId+"&toDate="+toDate+"&fromDate="+fromDate
				+"&isBankAccount="+isBankAccount+"&isCustomer="+isCustomer+"&isSupplier="+isSupplier+"&isOthers="+isOthers+"&isPDC="+isPDC+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
	
	 $('#paymentMode').change(function(){
		if($(this).val()!=null && $(this).val()!=''){
			paymentModeId = $('#paymentMode').val()
		}
		populateDatatable();
	});
	 
	 $('#payeeType').change(function(){
		if($(this).val()!=null && $(this).val()!=''){
			payeeTypeId = $('#payeeType').val();
		}
		populateDatatable();
	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
	
	$('#bankAccount').change(function() {
		isBankAccount = $('#bankAccount').is(':checked');
		populateDatatable();
	});
	
	$('#customer').change(function() {
		isCustomer = $('#customer').is(':checked');
		populateDatatable();
	});
		
	$('#supplier').change(function() {
		isSupplier = $('#supplier').is(':checked');
		populateDatatable();
	});
	
	$('#others').change(function() {
		isOthers = $('#others').is(':checked');
		populateDatatable();
	});
	
	$('#pdc').change(function() {
		isPDC = $('#pdc').is(':checked');
		populateDatatable();
	});
	
	$('#paymentMode').combobox({
		selected : function(event, ui) {
			paymentModeId = $(this).val();
			populateDatatable();
		}
	});
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Direct Payment Report
		</div>
		<div class="portlet-content">
			<div class="width40 float-left" style="padding: 5px;">
				<div>
					<label class="width30">Payment Mode</label>
					<select name="paymentMode" class="width60 modeOfPay" id="paymentMode">
					 	<option value="">Select</option>
					 	<c:choose>
					 		<c:when test="${PAYMENT_MODE ne null && PAYMENT_MODE ne ''}">
					 			<c:forEach items="${PAYMENT_MODE}" var="MODE">
					 				<option value="${MODE.key}">${MODE.value}</option>
					 			</c:forEach> 
					 		</c:when>
					 	</c:choose> 
					 </select>
				</div>
			</div>

			<div class="width40 float-right" style="padding: 5px;">
				<div >
					&nbsp;
					<label class="width30" style="display: none;">Payee Type</label> 
					<select name="payeeType" id="payeeType" class="width60" style="display: none;">
						<option value="">Select</option>  
						<c:forEach items="${PAYEE_TYPES}" var="PAYEE">
							<option value="${member.key}">${member.value}</option>
						</c:forEach> 
					</select>
				
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /> :</label> <input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /> :</label> <input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
			</div>
			<div class="float-left">
				 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<s:checkbox name="bankAccount" id="bankAccount"/> Bank Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<s:checkbox name="supplier" id="supplier"/> Supplier &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<s:checkbox name="customer" id="customer"/> Customers &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<s:checkbox name="others" id="others"/> Others &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<s:checkbox name="pdc" id="pdc"/> PDC
			</div>
			<br><br>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="DirectPaymentRT">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>