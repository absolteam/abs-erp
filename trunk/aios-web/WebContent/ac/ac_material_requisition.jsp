<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var materialRequisitionId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData=""; var editStatus = false;
$(function(){
	 $('.formError').remove(); 
 	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
	 oTable = $('#MaterialRequisitionList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allmaterial_requisition.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 },{
			"mDataProp" : "date"
		 },{
			"mDataProp" : "storeName"
		 },{
			"mDataProp" : "transferReference"
		 },{
			"mDataProp" : "status"
		 },{
			"mDataProp" : "personName"
		 } ], 
	});	 
	 oTable.fnSort( [ [0,'desc'] ] );	
	 
	$('#add').click(function(){   
		materialRequisitionId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/material_requisition_entry.action", 
	     	async: false, 
	     	data:{materialRequisitionId: materialRequisitionId,recordId:0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
 		if(materialRequisitionId != null && materialRequisitionId != 0){
 			if(!editStatus){
				$('#error_message').hide().html("Can not edit!!! Requisition is processed.").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/material_requisition_entry.action", 
		     	async: false, 
		     	data:{materialRequisitionId: materialRequisitionId,recordId:0},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(materialRequisitionId != null && materialRequisitionId != 0){
			if(!editStatus){
				$('#error_message').hide().html("Can not delete!!! Requisition is processed.").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/material_requisition_delete.action", 
	     	async: false,
	     	data:{materialRequisitionId: materialRequisitionId,alertId: 0},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_material_requisition.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted..").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	 
	/* Click event handler */
	$('#MaterialRequisitionList tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  materialRequisitionId = aData.materialRequisitionId;
			  editStatus = aData.editStatus;
	          if(editStatus==false){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          materialRequisitionId = aData.materialRequisitionId;
	          editStatus = aData.editStatus;
	          if(editStatus==false){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
  	});
	
	$("#print").click(function() {  
		if(materialRequisitionId>0){	 
			window.open('show_material_requisition_printout.action?materialRequisitionId='+ materialRequisitionId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>material requisition</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
 		 	<div id="rightclickarea">
			 	<div id="material_requisition_list">
					<table id="MaterialRequisitionList" class="display">
						<thead>
							<tr>
								<th>Reference Number</th> 
								<th>Requisition Date</th>  
								<th>Store</th>  
								<th>Transfer Reference</th>  
								<th>Requisition Status</th>  
								<th>Created By</th>
							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
 				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>  
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>