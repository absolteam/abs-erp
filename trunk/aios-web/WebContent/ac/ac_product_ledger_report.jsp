
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var productId =0;
var oTable;

$(function(){ 
	$('#example').dataTable({ 
		"sAjaxSource": "product_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Product_ID", "bVisible": false},
			{ "sTitle": "Product Code"},
			{ "sTitle": "Product Name"},
			{ "sTitle": 'Unit Name'}, 
			{ "sTitle": 'Item Type'},
			{ "sTitle": 'Item Nature'}, 
			{ "sTitle": 'Status'},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			 /* if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			} */
		}
	});
	
	oTable = $('#example').dataTable();
});



var selectedType = null;

$(".pdf-download-call").click(function(){ 
		
			
		if (productId != 0 || selectedType != null) {

		window.open('<%=request.getContextPath()%>/generate_product_ledger_report.action?productId='+productId+'&selectedType='+selectedType,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');		
		} else {
			alert("Please Either Select Item Type Or A Record");
		}
			return false;

});

	$('#example tbody tr').live('click', function() {

		if ($(this).hasClass('row_selected')) {
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);

			purchaseId = aData[0];
		} else {
			oTable.$('tr.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);
			productId = aData[0];
		}
		
		
	});

	$("select[name='itemType']").change(function() {
		// multipleValues will be an array
		/* multipleValues = $(this).val() || []; */
		selectedType = $(this).val();

	});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Inventory Balance

		</div>
		<div class="portlet-content">





			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;">Item Type</label> <select
						name="itemType" id="itemType" style="width: 51%;"
						multiple="multiple">
						<option value="A">Assets</option>
						<option value="E">Expense</option>
						<option value="I">Inventory</option>
					</select>

				</div>

			</div>

			<div class="width45 float-right" style="padding: 5px;"></div>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="ledgerFiscals">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>

	</div>
</div>