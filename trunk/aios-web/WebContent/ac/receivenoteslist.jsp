<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var receiveId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 $('#DOMWindow').remove();
	 $('#DOMWindowOverlay').remove();
	 $('#store-popup').dialog('destroy');		
	 $('#store-popup').remove();  
	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove();   
	 $('.formError').remove();
	$('#example').dataTable({ 
		"sAjaxSource": "receivenotes_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Receive_ID", "bVisible": false},
			{ "sTitle": "Receive Number"},
			{ "sTitle": 'Receive Date'},  
			{ "sTitle": "Reference NO."},
			{ "sTitle": 'Amount'}, 
			{ "sTitle": 'Supplier NO.'}, 
			{ "sTitle": 'Supplier Name'}, 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){  
		receiveId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/receivenotes_showentry.action", 
	     	async: false,
	     	data:{receiveId:receiveId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#edit').click(function(){    
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/receivenotes_showentry.action", 
	     	async: false,
	     	data:{receiveId:receiveId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#delete').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/receivenotes_deletentry.action", 
	     	async: false,
	     	data:{receiveId:receiveId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#print').click(function(){  
		if(receiveId>0){
			// HTML PRINT  
	   		if('${THIS.templatePrint}' == null || '${THIS.templatePrint}' == "false" || '${THIS.templatePrint}'== '') {  
	    		window.open('receive_note_print.action?receiveId='+ receiveId +'&format=HTML', '',
							'width=800,height=800,scrollbars=yes,left=100px'); 
	   		} 
	   		// TEMPLATE PRINT
	   		else if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true") { 
	   		} 
		}else{
			$('#error_message').hide().html("Please select a record to print.").slideDown();
			$('#error_message').delay(3000).hide();
			return false;
		} 
	}); 

	//init datatable
	oTable = $('#example').dataTable();
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          receiveId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          receiveId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Receive Notes</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">Transaction Success</div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">Transaction Failure</div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
	   	    <div id="rightclickarea">
			 	<div id="journal_accounts">
					<table class="display" id="example"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"  style="display: none;"><span>Edit</span></div> 
				<div class="first_li"><span>Print</span></div>
				<div class="first_li"  style="display: none;"><span>View</span></div> 
				<div class="first_li"  style="display: none;"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" style="display:none;"><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view" style="display: none;"><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit"  style="display:none;"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>