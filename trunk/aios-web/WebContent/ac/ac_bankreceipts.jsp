<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var bankReceiptsId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
 	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#codecombination-popup').dialog('destroy');		
	$('#codecombination-popup').remove(); 
	$('.formError').remove();
	$('#BankReceiptsDT').dataTable({ 
		"sAjaxSource": "show_json_receipts.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'BankReceipts', "bVisible": false},
			{ "sTitle": 'Reference NO'},
			{ "sTitle": 'Receipt Type'},  
			{ "sTitle": 'Date'},
			{ "sTitle": 'Currency'},  
			{ "sTitle": 'Amount'}, 
			{ "sTitle": 'Ref.No'},
			{ "sTitle": 'Receiver'},
			{ "sTitle": 'Created By'}
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	
	$('#processJV').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/bankreceipt_processjv.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	  
	$('#add').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/bankreceipts_addentry.action", 
	     	async: false, 
			dataType: "html",
			data : {bankReceiptsId : 0,recordId:Number(0)},
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#edit').click(function(){   
		if(bankReceiptsId!=null && bankReceiptsId!='' && bankReceiptsId > 0) {
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/bankreceipts_addentry.action", 
		     	async: false,
		     	data:{bankReceiptsId: bankReceiptsId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
					$.scrollTo(0,300);
				} 		
			}); 
		} else {
			 alert("Please select a record to edit.");
			 return false;
		} 
	}); 

	$('#delete').click(function(){  
		if(bankReceiptsId!=null && bankReceiptsId!=0) {
			var cnfrm = confirm('Selected record will be deleted permanently');
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/bankreceipts_deletentry.action", 
		     	async: false,
		     	data:{bankReceiptsId: bankReceiptsId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_receipts_voucher.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(2000).slideUp();
						 return false;
					 }
				}
			 });
		} else{
			alert("Please select a record to delete.");
			 return false;
		 }
	}); 
	
	$('#print').click(function(){  
		if(bankReceiptsId != null && bankReceiptsId != 0){ 
			window.open("<%=request.getContextPath()%>/show_receipts_printout.action?bankReceiptsId="+bankReceiptsId+"&format=HTML",
					'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
		}else{
			alert("Please select a record to print."); 
		} 
		return false;
	}); 

	//init datatable
	oTable = $('#BankReceiptsDT').dataTable();
	 
	/* Click event handler */
	$('#BankReceiptsDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          bankReceiptsId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          bankReceiptsId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>receipts</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="display:none;"></div>  
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    <div id="rightclickarea">
			 	<div id="bank_receipts">
					<table class="display" id="BankReceiptsDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
 				<div class="first_li"><span>Delete</span></div>
 				<div class="first_li"><span>Print</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">  
				<div class="portlet-header ui-widget-header float-right" id="processJV" style="display: none;">Process JV</div>
				<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div> 
 				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div> 
</div> 