<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var productPointId =0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
 	 oTable = $('#ProductPointDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_all_product_points.action",

		"aoColumns" : [ { 
			"mDataProp" : "product.code"
		 }, {
			"mDataProp" : "product.productName"
		 }, {
			"mDataProp" : "statusPoints"
		 },{
			"mDataProp" : "purchaseAmount"
		 }], 
	});	 
		
	 

	 $('#fromDate,#toDate').datepick({
			showTrigger: '#calImg'}); 

	 $jquery("#product_point_validation").validationEngine('attach'); 

	 $('.common-popup').click(function(){  
			$('.ui-dialog-titlebar').remove(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_common_popup.action", 
			 	async: false,  
			 	data: {itemType:'I', rowId: 0},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.common-result').html(result);  
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0);
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('#common-popup').dialog({
		 autoOpen: false,
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
		
	$('#edit').click(function(){   
 		if(productPointId != null && productPointId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_point_entry.action", 
		     	async: false, 
		     	data:{productPointId: productPointId},
				dataType: "json",
				cache: false,
				success: function(response){ 
					$('#productCode').val(response.productPointVO.product.code);
					$('#productId').val(response.productPointVO.product.productId);
					$('#statusPoints').val(response.productPointVO.statusPoints);
					$('#purchaseAmount').val(response.productPointVO.purchaseAmount);
					$('#status').attr('checked',response.productPointVO.status ? true : false); 
					$('#fromDate').val(response.productPointVO.validFrom);
					$('#toDate').val(response.productPointVO.validTo);
					$('#description').val(response.productPointVO.description);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
 		if(productPointId != null && productPointId != 0){
 			var cnfrm = confirm("Selected record will be permanently deleted.");
 			if(!cnfrm)
 				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_product_point.action", 
	     	async: false,
	     	data:{productPointId: productPointId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_product_point.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted.")
																			.slideDown(
																					);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown();
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert(
										"Please select a record to delete.");
								return false;
							}
						});

	$('#product-common-popup').live('click', function() {
		$('#common-popup').dialog('close');
	});
		/* Click event handler */
		$('#ProductPointDT tbody tr').live('dblclick', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				productPointId = aData.productPointId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				productPointId = aData.productPointId;
			}
			$('#edit').trigger('click');
		});

		/* Click event handler */
		$('#ProductPointDT tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				productPointId = aData.productPointId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				productPointId = aData.productPointId;
			}
 		});

		$('.product_point_discard').click(function(){ 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_product_point.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) { 
												$(
												'#common-popup')
												.dialog(
														'destroy');
										$(
												'#common-popup')
												.remove();
												$("#main-wrapper").html(result);
											}
										});
							});

		$('.product_point_save').click(function(){   
			if($jquery("#product_point_validation").validationEngine('validate')){
				var productId =  Number($('#productId').val()); 
	 			var statusPoints = Number($('#statusPoints').val()); 
	 			var purchaseAmount = Number($('#purchaseAmount').val()); 
	  			var fromDate = $('#fromDate').val();
	 			var toDate = $('#toDate').val();
	 			var status =$('#status').val(); 
				var description =$('#description').val();  
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/save_product_point.action", 
			     	async: false,
			     	data:{
			     			productPointId : productPointId, productId: productId, statusPoints: statusPoints, purchaseAmount: purchaseAmount,
			     			fromDate: fromDate, toDate: toDate, status: status, description: description
			     		 },
					dataType: "json",
					cache: false,
					success: function(response){  
						if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
							
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/show_product_point.action",
																	async : false,
																	dataType : "html",
																	cache : false,
																	success : function(
																			result) {
																		$(
																				'#common-popup')
																				.dialog(
																						'destroy');
																		$(
																				'#common-popup')
																				.remove();
																		$(
																				"#main-wrapper")
																				.html(
																						result);
																		if (productPointId > 0)
																			$(
																					'#success_message')
																					.hide()
																					.html(
																							"Record updated.")
																					.slideDown(
																							1000);
																		else
																			$(
																					'#success_message')
																					.hide()
																					.html(
																							"Record created.")
																					.slideDown(
																							1000);
	
																		$(
																				'#success_message')
																				.delay(
																						2000)
																				.slideUp();
																	}
																});
													} else {
														$('#error_message')
																.hide()
																.html(
																		response.returnMessage)
																.slideDown(1000);
														$('#error_message').delay(
																2000).slideUp();
														return false;
													}
												}
											});
							} else
								return false;
						});
		
		{
			productPointId = Number($('#productRecordPointId').val());
			if(productPointId > 0) {
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/product_point_entry.action", 
			     	async: false, 
			     	data:{productPointId: Number($('#productRecordPointId').val())},
					dataType: "json",
					cache: false,
					success: function(response){ 
						$('#productCode').val(response.productPointVO.product.code);
						$('#productId').val(response.productPointVO.product.productId);
						$('#statusPoints').val(response.productPointVO.statusPoints);
						$('#purchaseAmount').val(response.productPointVO.purchaseAmount);
						$('#status').attr('checked',response.productPointVO.status ? true : false); 
						$('#fromDate').val(response.productPointVO.validFrom);
						$('#toDate').val(response.productPointVO.validTo);
						$('#description').val(response.productPointVO.description);
						$('#productRecordPointId').val(Number(0));
					} 		
				}); 
			} 
		} 
	});

	function commonProductPopup(productId, productName, rowId){
		$('#productCode').val(productName);
		$('#productId').val(productId);
	}
</script>
<div id="main-content">
	<c:choose> 
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
			points
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<form id="product_point_validation" style="position: relative;">
				<input type="hidden" id="productRecordPointId" value="${requestScope.productPointId}"/>
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset>
								<div>
									<label class="width30" for="fromDate">From Date<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" name="fromDate" id="fromDate"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="toDate">To Date<span class="mandatory">*</span>  </label> <input type="text"
										readonly="readonly" name="toDate" id="toDate"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30">Description</label>
									<textarea id="description" name="description" class="width51"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label class="width30" for="productCode">Product Name<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" name="productCode" id="productCode"
										class="width50 validate[required]" /> <input type="hidden"
										id="productId" name="productId" /> <span class="button">
										<a style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>

								<div>
									<label class="width30" for="statusPoints">Status Points<span
										class="mandatory">*</span> </label> <input type="text"
										id="statusPoints" name="statusPoints"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="purchaseAmount">Purchase
										Amount<span class="mandatory">*</span> </label> <input type="text"
										name="purchaseAmount" id="purchaseAmount"
										class="width50 validate[required]" />
								</div>

								<div>
									<label class="width30">Status</label> <select name="status"
										id="status" class="validate[required]" style="width: 51%;">
										<option value="true">Active</option>
										<option value="inactive">In active</option>
									</select>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
						<div
							class="portlet-header ui-widget-header float-right product_point_discard">
							<fmt:message key="accounts.common.button.cancel" />
						</div>
						<div
							class="portlet-header ui-widget-header float-right product_point_save">
							<fmt:message key="accounts.common.button.save" />
						</div>
					</div>
				</div>
			</form>
			<div id="rightclickarea">
				<div id="product_point_list">
					<table id="ProductPointDT" class="display">
						<thead>
							<tr>
								<th>Product Code</th>
								<th>Product Name</th>
								<th>Status Points</th>
 								<th>Purchase Amount</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div 
					class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div 
					class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="accounts.common.button.edit" />
				</div>
			</div>
		</div>
	</div>
	<div
		style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		<div
			class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
			unselectable="on" style="-moz-user-select: none;">
			<span class="ui-dialog-title" id="ui-dialog-title-dialog"
				unselectable="on" style="-moz-user-select: none;">Dialog
				Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
				role="button" unselectable="on" style="-moz-user-select: none;"><span
				class="ui-icon ui-icon-closethick" unselectable="on"
				style="-moz-user-select: none;">close</span> </a>
		</div>
		<div id="common-popup" class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>
</div>