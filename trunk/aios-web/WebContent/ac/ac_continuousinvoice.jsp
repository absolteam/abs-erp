 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var invoiceId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
	$('#DOMWindow').remove();
	$('#DOMWindowOverlay').remove();
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove(); 
	$('#price-common-popup').dialog('destroy');		
	$('#price-common-popup').remove(); 
	 
	 oTable = $('#continuousInvoiceDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_purchase_invoice_continuous.action",

		"aoColumns" : [ { 
			"mDataProp" : "invoiceNumber"
		 }, {
			"mDataProp" : "invoiceDate"
		 }, {
			"mDataProp" : "totalInvoiceStr"
		 }, {
			"mDataProp" : "pendingInvoiceStr"
		 },{
			"mDataProp" : "supplierName"
		 },{
			"mDataProp" : "createdPerson"
		 } ], 
	});	 
	 oTable.fnSort( [ [0,'desc'] ] );	
	$('#add').click(function(){   
		if(invoiceId != null && invoiceId != 0){
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/purchase_invoice_continuous_entry.action", 
	     	async: false, 
	     	data:{invoiceId: invoiceId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
		}else{
			alert("Please select a record to settle.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(invoiceId != null && invoiceId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/invoice_deletentry.action", 
	     	async: false,
	     	data:{invoiceId: invoiceId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_continuous_purchase_invoice.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	
	$('#print').click(function(){  
		if(invoiceId>0){
			window.open('continuous_invoice_payment_print.action?invoiceId='+ invoiceId +'&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px'); 
		}else{
			alert("Please select a record to print.");
			return false;
		} 
	}); 
	 
	/* Click event handler */
	$('#continuousInvoiceDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  invoiceId = aData.invoiceId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          invoiceId = aData.invoiceId;
	      }
  	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Continuous Invoice</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="continous_purchase_invoice_list">
					<table id="continuousInvoiceDT" class="display">
						<thead>
							<tr>
								<th>Reference Number</th> 
								<th>Date</th> 
								<th>Total Invoice</th> 
								<th>Pending Invoice</th> 
 								<th>Supplier</th> 
 								<th>Created By</th> 
 							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Settle</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
 				<div class="portlet-header ui-widget-header float-right" id="add" >Settle</div>	 
			</div>
		</div>
	</div>
</div>