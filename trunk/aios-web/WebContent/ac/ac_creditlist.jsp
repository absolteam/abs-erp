<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script> 
<script type="text/javascript"> 
var creditId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	if(typeof($('#common-popup')!="undefined")){  
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();   
	 }
	 oTable = $('#CreditNoteDT').dataTable({ 
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "getcredit_jsonlist.action",

			"aoColumns" : [ { 
				"mDataProp" : "creditNumber"
			 }, {
				"mDataProp" : "creditDate"
			 }, {
				"mDataProp" : "salesReference"
			 }, {
				"mDataProp" : "customerReference"
			 }, {
				"mDataProp" : "customerName"
			 }, {
				"mDataProp" : "description"
			 }], 
		});	 
	 oTable.fnSort( [ [0,'desc'] ] );	
	$('#add').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/creditnote_addentry.action", 
	     	async: false, 
	     	data:{creditId: 0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
 			} 		
		}); 
	}); 

	$('#view').click(function(){ 
		if(creditId != null && creditId != 0){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/creditnote_view.action", 
		     	async: false,
		     	data:{creditId: creditId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
				} 		
			}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}
	}); 

	$('#delete').click(function(){   
		if(creditId != null && creditId != 0){ 
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/creditnote_deletentry.action", 
		     	async: false,
		     	data:{creditId: creditId},
				dataType: "json",
				cache: false,
				success: function(response){ 
 					 if(response.returnMessage == "SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/getcredit_listdetails.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(response.returnMessage).slideDown(1000);
						 $('#error_message').delay(3000).slideUp();
						 return false;
					 }
				}
			 });
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});

	/* Click event handler */
	$('#CreditNoteDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  creditId = aData.creditId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          creditId = aData.creditId;
	      }
  	});
	
	$("#credit-print").click(function(){ 
		if(creditId != null && creditId != 0){
			window.open("<%=request.getContextPath()%>/get_credit_note_report_printout.action?creditId="+creditId+"&format=HTML",
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			return false; 
		} else{
			alert("Please select a record to print.");
			return false;
		}
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Credit Note</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">Success</div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">Failure</div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
	   	    <div id="rightclickarea">
			 	<div id="credit_note_list">
					<table id="CreditNoteDT" class="display">
						<thead>
							<tr>
								<th>Reference No</th>
								<th>Date</th>
								<th>Delivery Note</th>
								<th>Customer Ref</th>
								<th>Customer Name</th>
								<th>Description</th> 
							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu"> 
				<div class="first_li"><span>View</span></div>
 				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view"><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="credit-print"><fmt:message key="accounts.common.button.print"/></div>
 				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>