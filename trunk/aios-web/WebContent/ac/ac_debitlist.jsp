<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var debitId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData=""; var barPrint = false;
$(function(){
	 if(typeof($('#common-popup')!="undefined")){ 
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove();  
	 }
	 $('.formError').remove();
	$('#DebitNoteDT').dataTable({ 
		"sAjaxSource": "getdebit_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Debit ID", "bVisible": false},
			{ "sTitle": "Debit Number"},
			{ "sTitle": 'Date'},  
			{ "sTitle": 'Return Number'},  
			{ "sTitle": 'Supplier Number'},
			{ "sTitle": 'Supplier Name'},
			{ "sTitle": 'exchange', "bVisible": false},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/debitnote_addentry.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#edit').click(function(){   
		if(debitId!=null && debitId!=0){
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/debitnote_editentry.action", 
	     	async: false,
	     	data:{debitId:debitId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
				$.scrollTo(0,300);
			} 		
		}); 
		}	else{alert("Please select a record to edit.");
		return false;}
	}); 

	$('#delete').click(function(){ 
		$('.error').hide();
		$('.success').hide();
		if(debitId!=null && debitId!=0) {
		   var cnfrm = confirm('Selected record will be deleted permanently');
		   if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/debitnote_deletentry.action", 
		     	async: false,
		     	data:{debitId: debitId},
				dataType: "json",
				cache: false,
				success: function(response){   
					 if(response.returnMessage=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/getdebit_listdetails.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(response.returnMessage).slideDown(1000);
						 $('#error_message').delay(2000).slideUp();
						 return false;
					 }
				}
			 });
		} else{
			 alert("Please select a record to delete.");
			 return false;
		 }
	}); 
	
	$('#print_barcode').click(function(){    
		if(barPrint == true){
			 window.open('debitnote_barcode_print_pdf.action?debitId='+ debitId + '&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');   
		}
		return false;
	});

	//init datatable
	oTable = $('#DebitNoteDT').dataTable();
	 
	/* Click event handler */
	$('#DebitNoteDT tbody tr').live('click', function () {  
		$('#print_barcode').css('opacity', 0.5);
		barPrint = false;   
		if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          debitId=aData[0];  
	          var bprint =  aData[6];
 	          if(bprint == true){
 	        	 barPrint = true;   
	        	  $('#print_barcode').css('opacity', 1);
	          } 
	     }
	     else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          debitId=aData[0];  
	          var bprint =  aData[6];
 	          if(bprint == true){
 	        	 barPrint = true;   
	        	  $('#print_barcode').css('opacity', 1);
	          } 
	      }
	});
	
	$("#debit-print").click(function(){ 
		if(debitId != null && debitId != 0){
			window.open("<%=request.getContextPath()%>/get_debit_note_report_printout.action?debitId="+debitId+"&format=HTML",
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			return false; 
		} else{
			alert("Please select a record to print.");
			return false;
		}
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Debit Note</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">Success</div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">Failure</div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
	   	    <div id="rightclickarea">
			 	<div id="journal_accounts">
					<table class="display" id="DebitNoteDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
 				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div>
 				<div class="portlet-header ui-widget-header float-right" id="edit" style="display:none;"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="print_barcode" style="opacity: 0.5!important;">Barcode Print</div>
				<div class="portlet-header ui-widget-header float-right" id="debit-print">Print</div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>