<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var salesOrderId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var deliveryFlag=null;
$(function(){
	$('.formError').remove(); 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();   
 
	$('#baseprice-popup-dialog').dialog('destroy');		
	$('#baseprice-popup-dialog').remove(); 
	$('#sales-product-inline-popup').dialog('destroy');
	$('#sales-product-inline-popup').remove();
		 
	oTable = $('#SalesOrderDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "sales_order_list.action",  
		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber" 
		 }, {
			"mDataProp" : "salesDate"
		 }, {
			"mDataProp" : "shipDate"
		 }, {
			"mDataProp" : "expirySales"
		 },{
			"mDataProp" : "customerName"
		 },{
			"mDataProp" : "salesReperesentive" 
		 }, {
			"mDataProp" : "salesStatus"
		 } , {
			"mDataProp" : "description"
		 }]
	});	 
	oTable.fnSort( [ [0,'desc'] ] );	
	$('#add').click(function(){   
		salesOrderId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/sales_order_entry.action", 
	     	async: false, 
	     	data:{salesOrderId: salesOrderId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  
	
	$('#view').click(function(){   
		if(salesOrderId != null && salesOrderId != 0){
				
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_order_view.action", 
		     	async: false, 
		     	data:{salesOrderId: salesOrderId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}
	});  
	
	$('#edit').click(function(){   
		if(salesOrderId != null && salesOrderId != 0){
			if(deliveryFlag){
				$('#error_message').hide().html("Can not edit!!! Sales has been delivered").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
				
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_order_entry.action", 
		     	async: false, 
		     	data:{salesOrderId: salesOrderId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(salesOrderId != null && salesOrderId != 0){
			if(deliveryFlag){
				$('#error_message').hide().html("Can not delete!!! Sales has been delivered").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/sales_order_deletentry.action", 
	     	async: false,
	     	data:{salesOrderId: salesOrderId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
				var message=$.trim($('.tempresult').html().toLowerCase()); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_sales_order.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(3000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					$('#error_message').delay(3000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	 
	/* Click event handler */
	$('#SalesOrderDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  salesOrderId = aData.salesOrderId;
			  deliveryFlag=aData.deliveryFlag;
			  if(deliveryFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          salesOrderId = aData.salesOrderId;
	          deliveryFlag=aData.deliveryFlag;
	          if(deliveryFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
  	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sales Order</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="sales_order_list">
					<table id="SalesOrderDT" class="display">
						<thead>
							<tr>
								<th>Reference Number</th>
								<th>Order Date</th>
								<th>Ship Date</th>
								<th>Expiry Date</th>
								<th>Customer</th>
								<th>Representative</th>
								<th>Order Status</th>
								<th>Description</th>
							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
				<div class="first_li"><span>View</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>