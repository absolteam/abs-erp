 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
table.display td{
	padding:7px 10px;
}
#example1{
	cursor: pointer;
}
</style>
<script type="text/javascript">
var eiborId=0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){   
	$('#example1').dataTable({ 
		"sAjaxSource": "eibor_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "EIBOR_ID", "bVisible": false},
			{ "sTitle": "EIBOR_TYPE_ID", "bVisible": false},
			{ "sTitle": "CODE_COMBINATION_ID", "bVisible": false}, 
			{ "sTitle": "EIBOR Type"},
			{ "sTitle": "Date"},
			{ "sTitle": "EIBOR Rate"},
			{ "sTitle": 'Natural Account'},   
			{ "sTitle": 'Option'},  
		], 
		"sScrollY": $("#main-content").height() - 385,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	
	$("#eiborValidation").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});  
	
	$('#eiborDate').datepick({
		minDate:0, showTrigger: '#calImg'});  
	
	$('#save').click(function(){  
		if($("#eiborValidation").validationEngine({returnIsValid:true})){   
			var eiborId=Number($('#eiborId').val());
			var eiborType=$('#eiborType').val();
			var combinationId=$('#combinationId').val();
			var bankId=0;//$('#bankId').val();
			var eiborDate=$('#eiborDate').val(); 
			var eiborRate=$('#eiborRate').val(); 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/eibor_saveentry.action", 
		     	async: false,
		     	data:{eiborId:eiborId,eiborType:eiborType,combinationId:combinationId,bankId:bankId, eiborDate:eiborDate, eiborRate:eiborRate},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);   
					var message=$('.tempresult').html().toLowerCase().trim(); 
					if(message!=null && message=="success"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/eibor_showlist.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$("#main-wrapper").html(result);   
								$('#success_message').hide().html(message).slideDown(1000);
								$.scrollTo(0,300);
							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(message).slideDown(1000);
					}
				},
				error:function(result){
					alert(result);
				}
			}); 
		}
		else{
			return false;
		}
	});  

	$('.delete').live('click',function(){
		var tempvar=$(this).attr('id');
		var idVal=tempvar.split('_');
		var eiborId=Number(idVal[1]);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/eibor_deleteentry.action", 
	     	async: false, 
	     	data:{eiborId:eiborId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);   
				var message=$('.tempresult').html().toLowerCase().trim(); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/eibor_showlist.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html(message).slideDown(1000);
							$.scrollTo(0,300);
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
				}
			} 		
		}); 
	}); 

	//init datatable
	oTable = $('#example1').dataTable(); 
	/* Click event handler */
	$('#example1 tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected'); 
	      }
	});
	
	$('#example1 tbody tr').live('dblclick', function(){  
		 aData =oTable.fnGetData( this );  
		 $('#eiborId').val(aData[0]);
		 $('#eiborType').val(aData[1]);
		 $('#combinationId').val(aData[2]);
		 $('#bankId').val(aData[3]);
		 $('#codeCombination').val(aData[8]);
		 $('#eiborDate').val(aData[5]);
		 $('#eiborRate').val(aData[6]);
		 $('#bankName').val(aData[7]);
	});
	
	
	$('.bank-popup').click(function(){ 
	       tempid=$(this).parent().get(0);   
	       $('.ui-dialog-titlebar').remove(); 
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_banklist.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.bank-result').html(result);  
				},
				error:function(result){  
					 $('.bank-result').html(result); 
				}
			});  
		});
		 $('#bank-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:600, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
	
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0);   
	      $('.ui-dialog-titlebar').remove();   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
	
	 $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true
	 });
	 
	 $('#clear').click(function(){
		 $('#eiborType').val("");
		 $('#eiborDate').val("");
		 $('#eiborRate').val("");
		 $('#bankName').val("");
		 $('#bankId').val("");
		 $('#bankId').val("");
		 $('#codeCombination').val("");
	 });
});
function setCombination(combinationTreeId,combinationTree){
	$('#combinationId').val(combinationTreeId);
	$('#codeCombination').val(combinationTree);
} 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>eibor information</div>	 	 
		<div class="portlet-content"> 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general information</div>	 	 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    <form id="eiborValidation"> 
		   	    <div class="width100" id="hrm">
		   	    	<fieldset> 
		   	    		<div class="width48 float-right">
		   	    			<input type="hidden" readonly="readonly" name="eiborId" id="eiborId"/>
			   	    		<div>
			   	    			<label for="combination">Combination</label>
			   	    			<input type="hidden" readonly="readonly" name="combinationId" id="combinationId"/>
			   	    			<input type="text" readonly="readonly" name="combination" id="codeCombination" tabindex="5" class="width40 validate[required]"/>
			   	    			<span class="button">
									<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
			   	    		</div>
			   	    	</div>
			   	    	<div class="width50 float-left">
			   	    		<div>
			   	    			<label for="eiborType">EIBOR Type</label>
			   	    			<select name="eiborType" id="eiborType" style="width:41%!important;" tabindex="1" class="validate[required]">
			   	    				<option value="">Select</option>
			   	    				 <c:forEach var="entry" items="${EIBOR_TYPES}">
			   	    				 	<option value="${entry.key}">${entry.value}</option>
			   	    				 </c:forEach>
			   	    			</select>
			   	    		</div>
			   	    		<div>
			   	    			<label for="eiborDate">EIBOR Date</label>
			   	    			<input type="text" readonly="readonly" name="eiborDate" id="eiborDate" tabindex="2" class="width40 validate[required]"/>
			   	    		</div>
			   	    		<div>
			   	    			<label for="eiborRate">EIBOR Rate</label>
			   	    			<input type="text" name="eiborRate" id="eiborRate" tabindex="3" class="width40 validate[required,custom[onlyFloat,percentRange]]"/>
			   	    		</div>
			   	    	</div>
			   	    	<div class="clearfix"></div> 
			   	    	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">  
							<div class="portlet-header ui-widget-header float-right" id="clear">clear</div>
							<div class="portlet-header ui-widget-header float-right" id="save" ><fmt:message key="accounts.common.button.save"/></div>	 
						</div>
		   	    	</fieldset> 
		   	    </div>
		   	 </form> 
	   	    <div class="clearfix"></div> 
	   	    <div id="rightclickarea"> 
				<table class="display" id="example1"></table> 
			</div>	 
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
			<!-- 	<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>View</span></div> -->
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="clearfix"></div>  
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="bank-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="bank-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
</div>