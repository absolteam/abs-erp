 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
 
var id;
var calName;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s="";  
$(function(){ 
	$('.formError').remove(); 
	$('#example').dataTable({ 
		"sAjaxSource": "gl_createcalendar_retrievejson.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Calendar ID', "bVisible": false},
			{ "sTitle": 'Financial Name'},
			{ "sTitle": 'Year From'},
			{ "sTitle": 'Year To'},
			{ "sTitle": 'Status'}, 
		],
		"sScrollY": $("#main-content").height() - 240,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();

	/* Click event handler */
	$('#example tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        calendarId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        calendarId=aData[0];
	    }
	});
 	$("#add").click( function() { 
 	 	var showPage="";
 	 	callMessageHide();
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/validate_calendar.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);   
				var message=$('.tempresult').html().trim(); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/gl_createcalendar_addcalendar.action", 
				     	async: false, 
				     	data: {showPage:showPage},  
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Financial Year Deleted.").slideDown(1000);
							return false;
							$.scrollTo(0,300);
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					return false;
				}
			}  
		});  
	});
	
	$("#edit").click( function() { 
	 
		if(s == null){
			alert("Please select one row to edit");
		}
		else{			
			id = s; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/gl_createcalendar_editcalendar.action",  
		     	data: {calendarId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(result){
		     		$('#main-wrapper').html(result); 
				},
				success:function(result){ 
					$('.formError').hide();  
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html();
				     if(tempeMsg!="" && tempeMsg!=null){
				    	$('#sMsg').hide(); 
				    	$('#eMsg').show();
				    	$('#eMsg').hide().html(tempeMsg).slideDown();
				    }
			    	else{ 
				    	$('#sMsg').hide(); 
				    	$('#eMsg').hide();
			    	}   	
			 	}
			});
			return true;	
		} 
		return false;
	});
	
	$("#delete").click(function(){  
	 	if(calendarId!=null && calendarId!="" && calendarId>0){
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/gl_calendar_delete.action",  
		     	data: {calendarId:calendarId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);   
					var message=$('.tempresult').html().trim(); 
					if(message!=null && message=="success"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/calendar_master_details_retrive.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$("#main-wrapper").html(result);   
								$('#success_message').hide().html("Financial Year Deleted.").slideDown(1000);
								return false;
								$.scrollTo(0,300);
							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(message).slideDown(1000);
						return false;
					}
				}
        	}); 
	 	}else{
	 		$('#error_message').hide().html("Select a financial year to delete.").slideDown(1000);
		 	}
	});
	$("#search").click(function(){
		 calName = $('#calName').val().trim();   
		if( (calName != null && calName.trim().length > 0) ){  
			var regExp = /%/;  
			 var matchPosition =  calName.search(regExp); 
			 if(matchPosition!=-1){ 
				 while(matchPosition!=-1){
					 calName=calName.replace('%','$'); 
					 matchPosition =  calName.search(regExp); 
				 }
			 }
			 else{
				 calName='$'+calName+'$';
			 }
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/gl_createcalendar_search.action",   
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) { 
		     		$("#gridDiv").html(data);  
				},
		     	success: function(data){  
					$('.commonErr').hide();
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			}); 
		}
		else{ 
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown(1000);
			return false; 
		} 
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/calendar_master_details_retrive.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 	
});
function callMessageHide(){
	$('.success').hide();
	$('.error').hide();
}
</script>
 
 <body>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Financial List</div>	 
		<div class="portlet-content">
			<div class="tempresult" style="display:none;"></div>
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			
			<div  class="response-msg success ui-corner-all" style="width:80%; display:none;" id="sMsg">${requestScope.succMsg1}</div>
			<div  class="response-msg error ui-corner-all" style="width:80%; display:none;" id="eMsg">${requestScope.errMsg1}</div>
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
	
	<div id="rightclickarea">
	<div id="gridDiv">
		<table class="display" id="example"></table>
	</div>
	</div>		
	<div class="vmenu">
	       	<div class="first_li"><span>Add</span></div>
		<div class="sep_li"></div>		 
		<div class="first_li"><span>Edit</span></div>
		<div class="first_li"><span>View</span></div>
		<div class="first_li"><span>Delete</span></div>
	</div>
	 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin: 10px;">
		<div class="portlet-header ui-widget-header float-right" id="delete" style="cursor:pointer;"><fmt:message key="accounts.calendar.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right"  style=" cursor:pointer;">VIEW</div>
		<div class="portlet-header ui-widget-header float-right" id="edit" style=" cursor:pointer;"><fmt:message key="accounts.calendar.button.edit"/></div>
		<div class="portlet-header ui-widget-header float-right" id="add" style="cursor:pointer;"><fmt:message key="accounts.calendar.button.add"/></div>
	</div> 
</div>
</div>
</div>
</body>
 