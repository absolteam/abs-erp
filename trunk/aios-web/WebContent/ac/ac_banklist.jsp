<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var bankId = 0;
 var oTable; var selectRow=""; var aData="";var aSelected = [];
$(function (){
 $('#codecombination-popup').dialog('destroy');		
 $('#codecombination-popup').remove(); 
  $('.formError').remove(); 
  $('#bank').dataTable({ 
	 "sAjaxSource": "get_bank_list.action",
	 "sPaginationType": "full_numbers",
	 "bJQueryUI": true, 
	 "iDisplayLength": 25,
	 "aoColumns": [
			{ "sTitle": 'Bank ID', "bVisible": false},
			{ "sTitle": 'Institution Type',"sWidth": "100px"}, 
			{ "sTitle": 'Name',"sWidth": "100px"}, 
			{ "sTitle": 'Created By',"sWidth": "150px"},
			{ "sTitle": 'Created Date',"sWidth": "125px"},
		],
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}	 
	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#bank').dataTable();

 	/* Click event handler */
 	$('#bank tbody tr').live('click', function () { 
 	  if ( $(this).hasClass('row_selected') ) {
 			$(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        bankId=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       bankId=aData[0];
 	    }
 	});
 		
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_bank_get.action",  
			data: {bankId: Number(0),recordId:Number(0)},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

	$("#edit").click( function() { 
		if(bankId == 0) {
			alert("Please select one record to edit.");
			return false; 
		}
		else{	
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/add_bank_get.action",  
		     	data: {bankId: bankId,recordId:Number(0)}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		if(bankId == 0) {
			alert("Please select one record to delete.");
			return false; 
		}
		else{	
			var cnfrm = confirm("Selected record will be deleted permanently");
			if(!cnfrm)
				return false; 
 			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_bank.action",  
		     	data:{bankId: bankId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	}); 
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Bank</div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
					 <div class="success response-msg ui-corner-all" id="success_message" style="display: none;"></div>
				 </div> 
				 <div id="rightclickarea">
					 <div id="gridDiv">
						<table class="display" id="bank"></table>
					 </div>
				 </div>		
				 <div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>Delete</span></div>
				 </div> 
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add" /></div>
				 </div>
			</div>
		</div>
	</div>
</body>