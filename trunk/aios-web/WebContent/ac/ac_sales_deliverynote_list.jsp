 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var salesDeliveryNoteId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var deliveryFlag=null;
$(function(){
	$('.formError').remove(); 
	if(typeof($('#common-popup')!="undefined")){  
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();   
	 }
	 if(typeof($('#baseprice-popup-dialog')!="undefined")){  
		 $('#baseprice-popup-dialog').dialog('destroy');		
		 $('#baseprice-popup-dialog').remove(); 
	 }
	 if(typeof($('#codecombination-popup') != "undefined")){  
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	 }
	 oTable = $('#SalesDeliveryDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "sales_delivery_note_list.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 }, {
			"mDataProp" : "dispatchDate"
		 }, {
			"mDataProp" : "customerName"
		 }, {
			"mDataProp" : "salesPerson"
		 }, {
			"mDataProp" : "deliveryStatus"
		 } ], 
	});	 
	oTable.fnSort( [ [0,'desc'] ] );	
	$('#add').click(function(){   
		salesDeliveryNoteId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/sales_delivery_note_entry.action", 
	     	async: false, 
	     	data:{salesDeliveryNoteId: salesDeliveryNoteId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  
	
	$('#view').click(function(){   
		if(salesDeliveryNoteId != null && salesDeliveryNoteId != 0){
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_delivery_note_view.action", 
		     	async: false, 
		     	data:{salesDeliveryNoteId: salesDeliveryNoteId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}
	});  

	$('#edit').click(function(){   
		if(salesDeliveryNoteId != null && salesDeliveryNoteId != 0){
			
			if(deliveryFlag){
				$('#error_message').hide().html("Can not edit!!! Order has been invoiced").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_delivery_note_entry.action", 
		     	async: false, 
		     	data:{salesDeliveryNoteId: salesDeliveryNoteId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(salesDeliveryNoteId != null && salesDeliveryNoteId != 0){
			if(deliveryFlag){
				$('#error_message').hide().html("Can not delete!!! Order has been invoiced").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/sales_delivery_note_delete.action", 
	     	async: false,
	     	data:{salesDeliveryNoteId: salesDeliveryNoteId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_delivery_notes.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(3000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	 
	/* Click event handler */
	$('#SalesDeliveryDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  salesDeliveryNoteId = aData.salesDeliveryNoteId;
			  deliveryFlag=aData.deliveryFlag;
	          if(deliveryFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          salesDeliveryNoteId = aData.salesDeliveryNoteId;
	          deliveryFlag=aData.deliveryFlag;
	          if(deliveryFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
  	}); 
		 
	$("#delivery_print").click(function(){ 
		if(salesDeliveryNoteId != null && salesDeliveryNoteId != 0){
	 		window.open("<%=request.getContextPath()%>/sales_delivery_printout.action?salesDeliveryNoteId="+salesDeliveryNoteId+"&salesDeliveryId="+salesDeliveryNoteId+"&format=HTML",
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	 		
	 		var compKey='${THIS.companyKey}';
	 		if(compKey=='kane' ){
	 			window.open("<%=request.getContextPath()%>/sales_delivery_advice_printout.action?salesDeliveryNoteId="+salesDeliveryNoteId+"&salesDeliveryId="+salesDeliveryNoteId+"&format=HTML",
						'_blank','width=900,height=600,scrollbars=yes,left=100px,top=2px');
	 		}
			return false;
		}else{
			alert("Please select a record to print.");
			return false;
		} 
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Delivery Note</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="sales_delivery_list">
					<table id="SalesDeliveryDT" class="display">
						<thead>
							<tr>
								<th>Reference Number</th> 
								<th>Delivery Date</th>  
								<th>Customer</th>
								<th>Representative</th>
								<th>Delivery Status</th>
							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
				<div class="first_li"><span>View</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">  
				<div class="portlet-header ui-widget-header float-right" id="delivery_print" >print</div> 
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>