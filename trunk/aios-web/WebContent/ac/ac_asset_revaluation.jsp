<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var assetRevaluationId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
 	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	oTable = $('#AssetRevaluationListDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allasset_revaluation.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 },{ 
			"mDataProp" : "assetName"
		 },{
			"mDataProp" : "date"
		 }, {
			"mDataProp" : "valuationTypeName"
		 }, {
			"mDataProp" : "revaluationTypeName"
		 }, {
			"mDataProp" : "sourceName"
		 }, {
			"mDataProp" : "sourceDetailName"
		 }]
	});	 
		
	$('#add').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/asset_revaluation_entry.action", 
	     	async: false, 
	     	data:{assetRevaluationId: Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(assetRevaluationId != null && assetRevaluationId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/asset_revaluation_delete.action", 
	     	async: false,
	     	data:{assetRevaluationId: assetRevaluationId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_asset_revaluation.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted..").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});

		/* Click event handler */
		$('#AssetRevaluationListDT tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				assetRevaluationId = aData.assetRevaluationId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				assetRevaluationId = aData.assetRevaluationId;
			}
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			revaluation
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
				<div id="asset_disposal_list">
					<table id="AssetRevaluationListDT" class="display">
						<thead>
							<tr>
								<th>Reference</th>
								<th>Asset</th>
								<th>Date</th>
								<th>Valuation Type</th>
								<th>Reevaluation</th>
 								<th>Source</th>
								<th>Source Detail</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Add</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="add">
					<fmt:message key="accounts.common.button.add" />
				</div> 
			</div>
		</div>
	</div>
</div>