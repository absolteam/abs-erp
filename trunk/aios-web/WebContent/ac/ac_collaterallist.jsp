  <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
table.display td{
	padding:7px 10px;
} 
</style>
<script type="text/javascript">
var collateralId=0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){  
	$('.formError').remove();
	 if(typeof($('#common-popup')!="undefined")){ 
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove();  
	 }
	$('#example').dataTable({ 
		"sAjaxSource": "collateral_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "COLLATERAL_ID", "bVisible": false}, 
			{ "sTitle": "Bank Name"},
			{ "sTitle": "Account Number"},
			{ "sTitle": "Loan Number"},
			{ "sTitle": "Loan Amount"},
			{ "sTitle": 'Asset'},  
			{ "sTitle": 'Collateral Value'} 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	  
	
	$('#add').click(function(){ 
		var showPage="add";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/collateral_addentry.action", 
	     	async: false,
	     	data:{showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	});
	
	$('#edit').click(function(){ 
		var showPage="edit";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/collateral_updateentry.action", 
	     	async: false,
	     	data:{showPage:showPage, collateralId:collateralId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	});
	
	
	
	$('#delete').click(function(){ 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/collateral_deleteentry.action", 
	     	async: false, 
	     	data:{collateralId:collateralId},
			dataType: "html",
			cache: false,
			success: function(result){  
				$(".tempresult").html(result);   
				var message=$('.tempresult').html().toLowerCase().trim();  
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/collateral_showlist.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html(message).slideDown(1000);
							$.scrollTo(0,300);
						} 		
					}); 
				}
				else{  
					$('#error_message').hide().html(message).slideDown(1000);
				} 
				return false;
			} 
		}); 
		collateralId=0;
	}); 

	//init datatable
	oTable = $('#example').dataTable(); 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          collateralId=aData[0];   
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          collateralId=aData[0];   
	      }
	});
}); 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>collateral information</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if>  
			<div class="tempresult" style="display:none"></div>
	   	    <div id="rightclickarea"> 
				<table class="display" id="example"></table> 
			</div>	 
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>View</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="clearfix"></div>  
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
								<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit"  ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div> 
</div>