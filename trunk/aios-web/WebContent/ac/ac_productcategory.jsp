<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var productCategoryId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
  
	 if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 } 
	 oTable = $('#ProductCategoryList').dataTable({ 
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "show_product_category_json.action",

			"aoColumns" : [ { 
				"mDataProp" : "categoryName"
			 }, {
				"mDataProp" : "parentCatgoryName"
			 }, {
				"mDataProp" : "description"
			 } ]
		});	 

	 $jquery("#productcategory_validation").validationEngine('attach'); 
	  

	 $('#add').click(function(){ 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_product_category_entry.action", 
		     	async: false, 
		     	data:{productCategoryId: Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
				} 		
			});  
	 		return false;
		}); 

	 $('#edit').click(function(){   
	 		if(productCategoryId != null && productCategoryId != 0){
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_product_category_entry.action", 
			     	async: false, 
			     	data:{productCategoryId: productCategoryId},
					dataType: "html",
					cache: false,
					success: function(result){ 
						$("#main-wrapper").html(result);  
 					} 		
				}); 
			}else{
				alert("Please select a record to edit.");
				return false;
			}
	 		return false;
		});   

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(productCategoryId != null && productCategoryId != 0){
			var cnfrm = confirm('Selected record will be deleted permanently');
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_category_delete.action", 
		     	async: false,
		     	data:{productCategoryId: productCategoryId},
				dataType: "json",
				cache: false,
				success: function(response){  
					if(response.returnMessage=="SUCCESS"){
						callScreenDiscard("Record Deleted.");
					} else {
						$('#error_message')
								.hide()
								.html(
										response.returnMessage)
								.slideDown(1000);
						$('#error_message').delay(
								3000).slideUp();
						return false;
					}
				}
				});
			} else {
				alert(
						"Please select a record to delete.");
				return false;
			}
	});

	/* Click event handler */
	$('#ProductCategoryList tbody tr').live('click', function() {
		if ($(this).hasClass('row_selected')) {
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);
			productCategoryId = aData.productCategoryId;
		} else {
			oTable.$('tr.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);
			productCategoryId = aData.productCategoryId;
		}
	});
});

function callScreenDiscard(statusMessage){
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/show_product_category.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$("#main-wrapper").html(result);
				if (statusMessage != null && statusMessage != '') {
					$('#success_message').hide().html(statusMessage).slideDown(
							1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
			category
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div class="clearfix"></div> 
			<div id="rightclickarea">
				<div id="product_category_list">
					<table id="ProductCategoryList" class="display">
						<thead>
							<tr>
								<th>Category Name</th>
								<th>Parent Category</th>
								<th>Description</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
		</div>
	</div>
</div>