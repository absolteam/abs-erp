
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}


.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width: 50%;
	}
	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}


</style>
<script type="text/javascript"> 


var purchaseId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";

$('#common-popup').dialog({
	 autoOpen: false,
	 width:800, 
	 bgiframe: false,
	 overflow:'hidden',
	 modal: true 
});
$(function(){ 
	 /* if(typeof($('#common-popup')!="undefined")){  
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();  
	 } */
	 $('.formError').remove();
	
	 changeEventHandler();
	 
	$('#deliveryDate').datepick();
	
		$('#purchase_print')
				.dialog(
						{
							autoOpen : false,
							width : 600,
							height : 200,
							bgiframe : true,
							modal : true,
							buttons : {
								"Ok" : function() {
									var contactPerson = $('#contactPerson')
											.val();
									var deliveryDate = $('#deliveryDate').val();
									var deliveryAddress = $('#deliveryAddress')
											.val();
									$(this).dialog("close");
									window
											.open(
													'show_purchase_printout.action?purchaseId='
															+ purchaseId
															+ '&contactPerson='
															+ contactPerson
															+ '&deliveryDate='
															+ deliveryDate
															+ '&deliveryAddress='
															+ deliveryAddress
															+ '&format=HTML',
													'',
													'width=1000,height=800,scrollbars=yes,left=100px');
								},
								"Cancel" : function() {
									$('#contactPerson').val("");
									$('#deliveryDate').val("");
									$('#deliveryAddress').val("");
									$(this).dialog("close");
								}
							}
						});

		$("#print")
				.click(
						function() {
							$('#contactPerson').val('');
							$('#deliveryDate').val('');
							$('#deliveryAddress').val('');
							if (purchaseId > 0) {
								$('.ui-dialog-titlebar').remove();
								$('#purchase_print').dialog('open');
							} else {
								$('#error_message').hide().html(
										"Please select a record to print.")
										.slideDown();
								$('#error_message').delay(3000).slideUp();
								return false;
							}
						});

		//init datatable
		oTable = $('#example').dataTable();

		/* Click event handler */
		$('#example tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				purchaseId = aData[0];
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				purchaseId = aData[0];
			}
		});
	});

	$('#startPicker,#endPicker').datepick({
		onSelect : customRange,
		showTrigger : '#calImg'
	});

	function customRange(dates) {
		if (this.id == 'startPicker') {
			$('.fromDate').trigger('change');
			$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		} else {
			$('.toDate').trigger('change');
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}

	$('.fromDate').change(function() {

		changeEventHandler();

	});

	$('.toDate').change(function() {
		
		changeEventHandler();
	});

	function getPurchaseOrdersList(fromDate, toDate, productId, isReceived,
			supplierId,departmentId) {
		


		$('#example').dataTable(
				{
					"sAjaxSource" : "purchase_report_jsonlist.action?purchaseDate="
							+ fromDate + "&expiryDate=" + toDate + "&productId="
							+ productId + "&received=" + isReceived
							+ "&supplier=" + supplierId
							+ "&departmentId=" + departmentId,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [ {
						"sTitle" : "Purchase_ID",
						"bVisible" : false
					}, {
						"sTitle" : "Purchase Number"
					}, {
						"sTitle" : "Date"
					}, {
						"sTitle" : "Requisition Number"
					}, {
						"sTitle" : "Receive Number"
					}, {
						"sTitle" : 'Supplier' 
					}, {
						"sTitle" : "Code"
					}, {
						"sTitle" : 'Product Name'
					}, {
						"sTitle" : 'Unit'
					}, {
						"sTitle" : 'Unit Rate' 
					}, {
						"sTitle" : 'Quantity' 
					}, {
						"sTitle" : 'Total' 
					} ],
					"sScrollY" : $("#main-content").height() - 235,
					//"bPaginate": false,
					"aaSorting" : [ [ 1, 'desc' ] ],
					"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
						if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
							$(nRow).addClass('row_selected');
						}
					}
				});
	}
	
	$('.common-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();
		
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_supplier_combo_list.action", 
		 	async: false,  
		 	data:{itemNature:'D', rowId:0, pageInfo: "purchase"},
		    dataType: "html",
		    cache: false,
			success:function(result){
					alert(result);
				 $('.common-result').html(result); 

			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		}); 
		
	});

	function changeEventHandler() {
		var fromDate = null;
		var toDate = null;
		var supplier =null;
		var isReceived=null;
		var productId=0;
		var departmentId=0;
				
		if ($("#productId option:selected").val() != null
				&& $("#productId option:selected").val() != "") {
			productId = $("#productId option:selected").val();
		} 
		
		if ($("#isReceived option:selected").val() != null
				&& $("#isReceived option:selected").val() != "") {
			isReceived = $("#isReceived option:selected").val();
		} 
		
		if ($("#supplierId option:selected").val() != null
				&& $("#supplierId option:selected").val() != "") {
			supplier = $("#supplierId option:selected").val();
		} 
		if ($('.fromDate').val() != null && $('.fromDate').val() != '') {
			fromDate = $('.fromDate').val();
			toDate = $('.toDate').val();
		}
		if ($('.toDate').val() != null && $('.toDate').val() != '') {
			fromDate = $('.fromDate').val();
			toDate = $('.toDate').val();			
		}

		if ($("#departmentId option:selected").val() != null
				&& $("#departmentId option:selected").val() != "") {
			departmentId = $("#departmentId option:selected").val();
		} 
		getPurchaseOrdersList(fromDate,toDate,productId,isReceived,supplier,departmentId);

	}
	
	
	 $(".print-call").click(function(){
		 
		 var fromDate = null;
			var toDate = null;
			var supplier =null;
			var isReceived=null;
			var productId=0;
			var departmentId=0;		
			if ($("#productId option:selected").val() != null
					&& $("#productId option:selected").val() != "") {
				productId = $("#productId option:selected").val();
			} 
			
			if ($("#isReceived option:selected").val() != null
					&& $("#isReceived option:selected").val() != "") {
				isReceived = $("#isReceived option:selected").val();
			} 
			
			if ($("#supplierId option:selected").val() != null
					&& $("#supplierId option:selected").val() != "") {
				supplier = $("#supplierId option:selected").val();
			} 
			if ($('.fromDate').val() != null && $('.fromDate').val() != '') {
				fromDate = $('.fromDate').val();
				toDate = $('.toDate').val();
			}
			if ($('.toDate').val() != null && $('.toDate').val() != '') {
				fromDate = $('.fromDate').val();
				toDate = $('.toDate').val();			
			}
			
			if ($("#departmentId option:selected").val() != null
					&& $("#departmentId option:selected").val() != "") {
				departmentId = $("#departmentId option:selected").val();
			} 
		 if (purchaseId != 0) {
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/purchaseorder_report_printout.action?fromDate='+fromDate+'&toDate='+toDate+'&purchaseId='+purchaseId,
						'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
		 } else {
				window.open('<%=request.getContextPath()%>/purchaseorder_full_report_printout.action?fromDate='+fromDate+'&toDate='+toDate+'&supplier='+supplier+'&received='+isReceived+'&departmentId='+departmentId+'&productId='+productId,
						'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;	
		 }
		 
		 });
	 
	  $(".pdf-download-call").click(function(){ 
		  if (purchaseId != 0) {
				 fromDate=$('.fromDate').val();
				 toDate=$('.toDate').val();
				 percentage=$('#percentage').val();
	
				window.open('<%=request.getContextPath()%>/purchase_order_report_download_jasper.action?fromDate='
						+ fromDate + '&toDate='
						+ toDate + '&purchaseId='
						+ purchaseId, +'_blank',
				'width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;
	 		 }  else {
	 			$('#error_message').hide().html("Please select a record to view.").slideDown();
				$('#error_message').delay(3000).slideUp();
			 }
			
		}); 
	  
	  $(".xls-download-call").click(function(){
		  if (purchaseId != 0) {
				 fromDate=$('.fromDate').val();
				 toDate=$('.toDate').val();
				window.open('<%=request.getContextPath()%>/purchase_order_report_download_xls.action?fromDate='
										+ fromDate + '&toDate='
										+ toDate + '&purchaseId='
										+ purchaseId, +'_blank',
								'width=800,height=700,scrollbars=yes,left=100px,top=2px');
	
				return false;
		  } else {
			  $('#error_message').hide().html("Please select a record to view.").slideDown();
			  $('#error_message').delay(3000).slideUp();
		  }
		});
	  $('#isReceived').combobox({
			selected : function(event, ui) {
				//changeEventHandler();
			}
		});
	  
	  $('#supplierId').combobox({
			selected : function(event, ui) {
				//changeEventHandler();
			}
		});
	  
	  $('#status').combobox({
			selected : function(event, ui) {
				//changeEventHandler();
			}
		});
	  
	  $('.product').combobox({
			selected : function(event, ui) {
				//changeEventHandler();
			}
		}); 
	  var compKey='${THIS.companyKey}';
		if(compKey=='maghaweer'){
		$("#departmentFilter").show();
		  $('.departmentId').combobox({
				selected : function(event, ui) {
					//changeEventHandler();
				}
			}); 
		}
	  $('#list_grid').click(function(){ 
		  changeEventHandler();
		});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>purchase
			information
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="display: none;"></div>
			<c:if
				test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">
					<fmt:message key="${requestScope.success}" />
				</div>
			</c:if>
			<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">
					<fmt:message key="${requestScope.error}" />
				</div>
			</c:if>
			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> <input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /></label> <input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div style="display:none;">
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="accounts.purchaseorder.label.status" /></label> <select
						name="status" id="status" style="width: 51%;">
						<option value="">Select</option>
						<option value="1">Open</option>
						<option value="2">Close</option>
					</select>
				</div>
				<div>
					<label class="width30" for="productId">Product</label>
					<select id="productId" class="product" name="product" style="width: 51%;">
						<option value="0">--All--</option>
						<c:forEach var="product" items="${PRODUCT_INFO}">
							<option value="${product.productId}">${product.code} -- ${product.productName}</option>						
						</c:forEach> 
					</select>		
				</div>  
				<div id="departmentFilter" style="display: none;">
					<label class="width30" for="departmentId">Department</label>
					<select id="departmentId" class="departmentId" name="departmentId" style="width: 51%;">
						<option value="0">--All--</option>
						<c:forEach var="dept" items="${DEPARTMENT_LIST}">
							<option value="${dept.departmentId}">${dept.departmentName}</option>						
						</c:forEach> 
					</select>		
				</div>  
			</div>

			<div class="width45 float-right" style="padding: 5px;">
				<div>
					<label class="width30" for="supplierName">Supplier</label>
					<select id="supplierId">
						<option value="">Select</option>
						<c:forEach var="supplier" items="${suppliersList}">
							<option value="${supplier.supplierId}">${supplier.supplierName}</option>						
						</c:forEach>
					
					</select>		
				</div>  
		

				<div>
						<label class="width30" style="margin-top: 5px;">Status</label> <select
						name="received" id="isReceived" style="width: 51%;">
						<option value="">Select</option>
						<option value="true">Received</option>
						<option value="false">Not Received</option>
					</select>
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
					style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
					<div class="portlet-header ui-widget-header float-right"
						id="list_grid" style="cursor: pointer; color: #fff;">
						list gird
					</div> 
				</div>
			</div>
			

			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="purchase_list">
					<table class="display" id="example"></table>
				</div>
			</div>
			
		</div>
	</div>
	 <div style="display: none; position: absolute; overflow: auto; z-index: 9007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>  
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>