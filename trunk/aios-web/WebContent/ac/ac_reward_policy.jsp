<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var rewardPolicyId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
 	 oTable = $('#RewardPolicy').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_all_reward_policy.action",

		"aoColumns" : [ { 
			"mDataProp" : "policyName"
		 },{
			"mDataProp" : "couponVO.couponName"
		 },{
			"mDataProp" : "rewardPercentageOrAmount"
		 },{
			"mDataProp" : "rewardTypeName"
		 }], 
	});	 

	 
 	
 	
 	$jquery("#reward_policy_validation").validationEngine('attach'); 

	 $('.common-popup').click(function(){  
			$('.ui-dialog-titlebar').remove(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_coupon_popup.action", 
			 	async: false,  
			    dataType: "html",
			    data : {rowId : Number(0)},
			    cache: false,
				success:function(result){  
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0);
					$('.common-result').html(result);  
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('#common-popup').dialog({
		 autoOpen: false,
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
		
	$('#edit').click(function(){   
 		if(rewardPolicyId != null && rewardPolicyId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_reward_policy_entry.action", 
		     	async: false, 
		     	data:{rewardPolicyId: rewardPolicyId},
				dataType: "json",
				cache: false,
				success: function(response){ 
 					 $('#policyName').val(response.rewardPolicyVO.policyName);
					 $('#reward').val(response.rewardPolicyVO.reward);
					 $('#rewardType').val(response.rewardPolicyVO.rewardType);
					 $('#couponName').val(response.rewardPolicyVO.couponVO.couponName);
					 $('#couponId').val(response.rewardPolicyVO.couponVO.couponId);
					 $('#percentage').attr('checked', response.rewardPolicyVO.isPercentage ? true : false);
 					 $('#description').val(response.rewardPolicyVO.description);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
 		return false;
	});  

	$('#delete').click(function(){  
		if(rewardPolicyId != null && rewardPolicyId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_reward_policy.action", 
	     	async: false,
	     	data:{rewardPolicyId: rewardPolicyId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_reward_policy.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted.")
																			.slideDown(
																					);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown();
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert(
										"Please select a record to delete.");
								return false;
							}
							return false;
						});

		/* Click event handler */
		$('#RewardPolicy tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				rewardPolicyId = aData.rewardPolicyId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				rewardPolicyId = aData.rewardPolicyId;
			}
		});

		/* Click event handler */
		$('#RewardPolicy tbody tr').live('dblclick', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				rewardPolicyId = aData.rewardPolicyId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				rewardPolicyId = aData.rewardPolicyId;
			}
			$('#edit').trigger('click');
		});

		$('.reward_policy_discard').click(function(){ 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_reward_policy.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) { 
												$(
												'#common-popup')
												.dialog(
														'destroy');
										$(
												'#common-popup')
												.remove();
												$("#main-wrapper").html(result);
											}
										});
							}); 
		 

		$('.reward_policy_save').click(function(){   
			 if($jquery("#reward_policy_validation").validationEngine('validate')){
			var policyName = $('#policyName').val();
 			var reward = Number($('#reward').val()); 
 			var rewardType = Number($('#rewardType').val()); 
 			var couponId = Number($('#couponId').val()); 
 			var percentage = $('#percentage').attr('checked'); 
			var description =$('#description').val();  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/save_reward_policy.action", 
		     	async: false,
		     	data:{
		     			rewardPolicyId : rewardPolicyId, policyName: policyName, reward: reward, rewardType: rewardType,
		     			couponId: couponId, percentage: percentage, description: description
		     		 },
				dataType: "json",
				cache: false,
				success: function(response){  
					if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_reward_policy.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			'#common-popup')
																			.dialog(
																					'destroy');
																	$(
																			'#common-popup')
																			.remove();
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	if (rewardPolicyId > 0)
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						"Record updated")
																				.slideDown(
																						1000);
																	else
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						"Record created")
																				.slideDown(
																						1000);

																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else
								return false;
						});
		$('#coupon-common-popup').live('click', function() {
			$('#common-popup').dialog("close");
		});
		
		{ 
	 		rewardPolicyId: Number($('#rewardPolicyRecordId').val());
		 	if(rewardPolicyId > 0){
		 		$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_reward_policy_entry.action", 
			     	async: false, 
			     	data:{rewardPolicyId: Number($('#rewardPolicyRecordId').val())},
					dataType: "json",
					cache: false,
					success: function(response){ 
						 $('#policyName').val(response.rewardPolicyVO.policyName);
						 $('#reward').val(response.rewardPolicyVO.reward);
						 $('#rewardType').val(response.rewardPolicyVO.rewardType);
						 $('#couponName').val(response.rewardPolicyVO.couponVO.couponName);
						 $('#couponId').val(response.rewardPolicyVO.couponVO.couponId);
						 $('#percentage').attr('checked', response.rewardPolicyVO.isPercentage ? true : false);
						 $('#description').val(response.rewardPolicyVO.description);
					} 		
				});
		 	} 
		} 
	});
	function commonCouponMethod(couponId, couponName) {
		$('#couponId').val(couponId);
		$('#couponName').val(couponName);
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>reward
			policy
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div class="clearfix"></div>
			<form id="reward_policy_validation" style="position: relative;">
				<input type="hidden" id="rewardPolicyRecordId" value="${requestScope.rewardPolicyId}"/>
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 115px;">
								<div>
									<label for="percentage">Percentage </label> <input
										type="checkbox" id="percentage" name="percentage" />
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="6" id="description"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label for="policyName">Policy Name<span
										style="color: red;">*</span> </label> <input type="text"
										name="policyName" id="policyName" tabindex="1"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="reward">Reward<span style="color: red;">*</span>
									</label> <input type="text" name="reward" id="reward" tabindex="3"
										class="width50 validate[required,custom[number]]" />
								</div>
								<div>
									<label for="rewardType">Reward Type<span
										style="color: red;">*</span> </label> <select name="rewardType"
										id="rewardType" class="width51 validate[required]"
										tabindex="4">
										<option value="">Select</option>
										<c:forEach var="rewardType" items="${REWARD_TYPES}">
											<option value="${rewardType.key}">${rewardType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label for="coupon">Coupon<span style="color: red;">*</span>
									</label> <input type="text" name="couponName" id="couponName"
										tabindex="5" class="width50 validate[required]"
										readonly="readonly" /> <input type="hidden" id="couponId"
										name="couponId" /> <span class="button"> <a
										style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						<div
							class="portlet-header ui-widget-header float-right reward_policy_discard">
							<fmt:message key="accounts.common.button.cancel" />
						</div>
						<div
							class="portlet-header ui-widget-header float-right reward_policy_save">
							<fmt:message key="accounts.common.button.save" />
						</div>
					</div>
				</div>
			</form>
			<div id="rightclickarea">
				<div id="coupon_list">
					<table id="RewardPolicy" class="display">
						<thead>
							<tr>
								<th>Policy</th>
								<th>Coupon</th>
								<th>Reward</th>
								<th>Reward Type</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="accounts.common.button.edit" />
				</div>
			</div>
		</div>
	</div>
	<div
		style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		<div
			class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
			unselectable="on" style="-moz-user-select: none;">
			<span class="ui-dialog-title" id="ui-dialog-title-dialog"
				unselectable="on" style="-moz-user-select: none;">Dialog
				Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
				role="button" unselectable="on" style="-moz-user-select: none;"><span
				class="ui-icon ui-icon-closethick" unselectable="on"
				style="-moz-user-select: none;">close</span> </a>
		</div>
		<div id="common-popup" class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>
</div>