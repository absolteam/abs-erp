<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tender</title>
</head>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 12% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 40%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script>
	var fromDate="";
	var toDate="";
	var oTable; var selectRow = ""; var aData = ""; var aSelected = [];
	var tenderId = 0; var currencyId = 0;
	
	$(function() {
		 
		$('.formError').remove(); 
		
		 $('#startPicker,#endPicker').datepick({
			 onSelect: customRange, showTrigger: '#calImg'});	
		 
		  
		 
		tenderGridLoad();		 
		$('#example tbody tr').live('click', function () { 
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          tenderId = aData[0]; 
		          currencyId = aData[5]; 
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          tenderId = aData[0];
		          currencyId = aData[5]; 
		      }
			  $('#tenderId').val(tenderId);
		});
		
		 $('.fromDate').change(function(){  
			 if($('.toDate').val()!=null && $('.toDate').val()!='')
				 tenderGridLoad();
			 
		 }); 
		 
		 $('.toDate').change(function(){
			 
			 if($('.fromDate').val()!=null && $('.fromDate').val()!='')
				 tenderGridLoad();
		 });
		 
		 			
	});
	
	function customRange(dates) {
		if (this.id == 'startPicker') {
			$('.fromDate').trigger('change');
			$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('.toDate').trigger('change');
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
	function checkDataTableExsist(){
		oTable = $('#example').dataTable();
		oTable.fnDestroy();
		$('#example').remove();
		$('#gridDiv').html("<table class='display' id='example'></table>");
		tenderGridLoad();
	}
	function tenderGridLoad(){
		
		var fromDate=$('.fromDate').val();
		var toDate=$('.toDate').val(); 
		var isQuoted = null;
		var productId = null;
		
		if ($("#isQuoted option:selected").val() != undefined
				&& $("#isQuoted option:selected").val() != null
				&& $("#isQuoted option:selected").val() != "") {
			isQuoted = $("#isQuoted option:selected").val();
		} 
		
		if ($("#productId option:selected").val() != undefined
				&& $("#productId option:selected").val() != null
				&& $("#productId option:selected").val() != "") {
			productId = $("#productId option:selected").val();
		} 
		
	
		
		$('#example').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/json_tender_detail_list.action?fromDate="+fromDate+"&toDate="+toDate+"&quoted="+isQuoted+"&productId="+productId,
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "bDestroy" : true,
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "tenderId", "bVisible": false},	
				{ "sTitle": '<fmt:message key="accounts.tender.list.tenderNumber" />', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.tender.list.date" />', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.tender.list.expiry" />', "sWidth": "100px"}, 
				{ "sTitle": "currencyId", "bVisible": false},
				/* { "sTitle": '<fmt:message key="accounts.tender.currency" />', "sWidth": "100px"}, */ 
			],
			"sScrollY": $("#main-content").height() - 235,
			"aaSorting": [[1, 'asc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	
	
		oTable = $('#example').dataTable();		 
	}
	
	
	 $(".print-call").click(function(){ 
		 
		 if (tenderId != 0) {
		 fromDate=$('.fromDate').val();
		 toDate=$('.toDate').val(); 
		 tenderId=$('#tenderId').val();
			window.open('<%=request.getContextPath()%>/tender_report_printout.action?fromDate='+fromDate+'&toDate='+toDate+'&tenderId='+tenderId,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
		 } else {
			 $('#error_message').hide().html("Please select a record to view.").slideDown();
			$('#error_message').delay(3000).slideUp();
		 }
			
		});
	  $(".pdf-download-call").click(function(){ 
		  if (tenderId != 0) {
				 fromDate=$('.fromDate').val();
				 toDate=$('.toDate').val();
				 percentage=$('#percentage').val();
	
				window.open('<%=request.getContextPath()%>/Tender_report_download_jasper.action?fromDate='
						+ fromDate + '&toDate='
						+ toDate + '&tenderId='
						+ tenderId, +'_blank',
				'width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;
		  } else {
			  $('#error_message').hide().html("Please select a record to view.").slideDown();
			  $('#error_message').delay(3000).slideUp();
			 }
			
		}); 
	  $(".xls-download-call").click(function(){
		  if (tenderId != 0) {
				 fromDate=$('.fromDate').val();
				 toDate=$('.toDate').val();
				window.open('<%=request.getContextPath()%>/Tender_report_download_xls.action?fromDate='
													+ fromDate + '&toDate='
													+ toDate + '&tenderId='
													+ tenderId, +'_blank',
											'width=800,height=700,scrollbars=yes,left=100px,top=2px');
	
							return false;
			} else {
				$('#error_message').hide().html("Please select a record to view.").slideDown();
				$('#error_message').delay(3000).slideUp();
			 }
					});
	  
	 $('#productId').combobox({
		selected : function(event, ui) {
			tenderGridLoad();
		}
	});
	 
	$('#isQuoted').combobox({
		selected : function(event, ui) {
			tenderGridLoad();
		}
	});
</script>


<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.tender.tenderInformation" />
		</div>


		<div id="success_message" class="response-msg success ui-corner-all"
			style="display: none;"></div>
		<div id="error_message" class="response-msg error ui-corner-all"
			style="display: none;"></div>
		<div class="portlet-content">
			<input type="hidden" id="tenderId" />



				<table  width="100%">
					 <tr>
					 	<td width="8%"><fmt:message key="re.offer.offerDatefrom" /> :</td>
					 	<td width="30%">
					 		<input type="text" name="fromDate" class="width60 fromDate" 
					 			id="startPicker" readonly="readonly">
						</td>
					 	<td width="8%"><fmt:message
							key="re.offer.offerDateTo" /> :</td>
					 	<td width="30%">
					 		<input type="text" name="toDate"
								class="width60 toDate" id="endPicker" readonly="readonly">
					 	</td>
					 </tr>
					  <tr>
					 	<td>Product :</td>
					 	<td>
					 		<div class="width60">
						 		<select
									id="productId">
									<option value="">Select</option>
									<c:forEach var="product" items="${productList}">
										<option value="${product.productId}">${product.productName}</option>						
									</c:forEach>
								</select>
							</div>
					 	</td>
					 	<td>Is Quoted :</td>
					 	<td>
					 		<div class="width60">
						 		<select 
									id="isQuoted">
									<option value="">Select</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
					 	</td>
					 </tr>
				</table>














			<div class="width100 float-left" id="temp-result"></div>
			<div id="rightclickarea">
				<div id="gridDiv" style="max-height: 50%;">
					<table id="example" class="display"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>