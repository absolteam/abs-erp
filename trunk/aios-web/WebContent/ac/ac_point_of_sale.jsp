<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.icon-search-class {
	cursor: pointer;
	position: absolute;
	right: 5%;
	z-index: 1;
	margin: 4px 0px !important;
}

.keypad-inline {
	width: 75% !important;
}

.cart_table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

.cart_table thead>tr>th {
	text-align: center;
	background: #FAAC58;
	font-size: 13px;
}

.cart_table td {
	padding-right: 10px;
}

.cart_table tr:nth-child(even) {
	background: #58FA58;
}

.cart_table tr:nth-child(odd) {
	background: #F4FA58;
}

.rowid td {
	font-weight: bold;
	font-size: 18px;
	color: #000 !important;
}

.invoice-total-div>span {
	font-weight: bold !important;
	font-size: 20px !important;
	color: #000 !important;
	position: relative;
	top: 10px;
	right: 25%;
}
.ui-autocomplete-input{
	width:73%!important;
}
.nostock{font-size: 10px!important; color: #df0101;}


</style>
 <script type="text/javascript">
var accessCode=null;
var posSessionId='';

 	$(function() { 
 		
		$jquery("#pos_form_validation").validationEngine('attach'); 
		
		$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
			separator: '|', prompt: '', 
	    layout: ['7|8|9|' + $jquery.keypad.BACK, 
	        '4|5|6|' + $jquery.keypad.CLEAR, 
	        '1|2|3|' + $jquery.keypad.CLOSE, 
	        '.|0|00'],onClose: function(value, inst) { 
	        	$(this).trigger( "change" );}});
		
		var categorytdsize = $($("#maincategory-table>tbody>tr:first").children()).size();
 		if (categorytdsize < 4) {
 			$($("#maincategory-table>tbody>tr:first").children()).each(
 					function() {
 						$(this).addClass('float-left width20');
 					});
 		} 
 		
 		if('${POINT_OF_SALE.posSalesDate}' == null || '${POINT_OF_SALE.posSalesDate}' == '')
	 		$('#salesDate').datepick({ 
		   	 	defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
 		else
 			$('#salesDate').datepick();
 		
		$('.mainCategory').click(function(){  
			var alertCustomer = Number($('#alertCustomer').val());
			var customerId = Number($('#customerId').val());
			if(alertCustomer == 0 && customerId == 0){
				$('.customer-common-popup-new').trigger('click');
			}else{
				var categoryId = getRowId($(this).find('input').attr('id'));
				var specialProduct = $.trim($('#specialProduct_'+categoryId).val());
				var categoryName = $.trim($(this).text());
				if(specialProduct == "true"){  
		 			showProductDefinitions(categoryId, categoryName);
				}else{
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/show_categorybased_subcategory.action",
						async : false,
						data : {categoryId: categoryId, specialProduct: specialProduct},
						dataType : "html",
						cache : false,
						success : function(result) { 
							$("#filter-result").html(result);  
							$("#filter-result").show();
							$(".product-listing").hide();
						}
					});
					return false;
				}
			} 
			return false;
		});

		
		
		$('.subCategory').live('click',function(){ 
			var categoryId = getRowId($(this).find('input').attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_categorybased_posproduct.action",
				async : false,
				data : {categoryId: categoryId},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$("#filter-product-result").html(result);
					$(".product-listing").show();
				}
			});
			return false;
		});

		$('#pos-discard').click(function(){
			var securedSales = $.trim($('#securedSales').val());
			commonOrderDiscard();
			return false;
		});

		$('#pos-session-discard').live('click',function(){ 
			$('#pos-common-popup').dialog('close'); 
		});
		
		$('.callJq').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#temp-result', 
			loaderHeight:16, 
			loaderWidth:17 
		});
		
		
		$('#show-dispatch-orders').live('click',function(){ 
			$('#pos-common-popup').dialog('close'); 
			showAllDispatchOrder(); 
			return false;
		});

		$('#salesType').change(function(){  
			var saleType = Number($(this).val());
			$('.employeeserve').hide();
			$('.customerserve').show();
			$('#print-pos-order').show();
			$('#finalize-pos-order').hide();
			if(saleType == 3){
				$('.common-popup-new').trigger('click'); 
			} 
			else if(saleType == 4){
				$('.customerserve').hide();
				$('#print-pos-order').hide();
				$('#finalize-pos-order').show();
				$('.common-popup-employee').trigger('click');
				$('.employeeserve').show();
			}
		});
		
		$('.common-popup-employee').click(function(){  
			$('.ui-dialog-titlebar').remove();  
			tempid = $(this).parent().get(0);
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data:{personTypes: "1"},
			    dataType: "html",
			    cache: false,
				success:function(result){   
	 				 $('.common-result-new').html(result);  
					 $('#common-popup-new').dialog('open'); 
					 $($($('#common-popup-new').parent()).get(0)).css('top',0,'important');
				},
				error:function(result){   
 					 $('.common-result-new').html(result); 
				}
			});  
			return false;
		});

		 $('#salesdispatch-common-popup').live('click',function(){  
	    	 $('#common-popup-new').dialog('close'); 
	     });
		 
		 $('#customer-common-popup').live('click',function(){  
			 $('#alertCustomer').val(1);
	    	 $('#common-popup-new').dialog('close'); 
	     }); 
		 
		 $('#person-list-close').live('click',function(){  
	    	 $('#common-popup-new').dialog('close'); 
	     }); 
		 
		 $('#customer-common-create-popup').live('click',function(){ 
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove(); 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/special_customer_addentry.action", 
				 	async: false,  
				 	data: { pageInfo: "pos_invoice", customerId: Number(0)},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('#common-popup-new').dialog('close'); 
						 $('#temp-result').html(result); 
						 $('.callJq').trigger('click');   
					} 
				});  
		 });
		 
		 $('#customer-common-edit-popup').live('click',function(){  
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove(); 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/special_customer_addentry.action", 
				 	async: false,  
				 	data: { pageInfo: "pos_invoice", customerId: customerUpdateId},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						 $('#common-popup-new').dialog('close'); 
						 $('#temp-result').html(result); 
						 $('.callJq').trigger('click');   
					} 
				});  
		 });
	     
		$('.customer-common-popup-new').click(function(){  
			$('.ui-dialog-titlebar').remove();  
			tempid = $(this).parent().get(0); 
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_customer_popup_create.action", 
			 	async: false,  
			 	data: { pageInfo: "pos_invoice"},
			    dataType: "html",
			    cache: false,
				success:function(result){   
	 				 $('.common-result-new').html(result);  
					 $('#common-popup-new').dialog('open'); 
					 $($($('#common-popup-new').parent()).get(0)).css('top',0,'important');
				},
				error:function(result){   
 					 $('.common-result-new').html(result); 
				}
			});  
			return false;
		});
		
		$('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup-new').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result-new').html(result);
	                    $($($('#common-popup-new').parent()).get(0)).css('top',0,'important');
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result-new').html(result);
	               }
	           });
	            return false;
	   	}); 
		
		$('.rowid').live('dblclick',function(){
			var currentRowId = Number(getRowId($(this).attr('id')));
			var specialPos = $.trim($('#comboType_'+currentRowId).val()); 
			if(specialPos == "true"){
				var productId = Number($('#productid_'+currentRowId).val());
				var itemOrder = Number($('#itemOrder_'+currentRowId).val());
				 $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/filter_splproduct_modifydefinition.action",
	               data:{productId: productId, itemOrder: itemOrder},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('#temp-result').html(result);
	                    $('.callJq').trigger('click');   
	                    return false;
	               } 
	           }); 
			}
			return false;
		});
		
		
		
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="DELIVERY_OPTION"){
				$('#deliveryOption').html("");
				$('#deliveryOption').append("<option value=''>Select</option>");
				loadLookupList("deliveryOption");
				
			}
		});

		$('#common-popup-new').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 

		$('#pos-common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
		posSessionId='${posSessionId}';
		
		$('#finalize-pos-order').click(function(){  
			if($jquery("#pos_form_validation").validationEngine('validate')){
				if(Number($('#product_preview>.rowid').size())>0){ 
					var employeeId = Number($('#employeeId').val());  
					var salesType = Number($('#salesType').val()); 
					var deliveryOption = Number($('#deliveryOption').val());
					var employeeName = $('#employeeName').val();
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/employee_order_pointofsale.action",
						async : false,
						data:{employeeId: employeeId, deliveryOption: deliveryOption, salesType: salesType, employeeName: employeeName},
						dataType : "json",
						cache : false,
						success : function(response) {
							if(response.returnMessage == "SUCCESS"){ 
								$('#finalize-pos-order').remove();
								$('#print-pos-ordercopy').show();
								$('#pos-discard').text('Close');
								employeePrintHTML(); 
							} 
						}
					});
				}else{
					$('#product_preview').html('');
					var tableScript="<tr>"
						+"<td colspan='5'>Cart is empty</td>"
						+"</tr>";
						$('#product_preview').append(tableScript);
						$('#product_preview>tr>td').css('color','red'); 
						$('#product_preview>tr>td').css('font-weight','bold'); 
						$('#product_preview>tr>td').css('font-size','18px'); 
				}
			}
			return false;
		});
		
		$('#print-pos-ordercopy').click(function(){  
			employeePrintHTML(); 
			return false;
		});

		$('#print-pos-order').click(function(){   
			if($jquery("#pos_form_validation").validationEngine('validate')){
				if(Number($('#product_preview>.rowid').size())>0){ 
					var customerId = Number($('#customerId').val()); 
					var shippingId = Number($('#shippingDetail').val());
					var pointOfSaleId = Number($('#pointOfSaleId').val());
					var salesType = Number($('#salesType').val());
					var salesDate = $('#salesDate').val();
					var customerName = '';
					var shippingAddress = '';
					var deliveryOption = null; 
					if(customerId > 0){
						 customerName = $('#customerName').val();
						 shippingAddress = $('#shippingDetail :selected').text();
					}
					deliveryOption = Number($('#deliveryOption').val());
					deliveryOptionName = $('#deliveryOption :selected').text();
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/generate_pointofsale_invoice.action",
						async : false,
						data:{customerId: customerId, shippingId: shippingId, customerName: customerName,
							shippingAddress: shippingAddress,deliveryOption:deliveryOption, salesType: salesType,
							deliveryOptionName:deliveryOptionName,pointOfSaleId:pointOfSaleId, salesDate: salesDate},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('#stock-result').html(result); 
							var nonstocks = $('#nonStockProductIds').val();
							$('.nonstockTR, .nostock').remove();
							if(nonstocks == "" ){
								$('#stock-result').remove(); 
								$('#common-popup-new').dialog('destroy');		
								$('#common-popup-new').remove(); 
								$('#pos-common-popup').dialog('destroy');		
								$('#pos-common-popup').remove();   
								$('#pos-invoice-popup').dialog('destroy');		
								$('#pos-invoice-popup').remove(); 
								$('#common-popup').dialog('destroy');		
								$('#common-popup').remove(); 
								$("#main-wrapper").html(result); 
							} else{
								var tableScript="<tr class='nonstockTR'>"
									+"<td colspan='5'>Unavailable Stock</td>"
								+"</tr>";
								$('#product_preview').prepend(tableScript);
								$('#product_preview>tr>td').css('color','#df0101'); 
								$('#product_preview>tr>td').css('font-weight','bold'); 
								$('#product_preview>tr>td').css('font-size','18px');  
								$('.stockerrorList').each(function(){
									var errorprdId = $.trim($(this).attr('id'));  
									$('.rowid').each(function(){ 
										var lineId = getRowId($(this).attr('id')); 
										var prdId = Number($('#productid_'+lineId).val()); 
										if(prdId == errorprdId){
											$('#productname_'+lineId).append(" <span class='nostock'>(No Stock)</span>");
										}
									}); 
								}); 
							}
						}
					});
				}else{
					$('#product_preview').html('');
					var tableScript="<tr>"
						+"<td colspan='5'>Cart is empty</td>"
						+"</tr>";
						$('#product_preview').append(tableScript);
						$('#product_preview>tr>td').css('color','#df0101'); 
						$('#product_preview>tr>td').css('font-weight','bold'); 
						$('#product_preview>tr>td').css('font-size','18px'); 
				}
			}return false;
		});
		
		$('#save-order').click(function(){  
			var customerId = Number($('#customerId').val()); 
			var shippingId = Number($('#shippingDetail').val());
			var customerName = '';
			var shippingAddress = '';
			var deliveryOption = null;
			var deliveryOptionName="";
			if(customerId > 0){
				 customerName = $('#customerName').val();
				 shippingAddress = $('#shippingDetail :selected').text();
			}
			deliveryOption = Number($('#deliveryOption').val());
			deliveryOptionName = $('#deliveryOption :selected').text();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_pos_order.action",
				data:{customerId: customerId, shippingId: shippingId, customerName: customerName,
					shippingAddress: shippingAddress,deliveryOption:deliveryOption,
					deliveryOptionName:deliveryOptionName,posSessionId:posSessionId},
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) { 
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove(); 
					$('#pos-common-popup').dialog('destroy');		
					$('#pos-common-popup').remove(); 
					$("#main-wrapper").html(result);  
					posSessionId=null;
					return false;
				}
			});
			return false;
		});

		 var keyPressed = false; 
         var chars = [];  
         $(window).keypress(function (e) {
             if (e.which >= 48 && e.which <= 57) {
                 chars.push(String.fromCharCode(e.which));
             } 
             if (keyPressed == false) {  
                 setTimeout(function(){   
                     if (chars.length >= 10) { 
                         var barcode = chars.join(""); 
                         $("#barCode").val(barcode); 
                         $('#barCode').trigger('click');  
                        } 
                     chars = [];
                     keyPressed = false; 
                  },500);
             }
             keyPressed = true;  
           }); 

		$('#barCode').click(function(){ 
			var barCode = $.trim($(this).val()); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_productby_barcode.action",
				async : false,
				data : {barCode: barCode},
				dataType : "json",
				cache : false,
				success : function(response) { 
					if(response.productDetail != null){
						var previewObject = { 
								"productId" : Number(response.productDetail.productId),
								"productName" : response.productDetail.productName,
								"comboType" : false,
								"comboProductId" : Number(0),
								"specialProduct" : false
						};
						addProductPreview(previewObject);  
 						return false;
					}  
				}
			});
 			return false;
		});

		$('.product-search-1').comboboxpos({ 
			selected : function(event, ui) {
				var productId = Number($(this).val());
				var productName = $.trim($('.product-search-1 :selected')
						.text());  
				productName = productName.substring(productName.indexOf('-') + 1); 
				var previewObject = {
					"productId" : productId,
					"productName" : productName,
					"comboType" : false,
					"comboProductId" : Number(0),
					"specialProduct" : false
				};
				addProductPreview(previewObject);
			}
		});
		
		$('.productqty').live('change', function() {
			var rowId = getRowId($(this).attr('id'));
			var productId = Number($('#productid_'+rowId).val());
			var quantity = Number($('#productqty_'+rowId).val()); 
			commonOrderModification(rowId,productId,quantity);
			 
			return false;
		});

		$('.deleteoption').live('click', function() {
			var rowId = getRowId($(this).attr('id'));
			var productId = Number($('#productid_'+rowId).val()); 
			commonOrderDelete(rowId,productId); 
			return false;
		});
		
		
		
		if(Number($('#tempsalesType').val())>0)
			$('#salesType').val($('#tempsalesType').val());
		
		$('.ui-combobox-button').attr('disabled', true);
		
});
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function openSavedOrder(sessionId){
		posSessionId=sessionId;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_saved_pos_order.action",
			data:{pointOfSaleId:Number(posSessionId)},
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('#common-popup-new').dialog('destroy');		
				$('#common-popup-new').remove(); 
				$('#pos-common-popup').dialog('destroy');		
				$('#pos-common-popup').remove(); 
				$("#main-wrapper").html(result);
			}
		});
	}
	function commonOrderDiscard(){
		posSessionId='';
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/discard_pointofsales.action", 
			async : false,
			dataType : "html",
				cache : false,
			success : function(result) {
				$('#common-popup-new').dialog('destroy');		
				$('#common-popup-new').remove(); 
				$('#pos-invoice-popup').dialog('destroy');		
				$('#pos-invoice-popup').remove(); 
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove(); 
				$('#pos-common-popup').dialog('destroy');		
				$('#pos-common-popup').remove();  
				$("#main-wrapper").html(result);
			}
		});
	}
	function commonOrderModification(rowId,productId,quantity){
		if(rowId > 0){
			var displayPrice = Number(quantity * Number($('#unitprice_'+rowId).val())).toFixed(2);
			$('#price_'+rowId).text(displayPrice); 
			var specialProductId =  Number($('#comboProductId_'+rowId).val());
			var itemOrder = Number($('#itemOrder_'+rowId).val());
			var comboItemOrder = Number($('#comboItemOrder_'+rowId).val());
			var comboType = $('#comboType_'+rowId).val();
			if(specialProductId >= 0 && itemOrder >= 0 &&  comboItemOrder >= 0 && productId > 0){
				$('#product_preview').html(''); 
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/update_product_preview_session.action",
											async : false,
											dataType : "json",
											data : {
												productId : productId, quantity: quantity, specialProductId: specialProductId, itemOrder: itemOrder,
												comboType: comboType, comboItemOrder: comboItemOrder
											},
											cache : false,
											success : function(response) {
												var tableScript = "";
												var count = 0;
												var previewObjects = response.previewObjects;
									 			$(previewObjects)
														.each(
																function(index) {
																	count = count + 1;
																	tableScript += "<tr id='fieldrow_"+count+"' class='rowid' >"
																			+ "<td id='lineId_"+count+"'>"
																			+ count
																			+ "</td>"
																			+ "<td id='productname_"+count+"' class='productname'>"
																			+ previewObjects[index].productName
																			+ "</td>"
																			+ "<td><input type='text' id='productqty_"
																							+ count
																							+ "' class='productqty' value="
																							+ previewObjects[index].quantity
																							+ " style='border: 0px;'></td>"
																			+ "<td id='price_"+count+"' class='price' style='text-align: right;'>"
																			+ previewObjects[index].displayPrice
																			+ "</td>"
																			+ "<td><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'>"
																			+ "<img src='./images/cancel.png' width=10 height=25></img></span>"
																			+ "<input type='hidden' id='productid_"+count+"' value="+previewObjects[index].productId+">"
																			+ "<input type='hidden' id='unitprice_"+count+"' value="+previewObjects[index].finalUnitPrice+">"
																			+ "<input type='hidden' id='orderPrinted_"+count+"' value="+previewObjects[index].orderPrinted+">"
																			+ "<input type='hidden' id='comboProductId_"+count+"' value="+previewObjects[index].comboProductId+">"
																			+ "<input type='hidden' id='itemOrder_"+count+"' value="+previewObjects[index].itemOrder+">"
																			+ "<input type='hidden' id='comboType_"+count+"' value="+previewObjects[index].specialPos+">"
																			+ "<input type='hidden' id='comboItemOrder_"+count+"' value="+previewObjects[index].comboItemOrder+">"
																			+ "</td>" + "</tr>";
																});
													$('#product_preview').append(tableScript);
													 if($(".cart_table_div").height()>385)
														 $(".cart_table_div").css({"overflow-x":"hidden","overflow-y":"auto","height":"385px"}); 
													$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
														separator: '|', prompt: '', 
												    layout: ['7|8|9|' + $jquery.keypad.BACK, 
												        '4|5|6|' + $jquery.keypad.CLEAR, 
												        '1|2|3|' + $jquery.keypad.CLOSE, 
												        '.|0|00'],onClose: function(value, inst) { 
												        	$(this).trigger( "change" );}});
													calucaltePOSInvoice();	
													return false;
											}
										});
								return false;
			} else{return false;}
		}else{return false;} 
	}
	function calucaltePOSInvoice(){
		var totalPOSAmount = Number(0);
		$('.price')
		.each(
				function() {  
					totalPOSAmount += convertStringToDouble($.trim($(this).text()));  
		});
		$('.invoice-total').html(convertNumberToAmount(totalPOSAmount.toFixed(2)));
	}
	function convertNumberToAmount(number) { 
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function convertStringToDouble(amount) {
		return Number(amount.replace(/\,/g, ''));
	}
	function commonOrderDelete(rowId,productId){
		if(rowId > 0){
			var specialProductId =  Number($('#comboProductId_'+rowId).val());
			var itemOrder = Number($('#itemOrder_'+rowId).val());
			var comboItemOrder = Number($('#comboItemOrder_'+rowId).val());
			var comboType = $('#comboType_'+rowId).val();
			if(specialProductId >= 0 && itemOrder >= 0 && comboItemOrder >= 0){
				$('#product_preview').html(''); 
				$('.splproductset').remove();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/delete_product_preview_session.action",
											async : false,
					dataType : "json",
					data : {
						productId : productId, specialProductId: specialProductId, itemOrder: itemOrder, comboType: comboType, comboItemOrder: comboItemOrder
					},
					cache : false,
					success : function(response) {
						if (response.returnMessage == "SUCCESS") {
							var tableScript = "";
							var count = 0;
							var previewObjects = response.previewObjects;
							 if(previewObjects.length > 0){
								 $(previewObjects)
									.each(
											function(index) {
												count = count + 1;
												tableScript += "<tr id='fieldrow_"+count+"' class='rowid masterTooltip' title='"+previewObjects[index].storeName+"' >"
														+ "<td id='lineId_"+count+"'>"
														+ count
														+ "</td>"
														+ "<td id='productname_"+count+"' class='productname'>"
														+ previewObjects[index].productName
														+ "</td>"
														+ "<td><input type='text' id='productqty_"
																		+ count
																		+ "' class='productqty' value="
																		+ previewObjects[index].quantity
																		+ " style='border: 0px;'></td>"
														+ "<td id='price_"+count+"' class='price' style='text-align: right;'>"
														+ previewObjects[index].displayPrice
														+ "</td>"
														+ "<td><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'>"
														+ "<img src='./images/cancel.png' width=10 height=25></img></span>"
														+ "<input type='hidden' id='productid_"+count+"' value="+previewObjects[index].productId+">"
														+ "<input type='hidden' id='unitprice_"+count+"' value="+previewObjects[index].finalUnitPrice+">"
														+ "<input type='hidden' id='orderPrinted_"+count+"' value="+previewObjects[index].orderPrinted+">"
														+ "<input type='hidden' id='comboProductId_"+count+"' value="+previewObjects[index].comboProductId+">"
														+ "<input type='hidden' id='itemOrder_"+count+"' value="+previewObjects[index].itemOrder+">"
														+ "<input type='hidden' id='comboType_"+count+"' value="+previewObjects[index].specialPos+">"
														+ "<input type='hidden' id='comboItemOrder_"+count+"' value="+previewObjects[index].comboItemOrder+">"
														+ "</td>" + "</tr>";
											});
								$('#product_preview').append(tableScript);
								 if($(".cart_table_div").height()>385)
									 $(".cart_table_div").css({"overflow-x":"hidden","overflow-y":"auto","height":"385px"}); 
								$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
									separator: '|', prompt: '', 
							    layout: ['7|8|9|' + $jquery.keypad.BACK, 
							        '4|5|6|' + $jquery.keypad.CLEAR, 
							        '1|2|3|' + $jquery.keypad.CLOSE, 
							        '.|0|00'],onClose: function(value, inst) { 
							        	$(this).trigger( "change" );}});
								calucaltePOSInvoice();	
								return false;
							if(Number(response.pointOfSaleTempId) > 0){ 
								$('#'+response.pointOfSaleTempId).remove(); 
								$('#pointOfSaleId').val('');
							}
								 
					 			
								if($(".tab>tr").size() > 0){
									var i = 1;
										$('.rowid')
											.each(
													function() { 
														var lineId = getRowId($(
																this)
																.attr(
																		'id'));
														$(
																'#lineId_'
																		+ lineId)
																.html(
																		i);
														i = i + 1;
													}); 
									calucaltePOSInvoice();
								}else{
									var tableScript="<tr>"
									+"<td colspan='5' style='font-size: 18px;font-weight:bold;color: #000!important;'>Cart is empty</td>"
									+"</tr>";
									$('#product_preview').append(tableScript);
									$('.invoice-total').html("0.0");
								}  
							 }else{
									var tableScript="<tr>"
										+"<td colspan='5' style='font-size: 18px;font-weight:bold;color: #000!important;'>Cart is empty</td>"
										+"</tr>";
										$('#product_preview').append(tableScript);
										$('.invoice-total').html("0.0");
							 }
						} else {
							return false;
						}
					}
				});
			}else{return false;} 
		} else{return false;}
		return false;
	}
	function deleteSavedOrder(posSessionId){
		$('#pointOfSaleId').val(posSessionId); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/delete_saved_pos_order.action",
			data:{pointOfSaleId: Number(posSessionId)},
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				posSessionId=null;
				$('#common-popup-new').dialog('destroy');		
				$('#common-popup-new').remove(); 
				$('#pos-common-popup').dialog('destroy');		
				$('#pos-common-popup').remove(); 
				$("#main-wrapper").html(result);
			}
		});
		 
	}
	function addProductPreview(previewObject){
		$('.ui-autocomplete-input').val('');
		$('#product_preview').html(''); 
		var itemOrder =  Number(0);
		var productId = Number(0); 
		if (typeof $('#itemOrder').val() == "undefined") {
			itemOrder = Number(0);
			productId = Number(0); 
		}else{
			itemOrder = Number($('#itemOrder').val());
			productId = Number($('#specialComboProductId').val());
		} 
		 
   		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_product_preview_session.action",
			async : false,
			dataType : "json",
			data : {
				 productPreview : JSON.stringify(previewObject), posSessionId: posSessionId, itemOrder: itemOrder, specialProductId: productId
			},
			cache : false,
			success : function(response) {
 			var tableScript = "";
			var count = 0;
			var previewObjects = response.previewObjects;
 			$(previewObjects)
					.each(
							function(index) {
								count = count + 1;
								tableScript += "<tr id='fieldrow_"+count+"' class='rowid masterTooltip' title='"+previewObjects[index].storeName+"'>"
										+ "<td id='lineId_"+count+"'>"
										+ count
										+ "</td>"
										+ "<td id='productname_"+count+"' class='productname'>"
										+ previewObjects[index].productName
										+ "</td>"
										+ "<td><input type='text' id='productqty_"
														+ count
														+ "' class='productqty' value="
														+ previewObjects[index].quantity
														+ " style='border: 0px;'></td>"
										+ "<td id='price_"+count+"' class='price' style='text-align: right;'>"
										+ previewObjects[index].displayPrice
										+ "</td>"
										+ "<td><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'>"
										+ "<img src='./images/cancel.png' width=10 height=25></img></span>"
										+ "<input type='hidden' id='productid_"+count+"' value="+previewObjects[index].productId+">"
										+ "<input type='hidden' id='unitprice_"+count+"' value="+previewObjects[index].finalUnitPrice+">"
										+ "<input type='hidden' id='orderPrinted_"+count+"' value="+previewObjects[index].orderPrinted+">"
										+ "<input type='hidden' id='comboProductId_"+count+"' value="+previewObjects[index].comboProductId+">"
										+ "<input type='hidden' id='itemOrder_"+count+"' value="+previewObjects[index].itemOrder+">"
										+ "<input type='hidden' id='comboType_"+count+"' value="+previewObjects[index].specialPos+">"
										+ "<input type='hidden' id='comboItemOrder_"+count+"' value="+previewObjects[index].comboItemOrder+">"
										+ "</td>" + "</tr>";
							});
				$('#product_preview').append(tableScript);
				 if($(".cart_table_div").height()>385)
					 $(".cart_table_div").css({"overflow-x":"hidden","overflow-y":"auto","height":"385px"}); 
				$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
					separator: '|', prompt: '', 
			    layout: ['7|8|9|' + $jquery.keypad.BACK, 
			        '4|5|6|' + $jquery.keypad.CLEAR, 
			        '1|2|3|' + $jquery.keypad.CLOSE, 
			        '.|0|00'],onClose: function(value, inst) { 
			        	$(this).trigger( "change" );}});
				calucaltePOSInvoice();	
				return false;
			}
		});
		return false; 
	}
	function showShippingSite(customerId) {
		$('#shippingDetail').html('');
		$('#shippingDetail').append('<option value="">Select</option>');
		$
				.ajax({
					type : "POST",
					url : "<%=request.getContextPath()%>/show_customer_shipping_site.action",
					async : false,
					data : {
						customerId : customerId
					},
					dataType : "json",
					cache : false,
					success : function(response) {
						$(response.aaData)
								.each(
										function(index) {
											$('#shippingDetail')
													.append(
															'<option value='
								+ response.aaData[index].shippingId
								+ '>'
																	+ response.aaData[index].name
																	+ '</option>');
										});
					}
				});
		return false;
	}

	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#customerId').val(customerId);
		$('#customerName').val(customerName); 
 	}  
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	
	function showAllSavedOrder(){
		$('.ui-dialog-titlebar').remove();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_all_saved_pos_order.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$(".pos-common-result").html(result);
						$('#pos-common-popup').dialog('open');
						$($($('#pos-common-popup').parent()).get(0)).css('top',
								0);
					}
				});
		return false;
	}
	
	function showAllDispatchOrder(){
		$('.ui-dialog-titlebar').remove();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_all_sales_dispatch_order.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$(".common-result-new").html(result);
						$('#common-popup-new').dialog('open');
						$($($('#common-popup-new').parent()).get(0)).css('top',
								0);
					}
				});
		return false;
	}
	
	function selectedDispatchSales(salesDetails){
		var pointOfSaleId = salesDetails.pointOfSaleId;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/dispatch_pointofsale_invoice.action",
			async : false,
			data:{pointOfSaleId: pointOfSaleId},
			dataType : "html",
			cache : false,
			success : function(result) { 
				$('#common-popup-new').dialog('destroy');		
				$('#common-popup-new').remove(); 
				$('#pos-common-popup').dialog('destroy');		
				$('#pos-common-popup').remove(); 
				$("#main-wrapper").html(result);
				return false;
			}
		});
		return false;
	}
	
	function showProductDefinitions(productId, productName){
		$("#filter-product-result").html(''); 
		$("#filter-product-result").hide(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/filter_product_definition.action",
					async : false,
					data: {productId: productId},
					dataType : "html",
					cache : false,
					success : function(result) { 
						$("#filter-result").html(result); 
						var previewObject = { 
								"productId" : productId,
								"productName" : productName,
								"comboType" : false,
								"comboProductId" : productId,
								"specialProduct" :true
						};
			 			addProductPreview(previewObject);  
					}
				});
		$("#filter-result").show();
		return false;
	}
	
	function showSpecialProductDefinitions(productId, productName){ 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/filter_specialproduct_definition.action",
					async : false,
					data: {productId: productId, itemOrder: Number(0)},
					dataType : "html",
					cache : false,
					success : function(result) { 
						$("#temp-result").html(result); 
						$('.callJq').trigger('click');  
						var previewObject = { 
								"productId" : productId,
								"productName" : productName,
								"comboType" : false,
								"comboProductId" : productId,
								"specialProduct" :true
						};
			 			addProductPreview(previewObject);  
					}
				});
		$("#filter-result").show();
	}
	
	function showProductDefinitionFilter(productDefinitionId, productId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/filter_productsubcategory_definition.action",
					async : false,
					data: {productDefinitionId: productDefinitionId, productId: productId},
					dataType : "html",
					cache : false,
					success : function(result) { 
						$("#filter-product-result").html(result);  
					}
				});
		$("#filter-product-result").show();
	} 
	
	function showSpecialProductDefinitionFilter(productDefinitionId, productId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/filter_productsubcategory_definition.action",
					async : false,
					data: {productDefinitionId: productDefinitionId, productId: productId},
					dataType : "html",
					cache : false,
					success : function(result) { 
						$("#filter-splproduct-result").html(result);  
					}
				});
		$("#filter-splproduct-result").show();
	} 
	
	function showSpecialProductDefinitionFilterPopup(productDefinitionId, productId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/filter_productsub_definitionpopup.action",
					async : false,
					data: {productDefinitionId: productDefinitionId, productId: productId},
					dataType : "html",
					cache : false,
					success : function(result) { 
						$("#filter-splproduct-result").html(result);  
					}
				});
		$("#filter-splproduct-result").show();
	} 
	
	function personPopupResult(personId, personName, params){
		$('#employeeName').val(personName);
		$('#employeeId').val(personId);
		$('#common-popup-new').dialog('close');
 	}
	
	function checkModificationDeleteApprover(){
		var pointOfSaleId = Number($('#pointOfSaleId').val());
		var ordPrinted =false;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/check_pos_printorder.action",
			data:{pointOfSaleId:pointOfSaleId},
			async : false,
			dataType : "json",
			cache : false,
			success : function(response) {
				ordPrinted=response.printerFlag;
			}
		});
		if(ordPrinted){
			 $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup-new').dialog('open');
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/validate_approver.action",
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('.common-result-new').html(result);
				}
			});
			return false;
		}else{
			return true;
		}
	
	}
</script>
<div id="main-content" style="overflow: hidden;"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"
			style="padding: 8px 4px 4px 8px !important; width: auto;">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Point Of Sale :: <i>${requestScope.SALES_SOTRE_NAME}</i> 
			<c:if test="${sessionScope.SHIFT_SESSION ne null && fn:length(sessionScope.SHIFT_SESSION) > 0}">
				<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> | Work Schedule :: 
				<i>
					<c:forEach var="shift" items="${sessionScope.SHIFT_SESSION}">
						${shift.employeeName} (${shift.startTime} - ${shift.endTime})<span>,</span>
					</c:forEach>
				</i>
			</c:if> 
			<div class="width100 float-right">
				<input type="hidden" id="alertCustomer" value="0"/>
				<c:set var="fieldLength" value="${fn:length(POS_ORDERS_SESSION)}" />
				<c:forEach items="${POS_ORDERS_SESSION}" varStatus="status">
					<c:if test="${status.index + 1 <= 4}">
						<c:choose>
							<c:when test="${status.count % 2 == 0}">
								<div
									style="margin-right: 15px; width: 23%; background: #81F7D8;"
									class="float-left"
									id="${POS_ORDERS_SESSION[fieldLength - status.count].pointOfSaleId}">
									<span class="float-left count_index">${status.index+1} .</span>
									<span class="float-left" style="cursor: pointer;"
										onclick="javascript:openSavedOrder('${POS_ORDERS_SESSION[fieldLength - status.count].pointOfSaleId}')">
										${POS_ORDERS_SESSION[fieldLength -
										status.count].deliveryOptionName}</span> <span
										style="cursor: pointer; float: right;"
										onclick="javascript:deleteSavedOrder('${POS_ORDERS_SESSION[fieldLength - status.count].pointOfSaleId}')">
										<img width="10" height="10" src="./images/cancel.png"> </span>
								</div>
							</c:when>
							<c:otherwise>
								<div
									style="margin-right: 15px; width: 23%; background: #9FF781;"
									class="float-left"
									id="${POS_ORDERS_SESSION[fieldLength - status.count].pointOfSaleId}">
									<span class="float-left count_index">${status.index+1} .</span>
									<span class="float-left" style="cursor: pointer;"
										onclick="javascript:openSavedOrder('${POS_ORDERS_SESSION[fieldLength - status.count].pointOfSaleId}')">
										${POS_ORDERS_SESSION[fieldLength -
										status.count].deliveryOptionName}</span> <span
										style="cursor: pointer; float: right;"
										onclick="javascript:deleteSavedOrder('${POS_ORDERS_SESSION[fieldLength - status.count].pointOfSaleId}')">
										<img width="10" height="10" src="./images/cancel.png"> </span>
								</div>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:forEach>
				<c:if
					test="${POS_ORDERS_SESSION ne null && fn:length(POS_ORDERS_SESSION) > 4}">
					<div class="float-right" style="height: 1px;">
						<span class="button float-right"
							style="position: relative; right: 15px; top: -11px; width: 15px;">
							<a
							style="background: none repeat scroll 0 0 #FAAC58; cursor: pointer; float: left; height: 5px; width: 100%;"
							onclick="javascript:showAllSavedOrder()"
							class="btn ui-state-default ui-corner-all"> <span
								class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div>
				</c:if>
				
				<c:if
					test="${DISPATCH_ORDER ne null && DISPATCH_ORDER eq true
						 && fn:length(POS_ORDERS_SESSION) < 4}">
					<div class="float-right" style="height: 1px;">
						<span class="button float-right"
							style="position: relative; right: 15px; top: -11px; width: 15px;">
							<a
							style="background: none repeat scroll 0 0 #FAAC58; cursor: pointer; float: left; height: 5px; width: 100%;"
							onclick="javascript:showAllDispatchOrder()"
							class="btn ui-state-default ui-corner-all"> <span
								class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div>
				</c:if>
			</div>
		</div>
		<div class="clearfix"></div> 
		<div class="tempresult" style="display: none;"></div>
		<form name="pos_form_validation" id="pos_form_validation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width50"
					style="display: none;"></div>
				<input type="hidden" id="securedSales" value="${POINT_OF_SALE.orderPrinted}"/>	
				<input type="hidden" id="pointOfSaleId"
					value="${POINT_OF_SALE.pointOfSaleId}" /> <input type="hidden"
					id="barCode" name="barCode" />
				<div class="width100 float-left" id="hrm">
					<div class="width40 float-left">
						<fieldset style="height: 500px;">
							<div id="hrm" class="hastable width100">
								<div>
									<label class="width30" for="salesType"> Sales Type<span
										style="color: red;">*</span> </label> <select name="salesType"
										id="salesType" class="salesType validate[required] width58"> 
										<c:forEach var="saleType" items="${SALES_TYPE}">
											<c:choose>
												<c:when test="${saleType.value eq 'Take Away'}">
													<option value="${saleType.key}" selected="selected">${saleType.value}</option>
												</c:when>
												<c:otherwise>
													<option value="${saleType.key}">${saleType.value}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
									<input type="hidden" id="tempsalesType" value="${POINT_OF_SALE.saleType}"/>
								</div>
								<c:choose>
									<c:when test="${sessionScope.USER.username eq 'deerfeild_14' || sessionScope.USER.username eq 'marasy_14'
										|| sessionScope.USER.username eq 'raha_6' ||  sessionScope.USER.username eq 'spar_6' ||  sessionScope.USER.username eq 'alwahda_6'
										|| sessionScope.USER.username eq 'mushrif_6' }">
										<div>
											<label class="width30" for="salesDate"> Sales Date<span
												style="color: red;">*</span> </label>  
											<input type="text" id="salesDate" name="salesDate" value="${POINT_OF_SALE.posSalesDate}"
												class="salesDate width58" />
		 								</div>
	 								</c:when>
	 								<c:otherwise>
	 									<div style="display:none;">
											<label class="width30" for="salesDate"> Sales Date<span
												style="color: red;">*</span> </label>  
											<input type="text" id="salesDate" name="salesDate" value="${POINT_OF_SALE.posSalesDate}"
												class="salesDate width58" />
		 								</div>
	 								</c:otherwise>
 								</c:choose>
								<div class="customerserve">
									<label class="width30" for="customerName">Customer</label> <input
										type="text" readonly="readonly" name="customerName"
										id="customerName" value="${POINT_OF_SALE.customerName}"
										style="width: 56%;" /> <input type="hidden" id="customerId"
										value="${POINT_OF_SALE.customerId}" name="customerId" />
										<span
										class="button" style="position: relative; left: 5px;">
										<a style="cursor: pointer;" id="customer-popup"
										class="btn ui-state-default ui-corner-all customer-common-popup-new width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div class="employeeserve" style="display: none;">
									<label class="width30" for="employeeName">Employee</label> <input
										type="text" readonly="readonly" name="employeeName"
										id="employeeName" style="width: 56%;" /> <input type="hidden" id="employeeId"
										name="employeeId" />
										<span
										class="button" style="position: relative; left: 5px;">
										<a style="cursor: pointer;" id="employee-popup"
										class="btn ui-state-default ui-corner-all common-popup-employee width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label class="width30">Delivery Option</label>
									<div>
										<select id="deliveryOption" name="deliveryOption"
											class="width58">
											<option value="">Select</option>
											<c:forEach items="${DELIVERY_OPTION}" var="nltls">
												<c:choose>
													<c:when
														test="${POINT_OF_SALE.deliveryOption eq nltls.lookupDetailId}">
														<option value="${nltls.lookupDetailId}"
															selected="selected">${nltls.displayName}</option>
													</c:when>
													<c:otherwise>
														<option value="${nltls.lookupDetailId}">${nltls.displayName}
														</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select> <span class="button" id="DELIVERY_OPTION_1"
											style="position: relative; display: none;"> <a
											style="cursor: pointer;" id="DELIVERY_OPTION"
											class="btn ui-state-default ui-corner-all recruitment-lookup width8">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="deliveryOptionTemp"
											id="deliveryOptionTemp" />
									</div>
								</div>
								<div style="display: none;">
									<label class="width30" for="shippingDetail"> Shipping
										Address</label> <select name="shippingDetail" id="shippingDetail"
										class="width58">
										<option value="">Select</option>
									</select> <input type="hidden" name="shippingIdTemp" id="shippingIdTemp" value="0" />
								</div>
								<div class="cart_table_div"
									style="max-height: 385px; overflow-x: hidden; overflow-y: auto; height: 385px">
									<table class="width100 cart_table"
										style="margin-top: 5px !important;" title="Cart">
										<thead>
											<tr>
												<th style="width: 0.05%">S.NO</th>
												<th style="width: 7%">Product</th>
												<th style="width: 1%">Quantity</th>
												<th style="width: 3%">Price</th>
												<th style="width: 0.01%;">Del</th>
											</tr>
										</thead>
										<tbody class="tab" id="product_preview">
											<c:set var="totalPos" value="${0.00}" />
											<c:choose>
												<c:when
													test="${PRODUCT_PREVIEW ne null && PRODUCT_PREVIEW ne ''}">
													<c:forEach items="${PRODUCT_PREVIEW}" var="product"
														varStatus="status">
														<tr id="fieldrow_${status.index+1}" class="rowid">
															<td id="lineId_${status.index+1}">${status.index+1}</td>
															<td id="productname_${status.index+1}"
																class="productname">${product.productName}</td>
															<td><input type="text"
																id="productqty_${status.index+1}" class="productqty"
																value="${product.quantity}" style="border: 0px;">
															</td>
															<td id="price_${status.index+1}" class="price"
																style="text-align: right;">${product.displayPrice}</td>
															<td><span class="deleteoption"
																id="deleteoption_${status.index+1}"
																style="cursor: pointer;"> <img
																	src='./images/cancel.png' width=10 height=25></img> </span> <input
																type='hidden' id='productid_${status.index+1}'
																value="${product.productId}"> <input
																type='hidden' id='unitprice_${status.index+1}'
																value="${product.finalUnitPrice}"> <input
																type='hidden' id='orderPrinted_${status.index+1}'
																value="${product.orderPrinted}">
																<input
																type='hidden' id='comboProductId_${status.index+1}'
																value="${product.comboProductId}">
																<input
																type='hidden' id='itemOrder_${status.index+1}'
																value="${product.itemOrder}"> 
																<input
																type='hidden' id='comboItemOrder_${status.index+1}'
																value="${product.comboItemOrder}"> 
																<input
																type='hidden' id='comboType_${status.index+1}'
																value="${product.specialPos}"> 
																 <c:set
																	var="totalPos"
																	value="${totalPos +  (product.finalUnitPrice * product.quantity)}" />
															</td>
														</tr>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<tr>
														<td colspan="5"
															style="font-size: 18px; color: #000; font-weight: bold;">Cart
															is empty</td>
													</tr>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</div>
							</div>
						</fieldset>
						<div class="float-right invoice-total-div">
							<span class="invoice-total">${totalPos}</span>
						</div>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
							<div
								class="portlet-header ui-widget-header float-right pos-discard"
								id="pos-discard"
								style="background: #DF0101; color: #fff; padding: 10px;">
								Refresh</div>
							<div
								class="portlet-header ui-widget-header float-right generate-invoice"
								id="generate-invoice"
								style="background: #0B610B; color: #fff; padding: 10px; display: none;">Generate
								Invoice</div>
							<div
								class="portlet-header ui-widget-header float-left print-pos-order"
								id="print-pos-order"
								style="background: #01DF01; color: #fff; padding: 10px;">${sessionScope.pos_movetoinvoice}</div>
								<div
								class="portlet-header ui-widget-header float-left finalize-pos-order"
								id="finalize-pos-order"
								style="background: #01DF01; color: #fff; padding: 10px; display: none;">Finalize
								Order</div>
								<div
								class="portlet-header ui-widget-header float-left finalize-pos-order"
								id="print-pos-ordercopy"
								style="background: #01DF01; color: #fff; padding: 10px; display: none;">Print
								Copy</div>
						</div>
					</div> 
					<div class="width60 float-left" style="margin-top: -3px;">   
						<div class="float-right width98">
							<div class="float-right width50">
								<span>Product</span>
								 <select
									name="productSearch" id="productSearch-1" class="product-search-1">
									<option value="">Search</option>
									<c:forEach var="productDetails" items="${PRODUCT_DETAILS}">
										<option value="${productDetails.productId}">${productDetails.productName}</option>
									</c:forEach>
								</select>
								<span class="float-right icon-search-class" style="right: 4%;"><img
									width="24" height="24" src="images/icons/Glass.png"> </span>
							</div> 
						</div> 
						<fieldset
							style="max-height: 130px;">
							<legend>
								<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Category
							</legend>
							<c:choose>
								<c:when test="${PRODUCT_CATEGORIES ne null && fn:length(PRODUCT_CATEGORIES)>0}">
									<div class="width100 content-item-table" 
										style="max-height: 100px; overflow-y: auto; overflow-x: hidden;">
										<table id="maincategory-table" class="pos-item-table width100">
											<c:set var="categoryRow" value="0" />
											<c:forEach begin="0" step="8" items="${PRODUCT_CATEGORIES}">
												<tr>
													<!-- TD 1 -->
													<td>
														<div class="box-model" style="background: #9FF781;">
															<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+0].categoryName}
																<input type="hidden"
																id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+0].productCategoryId}"
																class="mainCategoryId" /> 
																<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+0].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+0].productCategoryId}"/>
															</span>
														</div>
													</td>
													<!-- TD 2 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+1]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+1].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+1].productCategoryId}"
																	class="mainCategoryId" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+1].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+1].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<!-- TD 3 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+2]!= null}">
														<td>
															<div class="box-model" style="background: #9FF781;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+2].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+2].productCategoryId}"
																	class="mainCategoryId" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+2].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+2].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<!-- TD 4 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+3]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+3].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+3].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+3].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+3].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<!-- TD 5 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+4]!= null}">
														<td>
															<div class="box-model" style="background: #9FF781;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+4].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+4].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+4].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+4].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+5]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+5].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+5].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+5].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+5].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+6]!= null}">
														<td>
															<div class="box-model" style="background: #9FF781;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+6].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+6].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+6].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+6].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+7]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+7].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+7].productCategoryId}"
																	class="mainCategory" />
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+7].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+7].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if> 
													<c:set var="categoryRow" value="${categoryRow + 8}" />
												</tr>
											</c:forEach>
										</table> 
									</div>
								</c:when>
								<c:otherwise>
									<div style="font-size: 18px; color: #000; font-weight: bold;">No Categories found.</div>
								</c:otherwise>
							</c:choose> 
						</fieldset>
						
						<div id="filter-result" style="display: none;"> 
						</div> 
						<div style="margin-top: 5px; display:none;" id="filter-product-result" class="product-listing"> 
						</div>
					</div>
				</div>

			</div>
			<div class="clearfix"></div>
			<div align="center" id="content">
				<h1 id="title" style="display: none;"></h1> 
				<applet id="qz" code="qz.PrintApplet.class"
					archive="${pageContext.request.contextPath}/printing/applet/qz-print.jar" 
					width="0" height="0" align="left"> 
					<param name="jnlp_href" value="${pageContext.request.contextPath}/printing/applet/qz-print_jnlp.jnlp">
					<param name="cache_option" value="plugin">
				</applet>
			</div>
			<div class="clearfix"></div>
			<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<span class="callJq"></span>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup-new" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result-new width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>

			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="pos-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="pos-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="stock-result" style="display: none;"></div>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jZebra.js"></script>