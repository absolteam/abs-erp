<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var assetWarrantyId=0; 
var accessCode = "";
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	 oTable = $('#AssetWarrantyList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allasset_warranty.action",

		"aoColumns" : [ { 
			"mDataProp" : "assetName"
		 },{ 
			"mDataProp" : "warrantyNumber"
		 },{ 
			"mDataProp" : "warrantyType"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 }]
	});	 

	 	$('#add').click(function(){   
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/asset_warranty_entry.action", 
		     	async: false, 
		     	data:{assetWarrantyId: Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
		     		$("#main-wrapper").html(result);   
				} 		
			});  
	 		return false;
		});  
		
	$('#edit').click(function(){   
 		if(assetWarrantyId != null && assetWarrantyId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/asset_warranty_entry.action", 
		     	async: false, 
		     	data:{assetWarrantyId: assetWarrantyId},
				dataType: "html",
				cache: false,
				success: function(result){ 
		     		$("#main-wrapper").html(result);   
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
 		return false;
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(assetWarrantyId != null && assetWarrantyId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_asset_warranty.action", 
	     	async: false,
	     	data:{assetWarrantyId: assetWarrantyId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_asset_warranty.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted..").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
		return false;
	}); 
 
		/* Click event handler */
		$('#AssetWarrantyList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				assetWarrantyId = aData.assetWarrantyId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				assetWarrantyId = aData.assetWarrantyId;
			}
			return false;
		}); 
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			warranty
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div> 
			<div class="clearfix"></div>
			<div id="rightclickarea">
				<div id="asset_warranty_list">
					<table id="AssetWarrantyList" class="display">
						<thead>
							<tr>
								<th>Asset Name</th>
								<th>Warranty Number</th>
								<th>Warranty Type</th>
								<th>Start Date</th>
								<th>End Date</th>
							</tr>
						</thead> 
					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
			<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
			<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
		</div>
	</div>
</div>