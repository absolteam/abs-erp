<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
</style>
<script type="text/javascript"> 
var productId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#store-popup').dialog('destroy');		
	$('#store-popup').remove();
	$('#store-popuprcv').dialog('destroy');		
	$('#store-popuprcv').remove();
	$('#codecombination-popup').dialog('destroy');
	$('#codecombination-popup').remove();
	$('.formError').remove();
	$('#ProductDataTableList').dataTable({ 
		"sAjaxSource": "product_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Product_ID", "bVisible": false},
			{ "sTitle": "Code"},
			{ "sTitle": "Product"},
			{ "sTitle": 'Unit Name'}, 
			{ "sTitle": 'Item Type'},
			{ "sTitle": 'Sub Type'},
			{ "sTitle": 'Category'}, 
			{ "sTitle": 'Status'},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	
	$('#processJV').click(function(){   
		var pointOfSaleId = Number($('#pointOfSaleId').val());
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pointofsale_processjv.action", 
	     	async: false, 
	     	data:{pointOfSaleId: pointOfSaleId},
			dataType: "json",
			cache: false,
			success: function(response){ 
				$('#pointOfSaleId').val(response.pointOfSaleId);
				alert(response.pointOfSaleId);
			} 		
		}); 
	}); 
	
	$('#processJVbydates').click(function(){   
		var dateFrom = $('#dateFrom').val();
		var dateTo = $('#dateTo').val();
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pointofsale_processjvbydate.action", 
	     	async: false, 
	     	data:{dateFrom: dateFrom, dateTo: dateTo},
			dataType: "json",
			cache: false,
			success: function(response){  
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#posCharges').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pointofsale_processposcharges.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#deleteposduplciate').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pointofsale_duplicatesdelete.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#pointofsale_duplicates_delete').click(function(){   
		var dateFrom = $('#dateFrom').val();
		var dateTo = $('#dateTo').val();
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pointofsale_duplicates_delete.action", 
	     	async: false, 
			dataType: "json",
			data:{dateFrom: dateFrom, dateTo: dateTo},
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#pointofsale_updatereference').click(function(){   
		var dateFrom = $('#dateFrom').val();
		var dateTo = $('#dateTo').val();
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pointofsale_updatereference.action", 
	     	async: false, 
			dataType: "json",
			data:{dateFrom: dateFrom, dateTo: dateTo},
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#audit_pointofsale').click(function(){   
		var dateFrom = $('#dateFrom').val();
		var dateTo = $('#dateTo').val();
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/audit_pointofsale_transaction.action", 
	     	async: false, 
			dataType: "json",
			data:{dateFrom: dateFrom, dateTo: dateTo},
			cache: false,
			success: function(response){ 
 			} 		
		}); 
	}); 
	
	$('#audit_pointofsale_receipts').click(function(){   
		var dateFrom = $('#dateFrom').val();
		var dateTo = $('#dateTo').val();
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/audit_pointofsale_receipts.action", 
	     	async: false, 
			dataType: "json",
			data:{dateFrom: dateFrom, dateTo: dateTo},
			cache: false,
			success: function(response){ 
 			} 		
		}); 
	}); 
	
	$('#uploadProduct').click(function(){  
		var showPage=$("#xlFileName").val();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/upload_products.action", 
	     	async: false,
	     	data:{showPage: showPage},
			dataType: "json",
			cache: false,
			success: function(response){ 
				if(response.sqlReturnMessage=='SUCCESS'){
					alert("Uploaded successfully");
					return false;
				}else{
					alert("Error...");
					return false;
				}
			} 		
		}); 
		return false;
	}); 
	  
	$('#add').click(function(){  
		var showPage="add";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/product_addentry.action", 
	     	async: false,
	     	data:{productId: Number(0), showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
			} 		
		}); 
	}); 

	$('#edit').click(function(){   
		var showPage="edit";  
		if(productId>0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_addentry.action", 
		     	async: false,
		     	data:{productId:productId, showPage:showPage},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	}); 

	$('#delete').click(function(){  
		$('#success_message').hide();
		if(productId>0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/product_deletentry.action", 
		     	async: false,
		     	data:{productId:productId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);  
					 var message=$('#returnMsg').html(); 
					 if(message.trim()=="Success"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/product_retrieve.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record Deleted.").slideDown(1000);
									$('#success_message').delay(3000).slideUp();
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(3000).slideUp();
						 return false;
					 } 
				} 		
			}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$('#view').click(function(){  
		window.open('product_print_report.action?format=HTML','','width=1000,height=800,scrollbars=yes,left=100px');  
	});
	
	$('#making_print').click(function(){  
		 window.open('product_inventory_pdfprint.action?format=PDF', '',
			'width=800,height=800,scrollbars=yes,left=100px');  
 	});
	
	$('#making_finishedproduct_print').click(function(){  
		 window.open('product_finishedinventory_pdfprint.action?format=PDF', '',
			'width=800,height=800,scrollbars=yes,left=100px');  
	});

	//init datatable
	oTable = $('#ProductDataTableList').dataTable();
	 
	/* Click event handler */
	$('#ProductDataTableList tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          productId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          productId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Product</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<input type="hidden" id="pointOfSaleId" name="pointOfSaleId"/>
			<input type="hidden" id="dateFrom" name="dateFrom" value="01-May-2015"/>
			<input type="hidden" id="dateTo" name="dateTo" value="31-May-2015"/>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
			</c:if>
			<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="journal_accounts">
						<table class="display" id="ProductDataTableList"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>View</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div style="display: block;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
				<div class="portlet-header ui-widget-header float-left" id="uploadProduct">Upload Product</div>
				<input type="text" name="xlFileName" id="xlFileName" class="width60" value="D:\test\ProductMissed.csv">
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="pointofsale_updatereference" style="display: none;">UPDATE POS REFERENCE</div>
				<div class="portlet-header ui-widget-header float-right" id="pointofsale_duplicates_delete" style="display: none;">DELETE DUPLICATE POS</div>
				<div class="portlet-header ui-widget-header float-right" id="audit_pointofsale_receipts" style="display: none;">Audit Receipts</div>
				<div class="portlet-header ui-widget-header float-right" id="audit_pointofsale" style="display: none;">Audit Sales</div>
				<div class="portlet-header ui-widget-header float-right" id="posCharges" style="display: none;">POS Charges</div>
				<div class="portlet-header ui-widget-header float-right" id="deleteposduplciate" style="display: none;">Delete POS</div>
				<div class="portlet-header ui-widget-header float-right" id="processJV" style="display: none;">Process JV</div>
				<div class="portlet-header ui-widget-header float-right" id="processJVbydates" style="display: none;">Process JV ByDates</div>
 				<c:if test="${THIS.companyKey eq 'gcc' || THIS.companyKey eq 'gcc-coffee'}">
					<div class="portlet-header ui-widget-header float-right" id="making_finishedproduct_print">Finished Product Print</div> 
				</c:if> 
				<div class="portlet-header ui-widget-header float-right" id="making_print">Print</div>
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
 				<div class="portlet-header ui-widget-header float-right" id="edit"  ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>