 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var purchaseId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var receiveFlag=false;
$(function(){ 
	 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#store-popup').dialog('destroy');		
	$('#store-popup').remove();  
	$('#store-popuprcv').dialog('destroy');		
	$('#store-popuprcv').remove();
	$('#product-inline-popup').dialog('destroy');
	$('#product-inline-popup').remove();
	$('#product_json_list').remove();
	$('#ProductCommonList').remove();
	$('.formError').remove();
	
	$('#PurchaseDataTable').dataTable({ 
		"sAjaxSource": "purchase_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Purchase_ID", "bVisible": false},
			{ "sTitle": "Purchase Number","sWidth": "50px"},
			{ "sTitle": "Currency", "bVisible": false},
			{ "sTitle": "Total Amount","sWidth": "100px"},
			{ "sTitle": 'Date',"sWidth": "50px"}, 
			{ "sTitle": 'Expiry Date',"sWidth": "50px"}, 
			{ "sTitle": 'Supplier NO.', "bVisible": false}, 
			{ "sTitle": 'Supplier Name'}, 
			{ "sTitle": 'Payment Term',"sWidth": "100px"},
			{ "sTitle": "Receive", "bVisible": false},
			{ "sTitle": "Status"},
			{ "sTitle": "Receive Ref.","sWidth": "100px"},
			{ "sTitle": "Requsition Ref.","sWidth": "100px"},
			{ "sTitle": "Created By"}
		], 
		"sScrollY": $("#main-content").height() - 240,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	

	 $('#add').click(function(){   
		$('#success_message').hide();
		$('#error_message').hide();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/purchase_addentry.action", 
	     	async: false, 
	     	data:{purchaseId:Number(0),recordId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
			} 		
		}); 
	}); 
	 
	 $('#purchase_barcode').click(function(){    
		 if(purchaseId!=null && purchaseId!=0){
			 window.open('purchase_barcode_print_pdf.action?purchaseId='+ purchaseId + '&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');  
		 }else{
				alert("Please select a record to edit.");
				return false;
			}
		});

	$('#edit').click(function(){  
		$('.success').hide();
		$('.error').hide(); 
		if(purchaseId!=null && purchaseId!=0){
			if(receiveFlag){
				$('#error_message').hide().html("Can not edit!!! Goods Receive has been done for the selected PO ").slideDown();
				$('#error_message').delay(2000).slideUp();
				return false;
			}
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/purchase_editentry.action", 
	     	async: false,
	     	data:{purchaseId:purchaseId,recordId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
			} 		
		}); 
		}
		else{
			alert("Please select a record to edit.");
			return false;
		}
	}); 
	
	$('#view').click(function(){  
		$('.success').hide();
		$('.error').hide(); 
		if(purchaseId!=null && purchaseId!=0){
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/purchase_view.action", 
	     	async: false,
	     	data:{purchaseId:purchaseId,recordId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
			} 		
		}); 
		}
		else{
			alert("Please select a record to view.");
			return false;
		}
	}); 

	$('#delete').click(function(){  
		$('.success').hide();
		$('.error').hide(); 
		if(purchaseId!=null && purchaseId!=0){
			
		if(receiveFlag){
			$('#error_message').hide().html("Can not delete!!! Goods Receive has been done for the selected PO").slideDown(1000);
			$('#error_message').delay(3000).slideUp();
			return false;
		}
				
		  var cnfrm = confirm('Selected record will be deleted permanently');
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/purchase_deletentry.action", 
	     	async: false,
	     	data:{purchaseId:purchaseId},
			dataType: "json",
			cache: false,
			success: function(response){ 
  				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/getpurchaseorder.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
 						} 		
					}); 
					$('#success_message').hide().html("Record deleted.").slideDown(1000); 
					$('#success_message').delay(3000).slideUp();
				}
				else{
					$('#error_message').hide().html("Record delete failure").slideDown(1000);
					$('#error_message').delay(3000).slideUp();
					return false;
				} 
			} 		
		}); 
		}
		else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$("#print").click(function() {  
		if(purchaseId>0){	 
			window.open('show_purchase_printout.action?purchaseId='+ purchaseId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});

	//init datatable
	oTable = $('#PurchaseDataTable').dataTable();
	 
	/* Click event handler */
	$('#PurchaseDataTable tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          purchaseId=aData[0];
	          receiveFlag=aData[9];
	          if(receiveFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          purchaseId=aData[0];
	          receiveFlag=aData[9];
	          if(receiveFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	}); 
});
</script>
<div id="main-content"> 		
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>purchase order</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="purchase_list">
					<table class="display" id="PurchaseDataTable"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>View</span></div>
				<div class="first_li"><span>Print</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="purchase_barcode" >Barcode Print</div>
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit"  ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right " id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>