 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var customerId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
	 $('#codecombination-popup').dialog('destroy');		
	 $('#codecombination-popup').remove();  
	 $('#popup-representative').dialog('destroy');		
	 $('#popup-representative').remove();  
	 $('#job-common-popup').dialog('destroy');		
	 $('#job-common-popup').remove(); 
	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove();  
	$('#CustomerDT').dataTable({ 
		"sAjaxSource": "getcustomer_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Customer", "bVisible": false},
			{ "sTitle": "<fmt:message key="accounts.customer.customerno"/>"},
			{ "sTitle": "<fmt:message key="accounts.customer.customertype"/>", "bVisible": false}, 
			{ "sTitle": "<fmt:message key="accounts.customer.creditterm"/>", "bVisible": false}, 
			{ "sTitle": "<fmt:message key="accounts.customer.customertype"/>"}, 
			{ "sTitle": "<fmt:message key="accounts.customer.customername"/>"}, 
			{ "sTitle": "<fmt:message key="accounts.customer.salerepresentative"/>"},   
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	
	$('#uploadCustomer').click(function(){  
		var fileUploadFileName=$("#xlFileName").val();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/upload_person_customers.action", 
	     	async: false,
	     	data:{fileUploadFileName: fileUploadFileName},
			dataType: "json",
			cache: false,
			success: function(response){ 
				if(response.returnMessage=='SUCCESS'){
					alert("Uploaded successfully");
					return false;
				}else{
					alert("Error...");
					return false;
				}
			} 		
		}); 
		return false;
	}); 
	
	$('#updateCustomer').click(function(){  
		var fileUploadFileName=$("#xlFileName").val();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/update_customer_fromfile.action", 
	     	async: false,
	     	data:{fileUploadFileName: fileUploadFileName},
			dataType: "json",
			cache: false,
			success: function(response){ 
				if(response.returnMessage=='SUCCESS'){
					alert("Uploaded successfully");
					return false;
				}else{
					alert("Error...");
					return false;
				}
			} 		
		}); 
		return false;
	}); 
	 
	$('#updatecustomernumber').click(function(){   
		customerId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/update_customernumber.action", 
	     	async: false,  
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	});  
	
	$('#add').click(function(){   
		customerId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/customer_addentry.action", 
	     	async: false, 
	     	data:{customerId: customerId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
		if(customerId != null && customerId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/customer_addentry.action", 
		     	async: false, 
		     	data:{customerId: customerId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(customerId != null && customerId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/customer_deletentry.action", 
	     	async: false,
	     	data:{customerId: customerId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
				var message=$.trim($('.tempresult').html().toLowerCase()); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_all_customers.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Customer deleted..").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});  
	
	//init datatable
	oTable = $('#CustomerDT').dataTable();
	 
	/* Click event handler */
	$('#CustomerDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this );
	          customerId = aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this ); 
	          customerId = aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>customer</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="customer_list">
					<table class="display" id="CustomerDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" > 
				<div class="portlet-header ui-widget-header float-left" id="uploadCustomer">Upload Customer</div>
				<input type="text" name="xlFileName" id="xlFileName" class="width60">
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="updateCustomer" style="display: none;">Update Customer</div> 
				<div class="portlet-header ui-widget-header float-right" id="updatecustomernumber" style="display: none;">update customer number</div> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div> 
		</div>
	</div>
</div>