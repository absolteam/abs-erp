<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var selectedRowIds = [];
var oTable;

var fromDate = null;
var toDate = null;

var selectedCustomerId  = 0;
var selectedStoreId = 0;

populateDatatable();

$('#stores').combobox({
	selected : function(event, ui) {
		selectedStoreId = $('#stores :selected').val();
		populateDatatable();
	}
});


$('#customers').combobox({
	selected : function(event, ui) {
		selectedCustomerId  = $('#customers :selected').val();
		populateDatatable();
	}
});


function populateDatatable(){
	$('#CustomerPOSList').dataTable({ 
		"sAjaxSource": "gecustomer_sales_report_jsonlist.action?storeId="+selectedStoreId
			+"&customerId="+selectedCustomerId+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "POS_ID", "bVisible": false},
			{ "sTitle": "Reference"},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Store"},
			{ "sTitle": "Customer"},
	     ], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#CustomerPOSList').dataTable();
}

$('#CustomerPOSList tbody tr').live('click', function () {  
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
      aData =oTable.fnGetData( this );
      selectedRowId=aData[0];
      var position = $.inArray(selectedRowId, selectedRowIds);
      if ( position >= 0 )  {
   	  	selectedRowIds.splice(position, 1);
   	  }
  }
  else {
      oTable.$('tr.row_selected').removeClass('row_selected');
      $(this).addClass('row_selected');
      aData =oTable.fnGetData( this ); 
      selectedRowId=aData[0]; 
      selectedRowIds.push(selectedRowId);
  }
});


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
			window.open("<%=request.getContextPath()%>/get_POS_sales_report_XLS.action?userTillId="+selectedUserTillId
					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
 	
 	$(".print-call").click(function(){
 			window.open("<%=request.getContextPath()%>/getcustomer_sales_report_printout.action?pointOfSaleIds="+selectedRowIds+"&storeId="+selectedStoreId+
 		 			"&customerId="+selectedCustomerId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
 			window.open("<%=request.getContextPath()%>/get_customer_sales_report_pdf.action?pointOfSaleIds="+selectedRowIds+"&userTillId="+selectedUserTillId
 					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Customer Sales Report
		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr>
					<td>Customer</td>
					<td>
						<select id="customers" name="customers">
							<option value="">Select</option>
							<c:forEach var="customer" items="${CUSTOMER_LIST}">
								<option value="${customer.customerId}">${customer.customerName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>Store :</td>
					<td>
						<select id="stores">
							<option value="">Select</option>
							<c:forEach var="st" items="${STORE_LIST}">
								<option value="${st.storeId}">${st.storeName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td width="15%" align="center">
						From 
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td width="15%" align="center">
						To 
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
			</table>

			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="CustomerPOSList">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>