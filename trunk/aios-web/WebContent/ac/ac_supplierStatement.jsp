<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>

<script type="text/javascript">

var supplierId =0;
var oTable;

var fromDate = null;
var toDate = null;

supplierId =0;
getData();

function getData(){ 
	
	/* var personType=Number($('#person').val());
	var companyType=Number($('#company').val());
	var status=$('#status').val(); */
	
	
	
	 $('#example').dataTable(
				{
					"sAjaxSource" : 'get_supplier_statement_table_data.action?fromDate='+fromDate+'&toDate='+toDate+"&selectedSupplierId="+supplierId,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [
						{"sTitle" : "Supplier Id", "bVisible": false},
						{"sTitle" : "Transaction Number"},
						{"sTitle" : "Date"},
						{"sTitle" : "Currency"}, 
						{"sTitle" : "Amount"}, 
						{"sTitle" : "Description"}, 
					], 
				
				
				"sScrollY" : $("#main-content").height() - 235,
	    
	    
	
	});
	 oTable = $('#example').dataTable();
 }



	$('#example tbody tr').live('click', function () {
  	  if ( $(this).hasClass('row_selected') ) {
  		  $(this).addClass('row_selected');
          aData =oTable.fnGetData( this );
          supplierId=aData[0];
      }
      else {
          oTable.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
          aData =oTable.fnGetData( this );
          supplierId=aData[0];
      }
  });

					
	$(".xls-download-call").click(function(){
		
			window.open('<%=request.getContextPath()%>/get_supplier_statement_report_xls.action?fromDate='+fromDate+'&toDate='+toDate+"&selectedSupplierId="+supplierId,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
	
	});
	
 	$(".print-call").click(function(){ 
		 
 		/* var personType=Number($('#person').val());
 		var companyType=Number($('#company').val());
 		var status=$('#status').val(); */
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/get_supplier_statement_report_printout.action?fromDate='+fromDate+'&toDate='+toDate+"&selectedSupplierId="+supplierId+"&format=HTML",
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
		 
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		
				window.open('<%=request.getContextPath()%>/get_supplier_statement_report_pdf.action?fromDate='+fromDate+'&toDate='+toDate+"&selectedSupplierId="+supplierId+"&format=PDF",
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;	

	});

	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		height : 600,
		bgiframe : false,
		modal : true
	});
	
	
	$('#startPicker,#endPicker').datepick({
		onSelect : customRange,
		showTrigger : '#calImg'
	});
	
	function customRange(dates) {
		if (this.id == 'startPicker') {
			$('.fromDate').trigger('change');
			$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		} else {
			$('.toDate').trigger('change');
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			getData();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			getData();
		}

	});
</script>

<div id="main-content">
	
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Supplier Statement Report

		</div>
		<div class="portlet-content">
			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> : <input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /> &nbsp;&nbsp;  :</label> <input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>

			</div>

			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>