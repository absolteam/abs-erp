<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var voidPaymentId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('.formError').remove();
	$('#VoidChequeDT').dataTable({ 
		"sAjaxSource": "getvoidpaymment_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "VOIDPAYMENTID", "bVisible": false},  
			{ "sTitle": "Reference No"}, 
			{ "sTitle": "Date"}, 
			{ "sTitle": "Payment Type"}, 
			{ "sTitle": "Payment No"},
			{ "sTitle": "Bank"}, 
			{ "sTitle": "Account No"},  
			{ "sTitle": "Cheque Book No"}, 
			{ "sTitle": "Cheque No"}, 
			{ "sTitle": "Created By"}, 
			{ "sTitle": "Created Date"}, 
		], 
		"sScrollY": $("#main-content").height() - 345, 
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});

	$('#add').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/voidcheque_addentry.action", 
	     	async: false, 
			dataType: "html",
			data : {voidPaymentId : 0,recordId:Number(0)},
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
			} 		
		}); 
	}); 

	$('#edit').click(function(){   
		if(voidPaymentId!=null && voidPaymentId!='' && voidPaymentId > 0) {
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/voidcheque_addentry.action", 
		     	async: false,
		     	data:{voidPaymentId: voidPaymentId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);    
				} 		
			}); 
		} else {
			 alert("Please select a record to edit.");
			 return false;
		} 
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(voidPaymentId!=null && voidPaymentId!=0){
			 var cnfrm = confirm('Selected record will be deleted permanently');
				if(!cnfrm)
					return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_voidpayment.action", 
	     	async: false,
	     	data:{voidPaymentId: voidPaymentId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
				var message = $.trim($('.tempresult').html().toLowerCase()); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_all_voidpayment.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted.")
																			.slideDown(
																					1000);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message').hide()
															.html(message)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert(
										"Please select a record to delete.");
								return false;
							}
						});

		//init datatable
		oTable = $('#VoidChequeDT').dataTable();

		/* Click event handler */
		$('#VoidChequeDT tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				voidPaymentId = aData[0];
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				voidPaymentId = aData[0];
			}
		});
	});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>void cheque</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    <div id="rightclickarea">
			 	<div id="bank_receipts">
					<table class="display" id="VoidChequeDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
 				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div>
 				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>