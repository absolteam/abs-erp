<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
 .ui-autocomplete-input { width:60%!important;}
	  .ui-autocomplete{height:250px;
	  	overflow-y: auto;
	  	overflow-x: hidden;
	  	}
	  	.ui-combobox-button{height: 32px;}
</style>
<script type="text/javascript"> 
var transactionId=0;  
var oTable; var selectRow=""; var aSelected = []; var aData="";
var periodId = 0;
$(function(){
	$('.formError').remove();
	filterJsonTransaction(periodId);
	
	$('.print-call').click(function(){
		if(periodId!=null && periodId!=""){
			window.open('get_incomestatement_fiscal_report_printout.action?periodId='+ periodId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			 $('#error_message').hide().html("Please select a period.").slideDown();
			 $('#error_message').delay(2000).slideUp();
		}
		return false;
	}); 
	 
	/* Click event handler 
	$('#example tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          transactionId=aData[0];   
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          transactionId=aData[0];  
	      }
	});*/

	$('.pdf-download-call').click(function(){
		if(periodId!=null && periodId!="" && periodId > 0){
			window.open('show_incomestatement_fiscalreport.action?periodId='+ periodId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			$('#error_message').hide().html("Please select a period.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	});

	$(".xls-download-call").click(function(){ 
		if(periodId!=null && periodId!="" && periodId > 0){
			window.open('<%=request.getContextPath()%>/incomestatement_fiscal_excel_report.action?periodId='+ periodId ,
				'_blank','width=0,height=0'); 
		 }else{
			$('#error_message').hide().html("Please select a period.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	}); 
	
	$('.periodlist').combobox({ 
       selected: function(event, ui){   
    	   periodId=$(this).val(); 
          if(periodId>0){ 
        	  checkDataTableExsist(periodId); 
           }else{return false;} 
       }
	}); 

	$('#reset').click(function(){ 
		 checkDataTableExsist(0); 
	});
});
function checkDataTableExsist(periodId){
	oTable = $('#example').dataTable();
	oTable.fnDestroy();
	$('#example').remove(); 
	$('#fiscalstatement').html("<table class='display' id='example'></table>");
	filterJsonTransaction(periodId);
}
function filterJsonTransaction(periodId){ 
	$('#example').dataTable({ 
		"sAjaxSource": "transaction_jsonlist.action?periodId="+periodId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "JOURNAL_ID", "bVisible": false}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.vouchernumber"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.jvdate"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.periodname"/>'},
			{ "sTitle": '<fmt:message key="accounts.jv.label.currencycode"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.categoryname"/>'},
 			{ "sTitle": '<fmt:message key="accounts.jv.label.entrydate"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.description"/>'},
		], 
		"sScrollY": $("#main-content").height() - 225,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	//init datatable
	oTable = $('#example').dataTable();
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.ledgerfiscal.label.incomestatement"/> 
		</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			 <div class="width40 float-left">
	   	    	<label class="width30" style="padding: 5px;"><fmt:message key="accounts.calendar.label.periodname"/></label>
	   	    	<select name="periodlist" class="periodlist">
	   	    		<option></option>
	   	    		<c:forEach var="PERIOD" items="${PERIOD_LIST}">
	   	    			<option value="${PERIOD.periodId}">${PERIOD.name}</option>
	   	    		</c:forEach> 
	   	    	</select>
	   	    </div>
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="fiscalstatement">
				<table class="display" id="example"></table>
			</div> 
		</div> 
	</div> 
</div>
<div class="process_buttons">
		<div id="hrm" class="width100 float-right ">
		  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
		 	<div class="width5 float-right height30" title="Download as XLS" ><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
		  	<div class="width5 float-right height30" title="Print" ><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
		 </div>
</div> 