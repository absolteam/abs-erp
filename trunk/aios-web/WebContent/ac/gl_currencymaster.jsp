<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">

var id, code, desc, country, precision, status;
var currencyCode="";
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 

$(document).ready(function (){
	  
	$('.formError').remove(); 

	$('#CurrencyDT').dataTable({ 
		"sAjaxSource": "currecy_master_grid.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Currency ID', "bVisible": false},
			{ "sTitle": '<fmt:message key="accounts.currencymaster.label.currencycode"/>'},
			{ "sTitle": '<fmt:message key="accounts.currencymaster.label.description"/>'},
			{ "sTitle": '<fmt:message key="accounts.currencymaster.label.country"/>'}, 
		],
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#CurrencyDT').dataTable();

	/* Click event handler */
	$('#CurrencyDT tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        id=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        id=aData[0];
	    }
	});
 	$("#add").click( function() {  
 		 $('#othererror').hide();
		$.ajax({
			type:"GET", 
			url:"<%=request.getContextPath()%>/gl_currency_add.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	}); 

	$('#edit').click(function(){  
		if(id!=null && id > 0){
		$.ajax({
			type:"GET", 
			url:"<%=request.getContextPath()%>/gl_currency_edit.action", 
	     	async: false,
	     	data:{currencyId:id},
			dataType: "html",
			cache: false,
			error: function(data){
	     		 $("#main-wrapper").html(data);
				 $(".error").show();
				 $('.success').hide(); 
			},
	     	success: function(data){
				 $("#main-wrapper").html(data);
				 if($('.success').html()==""){
					 $(".error").show();
				 	$('.success').hide();
				 }
				 else{
					 $(".error").hide();
					 $('.success').show();
				 }
	     	} 
		}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}  
	}); 
	
	$('#view').click(function(){  
		if(id!=null && id>0){ 
		$.ajax({
			type:"GET", 
			url:"<%=request.getContextPath()%>/gl_currency_view.action", 
	     	async: false,
	     	data:{currencyId:id},
			dataType: "html",
			cache: false,
			error: function(data){
	     		 $("#main-wrapper").html(data);
				 $(".error").show();
				 $('.success').hide(); 
			},
	     	success: function(data){
				 $("#main-wrapper").html(data);
				 if($('.success').html()==""){
					 $(".error").show();
				 	$('.success').hide();
				 }
				 else{
					 $(".error").hide();
					 $('.success').show();
				 }
	     	} 
		}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}  
	}); 
	
	 $("#delete").click(function(){ 
		 if(id!=null && id>0){ 
			 var cnfrm = confirm('Selected record will be deleted permanently');
				if(!cnfrm)
					return false;
			$.ajax({
				type:"GET", 
				url:"<%=request.getContextPath()%>/gl_currency_delete.action", 
		     	async: false,
		     	data:{currencyId:id},
				dataType: "html",
				cache: false,
				error: function(data){
		     		 $("#main-wrapper").html(data);
					
				},
		     	success: function(data){
					 $("#main-wrapper").html(data);
		     	}  
			}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	 });

	 //to cancel the search
	 $('#cancel').click(function(){ 
		 $.ajax({
		 	type: "POST",  
		 	url: "<%=request.getContextPath()%>/currency_master_code.action",  
	      	async: false,
		 	dataType: "html",
		 	cache: false,
		 	error: function(data) {
		 		$("#main-wrapper").html(data);  //gridDiv main-wrapper
		 	},
	      	success: function(data){
		 		$("#main-wrapper").html(data);  //gridDiv main-wrapper
	      	}
		 }); 
	 }); 
});	 
</script>
 
 <body>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Currency
		</div> 
		<div class="portlet-content">
		<div>
			 <c:if test="${requestScope.errMsg1 ne null && requestScope.errMsg1 ne ''}">
				<div class="error response-msg ui-corner-all"><c:out value="${requestScope.errMsg1}"/></div>
			</c:if>
			<c:if test="${requestScope.succMsg1 ne null && requestScope.succMsg1 ne ''}">
		  	  <div class="success response-msg ui-corner-all"><c:out value="${requestScope.succMsg1}"/></div> 
	    	</c:if>
    	</div>
		<div  id="othererror" class="response-msg error ui-corner-all" style="width:80%; display:none;"></div>  
 	<div id="rightclickarea">	
 		<div id="gridDiv">
			<table class="display" id="CurrencyDT"></table>
		</div>
	</div>		
	<div class="vmenu">
	       	<div class="first_li"><span>Add</span></div>
		<div class="sep_li"></div>		 
		<div class="first_li"><span>Edit</span></div>
 		<div class="first_li"><span>Delete</span></div>
	</div>
	
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 

		<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.currencymaster.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right" id="view" style="display: none;">view</div>
		<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.currencymaster.button.edit"/></div>
		<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.currencymaster.button.add"/></div> 
	</div>

</div>
</div>
</div>
</body>