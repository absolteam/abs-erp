<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.lazyload.js?v=1.9.1"></script>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<style>
.icon-search-class {
	cursor: pointer;
	position: absolute;
	right: 5%;
	z-index: 1;
	margin: 4px 0px !important;
}

.keypad-inline {
	width: 75% !important;
}

.cart_table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

.cart_table thead>tr>th {
	text-align: center;
	background: #FAAC58;
	font-size: 13px;
}

.cart_table td {
	padding-right: 10px;
}

.cart_table tr:nth-child(even) {
	background: #58FA58;
}

.cart_table tr:nth-child(odd) {
	background: #F4FA58;
}

.rowid td {
	font-weight: bold;
	font-size: 18px;
	color: #000 !important;
}

.invoice-total-div>span {
	font-weight: bold !important;
	font-size: 20px !important;
	color: #000 !important;
	position: relative;
	top: 10px;
	right: 25%;
}
.ui-autocomplete-input{
	width:73%!important;
}
.nostock{font-size: 10px!important; color: #df0101;}

/* NEW STYLING */

#page-wrapper #main-wrapper #main-content {
	background: #fff; /* url(images/ordering-menu/company/gcc/menu_background.png) repeat; */
	border: none;
}

.ui-widget-content {
	background: transparent !important;
}

.content_left {
	height: 100%;
}

.wrap {
	display: none;
}

.portlet-content {
	background: transparent;
}

#hrm fieldset {
	border: none;
	
}

.box-model {
	/* background:  rgba(255, 255, 255, 0.73) !important; */
	background: #FFF none repeat scroll 0% 0% !important;
	border: 1px solid #E3E3E3;
	border-radius: 15px;
	/* box-shadow: 0px 0px 2px 1px #8F458F; */
	color: #8F458F;
	margin: 5px;
	transition: background-color 0.5s ease;
	width: 90px;
	display: inline-block !important;
	min-height: 105px;
	float: none;
}

.box-model:hover {
	background: rgba(120, 32, 113, 0.7) none repeat scroll 0% 0% !important;
	border: 1px solid #8F458F;
	border-radius: 5px;
	box-shadow: 0px 0px 2px 1px #8F458F;
	color: white;
}

.box-model-selected {
	background: rgba(120, 32, 113, 0.7) none repeat scroll 0% 0% !important;
	border: 1px solid #70365B;
	border-radius: 5px;
	box-shadow: 0px 0px 2px 1px #70365B;
	color: white;
}

.box-model-prod-selected {
	background: rgba(120, 32, 113, 0.7) none repeat scroll 0% 0% !important;
	border: 1px solid #70365B;
	border-radius: 5px;
	box-shadow: 0px 0px 2px 1px #70365B;
	color: white;
}

.content-item-table {
	height: auto !important;
	max-height: none;
	overflow: hidden;
}

.form-container {
	margin: 0 auto !important;
	background: rgba(224, 222, 222, 0.14) none repeat scroll 0% 0% !important;
	width: 75% !important;
	margin-top: 10px !important;
	height: auto;
	min-width: 265px;
	box-shadow: 0 0 2px 0 #ddd;
}

#filter-result > fieldset {
	max-height: auto;
	height: auto;
	overflow: auto;
}

.product_image {
	width: 70px;
	height: 70px;
	border: none;
	border-radius: 50%;
	margin: 0 auto;
	display: block;
	box-shadow: 0 0 5px 0 #E7E2DB;
	margin-top: 5px;
}

.box-model > span {
	display: block;
	font-weight: normal !important;
	font-size: 10px;
	min-height: 25px;
	vertical-align: middle;
	padding: 3px;
}

.box-model:hover > span {
	display: block;
	font-weight: bold !important;
}

.subCategory {
	display: inline-block !important;
}

.mainCategory {
	text-transform: none !important;
	display: inline-block !important;
}

#hrm span {
	font-family: inherit;
}

.heading_title {
	font-size: 17px !important;
	color: gray !important;
	padding-left: 30px;
	margin-top: 75px;
	display: inline-block;
	text-align: left;
	font-weight: normal !important;
	width: 100%;
}

.line {
	display: block;
	width: 90%;
	border-top: 1px solid #eee;
}

#bannerDiv {
	position: fixed;
    top: 0px;
    width: 100%;
    height: 50px;
    z-index: 99;
    background: rgba(255, 255, 255, 0.96);
    border-bottom: 1px solid #eee;
    box-shadow: 0px 0px 6px 0px #D2D2D2;
}

</style>
 <script type="text/javascript">
var accessCode=null;
var posSessionId='';
var topscroll = null;
var topscrollSubCat = null;

 	$(function() { 
 		
		$jquery("#pos_form_validation").validationEngine('attach'); 
		
		$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
			separator: '|', prompt: '', 
	    layout: ['7|8|9|' + $jquery.keypad.BACK, 
	        '4|5|6|' + $jquery.keypad.CLEAR, 
	        '1|2|3|' + $jquery.keypad.CLOSE, 
	        '.|0|00'],onClose: function(value, inst) { 
	        	$(this).trigger( "change" );}});
		
		var categorytdsize = $($("#maincategory-table>tbody>tr:first").children()).size();
 		if (categorytdsize < 4) {
 			$($("#maincategory-table>tbody>tr:first").children()).each(
 					function() {
 						$(this).addClass('float-left width20');
 					});
 		} 
 		
 		if('${POINT_OF_SALE.posSalesDate}' == null || '${POINT_OF_SALE.posSalesDate}' == '')
	 		$('#salesDate').datepick({ 
		   	 	defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
 		else
 			$('#salesDate').datepick();
 		
		$('.mainCategory').click(function(){  
			var categoryId = getRowId($(this).find('input').attr('id'));
			var specialProduct = $.trim($('#specialProduct_'+categoryId).val());
			var categoryName = $.trim($(this).text());
			if(specialProduct == "true"){  
	 			showProductDefinitions(categoryId, categoryName);
			}else{
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/online-menu-show-subcategory.action",
					async : false,
					data : {categoryId: categoryId, specialProduct: specialProduct},
					dataType : "html",
					cache : false,
					success : function(result) { 
						$(".box-model").removeClass('box-model-selected');
						$("#mainCatBoxModel_" + categoryId).addClass('box-model-selected');
						$("#filter-result").html(result);  
						$("#filter-result").fadeIn();
						$(".product-listing").hide();
						
						if (topscrollSubCat == null) topscrollSubCat = $(".sub-category-panel").offset().top;
						$('#main-content,html').animate({
					        scrollTop: topscrollSubCat - 50
					    }, 1000);
					}
				});
				return false;
			}
			return false;
		});

		
		
		$('.subCategory').live('click',function(){ 
			var categoryId = getRowId($(this).find('input').attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/online-menu-show-product-for-subcategory.action",
				async : false,
				data : {categoryId: categoryId},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$(".subCategory").removeClass('box-model-prod-selected');
					$("#subCatBoxModel_" + categoryId).addClass('box-model-prod-selected');
					$("#filter-product-result").html(result);
					$(".product-listing").show();
										
					topscroll = $(".products-panel").offset().top;
													
					$('#main-content,html').animate({
				        scrollTop: topscroll + topscrollSubCat
				    }, 1000);
					
				}
			});
			return false;
		});

		$('#pos-discard').click(function(){ 
			commonOrderDiscard();
			return false;
		});

		$('#pos-session-discard').live('click',function(){ 
			$('#pos-common-popup').dialog('close'); 
		});
		
		$('.callJq').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#temp-result', 
			loaderHeight:16, 
			loaderWidth:17 
		});
		
		
		$('#show-dispatch-orders').live('click',function(){ 
			$('#pos-common-popup').dialog('close'); 
			showAllDispatchOrder(); 
			return false;
		});  

		 $('#salesdispatch-common-popup').live('click',function(){  
	    	 $('#common-popup-new').dialog('close'); 
	     });
		 
		 $('#customer-common-popup').live('click',function(){  
	    	 $('#common-popup-new').dialog('close'); 
	     }); 
		 
		 $('#person-list-close').live('click',function(){  
	    	 $('#common-popup-new').dialog('close'); 
	     }); 
		 
		 $('#customer-common-create-popup').live('click',function(){ 
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/special_customer_addentry.action", 
				 	async: false,  
				 	data: { pageInfo: "pos_invoice", customerId: Number(0)},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('#common-popup-new').dialog('close'); 
						 $('#temp-result').html(result); 
						 $('.callJq').trigger('click');   
					} 
				});  
		 });
		 
		 $('#customer-common-edit-popup').live('click',function(){  
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/special_customer_addentry.action", 
				 	async: false,  
				 	data: { pageInfo: "pos_invoice", customerId: customerUpdateId},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						 $('#common-popup-new').dialog('close'); 
						 $('#temp-result').html(result); 
						 $('.callJq').trigger('click');   
					} 
				});  
		 });
	     
		$('.customer-common-popup-new').click(function(){  
			$('.ui-dialog-titlebar').remove();  
			tempid = $(this).parent().get(0);
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_customer_popup_create.action", 
			 	async: false,  
			 	data: { pageInfo: "pos_invoice"},
			    dataType: "html",
			    cache: false,
				success:function(result){   
	 				 $('.common-result-new').html(result);  
					 $('#common-popup-new').dialog('open'); 
					 $($($('#common-popup-new').parent()).get(0)).css('top',0,'important');
				},
				error:function(result){   
 					 $('.common-result-new').html(result); 
				}
			});  
			return false;
		});
		
		$('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup-new').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result-new').html(result);
	                    $($($('#common-popup-new').parent()).get(0)).css('top',0,'important');
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result-new').html(result);
	               }
	           });
	            return false;
	   	}); 
		
		$('.rowid').live('dblclick',function(){
			var currentRowId = Number(getRowId($(this).attr('id')));
			var specialPos = $.trim($('#comboType_'+currentRowId).val()); 
			if(specialPos == "true"){
				var productId = Number($('#productid_'+currentRowId).val());
				var itemOrder = Number($('#itemOrder_'+currentRowId).val());
				 $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/filter_splproduct_modifydefinition.action",
	               data:{productId: productId, itemOrder: itemOrder},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('#temp-result').html(result);
	                    $('.callJq').trigger('click');   
	                    return false;
	               } 
	           }); 
			}
			return false;
		});
		
		
		
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="DELIVERY_OPTION"){
				$('#deliveryOption').html("");
				$('#deliveryOption').append("<option value=''>Select</option>");
				loadLookupList("deliveryOption");
				
			}
		});

		$('#common-popup-new').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 

		$('#pos-common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
		posSessionId='${posSessionId}';   

		 var keyPressed = false; 
         var chars = [];  
         $(window).keypress(function (e) {
             if (e.which >= 48 && e.which <= 57) {
                 chars.push(String.fromCharCode(e.which));
             } 
             if (keyPressed == false) {  
                 setTimeout(function(){   
                     if (chars.length >= 10) { 
                         var barcode = chars.join(""); 
                         $("#barCode").val(barcode); 
                         $('#barCode').trigger('click');  
                        } 
                     chars = [];
                     keyPressed = false; 
                  },500);
             }
             keyPressed = true;  
           }); 

		$('#barCode').click(function(){ 
			var barCode = $.trim($(this).val()); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_productby_barcode.action",
				async : false,
				data : {barCode: barCode},
				dataType : "json",
				cache : false,
				success : function(response) { 
					if(response.productDetail != null){
						var previewObject = { 
								"productId" : Number(response.productDetail.productId),
								"productName" : response.productDetail.productName,
								"comboType" : false,
								"comboProductId" : Number(0),
								"specialProduct" : false
						};
						addProductPreview(previewObject);  
 						return false;
					}  
				}
			});
 			return false;
		});

		$('.product-search-1').comboboxpos({ 
			selected : function(event, ui) {
				var productId = Number($(this).val());
				var productName = $.trim($('.product-search-1 :selected')
						.text());  
				productName = productName.substring(productName.indexOf('-') + 1); 
				var previewObject = {
					"productId" : productId,
					"productName" : productName,
					"comboType" : false,
					"comboProductId" : Number(0),
					"specialProduct" : false
				};
				addProductPreview(previewObject);
			}
		});
		
		$('.productqty').live('change', function() {
			var rowId = getRowId($(this).attr('id'));
			var productId = Number($('#productid_'+rowId).val());
			var quantity = Number($('#productqty_'+rowId).val()); 
			commonOrderModification(rowId,productId,quantity);
			 
			return false;
		});

		$('.deleteoption').live('click', function() {
			var rowId = getRowId($(this).attr('id'));
			var productId = Number($('#productid_'+rowId).val()); 
			commonOrderDelete(rowId,productId); 
			return false;
		});
		
		
		
		if(Number($('#tempsalesType').val())>0)
			$('#salesType').val($('#tempsalesType').val());
		
		$('.ui-combobox-button').attr('disabled', true);		
});
 	
$(document).ready(function(){
	$("#main-content").scroll(function(){       
		//console.debug($("#main-content").scrollTop());		
		if ($("#main-content").scrollTop() > 100) {
            $("#bannerDiv").show();
        } else {
            $("#bannerDiv").hide();
        }
    });
}); 	

function getRowId(id) {
	var idval = id.split('_');
	var rowId = Number(idval[1]);
	return rowId;
}
	 
function commonOrderDiscard(){
	posSessionId='';
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/online-menu-discard.action", 
		async : false,
		dataType : "html",
			cache : false,
		success : function(result) {
			$('#common-popup-new').dialog('destroy');		
			$('#common-popup-new').remove(); 
			$('#pos-invoice-popup').dialog('destroy');		
			$('#pos-invoice-popup').remove(); 
			$('#common-popup').dialog('destroy');		
			$('#common-popup').remove(); 
			$('#pos-common-popup').dialog('destroy');		
			$('#pos-common-popup').remove();  
			$("#main-wrapper").html(result);
		}
	});
}
function commonOrderModification(rowId,productId,quantity){
	if(rowId > 0){
		var displayPrice = Number(quantity * Number($('#unitprice_'+rowId).val())).toFixed(2);
		$('#price_'+rowId).text(displayPrice); 
		var specialProductId =  Number($('#comboProductId_'+rowId).val());
		var itemOrder = Number($('#itemOrder_'+rowId).val());
		var comboItemOrder = Number($('#comboItemOrder_'+rowId).val());
		var comboType = $('#comboType_'+rowId).val();
		if(specialProductId >= 0 && itemOrder >= 0 &&  comboItemOrder >= 0 && productId > 0){
			$('#product_preview').html(''); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_product_preview_session.action",
										async : false,
										dataType : "json",
										data : {
											productId : productId, quantity: quantity, specialProductId: specialProductId, itemOrder: itemOrder,
											comboType: comboType, comboItemOrder: comboItemOrder
										},
										cache : false,
										success : function(response) {
											var tableScript = "";
											var count = 0;
											var previewObjects = response.previewObjects;
								 			$(previewObjects)
													.each(
															function(index) {
																count = count + 1;
																tableScript += "<tr id='fieldrow_"+count+"' class='rowid' >"
																		+ "<td id='lineId_"+count+"'>"
																		+ count
																		+ "</td>"
																		+ "<td id='productname_"+count+"' class='productname'>"
																		+ previewObjects[index].productName
																		+ "</td>"
																		+ "<td><input type='text' id='productqty_"
																						+ count
																						+ "' class='productqty' value="
																						+ previewObjects[index].quantity
																						+ " style='border: 0px;'></td>"
																		+ "<td id='price_"+count+"' class='price' style='text-align: right;'>"
																		+ previewObjects[index].displayPrice
																		+ "</td>"
																		+ "<td><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'>"
																		+ "<img src='./images/cancel.png' width=10 height=25></img></span>"
																		+ "<input type='hidden' id='productid_"+count+"' value="+previewObjects[index].productId+">"
																		+ "<input type='hidden' id='unitprice_"+count+"' value="+previewObjects[index].finalUnitPrice+">"
																		+ "<input type='hidden' id='orderPrinted_"+count+"' value="+previewObjects[index].orderPrinted+">"
																		+ "<input type='hidden' id='comboProductId_"+count+"' value="+previewObjects[index].comboProductId+">"
																		+ "<input type='hidden' id='itemOrder_"+count+"' value="+previewObjects[index].itemOrder+">"
																		+ "<input type='hidden' id='comboType_"+count+"' value="+previewObjects[index].specialPos+">"
																		+ "<input type='hidden' id='comboItemOrder_"+count+"' value="+previewObjects[index].comboItemOrder+">"
																		+ "</td>" + "</tr>";
															});
												$('#product_preview').append(tableScript);
												 if($(".cart_table_div").height()>385)
													 $(".cart_table_div").css({"overflow-x":"hidden","overflow-y":"auto","height":"385px"}); 
												$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
													separator: '|', prompt: '', 
											    layout: ['7|8|9|' + $jquery.keypad.BACK, 
											        '4|5|6|' + $jquery.keypad.CLEAR, 
											        '1|2|3|' + $jquery.keypad.CLOSE, 
											        '.|0|00'],onClose: function(value, inst) { 
											        	$(this).trigger( "change" );}});
												calucaltePOSInvoice();	
												return false;
										}
									});
							return false;
		} else{return false;}
	}else{return false;} 
}
function calucaltePOSInvoice(){
	var totalPOSAmount = Number(0);
	$('.price')
	.each(
			function() {  
				totalPOSAmount += convertStringToDouble($.trim($(this).text()));  
	});
	$('.invoice-total').html(convertNumberToAmount(totalPOSAmount.toFixed(2)));
}
function convertNumberToAmount(number) { 
	return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function convertStringToDouble(amount) {
	return Number(amount.replace(/\,/g, ''));
}
function commonOrderDelete(rowId,productId){
	if(rowId > 0){
		var specialProductId =  Number($('#comboProductId_'+rowId).val());
		var itemOrder = Number($('#itemOrder_'+rowId).val());
		var comboItemOrder = Number($('#comboItemOrder_'+rowId).val());
		var comboType = $('#comboType_'+rowId).val();
		if(specialProductId >= 0 && itemOrder >= 0 && comboItemOrder >= 0){
			$('#product_preview').html(''); 
			$('.splproductset').remove();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/delete_product_preview_session.action",
										async : false,
				dataType : "json",
				data : {
					productId : productId, specialProductId: specialProductId, itemOrder: itemOrder, comboType: comboType, comboItemOrder: comboItemOrder
				},
				cache : false,
				success : function(response) {
					if (response.returnMessage == "SUCCESS") {
						var tableScript = "";
						var count = 0;
						var previewObjects = response.previewObjects;
						 if(previewObjects.length > 0){
							 $(previewObjects)
								.each(
										function(index) {
											count = count + 1;
											tableScript += "<tr id='fieldrow_"+count+"' class='rowid masterTooltip' title='"+previewObjects[index].storeName+"' >"
													+ "<td id='lineId_"+count+"'>"
													+ count
													+ "</td>"
													+ "<td id='productname_"+count+"' class='productname'>"
													+ previewObjects[index].productName
													+ "</td>"
													+ "<td><input type='text' id='productqty_"
																	+ count
																	+ "' class='productqty' value="
																	+ previewObjects[index].quantity
																	+ " style='border: 0px;'></td>"
													+ "<td id='price_"+count+"' class='price' style='text-align: right;'>"
													+ previewObjects[index].displayPrice
													+ "</td>"
													+ "<td><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'>"
													+ "<img src='./images/cancel.png' width=10 height=25></img></span>"
													+ "<input type='hidden' id='productid_"+count+"' value="+previewObjects[index].productId+">"
													+ "<input type='hidden' id='unitprice_"+count+"' value="+previewObjects[index].finalUnitPrice+">"
													+ "<input type='hidden' id='orderPrinted_"+count+"' value="+previewObjects[index].orderPrinted+">"
													+ "<input type='hidden' id='comboProductId_"+count+"' value="+previewObjects[index].comboProductId+">"
													+ "<input type='hidden' id='itemOrder_"+count+"' value="+previewObjects[index].itemOrder+">"
													+ "<input type='hidden' id='comboType_"+count+"' value="+previewObjects[index].specialPos+">"
													+ "<input type='hidden' id='comboItemOrder_"+count+"' value="+previewObjects[index].comboItemOrder+">"
													+ "</td>" + "</tr>";
										});
							$('#product_preview').append(tableScript);
							 if($(".cart_table_div").height()>385)
								 $(".cart_table_div").css({"overflow-x":"hidden","overflow-y":"auto","height":"385px"}); 
							$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
								separator: '|', prompt: '', 
						    layout: ['7|8|9|' + $jquery.keypad.BACK, 
						        '4|5|6|' + $jquery.keypad.CLEAR, 
						        '1|2|3|' + $jquery.keypad.CLOSE, 
						        '.|0|00'],onClose: function(value, inst) { 
						        	$(this).trigger( "change" );}});
							calucaltePOSInvoice();	
							return false;
						if(Number(response.pointOfSaleTempId) > 0){ 
							$('#'+response.pointOfSaleTempId).remove(); 
							$('#pointOfSaleId').val('');
						}
							 
				 			
							if($(".tab>tr").size() > 0){
								var i = 1;
									$('.rowid')
										.each(
												function() { 
													var lineId = getRowId($(
															this)
															.attr(
																	'id'));
													$(
															'#lineId_'
																	+ lineId)
															.html(
																	i);
													i = i + 1;
												}); 
								calucaltePOSInvoice();
							}else{
								var tableScript="<tr>"
								+"<td colspan='5' style='font-size: 18px;font-weight:bold;color: #000!important;'>Cart is empty</td>"
								+"</tr>";
								$('#product_preview').append(tableScript);
								$('.invoice-total').html("0.0");
							}  
						 }else{
								var tableScript="<tr>"
									+"<td colspan='5' style='font-size: 18px;font-weight:bold;color: #000!important;'>Cart is empty</td>"
									+"</tr>";
									$('#product_preview').append(tableScript);
									$('.invoice-total').html("0.0");
						 }
					} else {
						return false;
					}
				}
			});
		}else{return false;} 
	} else{return false;}
	return false;
}
function deleteSavedOrder(posSessionId){
	$('#pointOfSaleId').val(posSessionId); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/delete_saved_pos_order.action",
		data:{pointOfSaleId: Number(posSessionId)},
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			posSessionId=null;
			$('#common-popup-new').dialog('destroy');		
			$('#common-popup-new').remove(); 
			$('#pos-common-popup').dialog('destroy');		
			$('#pos-common-popup').remove(); 
			$("#main-wrapper").html(result);
		}
	});
	 
}
function addProductPreview(previewObject){
	$('.ui-autocomplete-input').val('');
	$('#product_preview').html(''); 
	var itemOrder =  Number(0);
	var productId = Number(0); 
	if (typeof $('#itemOrder').val() == "undefined") {
		itemOrder = Number(0);
		productId = Number(0); 
	}else{
		itemOrder = Number($('#itemOrder').val());
		productId = Number($('#specialComboProductId').val());
	} 
	 
  		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/save_product_preview_session.action",
		async : false,
		dataType : "json",
		data : {
			 productPreview : JSON.stringify(previewObject), posSessionId: posSessionId, itemOrder: itemOrder, specialProductId: productId
		},
		cache : false,
		success : function(response) {
			var tableScript = "";
		var count = 0;
		var previewObjects = response.previewObjects;
			$(previewObjects)
				.each(
						function(index) {
							count = count + 1;
							tableScript += "<tr id='fieldrow_"+count+"' class='rowid masterTooltip' title='"+previewObjects[index].storeName+"'>"
									+ "<td id='lineId_"+count+"'>"
									+ count
									+ "</td>"
									+ "<td id='productname_"+count+"' class='productname'>"
									+ previewObjects[index].productName
									+ "</td>"
									+ "<td><input type='text' id='productqty_"
													+ count
													+ "' class='productqty' value="
													+ previewObjects[index].quantity
													+ " style='border: 0px;'></td>"
									+ "<td id='price_"+count+"' class='price' style='text-align: right;'>"
									+ previewObjects[index].displayPrice
									+ "</td>"
									+ "<td><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'>"
									+ "<img src='./images/cancel.png' width=10 height=25></img></span>"
									+ "<input type='hidden' id='productid_"+count+"' value="+previewObjects[index].productId+">"
									+ "<input type='hidden' id='unitprice_"+count+"' value="+previewObjects[index].finalUnitPrice+">"
									+ "<input type='hidden' id='orderPrinted_"+count+"' value="+previewObjects[index].orderPrinted+">"
									+ "<input type='hidden' id='comboProductId_"+count+"' value="+previewObjects[index].comboProductId+">"
									+ "<input type='hidden' id='itemOrder_"+count+"' value="+previewObjects[index].itemOrder+">"
									+ "<input type='hidden' id='comboType_"+count+"' value="+previewObjects[index].specialPos+">"
									+ "<input type='hidden' id='comboItemOrder_"+count+"' value="+previewObjects[index].comboItemOrder+">"
									+ "</td>" + "</tr>";
						});
			$('#product_preview').append(tableScript);
			 if($(".cart_table_div").height()>385)
				 $(".cart_table_div").css({"overflow-x":"hidden","overflow-y":"auto","height":"385px"}); 
			$jquery('.productqty').keypad({ keypadOnly: false,closeText: 'Done',
				separator: '|', prompt: '', 
		    layout: ['7|8|9|' + $jquery.keypad.BACK, 
		        '4|5|6|' + $jquery.keypad.CLEAR, 
		        '1|2|3|' + $jquery.keypad.CLOSE, 
		        '.|0|00'],onClose: function(value, inst) { 
		        	$(this).trigger( "change" );}});
			calucaltePOSInvoice();	
			return false;
		}
	});
	return false; 
}
function showShippingSite(customerId) {
	$('#shippingDetail').html('');
	$('#shippingDetail').append('<option value="">Select</option>');
	$
			.ajax({
				type : "POST",
				url : "<%=request.getContextPath()%>/show_customer_shipping_site.action",
				async : false,
				data : {
					customerId : customerId
				},
				dataType : "json",
				cache : false,
				success : function(response) {
					$(response.aaData)
							.each(
									function(index) {
										$('#shippingDetail')
												.append(
														'<option value='
							+ response.aaData[index].shippingId
							+ '>'
																+ response.aaData[index].name
																+ '</option>');
									});
				}
			});
	return false;
}

function commonCustomerPopup(customerId, customerName, combinationId,
		accountCode, creditTermId, commonParam) {
	$('#customerId').val(customerId);
	$('#customerName').val(customerName); 
	}  
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}

function showAllSavedOrder(){
	$('.ui-dialog-titlebar').remove();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_all_saved_pos_order.action",
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$(".pos-common-result").html(result);
					$('#pos-common-popup').dialog('open');
					$($($('#pos-common-popup').parent()).get(0)).css('top',
							0);
				}
			});
	return false;
}

function showAllDispatchOrder(){
	$('.ui-dialog-titlebar').remove();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_all_sales_dispatch_order.action",
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$(".common-result-new").html(result);
					$('#common-popup-new').dialog('open');
					$($($('#common-popup-new').parent()).get(0)).css('top',
							0);
				}
			});
	return false;
}

function selectedDispatchSales(salesDetails){
	var pointOfSaleId = salesDetails.pointOfSaleId;
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/dispatch_pointofsale_invoice.action",
		async : false,
		data:{pointOfSaleId: pointOfSaleId},
		dataType : "html",
		cache : false,
		success : function(result) { 
			$('#common-popup-new').dialog('destroy');		
			$('#common-popup-new').remove(); 
			$('#pos-common-popup').dialog('destroy');		
			$('#pos-common-popup').remove(); 
			$("#main-wrapper").html(result);
			return false;
		}
	});
	return false;
}

function showProductDefinitions(productId, productName){
	$("#filter-product-result").html(''); 
	$("#filter-product-result").hide(); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/filter_product_definition.action",
				async : false,
				data: {productId: productId},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$("#filter-result").html(result); 
					var previewObject = { 
							"productId" : productId,
							"productName" : productName,
							"comboType" : false,
							"comboProductId" : productId,
							"specialProduct" :true
					};
		 			addProductPreview(previewObject);  
				}
			});
	$("#filter-result").show();
	return false;
}

function showSpecialProductDefinitions(productId, productName){ 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/filter_specialproduct_definition.action",
				async : false,
				data: {productId: productId, itemOrder: Number(0)},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$("#temp-result").html(result); 
					$('.callJq').trigger('click');  
					var previewObject = { 
							"productId" : productId,
							"productName" : productName,
							"comboType" : false,
							"comboProductId" : productId,
							"specialProduct" :true
					};
		 			addProductPreview(previewObject);  
				}
			});
	$("#filter-result").show();
}

function showProductDefinitionFilter(productDefinitionId, productId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/filter_productsubcategory_definition_for_ordering_menu.action",
				async : false,
				data: {productDefinitionId: productDefinitionId, productId: productId},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$("#filter-product-result").html(result); 					
				}
			});
	$("#filter-product-result").show();
	/* if(topscrollSubCat == null) topscrollSubCat = $("#filter-product-result").offset().top;
	$('html, body').animate({
        scrollTop: topscrollSubCat
    }, 500); */
} 

function showSpecialProductDefinitionFilter(productDefinitionId, productId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/filter_productsubcategory_definition.action",
				async : false,
				data: {productDefinitionId: productDefinitionId, productId: productId},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$("#filter-splproduct-result").html(result);  
				}
			});
	$("#filter-splproduct-result").show();
} 

function showSpecialProductDefinitionFilterPopup(productDefinitionId, productId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/filter_productsub_definitionpopup.action",
				async : false,
				data: {productDefinitionId: productDefinitionId, productId: productId},
				dataType : "html",
				cache : false,
				success : function(result) { 
					$("#filter-splproduct-result").html(result);  
				}
			});
	$("#filter-splproduct-result").show();
} 

function personPopupResult(personId, personName, params){
	$('#employeeName').val(personName);
	$('#employeeId').val(personId);
	$('#common-popup-new').dialog('close');
	}

function checkModificationDeleteApprover(){
	var pointOfSaleId = Number($('#pointOfSaleId').val());
	var ordPrinted =false;
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/check_pos_printorder.action",
		data:{pointOfSaleId:pointOfSaleId},
		async : false,
		dataType : "json",
		cache : false,
		success : function(response) {
			ordPrinted=response.printerFlag;
		}
	});
	if(ordPrinted){
		 $('.ui-dialog-titlebar').remove(); 
         $('#common-popup-new').dialog('open');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/validate_approver.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('.common-result-new').html(result);
			}
		});
		return false;
	} else {
		return true;
	}

}
</script>
<div id="main-content" style="overflow: auto;">
	
	<div align="center" style="width: 75%; min-width: 265px; margin: 0 auto; margin-bottom: -85px;">
		<table id="headLogo" width="100%" style="">
			<tr valign="top">
				<td width="45%" style="text-align: right !important; padding-top: 35px;">
					<span style="color: gray; font-size: 14px; text-transform: uppercase;"><%-- ${THIS.companyName} --%></span>	
				</td>
				<td width="10%" style="text-align: center !important;">
					<img onclick="window.location.reload();" src="images/grandma's_cheesecake_indexlogo.png" height="150px" style="cursor: pointer;" title="${THIS.companyName}" />	
				</td>
				<td width="45%" style="text-align: right !important; padding-top: 17px;">
					<span style="color: gray; font-size: 10px;"><span style="color: #24B0F2 !important; font-size: 18px;">+971502328866</span>
					<br/>
					<span style="text-transform: uppercase !important; font-size: 8px;">Call now or visit</span> <a href="http://cheesecake.ae" target="_blank" title="${THIS.companyName}">cheesecake.ae</a>
					</span>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="bannerDiv" style="display: none;">
		<table width="100%" style="z-index: 99;">
			<tr valign="top">
				<td style="text-align: center !important;">
					<img onclick="window.location.reload();" src="images/grandma's_cheesecake_indexlogo.png" height="45px" style="cursor: pointer; margin-top: 3px; vertical-align: top;" title="${THIS.companyName}" />				
					<span style="color: gray; font-size: 10px; display: inline-block; margin-top: 11px;"><span style="color: #24B0F2 !important; font-size: 18px;">+971502328866</span>
						<br/>
						<span style="text-transform: uppercase !important; font-size: 8px;">Call now or visit</span> <a href="http://cheesecake.ae" target="_blank" title="${THIS.companyName}">cheesecake.ae</a>
					</span>
				</td>
			</tr>
		</table>
	</div>
	
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<span class="heading_title">Explore Our Products</span>
		<div class="mainhead portlet-header ui-widget-header"
			style="padding: 8px 4px 4px 8px !important; width: auto; display: none;">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Point Of Sale :: <i>${requestScope.SALES_SOTRE_NAME}</i>  
		</div>
		<div class="clearfix"></div> 
		<div class="tempresult" style="display: none;"></div>
		<form name="pos_form_validation" id="pos_form_validation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width50"
					style="display: none;"></div>
				<input type="hidden" id="securedSales" value="${POINT_OF_SALE.orderPrinted}"/>	
				<input type="hidden" id="pointOfSaleId"
					value="${POINT_OF_SALE.pointOfSaleId}" /> <input type="hidden"
					id="barCode" name="barCode" />
				<div class="width100 float-left" id="hrm">
					<div class="width40 float-left" style="display: none;"> <!-- NONE ADDED -->
						<fieldset style="height: 500px;">
							<div id="hrm" class="hastable width100"> 
								<c:choose>
									<c:when test="${sessionScope.USER.username eq 'deerfeild_14' || sessionScope.USER.username eq 'marasy_14'
										|| sessionScope.USER.username eq 'raha_6' ||  sessionScope.USER.username eq 'spar_6' ||  sessionScope.USER.username eq 'alwahda_6'
										|| sessionScope.USER.username eq 'mushrif_6' || sessionScope.USER.username eq 'gcc-ordering_6'}">
										<div>
											<label class="width30" for="salesDate"> Sales Date<span
												style="color: red;">*</span> </label>  
											<input type="text" id="salesDate" name="salesDate" value="${POINT_OF_SALE.posSalesDate}"
												class="salesDate width58" />
		 								</div>
	 								</c:when>
	 								<c:otherwise>
	 									<div style="display:none;">
											<label class="width30" for="salesDate"> Sales Date<span
												style="color: red;">*</span> </label>  
											<input type="text" id="salesDate" name="salesDate" value="${POINT_OF_SALE.posSalesDate}"
												class="salesDate width58" />
		 								</div>
	 								</c:otherwise>
 								</c:choose>
								<div class="customerserve">
									<label class="width30" for="customerName">Customer<span
												style="color: red;">*</span></label> <input
										type="text" readonly="readonly" name="customerName"
										id="customerName" value="${POINT_OF_SALE.customerName}"
										style="width: 56%;" /> <input type="hidden" id="customerId"
										value="${POINT_OF_SALE.customerId}" name="customerId" />
										<span
										class="button" style="position: relative; left: 5px;">
										<a style="cursor: pointer;" id="customer-popup"
										class="btn ui-state-default ui-corner-all customer-common-popup-new width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div> 
								<div>
									<label class="width30">Delivery Option</label>
									<div>
										<select id="deliveryOption" name="deliveryOption"
											class="width58">
											<option value="">Select</option>
											<c:forEach items="${DELIVERY_OPTION}" var="nltls">
												<c:choose>
													<c:when
														test="${POINT_OF_SALE.deliveryOption eq nltls.lookupDetailId}">
														<option value="${nltls.lookupDetailId}"
															selected="selected">${nltls.displayName}</option>
													</c:when>
													<c:otherwise>
														<option value="${nltls.lookupDetailId}">${nltls.displayName}
														</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select> <span class="button" id="DELIVERY_OPTION_1"
											style="position: relative; display: none;"> <a
											style="cursor: pointer;" id="DELIVERY_OPTION"
											class="btn ui-state-default ui-corner-all recruitment-lookup width8">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="deliveryOptionTemp"
											id="deliveryOptionTemp" />
									</div>
								</div>
								<div style="display: none;">
									<label class="width30" for="shippingDetail"> Shipping
										Address</label> <select name="shippingDetail" id="shippingDetail"
										class="width58">
										<option value="">Select</option>
									</select> <input type="hidden" name="shippingIdTemp" id="shippingIdTemp" value="0" />
								</div>
								<div class="cart_table_div"
									style="max-height: 385px; overflow-x: hidden; overflow-y: auto; height: 385px">
									<table class="width100 cart_table"
										style="margin-top: 5px !important;" title="Cart">
										<thead>
											<tr>
												<th style="width: 0.05%">S.NO</th>
												<th style="width: 7%">Product</th>
												<th style="width: 1%">Quantity</th>
												<th style="width: 3%">Price</th>
												<th style="width: 0.01%;">Del</th>
											</tr>
										</thead>
										<tbody class="tab" id="product_preview">
											<c:set var="totalPos" value="${0.00}" />
											<c:choose>
												<c:when
													test="${PRODUCT_PREVIEW ne null && PRODUCT_PREVIEW ne ''}">
													<c:forEach items="${PRODUCT_PREVIEW}" var="product"
														varStatus="status">
														<tr id="fieldrow_${status.index+1}" class="rowid">
															<td id="lineId_${status.index+1}">${status.index+1}</td>
															<td id="productname_${status.index+1}"
																class="productname">${product.productName}</td>
															<td><input type="text"
																id="productqty_${status.index+1}" class="productqty"
																value="${product.quantity}" style="border: 0px;">
															</td>
															<td id="price_${status.index+1}" class="price"
																style="text-align: right;">${product.displayPrice}</td>
															<td><span class="deleteoption"
																id="deleteoption_${status.index+1}"
																style="cursor: pointer;"> <img
																	src='./images/cancel.png' width=10 height=25></img> </span> <input
																type='hidden' id='productid_${status.index+1}'
																value="${product.productId}"> <input
																type='hidden' id='unitprice_${status.index+1}'
																value="${product.finalUnitPrice}"> <input
																type='hidden' id='orderPrinted_${status.index+1}'
																value="${product.orderPrinted}">
																<input
																type='hidden' id='comboProductId_${status.index+1}'
																value="${product.comboProductId}">
																<input
																type='hidden' id='itemOrder_${status.index+1}'
																value="${product.itemOrder}"> 
																<input
																type='hidden' id='comboItemOrder_${status.index+1}'
																value="${product.comboItemOrder}"> 
																<input
																type='hidden' id='comboType_${status.index+1}'
																value="${product.specialPos}"> 
																 <c:set
																	var="totalPos"
																	value="${totalPos +  (product.finalUnitPrice * product.quantity)}" />
															</td>
														</tr>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<tr>
														<td colspan="5"
															style="font-size: 18px; color: #000; font-weight: bold;">Cart
															is empty</td>
													</tr>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</div>
							</div>
						</fieldset>
						<div class="float-right invoice-total-div">
							<span class="invoice-total">${totalPos}</span>
						</div>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
							<div
								class="portlet-header ui-widget-header float-right pos-discard"
								id="pos-discard"
								style="background: #DF0101; color: #fff; padding: 10px;">
								Refresh</div>
							<div
								class="portlet-header ui-widget-header float-right pos-order"
								id="pos-order"
								style="background: #0B610B; color: #fff; padding: 10px;">Order</div> 
						</div>
					</div> 
					<div class="width100 float-left" style="margin-top: -5px;" align="center">   
						<div class="float-right" style="display: none;"> <!-- NONE ADDED -->
							<div class="float-right width50">
								<span>Product</span>
								 <select
									name="productSearch" id="productSearch-1" class="product-search-1">
									<option value="">Search</option>
									<c:forEach var="productDetails" items="${PRODUCT_DETAILS}">
										<option value="${productDetails.productId}">${productDetails.productName}</option>
									</c:forEach>
								</select>
								<span class="float-right icon-search-class" style="right: 4%;"><img
									width="24" height="24" src="images/icons/Glass.png"> </span>
							</div> 
						</div> 
						<fieldset
							style="height: auto; text-align: center;">
							<!-- <legend>
								<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Category
							</legend> -->
							<c:choose>
								<c:when test="${PRODUCT_CATEGORIES ne null && fn:length(PRODUCT_CATEGORIES)>0}">
									<div class="width100 content-item-table" 
										style="" align="center">
										<div id="maincategory-table" class="pos-item-table width100" align="center"> 
											<c:set var="categoryRow" value="0" />
											<c:forEach items="${PRODUCT_CATEGORIES}" var="cat">
												<!-- <tr> -->
													<!-- TD 1 -->
													<!-- <td> -->
														<div id="mainCatBoxModel_${cat.productCategoryId}" class="box-model mainCategory" style="background: #9FF781;">															
															<span class="mainCategory">${cat.categoryName}
																<input type="hidden"
																id="mainCategoryId_${cat.productCategoryId}"
																class="mainCategoryId" /> 
																<input type="hidden" class="specialProduct" value="${cat.specialProduct}"
																		id="specialProduct_${cat.productCategoryId}"/>
															</span>
															<img class="product_image" src="images/ordering-menu/company/${THIS.companyKey}/test.jpg" />
															<%-- <img class="product_image" src="images/ordering-menu/company/${THIS.companyKey}/categories/cat_${sub_cat.productCategoryId}.png" /> --%>
														</div>
													<!-- </td> -->
													<!-- TD 2 -->
													<%-- <c:if test="${PRODUCT_CATEGORIES[categoryRow+1]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+1].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+1].productCategoryId}"
																	class="mainCategoryId" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+1].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+1].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<!-- TD 3 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+2]!= null}">
														<td>
															<div class="box-model" style="background: #9FF781;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+2].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+2].productCategoryId}"
																	class="mainCategoryId" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+2].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+2].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<!-- TD 4 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+3]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+3].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+3].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+3].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+3].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<!-- TD 5 -->
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+4]!= null}">
														<td>
															<div class="box-model" style="background: #9FF781;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+4].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+4].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+4].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+4].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+5]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+5].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+5].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+5].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+5].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+6]!= null}">
														<td>
															<div class="box-model" style="background: #9FF781;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+6].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+6].productCategoryId}"
																	class="mainCategory" /> 
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+6].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+6].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if>
													<c:if test="${PRODUCT_CATEGORIES[categoryRow+7]!= null}">
														<td>
															<div class="box-model" style="background: #81F7D8;">
																<span class="mainCategory">${PRODUCT_CATEGORIES[categoryRow+7].categoryName}
																	<input type="hidden"
																	id="mainCategoryId_${PRODUCT_CATEGORIES[categoryRow+7].productCategoryId}"
																	class="mainCategory" />
																	<input type="hidden" class="specialProduct" value="${PRODUCT_CATEGORIES[categoryRow+7].specialProduct}"
																		id="specialProduct_${PRODUCT_CATEGORIES[categoryRow+7].productCategoryId}"/>
																</span>
															</div>
														</td>
													</c:if> 
													<c:set var="categoryRow" value="${categoryRow + 8}" /> --%>
												<!-- </tr> -->
											</c:forEach>
										</div> 
									</div>
								</c:when>
								<c:otherwise>
									<div style="font-size: 18px; color: #000; font-weight: bold;">No Categories found.</div>
								</c:otherwise>
							</c:choose> 
						</fieldset>
						
						<div id="filter-result" style="display: none;"> 
						</div> 
						<div style="margin-top: 5px; display:none;" id="filter-product-result" class="product-listing"> 
						</div>
					</div>
				</div>

			</div>
			<div class="clearfix"></div> 
					
			<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<span class="callJq"></span>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup-new" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result-new width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>

			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="pos-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto; display: none;">
					<div class="pos-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
		
	</div>
	<div id="footInfo" style="font-family: inherit; color: gray;">
		<!-- <div id="fade"> -->
			<table width="75%" style="margin: 0 auto !important;">
				<tr>
					<!-- <td width="33%" style="padding: 15px; padding-left: 35px; font-family: wf_segoe-ui_normal,'Segoe UI',Tahoma,Helvetica,sans-serif;">
						
					</td> -->
					<td width="100%" style="padding: 20px; font-size: 10px; text-transform: uppercase; text-align: center;" align="center">
						${THIS.companyName}	&copy; All Rights Reserved 2015
					</td>
					<!-- <td width="33%" style="padding: 15px;">
						
					</td> -->
				</tr>
			</table>
		<!-- </div> -->
	</div>
</div>
<style>
	
/* #footInfo {
	background: url(images/ordering-menu/company/gcc/menu_background.png);
	display: block;
	width: 100%;
	height: 250px;
	margin-top: 20px;
	box-shadow: 0 0 4px 0 #ccc;
}

#fade {
	width: 100%;
	height: 100%;
	display: block;
	background: #fff;
} */
</style>
<div id="stock-result" style="display: none;"></div>