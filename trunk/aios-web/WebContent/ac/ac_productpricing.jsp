<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var productPricingId=0; 
var productId = 0;
var productPricingDetailId = 0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
	$('#DOMWindow').remove();
	$('#DOMWindowOverlay').remove();
	$('#pricing-common-popup').dialog('destroy');		
	$('#pricing-common-popup').remove(); 
	$('#product-pricing-lookup').dialog('destroy');		
	$('#product-pricing-lookup').remove();  
	$('#customer_multiselect_popup').dialog('destroy');	
	$('#customer_multiselect_popup').remove();  
	 oTable = $('#ProductPricingDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 25,
		"sScrollY": $("#main-content").height() - 195,
		"sAjaxSource" : "product_pricing_list.action",

		"aoColumns" : [ {
    	    "mDataProp" : "productCode"
   		 },{
			"mDataProp" : "productName"
		 }
   		<c:if test="${sessionScope.unified_price eq 'false'}">
   		 ,{
			"mDataProp" : "storeName"
		 }</c:if>
   		 , {
			"mDataProp" : "costPrice"
		 }, {
			"mDataProp" : "salePrice"
		 }, {
			"mDataProp" : "pricingTitle"
		 } ], 
	});	 
		
	$('#add').click(function(){   
		productPricingId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/product_pricing_entry.action", 
	     	async: false, 
	     	data:{productPricingId: productPricingId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/product_pricing_entry.action", 
	     	async: false, 
	     	data:{productPricingId: productPricingId, productId: productId, productPricingDetailId: productPricingDetailId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(productPricingDetailId != null && productPricingDetailId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/product_pricing_detail_delete.action", 
	     	async: false,
	     	data:{productPricingDetailId: productPricingDetailId},
			dataType: "json",
			cache: false,
			success: function(response){ 
				 if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_product_pricing.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	 
	/* Click event handler */
	$('#ProductPricingDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  productPricingId = aData.productPricingId;
			  productId = aData.productId;
			  productPricingDetailId = aData.productPricingDetailId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          productPricingId = aData.productPricingId;
	          productId = aData.productId;
	          productPricingDetailId = aData.productPricingDetailId;
	      }
  	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s">
			</span>Product Pricing</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="sales_delivery_list">
					<table id="ProductPricingDT" class="display">
						<thead>
							<tr> 
								<th>Code</th> 
								<th>Product</th> 
								<c:if test="${sessionScope.unified_price eq 'false'}">
									<th>Store</th> 
								</c:if>
								<th>Cost Price</th>  
								<th>Selling Price</th> 
								<th>Pricing Title</th> 
							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class=" ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
 				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>