<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
.waiting_for_approval{
   display: block;
    height: 20px;
    width: 20px; 
    background: url(images/waiting.png) no-repeat;
    background-position: center;
    float: right; 
} 
</style>
<script type="text/javascript"> 
var journalId=0; 
var tempJournalId = 0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
 	$('#codecombination-popup').dialog('destroy');		
	$('#codecombination-popup').remove(); 
	$('.codecombination-popup').remove(); 
	$('#TransactionList').dataTable({ 
		"sAjaxSource": "journal_voucher_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "JOURNAL_ID", "bVisible": false}, 
			{ "sTitle": "TEMP_ID", "bVisible": false}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.vouchernumber"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.jvdate"/>'}, 
			{ "sTitle": 'Debit'},
			{ "sTitle": 'Credit'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.categoryname"/>'},
 			{ "sTitle": '<fmt:message key="accounts.jv.label.entrydate"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.jv.label.description"/>'},
		], 
		"sScrollY": $("#main-content").height() - 260,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#delete_ledger').click(function(){   
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_duplicate_ledgers.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.sqlReturnMessage);
			} 		
		}); 
	}); 
	
	$('#add').click(function(){  
		var showPage="add";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/journalvoucher_addentry.action", 
	     	async: false,
	     	data:{showPage:showPage, journalId: 0, tempJournalId: 0, recordId: 0, alertId: 0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	}); 

	$('#edit').click(function(){   
		var showPage="edit";  
		if((journalId!=null && journalId!="") || (tempJournalId!=null && tempJournalId!="")){   
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/journalvoucher_editentry.action", 
	     	async: false,
	     	data:{journalId: journalId, tempJournalId: tempJournalId, showPage: showPage, recordId:0, alertId: 0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
				$.scrollTo(0,300);
			} 		
		}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	}); 

	$('#resetLedger').click(function(){      
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/journalvoucher_resetledger.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				if(($.trim(response.sqlReturnMessage)=="SUCCESS")) {
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/journal_voucher_retrive.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html(response.sqlReturnMessage).slideDown(1000);  
						} 		
					}); 
				} else{
					$('#error_message').hide().html(response.sqlReturnMessage).slideDown(1000);
					return false;
				}  		
			} 		
		}); 
	}); 
	
	$('#calendarResetLedger').click(function(){    
		var calendarId = Number($('#calendarId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/journalvoucher_calendarresetledger.action", 
	     	async: false, 
	     	data:{calendarId: calendarId},
			dataType: "json",
			cache: false,
			success: function(response){ 
				if(($.trim(response.sqlReturnMessage)=="SUCCESS")) {
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/journal_voucher_retrive.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html(response.sqlReturnMessage).slideDown(1000);  
						} 		
					}); 
				} else{
					$('#error_message').hide().html(response.sqlReturnMessage).slideDown(1000);
					return false;
				}  		
			} 		
		}); 
	}); 
	
	$('#bigCalendarResetLedger').click(function(){    
		var calendarId = Number($('#calendarId').val());
		var transactionId = Number($('#transactionId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/journalvoucher_bigcalendarresetledger.action", 
	     	async: false, 
	     	data:{calendarId: calendarId, transactionId: transactionId},
			dataType: "json",
			cache: false,
			success: function(response){ 
				$('#transactionId').val(response.transactionId);
				alert(response.transactionId);
			} 		
		}); 
	}); 

	$('#delete').click(function(){
		if((journalId!=null && journalId!="") || (tempJournalId!=null && tempJournalId!="")){   
		var cnfrm = confirm('Selected Details will be deleted permanently');
		if(!cnfrm)
			return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/journalvoucher_delete.action", 
	     	async: false,
	     	data:{journalId:journalId, tempJournalId: tempJournalId, alertId: 0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);   
				var message=$.trim($('.tempresult').html().toLowerCase()); 
				if(message!=null){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/journal_voucher_retrive.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Transaction send for approval for delete. ").slideDown(1000);  
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$('#view').click(function(){
		if((journalId!=null && journalId!="") || (tempJournalId!=null && tempJournalId!="")){   
			window.open('show_journal_report.action?journalId='+ journalId + '&recordId='+ tempJournalId + '&format=HTML', '',
			'width=1000,left=100px');
		 }else{
			alert("Please select a record to view.");
			return false;
		}
	});

	//init datatable
	oTable = $('#TransactionList').dataTable();
	 
	/* Click event handler */
	$('#TransactionList tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          journalId=aData[0];   
	          tempJournalId=aData[1];
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          journalId=aData[0];   
	          tempJournalId=aData[1];
	      }
	});
	
	$('#print').click(function(){
		window.open("show_transaction_report.action?journalId="+ journalId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		return false;
	});  
	
	$('#loadJVFromXL').click(function(){  
		var showPage=$("#xlFileName").val();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/export_date_journalvoucher.action", 
	     	async: false,
	     	data:{showPage:showPage},
			dataType: "json",
			cache: false,
			success: function(object){ 
				if(object.sqlReturnMessage=='SUCCESS'){
					alert("Successfully Done");
					return false;
				}else{
					alert("Error...");
					return false;
				}
			} 		
		}); 
		return false;
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.journal.label.journalvoucher"/>
		</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    <input name="calendarId" id="calendarId" type="hidden"/>
	   	    <input name="transactionId" id="transactionId" type="hidden"/> 
	   	    <div id="rightclickarea">
			 	<div id="journal_accounts">
						<table class="display" id="TransactionList"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>View</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" > 
				<div class="portlet-header ui-widget-header float-left" id="loadJVFromXL">Load JV</div>
				<input type="text" id="xlFileName" class="width60">
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="bigCalendarResetLedger" style="display:none;">Big Calendar Reset Ledger</div>
				<div class="portlet-header ui-widget-header float-right" id="calendarResetLedger" style="display:none;">Calendar Reset Ledger</div>
				<div class="portlet-header ui-widget-header float-right" id="resetLedger" style="display:none;">Reset Ledger</div>
				<div class="portlet-header ui-widget-header float-right" id="delete_ledger" style="display:none;">Delete Ledger</div>
				<div class="portlet-header ui-widget-header float-right" id="view"><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="print"><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>