<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript"> 
var storeId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var personId = 0;
$(function(){
	$('.formError').remove(); 
	callStoreDataTable(); 
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  storeId = aData.storeId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          storeId = aData.storeId;
	      }
  	});

	$('.storeName').combobox({ 
       selected: function(event, ui){   
          storeId = Number($(this).val()); 
          callStoreDataTable(); 
       }
	}); 

	$('.personName').combobox({ 
	       selected: function(event, ui){   
	          personId = Number($(this).val()); 
	          callStoreDataTable(); 
	       }
		}); 

	$('.print-call').click(function(){
		
		if(storeId!=null && storeId!="" ){
			window.open('show_html_store_detail_report.action?recordId='+ storeId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			 $('#error_message').hide().html("Please select a record.").slideDown();
			 $('#error_message').delay(3000).slideUp();
		}
		return false;
	}); 

	$(".xls-download-call").click(function(){ 
		if(storeId!=null && storeId!="" ){
			window.open('<%=request.getContextPath()%>/show_store_detail_excel_report.action?recordId='+storeId,
					'_blank','width=0,height=0'); 
		}else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	}); 

	$('.pdf-download-call').click(function(){
		if(storeId!=null && storeId!="" && storeId > 0){
			window.open('show_storedetail_jasperreport.action?recordId='+ storeId + '&format=PDF', '_blank',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	});
});
function callStoreDataTable(){  
	oTable = $('#example').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "store_report_list.action?storeId="+storeId+"&personId="+personId, 
		"aoColumns" : [ { 
			"mDataProp" : "storeNumber"
		 }, {
			"mDataProp" : "storeName"
		 }, {
			"mDataProp" : "personNumber"
		 }, {
			"mDataProp" : "personName"
		 }, {
			"mDataProp" : "storageType"
		 }, {
			"mDataProp" : "storageSection"
		 },{
			"mDataProp" : "binType"
		 },{
			"mDataProp" : "address" 
		 } ], 
		 "fnRowCallback" : function(nRow, aData,
					iDisplayIndex, iDisplayIndexFull) {   
			$('.storeName')
			.append('<option value='
					+ aData.storeId
					+ '>' 
					+ aData.storeName
					+ '</option>'); 
			$('.personName')
				.append('<option value='
					+ aData.personId
					+ '>' 
					+ aData.personName
					+ '</option>'); 
	 		},"fnInitComplete": function(oSettings, json) {
	 		     callBackDataTable();
	 	    }  
	}); 
}
function  callBackDataTable(){
	$('.personName option').each(function() { 
		  $(this).prevAll('option[value="' + this.value + '"]').remove();
	}); 
	$('.storeName option').each(function() { 
		  $(this).prevAll('option[value="' + this.value + '"]').remove();
	}); 
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Store Report
		</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<div class="width50 float-left" id="hrm" style="padding: 3px;">
				<div>
		   	    	<label class="width30">Store Name</label>
		   	    	<select class="storeName">
		   	    		<option>Select</option> 
		   	    	</select>
	   	    	</div>
	   	    	<div>
		   	    	<label class="width30">Person Name</label>
		   	    	<select class="personName">
		   	    		<option>Select</option> 
		   	    	</select>
		   	    </div>
			</div> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    <div id="rightclickarea">
			 	<div id="store_list">
					<table id="example" class="display">
						<thead>
							<tr>
								<th>Store Reference</th>
								<th>Store Name</th>
								<th>Person Number</th>
								<th>Person Name</th>
								<th>Storage Type</th>
								<th>Storage Section</th>
								<th>Bin Type</th>
								<th>Address</th> 
							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Print</span></div> 
			</div>  
		</div> 
	</div> 
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 