 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var creditTermId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
	if(typeof($('#codecombination-popup')!="undefined")){  
		 $('#codecombination-popup').dialog('destroy');		
		 $('#codecombination-popup').remove(); 
	 }
	$('#CreditTermDT').dataTable({ 
		"sAjaxSource": "getcreditterm_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Credit Term", "bVisible": false},
			{ "sTitle": "<fmt:message key="accounts.creditterm.credittermname"/>"},
			{ "sTitle": "Due Date"}, 
			{ "sTitle": "Due Days"}, 
			{ "sTitle": "<fmt:message key="accounts.creditterm.credittermdiscountmode"/>"}, 
			{ "sTitle": "<fmt:message key="accounts.creditterm.credittermdiscount"/>"}, 
			{ "sTitle": "<fmt:message key="accounts.creditterm.credittermpenaltytype"/>"}, 
			{ "sTitle": "<fmt:message key="accounts.creditterm.credittermpenaltymode"/>"}, 
			{ "sTitle": "<fmt:message key="accounts.creditterm.credittermpenalty"/>"}, 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){   
		creditTermId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/creditterm_addentry.action", 
	     	async: false, 
	     	data:{creditTermId: creditTermId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
		if(creditTermId != null && creditTermId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/creditterm_addentry.action", 
		     	async: false, 
		     	data:{creditTermId: creditTermId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(creditTermId != null && creditTermId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/creditterm_deletentry.action", 
	     	async: false,
	     	data:{creditTermId: creditTermId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
				var message=$.trim($('.tempresult').html().toLowerCase()); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_all_creditterms.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(3000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					$('#error_message').delay(3000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});  
	
	//init datatable
	oTable = $('#CreditTermDT').dataTable();
	 
	/* Click event handler */
	$('#CreditTermDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this );
	          creditTermId = aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this ); 
	          creditTermId = aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>credit term</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="credit_term_list">
					<table class="display" id="CreditTermDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>