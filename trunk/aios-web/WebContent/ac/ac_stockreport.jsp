<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
.ui-menu-item{width:66%!important;}
.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:40%!important;}
.ui-button { margin: 0px; right: 135px; top: 4px; height:20px;}  
</style>
<script type="text/javascript"> 
var storeId = 0; var stockId = 0; var productId = 0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
var minimumQuantity = 0; var maximumQuantity = 0;
$(function(){
	$('.formError').remove(); 
	callStockDataTable(); 
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  stockId = aData.stockId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          stockId = aData.stockId;
	      }
  	});

	$('.storeName').combobox({ 
       selected: function(event, ui){   
          storeId = Number($(this).val());  
          callStockDataTable();  
       }
	}); 

	$('.productName').combobox({ 
       selected: function(event, ui){   
          productId = Number($(this).val());  
          callStockDataTable(); 
       }
	});

	$('#minimumQuantity').change(function(){
		minimumQuantity = Number($(this).val());
		$('#maximumQuantity').val('');
		maximumQuantity = 0;
		callStockDataTable(); 
	});

	$('#maximumQuantity').change(function(){
		maximumQuantity = Number($(this).val());
		$('#minimumQuantity').val('');
		minimumQuantity = 0;
		callStockDataTable(); 
	});

	$('.print-call').click(function(){
		
		if(stockId!=null && stockId!="" ){
			window.open('show_html_stock_detail_report.action?stockId='+ stockId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			 window.open('show_html_stock_report.action?format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');
		}
		return false;
	}); 

	$(".xls-download-call").click(function(){ 
		if(storeId!=null && storeId!="" ){
			window.open('<%=request.getContextPath()%>/show_stock_detail_excel_report.action?recordId='+stockId,
					'_blank','width=0,height=0'); 
		}else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	}); 

	$('.pdf-download-call').click(function(){
		if(storeId!=null && storeId!="" && storeId > 0){
			window.open('show_stockdetail_jasperreport.action?recordId='+ stockId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	});
});
function callStockDataTable(){   
	oTable = $('#example').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_stock_report_list.action?storeId="+storeId+"&productId="
						+productId+"&minimumQuantity="+minimumQuantity+"&maximumQuantity="+maximumQuantity, 
		"aoColumns" : [ { 
			"mDataProp" : "storeReference"
		 }, {
			"mDataProp" : "storeName"
		 }, {
			"mDataProp" : "productName"
		 }, {
			"mDataProp" : "productUnit"
		 }, {
			"mDataProp" : "costingType"
		 },{
			"mDataProp" : "availableQuantity"
		 },{
			"mDataProp" : "totalReceivedQuantity" 
		 }, {
			"mDataProp" : "totalIssuedQuantity" 
		 } ], 
		 "fnRowCallback" : function(nRow, aData,
					iDisplayIndex, iDisplayIndexFull) { 
			$('.storeName')
			.append('<option value='
					+ aData.storeId
					+ '>' 
					+ aData.storeName
					+ '</option>'); 
			$('.productName')
				.append('<option value='
					+ aData.productId
					+ '>' 
					+ aData.productName
					+ '</option>'); 
	 		},"fnInitComplete": function(oSettings, json) {
	 		     callBackDataTable();
	 	    }  
	});	 
}
function  callBackDataTable(){
	$('.storeName option').each(function() { 
		  $(this).prevAll('option[value="' + this.value + '"]').remove();
	}); 
	$('.productName option').each(function() { 
		  $(this).prevAll('option[value="' + this.value + '"]').remove();
	}); 
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			store information
		</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<div class="width48 float-right" id="hrm" style="padding: 3px;">
				<div>
		   	    	<label class="width30">Quantity &lt;=</label>
		   	    	<input type="text" id="minimumQuantity"/>
	   	    	</div>
	   	    	<div>
		   	    	<label class="width30">Quantity &gt;=</label>
		   	    	<input type="text" id="maximumQuantity"/>
	   	    	</div>
			</div>
			<div class="width50 float-left" id="hrm" style="padding: 3px;">
				<div>
		   	    	<label class="width30">Store Name</label>
		   	    	<select class="storeName">
		   	    		<option>Select</option> 
		   	    	</select>
	   	    	</div>
	   	    	<div>
		   	    	<label class="width30">Product Name</label>
		   	    	<select class="productName">
		   	    		<option>Select</option> 
		   	    	</select>
		   	    </div>
			</div> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    <div id="rightclickarea">
			 	<div id="store_list">
					<table id="example" class="display">
						<thead>
							<tr> 
								<th>Store Ref</th>
								<th>Store Name</th>
								<th>Product Name</th>
								<th>Unit</th>
								<th>Costing Type</th>
								<th>Available Quantity</th> 
								<th>Total Receivables</th>
								<th>Total Issuance</th>
							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Print</span></div> 
			</div>  
		</div> 
	</div> 
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 