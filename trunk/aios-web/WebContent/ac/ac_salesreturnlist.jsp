<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script> 
<script type="text/javascript"> 
var creditId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var paymentFlag=null;
$(function(){
	$('.formError').remove(); 
	if(typeof($('#common-popup')!="undefined")){  
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();   
	 }
	 oTable = $('#example').dataTable({ 
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "sales_return_list_json.action",

			"aoColumns" : [ { 
				"mDataProp" : "creditNumber"
			 }, {
				"mDataProp" : "creditDate"
			 }, {
				"mDataProp" : "customerReference"
			 }, {
				"mDataProp" : "customerName"
			 }, {
				"mDataProp" : "salesReference"
			 }, {
				"mDataProp" : "processType"
			 }, {
				"mDataProp" : "description"
			 }], 
		});	 
	 oTable.fnSort( [ [0,'desc'] ] );	
	$('#print').click(function(){ 
		if(creditId != null && creditId != 0){ 
 			window.open("<%=request.getContextPath()%>/sales_return_printout.action?recordId="+creditId
 					+"&format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
			return false;
				
		}else{
			alert("Please select a record.");
			return false;
		}
	}); 

	
	
	$('#add').click(function(){ 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_return_addentry.action", 
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
				} 		
			}); 
		return false
	}); 

	$('#delete').click(function(){   
		if(creditId != null && creditId != 0){ 
			
			if(paymentFlag){
				$('#error_message').hide().html("Can not delete!!! Payment has been processed already").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_sales_return.action", 
		     	async: false,
		     	data:{creditId: creditId},
				dataType: "json",
				cache: false,
				success: function(response){ 
 					 if(response.returnMessage == "SUCCESS"){
 						 $.ajax({
 							type:"POST",
 							url:"<%=request.getContextPath()%>/sales_return_list.action",
 							async : false,
 							dataType : "html",
 							cache : false,
 							success : function(result) {
 								$("#main-wrapper").html(result); 
 								if (message != null && message != '') {
 									$('#success_message').hide().html("Successfully Deleted").slideDown(1000);
 									$('#success_message').delay(2000).slideUp();
 								}
 							}
 						});
					 }
					 else{
						 $('#error_message').hide().html(response.returnMessage).slideDown(1000);
						 $('#error_message').delay(3000).slideUp();
						 return false;
					 }
				}
			 });
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});

	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		 $("#delete").css('opacity','1');
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  creditId = aData.creditId;
			  paymentFlag= aData.paymentFlag;
			  if(paymentFlag==true){
	        	  $("#delete").css('opacity','0.5');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          creditId = aData.creditId;
	          paymentFlag= aData.paymentFlag;
			  if(paymentFlag==true){
	        	  $("#delete").css('opacity','0.5');
	          }
	      }
  	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sale Returns</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">Success</div>
			</c:if>
			<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">Failure</div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
	   	    <div id="rightclickarea">
			 	<div id="credit_note_list">
					<table id="example" class="display">
						<thead>
							<tr>
								<th>Reference No</th>
								<th>Date</th>
								<th>Customer Ref</th>
								<th>Customer Name</th>
								<th>Sales Reference</th>
								<th>Process Type</th>
								<th>Description</th> 
							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu"> 
				<div class="first_li"><span>Add</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
				<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>
			</div>
		</div>
	</div>
</div>