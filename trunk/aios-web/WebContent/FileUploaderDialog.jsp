
<div>

<form id="file_upload" action="uploadFile.action" method="POST" enctype="multipart/form-data">
	
    
    <div id="drop_zone_1"  style = "width: 120px;">
        <input id="file_1" type="file" name="fileToUpload" multiple/>
        <div>Upload/Drop files</div>
    </div>
    <button>Upload</button>      
    <br/>
    
   <table id="files_1" cellpadding="5" cellspacing="5" width = "100%"  >
    	<TR style = "border:1px solid black;background-color: #EEEEEE;" height="30" class = "tableHeader">
    		<TD width = "25%" >
    			File Name&nbsp;&nbsp;&nbsp;&nbsp;
    		</TD>
    		<TD width = "50%">
    			Progress
    		</TD>
    		<TD width = "20%" >
    			size (KB)
    		</TD>
    		<TD width = "5%" >
    			Action(s)
    		</TD>
    	</TR>
   </table>

</form>

<script>
/*global $ */
$(function () {
    var initFileUpload = function (suffix) {
        $('#file_upload').fileUploadUI({
            namespace: 'file_upload_' + suffix,
            fileInputFilter: '#file_' + suffix,
            dropZone: $('#drop_zone_' + suffix),
            uploadTable: $('#files_' + suffix),
            downloadTable: $('#files_' + suffix),
            buildUploadRow: function (files, index) {
        		
                return $('<tr><td width = '+files[index].name.length+'>' + files[index].name + '<\/td>' +
                        '<td class="file_upload_progress"><div></div><\/td>' +
                        '<td class="file_upload_cancel">' +
                        '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                        '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                        '<\/button><\/td>'+
                        '<td>&nbsp;</TD>'+
                        '<\/tr>');
				
            },
            buildDownloadRow: function (file) {                               
            	var randomNumber = Math.floor(Math.random()*11);
                return $("<tr id = "+randomNumber+"><td>" + file.name + "<\/td><td>"+file.progressMsg+"</td><TD>"+file.size+"</TD><td><img src = 'fileupload/deleteFile.png' class = "+file.name+" onmoouseover = 'setPointerAsHand();' onclick = 'deleteUploadedFile(this,"+randomNumber+")'/></td><\/tr>");
            }
        });
    };
    initFileUpload(1); 
});

var deleteUploadedFile = function (ctrl,rowId) {	
	var fileName = $(ctrl).attr("class");
	$("#"+rowId).remove();
	$.ajax({
		url:"deleteUploadedFile.action",
		data: "fileToUploadFileName="+fileName,
		success:function (){}
	});
};

var setPointerAsHand = function (ctrl){
	$(ctrl).css({"cursor":"hand"});
};
</script> 
</div>