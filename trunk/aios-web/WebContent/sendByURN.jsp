<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<meta http-equiv="Content-Type charset=utf-8" /> 
	<title>AIO | URN Verification</title>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/resetlogin.css"  type="text/css" media="screen" />
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/adel1.jpg" type="image/x-icon" />
	<link href="${pageContext.request.contextPath}/css/reset.css" rel="stylesheet"   type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"  type="text/css" media="screen" />  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/invalid.css"  type="text/css" media="screen" />	
	<link href="${pageContext.request.contextPath}/css/messages.css" rel="stylesheet"  type="text/css" media="all" /> 
<c:choose>
<c:when test="${IS_MOBILE eq true}">
	<link href="${pageContext.request.contextPath}/css/general-mobile.css" rel="stylesheet"  type="text/css" media="all" />
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style-mobile.css" type="text/css" media="screen"/>
</c:when>
<c:otherwise>
	<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet"  type="text/css" media="all" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css" type="text/css" media="screen"/>
</c:otherwise>
</c:choose>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/countDown.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/countDownMail.js"></script>  
<script>

function confirmLogout() {
	$.ajax({
		type : "POST",
		async : false,
		dataType : "json",
		url : "<%=request.getContextPath()%>/confirm_logout.action",
		cache : false,
		success : function(response) {  
			$(location).attr('href','logout.do'); 
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", "${pageContext.request.contextPath}/index.jsp");
			$('<input>').attr({
			    type: 'hidden',
			    id: 'KEY',
			    name: 'KEY',
				value : response.implementationId
			}).appendTo(form);
			document.body.appendChild(form);
			form.submit();
		},
		error : function(e) {
			
		}
	}); 
}

function showResendOptions() {

	$("#resendOptions").fadeIn();
}

function resendURN() {
	
	console.debug('urn resend requested ' + ($("#resendInEmail").attr("checked") == "checked"));
	
	$("#errorReport").text("");
	$(".notifyUser").html("Please wait, resending URN...");
	$("#resendURN").attr("disabled", true);
	
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/urn_resend_verification.action", 
	 	async: false,
	 	data: {
	 		isURNResendInEmail: ($("#resendInEmail").attr("checked") == "checked")
	 	},
	    dataType: "json",
	    cache: false,
		success: function(result) {
			$(".notifyUser").html(result.notifyUser);
			$("#resendOptions").hide();
			setTimeout("showResendOptions()", 15000);
			$("#resendURN").attr("disabled", false);
			$("#text-corner").focus();
	 	},  
	 	error:function(result) {
	 		$("#errorReport").text("There was an error processing this request, please try again in a while.");
	 		$("#errorReport").show();
	 		$("#resendURN").attr("disabled", false);
	 	} 
   	});
}

 $(document).ready(function(){ 
	 
	 $("#smsmode").hide();
	 $("#emailmode").show();
	 
	 //mailcounter();
	 
	 //$('.formError').remove(); 
	 $("#text-corner").focus();

	 $('#text-corner').bind('keypress', function(e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 if(keyCode == 13) { 
			 $("#gomail").trigger('click');
		 }
	});
	 
	$("#gomail").click(function() {
		
		var textcorner = $('#text-corner').val();
		$("#errorReport").hide();
		
       	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/urn_verification.action", 
		 	async: false,
		 	data: {
		 		urnNumber: Number(textcorner)
		 	},
		    dataType: "json",
		    cache: false,
			success: function(result) {
				if(result.urnVerificationResult == "PASS") {					
					window.location.href = "${pageContext.request.contextPath}/user_selected_company.action";
				} else if (result.urnVerificationResult == "FORCE_PASSWORD_UPDATE") {
					window.location.href = "${pageContext.request.contextPath}/forceUpdatePasswordRedirect.action?passwordFlag=C";
				} else {
					$("#errorReport").show();
					$("#errorReport").text("Please try again, entered code is invalid.");
				}
		 	},  
		 	error:function(result) {
		 		$("#errorReport").show();
		 		$("#errorReport").text("Please provide valid code, only numbers are allowed.");
		 	} 
       	});
	});
	
	setTimeout("showResendOptions()", 20000);
 });
</script> 
</head>

<style>
	.notifyUser {
		font-size: 11px;
		color: gray;
		text-align: center;
		width: 275px;
	}
	
	#errorReport {
		font-size: 11px;
		color: #FB2F2F;
	}
	
	.resend_box {
		display: none;
		height: 85px;
		width: 55%;
		background: rgba(221, 221, 221, 0.36) none repeat scroll 0% 0%;
		color: #625F5F;
		border: 1px solid rgba(204, 204, 204, 0.43);
		border-radius: 3px;
		padding: 10px;
		margin-top: 8px;
	}
	
	.resend_span {
		width: 105px;
		display: inline-block;
		text-transform: uppercase;
		font-size: 10px;
		vertical-align: text-top;
		text-align: left;
	}
	
	#resendURN {
		cursor: pointer;
		background: #5860FF none repeat scroll 0% 0%;
		color: #FFF;
		border-radius: 2px;
		margin-top: 5px;
		padding: 5px;
	}
</style>

<body style="height: auto;">
<div id="wrapper">		
	<div id="login-content" class="loginbox">
		<div id="sendURN" class="loginbox_orignal" align="center">
				<div align="center" style="margin-bottom: 10px;">
		    		<img src="images/logo.png" alt="" />
		    	</div>
			<div id="emailmode">
				<form name="mailcounter" id="urnVerification">
				 	<div style="display:none;" class="tempresult"></div>
				 	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>
					
				 	<c:choose>
					<c:when test="${IS_MOBILE eq true}">
						<div class="notification information_urn png_bg" align="left"
							 style="width: 50%; height: 35px;font-size: 30px; padding-left: 35px; padding-top: 12px; font-size: 10px;"> 
							PROVIDE URN & HIT ENTER | <a href="logout.do" style="font-size: 8px;">CANCEL</a>
					 	</div>
						<div style="width: 100%;">
							<input class="validate[required,custom[numberOnly,max[6]]]" 
							 		type="text" name="urnmail" id="text-corner" value=""
							 		MAXLENGTH="6" style="width: 500px!important; height: 70px;font-size: 30px;" />
							<input style="display: none;" id="time_counter" readonly="true" type="text" value="03:00" name="dispmail" />
						</div>	
						<br/><br/>
						<div class="notifyUser">${notifyUser}</div>
						<br/><br/>
						<span id="errorReport"></span>	
						<div align="center">					
						<div style="margin-top: 10px; text-align: center;" align="center">
							<img name="go" id="gomail" style="cursor: pointer;" src="images/signin_mobile.png" />																
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="notification information_urn png_bg" align="left"
							 style="width: 50%; height: 20px; padding-left: 30px; padding-top: 8px; font-size: 10px;"> 
							PROVIDE URN & HIT ENTER | <a href="#" onclick="confirmLogout()" style="font-size: 8px;">CANCEL</a>
					 	</div>					 	
						<div style="width: 80%;">
							<input class="validate[required,custom[numberOnly,max[6]]]" 
							 		type="text" name="urnmail" id="text-corner" value=""
							 		MAXLENGTH="6" style="width: 229px!important; height: 27px; font-size: 11px;" />
							<input style="display: none;" id="time_counter" readonly="true" type="text" value="03:00" name="dispmail">
						</div>
						<br/><br/>
						<div class="notifyUser">${notifyUser}</div>
						<br/><br/>
						<span id="errorReport"></span>	
						<div align="center">					
							<div style="margin-top: 10px; text-align: center; display: none;" align="center">
								<img name="go" id="gomail" style="cursor: pointer;" src="images/signin.png" />																
							</div>
						</div>						
					</c:otherwise>
					</c:choose>
				</form>
			</div>
			<div id="resendOptions" class="resend_box">
				Haven't received the code? <br/> <br/>
				<input name="resend" type="radio" id="resendInEmail" checked="checked" /> <span class="resend_span">Resend via Email</span> <br/>
				<input name="resend" type="radio" id="resendInSMS" /> <span class="resend_span">Resend via SMS</span> <br/>
				<input type="button" name="resendURN" id="resendURN" style="cursor: pointer;" value="RESEND" onclick="resendURN()" />
			</div>
		</div>
	</div>
</div>
<div class="termsbot" align="center">
   	<!-- <img src="images/shadow.png" alt="" /> -->
   </div>
</body>
</html>