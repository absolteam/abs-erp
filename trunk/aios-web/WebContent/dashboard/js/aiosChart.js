/**
 * 
 */
function HideContent(d) {
document.cookie=d+"=hide";
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {

document.cookie=d+"=show";
document.getElementById(d).style.display = "block";
}
function ReverseDisplay(d) {
	if(document.getElementById(d).style.display === "none") { 
		document.cookie=d+"=show";
		document.getElementById(d).style.display = "block"; 
		$('#'+d+"_check").attr("checked",true);
	}
	else { 
		document.cookie=d+"=hide";
		document.getElementById(d).style.display = "none"; 
		$('#'+d+"_check").attr("checked",false);
	}
	
	$('#dashboard_position_change_save').show();
}
var xpos; var ypos; var yip; var xip;

$(function() {
	
	 //Check box check all and uncheck all
	 $('#dg_check_all').change(function(){
		 if($('#dg_check_all').attr("checked")) { 
			 $('.dg_check').each(function(){
				var tempId=$(this).attr("id").split("_");
				 document.cookie=tempId[0]+"=show";
				 document.getElementById(tempId[0]).style.display = "block"; 
				 $("#"+$(this).attr("id")).attr("checked",true);
				//Create the function
				 var fn = window[tempId[0]];
				  
				 //Call the function
				 fn();
			});
			 
		 }
		 else { 
			 $('.dg_check').each(function(){
				var tempId=$(this).attr("id").split("_");
				 document.cookie=tempId[0]+"=hide";
				 document.getElementById(tempId[0]).style.display = "none"; 
				 $("#"+$(this).attr("id")).attr("checked",false);
			}); 
		}
		 
		 $('#dashboard_position_change_save').show();
	 });
	 
	 $('.dg_check').change(function(){
		 if($(this).attr("checked")) { 
			var tempId=$(this).attr("id").split("_");
			 document.cookie=tempId[0]+"=show";
			 document.getElementById(tempId[0]).style.display = "block"; 
			 $("#"+$(this).attr("id")).attr("checked",true);
			//Create the function
			 var fn = window[tempId[0]];
			  
			 //Call the function
			 fn();
		 }
		 else { 
			var tempId=$(this).attr("id").split("_");
			 document.cookie=tempId[0]+"=hide";
			 document.getElementById(tempId[0]).style.display = "none"; 
			 $("#"+$(this).attr("id")).attr("checked",false);
		}
		 
		 $('#dashboard_position_change_save').show();
	 });
	 
	$.jqplot._noToImageButton = true;
	$.jqplot.config.enablePlugins = true;
	  
	$('.content')
    .resizable({
		grid: [20, 20,20,20],
		handles: "se" ,
		containment: "#containment-wrapper", scroll: true, scrollSensitivity: 100,
        start: function(e, ui) {
         
        },
        resize: function(e, ui) {
         
        },
        stop: function(e, ui) {
           // alert('resizing stopped'); 
		   
		   var wid = ui.size.width;
		   var hei = ui.size.height;
		   /*
		   document.form1.w.value = ui.size.width;
	       document.form1.h.value = ui.size.height;
		   */
		   var dataString = 'w='+ wid + '&h=' + hei + '&id=' + 1;
		   var tempId=$(this).attr('id');
		   document.cookie=tempId+"wid="+wid;
		   document.cookie=tempId+"hei="+hei;
		   $('#dashboard_position_change_save').show();
		   $("#"+tempId).css({
				  'width':wid+'px',
				  'height':hei+'px'
				  });
	/* $.ajax({
    type:'POST',
    data:dataString,
    url:'insertsize.php',
    success:function(data) {
    },error:function(data){
		
	}
  }); */
        }
    });
	
  // sets draggable the element with id='dg'
	 $('.dg').draggable({
	grid: [20, 20,20,20],
	containment: "#containment-wrapper", scroll: true, scrollSensitivity: 100,
    // get the initial X and Y position when dragging starts
    start: function(event, ui) {
      xpos = ui.position.left;
      ypos = ui.position.top;
    },
    // when dragging stops
    stop: function(event, ui) {
      // calculate the dragged distance, with the current X and Y position and the 'xpos' and 'ypos'
      var xmove = ui.position.left - xpos;
      var ymove = ui.position.top - ypos;
	var yip = ui.position.left - ypos;
	var xip = ui.position.top - xpos;
      // define the moved direction: right, bottom (when positive), left, up (when negative)
      var xd = xmove >= 0 ? ' To right: ' : ' To left: ';
      var yd = ymove >= 0 ? ' Bottom: ' : ' Up: ';
	var winid = '1'; 
	
	var x = xip + xpos;
	var y = yip + ypos;
	 
	var dataString = 'x='+ x + '&y=' + y + '&id=' + 1;
	var tempId=$(this).attr('id');	
	document.cookie=tempId+"xpos="+x;
	document.cookie=tempId+"ypos="+y;
	$('#dashboard_position_change_save').show();
	$("#"+tempId).css({
		  'left':y+'px',
		  'top':x+'px'
		  });
	/* $.ajax({
    type:'POST',
    data:dataString,
    url:'insertpos.php',
    success:function(data) {
     // alert(data);
    },error:function(data){
		
	}
  }); */
	
    }
  });
	 
	
}); 

function drawBarChart(data, ticks, chartId,legendLabels ) {
	if(data=='')
		data=[[null]];
	if(ticks=='')
		ticks=[[null]];
	var plot = $.jqplot(chartId, data, {
		   		    seriesDefaults:{
		      renderer:$.jqplot.BarRenderer,
		      rendererOptions: {
		          barMargin: 20,
		          /*shadowOffset: 2,    // offset from the bar edge to stroke the shadow.
		          shadowDepth: 5,     // nuber of strokes to make for the shadow.
		          shadowAlpha: 0.8,   // transparency of the shadow.
*/		      }
		    },
		    seriesColors: [ "#009FE0", "#FF9F26", "#EF6596", "#21B15C", "#FF3334", "#958c12",
		                    "#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
            grid: {
                drawGridLines: false,        // wether to draw lines across the grid or not.
                gridLineColor: '#f3f3f3',    // *Color of the grid lines.
                background: '#fff',      // CSS color spec for background color of grid.
                borderColor: '#ddd',     // CSS color spec for border around grid.
                borderWidth: 0.5,           // pixel width of border around grid.
                shadow: true,               // draw a shadow for grid.
                shadowAngle: 45,            // angle of the shadow.  Clockwise from x axis.
                shadowOffset: 1.5,          // offset from the line of the shadow.
                shadowWidth: 1,             // width of the stroke for the shadow.
                shadowDepth: 1,             // Number of strokes to make when drawing shadow.
                                            // Each stroke offset by shadowOffset from the last.
                shadowAlpha: 0.05,          // Opacity of the shadow
            },

		    gridPadding:{
		        left:50
		    },
		    axes: {
		      xaxis: {
		          renderer: $.jqplot.CategoryAxisRenderer,
		          ticks:ticks,
		      },
		      yaxis: {
		          padMin: 0,
		          
		      }
		    },
		    legend: {
		        show: true,
		        location: 'ne',
		        rendererOptions:{placement: "outsideGrid"},
		       labels: legendLabels,
		     },   highlighter: {
		    	 show: false,
		    	 
			},
			cursor: {
		        show: false,
		        
		    },        
		  });
	return plot;
}

function drawBarChartWithoutLegends(data, ticks, chartId) {
	if(data=='')
		data=[[null]];
	if(ticks=='')
		ticks=[[null]];
	else{
		$.each( ticks, function( key, value ) {
		  ticks[key]=value.replace(/\s/g,"<br/>");
		});
	}
	data[0].push(0);
	
	var plot = $.jqplot(chartId, data, {
		series:[{renderer:$.jqplot.BarRenderer}],
		highlighter: {
			show: false
		},
		seriesColors: [ "#009FE0", "#FF9F26", "#EF6596", "#21B15C", "#FF3334", "#958c12",
	                    "#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
        grid: {
            drawGridLines: false,        // wether to draw lines across the grid or not.
            gridLineColor: '#f3f3f3',    // *Color of the grid lines.
            background: '#fff',      // CSS color spec for background color of grid.
            borderColor: '#ddd',     // CSS color spec for border around grid.
            borderWidth: 0.5,           // pixel width of border around grid.
            shadow: true,               // draw a shadow for grid.
            shadowAngle: 45,            // angle of the shadow.  Clockwise from x axis.
            shadowOffset: 1.5,          // offset from the line of the shadow.
            shadowWidth: 1,             // width of the stroke for the shadow.
            shadowDepth: 1,             // Number of strokes to make when drawing shadow.
                                        // Each stroke offset by shadowOffset from the last.
            shadowAlpha: 0.05,          // Opacity of the shadow
        },

		gridPadding:{
	        left:50
	    },
	    axes: {
	      xaxis: {
	        renderer: $.jqplot.CategoryAxisRenderer,
	        ticks:ticks,
	      },yaxis: {
	          padMin: 0,
		   }
	    }, cursor: {
	        show: false
	    },
		    
		  });
	return plot;
}
function drawPieChart(data, chartId) {
	if(data=='')
		data=[[null]];
	var plot =$.jqplot(chartId, 
			data, 
		      {
		        seriesDefaults: {shadow: true, renderer: $.jqplot.PieRenderer, rendererOptions: { showDataLabels: true } }, 
		         legend: {
		  		  show: true,
		  		  location: 's',
		  		  rendererOptions:{numberRows:2, placement: "outsideGrid"},
		  		  marginTop: '5px'
		  		},
		  		seriesColors: [ "#009FE0", "#FF9F26", "#EF6596", "#21B15C", "#FF3334", "#958c12",
			                    "#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
		        grid: {
		            drawGridLines: false,        // wether to draw lines across the grid or not.
		            gridLineColor: '#f3f3f3',    // *Color of the grid lines.
		            background: '#fff',      // CSS color spec for background color of grid.
		            borderColor: '#ddd',     // CSS color spec for border around grid.
		            borderWidth: 0.5,           // pixel width of border around grid.
		            shadow: true,               // draw a shadow for grid.
		            shadowAngle: 45,            // angle of the shadow.  Clockwise from x axis.
		            shadowOffset: 1.5,          // offset from the line of the shadow.
		            shadowWidth: 1,             // width of the stroke for the shadow.
		            shadowDepth: 1,             // Number of strokes to make when drawing shadow.
		                                        // Each stroke offset by shadowOffset from the last.
		            shadowAlpha: 0.05,          // Opacity of the shadow
		        },

		  		
		  		highlighter: {
		  		    show: true,
		  		   tooltipFormatString: '%s',
			  		 tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
	                     return '<b><span style=" border: 0.5px solid #aaaaaa;padding: 1px 3px;'
	                     +'background-color: #ddd; color: #333;">'+str.split(",")[0]+', Value '+str.split(",")[1]+'</span></b>';
	                 },
		  		   tooltipLocation: 'ne',
		  		   lineWidthAdjust:3,
			       sizeAdjust:10,
		  		   useAxesFormatters: false
		  		}
		      }
		     
		    );
	return plot;
}

function drawPieChartLegendByRight(data, chartId) {
	if(data=='')
		data=[[null]];
	var plot =$.jqplot(chartId, 
			data, 
		      {
		        seriesDefaults: {
		        	shadow: true, 
		        	renderer: $.jqplot.PieRenderer, 
		        	rendererOptions: { 
		        		showDataLabels: true,
		        	} 
		        }, 
		         legend: {
		  		  show: true,
		  		  location: 'ne',
		  		  rendererOptions:{placement: "insideGrid"},
		  		  marginTop: '5px',
		  		},
		  		seriesColors: [ "#009FE0", "#FF9F26", "#EF6596", "#21B15C", "#FF3334", "#958c12",
			                    "#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
		        grid: {
		            drawGridLines: false,        // wether to draw lines across the grid or not.
		            gridLineColor: '#f3f3f3',    // *Color of the grid lines.
		            background: '#fff',      // CSS color spec for background color of grid.
		            borderColor: '#ddd',     // CSS color spec for border around grid.
		            borderWidth: 0.5,           // pixel width of border around grid.
		            shadow: true,               // draw a shadow for grid.
		            shadowAngle: 45,            // angle of the shadow.  Clockwise from x axis.
		            shadowOffset: 1.5,          // offset from the line of the shadow.
		            shadowWidth: 1,             // width of the stroke for the shadow.
		            shadowDepth: 1,             // Number of strokes to make when drawing shadow.
		                                        // Each stroke offset by shadowOffset from the last.
		            shadowAlpha: 0.05,          // Opacity of the shadow
		        },

		  		
		  		highlighter: {
		  		    show: true,
		  		   tooltipFormatString: '%s',
			  		 tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
	                     return '<b><span style=" border: 0.5px solid #aaaaaa;padding: 1px 3px;'
	                     +'background-color: #ddd; color: #333;">'+str.split(",")[0]+', Value '+str.split(",")[1]+'</span></b>';
	                 },
		  		   tooltipLocation: 'ne',
		  		   lineWidthAdjust:3,
			       sizeAdjust:10,
		  		   useAxesFormatters: false
		  		}
		      }
		     
		    );
	return plot;
}

var count = 1;

function setStyle(){
		if(getCookie("content1wid")==null || getCookie("content1wid")==""){
			defaultPosition();
		}
		//console.debug(document.cookie);

		$('.dg').each(function(){
		  	var tempId=$(this).attr("id");
	
			var hideShow=getCookie(tempId);
			if(hideShow=='show'){
				$('#'+tempId).show();
				$('#'+tempId+"_check").attr("checked",true);
			}else{
				$('#'+tempId).hide();
				$('#'+tempId+"_check").attr("checked",false);
			}	
			xpos=getCookie(tempId+"xpos");
			ypos=getCookie(tempId+"ypos");
			$("#"+tempId).css({
				  'left':ypos+'px',
				  'top':xpos+'px'
				  });
		  });
		  
		$('.content').each(function(){
			var tempId=$(this).attr("id");
			var wid=getCookie(tempId+"wid");
			var hei=getCookie(tempId+"hei");
			$("#"+tempId).css({
				  'width':wid+'px',
				  'height':hei+'px'
				  });
			 $('#'+tempId).resizable({delay:5});
			 $('#'+tempId).resizable({delay:5, helper:'ui-resizable-helper'});
		  }); 
}


function getCookie(cname){
	var name = cname + "=";
	//console.debug(document.cookie);
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	  {
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	  }
	return "";
}

function defaultPosition(){
	//alert(user_dashboard);
	var cookieVar=null;
	/* user_dashboard defined in dashboard-home.jsp file */
		
	if(typeof(user_dashboard) != 'undefined' && user_dashboard!=null && user_dashboard!= ""){
		cookieVar = user_dashboard;
	} else { 
			/*cookieVar="content1wid=1240; content1hei=271; content8wid=384; content8hei=511; content7wid=810; content7hei=252; " +
			"content5wid=504; content5hei=331; content6wid=287; content6hei=327; content3wid=592; content3hei=307; content2wid=638; " +
			"content2hei=305; dg1xpos=6; dg1ypos=7; dg3xpos=286; dg3ypos=7; dg2xpos=286; dg2ypos=607; dg8xpos=86; dg8ypos=7; dg7xpos=6; " +
			"dg7ypos=407; dg5xpos=266; dg5ypos=407; dg6xpos=266; dg6ypos=927; dg4=hide; dg102xpos=106; dg102ypos=467; dg101xpos=306; " +
			"dg101ypos=487; dg100xpos=6; dg100ypos=487; content101wid=722; content101hei=290; content100wid=762; content100hei=247; " +
			"content102wid=626; content102hei=208; dg10xpos=6; dg10ypos=547; dg9xpos=6; dg9ypos=7; content10wid=650; content10hei=279; " +
			"content9wid=519; content9hei=279; dg1000xpos=6; dg1000ypos=7; content1000wid=388; content1000hei=66; dg103xpos=6; dg103ypos=7; " +
			"content103wid=472; content103hei=251; dg102=hide; dg14xpos=6; dg14ypos=587; dg13xpos=286; dg13ypos=587; content14wid=666; " +
			"content14hei=270; dg114xpos=266; dg114ypos=787; dg113xpos=266; dg113ypos=787; dg110xpos=306; dg110ypos=407; dg113=hide; " +
			"dg114=show; dg109=hide; dg110=hide; dg101=hide; dg108=hide; dg107=hide; dg106=hide; dg105=hide; dg104=hide; content13wid=656; " +
			"content13hei=308; dg15xpos=6; dg15ypos=7; content15wid=575; content15hei=281; dg115=show; dg=show; dg103=show; dg100=show; " +
			"dg13=show; dg14=show; dg15=show; dg5=show; dg6=show; dg7=show; dg8=show; dg1000=show; dg9=show; dg10=show; dg1=show; dg2=show; " +
			"dg3=show; dg16=show; content16wid=567; content16hei=290; dg16xpos=306; dg16ypos=7; dg116=show; content116wid=355; content116hei=331; " +
			"dg116xpos=266; dg116ypos=7; dg115xpos=266; dg115ypos=367; content113wid=452; content113hei=334; " +
			"content115wid=409; content115hei=333; content114wid=463; content114hei=327;";*/
	
			cookieVar = "content1wid=1240; content1hei=271; content8wid=384; content8hei=511; content7wid=810; content7hei=252; content5wid=504; content5hei=331; content6wid=287; content6hei=327; content3wid=592; content3hei=307; content2wid=638; content2hei=305; dg1xpos=6; dg1ypos=7; dg3xpos=286; dg3ypos=7; dg2xpos=286; dg2ypos=607; dg8xpos=86; dg8ypos=7; dg7xpos=6; dg7ypos=407; dg5xpos=266; dg5ypos=407; dg6xpos=266; dg6ypos=927; dg4=hide; dg102xpos=106; dg102ypos=467; dg101xpos=306; dg101ypos=487; dg100xpos=6; dg100ypos=487; content101wid=722; content101hei=290; content100wid=762; content100hei=247; content102wid=626; content102hei=208; dg10xpos=6; dg10ypos=547; dg9xpos=6; dg9ypos=7; content10wid=650; content10hei=279; content9wid=519; content9hei=279; dg1000xpos=6; dg1000ypos=7; content1000wid=388; content1000hei=66; dg103xpos=6; dg103ypos=7; content103wid=1235; content103hei=590; dg102=hide; dg14xpos=6; dg14ypos=587; dg13xpos=286; dg13ypos=587; content14wid=666; content14hei=270; dg114xpos=266; dg114ypos=787; dg113xpos=266; dg113ypos=787; dg110xpos=306; dg110ypos=407; dg113=hide; dg114=hide; dg109=hide; dg110=hide; dg101=hide; dg108=hide; dg107=hide; dg106=hide; dg105=hide; dg104=hide; content13wid=656; content13hei=308; dg15xpos=6; dg15ypos=7; content15wid=575; content15hei=281; dg115=hide; dg=show; dg103=show; dg100=hide; dg13=show; dg14=show; dg15=show; dg5=show; dg6=show; dg7=show; dg8=show; dg1000=show; dg9=show; dg10=show; dg1=show; dg2=show; dg3=show; dg16=show; content16wid=567; content16hei=290; dg16xpos=306; dg16ypos=7; dg116=hide; content116wid=355; content116hei=331; dg116xpos=266; dg116ypos=7; dg115xpos=266; dg115ypos=367; content113wid=452; content113hei=334; content115wid=409; content115hei=333; content114wid=463; content114hei=327;";
	}
	var ca = cookieVar.split(';');
	for(var i=0; i<ca.length; i++) 
	  {
	  var c = ca[i].trim();
	  document.cookie=c;
	  }
}

function saveContentPositionByUser(){
	var cookievalue=document.cookie	;
	$.ajax({
		type:"POST",
		url:"save_dashboard_position.action", 
	 	async: false, 
	 	data:{	dashboardContent:cookievalue },
	    dataType: "json",
	    cache: false,
		success:function(response){   
			 if(($.trim(response.returnMessage)=="SUCCESS")) {
				 $('#dashboard_position_change_save').hide();
				 $('#common_success_message').hide().html("Dashboard Saved").slideDown(800);
			 }
			 else{
				 $('#common_error_message').hide().html("Failed to Save").slideDown(800);
				 return false;
			 }
		},
		error:function(response){  
			 $('#common_error_message').hide().html("Failed to Save").slideDown(800);
			 return false;
		}
	}); 
	
}
function findWidgetVisibility(widgetName){
	var returnStatus=false;
	var hideShow=getCookie(widgetName);
	if(hideShow=='hide'){
		returnStatus=false;
	}else{
		returnStatus=true;
	}	
	
	return returnStatus;
}