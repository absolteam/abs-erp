<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<meta http-equiv="Content-Type charset=utf-8" /> 
<title>Adel | Sign In</title>  
<link rel="shortcut icon" href="/images/adel1.jpg" type="image/x-icon" />
<link href="/css/general.css" rel="stylesheet"  type="text/css" media="all" />
<link href="/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />  
<link rel="stylesheet" href="/css/style.css"  type="text/css" media="screen" />  
 <link href="/css/messages.css" rel="stylesheet"  type="text/css" media="all" /> 

 <!--[if IE 6]>
	<link href="css/ie6.css" rel="stylesheet" media="all" />
	
	<script src="js/pngfix.js"></script>
	<script>
	  /* EXAMPLE */
	  DD_belatedPNG.fix('.logo, .other ul#dashboard-buttons li a');

	</script>
	<![endif] ttt-->
	<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" media="all" />
	<![endif]-->
<script type="text/javascript"> 

</script>
</head>
<body id="login"> 
	<div id="login-wrapper" class="png_bg">
		<div id="login-top"> 
			<h1>Adel Business Solutions</h1> 
		</div> 
		<div id="login-content">
			<div id="sendURN">
				<div id="ErrLog">
					<form name="login1" method="post" > 
						<div class="notification information png_bg">
							<div>
								Server Under Maintenance...
							</div>
						</div> 
						<div class="clear"></div>
						<div class="notification png_bg ">
							<div>
								From ${requestScope.startDate } at ${requestScope.startTime }
							</div>
						</div>
						<div class="notification png_bg ">
							<div>
								To ${requestScope.endDate } at ${requestScope.endTime }
							</div>
						</div> 
						<div class="clear"></div>
						<div class="notification png_bg">
							<div>
								For  ${requestScope.description } 
							</div>
						</div> 
					</form>
					</div> 
				</div>
			</div>
		</div>   
  </body>
</html>