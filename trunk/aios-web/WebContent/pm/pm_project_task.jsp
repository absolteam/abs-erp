<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var projectTaskId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var editFlag=null; var expensePrint = false;  var status = null;
$(function(){
	 $('.formError').remove();
  	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
	 $('#descriptive-popup').dialog('destroy');
		$('#descriptive-popup').remove();
		$('#accessrights-popup').dialog('destroy');
		$('#accessrights-popup').remove();
		$('#baseprice-popup-dialog').dialog('destroy');		
		$('#baseprice-popup-dialog').remove(); 
		$('#sales-product-inline-popup').dialog('destroy');
		$('#sales-product-inline-popup').remove();
	 oTable = $('#ProjectList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSort": false,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allprojects_task.action?projectId=0",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 }, {
			"mDataProp" : "taskTitle"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 }, {
			"mDataProp" : "projectTitle"
		 }, {
			"mDataProp" : "parentTaskTitle"
		 },{
			"mDataProp" : "currentStatus"
		 } ]
	});	 
		
	$('#add').click(function(){   
		projectTaskId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_task_entry.action", 
	     	async: false, 
	     	data:{projectTaskId: projectTaskId,projectId:Number(0),recordId:0,alertId:0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
 		if(projectTaskId != null && projectTaskId != 0){
 			if(!editFlag){
				$('#error_message').hide().html("Can not edit!!! Status is closed").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/project_task_entry.action", 
		     	async: false, 
		     	data:{projectTaskId: projectTaskId,projectId:Number(0),recordId:0,alertId:0},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  
	
	$('#view').click(function(){   
 		if(projectTaskId != null && projectTaskId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/project_task_view.action", 
		     	async: false, 
		     	data:{projectTaskId: projectTaskId,projectId:Number(0),recordId:0,alertId:0},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(projectTaskId != null && projectTaskId != 0){
			if(!editFlag){
				$('#error_message').hide().html("Can not delete!!! Status is closed").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_task_delete.action", 
	     	async: false,
	     	data:{projectTaskId: projectTaskId,recordId:0,alertId:0},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_project_task.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted..")
																			.slideDown(
																					1000);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert("Please select a record to delete.");
								return false;
							}
						});
		
		$('#print').click(function(){ 
			if(expensePrint == true){ 
				var actionName = '';
				 if(status == 'Open'){
					 actionName = "get_projecttask_info_printout";
				 }else{
					 actionName = "projecttask_delivery_print";
				 }
				if(projectTaskId != null && projectTaskId != 0){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/"+actionName+".action", 
				     	async: false, 
				     	data:{projectTaskId: projectTaskId},
						dataType: "json",
						cache: false,
						success: function(response){  
							 if(status == 'Open'){
								jobTaskTemplatePrint(response);
							 }else{
								 taskDeliveryTemplatePrint(response); 
							 }
						} 		
					}); 
				}else{
					alert("Please select a record to print."); 
				} 
			} 
			return false;
		});
		
		/* Click event handler */
		$('#ProjectList tbody tr').live('click', function() {
			  $('#print').css('opacity', 0.5);
			  expensePrint = false;   	
			 $("#edit").css('opacity','0.5');
	    	  $("#delete").css('opacity','0.5');
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				projectTaskId = aData.projectTaskId;
				editFlag = aData.editFlag;
		          if(editFlag==true){
		        	  $("#edit").css('opacity','1');
		 	   	  	 $("#delete").css('opacity','1');
		          }
		         status =  $.trim(aData.currentStatus);
 	          	 if(status == 'Open' || status == 'Delivered'){
 	        	   expensePrint = true;   
	        	   $('#print').css('opacity', 1);
	          	} 
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				projectTaskId = aData.projectTaskId;
				editFlag = aData.editFlag;
	            if(editFlag==true){
	        	 $("#edit").css('opacity','1');
	 	   	  	 $("#delete").css('opacity','1');
	            }
	            status =  $.trim(aData.currentStatus);
	          	 if(status == 'Open' || status == 'Delivered'){
	        	   expensePrint = true;   
	        	   $('#print').css('opacity', 1);
	          	} 
			}
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Task
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
				<div id="project_list">
					<table id="ProjectList" class="display">
						<thead>
							<tr>
								<th style="width:0.5%">Reference Number</th>
								<th>Title</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>${sessionScope.project_projectTitle}</th>
								<th>Parent Task</th>
								<th>Status</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Add</span>
				</div>
				<div class="sep_li"></div>
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
				<div class="first_li">
					<span>View</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="view">
					View
				</div>
				<div class="portlet-header ui-widget-header float-right" id="print">
					Print
				</div>
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="accounts.common.button.edit" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="add">
					<fmt:message key="accounts.common.button.add" />
				</div>
			</div>
		</div>
	</div>
		<div align="center" id="content">
		<h1 id="title" style="display: none;"></h1> 
		<applet id="qz" code="qz.PrintApplet.class"
			archive="${pageContext.request.contextPath}/printing/applet/qz-print.jar" 
			width="0" height="0" align="left"> 
			<param name="jnlp_href" value="${pageContext.request.contextPath}/printing/applet/qz-print_jnlp.jnlp">
			<param name="cache_option" value="plugin">
		</applet>
	</div> 
</div>
<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jZebra.js"></script>