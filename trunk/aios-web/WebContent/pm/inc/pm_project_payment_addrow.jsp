<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}" style="display: none;">${rowId}</td>
	<td><input type="text" class="width96 paymentReference"
		id="paymentReference_${rowId}" />
	</td>
	<td><input type="text" class="width96 paymentDate"
		readonly="readonly" id="paymentDate_${rowId}" />
	</td>
	<td><input type="text" class="width96 paymentAmount right-align"
		id="paymentAmount_${rowId}" />
	</td>
	<td><input type="text" name="taskDescription"
		id="taskDescription_${rowId}" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		id="paymentDetailId_${rowId}" />
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$('.paymentDate').datepick({showTrigger: '#calImg'}); 
});
</script>