<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript">
var slidetab = "";
var projectPayments = [];
$(function(){
	
	$jquery("#projectMileStoneValidation").validationEngine('attach'); 

	$('#startDate,#endDate').datepick({
		 onSelect: customRange, showTrigger: '#calImg'});
	
	$('.paymentDate').datepick();

	 $('#project_milestone_discard').click(function(event){  
		 projectMileStoneDiscard("");
		 return false;
	 });
	 
	 manupulateLastRow();

	 $('#project_milestone_save').click(function(){ 
		 if($jquery("#projectMileStoneValidation").validationEngine('validate')){ 
 			var referenceNumber = $('#referenceNumber').val(); 
	 		var milestoneTitle = $('#milestoneTitle').val(); 
 			var projectId = Number($('#projectId').val());   
 			var startDate = $('#startDate').val(); 
 			var endDate = $('#endDate').val();
  			var estimationCost = Number($('#estimationCost').val());
 			var description=$('#description').val();  
 			var projectMileStoneId = Number($('#projectMileStoneId').val()); 
 			projectPayments = getProjectPaymentDetails();  
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_project_milestone.action", 
			 	async: false, 
			 	data:{	projectMileStoneId: projectMileStoneId, projectId: projectId, referenceNumber: referenceNumber, milestoneTitle: milestoneTitle,
			 			startDate: startDate, endDate: endDate, description: description, estimationCost: estimationCost,
			 			projectPayments: JSON.stringify(projectPayments)
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){   
					 if(($.trim(response.returnMessage)=="SUCCESS")) 
						 projectMileStoneDiscard(projectMileStoneId > 0 ? "Record updated.":"Record created.");
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
		}else{
			return false;
		}
	 });  
	 
	 var getProjectPaymentDetails = function(){
		 projectPayments = []; 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var paymentReference = $('#paymentReference_'+rowId).val();  
			var paymentDate = $('#paymentDate_'+rowId).val(); 
			var paymentAmount = Number($('#paymentAmount_'+rowId).val());  
			var linesDescription = $.trim($('#linesDescription_'+rowId).text());  
			var paymentDetailId =  Number($('#paymentDetailId_'+rowId).val()); 
			if(typeof paymentReference != 'undefined' && paymentAmount > 0 && paymentDate != '' ){
				var jsonData = [];
				jsonData.push({
					"paymentReference" : paymentReference,
					"paymentDate" : paymentDate,
					"paymentAmount" : paymentAmount,
					"description" : linesDescription,
					"paymentDetailId": paymentDetailId
				});   
				projectPayments.push({
					"projectPayments" : jsonData
				});
			} 
		});  
		return projectPayments;
	 };
	 
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/projectpayment_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 $('.paymentReference,.paymentDate,.paymentAmount').live('change',function(){
		 var rowId = getRowId($(this).attr('id'));
		 triggerAddRow(rowId);
		 return false;
	 });
	 
	 $('.project-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_project_popup.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			}
		});  
		return false;
	});

	 $('#project-list-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 }); 
	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
 	
 	$('#projectmilestone_document_information').click(function(){
		 var projectMileStoneId=Number($('#projectMileStoneId').val());	
			if(projectMileStoneId>0){
				AIOS_Uploader.openFileUploader("doc","projectMileStoneDocuments","ProjectMileStone",projectMileStoneId,"ProjectMileStoneDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","projectMileStoneDocuments","ProjectMileStone","-1","ProjectMileStoneDocuments");
			}
		});
 	
 	if(Number($('#projectId').val()> 0)){    
 	 	$('#status').val($('#tempstatus').val());
		populateUploadsPane("doc","projectMileStoneDocuments","ProjectMileStone", $('#projectMileStoneId').val()); 
		$('.rowid').each(function(){
	 		var rowId = getRowId($(this).attr('id'));
	 		$('#resourceType_'+rowId).val($('#tempresourceType_'+rowId).val());
	 		var resourceType = Number($('#resourceType_'+rowId).val());
			$('#resourceDetail_'+rowId).val(''); 
			$('#resourceCount_'+rowId).attr('readOnly', false);
			if(resourceType == 2){
				 $('#resourceDetail_'+rowId).hide();
				 $('#resourceMaterial_'+rowId).hide();
				 $('#resourceLabour_'+rowId).show();
			 }else{
				 $('#resourceDetail_'+rowId).show();
				 $('#resourceMaterial_'+rowId).show();
				 $('#resourceLabour_'+rowId).hide();
			 }
	 		$('#resourceDetail_'+rowId).val($('#tempresourceDetail_'+rowId).val());
	 	});	  
	}
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId) {
	var paymentReference = $('#paymentReference_' + rowId).val();
	var paymentDate = $('#paymentDate_' + rowId).val();
	var paymentAmount = $('#paymentAmount_' + rowId).val(); 
	var nexttab = $('#fieldrow_' + rowId).next();
	if (paymentReference != null && paymentReference != "" && paymentDate != null
			&& paymentDate != "" && paymentAmount != null && paymentAmount != "" 
			&& $(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
	}
	return false;
}
function projectPopupResult(projectObj) {
	$('#projectId').val(projectObj.projectId);
	$('#projectTitle').val(projectObj.projectTitle); 
	$('#common-popup').dialog("close");
} 
function customRange(dates) {
	if (this.id == 'startDate') {
		$('#endDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#startDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}

function customDelvieryRange(dates) {
	if (this.id == 'endDate') {
		$('#deliveryDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#endDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}

function projectMileStoneDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_project_milestone.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}

</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Project
			milestone
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.generalinfo" />
		</div>
		<form name="projectMileStoneValidation" id="projectMileStoneValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="projectMileStoneId"
					value="${PROJECT_MILESTONE.projectMileStoneId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 240px;">
							 <div style="height: 160px;">
								<div id="projectmilestone_document_information"
									style="cursor: pointer; color: blue;">
									<u>Upload document here</u>
								</div>
								<div
									style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
									<span id="projectMileStoneDocuments"></span>
								</div>
							</div>  
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 240px;">
							<div>
								<label class="width30" for="referenceNumber"> Reference
									No.<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_MILESTONE.referenceNumber ne null && PROJECT_MILESTONE.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber" value="${PROJECT_MILESTONE.referenceNumber}"
											class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="milestoneTitle"> Milestone Title<span
									style="color: red;">*</span> </label> <input type="text" id="milestoneTitle"
									name="milestoneTitle" value="${PROJECT_MILESTONE.milestoneTitle}"
									class="validate[required] width50" />
							</div>
							<div>
								<label class="width30" for="projectTitle">Project<span
									style="color: red;">*</span> </label> <input type="hidden"
									readonly="readonly" name="projectId" id="projectId"
									value="${PROJECT_MILESTONE.project.projectId}" /> <input type="text"
									id="projectTitle" name="projectTitle" readonly="readonly"
									value="${PROJECT_MILESTONE.project.projectTitle}"
									class="validate[required] width50" /> <span class="button">
									<a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all project-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="startDate"> Start Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_MILESTONE.startDate ne null && PROJECT_MILESTONE.startDate ne ''}">
										<c:set var="startDate" value="${PROJECT_MILESTONE.startDate}" />
										<input name="startDate" type="text" readonly="readonly"
											id="startDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("startDate").toString())%>"
											class="startDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="startDate" type="text" readonly="readonly"
											id="startDate" class="startDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="endDate"> End Date<span
									style="color: red;">*</span></label>
								<c:choose>
									<c:when
										test="${PROJECT_MILESTONE.endDate ne null && PROJECT_MILESTONE.endDate ne ''}">
										<c:set var="endDate" value="${PROJECT_MILESTONE.endDate}" />
										<input name="endDate" type="text" readonly="readonly"
											id="endDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("endDate").toString())%>"
											class="endDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="endDate" type="text" readonly="readonly"
											id="endDate" class="endDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div> 
							<div>
								<label class="width30" for="estimationCost"> Estimation Cost<span
									style="color: red;">*</span> </label> <input type="text" id="estimationCost"
									name="estimationCost" value="${PROJECT_MILESTONE.estimationCost}"
									class="validate[required] width50" />
							</div> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									style="height: 60px;" class="width51">${PROJECT_MILESTONE.description}</textarea>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content class90" id="hrm">
				<fieldset>
					<legend> Receipts </legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th class="width5">Reference</th>
									<th class="width5">Receipt Date</th>
									<th style="width: 3%">Amount</th>  
									<th style="width: 7%"><fmt:message
											key="accounts.journal.label.desc" /></th>
									<th style="width: 1%;"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:if
									test="${PROJECT_MILESTONE.projectPaymentDetails ne null && fn:length(PROJECT_MILESTONE.projectPaymentDetails)>0}">
									<c:forEach var="detail"
										items="${PROJECT_MILESTONE.projectPaymentDetails}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
											 <td><input type="text" class="width96 paymentReference" value="${detail.paymentTitle}"
											id="paymentReference_${status.index+1}" /></td> 
										<td>
											<c:set var="paymentDate" value="${detail.paymentDate}" />
											<input name="paymentDate" type="text" readonly="readonly" id="paymentDate_${status.index+1}"
											value="<%=DateFormat.convertDateToString(pageContext
												.getAttribute("paymentDate").toString())%>"
											class="paymentDate validate[required] width96"></td> 
										<td><input type="text" class="width96 paymentAmount right-align" value="${detail.paymentAmount}"
											id="paymentAmount_${status.index+1}"/></td> 
										<td><input type="text" name="taskDescription" value="${detail.description}"
											id="taskDescription_${status.index+1}" />
										</td>
											<td style="width: 0.01%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}"
												style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" id="paymentDetailId_${status.index+1}" value="${detail.projectPaymentDetailId}"/></td>
										</tr>
									</c:forEach>
								</c:if>
								<c:forEach var="i"
									begin="${fn:length(PROJECT_MILESTONE.projectPaymentDetails)+1}"
									end="${fn:length(PROJECT_MILESTONE.projectPaymentDetails)+2}" step="1"
									varStatus="status1">
									<tr class="rowid" id="fieldrow_${i}">
										<td id="lineId_${i}" style="display: none;">${i}</td>
										<td><input type="text" class="width96 paymentReference"
											id="paymentReference_${i}" /></td> 
										<td><input type="text" class="width96 paymentDate" readonly="readonly"
											id="paymentDate_${i}" /></td> 
										<td><input type="text" class="width96 paymentAmount right-align"
											id="paymentAmount_${i}" /></td> 
										<td><input type="text" name="taskDescription"
											id="taskDescription_${i}" />
										</td>
										<td style="width: 0.01%;" class="opn_td" id="option_${i}">
											<a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${i}" style="display: none; cursor: pointer;"
											title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${i}" style="display: none; cursor: pointer;"
											title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${i}" style="display: none; cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${i}" style="display: none;" title="Working">
												<span class="processing"></span> </a> <input type="hidden"
											id="paymentDetailId_${i}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right"
					id="project_milestone_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="project_milestone_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>

			</div>
		</form>
	</div>
</div>