<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}" style="display: none;">${rowId}</td>
	<td style="width: 10%;display:none;"><select name="resourceType" class="width98 resourceType"
		id="resourceType_${rowId}">
			<option value="">Select</option>
			<c:forEach var="resourceType" items="${RESOURCE_TYPE}">
				<option value="${resourceType.key}">${resourceType.value}</option>
			</c:forEach>
	</select></td>
	<td><select name="resourceDetail" class="width72 resourceDetail"
		id="resourceDetail_${rowId}">
			<option value="">Select</option>
			<c:forEach var="resourceDetail" items="${RESOURCE_DETAIL}">
				<option value="${resourceDetail.lookupDetailId}">${resourceDetail.displayName}</option>
			</c:forEach>
	</select> 
	<span id="labour_${rowId}"></span>
	<span class="button float-right" id="resourceMaterial_${rowId}"
		style="height: 16px;"> <a
			style="cursor: pointer; height: 100%;"
			id="PROJECT_RESOURCE_DETAIL_${rowId}"
			class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <span
		class="button float-right" id="resourceLabour_${rowId}"
		style="height: 16px; display: none;"> <a
			style="cursor: pointer; height: 100%;" id="resourceemployee_${rowId}"
			class="btn ui-state-default ui-corner-all resource_labour_popup width100 float-right">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td style="width: 10%;display:none;"><input type="text" id="resourceCount_${rowId}"
		class="width96 resourceCount validate[optional,custom[number]]" /></td>
	<td style="width: 10%;display:none;"><input type="text" id="unitPrice_${rowId}"
		class="width96 unitPrice right-align validate[optional,custom[number]]" />
	</td>
	<td class="totalamount" id="totalamount_${rowId}"
		style="text-align: right;display:none;"></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${rowId}" />
	</td>
	<td>
		<span class="button float-right"
		id="accessRightsSpan_${rowId}"
		style="height: 16px;"> <a
		style="cursor: pointer; height: 100%;"
		id="accessRightsA_${rowId}"
			class="btn ui-state-default ui-corner-all access_rights_popup width100 float-right">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
			<input
		type="hidden" id="accessRights_${rowId}"
				value="" />	
	</td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		id="resourceId_${rowId}" />
	</td>
</tr>