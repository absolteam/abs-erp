<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"> 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 oTable = $('#ProjectPopupList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_activeprojects.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 }, {
			"mDataProp" : "projectTitle"
		 }, {
			"mDataProp" : "customerName"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 },{
			"mDataProp" : "projectType"
		 } ]
	});	  
	/* Click event handler */
	$('#ProjectPopupList tbody tr').live('click', function() {
		if ($(this).hasClass('row_selected')) {
			$(this).addClass('row_selected'); 
		} else {
			oTable.$('tr.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected'); 
		}
	});
	
	/* Click event handler */
	$('#ProjectPopupList tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this); 
		projectPopupResult(aData);  
		$('#common-popup').dialog("close");
  	});
	
	$('#project-list-popup').click(function(){  
		$('#common-popup').dialog("close");
 	}); 
	
});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Projects
		</div>
		<div class="portlet-content">
			<div id="project_popuplist">
				<table id="ProjectPopupList" class="display">
					<thead>
						<tr>
							<th>Reference Number</th>
							<th>Title</th>
							<th>Client</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Project Type</th> 
						</tr>
					</thead>

				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="project-list-popup">
					close
				</div>
			</div>
		</div>
	</div>
</div>