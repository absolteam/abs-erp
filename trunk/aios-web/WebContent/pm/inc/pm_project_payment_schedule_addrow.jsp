<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="paymentrowid" id="paymentfieldrow_${rowId}">
	<td id="paymentlineId_${rowId}" style="display: none;">${rowId}</td>
	<td>
		<select name="paymentType"
			class="width80 paymentType"
			id="paymentType_${rowId}">
				<option value="">Select</option>
				<c:forEach var="paymentType" items="${PAYMENT_TYPES}">
					<option value="${paymentType.lookupDetailId}">${paymentType.displayName}</option>
				</c:forEach>
		</select> 
		<input type="hidden"
			id="tempPaymentType_${rowId}"
			value="${detail.lookupDetail.lookupDetailId}" />
		<span style="height: 16px;" id="paymentType_${rowId}" class="button float-right"> 
		<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
		id="CASE_PAYMENT_TYPE_${i}" style="cursor: pointer; height: 100%;">
			<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
	</td>
	
	<td><input type="text" id="paymentAmount_${rowId}"
		value="${detail.amount}"
		class="width96 paymentAmount right-align validate[optional,custom[number]]" />
	</td>
	<td><input type="text" name="paymentDate" class="paymentDate" readonly="readonly"
		id="paymentDate_${rowId}" value="${detail.paymentDateView}"/></td>
	<td style="width: 5%;display:none;">
			<select name="paymentMode"
			class="width98 paymentMode"
			id="paymentMode_${rowId}">
				<option value="">Select</option>
				<c:forEach var="resourceType" items="${PAYMENT_MODE}">
					<option value="${resourceType.key}">${resourceType.value}</option>
				</c:forEach>
		</select> <input type="hidden" id="tempPaymentMode_${i}"
			value="${detail.resourceType}" />
	</td>
	<td><input type="text" name="paymentDescription"
		id="paymentDescription_${rowId}" />
	</td>
	<td style="width: 5%;display:none;">
			<select name="paymentStatus"
			class="width98 paymentStatus"
			id="paymentStatus_${rowId}">
				<option value="">Select</option>
				<c:forEach var="sts" items="${PAYMENT_STATUS}">
					<option value="${sts.key}">${sts.value}</option>
				</c:forEach>
		</select> <input type="hidden" id="tempPaymentStatus_${rowId}"
			value="${detail.resourceType}" />
	</td>
	<td style="width: 0.01%;" class="opn_td"
		id="paymentoption_${rowId}"> 
			<a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip paymentdelrow"
		id="paymentDeleteImage_${rowId}" style="cursor: pointer;"
		title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a>
			
	 <input type="hidden" id="paymentScheduleId_${rowId}"
		value="${detail.paymentScheduleId}" />
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$('.paymentDate').datepick({showTrigger: '#calImg'}); 
});
</script>