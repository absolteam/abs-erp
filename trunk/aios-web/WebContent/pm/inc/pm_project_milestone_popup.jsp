<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">
 	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	$(function() {
		oTable = $('#ProjectMileStoneListPopup').dataTable({
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "get_milestones_by_project.action?projectId="+$('#projectTempId').val(),

			"aoColumns" : [ {
				"mDataProp" : "projectTitle"
			}, {
				"mDataProp" : "referenceNumber"
			}, {
				"mDataProp" : "milestoneTitle"
			}, {
				"mDataProp" : "fromDate"
			}, {
				"mDataProp" : "toDate"
			}, {
				"mDataProp" : "currentStatus"
			} ]
		});
		/* Click event handler */
		$('#ProjectMileStoneListPopup tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
			}
		});
		
		/* Click event handler */
		$('#ProjectMileStoneListPopup tbody tr').live('dblclick', function () {  
			aData = oTable.fnGetData(this); 
			projectMileStonePopupResult(aData);  
			$('#project-milestonelist-popup').trigger('click');
	  	});
		

		$('#project-milestonelist-popup').click(function(){  
			$('#common-popup').dialog("close");
	 	}); 
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>project
			milestone
		</div>
		<div class="portlet-content">
			<div id="project_milestones">
				<table id="ProjectMileStoneListPopup" class="display">
					<thead>
						<tr>
							<th>Project</th>
							<th>Reference Number</th>
							<th>Title</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Status</th>
						</tr>
					</thead> 
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="project-milestonelist-popup">
					close
				</div>
				<input type="hidden" id="projectTempId" value="${requestScope.projectId}"/>
			</div>
		</div>
	</div>
</div>