<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="inventoryrowid" id="inventoryfieldrow_${rowId}">
	<td style="display: none;" id="lineId_${rowId}">${rowId}</td>
	<td><input type="hidden" name="productId"
		id="productid_${rowId}" /> <span id="product_${rowId}"></span> <span
		class="button float-right"> <a
			style="cursor: pointer;" id="prodID_${rowId}"
			class="btn ui-state-default ui-corner-all show_product_list_sales_order width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td id="uom_${rowId}" style="display: none;"></td>
	<td id="costingType_${rowId}" style="display: none;"></td>
	<td style="display: none;"><input type="hidden" name="storeId"
		id="storeid_${rowId}" /> <span id="store_${rowId}"></span> <span
		class="button float-right"> <a
			style="cursor: pointer;" id="storeID_${rowId}"
			class="btn ui-state-default ui-corner-all common-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td><input type="text" name="productQty"
		id="productQty_${rowId}" class="productQty validate[optional,custom[number]] width80"> <input
		type="hidden" name="totalAvailQty" id="totalAvailQty_${rowId}" />
	</td>
	<td><input type="text" name="inventoryamount" id="inventoryamount_${rowId}"
		style="text-align: right;"
		class="inventoryamount width98 validate[optional,custom[number]] right-align">
	</td>
	<td>
		<div class="width80 float-left">
			<input type="radio" name="inventoryDiscountType_${rowId}"
			id="inventoryDiscountTypeFixed_${rowId}" value="false"
			class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
		</div>
		<div class="width80 float-left">
			<input type="radio" name="inventoryDiscountType_${rowId}"
			id="inventoryDiscountTypePercentage_${rowId}" value="true"
			class="inventoryDiscountType width10 float-left">
			<label class="width65 float-left">Percentage</label>
		</div>
	</td>
	<td><input type="text" name="inventoryDiscount"
		id="inventoryDiscount_${rowId}" value=""
		style="text-align: right;"
		class="inventoryDiscount width98 validate[optional,custom[number]] right-align">
	</td>
	<td><span id="inventorytotalAmount_${rowId}" style="float: right;"></span>
	</td>
	<td><input type="text" name="inventoryDescription"
		id="inventoryDescription_${rowId}" class="width98" maxlength="150">
	</td>
	<td style="width: 1%;" class="opn_td" id="inventoryoption_${rowId}">
		 <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="inventoryDeleteImage_${rowId}"
		style="cursor: pointer; display: none;"
		title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <input
		type="hidden" name="projectInventoryId"
		id="projectInventoryId_${rowId}" />
		<input
		type="hidden" name="projectInventoryStatus"
		id="projectInventoryStatus_${rowId}"
		value="" />
		<input
		type="hidden" name="projectInventoryStatusName"
		id="projectInventoryStatusName_${rowId}"
		value="" />
	</td>
</tr>
<script type="text/javascript">
$(function(){
});
</script>