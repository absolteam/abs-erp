<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript">
var slidetab = "";
var accessCode = "";
var sectionRowId = 0;
var projectResources = [];
var projectExpenses =[];
var projectInventories =[];
var accessRightRowId =null;
var getProjectResources = null;
var getProjectExpenses = null;
var getInventoryDetails = null;

$(function(){
	
	$jquery("#projectValidation").validationEngine('attach'); 
	
	$('#tabs').tabs({select: function(event, ui){
	     // Do stuff here
        $("#tabs").tabs().css({
		   'min-height': '500px',
		   'overflow': 'auto'
		});
                	
	  }});
	
	var hiddenAccessRightsValue=$('#accessRightsHidden').val();
	$('.specialTab').each(function(){   
		$(this).hide();
	 });  
	if(hiddenAccessRightsValue!=null && hiddenAccessRightsValue!=''){
		var listTemp=[];
		listTemp=hiddenAccessRightsValue.split(",");
		 
		$('.specialTab').each(function(){   
			for(var i=0;i<=listTemp.length;i++){
				  if((listTemp[i]+"Tab")==$(this).attr('id')){
					  $(this).show();
				  }
			  }
		 });   
	}
	
	$('#startDate,#endDate').datepick({
		 onSelect: customRange, showTrigger: '#calImg'}); 
	
	$('#actualStartDate,#actualEndDate').datepick({
		 onSelect: customRangeActual, showTrigger: '#calImg'}); 
	
	$('.expenseDate').datepick({onSelect: function(){
		 var rId=getRowId($(this).attr('id')); 
			expensetriggerAddRow(rId);
		}, showTrigger: '#calImg'}); 
	
	
	 $('#project_discard').click(function(event){  
		 projectDiscard("");
		 return false;
	 });
	 
	
	 
	 
	 manupulateLastRow();

	 $('#project_save').click(function(){ 
		 projectTaskSave("SAVE");
	 }); 
	 
	 $('#project_save_and_update').click(function(){ 
		 projectTaskSave("UPDATE");
	 }); 
	 
	 $('#send_requisition').click(function(){ 
		 projectTaskSave("REQUISITION");
		 
	 }); 
	 
	 getProjectResources = function(){
		 projectResources = [];
		 $('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var resourceType = Number($('#resourceType_'+rowId).val());  
			var resourceDetail = Number(0);
			var personData  = [];
			if(resourceType == 2){
				$('.personObject_'+rowId).each(function(){ 
					var prrowId = getRowId($(this).attr('id'));  
					personData.push({
						personId : $('#personId_'+prrowId).val()
					});
				});
			}else{
				resourceDetail = Number($('#resourceDetail_'+rowId).val()); 
			}
			var resourceCount = Number($('#resourceCount_'+rowId).val());  
			var unitPrice = Number($('#unitPrice_'+rowId).val());  
			var linesDescription = $('#linedescription_'+rowId).val();  
			var resourceId =  Number($('#resourceId_'+rowId).val()); 
			var accessRights =  $('#accessRights_'+rowId).val(); 
			if(typeof resourceType != 'undefined' && resourceType > 0 && resourceCount > 0){
				var jsonData = [];
				jsonData.push({
					"resourceType" : resourceType,
					"resourceCount" : resourceCount,
					"description": linesDescription,
					"resourceId": resourceId,
					"personData" : personData,
					"resourceDetail" : resourceDetail,
					"unitPrice" : unitPrice,
					"resourceId" : resourceId,
					"accessRights" : accessRights,
				});   
				projectResources.push({
					"projectResources" : jsonData
				});
			} 
		});  
		return projectResources;
	 };
	 
	 getProjectExpenses = function(){
		 projectExpenses = [];
		 $('.expenserowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var projectExpenseId = Number($('#projectExpenseId_'+rowId).val());  
			var expenseType = Number($('#expenseType_'+rowId+' option:selected').val());
			var expenseMode = Number($('#expenseMode_'+rowId+' option:selected').val());  
			var expenseAmount = $('#expenseAmount_'+rowId).val(); 
			var expenseDate = $('#expenseDate_'+rowId).val(); 
			var costToClient = $('#costToClient_'+rowId).val();  
			var expenseReference = $('#expenseReference_'+rowId).val();  
			var description = $('#expenseDescription_'+rowId).val();  
			if(typeof expenseAmount != 'undefined' && expenseAmount !=null  && expenseAmount!='' && expenseDate !=null  && expenseDate!=''){
				var jsonData = [];
				jsonData.push({
					"projectExpenseId" : projectExpenseId,
					"expenseType" : expenseType,
					"expenseMode" : expenseMode,
					"expenseAmount": expenseAmount,
					"expenseDate" : expenseDate,
					"costToClient" : costToClient,
					"expenseReference" : expenseReference,
					"description" : description
				});   
				projectExpenses.push({
					"projectExpenses" : jsonData
				});
			} 
		});  
		return projectExpenses;
	 };
	 
	 getInventoryDetails = function(){ 
		 projectInventories = [];
	 		$('.inventoryrowid').each(function(){ 
		 		
	 			 var rowId = getRowId($(this).attr('id'));  
	 			 var productId = $('#productid_'+rowId).val();   
	 			 var quantity = $('#productQty_'+rowId).val();  
	 			 var amount = $('#inventoryamount_'+rowId).val();    
	 			 var description=$('#inventoryDescription_'+rowId).val();
	 			 var projectInventoryId = Number($('#projectInventoryId_'+rowId).val());
	 			var discount = Number($('#inventoryDiscount_'+rowId).val());
	 			 var storeId = Number($('#storeid_'+rowId).val());
	 			var projectInventoryStatus = Number($('#projectInventoryStatus_'+rowId).val());
	 			var isPercentage = $('input[name=inventoryDiscountType_'+rowId+']:checked').val();
	 			if(typeof productId != 'undefined' && productId !=null  && productId!='' && amount !=null  && amount!=''){
		 			var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"quantity" : quantity,
						"amount": amount,
						"description" : description,
						"storeId" : storeId,
						"projectInventoryId" : projectInventoryId,
						"discount":discount,
						"projectInventoryStatus":projectInventoryStatus,
						"isPercentage":isPercentage
					});   
					projectInventories.push({
						"projectInventories" : jsonData
					});
	 			}
	 		}); 
		
			return projectInventories;
		};
		
	 $('.project-type-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	     $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				  $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
	  	 return false;
	}); 
	 
	 $('.resource_detail_popup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	          $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 
	 
	 
	 $('.project-milestone-popup').click(function(){
		 var projectId=Number($("#projectId").val());
		 if(projectId>0){
			$('.ui-dialog-titlebar').remove();  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_project_milestone_list.action", 
			 	async: false,  
			 	data: {projectId: projectId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
		 }else{
			 $('#page-error').hide().html("Select Project to list the milestone details").slideDown(1000);
				$('#page-error').delay(2000).slideUp();
		 }
			return false;
		});
	 
	 $('.project-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_project_popup.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
	 
	 
	 $('.customer-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			}
		});  
		return false;
	});
	 
	
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/projectresource_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  $('#resourceType_'+id).val(2);
			 $('#resourceDetail_'+id).hide();
			 $('#resourceMaterial_'+id).hide();
			 $('#resourceLabour_'+id).show();
		  return false;
	 }); 
	 
	 $('.expenseaddrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".expensetab>.expenserowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/project_expense_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.expensetab tr:last').before(result);
					 if($(".expensetab").height()>255)
						 $(".expensetab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.expenserowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#expenselineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 $('.inventoryaddrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".inventorytab>.inventoryrowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/project_inventory_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.inventorytab tr:last').before(result);
					 if($(".inventorytab").height()>255)
						 $(".inventorytab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.inventoryrowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#inventorylineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 
	 $('.resource_labour_popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			tempid = $(this).parent().get(0);
			var rowId = getRowId($(this).attr('id'));
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_multiselect_person_list.action", 
			 	async: false,  
			 	data:{personTypes: "1", id: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
	 				 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
	 
//---------------------Delete Row section-----------------	 

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 }); 
	 
	 $('.expensedelrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
     	$('#pettyCashDiv').hide();
       	var pettyCashAmount = Number(0);
    	 $('.expenserowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#expenselineId_'+rowId).html(i);
    		 var expenseMode = Number($('#expenseMode_'+rowId).val());
			 if(expenseMode == 2){
				pettyCashAmount += Number($('#expenseAmount_'+rowId).val());
				$('#pettyCashDiv').show();
			 }
			 i=i+1; 
 		 }); 
    	 $('#generalExpenseAmount').text(pettyCashAmount);
	 }); 
	 
	 $('.inventorydelrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
     	 $(slidetab).remove();  
     	 var i=1;
	   	 $('.inventoryrowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#inventorylineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});
//---------------------Change Trigger section-----------------
	 
	 $('.resourceDetail').live('change',function(){
		 var rowId = getRowId($(this).attr('id'));
		 triggerAddRow(rowId);
		 return false;
	 });
	 
	
	 $('.resourceCount,.unitPrice').live('change',function(){
		 var rowId = getRowId($(this).attr('id'));
		 var count = Number($('#resourceCount_'+rowId).val());  
		 var unitRate = Number($('#unitPrice_'+rowId).val());  
		 if(count > 0 && unitRate > 0){
			$('#totalamount_'+rowId).text(Number(count * unitRate).toFixed(2));
		 }else{
			$('#totalamount_'+rowId).text('');
		 } 
		 triggerAddRow(rowId);
		 return false;
	 });

	
	 
	 $('.resourceType').live('change',function(){  
		 var rowId = getRowId($(this).attr('id'));
		 var resourceType = Number($('#resourceType_'+rowId).val());
		 $('#resourceDetail_'+rowId).val(''); 
		 if(resourceType == 2){ 
			 $('#resourceDetail_'+rowId).hide();
			 $('#resourceMaterial_'+rowId).hide();
			 $('#resourceLabour_'+rowId).show();
		 }else{
			 $('#resourceDetail_'+rowId).show();
			 $('#resourceMaterial_'+rowId).show();
			 $('#resourceLabour_'+rowId).hide();
		 }
		 triggerAddRow(rowId);
		 return false;
	 });
	 
	 $('.expenseAmount').live('change',function(){  
		 var rowId = getRowId($(this).attr('id'));
		 var pettyCashAmount = Number(0);
		 $('.expenserowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var expenseMode = Number($('#expenseMode_'+rowId).val());
			if(expenseMode == 2){
				pettyCashAmount += Number($('#expenseAmount_'+rowId).val());
				$('#pettyCashDiv').show();
			}
		 });  
		 $('#generalExpenseAmount').text(pettyCashAmount);
		 expensetriggerAddRow(rowId);
		 return false;
	 });
	 
	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#customer-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 
	 
	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="PROJECT_TYPE"){
			$('#projectType').html("");
			$('#projectType').append("<option value=''>Select</option>");
			loadLookupList("projectType"); 
		}else if(accessCode=="PROJECT_RESOURCE_DETAIL") {
			$('#resourceDetail_'+sectionRowId).html("");
			$('#resourceDetail_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("resourceDetail_"+sectionRowId);  
		}
	}); 
	
 	
	
	$('.access_rights_popup').live('click',function(){  
		accessRightRowId = getRowId($(this).attr('id'));
		var accessRights=$('#accessRights_'+accessRightRowId).val();
 	   if(accessRights!=null && accessRights!=""){ 
 		   var arrayList=[];
 		  arrayList=accessRights.split(",");
 		 $('.accessRightsCheckBox').each(function(){
			 	$(this).attr('checked',false);
	 	   });
 		  var shortedArrayList=[];
 		  $('.accessRightsCheckBox').each(function(){
 			  for(var i=0;i<=arrayList.length;i++){
 				  if(arrayList[i]==$(this).attr('id')){
 				 		shortedArrayList.push(arrayList[i]);
 				  }
 			  }
	 	   });
 		 $(shortedArrayList)
			.each(function(index){
				$('#'+shortedArrayList[index]).attr('checked','checked');
	 	   });
 	   }else{
 		  $('.accessRightsCheckBox').each(function(){
 			 	$(this).attr('checked','checked');
 	 	   });
 	   } 
 	   
 	  $('.ui-dialog-titlebar').remove();    
	      $('#accessrights-popup').dialog('open');
	      $($($('#accessrights-popup').parent()).get(0)).css('top',0);
	});
		
	$('#accessrights-popup').dialog({
			autoOpen : false,
			width : 500,
			height : 300,
			bgiframe : true,
			modal : true,
			buttons : {
				"Close" : function() {
					$(this).dialog("close");
				},"Confirm" : function() {
					var listString="";
					 $('.accessRightsCheckBox').each(function(){
						 if($(this).attr('checked'))
							 listString+=$(this).attr('id')+",";
				 	   });
					$('#accessRights_'+accessRightRowId).val(listString);
					$(this).dialog("close");
				},
			}
	});
	
	{
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_sales_product_common_popup_preload.action", 
		 	async: false,  
		 	data: {itemType: "I"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('#sales-product-inline-popup').html(result);  
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});
	}

	$('.show_product_list_sales_order').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		globalRowId = Number($(this).attr('id').split("_")[1]);  
		$('#sales-product-inline-popup').dialog('open'); 
		return false;
	});
	
	$('#sales-product-inline-popup').dialog({
		autoOpen : false,
		width : 800,
		height : 600,
		bgiframe : true,
		modal : true,
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			}
		}
	});
	 
		$('.show_product_list_sales_order').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_product_stock_popup.action", 
			 	async: false,  
			 	data: {itemType: "E", rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
 	
 	$('.deleteoption').live('click',
			function() {
		var rowId =  $(this).attr('id').split("_")[1];
		var divId = $(this).parent('div').attr('id'); 
		var parentRowId = $("#"+divId).parent('span').attr('id');
			$('#div_'+rowId).remove(); 
			var i = 0; 
		$('.count_index')
		.each(
				function() { 
					i = i +1;
			$(this).text(i+".");		
		});  
		resourceLabourCount(getRowId(parentRowId), i);
		return false;
	});
 	
 	
 	
 	$('#project_document_information').click(function(){
		 var projectTaskId=Number($('#projectTaskId').val());	
			if(projectTaskId>0){
				AIOS_Uploader.openFileUploader("doc","projectTaskDocs","ProjectTask",projectTaskId,"TaskDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","projectTaskDocs","ProjectTask","-1","TaskDocuments");
			}
		});
 	
 	if(projectTaskId>0){
 		discussionList();
 		notesList();
 	}
 	
 	 $('#discussion_message_save').click(function(event){  
		var discussion = $('#discussions').val(); 
		 if(discussion!=null && discussion!=''){ 
			 	var projectTaskId = Number($('#projectTaskId').val());
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_project_task_discussion.action", 
				 	async: false, 
				 	data:{	projectTaskId: projectTaskId, message:discussion,
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						 if(($.trim(response.returnMessage)=="SUCCESS")){
							 $('#discussions').val("");
							 discussionList();

						 }else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				}); 
			}else{
				return false;
			}
	 });
 	 
 	$('.expenseMode').live('change',function(){  
		$('#pettyCashDiv').hide();
		var pettyCashAmount = Number(0);
		$('.expenserowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var expenseMode = Number($('#expenseMode_'+rowId).val());
			if(expenseMode == 2){
				pettyCashAmount += Number($('#expenseAmount_'+rowId).val());
				$('#pettyCashDiv').show();
			}
		}); 
		$('#generalExpenseAmount').text(pettyCashAmount);
	});
 	 
 	$('#notes_save').click(function(event){  
		var notes = $('#notes').val(); 
		 if(notes!=null && notes!=''){ 
			 	var projectTaskId = Number($('#projectTaskId').val());
			 	var isPublic = $('#public_notes').attr("checked");
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_project_task_notes.action", 
				 	async: false, 
				 	data:{	projectTaskId: projectTaskId, message:notes,isPublic:isPublic
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						 if(($.trim(response.returnMessage)=="SUCCESS")){
							 $('#notes').val("");
							 notesList();

						 }else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				}); 
			}else{
				return false;
			}
	 });
 	$('.supplierquotation-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result);  
			}
		});  
		 return false;
	});
 	
 	$('.project-task-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_common_all_project_task.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			}
		});  
		return false;
	});
 	
 	$('#add_delivery').click(function(){  
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		var projectDeliveryId = Number(0);
		var projectTaskId = Number($('#projectTaskId').val());

		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_delivery_entry.action", 
	     	async: false, 
	     	data:{projectDeliveryId: projectDeliveryId,projectTaskId:projectTaskId,projectId:0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
 	
 	$('.update_delivery').click(function(){   
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		var rowId = getRowId($(this).attr('id'));
		var projectDeliveryId = Number($('#projectDeliveryId_'+rowId).val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_delivery_entry.action", 
	     	async: false, 
	     	data:{projectDeliveryId: projectDeliveryId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
 	
 	$('#pettycashvoucher-close').live('click',function(){
		$('#common-popup').dialog('close');  
		return false;
	});
	
	$('.pettycash_popup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 var pettyCashType	="2@@AIPC";
	     $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/show_all_settlement_voucher.action",
             async: false,
             data:{pettyCashType: pettyCashType},
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				  $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
	  	 return false;
	}); 
 	
 	
 	$('#add_task').click(function(){   
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
		var projectTaskId = Number($('#projectTaskId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_task_entry.action", 
	     	async: false, 
	     	data:{parentProjectTaskId: projectTaskId,projectTaskId:Number(0),projectId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
 	
 	$('.update_task').click(function(){   
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		var rowId = getRowId($(this).attr('id'));
		var projectTaskId = Number($('#projectTaskId_'+rowId).val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_task_entry.action", 
	     	async: false, 
	     	data:{projectTaskId: projectTaskId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});
 	
 	$('.TDRequsitionTable').click(function(){  
 		var rowId = getRowId($(this).attr('id'));
 		
 		$('#RequsitionTable_'+rowId).toggle();
 	}); 
 	
 	$('.productQty').change(function(){ 
 		$('.quantityerr').remove(); 
 	 	 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#inventoryamount_'+lineID).val());
 	 	 var discount = Number($('#inventoryDiscount_'+lineID).val());
 	 	 var availQty = Number($('#totalAvailQty_'+lineID).val());
 	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
 	 	
 		 if(productQty >0 && amount > 0){ 
 	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
 	 	 }
 		triggerAddRow(lineID);
 	 	  
 		 return false;
 	 });

 	$('.inventoryamount').change(function(){ 
 		
 		 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#inventoryamount_'+lineID).val());
 	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
 	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
 	 	 if(productQty >0 && amount > 0){
 	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
 	 	 }
 	 	 
 	 	
 		return false;
 	 });
 	
 	$('.inventoryDiscountType').live('change',function(){ 
	 	$('.inventoryDiscount').trigger('change');
		return false;
	 });
 	
 	$('.inventoryDiscount').live('change',function(){ 
 		$('.discount-warning').remove(); 
		 var lineID = getRowId($(this).attr('id'));
	 	 var productQty =  Number($('#productQty_'+lineID).val());
	 	 var amount = Number($('#inventoryamount_'+lineID).val());
	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
	 	var suggestedPrice = Number($('#suggestedRetailPrice_'+lineID).val());
	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
	 	 if(productQty >0 && amount > 0){
	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
	 	 }
	 	 
	 	suggestedPrice=suggestedPrice*productQty;
	 	 var inventoryTotal=Number($('#inventorytotalAmount_'+lineID).text());
	 	 if(inventoryTotal<suggestedPrice){
	 		$(this).parent().append("<span class='discount-warning warning-inline' id=discount-warning_"+lineID+">Warning! Maximum allowed discount is "+Number(Number(productQty) *  (amount)-suggestedPrice)+" for given "+productQty+" quantitie's</span>"); 
	 	 }
	 	resetPayment();
		return false;
	 });
 	
 	$('.productQty').live('change',function(){ 
 		
 		$('.quantityerr').remove(); 
 	 	 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#inventoryamount_'+lineID).val());
 	 	 var availQty = Number($('#totalAvailQty_'+lineID).val());
 	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
 	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
 	 	
 		 if(productQty >0 && amount > 0){ 
 	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
 	 	 }
 		triggerAddRow(lineID);
 	 	 	 
 	 	
 		 return false;
 	 });

 	$('.inventoryamount').live('change',function(){ 
 		 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#inventoryamount_'+lineID).val());
 	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
 	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
 	 	 if(productQty >0 && amount > 0){
 	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
 	 	 }
 	 	 
 	 	
 		return false;
 	 });
 	
 	if(Number($('#projectTaskId').val()> 0)){    
 	 	$('#projectStatus').val($('#tempProjectStatus').val());
 	 	$('#priority').val($('#tempPriority').val());
 	 	
		populateUploadsPane("doc","projectTaskDocs","ProjectTask", $('#projectTaskId').val()); 
		$('.rowid').each(function(){
	 		var rowId = getRowId($(this).attr('id'));
	 		$('#resourceType_'+rowId).val($('#tempresourceType_'+rowId).val());
	 		var resourceType = Number($('#resourceType_'+rowId).val());
			$('#resourceDetail_'+rowId).val(''); 
			$('#resourceCount_'+rowId).attr('readOnly', false);
			if(resourceType == 2){
				 $('#resourceDetail_'+rowId).hide();
				 $('#resourceMaterial_'+rowId).hide();
				 $('#resourceLabour_'+rowId).show();
				 $('#resourceCount_'+rowId).attr('readOnly', true);
			 }else{
				 $('#resourceDetail_'+rowId).show();
				 $('#resourceMaterial_'+rowId).show();
				 $('#resourceLabour_'+rowId).hide();
			 }
	 		$('#resourceDetail_'+rowId).val($('#tempresourceDetail_'+rowId).val());
	 	});	 
		
		{
			var expenseTotalAmount = Number(0);
			$('.expenserowid').each(function(){
				var rowId = getRowId($(this).attr('id'));
				$('#expenseType_'+rowId).val($('#tempExpenseType_'+rowId).val());
				$('#expenseMode_'+rowId).val($('#tempExpenseMode_'+rowId).val());
 				var expenseMode = Number($('#expenseMode_'+rowId).val());
				if(expenseMode == 2){
					expenseTotalAmount += Number($('#expenseAmount_'+rowId).val());
					$('#pettyCashDiv').show();
				}
			});
			
			$('#generalExpenseAmount').text(expenseTotalAmount);
		}
		
		
		$('.inventoryrowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var status=$('#projectInventoryStatusName_'+rowId).val();
			if(status=='Completed')
				$(this).hide();
			
		});
		
	}
});

function projectTaskSave(saveNupdate){
	if($jquery("#projectValidation").validationEngine('validate')){ 
			var referenceNumber = $('#referenceNumber').val(); 
			var projectTaskId = Number($('#projectTaskId').val()); 
			var startDate = $('#startDate').val(); 
			var taskTitle = $('#taskTitle').val(); 
			var endDate = $('#endDate').val();  
			var description=$('#description').val();  
			var projectId = Number($('#projectId').val()); 
			var projectMileStoneId = Number($('#projectMileStoneId').val()); 
			var parentProjectTaskId = Number($('#parentProjectTaskId').val()); 
			var status = Number($('#projectStatus').val()); 
			var supplierId = Number($('#supplierId').val()); 
			var priority = Number($('#priority').val());
			var alertId = Number($('#alertId').val());
			var pettyCashId = Number($('#pettcash_accountid').val());
			var pettyCashAmount = Number(0);
			var generalExpenseAmount = Number(0);
			if(pettyCashId > 0){
				 pettyCashAmount = Number($.trim($('#pettyCashAmount').text()));
				 generalExpenseAmount = Number($.trim($('#generalExpenseAmount').text())); 
			}
			projectResources = getProjectResources();
			projectExpenses=getProjectExpenses();
			projectInventories=getInventoryDetails(); 
			if(pettyCashAmount >= generalExpenseAmount){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_project_task.action", 
				 	async: false, 
				 	data:{	projectTaskId: projectTaskId, referenceNumber: referenceNumber, taskTitle: taskTitle, startDate: startDate, endDate: endDate,
				 			 description: description, projectResources: JSON.stringify(projectResources), alertId:alertId, pettyCashId: pettyCashId,
				 			status:status,projectId:projectId,projectMileStoneId:projectMileStoneId,parentProjectTaskId:parentProjectTaskId,
				 			supplierId:supplierId,priority:priority,saveType:saveNupdate,projectExpenses:JSON.stringify(projectExpenses),
				 			projectInventories:JSON.stringify(projectInventories)
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						 if(($.trim(response.returnMessage)=="SUCCESS")) 
							 callProject(response.projectTaskId,projectTaskId > 0 ? "Record updated.":"Record created.");
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 };
						 
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				}); 
		  } else{
			 $('#page-error').hide().html("General expense should be lesser than or equal to pettycash amount.").slideDown(1000);
			 $('#page-error').delay(2000).slideUp();
			 return false;
			}
	}else{
		return false;
	}
}

function callProject(projectTaskId,message){
	$('#common-popup').dialog('destroy');
	$('#common-popup').remove(); 
	$('#accessrights-popup').dialog('destroy');
	$('#accessrights-popup').remove();
	$('#baseprice-popup-dialog').dialog('destroy');		
	$('#baseprice-popup-dialog').remove(); 
	$('#sales-product-inline-popup').dialog('destroy');
	$('#sales-product-inline-popup').remove();
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/project_task_entry.action", 
     	async: false, 
     	data:{projectTaskId: projectTaskId},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#main-wrapper").html(result);
		} 		
	}); 
	$('#page-success').hide().html(message).slideDown(1000);
	$('#page-success').delay(2000).slideUp();
	return false;
}
function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
	$('.rowid').each(function() {
		var id = getRowId($(this).attr('id'));
		$('#resourceType_'+id).val(2);
		 $('#resourceDetail_'+id).hide();
		 $('#resourceMaterial_'+id).hide();
		 $('#resourceLabour_'+id).show();
	});
	
	//Expense 
	var expensehiddenSize = 0;
	$($(".expensetab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++expensehiddenSize;
		}
	});
	var expensetdSize = $($(".expensetab>tr:first").children()).size();
	var expenseactualSize = Number(expensetdSize - expensehiddenSize);
	$('.expensetab>tr:last').removeAttr('id');
	$('.expensetab>tr:last').removeClass('expenserowid').addClass('lastrow');
	$($('.expensetab>tr:last').children()).remove();
	for ( var i = 0; i < expenseactualSize; i++) {
		$('.expensetab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
	
	//Inventory 
	var inventoryhiddenSize = 0;
	$($(".inventorytab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++inventoryhiddenSize;
		}
	});
	var inventorytdSize = $($(".inventorytab>tr:first").children()).size();
	var inventoryactualSize = Number(inventorytdSize - inventoryhiddenSize);
	$('.inventorytab>tr:last').removeAttr('id');
	$('.inventorytab>tr:last').removeClass('inventoryrowid').addClass('lastrow');
	$($('.inventorytab>tr:last').children()).remove();
	for ( var i = 0; i < inventoryactualSize; i++) {
		$('.inventorytab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
}
function triggerAddRow(rowId) {
	var resourceType = $('#resourceType_' + rowId).val();
	var resourceDetail = $('#resourceDetail_' + rowId).val();
	var labour = $.trim($('#labour_' + rowId).html());
	var nexttab = $('#fieldrow_' + rowId).next();
	if (resourceType != null && resourceType != "" && (resourceDetail != null
			&& resourceDetail != "" || labour != null && labour != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
	}
	
	$('#resourceType_'+rowId).val(2);
	 $('#resourceDetail_'+rowId).hide();
	 $('#resourceMaterial_'+rowId).hide();
	 $('#resourceLabour_'+rowId).show();
}

function expensetriggerAddRow(rowId) {
	var amount = $('#expenseAmount_' + rowId).val();
	var expenseType = Number($('#expenseType_'+rowId+' option:selected').val());  
	var nexttab = $('#expensefieldrow_' + rowId).next();
	if (amount != null && amount != "" && (expenseType != null
			&& expenseType != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#expenseDeleteImage_' + rowId).show();
		$('.expenseaddrows').trigger('click');
	}
}
function inventorytriggerAddRow(rowId) {
	var amount = $('#inventoryamount_' + rowId).val();
	var productId=$('#productid_' + rowId).val();
	
	var nexttab = $('#inventoryfieldrow_' + rowId).next();
	if (amount != null && amount != "" && (productId != null
			&& productId != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#inventoryDeleteImage_' + rowId).show();
		$('.inventoryaddrows').trigger('click');

	}
}

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#projectCoordinator').val(personname);
	$('#coordinatorId').val(personid); 
	$('#common-popup').dialog("close");
}
function commonCustomerPopup(customerId, customerName, param1, param2, param3, param4){
	$('#customerName').val(customerName);
	$('#customerId').val(customerId);
	$('#common-popup').dialog("close");
}

function projectPopupResult(projectObj) {
	$('#projectId').val(projectObj.projectId);
	$('#projectTitle').val(projectObj.projectTitle); 
	$('#common-popup').dialog("close");
	$('#task_div').hide();
} 

function mileStonePopupResult(projectObj) {
	$('#projectMileStoneId').val(projectObj.projectMileStoneId);
	$('#projectMileStoneTitle').val(projectObj.mileStoneTitle); 
	$('#common-popup').dialog("close");
} 

 
/* function commonProductPopup(aData, rowId) {
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.productName);
	$('#store_' + rowId).html(aData.storeName);
	$('#totalAvailQty_' + rowId).val(aData.availableQuantity);
	$('#productQty_' + rowId).val(1);
	$('#inventoryamount_' + rowId).val(aData.sellingPrice); 
	$('#storeid_' + rowId).val(aData.shelfId);
	$('#suggestedRetailPrice_' + rowId).val(aData.standardPrice); 
	$('.inventoryamount').trigger('change');
	inventorytriggerAddRow(rowId);
} */

function commonProductPopup(rowId, aData) {
	if(rowId==null || row==0)
		rowId=globalRowId;
	
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.code +" - "+aData.productName); 
	$('#productQty_' + rowId).val(1);
	$('#standardPrice_' + rowId).val(aData.standardPrice); 
	var customerId = Number($('#customerId').val());
	
	if(customerId > 0){
		checkCustomerSalesPrice(aData.productId, customerId, aData.sellingPrice, rowId);
	}else{
		$('#inventoryamount_' + rowId).val(aData.sellingPrice); 
		$('#inventorytotalamount_' + rowId).text(aData.sellingPrice); 
	} 
	$('#sales-product-inline-popup').dialog("close");
	inventorytriggerAddRow(rowId);
	return false;
}

function commonSupplierPopup(supplierId, supplierName, combinationId,
		accountCode, param) {
	$('#supplierId').val(supplierId);
	$('#supplierName').val(supplierName);
}



function projectTaskPopupResult(taskObj) {
	$('#parentProjectTaskId').val(taskObj.projectTaskId);
	$('#parentProjectTitle').val(taskObj.taskTitle); 
	$('#common-popup').dialog("close");
	$('#project_div').hide();
} 
function customRange(dates) {
	if (this.id == 'startDate') {
		$('#endDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#startDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}

function customRangeActual(dates) {
	if (this.id == 'actualStartDate') {
		$('#actualEndDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#actualStartDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}
function projectDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_project_task.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$('#descriptive-popup').dialog('destroy');
			$('#descriptive-popup').remove();
			$('#accessrights-popup').dialog('destroy');
			$('#accessrights-popup').remove();
			$('#baseprice-popup-dialog').dialog('destroy');		
			$('#baseprice-popup-dialog').remove(); 
			$('#sales-product-inline-popup').dialog('destroy');
			$('#sales-product-inline-popup').remove();
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	function selectedPersonList(jsonData, rowId) {
		personObject = [];
		personObject = {
			"title" : "Employees",
			"record" : jsonData
		};
		var htmlString = "";
		var count = Number(0);
		$(personObject.record)
				.each(
						function(index) {
							count += 1;
							htmlString += "<div id=div_"+rowId+""+personObject.record[index].personId+""+count+" class='width100 float-left personObject personObject_"+rowId+"' style='padding: 3px;' >"
									+ "<span id='countindex_"+rowId+""+personObject.record[index].personId+""+count+"' class='float-left count_index' style='margin: 0 5px;'>"
									+ count
									+ ".</span>"
									+ "<span class='width40 float-left'>"
									+ personObject.record[index].personName
									+ "</span> <input type='hidden' id='personId_"+rowId+""+personObject.record[index].personId+""+count+"' value='"+personObject.record[index].personId+"'/>"
									+ "<span class='deleteoption' id='deleteoption_"+rowId+""+personObject.record[index].personId+""+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
						});
		$('#labour_' + rowId).html(htmlString); 
		resourceLabourCount(rowId, personObject.record.length);
	}
	function resourceLabourCount(rowId, count){  
		$('#resourceCount_' + rowId).val(count);
		$('#resourceCount_' + rowId).attr('readOnly', true);
 		var unitRate = Number($('#unitPrice_'+rowId).val());
		if(count > 0 && unitRate > 0){
			$('#totalamount_'+rowId).text(Number(count * unitRate).toFixed(2));
		}else{
			$('#totalamount_'+rowId).text('');
		}
		
		triggerAddRow(rowId);
		
		$('#resourceType_'+rowId).val(2);
		 $('#resourceDetail_'+rowId).hide();
		 $('#resourceMaterial_'+rowId).hide();
		 $('#resourceLabour_'+rowId).show();
		return false;
	}
	
	//Generate Discussion list
	function discussionList(){
		var projectTaskId = Number($('#projectTaskId').val());
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getAll_project_task_discussions.action",
					data : {
						projectTaskId : projectTaskId
					},
					async : false,
					dataType : "html",
					cache : false,
					success : function(response) {
						$("#discussion_list").html(response);
					}
				});
	}
	
	//Generate Notes list
	function notesList(){
		var projectTaskId = Number($('#projectTaskId').val());
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getAll_project_task_notes.action",
					data : {
						projectTaskId : projectTaskId
					},
					async : false,
					dataType : "html",
					cache : false,
					success : function(response) {
						$("#notes_list").html(response);
					}
				});
	}
	
	function commonPettyCashDetail(pettyCashId, param1, param2, cashOut, voucherNumber, param4, param5){
 		$('#pettcash_accountid').val(pettyCashId); 
		$('#pettcash_account').val(voucherNumber); 
		$('#pettycashvoucher-close').trigger('click');
		getMaximumPettyCash(pettyCashId);
		return false;
	 }
	function getMaximumPettyCash(settlementVoucherId) {   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_maximum_pettycash.action", 
		 	async: false,  
		 	data : {settlementVoucherId: settlementVoucherId}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#pettyCashAmount').text($.trim(result));
			} 
		}); 
		return false;
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Task
		</div>
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div id="page-success"
					class="response-msg success ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<form name="projectValidation" id="projectValidation"
			style="position: relative;">
				<input type="hidden" id="projectTaskId"
					value="${PROJECT_TASK_INFO.projectTaskId}" />
					<input type="hidden" id="alertId"
					value="${PROJECT_TASK_INFO.alertId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-left width48">
						<fieldset style="min-height: 200px;">
							<div>
								<label class="width30" for="referenceNumber"> Reference
									No.<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_TASK_INFO.referenceNumber ne null && PROJECT_TASK_INFO.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${PROJECT_TASK_INFO.referenceNumber}"
											class="validate[required] width60" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width60" />
									</c:otherwise>
								</c:choose>
							</div>
							<c:choose>
								<c:when test="${PROJECT_TASK_INFO.projectTask.taskTitle}">
									<div style="display:none;" id="project_div">
										<label class="width30" for="project">
											${sessionScope.project_projectTitle}
										</label> <input type="text" readonly="readonly"
											name="projectTitle" id="projectTitle"
											class="width60"
											value="${PROJECT_TASK_INFO.project.projectTitle}" />
										<input type="hidden" readonly="readonly" name="projectId"
											id="projectId"
											value="${PROJECT_TASK_INFO.project.projectId}" /> 
											<c:if test="${PROJECT_TASK_INFO.projectTask eq null}">
												<span
												class="button"> <a style="cursor: pointer;" id="person"
												class="btn ui-state-default ui-corner-all project-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</c:if>
									</div>
								</c:when> 
								<c:otherwise>
									<div>
										<label class="width30" for="project">
											${sessionScope.project_projectTitle}
										</label> <input type="text" readonly="readonly"
											name="projectTitle" id="projectTitle"
											class="width60"
											value="${PROJECT_TASK_INFO.project.projectTitle}" />
										<input type="hidden" readonly="readonly" name="projectId"
											id="projectId"
											value="${PROJECT_TASK_INFO.project.projectId}" /> 
											<c:if test="${PROJECT_TASK_INFO.projectTask eq null}">
												<span
												class="button"> <a style="cursor: pointer;" id="person"
												class="btn ui-state-default ui-corner-all project-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</c:if>
									</div>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${PROJECT_TASK_INFO.project.projectTitle}">
									<div style="display:none;" id="task_div">
										<label class="width30" for="project">
											Parent Task
										</label> <input type="text" readonly="readonly"
											name="projectTitle" id="projectTitle"
											class="width60"
											value="${PROJECT_TASK_INFO.projectTask.taskTitle}" />
										<input type="hidden" readonly="readonly" name="parentProjectTaskId"
											id="parentProjectTaskId"
											value="${PROJECT_TASK_INFO.projectTask.projectTaskId}" /> 
											<c:if test="${PROJECT_TASK_INFO.project eq null}">
												<span
												class="button"> <a style="cursor: pointer;" id="person"
												class="btn ui-state-default ui-corner-all project-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</c:if>
									</div>
								</c:when>
								<c:otherwise>
									<div>
										<label class="width30" for="project">
											Parent Task
										</label> <input type="text" readonly="readonly"
											name="projectTitle" id="projectTitle"
											class="width60"
											value="${PROJECT_TASK_INFO.projectTask.taskTitle}" />
										<input type="hidden" readonly="readonly" name="parentProjectTaskId"
											id="parentProjectTaskId"
											value="${PROJECT_TASK_INFO.projectTask.projectTaskId}" /> 
											<c:if test="${PROJECT_TASK_INFO.project eq null}">
												<span
												class="button"> <a style="cursor: pointer;" id="person"
												class="btn ui-state-default ui-corner-all project-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</c:if>
									</div>
								</c:otherwise>
							</c:choose>
							<div>
								<label class="width30" for="taskTitle">Title<span
									style="color: red;">*</span> </label> <input type="text"
									id="taskTitle" name="taskTitle"
									value="${PROJECT_TASK_INFO.taskTitle}"
									class="validate[required] width60" />
							</div>
							<div>
								<label class="width30" for="startDate"> Start Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_TASK_INFO.fromDate ne null && PROJECT_TASK_INFO.fromDate ne ''}">
										<c:set var="startDate" value="${PROJECT_TASK_INFO.fromDate}" />
										<input name="startDate" type="text" readonly="readonly"
											id="startDate"
											value="${PROJECT_TASK_INFO.fromDate}"
											class="startDate validate[required] width60">
									</c:when>
									<c:otherwise>
										<input name="startDate" type="text" readonly="readonly"
											id="startDate" class="startDate validate[required] width60">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="endDate"> End Date<span
									style="color: red;">*</span></label>
								<c:choose>
									<c:when
										test="${PROJECT_TASK_INFO.endDate ne null && PROJECT_TASK_INFO.endDate ne ''}">
										<c:set var="endDate" value="${PROJECT_TASK_INFO.endDate}" />
										<input name="endDate" type="text" readonly="readonly"
											id="endDate"
											value="${PROJECT_TASK_INFO.toDate}"
											class="endDate width60 validate[required]">
									</c:when>
									<c:otherwise>
										<input name="endDate" type="text" readonly="readonly"
											id="endDate" class="endDate  width60 validate[required]">
									</c:otherwise>
								</c:choose>
							</div>
							
							
							
							<div>
								<label class="width30">Priority</label>
								<select name="priority"
									class="width61 priority"
									id="priority">
									<c:forEach var="priority" items="${PRIORITES}">
										<option value="${priority.key}">${priority.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempPriority" value="${PROJECT_TASK_INFO.priority}">
							</div>
							<div>
								<label class="width30">Status</label>
								<select name="projectStatus"
									class="width61 projectStatus"
									id="projectStatus">
									<c:forEach var="resourceType" items="${PROJECT_STATUS}">
										<option value="${resourceType.key}">${resourceType.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempProjectStatus" value="${PROJECT_TASK_INFO.status}">
							</div>
						</fieldset>
					</div>
					<div class="float-left  width48">
						<fieldset style="min-height: 200px;">
							
							<%-- <div>
								<label class="width30" for="project">
									Parent Task
								</label> <input type="text" readonly="readonly"
									name="parentProjectTitle" id="parentProjectTitle"
									class="width60"
									value="${PROJECT_TASK_INFO.projectTask.taskTitle}" />
								<input type="hidden" readonly="readonly" name="parentProjectTaskId"
									id="parentProjectTaskId"
									value="${PROJECT_TASK_INFO.projectTask.projectTaskId}" /> <span
									class="button"> <a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all project-task-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div> --%>
							<div>
								<label class="width30" for="projectMileStone">
									MileStone
								</label> <input type="text" readonly="readonly"
									name="projectMileStoneTitle" id="projectMileStoneTitle"
									class="width60"
									value="${PROJECT_TASK_INFO.projectMileStone.title}" />
								<input type="hidden" readonly="readonly" name="projectMileStoneId"
									id="projectMileStoneId"
									value="${PROJECT_TASK_INFO.projectMileStone.projectMileStoneId}" /> <span
									class="button"> <a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all project-milestone-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div>
							<div>
								<label class="width30" for="supplierName">
									Supplier
								</label> 
								
								<c:choose>
									<c:when
										test="${PROJECT_TASK_INFO.supplier ne null && PROJECT_TASK_INFO.supplier ne ''}">
										<input type="text" readonly="readonly"
									name="supplierName" id="supplierName"
									class="width60"
									value="${PROJECT_TASK_INFO.supplierName} [${PROJECT_TASK_INFO.supplier.supplierNumber}]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly"
									name="supplierName" id="supplierName"
									class="width60"/>
									</c:otherwise>
								</c:choose>
								<input type="hidden" readonly="readonly" name="supplierId"
									id="supplierId"
									value="${PROJECT_TASK_INFO.supplier.supplierId}" /> <span
									class="button"> <a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all supplierquotation-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div>
							<div>
								<label class="width30" for="actualStartDate">Actual Start Date </label>
								<c:choose>
									<c:when
										test="${PROJECT_TASK_INFO.actualStartDate ne null && PROJECT_TASK_INFO.actualStartDate ne ''}">
										<input name="actualStartDate" type="text" readonly="readonly"
											id="actualStartDate"
											value="${PROJECT_TASK_INFO.actualFromDate}"
											class="startDate width60">
									</c:when>
									<c:otherwise>
										<input name="actualStartDate" type="text" readonly="readonly"
											id="actualStartDate" class="actualStartDate width60">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="actualEndDate">Actual End Date</label>
								<c:choose>
									<c:when
										test="${PROJECT_TASK_INFO.actualEndDate ne null && PROJECT_TASK_INFO.actualEndDate ne ''}">
										<input name="actualEndDate" type="text" readonly="readonly"
											id="actualEndDate"
											value="${PROJECT_TASK_INFO.actualToDate}"
											class="actualEndDate width60">
									</c:when>
									<c:otherwise>
										<input name="actualEndDate" type="text" readonly="readonly"
											id="actualEndDate" class="actualEndDate width60">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									style="height: 70px;" class="width61">${PROJECT_TASK_INFO.description}</textarea>
							</div>
							
						</fieldset>
					</div>
					
				</div>
			</form>
			</div>
			<div class="clearfix"></div>
			 <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
				<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
					<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" id="resouceAccessTab"><a href="#tabs-1">Resources</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="expenseAccessTab"><a href="#tabs-8">Expense</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="inventoryAccessTab"><a href="#tabs-9">Inventory</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="docuemntAccessTab"><a href="#tabs-2">Documents</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="discussionAccessTab"><a href="#tabs-3">Discussions</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="noteAccessTab"><a href="#tabs-4">Notes</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="deliveryAccessTab"><a href="#tabs-5">Delivery</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="taskAccessTab"><a href="#tabs-6">Task</a></li>
					
				</ul>

<!--##############################################################  Tab 1 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1">
					<input type="hidden" id="accessRightsHidden" value="${PROJECT_TASK_INFO.projectAccessRights}" />
					<div class="portlet-content quotation_detail"
						style="margin-top: 10px;" id="hrm">
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="childCount" id="childCount"
									value="${fn:length(RESOURCE_INFO)}" />
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th style="width: 10%;display:none;">Resource Type</th>
											<th style="width: 25%">Resource</th>
											<th style="width: 5%;display:none;">Count</th>
											<th style="width: 5%;display:none;">Unit Rate</th>
											<th style="width: 5%;display:none;">Total Amount</th>
											<th style="width: 20%">Description</th>
											<th style="width: 0.1%; " >Access Rights</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:if
											test="${RESOURCE_INFO ne null && fn:length(RESOURCE_INFO)>0}">
											<c:forEach var="detail"
												items="${RESOURCE_INFO}" varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
													<td style="width: 10%;display:none;"><select name="resourceType"
														class="width98 resourceType"
														id="resourceType_${status.index+1}">
															<option value="">Select</option>
															<c:forEach var="resourceType" items="${RESOURCE_TYPE}">
																<option value="${resourceType.key}">${resourceType.value}</option>
															</c:forEach>
													</select> <input type="hidden" id="tempresourceType_${status.index+1}"
														value="${detail.resourceType}" /></td>
													<td><select name="resourceDetail"
														class="width72 resourceDetail"
														id="resourceDetail_${status.index+1}">
															<option value="">Select</option>
															<c:forEach var="resourceDetail" items="${RESOURCE_DETAIL}">
																<option value="${resourceDetail.lookupDetailId}">${resourceDetail.displayName}</option>
															</c:forEach>
													</select> <input type="hidden"
														id="tempresourceDetail_${status.index+1}"
														value="${detail.lookupDetail.lookupDetailId}" /> <span
														id="labour_${status.index+1}">
															<c:forEach var="rsDetail" items="${detail.projectResourceDetails}" varStatus="status3">
																<div style="padding: 3px;"
																class="width100 float-left personObject personObject_${status.index+1}"
																id="div_${status3.index+1}${rsDetail.person.personId}${status3.index+1}">
																<span style="margin: 0 5px;"
																	class="float-left count_index" id="countindex_${status3.index+1}${rsDetail.person.personId}${status3.index+1}">${status3.index+1}.</span>
																<span class="width40 float-left">${rsDetail.person.firstName} ${rsDetail.person.lastName}
																	[${rsDetail.person.personNumber}]</span> <input type="hidden" value="${rsDetail.person.personId}"
																	id="personId_${status3.index+1}${rsDetail.person.personId}${status3.index+1}"><span
																	style="cursor: pointer;" id="deleteoption_${status3.index+1}${rsDetail.person.personId}${status3.index+1}"
																	class="deleteoption"> <img width="10" height="10"
																	src="./images/cancel.png"> </span>
															</div>
															</c:forEach>
														</span> <span class="button float-right"
														id="resourceMaterial_${status.index+1}"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="PROJECT_RESOURCE_DETAIL_${status.index+1}"
															class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <span
														class="button float-right"
														id="resourceLabour_${status.index+1}"
														style="height: 16px; display: none;"> <a
															style="cursor: pointer; height: 100%;"
															id="resourceemployee_${status.index+1}"
															class="btn ui-state-default ui-corner-all resource_labour_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
													<td style="width: 10%;display:none;"><input type="text"
														id="resourceCount_${status.index+1}" value="${detail.count}"
														class="width96 resourceCount validate[optional,custom[number]]" />
													</td>
													<td style="width: 10%;display:none;"><input type="text" id="unitPrice_${status.index+1}"
														value="${detail.unitPrice}"
														class="width96 unitPrice right-align validate[optional,custom[number]]" />
													</td>
													<td class="totalamount" id="totalamount_${status.index+1}"
														style="text-align: right;display:none;">${detail.count *	detail.unitPrice}</td>
													<td><input type="text" name="linedescription"
														id="linedescription_${status.index+1}" value="${detail.description}" />
													</td>
													<td>
														<span class="button float-right"
														id="accessRightsSpan_${status.index+1}"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="accessRightsA_${status.index+1}"
															class="btn ui-state-default ui-corner-all access_rights_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															<input
														type="hidden" id="accessRights_${status.index+1}"
														value="${detail.accessRights}" />	
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status.index+1}"
														style="display: none; cursor: pointer;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status.index+1}"
														style="display: none; cursor: pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a> <input
														type="hidden" id="resourceId_${status.index+1}"
														value="${detail.projectResourceId}" /></td>
												</tr>
											</c:forEach>
										</c:if>
										<c:forEach var="i"
											begin="${fn:length(RESOURCE_INFO)+1}"
											end="${fn:length(RESOURCE_INFO)+2}" step="1">
											<tr class="rowid" id="fieldrow_${i}">
												<td id="lineId_${i}" style="display: none;">${i}</td>
												<td style="width: 10%;display:none;"><select name="resourceType"
													class="width98 resourceType" id="resourceType_${i}">
														<option value="">Select</option>
														<c:forEach var="resourceType" items="${RESOURCE_TYPE}">
															<option value="${resourceType.key}">${resourceType.value}</option>
														</c:forEach>
												</select></td>
												<td><select name="resourceDetail"
													class="width72 resourceDetail" id="resourceDetail_${i}">
														<option value="">Select</option>
														<c:forEach var="resourceDetail" items="${RESOURCE_DETAIL}">
															<option value="${resourceDetail.lookupDetailId}">${resourceDetail.displayName}</option>
														</c:forEach>
												</select> <span id="labour_${i}"></span> <span
													class="button float-right" id="resourceMaterial_${i}"
													style="height: 16px;"> <a
														style="cursor: pointer; height: 100%;"
														id="PROJECT_RESOURCE_DETAIL_${i}"
														class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <span
													class="button float-right" id="resourceLabour_${i}"
													style="height: 16px; display: none;"> <a
														style="cursor: pointer; height: 100%;"
														id="resourceemployee_${i}"
														class="btn ui-state-default ui-corner-all resource_labour_popup width100 float-right">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
												<td style="width: 10%;display:none;"><input type="text" id="resourceCount_${i}"
													class="width96 resourceCount validate[optional,custom[number]]" />
												</td>
												<td style="width: 10%;display:none;"><input type="text" id="unitPrice_${i}"
													class="width96 unitPrice right-align validate[optional,custom[number]]" />
												</td>
												<td class="totalamount" id="totalamount_${i}"
													style="text-align: right;display:none;"></td>
												<td><input type="text" name="linedescription"
													id="linedescription_${i}" />
												</td>
												<td>
														<span class="button float-right"
														id="accessRightsSpan_${i}"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="accessRightsA_${i}"
															class="btn ui-state-default ui-corner-all access_rights_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															<input
														type="hidden" id="accessRights_${i}"
														value="" />	
												</td>
												<td style="width: 0.01%;" class="opn_td" id="option_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}" style="display: none; cursor: pointer;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}" style="display: none; cursor: pointer;"
													title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}" style="display: none; cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;" title="Working">
														<span class="processing"></span> </a> <input type="hidden"
													id="resourceId_${i}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
					</div>
				</div>
					<!--##############################################################  Tab 8 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-8">
					<div class="portlet-content project_expense"
						style="margin-top: 10px;" id="hrm">
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="expensechildCount" id="expensechildCount"
									value="${fn:length(PROJECT_EXPENSE_INFO)}" />
							    <div class="float-left width100" style="display: none;" id="pettyCashDiv">
									<div class="float-left" style="width: 35%;">
										<label class="width20">Petty Cash<span 
										class="mandatory">*</span></label>
										<input type="text" class="width60" value="${PROJECT_TASK_INFO.pettyCash.pettyCashNo}"
											id="pettcash_account" readonly="readonly"/>
										<input type="hidden" id="pettcash_accountid" value="${PROJECT_TASK_INFO.pettyCash.pettyCashId}"/>
										<span style="height: 16px; position: relative; right: 40px;" id="pettyCash" class="button float-right"> 
											<a class="btn ui-state-default ui-corner-all pettycash_popup width100 float-right" 
											 style="cursor: pointer; height: 100%;">
												<span class="ui-icon ui-icon-newwin"> </span>
										</a></span>
									</div>
									<div class="float-left width30">
										<div>
											<span>Petty Cash : </span>
											<span id="pettyCashAmount">${PROJECT_TASK_INFO.pettyCashAmount}</span>
										</div>
										<div>
											<span>General Expense : </span>
											<span id="generalExpenseAmount"></span>
										</div>
									</div> 
								</div> 
								<div class="clearfix"></div>
								<table id="expensehastab" class="width100">
									<thead>
										<tr>
											<th style="width: 15%">Expense Type</th>
											<th style="width: 15%">Expense Mode</th>
											<th style="width: 5%">Amount</th>
											<th style="width: 5%">Date</th>
											<th style="width: 5%">Cost to Client</th>
											<th style="width: 10%">Reference</th>
											<th style="width: 20%">Description</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="expensetab">
										<c:if
											test="${PROJECT_EXPENSE_INFO ne null && fn:length(PROJECT_EXPENSE_INFO)>0}">
											<c:forEach var="detail"
												items="${PROJECT_EXPENSE_INFO}" varStatus="pstatus">
												<tr class="expenserowid" id="expensefieldrow_${pstatus.index+1}">
													<td id="expenselineId_${pstatus.index+1}" style="display: none;">${pstatus.index+1}</td>
													<td>
														<select name="expenseType"
															class="width72 expenseType"
															id="expenseType_${pstatus.index+1}">
																<option value="">Select</option>
																<c:forEach var="expenseType" items="${EXPENSE_TYPES}">
																	<option value="${expenseType.lookupDetailId}">${expenseType.displayName}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseType_${pstatus.index+1}"
															value="${detail.lookupDetail.lookupDetailId}" />
														<span style="height: 16px;" id="expenseType1_${pstatus.index+1}" class="button float-right"> 
														<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
														id="CASE_EXPENSE_TYPE_${pstatus.index+1}" style="cursor: pointer; height: 100%;">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
													</td>
													<td>
														<select name="expenseMode"
															class="width72 expenseMode"
															id="expenseMode_${pstatus.index+1}">
																<option value="">Select</option>
																<c:forEach var="expenseMode" items="${EXPENSE_MODES}">
																	<option value="${expenseMode.key}">${expenseMode.value}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseMode_${pstatus.index+1}"
															value="${detail.expenseMode}" /> 
													</td>
													<td><input type="text" id="expenseAmount_${pstatus.index+1}"
														value="${detail.amount}"
														class="width96 expenseAmount right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" name="expenseDate" class="expenseDate" readonly="readonly"
														id="expenseDate_${pstatus.index+1}" value="${detail.paymentDateView}"/></td>
													<td><input type="text" id="costToClient_${pstatus.index+1}"
														value="${detail.chargableAmount}"
														class="width96 costToClient right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" id="expenseReference_${pstatus.index+1}"
														value="${detail.referenceNumber}"
														class="width96 expenseReference" />
													</td>
													<td><input type="text" name="expenseDescription"
														id="expenseDescription_${pstatus.index+1}" value="${detail.description}" />
													</td>
													
													<td style="width: 0.01%;" class="opn_td"
														id="expenseoption_${pstatus.index+1}"> 
															<a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip expensedelrow"
														id="expenseDeleteImage_${pstatus.index+1}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
															
													 <input type="hidden" id="projectExpenseId_${pstatus.index+1}"
														value="${detail.projectExpenseId}" />
													</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:forEach var="i"
											begin="${fn:length(PROJECT_EXPENSE_INFO)+1}"
											end="${fn:length(PROJECT_EXPENSE_INFO)+2}" step="1">
											<tr class="expenserowid" id="expensefieldrow_${i}">
													<td id="expenselineId_${i}" style="display: none;">${i}</td>
													<td>
														<select name="expenseType"
															class="width72 expenseType"
															id="expenseType_${i}">
																<option value="">Select</option>
																<c:forEach var="expenseType" items="${EXPENSE_TYPES}">
																	<option value="${expenseType.lookupDetailId}">${expenseType.displayName}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseType_${i}"
															value="${detail.lookupDetail.lookupDetailId}" />
														<span style="height: 16px;" id="expenseType1_${i}" class="button float-right"> 
														<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
														id="CASE_EXPENSE_TYPE_${i}" style="cursor: pointer; height: 100%;">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
													</td>
													<td>
														<select name="expenseMode"
															class="width72 expenseMode"
															id="expenseMode_${i}">
																<option value="">Select</option>
																<c:forEach var="expenseMode" items="${EXPENSE_MODES}">
																	<option value="${expenseMode.key}">${expenseMode.value}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseMode_${i}"
															value="${1}" /> 
													</td>
													<td><input type="text" id="expenseAmount_${i}"
														value="${detail.amount}"
														class="width96 expenseAmount right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" name="expenseDate" class="expenseDate" readonly="readonly"
														id="expenseDate_${i}" value="${detail.paymentDateView}"/></td>
													<td><input type="text" id="costToClient_${i}"
														value=""
														class="width96 costToClient right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" id="expenseReference_${i}"
														value="${detail.referenceNumber}"
														class="width96 expenseReference" />
													</td>
													<td><input type="text" name="expenseDescription"
														id="expenseDescription_${i}" value="${detail.description}" />
													</td>
													
													<td style="width: 0.01%;" class="opn_td"
														id="expenseoption_${i}"> 
															<a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip expensedelrow"
														id="expenseDeleteImage_${i}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
															
													 <input type="hidden" id="projectExpenseId_${i}"
														value="${detail.projectExpenseId}" />
													</td>
												</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
					</div>
				</div>
				
		<!--##############################################################  Tab 9 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-9">
					<c:choose>
						<c:when	test="${REQUISITION_INFO ne null && fn:length(REQUISITION_INFO)>0}">
							<div id="hrm" class="hastable width100">
								<fieldset>
								<legend>Requisition</legend>
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th>Reference</th>
											<th>Date</th>
											<th>Details</th>
<!-- 											<th style="width: 0.01%;">Action</th>
 -->										</tr>
									</thead>
									<tbody class="requisitiontab">
										
											<c:forEach var="requistion"
												items="${REQUISITION_INFO}" varStatus="status">
												<tr>
													<td>${requistion.referenceNumber}</td>
													<td>${requistion.date}</td>
													<td id="TDRequsitionTable_${status.index+1}" class="TDRequsitionTable">
														Click here to show/hide
														<table id="RequsitionTable_${status.index+1}" style="display: none;">
															<tr>
																<th>Product</th>
																<th>Quantity</th>
																<th>Amount</th>
															</tr>
															<tbody>
																<c:forEach var="detail"
																		items="${requistion.materialRequisitionDetailVOs}" varStatus="status1">
																	<tr>
																		<td>${detail.product.code} - ${detail.product.productName}</td>
																		<td>${detail.quantity}</td>
																		<td>${(detail.projectInventory.quantity * detail.projectInventory.unitRate)-detail.projectInventory.discount}</td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</td>
													<%-- <td>
														<div
															class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
															>
															<div class="portlet-header ui-widget-header float-right update_task"
																id="updateRequisition_${status.index+1}" style="cursor: pointer;">
																Update
															</div>
															
														</div>
														<input type="hidden" id="materialRequisitionId_${status.index+1}" value="${requistion.materialRequisitionId}">
													</td> --%>
												</tr>
											</c:forEach>
									</tbody>
								</table>
								</fieldset>
							</div>
						</c:when>
					</c:choose>
						<div id="hrm" class="hastable width100">
							<fieldset>
								<legend>Assigned Items</legend>
							<div id="discount-warning"
										class="response-msg error ui-corner-all width80"
										style="display: none;"></div>
							<table id="inventoryhastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th style="width: 5%;">Quantity</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 5%;">Discount Method</th>
										<th style="width: 5%;">Discount</th>
										<th style="width: 5%;">Total</th>
										<th style="width: 10%;"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="inventorytab">
									<c:forEach var="DETAIL"
										items="${INVENTORY_DETAIL}"
										varStatus="status">
										<tr class="inventoryrowid" id="inventoryfieldrow_${status.index+1}" >
											<td style="display: none;" id="inventorylineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_product_list_sales_order width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
														
												<input type="hidden" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${status.index+1}"
												value="${DETAIL.suggestedRetailPrice}" />
											</td>
											<td id="uom_${status.index+1}" style="display: none;">${DETAIL.product.lookupDetailByProductUnit.displayName}</td>
											<td id="costingType_${status.index+1}" style="display: none;"></td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.shelf.shelfId}" /> <span
												id="store_${status.index+1}"></span>
												<span class="button float-right"> <a
													style="cursor: pointer; display: none;" id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80"> <input type="hidden"
												name="totalAvailQty" id="totalAvailQty_${status.index+1}" />
											</td>
											<td><input type="text" name="inventoryamount" 
												id="inventoryamount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="inventoryamount width98 validate[optional,custom[number]] right-align">
											</td>
											<td>
												<div class="width80 float-left">
													<c:if test="${DETAIL.isPercentage eq false}">
														<input type="radio" checked="checked" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypeFixed_${status.index+1}" value="false"
														class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
													</c:if>
													<c:if test="${DETAIL.isPercentage eq true || DETAIL.isPercentage eq null}">
														<input type="radio" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypeFixed_${status.index+1}" value="false"
														class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
													</c:if>
												</div>
												<div class="width80 float-left">
													<c:if test="${DETAIL.isPercentage eq true}">
														<input type="radio" checked="checked" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypePercentage_${status.index+1}" value="true"
														class="inventoryDiscountType width10 float-left">
														<label class="width65 float-left">Percentage</label>
													</c:if>
													<c:if test="${DETAIL.isPercentage eq false || DETAIL.isPercentage eq null}">
														<input type="radio" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypePercentage_${status.index+1}" value="true"
														class="inventoryDiscountType width10 float-left">
														<label class="width65 float-left">Percentage</label>
													</c:if>
												</div>
												<input type="hidden" id="inventoryDiscountTypeTemp_${status.index+1}" 
												value="${DETAIL.isPercentage}">
											</td>
											<td><input type="text" name="inventoryDiscount"
												id="inventoryDiscount_${status.index+1}" value="${DETAIL.discount}"
												style="text-align: right;"
												class="inventoryDiscount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><c:set var="inventorytotalAmount"
													value="${DETAIL.totalAmount}" /> <span
												id="inventorytotalAmount_${status.index+1}" style="float: right;">${inventorytotalAmount}</span>
											</td>
											<td><input type="text" name="inventoryDescription"
												id="inventoryDescription_${status.index+1}"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td"
												id="inventoryoption_${status.index+1}"> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="inventoryDeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="inventoryWorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="projectInventoryId"
												id="projectInventoryId_${status.index+1}"
												value="${DETAIL.projectInventoryId}" />
												<input
												type="hidden" name="projectInventoryStatus"
												id="projectInventoryStatus_${status.index+1}"
												value="${DETAIL.status}" />
												<input
												type="hidden" name="projectInventoryStatusName"
												id="projectInventoryStatusName_${status.index+1}"
												value="${DETAIL.statusName}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(INVENTORY_DETAIL)+1}"
										end="${fn:length(INVENTORY_DETAIL)+2}"
										step="1" varStatus="status">

										<tr class="inventoryrowid" id="inventoryfieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all show_product_list_sales_order width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
														<input type="hidden" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${i}"
												value="" />
											</td>
											<td id="uom_${i}" style="display: none;"></td>
											<td id="costingType_${i}" style="display: none;"></td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${i}" /> <span id="store_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="storeID_${i}"
													class="btn ui-state-default ui-corner-all common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${i}" class="productQty validate[optional,custom[number]] width80"> <input
												type="hidden" name="totalAvailQty" id="totalAvailQty_${i}" />
											</td>
											<td><input type="text" name="inventoryamount" id="inventoryamount_${i}"
												style="text-align: right;" 
												class="inventoryamount width98 validate[optional,custom[number]] right-align">
											</td>
											<td>
												<div class="width80 float-left">
													<input type="radio" name="inventoryDiscountType_${i}"
													id="inventoryDiscountTypeFixed_${i}" value="false"
													class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
												</div>
												<div class="width80 float-left">
													<input type="radio" name="inventoryDiscountType_${i}"
													id="inventoryDiscountTypePercentage_${i}" value="true"
													class="inventoryDiscountType width10 float-left">
													<label class="width65 float-left">Percentage</label>
												</div>
											</td>
											<td><input type="text" name="inventoryDiscount"
												id="inventoryDiscount_${i}" value=""
												style="text-align: right;"
												class="inventoryDiscount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><span id="inventorytotalAmount_${i}" style="float: right;"></span>
											</td>
											<td><input type="text" name="inventoryDescription"
												id="inventoryDescription_${i}" class="width98" maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td" id="inventoryoption_${i}">
												 <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="inventoryDeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <input
												type="hidden" name="projectInventoryId"
												id="projectInventoryId_${i}" />
												<input
												type="hidden" name="projectInventoryStatus"
												id="projectInventoryStatus_${i}"
												value="" />
												<input
												type="hidden" name="projectInventoryStatusName"
												id="projectInventoryStatusName_${i}"
												value="" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							</fieldset>
							
						</div>
						<br><br>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left ">
							<c:if test="${project_send_requisition_button eq 'show'}">
								<div class="portlet-header ui-widget-header float-right"
									id="send_requisition" style="cursor: pointer;">Send Requisition
								</div>
							</c:if>
						</div>
				</div>
				<!--##############################################################  Tab 2 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2">
					<div class="float-left  width60">
						<div>
							<div id="project_document_information"
								style="cursor: pointer; color: blue;">
								<u>Upload document here</u>
							</div>
							<div
								style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
								<span id="projectTaskDocs"></span>
							</div>
						</div>
					</div>
				</div>
				<!--##############################################################  Tab 3 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3">
					<div class=" width100">
						<div class="width35 float-left">
							<div class="width100">
									<textarea id="discussions" name="discussions"
										style="height: 200px;" class="width100"></textarea>
									
							</div>
							<div class="width100">
								<div class=" width90 portlet ui-widget ui-widget-content 
									ui-helper-clearfix ui-corner-all form-container float-right ">
										<div class="portlet-header ui-widget-header float-right task_id"
											id="discussion_message_save" style="cursor: pointer;">
											Submit
										</div>
										
									</div>
							</div>
						</div>
						<div class="width60 float-right discussion-body" id="discussion_list">
							
						</div>
					</div>
				</div>
				
				<!--##############################################################  Tab 4 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4">
					<div class="float-left  width100">
						<div class="width35 float-left">
							<div class="width100 float-left">
									<textarea id="notes" name="notes"
										style="height: 200px;" class="width100"></textarea>
							</div>
							<div class="width100 float-left">
								<input type="checkbox" class="width5 float-left" name="public_notes" id="public_notes" />
								<label class="width30 float-left" for="public_notes">Public
										</label>
										
								<div class=" width5 portlet ui-widget ui-widget-content 
									ui-helper-clearfix ui-corner-all form-container float-right ">
										<div class="portlet-header ui-widget-header float-right task_id"
											id="notes_save" style="cursor: pointer;">
											Submit
										</div>
										
								</div>
							</div>
						</div>
						<div class="width60 float-right" id="notes_list">
								
						</div>
					</div>
				</div>
		<!--##############################################################  Tab 6 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-6">
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Task Title</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Status</th>
									<th>Milestone</th>
									<th style="width: 0.01%;">Action</th>
								</tr>
							</thead>
							<tbody class="tasktab">
								<c:choose>
								<c:when
									test="${TASKS_INFO ne null && fn:length(TASKS_INFO)>0}">
									<c:forEach var="task"
										items="${TASKS_INFO}" varStatus="status">
										<tr>
											<td>${status.index+1}</td>
											<td>${task.taskTitle}</td>
											<td>${task.fromDate}</td>
											<td>${task.toDate}</td>
											<td>${task.currentStatus}</td>
											<td>${task.projectMileStone.title}</td>
											<td>
												<div
													class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
													>
													<div class="portlet-header ui-widget-header float-right update_task"
														id="updatetask_${status.index+1}" style="cursor: pointer;">
														Update
													</div>
													
												</div>
												<input type="hidden" id="projectTaskId_${status.index+1}" value="${task.projectTaskId}">
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="7">
											No Task Found
										</td>
									</tr>
								</c:otherwise>
								</c:choose>
								
							</tbody>
						</table>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left ">
							<div class="portlet-header ui-widget-header float-left add_task"
								id="add_task" style="cursor: pointer;">
								Add Task
							</div>
							
						</div>
						
					</div>
				</div>
				<!--##############################################################  Tab 5 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-5">
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Reference</th>
									<th>Title</th>
									<th>Date</th>
									<th>Status</th>
									<th style="width: 0.01%;">Action</th>
								</tr>
							</thead>
							<tbody class="deliverytab">
								<c:choose>
								<c:when
									test="${DELIVERY_INFO ne null && fn:length(DELIVERY_INFO)>0}">
									<c:forEach var="task"
										items="${DELIVERY_INFO}" varStatus="status">
										<tr>
											<td>${status.index+1}</td>
											<td>${task.referenceNumber}</td>
											<td>${task.deliveryTitle}</td>
											<td>${task.date}</td>
											<td>${task.currentStatus}</td>
											<td>
												<div
													class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
													>
													<div class="portlet-header ui-widget-header float-right update_task"
														id="updatetask_${status.index+1}" style="cursor: pointer;">
														Update
													</div>
													
												</div>
												<input type="hidden" id="projectDeliveryId_${status.index+1}" value="${task.projectDeliveryId}">
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="6">
											No Delivery Found
										</td>
									</tr>
								</c:otherwise>
								</c:choose>
								
							</tbody>
						</table>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left ">
							<div class="portlet-header ui-widget-header float-left add_delivery"
								id="add_delivery" style="cursor: pointer;">
								Add Delivery
							</div>
							
						</div>
						
					</div>
				</div>
				<div id="sales-product-inline-popup" style="display: none;"
					class="width100 float-left" id="hrm">
					
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right"
					id="project_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<c:if test="${sessionScope.project_simple_save_button eq 'show'}">
					<div class="portlet-header ui-widget-header float-right"
						id="project_save" style="cursor: pointer;">
						<fmt:message key="accounts.common.button.save" />
					</div>
				</c:if>
				<c:if test="${sessionScope.project_update_and_save_button eq 'show'}">
					<div class="portlet-header ui-widget-header float-right"
						id="project_save_and_update" style="cursor: pointer;">${sessionScope.project_saveandupdate_stock}
					</div>
				</c:if>
			</div>
			
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left expenseaddrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left inventoryaddrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			
			<div class="clearfix"></div>
			
			<div id="accessrights-popup" style="display: none;" class="width100 float-left" id="hrm">
				<div class="width100 float-left">
					<div class="width100" style="text-align: center;font-size: 30px;font-weight: bold;">Access Rights</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10 accessRightsCheckBox" type="checkbox" name="resouceAccess" id="resouceAccess"/>
							<label class="width30 ">Resource</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="expenseAccess" id="expenseAccess"/>
							<label class="width30 ">Expense</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="inventoryAccess" id="inventoryAccess"/>
							<label class="width30 ">Inventory</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10 accessRightsCheckBox" type="checkbox" name="docuemntAccess" id="docuemntAccess"/>
							<label class="width30 ">Documents</label>
							
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="discussionAccess" id="discussionAccess"/>
							<label class="width30 ">Discussions</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="noteAccess" id="noteAccess"/>
							<label class="width30 ">Notes</label>
						</div>
					</div>
				</div>
			</div>
			
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>

			</div>
		</div>
</div>
