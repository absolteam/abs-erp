<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style type="text/css">

.list {
  font-family:sans-serif;
  margin:0;
  padding:3px 0 0;
}
.list > li {
  display:block;
  background-color: #FFE6E6;
  padding:10px;
  box-shadow: inset 0 1px 0 #fff;
}
.list > .reply{
	background-color: #BBEEEE;
}
.list > h4 {
  display:block;
  background-color: #eee;
  padding:10px;
  box-shadow: inset 0 1px 0 #fff;
}
.avatar {
  max-width: 150px;
}
img {
  max-width: 100%;
}
h3 {
  font-size: 16px;
  margin:0 0 0.3rem;
  font-weight: normal;
  font-weight:bold;
}
h4 {
  font-size: 13px;
  margin:0.2 0 0.3rem;
  font-weight: normal;
  font-weight:bold;
  text-align: center;
}
p {
  margin:0;
}

input {
  border:solid 1px #ccc;
  border-radius: 5px;
  padding:7px 14px;
  margin-bottom:10px
}
input:focus {
  outline:none;
  border-color:#aaa;
}
.sort {
  padding:8px 30px;
  border-radius: 6px;
  border:none;
  display:inline-block;
  color:#fff;
  text-decoration: none;
  background-color: #28a8e0;
  height:30px;
}
.sort:hover {
  text-decoration: none;
  background-color:#1b8aba;
}
.sort:focus {
  outline:none;
}
.sort:after {
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-bottom: 5px solid transparent;
  content:"";
  position: relative;
  top:-10px;
  right:-5px;
}
.sort.asc:after {
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid #fff;
  content:"";
  position: relative;
  top:13px;
  right:-5px;
}
.sort.desc:after {
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-bottom: 5px solid #fff;
  content:"";
  position: relative;
  top:-10px;
  right:-5px;
}

</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/list.min.js"></script>
<script type="text/javascript">
$(function(){
	var options = {
			  valueNames: [ 'name', 'message','time' ]
			};
	var discussionsList = new List('discussionsList', options);
});
</script>
<div id="discussionsList">
   <input class="search" placeholder="Search" />
  <ul class="list">
  	<c:choose>
  		<c:when test="${DISCUSSION_MAP_LIST ne null}">
	  		<c:forEach var="mapObject" items="${DISCUSSION_MAP_LIST}" varStatus="status1">
	  			<h4 class="date ">${mapObject.key}</h4>
	  			<c:forEach var="discussion" items="${mapObject.value}" varStatus="status">
	  				<c:if test="${discussion.signedUser eq true}">
		  				<li >
					      <h3 class="name">${discussion.person.firstName} ${discussion.person.lastName}</h3>
					      <p class="message">${discussion.message}</p>
					      <span class="time float-right" style="margin-top:-10px;position: relative;">${discussion.time}</span>
					      
					    </li>
					 </c:if>
					 <c:if test="${discussion.signedUser eq false}">
					 	<li class="reply">
					      <h3 class="name" >${discussion.person.firstName} ${discussion.person.lastName}</h3>
					      <p class="message">${discussion.message}</p>
					      <span class="time float-right" style="margin-top:-10px;position: relative;">${discussion.time}</span>
					    </li>
					  </c:if>
	  			</c:forEach>
	  		</c:forEach>
  		</c:when>
  		<c:otherwise>
  			<li>
  				 <h3 class="name">No Discussion Found</h3>
  			</li>
  		</c:otherwise>
  	</c:choose>
  </ul>

</div>