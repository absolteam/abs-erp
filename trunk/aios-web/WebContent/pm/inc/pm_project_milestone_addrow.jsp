<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="milerowid" id="milefieldrow_${rowId}">
	<td id="mileLineId_${rowId}" style="display: none;">${rowId}</td>
	<td><input type="text" name="mileTitle" class="mileTitle"
		id="mileTitle_${rowId}" /></td>
	<td><input type="text" name="mileStartDate" class="mileStartDate"
		id="mileStartDate_${rowId}" /></td>
	<td><input type="text" name="mileEndDate" class="mileEndDate"
		id="mileEndDate_${rowId}"/></td>
	<td><input type="text" name="mileDescription"
		id="mileDescription_${rowId}" />
	</td>
	<td style="width: 0.01%;" class="opn_td" id="mileoption_${rowId}">
		<a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="mileDeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> 
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$('.mileStartDate,.mileEndDate').datepick({showTrigger: '#calImg'}); 
});
</script>