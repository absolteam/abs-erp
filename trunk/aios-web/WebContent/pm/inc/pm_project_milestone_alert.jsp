<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
fieldset>div {
	padding: 3px;
}
</style>
<script type="text/javascript">
 $(function(){
	 $jquery("#projectMileStoneValidation").validationEngine('attach'); 

	$('#actualStartDate,#actualEndDate').datepick({
		 onSelect: customRange, showTrigger: '#calImg'});
	
	 $('#project_milestone_discard').click(function(event){  
		 projectMileStoneDiscard("");
		 return false;
	 });
	 
	 $('#project_milestone_save').click(function(){ 
		 if($jquery("#projectMileStoneValidation").validationEngine('validate')){  
 			var projectMileStoneId = Number($('#projectMileStoneId').val());  
 			var actualStartDate = $('#actualStartDate').val();
 			var actualEndDate = $('#actualEndDate').val();
 			var status = $('#milestoneStatus').val();
 			var actualCost = Number($('#actualCost').val());  
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_project_milestone.action", 
			 	async: false, 
			 	data:{ projectMileStoneId: projectMileStoneId, actualStartDate: actualStartDate, actualEndDate: actualEndDate, 
			 		   status: status, actualCost: actualCost},
			    dataType: "json",
			    cache: false,
				success:function(response){   
					 if(($.trim(response.returnMessage)=="SUCCESS")) 
						 projectMileStoneDiscard("Record updated.");
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
		}else{
			return false;
		}
	 });  
	 
	 $('#milestoneStatus').val($('#milestoneStatustemp').val());
});
 function customRange(dates) {
	if (this.id == 'actualStartDate') {
		$('#actualEndDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#actualStartDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}
function projectMileStoneDiscard(message){ 
 	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_project_milestone.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Project
			milestone
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.generalinfo" />
		</div>
		<form name="projectMileStoneValidation"
			id="projectMileStoneValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="projectMileStoneId"
					value="${PROJECT_MILESTONE.projectMileStoneId}" /> <input
					type="hidden" id="alertId" value="${requestScope.alertId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 140px;">
							<div>
								<label class="width20" for="actualStartDate"> Actual
									Start Date<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_MILESTONE.startDate ne null && PROJECT_MILESTONE.startDate ne ''}">
										<c:set var="startDate" value="${PROJECT_MILESTONE.startDate}" />
										<input name="actualStartDate" type="text" readonly="readonly"
											id="actualStartDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("startDate").toString())%>"
											class="actualStartDate validate[required] width30">
									</c:when>
									<c:otherwise>
										<input name="actualStartDate" type="text" readonly="readonly"
											id="actualStartDate"
											class="actualStartDate validate[required] width30">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width20" for="actualEndDate">Actual End
									Date<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_MILESTONE.endDate ne null && PROJECT_MILESTONE.endDate ne ''}">
										<c:set var="endDate" value="${PROJECT_MILESTONE.endDate}" />
										<input name="actualEndDate" type="text" readonly="readonly"
											id="actualEndDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("endDate").toString())%>"
											class="actualEndDate validate[required] width30">
									</c:when>
									<c:otherwise>
										<input name="actualEndDate" type="text" readonly="readonly"
											id="actualEndDate"
											class="actualEndDate validate[required] width30">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width20" for="actualCost"> Actual Cost<span
									style="color: red;">*</span> </label> <input type="text"
									id="actualCost" name="actualCost"
									value="${PROJECT_MILESTONE.estimationCost}"
									class="validate[required] width30" />
							</div>
							<div>
								<label class="width20" for="milestoneStatus">Status<span
									style="color: red;">*</span> </label> <select name="milestoneStatus"
									id="milestoneStatus" class="width30 validate[required]">
									<c:forEach var="milestoneStatus" items="${PROJECT_STATUS}">
										<option value="${milestoneStatus.key}">${milestoneStatus.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="milestoneStatustemp"
									value="${PROJECT_MILESTONE.status}" />
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 140px;">
							<div>
								<label class="width20" for="referenceNumber"> Reference
									No</label><span>:</span> <span>${PROJECT_MILESTONE.referenceNumber}</span>
							</div>
							<div>
								<label class="width20" for="milestoneTitle"> Milestone
									Title </label> <span>:</span> <span>${PROJECT_MILESTONE.milestoneTitle}</span>
							</div>
							<div>
								<label class="width20" for="projectTitle">Project </label> <span>:</span>
								<span>${PROJECT_MILESTONE.project.projectTitle}</span>
							</div>
							<div>
								<label class="width20" for="startDate"> Start Date </label> <span>:</span>
								<c:set var="startDate" value="${PROJECT_MILESTONE.startDate}" />
								<span><%=DateFormat.convertDateToString(pageContext
									.getAttribute("startDate").toString())%></span>
							</div>
							<div>
								<label class="width20" for="endDate"> End Date</label> <span>:</span>
								<c:set var="endDate" value="${PROJECT_MILESTONE.endDate}" />
								<span><%=DateFormat.convertDateToString(pageContext
									.getAttribute("endDate").toString())%></span>
							</div>
							<div>
								<label class="width20" for="estimationCost"> Estimation
									Cost</label> <span>:</span> <span>${PROJECT_MILESTONE.estimationCost}</span>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right"
					id="project_milestone_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="project_milestone_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
</div>