<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$(function(){
	$('#deliveryDate').datepick({showTrigger: '#calImg'}); 
	
	$jquery("#project_delivery_validation").validationEngine('attach'); 
	
	 $('#project-delivery-discard').click(function(event){  
		 projectDeliveryDiscard("");
		 return false;
	 });
	 
	 if(Number($('#projectDeliveryId').val()> 0)){ 
		 $('#deliveryStatus').val($('#tempDeliveryStatus').val());
	 }else{
		 $('#deliveryStatus').val(2);
	 }
	 
	$('.project-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_project_popup.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			}
		});  
		return false;
	});
	
	$('#project-delivery-save').click(function(){ 
		 if($jquery("#project_delivery_validation").validationEngine('validate')){ 
			var referenceNumber = $('#referenceNumber').val(); 
	 		var deliveryTitle = $('#deliveryTitle').val(); 
			var projectTaskId = Number($('#projectTaskId').val());  
			var projectId = Number($('#projectId').val()); 
			var projectDeliveryId = Number($('#projectDeliveryId').val());   
			var deliveryDate = $('#deliveryDate').val(); 
   			var description=$('#description').val();  
   			var deliveryStatus = Number($('#deliveryStatus').val());  
 			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_project_delivery.action", 
			 	async: false, 
			 	data:{	projectTaskId: projectTaskId, referenceNumber: referenceNumber, deliveryTitle: deliveryTitle,
			 			deliveryDate: deliveryDate, description: description, projectDeliveryId: projectDeliveryId,
			 			deliveryStatus:deliveryStatus,projectId:projectId
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){   
					 if(($.trim(response.returnMessage)=="SUCCESS")) 
						 projectDeliveryDiscard(projectDeliveryId > 0 ? "Record updated.":"Record created.");
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
		}else{
			return false;
		}
	 }); 
	
	$('.project-task-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
		var projectId = Number($('#projectId').val());   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_common_all_project_task.action", 
			data:{	projectId: projectId
		 	 },
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			}
		});  
		return false;
	});
	
	$('#project-milestonelist-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#project-list-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 }); 
	 
	 $('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
	
});
function projectPopupResult(projectObj) {
	$('#projectId').val(projectObj.projectId);
	$('#projectTitle').val(projectObj.projectTitle); 
	$('#common-popup').dialog("close");
} 
function projectMileStonePopupResult(projectMileStoneObj) {
	$('#projectMileStoneId').val(projectMileStoneObj.projectMileStoneId);
	$('#projectMileStone').val(projectMileStoneObj.milestoneTitle); 
	$('#common-popup').dialog("close");
} 
function projectTaskPopupResult(taskObj) {
	$('#projectTaskId').val(taskObj.projectTaskId);
	$('#taskTitle').val(taskObj.taskTitle); 
	$('#common-popup').dialog("close");
} 

function projectDeliveryDiscard(message){ 
 	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_project_deliverables.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();  
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Deliverable
		</div>
	
		<form name="project_delivery_validation" id="project_delivery_validation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="projectDeliveryId" value="${DELIVERY_INFO.projectDeliveryId}">
				<div class="width100 float-left" id="hrm">
					
					<div class="width48 float-right">
						<fieldset style="min-height: 150px;"> 
							 <div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									style="height: 95px;" class="width51">${DELIVERY_INFO.description}</textarea>
							</div>
							<div>
								<label class="width30">Status</label>
								<select name="projectStatus"
									class="width51 deliveryStatus"
									id="deliveryStatus">
									<c:forEach var="resourceType" items="${DELIVERY_STATUS}">
										<option value="${resourceType.key}">${resourceType.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempDeliveryStatus" value="${DELIVERY_INFO.deliveryStatus}">
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 150px;">
							<div>
								<label class="width30" for="referenceNumber"> Reference
									No.<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${DELIVERY_INFO.referenceNumber ne null && DELIVERY_INFO.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${DELIVERY_INFO.referenceNumber}"
											class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="taskTitle">Task</label> <input type="hidden"
									readonly="readonly" name="projectTaskId" id="projectTaskId"
									value="${DELIVERY_INFO.projectTask.projectTaskId}" /> <input type="text"
									id="taskTitle" name="taskTitle" readonly="readonly"
									value="${DELIVERY_INFO.projectTask.taskTitle}"
									class=" width50" /> 
									<c:if test="${DELIVERY_INFO.project eq null}">
									<span class="button">
									<a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all project-task-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</c:if>
							</div>
							<div>
								<label class="width30" for="taskTitle">Request</label> <input type="hidden"
									readonly="readonly" name="projectId" id="projectId"
									value="${DELIVERY_INFO.project.projectId}" /> <input type="text"
									id="projectTitle" name="projectTitle" readonly="readonly"
									value="${DELIVERY_INFO.project.projectTitle}"
									class="width50" /> 
									<c:if test="${DELIVERY_INFO.projectTask eq null}">
										<span class="button">
										<a style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all project-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</c:if>
							</div>
							
							<div>
								<label class="width30" for="deliveryTitle"> Delivery
									Title<span style="color: red;">*</span> </label> <input type="text"
									id="deliveryTitle" name="deliveryTitle"
									value="${DELIVERY_INFO.deliveryTitle}"
									class="validate[required] width50" />
							</div>
							<div>
								<label class="width30" for="delvieryDate"> Delivery Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${DELIVERY_INFO.date ne null && DELIVERY_INFO.date ne ''}">
										<input name="deliveryDate" type="text" readonly="readonly"
											id="deliveryDate"
											value="${DELIVERY_INFO.date}"
											class="deliveryDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="deliveryDate" type="text" readonly="readonly"
											id="deliveryDate"
											class="deliveryDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							
							
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div
					class="portlet-header ui-widget-header float-right project-delivery-discard"
					id="project-delivery-discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right project-delivery-save"
					id="project-delivery-save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>

			</div>
		</form>
	</div>
</div>