<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
fieldset>div{
	padding: 3px;
}
</style>
<script type="text/javascript">
$(function(){
	
	$jquery("#project_delivery_validation").validationEngine('attach'); 
	
	 $('#project-delivery-discard').click(function(event){  
		 projectDeliveryDiscard("");
		 return false;
	 });
	 
	 $('#project-delivery-save').click(function(){ 
		 if($jquery("#project_delivery_alertvalidation").validationEngine('validate')){  
			var projectDeliveryId = Number($('#projectDeliveryId').val());  
			var deliveryStatus = Number($('#deliveryStatus').val()); 
			var alertId =  Number($('#alertId').val()); 
 			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_project_delivery.action", 
			 	async: false, 
			 	data:{	
			 			deliveryStatus: deliveryStatus, projectDeliveryId: projectDeliveryId, alertId: alertId
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){   
					 if(($.trim(response.returnMessage)=="SUCCESS")) 
						 projectDeliveryDiscard("Record updated.");
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
		}else{
			return false;
		}
	 }); 
	$('#deliveryStatus').val($('#deliveryStatustemp').val());
});
function projectDeliveryDiscard(message){ 
 	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_project_deliverables.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) { 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
</script> 
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>project
			deliverables
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general
			information
		</div>
		<form name="project_delivery_alertvalidation" id="project_delivery_alertvalidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="projectDeliveryId" value="${DELIVERY_INFO.projectDeliveryId}">
				<input type="hidden" id="alertId" value="${requestScope.alertId}"> 
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 150px;"> 
							 <div>
								<label class="width15"><fmt:message
										key="accounts.jv.label.description" /> </label><span>:</span>
								<span>${DELIVERY_INFO.description}</span>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 150px;">
							<div>
								<label class="width20" for="referenceNumber"> Reference
									No </label><span>:</span>
								<span>${DELIVERY_INFO.referenceNumber}</span>
							</div>
							<div>
								<label class="width20" for="deliveryTitle"> Delivery
									Title </label> <span>:</span>
									<span>${DELIVERY_INFO.deliveryTitle}</span>
							</div>
							<div>
								<label class="width20" for="delvieryDate"> Delivery Date</label><span>:</span>
									<c:set var="deliveryDate"
										value="${DELIVERY_INFO.deliveryDate}" /> 
									<span><%=DateFormat.convertDateToString(pageContext
										.getAttribute("deliveryDate").toString())%></span>
							</div>
							<div>
								<label class="width20" for="projectTitle">Project</label><span>:</span>
								<span>${DELIVERY_INFO.project.projectTitle}</span>
							</div>
							<c:if test="${DELIVERY_INFO.projectMileStone ne null}">
								<div>
									<label class="width20" for="projectMileStone">MileStone </label> <span>:</span>
									<span>${DELIVERY_INFO.projectMileStone.milestoneTitle}</span>
								</div>
							</c:if> 
							<div>
								<label class="width20" for="deliveryStatus">Status <span
									style="color: red;">*</span></label> 
								<select name="deliveryStatus" id="deliveryStatus" class="width30 validate[required]">
									<c:forEach var="deliveryStatus" items="${DELIVERY_STATUS}">
										<option value="${deliveryStatus.key}">${deliveryStatus.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="deliveryStatustemp" value="${DELIVERY_INFO.deliveryStatus}"/>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div
					class="portlet-header ui-widget-header float-right project-delivery-discard"
					id="project-delivery-discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right project-delivery-save"
					id="project-delivery-save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div> 
		</form>
	</div>
</div>