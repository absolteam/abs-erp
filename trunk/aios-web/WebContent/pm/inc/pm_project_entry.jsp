<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<style>
.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>
<script type="text/javascript">
var slidetab = "";
var accessCode = "";
var sectionRowId = 0;
var projectResources = [];
var projectMilestones = [];
var paymentSchedules =[];
var projectExpenses =[];
var projectInventories =[];
var accessRightRowId =null;
var getProjectMilestones =null;
var getProjectResources = null;
var getPaymentSchedules = null;
var getProjectExpenses = null;
var getInventoryDetails = null;
var globalRowId=null;
$(function(){
	
	$jquery("#projectValidation").validationEngine('attach');
	
	
	$('#tabs').tabs({select: function(event, ui){
	     // Do stuff here
         $("#tabs").tabs().css({
		   'min-height': '500px',
		   'overflow': 'auto'
		});
                 	 /* $("#main-content").animate({ scrollTop: $('#main-content')[0].scrollHeight
                 		 }, 3000); */
	  }});
	
	$('#resourceType_'+1).val(2);
	 $('#resourceDetail_'+1).hide();
	 $('#resourceMaterial_'+1).hide();
	 $('#resourceLabour_'+1).show();
	
	var hiddenAccessRightsValue=$('#accessRightsHidden').val();
	$('.specialTab').each(function(){   
		$(this).hide();
	 });  
	if(hiddenAccessRightsValue!=null && hiddenAccessRightsValue!=''){
		var listTemp=[];
		listTemp=hiddenAccessRightsValue.split(",");
		 
		$('.specialTab').each(function(){   
			for(var i=0;i<=listTemp.length;i++){
				  if((listTemp[i]+"Tab")==$(this).attr('id')){
					  $(this).show();
				  }
			  }
		 });   
	}
		
		
	
	$('#startDate,#endDate').datepick({
		 onSelect: customRange, showTrigger: '#calImg'}); 
	
	$('.mileStartDate,.mileEndDate').datepick({
		 onSelect: function(){
			 var rId=getRowId($(this).attr('id')); 
				miletriggerAddRow(rId);
			}, showTrigger: '#calImg'
	}); 
	
	$('#startDate,#depositDate').datepick({
		 onSelect: customReleaseRange, showTrigger: '#calImg'}); 
	
	$('#startDate,#depositReleaseDate').datepick({
		 onSelect: customReleaseRange, showTrigger: '#calImg'}); 

	$('.paymentDate').datepick({onSelect: function(){
		 var rId=getRowId($(this).attr('id')); 
			paymenttriggerAddRow(rId);
		}, showTrigger: '#calImg'}); 
	
	$('.expenseDate').datepick({onSelect: function(){
		 var rId=getRowId($(this).attr('id')); 
			expensetriggerAddRow(rId);
		}, showTrigger: '#calImg'}); 
	
	 $('#project_discard').click(function(event){  
		 projectDiscard("");
		 return false;
	 });
	 
	 
	 
	 
	 manupulateLastRow();

	 $('#project_save').click(function(){ 
		 projectSave("SAVE");
		 
	 }); 
	 
	 $('#project_save_and_update').click(function(){ 
		 projectSave("UPDATE");
		 
	 }); 
	 
	 
	 $('#send_requisition').click(function(){ 
		 projectSave("REQUISITION");
		 
	 }); 
	 
	 $('#print').click(function(){ 
		 var actionName = '';
		 var status= $.trim($('#projectStatus :selected').text());
		 if(status == 'Open'){
			 actionName = "get_project_info_printout";
		 }else{
			 actionName = "project_delivery_print";
		 }
		if(projectId != null && projectId != 0 && actionName !=''){
			window.open(actionName+'.action?projectId='+ projectId+ '&recordId=0&alertId=0&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
			<%-- $.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/"+actionName+".action", 
		     	async: false, 
		     	data:{projectId: projectId},
				dataType: "json",
				cache: false,
				success: function(response){ 
					if(status == 'Open'){
						jobTemplatePrint(false);
					 }else{
						 deliveryTemplatePrint(response); 
					 }
				} 		
			});  --%>
		}else{
			 
		} 
		return false;
	});
	 
	 getProjectMilestones = function(){
		 projectMilestones = [];
		 $('.milerowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var projectMileStoneId = Number($('#projectMileStoneId_'+rowId).val());  
			var title = $('#mileTitle_'+rowId).val();  
			var startDate = $('#mileStartDate_'+rowId).val(); 
			var endDate = $('#mileEndDate_'+rowId).val(); 
			var description = $('#mileDescription_'+rowId).val();  
			if(typeof title != 'undefined' && title !=null  && title!='' && startDate !=null  && startDate!='' && endDate !=null  && endDate!=''){
				var jsonData = [];
				jsonData.push({
					"title" : title,
					"startDate" : startDate,
					"endDate": endDate,
					"projectMileStoneId" : projectMileStoneId,
					"description" : description
				});   
				projectMilestones.push({
					"projectMilestones" : jsonData
				});
			} 
		});  
		return projectMilestones;
	 };
	 
	 getProjectResources = function(){
		 projectResources = [];
		 $('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var resourceType = Number($('#resourceType_'+rowId).val());  
			var resourceDetail = Number(0);
			var personData  = [];
			if(resourceType == 2){
				$('.personObject_'+rowId).each(function(){ 
					var prrowId = getRowId($(this).attr('id'));  
					personData.push({
						personId : $('#personId_'+prrowId).val()
					});
				});
			}else{
				resourceDetail = Number($('#resourceDetail_'+rowId).val()); 
			}
			var resourceCount = Number($('#resourceCount_'+rowId).val());  
			var unitPrice = Number($('#unitPrice_'+rowId).val());  
			var linesDescription = $('#linedescription_'+rowId).val();  
			var resourceId =  Number($('#resourceId_'+rowId).val()); 
			var accessRights =  $('#accessRights_'+rowId).val(); 
			if(typeof resourceType != 'undefined' && resourceType > 0 && resourceCount > 0){
				var jsonData = [];
				jsonData.push({
					"resourceType" : resourceType,
					"resourceCount" : resourceCount,
					"description": linesDescription,
					"resourceId": resourceId,
					"personData" : personData,
					"resourceDetail" : resourceDetail,
					"unitPrice" : unitPrice,
					"resourceId" : resourceId,
					"accessRights" : accessRights,
				});   
				projectResources.push({
					"projectResources" : jsonData
				});
			} 
		});  
		return projectResources;
	 };
	 
	 getPaymentSchedules = function(){
		 paymentSchedules = [];
		 $('.paymentrowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var paymentScheduleId = Number($('#paymentScheduleId_'+rowId).val());  
			var paymentType = Number($('#paymentType_'+rowId+' option:selected').val());  
			var paymentAmount = $('#paymentAmount_'+rowId).val(); 
			var paymentDate = $('#paymentDate_'+rowId).val(); 
			var paymentMode = Number($('#paymentMode_'+rowId+' option:selected').val());  
			var paymentStatus = Number($('#paymentStatus_'+rowId+' option:selected').val());  
			var description = $('#paymentDescription_'+rowId).val();  
			if(typeof paymentAmount != 'undefined' && paymentAmount !=null  && paymentAmount!='' && paymentDate !=null  && paymentDate!=''){
				var jsonData = [];
				jsonData.push({
					"paymentScheduleId" : paymentScheduleId,
					"paymentType" : paymentType,
					"paymentAmount": paymentAmount,
					"paymentDate" : paymentDate,
					"paymentMode" : paymentMode,
					"paymentStatus" : paymentStatus,
					"description" : description
				});   
				paymentSchedules.push({
					"paymentSchedules" : jsonData
				});
			} 
		});  
		return paymentSchedules;
	 };
	 
	 getProjectExpenses = function(){
		 projectExpenses = [];
		 $('.expenserowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var projectExpenseId = Number($('#projectExpenseId_'+rowId).val());  
			var expenseType = Number($('#expenseType_'+rowId+' option:selected').val());  
			var expenseMode = Number($('#expenseMode_'+rowId+' option:selected').val());  
			var expenseAmount = $('#expenseAmount_'+rowId).val(); 
			var expenseDate = $('#expenseDate_'+rowId).val(); 
			var costToClient = $('#costToClient_'+rowId).val();  
			var expenseReference = $('#expenseReference_'+rowId).val();  
			var description = $('#expenseDescription_'+rowId).val();  
			if(typeof expenseAmount != 'undefined' && expenseAmount !=null  && expenseAmount!='' && expenseDate !=null  && expenseDate!=''){
				var jsonData = [];
				jsonData.push({
					"projectExpenseId" : projectExpenseId,
					"expenseType" : expenseType,
					"expenseMode" : expenseMode,
					"expenseAmount": expenseAmount,
					"expenseDate" : expenseDate,
					"costToClient" : costToClient,
					"expenseReference" : expenseReference,
					"description" : description
				});   
				projectExpenses.push({
					"projectExpenses" : jsonData
				});
			} 
		});  
		return projectExpenses;
	 };
	 
	 getInventoryDetails = function(){ 
		 projectInventories = [];
	 		$('.inventoryrowid').each(function(){ 
		 		
	 			 var rowId = getRowId($(this).attr('id'));  
	 			 var productId = $('#productid_'+rowId).val();   
	 			 var quantity = $('#productQty_'+rowId).val();  
	 			 var amount = $('#inventoryamount_'+rowId).val();    
	 			 var description=$('#inventoryDescription_'+rowId).val();
	 			 var projectInventoryId = Number($('#projectInventoryId_'+rowId).val());
	 			var discount = Number($('#inventoryDiscount_'+rowId).val());
	 			var isPercentage = $('input[name=inventoryDiscountType_'+rowId+']:checked').val();
	 		 	
	 			 var storeId = Number($('#storeid_'+rowId).val());
	 			var projectInventoryStatus = Number($('#projectInventoryStatus_'+rowId).val());
	 			if(typeof productId != 'undefined' && productId !=null  && productId!='' && amount !=null  && amount!=''){
		 			var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"quantity" : quantity,
						"amount": amount,
						"description" : description,
						"storeId" : storeId,
						"projectInventoryId" : projectInventoryId,
						"discount":discount,
						"projectInventoryStatus":projectInventoryStatus,
						"isPercentage":isPercentage
					});   
					projectInventories.push({
						"projectInventories" : jsonData
					});
	 			}
	 		}); 
		
			return projectInventories;
		};
		
		$('#pettycashvoucher-close').live('click',function(){
			$('#common-popup').dialog('close');  
			return false;
		});
		
		$('.pettycash_popup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 var pettyCashType	="2@@AIPC";
		     $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/show_all_settlement_voucher.action",
	             async: false,
	             data:{pettyCashType: pettyCashType},
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
		  	 return false;
		}); 

	 $('.project-type-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	     $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				  $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
	  	 return false;
	}); 
	 
	 $('.resource_detail_popup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	          $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 
	 
	 
	 
	 
	 $('.customer-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			}
		});  
		return false;
	});
	 
	 $('.mileaddrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".miletab>.milerowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/projectmilestone_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.miletab tr:last').before(result);
					 if($(".miletab").height()>255)
						 $(".miletab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.milerowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#milelineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/projectresource_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  	$('#resourceType_'+id).val(2);
			 $('#resourceDetail_'+id).hide();
			 $('#resourceMaterial_'+id).hide();
			 $('#resourceLabour_'+id).show();
		  return false;
	 }); 
	 
	 $('.paymentaddrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".paymenttab>.paymentrowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/projectpayment_schedule_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.paymenttab tr:last').before(result);
					 if($(".paymenttab").height()>255)
						 $(".paymenttab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.paymentrowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#paymentlineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 $('.expenseaddrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".expensetab>.expenserowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/project_expense_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.expensetab tr:last').before(result);
					 if($(".expensetab").height()>255)
						 $(".expensetab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.expenserowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#expenselineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 $('.inventoryaddrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".inventorytab>.inventoryrowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/project_inventory_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.inventorytab tr:last').before(result);
					 if($(".inventorytab").height()>255)
						 $(".inventorytab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.inventoryrowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#inventorylineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
	 
	 $('.resource_labour_popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			tempid = $(this).parent().get(0);
			var rowId = getRowId($(this).attr('id'));
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_multiselect_person_list.action", 
			 	async: false,  
			 	data:{personTypes: "1", id: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
	 				 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
	 
//---------------------Delete Row section-----------------	 

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 }); 
	 
	 $('.miledelrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.milerowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#milelineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 }); 
	 
	 $('.paymentdelrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.paymentrowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#paymentlineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 }); 
	 
	 $('.expensedelrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
       	$('#pettyCashDiv').hide();
       	var pettyCashAmount = Number(0);
    	 $('.expenserowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#expenselineId_'+rowId).html(i);
    		 var expenseMode = Number($('#expenseMode_'+rowId).val());
			 if(expenseMode == 2){
				pettyCashAmount += Number($('#expenseAmount_'+rowId).val());
				$('#pettyCashDiv').show();
			 }
			 i=i+1; 
 		 });   
    	 $('#generalExpenseAmount').text(pettyCashAmount);
	 }); 
	 
	 $('.inventorydelrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
     	 $(slidetab).remove();  
     	 var i=1;
	   	 $('.inventoryrowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#inventorylineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});
//---------------------Change Trigger section-----------------
	 
	 $('.resourceDetail').live('change',function(){
		 var rowId = getRowId($(this).attr('id'));
		 triggerAddRow(rowId);
		 return false;
	 });
	 
	 $('.mileTitle').live('change',function(){
		 var rowId = getRowId($(this).attr('id'));
		 miletriggerAddRow(rowId);
		 return false;
	 });
	 
		 
	 $('.resourceCount,.unitPrice').live('change',function(){
		 var rowId = getRowId($(this).attr('id'));
		 var count = Number($('#resourceCount_'+rowId).val());  
		 var unitRate = Number($('#unitPrice_'+rowId).val());  
		 if(count > 0 && unitRate > 0){
			$('#totalamount_'+rowId).text(Number(count * unitRate).toFixed(2));
		 }else{
			$('#totalamount_'+rowId).text('');
		 } 
		 triggerAddRow(rowId);
		 return false;
	 });

	
	 
	 $('.resourceType').live('change',function(){  
		 var rowId = getRowId($(this).attr('id'));
		 var resourceType = Number($('#resourceType_'+rowId).val());
		 $('#resourceDetail_'+rowId).val(''); 
		 if(resourceType == 2){ 
			 $('#resourceDetail_'+rowId).hide();
			 $('#resourceMaterial_'+rowId).hide();
			 $('#resourceLabour_'+rowId).show();
		 }else{
			 $('#resourceDetail_'+rowId).show();
			 $('#resourceMaterial_'+rowId).show();
			 $('#resourceLabour_'+rowId).hide();
		 }
		 triggerAddRow(rowId);
		 return false;
	 });
	 
	 $('.paymentAmount,.paymentDate').live('change',function(){  
		 var rowId = getRowId($(this).attr('id'));
		 paymenttriggerAddRow(rowId);
		 
		var amount=0.0;
			$('.paymentAmount').each(function(){
		 		 var lineID = getRowId($(this).attr('id'));
		 	 	 amount += Number($('#paymentAmount_'+lineID).val());
		 	 
		 	 });
			$('#paymentTotal').html(amount);
		 return false;
	 });
	 
	 $('.expenseAmount').live('change',function(){  
		 var rowId = getRowId($(this).attr('id')); 
		 var pettyCashAmount = Number(0);
		 $('.expenserowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var expenseMode = Number($('#expenseMode_'+rowId).val());
			if(expenseMode == 2){
				pettyCashAmount += Number($('#expenseAmount_'+rowId).val());
				$('#pettyCashDiv').show();
			}
		 });  
		 $('#generalExpenseAmount').text(pettyCashAmount);
		 expensetriggerAddRow(rowId);
		 return false;
	 });
	 
	 $('.costToClient').live('change',function(){  
		 var rowId = getRowId($(this).attr('id'));
		 resetPayment();
		 return false;
	 });
	 
	
	 
	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#customer-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 
	 
	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="PROJECT_TYPE"){
			$('#projectType').html("");
			$('#projectType').append("<option value=''>Select</option>");
			loadLookupList("projectType"); 
		}else if(accessCode=="PROJECT_RESOURCE_DETAIL") {
			$('#resourceDetail_'+sectionRowId).html("");
			$('#resourceDetail_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("resourceDetail_"+sectionRowId);  
		}else if(accessCode=="CASE_PAYMENT_TYPE") {
			$('#paymentType_'+sectionRowId).html("");
			$('#paymentType_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("paymentType_"+sectionRowId);  
		}else if(accessCode=="CASE_EXPENSE_TYPE") {
			$('#expenseType_'+sectionRowId).html("");
			$('#expenseType_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("expenseType_"+sectionRowId);  
		}
	}); 
	
 	$('.coordinator-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
 	
 	$('#penalityDescriptive').change(function(){  
 	   if($('#penalityDescriptive').attr('checked') == true){ 
 		  $('.ui-dialog-titlebar').remove();    
 	      $('#descriptive-popup').dialog('open');
 	      $(
 					$(
 							$('#descriptive-popup')
 									.parent())
 							.get(0)).css('top',
 					0);
 	   } 
	});
	
	$('#descriptive-popup').dialog({
		autoOpen : false,
		width : 500,
		height : 300,
		bgiframe : true,
		modal : true,
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			}
		}
	});
	
	
	$('.access_rights_popup').live('click',function(){  
		accessRightRowId = getRowId($(this).attr('id'));
		var accessRights=$('#accessRights_'+accessRightRowId).val();
 	   if(accessRights!=null && accessRights!=""){ 
 		   var arrayList=[];
 		  arrayList=accessRights.split(",");
 		 $('.accessRightsCheckBox').each(function(){
			 	$(this).attr('checked',false);
	 	   });
 		  var shortedArrayList=[];
 		  $('.accessRightsCheckBox').each(function(){
 			  for(var i=0;i<=arrayList.length;i++){
 				  if(arrayList[i]==$(this).attr('id')){
 				 		shortedArrayList.push(arrayList[i]);
 				  }
 			  }
	 	   });
 		 $(shortedArrayList)
			.each(function(index){
				$('#'+shortedArrayList[index]).attr('checked','checked');
	 	   });
 	   }else{
 		  $('.accessRightsCheckBox').each(function(){
 			 	$(this).attr('checked','checked');
 	 	   });
 	   } 
 	   
 	  $('.ui-dialog-titlebar').remove();    
	      $('#accessrights-popup').dialog('open');
	      $($($('#accessrights-popup').parent()).get(0)).css('top',0);
	});
		
	$('#accessrights-popup').dialog({
			autoOpen : false,
			width : 500,
			height : 300,
			bgiframe : true,
			modal : true,
			buttons : {
				"Close" : function() {
					$(this).dialog("close");
				},"Confirm" : function() {
					var listString="";
					 $('.accessRightsCheckBox').each(function(){
						 if($(this).attr('checked'))
							 listString+=$(this).attr('id')+",";
				 	   });
					$('#accessRights_'+accessRightRowId).val(listString);
					$(this).dialog("close");
				},
			}
	});
	
	$('#customer-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		tempid = $(this).parent().get(0);
	  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_popup_create.action", 
		 	async: false,  
		 	data: { pageInfo: "pos_invoice"},
		    dataType: "html",
		    cache: false,
			success:function(result){   
 				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0,'important');
			},
			error:function(result){   
					 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	
	{
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_sales_product_common_popup_preload.action", 
		 	async: false,  
		 	data: {itemType: "I"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('#sales-product-inline-popup').html(result);  
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});
	}

	$('.show_product_list_sales_order').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		globalRowId = Number($(this).attr('id').split("_")[1]);  
		$('#sales-product-inline-popup').dialog('open'); 
		return false;
	});
	
	$('#sales-product-inline-popup').dialog({
		autoOpen : false,
		width : 800,
		height : 600,
		bgiframe : true,
		modal : true,
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			}
		}
	});
	 
		$('.show_product_list_sales_order').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_product_stock_popup.action", 
			 	async: false,  
			 	data: {itemType: "E", rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	
 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
 	
 	$('.deleteoption').live('click',
			function() {
		var rowId =  $(this).attr('id').split("_")[1];
		var divId = $(this).parent('div').attr('id'); 
		var parentRowId = $("#"+divId).parent('span').attr('id');
			$('#div_'+rowId).remove(); 
			var i = 0; 
		$('.count_index')
		.each(
				function() { 
					i = i +1;
			$(this).text(i+".");		
		});  
		resourceLabourCount(getRowId(parentRowId), i);
		return false;
	});
 	
 	
 	
 	$('#project_document_information').click(function(){
		 var projectId=Number($('#projectId').val());	
			if(projectId>0){
				AIOS_Uploader.openFileUploader("doc","projectDocs","Project",projectId,"ProjectDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","projectDocs","Project","-1","ProjectDocuments");
			}
		});
 	
 	 $('#project_image_information').click(function(){
 		var projectId=Number($('#projectId').val());
			if(projectId>0){
				AIOS_Uploader.openFileUploader("img","projectImages", "Project", projectId,"ProjectImages");
			}else{
				AIOS_Uploader.openFileUploader("img","projectImages","Project","-1","ProjectImages");
			}
		});
 	if(projectId>0){
 		discussionList();
 		notesList();
 	}
 	
 	 $('#discussion_message_save').click(function(event){  
		var discussion = $('#discussions').val(); 
		 if(discussion!=null && discussion!=''){ 
			 	var projectId = Number($('#projectId').val());
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_project_discussion.action", 
				 	async: false, 
				 	data:{	projectId: projectId, message:discussion,
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						 if(($.trim(response.returnMessage)=="SUCCESS")){
							 $('#discussions').val("");
							 discussionList();

						 }else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				}); 
			}else{
				return false;
			}
	 });
 	 
 	$('#notes_save').click(function(event){  
		var notes = $('#notes').val(); 
		 if(notes!=null && notes!=''){ 
			 	var projectId = Number($('#projectId').val());
			 	var isPublic = $('#public_notes').attr("checked");
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_project_notes.action", 
				 	async: false, 
				 	data:{	projectId: projectId, message:notes,isPublic:isPublic
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						 if(($.trim(response.returnMessage)=="SUCCESS")){
							 $('#notes').val("");
							 notesList();

						 }else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				}); 
			}else{
				return false;
			}
	 });
 	
 	$('.supplierquotation-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result);  
			}
		});  
		 return false;
	});
 	
 	
 	$('#add_task').click(function(){   
 		
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		$('#baseprice-popup-dialog').dialog('destroy');		
		$('#baseprice-popup-dialog').remove(); 
		$('#sales-product-inline-popup').dialog('destroy');
		$('#sales-product-inline-popup').remove();
		var projectId = Number($('#projectId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_task_entry.action", 
	     	async: false, 
	     	data:{parentProjectTaskId: Number(0),projectId:projectId,projectTaskId:Number(0),recordId:0,alertId:0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
 	
 	$('.update_task').click(function(){   
 		
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		$('#baseprice-popup-dialog').dialog('destroy');		
		$('#baseprice-popup-dialog').remove(); 
		$('#sales-product-inline-popup').dialog('destroy');
		$('#sales-product-inline-popup').remove();
 		var rowId = getRowId($(this).attr('id'));
		var projectTaskId = Number($('#projectTaskId_'+rowId).val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_task_entry.action", 
	     	async: false, 
	     	data:{projectTaskId: projectTaskId,recordId:0,alertId:0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});
 	
 	$('#add_delivery').click(function(){  
 		
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		var projectDeliveryId = Number(0);
		var projectId = Number($('#projectId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_delivery_entry.action", 
	     	async: false, 
	     	data:{projectDeliveryId: Number(0),projectTaskId:Number(0),projectId:projectId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
 	
 	$('.update_delivery').click(function(){   
 		$('#common-popup').dialog('destroy');
 		$('#common-popup').remove(); 
 		$('#accessrights-popup').dialog('destroy');
 		$('#accessrights-popup').remove();
 		var rowId = getRowId($(this).attr('id'));
		var projectDeliveryId = Number($('#projectDeliveryId_'+rowId).val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_delivery_entry.action", 
	     	async: false, 
	     	data:{projectDeliveryId: projectDeliveryId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
 	
 	$('.TDRequsitionTable').click(function(){  
 		var rowId = getRowId($(this).attr('id'));
 		
 		$('#RequsitionTable_'+rowId).toggle();
 	}); 
 	
	
 	
 	$('#customer-common-create-popup').live('click',function(){ 
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_customer_addentry.action", 
			 	async: false,  
			 	data: { pageInfo: "pos_invoice", customerId: Number(0)},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('#common-popup').dialog('close'); 
					 $('#temp-result').html(result); 
					 $('.callJq').trigger('click');   
				} 
			});  
	 });
	

 	$('.inventoryamount').change(function(){ 
 		
 		 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#inventoryamount_'+lineID).val());
 	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
 		var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
 	 	 if(productQty >0 && amount > 0){
 	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
 	 	 }
 	 	 
 	 	
 		return false;
 	 });
 	
 	
 	
	$('.inventoryDiscountType').live('change',function(){ 
	 	$('.inventoryDiscount').trigger('change');
		return false;
	 });
 	
 	
 	$('.inventoryDiscount').live('change',function(){ 
 		$('.discount-warning').remove(); 
		 var lineID = getRowId($(this).attr('id'));
	 	 var productQty =  Number($('#productQty_'+lineID).val());
	 	 var amount = Number($('#inventoryamount_'+lineID).val());
	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
	 	var suggestedPrice = Number($('#suggestedRetailPrice_'+lineID).val());
	 	 if(productQty >0 && amount > 0){
	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
	 	 }
	 	 
	 	suggestedPrice=suggestedPrice*productQty;
	 	 var inventoryTotal=Number($('#inventorytotalAmount_'+lineID).text());
	 	 if(inventoryTotal<suggestedPrice){
	 		$(this).parent().append("<span class='discount-warning warning-inline' id=discount-warning_"+lineID+">Warning! Maximum allowed discount is "+Number(Number(productQty) *  (amount)-suggestedPrice)+" for given "+productQty+" quantitie's</span>"); 
	 	 }
	 	resetPayment();
		return false;
	 });
 	
 	$('.productQty').live('change',function(){ 
 		
 		$('.quantityerr').remove(); 
	 	 var lineID = getRowId($(this).attr('id'));
	 	 var productQty =  Number($('#productQty_'+lineID).val());
	 	 var amount = Number($('#inventoryamount_'+lineID).val());
	 	 var discount = Number($('#inventoryDiscount_'+lineID).val());
		var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
	 	 
		 if(productQty >0 && amount > 0){ 
	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
	 	 }
		triggerAddRow(lineID);
		 return false;
 	 });

 	$('.inventoryamount').live('change',function(){ 
 		 var lineID = getRowId($(this).attr('id'));
 	 	 var productQty =  Number($('#productQty_'+lineID).val());
 	 	 var amount = Number($('#inventoryamount_'+lineID).val());
 	 	var discount = Number($('#inventoryDiscount_'+lineID).val());
 	 	var discountType = $('input[name=inventoryDiscountType_'+lineID+']:checked').val();
	 	if(discountType=='true' && discount>0){
	 		discount=Number(Number(productQty *  amount *discount)/100);
	 	 }
 	 	 if(productQty >0 && amount > 0){
 	 		$('#inventorytotalAmount_'+lineID).text((Number(productQty) *  (amount))-discount);
 	 	 }
 	 	 
 	 	
 		return false;
 	 });
		
		$('.callJq').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#temp-result', 
			loaderHeight:16, 
			loaderWidth:17 
		});
		
		$('.expenseDeleteAll').click(function(){  
			$('.expensedelrow').each(function(){
				var rowId = getRowId($(this).attr('id'));
				var projectExpenseId = Number($('#projectExpenseId_'+rowId).val());  
				var expenseAmount = $('#expenseAmount_'+rowId).val(); 
				if(typeof expenseAmount != 'undefined' && expenseAmount !=null  && expenseAmount!='' 
						&& projectExpenseId!=null 
						&& projectExpenseId>0){
					$(this).trigger('click');
				}
			}); 
			 return false;
		 });
		
		$('#projectStatus').change(function(){  
			//projectPaymentStatusCheck();
			 return false;
		 });
		
		$('.expenseMode').live('change',function(){  
			$('#pettyCashDiv').hide();
			var pettyCashAmount = Number(0);
			$('.expenserowid').each(function(){
				var rowId = getRowId($(this).attr('id'));
				var expenseMode = Number($('#expenseMode_'+rowId).val());
				if(expenseMode == 2){
					pettyCashAmount += Number($('#expenseAmount_'+rowId).val());
					$('#pettyCashDiv').show();
				}
			}); 
			$('#generalExpenseAmount').text(pettyCashAmount);
		});
		
		
		if(Number($('#projectId').val()> 0)){    
	 		$('#projectType').val($('#tempprojectType').val());
	 	 	$('#projectStatus').val($('#tempProjectStatus').val());
	 	 	$('#creditTerm').val($('#tempCreditTerm').val());
	 	 	$('#priority').val($('#tempPriority').val());
	 	 	
			populateUploadsPane("doc","projectDocs","Project", $('#projectId').val()); 
			populateUploadsPane("img","projectImages","Project",$('#projectId').val());
			$('.rowid').each(function(){
		 		var rowId = getRowId($(this).attr('id'));
		 		$('#resourceType_'+rowId).val($('#tempresourceType_'+rowId).val());
		 		var resourceType = Number($('#resourceType_'+rowId).val());
				$('#resourceDetail_'+rowId).val(''); 
				$('#resourceCount_'+rowId).attr('readOnly', false);
				if(resourceType == 2){
					 $('#resourceDetail_'+rowId).hide();
					 $('#resourceMaterial_'+rowId).hide();
					 $('#resourceLabour_'+rowId).show();
					 $('#resourceCount_'+rowId).attr('readOnly', true);
				 }else{
					 $('#resourceDetail_'+rowId).show();
					 $('#resourceMaterial_'+rowId).show();
					 $('#resourceLabour_'+rowId).hide();
				 }
		 		$('#resourceDetail_'+rowId).val($('#tempresourceDetail_'+rowId).val());
		 	});	 
			
			$('.paymentrowid').each(function(){
				var rowId = getRowId($(this).attr('id'));
				$('#paymentType_'+rowId).val($('#tempPaymentType_'+rowId).val());
				$('#paymentMode_'+rowId).val($('#tempPaymentMode_'+rowId).val());
				$('#paymentStatus_'+rowId).val($('#tempPaymentStatus_'+rowId).val());
			});	 
			
			{
				var expenseTotalAmount = Number(0);
				$('.expenserowid').each(function(){
					var rowId = getRowId($(this).attr('id'));
					$('#expenseType_'+rowId).val($('#tempExpenseType_'+rowId).val());
					$('#expenseMode_'+rowId).val($('#tempExpenseMode_'+rowId).val());
					var expenseMode = Number($('#expenseMode_'+rowId).val());
					if(expenseMode == 2){
						expenseTotalAmount += Number($('#expenseAmount_'+rowId).val());
						$('#pettyCashDiv').show();
					}
				});
				
				$('#generalExpenseAmount').text(expenseTotalAmount);
			}
			
			$('.inventoryrowid').each(function(){
				var rowId = getRowId($(this).attr('id'));
				var status=$('#projectInventoryStatusName_'+rowId).val();
				if(status=='Completed')
					$(this).hide();
				
			});
			
			//Project Completion Status
			{
				var status=$('#projectStatusView').val();
				if(status=='Completed'){
					$("#projectValidation :input").attr("disabled", true);
					$(".button").remove();
					
					$('#project_save').hide();
					$('#add_task').hide();
					$('#add_delivery').hide();
					$('#discussion_message_save').hide();
					$('#notes_save').hide();
					$('#project_save_and_update').hide();
					$('#send_requisition').hide();
					
					
					$('.update_task').each(function(){
						$(this).hide();
					});
					
					$('.update_delivery').each(function(){
						$(this).hide();
					});
					
					
				}
			}
			
		}
		
		resetPayment();
});

function projectSave(saveNupdate){
	 if($jquery("#projectValidation").validationEngine('validate')){ 
			var referenceNumber = $('#referenceNumber').val(); 
	 		var projectTitle = $('#projectTitle').val(); 
	 		var projectType = Number($('#projectType').val());
	 		var personId = Number($('#coordinatorId').val());
	 		var startDate = $('#startDate').val(); 
			var endDate = $('#endDate').val();  
			var budget = Number($('#budget').val());
			var projectValue = $('#projectValue').val();
			
			var customerId = Number($('#customerId').val());   
			var clientCoordinator = $('#clientCoordinator').val();   
			var coordinatorContact = $('#coordinatorContact').val();   
			var creditTerm = Number($('#creditTerm').val());   
			var contractTerms = $('#contractTerms').val(); 
			
			var securityDeposit = $('#securityDeposit').val();
			var depositReleaseDate = $('#depositReleaseDate').val(); 
			var depositDate = $('#depositDate').val(); 
			var description=$('#description').val();  
			var projectId = Number($('#projectId').val()); 
			var penaltyDescription = $('#penaltyDescription').val();
			var penalityDescriptive = $('#penalityDescriptive').attr('checked');
			var projectStatus = Number($('#projectStatus').val()); 
			var supplierId = Number($('#supplierId').val()); 
			var priority = Number($('#priority').val());
			var pettyCashId = Number($('#pettcash_accountid').val());
			var alertId = Number($('#alertId').val());
			var pettyCashAmount = Number(0);
			var generalExpenseAmount = Number(0);
			if(pettyCashId > 0){
				 pettyCashAmount = Number($.trim($('#pettyCashAmount').text()));
				 generalExpenseAmount = Number($.trim($('#generalExpenseAmount').text())); 
			}
			projectResources = getProjectResources();
			projectMilestones = getProjectMilestones();
			paymentSchedules=getPaymentSchedules();
			projectExpenses=getProjectExpenses();
			projectInventories=getInventoryDetails(); 
			 
			if(pettyCashAmount >= generalExpenseAmount){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_project.action", 
				 	async: false, 
				 	data:{	projectId: projectId, referenceNumber: referenceNumber, projectTitle: projectTitle, startDate: startDate, endDate: endDate,
				 			projectType: projectType, personId: personId, status: status, customerId: customerId, clientCoordinator: clientCoordinator,
				 			coordinatorContact: coordinatorContact, creditTerm: creditTerm, contractTerms: contractTerms, projectValue: projectValue,
				 			securityDeposit: securityDeposit, depositReleaseDate: depositReleaseDate, description: description, penaltyDescription: penaltyDescription,
				 			projectResources: JSON.stringify(projectResources), penalityDescriptive: penalityDescriptive, budget: budget,
				 			saveType:saveNupdate, pettyCashId: pettyCashId, alertId:alertId,
				 			depositDate:depositDate,projectMilestones: JSON.stringify(projectMilestones),status:projectStatus,
				 			paymentSchedules:JSON.stringify(paymentSchedules),projectExpenses:JSON.stringify(projectExpenses),
				 			supplierId:supplierId,priority:priority,projectInventories:JSON.stringify(projectInventories)
				 			
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						 if(($.trim(response.returnMessage)=="SUCCESS")){
							
							 callProject(response.projectId,(projectId > 0 ? "Record updated.":"Record created."));
						 }else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 };
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				});
			} else{
				$('#page-error').hide().html("General expense should be lesser than or equal to pettycash amount.").slideDown(1000);
				$('#page-error').delay(2000).slideUp();
				return false;
			}
		}else{
			return false;
		}
}
function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
	$('.rowid').each(function() {
		var id = getRowId($(this).attr('id'));
		$('#resourceType_'+id).val(2);
		 $('#resourceDetail_'+id).hide();
		 $('#resourceMaterial_'+id).hide();
		 $('#resourceLabour_'+id).show();
	});

	
	//Milestones
	var milehiddenSize = 0;
	$($(".miletab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++milehiddenSize;
		}
	});
	var miletdSize = $($(".miletab>tr:first").children()).size();
	var mileactualSize = Number(miletdSize - milehiddenSize);
	$('.miletab>tr:last').removeAttr('id');
	$('.miletab>tr:last').removeClass('milerowid').addClass('lastrow');
	$($('.miletab>tr:last').children()).remove();
	for ( var i = 0; i < mileactualSize; i++) {
		$('.miletab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
	//Payment Scedule
	var paymenthiddenSize = 0;
	$($(".paymenttab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++paymenthiddenSize;
		}
	});
	var paymenttdSize = $($(".paymenttab>tr:first").children()).size();
	var paymentactualSize = Number(paymenttdSize - paymenthiddenSize);
	$('.paymenttab>tr:last').removeAttr('id');
	$('.paymenttab>tr:last').removeClass('paymentrowid').addClass('lastrow');
	$($('.paymenttab>tr:last').children()).remove();
	for ( var i = 0; i < paymentactualSize; i++) {
		$('.paymenttab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
	//Expense 
	var expensehiddenSize = 0;
	$($(".expensetab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++expensehiddenSize;
		}
	});
	var expensetdSize = $($(".expensetab>tr:first").children()).size();
	var expenseactualSize = Number(expensetdSize - expensehiddenSize);
	$('.expensetab>tr:last').removeAttr('id');
	$('.expensetab>tr:last').removeClass('expenserowid').addClass('lastrow');
	$($('.expensetab>tr:last').children()).remove();
	for ( var i = 0; i < expenseactualSize; i++) {
		$('.expensetab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	
	
	//Inventory 
	var inventoryhiddenSize = 0;
	$($(".inventorytab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++inventoryhiddenSize;
		}
	});
	var inventorytdSize = $($(".inventorytab>tr:first").children()).size();
	var inventoryactualSize = Number(inventorytdSize - inventoryhiddenSize);
	$('.inventorytab>tr:last').removeAttr('id');
	$('.inventorytab>tr:last').removeClass('inventoryrowid').addClass('lastrow');
	$($('.inventorytab>tr:last').children()).remove();
	for ( var i = 0; i < inventoryactualSize; i++) {
		$('.inventorytab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	  
	
}

function commonProductPopup(rowId, aData) {
	if(rowId==null || row==0)
		rowId=globalRowId;
	
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.code +" - "+aData.productName); 
	$('#productQty_' + rowId).val(1);
	$('#standardPrice_' + rowId).val(aData.standardPrice); 
	var customerId = Number($('#customerId').val());
	
	if(customerId > 0){
		checkCustomerSalesPrice(aData.productId, customerId, aData.sellingPrice, rowId);
	}else{
		$('#inventoryamount_' + rowId).val(aData.sellingPrice); 
		$('#inventorytotalamount_' + rowId).text(aData.sellingPrice); 

	} 
	$('#sales-product-inline-popup').dialog("close");
	inventorytriggerAddRow(rowId);
	resetPayment();
	return false;
}
function checkCustomerSalesPrice(productId, customerId, sellingPrice, rowId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getcustomer_salesprice.action",
			data : {
				productId : productId, customerId: customerId
			},
			async : false,
			dataType : "json",
			cache : false,
			success : function(response) { 
				if(response.sellingPrice != null && response.sellingPrice != 0){
					$('#inventoryamount_' + rowId).val(response.sellingPrice); 
				}else{
					$('#inventoryamount_' + rowId).val(sellingPrice); 
				}
			}
		});
	return true;
}
function triggerAddRow(rowId) {
	var resourceType = $('#resourceType_' + rowId).val();
	var resourceDetail = $('#resourceDetail_' + rowId).val();
	var labour = $.trim($('#labour_' + rowId).html());
	var nexttab = $('#fieldrow_' + rowId).next();
	if (resourceType != null && resourceType != "" && (resourceDetail != null
			&& resourceDetail != "" || labour != null && labour != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
	}
	
	$('#resourceType_'+rowId).val(2);
	 $('#resourceDetail_'+rowId).hide();
	 $('#resourceMaterial_'+rowId).hide();
	 $('#resourceLabour_'+rowId).show();
}
function miletriggerAddRow(rowId) {
	var title = $('#mileTitle_' + rowId).val();
	var startDate = $('#mileStartDate_' + rowId).val();
	var endDate =	$('#mileEndDate_' + rowId).val();
	var nexttab = $('#milefieldrow_' + rowId).next();
	if (title != null && title != "" && (startDate != null
			&& startDate != "" && endDate != null && endDate != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#mileDeleteImage_' + rowId).show();
		$('.mileaddrows').trigger('click');
	}
}

function paymenttriggerAddRow(rowId) {
	var amount = $('#paymentAmount_' + rowId).val();
	var paymentDate = $('#paymentDate_' + rowId).val();
	
	var nexttab = $('#paymentfieldrow_' + rowId).next();
	if (amount != null && amount != "" && (paymentDate != null
			&& paymentDate != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#paymentDeleteImage_' + rowId).show();
		$('.paymentaddrows').trigger('click');
	}
}
function expensetriggerAddRow(rowId) {
	var amount = $('#expenseAmount_' + rowId).val();
	var expenseType = Number($('#expenseType_'+rowId+' option:selected').val());  
	var nexttab = $('#expensefieldrow_' + rowId).next();
	if (amount != null && amount != "" && (expenseType != null
			&& expenseType != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#expenseDeleteImage_' + rowId).show();
		$('.expenseaddrows').trigger('click');
	}
}
function inventorytriggerAddRow(rowId) {
	var amount = $('#inventoryamount_' + rowId).val();
	var productId=$('#productid_' + rowId).val();
	
	var nexttab = $('#inventoryfieldrow_' + rowId).next();
	if (amount != null && amount != "" && (productId != null
			&& productId != "") 
			&& $(nexttab).hasClass('lastrow')) {
		$('#inventoryDeleteImage_' + rowId).show();
		$('.inventoryaddrows').trigger('click');

	}
}

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#projectCoordinator').val(personname);
	$('#coordinatorId').val(personid); 
	$('#common-popup').dialog("close");
}
function commonCustomerPopup(customerId, customerName, param1, param2, param3, param4){
	$('#customerName').val(customerName);
	$('#customerId').val(customerId);
	$('#common-popup').dialog("close");
}
/* function commonProductPopup(aData, rowId) {
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.productName);
	$('#store_' + rowId).html(aData.storeName);
	$('#totalAvailQty_' + rowId).val(aData.availableQuantity);
	$('#productQty_' + rowId).val(1);
	$('#inventoryamount_' + rowId).val(aData.sellingPrice); 
	$('#storeid_' + rowId).val(aData.shelfId);
	$('#suggestedRetailPrice_' + rowId).val(aData.standardPrice); 
	$('.inventoryamount').trigger('change');
	inventorytriggerAddRow(rowId);
	resetPayment();
} */
function customRange(dates) {
	if (this.id == 'startDate') {
		$('#endDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#startDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}

function customReleaseRange(dates) {
	if (this.id == 'startDate') {
		$('#depositReleaseDate').datepick('option', 'minDate', dates[0] || null); 
	}
	else{
		$('#startDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}

function projectDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_project.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$('#descriptive-popup').dialog('destroy');
			$('#descriptive-popup').remove();
			$('#accessrights-popup').dialog('destroy');
			$('#accessrights-popup').remove();
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function callProject(projectId,message){
	$('#common-popup').dialog('destroy');
	$('#common-popup').remove(); 
	$('#descriptive-popup').dialog('destroy');
	$('#descriptive-popup').remove();
	$('#accessrights-popup').dialog('destroy');
	$('#accessrights-popup').remove();
	$('#baseprice-popup-dialog').dialog('destroy');		
	$('#baseprice-popup-dialog').remove(); 
	$('#sales-product-inline-popup').dialog('destroy');
	$('#sales-product-inline-popup').remove();
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/project_entry.action", 
     	async: false, 
     	data:{projectId: projectId},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#main-wrapper").html(result);
		} 		
	}); 
	$('#page-success').hide().html(message).slideDown(1000);
	$('#page-success').delay(2000).slideUp();
	return false;
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	function selectedPersonList(jsonData, rowId) {
		personObject = [];
		personObject = {
			"title" : "Employees",
			"record" : jsonData
		};
		var htmlString = "";
		var count = Number(0);
		$(personObject.record)
				.each(
						function(index) {
							count += 1;
							htmlString += "<div id=div_"+rowId+""+personObject.record[index].personId+""+count+" class='width100 float-left personObject personObject_"+rowId+"' style='padding: 3px;' >"
									+ "<span id='countindex_"+rowId+""+personObject.record[index].personId+""+count+"' class='float-left count_index' style='margin: 0 5px;'>"
									+ count
									+ ".</span>"
									+ "<span class='width40 float-left'>"
									+ personObject.record[index].personName
									+ "</span> <input type='hidden' id='personId_"+rowId+""+personObject.record[index].personId+""+count+"' value='"+personObject.record[index].personId+"'/>"
									+ "<span class='deleteoption' id='deleteoption_"+rowId+""+personObject.record[index].personId+""+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
						});
		$('#labour_' + rowId).html(htmlString); 
		resourceLabourCount(rowId, personObject.record.length);
	}
	function resourceLabourCount(rowId, count){  
		$('#resourceCount_' + rowId).val(count);
		$('#resourceCount_' + rowId).attr('readOnly', true);
 		var unitRate = Number($('#unitPrice_'+rowId).val());
		if(count > 0 && unitRate > 0){
			$('#totalamount_'+rowId).text(Number(count * unitRate).toFixed(2));
		}else{
			$('#totalamount_'+rowId).text('');
		}
		
		triggerAddRow(rowId);
		
		$('#resourceType_'+rowId).val(2);
		 $('#resourceDetail_'+rowId).hide();
		 $('#resourceMaterial_'+rowId).hide();
		 $('#resourceLabour_'+rowId).show();
		return false;
	}
	
	//Generate Discussion list
	function discussionList(){
		var projectId = Number($('#projectId').val());
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getAll_project_based_discussions.action",
					data : {
						projectId : projectId,projectTaskId:null
					},
					async : false,
					dataType : "html",
					cache : false,
					success : function(response) {
						$("#discussion_list").html(response);
					}
				});
	}
	
	//Generate Notes list
	function notesList(){
		var projectId = Number($('#projectId').val());
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getAll_project_based_notes.action",
					data : {
						projectId : projectId,projectTaskId:null
					},
					async : false,
					dataType : "html",
					cache : false,
					success : function(response) {
						$("#notes_list").html(response);
					}
				});
	}
	
	function commonSupplierPopup(supplierId, supplierName, combinationId,
			accountCode, param) {
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
	}
	
	function resetPayment(){
		var amount=0.0;
	 	/*$('.totalamount').each(function(){
	 		 var lineID = getRowId($(this).attr('id'));
	 	 	 amount += Number($('#totalamount_'+lineID).html());
	 	 
	 	 });
		$('#serviceTotal').html(amount); */
		
		amount=0.0;
		$('.costToClient').each(function(){
	 		 var lineID = getRowId($(this).attr('id'));
	 	 	 amount += Number($('#costToClient_'+lineID).val());
	 	 
	 	 });
		
		$('.expenseAmountSubTask').each(function(){
	 	 	 amount += Number($(this).text());
	 	 });
		
		$('#expenseTotal').html(amount);
		
		amount=0.0;
		$('.inventoryamount').each(function(){
	 		 var lineID = getRowId($(this).attr('id'));
	 	 	 amount += Number($('#inventorytotalAmount_'+lineID).html());
	 	 
	 	 });
		$('.inventoryAmountSubTask').each(function(){
	 	 	 amount += Number($(this).text());
	 	 });
		$('#inventoryTotal').html(amount);
		
		
		amount=Number($('#expenseTotal').html())+Number($('#inventoryTotal').html());
		$('#grandTotal').html(amount);
		
		amount=0.0;
		$('.paymentAmount').each(function(){
	 		 var lineID = getRowId($(this).attr('id'));
	 	 	 amount += Number($('#paymentAmount_'+lineID).val());
	 	 
	 	 });
		$('#paymentTotal').html(amount);
	}
	
	function projectPaymentStatusCheck(){ 
		var projectId = Number($('#projectId').val()); 
		var projectStatus = Number($('#projectStatus').val()); 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/project_payment_status_check.action",
				async : false,
				dataType : "json",
				cache : false,
				
				data : {
					projectId : projectId,status:projectStatus
				},
				cache : false,
				success : function(response) {
					if(($.trim(response.returnMessage)=="SUCCESS")){
						 
					 }else{
						 $('#projectStatus').val(projectStatus-1);
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(3000).slideUp();
						 
						 return false;
					 };
				}
			});
		}
	
	function commonPettyCashDetail(pettyCashId, param1, param2, cashOut, voucherNumber, param4, param5){
 		$('#pettcash_accountid').val(pettyCashId); 
		$('#pettcash_account').val(voucherNumber); 
		$('#pettycashvoucher-close').trigger('click');
		getMaximumPettyCash(pettyCashId);
		return false;
	 }
	
	function getMaximumPettyCash(settlementVoucherId) {   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_maximum_pettycash.action", 
		 	async: false,  
		 	data : {settlementVoucherId: settlementVoucherId}, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#pettyCashAmount').text($.trim(result));
			} 
		}); 
		return false;
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> ${sessionScope.project_projectTitle}
		</div>
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
					<div id="page-success"
					class="response-msg success ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<form name="projectValidation" id="projectValidation"
			style="position: relative;">
				<input type="hidden" id="projectId"
					value="${PROJECT_INFO.projectId}" />
					<input type="hidden" id="alertId"
					value="${PROJECT_INFO.alertId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-left width48">
						<fieldset style="min-height: 280px;">
							<div>
								<label class="width30" for="referenceNumber"> ${sessionScope.project_reference}<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_INFO.referenceNumber ne null && PROJECT_INFO.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${PROJECT_INFO.referenceNumber}"
											class="validate[required] width60" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width60" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="customerName">${sessionScope.project_client}<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_INFO.customer.personByPersonId ne null && PROJECT_INFO.customer.personByPersonId ne ''}">
										<input
										type="text" readonly="readonly" name="customerName" class="validate[required] width60"
										id="customerName" value="${PROJECT_INFO.customer.personByPersonId.firstName} ${PROJECT_INFO.customer.personByPersonId.lastName}"
										/>
									</c:when>
									<c:when
										test="${PROJECT_INFO.customer.company ne null && PROJECT_INFO.customer.company ne ''}">
										<input type="text" readonly="readonly" id="customerName"
											name="customerName"
											value="${PROJECT_INFO.customer.company.companyName}"
											class="validate[required] width60" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="customerName"
											name="customerName" class="validate[required] width60" />
									</c:otherwise>
								</c:choose>
								<input type="hidden" readonly="readonly" name="customerId"
									id="customerId" value="${PROJECT_INFO.customer.customerId}" />
								<span
										class="button" style="position: relative; left: 5px;">
										<a style="cursor: pointer;" id="customer-popup"
										class="btn ui-state-default ui-corner-all common-popup-new width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="projectTitle"> Title<span
									style="color: red;">*</span> </label> <input type="text"
									id="projectTitle" name="projectTitle"
									value="${PROJECT_INFO.projectTitle}"
									class="validate[required] width60" />
							</div>
							<div>
								<label class="width30" for="projectCoordinator">
									Lead<span style="color: red;">*</span>
								</label> <input type="text" readonly="readonly"
									name="projectCoordinator" id="projectCoordinator"
									class="width60 validate[required]"
									value="${PROJECT_INFO.personByProjectLead.firstName} ${PROJECT_INFO.personByProjectLead.lastName }" />
								<input type="hidden" readonly="readonly" name="coordinatorId"
									id="coordinatorId"
									value="${PROJECT_INFO.personByProjectLead.personId}" />
									<c:if test="${PROJECT_INFO.allowModification eq true}">
										 <span
											class="button"> <a style="cursor: pointer;" id="person"
											class="btn ui-state-default ui-corner-all coordinator-person-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> 
										</span> 
									</c:if>
									<input
									type="hidden" id="tempprojectCoordinator"
									value="${PROJECT_INFO.personByProjectLead.personId}" />
							</div>
							<div>
								<label class="width30" for="startDate"> Start Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PROJECT_INFO.fromDate ne null && PROJECT_INFO.fromDate ne ''}">
										<input name="startDate" type="text" readonly="readonly"
											id="startDate"
											value="${PROJECT_INFO.fromDate}" 
											class="startDate validate[required] width60">
									</c:when>
									<c:otherwise>
										<input name="startDate" type="text" readonly="readonly"
											id="startDate" class="startDate validate[required] width60">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="endDate"> End Date</label>
								<c:choose>
									<c:when
										test="${PROJECT_INFO.endDate ne null && PROJECT_INFO.endDate ne ''}">
										<c:set var="endDate" value="${PROJECT_INFO.endDate}" />
										<input name="endDate" type="text" readonly="readonly"
											id="endDate"
											value="<%=DateFormat.convertDateToString(pageContext.getAttribute("endDate").toString())%>"
											class="endDate width60">
									</c:when>
									<c:otherwise>
										<input name="endDate" type="text" readonly="readonly"
											id="endDate" class="endDate width60">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="projectValue">Project Value<span
									style="color: red;">*</span></label>
								<input name="projectValue" type="text" id="projectValue"
									value="${PROJECT_INFO.projectValue}"
									class="width60 right-align validate[required]">
							</div>
							
							<div>
								<label class="width30">Priority</label>
								<select name="priority"
									class="width61 priority"
									id="priority">
									<c:forEach var="priority" items="${PRIORITES}">
										<option value="${priority.key}">${priority.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempPriority" value="${PROJECT_INFO.priority}">
							</div>
							<div>
								<label class="width30">Status</label>
								<select name="projectStatus"
									class="width61 projectStatus"
									id="projectStatus">
									<c:forEach var="resourceType" items="${PROJECT_STATUS}">
										<c:if test="${PROJECT_INFO.projectStatus le resourceType.key}">
											<option value="${resourceType.key}">${resourceType.value}</option>
										</c:if>
									</c:forEach>
								</select>
								<input type="hidden" id="tempProjectStatus" value="${PROJECT_INFO.projectStatus}">
								<input type="hidden" id="projectStatusView" value="${PROJECT_INFO.statusView}">
							</div>
							<%-- <c:if test="${PROJECT_HISTORY ne null && fn:length(PROJECT_HISTORY)>0}">
								<div class="width30">
									<fieldset>
										<legend>History</legend>
										<c:forEach var="history" items="${PROJECT_HISTORY}" varStatus="status">
											<a href="#" class="width30" id="projectHistoryId_${history.projectId}">${status.index+1} . ${history.statusView} - ${history.projectTitle}</a>
										</c:forEach>
									</fieldset>
								</div>
							</c:if>  --%>
						</fieldset>
					</div>
					<div class="float-left  width48">
						<fieldset style="min-height: 280px;">
							<div>
								<label class="width30" for="projectType">Type</label> <select name="projectType"
									id="projectType" class="width61">
									<option value="">Select</option>
									<c:forEach var="projectType" items="${PROJECT_TYPE}">
										<option value="${projectType.lookupDetailId}">${projectType.displayName}</option>
									</c:forEach>
								</select> <span class="button"> <a style="cursor: pointer;"
									id="PROJECT_TYPE"
									class="btn ui-state-default ui-corner-all project-type-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" id="tempprojectType"
									value="${PROJECT_INFO.lookupDetail.lookupDetailId}" />
							</div>
							
							
							<div>
								<label class="width30" for="supplierName">
									Supplier
								</label> <c:choose>
									<c:when
										test="${PROJECT_INFO.supplier ne null && PROJECT_INFO.supplier ne ''}">
										<input type="text" readonly="readonly"
									name="supplierName" id="supplierName"
									class="width60"
									value="${PROJECT_INFO.supplierName} [${PROJECT_INFO.supplier.supplierNumber}]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly"
									name="supplierName" id="supplierName"
									class="width60"
									/>
									</c:otherwise>
								</c:choose>
								<input type="hidden" readonly="readonly" name="supplierId"
									id="supplierId"
									value="${PROJECT_INFO.supplier.supplierId}" /> <span
									class="button"> <a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all supplierquotation-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div>
							
							<div>
								<label class="width30" for="clientCoordinator">${sessionScope.project_client}
									Coordinator</label> <input type="text" id="clientCoordinator"
									value="${PROJECT_INFO.clientCoordinator}"
									name="clientCoordinator" class="width60" />
							</div>
							<div>
								<label class="width30" for="coordinatorContact">Coordinator
									Mobile</label> <input type="text" id="coordinatorContact"
									value="${PROJECT_INFO.coordinatorContact}"
									name="coordinatorContact" class="width60" />
							</div>
							<div>
								<label class="width30" for="budget">Budget</label> <input
									type="text" id="budget" class="width60 right-align"
									value="${PROJECT_INFO.budget}" />
							</div>
							<div>
								<label class="width30" for="securityDeposit">Security
									Deposit</label> <input name="securityDeposit" type="text"
									id="securityDeposit" value="${PROJECT_INFO.securityDeposit}"
									class="width60 right-align">
							</div>
							<div style="display:none;">
								<label class="width30" for="depositReleaseDate">Deposit
									Date</label>
								<c:choose>
									<c:when
										test="${PROJECT_INFO.depositDate ne null && PROJECT_INFO.depositDate ne ''}">
										<c:set var="depositDate"
											value="${PROJECT_INFO.depositDate}" />
										<input name="depositDate" type="text"
											readonly="readonly" id="depositDate"
											value="<%=DateFormat.convertDateToString(pageContext.getAttribute("depositDate").toString())%>"
											class="depositDate width60">
									</c:when>
									<c:otherwise>
										<input name="depositDate" type="text"
											readonly="readonly" id="depositDate"
											class="depositDate width60">
									</c:otherwise>
								</c:choose>
							</div>
							<div style="display:none;">
								<label class="width30" for="depositReleaseDate">Deposit
									Release</label>
								<c:choose>
									<c:when
										test="${PROJECT_INFO.depositReleaseDate ne null && PROJECT_INFO.depositReleaseDate ne ''}">
										<c:set var="depositReleaseDate"
											value="${PROJECT_INFO.depositReleaseDate}" />
										<input name="depositReleaseDate" type="text"
											readonly="readonly" id="depositReleaseDate"
											value="<%=DateFormat.convertDateToString(pageContext.getAttribute("depositReleaseDate").toString())%>"
											class="depositReleaseDate width60">
									</c:when>
									<c:otherwise>
										<input name="depositReleaseDate" type="text"
											readonly="readonly" id="depositReleaseDate"
											class="depositReleaseDate width60">
									</c:otherwise>
								</c:choose>
							</div>
							<c:if test="${sessionScope.project_access_paymentTerms eq 'show'}">
								<div>
									<label class="width30" for="creditTerm">Payment Terms</label> <select
										name="creditTerms" id="creditTerm" class="width61">
										<option value="">Select</option>
										<c:forEach var="creditTerm" items="${CREDIT_TERMS}">
											<option value="${creditTerm.creditTermId}">${creditTerm.name}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempCreditTerm"
										value="${PROJECT_INFO.creditTerm.creditTermId}" />
								</div>
							</c:if>
							<c:if test="${sessionScope.project_access_paymentTerms eq 'hide'}">
								<div style="display:none;">
									<label class="width30" for="creditTerm">Payment Terms</label> <select
										name="creditTerms" id="creditTerm" class="width61">
										<option value="">Select</option>
										<c:forEach var="creditTerm" items="${CREDIT_TERMS}">
											<option value="${creditTerm.creditTermId}">${creditTerm.name}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempCreditTerm"
										value="${PROJECT_INFO.creditTerm.creditTermId}" />
								</div>
							</c:if>
							<c:if test="${sessionScope.project_access_penalityDescriptive eq 'show'}">
								<div>
									<label class="width30" for="penalityDescriptive">Penality
										Descriptive</label>
									<c:choose>
										<c:when
											test="${PROJECT_INFO.penalityDescriptive ne null &&  PROJECT_INFO.penalityDescriptive ne ''}">
											<input type="checkbox" name="penalityDescriptive"
												id="penalityDescriptive" checked="checked" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="penalityDescriptive"
												id="penalityDescriptive" />
										</c:otherwise>
									</c:choose>
								</div>
							</c:if>
							<c:if test="${sessionScope.project_access_penalityDescriptive eq 'hide'}">
								<div style="display:none;">
									<label class="width30" for="penalityDescriptive">Penality
										Descriptive</label>
									<c:choose>
										<c:when
											test="${PROJECT_INFO.penalityDescriptive ne null &&  PROJECT_INFO.penalityDescriptive ne ''}">
											<input type="checkbox" name="penalityDescriptive"
												id="penalityDescriptive" checked="checked" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="penalityDescriptive"
												id="penalityDescriptive" />
										</c:otherwise>
									</c:choose>
								</div>
							</c:if>
							<c:if test="${sessionScope.project_access_contractTerms eq 'show'}">
								<div>
									<label class="width30" for="contractTerms">Contract
										Terms</label>
									<textarea id="contractTerms" name="contractTerms"
										style="height: 90px;" class="width61">${PROJECT_INFO.contractTerms}</textarea>
								</div>
							</c:if>
							<c:if test="${sessionScope.project_access_contractTerms eq 'hide'}">
								<div style="display:none;">
									<label class="width30" for="contractTerms">Contract
										Terms</label>
									<textarea id="contractTerms" name="contractTerms"
										style="height: 90px;" class="width61">${PROJECT_INFO.contractTerms}</textarea>
								</div>
							</c:if>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									style="height: 70px;" class="width61">${PROJECT_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>
					
				</div>
			</form>
			</div>
			<div class="clearfix"></div>
			 <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
				<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
					<li class="ui-state-default ui-corner-top ui-tabs-selected "><a href="#tabs-1">Milestones</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="resouceAccessTab"><a href="#tabs-2">Resources</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="paymentAccessTab"><a href="#tabs-7">Payments</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="expenseAccessTab"><a href="#tabs-8">Expense</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="inventoryAccessTab"><a href="#tabs-9">Inventory</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="taskAccessTab"><a href="#tabs-3">Tasks</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="docuemntAccessTab"><a href="#tabs-4">Documents</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="discussionAccessTab"><a href="#tabs-5">Discussions</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="noteAccessTab"><a href="#tabs-6">Notes</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected specialTab" id="deliveryAccessTab"><a href="#tabs-10">Delivery</a></li>
				</ul>
<!--##############################################################  Tab 1 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1">
					<div class="portlet-content quotation_detail"
						style="margin-top: 10px;" id="hrm">
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="childCount" id="childCount"
									value="${fn:length(MILESONTES_INFO)}" />
								<table id="milehastab" class="width100">
									<thead>
										<tr>
											<th style="width: 30%">Title</th>
											<th style="width: 10%">Start Date</th>
											<th style="width: 10%">End Date</th>
											<th style="width: 25%">Description</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="miletab">
										<c:if
											test="${MILESONTES_INFO ne null && fn:length(MILESONTES_INFO)>0}">
											<c:forEach var="miledetail"
												items="${MILESONTES_INFO}" varStatus="milestatus">
												<tr class="milerowid" id="milefieldrow_${milestatus.index+1}">
													<td id="mileLineId_${milestatus.index+1}" style="display: none;">${milestatus.index+1}</td>
													<td><input type="text" name="mileTitle" class="mileTitle"
														id="mileTitle_${milestatus.index+1}" value="${miledetail.title}"/></td>
													<td><input type="text" name="mileStartDate" class="mileStartDate" readonly="readonly"
														id="mileStartDate_${milestatus.index+1}" value="${miledetail.fromDate}"/></td>
													<td><input type="text" readonly="readonly" name="mileEndDate" class="mileEndDate"
														id="mileEndDate_${milestatus.index+1}" value="${miledetail.toDate}"/></td>
													<td><input type="text" name="mileDescription"
														id="mileDescription_${milestatus.index+1}"  value="${miledetail.description}"/>
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="mileoption_${milestatus.index+1}"> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="mileDeleteImage_${milestatus.index+1}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> 
													 <input type="hidden" id="projectMileStoneId_${milestatus.index+1}"
														value="${miledetail.projectMileStoneId}" /></td>
												</tr>
											</c:forEach>
										</c:if>
										<c:forEach var="i"
											begin="${fn:length(MILESONTES_INFO)+1}"
											end="${fn:length(MILESONTES_INFO)+2}" step="1">
											<tr class="milerowid" id="milefieldrow_${i}">
												<td id="mileLineId_${i}" style="display: none;">${i}</td>
												<td><input type="text" name="mileTitle" class="mileTitle"
													id="mileTitle_${i}" /></td>
												<td><input type="text" name="mileStartDate" class="mileStartDate"
													id="mileStartDate_${i}" /></td>
												<td><input type="text" name="mileEndDate" class="mileEndDate"
													id="mileEndDate_${i}"/></td>
												<td><input type="text" name="mileDescription"
													id="mileDescription_${i}" />
												</td>
												<td style="width: 0.01%;" class="opn_td" id="mileoption_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="mileDeleteImage_${i}" style="display: none; cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> 
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
					</div>
				</div>
<!--##############################################################  Tab 2 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2">
					<input type="hidden" id="accessRightsHidden" value="${PROJECT_INFO.projectAccessRights}" />
					<div class="portlet-content quotation_detail"
						style="margin-top: 10px;" id="hrm">
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="childCount" id="childCount"
									value="${fn:length(RESOURCE_INFO)}" />
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th style="width: 10%;display:none;">Resource Type</th>
											<th style="width: 25%">Resource</th>
											<th style="width: 5%;display:none;">Count</th>
											<th style="width: 5%;display:none;">Unit Rate</th>
											<th style="width: 5%;display:none;">Total Amount</th>
											<th style="width: 20%">Description</th>
											<th style="width: 5%">Access Rights</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:if
											test="${RESOURCE_INFO ne null && fn:length(RESOURCE_INFO)>0}">
											<c:forEach var="detail"
												items="${RESOURCE_INFO}" varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
													<td style="width: 10%;display:none;"><select name="resourceType"
														class="width98 resourceType"
														id="resourceType_${status.index+1}">
															<option value="">Select</option>
															<c:forEach var="resourceType" items="${RESOURCE_TYPE}">
																<option value="${resourceType.key}">${resourceType.value}</option>
															</c:forEach>
													</select> <input type="hidden" id="tempresourceType_${status.index+1}"
														value="${detail.resourceType}" /></td>
													<td><select name="resourceDetail"
														class="width72 resourceDetail"
														id="resourceDetail_${status.index+1}">
															<option value="">Select</option>
															<c:forEach var="resourceDetail" items="${RESOURCE_DETAIL}">
																<option value="${resourceDetail.lookupDetailId}">${resourceDetail.displayName}</option>
															</c:forEach>
													</select> <input type="hidden"
														id="tempresourceDetail_${status.index+1}"
														value="${detail.lookupDetail.lookupDetailId}" /> <span
														id="labour_${status.index+1}">
															<c:forEach var="rsDetail" items="${detail.projectResourceDetails}" varStatus="status3">
																<div style="padding: 3px;"
																class="width100 float-left personObject personObject_${status.index+1}"
																id="div_${status3.index+1}${rsDetail.person.personId}${status3.index+1}">
																<span style="margin: 0 5px;"
																	class="float-left count_index" id="countindex_${status3.index+1}${rsDetail.person.personId}${status3.index+1}">${status3.index+1}.</span>
																<span class="width40 float-left">${rsDetail.person.firstName} ${rsDetail.person.lastName}
																	[${rsDetail.person.personNumber}]</span> <input type="hidden" value="${rsDetail.person.personId}"
																	id="personId_${status3.index+1}${rsDetail.person.personId}${status3.index+1}"><span
																	style="cursor: pointer;" id="deleteoption_${status3.index+1}${rsDetail.person.personId}${status3.index+1}"
																	class="deleteoption"> <img width="10" height="10"
																	src="./images/cancel.png"> </span>
															</div>
															</c:forEach>
														</span> <span class="button float-right"
														id="resourceMaterial_${status.index+1}"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="PROJECT_RESOURCE_DETAIL_${status.index+1}"
															class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <span
														class="button float-right"
														id="resourceLabour_${status.index+1}"
														style="height: 16px; display: none;"> <a
															style="cursor: pointer; height: 100%;"
															id="resourceemployee_${status.index+1}"
															class="btn ui-state-default ui-corner-all resource_labour_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
													<td style="width: 10%;display:none;"><input type="text"
														id="resourceCount_${status.index+1}" value="${detail.count}"
														class="width96 resourceCount validate[optional,custom[number]]" />
													</td>
													<td style="width: 10%;display:none;"><input type="text" id="unitPrice_${status.index+1}"
														value="${detail.unitPrice}"
														class="width96 unitPrice right-align validate[optional,custom[number]]" />
													</td>
													<td class="totalamount" id="totalamount_${status.index+1}"
														style="text-align: right;display:none;">${detail.count *	detail.unitPrice}</td>
													<td><input type="text" name="linedescription"
														id="linedescription_${status.index+1}" value="${detail.description}"/>
													</td>
													<td>
														<span class="button float-right"
														id="accessRightsSpan_${status.index+1}"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="accessRightsA_${status.index+1}"
															class="btn ui-state-default ui-corner-all access_rights_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															<input
														type="hidden" id="accessRights_${status.index+1}"
														value="${detail.accessRights}" />	
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status.index+1}"
														style="display: none; cursor: pointer;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status.index+1}"
														style="display: none; cursor: pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a> <input
														type="hidden" id="resourceId_${status.index+1}"
														value="${detail.projectResourceId}" /></td>
												</tr>
											</c:forEach>
										</c:if>
										<c:forEach var="i"
											begin="${fn:length(RESOURCE_INFO)+1}"
											end="${fn:length(RESOURCE_INFO)+2}" step="1">
											<tr class="rowid" id="fieldrow_${i}">
												<td id="lineId_${i}" style="display: none;">${i}</td>
												<td style="width: 10%;display:none;"><select name="resourceType"
													class="width98 resourceType" id="resourceType_${i}">
														<option value="">Select</option>
														<c:forEach var="resourceType" items="${RESOURCE_TYPE}">
															<option value="${resourceType.key}">${resourceType.value}</option>
														</c:forEach>
												</select></td>
												<td><select name="resourceDetail"
													class="width72 resourceDetail" id="resourceDetail_${i}">
														<option value="">Select</option>
														<c:forEach var="resourceDetail" items="${RESOURCE_DETAIL}">
															<option value="${resourceDetail.lookupDetailId}">${resourceDetail.displayName}</option>
														</c:forEach>
												</select> <span id="labour_${i}"></span> <span
													class="button float-right" id="resourceMaterial_${i}"
													style="height: 16px;"> <a
														style="cursor: pointer; height: 100%;"
														id="PROJECT_RESOURCE_DETAIL_${i}"
														class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <span
													class="button float-right" id="resourceLabour_${i}"
													style="height: 16px; display: none;"> <a
														style="cursor: pointer; height: 100%;"
														id="resourceemployee_${i}"
														class="btn ui-state-default ui-corner-all resource_labour_popup width100 float-right">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
												<td style="width: 10%;display:none;"><input type="text" id="resourceCount_${i}"
													class="width96 resourceCount validate[optional,custom[number]]" />
												</td>
												<td style="width: 10%;display:none;"><input type="text" id="unitPrice_${i}"
													class="width96 unitPrice right-align validate[optional,custom[number]]" />
												</td>
												<td class="totalamount" id="totalamount_${i}"
													style="text-align: right;display:none;"></td>
												<td><input type="text" name="linedescription"
													id="linedescription_${i}" />
												</td>
												<td>
														<span class="button float-right"
														id="accessRightsSpan_${i}"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="accessRightsA_${i}"
															class="btn ui-state-default ui-corner-all access_rights_popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															<input
														type="hidden" id="accessRights_${i}"
														value="" />	
												</td>
												<td style="width: 0.01%;" class="opn_td" id="option_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}" style="display: none; cursor: pointer;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}" style="display: none; cursor: pointer;"
													title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}" style="display: none; cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;" title="Working">
														<span class="processing"></span> </a> <input type="hidden"
													id="resourceId_${i}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
					</div>
				</div>
				<!--##############################################################  Tab 3 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3">
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Task Title</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Status</th>
									<th>Milestone</th>
									<th style="width: 0.01%;">Action</th>
								</tr>
							</thead>
							<tbody class="tasktab">
								<c:choose>
								<c:when
									test="${TASKS_INFO ne null && fn:length(TASKS_INFO)>0}">
									<c:forEach var="task"
										items="${TASKS_INFO}" varStatus="status">
										<tr>
											<td>${status.index+1}</td>
											<td>${task.taskTitle}</td>
											<td>${task.fromDate}</td>
											<td>${task.toDate}</td>
											<td>${task.currentStatus}</td>
											<td>${task.projectMileStone.title}</td>
											<td>
												<div
													class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
													>
													<div class="portlet-header ui-widget-header float-right update_task"
														id="updatetask_${status.index+1}" style="cursor: pointer;">
														Update
													</div>
													
												</div>
												<input type="hidden" id="projectTaskId_${status.index+1}" value="${task.projectTaskId}">
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="7">
											No Task Found
										</td>
									</tr>
								</c:otherwise>
								</c:choose>
								
							</tbody>
						</table>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left ">
							<div class="portlet-header ui-widget-header float-left add_task"
								id="add_task" style="cursor: pointer;">
								Add Task
							</div>
							
						</div>
						
					</div>
				</div>
				<!--##############################################################  Tab 4 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4">
					<div class="float-left  width60">
						<div>									
							
							<div id="project_image_information" style="cursor: pointer; color: blue;">
								<u>Upload image here</u>
							</div>
							<div style="padding-top: 10px; margin-top:3px; ">
								<span id="projectImages"></span>
							</div>
							
						</div>	
						<div>
							<div id="project_document_information"
								style="cursor: pointer; color: blue;">
								<u>Upload document here</u>
							</div>
							<div
								style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
								<span id="projectDocs"></span>
							</div>
						</div>
					</div>
				</div>
				<!--##############################################################  Tab 5 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-5">
					<div class=" width100">
						<div class="width35 float-left">
							<div class="width100">
									<textarea id="discussions" name="discussions"
										style="height: 200px;" class="width100"></textarea>
									
							</div>
							<div class="width100">
								<div class=" width90 portlet ui-widget ui-widget-content 
									ui-helper-clearfix ui-corner-all form-container float-right ">
										<div class="portlet-header ui-widget-header float-right task_id"
											id="discussion_message_save" style="cursor: pointer;">
											Submit
										</div>
										
									</div>
							</div>
						</div>
						<div class="width60 float-right discussion-body" id="discussion_list">
							
						</div>
					</div>
				</div>
				
				<!--##############################################################  Tab 6################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-6">
					<div class="float-left  width100">
						<div class="width35 float-left">
							<div class="width100 float-left">
									<textarea id="notes" name="notes"
										style="height: 200px;" class="width100"></textarea>
							</div>
							<div class="width100 float-left">
								<input type="checkbox" class="width5 float-left" name="public_notes" id="public_notes" />
								<label class="width30 float-left" for="public_notes">Public
										</label>
										
								<div class=" width5 portlet ui-widget ui-widget-content 
									ui-helper-clearfix ui-corner-all form-container float-right ">
										<div class="portlet-header ui-widget-header float-right task_id"
											id="notes_save" style="cursor: pointer;">
											Submit
										</div>
										
								</div>
							</div>
						</div>
						<div class="width60 float-right" id="notes_list">
								
						</div>
					</div>
				</div>
				
		<!--##############################################################  Tab 7 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-7">
					<div class="portlet-content payment_schedule"
						style="margin-top: 10px;" id="hrm">
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="paymentchildCount" id="paymentchildCount"
									value="${fn:length(PAYMENT_SCHEDULE_INFO)}" />
								<table id="paymenthastab" class="width100">
									<thead>
										<tr>
											<th style="width: 15%">Payment Type</th>
											<th style="width: 5%">Amount</th>
											<th style="width: 5%">Date</th>
											<th style="width: 5%;display:none;">Mode</th>
											<th style="width: 20%">Description</th>
											<th style="width: 5%;display:none;">Status</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="paymenttab">
										<c:if
											test="${PAYMENT_SCHEDULE_INFO ne null && fn:length(PAYMENT_SCHEDULE_INFO)>0}">
											<c:forEach var="detail"
												items="${PAYMENT_SCHEDULE_INFO}" varStatus="pstatus">
												<tr class="paymentrowid" id="paymentfieldrow_${pstatus.index+1}">
													<td id="paymentlineId_${pstatus.index+1}" style="display: none;">${pstatus.index+1}</td>
													<td>
														<c:choose>
															<c:when test="${detail.status eq 2}">
																<select name="paymentType"
																	class="width80 paymentType" disabled="disabled"
																	id="paymentType_${pstatus.index+1}">
																		<option value="">Select</option>
																		<c:forEach var="paymentType" items="${PAYMENT_TYPES}">
																			<option value="${paymentType.lookupDetailId}">${paymentType.displayName}</option>
																		</c:forEach>
																</select> 
																<input type="hidden"
																	id="tempPaymentType_${pstatus.index+1}"
																	value="${detail.lookupDetail.lookupDetailId}" />
															</c:when>
															<c:otherwise>
																	<select name="paymentType"
																	class="width80 paymentType"
																	id="paymentType_${pstatus.index+1}">
																		<option value="">Select</option>
																		<c:forEach var="paymentType" items="${PAYMENT_TYPES}">
																			<option value="${paymentType.lookupDetailId}">${paymentType.displayName}</option>
																		</c:forEach>
																</select> 
																<input type="hidden"
																	id="tempPaymentType_${pstatus.index+1}"
																	value="${detail.lookupDetail.lookupDetailId}" />
																<span style="height: 16px;" id="paymentType_${pstatus.index+1}" class="button float-right"> 
																<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
																id="CASE_PAYMENT_TYPE_${pstatus.index+1}" style="cursor: pointer; height: 100%;">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
															</c:otherwise>
														</c:choose>	
													</td>
													
													<td>
														 <c:choose>
															 <c:when test="${detail.status eq 2}">
																<input type="text" id="paymentAmount_${pstatus.index+1}"
															value="${detail.amount}" disabled="disabled"
															class="width96 paymentAmount right-align validate[optional,custom[number]]" />
															</c:when>
															<c:otherwise>
																<input type="text" id="paymentAmount_${pstatus.index+1}"
															value="${detail.amount}"
															class="width96 paymentAmount right-align validate[optional,custom[number]]" />
															</c:otherwise>
														</c:choose>
													</td>
													<td>
														 <c:choose>
															 <c:when test="${detail.status eq 2}">
																<input type="text" name="paymentDate" class="paymentDate" disabled="disabled"
															id="paymentDate_${pstatus.index+1}" value="${detail.paymentDateView}"/>
															</c:when>
															<c:otherwise>
																<input type="text" name="paymentDate" class="paymentDate" readonly="readonly"
															id="paymentDate_${pstatus.index+1}" value="${detail.paymentDateView}"/>
															</c:otherwise>
														</c:choose>
													</td>
													<td style="width: 5%;display:none;">
															<select name="paymentMode"
															class="width98 paymentMode"
															id="paymentMode_${pstatus.index+1}">
																<option value="">Select</option>
																<c:forEach var="resourceType" items="${PAYMENT_MODE}">
																	<option value="${resourceType.key}">${resourceType.value}</option>
																</c:forEach>
														</select> <input type="hidden" id="tempPaymentMode_${pstatus.index+1}"
															value="${detail.paymentMode}" />
													</td>
													<td>
														<c:choose>
															 <c:when test="${detail.status eq 2}">
																<input type="text" name="paymentDescription" disabled="disabled"
																	id="paymentDescription_${pstatus.index+1}" value="${detail.description}" />
															</c:when>
															<c:otherwise>
																<input type="text" name="paymentDescription"
																id="paymentDescription_${pstatus.index+1}" value="${detail.description}" />
															</c:otherwise>
														</c:choose>
													
													</td>
													<td style="width: 5%;display:none;">
															<select name="paymentStatus"
															class="width98 paymentStatus"
															id="paymentStatus_${pstatus.index+1}">
																<option value="">Select</option>
																<c:forEach var="sts" items="${PAYMENT_STATUS}">
																	<option value="${sts.key}">${sts.value}</option>
																</c:forEach>
														</select> <input type="hidden" id="tempPaymentStatus_${pstatus.index+1}"
															value="${detail.status}" />
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="paymentoption_${pstatus.index+1}"> 
														 <c:choose>
														 	<c:when test="${detail.status eq 2}">
														 		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip "
																	id="" style="cursor: pointer;background: url('./images/checked.png');
																	background-repeat: no-repeat; cursor: pointer; width:35px!important;height:27px;"
																	title="Paid">
																</a>
														 	</c:when>
														 	<c:otherwise>
																<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip paymentdelrow"
																	id="paymentDeleteImage_${pstatus.index+1}" style="cursor: pointer;"
																	title="Delete Record"> <span
																		class="ui-icon ui-icon-circle-close"></span> 
																</a>
															</c:otherwise>
															</c:choose>
													 <input type="hidden" id="paymentScheduleId_${pstatus.index+1}"
														value="${detail.projectPaymentScheduleId}" />
													</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:forEach var="i"
											begin="${fn:length(PAYMENT_SCHEDULE_INFO)+1}"
											end="${fn:length(PAYMENT_SCHEDULE_INFO)+2}" step="1">
											<tr class="paymentrowid" id="paymentfieldrow_${i}">
													<td id="paymentlineId_${i}" style="display:none;">${i}</td>
													<td>
														<select name="paymentType"
															class="width80 paymentType"
															id="paymentType_${i}">
																<option value="">Select</option>
																<c:forEach var="paymentType" items="${PAYMENT_TYPES}">
																	<option value="${paymentType.lookupDetailId}">${paymentType.displayName}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempPaymentType_${i}"
															value="" />
														<span style="height: 16px;" id="paymentType_${i}" class="button float-right"> 
														<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
														id="CASE_PAYMENT_TYPE_${i}" style="cursor: pointer; height: 100%;">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
													</td>
													
													<td><input type="text" id="paymentAmount_${i}"
														value=""
														class="width96 paymentAmount right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" name="paymentDate" class="paymentDate" readonly="readonly"
														id="paymentDate_${i}" value=""/></td>
													<td style="width: 5%;display:none;">
															<select name="paymentMode"
															class="width98 paymentMode"
															id="paymentMode_${i}">
																<option value="">Select</option>
																<c:forEach var="resourceType" items="${PAYMENT_MODE}">
																	<option value="${resourceType.key}">${resourceType.value}</option>
																</c:forEach>
														</select> <input type="hidden" id="tempPaymentMode_${i}"
															value="" />
													</td>
													<td><input type="text" name="paymentDescription"
														id="paymentDescription_${i}" />
													</td>
													<td style="width: 5%;display:none;">
															<select name="paymentStatus"
															class="width98 paymentStatus"
															id="paymentStatus_${i}">
																<option value="">Select</option>
																<c:forEach var="sts" items="${PAYMENT_STATUS}">
																	<option value="${sts.key}">${sts.value}</option>
																</c:forEach>
														</select> <input type="hidden" id="tempPaymentStatus_${i}"
															value="" />
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="paymentoption_${i}"> 
															<a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip paymentdelrow"
														id="paymentDeleteImage_${i}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
															
													 <input type="hidden" id="paymentScheduleId_${i}"
														value="" />
													</td>
												</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<div id="hrm" class="hastable width100">
									<table class="width100" >
										<tbody>
											<tr>
												<td style="font-weight: bold;float: right;">Total Payment :</td>
												<td id="paymentTotal" style="font-weight: bold;"></td>
											</tr>
										</tbody>
									</table>
								</div>
								<br>
								<div id="hrm" class="hastable width50 float-right">
									<table class="width100">
										<tbody>
											<tr>
												<td>Inventory Total :</td>
												<td id="inventoryTotal"></td>
											</tr>
											<tr>
												<td>Expense Total :</td>
												<td id="expenseTotal"></td>
											</tr>
											<tr>
												<td bgcolor="green" style="font-weight: bold;color: white;">Grand Total :</td>
												<td id="grandTotal" bgcolor="green" style="font-weight: bold;color: white;"></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="hrm" class="width50">
									<fieldset>
									<legend>Expense & Inventory Splits</legend>
									<table class="width100" cellpadding="4" cellspacing="2">
										<tr bordercolor="#595885"><th colspan="6" style="font-size: 15px;font-weight: bold;">Details</th><th style="font-size: 15px;font-weight: bold;">Amount</th></tr>
										<tr><td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>
										<tbody>
										<tr>
											<td colspan="6" style="font-size: 15px;font-weight: bold;">${PROJECT_INFO.referenceNumber} - ${PROJECT_INFO.projectTitle}</td><td></td>
										</tr>
									<c:forEach var="projectExp"
												items="${PROJECT_EXPENSE_INFO}" varStatus="pstatus">
										<tr>
											<td></td><td colspan="5">${projectExp.lookupDetail.displayName}</td><td class="">${projectExp.chargableAmount}</td>
										</tr>
									</c:forEach>
									<c:forEach var="projectInventory"
										items="${INVENTORY_DETAIL}" varStatus="status2">
											<tr>
												<td></td><td colspan="5">${projectInventory.product.code}-${projectInventory.product.productName}</td><td class="">${projectInventory.totalAmount}</td>
											</tr>
									</c:forEach>
									<c:choose>
										<c:when
											test="${TASKS_INFO ne null && fn:length(TASKS_INFO)>0}">
												<c:forEach var="task"
													items="${TASKS_INFO}" varStatus="status">
															<tr>
																<td colspan="2"></td><td colspan="4" style="font-size: 15px;font-weight: bold;">${task.referenceNumber} - ${task.taskTitle}<td>
															</tr>
															<c:forEach var="expense"
																items="${task.projectExpenseVOs}" varStatus="status2">
																<tr>
																	<td colspan="3"></td><td colspan="3">${expense.lookupDetail.displayName}</td><td class="expenseAmountSubTask">${expense.chargableAmount}</td>
																</tr>
															</c:forEach>
															<c:forEach var="inventory"
																items="${task.projectInventoryVOs}" varStatus="status2">
																<tr>
																	<td colspan="3"></td><td colspan="3">${inventory.product.code}-${inventory.product.productName}</td><td class="inventoryAmountSubTask">${inventory.totalAmount}</td>
																</tr>
															</c:forEach>
															
															<c:forEach var="subtask"
																items="${task.subTasks}" varStatus="status1">
																<tr>
																	<td colspan="4"></td><td colspan="2" style="font-size: 15px;font-weight: bold;">${subtask.referenceNumber} - ${subtask.taskTitle}<td>
																</tr>
																	<c:forEach var="expense1"
																		items="${subtask.projectExpenseVOs}" varStatus="status3">
																			<tr>
																				<td colspan="5"></td><td colspan="1">${expense1.lookupDetail.displayName}</td><td class=" expenseAmountSubTask">${expense1.chargableAmount}</td>
																			</tr>
																	</c:forEach>
																	<c:forEach var="inventory1"
																		items="${subtask.projectInventoryVOs}" varStatus="status3">
																			<tr>
																				<td colspan="5"></td><td colspan="1">${inventory1.product.code}-${inventory1.product.productName}</td><td class="inventoryAmountSubTask">${inventory.totalAmount}</td>
																			</tr>
																	</c:forEach>
																
															</c:forEach>
																	
															
														</c:forEach>
												
										</c:when>
									</c:choose>
									</tbody>
									</table>
									</fieldset>
								</div>
					</div>
				</div>
				
				
				<!--##############################################################  Tab 8 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-8">
					<div class="portlet-content project_expense"
						style="margin-top: 10px;" id="hrm">
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="expensechildCount" id="expensechildCount"
									value="${fn:length(PROJECT_EXPENSE_INFO)}" />
								<div class="float-left width100" style="display: none;" id="pettyCashDiv">
									<div class="float-left" style="width: 35%;">
										<label class="width20">Petty Cash<span 
										class="mandatory">*</span></label>
										<input type="text" class="width60" value="${PROJECT_INFO.pettyCash.pettyCashNo}"
											id="pettcash_account" readonly="readonly"/>
										<input type="hidden" id="pettcash_accountid" value="${PROJECT_INFO.pettyCash.pettyCashId}"/>
										<span style="height: 16px; position: relative; right: 40px;" id="pettyCash" class="button float-right"> 
											<a class="btn ui-state-default ui-corner-all pettycash_popup width100 float-right" 
											 style="cursor: pointer; height: 100%;">
												<span class="ui-icon ui-icon-newwin"> </span>
										</a></span>
									</div>
									<div class="float-left width30">
										<div>
											<span>Petty Cash : </span>
											<span id="pettyCashAmount">${PROJECT_INFO.pettyCashAmount}</span>
										</div>
										<div>
											<span>General Expense : </span>
											<span id="generalExpenseAmount"></span>
										</div>
									</div> 
								</div>
								<div class="clearfix"></div>
								<table id="expensehastab" class="width100">
									<thead>
										<tr>
											<th style="width: 15%">Expense Type<span 
										class="mandatory">*</span></th>
										<th style="width: 15%">Expense Mode<span 
										class="mandatory">*</span></th>
											<th style="width: 5%">Amount<span 
										class="mandatory">*</span></th>
											<th style="width: 5%">Date<span 
										class="mandatory">*</span></th>
											<th style="width: 5%">Cost to Client</th>
											<th style="width: 10%">Reference</th>
											<th style="width: 20%">Description</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" /><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip expenseDeleteAll"
														id="expenseDeleteAllImage" style="cursor: pointer;"
														title="Delete All Expense"> <span
															class="ui-icon ui-icon-circle-close"></span> </a></th>
										</tr>
									</thead>
									<tbody class="expensetab">
										<c:if
											test="${PROJECT_EXPENSE_INFO ne null && fn:length(PROJECT_EXPENSE_INFO)>0}">
											<c:forEach var="detail"
												items="${PROJECT_EXPENSE_INFO}" varStatus="pstatus">
												<tr class="expenserowid" id="expensefieldrow_${pstatus.index+1}">
													<td id="expenselineId_${pstatus.index+1}" style="display: none;">${pstatus.index+1}</td>
													<td>
														<select name="expenseType"
															class="width72 expenseType"
															id="expenseType_${pstatus.index+1}">
																<option value="">Select</option>
																<c:forEach var="expenseType" items="${EXPENSE_TYPES}">
																	<option value="${expenseType.lookupDetailId}">${expenseType.displayName}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseType_${pstatus.index+1}"
															value="${detail.lookupDetail.lookupDetailId}" />
														<span style="height: 16px;" id="expenseType1_${pstatus.index+1}" class="button float-right"> 
														<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
														id="CASE_EXPENSE_TYPE_${pstatus.index+1}" style="cursor: pointer; height: 100%;">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
													</td>
													<td>
														<select name="expenseMode"
															class="width72 expenseMode"
															id="expenseMode_${pstatus.index+1}">
																<option value="">Select</option>
																<c:forEach var="expenseMode" items="${EXPENSE_MODES}">
																	<option value="${expenseMode.key}">${expenseMode.value}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseMode_${pstatus.index+1}"
															value="${detail.expenseMode}" /> 
													</td>
													<td><input type="text" id="expenseAmount_${pstatus.index+1}"
														value="${detail.amount}"
														class="width96 expenseAmount right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" name="expenseDate" class="expenseDate" readonly="readonly"
														id="expenseDate_${pstatus.index+1}" value="${detail.paymentDateView}"/></td>
													<td><input type="text" id="costToClient_${pstatus.index+1}"
														value="${detail.chargableAmount}"
														class="width96 costToClient right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" id="expenseReference_${pstatus.index+1}"
														value="${detail.referenceNumber}"
														class="width96 expenseReference" />
													</td>
													<td><input type="text" name="expenseDescription"
														id="expenseDescription_${pstatus.index+1}" value="${detail.description}" />
													</td>
													
													<td style="width: 0.01%;" class="opn_td"
														id="expenseoption_${pstatus.index+1}"> 
															<a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip expensedelrow"
														id="expenseDeleteImage_${pstatus.index+1}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
															
													 <input type="hidden" id="projectExpenseId_${pstatus.index+1}"
														value="${detail.projectExpenseId}" />
													</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:forEach var="i"
											begin="${fn:length(PROJECT_EXPENSE_INFO)+1}"
											end="${fn:length(PROJECT_EXPENSE_INFO)+2}" step="1">
											<tr class="expenserowid" id="expensefieldrow_${i}">
													<td id="expenselineId_${i}" style="display: none;">${i}</td>
													<td>
														<select name="expenseType"
															class="width72 expenseType"
															id="expenseType_${i}">
																<option value="">Select</option>
																<c:forEach var="expenseType" items="${EXPENSE_TYPES}">
																	<option value="${expenseType.lookupDetailId}">${expenseType.displayName}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseType_${i}"
															value="${detail.lookupDetail.lookupDetailId}" />
														<span style="height: 16px;" id="expenseType1_${i}" class="button float-right"> 
														<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
														id="CASE_EXPENSE_TYPE_${i}" style="cursor: pointer; height: 100%;">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
													</td>
													<td>
														<select name="expenseMode"
															class="width72 expenseMode"
															id="expenseMode_${i}">
																<option value="">Select</option>
																<c:forEach var="expenseMode" items="${EXPENSE_MODES}">
																	<option value="${expenseMode.key}">${expenseMode.value}</option>
																</c:forEach>
														</select> 
														<input type="hidden"
															id="tempExpenseMode_${i}"
															value="${1}" /> 
													</td>
													<td><input type="text" id="expenseAmount_${i}"
														value="${detail.amount}"
														class="width96 expenseAmount right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" name="expenseDate" class="expenseDate" readonly="readonly"
														id="expenseDate_${i}" value="${detail.paymentDateView}"/></td>
													<td><input type="text" id="costToClient_${i}"
														value=""
														class="width96 costToClient right-align validate[optional,custom[number]]" />
													</td>
													<td><input type="text" id="expenseReference_${i}"
														value="${detail.referenceNumber}"
														class="width96 expenseReference" />
													</td>
													<td><input type="text" name="expenseDescription"
														id="expenseDescription_${i}" value="${detail.description}" />
													</td>
													
													<td style="width: 0.01%;" class="opn_td"
														id="expenseoption_${i}"> 
															<a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip expensedelrow"
														id="expenseDeleteImage_${i}" style="cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
															
													 <input type="hidden" id="projectExpenseId_${i}"
														value="${detail.projectExpenseId}" />
													</td>
												</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
					</div>
				</div>
				
		<!--##############################################################  Tab 9 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-9">
					<c:choose>
						<c:when	test="${REQUISITION_INFO ne null && fn:length(REQUISITION_INFO)>0}">
							<div id="hrm" class="hastable width100">
								<fieldset>
								<legend>Requisition</legend>
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th>Reference</th>
											<th>Date</th>
											<th>Details</th>
<!-- 											<th style="width: 0.01%;">Action</th>
 -->										</tr>
									</thead>
									<tbody class="requisitiontab">
										
											<c:forEach var="requistion"
												items="${REQUISITION_INFO}" varStatus="status">
												<tr>
													<td>${requistion.referenceNumber}</td>
													<td>${requistion.date}</td>
													<td id="TDRequsitionTable_${status.index+1}" class="TDRequsitionTable">
														Click here to show/hide
														<table id="RequsitionTable_${status.index+1}" style="display: none;">
															<tr>
																<th>Product</th>
																<th>Quantity</th>
																<th>Amount</th>
															</tr>
															<tbody>
																<c:forEach var="detail"
																		items="${requistion.materialRequisitionDetailVOs}" varStatus="status1">
																	<tr>
																		<td>${detail.product.code} - ${detail.product.productName}</td>
																		<td>${detail.quantity}</td>
																		<td>${(detail.projectInventory.quantity * detail.projectInventory.unitRate)-detail.projectInventory.discount}</td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</td>
													<%-- <td>
														<div
															class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
															>
															<div class="portlet-header ui-widget-header float-right update_task"
																id="updateRequisition_${status.index+1}" style="cursor: pointer;">
																Update
															</div>
															
														</div>
														<input type="hidden" id="materialRequisitionId_${status.index+1}" value="${requistion.materialRequisitionId}">
													</td> --%>
												</tr>
											</c:forEach>
									</tbody>
								</table>
								</fieldset>
							</div>
						</c:when>
					</c:choose>
						<div id="hrm" class="hastable width100">
							<fieldset>
								<legend>Assigned Items</legend>
							<div id="discount-warning"
										class="response-msg error ui-corner-all width80"
										style="display: none;"></div>
							<table id="inventoryhastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th style="width: 5%;">Quantity</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 5%;">Discount Method</th>
										<th style="width: 3%;">Discount</th>
										<th style="width: 5%;">Total</th>
										<th style="width: 10%;"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="inventorytab">
									<c:forEach var="DETAIL"
										items="${INVENTORY_DETAIL}"
										varStatus="status">
										<tr class="inventoryrowid" id="inventoryfieldrow_${status.index+1}" >
											<td style="display: none;" id="inventorylineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_product_list_sales_order width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
														
												<input type="hidden" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${status.index+1}"
												value="${DETAIL.suggestedRetailPrice}" />
											</td>
											<td id="uom_${status.index+1}" style="display: none;">${DETAIL.product.lookupDetailByProductUnit.displayName}</td>
											<td id="costingType_${status.index+1}" style="display: none;"></td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.shelf.shelfId}" /> <span
												id="store_${status.index+1}"></span>
												<span class="button float-right"> <a
													style="cursor: pointer; display: none;" id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80"> <input type="hidden"
												name="totalAvailQty" id="totalAvailQty_${status.index+1}" />
											</td>
											<td><input type="text" name="inventoryamount" 
												id="inventoryamount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="inventoryamount width98 validate[optional,custom[number]] right-align">
											</td>
											<td>
												<div class="width80 float-left">
													<c:if test="${DETAIL.isPercentage eq false}">
														<input type="radio" checked="checked" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypeFixed_${status.index+1}" value="false"
														class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
													</c:if>
													<c:if test="${DETAIL.isPercentage eq true || DETAIL.isPercentage eq null}">
														<input type="radio" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypeFixed_${status.index+1}" value="false"
														class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
													</c:if>
												</div>
												<div class="width80 float-left">
													<c:if test="${DETAIL.isPercentage eq true}">
														<input type="radio" checked="checked" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypePercentage_${status.index+1}" value="true"
														class="inventoryDiscountType width10 float-left">
														<label class="width65 float-left">Percentage</label>
													</c:if>
													<c:if test="${DETAIL.isPercentage eq false || DETAIL.isPercentage eq null}">
														<input type="radio" name="inventoryDiscountType_${status.index+1}"
														id="inventoryDiscountTypePercentage_${status.index+1}" value="true"
														class="inventoryDiscountType width10 float-left">
														<label class="width65 float-left">Percentage</label>
													</c:if>
												</div>
												<input type="hidden" id="inventoryDiscountTypeTemp_${status.index+1}" 
												value="${DETAIL.isPercentage}">
											</td>
											<td><input type="text" name="inventoryDiscount"
												id="inventoryDiscount_${status.index+1}" value="${DETAIL.discount}"
												style="text-align: right;"
												class="inventoryDiscount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><c:set var="inventorytotalAmount"
													value="${DETAIL.totalAmount}" /> <span
												id="inventorytotalAmount_${status.index+1}" style="float: right;">${inventorytotalAmount}</span>
											</td>
											<td><input type="text" name="inventoryDescription"
												id="inventoryDescription_${status.index+1}"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td"
												id="inventoryoption_${status.index+1}"> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="inventoryDeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="inventoryWorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="projectInventoryId"
												id="projectInventoryId_${status.index+1}"
												value="${DETAIL.projectInventoryId}" />
												<input
												type="hidden" name="projectInventoryStatus"
												id="projectInventoryStatus_${status.index+1}"
												value="${DETAIL.status}" />
												<input
												type="hidden" name="projectInventoryStatusName"
												id="projectInventoryStatusName_${status.index+1}"
												value="${DETAIL.statusName}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(INVENTORY_DETAIL)+1}"
										end="${fn:length(INVENTORY_DETAIL)+2}"
										step="1" varStatus="status">

										<tr class="inventoryrowid" id="inventoryfieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all show_product_list_sales_order width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
														<input type="hidden" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${i}"
												value="" />
											</td>
											<td id="uom_${i}" style="display: none;"></td>
											<td id="costingType_${i}" style="display: none;"></td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${i}" /> <span id="store_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="storeID_${i}"
													class="btn ui-state-default ui-corner-all common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${i}" class="productQty validate[optional,custom[number]] width80"> <input
												type="hidden" name="totalAvailQty" id="totalAvailQty_${i}" />
											</td>
											<td><input type="text" name="inventoryamount" id="inventoryamount_${i}"
												style="text-align: right;" 
												class="inventoryamount width98 validate[optional,custom[number]] right-align">
											</td>
											<td>
												<div class="width80 float-left">
													<input type="radio" name="inventoryDiscountType_${i}"
													id="inventoryDiscountTypeFixed_${i}" value="false"
													class="inventoryDiscountType width10 float-left"><label class="width65 float-left">Fixed</label>
												</div>
												<div class="width80 float-left">
													<input type="radio" name="inventoryDiscountType_${i}"
													id="inventoryDiscountTypePercentage_${i}" value="true"
													class="inventoryDiscountType width10 float-left">
													<label class="width65 float-left">Percentage</label>
												</div>
											</td>
											<td><input type="text" name="inventoryDiscount"
												id="inventoryDiscount_${i}" value=""
												style="text-align: right;"
												class="inventoryDiscount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><span id="inventorytotalAmount_${i}" style="float: right;"></span>
											</td>
											<td><input type="text" name="inventoryDescription"
												id="inventoryDescription_${i}" class="width98" maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td" id="inventoryoption_${i}">
												 <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="inventoryDeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <input
												type="hidden" name="projectInventoryId"
												id="projectInventoryId_${i}" />
												<input
												type="hidden" name="projectInventoryStatus"
												id="projectInventoryStatus_${i}"
												value="" />
												<input
												type="hidden" name="projectInventoryStatusName"
												id="projectInventoryStatusName_${i}"
												value="" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							</fieldset>
							
						</div>
						<br><br>
						<c:if test="${project_send_requisition_button eq 'show'}">
							<div
								class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left ">
									<div class="portlet-header ui-widget-header float-right"
										id="send_requisition" style="cursor: pointer;">Send Requisition
									</div>
							</div>
						</c:if>
				</div>
				
				<!--##############################################################  Tab 10 ################################################################ -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-10">
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Reference</th>
									<th>Title</th>
									<th>Date</th>
									<th>Status</th>
									<th style="width: 0.01%;">Action</th>
								</tr>
							</thead>
							<tbody class="deliverytab">
								<c:choose>
								<c:when
									test="${DELIVERY_INFO ne null && fn:length(DELIVERY_INFO)>0}">
									<c:forEach var="task"
										items="${DELIVERY_INFO}" varStatus="status">
										<tr>
											<td>${status.index+1}</td>
											<td>${task.referenceNumber}</td>
											<td>${task.deliveryTitle}</td>
											<td>${task.date}</td>
											<td>${task.currentStatus}</td>
											<td>
												<div
													class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
													>
													<div class="portlet-header ui-widget-header float-right update_task"
														id="updatetask_${status.index+1}" style="cursor: pointer;">
														Update
													</div>
													
												</div>
												<input type="hidden" id="projectDeliveryId_${status.index+1}" value="${task.projectDeliveryId}">
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="6">
											No Delivery Found
										</td>
									</tr>
								</c:otherwise>
								</c:choose>
								
							</tbody>
						</table>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left ">
							<div class="portlet-header ui-widget-header float-left add_delivery"
								id="add_delivery" style="cursor: pointer;">
								Add Delivery
							</div>
							
						</div>
						
					</div>
				</div>
					
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right"
					id="project_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<c:if test="${PROJECT_INFO.projectId ne null && PROJECT_INFO.projectId>0}">
					<div class="portlet-header ui-widget-header float-right"
						id="print" style="cursor: pointer;">
						Print
					</div>
				</c:if>
				<c:if test="${sessionScope.project_simple_save_button eq 'show'}">
					<div class="portlet-header ui-widget-header float-right"
						id="project_save" style="cursor: pointer;">
						<fmt:message key="accounts.common.button.save" />
					</div>
				</c:if>
				<c:if test="${sessionScope.project_update_and_save_button eq 'show'}">
					<div class="portlet-header ui-widget-header float-right"
						id="project_save_and_update" style="cursor: pointer;">${sessionScope.project_saveandupdate_stock}
					</div>
				</c:if>
			</div>
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left mileaddrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left paymentaddrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left expenseaddrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left inventoryaddrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="descriptive-popup" style="display: none;"
				class="width100 float-left" id="hrm">
				<div>
					<label class="width40">Descriptive Penalty</label>
					<textarea class="width68" id="penaltyDescription"
						style="height: 150px;">${PROJECT_INFO.penalityDescriptive}</textarea>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="accessrights-popup" style="display: none;" class="width100 float-left" id="hrm">
				<div class="width100 float-left">
					<div class="width100" style="text-align: center;font-size: 30px;font-weight: bold;">Access Rights</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10 accessRightsCheckBox" type="checkbox" name="resouceAccess" id="resouceAccess"/>
							<label class="width30 ">Resource</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="expenseAccess" id="expenseAccess"/>
							<label class="width30 ">Expense</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="inventoryAccess" id="inventoryAccess"/>
							<label class="width30 ">Inventory</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10 accessRightsCheckBox" type="checkbox" name="docuemntAccess" id="docuemntAccess"/>
							<label class="width30 ">Document</label>
							
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10 accessRightsCheckBox" type="checkbox" name="taskAccess" id="taskAccess"/>
							<label class="width30 ">Task</label>
							
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="discussionAccess" id="discussionAccess"/>
							<label class="width30 ">Discussion</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="noteAccess" id="noteAccess"/>
							<label class="width30 ">Note</label>
						</div>
					</div>
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="paymentAccess" id="paymentAccess"/>
							<label class="width30 ">Payment</label>
						</div>
					</div>
					
					<div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="deliveryAccess" id="deliveryAccess"/>
							<label class="width30 ">Delivery</label>
						</div>
					</div>
					<!-- <div class="width100 float-left">
						<div class="width40" style="margin-right: 30px;">
							<input class="width10  accessRightsCheckBox" type="checkbox" name="requisitionAccess" id="requisitionAccess"/>
							<label class="width30 ">Inventory Requisition</label>
						</div>
					</div> -->
				</div>
				<div id="sales-product-inline-popup" style="display: none;"
					class="width100 float-left" id="hrm">
					
				</div>
			</div>
			
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>

			</div>
			
			<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<span class="callJq"></span>
		</div>
		<div align="center" id="content">
		<h1 id="title" style="display: none;"></h1> 
		<%-- <applet id="qz" code="qz.PrintApplet.class"
			archive="${pageContext.request.contextPath}/printing/applet/qz-print.jar" 
			width="0" height="0" align="left"> 
			<param name="jnlp_href" value="${pageContext.request.contextPath}/printing/applet/qz-print_jnlp.jnlp">
			<param name="cache_option" value="plugin">
		</applet> --%>
	</div> 
</div>
<%-- <script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jZebra.js"></script> --%>