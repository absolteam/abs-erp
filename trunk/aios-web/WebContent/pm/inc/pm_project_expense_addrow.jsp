<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="expenserowid" id="expensefieldrow_${rowId}">
	<td id="expenselineId_${rowId}" style="display: none;">${rowId}</td>
	<td>
		<select name="expenseType"
			class="width72 expenseType"
			id="expenseType_${rowId}">
				<option value="">Select</option>
				<c:forEach var="expenseType" items="${EXPENSE_TYPES}">
					<option value="${expenseType.lookupDetailId}">${expenseType.displayName}</option>
				</c:forEach>
		</select> 
		<input type="hidden"
			id="tempExpenseType_${rowId}"
			value="${detail.lookupDetail.lookupDetailId}" />
		<span style="height: 16px;" id="expenseType1_$rowId}" class="button float-right"> 
		<a class="btn ui-state-default ui-corner-all resource_detail_popup width100 float-right" 
		id="CASE_EXPENSE_TYPE_${rowId}" style="cursor: pointer; height: 100%;">
			<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
	</td>
	<td>
		<select name="expenseMode"
			class="width72 expenseMode"
			id="expenseMode_${rowId}">
				<option value="">Select</option>
				<c:forEach var="expenseMode" items="${EXPENSE_MODES}">
					<c:choose>
						<c:when test="${expenseMode.key ne 1}">
							<option value="${expenseMode.key}">${expenseMode.value}</option>
						</c:when>
						<c:otherwise>
							<option value="${expenseMode.key}" selected="selected">${expenseMode.value}</option>
						</c:otherwise>
					</c:choose> 
				</c:forEach>
		</select>  
	</td>
	<td><input type="text" id="expenseAmount_${rowId}"
		value=""
		class="width96 expenseAmount right-align validate[optional,custom[number]]" />
	</td>
	<td><input type="text" name="expenseDate" class="expenseDate" readonly="readonly"
		id="expenseDate_${rowId}" value=""/></td>
	<td><input type="text" id="costToClient_${rowId}"
		value=""
		class="width96 costToClient right-align validate[optional,custom[number]]" />
	</td>
	<td><input type="text" id="expenseReference_${rowId}"
		value="${detail.referenceNumber}"
		class="width96 expenseReference" />
	</td>
	<td><input type="text" name="expenseDescription"
		id="expenseDescription_${rowId}" value="${detail.description}" />
	</td>
	
	<td style="width: 0.01%;" class="opn_td"
		id="expenseoption_${rowId}"> 
			<a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip expensedelrow"
		id="expenseDeleteImage_${rowId}" style="cursor: pointer;"
		title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a>
			
	 <input type="hidden" id="projectExpenseId_${rowId}"
		value="" />
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$('.expenseDate').datepick({showTrigger: '#calImg'}); 
});
</script>