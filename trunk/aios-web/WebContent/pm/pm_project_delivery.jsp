<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var projectDeliveryId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData=""; var deliveryPrint = false;
$(function(){
	 $('.formError').remove();
  	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
	 oTable = $('#ProjectDeliveryList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSort": false,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allprojectdeliverables.action",

		"aoColumns" : [ {  
			"mDataProp" : "referenceNumber"
		 }, {
			 "mDataProp" : "deliveryTitle"
		 }, {
			"mDataProp" : "date"
		 },{
			"mDataProp" : "taskTitle"
		 },{
			"mDataProp" : "projectTitle"
		 },{
			"mDataProp" : "currentStatus"
		 }]
	});	 
		
	$('#add').click(function(){   
		projectDeliveryId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_delivery_entry.action", 
	     	async: false, 
	     	data:{projectDeliveryId: projectDeliveryId, recordId: projectDeliveryId,projectId:Number(0),projectTaskId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
 		if(projectDeliveryId != null && projectDeliveryId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/project_delivery_entry.action", 
		     	async: false, 
		     	data:{projectDeliveryId: projectDeliveryId, recordId: Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(projectDeliveryId != null && projectDeliveryId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/project_delivery_delete.action", 
	     	async: false,
	     	data:{projectDeliveryId: projectDeliveryId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_project_deliverables.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted..")
																			.slideDown(
																					1000);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert("Please select a record to delete.");
								return false;
							}
						});

		/* Click event handler */
		$('#ProjectDeliveryList tbody tr').live('click', function() {
			deliveryPrint = false;
			$('#print').css('opacity', 0.5);
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				projectDeliveryId = aData.projectDeliveryId;
				var status =  $.trim(aData.currentStatus);
				var taskTitle = $.trim(aData.taskTitle);
				if(status == 'Delivered' && taskTitle == ''){
				  deliveryPrint = true;   
	        	  $('#print').css('opacity', 1);
	          	} 
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				projectDeliveryId = aData.projectDeliveryId;
				var taskTitle = $.trim(aData.taskTitle);
				var status =  $.trim(aData.currentStatus);
				if(status == 'Delivered' && taskTitle == ''){
				  deliveryPrint = true;   
	        	  $('#print').css('opacity', 1);
	          	} 
			}
		}); 
		
		$('#print').click(function(){ 
			if(projectDeliveryId != null && projectDeliveryId != 0){
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/project_delivery_print.action", 
			     	async: false, 
			     	data:{projectDeliveryId: projectDeliveryId},
					dataType: "json",
					cache: false,
					success: function(response){ 
						deliveryTemplatePrint(response);
					} 		
				}); 
			}else{
				alert("Please select a record to print."); 
			} 
			return false;
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>deliverables
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
				<div id="project_deliverylist">
					<table id="ProjectDeliveryList" class="display">
						<thead>
							<tr> 
								<th style="width:0.5%">Reference Number</th>
								<th>Title</th>
 								<th>Delivery Date</th>
								<th>Task</th>   
								<th>${sessionScope.project_projectTitle}</th>
								<th>Status</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Add</span>
				</div>
				<div class="sep_li"></div>
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="accounts.common.button.edit" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="add">
					<fmt:message key="accounts.common.button.add" />
				</div>
			</div>
		</div>
	</div>
</div>