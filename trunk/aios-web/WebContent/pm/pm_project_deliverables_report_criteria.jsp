<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;

var selectedClientId = 0;
var selectedLabourId = 0;
var selectedResourceId = 0;
var selectedLeadId = 0;
var selectedStatusId = 0;
var selectedProjectTypeId = 0;

populateDatatable();


$('#clients').combobox({
	selected : function(event, ui) {
		selectedClientId = $('#clients :selected').val();
		populateDatatable();
	}
});

$('#labours').combobox({
	selected : function(event, ui) {
		selectedLabourId = $('#labours :selected').val();
		populateDatatable();
	}
});


$('#resources').combobox({
	selected : function(event, ui) {
		selectedResourceId = $('#resources :selected').val();
		populateDatatable();
	}
});

$('#leads').combobox({
	selected : function(event, ui) {
		selectedLeadId = $('#leads :selected').val();
		populateDatatable();
	}
});

$('#status').combobox({
	selected : function(event, ui) {
		selectedStatusId = $('#status :selected').val();
		populateDatatable();
	}
});

$('#projectType').combobox({
	selected : function(event, ui) {
		selectedProjectTypeId = $('#projectType :selected').val();
		populateDatatable();
	}
});


function populateDatatable(){
	$('#example').dataTable({ 
		"sAjaxSource": "get_project_deliverables_report_list_json.action?clientId="+selectedClientId+"&statusId="+selectedStatusId+"&projectTypeId="
				+selectedProjectTypeId+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "Project Id", "bVisible": false},
			{ "sTitle": "Reference"},
			{ "sTitle": "Delivery Title"}, 
			{ "sTitle": "Date"},
			{ "sTitle": "Project Title"},
			{ "sTitle": "Milestone Title"},
			
	     ], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();
}


$('#example tbody tr').live('click', function () {  
  if ( $(this).hasClass('row_selected') ) {
	  $(this).addClass('row_selected');
      aData =oTable.fnGetData( this );
      selectedRowId=aData[0];
     
  }
  else {
      oTable.$('tr.row_selected').removeClass('row_selected');
      $(this).addClass('row_selected');
      aData =oTable.fnGetData( this ); 
      selectedRowId=aData[0]; 
  }
});


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
			window.open("<%=request.getContextPath()%>/get_project_deliverables_report_XLS.action?projectDeliveryId="+selectedRowId
 					+"&clientId="+selectedClientId+"&statusId="+selectedStatusId+"&projectTypeId="
 					+selectedProjectTypeId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
 	
 	$(".print-call").click(function(){
 			window.open("<%=request.getContextPath()%>/get_project_deliverables_report_printout.action?projectDeliveryId="+selectedRowId
 					+"&clientId="+selectedClientId+"&statusId="+selectedStatusId+"&projectTypeId="
 					+selectedProjectTypeId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=880,height=700,scrollbars=yes,left=100px,top=2px');
 		
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){
 			window.open("<%=request.getContextPath()%>/get_project_deliverables_report_pdf.action?projectDeliveryId="+selectedRowId
 					+"&clientId="+selectedClientId+"&statusId="+selectedStatusId+"&projectTypeId="
 					+selectedProjectTypeId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Project Deliverables Report
		</div>
		<div class="portlet-content">
		<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<table width="100%">
				<tr>
					<td>Client :</td>
					<td>
						<div class="width80">
							<select id="clients">
								<option value="">Select</option>
								<c:forEach var="customer" items="${CUSTOMER_LIST}">
									<option value="${customer.customerId}">${customer.customerName}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
					<td>Project Type :</td>
					<td>
						<div class="width80">
							<select id="projectType" class="width80">
								<option value="">Select</option>
								<c:forEach var="type" items="${PROJECT_TYPE}">
									<option value="${type.lookupDetailId}">${type.displayName} </option>						
								</c:forEach>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td>Status :</td>
					<td>
						<div class="width80">
							<select id="status">
								<option value="">Select</option>
								<c:forEach var="status" items="${PROJECT_STATUS}">
									<option value="${status.key}">${status.value}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="10%" align="center">
						From 
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width80 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td width="10%" align="center">
						To 
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width80 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
			</table>

			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>