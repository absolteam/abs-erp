<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
.buttons {
	margin: 4px;
}
.ui-autocomplete-input {
	width: 40% !important;
}
.ui-combobox-button{
	height: 32px;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;

var selectedProjectId = 0;
var selectedClientId = 0;
var selectedStatus = 0;

populateDatatable();


$('#clientId').combobox({
	selected : function(event, ui) {
		selectedClientId = $('#clientId :selected').val();
		populateDatatable();
	}
});

$('#projectId').combobox({
	selected : function(event, ui) {
		selectedProjectId = $('#projectId :selected').val();
		populateDatatable();
	}
});

function populateDatatable(){
	 
	oTable = $('#ProjectMileStoneCriteria').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allprojectmilestone_criteria.action?recordId="+selectedProjectId
						+"&clientId="+selectedClientId+"&fromDate="+fromDate
						+"&toDate="+toDate+"&projectMileStoneId="+selectedRowId, 
		"aoColumns" : [ {  
			"mDataProp" : "projectTitle"
		 }, {
			 "mDataProp" : "referenceNumber"
		 }, {
			"mDataProp" : "mileStoneTitle"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 },{
			"mDataProp" : "description"
		 },{
			"mDataProp" : "taskName"
		 },{
			"mDataProp" : "taskReference"
		 } ]
	});	 
	selectedRowId = 0;
}

$('#ProjectMileStoneCriteria tbody tr').live('click', function () {  
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
      aData =oTable.fnGetData( this );
      selectedRowId=aData.projectMileStoneId;  
  }
  else {
      oTable.$('tr.row_selected').removeClass('row_selected');
      $(this).addClass('row_selected');
      aData =oTable.fnGetData( this ); 
      selectedRowId=aData.projectMileStoneId;  
  }
});


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		
		return false; 
	});
 	
 	$(".print-call").click(function(){
 			window.open("<%=request.getContextPath()%>/get_projectmilestone_report_printout.action?format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
 		
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
		return false;

	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			project milestone report
		</div>
		<div class="portlet-content">
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="width100 float-left" id="hrm">
				<div class="float-right  width48">
					
					<div>
						<label class="width20">End Date</label> <input type="text" name="toDate" class="toDate width40"
							id="endPicker" readonly="readonly" onblur="triggerDateFilter()" />
					</div>
				</div>
				<div class="float-left  width50">
					<%-- <div>
						<label class="width20">Client</label> <select id="clientId" name="client">
							<option value="">Select</option>
							<c:forEach var="client" items="${CLIENT_DATA}">
								<option value="${client.customerId}">${client.customerName}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width20">Project</label> <select id="projectId" name="projectId">
							<option value="">Select</option>
							<c:forEach var="project" items="${PROJECT_DATA}">
								<option value="${project.projectId}">${project.projectTitle}</option>
							</c:forEach>
						</select>
					</div> --%>
					<div>
						<label class="width20">Start Date</label> <input type="text" name="fromDate"
							class="width40 fromDate" id="startPicker" readonly="readonly"
							onblur="triggerDateFilter()" />
					</div>
				</div>
			</div>
			<div class="tempresult" style="display: none; width: 100%"></div>

			<div id="rightclickarea">
				<div id="project_milestonelist_criteria">
					<table id="ProjectMileStoneCriteria" class="display">
						<thead>
							<tr>
								<th>Project</th>
								<th>Reference Number</th>
								<th>Title</th>
								<th>Start Date</th>
								<th>End Date</th>  
								<th>Description</th>
								<th>Task</th>
								<th>Task Number</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>