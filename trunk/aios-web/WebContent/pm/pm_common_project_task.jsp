<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var projectMileStoneId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	 $('.formError').remove();
	 var projectId=Number($('#projectId').val());
	 oTable = $('#ProjectMileStoneList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "get_common_all_project_task_json.action?projectId="+projectId,

		"aoColumns" : [ {  
			"mDataProp" : "projectTitle"
		 }, {
			"mDataProp" : "taskTitle"
		 }, {
			"mDataProp" : "mileStoneTitle"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 },{
			"mDataProp" : "currentStatus"
		 } ]
	});	 
		
	
		/* Click event handler */
		$('#ProjectMileStoneList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				projectTaskId = aData.projectTaskId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				projectTaskId = aData.projectTaskId;
			}
		});
		
		/* Click event handler */
		$('#ProjectMileStoneList tbody tr').live('dblclick', function () {  
			aData = oTable.fnGetData(this); 
			projectTaskPopupResult(aData);  
			$('#common-popup').dialog("close");
	  	});
		
		$('#milestone-close').click(function () { 
			$('#common-popup').dialog("close");
		});
		
		
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Task
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
				<div id="project_milestonelist">
					<table id="ProjectMileStoneList" class="display">
						<thead>
							<tr>
								<th>Request</th>
								<th>Task</th>
								<th>Milestone</th>
 								<th>Start Date</th>
								<th>End Date</th>  
								<th>Status</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="milestone-close"><fmt:message key="common.button.close"/></div>
			</div>
		</div>
	</div>
</div>