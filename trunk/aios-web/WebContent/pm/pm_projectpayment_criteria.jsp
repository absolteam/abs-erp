<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
.buttons {
	margin: 4px;
}
.ui-autocomplete-input {
	width: 40% !important;
}
.ui-combobox-button{
	height: 32px;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;

var selectedPaymentId = 0;
var selectedClientId = 0;
var selectedStatus = 0;
var selectedProjectId = 0;
var selectedMileStoneId = 0;
populateDatatable();


$('#clientId').combobox({
	selected : function(event, ui) {
		selectedClientId = $('#clientId :selected').val();
		populateDatatable();
	}
});

$('#currentStatus').combobox({
	selected : function(event, ui) {
		selectedClientId = $('#currentStatus :selected').val();
		populateDatatable();
	}
});

$('#projectPaymentId').combobox({
	selected : function(event, ui) {
		selectedPaymentId = $('#projectPaymentId :selected').val();
		populateDatatable();
	}
});

$('#projectId').combobox({
	selected : function(event, ui) {
		selectedProjectId = $('#projectId :selected').val();
		populateDatatable();
	}
});

$('#mileStoneId').combobox({
	selected : function(event, ui) {
		selectedMileStoneId = $('#mileStoneId :selected').val();
		populateDatatable();
	}
});

function populateDatatable(){
	 
	oTable = $('#ProjectPaymentCriteria').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allprojectpayment_criteria.action?recordId="+selectedPaymentId
						+"&clientId="+selectedClientId+"&fromDate="+fromDate
						+"&projectId="+selectedProjectId+"&projectMileStoneId="+selectedMileStoneId
						+"&toDate="+toDate+"&projectPaymentId="+selectedRowId, 
		"aoColumns" : [	{
			 "mDataProp" : "paymentTitle"
		 }, {
			"mDataProp" : "projectTitle"
		 },{
			"mDataProp" : "milestoneTitle"
		 },{
			"mDataProp" : "clientName"
		 },{
			"mDataProp" : "receiptDate"
		 },{
			"mDataProp" : "receiptStatus"
		 } ]
	});	 
	selectedRowId = 0;
}

$('#ProjectPaymentCriteria tbody tr').live('click', function () {  
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
      aData =oTable.fnGetData( this );
      selectedRowId=aData.projectPaymentDetailId;  
  }
  else {
      oTable.$('tr.row_selected').removeClass('row_selected');
      $(this).addClass('row_selected');
      aData =oTable.fnGetData( this ); 
      selectedRowId=aData.projectPaymentDetailId;  
  }
});


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		if(selectedRowId > 0 ) {
			window.open("<%=request.getContextPath()%>/get_projectpayment_report_XLS.action?projectPaymentId="+selectedRowId,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		} else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(2000).slideUp();
 		}
		return false; 
	});
 	
 	$(".print-call").click(function(){
 		if(selectedRowId > 0 ) {
 			window.open("<%=request.getContextPath()%>/get_projectpayment_report_printout.action?projectPaymentId="+selectedRowId+"&format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
 		} else{
 			window.open('<%=request.getContextPath()%>/get_allprojectpayment_report_printout.action','_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
 		}
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		if(selectedRowId > 0 ) {
 			window.open("<%=request.getContextPath()%>/get_projectpayment_report_pdf.action?projectPaymentId="
													+ selectedRowId
													+ "&format=PDF", '_blank',
											'width=800,height=700,scrollbars=yes,left=100px,top=2px');
						} else {
							$('#error_message').hide().html(
									"Please select a record.").slideDown();
							$('#error_message').delay(2000).slideUp();
						}
						return false;

					});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
</script>
<div id="main-content"> 
 	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			project payment report
		</div>
		<div class="portlet-content">
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="width100 float-left" id="hrm">
				<div class="float-right  width48">
					<div>
						<label class="width20">Start Date</label> <input type="text" name="fromDate"
							class="width40 fromDate" id="startPicker" readonly="readonly"
							onblur="triggerDateFilter()" />
					</div>
					<div>
						<label class="width20">End Date</label> <input type="text" name="toDate" class="toDate width40"
							id="endPicker" readonly="readonly" onblur="triggerDateFilter()" />
					</div>
					<div>
						<label class="width20">Status</label>
						 <select id="currentStatus" name="currentStatus">
							<option value="">Select</option>
							<c:forEach var="mapdata" items="${STATUS_DATA}">
								<option value="${mapdata.key}">${mapdata.value}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="float-left  width50">
					<div>
						<label class="width20">Client</label> <select id="clientId" name="client">
							<option value="">Select</option>
							<c:forEach var="client" items="${CLIENT_DATA}">
								<option value="${client.customerId}">${client.customerName}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width20">Project</label> <select id="projectId" name="projectId">
							<option value="">Select</option>
							<c:forEach var="project" items="${PROJECT_DATA}">
								<option value="${project.projectId}">${project.projectTitle}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width20">MileStone</label> <select id="mileStoneId" name="mileStoneId">
							<option value="">Select</option>
							<c:forEach var="milestone" items="${MILESTONE_DATA}">
								<option value="${milestone.projectMileStoneId}">${milestone.milestoneTitle}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<div class="tempresult" style="display: none; width: 100%"></div>

			<div id="rightclickarea">
				<div id="project_paymentlist_criteria">
					<table id="ProjectPaymentCriteria" class="display">
						<thead>
							<tr> 
 								<th>Title</th>
								<th>Project</th>
								<th>MileStone</th>
								<th>Client</th>
								<th>Payment Date</th>  
								<th>Status</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>