<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transiional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>

</head>
<body>
<%
    String ua = request.getHeader("User-Agent");
    if (ua != null && (ua.indexOf("Firefox/") != -1) && request.getParameter("KEY") != null
    		&& request.getParameter("KEY").trim().length() > 0) {
%>
		<form action="<%=request.getContextPath()%>/index_main.action" method="post">
		    <input type="hidden" name="KEY" id="KEY" value="${param.KEY}"/>
		</form>
		<script type="text/javascript">
		    var width = screen.availWidth;
		    var height = screen.availHeight;
			
		    if (parent.main != undefined) {
		       // if (screen.availHeight >= 700) {
		            var win = window.open("", "newWin", "left=0,top=0,width=" + width + ",height=" + height + ",status=yes", false);
		
		            if (!win)
		                parent.main.alert("Allow pop-up in order to start the Application");
		            else {
		                document.forms[0].target = "newWin";
		                document.forms[0].submit();
		                parent.main.document.getElementById("helpText").innerHTML = "You may close this window now, " + 
		                		"closing this window will not effect the application.<br/><br/>" + 
		                		"<span style=\"color: #555; text-transform: uppercase; font-size: 13px;\"><b>CAUTION</b> | Do not Refresh this window while Main Window is Open |</span>";
		                
		            }
		
		       // } else {
		            //parent.main.document.getElementById("helpText").innerHTML = "Available height of your screen is insufficient to run this application. " +
		            	//"Increase \"resolution\" of your screen and then try again ";
		           // parent.main.document.getElementById("helpText").style.color = "red";
		       // }
		    } else {
		    	document.getElementById("helpText").innerHTML = "You are not allowed to access this application from any other browser. Use FIREFOX only";
		    }
		</script>
<%
    }
%>
</body>
</html>