<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.aiotech.aios.system.*"%>
<%@page import="com.aiotech.aios.workflow.domain.entity.Menu"%>
<%@page import="com.aiotech.aios.system.domain.entity.vo.MenuVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%-- <link href="${pageContext.request.contextPath}/js/sitemapstyler/sitemapstyler-mobile.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sitemapstyler/sitemapstyler.js"></script>	 --%>

<style>
.table-custom-design{
	width:100% !important;
	
}
.reportingcurvedContainer{
	/* border:1px solid #C6CACC; */
 	/* -moz-border-radius: 8px;
	border-radius: 8px; */
	float: left; 
	height: auto;
	/* border-style: solid; 
	border-width: thin;  */ 
	width: 100%;
	overflow-y: auto; 
	overflow-x: hidden; 
}
.reportingcurvedContainer_content{
	/* border:1px solid #C6CACC; */
 	/* -moz-border-radius: 8px;
	border-radius: 8px; */
	float: left; 
	height: 100%;
	/* border-style: solid; 
	border-width: thin; */ 
	border-top: 1px solid #eee; 
	width: 99%;
	margin-left: 10px;
	margin-top: 60px;
}
.content_left{width:20%;}
#page-wrapper #main-wrapper #report-content{
	/* border: thin solid #CCCCCC;
	border-radius: 3px 3px 3px 3px;
	height:98%;*/
	z-index: 9996; 
}

* {
	font-family: wf_segoe-ui_normal,'Segoe UI',Segoe,'Segoe WP',Tahoma,"Trebuchet MS", Arial, Helvetica, sans-serif;
}

.tree-toggler > li {
	 border: 1px solid #ddd; 
	 background: #E7E7E7; 
	 padding: 5px; 
	 margin-left: -20px !important; 
	 margin-right: 18px;
}

.tree-toggler  {
	list-style: none;
}

</style>

<div id="report-content">
	
	<%!
		int count = 0;
		String className = "";
		Map<Long, Boolean> menuRights;
		String locale = "en";
		
		void renderReportingMenu(HttpSession session, JspWriter out) throws Exception {
			
			if (session.getAttribute("MENU_MAP") != null) {
			
				try {
					String tempLocale = (String) session.getAttribute("lang");
					if (tempLocale != null)
						locale = tempLocale;
				} catch (Exception e) {
					locale = "en";
				}
				
				Map<String, Object> menuMap = (Map<String, Object>) session.getAttribute("MENU_MAP");			
				List<MenuVO> reportMenuItems = (List<MenuVO>) menuMap.get("REPORT_MENU");
				menuRights = (Map<Long, Boolean>) menuMap.get("MENU_RIGHTS");
				
				List<MenuVO> menu = reportMenuItems;
				Collections.sort(menu);
				int i=1;
				for (MenuVO menuItem : menu) {
					
					if (menuRights.get(menuItem.getMenuItemId()) != null) {											
						
						out.println("<li><a class=\"\" id=\"" + menuItem.getScreen().getScreenPath() 
								+ "\" style=\"cursor: pointer;\" onclick=\"reportAjaxCallFunction(this);\">" 
								+ (locale.equals("ar") ? menuItem.getItemTitleAr()
										: menuItem.getItemTitle()) + "</a>");
						out.println("<ul class=\"tree-toggler nav-header\">");
						
						recursivelyPopulateMenu(menuItem,out);
						
						out.println("</ul>");
						out.println("</li>");
					}
					i++;
				}
			}
		}
		
		void recursivelyPopulateMenu(MenuVO menu, JspWriter out) throws Exception {
			
			if (menuRights.get(menu.getMenuItemId()) != null) {
						
				List<MenuVO> subMenu = menu.getMenus();
				Collections.sort(subMenu);
				
				for(MenuVO subItem : subMenu) {
					
					if (menuRights.get(subItem.getMenuItemId()) != null) {	

						if (subItem.getMenus() == null || subItem.getMenus().size() == 0)	{
							
							className = "";		
							out.println("<li><a id=\"" + subItem.getScreen().getScreenPath() 
									+ "\" onclick=\"reportAjaxCallFunction(this);\" style=\"cursor: pointer;\" class=\"" + className 
									+ "\">" + (locale.equals("ar") ? subItem.getItemTitleAr()
											: subItem.getItemTitle()) + "</a> </li>");						
							continue;
						}
						else {
							className = "";	
							out.println("<li><a id=\"" + subItem.getScreen().getScreenPath() 
									+ "\" onclick=\"reportAjaxCallFunction(this);\" style=\"cursor: pointer;\" class=\"" + className 
									+ "\">" + (locale.equals("ar") ? subItem.getItemTitleAr()
											: subItem.getItemTitle()) + "</a>");
							
							out.println("<ul class=\"nav nav-list tree\">");
							recursivelyPopulateMenu(subItem,out);
							out.println("</ul>");
							out.println("</li>");
						}
					}
				}
			}
		}
	%>
	
	<div id="content" class="reportingcurvedContainer">
		<div class="row" style="margin-top: 65px; margin-left: 0px; margin-bottom: 15px;">
			<div class="well" style="width: 98%; padding: 0 5px; margin: 0 auto; padding-bottom: 15px;">
  					<div style="overflow-y: auto; overflow-x: hidden;">
      					<ul class="nav nav-list">	
						<%	
							renderReportingMenu(session,out);
						%>		
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div style="float: right; right: 17px; top: 65px; position: absolute; z-index:9999; display:none;" id="container-toggle-div"><a href="javascript:divToggle1();" >
	 	<!-- <img src="mobile/back_button.png" width=60px; height=60px; id="left-arrow"/> -->
	 	<button type="button" class="btn btn-primary" style="height: 35px;">Back</button>
	 </a>
 	</div>
	<div class="reportingcurvedContainer_content" style="display: none;">
		<div id="reporting-tree" style="height:100%">
			<div class="process_buttons" style="margin-top:98.3%">
				<div id="hrm" class="width100 float-right ">
				  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
				 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
				  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
				 </div>
			 </div>
		 </div>
	</div>
</div> 

<script type="text/javascript">
	$(function(){
		
		 $('#container-toggle-div').hide();
		 $('#show-hide').click(function(){
			 divToggle();
	     });  
	});
	function divToggle(){
		$('.reportingcurvedContainer_content').show();
		$('.reportingcurvedContainer').hide();
		$('#container-toggle-div').show();
	}
	function divToggle1(){
		$('.reportingcurvedContainer_content').hide();
		$('.reportingcurvedContainer').show();
		$('#container-toggle-div').hide();
	}
	function reportAjaxCallFunction(menuItem){
		
		var requiredAction = $(menuItem).attr('id');
		$.ajax({
			type:"POST",
			url:'<%=request.getContextPath()%>/'+requiredAction+'.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				 divToggle();
				$("#reporting-tree").find(".dataScript").remove();
				//$("#reporting-tree").empty().remove();
				//$(".process_buttons").before("<div id='reporting-tree' style='height:94%''></div>");
				$("#reporting-tree").html(result);
				$('#loading').fadeOut();
				$("#page-wrapper #main-wrapper #main-content").css("margin-right", 0);
			}
			
		});
	}
	$(document).ready(function () {
	    $('label.tree-toggler').click(function () {
	        $(this).parent().children('ul.tree').toggle(300);
	    });
	});
</script>
