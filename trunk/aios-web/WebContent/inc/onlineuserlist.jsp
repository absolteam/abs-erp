<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<c:choose>
	<c:when test="${userList ne null }" >
		<c:forEach items="${userList }" var="users" varStatus="status">
			<li> <a title="events">${users.userName} 
					<span style="float:right;">
						 <c:choose>
						 	<c:when test="${users.userStatus eq 1}" >
						 		<img src="/images/online.jpg" alt="online" width="9">
						 	</c:when>
						 	<c:otherwise>
						 		<img src="/images/offline.jpg" alt="offline">
						 	</c:otherwise>
						 </c:choose>
						  
					</span>
				</a>
			</li>
		</c:forEach>
	</c:when>
</c:choose>