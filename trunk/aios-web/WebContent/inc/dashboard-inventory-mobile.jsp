<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<link rel="stylesheet" type="text/css"
	href="dashboard/jquery-ui.css" />
<link class="include" rel="stylesheet" type="text/css"
	href="dashboard/jquery.jqplot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shThemejqPlot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/aiosChartMobile.css" />

<style type="text/css">
/* .jqplot-point-label {
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
 font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
}
.jqplot-data-label{
	border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
 font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
} */
</style>	
<!-- Don't touch this! -->

<script type="text/javascript" src="dashboard/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.trendline.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.blockRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.enhancedLegendRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pointLabels.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="dashboard/js/aiosChartMobile.js"></script>
<!-- End Don't touch this! -->

<script type="text/javascript">
var  plot9=null;var  plot10=null;var  plot11=null;var  plot12=null;

$(function() {
	setStyle();
	dg9(); 
});

function dg9(){
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'dashboardMonthlyProduction.action',
	      async: false,
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});
	    	  $('#content9_chart').html('');
	      }
	    });
	  
	plot9 = drawBarChartWithoutLegends([s1],ticks,'content9_chart');  
	
	$('#content9_chart').height($('#content9').height()*0.86);
	$('#content9_chart').width($('#content9').width()*0.96);
	plot9.replot( { resetAxes: true } );
	$('#content9').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content9_chart').height($('#content9').height()*0.86);
	 $('#content9_chart').width($('#content9').width()*0.96);
	 plot9.replot( { resetAxes: true } );
	}); 
 	setTimeout(dg10(), 2000);
} 
function dg10(){
	$('#content6_chart img').css('display', 'block');
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'dashboardDailyProduction.action',
	      async: false,
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});
	    		$('#content10_chart').html('');
	      }
	    });

	plot10 = drawBarChartWithoutLegends([s1],ticks,'content10_chart');
	$('#content10_chart').height($('#content9').height()*0.86);
	 $('#content10_chart').width($('#content10').width()*0.96);
	 plot9.replot( { resetAxes: true } );
	 $('#content10').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content10_chart').height($('#content9').height()*0.86);
	 $('#content10_chart').width($('#content9').width()*0.96);
	 plot10.replot( { resetAxes: true } );
	 }); 
}
</script>


</head>

<div id="valids"
	style="display: none; min-height: 200px; width: 150px; display: none; background-color: #FFF; 
		border: 1px solid #D7D7D7; margin: 200px 0 0 1px; position: absolute; z-index:9999;">
	<input id="effectTypes" type="hidden" name="effects" value="blind">

	<div style="width: 150px; background-color: #eee; height: 20px;">
		<input type="checkbox" id="dg_check_all" /><a href="#" id="button">Charts</a>
	</div> 

	<div class="toggler">
		<div id="effect">
			<p>
				<input type="checkbox" id="dg9_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg9')">1.Production (Monthly)</a><br>
				<input type="checkbox" id="dg10_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg10')">2.Production (Daily)</a><br>
			</p> 

		</div>
    </div> 
</div> 

<div id="containment-wrapper">

	<div id='dg9' class="dg">

		<div class='content' id='content9'>
			<div class='bar' id='bar9'>
				<a href="javascript:ReverseDisplay('dg9')"><img border='0'
					src='dashboard/close.png' height='16'>
				</a>
				<div
					class="dashboard-content-title">Monthly
					Production</div>
			</div>
			<div id="content9_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>
	</div>

	<div id='dg10' class="dg">
		<div class='content' id='content10'>
			<div class='bar' id='bar10'><a href="javascript:ReverseDisplay('dg10')">&nbsp;</a>
				<div class="dashboard-content-title">Daily Production</div>
			</div>
			<div id="content10_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>
	</div>
	
</div> 