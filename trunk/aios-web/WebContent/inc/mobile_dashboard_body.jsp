<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
$(function() {
	menuAjaxCallFunction("<div id='workflow_notifications_mobile'></div>");	
});
</script>

<div id="page-wrapper" >
	<div id="page-wrapper-hidden-div" style="display:none;"></div>
	<div class="contact_container">
		<div id="main-wrapper" class="content_left">
			<div id="main-content">
			</div>
		</div>
	</div>
</div>