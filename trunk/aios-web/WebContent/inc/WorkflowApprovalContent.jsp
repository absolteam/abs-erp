<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">

$(function(){  
	
	 
	 $('#loadMore').live('click', function () { 
		 leftsideAlertCall(false);
	 });
	
	 messageId=Number(getParameterByName('processKey'));
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_fetch_by_message.action",
			data: {
	     		messageId:messageId
	     		},
		 	async: false,
		    dataType: "json",
		    cache: false,
		    success:function(response){
		    	//Required param has set in RightSideNavigation page
		    	recordId=response.recordId;
	     		workflowDetailId=response.workflowDetailId;
	     		useCase=response.useCase;
	     		processFlag=response.operationFlagInteger;
				 wfstandardCall();
				 $(".ui-widget-overlay").css("opacity",0.6);
		    }
		});
	 //Call New dashboard call   
});
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
</script>

<div id="page-wrapper" >
	<div id="page-wrapper-hidden-div" style="display:none;"></div>
	<div class="contact_container">
	
		<div id="main-wrapper" class="content_left">
		
			<div id="main-content">
	
			
	
			</div>
		</div>
	</div>
</div>