<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<style>
	.border_class {
		border-style: solid;
		border-color: #f3f3f3;
		border-width: thin;
	}
	.image_box {
		max-height: 206px;
		min-height: 206px;
		max-width: 178px;
		min-width: 178px;
		margin: 25px;
		padding-top: 10px;
		padding-bottom: 5px;
		display: inline-table;
		color: gray;
		font-size: 12px;
		cursor: pointer;
	}
	.templates_preview {
		background-color: white; 
		margin: 20px; 
		padding: 10px;
		display: block;
		min-height: 99%;
	}
	.action_btn, div[style*="cursor"][style*="pointer"], div[class*="buttons"] div {
		background: none;
	}
	.action_btn:hover, div[style*="cursor"][style*="pointer"]:hover, div[class*="buttons"] div:hover {
		background: none;
	}
</style>

<script type="text/javascript">

	var templatesAvailable;

	$(function(){
		
		if('${lang}' == 'ar')
			$('#main-content').css('direction','rtl');
		
		$('.dms_document_information').click(function(){
			AIOS_Uploader.openFileUploader("img","templateDocs","Template","-1","Templates");
		});

		$('#download-popup-screen').dialog({
			minwidth: 'auto',
			width:300,
			height:260,  
			bgiframe: false,
			modal: true,
			autoOpen: false,
			buttons: { 
				"Close": function() {
					$(this).dialog("close"); 
				}
			} 	
		});
		
		var getUploadedTemplates = function() {
			 $.ajax({
				    type : "POST",
				    url : "populateUploadedTemplateFile.action",
				    async : false,
				    dataType : "json",
				    cache : false,
				    success : function(result) {
				    	templatesAvailable = false;
					    var list = result.fileList;
					    for ( var itr = 0; itr < list.length; itr++) {
						    var rec = list[itr];
							$("#" + (rec.fileName.split("_")[0]).split(".")[0]).fadeIn();
							templatesAvailable = true;
						}
					    if(templatesAvailable == true) {
					    	$("#noTemplates").hide();
					    	$("#loadingIcon").hide();
					    } else if (templatesAvailable == false){
					    	$("#loadingIcon").fadeOut();
					    	$("#noTemplates").fadeIn();					    	
					    }
				    },
			 	error : function(result) {
			 		$("#loadingIcon").fadeOut();
			    	$("#noTemplates").fadeIn();			
			 	}
			});
		};
		getUploadedTemplates();
		
		var getSamples = function () {
			
			return ' <html> '
			+ '<body> '
			+ '<div style="margin-top: 5px; padding: 5px; color: activecaption;">'
			+ '<div>'
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/ContractDetails.doc"> ContractDetails.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/OfferLetter.doc"> OfferLetter.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/OfferLetterGovernment.doc"> OfferLetterGovernment.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/OfferLetterMilitary.doc"> OfferLetterMilitary.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/Release.doc"> Release.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/ClearanceLetter.doc"> ClearanceLetter.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/Siteplan.doc"> Siteplan.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '<img height="15px;" width="15px;" src="${pageContext.request.contextPath}/images/MicrosoftWord.png"/>'
			+ '<a href="${pageContext.request.contextPath}/printing/applet/SampleTemplates/ChequePayment.doc"> ChequePayment.doc</a>'
			+ '<br/>'
			+ '<br/>'
			
			+ '</div>'
			+ '</div>'
			+ '</body>'
			+ '</html> '; 
		};
		
		$('#downloadSamples').live('click',function(){ 
		 		 		
	 		$('#download-popup-screen').dialog('open');
			$('#ui-dialog-title-download-popup-screen').text("Download Template Samples");
			$('.download-result').html(getSamples()); 	
			return false;	
		});	
		
		if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true")
			$('#templateStatus').attr('checked', true);
		else
			$('#templateStatus').attr('checked', false);
		
		$('#templateStatus').change(function(){ 
		 		
			var templateStatus = $('#templateStatus').attr('checked');
			
			 $.ajax({
				    type : "POST",
				    url : "templateStatusUpdate.action?templateStatus=" + templateStatus,
					async: false,
					dataType: "html",
					cache: false,
				    success : function(result) {
				    	//alert("updated");
				    },
				    error : function(result) {
				    	alert("Error upating template status.");
				    }
			});
		});
	});

</script>

<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 98%">
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				<fmt:message key="sys.templatesManager" />
			</div>
			
			<div>
				<p style="padding: 15px;">
					<fmt:message key="sys.templates.pretext" /> <span id="downloadSamples" style="color: activecaption; cursor: pointer;"><u><fmt:message key="sys.templates.clickHere" /></u></span> <fmt:message key="sys.templates.posttext" />
				</p>
			</div>
			<div style="margin-left: 30px; margin-right: 30px; font-weight: bold; color: graytext;">
				<input type="checkbox" id="templateStatus"/> <fmt:message key="sys.templates.statusMessage" />
			</div>
			<div align="center" class="templates_preview border_class">
				<div>
					<div>
						<span><b><fmt:message key="sys.templates.currentlyUploaded" /></b></span>	
						<br/>
						<br/>
						<hr/>
					</div>
					
					<span id="ContractDetails" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/contract.png">
						Contract
					</span>
					<span id="ContractDetailsGovernment" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/contract.png">
						Contract(Government)
					</span>
					<span id="ContractDetailsMilitary" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/contract.png">
						Contract(Military)
					</span>
					<span id="OfferLetter" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/offer.png">
						Offer Letter
					</span>
					<span id="OfferLetterGovernment" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/offer.png">
						Offer Letter(Government)
					</span>
					<span id="OfferLetterMilitary" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/offer.png">
						Offer Letter(Military)
					</span>
					<span id="Release" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/release.png">
						Release
					</span>
					<span id="Siteplan" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/transfer.png">
						Site Plan
					</span>
					<span id="ClearanceLetter" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/transfer.png">
						Clearance Letter
					</span>
					<span id="ChequePayment" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/transfer.png">
						Cheque Payment
					</span>
					<span id="EmployeeContract" style="display: none;" class="image_box border_class">
						<%-- <img class="dms_document_information" alt="Upload New Template" style="position: absolute; margin-left: 140px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/upload.png"> --%>
						<%-- <img alt="Delete" style="position: absolute; margin-left: 160px;" height="10px;" width="10px;" src="${pageContext.request.contextPath}/images/delete.gif"> --%>
						<img src="${pageContext.request.contextPath}/images/transfer.png">
						Employee Contract
					</span>
				</div>
				<div id="loadingIcon" style="margin: 30px; color: graytext;" align="center">
					<img src="${pageContext.request.contextPath}/images/loadingTemplates.gif">
				</div>
				<div id="noTemplates" style="margin: 30px; color: graytext; display: none;" align="center">
					<strong>- <fmt:message key="sys.templates.notUploaded" /> -</strong>
				</div>				
				<div class="dms_document_information" style="color: graytext; cursor: pointer; width: 109px;">
					<fmt:message key="sys.templates.manageTemplates" />
				</div>
			</div>
		</div>
		<div id="download-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="download-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
</body>
