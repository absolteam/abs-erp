<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<link rel="stylesheet" type="text/css"
	href="dashboard/jquery-ui.css" />
<link class="include" rel="stylesheet" type="text/css"
	href="dashboard/jquery.jqplot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shThemejqPlot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/aiosChart.css" />

	
<!-- Don't touch this! -->

<script type="text/javascript" src="dashboard/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.trendline.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.blockRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.enhancedLegendRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pointLabels.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="dashboard/js/aiosChart.js"></script>
<!-- End Don't touch this! -->
<style type="text/css">
/* .jqplot-point-label {
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
  font-family: wf_segoe-ui_normal,'Segoe UI',Segoe,'Segoe WP',Tahoma,"Trebuchet MS",Arial,Helvetica,sans-serif;
  font-weight: bold;
  font-size: 12px;
}
.jqplot-data-label{
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
  font-family: wf_segoe-ui_normal,'Segoe UI',Segoe,'Segoe WP',Tahoma,"Trebuchet MS",Arial,Helvetica,sans-serif;
  font-weight: bold;
  font-size: 12px;
} */
.ui-widget-header {
	border: 0px;
	background: none;
}
</style>
<script type="text/javascript">

var user_dashboard = '${DASHBOARD_ACCESS}';

var  plot100=null;var  plot101=null;var  plot102=null;var  plot103=null;
var  plot104=null;var  plot105=null;var  plot106=null;var  plot107=null;
var  plot108=null;var  plot109=null;var  plot110=null;var  plot111=null;
var  plot112=null;var  plot113=null;var  plot114=null;var  plot115=null;
var plot116=null;
var headCountfor='DESIGNATION';var salaryTotalfor='DESIGNATION';
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var storeId=0;
$(function() {
	//Tab content
	 $(".button_left2 a[id^=tab_menu]").hover(function() {
	        var curMenu=$(this);
	        $(".button_left2 a[id^=tab_menu]").removeClass("active");
	        curMenu.addClass("active");

	        var index=curMenu.attr("id").split("tab_menu_")[1];
	        $(".contact_container .tabcontent").css("display","none");
	        $(".contact_container #tab_content_"+index).css("display","block");
	    });
	
	
	
	$('.chart-date').datepick({
		onSelect: customRange, showTrigger: '#calImg'}); 
	
	$('.chart-datedg115').datepick({
		onSelect: customRange1, showTrigger: '#calImg'}); 
		setStyle();
		
		 
		
		 if(findWidgetVisibility("dg100")){
			dg100();
			 $('#content100_chart').height($('#content100').height()*0.86);
			 $('#content100_chart').width($('#content100').width()*0.96);
			 plot100.replot( { resetAxes: true } );
		 }
		 $('#content100').bind('resize', function(event, ui) {
			 $('#content100_chart').height($('#content100').height()*0.86);
			 $('#content100_chart').width($('#content100').width()*0.96);
			 plot100.replot( { resetAxes: true } );
			});
			
			
		 if(findWidgetVisibility("dg101")){
			dg101();
			 $('#content101_chart').height($('#content101').height()*0.86);
			 $('#content101_chart').width($('#content101').width()*0.96);
			 plot101.replot( { resetAxes: true } );
		 }
		 $('#content101').bind('resize', function(event, ui) {
			 $('#content101_chart').height($('#content101').height()*0.86);
			 $('#content101_chart').width($('#content101').width()*0.96);
			 plot101.replot( { resetAxes: true } );
			});
		if(findWidgetVisibility("dg102")){
			dg102();
			 $('#content102_chart').height($('#content102').height()*0.86);
			 $('#content102_chart').width($('#content102').width()*0.96);
			 plot102.replot( { resetAxes: true } );
		}
		
		
		 
		
		 $('#content102').bind('resize', function(event, ui) {
			 $('#content102_chart').height($('#content102').height()*0.86);
			 $('#content102_chart').width($('#content102').width()*0.96);
			 plot102.replot( { resetAxes: true } );
		});
		 if(findWidgetVisibility("dg103")){
				dg103(); 
				 $('#content103_chart').height($('#content103').height()*0.80 - 3);
				 $('#content103_chart').width($('#content103').width() - 1);
			 }
		 
		 $('#content103').bind('resize', function(event, ui) {
			 $('#content103_chart').height($('#content103').height()*0.80 - 3);
			 $('#content103_chart').width($('#content103').width() - 1);
		});
		
		 if(findWidgetVisibility("dg104")){
			 dg104(); 
			 $('#content104_chart').height($('#content104').height()*0.86);
			 $('#content104_chart').width($('#content104').width()*0.96);
			 plot1.replot( { resetAxes: true } );
		 }
		 $('#content104').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content104_chart').height($('#content104').height()*0.86);
		 $('#content104_chart').width($('#content104').width()*0.96);
		 plot104.replot( { resetAxes: true } );
		 }); 
		 
		 if(findWidgetVisibility("dg105")){
			 dg105(); 
			 $('#content105_chart').height($('#content105').height()*0.86);
			 $('#content105_chart').width($('#content105').width()*0.96);
			 plot105.replot( { resetAxes: true } );
		 }
		 $('#content105').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content105_chart').height($('#content105').height()*0.86);
		 $('#content105_chart').width($('#content105').width()*0.96);
		 plot105.replot( { resetAxes: true } );
		});
		 
		 if(findWidgetVisibility("dg106")){
			 dg106(); 
			 $('#content106_chart').height($('#content106').height()*0.86);
			 $('#content106_chart').width($('#content106').width()*0.96);
			 plot106.replot( { resetAxes: true } );
		 }
		 $('#content106').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
			 $('#content106_chart').height($('#content106').height()*0.86);
			 $('#content106_chart').width($('#content106').width()*0.96);
			 plot106.replot( { resetAxes: true } );
		 }); 
		 
		 if(findWidgetVisibility("dg107")){
			 dg107();
			 $('#content107_chart').height($('#content107').height()*0.86);
			 $('#content107_chart').width($('#content107').width()*0.96);
			 plot107.replot( { resetAxes: true } );
		 }
		 $('#content107').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
			 $('#content107_chart').height($('#content107').height()*0.86);
			 $('#content107_chart').width($('#content107').width()*0.96);
			 plot107.replot( { resetAxes: true } );
		});
		 
		 if(findWidgetVisibility("dg108")){
			 dg108();
			 $('#content108_chart').height($('#content108').height()*0.86);
			 $('#content108_chart').width($('#content108').width()*0.96);
			 plot108.replot( { resetAxes: true } );
		 }
		 $('#content108').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
			  // which should be recomputed based on new plot size.
			 $('#content108_chart').height($('#content108').height()*0.86);
			 $('#content108_chart').width($('#content108').width()*0.96);
			 plot108.replot( { resetAxes: true } );
		});
		 
		 if(findWidgetVisibility("dg109")){
			 dg109();
			 $('#content109_chart').height($('#content109').height()*0.86);
			 $('#content109_chart').width($('#content109').width()*0.96);
			 plot109.replot( { resetAxes: true } );
		 }
		 $('#content109').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content109_chart').height($('#content109').height()*0.86);
		 $('#content109_chart').width($('#content109').width()*0.96);
		 plot109.replot( { resetAxes: true } );
		 }); 
		 
		 if(findWidgetVisibility("dg110")){
			 dg110();
			 $('#content110_chart').height($('#content110').height()*0.86);
			 $('#content110_chart').width($('#content110').width()*0.96);
			 plot110.replot( { resetAxes: true } );
		 }
		 $('#content110').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content110_chart').height($('#content110').height()*0.86);
		 $('#content110_chart').width($('#content110').width()*0.96);
		 plot110.replot( { resetAxes: true } );
		});
		 
		 if(findWidgetVisibility("dg113")){
			 dg113();
			 $('#content113_chart').height($('#content113').height()*0.83);
			 $('#content113_chart').width($('#content113').width()*0.96);
			 plot113.replot( { resetAxes: true } );
		 }
		 $('#content113').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
			 $('#content113_chart').height($('#content113').height()*0.83);
			 $('#content113_chart').width($('#content113').width()*0.96);
			 plot113.replot( { resetAxes: true } );
		 }); 
		 
		 if(findWidgetVisibility("dg114")){
			 dg114();
			 $('#content114_chart').height($('#content114').height()*0.86);
			 $('#content114_chart').width($('#content114').width()*0.96);
			 plot114.replot( { resetAxes: true } );
		 }
		 $('#content114').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content114_chart').height($('#content114').height()*0.86);
		 $('#content114_chart').width($('#content114').width()*0.96);
		 plot114.replot( { resetAxes: true } );
		});
		 
		 if(findWidgetVisibility("dg116")){
			 dg116();
			 $('#content116_chart').height($('#content116').height()*0.83);
			 $('#content116_chart').width($('#content116').width()*0.96);
			 plot116.replot( { resetAxes: true } );
		 }
		 
		 $('#content116').bind('resize', function(event, ui) {
			  // pass in resetAxes: true option to get rid of old ticks and axis properties
			  // which should be recomputed based on new plot size.
			 $('#content116_chart').height($('#content116').height()*0.83);
			 $('#content116_chart').width($('#content116').width()*0.96);
			 plot116.replot( { resetAxes: true } );
		 }); 
		 
		 $('.headCountOption').change(function(){
			 headCountfor=$(this).val();
			 dg113();
			 $('#content113_chart').height($('#content113').height()*0.83);
			 $('#content113_chart').width($('#content113').width()*0.96);
			 plot113.replot( { resetAxes: true } );
		 });
		 
		 $('.headCountOptionPie').change(function(){
			 headCountfor=$(this).val();
			 dg116();
			 $('#content116_chart').height($('#content116').height()*0.86);
			 $('#content116_chart').width($('#content116').width()*0.96);
			 plot116.replot( { resetAxes: true } );
		 });
		 
		 $('.salaryOption').change(function(){
			 salaryTotalfor=$(this).val();
			 dg114();
			 $('#content114_chart').height($('#content114').height()*0.86);
			 $('#content114_chart').width($('#content114').width()*0.96);
			 plot114.replot( { resetAxes: true } );
		 });
		 
		 if(findWidgetVisibility("dg115")){
			dg115();
			 $('#content115_chart').height($('#content115').height()*0.86);
			 $('#content115_chart').width($('#content115').width()*0.96);
		}
		 
		 $('#content115').bind('resize', function(event, ui) {
			 $('#content115_chart').height($('#content115').height()*0.86);
			 $('#content115_chart').width($('#content115').width()*0.96);
		}); 
		 
		 $('.chart-datedg115').change(function(event, ui) {
			 dg115();
		 }); 
		 
});
function customRange(){
	dg102();
	$('#content102_chart').height($('#content102').height()*0.86);
	$('#content102_chart').width($('#content102').width()*0.96);
	 plot102.replot( { resetAxes: true } );
}

function customRange1(){
	dg115();
}
	function dg100(){
		var s1 = [];
		var s2 = [];
		var s3 = [];
		var s4 = [];
		var s5 = [];
		var ticks=[];
		$.ajax({
			  type:"POST",
				url:'getAllFiscalTransaction.action',
		      async: false,
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						ticks.push(response.data[index].ticks);
				    	var voList=response.data[index].dashboardVOs;
						s1.push(Number(voList[0].amount));	
						s2.push(Number(voList[1].amount));
						s3.push(Number(voList[2].amount));
						s4.push(Number(voList[3].amount));
						s5.push(Number(voList[4].amount));
					});

		      }
		    });
	  var legendLabels = ['Asset', 'Liability', 'Revenue','Expense','Owners Equity'];
	  plot100 = drawBarChart([s1,s2,s3,s4,s5],ticks,'content100_chart',legendLabels);

	}
	
	function dg101(){
		var s1=[];
		var ticks=[];
		  $.ajax({
			  type:"POST",
				url:'dashboardDailySales.action',
		      async: false,
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						s1.push(Number(response.data[index].amount));
						ticks.push(response.data[index].startDateView);
					});

		      }
		    });
	
		  plot101 = drawBarChartWithoutLegends([s1],ticks,'content101_chart');
		 }
	function dg102(){
		/* var data=[[['Sandwiches', 45000],['Puddings', 125600], ['Salads', 62045], 
				    ['Cakes', 15000],['Mamouls, Cookies & Others', 76500],['Breads', 13500],['Customized Drinks', 3500]
				,['Beverages', 200],['Other', 200]]]; */
			
		var data=[];
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		  $.ajax({
			  type:"POST",
				url:'salesByCategory.action',
		      async: false,
		      data:{startDate:startDate,endDate:endDate},
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						data.push([response.data[index].name,Number(response.data[index].amount)]);	
					});

		      }
		    });
		  plot102 = drawPieChart([data],'content102_chart');
 }
function dg103(){
	workflowcontentCall();
}
	function workflowcontentCall(){
		// ----------Workflow Notification approval------------------------
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_notification.action",
			data: {
	     			processFlag:3
	     		},
		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){
		 		 $("#workflowcontent-approval").html(result);
		 		 var count=Number($("#mainrecordCount_3").val());
		 		 if(count>0){
		 		 }else{
		 			count=0; 
		 		 }
		 		$("#approval_count").html("<p style='margin-top: -10px; font-size: small ! important;'>"+count+"</p>");
		 		
			},error : function(result) {
				$("#approval_count").html(
						"<p style='margin-top: -10px; font-size: small ! important;'>"
								+ 0 + "</p>");
			}
		});
		// ----------Workflow Notification approved------------------------
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_notification.action",
			data: {
					processFlag:8
	     		},
		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){
		 		 $("#workflowcontent-approved").html(result);
		 		 var count1=Number($("#mainrecordCount_8").val());
		 		 if(count1>0){
		 		 }else{
		 			count1=0; 
		 		 }
			 	$("#approved_count").html("<p style='margin-top: -10px; font-size: small ! important;'>"+count1+"</p>");
			},error : function(result) {
				$("#approved_count").html(
						"<p style='margin-top: -10px; font-size: small ! important;'>"
								+ 0 + "</p>");
			}
		});
		// -------Alert----------------------------
			leftsideAlertCall(true);

		}
		
		function leftsideAlertCall(applyLimit){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/workflow_alert.action",
					async : true,
					data:{applyLimit:applyLimit},
					dataType : "html",
					cache : false,
					success : function(result) {
						$("#workflowcontent-alert").html(result);
						var alertcount = 0;
						alertcount = Number($("#alertCount").val());
						if (alertcount > 0) {
						} else {
							alertcount = 0;
						}
						$("#alert_count").html(
								"<p style='margin-top: -10px; font-size: small ! important;'>"
										+ alertcount + "</p>");
					},error : function(result) {
						$("#alert_count").html(
								"<p style='margin-top: -10px; font-size: small ! important;'>"
										+ 0 + "</p>");
					}
				});
			
		}
		
		function dg104(){
			var s1 = [];
			var s2 = [];
			var s3 = [];
			var s4 = [];
			var s5 = [];
			var ticks=[];
			$.ajax({
				  type:"POST",
					url:'getAllFiscalTransactionByPeriod.action',
			      async: false,
			      dataType:"json",
			      success: function(response) {
			    	  $(response.data)
						.each(function(index) {
							ticks.push(response.data[index].ticks);
					    	var voList=response.data[index].dashboardVOs;
							s1.push(Number(voList[0].amount));	
							s2.push(Number(voList[1].amount));
							s3.push(Number(voList[2].amount));
							s4.push(Number(voList[3].amount));
							s5.push(Number(voList[4].amount));
						});

			      }
			    });
		  var legendLabels = ['Asset', 'Liability', 'Revenue','Expense','Owners Equity'];
		  plot104 = drawBarChart([s1,s2,s3,s4,s5],ticks,'content104_chart',legendLabels);
		    
	}
	function dg105(){
		var s1 = [];
		var s2 = [];
		var s3 = [];
		var s4 = [];
		var s5 = [];
		var ticks=[];
		$.ajax({
			  type:"POST",
				url:'getAllFiscalTransactionByCalendar.action',
		      async: false,
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						ticks.push(response.data[index].ticks);
				    	var voList=response.data[index].dashboardVOs;
						s1.push(Number(voList[0].amount));	
						s2.push(Number(voList[1].amount));
						s3.push(Number(voList[2].amount));
						s4.push(Number(voList[3].amount));
						s5.push(Number(voList[4].amount));
					});

		      }
		    });
	  var legendLabels = ['Asset', 'Liability', 'Revenue','Expense','Owners Equity'];
	  plot105 = drawBarChart([s1,s2,s3,s4,s5],ticks,'content105_chart',legendLabels);
}
function dg106(){
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'dashboardMonthlySales.action',
	      async: false,
	      data:{storeId:storeId},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});

	      }
	    });
	  
	  plot106 = drawBarChartWithoutLegends([s1],ticks,'content106_chart');
	    
}
function dg107(){
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'dashboardYearlySales.action',
	      async: false,
	      data:{storeId:storeId},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});

	      }
	    });
	  plot107 = drawBarChartWithoutLegends([s1],ticks,'content107_chart');
}
function dg108(){
	/* var data=[[['Sandwiches', 45000],['Puddings', 125600], ['Salads', 62045], 
			    ['Cakes', 15000],['Mamouls, Cookies & Others', 76500],['Breads', 13500],['Customized Drinks', 3500]
			,['Beverages', 200],['Other', 200]]]; */
		
	var data=[null];
	var startDate=$('#startDate').val();
	var endDate=$('#endDate').val();
	  $.ajax({
		  type:"POST",
			url:'salesByCategory.action',
	      async: false,
	      data:{startDate:startDate,endDate:endDate,storeId:storeId},
	      dataType:"json",
	      success: function(response) {
	    	  data=[];
	    	  $(response.data)
				.each(function(index) {
					data.push([response.data[index].name,Number(response.data[index].amount)]);	
				});

	      }
	    });
	  plot108 = drawPieChart([data],'content108_chart');
}

function dg109(){
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'dashboardMonthlyProduction.action',
	      async: false,
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});

	      }
	    });
	  
	  plot109 = drawBarChartWithoutLegends([s1],ticks,'content109_chart');
	    
}

function dg110(){
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'dashboardDailyProduction.action',
	      async: false,
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});

	      }
	    });

	  plot110 = drawBarChartWithoutLegends([s1],ticks,'content110_chart');
	 }
	 
function dg113(){
	
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'employeeHeadCounts.action',
	      async: false,
	      data:{name:headCountfor},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].count));
					ticks.push(response.data[index].name);
				});

	      }
	    });
	  
	  plot113 = drawBarChartWithoutLegends([s1],ticks,'content113_chart');
	    
}

function dg114(){
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'wagesByDepartment.action',
	      async: false,
	      data:{name:salaryTotalfor},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].name);
				});

	      }
	    });
	  plot114 = drawBarChartWithoutLegends([s1],ticks,'content114_chart');
	    

}
function dg115(){
	listCall();
}

function listCall(){
	var params="id=0";
	var startDate=$('#startDatedg115').val();
	var endDate=$('#endDatedg115').val();
	params+="&startDate="+startDate;
	params+="&endDate="+endDate;
	$('#AbsentTable').dataTable().fnDestroy();
	oTable = $('#AbsentTable')
	.dataTable(
			{
				"bJQueryUI" : true,
				"bPaginate" : true,
				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bAutoWidth": false,
				"sScrollY": $("#dg115").height()-185,
				"bDestroy" : true,
				"sAjaxSource" : "<%=request.getContextPath()%>/getAbsenteesmByPeriod.action?"+params,
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				"aoColumns" : [{
					"mDataProp" : "personName"
				},{
					"mDataProp" : "designationName"
				}, {
					"mDataProp" : "attedanceDate"
				}]

	}); 
	
}

function dg116(){
	var s1=[];
	  $.ajax({
		  type:"POST",
			url:'employeeHeadCounts.action',
	      async: false,
	      data:{name:headCountfor},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push([response.data[index].name,Number(response.data[index].count)]);	
				});

	      }
	    });
	  plot116 = drawPieChartLegendByRight([s1],'content116_chart');
	  //plot13 = drawBarChartWithoutLegends([s1],ticks,'content13_chart');
	    
}
</script>


</head>

<div id="containment-wrapper">

	<div id='dg100' class="dg">
		<div class='content' id='content100' style='width:486px;'>
			<div class='bar' id='bar100'><a href="javascript:ReverseDisplay('dg100')">&nbsp;</a>
				<div class="dashboard-content-title">Ledger Statement</div>
			</div>
			<div id="content100_chart"></div>
		</div>
	</div>
	
	<div id='dg101' class="dg">
		<div class='content' id='content101' style='width:486px;'>
			<div class='bar' id='bar101'><a href="javascript:ReverseDisplay('dg101')">&nbsp;</a>
				<div class="dashboard-content-title">Daily Sales</div>
			</div>
			<div id="content101_chart"></div>
		</div>
	</div>
	
	<div id='dg102' class="dg">
		<div class='content' id='content102' style='width:486px;'>
			<div class='bar' id='bar102'><a href="javascript:ReverseDisplay('dg102')">&nbsp;</a>
				<div class="dashboard-content-title">Sales By Category</div>
			</div>
			<div>
				<label class="dashboard-content-filter-label">Start :
				</label>
				<input type="text" id="startDate" name="startDate" class="chart-date" value="${startDate}">
				<label class="dashboard-content-filter-label">End :
				</label>
				<input type="text" id="endDate" name="endDate" class="chart-date" value="${endDate}">
			</div>
			<div id="content102_chart"></div>
		</div>
	</div>
	<div id='dg103' class="dg">
		<div class='content' id='content103' style='width:486px;max-height:80%!important;'>
			<div class='bar' id='bar103'><a href="javascript:ReverseDisplay('dg103')">&nbsp;</a>
				<div class="dashboard-content-title">Notification</div>
			</div>
			<div id="dashboard_buttons_div"  style="width:100%; float:left;position: relative;">
					<div class="button_left2" style="float:left;width:30%;">
						<div>
							<a id="tab_menu_1" class="active" style="display:inline-table;">
								<p>
									<span id="approval_count"></span>
								</p>
				
							</a>
						</div>
						<fmt:message key="common.dashboard.waitingforapproval" />
					</div>
					<div class="button_left2" style="float:left;width:30%;">
							<div>
							<a id="tab_menu_2" href="#" class="" style="display:inline-table;"><p>
									<span id="approved_count"></span>
								</p></a>
							</div>
							
							Approved Or Rejected
						
						<%-- <fmt:message key="common.dashboard.approved/rejected"/> --%>
					</div>
			
					<div class="button_left2" style="float:left;width:30%;">
						<div>
							<a id="tab_menu_3" href="#" class="" style="display:inline-table;"><p>
									<span id="alert_count"></span>
							</p></a>
						</div>
						&nbsp;&nbsp;&nbsp;<fmt:message key="common.dashboard.alerts" />
					</div>
					<div id="line" style="width:92%; float:left;position: relative;"></div>
				</div>
			<div id="content103_chart" style="overflow-y:auto;overflow-x:hidden;float:left;">
				
				<div class="tabcontent" id="tab_content_1" >
	
					<div id="workflowcontent-approval"></div>
	
				</div>
	
	
				<div class="tabcontent" id="tab_content_2" style="display: none;">
	
					<div id="workflowcontent-approved"></div>
	
				</div>
	
	
				<div class="tabcontent" id="tab_content_3" style="display: none;">
	
					<div id="workflowcontent-alert"></div>
	
				</div>
			</div>
		</div>
	</div>
	<div id='dg104' class="dg">

		<div class='content' id='content104'>
			<div class='bar' id='bar104'>
				<a href="javascript:ReverseDisplay('dg104')"><img border='0'
					src='dashboard/close.png' height='16'>
				</a>
				<div
					style='color: #666; width: auto; height: 20px; text-align: right; overflow: hidden; float: right; padding-top: 5px; padding-right: 5px;'>Periodic Ledger</div>
			</div>
			<div id="content104_chart"></div>
		</div>
	</div>

	<div id='dg105' class="dg">
		<div class='content' id='content105' style='width:486px;'>
			<div class='bar' id='bar105'><a href="javascript:ReverseDisplay('dg105')">&nbsp;</a>
				<div class="dashboard-content-title">Fiancial Year</div>
			</div>
			<div id="content105_chart"></div>
		</div>
	</div>
	
	<div id='dg106' class="dg">
		
		<div class='content' id='content106'>
			<div class='bar' id='bar106'>
				<a href="javascript:ReverseDisplay('dg106')"><img border='0'
					src='dashboard/close.png' height='16'>
				</a>
				<div
					class="dashboard-content-title">Monthly
					Sales</div>
			</div>
			
			<div id="content106_chart"></div>
		</div>
	</div>

	<div id='dg107' class="dg">
		<div class='content' id='content107' style='width:486px;'>
			<div class='bar' id='bar107' ><a href="javascript:ReverseDisplay('dg107')">&nbsp;</a>
				<div class="dashboard-content-title">Yearly Sales</div>
			</div>
			<div id="content107_chart"></div>
		</div>
	</div>
	<div id='dg108' class="dg">
		<div class='content' id='content108' style='width:486px;'>
			<div class='bar' id='bar108' ><a href="javascript:ReverseDisplay('dg108')"></a>
				<div class="dashboard-content-title">Sales By Category</div>
			</div>
			<div>
				<label class="dashboard-content-filter-label">Start :
				</label>
				<input type="text" id="startDate" name="startDate" class="chart-date" value="${startDate}">
				<label class="dashboard-content-filter-label">End :
				</label>
				<input type="text" id="endDate" name="endDate" class="chart-date" value="${endDate}">
			</div>
			<div id="content108_chart"></div>
		</div>
	</div>
	<div id='dg109' class="dg">

		<div class='content' id='content109'>
			<div class='bar' id='bar109'>
				<a href="javascript:ReverseDisplay('dg109')"><img border='0'
					src='dashboard/close.png' height='16'>
				</a>
				<div
					class="dashboard-content-title">Monthly
					Production</div>
			</div>
			<div id="content109_chart"></div>
		</div>
	</div>

	<div id='dg110' class="dg">
		<div class='content' id='content110' style='width:486px;'>
			<div class='bar' id='bar110'><a href="javascript:ReverseDisplay('dg110')">&nbsp;</a>
				<div class="dashboard-content-title">Daily Production</div>
			</div>
			<div id="content110_chart"></div>
		</div>
	</div>
	
	<div id='dg113' class="dg">
		<div class='content' id='content113'>
			<div class='bar' id='bar113'>
				<a href="javascript:ReverseDisplay('dg113')">
				</a>
				<div
					class="dashboard-content-title">Head Counts</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" name="headCountOption" checked="checked" class="headCountOption" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="headCountOption" class="headCountOption" value="DEPARTMENT"/></div>
			<div id="content113_chart"></div>
		</div>
	</div>

	<div id='dg114' class="dg">
		<div class='content' id='content114' style='width:486px;'>
			<div class='bar' id='bar114'><a href="javascript:ReverseDisplay('dg114')">&nbsp;</a>
				<div class="dashboard-content-title">Wages Detail</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" checked="checked" name="salaryOption" class="salaryOption" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="salaryOption" class="salaryOption"  value="DEPARTMENT"/></div>
			<div id="content114_chart"></div>
		</div>
	</div>
	<div id='dg115' class="dg">
		<div class='content' id='content115' style='width:486px;'>
			<div class='bar' id='bar115'><a href="javascript:ReverseDisplay('dg115')">&nbsp;</a>
				<div class="dashboard-content-title">Absenteesm</div>
			</div>
			<div>
				<label class="dashboard-content-filter-label">&nbsp;&nbsp;&nbsp;Start :
				</label>
				<input type="text" id="startDatedg115" class="chart-datedg115" value="${startDate}">
				<label class="dashboard-content-filter-label">&nbsp;&nbsp;End :
				</label>
				<input type="text" id="endDatedg115" class="chart-datedg115" value="${endDate}">
			</div>
			<div id="content115_chart">
				<table class="display" id="AbsentTable">
					<thead>
						<tr>
							<th>Employee</th>
							<th>Designation</th>
							<th>Date</th>
						</tr>
					</thead>
					

				</table>
			</div>
		</div>
	</div>
	<div id='dg116' class="dg">
		<div class='content' id='content116'>
			<div class='bar' id='bar116'>
				<a href="javascript:ReverseDisplay('dg116')">
				</a>
				<div
					class="dashboard-content-title">Head Counts(Pie)</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" name="headCountOption" checked="checked" class="headCountOptionPie" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="headCountOption" class="headCountOptionPie" value="DEPARTMENT"/></div>
			<div id="content116_chart"></div>
		</div>
	</div>
	<div id='dg117' class="dg">
		<div class='content' id='content117'>
			<div class='bar' id='bar117'>
				<a href="javascript:ReverseDisplay('dg117')">
				</a>
				<div
					class="dashboard-content-title">Quick Links</div>
			</div>
			<div style="padding: 10px;">
				<ul class="quick_div">
					<c:forEach items="${QUICK_LINKS}" var="quick_link">
						<li id="${quick_link.value}" onclick="menuAjaxCallFunction(this)">${quick_link.key}</li>
					</c:forEach>					
				</ul>
			</div>
		</div>
	</div>
</div>
<div id="valids"
	style="display: none; min-height: 81px; width: 150px; display: none; background-color: #F4F4F4; border-bottom: 1px solid #D7D7D7; border-top: 1px solid #D7D7D7; border-right: 1px solid #D7D7D7; margin: -400px 0 0 1px; position: absolute; z-index:9999; box-shadow: 0px 0px 4px 0px #ccc !important">
	<input id="effectTypes" type="hidden" name="effects" value="blind">

	<div style="width: 150px; background-color: #eee; height: 20px;">
		<input type="checkbox" id="dg_check_all" /><a href="#" style="margin-top:5px;" id="button">Widgets</a>
	</div>

	<div class="toggler">
		<div id="effect">
			<p>
				<input type="checkbox" id="dg103_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg103')">1.Notification</a><br>
				<!-- <input type="checkbox" id="dg100_check" class="dg_check gl_bi"/><a href="javascript:ReverseDisplay('dg100')">2.Ledger Statement</a><br>
				<input type="checkbox" id="dg101_check" class="dg_check sl_bi"/><a href="javascript:ReverseDisplay('dg101')">3.Sales(Daily)</a><br>
				<input type="checkbox" id="dg105_check" class="dg_check gl_bi"/><a href="javascript:ReverseDisplay('dg105')">4.Fiancial Year</a><br>
				<input type="checkbox" id="dg106_check" class="dg_check sl_bi"/><a href="javascript:ReverseDisplay('dg106')">5.Sales(Monthly)</a><br>
				<input type="checkbox" id="dg107_check" class="dg_check sl_bi"/><a href="javascript:ReverseDisplay('dg107')">6.Sales (Yearly)</a><br>
				<input type="checkbox" id="dg108_check" class="dg_check sl_bi"/><a href="javascript:ReverseDisplay('dg108')">7.Sales By Category</a><br>
				<input type="checkbox" id="dg109_check" class="dg_check inv_bi"/><a href="javascript:ReverseDisplay('dg109')">8.Production (Monthly)</a><br>
				<input type="checkbox" id="dg110_check" class="dg_check inv_bi"/><a href="javascript:ReverseDisplay('dg110')">9.Production (Daily)</a><br>
				<input type="checkbox" id="dg113_check" class="dg_check hr_bi"/><a href="javascript:ReverseDisplay('dg113')">10.Head Count</a><br>
				<input type="checkbox" id="dg114_check" class="dg_check hr_bi"/><a href="javascript:ReverseDisplay('dg114')">11.Wages Detail</a><br>
				<input type="checkbox" id="dg115_check" class="dg_check hr_bi"/><a href="javascript:ReverseDisplay('dg115')">12.Absenteesm</a><br>
				<input type="checkbox" id="dg116_check" class="dg_check hr_bi"/><a href="javascript:ReverseDisplay('dg116')">13.Head Count(Pie)</a><br> -->
				<input type="checkbox" id="dg117_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg117')">2.Quick Links</a><br>
			</p>
		</div>
    </div>    
</div> 
