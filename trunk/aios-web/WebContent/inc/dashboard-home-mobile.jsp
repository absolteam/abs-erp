<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-ui/flick/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript">
	var user_dashboard = '${DASHBOARD_ACCESS}';
	showGeneralLedgerAnalytics();

	function showGeneralLedgerAnalytics() {
		updateActive('generalledger');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/dashboard_general_ledgermobile.action",
 		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){ 
		    	$('#general-ledger-analytics').html(result); 
			} 
		});
	}  
	
	function showSalesAnalytics() {
		updateActive('sales');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/dashboard_sales_mobile.action",
 		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){ 
		    	$('#sales-analytics').html(result); 
			} 
		});
	}  
	
	function showInventoryAnalytics() {
		updateActive('inventory');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/dashboard_inventory_mobile.action",
 		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){ 
		    	$('#inventory-analytics').html(result); 
			} 
		});
	}  
	
	function showHrAnalytics() {
		updateActive('humanresource');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/dashboardhr_mobile.action",
 		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){ 
		    	$('#humanresource-analytics').html(result); 
			} 
		});
	}  
	
	function showWidgets() { 
		$(".opt").removeClass('active');
		$(".opt").addClass('in-active');
		$("#showwidget").removeClass('in-active');
		$("#showwidget").addClass('active'); 
		if($('#showwidget').hasClass('showwidgets')){
			$('#showwidget').html('Hide Widgets');
			$('#showwidget').removeClass('showwidgets').addClass('hidewidgets');
			$('#valids').fadeIn();
		}else{ 
			$('#showwidget').html('Show Widgets');
			$('#showwidget').removeClass('hidewidgets').addClass('showwidgets');
			$('#valids').fadeOut();
		}
	}  
	 
	function updateActive(id) {
 		$(".tabcontent").hide();
		$("#tab_content_" + id).show();

		$(".opt").removeClass('active');
		$(".opt").addClass('in-active');
		$("#" + id).removeClass('in-active');
		$("#" + id).addClass('active');
		
		$('.tabcontent').each(function(){
    		if(!$(this).children().hasClass(id)){
    			$(this).children().html("<img src=${pageContext.request.contextPath}/images/load.gif width=40 />");
    		}
    	});
		
		$('#showwidget').html('Show Widgets');
		$('#showwidget').removeClass('hidewidgets').addClass('showwidgets');
		$('#valids').fadeOut();
	}
</script>

<style>
.tabHandle {
	color: black;
	background: #fcfcfc;
	border-bottom: 1px solid #ddd;
	border-right: 1px solid #ddd;
	cursor: pointer;
	padding: 10px;
	padding-right: 3px;
	border-radius: 3px;
	display: block;
	margin-bottom: 5px;
	font-size: 12px;
}

.active {
	font-weight: bolder;
	color: #333;
	border-left: 3px solid #A6BAFF;
	padding-left: 3px;
}

.in-active {
	font-weight: normal;
	border-left: 0px;
	color: #aaa;
}

* {
	font-family: wf_segoe-ui_normal, "Segoe UI", Segoe, "Segoe WP", Tahoma,
		"Trebuchet MS", Arial, Helvetica, sans-serif !important;
}

.tab_options {
	position: fixed;
	width: 112px;
	top: 70px;
	left: -2px;
}

.row_button {
	display: none;
}

.row1 {
	line-height: 1;
	border-bottom: 1px solid #eee;
	font-size: 13px;
	padding: 8px;
	padding-top: 15px;
	box-shadow: 0px 0px 1px 0px #EEE;
	background: #FBFBFB none repeat scroll 0% 0%;
	border-radius: 3px;
	margin-bottom: 6px;
}

.load {
	font-size: 13px;
	color: #929292;
	text-transform: uppercase;
}

h2 {
	font-weight: 300;
}

h6 {
	color: gray;
	margin-top: -5px;
	font-weight: 400;
}

.tabcontent {
	margin-bottom: 50px;
}



.wf_timestamp {
	margin-top: 4px;
	margin-right: 4px;
}
</style>

<div class="tab_options" >
	<div>
		<span class="tabHandle" onclick="showGeneralLedgerAnalytics()">
			<span id="generalledger" class="active opt">General Ledger</span> </span> <span
			class="tabHandle" onclick="showSalesAnalytics()"> <span
			id="sales" class="in-active opt">Sales</span> </span> <span
			class="tabHandle" onclick="showInventoryAnalytics()"> <span
			id="inventory" class="in-active opt">Inventory</span> </span> <span
			class="tabHandle" onclick="showHrAnalytics()"> <span
			id="humanresource" class="in-active opt">HR</span> </span> <span
			class="tabHandle" onclick="showWidgets()"> <span
			id="showwidget" class="in-active opt showwidgets">Show Widgets</span> </span>
	</div>
	<div></div>
</div>

<div style="margin-left: 125px;">
	<div id="" style="overflow-y: auto; overflow-x: hidden;">
		<div class="tabcontent" id="tab_content_generalledger">
			<div id="general-ledger-analytics" class="generalledger">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>

		<div class="tabcontent" id="tab_content_sales">
			<div id="sales-analytics" class="sales">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>

		<div class="tabcontent" id="tab_content_inventory">
			<div id="inventory-analytics" class="inventory">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div> 
		
		<div class="tabcontent" id="tab_content_humanresource">
			<div id="humanresource-analytics" class="humanresource">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div> 
		
		<div class="tabcontent" id="tab_content_showwidget">
			<div id="showwidget-analytics" class="showwidget">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div> 
	</div>
</div>