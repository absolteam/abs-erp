<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.aiotech.aios.system.*"%>
<%@page import="com.aiotech.aios.workflow.domain.entity.Menu"%>
<%@page import="com.aiotech.aios.system.domain.entity.vo.MenuVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ResourceBundle"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/adel1.jpg"
	type="image/x-icon" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/fileupload/jquery.fileupload-ui.css">
<script
	src="${pageContext.request.contextPath}/fileupload/jquery.fileupload.js"></script>
<script
	src="${pageContext.request.contextPath}/fileupload/jquery.fileupload-ui.js"></script>
<script
	src="${pageContext.request.contextPath}/fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.DOMWindow.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery-autocomplete/jquery.ui.posautocomplete.js"></script>
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/1024.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/1280.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/1360.css"
	type="text/css" /> --%>

<link rel="stylesheet" media="screen and (min-width: 1024px)"
	href="${pageContext.request.contextPath}/css/W1024.css" type="text/css" />
<link rel="stylesheet" media="screen and (min-width: 1280px)"
	href="${pageContext.request.contextPath}/css/W1280.css" type="text/css" />
<link rel="stylesheet" media="screen and (min-width: 1360px)"
	href="${pageContext.request.contextPath}/css/W1360.css" type="text/css" />
 

<script type="text/javascript">
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
} 

$(function(){ 
	$.ajax({
		type:"POST",
		url: "<%=request.getContextPath()%>/getPOSMenuItems.action",
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#menu_menu").append(result);
		},
		error:function(result){
			alert("Menu generation failed.");
		}
	});
	
	
	
	callPointOfSale('discard_pointofsales');  
 	var host = window.location.host;
 	if(host=='localhost:8080')
 	 	$('#show_material_requisition_li').show();
 	else
 		$('#show_material_requisition_li').hide(); 
 	
 
	$('#synchronize-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 height: 'auto',
 		 bgiframe: true,
		 overflow:'hidden',
		 zIndex:100000,
		 modal: true 
	});  

});

	function callPointOfSale(requiredAction){ 
		$('#common-popup-new').dialog('destroy');		
		$('#common-popup-new').remove(); 
		$('#pos-invoice-popup').dialog('destroy');		
		$('#pos-invoice-popup').remove(); 
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
		$('#pos-common-popup').dialog('destroy');		
		$('#pos-common-popup').remove();  
		var pointOfSaleId = Number(0);
		var posSessionId = '';
		$.ajax({
			type:"POST",
			url:'<%=request.getContextPath()%>/' + requiredAction
							+ '.action',
					async : false,
					dataType : "html",
					data:{pointOfSaleId :pointOfSaleId, posSessionId: posSessionId},
					cache : false,
					success : function(result) {
						$("#main-wrapper").html(result); 
					}
				});
	} 
	
	function destroyPopups() {
		if (typeof ($('#recipient-popup-screen') != "undefined")) {
			$('#recipient-popup-screen').dialog('destroy');
			$('#recipient-popup-screen').remove();
		}
		if (typeof ($('#contract-popup-screen') != "undefined")) {
			$('#contract-popup-screen').dialog('destroy');
			$('#contract-popup-screen').remove();
		}
		if (typeof ($('#release-popup-screen') != "undefined")) {
			$('#release-popup-screen').dialog('destroy');
			$('#release-popup-screen').remove();
		}
		if (typeof ($('#bank-popup') != "undefined")) {
			$('#bank-popup').dialog('destroy');
			$('#bank-popup').remove();
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();
		}
		if (typeof ($('#common-popup') != "undefined")) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();
		}
		if (typeof ($('#pos-common-popup') != "undefined")) {
			$('#pos-common-popup').dialog('destroy');		
			$('#pos-common-popup').remove(); 
		} 
		if (typeof ($('#job-common-popup') != "undefined")) {
			$('#job-common-popup').dialog('destroy');
			$('#job-common-popup').remove();
		}
		
		$('#common-popup-new').dialog('destroy');		
		$('#common-popup-new').remove(); 
		$('#pos-invoice-popup').dialog('destroy');		
		$('#pos-invoice-popup').remove(); 
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
		$('#pos-common-popup').dialog('destroy');		
		$('#pos-common-popup').remove();   
	}

	function convertToAmount(number) {
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function convertToDouble(amount) {
		return Number(amount.replace(/\,/g, ''));
	}

	function trimSpaces(toTrim) {
		return toTrim.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}

	
	function confirmLogout() {
		$.ajax({
			type : "POST",
			async : false,
			dataType : "json",
			url : "<%=request.getContextPath()%>/sync_sales_transaction.action",
			cache : false,
			success : function(response) {  
				$.ajax({
					type : "POST",
					async : false,
					dataType : "json",
					url : "<%=request.getContextPath()%>/confirm_logout.action",
					cache : false,
					success : function(response) {  
						$(location).attr('href','logout.do'); 
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("action", "${pageContext.request.contextPath}/index.jsp");
						$('<input>').attr({
						    type: 'hidden',
						    id: 'KEY',
						    name: 'KEY',
							value : response.implementationId
						}).appendTo(form);
						document.body.appendChild(form);
						form.submit();
					},
					error : function(e) {
						
					}
				}); 
			},
			
		}); 
		
	}

	function synchronizeDataBase(){ 
		$('.ui-dialog-titlebar').remove();  
		$('#synchronize-popup').dialog('open'); 
		//$('#synchronize-popup').css('min-height','auto!important');
		//$($($('#synchronize-popup').parent()).get(0)).css('min-height', 250);
		$('#synchronize-popup').css('min-height','auto','important');
		$.ajax({
			type : "POST",
			async : false,
			dataType : "json",
			url : "<%=request.getContextPath()%>/synchronize_database.action",
			cache : false,
			success : function(response) { 
				if(response.returnMessage == "SUCCESS"){
					$('#show-message').html("Database synchronization successfully processed.");
				}else{
					$('#show-message').html("CAUTION: Database synchronization failure, Please close the application & contact supervisor.");
				}  
			},
			error : function(e) {
				
			}
		}); 
	}
</script>

<style>
.wrap {
	margin-left: 98px;
	width: 100%;
}
 </style>
 
	<div class="left_site" style="min-height: 35px;">
		<div class="logo">
			<a href="#"><img src="images/logo.jpg" alt="POS" title="ERPPOS" />
			</a>
		</div>
	</div>
	<div class="synchronize-popup"></div>  
	<div id="synchronize-popup" style="display: none;"> 		
		<div id="show-message">CAUTION: Do not refresh or close this window while the Database Synchronization process completes.</div>
		<div
			class="portlet-header ui-widget-header float-right"
			id="synchronize-close"
			style="background: #0B610B; color: #fff; padding: 10px; cursor: pointer;">Close</div>
	</div>
	<div class="wrap">
		<div class="orange">
			<div id="menu_menu">
				<div id="menuResult" style="display: none;"></div>
			</div>
		</div>
	</div>
	<div class="top_right_button" style="width: 35%;">
		<c:if
			test="${sessionScope.offline ne null || sessionScope.offline == 'true'}">
			<div style="display: none;"
				title="It's always preffered to work in Online mode, please check your internet connection and restart the application."
				class="response-msg offline-warning ui-corner-all width70 float-left masterTooltip">
				You are working in Offline mode.</div>
		</c:if> 
		<div class="top_right_button1 float-right">
			<a onclick="confirmLogout();" style="cursor: pointer;"><img
				style="margin: 5px 0 0 -35px !important;"
				src="images/top_b_icon3.png" /> </a> 
		</div>
		<div class="top_right_button2 float-right" style="display: none;">
			<a onclick="synchronizeDataBase();" style="cursor: pointer;" title="Live Database synchronization"><img
				style="margin: 13px 0 0 -5px !important;"
				src="images/livesyno.png" /> </a> 
		</div>
	</div>
 