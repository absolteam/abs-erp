 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <c:if test="${requestScope.bean != null && requestScope.bean !=''}">
	 <c:choose>
	 	<c:when test="${bean.sqlReturnStatus==1}">
	 		<div id="flag" style="display:none">${bean.sqlReturnStatus}</div>
	 		<div id="returnMsg" style="display:none;"></div>
	 	</c:when>
	 	<c:when test="${bean.sqlReturnStatus==0}"> 
	 		<div id="returnMsg" style="display:none;">${bean.sqlReturnMsg}</div>
	 	</c:when> 
	 	<c:otherwise>
	 		<div id="returnMsg" style="display:none;">${bean.sqlReturnMsg}</div>
	 	</c:otherwise>
	 </c:choose> 
 </c:if>