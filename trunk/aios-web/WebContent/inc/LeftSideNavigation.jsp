<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.row_button{
	width:100%!important;
 	max-width:135px!important;
}
.row1 p{
	width:98%!important;
	/* max-width:600px!important; */
}
.chart_opt {
	margin: 0 0 0 7px; 
	color: rgb(120, 120, 120) !important;
	text-decoration: none !important; 
	font-weight: 400; 
	cursor: pointer; 
	font-size: 11px !important;
	/* border-bottom: 1px solid #4c6214; */
	text-transform: uppercase;
}
</style>
<script type="text/javascript">
$(function() {
	$(".close_left_hover_menu").click(function(){
		hide_button();
	});	
});

function show_menu() {
	$('.menu_btn').show();
	$('#more_btn').hide();
	$('#close_btn').show();
}

function hide_button() {
	$('.menu_btn').hide();
	$('#more_btn').show();
	$('#close_btn').hide();
}

function show_chart() {
	$('#valids').fadeIn();
	$('#chart_show_btn').hide();
	$('#chart_hide_btn').show();
}

function hide_chart() {
	$('#valids').fadeOut();
	$('#chart_show_btn').show();
	$('#chart_hide_btn').hide();
}
</script>



<div class="left_site">

	<div class="logo">
		<a href="#"><img src="images/logo.jpg" alt="ERP" title="ERP" /></a>
	</div>

	<div id="time">
		<div id="txt">
			<div class="jclock"></div>
		</div>
	</div>

	<div class="left_menu">
	
		<c:if test="${sessionScope.GL_BI ne null && sessionScope.GL_BI eq true}">
			<div class="botm_button_left" style="margin-top:10px">
				<a href="#" onclick="menuAjaxCallFunction(this);" id="dashboardGeneralLedger" ><p></p></a> General Ledger
			</div>
		</c:if>

		<c:if test="${sessionScope.SL_BI ne null && sessionScope.SL_BI eq true}">
			<div class="botm_button_left" style="margin-top:10px">
				<a href="#" onclick="menuAjaxCallFunction(this);" id="dashboardSales" ><p></p></a> Sales
			</div>
		</c:if>
		
		<c:if test="${sessionScope.INV_BI ne null && sessionScope.INV_BI eq true}">
			<div class="botm_button_left" style="margin-top:10px">
				<a href="#" onclick="menuAjaxCallFunction(this);" id="dashboardInventory" ><p></p></a> Inventory
			</div>
		</c:if>
		
		<c:if test="${sessionScope.HR_BI ne null && sessionScope.HR_BI eq true}">
			<div class="botm_button_left" style="margin-top:10px">
				<a href="#" onclick="menuAjaxCallFunction(this);" id="dashboardHr" ><p></p></a> HR
			</div>
		</c:if>
		
		<br />
		<a class="chart_opt"
			href="#" onclick="show_chart();" id="chart_show_btn">Show Widgets</a>

		<a class="chart_opt"
			style="display: none;"
			href="#" onclick="hide_chart();" id="chart_hide_btn">Hide Widgets</a>
		
		
		<div id="line"></div>
		
	</div>
	<%-- <div id="dashboard_buttons_div">
		<div class="button_left2">
			<a id="tab_menu_1" class="active">
				<p>
					<span id="approval_count"></span>
				</p>

			</a>
			<fmt:message key="common.dashboard.waitingforapproval" />
		</div>
		<div class="button_left2">
			<a id="tab_menu_2" href="#" class=""><p>
					<span id="approved_count"></span>
				</p></a> Approved Or Rejected
			<fmt:message key="common.dashboard.approved/rejected"/>
		</div>

		<div class="button_left2">
			<a id="tab_menu_3" href="#" class=""><p>
					<span id="alert_count"></span>
				</p></a>
			<fmt:message key="common.dashboard.alerts" />
		</div>

		<div id="line"></div>
	</div> --%>

	<div class="left_menu">
		<div class="botm_button_left">
			<a href="#" class="custom_report"><p></p></a> Reports
		</div>
		<div class="botm_button_left2">
			<a href="#" class="print_templates"><p></p></a> Templates
		</div>
		<c:if test="${sessionScope.ADMIN eq true}">
			<div class="botm_button_left3">
				<a href="#" class="workflow"><p></p></a> Work Flow
			</div>
	
			<div class="botm_button_left4">
				<a href="#" class="setup"><p></p></a> Setup
			</div>
		</c:if>

	<%-- <div id="extra_button" style="display: none;">
			<div class="botm_button_left6">
				<a href="#" class="changePassword"><p></p></a> Change Password
			</div>

			<div class="botm_button_left7">
				<a href="#" class="resetPassword"><p></p></a> Reset Password
			</div>

			<div class="botm_button_left8">
				<a href="${pageContext.request.contextPath}/logout.do" class=""><p></p></a>
				Log Out
			</div>
		</div> --%>

		<br /> <a class="chart_opt" style="margin-left: 35px;"
			href="#" onclick="show_menu();" id="more_btn">More</a>

		<a class="chart_opt" style="display: none; margin-left: 35px;"
			href="#" onclick="hide_button();" id="close_btn">Close</a>

		<div class="menu_btn"
			style="display: none; height: 75px; width: 312px; display: none; background-color: #F4F4F4; border-bottom: 1px solid #D7D7D7; border-top: 1px solid #D7D7D7; border-right: 1px solid #D7D7D7; margin: -73px 0 0 99px; position: absolute; z-index:9999;">

			<div class="botm_button_left5" style="float: left">
				<a href="#" class="manage_reference_nos close_left_hover_menu"><p></p></a> References
			</div>


			<div class="botm_button_left6" style="float: left">
				<a href="#" class="changePassword close_left_hover_menu"><p></p></a> Change Password
			</div>
			
			<c:if test="${sessionScope.ADMIN eq true || (sessionScope.PASS_RESET ne null && sessionScope.PASS_RESET eq true)}">
				<div class="botm_button_left7" style="float: left">
					<a href="#" class="resetPassword close_left_hover_menu"><p></p></a> Reset Password
				</div>
			</c:if>

			<div class="botm_button_left8" style="float: left">
				<a onclick="confirmLogout();" class="close_left_hover_menu"><p></p></a>
				Logout
			</div>
		</div>

	</div>
	<div id="line2"></div>

	<div class="copy_rights">&copy; <%= AIOSCommons.getCurrentYear() %> All In One Solutions, All Rights Reserved.</div>
</div>