<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>
<html>
<head>
<script type="text/javascript">

function languageCall(langval)
{
	var strHref = window.location.href;
	//alert(langval+"href"+strHref);
	var answer = confirm ("Page will refresh ! Please Confirm there is nothing data without saving?");
	if (answer)
		location.href='languageSetting.action?language='+langval+'&redirectValue='+strHref;
	else
	{
	  if(langval=='ar')
		  alert("Your Language has not been change Still it is 'English' only");
	  else if(langval=='en')
		  alert("Your Language has not been change Still it is 'Arabic' only");
	}
}
</script>
<script type="text/javascript">
	$(document).ready( function() {

		$(".moveRight").click( function() {
			var com = $(this).html();
			var id=$(this).attr('id');
			location.href = "UserSelectedCompany.action?com=" + com+"&id="+id;
		});
	});
</script>
</head>
<body>
	<div id="header">
		<div id="top-menu">
			<span id="txt"> </span> 
			<span class="company_name">Adel Business Solutions</span> 
			<a href="#" onclick="return languageCall('en');">English</a>|
			<a href="#" onclick="return languageCall('ar');">Arabic</a>|
			<a href="#" title="My account">My account</a> | 
			<a href="#" title="Settings">Settings</a>| 
			<a href="#" title="Contact us">Contact us</a> 
			<span>Logged in as 
				<a href="#" title="Logged in as ${employee_ID}">${employee_ID}</a> 
			</span> | 
			
			<a href="logout.action"	title="Logout">Logout</a> 
			<span style="display: block;"> Last login : ${lastLoginDate } </span>
		</div>
		<div id="sitename">
			<div class="companyList">
				<a href="AdminAfterLogin.jsp" class="logo float-left" title="Select Company">Select Company</a>
				<div id="actions" class="scrollButtons">
					<a class="nextPage"></a> 
					<a class="prevPage"></a>
				</div>
			</div>
		</div>
		<form name="selectcompanies" method="post">
			<div class="scrollable vertical">
				<div class="items">
					<c:choose>
						<c:when test="${itemList != null}">
							<c:forEach items="${itemList}" var="company" varStatus="status">
								<div class="item">
									<p class="Category"><span class="moveRight" id="${company.organizationId }">${company.organizationName}</span></p>
								</div>
							</c:forEach>
						</c:when>
					</c:choose>
				</div>
			</div>
		</form>
	</div>
</body>
</html>


