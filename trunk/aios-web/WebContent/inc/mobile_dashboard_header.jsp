<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/js/menu/rmm-css/responsivemobilemenu.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/menu/rmm-js/responsivemobilemenu.js"></script>
 --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/bootstrap-3.3.1/css/bootstrap.min.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-3.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript">

$(function() {
		
	$(".backtodashboard").click(function() {
		//window.location.href = '<%=request.getContextPath()%>/mobile_dashboard.action';		
		menuAjaxCallFunction("<div id='dashboardHome_mobile'></div>");		
	});
	
	$(".switchToFull").click(function() {
		
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			alert("Full ERP version is available for desktop only.");
		} else if(!$.browser.mozilla) {				
			alert("Please use Mozilla Firefox browser in order to switch to full version.");
		} else {
			window.location.href = '<%=request.getContextPath()%>/user_selected_company.action';
		}
		
	});
		
	$(".custom_report").click(function(){
		$.ajax({
			type:"GET",
			url: '<%=request.getContextPath()%>/custom_reports_mobile.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
				$('#nav li:not(:first)').hide(); 
				/* sitemapstyler(); */	
				$('.reportingcurvedContainer_content').hide();  	
						
			},
			error:function(result){
				$("#main-wrapper").html(result);
			}
		});
 	});
	
	$(".notifications").click(function() {
		menuAjaxCallFunction("<div id='workflow_notifications_mobile'></div>");
 	});
	
});

function menuAjaxCallFunction(menuItem){
	//slideToggleSidebar();
	var requiredAction = $(menuItem).attr('id');
	$.ajax({
		type:"POST",
		url:'<%=request.getContextPath()%>/' + requiredAction
							+ '.action',
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$("#main-wrapper").html(result);
					}
				});

	}
	
function show_chart() {
	document.getElementById("chart_show_btn").style.display = "none";
	document.getElementById("chart_hide_btn").style.display = "block";
	document.getElementById("valids").style.display = "block";
}

function hide_chart() {
	document.getElementById("chart_show_btn").style.display = "block";
	document.getElementById("chart_hide_btn").style.display = "none";
	document.getElementById("valids").style.display = "none";
}
function confirmLogout() { 
	$.ajax({
		type : "POST",
		async : false,
		dataType : "json",
		url : "<%=request.getContextPath()%>/confirm_logout.action",
		cache : false,
		success : function(response) {  
			$(location).attr('href','logout.do'); 
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", "${pageContext.request.contextPath}/index_main.action");
			$('<input>').attr({
			    type: 'hidden',
			    id: 'KEY',
			    name: 'KEY',
				value : response.implementationId
			}).appendTo(form);
			document.body.appendChild(form);
			//form.submit();
		},
		error : function(e) {
			
		}
	}); 
}
</script>

<style>
.wrap {
	width: 100%;
	background-color:#537b9f;
}

.left_site {
	max-height: 100px !important;
	min-height: 10px !important;
}

.top_right_button {
	min-width: 10px !important;
}

.down {
	float: left !important;
}

.oe_overlay {
	background: #000;
	opacity: 0;
	position: fixed;
	top: 0px;
	left: 0px;
	width: 100%;
}
</style>

<div>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
			<img src="images/logo.jpg" alt="AIO ERP DASHBOARD" title="AIO ERP DASHBOARD" />
	  </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      	<li><a href='#' class="notifications">Notifications<span class="sr-only">(current)</span></a></li>
        <li><a href="#" class="backtodashboard">Analytics</a></li>        
        <li><a href='#' class="custom_report">Reports</a></li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li> -->
      </ul>
      <!-- <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a class="switchToFull" href='#'>Switch To Full Version</a></li>
        <!-- <li><a onclick="confirmLogout();" href='#'>Logout</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding-bottom: 10px; padding-top: 13px;"><img src="${pageContext.request.contextPath}/images/user.png" width="25" /> <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">          
          	<li style="padding: 3px 20px; margin-bottom: -5px;">${USER_PERSON_NAME}</li>
            <li style="padding: 3px 20px;"><span style="font-size: 10px; color: gray;">${USER.username}</span></li>
            <li class="divider"></li>
            <li><a onclick="confirmLogout();" href='#'>Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</div>
