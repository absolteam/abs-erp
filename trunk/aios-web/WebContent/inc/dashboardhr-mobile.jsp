<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<link rel="stylesheet" type="text/css"
	href="dashboard/jquery-ui.css" />
<link class="include" rel="stylesheet" type="text/css"
	href="dashboard/jquery.jqplot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shThemejqPlot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/aiosChartMobile.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style type="text/css">
/* .jqplot-point-label {
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
 font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
}
.jqplot-data-label{
	border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
} */
.ui-widget-header {
	border: 0px;
	background: none;
}
#startDate {
	width: 110px;
}
#endDate {
	width: 110px;
}
</style>	
<!-- Don't touch this! -->

<script type="text/javascript" src="dashboard/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.trendline.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.blockRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.enhancedLegendRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pointLabels.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="dashboard/js/aiosChartMobile.js"></script>
<!-- End Don't touch this! -->

<script type="text/javascript">
var  plot13=null; var  plot14=null; var  plot16=null;
var headCountfor='DESIGNATION';var salaryTotalfor='DESIGNATION';

$(function() { 
	setStyle(); 
	dg13();
}); 
 
function dg13(){
	$('#content13_chart img').css('display', 'block');
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
		  url:'employeeHeadCounts.action',
	      async: false,
	      data:{name:headCountfor},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].count));
					ticks.push(response.data[index].name);
				});
	    	  $('#content13_chart').html('');
	      }
	    });
	  
	  plot13 = drawBarChartWithoutLegends([s1],ticks,'content13_chart'); 
	  $('#content13_chart').height($('#content13').height()*0.86);
	  $('#content13_chart').width($('#content13').width()*0.96);
	  plot13.replot( { resetAxes: true } );
		 $('#content13_chart').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content13_chart').height($('#content13').height()*0.86);
		 $('#content13_chart').width($('#content13').width()*0.96);
		 plot13.replot( { resetAxes: true } );
	 }); 
	 setTimeout(dg14(), 2000);
}
	
function dg14(){
	$('#content14_chart img').css('display', 'block');
	var s1=[];
	var ticks=[];
	  $.ajax({
		  type:"POST",
			url:'wagesByDepartment.action',
	      async: false,
	      data:{name:salaryTotalfor},
	      dataType:"json",
	      success: function(response) {
	    	  s1=[];
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].name);
				});
	    	  $('#content14_chart').html('');
	      }
	    });
	  plot14 = drawBarChartWithoutLegends([s1],ticks,'content14_chart');
	  $('#content14_chart').height($('#content14').height()*0.86);
	  $('#content14_chart').width($('#content14').width()*0.96);
	  plot14.replot( { resetAxes: true } );
		 $('#content14_chart').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content14_chart').height($('#content14').height()*0.86);
		 $('#content14_chart').width($('#content14').width()*0.96);
		 plot14.replot( { resetAxes: true } );
	 }); 
	  setTimeout(dg16(), 2000);  
} 
	
	function dg16(){
		$('#content16_chart img').css('display', 'block');
		var s1=[];
		  $.ajax({
			  type:"POST",
				url:'employeeHeadCounts.action',
		      async: false,
		      data:{name:headCountfor},
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						s1.push([response.data[index].name,Number(response.data[index].count)]);	
					});
		    	  $('#content16_chart').html('');
		      }
		    });
		  plot16 = drawPieChartLegendByRight([s1],'content16_chart');
		  $('#content16_chart').height($('#content16').height()*0.86);
		  $('#content16_chart').width($('#content16').width()*0.96);
		  plot16.replot( { resetAxes: true } );
			 $('#content14_chart').bind('resize', function(event, ui) {
			  // pass in resetAxes: true option to get rid of old ticks and axis properties
			  // which should be recomputed based on new plot size.
			 $('#content16_chart').height($('#content16').height()*0.86);
			 $('#content16_chart').width($('#content16').width()*0.96);
			 plot16.replot( { resetAxes: true } );
		 }); 
	}
</script>


</head>

<div id="valids"
	style="display: none; min-height: 200px; width: 150px; display: none; background-color: #FFF; 
		border: 1px solid #D7D7D7; margin: 200px 0 0 1px; position: absolute; z-index:9999;">
	<input id="effectTypes" type="hidden" name="effects" value="blind">

	<div style="width: 150px; background-color: #eee; height: 20px;">
		<input type="checkbox" id="dg_check_all" /><a href="#" id="button">Charts</a>
	</div> 

	<div class="toggler">
		<div id="effect">
			<p>
				<input type="checkbox" id="dg13_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg13')">1.Head Count</a><br>
				<input type="checkbox" id="dg14_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg14')">2.Wages Detail</a><br>
 				<input type="checkbox" id="dg16_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg16')">4.Head Count(Pie)</a><br>
			</p> 
		</div>
    </div>   	
   
</div> 

<div id="containment-wrapper">

	<div id='dg13' class="dg">
		<div class='content' id='content13'>
			<div class='bar' id='bar13'>
				<a href="javascript:ReverseDisplay('dg13')">
				</a>
				<div
					class="dashboard-content-title">Head Counts</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" name="headCountOption" checked="checked" class="headCountOption" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="headCountOption" class="headCountOption" value="DEPARTMENT"/></div>
			<div id="content13_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>
	</div>

	<div id='dg14' class="dg">
		<div class='content' id='content14'>
			<div class='bar' id='bar14'><a href="javascript:ReverseDisplay('dg14')">&nbsp;</a>
				<div class="dashboard-content-title">Wages Detail</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" checked="checked" name="salaryOption" class="salaryOption" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="salaryOption" class="salaryOption"  value="DEPARTMENT"/></div>
			<div id="content14_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div> 
	
	<div id='dg16' class="dg">
		<div class='content' id='content16'>
			<div class='bar' id='bar16'>
				<a href="javascript:ReverseDisplay('dg16')">
				</a>
				<div
					class="dashboard-content-title">Head Counts(Pie)</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" name="headCountOption" checked="checked" class="headCountOptionPie" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="headCountOption" class="headCountOptionPie" value="DEPARTMENT"/></div>
			<div id="content16_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div>
</div> 