<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<link rel="stylesheet" type="text/css"
	href="dashboard/jquery-ui.css" />
<link class="include" rel="stylesheet" type="text/css"
	href="dashboard/jquery.jqplot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shThemejqPlot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/aiosChart.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style type="text/css">
/* .jqplot-point-label {
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
 font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
}
.jqplot-data-label{
	border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
} */
.ui-widget-header {
	border: 0px;
	background: none;
}
#startDate {
	width: 110px;
}
#endDate {
	width: 110px;
}
</style>	
<!-- Don't touch this! -->

<script type="text/javascript" src="dashboard/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.trendline.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.blockRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.enhancedLegendRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pointLabels.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="dashboard/js/aiosChart.js"></script>
<!-- End Don't touch this! -->

<script type="text/javascript">
var  plot13=null;var  plot14=null;var  plot15=null;var  plot16=null;
var headCountfor='DESIGNATION';var salaryTotalfor='DESIGNATION';
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var oSettings =null;
$(function() {

	$('.chart-date').datepick({
		onSelect: customRange, showTrigger: '#calImg'}); 
	setStyle();

		dg13();
		dg16();
		dg14();
		
		 dg15();
		$('#content15_chart').height($('#content15').height()*0.86);
		 $('#content15_chart').width($('#content15').width()*0.96);
		 
		 $('#content15').bind('resize', function(event, ui) {
			 $('#content15_chart').height($('#content15').height()*0.86);
			 $('#content15_chart').width($('#content15').width()*0.96);
		}); 
		 
	

		$('#content13_chart').height($('#content13').height()*0.86);
		 $('#content13_chart').width($('#content13').width()*0.96);
		 plot13.replot( { resetAxes: true } );
		 $('#content13').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content13_chart').height($('#content13').height()*0.86);
		 $('#content13_chart').width($('#content13').width()*0.96);
		 plot13.replot( { resetAxes: true } );
		 }); 
		 
		 
		 $('#content14_chart').height($('#content14').height()*0.86);
		 $('#content14_chart').width($('#content14').width()*0.96);
		 plot14.replot( { resetAxes: true } );
		 $('#content14').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content14_chart').height($('#content14').height()*0.86);
		 $('#content14_chart').width($('#content14').width()*0.96);
		 plot14.replot( { resetAxes: true } );
		});
		 
		 $('.headCountOption').change(function(){
			 headCountfor=$(this).val();
			 dg13();
			 $('#content13_chart').height($('#content13').height()*0.86);
			 $('#content13_chart').width($('#content13').width()*0.96);
			 plot13.replot( { resetAxes: true } );
		 });
		 
		 $('.headCountOptionPie').change(function(){
			 headCountfor=$(this).val();
			 dg16();
			 $('#content16_chart').height($('#content16').height()*0.83);
			 $('#content16_chart').width($('#content16').width()*0.96);
			 plot16.replot( { resetAxes: true } );
		 });
		 
		 $('.salaryOption').change(function(){
			 salaryTotalfor=$(this).val();
			 dg14();
			 $('#content14_chart').height($('#content14').height()*0.86);
			 $('#content14_chart').width($('#content14').width()*0.96);
			 plot14.replot( { resetAxes: true } );
		 });
		 
		
		 
		 $('#content16_chart').height($('#content16').height()*0.83);
		 $('#content16_chart').width($('#content16').width()*0.96);
		 plot16.replot( { resetAxes: true } );
		 $('#content16').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content16_chart').height($('#content16').height()*0.83);
		 $('#content16_chart').width($('#content16').width()*0.96);
		 plot16.replot( { resetAxes: true } );
		 }); 
		 
		 
		  
});

function customRange(){
	dg15();
}
	function dg13(){
		
		var s1=[];
		var ticks=[];
		  $.ajax({
			  type:"POST",
				url:'employeeHeadCounts.action',
		      async: false,
		      data:{name:headCountfor},
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						s1.push(Number(response.data[index].count));
						ticks.push(response.data[index].name);
					});

		      }
		    });
		  
		  plot13 = drawBarChartWithoutLegends([s1],ticks,'content13_chart');
		    
	}
	
	function dg14(){
		var s1=[];
		var ticks=[];
		  $.ajax({
			  type:"POST",
				url:'wagesByDepartment.action',
		      async: false,
		      data:{name:salaryTotalfor},
		      dataType:"json",
		      success: function(response) {
		    	  s1=[];
		    	  $(response.data)
					.each(function(index) {
						s1.push(Number(response.data[index].amount));
						ticks.push(response.data[index].name);
					});

		      }
		    });
		  plot14 = drawBarChartWithoutLegends([s1],ticks,'content14_chart');
		    
	
	}
	
	function dg15(){
		listCall();
	}
	
	function listCall(){
		var params="id=0";
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		params+="&startDate="+startDate;
		params+="&endDate="+endDate;
		$('#Absent').dataTable().fnDestroy();
		oTable = $('#Absent')
		.dataTable(
				{
					"bJQueryUI" : true,
					"bPaginate" : true,
					"bFilter" : true,
					"bInfo" : true,
					"bSortClasses" : false,
					"bLengthChange" : false,
					"bProcessing" : true,
					"bAutoWidth": false,
					"sScrollY": $("#dg15").height()-175,
					"bDestroy" : true,
					"sAjaxSource" : "<%=request.getContextPath()%>/getAbsenteesmByPeriod.action?"+params,
					"fnRowCallback" : function(nRow, aData,
							iDisplayIndex, iDisplayIndexFull) {
						//alert(aData.statusDisplay);
					},
					"aoColumns" : [{
						"mDataProp" : "personName"
					},{
						"mDataProp" : "designationName"
					}, {
						"mDataProp" : "attedanceDate"
					},],
					

		}); 
		
	}
	
	function dg16(){
		var s1=[];
		  $.ajax({
			  type:"POST",
				url:'employeeHeadCounts.action',
		      async: false,
		      data:{name:headCountfor},
		      dataType:"json",
		      success: function(response) {
		    	  $(response.data)
					.each(function(index) {
						s1.push([response.data[index].name,Number(response.data[index].count)]);	
					});

		      }
		    });
		  plot16 = drawPieChartLegendByRight([s1],'content16_chart');
		  //plot13 = drawBarChartWithoutLegends([s1],ticks,'content13_chart');
		    
	}
</script>


</head>

<div id="containment-wrapper">

	<div id='dg13' class="dg">
		<div class='content' id='content13'>
			<div class='bar' id='bar13'>
				<a href="javascript:ReverseDisplay('dg13')">
				</a>
				<div
					class="dashboard-content-title">Head Counts</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" name="headCountOption" checked="checked" class="headCountOption" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="headCountOption" class="headCountOption" value="DEPARTMENT"/></div>
			<div id="content13_chart"></div>
		</div>
	</div>

	<div id='dg14' class="dg">
		<div class='content' id='content14' style='width:486px;'>
			<div class='bar' id='bar14'><a href="javascript:ReverseDisplay('dg14')">&nbsp;</a>
				<div class="dashboard-content-title">Wages Detail</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" checked="checked" name="salaryOption" class="salaryOption" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="salaryOption" class="salaryOption"  value="DEPARTMENT"/></div>
			<div id="content14_chart"></div>
		</div>
	</div>
	<div id='dg15' class="dg">
		<div class='content' id='content15' style='width:486px;'>
			<div class='bar' id='bar15'><a href="javascript:ReverseDisplay('dg15')">&nbsp;</a>
				<div class="dashboard-content-title">Absenteesm</div>
			</div>
			<div>
				<label class="dashboard-content-filter-label">&nbsp;&nbsp;&nbsp;Start :
				</label>
				<input type="text" id="startDate" name="startDate" class="chart-date" value="${startDate}">
				<label class="dashboard-content-filter-label">&nbsp;&nbsp;End :
				</label>
				<input type="text" id="endDate" name="endDate" class="chart-date" value="${endDate}">
			</div>
			<div id="content15_chart">
				<table class="display" id="Absent">
					<thead>
						<tr>
							<th>Employee</th>
							<th>Designation</th>
							<th>Date</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div id='dg16' class="dg">
		<div class='content' id='content16'>
			<div class='bar' id='bar16'>
				<a href="javascript:ReverseDisplay('dg16')">
				</a>
				<div
					class="dashboard-content-title">Head Counts(Pie)</div>
			</div>
			<div>&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Designation</label><input type="radio" name="headCountOption" checked="checked" class="headCountOptionPie" value="DESIGNATION"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<label class="dashboard-content-filter-label">Department</label><input type="radio" name="headCountOption" class="headCountOptionPie" value="DEPARTMENT"/></div>
			<div id="content16_chart"></div>
		</div>
	</div>
</div>
<div id="valids"
	style="display: none; min-height: 200px; width: 150px; display: none; background-color: #FFF; border-bottom: 1px solid #D7D7D7; border-top: 1px solid #D7D7D7; border-right: 1px solid #D7D7D7; margin: -400px 0 0 1px; position: absolute; z-index:9999;">
	<input id="effectTypes" type="hidden" name="effects" value="blind">

	<div style="width: 150px; background-color: #eee; height: 20px;">
		<input type="checkbox" id="dg_check_all" /><a href="#" id="button">Charts</a>
	</div>



	<div class="toggler">
		<div id="effect">
			<p>
				<input type="checkbox" id="dg13_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg13')">1.Head Count</a><br>
				<input type="checkbox" id="dg14_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg14')">2.Wages Detail</a><br>
				<input type="checkbox" id="dg15_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg15')">3.Absenteesm</a><br>
				<input type="checkbox" id="dg16_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg16')">4.Head Count(Pie)</a><br>
			</p>
			

		</div>
    </div>   	
   
</div> 
