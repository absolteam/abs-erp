<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.aiotech.aios.system.*"%>
<%@page import="com.aiotech.aios.workflow.domain.entity.Menu"%>
<%@page import="com.aiotech.aios.system.domain.entity.vo.MenuVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>

<link href="${pageContext.request.contextPath}/js/sitemapstyler/sitemapstyler.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sitemapstyler/sitemapstyler.js"></script>	 
 
<style>
.table-custom-design{
	width:100% !important;
	
}
#page-wrapper #main-wrapper #main-content {
	border: none;
}
.reportingcurvedContainer{
	border: 1px solid #ccc;
 	/* -moz-border-radius: 8px;
	border-radius: 8px; */
	float: left; 
	height: 100%;
	border-style: solid; 
	border-width: thin;  
	width: 20%;
	overflow-y: auto; 
	overflow-x: hidden; 
	background: white;
	box-shadow: 0px 0px 3px 0px #ccc;
}
.reportingcurvedContainer_content{
	border:1px solid #C6CACC;
 	/* -moz-border-radius: 8px;
	border-radius: 8px; */
	float: left; 
	height: 100%;
	border-style: solid; 
	border-width: thin;  
	width: 78.9%;
	background: white;
	box-shadow: 0px 0px 3px 0px #ccc;
	margin-left: 5px; 
}
.content_left{width:91%;}
#page-wrapper #main-wrapper #report-content{
	/* border: thin solid #CCCCCC; */
	border-radius: 2px;
	height:98%;
	z-index: 999;
	/* background: white;
	box-shadow: 0px 0px 3px 0px #ccc; */
	margin-top: 5px;
	margin-left: 5px;
}
.pdf-download-call {
	border: none !important;
}
.xls-download-call {
	border: none !important;
}
.print-call {
	border: none !important;
}
.process_buttons > * {
	border: none !important;
}
.select_info {
	margin-top: 20%; color: #ccc; text-align: center; font-size: 16px; text-transform: uppercase; 
}
.action_btn, div[style*="cursor"][style*="pointer"], div[class*="buttons"] div {
	background: none !important;
	color: gray;
}
</style>
<script type="text/javascript">

	$(function(){
		$('.dashboard_right_container').hide();  
		$('#nav li:not(:first)').hide(); 
		sitemapstyler();	
	});
	
	function reportAjaxCallFunction(menuItem){
		
		//Popup window issue close
		if(typeof($('#job-common-popup')!="undefined")){ 
			$('#job-common-popup').dialog('destroy');		
			$('#job-common-popup').remove(); 
		} 
		var requiredAction = $(menuItem).attr('id');
		$.ajax({
			type:"POST",
			url:'<%=request.getContextPath()%>/'+requiredAction+'.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#reporting-tree").find(".dataScript").remove();
				//$("#reporting-tree").empty().remove();
				//$(".process_buttons").before("<div id='reporting-tree' style='height:94%''></div>");
				$("#reporting-tree").html(result);
				$('#loading').fadeOut();
				$("#page-wrapper #main-wrapper #main-content").css("margin-right", 0);
			}
			
		});
	}

</script>

<div id="report-content">
	
	<%!
		int count = 0;
		String className = "";
		Map<Long, Boolean> menuRights;
		String locale = "en";
		
		void renderReportingMenu(HttpSession session, JspWriter out) throws Exception {
			
			if (session.getAttribute("MENU_MAP") != null) {
			
				try {
					String tempLocale = (String) session.getAttribute("lang");
					if (tempLocale != null)
						locale = tempLocale;
				} catch (Exception e) {
					locale = "en";
				}
				
				Map<String, Object> menuMap = (Map<String, Object>) session.getAttribute("MENU_MAP");			
				List<MenuVO> reportMenuItems = (List<MenuVO>) menuMap.get("REPORT_MENU");
				menuRights = (Map<Long, Boolean>) menuMap.get("MENU_RIGHTS");
				
				List<MenuVO> menu = reportMenuItems;
				Collections.sort(menu);
				int i=1;
				for (MenuVO menuItem : menu) {
					
					if (menuRights.get(menuItem.getMenuItemId()) != null) {											
						
						out.println("<li><a class=\"\" id=\"" + menuItem.getScreen().getScreenPath() 
								+ "\" style=\"cursor: pointer;\" onclick=\"reportAjaxCallFunction(this);\">" 
								+ (locale.equals("ar") ? menuItem.getItemTitleAr()
										: menuItem.getItemTitle()) + "</a>");
						out.println("<ul>");
						
						recursivelyPopulateMenu(menuItem,out);
						
						out.println("</ul>");
						out.println("</li>");
					}
					i++;
				}
			}
		}
		
		void recursivelyPopulateMenu(MenuVO menu, JspWriter out) throws Exception {
			
			if (menuRights.get(menu.getMenuItemId()) != null) {
						
				List<MenuVO> subMenu = menu.getMenus();
				Collections.sort(subMenu);
				
				for(MenuVO subItem : subMenu) {
					
					if (menuRights.get(subItem.getMenuItemId()) != null) {	

						if (subItem.getMenus() == null || subItem.getMenus().size() == 0)	{
							
							className = "";		
							out.println("<li><a id=\"" + subItem.getScreen().getScreenPath() 
									+ "\" onclick=\"reportAjaxCallFunction(this);\" style=\"cursor: pointer;\" class=\"" + className 
									+ "\">" + (locale.equals("ar") ? subItem.getItemTitleAr()
											: subItem.getItemTitle()) + "</a> </li>");						
							continue;
						}
						else {
							className = "";	
							out.println("<li><a id=\"" + subItem.getScreen().getScreenPath() 
									+ "\" onclick=\"reportAjaxCallFunction(this);\" style=\"cursor: pointer;\" class=\"" + className 
									+ "\">" + (locale.equals("ar") ? subItem.getItemTitleAr()
											: subItem.getItemTitle()) + "</a>");
							
							out.println("<ul>");
							recursivelyPopulateMenu(subItem,out);
							out.println("</ul>");
							out.println("</li>");
						}
					}
				}
			}
		}
	%>
	
	<div id="content" class="reportingcurvedContainer">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s">
			</span>Select Report
		</div>	 
		<ul id="sitemap" style="margin-top: 10px;">
			<%	
				renderReportingMenu(session,out);
			%>		
		</ul>
	</div>
	<div  class="reportingcurvedContainer_content">
		<div id="reporting-tree" style="height: 94%">
		<div align="center" class="select_info">Please Select Report to view data</div>
		<!-- <div class="process_buttons">
			<div class="width100 float-right ">
			  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
			 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
			  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
			 </div>
		 </div> -->
		 </div>
	</div>
</div> 