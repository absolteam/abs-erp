<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/adel1.jpg"
	type="image/x-icon" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/fileupload/jquery.fileupload-ui.css">
<script
	src="${pageContext.request.contextPath}/fileupload/jquery.fileupload.js"></script>
<script
	src="${pageContext.request.contextPath}/fileupload/jquery.fileupload-ui.js"></script>
<script
	src="${pageContext.request.contextPath}/fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.DOMWindow.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/1024.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/1280.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/1360.css"
	type="text/css" /> --%>

<link rel="stylesheet" media="screen and (min-width: 1024px)"
	href="${pageContext.request.contextPath}/css/W1024.css" type="text/css" />
<link rel="stylesheet" media="screen and (min-width: 1280px)"
	href="${pageContext.request.contextPath}/css/W1280.css" type="text/css" />
<link rel="stylesheet" media="screen and (min-width: 1360px)"
	href="${pageContext.request.contextPath}/css/W1360.css" type="text/css" />
</head>

<script type="text/javascript">
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}

/**
 * Hides Waiting for Approval, Approved/Rejected and Alerts buttons.
 * As user navigates away from Dashboard, these buttons will be hidden.
 *
 * @author: Shoaib Ahmad Gondal
 */
function hideDashboardButtons() {
	if ($('#workflowcontent-approval').length == 0) {
		$('#dashboard_buttons_div').hide();
		
		$('#extra_button').show();
	}
}

$(document).ready(function() { 
	$.ajax({
		type:"POST",
		url: "<%=request.getContextPath()%>/getMenuItems.action",
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#menu_menu").append(result);
		},
		error:function(result){
			alert("Menu generation failed.");
		}
	});
});

$(function(){

	var options = {
	        timeNotation: '12h',
	        am_pm: true,
	        fontFamily: 'Verdana, Times New Roman',
	        foreground: 'white'
	      };
	$('.jclock').jclock(options); // Jclock Calling

	$(".custom_report").click(function(){
		 $('#loading').fadeIn();
		$.ajax({
			type:"POST",
			url: '<%=request.getContextPath()%>/custom_reports.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
				slideSidebar();
			}
		});
		$('#loading').fadeOut();
		hideDashboardButtons();
  	});
	
	$(".print_templates").click(function(){
		$('#loading').fadeIn();
		slideToggleSidebar();
		$.ajax({
			type:"POST",
			url: '<%=request.getContextPath()%>/print_templates.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});
		$('#loading').fadeOut();
		hideDashboardButtons();
  	});
	
	$(".manage_reference_nos").click(function(){
		$('#loading').fadeIn();
		slideToggleSidebar();
		$.ajax({
			type:"POST",
			url: '<%=request.getContextPath()%>/refManagerRedirect.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});
		$('#loading').fadeOut();
		
		hideDashboardButtons();
		
		return false;
  	});

  	$('.workflow').click(function(){
  		if(typeof($('#Operation_Form_Div')!="undefined")){ 
  			$('#Operation_Form_Div').dialog('destroy');		
  			$('#Operation_Form_Div').remove(); 
  		} 
  		$('#loading').fadeIn();
  		$.ajax({
			type:"GET",
			url: '<%=request.getContextPath()%>/workflow-management.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});
		$('#loading').fadeOut();
		hideDashboardButtons();
  	});
  	
	$('.setup').click(function(){
  		$('#loading').fadeIn();
  		$.ajax({
			type:"GET",
			url: '<%=request.getContextPath()%>/show_setup_wizard.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});
		$('#loading').fadeOut();
		hideDashboardButtons();
  	});
	
	$(".reportanissue").click(function(){
		 $('#loading').fadeIn();
		 
		//alert(urlval);
			$.ajax({
				type:"POST",
				url: '<%=request.getContextPath()%>/report_issue_add.action',
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
					//alert("header return : "+result);
					$("#main-wrapper").html(result);
					$('#loading').fadeOut();
				}
			});
	  	});

	$(".editprofile").click(function(){
		 $('#loading').fadeIn();
		//alert(urlval);
			$.ajax({
				type:"POST",
				url: '<%=request.getContextPath()%>/phone_book_friend_edit.action',
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
					//alert("header return : "+result);
					$("#main-wrapper").html(result);
					$('#loading').fadeOut();
				}
			});
	  	});

	langval=$('#headerLanguage').val();
	if(langval=='ar'){
		var script = document.createElement('script');
		script.src = '${pageContext.request.contextPath}/js/jquery.validationEngine-ar.js';
		script.type = 'text/javascript';
		document.getElementsByTagName('head')[0].appendChild(script);
	} else {
		var script = document.createElement('script');
		script.src = '${pageContext.request.contextPath}/js/jquery.validationEngine-en.js';
		script.type = 'text/javascript';
		document.getElementsByTagName('head')[0].appendChild(script);
	}
	
	$(".backtodashboard").click(function(){
		//window.location.reload();
		
		/* When user saves workflow, user is redirected to this page and url changes. 
		 * So when user clicks on Dashboard link, it is required to reload the page with its url.
		 	Added By: Shoaib Ahmad Gondal
		 	@Date: 20-Aug-13
		*/
		window.location.href = '<%=request.getContextPath()%>/user_selected_company.action';
		
	});
	
	$('#implementationGroups').change(function(){
		var newImplementationId= $(this).val();
  		$.ajax({
			type:"GET",
			url: '<%=request.getContextPath()%>/reset_company.action',
			async:false,
			data:{newImplementationId:newImplementationId},
			dataType:"html",
			cache:false,
			success:function(result){
				 $("#page-wrapper-hidden-div").html(result);
				 var message=$('#page-wrapper-hidden-div').html(); 
				 if(message.trim()=="SUCCESS"){
					 window.location.href = '<%=request.getContextPath()%>/user_selected_company.action';
				 }else{
					 $('#page-wrapper-error').hide().html("Unable to switch company.").slideDown(1000);
				 }
				
			}
		});
  	});
	
	$("#wf-save-comment").click(function(){
		saveComment();//First Call
	});
	$("#wf-close-comment").click(function(){
		window.location.reload();
	});
});
function slideSidebar(){
   		 $("#sidebar").hide('slide',{direction:'right'},1000);
    	$("#page-wrapper #main-wrapper #main-content").css("margin-right", 0);
}
function slideToggleSidebar(){
	if($("#sidebar").is(':visible')==false){
   	 	$("#sidebar").show('slide',{direction:'left'},0);
   		 $("#page-wrapper #main-wrapper #main-content").css("margin-right", 242);
	}
}
function destroyPopups() {
	if(typeof($('#recipient-popup-screen')!="undefined")){ 
		$('#recipient-popup-screen').dialog('destroy');		
		$('#recipient-popup-screen').remove(); 
	}
	if(typeof($('#contract-popup-screen')!="undefined")){ 
		$('#contract-popup-screen').dialog('destroy');		
		$('#contract-popup-screen').remove(); 
	}
	if(typeof($('#release-popup-screen')!="undefined")){ 
		$('#release-popup-screen').dialog('destroy');		
		$('#release-popup-screen').remove(); 
	}
	if(typeof($('#bank-popup')!="undefined")){ 
		$('#bank-popup').dialog('destroy');		
		$('#bank-popup').remove();  
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove();  
	}
	if(typeof($('#common-popup')!="undefined")){
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();  
	}

	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	}  
	
	if(typeof($('#definition_option_popup')!="undefined")){ 
		$('#definition_option_popup').dialog('destroy');		
		$('#definition_option_popup').remove(); 
	} 
}

function languageCall(langval)
{
	var strHref = window.location.href;
	var strHrefFormat=strHref.replace("&", "$");
	//alert(langval+"href"+strHrefFormat);
	var answer = confirm ("Page will refresh ! Please Confirm there is nothing data without saving?");
	if (answer)
		location.href='languageSetting.action?language='+langval+'&redirectValue='+strHrefFormat;
	else
	{
	  if(langval=='ar')
		  alert("Your Language has not been change Still it is 'English' only");
	  else if(langval=='en')
		  alert("Your Language has not been change Still it is 'Arabic' only");
	}
}

function switchToAr() {
	
	var strHref = window.location.href;
	var strHrefFormat = strHref.replace("&", "$");
	//alert(langval + ", href: " + strHrefFormat);
	
	var answer = confirm ("unsaved data will be lost... continue?");
	if (answer) {	
		//alert('<%=request.getContextPath()%>/switchLanguage.action?language=ar&redirectValue='+strHrefFormat);
		$.ajax({
			type:"POST",
			url: '<%=request.getContextPath()%>/switchLanguage.action?language=ar&redirectValue='+strHrefFormat,
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				//alert(strHrefFormat);
				location.reload(true);
				//location.href = strHrefFormat;
			},
			error: function() {
				alert("Error occured while switching lang.");
			}
		});		
	}
}

function switchToEn() {
	
	var strHref = window.location.href;
	var strHrefFormat = strHref.replace("&", "$");
	//alert(langval + ", href: " + strHrefFormat);
	
	var answer = confirm ("unsaved data will be lost... continue?");
	if (answer) {
		//alert('<%=request.getContextPath()%>/switchLanguage.action?language=en&redirectValue='+strHrefFormat);
		$.ajax({
			type:"POST",
			url: '<%=request.getContextPath()%>/switchLanguage.action?language=en&redirectValue='+strHrefFormat,
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				//alert(strHrefFormat);
				location.reload(true);
				//location.href = strHrefFormat;
			},
			error: function() {
				alert("Error occured while switching lang.");
			}
		});
	}
}

function convertToAmount(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function convertToDouble(amount) {
    return Number(amount.replace(/\,/g,''));
}

function trimSpaces(toTrim) {
	return toTrim.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}
//Added by rafiq for workflow approve / reject common call other then that popup window

function workflowApproveCall(recordId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflowProcess.action", 
	 	async: false, 
	 	data:{ 
	 		recordId:recordId,finalyzed:1,processFlag:1
	 	},
	    dataType: "html",
	    cache: false,
		success:function(result){
			window.location.reload();
		},
		error:function(result){  
			$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
		}
	});  
}
function workflowRejectCall(recordId) {
	$("#reject_comment").val("");
	$('.callJq').openDOMWindow({   
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#workflowreject-comment', 
		loaderHeight:16, 
		loaderWidth:25 
	}); 
}
function saveComment(){
	var comment=$("#wf-reject-comment").val();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/comment_workflow_reject_save.action",
		data:{comment:comment},
	 	async: false,
	    dataType: "html",
	    cache: false,
	    success:function(result){
	    	$.ajax({
	    		type:"POST",
	    		url:"<%=request.getContextPath()%>/workflowProcess.action",
									data : {
										processFlag : 2,
										finalyzed : 1
									},
									async : false,
									dataType : "html",
									cache : false,
									success : function(result) {
										//$('#DOMWindowOverlay').trigger('click');
										window.location.reload();
									}
								});

					}
				});
	}

function confirmLogout() {
	$.ajax({
		type : "POST",
		async : false,
		dataType : "json",
		url : "<%=request.getContextPath()%>/confirm_logout.action",
		cache : false,
		success : function(response) {  
			$(location).attr('href','logout.do'); 
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", "${pageContext.request.contextPath}/index.jsp");
			$('<input>').attr({
			    type: 'hidden',
			    id: 'KEY',
			    name: 'KEY',
				value : response.implementationId
			}).appendTo(form);
			document.body.appendChild(form);
			form.submit();
		},
		error : function(e) {
			
		}
	}); 
}
</script>

<style>
.wrap {
	margin-left: 98px;
	width: 100%;
}
</style>
<body>
	<%-- <div id="header" class="header_main">
		<div id="top-menu">
			<c:choose>
				<c:when test="${sessionScope.lang eq 'ar' }">
					<div class="header_left">
						<img src="images/logo_ar.png" alt="" style="margin-top: 0px;" />
					</div>
				</c:when>
				<c:otherwise>
					<div class="header_left">
						<img src="images/logo.jpg" alt="" style="margin-top: 0px;" />
					</div>
				</c:otherwise>
			</c:choose>
			<div id="txt" class="txt_date">
				<div class="jclock"></div>
			</div>
			<div class="header_right">&nbsp;</div>
			<div class="topright_buttons">
				<a href="#" title="Dashboard" class="dashboard"><span
					class="backtodashboard headerLink"><fmt:message
							key="header.label.dashboard" /></span></a> <a href="#"
					title="Workflow Management" class="dashboard"><span
					class="workflow headerLink"><fmt:message
							key="header.label.workflow" /></span></a> <a href="#" title="Report"
					class="report"><span class="custom_report headerLink"><fmt:message
							key="header.label.report" /></span></a> <a href="#" title="Templates"
					class="dashboard"><span class="headerLink print_templates"><fmt:message
							key="header.label.template" /></span></a> <a href="#"
					title="Manage Reference Numbers" class="dashboard"><span
					class="headerLink manage_reference_nos"><fmt:message
							key="header.label.references" /></span></a> <a href="#"
					title="Change Password" class="changepass" id="changePassword"><span
					class="headerLink"><fmt:message
							key="header.label.changePasswd" /></span></a> <a href="#"
					title="Reset Password" class="resetpass" id="resetPassword"><span
					class="headerLink"><fmt:message
							key="header.label.resetPasswd" /></span></a> <a href="logout.do"
					class="logout" class="logout" title="Logout"><fmt:message
						key="header.label.logout" /></a> <a href="#" onclick="switchToAr()"
					style="color: grey;" title="Arabic">العربية</a> <span>&nbsp;|&nbsp;</span>
				<a href="#" onclick="switchToEn()" style="color: grey;"
					title="English">English</a> <span>&nbsp;|&nbsp;</span> <a
					href="http://erp.aio.ae/bts/index_main.action?KEY=${THIS.implementationId}&user=${USER.username}"
					target="_blank" style="color: grey;" title="Report Bug">Report
					Bug</a>
			</div>
			<span style="display: none;"><fmt:message
					key="header.label.login" /> : ${lastLoginDate } </span>
		</div>
		<!--
		<div id="sitename">
			<span class="logo float-left" style="font-size: 18px; color: white; margin-top: 5px;" title="${com}">
				<img src="${pageContext.request.contextPath}/images/AdelLogo1.png" alt="${com}" title="${com}" height="35px"/>  
			</span>
		</div>
		-->

		<div class="apps-marquee" style="display: none;" id="loading">
			<span class="apps-marquee-item" style="">
				<div class="apps-marquee-round-top"></div>
				<div class="apps-marquee-text">Please Wait Loading...</div>
				<div class="apps-marquee-round-bottom"></div>
			</span>
		</div>
		<div class="apps-marquee" style="display: none;"
			id="passwordChangeMessage">
			<span class="apps-marquee-item" style="">
				<div class="apps-marquee-round-top"></div>
				<div class="apps-marquee-text">Password changed successfully</div>
				<div class="apps-marquee-round-bottom"></div>
			</span>
		</div>
		<div id="headerdetails" style="display: none;">
			<input type="hidden" value="${COMPANY_NAME}" id="headerCompanyName">
			<input type="hidden" value="${COMPANY_ID}" id="headerCompanyId">
			<input type="hidden" value="" id="headerCategoryId"> <input
				type="hidden" value="" id="headerApplicationId"> <input
				type="hidden" value="" id="headerApplicationIdForTheme"> <input
				type="hidden" value="<%=session.getAttribute("lang")%>"
				id="headerLanguage"> <input type="hidden"
				value="${headerLedgerId.ledgerId}" id="headerLedgerId">
		</div>
		<div id="workflowreject-comment" style="display: none;">
			<div class="width100 float-left" id="hrm">
				<label class="width30">Comments/Remarks</label>
				<textarea rows="20" cols="50" id="wf-reject-comment"></textarea>
			</div>
			<div class="width100 float-left" id="hrm">
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
					<div class="portlet-header ui-widget-header float-right"
						id="wf-save-comment">Finalize</div>
				</div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
					<div class="portlet-header ui-widget-header float-right"
						id="wf-close-comment">Close</div>
				</div>
			</div>
		</div>
		<div id="menu_menu" class="">
			<div id="menuResult" style="display: none;"></div>
		</div>
	</div> --%>


	<div>

		<div class="wrap">

			<div id="menu_menu" class="orange">
				<div id="menuResult" style="display: none;"></div>
			</div>






			<%-- <ul id="nav">
				<li class="active"><a href="#" class="dashboard"><span style="float:left;" class="backtodashboard headerLink"><fmt:message key="header.label.dashboard"/></span></a></li>
				<li><a href="inner.html" class="RealEstate">Real
							Estate</a></li>
				<li><a href="#" class="hrmodule">hr module</a></li>
				<li><a href="#" class="accounts"><span>accounts</span></a>

					<ul>
						<li><a href="#">Property</a></li>
						<li><a href="#">offer form</a>
						<li><a href="#">contracts </a></li>
						<li><a href="#">expense</a></li>
						<li><a href="#">legal</a></li>
						<li><a href="#">tenant groups</a>
							<ul>
								<li><a href="#">Level 2</a></li>
								<li><a href="#">Level 21</a></li>
								<li><a href="#">Level 2</a></li>
								<li><a href="#">Level 2</a></li>
							</ul></li>



					</ul></li>
			</ul> --%>
		</div>

		<div class="top_right_button">
			<c:if test="${implementationGroups ne null}">
				<div style="right:117%; margin: 15px 0 0 0 !important;position: absolute;">
					<select id="implementationGroups">
						<c:forEach items="${implementationGroups}" var="imp">
							<c:choose>
							<c:when test="${newImplementationId eq imp.implementationId}">
								<option value="${imp.implementationId}" selected="selected">${imp.companyName}</option>
							</c:when>
							<c:otherwise>
								<option value="${imp.implementationId}">${imp.companyName}</option>
							</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
			</c:if>

			<div class="top_right_button1" id="dashboard_position_change_save" style="display: none;">
				<a href="javascript:saveContentPositionByUser();">
					<img src="images/warning-icon.png" width="40px" title="Click to save dashboard changes" />
				</a> 

			</div>
			
			<!-- <img style="float: left; margin: 21px 0 0 0 !important;"
				src="images/text.png" /> -->

			<div class="top_right_button1">
				<a id="help_btn" href="#" title=""><img src="images/top_b_icon1.png" /></a>
			</div>
			<div id="helpDiv" style="display: none;">
				<div class="help_div" >
					<h5>SHORTCUTS</h5>
					<ul style="line-height: 20px; margin-top: 5px;">
						<li style="font-size: 10px; color: #878787;">Dashboard<span style="float: right; display: inline-block; font-size: 11px;">[CTRL+SHIFT+D]</span></li>
						<li style="font-size: 10px; color: #878787;">Logout<span style="float: right; display: inline-block; font-size: 11px;">[CTRL+SHIFT+L]</span></li>
						<li style="font-size: 10px; color: #878787;">Reports<span style="float: right; display: inline-block; font-size: 11px;">[CTRL+SHIFT+E]</span></li>						
					</ul>
				</div>
			</div>

			<!-- <div class="top_right_button1">
				<a href="#"><img src="images/top_b_icon2.png" /></a>
			</div> -->

			<div class="top_right_button1">
				<a onclick="confirmLogout();" style="cursor: pointer;" title="Logout"><img style="margin: 5px 0 0 8px !important;"
					src="images/top_b_icon3.png" /></a>
			</div>

		</div>
		<div id="common_success_message" class="response-msg success ui-corner-all" style="display:none;"></div>
		<div id="common_error_message" class="response-msg error ui-corner-all" style="display:none;"></div> 
	</div>
</body>

</html>