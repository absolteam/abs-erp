<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<link rel="stylesheet" type="text/css"
	href="dashboard/jquery-ui.css" />
<link class="include" rel="stylesheet" type="text/css"
	href="dashboard/jquery.jqplot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shThemejqPlot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/aiosChartMobile.css" />

<style type="text/css">
/* .jqplot-point-label {
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
  font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
}
.jqplot-data-label{
	border: 1.5px solid #aaaaaa;
  padding: 3px;
  background-color: #eeccdd;
 font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
} */
</style>	
<!-- Don't touch this! -->

<script type="text/javascript" src="dashboard/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.trendline.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.blockRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.enhancedLegendRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pointLabels.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="dashboard/js/aiosChartMobile.js"></script>
<!-- End Don't touch this! -->

<script type="text/javascript">
var  plot1=null;var  plot2=null;var  plot3=null;var  plot4=null;

$(function() {
	setStyle();
	dg1(); 
});

function dg1(){ 
	var s1 = [];
	var s2 = [];
	var s3 = [];
	var s4 = [];
	var s5 = [];
	var ticks=[];
	$.ajax({
	  type:"POST",
	  url:'getAllFiscalTransactionByPeriod.action',
      async: false,
      dataType:"json",
      success: function(response) {
    	  $(response.data)
			.each(function(index) {
				ticks.push(response.data[index].ticks);
		    	var voList=response.data[index].dashboardVOs;
				s1.push(Number(voList[0].amount));	
				s2.push(Number(voList[1].amount));
				s3.push(Number(voList[2].amount));
				s4.push(Number(voList[3].amount));
				s5.push(Number(voList[4].amount));
			});
		$('#content1_chart').html('');
	   }
	});
  	var legendLabels = ['Asset', 'Liability', 'Revenue','Expense','Owners Equity'];
  	plot1 = drawBarChart([s1,s2,s3,s4,s5],ticks,'content1_chart',legendLabels); 
   	
	$('#content1_chart').height($('#content1').height()*0.86);
	$('#content1_chart').width($('#content1').width()*0.96);
	 plot1.replot( { resetAxes: true } );
	 $('#content1').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content1_chart').height($('#content1').height()*0.86);
	 $('#content1_chart').width($('#content1').width()*0.96);
	 plot1.replot( { resetAxes: true } );
	 }); 
	 
  	setTimeout(dg2(), 2000);
}
function dg2(){
	var s1 = [];
	var s2 = [];
	var s3 = [];
	var s4 = [];
	var s5 = [];
	var ticks=[];
	$('#content2_chart img').css('display', 'block');
	$.ajax({
		  type:"POST",
			url:'getAllFiscalTransactionByCalendar.action',
	      async: false,
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					ticks.push(response.data[index].ticks);
			    	var voList=response.data[index].dashboardVOs;
					s1.push(Number(voList[0].amount));	
					s2.push(Number(voList[1].amount));
					s3.push(Number(voList[2].amount));
					s4.push(Number(voList[3].amount));
					s5.push(Number(voList[4].amount));
				});
	    	  $('#content2_chart').html('');
	      }
	    });
  	var legendLabels = ['Asset', 'Liability', 'Revenue','Expense','Owners Equity'];
  	plot2 = drawBarChart([s1,s2,s3,s4,s5],ticks,'content2_chart',legendLabels);
  	
  	$('#content2_chart').height($('#content2').height()*0.86);
	$('#content2_chart').width($('#content2').width()*0.96);
	plot2.replot( { resetAxes: true } );
	$('#content2').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content2_chart').height($('#content2').height()*0.86);
	 $('#content2_chart').width($('#content2').width()*0.96);
	 plot2.replot( { resetAxes: true } );
	});
	
  	 
	setTimeout(dg3(), 2000);
}
function dg3(){
	var s1 = [];
	var s2 = [];
	var s3 = [];
	var s4 = [];
	var s5 = [];
	var ticks=[];
	$('#content3_chart img').css('display', 'block');
	$.ajax({
		  type:"POST",
		  url:'getAllFiscalTransaction.action',
	      async: false,
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					ticks.push(response.data[index].ticks);
			    	var voList=response.data[index].dashboardVOs;
					s1.push(Number(voList[0].amount));	
					s2.push(Number(voList[1].amount));
					s3.push(Number(voList[2].amount));
					s4.push(Number(voList[3].amount));
					s5.push(Number(voList[4].amount));
				});
	    	  $('#content3_chart').html('');
	      }
	    });
  	var legendLabels = ['Asset', 'Liability', 'Revenue','Expense','Owners Equity'];
  	plot3 = drawBarChart([s1,s2,s3,s4,s5],ticks,'content3_chart',legendLabels);
  	
  	$('#content3_chart').height($('#content3').height()*0.86);
	$('#content3_chart').width($('#content3').width()*0.96);
	plot3.replot( { resetAxes: true } );
	$('#content3').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content3_chart').height($('#content3').height()*0.86);
	 $('#content3_chart').width($('#content3').width()*0.96);
	 plot3.replot( { resetAxes: true } );
	});
} 	 
</script>


</head>
<div id="valids"
	style="display: none; min-height: 100px; width: 150px; display: none; background-color: #FFF; 
		border: 1px solid #D7D7D7; margin: 200px 0 0 1px; position: absolute; z-index:9999;">
	<input id="effectTypes" type="hidden" name="effects" value="blind">

	<div style="width: 150px; background-color: #eee; height: 20px;">
		<input type="checkbox" id="dg_check_all" /><a href="#" style="margin-top:5px;" id="button">Charts</a>
	</div> 

	<div class="toggler">
		<div id="effect">
			<p>
				<input type="checkbox" id="dg1_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg1')">1.Periodic Ledger</a><br>
				<input type="checkbox" id="dg2_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg2')">2.Fiancial Year</a><br>
				<input type="checkbox" id="dg3_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg3')">3.Ledger Statement</a><br>
 			</p> 
		</div>
    </div> 
</div> 
<div id="containment-wrapper">

	<div id='dg1' class="dg">
		
		<div class='content' id='content1'>
			<div class='bar' id='bar1'>
				<a href="javascript:ReverseDisplay('dg1')">
				</a>
				<div
					class="dashboard-content-title">Periodic Ledger</div>
			</div>
			<div id="content1_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>
	</div>

	<div id='dg2' class="dg">
		<div class='content' id='content2'>
			<div class='bar' id='bar2'><a href="javascript:ReverseDisplay('dg2')">&nbsp;</a>
				<div class="dashboard-content-title">Fiancial Year</div>
			</div>
			<div id="content2_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div>

	<div id='dg3' class="dg">
		<div class='content' id='content3'>
			<div class='bar' id='bar3'><a href="javascript:ReverseDisplay('dg3')">&nbsp;</a>
				<div class="dashboard-content-title">Ledger Statement</div>
			</div>
			<div id="content3_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div> 
</div> 