<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var slidetab;
var slidetabA;
var taskidentity;
$(function(){  
	var processFlag=null;
	/* workflowcontentCall();
	//Tab content
	 $(".button_left2 a[id^=tab_menu]").hover(function() {
	        var curMenu=$(this);
	        $(".button_left2 a[id^=tab_menu]").removeClass("active");
	        curMenu.addClass("active");

	        var index=curMenu.attr("id").split("tab_menu_")[1];
	        $(".contact_container .tabcontent").css("display","none");
	        $(".contact_container #tab_content_"+index).css("display","block");
	    }); */
	 
	 $('#loadMore').live('click', function () { 
		 leftsideAlertCall(false);
	 });
	    
	 //Call New dashboard call   
	 menuAjaxCallFunction("<div id='dashboardHome'></div>");
});
<%-- function workflowcontentCall(){
	// ----------Workflow Notification approval------------------------
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflow_notification.action",
		data: {
     			processFlag:3
     		},
	 	async: false,
	    dataType: "html",
	    cache: false,
	    success:function(result){
	 		 $("#workflowcontent-approval").html(result);
	 		 var count=Number($("#mainrecordCount_3").val());
	 		 if(count>0){
	 		 }else{
	 			count=0; 
	 		 }
	 		$("#approval_count").html("<p style='margin-top: -10px; font-size: small ! important;'>"+count+"</p>");
	 		
		}
	});
	// ----------Workflow Notification approved------------------------
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflow_notification.action",
		data: {
				processFlag:8
     		},
	 	async: false,
	    dataType: "html",
	    cache: false,
	    success:function(result){
	 		 $("#workflowcontent-approved").html(result);
	 		 var count1=Number($("#mainrecordCount_8").val());
	 		 if(count1>0){
	 		 }else{
	 			count1=0; 
	 		 }
		 	$("#approved_count").html("<p style='margin-top: -10px; font-size: small ! important;'>"+count1+"</p>");
		}
	});
	// -------Alert----------------------------
		leftsideAlertCall(true);

	}
	
	function leftsideAlertCall(applyLimit){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_alert.action",
				async : true,
				data:{applyLimit:applyLimit},
				dataType : "html",
				cache : false,
				success : function(result) {
					$("#workflowcontent-alert").html(result);
					var alertcount = 0;
					alertcount = Number($("#alertCount").val());
					if (alertcount > 0) {
					} else {
						alertcount = 0;
					}
					$("#alert_count").html(
							"<p style='margin-top: -10px; font-size: small ! important;'>"
									+ alertcount + "</p>");
				}
			});
		
	} --%>
</script>
<!-- <style>
#highlight{
	cursor:pointer;
}

#page-wrapper #main-wrapper #main-content {
	/* margin-right: 242px; */
	/* padding: 10px 10px 10px; */
	z-index: 999;
	height: 98%;
	overflow-x: hidden;
	border: thin solid #CCCCCC;
	border-radius: 3px 3px 3px 3px;
}

#main-content {
	height:99%;
}

 <!-- Tab code-->
<!--  div.tabscontainer{
    margin:15px 0px;
}

div.cont_left a span{
   display: block;
    height: 23px;
    width: 25px;
    background: url(images/notification-bg.png) no-repeat;
    background-size:25px 23px;
    background-position:center;
    text-align: center;
    color:#ffffff;
    position:absolute;
    top:10px;
    right:10px;
    
}

div.tabs div.tab div.arrow{
    position: absolute;
    background: url(images/homeSelArrow.png) no-repeat;
    background-size:12px 45px;
    height: 50px;
    width: 17px;
    left: 100%;
    top: 0px;
    display: none;
    margin-top:8px;
}

div.tabs div.tab.selected div.arrow{
    display: block;
}
</style> -->
<%-- <div id="main-wrapper"  class="content_left">
	<div id="main-content">
	     <div class="cont_left">
	         <a class="active" style="border-top-left-radius:5px; border-top-right-radius:5px;" id="tab_menu_1">
	             <fmt:message key="common.dashboard.waitingforapproval"/><span id="approval_count"></span>
	             
	         </a>
	         <a id="tab_menu_2">
	             <fmt:message key="common.dashboard.approved/rejected"/><span id="approved_count"></span>
	             
	         </a>
	        <a id="tab_menu_3" style="border-bottom-left-radius:5px; border-bottom-right-radius:5px;">
	             <fmt:message key="common.dashboard.alerts"/><span id="alert_count"></span>
	             
	         </a>
	        <!--  <img src="images/shade.png" alt="" style="position:absolute; bottom:-22px; left:-8px; z-index:-1;">  -->
	    </div>
		<div class="cont_right">
			<div class="tabcontent_scroll">
				<div class="tabcontent" id="tab_content_1" style="display:block">
					<div>
						<ul>
							<div id="workflowcontent-approval"></div> 
						</ul>	
					</div>
				</div>
				<div class="tabcontent" id="tab_content_2">
					<div>
						<ul>
							<div id="workflowcontent-approved"></div> 
						</ul>	
					</div>
				</div>
				<div class="tabcontent" id="tab_content_3">
					<div>
						<ul>
							<div id="workflowcontent-alert"></div> 
						</ul>	
					</div>
				</div>
			</div>
			<!-- <div class="beta"><img src="images/beta.png" alt="" /></div> -->
		</div>
		
		<div class="clearfix"></div>
	 </div> 
</div>  --%>
<div id="page-wrapper" >
<!-- 	<div id="page-wrapper-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
 -->	<div id="page-wrapper-hidden-div" style="display:none;"></div>
	<div class="contact_container">
	
		<div id="main-wrapper" class="content_left">
		
			<div id="main-content">
	
			<!-- 	<div class="tabcontent" id="tab_content_1">
	
					<div id="workflowcontent-approval"></div>
	
				</div>
	
	
				<div class="tabcontent" id="tab_content_2" style="display: none;">
	
					<div id="workflowcontent-approved"></div>
	
				</div>
	
	
				<div class="tabcontent" id="tab_content_3" style="display: none;">
	
					<div id="workflowcontent-alert"></div>
	
				</div>
				
	 -->
	
			</div>
		</div>
	</div>
</div>