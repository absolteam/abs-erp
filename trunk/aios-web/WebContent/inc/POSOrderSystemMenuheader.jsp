<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.aiotech.aios.system.*"%>
<%@page import="com.aiotech.aios.workflow.domain.entity.Menu"%>
<%@page import="com.aiotech.aios.system.domain.entity.vo.MenuVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ResourceBundle"%>

<style>
.down {
	float: left !important;
}
.oe_overlay{
	background:#000;
	opacity:0;
	position:fixed;
	top:0px;
	left:0px;
	width:100%;
}
</style>


<div id="oe_overlay" class="oe_overlay"></div>
<ul id="mega-menu-5" class="mega-menu" style="display: none;">
	<li class="Dashboard "><a onclick="callPointOfSale('online-menu-discard')" id="dashboard"
		class="backtodashboard menu_text_wrap dashboard" href="#"> <span class="down menu_text_wrap_span_header">Dashboard</span>
	</a> 
	</li>

	<%!int count = 0;
	String className = "";
	Map<Long, Boolean> menuRights;
	String locale = "en";

	void renderGuiMenu(HttpSession session, JspWriter out) throws Exception {

		try {
			String tempLocale = (String) session.getAttribute("lang");
			if (tempLocale != null)
				locale = tempLocale;
		} catch (Exception e) {
			locale = "en";
		}

		if (session.getAttribute("MENU_MAP") != null) {

			Map<String, Object> menuMap = (Map<String, Object>) session
					.getAttribute("MENU_MAP");
			List<MenuVO> menuItems = (List<MenuVO>) menuMap.get("MENU");
			menuRights = (Map<Long, Boolean>) menuMap.get("MENU_RIGHTS");

			List<MenuVO> menu = menuItems;
			Collections.sort(menu);

			for (MenuVO menuItem : menu) {
				boolean flag = false;

				if (menuRights.get(menuItem.getMenuItemId()) != null) {

					if (menuItem.getMenus() != null
							&& menuItem.getMenus().size() > 0) {
						out.println("<li>&nbsp;&nbsp;<a href='#'"
								+ (menuItem.getLogo()!=null?"style=\"background: url(images/"+menuItem.getLogo()+") center left no-repeat;\"":"")
								+ "class=\"menu_text_wrap default_menu_logo "
								+ " top_link\" id=\""
								+ menuItem.getScreen().getScreenPath()
								+ "\" onclick=\"\"><span class=\"down menu_text_wrap_span_header \">"
								+ (locale.equals("ar") ? menuItem
										.getItemTitleAr() : menuItem
										.getItemTitle()) + "</span></a>");

						flag = true;

					} else {
						
						out.println("<li>&nbsp;&nbsp;<a href='#'"
								+ (menuItem.getLogo()!=null?"style=\"background: url(images/"+menuItem.getLogo()+") center left no-repeat;\"":"")
								+ "class=\"menu_text_wrap default_menu_logo "
								+ " top_link\" id=\""
								+ menuItem.getScreen().getScreenPath()
								+ "\" onclick=\"menuAjaxCallFunction(this);\"><span class=\"down menu_text_wrap_span_header \">"
								+ (locale.equals("ar") ? menuItem
										.getItemTitleAr() : menuItem
										.getItemTitle()) + "</span></a>");
						/* out.println("<ul id=\"nev" + ++count + "\"class=\"sub\">"); */
					}

					if (flag) {

						out.println("<ul id=\"nev" + ++count
								+ "\" class=\"sub bottom-left-rounded-corner bottom-right-rounded-corner\">");
						recursivelyPopulateMenu(menuItem, out);

						out.println("</ul>");
					}
					out.println("</li>");
				}
			}
		}
	}

	void recursivelyPopulateMenu(MenuVO menu, JspWriter out) throws Exception {

		if (menuRights.get(menu.getMenuItemId()) != null) {

			List<MenuVO> subMenu = menu.getMenus();
			Collections.sort(subMenu);

			for (MenuVO subItem : subMenu) {

				if (menuRights.get(subItem.getMenuItemId()) != null) {

					if (subItem.getMenus() == null
							|| subItem.getMenus().size() == 0) {

						className = "";
						out.println("<li><a href='#' id=\""
								+ subItem.getScreen().getScreenPath()
								+ "\" onclick=\"menuAjaxCallFunction(this);\" class=\"menu_text_wrap\""
								+ className
								+ "\"><span class=\"menu_text_wrap_span \">"
								+ (locale.equals("ar") ? subItem
										.getItemTitleAr() : subItem
										.getItemTitle()) + "</span></a> </li>");
						continue;
					} else {
						className = "parent";
						out.println("<li><a href='#' id=\""
								+ subItem.getScreen().getScreenPath()
								+ "\" onclick=\"\" class=\"fly menu_text_wrap \""
								+ className
								+ "\"><span class=\"menu_text_wrap_span \">"
								+ (locale.equals("ar") ? subItem
										.getItemTitleAr() : subItem
										.getItemTitle()) + "</span></a>");

						out.println("<ul>");
						recursivelyPopulateMenu(subItem, out);
						out.println("</ul>");
						out.println("</li>");
					}
				}
			}
		}
	}%>

	<%
		renderGuiMenu(session, out);
	%>

	<!-- <li class="last" style="display: none;"><a><span>dummy</span>
	</a>
	</li> -->
</ul>
<script type="text/javascript">




function menuAjaxCallFunction(menuItem){
	//slideToggleSidebar();
	destroyPopups();
	$('#oe_overlay').stop(true,true).fadeTo(200, 0);
	$('.sub').fadeOut();
	var requiredAction = $(menuItem).attr('id');
	$.ajax({
		type:"POST",
		url:'<%=request.getContextPath()%>/' + requiredAction
							+ '.action',
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$("#main-wrapper").html(result);
						$('#loading').fadeOut();
					}
				});

		// method defined in AfterCompanySelectionHeader.jsp
		hideDashboardButtons();
	}

	jQuery(document).ready(function($) {
			$('#mega-menu-5').dcMegaMenu({
                rowItems: '4',
                speed: 'fast',
                effect: 'slide'

            });
	});
	
	$(function() {
		var $oe_menu		= $('#mega-menu-5');
		var $oe_overlay		= $('#oe_overlay');
		$oe_menu.bind('mouseenter',function(){
			$("#oe_overlay").css("height",100+"%");
			var $this = $(this);
			$oe_overlay.stop(true,true).fadeTo(200, 0.6);
			$("#main-wrapper").css("z-index",-100);
		}).bind('mouseleave',function(){
			$("#oe_overlay").css("height",0+"%");
			var $this = $(this);
			$oe_overlay.stop(true,true).fadeTo(200, 0);
			$("#main-wrapper").css("z-index",100);
		})
    });

    function destroyPopups() {
    	$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();   
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
    }
</script> 