<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<meta http-equiv="Content-Type charset=utf-8" /> 
<title>Adel | Sign In</title>  
<link rel="stylesheet" href="/css/resetlogin.css"  type="text/css" media="screen" />
<link rel="shortcut icon" href="/images/adel1.jpg" type="image/x-icon" />
<link href="/css/general.css" rel="stylesheet"  type="text/css" media="all" />
<link href="/css/reset.css" rel="stylesheet"   type="text/css" media="all" />
<link href="/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />  
<link rel="stylesheet" href="/css/style.css"  type="text/css" media="screen" />  
<link rel="stylesheet" href="/css/invalid.css"  type="text/css" media="screen" />	
 <link href="/css/messages.css" rel="stylesheet"  type="text/css" media="all" /> 
 <link rel="stylesheet" href="/css/validationEngine.jquery.css" type="text/css" media="screen"/>
 
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script> 
<script type="text/javascript" src="/js/countDown.js"></script> 
<script type="text/javascript" src="/js/countDownMail.js"></script>  
<script src="/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
	

<script type="text/javascript">

$(document).ready(function (){ 
 	  //var com=$('#companyName').val();
		//var id=$('#companyId').val();
 		fullDomain = window.location.pathname;
		domainArray = fullDomain.split('/');
		subDomain=domainArray[1];
		urlName=subDomain.split('-');
 		window.location.href="/"+urlName[0]+"/user_selected_company.action";
  });

</script>
</head>
<body>
<div>
<input type="hidden" id="companyName" value="${loginBean.companyName}"/><input type="hidden" id="companyId" value="${loginBean.companyId}"/></div></body>
</html>