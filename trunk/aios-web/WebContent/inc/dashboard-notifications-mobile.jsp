<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">

	getWaitingForApproval();
	 		
	function getWaitingForApproval() {
		
		updateActive('pending');
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_notification.action",
			data: { processFlag: 3 },
		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){
		 		$("#workflowcontent-approval").html(result);
				count=0;
		 		$("#approval_count").html("<p style='margin-top: -10px; font-size: small ! important;'>" 
		 			+ count + "</p>");
			},
			error : function(result) {
				$("#approval_count").html(
					"<p style='margin-top: -10px; font-size: small ! important;'>"
						+ 0 + "</p>");
			}
		});
	} 	
	
	function getApprovedRejected() {
		
		updateActive('history');
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_notification.action",
			data: { processFlag: 8 },
		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){
		 		$("#workflowcontent-approved").html(result);
		 		count1=0; 
			 	$("#approved_count").html("<p style='margin-top: -10px; font-size: small ! important;'>" 
			 		+ count1 + "</p>");
			},
			error : function(result) {
				$("#approved_count").html(
					"<p style='margin-top: -10px; font-size: small ! important;'>"
						+ 0 + "</p>");
			}
		});
	}
	
	function getAlerts(applyLimit){
		
		updateActive('alerts');
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_alert.action",
				async : true,
				data:{ applyLimit: applyLimit },
				dataType : "html",
				cache : false,
				success : function(result) {
					$("#workflowcontent-alert").html(result);
					var alertcount = 0;						
					$("#alert_count").html(
						"<p style='margin-top: -10px; font-size: small ! important;'>"
							+ alertcount + "</p>");
				},error : function(result) {
					$("#alert_count").html(
						"<p style='margin-top: -10px; font-size: small ! important;'>"
							+ 0 + "</p>");
				}
		});	
	}
		
	function updateActive(id) {
		
		$(".tabcontent").hide();
		$("#tab_content_" + id).show();
		
		$(".opt").removeClass('active');
		$(".opt").addClass('in-active');
		$("#" + id).removeClass('in-active');
		$("#" + id).addClass('active');
	}
	
</script>

<style>
	.tabHandle {
		color: black;
		background: #fcfcfc;
		border-bottom: 1px solid #ddd;
		border-right: 1px solid #ddd;
		cursor: pointer;
		padding: 10px;
		padding-right: 3px;
		border-radius: 3px;
		display: block;
		margin-bottom: 5px;
		font-size: 12px; 
	}
	
	.active {
		font-weight: bolder;
		color: #333;
		border-left: 3px solid #A6BAFF;
		padding-left: 3px;
	}
	
	.in-active {
		font-weight: normal;
		border-left: 0px;
		color: #aaa;
	}
	
	* {
		font-family: wf_segoe-ui_normal,"Segoe UI",Segoe,"Segoe WP",Tahoma,"Trebuchet MS",Arial,Helvetica,sans-serif !important;
	}
	
	.tab_options {
		position: fixed;
		width: 85px;
		top: 70px;
		left: -2px;
	}
	
	.row_button {
		display: none;
	}
	
	.row1 {
		line-height: 1;
		border-bottom: 1px solid #eee;
		font-size: 13px;
		padding: 8px;
		padding-top: 15px;
		box-shadow: 0px 0px 1px 0px #EEE;
		background: #FBFBFB none repeat scroll 0% 0%;
		border-radius: 3px;
		margin-bottom: 6px;
	}
	
	.load {
		font-size: 13px;
		color: #929292;
		text-transform: uppercase;
	}
	
	h2 {
		font-weight: 300;
	}
	
	h6 {
		color: gray;
		margin-top: -5px;
		font-weight: 400;
	}
	
	.tabcontent {
		margin-bottom: 50px;
	}
	
	.float-left {
		display: none;
	}
	
	.wf_timestamp {
		margin-top: 4px;
		margin-right: 4px;
	}
</style>

<div class="tab_options">	
	<div>		
		<span class="tabHandle" onclick="getWaitingForApproval()">
			<span id="pending" class="active opt">Pending</span>
		</span>
	
		<span class="tabHandle" onclick="getApprovedRejected()">
			<span id="history" class="in-active opt">History</span>
		</span>
	
		<span class="tabHandle" onclick="getAlerts(true)">
			<span id="alerts" class="in-active opt">Alerts</span>
		</span>
	</div>
	<div>
		
	</div>
</div>

<div style="margin-top: 65px; margin-left: 110px;">	
	<div id="" style="overflow-y: auto; overflow-x: hidden;">		
		<div class="tabcontent" id="tab_content_pending">
			<h2>Waiting for Approval</h2>
			<h6>Following information requires your action, for further processing.</h6>
			<div id="workflowcontent-approval"><img src="${pageContext.request.contextPath}/images/load.gif" width="40"/></div>
		</div>

		<div class="tabcontent" id="tab_content_history">
			<h2>Approved & Rejected</h2>
			<h6>Information presented here has already been processed.</h6>
			<div id="workflowcontent-approved"><img src="${pageContext.request.contextPath}/images/load.gif" width="40"/></div>
		</div>

		<div class="tabcontent" id="tab_content_alerts">	
			<h2>Alerts</h2>
			<h6>Following alerts are system generated, and may or may not require your direct action.</h6>
			<div id="workflowcontent-alert"><img src="${pageContext.request.contextPath}/images/load.gif" width="40"/></div>	
		</div>
	</div>
</div>