<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<link rel="stylesheet" type="text/css"
	href="dashboard/jquery-ui.css" />
<link class="include" rel="stylesheet" type="text/css"
	href="dashboard/jquery.jqplot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/shThemejqPlot.min.css" />
<link type="text/css" rel="stylesheet"
	href="dashboard/styles/aiosChartMobile.css" />
<style type="text/css">
/* .jqplot-point-label {
  border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;
  font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
}
.jqplot-data-label{
	border: 1.5px solid #aaaaaa;
  padding: 3px;
  background-color: #eeccdd;
  font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
	font-weight: bold;
  	font-size: 12px;
} */
#startDate {
	width: 110px;
}
#endDate {
	width: 110px;
}
</style>
	
<!-- Don't touch this! -->

<script type="text/javascript" src="dashboard/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.trendline.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.blockRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.enhancedLegendRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pointLabels.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript"
	src="dashboard/js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="dashboard/js/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="dashboard/js/aiosChartMobile.js"></script>
<!-- End Don't touch this! -->

<script type="text/javascript">
var  plot5=null;var  plot6=null;var  plot7=null;var  plot8=null;
var startDate = null; var endDate = null; var storeId = Number(0);
var storeId=0;
$(function() {  
	setStyle();
	dg5();
});  
function dg5(){
	var s1=[];
	var ticks=[];
	$.ajax({
		type:"POST",
		url:'dashboardMonthlySales.action',
	    async: false,
	    data:{storeId:storeId},
	    dataType:"json",
	    success: function(response) {
    	  	$(response.data)
			.each(function(index) {
				s1.push(Number(response.data[index].amount));
				ticks.push(response.data[index].startDateView);
			}); 
    	  	$('#content5_chart').html('');
	      } 
	  }); 
	plot5 = drawBarChartWithoutLegends([s1],ticks,'content5_chart'); 
	$('#content5_chart').height($('#content5').height()*0.86);
	$('#content5_chart').width($('#content5').width()*0.96);
	plot5.replot( { resetAxes: true } );
	$('#content5').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content5_chart').height($('#content5').height()*0.86);
	 $('#content5_chart').width($('#content5').width()*0.96);
	 plot5.replot( { resetAxes: true } );
	}); 
	setTimeout(dg6(), 2000);
}
function dg6(){
	var s1=[];
	var ticks=[];
	$('#content6_chart img').css('display', 'block');
	  $.ajax({
		  type:"POST",
			url:'dashboardYearlySales.action',
	      async: false,
	      data:{storeId:storeId},
	      dataType:"json",
	      success: function(response) {
	    	  $(response.data)
				.each(function(index) {
					s1.push(Number(response.data[index].amount));
					ticks.push(response.data[index].startDateView);
				});
	    	  $('#content6_chart').html('');
	      }
	    });
	plot6 = drawBarChartWithoutLegends([s1],ticks,'content6_chart');
	 
	$('#content6_chart').height($('#content6').height()*0.86);
	$('#content6_chart').width($('#content6').width()*0.96);
	plot6.replot( { resetAxes: true } );
	$('#content6').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content6_chart').height($('#content6').height()*0.86);
	 $('#content6_chart').width($('#content6').width()*0.96);
	 plot6.replot( { resetAxes: true } );
	}); 
 	setTimeout(dg7(), 2000);
}
	
function dg7(){
	$('#content7_chart img').css('display', 'block');
  	var s1=[];
	var ticks=[];
  	$.ajax({
	  type:"POST",
		url:'dashboardDailySales.action',
      async: false,
      data:{storeId:storeId},
      dataType:"json",
      success: function(response) {
    	  $(response.data)
			.each(function(index) {
				s1.push(Number(response.data[index].amount));
				ticks.push(response.data[index].startDateView);
			});
    	  $('#content7_chart').html('');
      }
    }); 
  plot7 = drawBarChartWithoutLegends([s1],ticks,'content7_chart'); 
 
  $('#content7_chart').height($('#content7').height()*0.86);
  $('#content7_chart').width($('#content7').width()*0.96);
	plot7.replot( { resetAxes: true } );
	$('#content7').bind('resize', function(event, ui) {
	  // pass in resetAxes: true option to get rid of old ticks and axis properties
	  // which should be recomputed based on new plot size.
	 $('#content7_chart').height($('#content7').height()*0.86);
	 $('#content7_chart').width($('#content7').width()*0.96);
	 plot7.replot( { resetAxes: true } );
	}); 
  setTimeout(dg8(), 2000);
} 
function dg8(){ 
	$('#content8_chart img').css('display', 'block');
	var data=[]; 
	$.ajax({
		  type:"POST",
			url:'salesByCategory.action',
	      async: false,
	      data:{startDate:startDate, endDate:endDate, storeId:storeId},
	      dataType:"json",
	      success: function(response) {
	    	  data=[];
	    	  $(response.data)
				.each(function(index) {
					data.push([response.data[index].name,Number(response.data[index].amount)]);	
				});
	    	  $('#content8_chart').html('');
	      }
	    });
	plot8 = drawPieChart([data],'content8_chart');

	$('#content8_chart').height($('#content8').height()*0.86);
	  $('#content8_chart').width($('#content8').width()*0.96);
	  plot8.replot( { resetAxes: true } );
		$('#content7').bind('resize', function(event, ui) {
		  // pass in resetAxes: true option to get rid of old ticks and axis properties
		  // which should be recomputed based on new plot size.
		 $('#content8_chart').height($('#content8').height()*0.86);
		 $('#content8_chart').width($('#content8').width()*0.96);
		 plot8.replot( { resetAxes: true } );
		}); 
 }
</script>


</head>

<div id="valids"
	style="display: none; min-height: 200px; width: 150px; display: none; background-color: #FFF; 
		border: 1px solid #D7D7D7; margin: 200px 0 0 1px; position: absolute; z-index:9999;">
	<input id="effectTypes" type="hidden" name="effects" value="blind">
 	<div style="width: 150px; background-color: #eee; height: 20px;">
		<input type="checkbox" id="dg_check_all" /><a href="#" id="button">Charts</a>
	</div> 
	<div class="toggler">
		<div id="effect">
			<p>
				<input type="checkbox" id="dg5_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg5')">1.Sales(Monthly)</a><br>
				<input type="checkbox" id="dg6_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg6')">2.Sales (Yearly)</a><br>
				<input type="checkbox" id="dg7_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg7')">3.Sales(Daily)</a><br>
				<input type="checkbox" id="dg8_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg8')">4.Sales By Category</a><br>
				<input type="checkbox" id="dg1000_check" class="dg_check"/><a href="javascript:ReverseDisplay('dg1000')">5.Filter</a><br>
			</p> 
		</div>
    </div>  
</div> 

<div id="containment-wrapper">
	
	<div id='dg1000' class="dg">
		
		<div class='content' id='content1000'>
			<div class='bar' id='bar1000'
				style='width: auto; height: 23px; overflow: hidden;'>
				<a href="javascript:ReverseDisplay('dg1000')">
				</a>
				<div
					class="dashboard-content-title">Filters
					</div>
			</div>
			
			<div id="content1000_chart">
				<div style="padding: 5px;">
					<label class="dashboard-content-filter-label">Store :
					</label>
					<select id="store" style="min-width:40%;">
						<option value="">ALL</option>
						<c:forEach items="${STORES}" var="str">
							<option value="${str.storeId}">${str.storeName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>
	</div> 
	
	<div id='dg5' class="dg">
		<div class='content' id='content5'>
			<div class='bar' id='bar5'><a href="javascript:ReverseDisplay('dg5')">&nbsp;</a>
				<div class="dashboard-content-title">Monthly Sales</div>
			</div>
			<div id="content5_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif"
					width="40" />
			</div>
		</div>
	</div> 

	<div id='dg6' class="dg">
		<div class='content' id='content6'>
			<div class='bar' id='bar6'><a href="javascript:ReverseDisplay('dg6')">&nbsp;</a>
				<div class="dashboard-content-title">Yearly Sales</div>
			</div>
			<div id="content6_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div>

	<div id='dg7' class="dg">
		<div class='content' id='content7'>
			<div class='bar' id='bar7'><a href="javascript:ReverseDisplay('dg7')">&nbsp;</a>
				<div class="dashboard-content-title">Daily Sales</div>
			</div>
			<div id="content7_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div>
	
	<div id='dg8' class="dg">
		<div class='content' id='content8'>
			<div class='bar' id='bar8'><a href="javascript:ReverseDisplay('dg8')">&nbsp;</a>
				<div class="dashboard-content-title">Sales By Category(Daily)</div>
			</div>
			<div style="display: none;">
				<label class="dashboard-content-filter-label">&nbsp;&nbsp;&nbsp;Start :
				</label>
				<input type="text" id="startDate" name="startDate" class="chart-date" value="${startDate}">
				<label class="dashboard-content-filter-label">&nbsp;&nbsp;End :
				</label>
				<input type="text" id="endDate" name="endDate" class="chart-date" value="${endDate}">
			</div>
			<div id="content8_chart">
				<img src="${pageContext.request.contextPath}/images/load.gif" style="display: none;"
					width="40" />
			</div>
		</div>
	</div> 
</div> 