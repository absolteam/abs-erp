<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">

var workflowDetailId=0;
var screenPath="";
var	recordId=0;
var processFlag=0;
var url="";
var recordNumber="";
var recordName=null;
var useCase="";
var messageId = '';
var contractPrintFlag = "";
var screenId;
var	isHidden;
var operationId;
var operationGroupId;
var processType=0;
$(document).ready(function(){
	
	
	$('#NotificationDialog').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:950,
		height:600,
		bgiframe: false,
		modal: true 
	});
	// ----------Workflow Notification------------------------
	//workflowcontentsidebarCall();
	/* window.setInterval(function() {	
		workflowcontentsidebarCall();
		workflowcontentCall();
	},20000); */
	$(".workflowNotificationProcess").live('dblclick',function(){
		
		
		
		//Find the Id 
		var tempvar=$(this).attr("id");
		var idarray = tempvar.split('_');
		var id=Number(idarray[1]); 
		var flag=Number(idarray[0]); 
		workflowDetailId=$("#"+flag+"_workflowDetailId_"+id).val();
		screenPath=$("#"+flag+"_screen_"+id).val();
		useCase=$("#"+flag+"_useCase_"+id).val();
		recordId=$("#"+flag+"_recordId_"+id).val();
		processFlag=Number($("#"+flag+"_processFlag_"+id).val());
		recordNumber=$("#"+flag+"_recordNumber_"+id).val();
		recordName=$("#"+flag+"_recordName_"+id).val();
		isHidden=$("#"+flag+"_isHidden_"+id).val();
		
		
		
		messageId=$("#"+flag+"_messageId_"+id).val(); 
		screenId=$("#"+flag+"_screenId_"+id).val();
		processType=$("#"+flag+"_processType_"+id).val();
		operationId=$("#"+flag+"_operationId_"+id).val();
	
		//var toProcess=$("#"+flag+"_toProcess_"+id).val();
		url='<%=request.getContextPath()%>/'+screenPath+'.action';
	
		
		//alert(toProcess+"---"+isHidden);
		var usecaseArray = useCase.split("."); 
		var lastIndex = usecaseArray[usecaseArray.length-1];
		if(lastIndex=="Contract"){
			contractPrintFlag = getContractCallPopup(recordId);  
		}   
	
		
		wfstandardCall();
		$(".ui-widget-overlay").css("opacity",0.6);
		return false; 
	});
	
	$('.workflowAlertProcess').live('dblclick',function(){
		$(".ui-widget-overlay").css("opacity",0.6);
		var alertRowId=$(this).attr('id');
		var idVal=alertRowId.split('_');
		var rowId=Number(idVal[1]); 
		var message=$('#message_'+rowId).html(); 
		var useCase=$('#alertUseCase_'+rowId).val(); 
		var recordId=$('#alertRecordId_'+rowId).val(); 
		var urlaction=$('#alertScreenPath_'+rowId).val();
		var alertId=Number($("#alertId_"+rowId).val()); 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/"+urlaction+".action",   
	     	async: false,
	     	data:{attribute1:message,recordId:recordId,alertId:alertId,useCase:useCase},
			dataType: "html",
			cache: false,
			error: function(result) {
			},
	     	success: function(result){ 
	     		$("#main-wrapper").html(result);
	     	}
		});
		
		return false;
	});

	var getContractCallPopup = function(recordId){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_printtaken_status.action",   
	     	async: false,
	     	data:{recordId:recordId},
			dataType: "html",
			cache: false, 
	     	success: function(result){ 
	     		$(".contract-popup-result").html(result); 
	     		contractPrintFlag = $('.contract_flag').html();
	     	}
		});
		return contractPrintFlag;
	};
	
	$('#management_cn').dialog({
 		autoOpen: false,
 		width: 800,
 		height: 500,
 		bgiframe: true,
 		modal: true
 	});
	
	 $('#left-arrow').toggleClass('hidden').siblings().toggleClass('hidden');
	 $('#dashboard_right_container').addClass("hidden");
	 $('#main-wrapper').removeClass('collapsed');
	 $('#main-wrapper').addClass("expanded");
	 
	 $('#show-hide').click(function(){
         var src = $('#show-hide-img').attr('src');
         var property = $('#dashboard_right_container').css('width');
         var display = $('#dashboard_right_container').css('display');
         
        
         if(property == '0' || display == 'none') {
        	 $('#right-arrow').toggleClass('hidden').siblings().toggleClass('hidden');
        	 $('#dashboard_right_container').removeClass("hidden");
        	 $('#main-wrapper').removeClass('expanded');
        	 $('#main-wrapper').addClass("collapsed");
           
         }else {
        	 $('#left-arrow').toggleClass('hidden').siblings().toggleClass('hidden');
        	 $('#dashboard_right_container').addClass("hidden");
        	 $('#main-wrapper').removeClass('collapsed');
        	 $('#main-wrapper').addClass("expanded");
        	 
             
         }
     });  
	 
	 $('#loadMoreRight').live('click', function () { 
		 rightsideAlertCall(false);
	 });
	
});

function wfstandardCall(){
	
	
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflow_set_session.action",
		data: {
     		recordId: recordId,
     		workflowDetailId:workflowDetailId,
     		useCase:useCase,
     		processFlag:processFlag,
     		messageId:messageId
     		},
	 	async: false,
	    dataType: "json",
	    cache: false,
	    success:function(response){
	    	var screenPath = response.screenPath;
	    	var detailVO=response.objSeletectedNotificationWorkflowDetailVO;
	    	url='<%=request.getContextPath()%>/'+screenPath+'.action';
	    	
	    		 //No need to set any where
		 		  /* var win1=window.open(url+'?format=HTML&recordId='
		 		 		+ recordId+'&approvalFlag=Y&messageId='+messageId,'WorkFlowApprove','width=800,height=800,scrollbars=yes,left=100px'); */ 
		 		 		
		 		 		  $.ajax({
				    		type:"POST",
				    		url:url,
				    		data: {format:"HTML",recordId: recordId,alertId:messageId,processType:processType
				         		},
				    	 	async: false,
				    	    dataType: "html",
				    	    cache: false,
				    	    success:function(result){
				    	    	if(processFlag==2 || processFlag==6){
				    	    		if(detailVO.operationGroupName=='Preview'){
				    	    			$("#notificationContent").html(result); 	
					    				$("#NotificationDialog").dialog("open");
				    	    		}else{
					    				$("#main-wrapper").html(result);
				    	    			$("#main-wrapper").find("form").append("<button id='publishButton' type='button' onclick='publish()' class='buttons'>Publish</button>");
				    	    		}
				    	    		onNotificationRowSelect(operationGroupId,detailVO);
				    	    	}else{
				    				$("#notificationContent").html(result); 	
				    				$("#NotificationDialog").dialog("open");
				    		 		onNotificationRowSelect(operationGroupId,detailVO);
				    	    	}
				    				
				    	    }
				    	}); 
	    		 
		 	 
		 		//$.session("PROCESSFLAG_"+$.session("jSessionId"),dataString);
	    }
	});
	
	
	
}
function wfpopupcall(){
	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workflow_set_session.action", 
     	data: {
     		recordId: recordId,
     		workflowDetailId:workflowDetailId,
     		useCase:useCase,
     		processFlag:3
     		}, 

		 	async: false,
		    dataType: "html",
		    cache: false,
		    success:function(result){
		 		 //No need to set any where
		 		var win1=window.open(url+'?format=HTML&recordId='
		 		 		+ recordId+'&approvalFlag=Y&messageId='+messageId,'WorkFlowApprove','width=800,height=800,scrollbars=yes,left=100px');
		 		//$.session("PROCESSFLAG_"+$.session("jSessionId"),dataString);
			}
		});
}
function workflowcontentsidebarCall(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflow_notification_rightside.action",
		data: {
 			processFlag:3
 		},
	 	async: false,
	    dataType: "html",
	    cache: false,
	    success:function(result){
	 		 $("#workflowcontent_SideBar_approval").html(result);
	 		 var count=Number($("#recordCount_3").val());
	 		 if(count>0){
	 		 }else{
	 			count=0; 
	 		 }
		 	 $("#r_approval_count").html("<p style='margin-top: -10px; font-size: small ! important;'>"+count+"</p>");

		}
	});
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflow_notification_rightside.action",
		data: {
 			processFlag:8
 		},
	 	async: false,
	    dataType: "html",
	    cache: false,
	    success:function(result){
	 		 $("#workflowcontent_SideBar_approved").html(result);
	 		 var count1=Number($("#recordCount_1").val());
	 		 if(count1>0){
	 		 }else{
	 			count1=0; 
	 		 }
		 	 $("#r_approved_count").html("<p style='margin-top: -10px; font-size: small ! important;'>"+count1+"</p>");

		}
	});
	// -------Alert----------------------------
		rightsideAlertCall(true);
	}
function rightsideAlertCall(applyLimit){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflow_alert_right_side.action",
					async : false,
					data:{applyLimit:applyLimit},
					dataType : "html",
					cache : false,
					success : function(result) {

						$("#workflowcontent_SideBar_alerts").html(result);
						var alertcount = Number($("#alerRrecordCount").val());
						if (alertcount > 0) {
						} else {
							alertcount = 0;
						}
						$("#r_alerts_count").html(
								"<p style='margin-top: -10px; font-size: small ! important;'>"
										+ alertcount + "</p>");
					}
				});
}
</script>
<style type="text/css">
.max-height {
	height: auto !important;
}

.count-right-side {
	display: block;
	height: 23px;
	width: 25px;
	background: url(images/notification-bg.png) no-repeat;
	background-size: 25px 23px;
	background-position: center;
	float: right;
	text-align: center;
	top: 4px;
	position: absolute;
	right: 10px;
	color: #FFFFFF;
}
</style>
 
 <div style="float:right; right:0; z-index:1; margin:0 7px 0 -33px;display: none;"><a href="javascript:void(0);" id="show-hide" class="hide">
 <img src="images/left-arrow.png" id="left-arrow" class="hidden" /><img src="images/right-arrow.png" id="right-arrow" />
 </a></div>
 
<div class="dashboard_right_container" id="dashboard_right_container" style="display: none;">

	<div class=dashboard_right id="accordion">
		<h2>Notifications</h2>
		<div class="dashboard_right_in">
			
					<h3>
						<a href="#" class="on"><fmt:message
								key="common.dashboard.waitingforapproval" /></a>
					</h3>
					<div style="height: 100px; overflow-y: auto;">

						<div class="texts" id="workflowcontent_SideBar_approval"></div>

					</div>


					<h3>
						<a href="#" class="on"><fmt:message
								key="common.dashboard.approved/rejected" /></a>
					</h3>
					<div style="height: 100px; overflow-y: auto;">

						<div class="texts" id="workflowcontent_SideBar_approved"></div>

					</div>




					<h3>
						<a href="#" class="on"><fmt:message
								key="common.dashboard.alerts" /></a>
					</h3>
					<div style="height: 100px; overflow-y: auto;">

						<div class="texts" id="workflowcontent_SideBar_alerts"></div>

					</div>
				<div class="dashicon">
					<img src="images/dash.png" alt="" />
				</div>
			</div>

			
			 <h2 style="background:url(images/ster_bg.png) repeat-x center; padding:6px 0px 6px 0px; margin:0 !important;">
			 	Favourites <div class="ster"><img src="images/star.png" alt="" /></div></h2>
			<div class="dashboard_right_in" style="margin:0 0 8px 0 !important;">
					<h3>
						<a href="#" class="on"><fmt:message
								key="common.dashboard.waitingforapproval" /></a>
					</h3>
					<div style="height: 100px; overflow-y: auto;">
						<div class="texts" id="workflowcontent_SideBar_approval"></div>
					</div>

					<h3>
						<a href="#" class="on"><fmt:message
								key="common.dashboard.approved/rejected" /></a>
					</h3>
					<div style="height: 100px; overflow-y: auto;">
						<div class="texts" id="workflowcontent_SideBar_approved"></div>
					</div>

					<h3>
						<a href="#" class="on"><fmt:message
								key="common.dashboard.alerts" /></a>
					</h3>
					<div style="height: 100px; overflow-y: auto;">
						<div class="texts" id="workflowcontent_SideBar_alerts"></div>
					</div>
					
			</div>

		</div>
	</div>




	<!-- Dialog Box to show Waiting for Approval records -->
	<div id="NotificationDialog"
		style="overflow-y: auto !important; display: none;">

		<div id="notificationContent"></div>


		<div style="margin-top: 10px;">

			<button id="approveButton" onclick="approveNotification()"
				class="buttons">Approve</button>

			<button id="rejectButton" onclick="openRejectCommentDialog()"
				class="buttons_c">Reject</button>

			<button id="publishButton" onclick="publish()" class="buttons">
				Publish</button>

			<button id="mArButton" onclick="markAsRead()" class="buttons"
				style="width: 115px;">Mark As Read</button>



		</div>
	</div>
	<!-- Dialog box to show comment for rejected record -->
	<div id="commentDialog" style="display: none;">
		<table style="width: 100%;">
			<tr>
				<td>Comment: <span class="required">*</span></td>
			</tr>
			<tr>
				<td><textarea
						style="width: 92%; color: #7F775B !important; border: 1px solid #d8d8d8 !important; padding: 3px 10px; height: 76px; border-radius: 5px; margin: 0px 0px; overflow-y: auto;"
						id="rejComment"></textarea></td>
			</tr>
			<tr>
				<td><button class="buttons" id="workflow-reject-button" onclick="rejectNotification()">
						Ok</button>
			</tr>
		</table>

	</div>

</div>
<script src="${pageContext.request.contextPath}/js/dashboard.js"
	type="text/javascript"></script>