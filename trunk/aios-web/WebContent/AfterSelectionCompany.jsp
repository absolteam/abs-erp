<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <%if (session.getAttribute("lang").equals("ar")) {%> dir="rtl"
	<%}%>>
<head>
<meta http-equiv="Content-Type" charset="utf-8" />
<title>${THIS.companyName} | ${USER.username}</title>

<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
<link href="${pageContext.request.contextPath}/css/reset.css"
	rel="stylesheet" type="text/css" media="all" /> 

<link
	href="${pageContext.request.contextPath}/css/styles/default/ui.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/forms.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/tables.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/messages.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/HRM.css" type="text/css"
	media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/validationEngine.jquery.css"
	type="text/css" media="screen" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery.datepick.css">
<link href="${pageContext.request.contextPath}/css/jquery.notice.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/prettify.css"
	rel="stylesheet" type="text/css" media="all" />
	
<c:choose>
	<c:when test="${sessionScope.lang eq 'ar' }">
		<link
			href="${pageContext.request.contextPath}/css/styles/erp_new/erp_new_ar.css"
			rel="stylesheet" type="text/css" media="all" />
	</c:when>
	<c:otherwise>
		<link
			href="${pageContext.request.contextPath}/css/styles/erp_new/erp_new.css"
			rel="stylesheet" type="text/css" media="all" />
	</c:otherwise>
</c:choose>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
var $ = $.noConflict(false);
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript">
var $jquery = $.noConflict(true);
</script>
<script src="${pageContext.request.contextPath}/js/jquery.number.js" type="text/javascript"></script> 
<%-- <script src="${pageContext.request.contextPath}/css/styles/erp_new/js/jquery.js" type="text/javascript"></script> --%>
<script
	src="${pageContext.request.contextPath}/css/styles/erp_new/js/cufon-yui.js"
	type="text/javascript"></script>
<%-- <script src="${pageContext.request.contextPath}/css/styles/erp_new/js/Magistral.js" type="text/javascript"></script> --%>
<script
	src="${pageContext.request.contextPath}/css/styles/erp_new/js/customfunctions.js"
	type="text/javascript"></script>

<script 
	src="${pageContext.request.contextPath}/js/fancy-tree/jquery-ui.custom.js" type="text/javascript"></script>
<script 
	src="${pageContext.request.contextPath}/js/fancy-tree/jquery.fancytree.js" type="text/javascript"></script>
<script 
	src="${pageContext.request.contextPath}/js/prettify.js" type="text/javascript"></script>
<script 
	src="${pageContext.request.contextPath}/js/fancy-tree/jquery.contextMenu-1.6.5.js" type="text/javascript"></script>
<script 
	src="${pageContext.request.contextPath}/js/fancy-tree/jquery.fancytree.contextMenu.js" type="text/javascript"></script>	
<script 
	src="${pageContext.request.contextPath}/js/fancy-tree/jquery.fancytree.filter.js" type="text/javascript"></script>	
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-ui/flick/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/superfish.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/tooltip.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/tablesorter.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/tablesorter-pager.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/cookie.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/DateTime.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/date.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/custom.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/scrollTo.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.notice.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.validationEngine-en.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.validationEngine.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.num2words.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.layout.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.jclock-1.2.0.js"
	type="text/javascript"></script>
<script 
	src="${pageContext.request.contextPath}/js/jquery.columnmanager.js"
	type="text/javascript"> </script>

<!-- New Menu StyleSheet -->
<!--<link type="text/css" href="${pageContext.request.contextPath}/js/MainMenu/menu.css" rel="stylesheet" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/MainMenu/menu.js"></script>-->

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/menu/pro_menu.css" />
<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/menu/stuHover.js"></script> 
 --%>
<!-- FOR AUTOCOMPLETE -->
<script
	src="${pageContext.request.contextPath}/js/jquery-autocomplete/jquery.ui.autocompletescreen.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery-autocomplete/jquery.ui.posautocomplete.js"></script>
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/demos.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/jquery.ui.autocomplete.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/jquery.ui.button.css"
	rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.datepick.js"></script>
<script type="text/javascript">var contextPath = "${pageContext.request.contextPath}";</script>

<!-- FOR TREE -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/treeview/jquery.treeview.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/treeview/jquery.treeview.js"></script>
	
	
	
<script
	src="${pageContext.request.contextPath}/js/menu/jquery.hoverIntent.minified.js"></script>
	
<script
	src="${pageContext.request.contextPath}/js/menu/jquery.dcmegamenu.1.3.3.js"></script>
	
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/js/menu/orange.css"
	type="text/css" />
	
	
	


<script type="text/javascript">
	
		var datatableSearch = ('${lang}' == 'ar') ? "تقص: " : "SEARCH: ";
		var datatableFirst = ('${lang}' == 'ar') ? "الأول" : "First";
		var datatableLast = ('${lang}' == 'ar') ? "آخر" : "Last";
		var datatableNext = ('${lang}' == 'ar') ? "التالي" : "Next";
		var datatablePrevious = ('${lang}' == 'ar') ? "سابق" : "Previous";
		var datatableEmpty = ('${lang}' == 'ar') ? "لا توجد بيانات متاحة في الجدول" : "No data available in table";
		var datatableShowing = ('${lang}' == 'ar') ? "أداء" : "Showing";
		var datatableTo = ('${lang}' == 'ar') ? "إلى" : "to";
		var datatableOf = ('${lang}' == 'ar') ? "من" : "of";
		var datatableEntries = ('${lang}' == 'ar') ? "مقالات" : "Entries";
		var datatableInfoEmpty = ('${lang}' == 'ar') ? "إظهار 0 إلى 0 من 0 مقالات" : "Showing 0 to 0 of 0 entries";
		var datatableShow = ('${lang}' == 'ar') ? "عرض" : "Show";
		var datatableLoading = ('${lang}' == 'ar') ? "تحميل ..." : "Loading...";
		var datatableProcessing = ('${lang}' == 'ar') ? "تجهيز ..." : "Processing...";
		var datatableNoMatch = ('${lang}' == 'ar') ? "لا توجد سجلات مطابقة" : "No matching records found";
		
	</script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/data-table_css/demo_table.css"
	type="text/css" />
<script type="text/javascript" language="javascript"
	src="${pageContext.request.contextPath}/js/data-table_js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript"
src="${pageContext.request.contextPath}/js/data-table_js/jquery.dataTables.columnFilter.js"></script>
	
<link href="${pageContext.request.contextPath}/css/skin-win7/ui.fancytree.css" rel="stylesheet" type="text/css" media="all">
<link href="${pageContext.request.contextPath}/css/jquery.contextMenu.css" rel="stylesheet" type="text/css" media="all">
<link href="${pageContext.request.contextPath}/css/general.css"
	rel="stylesheet" type="text/css" media="all" />
	
<script type="text/javascript" language="javascript"
	src="${pageContext.request.contextPath}/js/mousetrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>

<link rel="stylesheet" type="text/css" media="screen and (min-device-width: 800px)" href="${pageContext.request.contextPath}/css/W800.css"/>
<link rel="stylesheet" type="text/css" media="screen and (min-device-width: 1024px)" href="${pageContext.request.contextPath}/css/W1024.css"/>
<link rel="stylesheet" type="text/css" media="screen and (min-device-width: 1280px)" href="${pageContext.request.contextPath}/css/W1280.css"/>
<link rel="stylesheet" type="text/css" media="screen and (min-device-width: 1360px)" href="${pageContext.request.contextPath}/css/W1360.css"/>




<script type="text/javascript">
	$(function(){

		/* $(document).bind("contextmenu",function(e){
		        e.preventDefault();
		    });*/
		    
		$('#password-update-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: true,
			buttons: {
				"Save": function() { 
					$("#updatePassword").trigger('click');				 
				} 
			}
		});

		var passwordFlag;
		$(".changePassword").click(function(){
				$('#password-update-popup').dialog('open');
				$('#ui-dialog-title-password-update-popup').text("Change Password");
				passwordFlag = "C";
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/updatePasswordRedirect.action",
				 	async: false, 
				 	data:{passwordFlag: passwordFlag},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
				 		$('.password-popup-result').html(result); 
					},
					error:function(result){ 
						$('.password-popup-result').html(result); 
					}
				}); 
		});

		$(".resetPassword").click(function(){
				passwordFlag = "R";
				$('#password-update-popup').dialog('open');
				$('#ui-dialog-title-password-update-popup').text("Reset Password");
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/updatePasswordRedirect.action",
										async : false,
										data : {
											passwordFlag : passwordFlag
										},
										dataType : "html",
										cache : false,
										success : function(result) {
											$('.password-popup-result').html(
													result);
										},
										error : function(result) {
											$('.password-popup-result').html(
													result);
										}
									});
						});

		var loggedUser = "${USER.username}";
		if (loggedUser.indexOf("admin_") == -1) {
			$("#resetPassword").remove();
		}

		setInterval(function(){ $('.success').fadeOut(); $('.error').fadeOut(); }, 3000);
		//setTime setTimeout("$('.success').fadeOut(); $('.error').fadeOut();", 3000);

		Mousetrap.bind(['command+shift+d', 'ctrl+shift+d'], function(e) {
			location.reload();
		    return false;
		});
		Mousetrap.bind(['command+shift+l', 'ctrl+shift+l'], function(e) {
			confirmLogout();
		    return false;
		});
		Mousetrap.bind(['command+shift+e', 'ctrl+shift+e'], function(e) {
			$(".custom_report").trigger('click');
		    return false;
		});
		$("#help_btn").hover(function() {
			$("#helpDiv").show();
		});
		$("#help_btn").mouseout(function() {
			$("#helpDiv").hide();
		});
	});
</script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery.notice.css"
	type="text/css" media="all" />
	
	
	<style>

.main_oe_overlay{
	background:#000;
	opacity:0;
	position:fixed;
	top:0px;
	left:0px;
	width:100%;
}
</style>
</head>

<body onload="startTime();">
	<div class="wrapper">

		<%@ include file="inc/LeftSideNavigation.jsp"%>
		<div>
			<span style="display: none;"
				class="ui-icon ui-icon-circle-arrow-w float-right show-side-bar"></span>
			<!-- <div id="page-wrapper"> -->

				<div id="right_side_container">
					<%@ include file="inc/AfterCompanySelectionheader.jsp"%>
					<%@ include file="inc/AfterLoginmainPage.jsp" %>
					<%@ include file="inc/RightSideNavigation.jsp"%>
				</div>
			<!-- </div> -->
			<div id="main_oe_overlay" class="main_oe_overlay"></div>
			<div id="ajaxLoader"
				style="width: 100px; left: 600px; top: 300px; position: absolute; z-index: 9999; display: none;">
				<div style="height: 50px; width: 220px; border: solid thin #ddd; background-color: white; box-shadow: 0px 0px 2px 0px #ccc;" title="Loading...">			
					<span style="text-align: center; color: grey; width: 220px; margin: 5px; display: block;">Loading</span>
					<img
						src="${pageContext.request.contextPath}/images/ajaxloadingbar.gif"
						title="Loading...">
				</div>
			</div>
		</div>
	</div>

	<%-- <div class="clearfix"></div>
	<%@ include file="inc/Footer.jsp"%> --%>

	<div id="message" style="display: none;">
		<!-- <a id="top-link" href="#top"><img height="53" width="53" alt="top" src="/images/top.png"></a> -->
	</div>

	<div
		style="display: none; position: absolute; overflow: auto; z-index: 10000; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">

		<div id="password-update-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="password-popup-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>

</body>
</html>