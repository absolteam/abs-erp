<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">

	var oTable; var selectRow=""; var aData =""; var screenStatus = "A";
	var alertRoleId = 0; var aSelected = [];
	var alertObject=null;
	$(function(){
		
		dataList();
		
		
		$("#addUser").click(function(){
			var alertRoleId = Number($("#alertRoleId").val());
			var moduleProcessId = Number($("#moduleProcessId").val());
			var personId = Number($("#personId").val());
			var roleId = Number($("#roleId").val());
			if(moduleProcessId==0){
				alert("Please select the access area");
				return false;
			}
			
			if(personId==0 && roleId==0){
				alert("Please select any one role or employee to save");
				return false;
			}
			
			if(personId>0 && roleId>0){
				alert("Please select any one role or employee to save");
				return false;
			}
				
			
			$("#notify").hide();
			$("#success").hide();
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_alert_role.action",
			 	async: false, 
			 	data:{ moduleProcessId: moduleProcessId, roleId: roleId, personId:personId,alertRoleId:alertRoleId},
			    dataType: "json",
			    cache: false,
				success:function(result){ 
					$("#notify").hide();
					if(alertRoleId ==null || alertRoleId==0 ) {
			 			$("#success").text("Access Rights added successfully.");
			 		} else {
			 			$("#success").text("Access Rights updated successfully.");
					}
					dataList();
			 		$("#success").fadeIn();
			 		setTimeout("$('#success').fadeOut();", 3000);
				},
				error:function(result){ 
					$("#success").hide();
			 		$("#notify").text("Access Rights already exists.");
			 		$("#notify").fadeIn();
				}
			}); 
		});	

		$("#clear").click(function(){ 
			screenStatus = "A";
			$("#alertRoleId").val("");
			$("#moduleProcessId").val("");
			$("#personId").val("");
			$("#roleId").val("");
		});
		
		$("#deleteUser").click(function(){
			deleteUser();
		});
		$('.formError').remove();
		
		 
		 
		 $('#AlertRole tbody tr').live('click', function() {
				if ($(this).hasClass('row_selected')) {
					$(this).addClass('row_selected');
					aData = oTable.fnGetData(this);
					alertRoleId = aData.alertRoleId;
				} else {
					oTable.$('tr.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					aData = oTable.fnGetData(this);
					alertRoleId = aData.alertRoleId;
				}
			});
		 
		 $('#AlertRole tbody tr').live('dblclick', function() {
				if ($(this).hasClass('row_selected')) {
					$(this).addClass('row_selected');
					aData = oTable.fnGetData(this);
					alertRoleId = aData.alertRoleId;
				} else {
					oTable.$('tr.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					aData = oTable.fnGetData(this);
					alertRoleId = aData.alertRoleId;
				}
				
				
			});
	});
	
	function editData(aData){
		screenStatus = "E";
		$("#alertRoleId").val(aData.alertRoleId);
		$("#moduleProcessId").val(aData.moduleProcessId);
		$("#personId").val(aData.personId);
		$("#roleId").val(aData.roleId);
	}

	function deleteUser() {

		
		if(alertRoleId == '') {
			$("#notify").text("Please select a user to delete.");
	 		$("#notify").fadeIn();
			return false;
		}
		
		var confirmDeletion = confirm("Data will be permanently deleted.");
		if (!confirmDeletion) {
			return false;
		}

		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/delete_alert_role.action",
		 	async: false, 
		 	data:{alertRoleId: alertRoleId},
		    dataType: "json",
		    cache: false,
			success:function(result){ 
				dataList();
				$("#notify").hide();
		 		$("#success").text("Access rights deleted successfully.");		 		
		 		$("#clear").trigger('click');
		 		$("#success").fadeIn();
		 		setTimeout("$('#success').fadeOut();", 3000);
			},
			error:function(result){ 
				$("#success").hide();
		 		$("#notify").text("Access rights  deletion failed.");
		 		$("#notify").fadeIn();
			}
		}); 
	};

	function dataList(){
		oTable = $('#AlertRole').dataTable({ 
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "get_all_alert_roles.action",
			"sScrollY": $("#main-content").height() - 390,
			"aoColumns" : [ { 
				"mDataProp" : "screenName"
			 }, {
				"mDataProp" : "roleName"
			 }, {
				"mDataProp" : "personName"
			 },{
				"mDataProp" : "createdDateView"
			 }]
		});	 
	}
</script>

<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Alert Access Rights</div>	 
			<div class="portlet-content" style="padding-left: 1%;float: left;width:60%; padding-bottom: 3%;">
					<input type="hidden" id="alertRoleId" value=""/>
					<div>
						<label style="padding-right: 50px;">Access Area: </label>
						<select id="moduleProcessId" style="width: 254px;" class="validate[required]">
							<option value="">--Select--</option>
							<c:forEach items="${moduleProcessList}" var="process">
								<option value="${process.processId}">${process.processDisplayName}</option>
							</c:forEach>
						</select>
					</div>				
					<div>
						<label style="padding-right: 92px;">Role: </label>
						<select id="roleId" style="width: 254px;" class="validate[required]">
							<option value="">--Select--</option>
							<c:forEach items="${rolesList}" var="role">
								<option value="${role.roleId}">${role.roleName}</option>
							</c:forEach>
						</select>
					</div>		
					<div>
						<label style="padding-right: 64px;">Employee: </label>
						<select id="personId" style="width: 254px;" class="validate[required]">
							<option value="">--Select--</option>
							<c:forEach items="${personsList}" var="person">
								<option value="${person.personId}">${person.firstName} ${person.lastName}</option>
							</c:forEach>
						</select>
					</div>		
					<div style="padding-top: 5px;">
						<label>Choose either employee or role.</label>
					</div>
				<div style="float: none;">
				<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="deleteUser">
						Delete
					</div>
				</div>	
				<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="clear">
						Clear
					</div>
				</div>
				<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="addUser">
						Save
					</div>
				</div>	
							
			</div>
							
			</div>
			
			
			<div id="ralertole_list">
				<table id="AlertRole" class="display">
					<thead>
						<tr>
							<th>Access Area</th>
							<th>Role</th>
							<th>Employee</th>
							<th>Created Date</th>
						</tr>
					</thead>

				</table>
			</div>
		</div>
	</div>
</body>