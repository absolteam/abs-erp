
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">


var wfId =0;
var oTable;

var fromDate = null;
var toDate = null;




function getData() { 

	
	var processId=Number($('#process').val());
	
	
	$.ajax({
		type : "POST",
		dataType : "json",
		url : '<%=request.getContextPath()%>/get_workflows_of_process.action',
		traditional : true,
		data : {
			selectedModuleProcessId : processId,
		},
		success : function(response) {
		
			populateReport(response);
		},
		error : function(e) {
			console.log(e.message);
		}
	});
	
	
	
	
	
 }
 
 function populateReport(data) {
	 
	
	
	var html ="";

	
		for (var i=1; i<= data.length; i++) {
		
			html += "<br/><br/><br/>"+"<div style='text-align:left'>&nbsp;</div>Workflow "+i+"<br/>";
			html +=  "<table id='wf"+i+"' cellpadding='0' cellspacing='0' border='0' class='display'></table>";
			 
		}
		
		
		
		$("#documents").html(html);
		
	 	for (var i=1; i<= data.length; i++) {

		 	
			 $('#wf'+i).dataTable( {
			        "aaData":data[i-1].aaData,
			        "sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					
			        "aoColumns": [
			            { "sTitle": "Role" },
			            { "sTitle": "Username" },
			            { "sTitle": "Person Name" },
			            { "sTitle": "Designation" },
			            { "sTitle": "Operation"}
			        ]
			    } );  
		 
		} 
	  
	
	 
	
 }








					
$(".xls-download-call").click(function(){
	

	window.open('<%=request.getContextPath()%>/workflow_users_report_xls.action?selectedWorkflowId='+wfId,
			'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	return false;		

});

$(".print-call").click(function(){ 
	 
	var processId=Number($('#process').val());
	
			window.open('<%=request.getContextPath()%>/workflow_users_report_printout.action?selectedModuleProcessId='+processId,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;			
	 
});
	
$(".pdf-download-call").click(function(){ 
			
	var processId=Number($('#process').val());
	
	window.open('<%=request.getContextPath()%>/workflow_users_report_pdf.action?selectedModuleProcessId='
												+ processId, '_blank',
										'width=800,height=700,scrollbars=yes,left=100px,top=2px');
						return false;

					});

	$('#example tbody tr').live('click', function() {

		if ($(this).hasClass('row_selected')) {
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);

			wfId = aData[0];
		} else {
			oTable.$('tr.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);
			wfId = aData[0];
		}

	});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			workflow users report

		</div>
		<div class="portlet-content">





			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30">Module Process</label> <select id="process"
						onchange="getData()" style="width: 200px;">
						<option value="">Select</option>
						<c:forEach var="process" items="${PROCESSES}">
							<option value="${process.processId}">${process.processTitle}</option>
						</c:forEach>

					</select>
				</div>

			</div>




			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents"></div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
