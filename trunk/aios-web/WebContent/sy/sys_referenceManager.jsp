<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>References Management</title>
</head>

<style>
	 .label_ref {
		width: 40%;
		display: inline-block;
	}
	.input_ref {
		width: 45%;
	}
	.hover_window {
		padding: 10px;
		background-color: white;
		border-style: solid;
		border-width: thin;
		border-color: gray;
		position: absolute;
		display: none;
		width: 150px;
		height: 70px;
		z-index: 999;
	}
	.hover_window_date {
		padding: 10px;
		background-color: white;
		border-style: solid;
		border-width: thin;
		border-color: gray;
		position: absolute;
		display: none;
		width: 210px;
		height: 140px;
		z-index: 999;
	}
</style>

<script type="text/javascript">
	
	var sample_ref_no = new Array(); var tempSelectedEntity = "";
	var oTable; var selectRow = ""; var aData = ""; var aSelected = [];
	var refId = 0; var currentRefNo = 0; selectedDateFormat = "MMDDYYYY";
	
	$(function() {
		/* Modification Warning: In this jsp page user can't modify the label of these 
			Prefix Value	
			Digits
			Date	
			Special	
			Postfix Value
			
			***If modification is necessary, then we have to modify the name into Jquery script also.
		*/		
		
		$('#example').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/getReferenceList.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "refId", "bVisible": false},	
				{ "sTitle": "Entity", "sWidth": "100px"},
				{ "sTitle": "Entity Full", "bVisible": false},
				{ "sTitle": "Prefix", "sWidth": "100px"},			
				{ "sTitle": "Ref.No", "sWidth": "100px"},
				{ "sTitle": "Ref. Digits", "bVisible": false},
				{ "sTitle": "Date Tag", "sWidth": "100px"},	
				{ "sTitle": "Symbol Tag", "sWidth": "100px"},
				{ "sTitle": "Postfix", "sWidth": "100px"},
				{ "sTitle": "Description", "sWidth": "100px"},
				{ "sTitle": "Ref.No Position", "bVisible": false},
				{ "sTitle": "Date Position", "bVisible": false},
				{ "sTitle": "Special Position", "bVisible": false},				
				{ "sTitle": "Sample", "sWidth": "100px"},
			],
			"sScrollY": $("#main-content").height() - 360,
			"aaSorting": [[1, 'asc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	

		oTable = $('#example').dataTable();		 
		$('#example tbody tr').live('click', function () {  
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
				  aData = oTable.fnGetData( this );
		          refId = aData[0];
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          refId = aData[0];
		          
		      }
		});
		
		$('#example tbody tr').live('dblclick', function () {  
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          refId = aData[0]; 
		          editRef(aData);
		          hideAllHovers();
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          refId = aData[0];
		          editRef(aData);
		          hideAllHovers();
		      }
		});
		
		var editRef = function(refRecord) {
			
			$("#entity").attr('disabled', true);
			$('#entity').append('<option value="' + refRecord[2] + '">' + refRecord[1] + '</option>');
			$("#entity").val(refRecord[2]);
			tempSelectedEntity = refRecord[2];
			
			$("#is_prefix").attr('checked', (refRecord[3] == "") ? false : true);
			$("#prefix_value").text(refRecord[3]);
			$("#prefix_value_temp").val(refRecord[3]);
			$("#is_prefix").trigger('change');
			
			currentRefNo = Number(refRecord[4]);
			
			$("#is_ref_no").attr('disabled', true);
			$("#is_ref_no").attr('checked', true);
			$("#ref_no_value_temp").attr('disabled', true);
			$("#ref_no_value_temp").val(convertToZeros(refRecord[5]));
			$("#ref_no_value").text(convertToZeros(refRecord[5]));
			$("#is_ref_no").trigger('change');
			
			$("#is_date").attr('checked', (refRecord[6] == "") ? false : true);
			$("#date_value").text(refRecord[6]);
			$("#date_value_temp").val(refRecord[6]);
			$("#is_date").trigger('change');
			
			$("#is_special").attr('checked', (refRecord[7] == "") ? false : true);
			$("#special_value").text(refRecord[7]);
			$("#special_value_temp").val(refRecord[7]);
			$("#is_special").trigger('change');
			
			$("#is_postfix").attr('checked', (refRecord[8] == "") ? false : true);
			$("#postfix_value").text(refRecord[8]);
			$("#postfix_value_temp").val(refRecord[8]);
			$("#is_postfix").trigger('change');
			
				
			$("#description").val(refRecord[9]);
			$("#ref_no_position").val(refRecord[10]);
			$("#date_position").val(refRecord[11]);
			$("#special_position").val(refRecord[12]);
			$("#sample").text(refRecord[13]);
			
			hideAllHovers();//Added by rafiq  
		};
		
		var convertToZeros = function(target) {
			
			var zeroString = "";
			while(zeroString.length < target)
				zeroString = zeroString + "0";
			return zeroString;
		};
		
		var convertToCount = function(target) {
						
			return target.length;
		};
		
		// Save
		$("#save").click(function(){
			
			var isStarted = false;
			var usecase = $("#entity").val();
			//Entity Validation Part
			if(usecase==0){
				alert("Select Entity.");
				return false;
			}
			
			if($("#ref_no_value").text()==null || $("#ref_no_value").text().trim()=="Digits" || $("#ref_no_value").text().trim()==""){
				alert("Enter Refrence Number.");
				return false;
			}
						
			var refDigits="";
			if(($("#ref_no_value").text()!="Digits"))
			 refDigits = convertToCount($("#ref_no_value").text());
			
			if($("#is_prefix").attr('checked') == false)
				var prefix = "XX$";
			else
				var prefix = $("#prefix_value").text();
						
			if($("#is_date").attr('checked') == false)
				var dateTag = "XX$";
			else
				var dateTag = $("#date_value").text();
			
			if($("#is_special").attr('checked') == false)
				var symbolTag = "XX$";
			else
				var symbolTag = $("#special_value").text();
			
			if($("#is_postfix").attr('checked') == false)
				var postfix = "XX$";
			else
				var postfix = $("#postfix_value").text();
			
			var description = $("#description").val();
			var refNumberPosition = $("#ref_no_position").val();
			var dateTagPosition = $("#date_position").val();
			var symbolTagPosition = $("#special_position").val();
			if(refId > 0 && currentRefNo >= 1) {
				isStarted = true;
			}
			
			$('#loading').fadeIn();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/addEditReference.action", 
		     	async: false,
		     	data: {
		     		refId: Number(refId),		     		
		     		usecase: usecase, 
		     		prefix:	prefix,	
		     		refNumber: Number(currentRefNo),
		     		refDigits: refDigits,
		     		dateTag: dateTag,
		     		symbolTag: symbolTag,
		     		postfix: postfix,
		     		description: description,
		     		refNumberPosition: refNumberPosition,
		     		dateTagPosition: dateTagPosition,
		     		symbolTagPosition: symbolTagPosition,
		     		isStarted: isStarted
		     	},
				dataType: "html",
				cache: false,
				success: function(result) { 
					 $(".manage_reference_nos").trigger('click');
				},
				error: function(result) { 
					alert("error");
				}
			}); 
			$('#loading').fadeOut();
		});
		
		// CLEAR
		$("#clear").click(function() {
			
			refId = 0;
			$('#entity option[value="' + tempSelectedEntity + '"]').remove();
			$("#entity").val(0);
			$("#entity").attr('disabled', false);
			
			$("#is_prefix").attr('checked', false);
			//$("#prefix_value").text("");
			$("#prefix_value_temp").val("");
			
			currentRefNo = 0;
			
			$("#is_ref_no").attr('checked', true);
			$("#is_ref_no").attr('disabled', false);
			$("#ref_no_value_temp").attr('disabled', false);
			$("#ref_no_value_temp").val("");
			//$("#ref_no_value").text("");
			
			$("#is_date").attr('checked', false);
			//$("#date_value").text("");
			$("#date_value_temp").val("");
			
			$("#is_special").attr('checked', false);
			//$("#special_value").text("");
			$("#special_value_temp").val("");
			
			$("#is_postfix").attr('checked', false);
			//$("#postfix_value").text("");
			$("#postfix_value_temp").val("");
			
			$("#description").val("");
			$("#ref_no_position").val("1");
			$("#date_position").val("2");
			$("#special_position").val("3");
			$("#sample").text("XX-XXXX-XXXX-XX-XX");
			sample_ref_no = new Array();
			hideAllHovers();
		});
		
		var triggerChangeMethods = function() {
			/* $("#is_prefix").trigger('change');
			$("#is_postfix").trigger('change');
			$("#is_ref_no").trigger('change');
			$("#is_date").trigger('change');
			$("#is_special").trigger('change'); */
		};
		triggerChangeMethods();
		
		//ENTITY
		$("#entity").change(function() {
			triggerChangeMethods();
		});
		
		$("#ref_no_value_temp").keyup(function(){ 
			var str=new Array();
			str=$(this).val().split();
			for(var j=0;j<str.length;j++){ 
				if(str[j]!=0){ 
					$("#ref_no_value_temp").val("");
					alert("Enter only Zero '0'");
					$("#hover_ref_no").show();
				} 
			} 
			
		});
		
		var hideAllHovers = function() {
			$("#hover_prefix").hide();
			$("#hover_postfix").hide();
			$("#hover_ref_no").hide();
			$("#hover_date").hide();
			$("#hover_special").hide();
		};
		
		var re_render_sample = function() {
			$("#sample").text(sample_ref_no.join("-"));
		};
		
		// PREFIX
		$("#is_prefix").change(function() {
			if($("#is_prefix").attr('checked') == false) {
				$("#hover_prefix").fadeOut();
				if($("#prefix_value_temp").val()!=null && $("#prefix_value_temp").val()!='')
					$("#prefix_value").text($("#prefix_value_temp").val());
				
				sample_ref_no[0] = "XX";
			} else {
				$("#is_prefix").trigger('mouseenter');
				if($("#prefix_value").text()!=null && $("#prefix_value").text().trim()!="Prefix Value")
					sample_ref_no[0] = $("#prefix_value").text();
				else
					sample_ref_no[0] = "XX";
			}
			re_render_sample();

			return false;
		});
		$("#is_prefix").mouseenter(function(){ 
			hideAllHovers();
			$("#hover_prefix").fadeIn();
			$("#hover_prefix").attr('style', 'display: inline !important; margin-left: 5px;');
		});		
		$("#hover_prefix").mouseleave(function(){ 
			$("#hover_prefix").fadeOut();
			if($("#prefix_value_temp").val()!=null && $("#prefix_value_temp").val()!='')
			$("#prefix_value").text($("#prefix_value_temp").val());
			if($("#is_prefix").attr('checked') == true)
				sample_ref_no[0] = $("#prefix_value").text();
			else
				sample_ref_no[0] = "XX";
			re_render_sample();
		});
		
		// POSTFIX
		$("#is_postfix").change(function() {
			if($("#is_postfix").attr('checked') == false) {
				$("#hover_postfix").fadeOut();
				if($("#postfix_value_temp").val()!=null && $("#postfix_value_temp").val()!='')
				$("#postfix_value").text($("#postfix_value_temp").val());
				sample_ref_no[4] = "XX";
			} else {
				$("#is_postfix").trigger('mouseenter');
				if($("#postfix_value").text()!=null && $("#postfix_value").text().trim()!="Postfix Value")
					sample_ref_no[4] = $("#postfix_value").text();
				else
					sample_ref_no[4] = "XX";
			}
			re_render_sample();
			return false;
		});
		$("#is_postfix").mouseenter(function(){ 
			hideAllHovers();
			$("#hover_postfix").fadeIn();
			$("#hover_postfix").attr('style', 'display: inline !important; margin-left: 5px;');
		});		
		$("#hover_postfix").mouseleave(function(){ 
			$("#hover_postfix").fadeOut();
			if($("#postfix_value_temp").val()!=null && $("#postfix_value_temp").val()!='')
			$("#postfix_value").text($("#postfix_value_temp").val());
			if($("#is_postfix").attr('checked') == true)
				sample_ref_no[4] = $("#postfix_value").text();
			else
				sample_ref_no[4] = "XX";
			re_render_sample();
		});
		
		// NO. OF REF DIGITS
		$( "#slider_ref_no" ).slider({
			value: 1,
			min: 1,
			max: 3,
			step: 1,
			orientation: "horizontal",
			slide: function( event, ui ) {
				$("#ref_no_position").val(ui.value);
			}
		});
		$("#is_ref_no").change(function() {
			if($("#is_ref_no").attr('checked') == false) {
				$("#hover_ref_no").fadeOut();
				if($("#ref_no_value_temp").val()!=null && $("#ref_no_value_temp").val()!='')
				$("#ref_no_value").text($("#ref_no_value_temp").val());
				sample_ref_no[Number($("#ref_no_position").val())] = "XX";
			} else {
				$("#is_ref_no").trigger('mouseenter');
				if($("#ref_no_value").text()!=null && $("#ref_no_value").text().trim()!="Digits")
					sample_ref_no[Number($("#ref_no_position").val())] = $("#ref_no_value").text();
				else
					sample_ref_no[Number($("#ref_no_position").val())] = "XX";
			}
			re_render_sample();
			return false;
		});
		$("#is_ref_no").mouseenter(function(){ 
			hideAllHovers();
			$("#hover_ref_no").fadeIn();
			$("#hover_ref_no").attr('style', 'display: inline !important; margin-left: 5px;');
		});
		$("#hover_ref_no").mouseleave(function(){ 
			$("#hover_ref_no").fadeOut();
			if($("#ref_no_value_temp").val()!=null && $("#ref_no_value_temp").val()!='')
			$("#ref_no_value").text($("#ref_no_value_temp").val());
			if($("#is_ref_no").attr('checked') == true)
				sample_ref_no[Number($("#ref_no_position").val())] = $("#ref_no_value").text();
			else
				sample_ref_no[Number($("#ref_no_position").val())] = "XX";
			re_render_sample();
		});
		
		// DATE TAG
		$( "#slider_date" ).slider({
			value: 2,
			min: 1,
			max: 3,
			step: 1,
			orientation: "horizontal",
			slide: function( event, ui ) {
				$("#date_position").val(ui.value);
			}
		});
		$("#is_date").change(function() {
			if($("#is_date").attr('checked') == false) {
				$("#hover_date").fadeOut();
				if($("#date_value_temp").val()!=null && $("#date_value_temp").val()!='')
					$("#date_value").text($("#date_value_temp").val());
				sample_ref_no[Number($("#date_position").val())] = "XX";
			} else {
				$("#is_date").trigger('mouseenter');
				if($("#date_value").text()!=null && $("#date_value").text().trim()!="Date")
					sample_ref_no[Number($("#date_position").val())] = $("#date_value").text();
				else
					sample_ref_no[Number($("#date_position").val())] = "XX";
			}
			re_render_sample();
			return false;
		});
		$("#is_date").mouseenter(function(){ 
			hideAllHovers();
			$("#hover_date").fadeIn();
			$("#hover_date").attr('style', 'display: inline !important; margin-left: 5px;');
		});
		$("#hover_date").mouseleave(function(){ 
			$("#hover_date").fadeOut();
			if($("#date_value_temp").val()!=null && $("#date_value_temp").val()!='')
			$("#date_value").text($("#date_value_temp").val());
			if($("#is_date").attr('checked') == true)
				sample_ref_no[Number($("#date_position").val())] = $("#date_value").text();
			else
				sample_ref_no[Number($("#date_position").val())] = "XX";
			re_render_sample();
		});
		
		// SPECIAL TAG
		$( "#slider_special" ).slider({
			value: 3,
			min: 1,
			max: 3,
			step: 1,
			orientation: "horizontal",
			slide: function( event, ui ) {
				$("#special_position").val(ui.value);
			}
		});
		$("#is_special").change(function() {
			if($("#is_special").attr('checked') == false) {
				$("#hover_special").fadeOut();
				if($("#special_value_temp").val()!=null && $("#special_value_temp").val()!='')
					$("#special_value").text($("#special_value_temp").val());
				sample_ref_no[Number($("#special_position").val())] = "XX";
				re_render_sample();
			} else {
				$("#is_special").trigger('mouseenter');
				if($("#special_value").text()!=null && $("#special_value").text().trim()!="Special")
					sample_ref_no[Number($("#special_position").val())] = $("#special_value").text();
				else
					sample_ref_no[Number($("#special_position").val())] = "XX";
				re_render_sample();
			}
			return false;
		});
		$("#is_special").mouseenter(function(){ 
			hideAllHovers();
			$("#hover_special").fadeIn();
			$("#hover_special").attr('style', 'display: inline !important; margin-left: 5px;');
		});
		$("#hover_special").mouseleave(function(){ 
			$("#hover_special").fadeOut();
			$("#special_value").text($("#special_value_temp").val());
			if($("#is_special").attr('checked') == true)
				sample_ref_no[Number($("#special_position").val())] = $("#special_value").text();
			else
				sample_ref_no[Number($("#special_position").val())] = "XX";
			re_render_sample();
		});		
	});
	
</script>

<body>
	<div id="main-content" style="direction: ltr;">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 98%;">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Manage Reference Numbers</div>	 
			<div style="padding: 10px; width: 50%;" class="references_1">
				<div>
					<label class="label_ref">Entity:<span style="color:red">*</span> </label>
					<select id="entity" style="width: 30.5%;">
						<option value="0">Select</option>
						<c:choose>
							<c:when test="${VALID_NEW_REFERENCES ne null and VALID_NEW_REFERENCES ne ''}">
								<c:forEach items="${VALID_NEW_REFERENCES}" var="VALID_ENTITY">
									<option value="${VALID_ENTITY.className}">${VALID_ENTITY.usecase}</option>
								</c:forEach>
							</c:when>
						</c:choose>
					</select>
				</div>	
				<div>
					<label id="prefix_label" class="label_ref">Prefix </label>
					<span><input id="is_prefix" type="checkbox"/></span>
					<div id="hover_prefix" class="hover_window">						
						<div style="float: left;">
							<div style="display: block; margin-bottom: 10px;">
								<label style="width: 50px; display: inline-block;">Value:</label>
								<input type="text" id="prefix_value_temp" style="width: 80px; margin-left: 10px;"/>
							</div>
						</div>
					</div>
					
					<label id="prefix_value" style="font-weight: bold; margin-left: 10px;">Prefix Value</label>				
				</div>	
				<div>
					<label id="ref_no_label" class="label_ref">Reference (No. of digits) <span style="color:red">*</span></label>
					<span><input id="is_ref_no" type="checkbox" readonly="readonly" checked="checked"/></span>
					<div id="hover_ref_no" class="hover_window">						
						<div style="float: left;">
							<div style="display: block; margin-bottom: 10px;">
								<label style="width: 50px; display: inline-block;">Digits:</label>
								<input id="ref_no_value_temp" type="text" style="width: 80px; margin-left: 10px;"/>
								<label style="font-size: 7px !important; color: blue; margin-left: 55px;">example: 0000 for 4 digits</label>
							</div>
							
							<div style="float: left; margin-right: 10px;">
								<label>Position:</label>
							</div>							
							<div style="float: right;">
								<div id="slider_ref_no" style="margin-top: 3px; width: 80px;"></div>
								<ul style="display: -moz-inline-box;">
									<li>1</li>
									<li style="margin-left: 32px;">2</li>
									<li style="margin-left: 32px;">3</li>							
								</ul>
							</div>
							
						</div>
					</div>
					
					<label id="ref_no_value" style="font-weight: bold; margin-left: 10px;">Digits</label>
					<input id="ref_no_position" type="hidden" value="1">				
				</div>	
				<div>
					<label id="date_label" class="label_ref">Date Tag Format</label>
					<span><input id="is_date" type="checkbox"/></span>
					<div id="hover_date" class="hover_window_date">						
						<div style="float: left;">									
							<div style="display: table-cell;">		
								<div style="float: left; margin-right: 10px;">
									<label>Position:</label>
								</div>							
								<div style="float: right;">
									<div id="slider_date" style="margin-top: 3px; width: 140px;"></div>
									<ul style="display: -moz-inline-box;">
										<li>1</li>
										<li style="margin-left: 62px;">2</li>
										<li style="margin-left: 63px;">3</li>							
									</ul>
								</div>
							</div>
						
							<div style="display: block; margin-bottom: 10px; margin-top: 5px;">
								<label style="width: 52px; display: inline-block;">Format:</label>
								<input id="date_value_temp" type="text" disabled="disabled" style="width: 140px; margin-left: 10px;"/>
							</div>
							
							<div style="margin-bottom: 20px; display: block;">
								<div style="float: left; margin-right: 20px;" >
									<input name="mainDateFormat" id="fullDateSelect" type="radio" checked="checked"> Full Date <br/>
									<input name="mainDateFormat" id="dateMonthSelect" type="radio"> Day-Month <br/>
									<input name="mainDateFormat" id="yearSelect" type="radio"> Year <br/>
									<script type="text/javascript">
										$(function() {
											
											$("#fullDateSelect").click(function() {
												
												$("#dayMonth").hide();
												$("#yearOnly").hide();
												$("#completeDate").fadeIn();												
											});
											$("#dateMonthSelect").click(function() {
												
												$("#completeDate").hide();
												$("#yearOnly").hide();
												$("#dayMonth").fadeIn();
											});
											$("#yearSelect").click(function() {
												
												$("#completeDate").hide();
												$("#dayMonth").hide();
												$("#yearOnly").fadeIn();
											});
											$(".dateFormatSelection").click(function(){
												
												selectedDateFormat = $(this).val();
												$("#date_value_temp").val(selectedDateFormat);												
												//alert(selectedDateFormat);
											});
										});
									</script>
								</div>
								<div style="float: right; width: 50%;">
									<div id="completeDate" style="display: block;">
										<input class="dateFormatSelection" name="subDateFormat1" type="radio" checked="checked" value="MMddyyyy"> MMDDYYYY <br/>
										<input class="dateFormatSelection" name="subDateFormat1" type="radio" value="MMMddyyyy"> MMMDDYYYY <br/>
										<input class="dateFormatSelection" name="subDateFormat1" type="radio" value="MM-dd-yyyy"> MM-DD-YYYY <br/>
										<input class="dateFormatSelection" name="subDateFormat1" type="radio" value="MMM-dd-yyyy"> MMM-DD-YYYY <br/>
									</div>
									<div id="dayMonth" style="display: none;">
										<input class="dateFormatSelection" name="subDateFormat2" type="radio" checked="checked" value="MMdd"> MMDD <br/>
										<input class="dateFormatSelection" name="subDateFormat2" type="radio" checked="checked" value="MMMdd"> MMMDD <br/>
										<input class="dateFormatSelection" name="subDateFormat2" type="radio" value="MM-dd"> MM-DD <br/>
										<input class="dateFormatSelection" name="subDateFormat2" type="radio" value="MMM-dd"> MMM-DD <br/>
									</div>
									<div id="yearOnly" style="display: none;">
										<input class="dateFormatSelection" name="subDateFormat3" type="radio" checked="checked" value="yyyy"> YYYY <br/>
										<input class="dateFormatSelection" name="subDateFormat3" type="radio" value="yy"> YY <br/>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<label id="date_value" style="font-weight: bold; margin-left: 10px;">Date</label>		
					<input id="date_position" type="hidden" value="2">			
				</div>	
				<div>
					<label id="special_label" class="label_ref">Special Tag </label>
					<span><input id="is_special" type="checkbox"/></span>
					<div id="hover_special" class="hover_window">						
						<div style="float: left;">
							<div style="display: block; margin-bottom: 10px;">
								<label style="width: 50px; display: inline-block;">Tag:</label>
								<input id="special_value_temp" type="text" style="width: 80px; margin-left: 10px;"/>
							</div>
							
							<div style="float: left; margin-right: 10px;">
								<label>Position:</label>
							</div>							
							<div style="float: right;">
								<div id="slider_special" style="margin-top: 3px; width: 80px;"></div>
								<ul style="display: -moz-inline-box;">
									<li>1</li>
									<li style="margin-left: 32px;">2</li>
									<li style="margin-left: 32px;">3</li>							
								</ul>
							</div>
							
						</div>
					</div>
					
					<label id="special_value" style="font-weight: bold; margin-left: 10px;">Special</label>	
					<input id="special_position" type="hidden" value="3">				
				</div>		
				<div>
					<label id="postfix_label" class="label_ref">Postfix </label>
					<span><input id="is_postfix" type="checkbox"/></span>
					<div id="hover_postfix" class="hover_window">						
						<div style="float: left;">
							<div style="display: block; margin-bottom: 10px;">
								<label style="width: 50px; display: inline-block;">Value:</label>
								<input type="text" id="postfix_value_temp" style="width: 80px; margin-left: 10px;"/>
							</div>
						</div>
					</div>
					
					<label id="postfix_value" style="font-weight: bold; margin-left: 10px;">Postfix Value</label>				
				</div>		
				<div>
					<label class="label_ref">Description: </label>
					<input class="input_ref" id="description" type="text"/>
				</div>	
				<div style="float: none; margin-right: 11%;">
				<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="clear">
						Clear
					</div>
				</div>
				<div style="position: relative; margin-right: 0px;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="save">
						Save
					</div>
				</div>				
			</div>				
			</div>
			<div style="width: 45%;" class="references_2">
				<div style="width: 90%; height: 122px; margin-top: 11px;
					 border: solid thin #f3f3f3; background-color: white; padding-top: 10px;" align="center">
					<span style="font-weight: bold;">Sample</span>
					<span id="sample" style="color: graytext; margin-top: 11px; display: block;">XX-XXXX-XXXX-XX-XX</span>
				</div>
			</div>
			<div id="gridDiv" style="max-height: 50%;">
				<table id="example" class="display"></table>					
			</div>
		</div>
	</div>
</body>
</html>