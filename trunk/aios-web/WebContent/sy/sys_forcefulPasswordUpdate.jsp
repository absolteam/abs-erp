<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<meta http-equiv="Content-Type charset=utf-8" /> 
	<title>AIO | Update Expired Password</title>  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/resetlogin.css"  type="text/css" media="screen" />
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/adel1.jpg" type="image/x-icon" />
	<link href="${pageContext.request.contextPath}/css/reset.css" rel="stylesheet"   type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"  type="text/css" media="screen" />  
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/invalid.css"  type="text/css" media="screen" />	
	<link href="${pageContext.request.contextPath}/css/messages.css" rel="stylesheet"  type="text/css" media="all" /> 
<c:choose>
<c:when test="${IS_MOBILE eq true}">
	<link href="${pageContext.request.contextPath}/css/general-mobile.css" rel="stylesheet"  type="text/css" media="all" />
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style-mobile.css" type="text/css" media="screen"/>
</c:when>
<c:otherwise>
	<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet"  type="text/css" media="all" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css" type="text/css" media="screen"/>
</c:otherwise>
</c:choose>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script> 
<script>

 $(document).ready(function(){ 
	
	$("#oldPassword").focus();
	
	$('.keypress').bind('keypress', function(e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 if(keyCode == 13) { 
			 $("#update_password").trigger('click');
		 }
	});
	 
	$("#update_password").click(function() {
		
		$("#errorReport").text("");
		
		if($("#oldPassword").val().trim() == "" 
				|| $("#newPassword").val().trim() == "" 
					|| $("#repeatNewPassword").val().trim() == "") {
			
			$("#errorReport").text("All feilds are required, in order to update your password.");
			return false;
		} else if($("#newPassword").val() != $("#repeatNewPassword").val()) {
			
			$("#errorReport").text("New password and repeated password should be same.");
			return false;
		} else if ($("#newPassword").val().trim() == $("#oldPassword").val().trim()) {
			
			$("#errorReport").text("Old and new password cannot same.");
			return false;
		}
		
       	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/updatePasswordJson.action", 
		 	async: false,
		 	data: {
		 		oldPassword: $("#oldPassword").val(),
		 		newPassword: $("#newPassword").val(),
		 	},
		    dataType: "json",
		    cache: false,
			success: function(result) {
				if(result.jsonResponseString == "success") {					
					window.location.href = "${pageContext.request.contextPath}/user_selected_company.action";
				} else if(result.jsonResponseString == "incorrect_old") {
					$("#errorReport").show();
					$("#errorReport").text("Old password is incorrect, please try again.");
				} else {
					$("#errorReport").show();
					$("#errorReport").text("Something went wrong, please try again.");
				}
		 	},  
		 	error:function(result) {
		 		$("#errorReport").show();
		 		$("#errorReport").text("Sorry, system was unable to process this request.");
		 	} 
       	});
	});
 });
</script> 
</head>

<style>
	.notifyUser {
		font-size: 11px;
		color: gray;
		text-align: center;
		width: 250px;
	}
	
	#errorReport {
		font-size: 11px;
		color: #FB2F2F;
	}
	
	label {
		width: 60px;
		display: inline-block;
		text-align: left;
		color: gray;
		text-transform: uppercase;
		font-size: 10px;
	}
	
	#update_password {
		cursor: pointer; 
		background: rgb(240, 70, 70) none repeat scroll 0% 0%; 
		color: white; 
		border-radius: 2px;
	}
</style>

<body style="height: auto;">
	<div id="wrapper">		
		<div id="login-content" class="loginbox">
			<div class="loginbox_orignal" align="center">
				<div align="center" style="margin-bottom: 10px;">
		    		<img src="images/logo.png" alt="" />
		    	</div>
				<div id="emailmode">
					 						
					<div class="notification information_urn png_bg" align="left"
						 style="width: 41%; height: 20px; padding-left: 30px; padding-top: 8px; font-size: 10px;"> 
						PROVIDE NEW PASSWORD
				 	</div>	
				 	<span style="margin-bottom: 10px; margin-top: -5px; text-align: center; width: 100%; display: block; height: 10px;" id="errorReport"></span>					 	
					<div style="width: 100%;">
						<label>Old</label> <input class="validate[required,custom[max[30]]] keypress" 
						 		type="password" name="oldPassword" id="oldPassword" value=""
						 		MAXLENGTH="30" style="width: 229px !important; height: 27px; font-size: 11px;" />
						<br/>
						<label>New</label> <input class="validate[required,custom[max[30]]] keypress" 
						 		type="password" name="newPassword" id="newPassword" value=""
						 		MAXLENGTH="30" style="width: 229px !important; height: 27px; font-size: 11px;" />
				 		<br/>
				 		<label>Repeat</label> <input class="validate[required,custom[max[30]]] keypress" 
						 		type="password" name="repeatNewPassword" id="repeatNewPassword" value=""
						 		MAXLENGTH="30" style="width: 229px !important; height: 27px; font-size: 11px;" />	
					</div>
					<br/>
					<div style="margin-top: 0px; text-align: right; width: 80%;" align="center">
						<input type="button" name="update_password" id="update_password" style="cursor: pointer;" value="Continue"/>																
						</div>
					</div>	
					<br/>
					<span class="notifyUser">${notifyUser}</span>
					<span style="font-size: 10px; text-align: right; width: 80%; display: block;">It's been atleast ${currentPasswordAge} days since you updated your password.</span>
					<br/>					
			</div>
		</div>
	</div>
</body>
</html>