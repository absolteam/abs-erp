<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">

	var oTable; var selectRow=""; var aData =""; var screenStatus = "A";
	var userId = 0; var aSelected = [];
	var roles = new Array();
	var index, roleId;

	$(function(){
		
		$('.roleCheck').change(function(){
			
			roleId = $(this).attr('id');
			index = jQuery.inArray(roleId, roles);
			
			if($("#" + roleId).is(':checked')){
				
				if(index == -1) {				
					roles.push(roleId);
				}
			} else {
				
				if(index != -1) {				
					roles.splice(index, 1);
				}
			}
		});
		
		$("#addUser").click(function(){
			
			var username = $("#username").val();
			var password = $("#password").val();
			var cnfrmpassword = $("#cnfrmpassword").val();
			var status = $("#status").val();
			var person = $("#person").val();
			$("#notify").hide();
			$("#success").hide();
			
			if((username == "" || cnfrmpassword == "" || password == "") && screenStatus == "A") {
				
				$("#notify").text("All fields are required.");
				$("#success").hide();
		 		$("#notify").fadeIn();
		 		return;
			} else if((password != cnfrmpassword)  && screenStatus == "A") {
				$("#notify").text("New and confirmed password mismatch.");
				$("#success").hide();
		 		$("#notify").fadeIn();
		 		return;
			} else if(roles.length == 0) {
				$("#notify").text("Atleast one role is required.");
				$("#success").hide();
		 		$("#notify").fadeIn();
		 		return;
			}
			
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/addUser.action",
			 	async: false, 
			 	data:{username_: username, password_: password, role_: roles.toString(), status_: status, userFlag: screenStatus,person_:person},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#notify").hide();
					if(screenStatus == "A") {
			 			$("#success").text("User added successfully.");
			 		} else {
			 			$("#success").text("User updated successfully.");
					}
					oTable.fnDraw();
			 		$("#success").fadeIn();
			 		setTimeout("$('#success').fadeOut();", 3000);
				},
				error:function(result){ 
					$("#success").hide();
			 		$("#notify").text("Username already exists.");
			 		$("#notify").fadeIn();
				}
			}); 
		});	

		$("#clear").click(function(){ 

			screenStatus = "A";
			$("#username").val("");
			$("#password").val("");
			$("#cnfrmpassword").val("");
			$("#status").val("true");
			$("#notify").hide();
			$("#success").hide();
			$('#username').attr('disabled', false);
			$('#password').attr('disabled', false);
			$('#cnfrmpassword').attr('disabled', false);
			clearAllCheckboxes();
			roles = new Array();
		});
		
		var clearAllCheckboxes = function clear() { 

			$('input:checkbox').removeAttr('checked');
		};
		
		var editUser = function populateEditFields(record) {

			if(record.length == 0) {
				return false;
			}

			screenStatus = "E";
			$('#username').attr('disabled', true);
			$('#password').attr('disabled', true);
			$('#cnfrmpassword').attr('disabled', true);

			$("#notify").hide();
			$("#success").hide();
			
			$("#username").val(record[0]);
			$("#status").val((record[4] == "Enabled" )? "true" : "false");
			$("#person").val(record[2]);
			var roleIds = new Array();
			roleIds = record[6].toString().split(",");
			clearAllCheckboxes();
			
			for(i = 0; i < roleIds.length - 1; i++) {

				$('#' + roleIds[i]).attr("checked", true);
				$('#' + roleIds[i]).trigger('change');
			}
		};
		 
		$('.formError').remove();
		
		$('#example').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/getAllUsersRedirect.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "bFilter" : true,
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "Username"},
				{ "sTitle": "Person"},
				{ "sTitle": "Person Id", "bVisible": false},
				{ "sTitle": "Roles"},			
				{ "sTitle": "Status"},
				{ "sTitle": "Options"},	
				{ "sTitle": "RoleIds",  "bVisible": false},			
			],
			"sScrollY": $("#main-content").height() - 390,
			"aaSorting": [[1, 'asc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	

		oTable = $('#example').dataTable();		 
		$('#example tbody tr').live('click', function () {  
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          userId=aData[0]; 
		          editUser(aData);
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          userId=aData[0];
		          editUser(aData);
		      }
		});
	});

	function deleteUser(row) {

		var parentTR=$(row).parent().parent().get(0);
		var userId = getId(parentTR); 

		if(userId == '') {
			$("#notify").text("Please select a user to delete.");
	 		$("#notify").fadeIn();
			return false;
		}
		
		var confirmDeletion = confirm("'" + userId + "' will be permanently deleted.");
		if (!confirmDeletion) {
			return false;
		}

		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/deleteUser.action",
		 	async: false, 
		 	data:{username_: userId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#notify").hide();
		 		$("#success").text("User deleted successfully.");		 		
		 		$("#clear").trigger('click');
		 		$("#success").fadeIn();
		 		setTimeout("$('#success').fadeOut();", 3000);
		 		oTable.fnDeleteRow( aData );
			},
			error:function(result){ 
				$("#success").hide();
		 		$("#notify").text("User deletion failed.");
		 		$("#notify").fadeIn();
			}
		}); 
	};

	function getId(parentTR){ 
		if ($(parentTR).hasClass('row_selected') ) { 
			$(parentTR).addClass('row_selected');
	        aData = oTable.fnGetData( parentTR );
	        return aData[0]; 
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(parentTR).addClass('row_selected');
	        aData = oTable.fnGetData( parentTR );
	        return aData[0];
	    }
	}
</script>

<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Users</div>	 
			<div class="portlet-content" style="padding-left: 1%;float: left;width:50%; padding-bottom: 3%;">
					<div>
						<label style="padding-right: 9px;">Username: </label>
						<input id="username" type="text" style="width: 200px;"/>
					</div>		
					<div>
						<label style="padding-right: 12px;">Password: </label>
						<input id="password" type="password" style="width: 200px;"/>
					</div>	
					<div>
						<label style="padding-right: 24px;">Confirm: </label>
						<input id="cnfrmpassword" type="password" style="width: 200px;"/>
					</div>	
					<div>
						<label style="padding-right: 30px;">Person: </label>
						<select id="person" style="width: 204px;" class="validate[required]">
							<option value="">--Select--</option>
							<c:forEach items="${personsList}" var="person">
								<%-- <c:choose>
									<c:when test="${person.personId eq userrole.person.personId">
										<option value="${person.personId}" selected="selected">${person.firstName} ${person.lastName}</option>
									</c:when>
									<c:otherwise>
										<option value="${person.personId}">${person.firstName} ${person.lastName}</option>
									</c:otherwise>
								</c:choose> --%>
								<option value="${person.personId}">${person.firstName} ${person.lastName}</option>
							</c:forEach>
						</select>
					</div>		
					<div>
						<label style="padding-right: 34px;">Status: </label>
						<select id="status" style="width: 204px;">
							<option value="true" selected="selected">Enabled</option>
							<option value="false">Disabled</option>
						</select>
					</div>	
					
							
			</div>
			<div class="portlet-content" style="float: left;width:45%; padding-bottom: 3%;">
					<div>
						<label style="padding-right: 45px; float: left; padding-top: 5px;">Role: </label>
						<div style="float: left; padding-top: 5px;">
							<c:forEach items="${rolesList}" var="role">
								<div>
									<input id="${role.roleId}" type="checkbox" class="roleCheck" value="${role.roleId}"/> ${role.roleName}										
								</div>
							</c:forEach>	
							<div style="padding-top: 5px;">
								<label id="notify" style="color: red; display: none;"></label>
							</div>
							<div style="padding-top: 5px;">
								<label id="success" style="color: green; display: none;"></label>
							</div>						
						</div>
					</div>	
			</div>
			<div style="float: none;">
				<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="clear">
						Clear
					</div>
				</div>
				<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
					 ui-corner-all form-container float-right buttons">							
					<div class="portlet-header ui-widget-header float-right" id="addUser">
						Save
					</div>
				</div>				
			</div>
			<div id="gridDiv" style="max-height: 50%;">
				<table id="example" class="display"></table>					
			</div>
		</div>
	</div>
</body>