<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script
	src="${pageContext.request.contextPath}/js/workflow-management.js"
	type="text/javascript"></script>

<%-- Alertify plugin --%>
<script src="${pageContext.request.contextPath}/js/alertify.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/css/styles/alertify.core.css" />
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/css/styles/alertify.default.css" />

<style>
.ui-autocomplete-input {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
	border-radius: 5px 0 0 5px;
	box-shadow: 0 0 1px #CCCCCC;
	width: 151px;
	float: left;
}

.ui-button {
	border: 2px solid #CCCCCC;
	cursor: pointer;
	display: inline-block;
	margin: 1px 1px 0 -4px;
	overflow: hidden;
	padding: 0;
	position: relative;
	text-align: center;
	text-decoration: none !important;
	float: left;
}

.ui-button-text {
	padding: 0 0 2px !important;
	/* 	text-indent: -1e +7px; */
}

.bordered_panel {
	margin: 0 0 0 10px;
}
</style>

<div>
	<div class="content" style="height: 575px; overflow-x: hidden;">
		<div class="header_panel ui-widget-header">
			<p>Workflow Management</p>
		</div>

		<s:form action="save-workflow" id="workflowManagementForm"
			theme="simple" method="post">
			<div class="bordered_panel">
				<table class="countryDropDownTable" width="100%">
					<tr>
						<td class="countryDropDownTableLabel"
							style="vertical-align: middle;width:10%;">Process:</td>
						<td class="countryDropDownTableField" style="width:40%;"><s:select
								name="processId" cssClass="combobox" id="moduleProcessesList"
								list="moduleProcesses" listValue="processDisplayName"
								listKey="processId" headerKey="" headerValue="--Select--"
								onselect="onModuleProcessSelect();">

							</s:select>
						</td>
						<td>
							<div style="margin-right: 1%;width:40%;" >
								<input id="operation_add" type="button" value="Add Operation"
									class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"/>
							</div>
						</td>
						
						<td>
							<div style="margin-right: 1%;width:40%;" >
								<input id="alert_responsibility_add" type="button" value="Alert Access"
									class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"/>
							</div>
						</td>
					</tr>
				</table>
				
			</div>


			<div class="inner_panel">
				<div class="inner_panel_header ui-widget-header">
					<p>Details</p>
				</div>


				<%--For redefine workflow --%>
				<s:hidden id="selectedWorkflowId" name="selectedWorkflowId" />
				<s:hidden id="selectedWorkflowIndex" name="selectedWorkflowIndex" />
				<s:hidden id="isRedefine" name="isRedefine" value="false" />
				<div id="processFlows"
					style="background: none !important; border: none !important; margin: 0 0 0 10px;">

					<%--Content will be appended from javascript after process selection. --%>
				</div>

				<div id="addWorkflow" class="inner_panel"
					style="margin-top: 1px; margin-left: 10px; background: none; display: none;">
					<br />
					<div  id="workflowTiltle-div"></div>
					<br />
					<table id="recentWorkflowTable"
						style="border-spacing: 0; width: 96%; margin: auto; display: none;"
						class="ui-datatable">
						<thead>
							<tr>
								<th class="ui-state-default">Operation</th>
								<th class="ui-state-default">Screen</th>
								<th class="ui-state-default">User/Role</th>
								<th class="ui-state-default">Operation Orders</th>
								<th class="ui-state-default">Escalation</th>
								<th class="ui-state-default">Reminder</th>
								<th class="ui-state-default">Quantum</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>

					<br />
					<div>
						<table style="width: 100%;">
							<tr>
								<td style="width: 15%; float: left;">Workflow Title<span style="color:red">*</span></td>
								<td style="width: 35%; float: left;"><input type="text" name="workflowTitle" id="workflowTitle" 
										 class="" /> 
								</td>
							</tr>
						</table>
						<table id="addWorkflowTable" style="width: 45%; float: left;">
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>Operation<span style="color:red">*</span></td>
								<td><select id="operations" class="combobox"
									style="margin-left: 2px; padding: 0px !important;">
										<option value="0">-- Select --</option>
								</select>
								</td>

							</tr>
							<tr>
								<td>Screen<span style="color:red">*</span></td>
								<td>
									<div>
										<select id="screens" class="combobox"
											style="margin-left: 2px; padding: 0px !important;">
											<option value="0">-- Select --</option>
										</select>
									</div>
								</td>

							</tr>
							<tr id="reject-screen-tr" style="display:none;">
								<td>Reject Screen<span style="color:red">*</span></td>
								<td>
									<div>
										<select id="rejectScreens" class="combobox"
											style="margin-left: 2px; padding: 0px !important;">
											<option value="0">-- Select --</option>
										</select>
									</div>
								</td>

							</tr>
							<tr>
								<td>Access Right Type<span style="color:red">*</span></td>
								<td><select id="accessRight" class="combobox"
									style="margin-left: 2px; padding: 0px !important;">
										<option value="0">Role</option>
										<option value="1">User</option>
								</select></td>

							</tr>
							<tr>
								<td>User/Role<span style="color:red">*</span></td>
								<td>
									<div id="userRolesDiv">
										<select id="userRoles" class="combobox"
											style="margin-left: 2px; padding: 0px !important;">
											<option value="0">-- Select --</option>
										</select>
									</div>
									<div id="personsDiv" style="display: none;">
										<select id="persons" class="combobox"
											style="margin-left: 2px; padding: 0px !important;">
											<option value="0">-- Select --</option>
										</select>
									</div>
								</td>

							</tr>
							<tr>
								<td>Operation Order<span style="color:red">*</span></td>
								<td><input type="text" id="operationOrder" value="1"
									style="width: 90%;" class="" /> <input
									type="hidden" id="operationNumber" name="operationNumber"
									value="1" /></td>

							</tr>

						</table>
					</div>
					<div>
						<table style="width: 45%; float: left; padding-left: 20px;">
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr class="quantum-rule-div" style="display: none;">
								<td>Quantum Value</td>
								<td><input type="text" id="quantumValue"
									style="width: 90%;" class="" />
								</td>
							</tr>
							<tr class="quantum-rule-div" style="display: none;">
								<td>Quantum Information</td>
								<td><textarea id="quantumText" style="width: 90%;"
									 ></textarea><span>Example : Quantum value(5000) &lt; Actual Value(6000)</span>
								</td>
							</tr>
							<tr>
								<td>Escalation</td>
								<td style="float: left"><input type="checkbox"
									id="isEscalation"></td>
							</tr>
							<tr class="escalation-div">
								<td>Validity</td>
								<td><input type="text" id="validity" style="width: 90%;"
									 class="" />
								</td>
							</tr>
							<tr class="escalation-div">
								<td>Validity Days</td>
								<td style="float: left"><input type="checkbox" id="isDays">
								</td>
							</tr>
							<tr class="escalation-div">
								<td>Escalation To</td>
								<td><select id="accessRightEscalation" class="combobox"
									style="margin-left: 2px; padding: 0px !important;">
										<option value="0">Role</option>
										<option value="1">User</option>
								</select></td>

							</tr>
							<tr class="escalation-div">
								<td>Notify User/Role</td>
								<td>
									<div id="userRolesDivEscalation">
										<select id="userRolesEscalation" class="combobox"
											style="margin-left: 2px; padding: 0px !important;">
											<option value="0">-- Select --</option>
										</select>
									</div>
									<div id="personsDivEscalation" style="display: none;">
										<select id="personsEscalation" class="combobox"
											style="margin-left: 2px; padding: 0px !important;">
											<option value="0">-- Select --</option>
										</select>
									</div>
								</td>

							</tr>
							<tr class="escalation-div">
								<td>Reminder</td>
								<td style="float: left"><input type="checkbox"
									id="isReminder"></td>
							</tr>
							<tr class="escalation-div">
								<td>Remind Before</td>
								<td><input type="text" id="reminderDays"
									style="width: 90%;" class="" /></td>
							</tr>
							<tr class="escalation-div">
								<td>Reminder Days</td>
								<td style="float: left"><input type="checkbox"
									id="isReminderDays"></td>
							</tr>
						</table>
					</div>
					<div style="clear: both;"></div>
					<%--Recent workflows list size --%>
					<s:hidden id="recentWorkflowListSize" name="recentWorkflowListSize"
						value="0" />
					<%--To keep track whether user has defined flow for all the three processes --%>
					<s:hidden id="isEntry" name="isEntry" value="0" />
					<s:hidden id="isApproval" name="isApproval" value="0" />
					<s:hidden id="isPublish" name="isPublish" value="0" />


					<br />
					<div align="right" style="margin-right: 1%; float: right;">
						<input type="button"
							class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
							id="addWorkflowButton" value="Add" onclick="addWorkflow();" /> <input
							type="button"
							class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover"
							id="saveWorkflowButton" value="Save" onclick="saveWorkflows();" />

						<input type="button"
							class="buttons_c ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
							value="Cancel" onclick="location.reload();" />
					</div>
					<br />
				</div>
				<br />
			</div>
		</s:form>
	</div>
	<%---------------------------------------------------------Operation Popup Form -------------------------------------------------------++ --%>
		
	<div id="Operation_Form_Div" style="display:none; margin:0px!important;" class="content">
			<s:form action="saveWorkflowOperation" id="Operation_Form"  theme="simple"	method="post"> 
				<s:hidden name="operationMaster.operationMasterId" id="operationMasterId"/>
				<s:hidden name="operationMaster.moduleProcess.processId" id="moduleProcessId"/>
				<div class="bordered_panel">
					<div class="inner_panel_header ui-widget-header">
					     <p>
						     Operation Information
					     </p>
					</div>
				
					<table class="contentTable" style="width:100%;">
						<tr>
							<td class="tableLabel" style="width:30%;">
								Operation Group <span style="color:red">*</span>: 
							</td>
							<td class="comboColumn" style="width: 50%;"><s:select class="combobox" 
									name="operationMaster.operationGroup" id="operationGroup"
									list="operationGroupList" listValue="value" listKey="key"
									headerKey="" headerValue="-Select-">
								</s:select>
							</td>
							
						</tr>	
						<tr>
							<td class="tableLabel" style="width:30%;">
								Title <span style="color:red">*</span>:
							</td>
							<td class="tableField" style="width: 50%;">
								<s:textfield name="operationMaster.title" id="operationTitle" 
											/>
							</td>
						</tr>
						<tr>
							<td class="tableLabel" style="width:30%;">
								Process Button :
							</td>
							<td class="tableField" style="width: 50%;">
								<s:textfield name="operationMaster.approveButton" id="approveButton" 
											/>
							</td>
						</tr>
						<tr id="reject-button-tr">
							<td class="tableLabel" style="width:30%;">
								Reject Button :
							</td>
							<td class="tableField" style="width: 50%;">
								<s:textfield name="operationMaster.rejectButton" id="rejectButton" 
											/>
							</td>
						</tr>
						
						<tr>
							<td colspan="2">
								<div align="right" style="margin-right: 1%; float: right;width:100%;">
					
									<input id="Operation_delete" type="button" value="Delete" style="float: right;width:20%;"
										class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"/>
									<input id="Operation_Reset" type="button" value="Reset" style="float: right;width:20%;"
										class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"/>
									<input id="Operation_save" type="button" value="Save" style="float: right;width:20%;"
										class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"/>
										
								</div></td>
	
						</tr>
						<tr>
							<td colspan="2">
								<div style="width:100%;float:left;">Operaions :</div>
								<select class="combobox" style="width: 100%;"
									name="operationList" id="operationList" size="10">
									<c:forEach var="opr" items="${operationList}" varStatus="status">
									<option value="${opr.operationMaster.operationMasterId}">
									${opr.operationMaster.title}
									</option>
									
									</c:forEach>
								</select>
							</td>
							
						</tr>
				</table>
				</div>
			</s:form>	
		</div>
</div>
<br />
