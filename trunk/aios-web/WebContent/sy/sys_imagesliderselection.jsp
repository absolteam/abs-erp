<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <style>
body { font-size: 62.5%; }
label, input { display:block; }
input.text { margin-bottom:12px; width:95%; padding: .4em; }
fieldset { padding:0; border:0; margin-top:25px; }
h1 { font-size: 1.2em; margin: .6em 0; }
div#users-contain { width: 350px; margin: 20px 0; }
div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
.ui-dialog .ui-state-error { padding: .3em; }
.validateTips { border: 1px solid transparent; padding: 0.3em; color: red;}
.dataTables_filter { width: 12%;}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var imageId=null;
var dialog = null;

var parentRow = null;
var specialNameCol = null;
var specialCodeCol = null;

$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	 $('#OpenPosition tbody tr').live('dblclick', function() {
		if (!$('#OpenPosition tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					viewRowFromImageSlider(aData);
					//images.push(aData.imageId);
	
					
				}
			}
			
		}
		return false;
	});
	 
	/* Click event handler */
	$('#OpenPosition tbody tr').live('click', function() {
		if (!$('#OpenPosition tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					$('#spcialName').val(aData.specialName);
					$('#code').val(aData.code);
				}
			}
		}
	});
	


$('#edit').click(function(){
		var values = "";
		$.each($("input[name='image_group[]']:checked"), function() {
		  values=values+","+($(this).val());
		  // or you can do something to the actual checked checkboxes by working directly with  'this'
		  // something like $(this).hide() (only something useful, probably) :P
		});
		if(values == null || values=='')
		{
			alert("Please select images");
			return false;
		}
		else
		{	
			values = values.substring(1);
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/update_image_slider.action",  
	     	data: {selectedImages:values},  
	     	async: false,
			dataType: "json",
			cache: false,
			success: function(response)
	     	{  
				if(response.returnStatus=='SUCCESS'){
					$('#success_message').hide().html("Successfully Updated").slideDown(1000); 
					$('#success_message').delay(2000).slideUp();
					listCall();
				}else{
					$('#error_message').hide().html("Error in update, please check the directory path").slideDown(1000);
					$('#error_message').delay(2000).slideUp();
				}
						
	     	},
	     	 error:function(result)
             {
                
             }
		}); 
		return false;
		}
		return false;  
});

dialog = $( "#dialog-slide-image" ).dialog({
	autoOpen: false,
	height: 300,
	width: 350,
	modal: true,
	buttons: {
	"Save": saveImage,
	Cancel: function() {
		dialog.dialog( "close" );
		}
	},
	close: function() {
	}
});
	

	
$('.slide-images').live('click', function() {

	if($(this).attr('checked') == 'checked' || $(this).attr('checked') == true) {		
		
		imageId=$(this).val();
		dialog.dialog( "open" );
		
		parentRow = $(this).closest("tr");
		specialNameCol = parentRow.children('td').eq(2);
		specialCodeCol = parentRow.children('td').eq(3);
	}
});


});

function saveImage(){
	var spcialName=$('#spcialName').val();
	var code=$('#code').val();
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/save_image_slider_info.action",  
     	data: {imageId:imageId,spcialName:spcialName,code:code},  
     	async: false,
		dataType: "json",
		cache: false,
		success: function(response)
     	{  
			if(response.returnStatus=='SUCCESS'){
				
				dialog.dialog( "close" );
				$('#success_message').hide().html("Successfully Updated").slideDown(1000); 
				$('#success_message').delay(2000).slideUp();
				
				specialNameCol.text($('#spcialName').val()) ;
				specialCodeCol.text($('#code').val());
				
				if (aData != undefined) {
					if (aData != null) {
						aData.specialName = $('#spcialName').val();
						aData.code = $('#code').val();
					}
				}
				
				$('#spcialName').val("");
				$('#code').val("");
				
				parentRow = null;
				specialNameCol = null;
				specialCodeCol = null;
				
				imageId=null;
				//listCall();
				
			}else{
				$('#error_message').hide().html("Error in update, please check the provided information").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
			}
					
     	},
     	 error:function(result)
            {
               
            }
	}); 
	return false;  
}
function listCall(){
	
	
	$('#OpenPosition').dataTable().fnDestroy();
	oTable = $('#OpenPosition')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				 "iDisplayLength": 300,
				 "sScrollY": $("#main-content").height() - 250,
				"sAjaxSource" : "<%=request.getContextPath()%>/image_slider_list_selection_json.action",
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				
				"aoColumns" : [
				{ "mDataProp": function(aData) {
					if(aData.isSlider!=null && aData.isSlider==1){
						return '<td><input type="checkbox" checked="checked" class="slide-images" name="image_group[]" value="'+aData.imageId+'"/></td>';
					}else{
						return '<td><input type="checkbox" class="slide-images" name="image_group[]" value="'+aData.imageId+'"/></td>';

					}
				},
					"sWidth": "7%"
				},{
					"mDataProp" : "name"
				}, {
					"mDataProp" : "specialName",
					"sWidth": "22%"
				}, {
					"mDataProp" : "code",
					"sWidth": "22%"
				}, {
					"mDataProp" : "tableName",
					"sWidth": "10%"
				}, {
					"mDataProp" : "compImgSize",
					"sWidth": "8%"
				}]

	}); 
	
	
		
}
function viewRowFromImageSlider(aData){
	var qryStr = "hashedName="+aData.hashedName+"&nodeText=" + aData.name + "&imgId=" + aData.imageId
	+ "&recordId=" + aData.recordId + "&tableName="
	+ aData.tableName + "&fileType=img" 
	+ "&fileToUploadFileName=" + aData.name;
	window.open('viewUploadedFile.action?'+qryStr,
			'_blank', 
			'width=800,height=600,scrollbars=no,left=100px,top=20px');  
	
	 return false;
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Image Slider Selection</div>
			
		<div class="portlet-content">
		 <div id="error_message" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div id="success_message" class="success response-msg ui-corner-all " style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
			 <div id="gridDiv">
				<table class="display" id="OpenPosition">
					<thead>
						<tr>
							<th>Choose</th>
							<th>Image Name</th>
							<th>Display Name</th>
							<th>Code</th>
							<th>Screen</th>
							<th>Size</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
	<div class="float-left buttons ui-widget-content ui-corner-all"> 
		&nbsp;&nbsp;&nbsp;**Double Click on the Record to view the photo	
	</div>
	<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="edit">Update List</div>
		
	</div>
	
	
	<div id="dialog-slide-image" title="Updating Image Information">
		<p class="validateTips">All form fields are required.</p>
		<p class="validateTips" style="color: gray;">Code value should not exceed 15 characters. Please press cancel if you don't need to update these values.</p>
		 <form>
		<fieldset>
			<label for="spcialName">Name</label>
			<input type="text" name="spcialName" id="spcialName"  class="text ui-widget-content ui-corner-all">
			<label for="code">Code</label>
			<input type="text" name="code" id="code" class="text ui-widget-content ui-corner-all">
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="button" tabindex="-1" id="save" style="position:absolute; top:-1000px">
		</fieldset>
		</form>
	</div>
</div>
</div>
</div>
