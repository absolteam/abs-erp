<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div>
	<div class="width100">
		<div id="users" style="padding: 5px; display: none;">
			<label style="width: 15%; display: block;">User: </label>
			<select id="users_list" style="width: 206px;">
				<option value="0">- Select -</option>
				<c:forEach items="${requestScope.USERS_LIST}" var="USERS">
					<option value="${USERS.username}">${USERS.displayUsername}</option>
				</c:forEach>
			</select>
		</div>
		<div id="oldPassword_div" style="padding: 5px;">
			<label style="width: 30%;">Old password: </label>
			<input id="oldPassword" type="password" style="width: 200px;"></input>
		</div>
		<div style="padding: 5px;">
			<label style="width: 30%;">New password: </label>
			<input id="newPassword" type="password" style="width: 200px;"></input>	
		</div>
		<div style="padding: 5px;">
			<label style="width: 30%;">Confirm password: </label>
			<input id="confirmNewPassword" type="password" style="width: 200px;"></input>	
		</div>
		<div style="padding: 5px;">
			<label id="success" style="display: none; color: green;">Password changed successfully.</label>
			<label id="failure" style="display: none; color: red;">Old password is incorrect.</label>
			<label id="mismatch" style="display: none; color: red;">New and confirmed password mismatch.</label>
			<label id="empty" style="display: none; color: red;">All fields are required.</label>
		</div>
		<div style="display:none; padding: 5px;">
			<input type="button" class="button" value="Update" id="updatePassword"></input>
		</div>
	</div>
</div>

<input type="hidden" name="passwordFlag" id="passwordFlag" value="${passwordFlag}"/>

<script type="text/javascript">
	$(function() {

		var pswdFlag = $("#passwordFlag").val();

		if(pswdFlag == "R") {
			$("#oldPassword_div").hide();
			$("#users").show();
		}
		
		$("#updatePassword").click(function() {

			var oldPassword = $("#oldPassword").val();
			var newPassword = $("#newPassword").val();
			var confirmNewPassword = $("#confirmNewPassword").val();
			var selectedUser = $("#users_list").val();

			if(((newPassword == '' || confirmNewPassword == '' || oldPassword == '') && pswdFlag == "C")
					|| ((newPassword == '' || confirmNewPassword == '' || selectedUser == '' ||  selectedUser == 0) && pswdFlag == "R")){

				$("#success").hide();
		 		$("#failure").hide();				
				$("#mismatch").hide();
				
				$("#empty").fadeIn();
				return false;
			}
			
			else if(newPassword != confirmNewPassword){

				$("#success").hide();
		 		$("#failure").hide();
				$("#empty").hide();
				
				$("#mismatch").fadeIn();
				return false;
			}
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/updatePassword.action",
			 	async: false, 
			 	data:{oldPassword: oldPassword, newPassword: newPassword, passwordFlag: pswdFlag, username: selectedUser},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#empty").hide();
					$("#mismatch").hide();
					$("#failure").hide();
					
			 		$("#success").fadeIn();			 		
			 		$('#password-update-popup').dialog("close");
			 		$('#passwordChangeMessage').fadeIn();
			 		setTimeout("$('#passwordChangeMessage').fadeOut()", 3000);
					return true;
				},
				error:function(result){ 
					$("#success").hide();
			 		$("#failure").hide();
					$("#empty").hide();
					$("#mismatch").hide();
					
					$("#failure").fadeIn();					
					return false;
				}
			}); 
		});
	});
</script>