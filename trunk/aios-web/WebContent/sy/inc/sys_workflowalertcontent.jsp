<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/popup/jquery.bubblepopup.v2.3.1.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/popup/jquery.bubblepopup.v2.3.1.min.js"></script>   
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<!-- <style>
.underlinecss{
	padding: 5px;  
/*     border-bottom: 1px dotted #8F8F8F;
 */   
 	 text-indent: 2px;
    background-color:#7C7C77;
    margin-top:2px;
    -moz-border-radius: 10px;
	border-radius: 10px;
}
.underlinecss span{
    color: #FFFFFF;
    font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    font-size: 14px;
}
.underlinecss:hover{ 
     background-color: #eff0c5;
     font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
     font-size: 13px;
}
.underlinecss:hover span{ 
    color: #646464;
}
</style> -->
<input type="hidden" id="alertCount_${requestScope.processFlag}" value="${fn:length(alertList)}"/>
<c:choose>
	<c:when test="${workflowList!= null && (fn:length(alertList) > 0)}" >
	<c:forEach var="wrkflwRList" items="${alertList}" varStatus="status">
		<li class="underlinecss">
			<a class="workflowAlertProcess" id="${requestScope.processFlag}_${status.index+1}" href="#">
				<span>${wrkflwRList.workflow.moduleProcess.processTitle}</span>
				<span style="font-size:15px;color: green;font-weight: bold;">  >>  </span>
			<c:if test="${wrkflwRList.recordName ne null && wrkflwRList.recordName ne ''}">
				<span>${wrkflwRList.recordName}</span>
				<span style="font-size:15px;color: green;font-weight: bold;">  >>  </span>
			</c:if>
			<span>${wrkflwRList.notification.notificationText}</span>
			<div id="pop-up_${status.index+1}" class="pop-up" style="overflow:hidden;">
	      	  <span style="display: none;" id="recordId1_${status.index+1}">${requestScope.processFlag}_${wrkflwRList.recordId}</span>
		 		<input type="hidden" id="workflowDetailId_${status.index+1}" value="${wrkflwRList.workflowDetailId}"/>
				<input type="hidden" id="screen_${status.index+1}" value="${wrkflwRList.screen.screenPath}"/>
				<input type="hidden" id="useCase_${status.index+1}" value="${wrkflwRList.workflow.useCase}"/>
				<input type="hidden" id="recordId_${status.index+1}" value="${wrkflwRList.recordId}"/>
				<input type="hidden" id="isHidden_${status.index+1}" value="${wrkflwRList.screen.isHidden}"/>
				<input type="hidden" id="toProcess_${status.index+1}" value="${wrkflwRList.toProcess}"/>
	   		 </div>
			</a>
			<div class="button_links"></div>
		</li>
	</c:forEach>
	</c:when>
	<c:otherwise>
	<li><fmt:message key="sys.nortification.noAlert" /></li>
	</c:otherwise>
</c:choose>	
		