<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/js/popup/jquery.bubblepopup.v2.3.1.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/popup/jquery.bubblepopup.v2.3.1.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<!-- <style>
.underlinecss{
	padding: 5px;  
/*     border-bottom: 1px dotted #8F8F8F;
 */   
 	 text-indent: 2px;
    background-color:#e4d9b6;
    margin-top:2px;
    -moz-border-radius: 10px;
	border-radius: 10px;
	border:1px solid #dbc680;
	box-shadow:1px 1px 1px #ccc;
}
.underlinecss span{
    color: #000;
    font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    font-size: 14px;
}
.underlinecss:hover{ 
     background-color: #7C7C77;
     font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
     font-size: 13px;
     border:1px solid #333;
}
.underlinecss:hover span{ 
    color: #fff;
}
</style> -->
<script type="text/javascript">
	
</script>
<input type="hidden" id="alertCount" value="${fn:length(alertList)}" />
<c:choose>
	<c:when test="${alertList!= null && (fn:length(alertList) > 0)}">
		<c:forEach var="wrkflwRList" items="${alertList}" varStatus="status">
			<div class="row1">
				<p>
					<a class="workflowAlertProcess" id="Alert_${status.index+1}"
						href="#"> <span>Reminder</span> <span
						style="font-size: 15px; color: green; font-weight: bold;">
							>> </span> <span id="message_${status.index+1}">${wrkflwRList.message} <span class="wf_timestamp">${wrkflwRList.createdDateString}</span></span>
						<input type="hidden" id="alertRecordId_${status.index+1}"
						value="${wrkflwRList.recordId}" /> <input type="hidden"
						id="alertScreenPath_${status.index+1}"
						value="${wrkflwRList.screen.screenPath}" /> <input type="hidden"
						id="alertId_${status.index+1}" value="${wrkflwRList.alertId}" /> <input
						type="hidden" id="alertUseCase_${status.index+1}"
						value="${wrkflwRList.tableName}" />
					</a>
				</p>

				<div class="row_button">
					<div class="r_b1">
						<a href="#"><img src="images/row_b1.png" /></a>
					</div>
					<div class="r_b1">
						<a href="#"><img style="margin: 8px 0 0 6px !important;"
							src="images/row_b2.png" /></a>
					</div>
					<div class="r_b1">
						<a href="#"><img style="margin: 6px 0 0 6px !important;"
							src="images/row_b3.png" /></a>
					</div>
					<div class="r_b1">
						<a href="#"><img style="margin: 5px 0 0 5px !important;"
							src="images/row_b4.png" /></a>
					</div>


				</div>
				<!-- <div class="button_links"></div> -->
			</div>
			
		</c:forEach>
		<div class="">
			<c:choose>
				<c:when test="${applyLimit eq true}">
					<div id="loadMore" style="font-size: 13px; font-weight: bold; cursor: pointer; padding: 5px; float:right">Load More...</div>
				</c:when>
			</c:choose>
		</div>
	</c:when>
	<c:otherwise>
		<li><fmt:message key="sys.nortification.noAlert" /></li>
	</c:otherwise>
</c:choose>
