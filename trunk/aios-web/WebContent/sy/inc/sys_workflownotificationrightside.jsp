<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<input type="hidden" id="recordCount_${requestScope.processFlag}" value="${fn:length(workflowList)}"/>
<c:choose>
 	<c:when test="${workflowList!= null && (fn:length(workflowList) > 0)}" >
		<c:forEach var="wrkflwRList" items="${requestScope.workflowList}" varStatus="status">
			<li>
				<div class="texts">
					<a class="workflowNotificationProcess" id="${requestScope.processFlag}_${status.index+1}" href="#">
					<c:choose>
						<c:when test="${sessionScope.lang ne null && (sessionScope.lang eq 'ar' || sessionScope.lang eq 'AR')}">
							<span>${wrkflwRList.moduleProcess.processTitleAr}:</span>
						</c:when>
						<c:otherwise>
							<span>${wrkflwRList.workflow.moduleProcess.processTitle}:</span>
						</c:otherwise>
					
						
				   		 </c:choose>
				   		 
				   		 <c:if
									test="${wrkflwRList.operation eq 3 or wrkflwRList.operation eq 1}">
									<fmt:message key="waitingForApproval" />
								</c:if> 
								
								<c:if test="${wrkflwRList.operation eq 4}">
									<fmt:message key="waitingforPublish" />
								</c:if> 
								<c:if test="${wrkflwRList.operation eq 8}">
									<fmt:message key="approvedNotice" />
								</c:if> 
								<c:if test="${wrkflwRList.operation eq 2}">
									<fmt:message key="rejectedNotice" />
								</c:if> 
								
				   		  
				   		 <div>
				      	  <span style="display: none;" id="${requestScope.processFlag}_recordId1_${status.index+1}">${wrkflwRList.recordId}</span>
					 		<input type="hidden" id="${requestScope.processFlag}_workflowDetailId_${status.index+1}" value="${wrkflwRList.workflowDetailId}"/>
							<input type="hidden" id="${requestScope.processFlag}_screen_${status.index+1}" value="${wrkflwRList.screenByScreenId.screenPath}"/>
							<input type="hidden" id="${requestScope.processFlag}_useCase_${status.index+1}" value="${wrkflwRList.workflow.moduleProcess.useCase}"/>
							<input type="hidden" id="${requestScope.processFlag}_recordId_${status.index+1}" value="${wrkflwRList.recordId}"/>
							<input type="hidden" id="${requestScope.processFlag}_isHidden_${status.index+1}" value="${wrkflwRList.screenByScreenId.isHidden}"/>
							<input type="hidden" id="${requestScope.processFlag}_processFlag_${status.index+1}" value="${wrkflwRList.processFlag}"/>
<%-- 							<input type="hidden" id="${requestScope.processFlag}_toProcess_${status.index+1}" value="${wrkflwRList.toProcess}"/>
 --%>							<input type="hidden" id="${requestScope.processFlag}_recordNumber_${status.index+1}" value="${wrkflwRList.recordNumber}"/>
							<input type="hidden" id="${requestScope.processFlag}_recordName_${status.index+1}" value="${wrkflwRList.recordName}"/>
							
							
							<input type="hidden" id="${requestScope.processFlag}_screenId_${status.index+1}" value="${wrkflwRList.screenId}"/>
							<input type="hidden" id="${requestScope.processFlag}_operationId_${status.index+1}" value="${wrkflwRList.operation}"/>
							<input type="hidden" id="${requestScope.processFlag}_messageId_${status.index+1}" value="${wrkflwRList.message.messageId}" />
				   		 </div>
					</a>
				</div>
			</li>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<li><fmt:message key="sys.nortification.notAvaiable" /></li>
	</c:otherwise>
</c:choose>	
	
