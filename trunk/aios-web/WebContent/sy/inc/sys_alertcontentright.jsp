<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<input type="hidden" id="alerRrecordCount" value="${fn:length(alertList)}"/>
<c:choose>
 	<c:when test="${alertList!= null && (fn:length(alertList) > 0)}" >
		<c:forEach var="wrkflwRList" items="${requestScope.alertList}" varStatus="status">
			
				<div class="texts">
					<a href="#">
					<span id="message_${status.index+1}">${wrkflwRList.message}</span>
					<input type="hidden" id="alertRecordId_${status.index+1}" value="${wrkflwRList.recordId}"/>
					<input type="hidden" id="alertScreenPath_${status.index+1}" value="${wrkflwRList.screen.screenPath}"/>	
					<input type="hidden" id="alertId_${status.index+1}" value="${wrkflwRList.alertId}"/>
					<input type="hidden" id="alertUseCase_${status.index+1}" value="${wrkflwRList.tableName}"/>
					</a>
				</div>
			
		</c:forEach>
		<div class="texts">
			<c:choose>
				<c:when test="${applyLimit eq true}">
					<div id="loadMoreRight" style="font-size: 13px;font-weight: bold;cursor: pointer;padding: 5px;float:right;">Load More...</div>
				</c:when>
			</c:choose>
		</div>
	</c:when>
	<c:otherwise>
		<li><fmt:message key="sys.nortification.noAlert" /></li>
	</c:otherwise>
</c:choose>	
	
