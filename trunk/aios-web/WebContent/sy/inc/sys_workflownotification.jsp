<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<!-- <style>
.underlinecss{
	padding: 5px;  
/*     border-bottom: 1px dotted #8F8F8F;
 */   
 	 text-indent: 2px;
    background-color:#7C7C77;
    margin-top:2px;
    -moz-border-radius: 10px;
	border-radius: 10px;
}

.underlinecss span{
    color: #FFFFFF;
    font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    font-size: 14px;
}
.underlinecss:hover{ 
     background-color: #eff0c5;
     font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
     font-size: 13px;
}
.underlinecss:hover span{ 
    color: #646464;
}
/* Reject mapping */
.underlinecss-reject{
	padding: 5px;  
/*     border-bottom: 1px dotted #8F8F8F;
 */   
 	 text-indent: 2px;
    background-color:#ADAD85;
    margin-top:2px;
    -moz-border-radius: 10px;
	border-radius: 10px;
}
.underlinecss-reject span{
    color: #FFFFFF;
    font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    font-size: 14px;
}
.underlinecss-reject:hover{ 
     background-color: #eff0c5;
     font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
     font-size: 13px;
}
.underlinecss-reject:hover span{ 
    color: #646464;
}
</style> --> 
<script type="text/javascript">
$(function(){
	$('#checkAllApproveRead').change(function(){
		var check = $(this).attr('checked');
		$('.checkApproveRead').attr('checked', check);
		if(check ==  false){
			$('.deleteApproveRead').hide();
		}else{
			$('.deleteApproveRead').show();
		} 
	});
	
	$('.checkApproveRead').change(function(){  
		var checkAll = true;
 		$('.checkApproveRead').each(
				function() { 
			var check = $(this).attr('checked');
			if(check == true){
				$('.deleteApproveRead').show();
			}else{
				checkAll = false;
			}
		});
 		if(checkAll ==  true){
 			$('#checkAllApproveRead').attr('checked', checkAll);
 		}
	});
	
	var getApproveReadContents = function(){
	  var approveReadContents = [];  
	  $('.row1 .workflowNotificationProcess').each(function(){  
		var processId = $(this).attr('id').split('_')[0];   
		var rowId = $(this).attr('id').split('_')[1];   
		var messageId = $('#'+processId+'_messageId_'+rowId).val();
		var checkApproveRead = $('#checkApproveRead_'+rowId).attr('checked');
		if(checkApproveRead == true && messageId > 0 && processId == Number(8)){ 
			approveReadContents.push({
				"messageId" : messageId
			}); 
		} 
	  });
	  return approveReadContents;
	};
	$('.deleteApproveRead').click(function(){
		var approveReadContents = getApproveReadContents();  
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_workflow_message.action", 
			 	async: false, 
			 	data:{ approveReadContents: JSON.stringify(approveReadContents)},
			    dataType: "json",
			    cache: false,
				success:function(response){   
				} 
			}); 
		window.location.reload();
	});
});
</script>
<input type="hidden" id="mainrecordCount_${requestScope.processFlag}"
	value="${fn:length(workflowList)}" />
<c:choose>
	<c:when test="${workflowList!= null && (fn:length(workflowList) > 0)}">
		<c:if test="${requestScope.processFlag == 8}">
			<div class="float-left">
				<div class="float-left" style="position: relative; left:3px; top: 4px;">
					<input type="checkbox" id="checkAllApproveRead"/> 
					<label style="font-weight: bold;" for="checkAllApproveRead">Check All</label>
				</div>  
				<div class="portlet-header ui-widget-header float-right deleteApproveRead"
					style="cursor: pointer; position: relative; left:20px; margin: 0px; display: none;">
					Delete
				</div> 
			</div> 
		</c:if>
		<c:forEach var="wrkflwRList" items="${requestScope.workflowList}"
			varStatus="status">
			<c:choose>
				<c:when
					test="${wrkflwRList.operation!=null && wrkflwRList.operation==2}">
					<div class="row1"> 
						<p>
							<a class="workflowNotificationProcess"
								id="${requestScope.processFlag}_${status.index+1}" href="#">
								<%-- <span>${wrkflwRList.workflow.moduleProcess.processTitle}</span> --%>
								<span style="font-weight: normal;">
									${wrkflwRList.message.message}
								</span>
									<%-- <c:if
										test="${wrkflwRList.operation eq 3 or wrkflwRList.operation eq 4}">
										<fmt:message key="waitingForApproval" />
									</c:if> <c:if test="${wrkflwRList.operation eq 9}">
										<fmt:message key="waitingforPublish" />
									</c:if> <c:if test="${wrkflwRList.operation eq 8}">
										<fmt:message key="approvedNotice" />
									</c:if> <c:if test="${wrkflwRList.operation eq 2}">
										<fmt:message key="rejectedNotice" />
									</c:if> <c:if test="${wrkflwRList.operation eq 1}">
										<fmt:message key="publishedNotice" />
									</c:if>
 --%>

							 <span style="display: none;" id="recordId1_${status.index+1}">${requestScope.processFlag}_${wrkflwRList.recordId}
									<input type="hidden" id="operationGroupId_${status.index+1}"
									value="${wrkflwRList.operationMaster.operationGroup}" /> 
									<input type="hidden" id="workflowDetailId_${status.index+1}"
									value="${wrkflwRList.workflowDetailId}" />  <input
									type="hidden" id="screen_${status.index+1}"
									value="${wrkflwRList.screenByScreenId.screenPath}" />  <input
									type="hidden" id="useCase_${status.index+1}"
									value="${wrkflwRList.workflow.moduleProcess.useCase}" /> <input
									type="hidden" id="recordId_${status.index+1}"
									value="${wrkflwRList.recordId}" /> <input type="hidden"
									id="isHidden_${status.index+1}"
									value="${wrkflwRList.screenByScreenId.isHidden}" /> <%-- 						<input type="hidden" id="toProcess_${status.index+1}" value="${wrkflwRList.toProcess}"/>
 --%> <input type="hidden" id="recordNumber_${status.index+1}"
									value="${wrkflwRList.recordNumber}" /> <input type="hidden"
									id="reocrdName_${status.index+1}"
									value="${wrkflwRList.recordName}" /> <%-- <input type="hidden"
									id="screenId_${status.index+1}" value="${wrkflwRList.screenId}" />
									<input type="hidden" id="operationId_${status.index+1}" value="${wrkflwRList.operation}" /> --%>
							</span>


								<div class="row_button">
									<div class="r_b1">
										<a href="#"><img src="images/row_b1.png" /></a>
									</div>
									<div class="r_b1">
										<a href="#"><img style="margin: 8px 0 0 6px !important;"
											src="images/row_b2.png" /></a>
									</div>
									<div class="r_b1">
										<a href="#"><img style="margin: 6px 0 0 6px !important;"
											src="images/row_b3.png" /></a>
									</div>
									<div class="r_b1">
										<a href="#"><img style="margin: 5px 0 0 5px !important;"
											src="images/row_b4.png" /></a>
									</div>


								</div>
							</a>
							<!-- <div class="button_links"></div> -->
						</p>
					</div>
				</c:when>
				<c:otherwise>
					<div class="row1"> 
						<p>
							<c:if test="${requestScope.processFlag == 8}">
								<span class="float-left" style="margin-right: 5px;">
									<input type="checkbox" class="checkApproveRead" id="checkApproveRead_${status.index+1}"/>
								</span> 
							</c:if>
							<a class="workflowNotificationProcess"
								id="${requestScope.processFlag}_${status.index+1}" href="#">
								<span style="font-weight: normal;">
									${wrkflwRList.message.message} <span class="wf_timestamp">${wrkflwRList.message.createdDate}</span>
								</span>
								<%-- <c:choose>
									<c:when
										test="${sessionScope.lang ne null && (sessionScope.lang eq 'ar' || sessionScope.lang eq 'AR')}">
										<span>${wrkflwRList.workflow.moduleProcess.processTitleAr}</span>
									</c:when>
									<c:otherwise>
										<span>${wrkflwRList.workflow.moduleProcess.processTitle}</span>
									</c:otherwise>
								</c:choose> <span style="font-size: 15px; color: green; font-weight: bold;">
									: </span> --%>  
							<%-- 	<c:if
									test="${wrkflwRList.operation eq 3 or wrkflwRList.operation eq 4}">
									<fmt:message key="waitingForApproval" />
								</c:if> <c:if test="${wrkflwRList.operation eq 9}">
									<fmt:message key="waitingforPublish" />
								</c:if> <c:if test="${wrkflwRList.operation eq 8}">
									<fmt:message key="approvedNotice" />
								</c:if> <c:if test="${wrkflwRList.operation eq 2}">
									<fmt:message key="rejectedNotice" />
								</c:if> <c:if test="${wrkflwRList.operation eq 1}">
									<fmt:message key="publishedNotice" />
								</c:if>
									 --%>
									
								<%--  <span style="display: none;" id="recordId1_${status.index+1}">${requestScope.processFlag}_${wrkflwRList.recordId}
									
									<input type="hidden" id="operationGroupId_${status.index+1}"
									value="${wrkflwRList.operationMaster.operationGroup}" /> 
									<input type="hidden" id="workflowDetailId_${status.index+1}"
									value="${wrkflwRList.workflowDetailId}" /> <input
									type="hidden" id="screen_${status.index+1}"
									value="${wrkflwRList.screenByScreenId.screenPath}" />
									
									  <input
									type="hidden" id="useCase_${status.index+1}"
									value="${wrkflwRList.workflow.moduleProcess.useCase}" /> <input
									type="hidden" id="recordId_${status.index+1}"
									value="${wrkflwRList.recordId}" /> <input type="hidden"
									id="isHidden_${status.index+1}"
									value="${wrkflwRList.screenByScreenId.isHidden}" /> 						<input type="hidden" id="toProcess_${status.index+1}" value="${wrkflwRList.toProcess}"/>
 <input type="hidden" id="recordNumber_${status.index+1}"
									value="${wrkflwRList.recordNumber}" /> <input type="hidden"
									id="reocrdName_${status.index+1}"
									value="${wrkflwRList.recordName}" /> <input type="hidden"
									id="screenId_${status.index+1}" value="${wrkflwRList.screenId}" />
									<input type="hidden" id="operationId_${status.index+1}" value="${wrkflwRList.operation}" />
							</span> --%>
				      	  <span style="display: none;" id="${requestScope.processFlag}_recordId1_${status.index+1}">${wrkflwRList.recordId}
					 		<input type="hidden" id="${requestScope.processFlag}_workflowDetailId_${status.index+1}" value="${wrkflwRList.workflowDetailId}"/>
							<input type="hidden" id="${requestScope.processFlag}_screen_${status.index+1}" value="${wrkflwRList.screenByScreenId.screenPath}"/>
							<input type="hidden" id="${requestScope.processFlag}_useCase_${status.index+1}" value="${wrkflwRList.workflow.moduleProcess.useCase}"/>
							<input type="hidden" id="${requestScope.processFlag}_recordId_${status.index+1}" value="${wrkflwRList.recordId}"/>
							<input type="hidden" id="${requestScope.processFlag}_isHidden_${status.index+1}" value="${wrkflwRList.screenByScreenId.isHidden}"/>
							<input type="hidden" id="${requestScope.processFlag}_processFlag_${status.index+1}" value="${wrkflwRList.processFlag}"/>
<%-- 							<input type="hidden" id="${requestScope.processFlag}_toProcess_${status.index+1}" value="${wrkflwRList.toProcess}"/>
 --%>							<input type="hidden" id="${requestScope.processFlag}_recordNumber_${status.index+1}" value="${wrkflwRList.recordNumber}"/>
							<input type="hidden" id="${requestScope.processFlag}_recordName_${status.index+1}" value="${wrkflwRList.recordName}"/>
							
							
							<input type="hidden" id="${requestScope.processFlag}_screenId_${status.index+1}" value="${wrkflwRList.screenId}"/>
							<input type="hidden" id="${requestScope.processFlag}_operationId_${status.index+1}" value="${wrkflwRList.operation}"/>
							<input type="hidden" id="${requestScope.processFlag}_messageId_${status.index+1}" value="${wrkflwRList.message.messageId}" />
							<input type="hidden" id="${requestScope.processFlag}_processType_${status.index+1}" 
							value="${wrkflwRList.message.workflowDetailProcess.processType}" />
				   		 </span>
							</a>
						</p>
						<div class="row_button">
							<div class="r_b1">
								<a href="#"><img src="images/row_b1.png" /></a>
							</div>
							<div class="r_b1">
								<a href="#"><img style="margin: 8px 0 0 6px !important;"
									src="images/row_b2.png" /></a>
							</div>
							<div class="r_b1">
								<a href="#"><img style="margin: 6px 0 0 6px !important;"
									src="images/row_b3.png" /></a>
							</div>
							<div class="r_b1">
								<a href="#"><img style="margin: 5px 0 0 5px !important;"
									src="images/row_b4.png" /></a>
							</div>


						</div>



					</div>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<li style="padding: 7px 0 0 7px;"><fmt:message
				key="sys.nortification.notAvaiable" /></li>
	</c:otherwise>
</c:choose>
