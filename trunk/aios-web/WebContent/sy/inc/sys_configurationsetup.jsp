<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style> 
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
}  
</style>
<script type="text/javascript">
$(function(){
	$('.save_projectconfiguration').click(function(){
		var projectTitle = $.trim($('#projectTitle').val());
		var projectReference = $.trim($('#projectReference').val());
		var projectCustomer = $.trim($('#projectCustomer').val());
		var projectCreatedBy = $.trim($('#projectCreatedBy').val());
		var projectLead = $.trim($('#projectLead').val());
		var projectManager = $.trim($('#projectManager').val());
		
		var projectPaymentTerms = "hide";
		var penalityDescriptive = "hide";
		var contractTerms = "hide";
		var simpleSaveButton = "hide";
		var updateSaveButton = "hide";
		var sendRequisitionButton = "hide";
		
		if($('#projectPaymentTermsOn').attr('checked') == true)
			projectPaymentTerms = "show";
		if($('#penalityDescriptiveOn').attr('checked') == true)
			penalityDescriptive = "show";
		if($('#contractTermsOn').attr('checked') == true)
			contractTerms = "show";
		if($('#simpleSaveButtonOn').attr('checked') == true)
			simpleSaveButton = "show";
		if($('#updateSaveButtonOn').attr('checked') == true)
			updateSaveButton = "show";
		if($('#sendRequisitionButtonOn').attr('checked') == true)
			sendRequisitionButton = "show";
		var projectAccessArea = "";
		$('.projectAreaAccess').each(function(){
			if($(this).attr('checked') == true){ 
				projectAccessArea += $(this).attr('id')+",";
			} 
		});  
		if(projectAccessArea.length > 0)
			projectAccessArea = projectAccessArea.substring(0, projectAccessArea.length - 1); 
		
		var imageSliderAccessArea = $.trim($('#image_slider_access_area').val());
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_project_configurations.action",
			data:{ projectTitle: projectTitle, projectReference: projectReference, projectCustomer: projectCustomer, projectCreatedBy: projectCreatedBy,
				   projectLead: projectLead, projectManager: projectManager, projectPaymentTerms: projectPaymentTerms, penalityDescriptive: penalityDescriptive,
				   contractTerms: contractTerms, simpleSaveButton: simpleSaveButton, updateSaveButton: updateSaveButton,
				   sendRequisitionButton: sendRequisitionButton, projectAccessArea: projectAccessArea,imageSliderAccessArea:imageSliderAccessArea},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				var message=response.redirectValue; 
				if(message=="Success"){  
					 $.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_configuration_setup.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){  
							$("#DOMWindow").html(result);    
							$('#setup_success_message').hide().html("Configuration updated.").slideDown(1000); 
							$('#setup_success_message').delay(2000).slideUp();
							return false;
						} 		
					});  
				 }
				else{
					$('#setup_error_message').hide().html(message).slideDown(1000);
					$('#setup_error_message').delay(2000).slideUp(1000);
					return false;
				}
			}
		 });
		return false;
	});
	
	$('.save_accountsconfiguration').click(function(){
		var posPrintOrder = $.trim($('#posPrintOrder').val());
		var posNextInvoice = $.trim($('#posNextInvoice').val());
		var productUnifiedPrice = $('#productUnifiedPriceOn').attr('checked');   
		var pdcAlertDays = $.trim($('#pdcAlertDays').val());
		var posPrintOrderButton = "hide";
		var posSaveOrderButton = "hide";
		
		if($('#posPrintOrderButtonOn').attr('checked') == true)
			posPrintOrderButton = "show";
		if($('#posSaveOrderButtonOn').attr('checked') == true)
			posSaveOrderButton = "show"; 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_accounts_configurations.action",
			data:{ posPrintOrder: posPrintOrder, posNextInvoice: posNextInvoice, productUnifiedPrice: productUnifiedPrice, posPrintOrderButton: posPrintOrderButton,
				posSaveOrderButton: posSaveOrderButton, pdcAlertDays: pdcAlertDays},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				var message=response.redirectValue; 
				if(message=="Success"){  
					 $.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_configuration_setup.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){  
							$("#DOMWindow").html(result);    
							$('#setup_success_message').hide().html("Configuration updated.").slideDown(1000); 
							$('#setup_success_message').delay(2000).slideUp();
							return false;
						} 		
					});  
				 }
				else{
					$('#setup_error_message').hide().html(message).slideDown(1000);
					$('#setup_error_message').delay(2000).slideUp(1000);
					return false;
				}
			}
		 });
		return false;
	});
	
	$('.save_realestateconfiguration').click(function(){
		var penaltyMonths = $.trim($('#penaltyMonths').val());
		var releaseNoticePeriod = $.trim($('#releaseNoticePeriod').val());
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_realestate_configurations.action",
			data:{ penaltyMonths: penaltyMonths, releaseNoticePeriod: releaseNoticePeriod},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				var message=response.redirectValue; 
				if(message=="Success"){  
					 $.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_configuration_setup.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){  
							$("#DOMWindow").html(result);    
							$('#setup_success_message').hide().html("Configuration updated.").slideDown(1000); 
							$('#setup_success_message').delay(2000).slideUp();
							return false;
						} 		
					});  
				 }
				else{
					$('#setup_error_message').hide().html(message).slideDown(1000);
					$('#setup_error_message').delay(2000).slideUp(1000);
					return false;
				}
			}
		 });
		return false;
	});
	
	$('.save_systemconfiguration').click(function(){
		var systemUrnOption = "hide";
		
		if($('#systemUrnOptionOn').attr('checked') == true)
			systemUrnOption = "show";
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_system_configurations.action",
			data:{systemUrnOption: systemUrnOption},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				var message=response.redirectValue; 
				if(message=="Success"){  
					 $.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_configuration_setup.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){  
							$("#DOMWindow").html(result);    
							$('#setup_success_message').hide().html("Configuration updated.").slideDown(1000); 
							$('#setup_success_message').delay(2000).slideUp();
							return false;
						} 		
					});  
				 }
				else{
					$('#setup_error_message').hide().html(message).slideDown(1000);
					$('#setup_error_message').delay(2000).slideUp(1000);
					return false;
				}
			}
		 });
		return false;
	});
	
	$('.skip_configuration_setup').click(function(){ 
		 $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_account_setup.action",  
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#DOMWindow").html(result);  
			} 		
		 });  
	 });
	
	 $(".discard").click(function(){
 		$.ajax({
			 type:"POST",
			 url: "<%=request.getContextPath()%>/show_setup_wizard.action",   
			 async: false,
			 dataType: "html",
			 cache: false,
			 success:function(result){  
				 $('.formError').remove(); 
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
				 $('#main-wrapper').html(result);  
			 } 
		 });
	 });
});
</script>
<div id="main-content">
	<div class="tempresult" style="display: none;"></div> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Configuration Setup
		</div>
		<div id="setup_success_message"
			class="response-msg success ui-corner-all width90"
			style="display: none;"></div>
		<div id="setup_error_message"
			class="response-msg error ui-corner-all width90"
			style="display: none;"></div>
		<div class="clearfix"></div>
		<div id="tabs"
			class="ui-tabs ui-widget ui-widget-content ui-corner-all width99"
			Style="float: left; *float: none;">
			<ul
				class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
				<li
					class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a
					href="#tabs-1">Project</a></li>
				<li class="ui-state-default ui-corner-top ui-tabs-selected"
					id="accountstab"><a href="#tabs-2">Accounts</a></li>
				<li class="ui-state-default ui-corner-top ui-tabs-selected"
					id="realestatetab"><a href="#tabs-3">RealEstate</a></li>
				<li class="ui-state-default ui-corner-top ui-tabs-selected"
					id="systemtab"><a href="#tabs-4">System</a></li>
			</ul>
			<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
			id="tabs-1">
			<form id="projecttab" name="projecttab" style="position: relative;">
				<div class="width48 float-right" id="hrm"> 
					<fieldset style="min-height: 170px;">
					<div class="width100 float-right" style="margin-bottom: 5px;"> 
						<label class="width30">Payment Terms<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.project_access_paymentTerms ne null && sessionScope.project_access_paymentTerms eq 'show'}">
								<span class="width10 float-left">
									<label class="width60" for="projectPaymentTermsOn">Show</label>
									<input type="radio" id="projectPaymentTermsOn" checked="checked" name="projectPaymentTerms"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="projectPaymentTermsOff">Hide</label>
									<input type="radio" id="projectPaymentTermsOff" name="projectPaymentTerms"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.project_access_paymentTerms ne null && sessionScope.project_access_paymentTerms eq 'hide'}">
								<span class="width10 float-left">
									<label class="width60" for="projectPaymentTermsOn">Show</label>
									<input type="radio" id="projectPaymentTermsOn" name="projectPaymentTerms"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="projectPaymentTermsOff">Hide</label>
									<input type="radio" id="projectPaymentTermsOff" checked="checked" name="projectPaymentTerms"/>
								</span>
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width60" for="projectPaymentTermsOn">Show</label>
									<input type="radio" id="projectPaymentTermsOn" name="projectPaymentTerms"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="projectPaymentTermsOff">Hide</label>
									<input type="radio" id="projectPaymentTermsOff" name="projectPaymentTerms"/>
								</span>
							</c:otherwise>
						</c:choose> 
					</div> 
					<div class="width100 float-right" style="margin-bottom: 5px;"> 
						<label class="width30">Penality Descriptive<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.project_access_penalityDescriptive ne null 
									&& sessionScope.project_access_penalityDescriptive eq 'show'}">
								<span class="width10 float-left">
									<label class="width60" for="penalityDescriptiveOn">Show</label>
									<input type="radio" id="penalityDescriptiveOn" checked="checked" name="penalityDescriptive"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="penalityDescriptiveOff">Hide</label>
									<input type="radio" id="penalityDescriptiveOff" name="penalityDescriptive"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.project_access_penalityDescriptive ne null 
									&& sessionScope.project_access_penalityDescriptive eq 'hide'}">
								<span class="width10 float-left">
									<label class="width60" for="penalityDescriptiveOn">Show</label>
									<input type="radio" id="penalityDescriptiveOn" name="penalityDescriptive"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="penalityDescriptiveOff">Hide</label>
									<input type="radio" id="penalityDescriptiveOff" checked="checked" name="penalityDescriptive"/>
								</span>
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width60" for="penalityDescriptiveOn">Show</label>
									<input type="radio" id="penalityDescriptiveOn" name="penalityDescriptive"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="penalityDescriptiveOff">Hide</label>
									<input type="radio" id="penalityDescriptiveOff" name="penalityDescriptive"/>
								</span>
							</c:otherwise>
						</c:choose> 
					</div> 
					<div class="width100 float-right" style="margin-bottom: 5px;"> 
						<label class="width30">Contract Terms<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.project_access_contractTerms ne null 
									&& sessionScope.project_access_contractTerms eq 'show'}">
								<span class="width10 float-left">
									<label class="width60" for="contractTermsOn">Show</label>
									<input type="radio" id="contractTermsOn" checked="checked" name="contractTerms"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="contractTermsOff">Hide</label>
									<input type="radio" id="contractTermsOff" name="contractTerms"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.project_access_contractTerms ne null 
									&& sessionScope.project_access_contractTerms eq 'hide'}">
								<span class="width10 float-left">
									<label class="width60" for="contractTermsOn">Show</label>
									<input type="radio" id="contractTermsOn" name="contractTerms"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="contractTermsOff">Hide</label>
									<input type="radio" id="contractTermsOff" checked="checked" name="contractTerms"/>
								</span>
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width60" for="contractTermsOn">Show</label>
									<input type="radio" id="contractTermsOn"  name="contractTerms"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="contractTermsOff">Hide</label>
									<input type="radio" id="contractTermsOff" name="contractTerms"/>
								</span>
							</c:otherwise>
						</c:choose> 
					</div> 
					<div class="width100 float-right" style="margin-bottom: 5px;"> 
						<label class="width30">Simple Save Button<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.project_simple_save_button ne null 
									&& sessionScope.project_simple_save_button eq 'show'}">
								<span class="width10 float-left">
									<label class="width60" for="simpleSaveButtonOn">Show</label>
									<input type="radio" id="simpleSaveButtonOn" checked="checked" name="simpleSaveButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="simpleSaveButtonOff">Hide</label>
									<input type="radio" id="simpleSaveButtonOff" name="simpleSaveButton"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.project_simple_save_button ne null 
									&& sessionScope.project_simple_save_button eq 'hide'}">
								<span class="width10 float-left">
									<label class="width60" for="simpleSaveButtonOn">Show</label>
									<input type="radio" id="simpleSaveButtonOn" name="simpleSaveButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="simpleSaveButtonOff">Hide</label>
									<input type="radio" id="simpleSaveButtonOff" checked="checked" name="simpleSaveButton"/>
								</span>
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width60" for="simpleSaveButtonOn">Show</label>
									<input type="radio" id="simpleSaveButtonOn" name="simpleSaveButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="simpleSaveButtonOff">Hide</label>
									<input type="radio" id="simpleSaveButtonOff" name="simpleSaveButton"/>
								</span>
							</c:otherwise>
						</c:choose> 
					</div> 
					<div class="width100 float-right" style="margin-bottom: 5px;"> 
						<label class="width30">Update &amp; Save<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.project_update_and_save_button ne null 
									&& sessionScope.project_update_and_save_button eq 'show'}">
								<span class="width10 float-left">
									<label class="width60" for="updateSaveButtonOn">Show</label>
									<input type="radio" id="updateSaveButtonOn" checked="checked" name="updateSaveButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="updateSaveButtonOff">Hide</label>
									<input type="radio" id="updateSaveButtonOff" name="updateSaveButton"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.project_update_and_save_button ne null 
									&& sessionScope.project_update_and_save_button eq 'hide'}">
								<span class="width10 float-left">
									<label class="width60" for="updateSaveButtonOn">Show</label>
									<input type="radio" id="updateSaveButtonOn" name="updateSaveButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="updateSaveButtonOff">Hide</label>
									<input type="radio" id="updateSaveButtonOff" checked="checked" name="updateSaveButton"/>
								</span>
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width60" for="updateSaveButtonOn">Show</label>
									<input type="radio" id="updateSaveButtonOn" name="updateSaveButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="updateSaveButtonOff">Hide</label>
									<input type="radio" id="updateSaveButtonOff" name="updateSaveButton"/>
								</span>
							</c:otherwise>
						</c:choose> 
					</div>
					<div class="width100 float-right" style="margin-bottom: 5px;"> 
						<label class="width30">Material Requisition<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.project_send_requisition_button ne null 
									&& sessionScope.project_send_requisition_button eq 'show'}">
								<span class="width10 float-left">
									<label class="width60" for="sendRequisitionButtonOn">Show</label>
									<input type="radio" id="sendRequisitionButtonOn" checked="checked" name="sendRequisitionButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="sendRequisitionButtonOff">Hide</label>
									<input type="radio" id="sendRequisitionButtonOff" name="sendRequisitionButton"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.project_send_requisition_button ne null 
									&& sessionScope.project_send_requisition_button eq 'hide'}">
								<span class="width10 float-left">
									<label class="width60" for="sendRequisitionButtonOn">Show</label>
									<input type="radio" id="sendRequisitionButtonOn" name="sendRequisitionButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="sendRequisitionButtonOff">Hide</label>
									<input type="radio" id="sendRequisitionButtonOff" checked="checked" name="sendRequisitionButton"/>
								</span>
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width60" for="sendRequisitionButtonOn">Show</label>
									<input type="radio" id="sendRequisitionButtonOn" name="sendRequisitionButton"/>
								</span>
								<span class="width10 float-left">
									<label class="width60" for="sendRequisitionButtonOff">Hide</label>
									<input type="radio" id="sendRequisitionButtonOff" name="sendRequisitionButton"/>
								</span>
							</c:otherwise>
						</c:choose> 
					</div> 
					<div class="width100 float-right"> 
						<label class="width30">Access Area<span style="color: red;">*</span></label>
						<div class="width70 float-right">
							<c:set var="accessRow" value="0" />  
							<c:forEach begin="0" step="2" items="${PROJECT_ACCESS_AREAS}">
								<div class="width50 float-left">
									<span class="width100 float-left" style="margin-bottom: 5px;">  
										<c:choose>
											<c:when test="${ ! empty PROJECT_ACCESS_AREAMAP[PROJECT_ACCESS_AREAS[accessRow+0].key]}">
												<input name="${PROJECT_ACCESS_AREAS[accessRow+0].key}" id="${PROJECT_ACCESS_AREAS[accessRow+0].key}" 
									 	 	 		class="float-left projectAreaAccess" style="margin-top:4px!important;" checked="checked" type="checkbox"/>
											</c:when>
											<c:otherwise>
												<input name="${PROJECT_ACCESS_AREAS[accessRow+0].key}" id="${PROJECT_ACCESS_AREAS[accessRow+0].key}" 
									 	 	 			class="float-left projectAreaAccess" style="margin-top:4px!important;" type="checkbox"/>
											</c:otherwise>
										</c:choose> 
								     <label class="width60" 
								     	 for="${PROJECT_ACCESS_AREAS[accessRow+0].key}">${PROJECT_ACCESS_AREAS[accessRow+0].value}</label>
								    </span> 
								 </div> 
							    <c:if test="${PROJECT_ACCESS_AREAS[accessRow+1]!= null}">
							     <div class="width48 float-left"> 
							    	<span class="width100 float-left" style="margin-bottom: 5px;">
							    		<c:choose>
											<c:when test="${ ! empty PROJECT_ACCESS_AREAMAP[PROJECT_ACCESS_AREAS[accessRow+1].key]}">
												<input name="${PROJECT_ACCESS_AREAS[accessRow+1].key}" id="${PROJECT_ACCESS_AREAS[accessRow+1].key}" 
									 	 	 		class="float-left projectAreaAccess" style="margin-top:4px!important;" checked="checked" type="checkbox"/>
											</c:when>
											<c:otherwise>
												<input name="${PROJECT_ACCESS_AREAS[accessRow+1].key}" id="${PROJECT_ACCESS_AREAS[accessRow+1].key}" 
									 	 	 		class="float-left projectAreaAccess" style="margin-top:4px!important;" type="checkbox"/>
											</c:otherwise>
										</c:choose>  
							     	 	<label class="width60" 
							     	 		for="${PROJECT_ACCESS_AREAS[accessRow+1].key}">${PROJECT_ACCESS_AREAS[accessRow+1].value}</label>
							    	</span> 
							    	</div>
							    </c:if>
							    <c:set var="accessRow" value="${accessRow + 2}" />
							    
							</c:forEach> 
						</div> 
					</div>	
					</fieldset>
				</div> 
 				<div class="width50 float-left" id="hrm"> 
 					<fieldset style="min-height: 170px;">
					<div> 
						<label class="width30">Project Title<span style="color: red;">*</span></label>
						<input type="text" id="projectTitle" class="width40" value="${sessionScope.project_projectTitle}"/>
					</div> 
					<div> 
						<label class="width30">Reference Number<span style="color: red;">*</span></label>
						<input type="text" id="projectReference" class="width40" value="${sessionScope.project_reference}"/>
					</div> 
					<div> 
						<label class="width30">Customer<span style="color: red;">*</span></label>
						<input type="text" id="projectCustomer" class="width40" value="${sessionScope.project_client}"/>
					</div> 
					<div> 
						<label class="width30">Created By<span style="color: red;">*</span></label>
						<input type="text" id="projectCreatedBy" class="width40" value="${sessionScope.project_createdby}"/>
					</div> 
					<div> 
						<label class="width30">Project Lead<span style="color: red;">*</span></label>
						<input type="text" id="projectLead" class="width40" value="${sessionScope.project_lead}"/>
					</div> 
					<div> 
						<label class="width30">Project Manager<span style="color: red;">*</span></label>
						<input type="text" id="projectManager" class="width40" value="${sessionScope.project_manager}"/>
					</div> 
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard" ><fmt:message key="accounts.common.button.cancel"/></div> 
						<div class="portlet-header ui-widget-header float-right save_projectconfiguration"><fmt:message key="accounts.common.button.save"/></div>
					</div>  
				</div>
			</form>
		</div>
		
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
			id="tabs-2">
			<form id="accountstab" name="accountstab" style="position: relative;">
				<div class="width50 float-right" id="hrm"> 
					<fieldset style="min-height: 80px;">
						<div class="width100 float-right" style="margin-bottom: 5px;"> 
							<label class="width30">Print Order Button<span style="color: red;">*</span></label>
							<c:choose>
								<c:when test="${sessionScope.pos_printorder_button ne null && sessionScope.pos_printorder_button eq 'show'}">
									<span class="width10 float-left">
										<label class="width60" for="posPrintOrderButtonOn">Show</label>
										<input type="radio" id="posPrintOrderButtonOn" checked="checked" name="posPrintOrderButton"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="posPrintOrderButtonOff">Hide</label>
										<input type="radio" id="posPrintOrderButtonOff" name="posPrintOrderButton"/>
									</span>
								</c:when>
								<c:when test="${sessionScope.pos_printorder_button ne null && sessionScope.pos_printorder_button eq 'hide'}">
									<span class="width10 float-left">
										<label class="width60" for="posPrintOrderButtonOn">Show</label>
										<input type="radio" id="posPrintOrderButtonOn" name="posPrintOrderButton"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="posPrintOrderButtonOff">Hide</label>
										<input type="radio" id="posPrintOrderButtonOff" checked="checked" name="posPrintOrderButton"/>
									</span>
								</c:when>
								<c:otherwise>
									<span class="width10 float-left">
										<label class="width60" for="posPrintOrderButtonOn">Show</label>
										<input type="radio" id="posPrintOrderButtonOn" name="posPrintOrderButton"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="posPrintOrderButtonOff">Hide</label>
										<input type="radio" id="posPrintOrderButtonOff" name="posPrintOrderButton"/>
									</span>
								</c:otherwise>
							</c:choose> 
						</div> 
						<div class="width100 float-right" style="margin-bottom: 5px;"> 
							<label class="width30">Save Order Button<span style="color: red;">*</span></label>
							<c:choose>
								<c:when test="${sessionScope.pos_saveorder_button ne null && sessionScope.pos_saveorder_button eq 'show'}">
									<span class="width10 float-left">
										<label class="width60" for="posSaveOrderButtonOn">Show</label>
										<input type="radio" id="posSaveOrderButtonOn" checked="checked" name="posSaveOrderButton"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="posSaveOrderButtonOff">Hide</label>
										<input type="radio" id="posSaveOrderButtonOff" name="posSaveOrderButton"/>
									</span>
								</c:when>
								<c:when test="${sessionScope.pos_saveorder_button ne null && sessionScope.pos_saveorder_button eq 'hide'}">
									<span class="width10 float-left">
										<label class="width60" for="posSaveOrderButtonOn">Show</label>
										<input type="radio" id="posSaveOrderButtonOn" name="posSaveOrderButton"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="posSaveOrderButtonOff">Hide</label>
										<input type="radio" id="posSaveOrderButtonOff" checked="checked" name="posSaveOrderButton"/>
									</span>
								</c:when>
								<c:otherwise>
									<span class="width10 float-left">
										<label class="width60" for="posSaveOrderButtonOn">Show</label>
										<input type="radio" id="posSaveOrderButtonOn" checked="checked" name="posSaveOrderButton"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="posSaveOrderButtonOff">Hide</label>
										<input type="radio" id="posSaveOrderButtonOff" name="posSaveOrderButton"/>
									</span>
								</c:otherwise>
							</c:choose> 
						</div> 
					</fieldset>
				</div>
 				<div class="width50 float-left" id="hrm"> 
 					<fieldset style="min-height: 80px;">
 					<div> 
						<label class="width30">PDC Alert Days<span style="color: red;">*</span></label>
						<input type="text" id="pdcAlertDays" class="width40" value="${sessionScope.pdc_alert_days}"/>
					</div>
					<div> 
						<label class="width30">POS PrintOrder<span style="color: red;">*</span></label>
						<input type="text" id="posPrintOrder" class="width40" value="${sessionScope.pos_printorder}"/>
					</div> 
					<div> 
						<label class="width30">POS Next<span style="color: red;">*</span></label>
						<input type="text" id="posNextInvoice" class="width40" value="${sessionScope.pos_movetoinvoice}"/>
					</div> 
					<div> 
						<label class="width30">Unified Price<span style="color: red;">*</span></label>
						<c:choose>
							<c:when test="${sessionScope.unified_price ne null && sessionScope.unified_price eq 'true'}">
								<span class="width10 float-left">
									<label class="width40" for="productUnifiedPriceOn">True</label>
									<input type="radio" id="productUnifiedPriceOn" checked="checked" name="productUnifiedPrice"/>
								</span>
								<span class="width10 float-left">
									<label class="width40" for="productUnifiedPriceOff">False</label>
									<input type="radio" id="productUnifiedPriceOff" name="productUnifiedPrice"/>
								</span>
							</c:when>
							<c:when test="${sessionScope.unified_price ne null && sessionScope.unified_price eq 'false'}">
								<span class="width10 float-left">
									<label class="width40" for="productUnifiedPriceOn">True</label>
									<input type="radio" id="productUnifiedPriceOn" name="productUnifiedPrice"/>
								</span>
								<span class="width10 float-left">
									<label class="width40" for="productUnifiedPriceOff">False</label>
									<input type="radio" id="productUnifiedPriceOff" name="productUnifiedPrice" checked="checked"/>
								</span> 
							</c:when>
							<c:otherwise>
								<span class="width10 float-left">
									<label class="width40" for="productUnifiedPriceOn">True</label>
									<input type="radio" id="productUnifiedPriceOn" class="width40"  name="productUnifiedPrice"/>
								</span>
								<span class="width10">
									<label class="width40" for="productUnifiedPriceOff">False</label>
									<input type="radio" id="productUnifiedPriceOff" class="width40" name="productUnifiedPrice"/>
								</span> 
							</c:otherwise>
						</c:choose>  
					</div>  
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard" ><fmt:message key="accounts.common.button.cancel"/></div> 
						<div class="portlet-header ui-widget-header float-right save_accountsconfiguration"><fmt:message key="accounts.common.button.save"/></div>
					</div>  
				</div>
			</form>
		</div>
		
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
			id="tabs-3">
			<form id="realestatetab" name="realestatetab" style="position: relative;">
 				<div class="width50" id="hrm"> 
 					<fieldset style="min-height: 60px;">
					<div> 
						<label class="width30">Contract Penalty Months<span style="color: red;">*</span></label>
						<input type="text" id="penaltyMonths" class="width40" value="${sessionScope.penalty_months}"/>
					</div> 
					<div> 
						<label class="width30">Release Notice Period<span style="color: red;">*</span></label>
						<input type="text" id="releaseNoticePeriod" class="width40" value="${sessionScope.release_notice_period}"/>
					</div> 
					</fieldset> 
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard" ><fmt:message key="accounts.common.button.cancel"/></div> 
						<div class="portlet-header ui-widget-header float-right save_realestateconfiguration"><fmt:message key="accounts.common.button.save"/></div>
					</div>  
				</div>
			</form>
		</div>
		
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
			id="tabs-4">
			<form id="realestatetab" name="realestatetab" style="position: relative;">
 				<div class="width50" id="hrm"> 
 					<fieldset style="min-height: 50px;">
 						<div class="width100 float-right" style="margin-bottom: 5px;display:none;"> 
 							<input type="text" id="image_slider_access_area" name="image_slider_access_area" value="${sessionScope.image_slider_access_area}">
						</div>
						<div class="width100 float-right" style="margin-bottom: 5px;"> 
							<label class="width30">URN Screen<span style="color: red;">*</span></label>
							<c:choose>
								<c:when test="${sessionScope.system_urn_option ne null && sessionScope.system_urn_option eq 'show'}">
									<span class="width10 float-left">
										<label class="width60" for="systemUrnOptionOn">Show</label>
										<input type="radio" id="systemUrnOptionOn" checked="checked" name="systemUrnOption"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="systemUrnOptionOff">Hide</label>
										<input type="radio" id="systemUrnOptionOff" name="systemUrnOption"/>
									</span>
								</c:when>
								<c:when test="${sessionScope.system_urn_option ne null && sessionScope.system_urn_option eq 'hide'}">
									 <span class="width10 float-left">
										<label class="width60" for="systemUrnOptionOn">Show</label>
										<input type="radio" id="systemUrnOptionOn" name="systemUrnOption"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="systemUrnOptionOff">Hide</label>
										<input type="radio" id="systemUrnOptionOff" checked="checked" name="systemUrnOption"/>
									</span>
								</c:when>
								<c:otherwise>
									 <span class="width10 float-left">
										<label class="width60" for="systemUrnOptionOn">Show</label>
										<input type="radio" id="systemUrnOptionOn" name="systemUrnOption"/>
									</span>
									<span class="width10 float-left">
										<label class="width60" for="systemUrnOptionOff">Hide</label>
										<input type="radio" id="systemUrnOptionOff" name="systemUrnOption"/>
									</span>
								</c:otherwise>
							</c:choose> 
						</div> 
					</fieldset> 
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard" ><fmt:message key="accounts.common.button.cancel"/></div> 
						<div class="portlet-header ui-widget-header float-right save_systemconfiguration"><fmt:message key="accounts.common.button.save"/></div>
					</div>  
				</div>
			</form>
		</div>
		</div>
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
		<div class="portlet-header ui-widget-header float-left skip_configuration_setup" style="cursor:pointer;">skip</div> 
	</div>
</div>