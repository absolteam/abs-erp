<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
<c:choose>
	<c:when test="${cityList != null }">
		<c:forEach items="${cityList}" var="cityBean" varStatus="status" >
		<option id="${cityBean.cityId}" value="${cityBean.cityId}">${cityBean.cityCode} - ${cityBean.cityName}</option>
	</c:forEach> 
	</c:when>
</c:choose>

<script type="text/javascript">
$('#city').change(function(){

	//if(editing == "true")
	//	return;
	
	cityId=$('#city').val();
	try {
		temp = $('#' + cityId).text().split('/')[1];
	} catch (e) {
		temp = "";
	}	
	var cityCode = trimSpaces($('#' + cityId).text().split(' - ')[0]);
	var cityCodeArabic = temp;
	autoPropertyName[2] = cityCode;
	autoPropertyNameArabic[2] = cityCodeArabic;
	$('#propertyCode').val(autoPropertyName.join("-"));
	$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));
	if(propertyNameProvided == false)
		$('#buildingName').val($('#propertyCode').val());
});
</script>