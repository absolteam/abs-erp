 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>   
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 <style>
#common-popup{
	overflow: hidden;
}
 td .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; right:3px;}
	td  .ui-button { margin: 0px; height:24px;width:2%; position: absolute; }
	.ui-autocomplete{
		width:194px!important;
	} 
</style>
<script type="text/javascript">
var slidetab="";
var workflowLineDetail="";

$(function(){
	
	//Edit process
	var workflowId=Number($('#workflowId').val());
	if(workflowId>0){
		
		$('#moduleProcess').val($('#moduleProcessTemp').val());
		$('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   			var operationId=Number($("#operationTemp_"+rowId).val());
   			var operation=0;
   		 	$("#operationAutoSelect option").each(function(){
   		 		if(operationId==$(this).val())
   		 			operation=$(this).text();
   		 	});
   		 	if(operation!=0)
   		 	$("#operation_"+rowId).text(operation);
   		 	
   			var accessRightsTypeId=Number($("#accessRightsTypeTemp_"+rowId).val());
			var accessRightsType=0;
   		 	$("#accessRightsTypeAutoSelect option").each(function(){
   		 		if(accessRightsTypeId==$(this).val())
   		 			accessRightsType=$(this).text();
		 	});
   		 	if(accessRightsType!=0)
   			 $("#accessRightsType_"+rowId).text(accessRightsType);
		 }); 
		
	}
	
	manupulateLastRow();
	 $("#workflowValidation").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction();}
	 	});  
	 	
	
	 $(".addData").click(function(){ 
		 if($("#workflowValidation").validationEngine({returnIsValid:true})) {

			 if($('#openFlag').val() == 0) {

			slidetab=$(this).parent().parent().get(0);  
			
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/workflow_detail_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			
			 }else{
				return false;
			 }
		 }
		 else{
			 return false;
		 }	
		 return false;
	});
	
	$(".editData").click(function(){
		 if($('#openFlag').val() == 0) { 
		 slidetab=$(this).parent().parent().get(0);  
		 var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		 if($("#workflowValidation").validationEngine({returnIsValid:true})) {
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/workflow_detail_add.action",
				 data:{id:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
	   		
        			$('#order').val($('#order_'+rowid).text()); 
        			var isDays=$('#isDays_'+rowid).text(); 
        			if(isDays=="Days"){
        				$('#isDays').attr("checked","checked");
        			}else if(isDays=="Hours"){
        				$('#isHours').attr("checked","checked");
        			}
        			$('#validity').val($('#validity_'+rowid).text());
        			$('#reminder').val($('#reminder_'+rowid).text());
        			$('#escalation').val($('#escalation_'+rowid).text());
        			
        			
        			var isSms=$('#isSms_'+rowid).text();
        			var isEmail=$('#isEmail_'+rowid).text();
        			var isSystem=$('#isSystem_'+rowid).text();
        			if(isSms=='YES'){
        				$('#isSms').attr("checked","checked");
        			}
        			if(isEmail=='YES'){
        				$('#isEmail').attr("checked","checked");
        			}
        			if(isSystem=='YES'){
        				$('#isSystem').attr("checked","checked");
        			}
        			$('#notification').val($('#notificationTemp_'+rowid).val());
        			//$('#notification').attr('selected', $('#notificationTemp_'+rowid).val()); 
        			$('#operation').val($('#operationTemp_'+rowid).val()); 
        			$('#screen').val($('#screenTemp_'+rowid).val());

					//$('#screen').combobox('autocomplete', $('#screen_'+rowid).text());
        			$('#accessRightsType').val($('#accessRightsTypeTemp_'+rowid).val());
        			$('#notifyTo').val($('#notifyToTemp_'+rowid).val());
        			//$('#notifyTo').combobox('autocomplete', $('#notifyTo_'+rowid).text());
        			
        			rights=$('#accessRightsTypeTemp_'+rowid).val();
        			if(rights==1){
        				$('#accessRightLable').hide();
        				$('#RoleSpan').hide();
        				$('#UserRoleSpan').show();
            			$('#userRole').val($('#userRoleTemp_'+rowid).val());
        				//$('#userRole').combobox('autocomplete', $('#userRole_'+rowid).text());
        			}else if(rights==2){
        				$('#accessRightLable').hide();
        				$('#RoleSpan').show();
        				$('#UserRoleSpan').hide();
        				$('#role').val($('#roleTemp_'+rowid).val());
        				//$('#role').combobox('autocomplete', $('#userRole_'+rowid).text());
        			}else{
        				$('#accessRightLable').show();
        				$('#RoleSpan').hide();
        				$('#UserRoleSpan').hide();
        			}
        			//accessRightsCall($('#accessRightsTypeTemp_'+rowid).val(),rowid);
        			if($('#isDays').attr("checked") || $('#isHours').attr("checked"))
							$('#showNotification').show();
					applyAutoComplete();
			    $('#openFlag').val(1);
			   
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			}
					
		 }else{
			 return false;
		 }
		 return false;
	});
	 $(".delrow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 var lineId=Number($('#lineId_'+rowid).val());
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/save_workflow_detail.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
					   $('.tempresult').html(result); 
						var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
						 	$("#fieldrow_"+rowid).remove(); 
			        		//To reset sequence 
			        		var i=0;
			     			$('.rowid').each(function(){  
								$('#lineId_'+rowId).val(i);
								i=i+1;  
		   					 }); 
						 }else{
							 
							 $('#page-error').hide().html("Delete Failure!").slideDown(1000);
						 }
					},
					error:function(result){
						 return false;
					}
				 });
				 return false; 
	 });
	$('.addrows').live('click',function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1; 
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/workflow_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).val(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	
	
	//Save & discard process
	$('.save').click(function(){ 
		if($("#JOBDetails").validationEngine({returnIsValid:true})){  
			loanLineDetail=""; loanChargesDetail="";
			var workflowId=Number($('#workflowId').val());
			var moduleProcessId=$('#moduleProcess').val();
			var workflowTitle=$('#name').val();
			
			workflowLineDetail=getWorkflowDetails(); 
			//alert(workflowLineDetail);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/workflow_save.action", 
			 	async: false, 
			 	data:{ 
			 		workflowId:workflowId,moduleProcessId:moduleProcessId,workflowTitle:workflowTitle,
			 		workflowLineDetail:workflowLineDetail
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/workflow_management.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(message).slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
		return false;
	});
	
	$('.discard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/workflow_management.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
		 return false;
	});
	
});
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function triggerAddRow(rowId){  
	var notification=$('#notification_'+rowId).val(); 
	var operation=$('#operation_'+rowId).val(); 
	var accessRightsType=$('#accessRightsType_'+rowId).val();
	var order=$('#order_'+rowId).val();
	
	var nexttab=$('#fieldrow_'+rowId).next(); 
	if(notification!=null && notification!="" 
			&& operation!=null && operation!=""
			&& order!=null && order!=""
			&& accessRightsType!=null && accessRightsType!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
var getWorkflowDetails=function(){
	workflowLineDetail="";
	var workflowDetailIds=new Array();
	var notificationIds=new Array();
	var screenIds=new Array();
	var operationIds=new Array();
	var accessRightsIds=new Array();
	var orders=new Array();
	var userOrRoleIds=new Array();
	$('.rowid').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var notification=$('#notification_'+rowId).val();
		if(notification!=null && notification!=""){
			var workflowDetailId=$('#workflowDetailId_'+rowId).val();
			if(workflowDetailId==null || workflowDetailId=='' || workflowDetailId==0 || workflowDetailId=='undefined')
				workflowDetailId=-1;
			var screen=$('#screen_'+rowId).val();
			if(screen==null || screen=='' || screen==0 || screen=='undefined')
				screen=-1;
			var operation=$('#operation_'+rowId).val();
			var accessRightsType=$('#accessRightsType_'+rowId).val();
			var order=$('#order_'+rowId).val();
			var userOrRoleId="";
			
			if(accessRightsType==1){
				 userOrRoleId=$('#userRole_'+rowId).val();
			}else if(accessRightsType==2){
				userOrRoleId=$('#role_'+rowId).val();
			}
			if(userOrRoleId==null || userOrRoleId=='' || userOrRoleId==0 || userOrRoleId=='undefined')
				userOrRoleId=-1;
			if(notification!=null && notification!="" && operation!=null && operation!=""){
				workflowDetailIds.push(workflowDetailId);
				notificationIds.push(notification);
				operationIds.push(operation);
				screenIds.push(screen);
				orders.push(order);
				accessRightsIds.push(accessRightsType);
				userOrRoleIds.push(userOrRoleId);
	
			}
		}
	});
	for(var j=0;j<notificationIds.length;j++){ 
		workflowLineDetail+=workflowDetailIds[j]+"@"+notificationIds[j]
		+"@"+operationIds[j]+"@"+screenIds[j]+"@"+orders[j]
		+"@"+accessRightsIds[j]+"@"+userOrRoleIds[j];
		if(j==notificationIds.length-1){   
		} 
		else{
			workflowLineDetail+="##";
		}
	} 
	return workflowLineDetail;
};
</script>
 <div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="systme.workflow.workflowentry"/></div>	
		  <form name="workflowform" id="workflowValidation"> 
			<div class="portlet-content">  
				<div id="workflow-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="workflowId" name="workflowId" value="${WORKFLOW_INFO.workflowId}"/>
			  	<input type="hidden" name="trnValue" value="" readonly="readonly" id="transURN" /> 
				<input type="hidden" name="openFlag" id="openFlag" value="0" />
				<div class="width100 float-left" id="hrm"> 
				<div class="float-left width100">
					<fieldset>
						<legend><fmt:message key="systme.workflow.workflow"/></legend>  
						<div class="width45 float-left">
							<div>
								<label class="width30" for="moduleProcess"> 
								<fmt:message key="systme.workflow.moduleprocess"/><span style="color:red">*</span></label>
								 <select name="moduleProcess" id="moduleProcess" class="validate[required] width50">
								 	<option value="">Select</option>
								 	<c:choose>
								 		<c:when test="${MODULE_PROCESS ne null && MODULE_PROCESS ne '' && fn:length(MODULE_PROCESS)>0}">
								 			<c:forEach var="module" items="${MODULE_PROCESS}">
								 				<option value="${module.processId}">${module.processTitle}</option>
								 			</c:forEach>
								 		</c:when>
								 	</c:choose>
								 </select>
								 <input type="hidden" name="moduleProcessTemp" id="moduleProcessTemp" value="${WORKFLOW_INFO.moduleProcess.processId}"/>
							</div> 
							
						</div>
						<div class="width45 float-left">
							<div>
								<label class="width20" for="role"> 
								<fmt:message key="systme.workflow.title"/><span style="color:red">*</span></label>
								 <input type="text" class="validate[required] width50" name="name" id="name" value="${WORKFLOW_INFO.workflowTitle}"/>
							</div>
						</div>
						<%--  <div>
							<label class="width30" for="role"> 
							Roles</label>
							  <select name="role" id="role" class="validate[required] width50">
							 	<option value="">Select</option>
							 	<c:choose>
							 		<c:when test="${ROLE_DETAIL ne null && ROLE_DETAIL ne '' && fn:length(ROLE_DETAIL)>0}">
							 			<c:forEach var="role" items="${ROLE_DETAIL}">
							 				<option value="${role.roleId}">${role.roleName}</option>
							 			</c:forEach>
							 		</c:when>
							 	</c:choose>
							 </select>
						</div>  --%>
					</fieldset>
				</div>
			 </div>
			<div class="clearfix"></div>   
			<div id="hrm" class="portlet-content width100" style="margin-top:10px;"> 
				<fieldset id="hrm">
					<legend><fmt:message key="systme.workflow.notificationdetails"/></legend> 
		 			<div class="portlet-content"> 
			 			<div  id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			 			<div id="warning_message" class="response-msg notice ui-corner-all width90" style="display:none;"></div>
							<div id="hrm" class="hastable width100"  > 
								 <input type="hidden" name="childCount" id="childCount" value="${fn:length(RECEIVE_LINE_INFO)}"/>  
								 <table id="hastab" class="width100"> 
									<thead>
									   <tr>  
									   		<th style="width:10%"><fmt:message key="systme.workflow.notification"/></th> 
										    <th style="width:5%"><fmt:message key="systme.workflow.operation"/></th>
										   	<th style="width:10%"><fmt:message key="systme.workflow.nextprocess"/></th> 
										    <th style="width:5%"><fmt:message key="systme.workflow.accessrights"/></th> 
										    <th style="width:10%"><fmt:message key="systme.workflow.user/role"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.order"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.day/hours"/></th>
										    <th style="width:5%;display:none;">Validity</th>
										    <th style="width:5%"><fmt:message key="systme.workflow.reminder"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.escalation"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.escalateto"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.sms"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.email"/></th>
										    <th style="width:5%"><fmt:message key="systme.workflow.system"/></th>
										    <th style="width:10%"><fmt:message key="systme.workflow.options"/></th>
									  </tr>
									</thead> 
									<tbody class="tab"> 
										<c:choose>
											<c:when test="${WORKFLOW_DETAIL_LIST ne null && WORKFLOW_DETAIL_LIST ne '' && fn:length(WORKFLOW_DETAIL_LIST)>0}">
												<c:forEach var="workflowDet" items="${WORKFLOW_DETAIL_LIST}" varStatus="status1">
													<tr class="rowid" id="fieldrow_${status1.index+1}">
														<td style="display:none;">
															<input type="hidden" name="lineId" class="width90 lineId" 
																id="lineId_${status1.index+1}" value="${status1.index+1}"/>   
															<input type="hidden" name="workflowDetailId" class="width90 workflowDetailId" 
																id="workflowDetailId_${status1.index+1}" value="${workflowDet.workflowDetailId}"/>   
															<input type="hidden" name="notificationTemp" class="width90 notificationTemp" 
																id="notificationTemp_${status1.index+1}" value="${workflowDet.notification.notificationId}"/>
															<input type="hidden" name="operationTemp" class="width90 operationTemp" 
															id="operationTemp_${status1.index+1}" value="${workflowDet.operation}"/>
															<input type="hidden" name="screenTemp" class="width90 screenTemp" 
															id="screenTemp_${status1.index+1}" value="${workflowDet.screen.screenId}"/>
															<input type="hidden" name="accessRightsTypeTemp" class="width90 accessRightsTypeTemp" 
															id="accessRightsTypeTemp_${status1.index+1}" value="${workflowDet.accessRightType}"/> 
															<c:forEach var="userrolcomb" items="${workflowDet.userRoleCombinations}" varStatus="status">
																<input type="hidden" name="userRoleTemp" class="width90 userRoleTemp" 
																id="userRoleTemp_${status1.index+1}" value="${userrolcomb.userRole.userRoleId}"/>
															</c:forEach>
															<c:forEach var="rolcomb" items="${workflowDet.userRoleCombinations}" varStatus="status">
																<input type="hidden" name="roleTemp" class="width90 roleTemp" 
																id="roleTemp_${status1.index+1}" value="${rolcomb.role.roleId}"/>
															</c:forEach> 
															<input type="hidden" name="notifyToTemp" class="width90 notifyToTemp" 
															id="notifyToTemp_${status1.index+1}" value="${workflowDet.userRole.userRoleId}"/> 
														</td>   
														<td id="notification_${status1.index+1}">${workflowDet.notification.notificationTitle}</td> 
														<td id="operation_${status1.index+1}">${workflowDet.operation}</td>
														<td id="screen_${status1.index+1}">${workflowDet.screen.screenName}</td>   
														<td id="accessRightsType_${status1.index+1}">${workflowDet.accessRightType}</td> 
														<c:choose>
															<c:when test="${workflowDet.accessRightType eq 1}">
																<td id="userRole_${status1.index+1}" >
																	<c:forEach var="userrolcomb" items="${workflowDet.userRoleCombinations}" varStatus="status">
																			${userrolcomb.userRole.user.username} - ${userrolcomb.userRole.role.roleName}
																	</c:forEach>
																</td>  
															</c:when> 
															<c:when test="${workflowDet.accessRightType eq 2}">
																<td id="userRole_${status1.index+1}">
																	<c:forEach var="rolcomb" items="${workflowDet.userRoleCombinations}" varStatus="status">
																			${rolcomb.role.roleName}
																	</c:forEach>
																</td> 
															</c:when>
															<c:otherwise>
																<td id="userRole_${status1.index+1}">-NA-</td>
															</c:otherwise>
														</c:choose>
														<td id="order_${status1.index+1}">${workflowDet.operationOrder}</td>
														<c:choose>
															<c:when test="${workflowDet.isDays eq true}">
																<td id="isDays_${status1.index+1}">Days</td>
															</c:when>
															<c:when test="${workflowDet.isDays eq false}">
																<td id="isDays_${status1.index+1}">Hours</td>
															</c:when>
															<c:otherwise>
																<td id="isDays_${status1.index+1}">-NA-</td>
															</c:otherwise>
														</c:choose>
														<td id="validity_${status1.index+1}" style="display:none;">${workflowDet.validity}</td>
														<td id="reminder_${status1.index+1}">${workflowDet.reminder}</td>
														<td id="escalation_${status1.index+1}">${workflowDet.escalation}</td>
														<td id="notifyTo_${status1.index+1}">${workflowDet.userRole.user.username}</td>
														<c:choose>
															<c:when test="${workflowDet.isSms eq true}">
																<td id="isSms_${status1.index+1}">YES</td>
															</c:when>
															<c:otherwise>
																<td id="isSms_${status1.index+1}">NO</td>
															</c:otherwise>
														</c:choose>
														<c:choose>
															<c:when test="${workflowDet.isEmail eq true}">
																<td id="isEmail_${status1.index+1}">YES</td>
															</c:when>
															<c:otherwise>
																<td id="isEmail_${status1.index+1}">NO</td>
															</c:otherwise>
														</c:choose>
														<c:choose>
															<c:when test="${workflowDet.isSystem eq true}">
																<td id="isSystem_${status1.index+1}">YES</td>
															</c:when>
															<c:otherwise>
																<td id="isSystem_${status1.index+1}">NO</td>
															</c:otherwise>
														</c:choose>
														<td style="width:5%;" class="opn_td" id="option_${status1.index+1}">
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
								 								<span class="ui-icon ui-icon-plus"></span>
														  </a>	
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status1.index+1}" style="cursor:pointer;" title="Edit Record">
																<span class="ui-icon ui-icon-wrench"></span>
														  </a> 
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																<span class="ui-icon ui-icon-circle-close"></span>
														  </a>
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status1.index+1}" style="display:none;" title="Working">
																<span class="processing"></span>
														  </a>
														</td>   
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>
										<c:forEach var="i" begin="${fn:length(WORKFLOW_DETAIL_LIST)+1}" end="${fn:length(WORKFLOW_DETAIL_LIST)+2}" step="1" varStatus="status">  
											<tr class="rowid" id="fieldrow_${i}">
													<td style="display:none;">
														<input type="hidden" name="lineId" class="width90 lineId" 
															id="lineId_${i}" value="${i}"/>   
														<input type="hidden" name="workflowDetailId" class="width90 workflowDetailId" 
															id="workflowDetailId_${i}" value=""/>   
														<input type="hidden" name="notificationTemp" class="width90 notificationTemp" 
															id="notificationTemp_${i}" value=""/>
														<input type="hidden" name="operationTemp" class="width90 operationTemp" 
														id="operationTemp_${i}" value=""/>
														<input type="hidden" name="screenTemp" class="width90 screenTemp" 
														id="screenTemp_${i}" value=""/>
														<input type="hidden" name="accessRightsTypeTemp" class="width90 accessRightsTypeTemp" 
														id="accessRightsTypeTemp_${i}" value=""/> 
														<input type="hidden" name="userRoleTemp" class="width90 userRoleTemp" 
															id="userRoleTemp_${i}" value=""/>
														<input type="hidden" name="roleTemp" class="width90 roleTemp" 
															id="roleTemp_${i}" value=""/>
													</td>   
													<td id="notification_${i}"></td> 
													<td id="operation_${i}"></td>
													<td id="screen_${i}"></td>   
													<td id="accessRightsType_${i}"></td> 
													<td id="userRole_${i}"></td>
													<td id="order_${i}"></td>
													<td id="isDays_${i}"></td>
													<td id="validity_${i}" style="display:none;"></td>
													<td id="reminder_${i}"></td>
													<td id="escalation_${i}"></td>
													<td id="notifyTo_${i}"></td>
													<td id="isSms_${i}"></td>
													<td id="isEmail_${i}"></td>
													<td id="isSystem_${i}"></td>
													<td style="width:5%;" class="opn_td" id="option_${i}">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="display:none;cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
												</td>   
											</tr>
										</c:forEach>
							 		</tbody>
								</table>
						</div> 
				</div>   
				</fieldset>
			</div>
		</div>  
		<div class="clearfix"></div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
			<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
			<div class="portlet-header ui-widget-header float-right save" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div> 
		<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
		</div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>  
		
		<select name="operationAutoSelect" class="width60" id="operationAutoSelect" style="display: none;">
					<option value="0">Entry</option>
					<option value="3">Approval</option> 
					<option value="1">Approved</option> 
					<option value="2">DisApproved</option> 
		</select> 
		<select name="accessRightsTypeAutoSelect" class="width60" id="accessRightsTypeAutoSelect" style="display: none;">
					<option value="1">User</option> 
					<option value="2">Role</option> 
					<option value="3">Automatic</option>
		</select>
  	</form>
  </div> 
</div>