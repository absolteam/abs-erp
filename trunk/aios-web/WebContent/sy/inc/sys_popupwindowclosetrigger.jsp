<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui/flick/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.DOMWindow.js"></script> 
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
<link href="${pageContext.request.contextPath}/css/reset.css" rel="stylesheet"  type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/forms.css" rel="stylesheet"  type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/tables.css" rel="stylesheet"  type="text/css"  media="all" />
<link href="${pageContext.request.contextPath}/css/messages.css" rel="stylesheet" type="text/css"  media="all" /> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/HRM.css" type="text/css" media="all" /> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>


<%!
	HttpSession session = ServletActionContext.getRequest()
	.getSession();
	Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
%>
<script type="text/javascript">
$(document).ready(function(){
	
	var pFlag=Number($("#processFlag").val()); 
	alert(pFlag);
	if(pFlag==2){  
		
		$("#reject_comment").val("");
		$('.callJq').openDOMWindow({   
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#workflowreject_comment', 
			loaderHeight:16, 
			loaderWidth:17 
		}); 
	}else{
		closeWindows();
	}
	$("#save_comment").click(function(){
		saveComment();//First Call
	});
	$("#close_comment").click(function(){
		self.close();
	});
	$("#DOMWindowOverlay").click(function(){
		self.close();
	});
	
});
function closeWindows(){
	if (window.opener && !window.opener.closed) {
	      window.opener.location.reload();
	      self.close();

	 } 
}
function saveComment(){
	
	var comment=$("#reject_comment").val();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/comment_workflow_reject_save.action",
		data:{comment:comment},
	 	async: false,
	    dataType: "html",
	    cache: false,
	    success:function(result){
	    	$.ajax({
	    		type:"POST",
	    		url:"<%=request.getContextPath()%>/workflowProcess.action",
	    		data:{processFlag:2,finalyzed:1},
	    	 	async: false,
	    	    dataType: "html",
	    	    cache: false,
	    	    success:function(result){
	    	    	//$('#DOMWindowOverlay').trigger('click');
	    	    	 //window.location.reload();
	    	    	closeWindows();

	    	    }
	    	});

	    }
	});
}
</script>
</head>
<div class="mainBody">
	<div id="page-wrapper">
		<div id="main-wrapper">
			<div id="main-content">
				<input type="hidden" id="processFlag" name="processFlag" value="${PROCESS_FLAG }"/>
				<div id="workflowreject_comment" style="display: none;"> 
					<div class="width100 float-left" id="hrm">
						<label class="width30">Comments/Remarks</label>
						<textarea rows="20" cols="50" id="reject_comment"></textarea>
					</div>
					<div class="width100 float-left" id="hrm">
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" > 
							<div class="portlet-header ui-widget-header float-right"  id="save_comment">  Finalize  </div>
						</div>	
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" > 
							<div class="portlet-header ui-widget-header float-right"  id="close_comment">  Close  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	