<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
<c:choose>
	<c:when test="${stateList != null }">
		<c:forEach items="${stateList}" var="stateBean" varStatus="status" >
		<option id="${stateBean.stateId}" value="${stateBean.stateId}">${stateBean.stateCode} - ${stateBean.stateName}</option>
	</c:forEach> 
	</c:when>
</c:choose>
<script type="text/javascript">
//Get City List On Change State List
$('#state').change(function(){

	//if(editing != "true") {

		stateId=$('#state').val();
		try {
			temp = $('#' + stateId).text().split('/')[1];
		} catch (e) {
			temp = "";
		}
		var stateCode = trimSpaces($('#' + stateId).text().split(' - ')[0]);
		var stateCodeArabic = temp;
		autoPropertyName[1] = stateCode;
		autoPropertyNameArabic[1] = stateCodeArabic;
		$('#propertyCode').val(autoPropertyName.join("-"));
		$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));
	//}
	if(propertyNameProvided == false) 
		$('#buildingName').val($('#propertyCode').val());
		
	$('#loading').fadeIn();
	$.ajax({
		type: "GET", 
		url: "${pageContext.request.contextPath}/getStateCities.action", 
     	async: false,
     	data:{stateId: stateId},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#city").html(result); 
		}, 
		error: function() {
		}
	}); 
	$('#loading').fadeOut();		
});

</script>