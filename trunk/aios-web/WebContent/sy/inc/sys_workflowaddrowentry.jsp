<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<tr class="rowid" id="fieldrow_${rowId}">
	<td style="display:none;">
			<input type="hidden" name="lineId" class="width90 lineId" 
				id="lineId_${rowId}" value="${rowId}"/>   
			<input type="hidden" name="workflowDetailId" class="width90 workflowDetailId" 
				id="workflowDetailId_${rowId}" value=""/>   
			<input type="hidden" name="notificationTemp" class="width90 notificationTemp" 
				id="notificationTemp_${rowId}" value=""/>
			<input type="hidden" name="operationTemp" class="width90 operationTemp" 
			id="operationTemp_${rowId}" value=""/>
			<input type="hidden" name="screenTemp" class="width90 screenTemp" 
			id="screenTemp_${rowId}" value=""/>
			<input type="hidden" name="accessRightsTypeTemp" class="width90 accessRightsTypeTemp" 
			id="accessRightsTypeTemp_${rowId}" value=""/> 
			<input type="hidden" name="userRoleTemp" class="width90 userRoleTemp" 
				id="userRoleTemp_${rowId}" value=""/>
			<input type="hidden" name="roleTemp" class="width90 roleTemp" 
				id="roleTemp_${rowId}" value=""/>
		</td>   
		<td id="notification_${rowId}"></td> 
		<td id="operation_${rowId}"></td>
		<td id="screen_${rowId}"></td>   
		<td id="accessRightsType_${rowId}"></td> 
		<td id="userRole_${rowId}"></td>
		<td id="order_${rowId}"></td>
		<td id="isDays_${rowId}"></td>
		<td id="validity_${rowId}" style="display:none;"></td>
		<td id="reminder_${rowId}"></td>
		<td id="escalation_${rowId}"></td>
		<td id="notifyTo_${rowId}"></td>
		<td id="isSms_${rowId}"></td>
		<td id="isEmail_${rowId}"></td>
		<td id="isSystem_${rowId}"></td>
		<td style="width:5%;" class="opn_td" id="option_${rowId}">
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataT" id="AddImage_${rowId}" style="cursor:pointer;" title="Add Record">
				<span class="ui-icon ui-icon-plus"></span>
	  </a>	
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataT" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
	  </a> 
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowT" id="DeleteImage_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
	  </a>
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
			<span class="processing"></span>
	  </a>
	</td> 
</tr>
<script>
$(function(){
	 $(".addDataT").click(function(){ 
		 if($("#workflowValidation").validationEngine({returnIsValid:true})) {

			 if($('#openFlag').val() == 0) {

			slidetab=$(this).parent().parent().get(0);  
			
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/workflow_detail_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			
			 }else{
				return false;
			 }
		 }
		 else{
			 return false;
		 }	
	});
	
	$(".editDataT").click(function(){
		 if($('#openFlag').val() == 0) { 
		 slidetab=$(this).parent().parent().get(0);  
		 var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		 if($("#workflowValidation").validationEngine({returnIsValid:true})) {
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/workflow_detail_add.action",
				 data:{id:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
	   		
        			$('#order').val($('#order_'+rowid).text()); 
        			var isDays=$('#isDays_'+rowid).text(); 
        			if(isDays=="Days"){
        				$('#isDays').attr("checked","checked");
        			}else if(isDays=="Hours"){
        				$('#isHours').attr("checked","checked");
        			}
        			$('#validity').val($('#validity_'+rowid).text());
        			$('#reminder').val($('#reminder_'+rowid).text());
        			$('#escalation').val($('#escalation_'+rowid).text());
        			
        			
        			var isSms=$('#isSms_'+rowid).text();
        			var isEmail=$('#isEmail_'+rowid).text();
        			var isSystem=$('#isSystem_'+rowid).text();
        			if(isSms=='true'){
        				$('#isSms').attr("checked","checked");
        			}
        			if(isEmail=='true'){
        				$('#isEmail').attr("checked","checked");
        			}
        			if(isSystem=='true'){
        				$('#isSystem').attr("checked","checked");
        			}
        			$('#notification').val($('#notificationTemp_'+rowid).val());
        			//$('#notification').attr('selected', $('#notificationTemp_'+rowid).val()); 
        			$('#operation').val($('#operationTemp_'+rowid).val()); 
        			$('#screen').val($('#screenTemp_'+rowid).val());

					//$('#screen').combobox('autocomplete', $('#screen_'+rowid).text());
        			$('#accessRightsType').val($('#accessRightsTypeTemp_'+rowid).val());
        			$('#notifyTo').val($('#notifyToTemp_'+rowid).val());
        			//$('#notifyTo').combobox('autocomplete', $('#notifyTo_'+rowid).text());
        			
        			rights=$('#accessRightsTypeTemp_'+rowid).val();
        			if(rights==1){
        				$('#accessRightLable').hide();
        				$('#RoleSpan').hide();
        				$('#UserRoleSpan').show();
            			$('#userRole').val($('#userRoleTemp_'+rowid).val());
        				//$('#userRole').combobox('autocomplete', $('#userRole_'+rowid).text());
        			}else if(rights==2){
        				$('#accessRightLable').hide();
        				$('#RoleSpan').show();
        				$('#UserRoleSpan').hide();
        				$('#role').val($('#roleTemp_'+rowid).val());
        				//$('#role').combobox('autocomplete', $('#userRole_'+rowid).text());
        			}else{
        				$('#accessRightLable').show();
        				$('#RoleSpan').hide();
        				$('#UserRoleSpan').hide();
        			}
        			//accessRightsCall($('#accessRightsTypeTemp_'+rowid).val(),rowid);
        			if($('#isDays').attr("checked") || $('#isHours').attr("checked"))
							$('#showNotification').show();
					applyAutoComplete();
			    $('#openFlag').val(1);
			   
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			}
					
		 }else{
			 return false;
		 } 
	});
	 $(".delrowT").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 var lineId=Number($('#lineId_'+rowid).val());
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/save_workflow_detail.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
					 success:function(result){ 
						   $('.tempresult').html(result); 
							var message=$('.tempresult').html(); 
							 if(message.trim()=="SUCCESS"){
							 	$("#fieldrow_"+rowid).remove(); 
				        		//To reset sequence 
				        		var i=0;
				     			$('.rowid').each(function(){  
									$('#lineId_'+rowId).val(i);
									i=i+1;  
			   					 }); 
							 }else{
								 
								 $('#page-error').hide().html("Delete Failure!").slideDown(1000);
							 }
						},
						error:function(result){
							 return false;
						}
				 });
			 
	 });
});
</script>