<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">

</style>
<script type="text/javascript">

$(function(){ 
	/* if($('#isActiveTemp').val().trim()=='true')
		$('#isActive').attr("checked","checked"); */
		 $jquery("#LOOKUP_ADD").validationEngine('attach');
		
	 $('#save-lookup').click(function() { 
			$('.success,.error').hide();
			if($jquery("#LOOKUP_ADD").validationEngine('validate')){ 
				var masterLookupDetailId=Number($('#masterLookupDetailId').val());
				var lookupMasterId=Number($('#lookupMasterId').val());
				var dataId=$('#dataId').val();
				var accessCode=$('#accessCode').val();
				var displayName=$('#displayName').val();
				var displayNameArabic=$('#displayNameArabic').val();
				var isSystem=$('#isSystem').attr("checked");
				var isActive=$('#isLookupActive').attr("checked");
				var errorMessage=null;
				var successMessage="Successfully Added";
				if(masterLookupDetailId>0)
					successMessage="Successfully Modified";

				$.ajax({
					type: "POST", 
					url:"<%=request.getContextPath()%>/save_lookup_detail.action",
					data: {
						lookupDetailId:masterLookupDetailId,
						lookupMasterId:lookupMasterId,
						dataId:dataId,
						accessCode:accessCode,
						displayName:displayName,
						displayNameArabic:displayNameArabic,
						isSystem:isSystem,
						isActive:isActive
					},
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result) { 
						$(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							 resetCall();
							 reloadLookupDetailList();
						 }else{
							 errorMessage=message;
						 }
						 
						 if(errorMessage==null)
								$('#lookup-success').hide().html(successMessage).slideDown();
							else
								$('#lookup-error').hide().html(errorMessage).slideDown();
					},
					error: function(result) { 
						$(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						$('#lookup-error').hide().html(message).slideDown(); 
					} 
				});
			}else{
				return false;
			}
		});
	   
	   $('#discard-lookup').click(function() { 
			$('.success,.error').hide();
			$('#common-popup').dialog("close");
		});
	   
	   $('#reset-lookup').click(function() { 
		   resetCall();
		});
	   
	   $('.edit-lookup').live('click', function() {
		   var lookupDetailId=Number($('#lookupDetailList').val());
			if(lookupDetailId!=null && lookupDetailId!='' && lookupDetailId>0){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_lookup_detail.action", 
						data:{lookupDetailId:lookupDetailId},
					 	async: false,
					 	dataType: "json",
					    cache: false,
						success:function(response){
							$('#masterLookupDetailId').val(response.lookupDetail.lookupDetailId);
							$('#displayName').val(response.lookupDetail.displayName);
							$('#displayNameArabic').val(response.lookupDetail.arabicDisplayName);
							$('#dataId').val(response.lookupDetail.dataId);
							$('#accessCode').val(response.lookupDetail.accessCode);
							if(response.lookupDetail.isActive)
								$('#isLookupActive').attr("checked",true);
							else
								$('#isLookupActive').attr("checked",true);
							
							if(response.lookupDetail.isSystem){
								$('#isSystem').attr("isSystem",true);
								$('#dataId').attr("disabled",true);
								$('#accessCode').attr("disabled",true);
								$('#isLookupActive').attr("disabled",true);
								$('#isSystem').attr("disabled",true);
							}else{
								$('#isSystem').attr("isSystem",true);
								$('#dataId').attr("disabled",false);
								$('#accessCode').attr("disabled",false);
								$('#isLookupActive').attr("disabled",false);
								$('#isSystem').attr("disabled",true);
							}
							
							$('#delete-lookup').show();
						}
					});
				}
		});
	   
	   $('#delete-lookup').click(function() { 
		   var lookupDetailId=Number($('#lookupDetailList').val());
			if(lookupDetailId!=null && lookupDetailId!='' && lookupDetailId>0){
				var successMessage="Successfully Deleted";
				var errorMessage=null;
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/delete_lookup_detail.action", 
						data:{lookupDetailId:lookupDetailId},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
							$(".tempresult").html(result);
							 var message=$('.tempresult').html(); 
							 if(message.trim()=="SUCCESS"){
								 resetCall();
								 reloadLookupDetailList();
							 }else{
								 errorMessage=message;
							 }
							 
							 if(errorMessage==null)
									$('#lookup-success').hide().html(successMessage).slideDown();
								else
									$('#lookup-error').hide().html(errorMessage).slideDown();
						},
						error: function(result) { 
							$(".tempresult").html(result);
							 var message=$('.tempresult').html(); 
							$('#lookup-error').hide().html(message).slideDown(); 
						} 
					});
				}
		});

});
function resetCall(){
	$('#delete-lookup').hide();
	$('#LOOKUP_ADD').each(function(){
	    this.reset();
	});
}
function reloadLookupDetailList(){
	var masterAccessCode=$('#masterAccessCode').val();
	if(masterAccessCode!=null && masterAccessCode!=''){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:masterAccessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					$('#lookupDetailList').html("");
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#lookupDetailList')
												.append(
														'<option class="edit-lookup" value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
		}
	}
</script>
<div id="main-content"> 
 	
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>${LOOKUP_MASTER.displayName}</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" id="lookup-error" style="display:none;"></div>
			<div  class="response-msg success commonErr ui-corner-all" id="lookup-success" style="display:none;"></div>
			
			<div id="hrm" class="float-left width40"  >
				 <fieldset>
				 	<input type="hidden" name="lookupMasterId" id="lookupMasterId" value="${LOOKUP_MASTER.lookupMasterId}"/>
				 	<input type="hidden" name="masterAccessCode" id="masterAccessCode" value="${LOOKUP_MASTER.accessCode}"/>
					<div>
		                 <label class="width90">${LOOKUP_MASTER.displayName}</label>
		              	 <select id="lookupDetailList" name="lookupDetailList" size="10" class="width90 validate[required]">
		              	  	<c:forEach items="${LOOKUP_MASTER.lookupDetails}" var="nltlst">
								<option class="edit-lookup" value="${nltlst.lookupDetailId}">${nltlst.displayName} </option>
							</c:forEach>
	                  	 </select>
		            </div>
				 </fieldset>
			</div>
		<form id="LOOKUP_ADD" name="LOOKUP_ADD">
			<div id="hrm" class="float-left width60">
				<input type="hidden" name="masterLookupDetailId" id="masterLookupDetailId"/>
				<fieldset>
					<div>
		                 <label class="width30">Lookup Name<span style="color:red">*</span></label>
		                 <input type="text" name="displayName" class="width60 validate[required]" id="displayName"/>
		            </div>
		            <div>
		                 <label class="width30">Lookup Name(Arabic)</label>
		                 <input type="text" name="displayNameArabic" class="width60" id="displayNameArabic">
		            </div>
		            <div>
		                 <label class="width30">ID</label>
		                 <input type="text" name="dataId" class="width60" id="dataId">
		            </div>
		            <div>
		                 <label class="width30">Code</label>
		                 <input type="text" name="accessCode" class="width60" id="accessCode">
		            </div>
		           <div>
		            	 <label class="float-left width20">Active</label>
		           		 <input type="checkbox" name="isLookupActive" checked="checked" class="width10 float-left" id="isLookupActive">
		           		 
		               	 <label class="float-left width30">Non Editable</label>
		                 <input type="checkbox" name="isSystem" class="float-left width10" id="isSystem">
		                 
		            </div>
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right delete-lookup" id="delete-lookup" style="display:none;">Delete</div> 
			<div class="portlet-header ui-widget-header float-right discard-lookup" id="discard-lookup" ><fmt:message key="common.button.close" /></div>
			<div class="portlet-header ui-widget-header float-right reset-lookup" id="reset-lookup" ><fmt:message key="common.button.reset" /></div> 
			<div class="portlet-header ui-widget-header float-right save-lookup" id="save-lookup" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
</div>