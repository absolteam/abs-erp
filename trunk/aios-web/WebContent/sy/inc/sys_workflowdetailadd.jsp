 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
  <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<tr style="background-color: #F8F8F8;" id="childRowId_${requestScope.rowId}">
<td colspan="15" class="tdidentity" id="childRowTD_${requestScope.rowId}">
<div id="errMsg"></div>
<form name="workflowDetailform" id="workflowDetailEditAddValidate">
<input type="hidden" name="addEditFlag" id="addEditFlag"  value="${requestScope.AddEditFlag}"/>
 
<div class="float-left width45" id="hrm" style="margin-left:10px;">
		<fieldset>
			<legend>Workflow Processes</legend> 
			<div class="width100">
				<label class="width30">Notification<span style="color:red">*</span></label>
				<select name="notification" class="width60 notification" id="notification">
					<option value=""></option> 
					 <c:choose>
				 		<c:when test="${NOTIFICATION_DETAIL ne null && NOTIFICATION_DETAIL ne '' && fn:length(NOTIFICATION_DETAIL)>0}">
				 			<c:forEach var="notify" items="${NOTIFICATION_DETAIL}">
				 				<option value="${notify.notificationId}">${notify.notificationTitle}</option>
				 			</c:forEach>
				 		</c:when>
				 	 </c:choose>
				</select>  
			</div>
			<div class="width100">
				<label class="width30">Operation<span style="color:red">*</span></label>
				<select name="operation" class="width60" id="operation">
					<option value="">-NA-</option>
					<option value="0">Entry</option>
					<option value="3">Approval</option> 
					<option value="1">Approved</option> 
					<option value="2">DisApproved</option> 
				</select> 
			</div> 
			<div class="width100">
				<label class="width30">Screen</label>
					<select name="screen" class="width40 screen" id="screen">
					<option value=""></option>
					 <c:choose>
				 		<c:when test="${SCREEN_DETAIL ne null && SCREEN_DETAIL ne '' && fn:length(SCREEN_DETAIL)>0}">
				 			<c:forEach var="screen" items="${SCREEN_DETAIL}">
				 				<option value="${screen.screenId}">${screen.screenName}</option>
				 			</c:forEach>
				 		</c:when>
				 	 </c:choose>
				</select>
			</div>
		
			<div class="width100">
				<label class="width30">Access Person<span style="color:red">*</span></label>
				<select name="accessRightsType" class="width60" id="accessRightsType">
					<option value="">-NA-</option>
					<option value="1">User</option> 
					<option value="2">Role</option> 
					<option value="3">Automatic</option>
				</select>
			</div>
			<div class="float-left width100">
				<label class="width30">User / Designation<span style="color:red">*</span></label>
					<span id="UserRoleSpan" style="display:none;">
						<select name="userRole" class="width60 userRole" style="display:none;" id="userRole">
							<option value="">-NA-</option>
							<c:choose>
						 		<c:when test="${USER_ROLE_DETAIL ne null && USER_ROLE_DETAIL ne '' && fn:length(USER_ROLE_DETAIL)>0}">
						 			<c:forEach var="users" items="${USER_ROLE_DETAIL}">
						 				<option value="${users.userRoleId}">${users.user.username} - ${users.role.roleName}</option>
						 			</c:forEach>
						 		</c:when>
						 	 </c:choose>
						</select>
					</span>  
					<span id="RoleSpan" style="display:none;">
						<select name="role" class="width60 role" style="display:none;" id="role">
							<option value="">-NA-</option>
							<c:choose>
						 		<c:when test="${ROLE_DETAIL ne null && ROLE_DETAIL ne '' && fn:length(ROLE_DETAIL)>0}">
						 			<c:forEach var="rols" items="${ROLE_DETAIL}">
						 				<option value="${rols.roleId}">${rols.roleName}</option>
						 			</c:forEach>
						 		</c:when>
						 	 </c:choose>
						</select>
					</span>
					<span id="accessRightLable" class="width60">-NA-</span>  
			</div>
			<div class="width100">
				<label class="width30">Operation Order<span style="color:red">*</span></label>
				<input type="text" name="order" class="width60 validate[required[custom[onlyNumber]]]" id="order">
			</div>
		</fieldset> 
	</div> 
	<div class="width45  float-left" id="hrm">
		<fieldset>
			<legend>Notification Information</legend> 
			<div class="width100 float-left">
				<div class="width30 float-left">
					<label class="width100">Schedule Type</label>
				</div>
				<div class="width30 float-left">
					<label class="width30">Day(s)</label>
					<input type="radio" tabindex="7" name="isDays" id="isDays" class="width10">
				</div>
				<div class="float-left width30">
					<label class="width30">Hour(s)</label>
					<input type="radio" tabindex="8" name="isDays" id="isHours" class="width10">
				</div>
				
			</div>
			<div id="showNotification" style="display: none;">
				<div class="width100" style="display:none;">
					<label class="width30">Validity</label>
					<input type="text" tabindex="8" name="validity" id="validity" class="width60 validate[optional[custom[onlyNumber]]]">
				</div>
				<div class="width100">
					<label class="width30">Reminder</label>
					<input type="text" tabindex="5" name="reminder" id="reminder" class="width60 validate[optional[custom[onlyNumber]]]">
				</div>
				<div class="width100">
					<label class="width30">Escalation</label>
					<input type="text" tabindex="6" name="escalation" id="escalation" class="width60 validate[optional[custom[onlyNumber]]]">
				</div> 
				<div class="width100">
					<label class="width30">Escalate To</label>
					<select name="notifyTo" class="width60" id="notifyTo">
						<option value="">-NA-</option>
						<c:choose>
					 		<c:when test="${USER_ROLE_DETAIL ne null && USER_ROLE_DETAIL ne '' && fn:length(USER_ROLE_DETAIL)>0}">
					 			<c:forEach var="users" items="${USER_ROLE_DETAIL}">
					 				<option value="${users.userRoleId}">${users.user.username}</option>
					 			</c:forEach>
					 		</c:when>
					 	 </c:choose>
				 	 </select>
				</div> 
				<div class="float-left width100">
					<div class="float-left width30">
						<label class="float-left width100">Notify Through</label>
					</div>
					<div class="float-left width20">
						<input type="checkbox" tabindex="7" name="notifyType" id="isSms" class="float-left width10">
						<label class="float-left width20">SMS</label>
					</div>
					<div class="float-left width20">
						<input type="checkbox" tabindex="8" name="notifyType" id="isEmail" class="float-left width10">
						<label class="float-left width20">Email</label>
					</div>
					<div class="float-left width20">
						<input type="checkbox" tabindex="8" name="notifyType" id="isSystem" class="float-left width10">
						<label class="float-left width20">System</label>
					</div>
					
				</div>
			</div>
		</fieldset>
		<div  id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>      
</div>	
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_detail" id="cancel_${requestScope.rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right update_detail" id="update_${requestScope.rowId}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
<script type="text/javascript"> 
 $(function(){ 
	if($('#addEditFlag').val().trim()!='E'){
		applyAutoComplete();
		
	}
	$('#isDays, #isHours').live('click',function(){
		$('#showNotification').show();
	});
	 $('#accessRightsType').live('change',function(){ 
			var rowId=getRowId($(this).attr('id')); 
			var rights=Number($(this).val());
			
			if(rights>0){
				if(rights==1){
					$('#accessRightLable').hide();
					$('#RoleSpan').hide();
					$('#UserRoleSpan').show();
					$('#userRole').combobox({ 
						 selected: function(event, ui){  
							 
						 }
					});
					
				}else if(rights==2){
					$('#accessRightLable').hide();
					$('#RoleSpan').show();
					$('#UserRoleSpan').hide();
					$('#role').combobox({ 
						 selected: function(event, ui){   
						 }
					});
				}else{
					$('#accessRightLable').show();
					$('#RoleSpan').hide();
					$('#UserRoleSpan').hide();
				}
			}
	 }); 
		  $('.cancel_detail').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
					$("#AddImage_"+rowid).show();
				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
				 	$("#EditImage_"+rowid).show();
				}
				 $("#childRowId_"+rowid).remove();	
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 $('#openFlag').val(0);
			    
		  });
		  $('.update_detail').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 
			 //var tempObject=$(this);
			 var workflowDetailId=Number($('#workflowDetailId_'+rowid).val());	
			 var notificationId=Number($('#notification option:selected').val());
			 var notification=$('#notification option:selected').text();
			 var operationId=Number($('#operation option:selected').val());
			 var operation=$('#operation option:selected').text();
			 var screenId=Number($('#screen option:selected').val());
			 var screen=$('#screen option:selected').text();
			 var accessRightsTypeId=Number($('#accessRightsType option:selected').val());
			 var accessRightsType=$('#accessRightsType option:selected').text();
			 var userRoleId=0;var userRoleName; var roleId=0;
			 if(accessRightsTypeId==1){
				 userRoleId=Number($('#userRole').val());
				 userRoleName=$('#userRole option:selected').text();
			 }else if(accessRightsTypeId==2){
				 roleId=Number($('#role').val());
				 userRoleName=$('#role option:selected').text();
			 }else{
				 userRoleName="-NA-";
			 }
				 
			 var order=Number($('#order').val());
			 var validity=Number($('#validity').val());
			 var reminder=Number($('#reminder').val());
			 var escalation=Number($('#escalation').val());
			 var notifyToId=Number($('#notifyTo option:selected').val());
			 var notifyTo=$('#notifyTo option:selected').text();
			 var isSms=false;
			 var sms="NO";
			 if($('#isSms').attr('checked')){
			 	isSms=true;
			 	sms="YES";
			 }
			 var email="NO";
			 var isEmail=false;
			 if($('#isEmail').attr('checked')){
				 isEmail=true;
				 email="YES";
			 }
			 var isSystem=false;
			 var system="NO";
			 if($('#isSystem').attr('checked')){
				 isSystem=true;
				 system="YES";
			 }
			 var isDays=$('#isDays').attr('checked');
			 var isHours=$('#isHours').attr('checked');
			 var isDayLabel="-NA-";
			 if(isDays==true){
				 isDayLabel="Days";
				 isDays=true;
			 }else if(isHours==true){
				 isDayLabel="Hours";
				 isHours=true;
			 }else{
				 
			 }
			 var lineId=$('#lineId_'+rowid).val();
			 var addEditFlag=$('#addEditFlag').val();
		        if($("#workflowDetailEditAddValidate").validationEngine({returnIsValid:true})){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						url:"<%=request.getContextPath()%>/save_workflow_detail.action",
					 	async: false, 
					 	data:{
					 		workflowDetailId:workflowDetailId,
					 		notificationId:notificationId,
					 		operationId:operationId,
					 		screenId:screenId,
					 		accessRightsTypeId:accessRightsTypeId,
					 		roleId:roleId,
					 		userRoleId:userRoleId,
					 		order:order,
					 		days:isDays,
					 		hours:isHours,
					 		validity:validity,
					 		reminder:reminder,
					 		escalation:escalation,
					 		notifyToId:notifyToId,
					 		sms:isSms,
					 		email:isEmail,
					 		system:isSystem,
					 		id:lineId,
					 		addEditFlag:addEditFlag
					 		},
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
							var message=$('.tempresult').html(); 
							 if(message.trim()=="SUCCESS"){
                                  flag = true;
     						   	  $('#openFlag').val(0);
						     }else{
						    	 $('#openFlag').val(1);
						    	 $('#page-error').hide().html("Save Failure!").slideDown(1000);
						     }
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
					 		$('#page-error').hide().html("Save Failure!").slideDown(1000);
                          return false;
					 	} 
		        	}); 
		        	if(flag==true){ 

			        	//Bussiness parameter
		        			$('#notification_'+rowid).text(notification); 
		        			$('#operation_'+rowid).text(operation); 
		        			$('#screen_'+rowid).text(screen);
		        			$('#accessRightsType_'+rowid).text(accessRightsType); 
		        			$('#userRole_'+rowid).text(userRoleName); 
		        			$('#order_'+rowid).text(order); 
		        			$('#isDays_'+rowid).text(isDayLabel); 
		        			$('#validity_'+rowid).text(validity);
		        			$('#reminder_'+rowid).text(reminder);
		        			$('#escalation_'+rowid).text(escalation);
		        			$('#notifyTo_'+rowid).text(notifyTo);
		        			$('#isSms_'+rowid).text(sms);
		        			$('#isEmail_'+rowid).text(email);
		        			$('#isSystem_'+rowid).text(system);
		        			
		        			$('#notificationTemp_'+rowid).val(notificationId); 
		        			$('#operationTemp_'+rowid).val(operationId); 
		        			$('#screenTemp_'+rowid).val(screenId);
		        			$('#accessRightsTypeTemp_'+rowid).val(accessRightsTypeId);
		        			$('#userRoleTemp_'+rowid).val(userRoleId);
		        			$('#roleTemp_'+rowid).val(roleId);
		        			$('#notifyToTemp_'+rowid).val(notifyToId);
		        			

						//Button(action) hide & show 
		    				 $("#AddImage_"+rowid).hide();
		    				 $("#EditImage_"+rowid).show();
		    				 $("#DeleteImage_"+rowid).show();
		    				 $("#WorkingImage_"+rowid).hide(); 
		        			 $("#childRowId_"+rowid).remove();	
   						
		        			 $('.addrows').trigger('click');

		        		}		  	 
		      } 
		      else{return false;}
		  });
 });
 function applyAutoComplete(){
	 $('#userRole').combobox({ 
		 selected: function(event, ui){   
		 }
	});

	$('#role').combobox({ 
		 selected: function(event, ui){  
		 }
	});
	$('#screen').combobox({ 
		 selected: function(event, ui){   
		 }
	});

	$('#notification').combobox({ 
		 selected: function(event, ui){  
		 }
	});
	$('#notifyTo').combobox({ 
		 selected: function(event, ui){  
		 }
	});
 }
 </script>	
<style>
td .ui-autocomplete-input{
	width:60%!important;
}
</style>