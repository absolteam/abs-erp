<%@ page language="java" isErrorPage="true" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WORKFLOW ACCESS DENIED</title>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
	<link href="${pageContext.request.contextPath}/css/reset.css" rel="stylesheet"  type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/forms.css" rel="stylesheet"  type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/tables.css" rel="stylesheet"  type="text/css"  media="all" />
	<link href="${pageContext.request.contextPath}/css/messages.css" rel="stylesheet" type="text/css"  media="all" /> 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/HRM.css" type="text/css" media="all" /> 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/validationEngine.jquery.css" type="text/css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.datepick.css"> 
	<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/themes/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/themes/ui.multiselect.css" />  
	<link href="${pageContext.request.contextPath}/css/scrollTo.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/jquery.notice.css" rel="stylesheet" type="text/css"  media="all" />  
	<link href="${pageContext.request.contextPath}/css/jquery.crossSelect.css" rel="stylesheet" type="text/css"  media="all" />  <!-- Cross Select css- Added by Nagu -->
	<link href="#" rel="stylesheet" title="style" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.notice.css" type="text/css" media="all" /> 


	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2-ie-fix.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui/flick/jquery-ui-1.8.2.custom.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-qtip-1.0.0-rc3140944/jquery.qtip-1.0.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/superfish.js"></script>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/tooltip.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/tablesorter-pager.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/cookie.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/DateTime.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/date.js"></script> 
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.tools.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/custom.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/scrollTo.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.notice.js"></script>
	 <script src="${pageContext.request.contextPath}/js/jquery.validationEngine-en.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.validationEngine.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.num2words.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.layout.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/js/ui.multiselect.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.jqprint.js" type="text/javascript"></script>	<!-- JqPrint - Added by Nagu -->
	<script src="${pageContext.request.contextPath}/js/jquery.popupWindow.js" type="text/javascript"></script>	<!-- Popup Window - Added by Nagu -->
	<script src="${pageContext.request.contextPath}/js/jQuery.crossSelect-0.5.js" type="text/javascript"></script>	<!-- Cross Select js- Added by Nagu -->
	<script src="${pageContext.request.contextPath}/js/jquery.jclock-1.2.0.js" type="text/javascript"></script>	<!-- Clock js- Added by Nagu -->
	
	<!--[if IE 8]> 
	<script src="${pageContext.request.contextPath}/js/grid.localeIE8-en.js"></script> 
	<![endif]-->
		<script src="${pageContext.request.contextPath}/js/grid.locale-en.js" type="text/javascript"></script>
	<!--[if IE 6]>
	<link href="css/ie6.css" rel="stylesheet" media="all" />
	
	<script src="js/pngfix.js"></script>
	<script>
	  /* EXAMPLE */
	  DD_belatedPNG.fix('.logo, .other ul#dashboard-buttons li a');

	</script>
	<![endif]-->
	<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" media="all" />
	<![endif]-->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/capson.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.datepick.js"></script>

	<script src="${pageContext.request.contextPath}/js/jquery.jqGrid.js" type="text/javascript"></script> 
</head>
<body onload="startTime();">
 	<%@ include file="inc/AfterCompanySelectionheader.jsp"%>
	<div class="mainBody">
		<span style="display:none;" class="ui-icon ui-icon-circle-arrow-w float-right show-side-bar"></span> 
	 		<div id="main-wrapper">
				<div id="main-content">
				 	<div  id="othererror" class="response-msg error ui-corner-all" style="width:80%;">Sorry! Requested information has been processed already</div>
			   	</div>
			</div>
	 		
			<%@ include file="inc/RightSideNavigation.jsp"%>
		
	</div>
	<div class="clearfix"></div>
	<%@ include file="inc/Footer.jsp"%>
	<div id="message" style="display: none;">
		<a id="top-link" href="#top"><img height="53" width="53" alt="top" src="/images/top.png"></a>
	</div>
	
</body>
</html>