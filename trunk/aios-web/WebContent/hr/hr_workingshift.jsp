<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var workingShiftId=0;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#WorkingShift tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
	});
	$('#WorkingShift tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/working_shift_add.action",
		data: {workingShiftId : 0,recordId:0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
	 $('.response-msg').hide(); 
		if(workingShiftId == null || workingShiftId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			workingShiftId = Number(workingShiftId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/working_shift_add.action",  
	     	data: {workingShiftId:workingShiftId,recordId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('.response-msg').hide(); 
	 $('#page-error').hide();
		if(workingShiftId == null || workingShiftId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			workingShiftId = Number(workingShiftId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/working_shift_delete.action",  
	     	data: {workingShiftId:workingShiftId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
					 workingShiftId=null;
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

});

function alreadyListCall(){
	oTable = $('#WorkingShift').dataTable();
	oTable.fnDestroy();
	$('#WorkingShift').remove();
	$('#gridDiv').html("<table class='display' id='WorkingShift'></table>");
	listCall();
}
function listCall(){
	$('#WorkingShift').dataTable({ 
		"sAjaxSource": "working_shift_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'WorkingShiftId', "bVisible": false},
			{ "sTitle": 'Shift Name',"sWidth": "350px"},
			{ "sTitle": 'Start Time'},
			{ "sTitle": 'End Time'},
			{ "sTitle": 'Late Entry'},
			{ "sTitle": 'Severe Late Entry'},
			{ "sTitle": 'Buffer Min/Hour(s)'},
			{ "sTitle": 'Break Min/Hour(S)'},
			{ "sTitle": 'Irregurlar Day'},
			{ "sTitle": 'Location',"sWidth": "200px"},
			{ "sTitle": 'Default'},
			{ "sTitle": 'Active'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#WorkingShift').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Working Shift (or) Shift Master</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="WorkingShift"></table>
			</div>	
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
