<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var departmentId=0;var locationId=0;
var compKey='${THIS.companyKey}';
$(document).ready(function (){ 
	
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
		$('.codecombination-popup').remove(); 
	} 
	$('.formError').remove();
	
	companySetupCall();
	
		
 	/* Click event handler */
 	$('#Department tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0]; 
 	       departmentId=aData[1]; 
 	     	locationId=aData[2]; 
 	     	
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	       departmentId=aData[1]; 
 	      locationId=aData[2];
 	    }
 	});
 	
 	

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/add_department.action",
		data: {departmentId : 0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false;
});

$('.edit_department').click(function(){
	 $('.response-msg').hide(); 
		if(departmentId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			departmentId = Number(departmentId); 
			
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/edit_department.action",  
	     	data: {departmentId : departmentId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return true;
		}
		return false;  
});
$('.edit_location').click(function(){
	 $('.response-msg').hide(); 
	if(locationId == 0)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		locationId = Number(locationId); 
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/edit_location.action",  
     	data: {locationId : locationId},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	},
     	 error:function(result)
         {
             $("#main-wrapper").html(result);
         }
	}); 
	return true;
	}
	return false;  
});

$('#delete').click(function(){
	 $('.response-msg').hide(); 
	 $('#page-error').hide();
	 $('#success_msg').hide();
		if(s == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			var cmpDeptLocId = Number(s); 
	
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_company_setup.action",  
		     	data: {cmpDeptLocId:cmpDeptLocId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
		     	{  
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $('#success_msg').hide().html("Successfully deleted").slideDown(1000);

						 alreadyExistcompanySetupCall();
						 cmpDeptLocId=null;
						 
					 }
					 else{
						 $('#page-error').hide().html("Can't Delete, Information is already in use").slideDown(1000);
						 return false;
					 }
		     	},
		     	error:function(result)
	            {
		     		$('#page-error').hide().html("Can't Delete, Information is already in use").slideDown(1000);
	            } 
			}); 
			return false;
		}
		return false; 
});

$('.delete_department').click(function(){
	 $('#page-error').hide();
	 $('.response-msg').hide(); 
	 $('#success_msg').hide();
		if(departmentId == 0)
		{
			alert("Please select any one department to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			departmentId = Number(departmentId); 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_department.action",  
		     	data: {departmentId:departmentId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
		     	{  
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $('#success_msg').hide().html("Department deleted successfully").slideDown(1000);
						 alreadyExistcompanySetupCall();
						 
					 }
					 else{
						 $('#page-error').hide().html("Cant't Delete, Department is already in use").slideDown(1000);
						 return false;
					 }
		     	},
		     	error:function(result)
	            {
		     		$('#page-error').hide().html(message).slideDown(1000);
	            } 
			}); 
			return false;
		}
		return false; 
});


$('.delete_location').click(function(){
	 $('.response-msg').hide(); 
	 $('#page-error').hide();
	 $('#success_msg').hide();
		if(locationId==null || locationId == 0)
		{
			alert("Please select any one location to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			 locationId = Number(locationId); 
	
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_location.action",  
		     	data: {locationId:locationId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
		     	{  
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $('#success_msg').hide().html("Location deleted successfully").slideDown(1000);
						 alreadyExistcompanySetupCall();
						 
					 }
					 else{
						 $('#page-error').hide().html("Cant't Delete, Location is already in use").slideDown(1000);
						 return false;
					 }
		     	},
		     	error:function(result)
	            {
		     		$('#page-error').hide().html(message).slideDown(1000);
	            } 
			}); 
			return false;
		}
		return false; 
});


$('#view').click(function(){
	 $('.response-msg').hide(); 
	var myedit  = $("#list2").jqGrid('getGridParam','selrow'); 				
		if(myedit == null)
		{
			alert("Please select one row to view");
			return false;
		}
		else
		{			
			var view = $("#list2").jqGrid('getRowData',myedit ); 
			organizationId = view.organizationId;
			locationId	   = view.locationId;
			
			var pageName=$('#pageName').val();
			//alert(" Visa ID= " + visaId2 );	
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/organization_details_view.action",  
	     	data: {organizationId : organizationId,locationId : locationId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	error:function(result)
            {
                $("#main-wrapper").html(result);
                alert("error"+result);
            } 
		}); 
		return true;
		}
		return false; 
		
		
		
});

$('.location-group').live('click', function() {
		if($(this).attr('checked') == 'checked' || $(this).attr('checked') == true) {		
			var cnfrm = confirm('Selected location will be shared with both GCC & Coffeeshop');
			if(!cnfrm)
				return false;
		}
		var cmpDeptLocId=Number($(this).val());
		var sharing=$(this).attr('checked');
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/update_location_sharing.action",  
	     	data: {cmpDeptLocId:cmpDeptLocId,sharing:sharing},  
	     	async: false,
			dataType: "json",
			cache: false,
			success: function(response)
	     	{  
				var message=response.returnMessage;

				 if(message.trim()=="SUCCESS"){
					 $('#success_msg').hide().html("updated successfully").slideDown(1000);
				 }
				 else{
					 $('#page-error').hide().html("Cant't update, failure").slideDown(1000);
				 }
	     	},
	     	error:function(response)
            {
	     		var message=response.returnMessage;

				 if(message.trim()=="SUCCESS"){
					 $('#success_msg').hide().html("updated successfully").slideDown(1000);

				 }
				 else{
					 $('#page-error').hide().html("Cant't update, failure").slideDown(1000);
				 }
            } 
		}); 
			
		return true;
	});
});
function alreadyExistcompanySetupCall(){
	oTable = $('#Department').dataTable();
	oTable.fnDestroy();
	$('#Department').remove();
	$('#gridDiv').html("<table class='display' id='Department'></table>");
	companySetupCall();
}
function companySetupCall(){
	$('#Department').dataTable({ 
 		"sAjaxSource": "company_setup_list_json.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Setup ID', "bVisible": false},
 			{ "sTitle": 'Department Id', "bVisible": false},
 			{ "sTitle": 'Location Id', "bVisible": false},
 			{ "sTitle": '<fmt:message key="hr.department.company"/>'},
 			{ "sTitle": '<fmt:message key="hr.department.department"/>'},
 			{ "sTitle": '<fmt:message key="hr.department.location"/>'},
	 		{ "sTitle": 'Shared Location',"mDataProp": function(aData) {
	 			if(compKey=='gcc' || compKey=='gcc-coffee'){
	 				if(aData[6]==true)
						return '<td><input type="checkbox" checked="checked" class="location-group" name="location-group[]" value="'+aData[0]+'"/></td>';
					else
						return '<td><input type="checkbox" class="location-group" name="location-group[]" value="'+aData[0]+'"/></td>';	
				}else{
					return "";
				}
 			 }
 			}
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#Department').dataTable();
 	if(compKey!='gcc' && compKey!='gcc-coffee'){
 		oTable.fnSetColumnVis(6, false );
 	}
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Department & Branchs</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"><span>Process Failure!</span></div>  
		 <div id="success_msg" class="success response-msg ui-corner-all" style="display: none;"></div>
		 
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Department"></table>
			</div>	
		</div>		
		<div class="vmenu">
 	       	<div class="first_li"><span>Add</span></div>
			<div class="sep_li"></div>		
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li edit_department" ><span >Edit Department</span></div>
			<div class="first_li edit_location" ><span >Edit Location</span></div>
			<div class="first_li delete_department" ><span >Delete Department</span></div>
			<div class="first_li delete_location" ><span >Delete Location</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"><!-- 
		<div class="portlet-header ui-widget-header float-right view">View</div>
		-->
		<div class="portlet-header ui-widget-header float-right delete_location" >Delete Location</div>
		<div class="portlet-header ui-widget-header float-right delete_department" >Delete Department</div>
		<div class="portlet-header ui-widget-header float-right edit_location" >Edit Location</div>
		<div class="portlet-header ui-widget-header float-right edit_department" >Edit Department</div>
		<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="common.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="common.button.add"/></div>
		
	</div>
</div>
</div>
</div>
