<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var candidateOfferId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#Candidate tbody tr').live('click', function() {
		if (!$('#Candidate tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					candidateOfferId=aData.candidateOfferId;
	
					
				}
			}
		}
	});
	

$('#Finalise').click(function(){
	if(candidateOfferId == null || candidateOfferId == 0)
	{
		alert("Please select candidate");
		return false;
	}
	else
	{		
		
		candidateOfferId = Number(candidateOfferId); 
		

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/candidate_offer_update.action",  
     	data: {candidateOfferId:candidateOfferId,recordId:0},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  	
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	},
     	 error:function(result)
         {
             $("#main-wrapper").html(result);
         }
	}); 
	return false;
	}
	return false;  
});


});


function listCall(){
	
	$('#Candidate').dataTable().fnDestroy();
	oTable = $('#Candidate')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sAjaxSource" : "<%=request.getContextPath()%>/candidate_offer_finalise_list_json.action",
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				"aoColumns" : [ {
					"mDataProp" : "candidateName"
				},{
					"mDataProp" : "positionReferenceNumber"
				}, {
					"mDataProp" : "rating"
				}, {
					"mDataProp" : "score"
				}, {
					"mDataProp" : "statusDisplay"
				} ]

	}); 
	
	
		
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Offered Candidates</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			 <div id="gridDiv">
				<table class="display" id="Candidate">
					<thead>
						<tr>
							<th>Candidate</th>
							<th>Position Reference</th>
							<th>Average Rating</th>
							<th>Score</th>
							<th>Status</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
			<!-- <div id="gridDiv">
				<table class="display" id=EmployeeLoan></table>
			</div>	 -->
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Finalise</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all">
		<div class="portlet-header ui-widget-header float-right" id="Finalise">Finalise</div> 
		
		
	</div>
</div>
</div>
</div>
