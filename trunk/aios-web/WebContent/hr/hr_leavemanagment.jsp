<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>  
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable; var selectRow=""; var aData="";var aSelected = [];var leaveProcessId=0;; 
var requestStatus="";
var leaveStatus=""; 
 $(function(){ 
	 if(typeof($('#job-common-popup')!="undefined")){ 
			$('#job-common-popup').dialog('destroy');		
			$('#job-common-popup').remove(); 
		}
	leaveHistoryCall();
    
	$('#add').click(function(){
    	 $('#page-error').hide();
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_add_leave.action", 
			 	async: false, 
			 	data:{ 
			 		leaveProcessId:Number(0)
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#main-wrapper").html(result);
				},
				error:function(result){  
					$("#main-wrapper").html(result);
				}
			});  
    	
    	 return false;
 	});
     $('#edit').click(function(){
    	 $('#page-error').hide();
    	 if(leaveProcessId == null || leaveProcessId=="" || leaveProcessId==0)
 		{
 			alert("Please select one row to Edit");
 			return false;
 		}
 		else
 		{	
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_edit_leave_management.action", 
				 	async: false, 
				 	data:{ 
				 		leaveProcessId:leaveProcessId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#main-wrapper").html(result);
					},
					error:function(result){  
						$("#main-wrapper").html(result);
					}
				});  
    	 
	    	 return false;
		}
    	 return false;
 	});
     $('#view').click(function(){
    	 $('#page-error').hide();
    	 if(leaveProcessId == null || leaveProcessId=="" || leaveProcessId==0)
 		{
 			alert("Please select one row to View");
 			return false;
 		}
 		else
 		{	
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_view_leave_management.action", 
				 	async: false, 
				 	data:{ 
				 		leaveProcessId:leaveProcessId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#main-wrapper").html(result);
					},
					error:function(result){  
						$("#main-wrapper").html(result);
					}
				});  
    	 
	    	 return false;
		}
    	 return false;
 	});
     
     $('#reconciliation').click(function(){
    	 //leave_reconciliation_execution
    	 //leave_reconciliation_correction
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/leave_reconciliation_execution.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					alert("Success");
				},
				error:function(result){  
				}
			});  
    	 return false;
 	});
     
    
          
     $('#delete').click(function(){
    	 if(leaveProcessId == null || leaveProcessId=="" || leaveProcessId==0)
    		{
    			alert("Please select one row to Delete");
    			return false;
    		}
    		else
    		{	
    			var cnfrm = confirm('Selected record will be deleted permanently');
    			if(!cnfrm)
    				return false;
		    	 $('#page-error').hide();
			    	 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/delete_leave.action", 
						 	async: false, 
						 	data:{ 
						 		leaveProcessId:leaveProcessId
						 	},
						    dataType: "html",
						    cache: false,
							success:function(result){
								 $(".tempresult").html(result);
								 var message=$('.tempresult').html(); 
								 if(message.trim()=="SUCCESS"){
									checkDataTableExsist();
						     		$('.success').hide().html("Deleted Successfully.").slideDown();

								 }else{
									 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
								 }
							},
							error:function(result){  
								$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
							}
						});  
    	
    		 return false;
    		}
    	 return false;
 	});
     
     $('#halfDay').change(function(){
    	 dateOnchangeCall();
     });
     
     //Popup

   //pop-up config
   $('.employee-popup').live('click',function(){
         $('.ui-dialog-titlebar').remove();  
         $('#job-common-popup').dialog('open');
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/get_employee_for_leavemanagment.action",
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.job-common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.job-common-result').html(result);
               }
           });
            return false;
   }); 

   $('#job-common-popup').dialog({
   	autoOpen: false,
   	minwidth: 'auto',
   	width:800,
   	height:450,
   	bgiframe: false,
   	modal: true 
   }); 
   /* Click event handler */
   $('#LeaveProcess tbody tr').live('click', function () {
  	 $('#page-error').hide();
   	  if ( $(this).hasClass('row_selected') ) {
   		  $(this).addClass('row_selected');
           aData =oTable.fnGetData( this );
           leaveProcessId=aData[0];
           leaveStatus=aData[12];
           requestStatus=aData[13];
       }
       else {
           oTable.$('tr.row_selected').removeClass('row_selected');
           $(this).addClass('row_selected');
           aData =oTable.fnGetData( this );
           leaveProcessId=aData[0];
           leaveStatus=aData[12];
           requestStatus=aData[13];

       }
   });
   
   $("#print").click(function() {  
	   var allVals = [];
	   $('.leave-group').each(function() {
		   if($(this).attr("checked"))
	      	 allVals.push($(this).val());
	     });
	   var stringArray="";
	   for(var j=0;j<allVals.length;j++){ 
		   if(j==0)
			   stringArray+=allVals[j];
		   else
			   stringArray+=","+allVals[j];
		} 
		if(allVals.length>0){	 
			window.open('show_leave_printout.action?leaveIds='+ stringArray+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
   
   $("#due-print").click(function() {  
	   var allVals = [];
	   $('.leave-group').each(function() {
		   if($(this).attr("checked"))
	      	 allVals.push($(this).val());
	     });
	   var stringArray="";
	   for(var j=0;j<allVals.length;j++){ 
		   if(j==0)
			   stringArray+=allVals[j];
		   else
			   stringArray+=","+allVals[j];
		} 
		if(allVals.length>0){	 
			window.open('leave_due_settlement_printout.action?leaveIds='+ stringArray+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
   
   
});

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function checkDataTableExsist(){
	oTable = $('#LeaveProcess').dataTable();
	oTable.fnDestroy();
	$('#LeaveProcess').remove();
	$('#gridDiv').html("<table class='display' id='LeaveProcess'></table>");
	leaveHistoryCall();
}
function leaveHistoryCall(){
	$('#page-error').hide();
	$('#LeaveProcess').dataTable({ 
		"sAjaxSource": "employee_leave_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'LeaveProcess ID', "bVisible": false},
			{ "sTitle": 'Employee',"sWidth": "250px"},
			{ "sTitle": 'Designation'},
			{ "sTitle": 'Location',"sWidth": "350px", "bVisible": false},
			{ "sTitle": 'Leave Type'},
			{ "sTitle": 'From'},
			{ "sTitle": 'To'},
			{ "sTitle": 'Half Day', "bVisible": false},
			{ "sTitle": 'No.Of Days'},
			{ "sTitle": 'Reason', "bVisible": false},
			{ "sTitle": 'Contact On Leave', "bVisible": false},
			{ "sTitle": 'Requested Date', "bVisible": false},
			{ "sTitle": 'Leave Status', "bVisible": false},
			{ "sTitle": 'Status'},
			{ "sTitle": 'Encashed'},
			{ "sTitle": 'Due Settled'},
			{ "sTitle": 'Group To Print',"mDataProp": function(aData) {
				return '<td><input type="checkbox" class="leave-group" name="leave_group[]" value="'+aData[0]+'"/></td>';

			}
			}
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#LeaveProcess').dataTable();
}

function checkDataTableExsist(){
	oTable = $('#LeaveProcess').dataTable();
	oTable.fnDestroy();
	$('#LeaveProcess').remove();
	$('#gridDiv').html("<table class='display' id='LeaveProcess'></table>");
	leaveHistoryCall();
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Control</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
				<div id="success_message" class="success response-msg ui-corner-all" style="display:none;"></div> 
			  	<div class="tempresult" style="display:none;"></div>
		</div>
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="LeaveProcess"></table>
			</div>
	 	</div>		
		<div class="vmenu">
			<div class="first_li"><span>Add</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>View</span></div>
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Print</span></div>
		</div> 
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
			<div class="portlet-header ui-widget-header float-right" id="reconciliation" style="display:none;">Reconciliation</div>
			<div class="portlet-header ui-widget-header float-right" id="due-print">Settlement Print</div>
			<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
			<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
			<div class="portlet-header ui-widget-header float-right" id="view" >View</div>
			<div class="portlet-header ui-widget-header float-right"  id="edit">Edit</div>
			<div class="portlet-header ui-widget-header float-right" id="add" >Add</div>
		</div>
	</div>	
</div>
