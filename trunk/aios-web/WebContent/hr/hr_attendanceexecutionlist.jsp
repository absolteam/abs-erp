<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<style>
div.border
{
	padding:5px;
	border:3px solid gray;
	margin:0px;
	cursor: pointer;
}

div.border span
{
	text-align: center;
	font-weight: bold;
	color: blue;
}

.container {float: left;padding: 1px;}
.container select{width: 33em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>


<script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
var j = jQuery.noConflict();
$(function(){ 
	$('.formError').remove();
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	
	attendanceCall();

/* Click event handler */
$('#Attendance tbody tr').live('click', function () { 
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
});

$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.error ').hide();
       }
});

$('#add').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/add_attendance_by_hr.action",  
     	data: {attendanceId:0},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	return false;
});

$('#edit').click(function(){
	if(s == null || s=="" || s==0)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		var attendanceId = Number(s);
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/add_attendance_by_hr.action",  
	     	data: {attendanceId:attendanceId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	}
		}); 
	}
	return false;
});

$('#delete').click(function(){
	if(s == null || s=="" || s==0)
	{
		alert("Please select one row to delete");
		return false;
	}
	else
	{			
		var attendanceId = Number(s);
		var cnfrm = confirm('Selected record will be deleted permanently');
		if(!cnfrm)
			return false;
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/delete_attendance_by_hr.action",  
	     	data: {attendanceId:attendanceId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result); 
	     		if($(".tempresult").html().trim()=="SUCCESS"){
		     		$('#success_message').hide().html("Attendance Deleted Successfully.").slideDown();
		     		attendanceAlreadyExsist();
		     	}else{
		     		$('#page-error').hide().html(result).slideDown(); 
			    }
				$('#loading').fadeOut(); 
	     	}
		}); 
	}
	return false;
});
{
	var payPolicy=Number(3);
	if(payPolicy>0){
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_payroll_employee_list.action", 
			 	async: false, 
			 	data:{ 
			 		payPolicy:payPolicy,
			 		toDate:toDate
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $("#person_result").html(result);
				},
				error:function(result){  
					 $("#person_result").html(result);
				}
			});  
	 	}
}
$('#view').click(function(){
	if(s == null || s=="" || s==0)
	{
		alert("Please select one row to view");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/view_attendance.action",  
     	data: {attendanceId:recId2},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
	return false;
});

$('#change-action').click(function(){

	var toDate=$('#toDate').val();
	var fromDate=$('#fromDate').val();
	var persons = generateDataToSendToDB("jobAssignmentList"); 
	if(toDate!=null && toDate!=''){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/get_apply_deviation_decision.action",  
	     	async: false,
	     	data:{toDate:toDate,fromDate:fromDate,persons:persons},
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	}
		}); 
	}else{
		alert("Please enter from & to date");
	}
	return false;
});
$('#update-inout').click(function(){

	var toDate=$('#toDate').val();
	var fromDate=$('#fromDate').val();
	var persons = generateDataToSendToDB("jobAssignmentList"); 
	if(toDate!=null && toDate!=''){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/update_in_out.action",  
	     	async: false,
	     	data:{toDate:toDate,fromDate:fromDate,persons:persons},
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result); 
	     		if($(".tempresult").html().trim()=="SUCCESS"){
					$('#success_message').hide().html("Successfully updated.").slideDown(3000);
				}else{
					$('#page-error').hide().html("Error in update.").slideDown(3000);
				}
	     	}
		}); 
	}else{
		 alert("Please enter from & to date");
	}
	return false;
});
$('#calculate').click(function(){
	$('.error,.success').hide();

	if($jquery("#ATTENDANCEFORM").validationEngine('validate')){
		var persons = generateDataToSendToDB("jobAssignmentList"); 
		var toDate=$('#toDate').val();
		var fromDate=$('#fromDate').val();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/attendance_calculate.action",  
			async: false,
			data:{toDate:toDate,fromDate:fromDate,persons:persons},
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result); 
	     		if($(".tempresult").html().trim()=="SUCCESS"){
		     		$('.success').hide().html("Successfully Processed.").slideDown(3000);
		     		attendanceAlreadyExsist();
		     	}else{
		     		$('.error').hide().html(result).slideDown(3000); 
			    }
	     	}
		}); 
		
	}else{
		alert("Please enter from & to date");

	}
	

	return false;
});

$('#readURL').click(function(){
	readURL();
});
$('#attendanceDate').datepick({});
$('#attendanceFile').click(function() {
	  $('#attendanceUploadDiv').toggle('slow', function() {
	    // Animation complete.
	  });
});

$('#processAttenance').click(function() {
	  $('#attendanceDateDiv').toggle('slow', function() {
	    // Animation complete.
	  });

	});
});
function attendanceAlreadyExsist(){
	oTable = $('#Attendance').dataTable();
	oTable.fnDestroy();
	$('#Attendance').remove();
	$('#gridDiv').html("<table class='display' id='Attendance'></table>");
	attendanceCall();
}
function attendanceCall(){
	$('#Attendance').dataTable({ 
		"sAjaxSource": "attendance_excution_listing_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Attendance ID', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": '<fmt:message key="hr.attendance.designation"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.location"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.date"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.timein"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.timout"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.timelossorexcess"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.totalhours"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.processstatus"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.decision"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.ishalfday"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.isabsent"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.systemsuggesion"/>', "bVisible": false},
			
		],
		"sScrollY": $("#main-content").height() - 280,
		//"bPaginate": false,
		"aaSorting": [], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Attendance').dataTable();
	 //Date process
    $('#fromDate,#toDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   

}
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}
function readURL() {
	$('.error,.success').hide();
	//var input=$("#fileUpload").files[0];
	fileUpload =$("#fileUpload")[0].files[0];
	if(fileUpload==null || fileUpload==''){
		$('.error').hide().html("Please upload a valid file/data").slideDown();
		return false;
	}
	var form_data = new FormData();
	form_data.append("fileUpload",fileUpload);
    j.ajax({
      type: "POST",
      url:"<%=request.getContextPath()%>/attendance_upload_send.action",
      data: form_data,
      processData: false,  // tell jQuery not to process the data
      contentType: false, // tell jQuery not to set contentType
      success: function(result)
	   	{  
    	  $(".tempresult").html(result); 
	   		if($(".tempresult").html().trim()=="SUCCESS"){
	     		$('.success').hide().html("Successfully Readed.").slideDown();
	     	}else{
	     		$('.error').hide().html(result).slideDown(); 
		    }
		}
    });
}

function generateDataToSendToDB(source){
	var typeList=new Array();
	var ids="";
	$('#'+source+' option:selected').each(function(){
		typeList.push($(this).val());
	});
	for(var i=0; i<typeList.length;i++){
		if(i==(typeList.length)-1)
			ids+=typeList[i];
		else
			ids+=typeList[i]+",";
	}
	
	return ids;
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="hr.attendance.attendanceinfo"/></div> 
		<div class="portlet-content">
		<div style="display:none;" class="tempresultMsg">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		<div style="display:none;" class="tempresult"></div>
		<div>
			 <div id="page-error" class="error response-msg ui-corner-all" style="display:none;"></div>
		  	 <div id="success_message" class="success response-msg ui-corner-all" style="display:none;"></div> 
    	</div>
		
	
	<div id="rightclickarea">
		<div id="gridDiv">
			<table class="display" id="Attendance"></table>
		</div>
 	</div>		
	<div class="vmenu">
		<div class="first_li"><span>Add</span></div>
		<div class="first_li"><span>Edit</span></div>
		<div class="first_li"><span>Delete</span></div>
		<div class="first_li"><span>View</span></div>
	</div>
		 <div class="width20 float-left" style="margin-top:12px;">
		 	<div id="attendanceFile" class="border width95 float-left">
				 <span><fmt:message key="hr.attendance.readattendance"/></span> 
			</div>
			<div id="attendanceUploadDiv" style="display:none;" class="border width95 float-left">
				<div>
					<%-- <s:form action="attendance_upload_send" method="POST" enctype="multipart/form-data">
					   <s:file name="fileUpload" label="Choose File" size="40" />
					   <s:submit value="Upload" name="submit" />
					</s:form> --%>
					<input class="width95"  type="file" name="fileUpload" id="fileUpload" style="cursor: pointer;"/>
					<div class="float-right buttons ui-widget-content ui-corner-all">
						<div class="portlet-header ui-widget-header float-right"
								id="readURL">Submit</div>
					</div>
				</div>
			</div>
		</div>
		<form id="ATTENDANCEFORM" class="" name="ATTENDANCEFORM">
		<div class="width50 float-left" style="margin-top:12px;"> 
		 	<div id="processAttenance" class="border width100 float-left">
				  <span><fmt:message key="hr.attendance.processattendance"/></span> 
			</div>
			<div id="attendanceDateDiv" class="border width100 float-left" style="display:none;">
				<div>
					<label class="width40 float-left" for="fromDate">From Date</label>
					<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="4" class="width50 validate[required]"/>
				</div>
				<div>
					<label class="width40 float-left" for="toDate">To Date</label>
					<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="4" class="width50 validate[required]"/>
				</div>
				<div class="width100 float-left" id="person_result">
				</div>

				<div class="float-right buttons ui-widget-content ui-corner-all">
					
					<div class="portlet-header ui-widget-header float-left" id="update-inout" >Organize In&Out</div>
					<div class="portlet-header ui-widget-header float-left" id="calculate">Perform Calculation</div>
					<div class="portlet-header ui-widget-header float-left"  id="change-action">Update Decision</div>
				</div>
			</div>
		</div>
	</form> 
	
	<div class="float-left buttons ui-widget-content ui-corner-all" style="display:none;"> 
		<div class="portlet-header ui-widget-header float-right" id="calculate"><fmt:message key="hr.attendance.processattendance"/></div>
		<div class="portlet-header ui-widget-header float-right" id="read"><fmt:message key="hr.attendance.readattendance"/></div>
	</div>
	<div class="float-right buttons ui-widget-content ui-corner-all">

		<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="hr.personaldetails.button.view"/></div>
		<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="common.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="common.button.edit"/></div>
		<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="common.button.add"/></div>
	</div>
	</div>
</div>
</div>
