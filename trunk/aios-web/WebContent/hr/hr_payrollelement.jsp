<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var payrollElementId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#PayrollElement tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        employeeCalendarId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        payrollElementId=aData[0];
	    }
	});
	$('#PayrollElement tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        payrollElementId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        payrollElementId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/payroll_elements_add.action",
		data: {payrollElementId : 0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
		if(payrollElementId == null || payrollElementId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			payrollElementId = Number(payrollElementId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/payroll_elements_add.action",  
	     	data: {payrollElementId:payrollElementId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(payrollElementId == null || payrollElementId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			payrollElementId = Number(payrollElementId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/delete_payroll_elements.action",  
	     	data: {payrollElementId:payrollElementId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
					 $('.success').delay(2000).slideUp(1000);
				 }
				 else{
					 $('#page-error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('#page-error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

});

function alreadyListCall(){
	oTable = $('#PayrollElement').dataTable();
	oTable.fnDestroy();
	$('#PayrollElement').remove();
	$('#gridDiv').html("<table class='display' id='PayrollElement'></table>");
	listCall();
}
function listCall(){
	$('#PayrollElement').dataTable({ 
		"sAjaxSource": "payroll_elements_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'PayrollElementID', "bVisible": false},
			{ "sTitle": 'Element Code'},
			{ "sTitle": 'Element Name'},
			{ "sTitle": 'Element Nature'},
			{ "sTitle": 'Element Type'},
			{ "sTitle": 'Conversion Element'},
			{ "sTitle": 'Account Code'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[3, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#PayrollElement').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Payroll Elements</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="PayrollElement"></table>
			</div>	
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
<!-- 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
 -->		
	</div>
</div>
</div>
</div>
