<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 

$(function(){ 
	$('.formError').remove();
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	 if(typeof($('#job-common-popup')!="undefined")){  
		 $('#job-common-popup').dialog('destroy');		
		 $('#job-common-popup').remove(); 
	 }
	 if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	 
		
		if(typeof($('#qualification-common-popup')!="undefined")){  
			 $('#qualification-common-popup').dialog('destroy');		
			 $('#qualification-common-popup').remove(); 
		 }
$('#example').dataTable({ 
	"sAjaxSource": "json-person.action",
    "sPaginationType": "full_numbers",
    "bJQueryUI": true, 
    "iDisplayLength": 25,
	"aoColumns": [
		{ "sTitle": '<fmt:message key="hr.personaldetails.label.recordid"/>', "bVisible": false,"bSearchable": false},
		{ "sTitle": '<fmt:message key="hr.personaldetails.label.firstname"/>'},
		{ "sTitle": '<fmt:message key="hr.personaldetails.label.lastname"/>'},
		{ "sTitle": '<fmt:message key="hr.personaldetails.label.persontype"/>'},
		{ "sTitle": '<fmt:message key="hr.personaldetails.label.country"/>'},
		{ "sTitle": '<fmt:message key="hr.personaldetails.label.personid"/>'},
		
	],
	"sScrollY": $("#main-content").height() - 235,
	//"bPaginate": false,
	"aaSorting": [[1, 'asc']], 
	"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
			$(nRow).addClass('row_selected');
		}
	}
});	
//Json Grid
//init datatable
oTable = $('#example').dataTable();

/* Click event handler */
$('#example tbody tr').live('click', function () { 
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
});

$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});
$('#cancel').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/hr_personal_details_list.action",   
     	async: false, 
		dataType: "html",
		cache: false,
		error: function(data) 
		{
		},
     	success: function(data)
     	{
			$("#main-wrapper").html(data);  //gridDiv main-wrapper
     	}
	});
	return true;
});  

$('#move-profile').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/move_profile_to_drive.action",   
     	async: false, 
		dataType: "json",
		cache: false,
		error: function(response) 
		{
			if(response.returnMessage=="SUCCESS"){
	   			alert("SUCCESS");
	   		}else{
	   			alert("ERROR");
	   		}
		},
     	success: function(response)
     	{
	   		if(response.returnMessage=="SUCCESS"){
	   			alert("SUCCESS");
	   		}else{
	   			alert("ERROR");
	   		}
     	}
	});
	return false;
});  


$('#report').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/person_report.action",   
     	async: false, 
		dataType: "html",
		cache: false,
		error: function(data) 
		{
			alert(data);
		},
     	success: function(data)
     	{
			$("#main-wrapper").html(data);  //gridDiv main-wrapper
     	}
	});
	return true;
});  

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.otherErr ').hide();
       }
});

$('#view').click(function(){
	if(s == null || s ==0)
	{
		alert("Please select one row to view");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/personal_details_view.action",  
     	data: {personId:recId2, isSupplier: false,viewType:""},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
	return false;
});

$('#add').click(function(){
	$.ajax({
		type:"GET",
		url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
		data: {personId: 0, isSupplier: false,recordId:0},
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false;
});
$('#edit').click(function(){
	if(s == null || s ==0)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/personal_details_add_redirect.action",  
     	data: {personId:recId2, isSupplier: false,recordId:0},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
	
	return false;
});
$('#delete').click(function(){
	if(s == null  || s ==0)
	{
		alert("Please select one row to Delete");
		return false;
	}
	else
	{		
		var cnfrm = confirm('Selected record will be deleted permanently');
		if(!cnfrm)
			return false;
		var recId2 = s;
			
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/personal_details_delete.action",  
			data: {recId:recId2,pageName:pageName,personId:personId,firstName:firstName},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	}
		}); 
	}
	return false;
});

});

</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>person</div> 
		<div class="portlet-content">
		<div>
			 <c:if test="${requestScope.errorMsg ne null && requestScope.errorMsg ne ''}">
				<div class="error response-msg ui-corner-all"><c:out value="${requestScope.errorMsg}"/></div>
			</c:if>
			<c:if test="${requestScope.successMsg ne null && requestScope.successMsg ne ''}">
		  	  <div class="success response-msg ui-corner-all"><c:out value="${requestScope.successMsg}"/></div> 
	    	</c:if>
    	</div>
		<div  class="otherErr response-msg error ui-corner-all" style="display:none;"></div>
		<div class="success response-msg ui-corner-all" style="display:none;"></div> 
		<div style="display:none;" class="tempresultMsg">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		
		<!--<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width80"> 
			<div class="float-left width80" style="margin-top:10px;margin-left:5px;">
				<label style="font-weight:bold;"><fmt:message key="hr.personaldetails.label.personname"/></label><input type="text" name="firstName" id="firstName"/>
				<label style="font-weight:bold;"><fmt:message key="person.label.persontypes"/></label>
				<select id="personType" class="width20" name="personType">
                    <option value="">- Select -</option>
                    <option value="0">In Active</option>
                    <option value="1">Active</option>
                </select>
			</div>
			<div class="portlet-header ui-widget-header float-right" id="cancel"  style="cursor:pointer;"><fmt:message key="hr.personaldetails.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right" id="search"  style="cursor:pointer;"><fmt:message key="hr.personaldetails.button.search"/></div>			
 		</div>
	-->
	
	<div id="rightclickarea">
		<div id="gridDiv">
			<table class="display" id="example"></table>
		</div>
 	</div>		
	<div class="vmenu">
	    <div class="first_li"><span>Add</span></div>
		<div class="sep_li"></div>		 
		<div class="first_li"><span>Edit</span></div>
		<div class="first_li"><span>View</span></div>
		<div class="first_li"><span>Delete</span></div>
	</div>
	
	
	<div class="float-left buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-left" id="report">Report</div>
	</div>
	
	<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="move-profile" style="display: none;">Move PP</div>
		<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="hr.personaldetails.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="hr.personaldetails.button.view"/></div>
		<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="hr.personaldetails.button.edit"/></div>
		<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="hr.personaldetails.button.createperson"/></div>
		
	</div>
	<!--<div id="add_person" style="margin-top:17px;float:right;"> 
			<select  name="createApplicant" class="validate[required]" id="pageName">
				<option value="personal_details">Personal Details</option>
				<option value="health_details">Health Details</option>
				<option value="addresses_details">Address Details</option>
				<option value="driving_details">Driving License Details</option>
			</select> 
		</div>
--></div>
</div>
</div>
