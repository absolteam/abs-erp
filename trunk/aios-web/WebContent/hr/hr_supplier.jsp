<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">

var oTable; var selectRow=""; var aData ="";
var supplierId=0; supplierType=""; propertyOwners=""; var aSelected = [];

$(function(){
	 $('#codecombination-popup').dialog('destroy');		
	 $('#codecombination-popup').remove(); 
	 $('#job-common-popup').dialog('destroy');		
	 $('#job-common-popup').remove(); 
	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
 	
	$('.formError').remove();
	$('#SupplierDTlist').dataTable({  
		"sAjaxSource": "get_supplier_list.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
 		"aoColumns": [
			{ "sTitle": "ID","bVisible": false},
			{ "sTitle": '<fmt:message key="hr.supplier.supplierNumber" />'},
			{ "sTitle": '<fmt:message key="hr.supplier.name" />'},
			{ "sTitle": 'Supplier Type'},
			{ "sTitle": '<fmt:message key="hr.supplier.type" />'},			
			{ "sTitle": '<fmt:message key="hr.supplier.classification" />'},
			{ "sTitle": '<fmt:message key="accounts.supplier.paymentTerms" />'}, 
		],
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	

	oTable = $('#SupplierDTlist').dataTable();
	 
	$('#SupplierDTlist tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          supplierId=aData[0]; 
	          supplierType = aData[2];
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this );
	          supplierId=aData[0];
	          supplierType = aData[2];
	      }
	});
			
 	$("#add").click( function() { 
 		
 		$('#loading').fadeIn();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/supplier_add_redirect.action", 
			data:{supplierId:0,recordId:Number(0)},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#main-wrapper").html(result); 
			} 		
		}); 
		$('#loading').fadeOut();
		return false; 
	});

 	$("#edit").click( function() { 
	    if(supplierId == null || supplierId == '' || supplierId == 0){
			alert("Please select one record to edit.");
			return false; 
		}
		else{			
			$('#loading').fadeIn(); 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/supplier_edit_redirect.action", 
				data:{supplierId:supplierId,recordId:Number(0)},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$("#main-wrapper").html(result); 
				} 		
			}); 
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	});


 	$("#delete").click( function() { 
	    if(supplierId == null || supplierId == '' || supplierId == 0){
	    	alert("Please select one record to delete...");
			return false; 
		}
		else{			
			$('#loading').fadeIn(); 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/supplier_delete.action", 
				data:{supplierId:supplierId},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$("#main-wrapper").html(result); 
				} 		
			}); 
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	}); 
});
</script>

<body>
	<div id="main-content">
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;"
					class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> supplier
			</div>
			<div id="error_entry"
				class="response-msg error commonErr ui-corner-all"
				style="display: none;"></div>
			<div id="success_entry" class="success response-msg ui-corner-all"
				style="display: none;"></div>

			<div class="portlet-content">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus"
						value="${bean.sqlReturnStatus}" />
				</c:if>
			</div>
			<div id="nortify">
				<c:choose>
					<c:when test="${SUPPLIER_ERROR eq 1 && SUPPLIER_ERROR ne 0}">
						<div class="error response-msg ui-corner-all">Error</div>
					</c:when>
				</c:choose>
			</div>
			<div>
				<c:if
					test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">
						<fmt:message key="${requestScope.succMsg}" />
					</div>
				</c:if>
				<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">
						<fmt:message key="${requestScope.errMsg}" />
					</div>
				</c:if>
			</div>

			<div id="rightclickarea">
				<div id="gridDiv" style="max-height: 98%;">
					<table id="SupplierDTlist" class="display"></table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Add</span>
				</div>
				<div class="sep_li"></div>
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div style="position: relative;" id="action_buttons"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="view"
					style="display: none;">
					<fmt:message key="re.property.info.view" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="re.property.info.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="re.property.info.edit" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="add">
					<fmt:message key="re.property.info.add" />
				</div>
			</div>

		</div>
	</div>
</body>