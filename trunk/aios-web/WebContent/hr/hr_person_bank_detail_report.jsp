
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var productId =0;
var oTable;

var fromDate = null;
var toDate = null;
getData();
function getData(){ 
	
	var personType=Number($('#person').val());
	var companyType=Number($('#company').val());
	var status=$('#status').val();
	
	
	
	 $('#example').dataTable(
				{
					"sAjaxSource" : "get_person_bank_details_table_data.action?personType="
						+ personType+"&companyType="+companyType+"&status="+status,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [{
						"sTitle" : "Account Holder",
						"sDefaultContent": "n/a"
					}, {
						"sTitle" : "Bank",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Account Number",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "IBAN",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Routing Code",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Branch",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Title",
						"sDefaultContent": "n/a",
					}, 
					], 
				
				
				"sScrollY" : $("#main-content").height() - 235,
	    
	    
	
	});
	 oTable = $('#example').dataTable();
 }





					
	$(".xls-download-call").click(function(){
		
		var personType=Number($('#person').val());
 		var companyType=Number($('#company').val());
 		var status=$('#status').val();
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/get_person_bank_details_report_xls.action?personType='+personType+'&companyType='+companyType+'&status='+status,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
	
	});
	
 	$(".print-call").click(function(){ 
		 
 		var personType=Number($('#person').val());
 		var companyType=Number($('#company').val());
 		var status=$('#status').val();
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/get_person_bank_details_report_printout.action?personType='+personType+'&companyType='+companyType+'&status='+status,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
		 
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		
 		
				
 		var personType=Number($('#person').val());
 		var companyType=Number($('#company').val());
 		var status=$('#status').val();
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/get_person_bank_details_report_pdf.action?personType='+personType+'&companyType='+companyType+'&status='+status,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;	

	});

	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		height : 600,
		bgiframe : false,
		modal : true
	});
	
	$('#person').combobox({
		selected : function(event, ui) {
			getData();
		}
	});
	
	$('#status').combobox({
		selected : function(event, ui) {
			getData();
		}
	});
	
	$('#company').combobox({
		selected : function(event, ui) {
			getData();
		}
	});
</script>

<div id="main-content">
	
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>person bank details
			report

		</div>
		<div class="portlet-content">
			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;">Person Type</label> 
					<select id="person" style="width: 200px;">
						<option value="0">All</option>
						<c:forEach var="personType" items="${PERSON_TYPES}">
							<option value="${personType.lookupDetailId}">${personType.displayName}</option>
						</c:forEach>
					</select>
					
					
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;">Status</label> 
					<select id="status" style="width: 200px;">
						<option value="0">Select</option>
						<option value="true"> Active</option>
						<option value="false"> Inactive</option>
					</select>
				</div>

			</div>

			<div class="width45 float-right" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;">Company Type</label> 
					<select id="company" style="width: 200px;">
						<option value="0">All</option>
						<c:forEach var="companyType" items="${COMPANY_TYPES}">
							<option value="${companyType.dataId}">${companyType.displayName}</option>
						</c:forEach>
					</select>
					
					
				</div>


			</div>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>