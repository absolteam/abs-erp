
<tr id="experiencefieldrow_${requestScope.id}" class="experiencerowid">
	<td class="width10" id="previousExperience_${requestScope.id}"></td>
	<td class="width10" id="currentPost_${requestScope.id}"></td>
	<td class="width10" id="jobTitle_${requestScope.id}"></td>
	<td class="width10" id="companyName_${requestScope.id}"></td>
	<td class="width10" id="startDate_${requestScope.id}"></td>
	<td class="width10" id="endDate_${requestScope.id}"></td>
	<td class="width10" id="location_${requestScope.id}"></td>
	<td style="width: 5%;" id="option_${requestScope.id}"><input type="hidden"
		name="experienceId_${requestScope.id}" id="experienceId_${requestScope.id}" value="" /> <input
		type="hidden" name="experiencelineId_${i}" id="experiencelineId_${requestScope.id}"
		value="${requestScope.id}" /> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip experienceaddDatanew"
		id="experienceAddImage_${requestScope.id}" style="cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip experienceeditDatanew"
		id="experienceEditImage_${requestScope.id}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip experiencedelrownew"
		id="experienceDeleteImage_${requestScope.id}" style="cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="experienceWorkingImage_${requestScope.id}"
		style="display: none; cursor: pointer;" title="Working"> <span
			class="processing"></span> </a></td>
</tr>
<script type="text/javascript">
	$(function() {
		 $(".experienceaddDatanew").click(function(){ 
				slidetab=$(this).parent().parent().get(0);  
				//Find the Id for fetch the value from Id
				var tempvar=$(this).parent().attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				if($("#editCalendarVal").validationEngine({returnIsValid:true})){
						$("#experienceAddImage_"+rowid).hide();
						$("#experienceEditImage_"+rowid).hide();
						$("#experienceDeleteImage_"+rowid).hide();
						$("#experienceWorkingImage_"+rowid).show(); 
						
						$.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/person_experience_add.action",
							data:{id:rowid,addEditFlag:"A"},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){
							 $(result).insertAfter(slidetab);
							 $('#openFlag').val(1);
							}
					});
					
				}
				else{
					return false;
				}	
				 
			});

			$(".experienceeditDatanew").click(function(){
				 
				 slidetab=$(this).parent().parent().get(0);  
				 
				 if($("#editCalendarVal").validationEngine({returnIsValid:true})) {
					 
						 
						//Find the Id for fetch the value from Id
							var tempvar=$(this).attr("id");
							var idarray = tempvar.split('_');
							var rowid=Number(idarray[1]); 
							$("#experienceAddImage_"+rowid).hide();
							$("#experienceEditImage_"+rowid).hide();
							$("#experienceDeleteImage_"+rowid).hide();
							$("#experienceWorkingImage_"+rowid).show(); 
					 $.ajax({
						 type : "POST",
						 url:"<%=request.getContextPath()%>/person_experience_add.action",
						 data:{id:rowid,addEditFlag:"E"},
						 async: false,
					  dataType: "html",
						 cache: false,
					   success:function(result){
						
					   $(result).insertAfter(slidetab);

					 //Bussiness parameter
					 
					 $('#experiencesId').val($('#experiencesId_'+rowid).val()); 
	        			$('#previousExperience').val($('#previousExperience_'+rowid).val()); 
	        			$('#currentPost').val($('#currentPost_'+rowid).text()); 
	        			$('#jobTitle').val($('#jobTitle_'+rowid).text()); 
	        			$('#companyName').val($('#companyName_'+rowid).text());
	        			$('#startDate').val($('#startDate_'+rowid).text());
	        			$('#endDate').val($('#endDate_'+rowid).text());
	        			$('#location').val($('#location_'+rowid).text());
	        			
					    $('#openFlag').val(1);
					  
					},
					error:function(result){
					//	alert("wee"+result);
					}
					
					});
					 			
				 }
				 else{
					 return false;
				 }	
				 
			});
			 
			 $(".experiencedelrownew").click(function(){ 
				 slidetab=$(this).parent().parent().get(0); 

				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
				 var lineId=$('#experiencelineId_'+rowid).val();
				
					 $.ajax({
						 type : "POST",
						 url:"<%=request.getContextPath()%>/person_experience_save.action", 
						 data:{id:lineId,addEditFlag:"D"},
						 async: false,
					  dataType: "html",
						 cache: false,
					   success:function(result){ 
							 $('.tempresult').html(result);   
							 if(result!=null){
								 $("#experiencefieldrow_"+rowid).remove(); 
								  var experiencecount=Number($('#experiencecount').val()); 
		         				  experiencecount-=1;
		         				  $('#experiencecount').val(experiencecount);
					        		//To reset sequence 
					        		var i=0;
					     			$('.experiencerowid').each(function(){  
										i=i+1;
										$($($(this).children().get(4)).children().get(1)).val(i); 
				   					 }); 
							     }  
						},
						error:function(result){
							 $('.tempresult').html(result);   
							 return false;
						}
							
					 });
					 
				 
			 });

});	
</script>	