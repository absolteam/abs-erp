<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>

</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var accessCode=null;
var rowId=null;
$(document).ready(function(){  
	
	var payByCountId=Number($("#payByCountId").val());
	if(payByCountId!=0){
		
		if($('#isFinanceImpactTemp').val()!=null && $('#isFinanceImpactTemp').val()=='true')
			$('#isFinanceImpact').attr("checked",true);
		else
			$('#isFinanceImpact').attr("checked",false);
		
		
	}
		
	
	$('#fromDate,#toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});		


	$jquery("#AllowanceForm").validationEngine('attach');

	//Leave onchange events
	$('.discard_allowance').click(function(){
		listLoadCall();
		 return false;
	});
	
	$('#payCount').change(function(){
		var amount=0.0;
			amount=$('#payCount').val() * ($('#maxAmount').val()/$('#maxCount').val());
		$('#amount').val(amount);
		 return false;
	});

	//loadEmployeePolicy();
	
	//Save & discard process
	$('.save').click(function(){ 
		if($jquery("#AllowanceForm").validationEngine('validate')){  
			 
			var payByCountId=Number($('#payByCountId').val());
			var successMsg="";
			if(payByCountId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var jobAssignmentId=$('#jobAssignmentId').val();
			var jobPayrollElementId=$('#jobPayrollElementId').val();
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var receiptNumber=$('#receiptNumber').val();
			var isFinanceImpact=$('#isFinanceImpact').attr("checked");
			var amount=$('#amount').val();
			var description=$('#description').val();
			
			var payPeriodTransactionId=$('#payPeriod').val();
			var count=$('#payCount').val();
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pay_by_count_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		payByCountId:payByCountId,
			 		jobAssignmentId:jobAssignmentId,
			 		jobPayrollElementId:jobPayrollElementId,
			 		fromDate:fromDate,
			 		toDate:toDate,
			 		receiptNumber:receiptNumber,
			 		isFinanceImpact:isFinanceImpact,
			 		amount:amount,
			 		description:description,
			 		count:count,
			 		payPeriodTransactionId:payPeriodTransactionId
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	
	 $('.recruitment-lookup').live('click',function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
       	 rowId=getRowId($(this).attr('id')); 
         accessCode=getAccessCode($(this).attr("id"));
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
   
	 $('#save-lookup').live('click',function(){ 
	 		
			
		}); 
	 
    
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

   
     //Date process
     
     //$(".fromDate").datepicker('dateFormat', 'dd-MMM-yyyy',"option", "minDate", new Date(Date.parse($("#periodStartDate").val())));
     //$(".toDate").datepicker('dateFormat', 'dd-MMM-yyyy',"option", "maxDate", new Date(Date.parse($("#periodEndDate").val())));
     
      $('.fromDate,.toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'}); 
     
    

});
function listLoadCall(){
	var params="id=0";
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var transactionDate=$('#transactionDate').val();
	var payPolicy=$('#payPolicy').val();
	var payPeriodTransactionId=$('#payPeriod').val();

	params+="&jobPayrollElementId="+jobPayrollElementId;
	params+="&jobAssignmentId="+jobAssignmentId;
	params+="&transactionDate="+transactionDate;
	params+="&payPolicy="+payPolicy;
	params+="&payPeriodTransactionId="+payPeriodTransactionId;

	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/pay_by_count.action?"+params, 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#allowance-manipulation-div").html(result);  
				return false;
			}
	 });
}
function loadEmployeePolicy(){
	var jobAssignmentId=$('#jobAssignmentId').val();
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/grade_view.action", 
		 	async: false,
		 	data:{jobAssignmentId:jobAssignmentId},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#grade-div").html(result);  
				$('.discard').hide();
				return false;
			}
	 });
}
function customRanges(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function getAccessCode(id){
	var idval=id.split('_');
	var acccessCode=idval[0];
	return acccessCode;
}

function customRanges(dates) {
	var rowId=getRowId(this.id); 
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		if($(this).val()!=""){
			return false;
		} 
		else{
			return false;
		}
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
		if($(this).val()!=""){
			return false;
		} 
		else{
			return false;
		}
	} 
}

function editDataSelectCall(){
	$('.rowidC').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#paymentType_"+rowId).val($("#paymentTypeId_"+rowId).val());
});
	
	
	
}

</script>

<div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay
			Information
		</div>

		<div class="portlet-content">
			<div id="page-error" class="response-msg error ui-corner-all width90"
				style="display: none;">
				<span>Process Failure!</span>
			</div>
			<div class="tempresult" style="display: none;"></div>
			<form id="AllowanceForm" class="" name="AllowanceForm" method="post" style="position: relative;">
				<input type="hidden" name="payByCountId"
					id="payByCountId" value="${ALLOWANCE.payByCountId}"
					class="width50" />
				<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="min-height: 150px;">
							<legend>Account Details </legend>
							<div class="width35 float-left">
								<div>
									<label class="width30 " for="count">Value / Count<span
										class="mandatory">*</span> </label> <input type="text" name="payCount"
										id="payCount" value="${ALLOWANCE.count}"
										class="width50 validate[required] " />
								</div>
								<div class=" ">
									<label class="width30 ">Total Amount<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" id="amount" name="amount"
										class="width50 validate[required] "
										value="${ALLOWANCE.amount}">
								</div>
								<div class=" ">
									<label class="width30 ">Voucher/Receipt Number</label> <input
										type="text" id="receiptNumber" name="receiptNumber"
										class="width50 " value="${ALLOWANCE.receiptNumber}">
								</div>
								<div class=" ">
									<label class="width30">Finance Impact</label> <input
										type="checkbox" id="isFinanceImpact" name="isFinanceImpact"
										class="width10"> <input type="hidden"
										id="isFinanceImpactTemp" name="isFinanceImpactTemp"
										class="width10"
										value="${ALLOWANCE.isFinanceImpact}"> 
								</div>

							</div>
							<div class="width40 float-left">
								
								<div>
									<label class="width30" for="fromDate">From Date</label> <input type="text"
										readonly="readonly" name="fromDate" id="fromDate"
										value="${ALLOWANCE.fromDateView}" class="width50" />
								</div>
								<div>
									<label class="width30" for="toDate">To Date<span
										class="mandatory">*</span>
									</label> <input type="text" readonly="readonly" name="toDate"
										id="toDate" value="${ALLOWANCE.toDateView}"
										class="width50" />
								</div>
								<div>
									<label class="width30">Description</label> <textarea 
										id="description" name="description" class="width50 "
										>${ALLOWANCE.description}</textarea>
								</div>
							</div>
							<div class="width20 float-left">
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Pay Elment : </label>
									<label class="width40 ">${JOBPAY_INFO.elementName}</label>
									<input type="hidden" id="jobPayrollElementId" value="${JOBPAY_INFO.jobPayrollElementId}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period From : </label>
									<label class="width40 ">${JOBPAY_INFO.periodStartDate}</label>
									<input type="hidden" id="periodStartDate" value="${JOBPAY_INFO.periodStartDate}"/>
									
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period To : </label>
									<label class="width40 ">${JOBPAY_INFO.periodEndDate}</label>
									<input type="hidden" id="periodEndDate" value="${JOBPAY_INFO.periodEndDate}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 " >Amount For(value /count = ${JOBPAY_INFO.count}) : </label>
									<label class="width40 ">${JOBPAY_INFO.allowedMaixmumAmount}</label>
									<input type="hidden" id="maxAmount" value="${JOBPAY_INFO.allowedMaixmumAmount}"/>
									<input type="hidden" id="maxCount" value="${JOBPAY_INFO.count}"/>
								</div>
							
							</div>
							
							<div class="clearfix"></div>
								<div class="float-right buttons ui-widget-content ui-corner-all">
									<div class="portlet-header ui-widget-header float-right discard_allowance"
										id="discard_allowance">
										<fmt:message key="common.button.cancel" />
									</div>
									<div class="portlet-header ui-widget-header float-right save"
										id="job_save">
										<fmt:message key="organization.button.save" />
									</div>
								</div>
								<div id="common-popup" class="ui-dialog-content ui-widget-content"
									style="height: auto; min-height: 48px; width: auto;">
									<div class="common-result width100"></div>
									<div
										class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
										<button type="button" class="ui-state-default ui-corner-all">Ok</button>
										<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
									</div>
								</div>
						</fieldset>
						
					</div>
					<div class="width100 float-left" id="grade-div">
					</div>
				</div>
			</form>

		</div>
		

	</div>

</div>


