<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style>
.ui-multiselect .count{
	padding:0!important;
}
.hr_main_content{
	overflow:hidden;
}
</style> 

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 

	
	$jquery("#candidate-details").validationEngine('attach');
	
		var candidateId=Number($("#candidateId").val());
		if(candidateId>0){
			
			autoSelectTheMultiList("candidateAvailabilityTemp","candidateAvailability");
			autoSelectTheMultiList("statusTemp","candidateStatus");
			var personId=Number($("#personIdTemp").val());
			personInfoLoadCall(personId);
		}else{
			personInfoLoadCall(0);
			
			
		}
		
		
		
		$('#RECRUITMENT_RESOURCES').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/common_recruitment_source_list.action",
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){

	                   $('.common-result').html(result);
	                   return false;
	              },
	              error:function(result){

	                   $('.common-result').html(result);
	              }
	          });
	           return false;
	  	}); 
		
	   
	   $('#OPEN_POSITION').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/common_open_position_list.action",
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	   $('#ADVERTISING_PANEL').click(function(){
		   $('.error,.success').hide();
	         var recruitmentSourceId=$('#recruitmentSourceId').val();
	         var openPositionId=$('#openPositionId').val();
	         if(recruitmentSourceId==0 || recruitmentSourceId==''){
	        	 $('#candidate-page-error').hide().html("Please select recruitment source").slideDown(1000);
	        	 return false;
	         }
	        	 
	         if(openPositionId==0 || openPositionId==''){
	        	 $('#candidate-page-error').hide().html("Please select open position").slideDown(1000);
	        	 return false;
	         }
	         
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/common_advertising_panel_list.action",
	               data:{
	            	   openPositionId:openPositionId,
	            	   recruitmentSourceId:recruitmentSourceId
	            	   },
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	   $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	   $('#employeepopup').click(function(){
	          $('.ui-dialog-titlebar').remove();  
	          $('#common-popup').dialog('open');
	          $('.common-result').html("");
	          var rowId=-1; 
	          var personTypes="ALL";
	             $.ajax({
	                type:"POST",
	                url:"<%=request.getContextPath()%>/common_person_list.action",
	                data:{id:rowId,personTypes:personTypes},
	                async: false,
	                dataType: "html",
	                cache: false,
	                success:function(result){
	                     $('.common-result').html(result);
	                     return false;
	                },
	                error:function(result){
	                     $('.common-result').html(result);
	                }
	            });
	             return false;
	    }); 
	  
		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
		
		
			
		$('#candidate-discard').click(function(){ 
			returnCallTOList();
			
		 });
		
		
	 $('#person_save_button').live('click',function(){
		try {
			if ($jquery("#candidate-details").validationEngine('validate')) {
			} else {
				return false;
			}
		} catch (e) {

		}
		return false
	});
	 $('#candidate-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
		 	
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(candidateId>0)
						successMessage="Successfully Modified";

					try {
						if ($jquery("#candidate-details").validationEngine('validate')) {
							//generateDataToSendToDB("responsibilityList","responsibility");
							
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/candidate_save.action", 
							 	async: false,
							 	data: $("#candidate-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					} catch (e) {

					}

				});
	 
	 
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="CANDIDATE_AVAILABILITY"){
				$('#candidateAvailability').html("");
				$('#candidateAvailability').append("<option value=''>--Select--</option>");
				loadLookupList("candidateAvailability");
				
			}
		});
	 
	 
		 $('#createdDate').datepick();
		 $('#expectedJoiningDate').datepick();
		 
	});
	function saveCandidateCall(){
		var errorMessage=null;
		var successMessage="Successfully Created";
		if(candidateId>0)
			successMessage="Successfully Modified";

		try {
			if ($jquery("#candidate-details").validationEngine('validate')) {
				//generateDataToSendToDB("responsibilityList","responsibility");
				
			
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/candidate_save.action", 
				 	async: false,
				 	data: $("#candidate-details").serialize(),
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						var message=result.trim();
						 if(message=="SUCCESS"){
							 returnCallTOList();
						 }else{
							 errorMessage=message;
						 }
						 
						 if(errorMessage==null)
								$('.success').hide().html(successMessage).slideDown();
							else
								$('.error').hide().html(errorMessage).slideDown();
					},
					error:function(result){ 
						errorMessage=result.trim();
						
						 if(errorMessage==null)
								$('.success').hide().html(successMessage).slideDown();
							else
								$('.error').hide().html(errorMessage).slideDown();
					}
			 });
			} else {
				//$('.error').hide().html("Please enter all required information").slideDown(1000);
				//event.preventDefault();
				return false;
			}
		} catch (e) {

		}
	}
	function personInfoLoadCall(personId){
		if(typeof($('#codecombination-popup')!="undefined")){ 
			$('#codecombination-popup').dialog('destroy');		
			$('#codecombination-popup').remove(); 
		} 
		 if(typeof($('#job-common-popup')!="undefined")){  
			 $('#job-common-popup').dialog('destroy');		
			 $('#job-common-popup').remove(); 
		 }
		if(typeof($('#qualification-common-popup')!="undefined")){  
				 $('#qualification-common-popup').dialog('destroy');		
				 $('#qualification-common-popup').remove(); 
			 }
		$('#person-selection-div').show();
		$("#sub-content").html(""); 
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
			data: {personId: personId, isCustomer: false, isSupplier: false},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result); 
				$('#personTypeIdTemp').val("1,"+$('#personTypeIdTemp').val());
				personTypeAutoSelectd();
			$('#personType').attr('disabled', true);
			$("#candidateinfo").val(true);
				return false;
			},
			error: function(result) { 
				$("#sub-content").html(result); 
			} 
		}); 
		
		
		return false;
	}
	
	
	function returnCallToCandidateList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/candidate_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
	
	function autoSelectTheMultiList(source,target) {
		 var idsArray=new Array();
		 var idlist=$('#'+source).val();
		 idsArray=idlist.split(',');
		$('#'+target+' option').each(function(){
			var txt=$(this).val().trim();
			if($.inArray(txt, idsArray)>=0)
				$(this).attr("selected","selected");
		});

	}
	
	function personPopupResult(personid,personname,commonParam){
		$("#hr_main_content").remove(); 
		$("#employeeName").val(personname);
		personInfoLoadCall(personid);
		
		return false;
	}

	function selectedOpenPositionResult(openPositionVO){
		$("#openPositionId").val(openPositionVO.openPositionId);
		$("#positionReferenceNumber").val(openPositionVO.positionReferenceNumber);
		$("#numberOfPosition").val(openPositionVO.numberOfPosition);
		$("#job").val(openPositionVO.designationName);
		
	}
	
	function selectedRecruitmentSourceResult(recritmentSourceVO){
		$("#recruitmentSourceId").val(recritmentSourceVO.recruitmentSourceId);
		$("#providerName").val(recritmentSourceVO.providerName);
		$("#sourceType").val(recritmentSourceVO.sourceType);
	}
	
	function selectedAdvertisingPanelResult(advertisingPanelVO){
		$("#advertisingPanelId").val(advertisingPanelVO.advertisingPanelId);
		$("#referenceNumber").val(advertisingPanelVO.referenceNumber);
		
	}
	
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Candidate Entry</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="candidate-page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="candidate-details" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden"
									name="candidate.candidateId"
									id="candidateId"
									value="${CANDIDATE_INFO.candidateId}"
									class="width50" />
									
								<fieldset style="min-height: 200px;">
									<legend>Candidate Information</legend>
									<div id="hrm" class="width50 float-left">
										<div>
											<label class="width30">Person </label> <input type="text"
												class="width50 employeeName" id="employeeName"
												value="${CANDIDATE_INFO.personByPersonId.firstName} ${CANDIDATE_INFO.personByPersonId.lastName}" />

											<span class="button" id="employee"
												style="position: relative;"> <a
												style="cursor: pointer;" id="employeepopup"
												class="btn ui-state-default ui-corner-all width10 person-popup">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" class="personIdTemp" id="personIdTemp"
												value="${CANDIDATE_INFO.personByPersonId.personId}" />
										</div>
										<div>
											<label class="width30">Recruitment Source<span
												style="color: red">*</span></label> <input type="hidden"
												name="candidate.recruitmentSource.recruitmentSourceId"
												id="recruitmentSourceId"
												value="${CANDIDATE_INFO.recruitmentSource.recruitmentSourceId}" />
											<div>
												<input type="text" id="providerName" readonly="readonly"
													class="width50 validate[required]"
													value="${CANDIDATE_INFO.recruitmentSource.providerName}" />
												<span class="button" id="RECRUITMENT_RESOURCES_1"
													style="position: relative;"> <a
													style="cursor: pointer;" id="RECRUITMENT_RESOURCES"
													class="btn ui-state-default ui-corner-all width10"> <span
														class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</div>

										</div>
										<div>
											<label class="width30">Open Position<span
												style="color: red">*</span> </label> <input type="hidden"
												name="candidate.openPosition.openPositionId"
												id="openPositionId"
												value="${CANDIDATE_INFO.openPosition.openPositionId}" />
											<div>
												<input type="text" id="positionReferenceNumber"
													readonly="readonly" class="width50 validate[required]"
													value="${CANDIDATE_INFO.openPosition.positionReferenceNumber}" />
												<span class="button" id="OPEN_POSITION_1"
													style="position: relative;"> <a
													style="cursor: pointer;" id="OPEN_POSITION"
													class="btn ui-state-default ui-corner-all width10"> <span
														class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</div>

										</div>
										<div>
											<label class="width30">Ad. Reference</label> <input type="hidden"
												name="candidate.advertisingPanel.advertisingPanelId"
												id="advertisingPanelId"
												value="${CANDIDATE_INFO.advertisingPanel.advertisingPanelId}" />
											<div>
												<input type="text" id="referenceNumber" readonly="readonly"
													class="width50"
													value="${CANDIDATE_INFO.advertisingPanel.referenceNumber}" />
												<span class="button" id="ADVERTISING_PANEL_1"
													style="position: relative;"> <a
													style="cursor: pointer;" id="ADVERTISING_PANEL"
													class="btn ui-state-default ui-corner-all width10"> <span
														class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</div>

										</div>
										<div>
											<label class="width30">Status<span style="color: red">*</span>
											</label>
												<select id="candidateStatus" name="candidate.status"
													class="width51 validate[required]">
													<c:forEach items="${STATUS_TYPE}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="statusTemp" id="statusTemp"
													value="${CANDIDATE_INFO.status}" />
										</div>
									
									</div>
									<div id="hrm" class="width50 float-left">
										<div>
											<label class="width30">Availability</label>
											<div>
												<select id="candidateAvailability"
													name="candidate.lookupDetail.lookupDetailId"
													class="width60">
													<option value="">--Select--</option>
													<c:forEach items="${CANDIDATE_AVAILABILITY}" var="nltls">
														<option value="${nltls.lookupDetailId}">${nltls.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="CANDIDATE_AVAILABILITY_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="CANDIDATE_AVAILABILITY"
													class="btn ui-state-default ui-corner-all recruitment-lookup width8">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="candidateAvailabilityTemp"
													id="candidateAvailabilityTemp"
													value="${CANDIDATE_INFO.lookupDetail.lookupDetailId}" />
											</div>
										</div>
										<div>
											<label class="width30">Description </label> <textarea 
												id="candidateDescription" class="width61"
												name="candidate.description"
												>${CANDIDATE_INFO.description}</textarea>
										</div>
										
										<div>
											<label class="width30">Expected Joining Date</label> <input
												type="text" class="width60" readonly="readonly"
												name="expectedJoiningDate" id="expectedJoiningDate"
												value="${CANDIDATE_INFO.expectedJoiningDateDisplay}" />
										</div>
										<div>
											<label class="width30">Created Date</label> <input
												type="text" class="width60" readonly="readonly"
												name="createdDate" id="createdDate"
												value="${CANDIDATE_INFO.createdDateDisplay}" />
										</div>
									</div>
								</fieldset>

									
							</div>

							
						</div>

					</form>
					<div class="clearfix"></div>
					<div id="sub-content"></div>
				<!-- 	<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					
						<div
							class="portlet-header ui-widget-header float-right consultancy-discard"
							id="candidate-discard">cancel</div>
						<div class="portlet-header ui-widget-header float-right consultancy-save"
							id="candidate-save">save</div>
					</div> -->
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

		
			</div>
		</div>
	</div>
	
</body>
</html>