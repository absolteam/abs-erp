<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var personId=0;var personName='';var personTypes="";
$(function(){ 
	$($($('#common-popup').parent()).get(0)).css('top',0+'px !important;'); 
	personTypes=$('#personTypesToList').val();
	$('#Person_DetailDT').dataTable({ 
		"sAjaxSource": "common_person_list_json.action?personTypes="+personTypes,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.recordid"/>', "bVisible": false,"bSearchable": false},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.firstname"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.lastname"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.persontype"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.country"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.personid"/>'},
			{ "sTitle": '<fmt:message key="hr.common.email"/>'},
			{ "sTitle": '<fmt:message key="hr.person.mobile"/>'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Person_DetailDT').dataTable();

	/* Click event handler */
	$('#Person_DetailDT tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2]+" ["+aData[5]+"]";
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2]+" ["+aData[5]+"]";
	    }
		 return false;
	});
	$('#Person_DetailDT tbody tr').live('dblclick', function (e) { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2]+" ["+aData[5]+"]";
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2]+" ["+aData[5]+"]";
	    }
		  var commonParam="";
		  
		personDetailPopupResult(personId,personName,commonParam , $('#personRowId').val());
		 $('#common-popup').dialog("close");
		 $('#holder-popup').dialog("close");
		 $('#Person_DetailDT').remove();
		 e.preventDefault();
		 return false;
	});
	
	$('#person-detail-list-close').click(function () { 
		$('#common-popup').dialog("close");
		 $('#holder-popup').dialog("close");
	});
	
});
</script>
<div id="main-content"> 
	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>${POPUP_TITLE}</div>
	<input type="hidden" id="personTypesToList" name="personTypesToList" value="${PERSON_TYPES}"/>
	<input type="hidden" id="personRowId" name="personRowId" value="${requestScope.id}"/>
 	<div id="trans_combination_accounts">
		<table class="display" id="Person_DetailDT"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="person-detail-list-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div> 
</div>