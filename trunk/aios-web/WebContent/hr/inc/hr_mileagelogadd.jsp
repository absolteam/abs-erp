<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style>
#ui-datepicker-div {
	background-color: #cfcfcf !important;
	/* left: 731.5px !important; */
	/* top: 124.967px !important; */
	width: 231px !important;
	z-index: 751!important;
	
}
.ui-widget-content {
    background: url("images/ui-bg_flat_75_ffffff_20x100.png") repeat-x scroll 50% 50% #FFFFFF;
    color: #444444;
}

.ui-slider-horizontal {
    border: 1px solid #CCCCCC;
    margin: 3px 0 4px 5px;
   
}
</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var accessCode=null;
var commonRowId=0;
var rowId=null;
$(document).ready(function(){  
	
	var mileageLogId=Number($("#mileageLogId").val());
	if(mileageLogId!=0){
		$('#jobAssignmentId').val($('#jobAssignmentIdTemp').val());
		$('#vehicleNumber').val($('#vehicleNumberTemp').val());
		$('#source').val($('#sourceTemp').val());
		$('#destination').val($('#destinationTemp').val());
		$('#fuelAllowanceId').val($('#fuelAllowanceIdTemp').val());
	}
	
	$('#start,#end').live('change',function(){
		if($(this).val()!=""){
			var start=Number($('#start').val());
			var end=Number($('#end').val());
			if($('#start').val()!=null && $('#start').val()!=null 
					&& $('#end').val()!="" && $('#end').val()!="" && start>end){
				alert("Please enter valid meter reading");
				return false;
			}
			$('#mileage').val(end-start);
				
			
		} 
		else{
			return false;
		}
	});	
	
	$('#date').datepick();	
	$('#timePick').timepicker({});	

	
	$jquery("#JOBDetails").validationEngine('attach');
	//Leave onchange events
	$('.discard').click(function(){
		listLoadCall();
		 return false;
	});
	

	

	
	
	//Save & discard process
	$('.save').click(function(){ 
		if($jquery("#JOBDetails").validationEngine('validate')){  
			 
			var mileageLogId=Number($('#mileageLogId').val());
			var successMsg="";
			if(mileageLogId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var jobAssignmentId=$('#jobAssignmentId').val();
			var vehicleNumber=$('#vehicleNumber').val();
			var date=$('#date').val();
			var time=$('#timePick').val();
			var source=$('#source').val();
			var destination=$('#destination').val();
			var start=$('#start').val();
			var end=$('#end').val();
			var purpose=$('#purpose').val();
			var fuelAllowanceId=$('#fuelAllowanceId').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/mileage_log_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		mileageLogId:mileageLogId,
			 		fuelAllowanceId:fuelAllowanceId,
			 		jobAssignmentId:jobAssignmentId,
			 		vehicleNumber:vehicleNumber,
			 		date:date,
			 		time:time,
			 		source:source,
			 		destination:destination,
			 		start:start,
			 		end:end,
			 		purpose:purpose
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	
	 $('.recruitment-lookup').live('click',function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
        rowId=getRowId($(this).parent().attr('id'));
         accessCode=$(this).attr("id"); 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
   
	 $('#save-lookup').live('click',function(){ 
	 		
		 if(accessCode=="VEHICLE_NUMBER"){
				$('#vehicleNumber').html("");
				$('#vehicleNumber').append("<option value=''>--Select--</option>");
				loadLookupList("vehicleNumber");
			}
		 if(accessCode=="LOCATION"){
				$('#source').html("");
				$('#source').append("<option value=''>--Select--</option>");
				loadLookupList("source");
		    }
		 if(accessCode=="LOCATION"){
				$('#destination').html("");
				$('#destination').append("<option value=''>--Select--</option>");
				loadLookupList("destination");
		    }	
			
			
		}); 
	 
	
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

   
  
     
});
function listLoadCall(){

	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/mileage_log.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#main-wrapper").html(result);  
				return false;
			}
	 });
}
function customRanges(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function getAccessCode(id){
	var idval=id.split('_');
	var acccessCode=idval[0];
	return acccessCode;
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Mileage Log</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
			<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
		  <form id="JOBDetails" class="" name="JOBDetails" method="post" style="position: relative;">
		  	<input type="hidden" name="mileageLogId" id="mileageLogId" value="${ALLOWANCE.mileageLogId}" class="width50"/>
		  	<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="min-height: 150px;">
							<legend>Mileage Information</legend>
							<div class="width50 float-left">
								<div class="float-left width100">
									<label class="width30" for="jobAssignmentId"><fmt:message
											key="hr.job.employees" /><span class="mandatory">*</span> </label> <select
										name="jobAssignmentId" id="jobAssignmentId"
										class="width50 validate[required]">
										<option value="">Select</option>
										<c:forEach items="${JOB_ASSIGNMENT_LIST}" var="job"
											varStatus="status">
											<option value="${job.jobAssignmentId}">${job.person.firstName}
												${job.person.lastName} [${job.jobAssignmentNumber}]</option>
										</c:forEach>
									</select>
									 <input
											type="hidden" name="jobAssignmentIdTemp"
											id="jobAssignmentIdTemp"
											value="${ALLOWANCE.mileageLog.jobAssignment.jobAssignmentId}" />
								</div>
								<div>
									<label class="width30">Vehicle Number<span
										style="color: red">*</span> </label>
									<div>
										<select id="vehicleNumber"
											name="vehicleNumber"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${VEHICLE_NUMBER}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="VEHICLE_NUMBER_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="VEHICLE_NUMBER"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="vehicleNumberTemp"
											id="vehicleNumberTemp"
											value="${ALLOWANCE.mileageLog.lookupDetailByVehicleNumber.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Source</label>
									<div>
										<select id="source"
											name="source"
											class="width50">
											<option value="">--Select--</option>
											<c:forEach items="${LOCATION}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="LOCATION_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="LOCATION"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="sourceTemp"
											id="sourceTemp"
											value="${ALLOWANCE.mileageLog.lookupDetailBySource.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Destination</label>
									<div>
										<select id="destination"
											name="destination"
											class="width50">
											<option value="">--Select--</option>
											<c:forEach items="${LOCATION}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="LOCATION_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="LOCATION"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="destinationTemp"
											id="destinationTemp" value="${ALLOWANCE.mileageLog.lookupDetailByDestination.lookupDetailId}" />
									</div>
								</div>
								<div class="float-left width100">
									<label class="width30" for="fuelAllowanceId">Fuel Allowance</label> <select
										name="fuelAllowanceId" id="fuelAllowanceId"
										class="width50 ">
										<option value="">Select</option>
										<c:forEach items="${FUEL_ALLOWANCES}" var="fuel"
											varStatus="status">
											<option value="${fuel.fuelAllowanceId}">${fuel.fuelAllowanceId}
												${fuel.lookupDetailByVehicleNumber.displayName}</option>
										</c:forEach>
									</select>
									 <input
											type="hidden" name="fuelAllowanceIdTemp"
											id="fuelAllowanceIdTemp" value="${ALLOWANCE.mileageLog.fuelAllowance.fuelAllowanceId}" />
								</div>
							</div>
							<div class="width50 float-left">
							
								<div>
									<label class="width30">Start Meter<span
										style="color: red">*</span></label>
									<input type="text" id="start" name="start" class="width50 validate[required,custom[number]]" value="${ALLOWANCE.start}">
								</div>
								<div>
									<label class="width30">End Meter<span
										style="color: red">*</span></label>
									<input type="text" id="end" name="end" class="width50  validate[required,custom[number]]" value="${ALLOWANCE.end}">
								</div>
								<div>
									<label class="width30">Total Mileage</label>
									<input type="text" id="mileage" name="mileage" readonly="readonly" class="width50" value="${ALLOWANCE.mileage}">
								</div>
							
								<div>
									<label class="width30" for="fromDate">Date<span
										style="color: red">*</span> 
										
									</label> <input type="text" readonly="readonly" name="date"
										id="date" value="${ALLOWANCE.date}"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="time">Time<span
										style="color: red">*</span>
										</label> <input type="text" 
										name="timePick" id="timePick"
										value="${ALLOWANCE.time}" class="width50 timePick validate[required]" />
								</div>
								<div>
									<label class="width30">Purpose</label> <textarea 
										id="purpose" name="purpose" class="width50 "
										>${ALLOWANCE.mileageLog.purpose}</textarea>
								</div>
							
							</div>
							
						</fieldset>
					</div>
			</div>
 		</form>
		
					
	
		</div>
		<div class="clearfix"></div>
		<div class="float-right buttons ui-widget-content ui-corner-all">
			<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right save" id="job_save"><fmt:message key="organization.button.save"/></div>
		</div>
		<div id="common-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;display:none;"> 
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
		
</div>

 
	

