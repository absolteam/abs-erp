<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
	
<c:choose>
	<c:when
		test="${EMPLOYEE_LOAN_REPAYMENT ne null && EMPLOYEE_LOAN_REPAYMENT ne '' && fn:length(EMPLOYEE_LOAN_REPAYMENT)>0}">
		<c:forEach var="paym"
			items="${EMPLOYEE_LOAN_REPAYMENT}" varStatus="status1">
			<tr class="rowidS" id="fieldrowS_${status1.index+1}">
				<td id="lineIdS_${status1.index+1}"
					style="display: none;">${status1.index+1}</td>
				<td>${status1.index+1}</td>
				<td>${paym.dueDate}</td>
				<td>${paym.pricipalBegining}</td>
				<td>${paym.installment}</td>
				<td>${paym.intrestRate}</td>
				<td>${paym.principal}</td>
				<td>${paym.principalEnd}</td>
				<td>${paym.status}</td>
			</tr>
		</c:forEach>
	</c:when>
</c:choose>
	