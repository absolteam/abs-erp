<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">
.hr_main_content{
	overflow-y: hidden;
}
</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
$(function (){  
	
   $jquery("#JOB_ASSIGNMENT").validationEngine('attach');
	
	
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#JOB_ASSIGNMENT").validationEngine('validate')){ 
			var jobAssignmentId=Number($('#jobAssignmentId').val());
			var jobAssignmentNumber=$('#jobAssignmentNumber').val();
			
			var jobId=Number($('#jobId').val());
			var personId=Number($('#personId').val());
			var designationId=Number($('#designationId').val());
			var cmpDeptLocId=Number($('#cmpDeptLocId').val());
			var personBankId=Number($('#personBankId').val());
			
			var swipeId=Number($('#swipeId').val());
			var payMode=$('#payMode').val();
			var effectiveDate=$('#effectiveDate').val();
			var endDate=$('#endDate').val();
			var rejoiningDate=$('#rejoiningDate').val();
			var payPolicy=$('#payPolicy').val();
			var accomodationType=$('#accomodationType').val();
			
			var isActive=$('#isActive').attr("checked");
			var isLocalPay=$('#isLocalPay').attr("checked");
			var isPrimary=$('#isPrimary').attr("checked");
			
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(jobAssignmentId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/job_assignment_save.action",
				data: {
					jobAssignmentId:jobAssignmentId,
					jobAssignmentNumber:jobAssignmentNumber,
					jobId:jobId,
					personId:personId,
					designationId:designationId,
					cmpDeptLocId:cmpDeptLocId,
					personBankId:personBankId,
					swipeId:swipeId,
					payMode:payMode,
					effectiveDate:effectiveDate,
					endDate:endDate,
					isActive:isActive,
					isLocalPay:isLocalPay,
					isPrimary:isPrimary,
					payPolicy:payPolicy,
					accomodationType:accomodationType,
					rejoiningDate:rejoiningDate
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('#success').hide().html(successMessage).slideDown();
						else
							$('#page-error').hide().html(errorMessage).slideDown(3000);
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 $('#page-error').hide().html(message).slideDown(3000); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		returnCallTOList();
		$('#loading').fadeOut();
	});

   $('#desigination-close').live('click',function() { 
	   $('#common-popup').dialog('close');
   });

   $('#person-list-close').live('click',function() { 
	   $('#common-popup').dialog('close');
   });

   $('#job-list-close').live('click',function() { 
	   $('#common-popup').dialog('close');
   });
   $('#department-list-close').live('click',function() { 
	   $('#common-popup').dialog('close');
   });
   $('#hrbank-list-close').live('click',function() { 
	   $('#job-common-popup').dialog('close');
   });
 //pop up process area
	//pop-up config
	 $('#jobpopup').click(function() { 
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var designationId=$('#designationId').val();
         var rowId=0; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_job_list.action",
               data:{id:rowId,designationId:designationId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
 
	 $('#employeepopup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var rowId=-1; 
         var personTypes="ALL";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 $('#locationpopup').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
         var rowId=-1; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
               data:{id:rowId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
 
   
   $('#designationpopup').click(function() { 
       $('.ui-dialog-titlebar').remove();  
       $('#common-popup').dialog('open');
       var rowId=0; 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/common_designation_list.action",
             data:{id:rowId},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.common-result').html(result);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 }); 
   
     
   $('#personbankpopup').click(function() { 
   	$('.error').hide();
   	 $('.ui-dialog-titlebar').remove();
   	var rowId=0; 
   	var personId= Number($('#personId').val());
   	if(personId>0){
       $('#job-common-popup').dialog('open');
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/get_person_bank_list.action",
             data:{id:rowId,personId:personId},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.job-common-result').html(result);
                  return false;
             },
             error:function(result){
                  $('.job-common-result').html(result);
             }
         });
   	}else{
   		$('.error').html("Selete Employee to get Bank Accounts").show();
   	}
          return false;
 }); 
   
   $('.job-assignment-lookup').click(function(){
       $('.ui-dialog-titlebar').remove(); 
       $('#common-popup').dialog('open');
       accessCode=$(this).attr("id"); 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode:accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.common-result').html(result);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	}); 
 
   
    $('#job-common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
    
    $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:550,
		bgiframe: false,
		modal: true 
	});

    $('#identity-close').live('click',function(){
   		 $('#common-popup').dialog('close');
    });
    
    //Date process
    
    $('#effectiveDate,#endDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
    
    
    $('#rejoiningDate').datepick();
    
    var jobAssignmentId=Number($('#jobAssignmentId').val());
	if(jobAssignmentId>0){
		$('#payPolicy').val($('#payPolicyTemp').val());
		$('#payMode').val($('#payModeTemp').val());
		$('#accomodationType').val($('#accomodationTypeTemp').val());
		//loadCurrentEmployeePolicy();
		//loadCurrentPayTemplate();
	}else{
		$('#isActive').attr("checked",true);
		$('#isLocalPay').attr("checked",true);
	}
	  
    
});

function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
   function returnCallTOList(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/job_assignment_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#codecombination-popup').dialog('destroy');
				$('#codecombination-popup').remove();	
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();	
				$("#main-wrapper").html(result); 
			}
	 	});
	}
   
   function jobPopupResult(id,name,commaseparatedValue){
		$('#jobName').val(name);
		$('#jobId').val(id);
	}
   
   function personPopupResult(personId,personName,commonParam){
		$('#employeeName').val(personName);
		$('#personId').val(personId);
	}

	function departmentBranchPopupResult(locationId,locationName,commaseparatedValue){
		$('#locationName').val(locationName);
		$('#cmpDeptLocId').val(locationId);
		
	}
  
  function designationPopupResult(id,name,commaseparatedValue){
		$('#designationName').val(name);
		$('#designationId').val(id);
	}
  
 
   function personBankPopupCall(id,name,commaseparatedValue){
		$('#personBank').val(name);
		$('#personBankId').val(id);
	}
   
  
   
   function customRanges(dates) {
		if (this.id == 'effectiveDate') {
			$('#endDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#effectiveDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
   
   function loadCurrentEmployeePolicy(){
		//clearTabs();
		var jobAssignmentId=$('#jobAssignmentId').val();
		var openPositionId=0;
		if(jobAssignmentId!=null && jobAssignmentId>0)
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/grade_view.action", 
			 	async: false,
			 	data:{jobAssignmentId:jobAssignmentId,openPositionId:openPositionId},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".current_grade").html(result);  
					$('.discard').hide();
					return false;
				}
		 });
		else
			return false;
	}

	function loadCurrentPayTemplate(){
		//clearTabs();
		var jobAssignmentId=$('#jobAssignmentId').val();
		if(jobAssignmentId!=null && jobAssignmentId>0)
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_template_view.action", 
			 	async: false,
			 	data:{jobAssignmentId:jobAssignmentId},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".current_job").html(result);  
					$('.discard').hide();
					return false;
				}
		 });
		else
			return false;
	}

</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Job Assignment Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" id="page-error" style="display:none;"></div>
			<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
			<form id="JOB_ASSIGNMENT" name="JOB_ASSIGNMENT" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" id="jobAssignmentId" value="${JOB_ASSIGNMENT.jobAssignmentId}" />
					<fieldset style="min-height: 170px;">
						   
					       	<div>
					       		<label class="width30">Assignment Number<span style="color:red">*</span></label>
								<input type="text" readonly="readonly" class="width50 jobAssignmentNumber" 
									name="jobAssignmentNumber" id="jobAssignmentNumber" value="${JOB_ASSIGNMENT.jobAssignmentNumber}"/>
							</div> 
							<div>
								<label class="width30">Designation<span style="color:red">*</span></label>
								<input type="text"  readonly="readonly"  id="designationName" 
									class="width50 validate[required]" value="${JOB_ASSIGNMENT.designationName}"/>
								<span class="button" id="designation"  style="position: relative; top:0px;">
									<a style="cursor: pointer;" id="designationpopup" class="btn ui-state-default ui-corner-all width10"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
								<input type="hidden" name="designationId" id="designationId" value="${JOB_ASSIGNMENT.designation.designationId}"/>  
							</div>
							<div>
				      	  		<label class="width30">Job Template<span style="color:red">*</span></label>
								<input type="text" id="jobName" readonly="readonly" class="width50 validate[required]"
									value="${JOB_ASSIGNMENT.jobName}"/>
								
								<span class="button" id="job"  style="position: relative; top:0px;">
									<a style="cursor: pointer;" id="jobpopup" class="btn ui-state-default ui-corner-all width10"> 
										<span class="ui-icon ui-icon-newwin">
										</span> 
									</a>
								</span> 
								<input type="hidden" id="jobId" value="${JOB_ASSIGNMENT.job.jobId}" class="jobId" />
							</div>
				      	  	<div>
				      	  		<label class="width30">Employee<span style="color:red">*</span></label>
								<input type="text" id="employeeName" readonly="readonly" class=" width50 validate[required]"
									value="${JOB_ASSIGNMENT.personName}"/>
								
								<span class="button" id="employee"  style="position: relative; top:0px;">
									<a style="cursor: pointer;" id="employeepopup" class="btn ui-state-default ui-corner-all width10"> 
										<span class="ui-icon ui-icon-newwin">
										</span> 
									</a>
								</span> 
								<input type="hidden" id="personId" value="${JOB_ASSIGNMENT.person.personId}" class="personId" />
							</div>
							
							<div>
								<label class="width30">Department & Branch<span style="color:red">*</span></label>
								<input type="text"  readonly="readonly"  id="locationName" 
										class="width50 validate[required]" value="${JOB_ASSIGNMENT.departmentLocationName}"/>
								<span class="button" id="location"  style="position: relative; top:0px; ">
									<a style="cursor: pointer;" id="locationpopup" class="btn ui-state-default ui-corner-all width10"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
								<input type="hidden" name="cmpDeptLocId" id="cmpDeptLocId"
									class="cmpDeptLocId"  value="${JOB_ASSIGNMENT.cmpDeptLocation.cmpDeptLocId}" />  
							</div>
														
							
							<div>
								<label class="width30">Bank Info.</label>
								<c:choose>
			                      	<c:when test="${JOB_ASSIGNMENT.personBank ne null && JOB_ASSIGNMENT.personBank ne ''}">
				                      	<input type="text" class="width50" title="" readonly="readonly" name="personBank" id="personBank" 
				                      	value="${JOB_ASSIGNMENT.bankNameAndNumber}"/>
				                     	<input type="hidden" name="personBankId" id="personBankId" value="${JOB_ASSIGNMENT.personBank.personBankId}"/> 
				                      	
			                      	</c:when>
			                      	<c:otherwise>
			                      		<input type="text" class="width50" title="" readonly="readonly" name="personBank" id="personBank" value=""/>
			                      		<input type="hidden" name="personBankId" id="personBankId" value=""/> 
			                      	</c:otherwise>
				                 </c:choose>
		                      	<span class="button" style="position: relative;top:0px; ">
									<a style="cursor: pointer;" id="personbankpopup" class="btn ui-state-default ui-corner-all width10"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div>
							
														
				 </fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="min-height: 170px;">
					<div>
							<label class="width30">Swipe ID</label>
							<input type="text" disabled="disabled" class="width50 swipeId" name="swipeId" id="swipeId" value="${JOB_ASSIGNMENT.swipeId}" />
						</div> 
						<div>
							<label class="width30">Pay Mode</label>
							<select class="width51 payMode" id="payMode">
								<option value="OTHER">OTHER</option>
								<option value="WPS">WPS</option>
								<option value="BANK">BANK</option>
								<option value="EXCHANGE">EXCHANGE</option>
								<option value="CASH">CASH</option>
							</select>
							<input type="hidden" id="payModeTemp" value="${JOB_ASSIGNMENT.payMode}"/>
						</div>
						<div>
							<label class="width30" for="payPolicy">Pay Policy<span class="mandatory">*</span></label>
							<select class="payPolicy width51 validate[required]" id="payPolicy">
								<option value="">Select</option>
								<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
									<option value="${policy.key}">${policy.value}</option>
								</c:forEach>
							</select>
							<input type="hidden" id="payPolicyTemp" value="${JOB_ASSIGNMENT.payPolicy}"/>
						</div> 
					<div>
						<label class="width30" for="effectiveDate">Effective Start Date<span class="mandatory">*</span></label>
						<input type="text" readonly="readonly" name="effectiveDate" 
							id="effectiveDate" value="${JOB_ASSIGNMENT.effectiveDateStr}" class="width50 validate[required]"/>
					</div>   
					<div>
						<label class="width30" for="endDate">Effective End Date</label>
						<input type="text" readonly="readonly" name="endDate" id="endDate" value="${JOB_ASSIGNMENT.endDateStr}" class="width50"/>
					</div> 
					<div>
						<label class="width30" for="rejoiningDate">Rejoining Date</label>
						<input type="text" name="rejoiningDate" id="rejoiningDate" value="${JOB_ASSIGNMENT.rejoiningDateStr}" class="width50"/>
					</div> 
					<div>
							<label class="width30">Accomodation Type</label>
							<select class="width51 accomodationType" id="accomodationType">
								<option value="">Select</option>
								<c:forEach var="type" items="${ACCOMODATION_OPTIONS}" varStatus="statusPolicy">
									<option value="${type.accessCode}">${type.displayName}</option>
								</c:forEach>
							</select>
							 <span class="button" id="ACCOMODATION_OPTIONS_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="ACCOMODATION_OPTIONS"
													class="btn ui-state-default ui-corner-all job-assignment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							<input type="hidden" id="accomodationTypeTemp" value="${JOB_ASSIGNMENT.lookupKey}"/>
					</div>
					<div class="width50 float-left">
						 <c:choose>
						 	<c:when test="${JOB_ASSIGNMENT.isLocalPay eq true}">
								<input type="checkbox" class="width10 float-left" checked="checked" id="isLocalPay" />
							</c:when>
							<c:otherwise>
								<input type="checkbox" class="width10 float-left" id="isLocalPay" />
							</c:otherwise>
						</c:choose> 
						<label class="width80 float-left">Local Currency Pay(AED)</label>
						
					</div> 
					<div class="width50 float-left">
						 <c:choose>
						 	<c:when test="${JOB_ASSIGNMENT.isActive eq true}">
								<input type="checkbox" class="width10 float-left" checked="checked" id="isActive" />
							</c:when>
							<c:otherwise>
								<input type="checkbox" class="width10 float-left" id="isActive" />
							</c:otherwise>
						</c:choose> 
						<label class="width40 float-left">Active</label>
					</div> 
		           <div class="width50 float-left">
						 <c:choose>
						 	<c:when test="${JOB_ASSIGNMENT.isPrimary eq true}">
								<input type="checkbox" class="width10 float-left" checked="checked" id="isPrimary" />
							</c:when>
							<c:otherwise>
								<input type="checkbox" class="width10 float-left" id="isPrimary" />
							</c:otherwise>
						</c:choose>
						<label class="width50 float-left">Primary Job</label>
						 
					</div> 
					 <div class="width50 float-left">
						 <c:choose>
						 	<c:when test="${JOB_ASSIGNMENT.freeze eq true}">
								<input type="checkbox" disabled="disabled" class="width10 float-left" checked="checked" id="isFreeze" />
							</c:when>
							<c:otherwise>
								<input type="checkbox" disabled="disabled" class="width10 float-left" id=isFreeze />
							</c:otherwise>
						</c:choose>
						<label class="width50 float-left">Holded</label>
						 
					</div> 
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
		
		<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
					
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
						<li class="ui-state-default ui-corner-top ui-tabs-selected promotion-tab"><a href="#tabs-200" id="current_grade">Current Grade</a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected promotion-tab"><a href="#tabs-201" id="current_job">Current Job</a></li>
					</ul>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-200">
						<div id="hrm" class="current_grade"> 
							<div>Information will be loaded on select the Employee</div>
						</div>
					</div>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-201">
						<div id="hrm" class="current_job"> 
							
						</div>
					</div>
					
		</div>	
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
</div>