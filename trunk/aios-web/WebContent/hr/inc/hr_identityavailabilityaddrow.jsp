<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td style="display: none;" id="lineId_${rowId}">${rowId}</td>
	<td>
		<input type="text" class="width70"
		readonly="readonly" name="identityType" id="identityType_${rowId}"
		/> <span class="button"
		style="position: relative; top: 0px;"> <a
		style="cursor: pointer;" id="identityCall_${rowId}"
		class="btn ui-state-default ui-corner-all width100 identityCall common-popup">
			<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
		type="hidden" name="identityId" id="identityId_${rowId}"
		 />	
		
	</td>
	<td><input type="text" name="documentName"
		id="documentName_${rowId}" 
		class="width90"> 
	</td>
	<td><input type="text" name="handoverDate"
		id="handoverDate_${rowId}" value="${handoverDate}"
		class="width90 handoverDate"> 
	</td>
	<td><input type="text" name="expectedReturnDate"
		id="expectedReturnDate_${rowId}" 
		class="width90 expectedReturnDate"> 
	</td>
	<td><input type="text" name="actualReturnDate"
		id="actualReturnDate_${rowId}" 
		class="width90 actualReturnDate"> 
	</td>
	<td><input type="text" name="purpose" 
		id="purpose_${rowId}"
		 class="width98"
		maxlength="1000">
	</td>
	<td style="width: 1%;" class="opn_td" id="option_${rowId}">
		 <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}"
		style="cursor: pointer; display: none;"
		title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <input
		type="hidden" name="identityAvailabilityId" id="identityAvailabilityId_${rowId}"
		/>		
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$('.handoverDate').datepick();
	$('.expectedReturnDate').datepick();
	$('.actualReturnDate').datepick();
});
</script>