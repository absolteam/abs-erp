<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">
select{
	width: 51%;
}
</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
var accessCode=null;
$(function (){  
	var gradeView=$('#gradeView').val();
	if(gradeView!=null && gradeView=='true'){
		 $('#save').hide();
	}
	var gradeId=Number($('#gradeId').val());
	if(gradeId>0){
		
		if($('#isDefalutTemp').val().trim()=='true')
			$('#isDefalut').attr("checked","checked");
		
		if($('#isPercentageIncrementTemp').val().trim()=='true')
			$('#isPercentageIncrement').attr("checked","checked");
		
		
		autoSelectTheMultiList("leavePolicyIdTemp","leavePolicyId");
		autoSelectTheMultiList("attendancePolicyIdTemp","attendancePolicyId");
		autoSelectTheMultiList("evaluationPeriodTemp","evaluationPeriod");
		autoSelectTheMultiList("incrementPeriodTemp","incrementPeriod");
		autoSelectTheMultiList("ticketClassTemp","ticketClass");
		autoSelectTheMultiList("insuranceTypeTemp","insuranceType");
		autoSelectTheMultiList("payPeriodIdTemp","payPeriodId");
		
		
	}else{
		$('#leavePolicyId').val(Number('${DEFAULT_LEAVE}'));
		$('#attendancePolicyId').val(Number('${DEFAULT_ATTENDANCE}'));
	}
	
	
  
	
});
 
function autoSelectTheMultiList(source,target) {
	 var idsArray=new Array();
	 var idlist=$('#'+source).val();
	 idsArray=idlist.split(',');
	$('#'+target+' option').each(function(){
		var txt=$(this).val().trim();
		if($.inArray(txt, idsArray)>=0)
			$(this).attr("selected","selected");
	});

}
</script>
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Grade Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<form id="GRADE_FORM" name="GRADE_FORM">
			<div id="hrm" class="float-left width50">
				<input type="hidden" name="grade.gradeId" id="gradeId"
					value="${GRADE_INFO.gradeId}" />
					<input type="hidden" name="gradeView" id="gradeView"
					value="${VIEW_FLAG}" />
				<fieldset style="min-height: 290px;">
					<div>
						<label class="width40">Grade Name<span style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required]"
							name="grade.gradeName" id="gradeName"
							value="${GRADE_INFO.gradeName}" />
					</div>
					<div>
						<label class="width40">Sequance<span style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required,custom[onlyNumber]]"
							name="grade.sequence" id="sequance"
							value="${GRADE_INFO.sequence}" />
					</div>
					<div>
						<label class="width40">Minimum Salary Range<span
							style="color: red">*</span> </label> <input type="text"
							class="width50 validate[required]" name="grade.minSalary"
							id="minSalary" value="${GRADE_INFO.minSalary}" />
					</div>
					<div>
						<label class="width40">Maximum Salary Range<span
							style="color: red">*</span> </label> <input type="text"
							class="width50 validate[required]" name="grade.maxSalary"
							id="maxSalary" value="${GRADE_INFO.maxSalary}" />
					</div>
					<div>
						<label class="width40">Increment Period</label>
						<div>
							<select id=incrementPeriod name="grade.incrementPeriod">
								<option value="">--Select--</option>
								<c:forEach items="${INCREMENT_PERIOD}" var="gen">
									<option value="${gen.key}">${gen.value}</option>
								</c:forEach>
							</select> <input type="hidden" name="incrementPeriodTemp"
								id="incrementPeriodTemp" value="${GRADE_INFO.incrementPeriod}" />
						</div>
					</div>
					<div>
						<label class="width40">No.of Increment </label> <input type="text"
							class="width50 validate[custom[onlyNumber]]"
							name="grade.numberOfIncrement" id="numberOfIncrement"
							value="${GRADE_INFO.numberOfIncrement}" />
					</div>
					<div class="width100">
						<label class="width40 float-left">Increment Value</label> <input
							type="text" class="width20 float-left"
							name="grade.incrementAmount" id="incrementAmount"
							value="${GRADE_INFO.incrementAmount}" /> <label
							class="width20 float-left">is percentage(%)?</label> <input type="checkbox"
							class="width10 float-left" name="grade.isPercentageIncrement"
							id="isPercentageIncrement" /> <input type="hidden"
							id="isPercentageIncrementTemp" class=""
							value="${GRADE_INFO.isPercentageIncrement}">
					</div>

					<div class="width100">
						<label class="width40">Ticket Class</label>
						<div>
							<select id=ticketClass name="grade.ticketClass">
								<option value="">--Select--</option>
								<c:forEach items="${TICKET_CLASS}" var="gen">
									<option value="${gen.key}">${gen.value}</option>
								</c:forEach>
							</select> <input type="hidden" name="ticketClassTemp" id="ticketClassTemp"
								value="${GRADE_INFO.ticketClass}" />
						</div>
					</div>
					<div>
						<label class="width40">No.of Ticket's</label> <input type="text"
							class="width50 validate[custom[onlyNumber]]"
							name="grade.numberOfTicket" id="numberOfTicket"
							value="${GRADE_INFO.numberOfTicket}" />
					</div>
					
				</fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="min-height: 290px;">
					<div>
						<label class="width40">Attendance Policy</label> <input
							type="hidden" id="attendancePolicyIdTemp"
							value="${GRADE_INFO.attendancePolicy.attendancePolicyId}">
						<select name="grade.attendancePolicy.attendancePolicyId" id="attendancePolicyId">
							<option value="">--Select--</option>
							<c:forEach items="${requestScope.ATTENDANCE_POLICIES}"
								var="nltlst">
								<option value="${nltlst.attendancePolicyId}">${nltlst.policyName}
								</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width40">Leave Policy</label> <input type="hidden"
							id="leavePolicyIdTemp"
							value="${GRADE_INFO.leavePolicy.leavePolicyId}"> <select
							name="grade.leavePolicy.leavePolicyId"
							id="leavePolicyId">
							<option value="">--Select--</option>
							<c:forEach items="${requestScope.LEAVE_POLICIES}" var="nltls">
								<option value="${nltls.leavePolicyId}">${nltls.leavePolicyName}
								</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width40">Pay Period</label> <input
							type="hidden" id="payPeriodIdTemp"
							value="${GRADE_INFO.payPeriod.payPeriodId}">
						<select name="grade.payPeriod.payPeriodId" id="payPeriodId">
							<option value="">--Select--</option>
							<c:forEach items="${requestScope.PAYPERIOD_LIST}"
								var="nltlst">
								<option value="${nltlst.payPeriodId}">${nltlst.periodName}
								</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width40">Evaluation Period</label>
						<div>
							<select id=evaluationPeriod name="grade.evaluationPeriod">
								<option value="">--Select--</option>
								<c:forEach items="${EVALUTATION_PERIOD}" var="gen">
									<option value="${gen.key}">${gen.value}</option>
								</c:forEach>
							</select> <input type="hidden" name="evaluationPeriodTemp"
								id="evaluationPeriodTemp" value="${GRADE_INFO.evaluationPeriod}" />
						</div>
					</div>
					<div>
						<label class="width40">No.of Evaluation </label> <input
							type="text" class="width50 validate[custom[onlyNumber]]"
							name="grade.evaluationPeriodCount" id="evaluationPeriodCount"
							value="${GRADE_INFO.evaluationPeriodCount}" />
					</div>
					<div>
						<label class="width40">Insurance Type</label>
						<div>
							<select id="insuranceType"
								name="grade.lookupDetail.lookupDetailId"
								class="validate[required]">
								<option value="">--Select--</option>
								<c:forEach items="${INSURANCE_TYPE}" var="nltlst">
									<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
									</option>
								</c:forEach>
							</select> <span class="button" id="INSURANCE_TYPE_1"
								style="position: relative; "> <a
								style="cursor: pointer;" id="INSURANCE_TYPE"
								class="btn ui-state-default ui-corner-all recruitment-lookup width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
								type="hidden" name="insuranceTypeTemp" id="insuranceTypeTemp"
								value="${GRADE_INFO.lookupDetail.lookupDetailId}" />
						</div>
					</div>
					<div class="width100 float-left">
						<label class="width40 float-left">Notice Period(Employee)</label> <input
							type="text" class="float-left validate[custom[onlyNumber]]"
							name="grade.noticeDaysEmployee" id="noticeDaysEmployee"
							value="${GRADE_INFO.noticeDaysEmployee}" style="width: 43%!important"/>
							<label class="width10 float-left" style="margin-top:11px;">Days</label> 
					</div>
					<div class="width100 float-left">
						<label class="width40 float-left">Notice Period(Employer)</label> <input
							type="text" class="float-left validate[custom[onlyNumber]]"
							name="grade.noticeDaysCompany" id="noticeDaysCompany"
							value="${GRADE_INFO.noticeDaysCompany}" style="width: 43%!important"/>
							<label class="width10 float-left" style="margin-top:11px;">Days</label>
					</div>
					<div class="width100 float-left">
						<label class="width40">Allowed Dependent's</label> <input
							type="text" class="width50 validate[custom[onlyNumber]]"
							name="grade.allowedDependents" id="allowedDependents"
							value="${GRADE_INFO.allowedDependents}" />
					</div>
					<div id="dependentTicket">
						<label class="width40">No.of Dependent Ticket's</label> <input
							type="text" class="width50 validate[custom[onlyNumber]]"
							name="grade.numberOfDependentTicket" id="numberOfDependentTicket"
							value="${GRADE_INFO.numberOfDependentTicket}" />
					</div>
					
					<div>
						<label class="width40">Default</label> <input type="checkbox"
							id="isDefalut" name="grade.isDefalut"> <input type="hidden"
							id="isDefalutTemp" class="width10"
							value="${GRADE_INFO.isDefalut}">
					</div>
				</fieldset>
			</div>
		</form>
		
	</div>
</div>