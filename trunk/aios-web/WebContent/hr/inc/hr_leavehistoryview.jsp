<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Leave History</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Leave History</span></div>
	<div class="width70 float-left"><span class="side-heading">Filter Condition</span></div>
	<div class="width30 float-right"><span class="side-heading">Printed on :${LEAVE_MASTER.createdDate}</span></div>
	<div class="width100 float-left">
		<div class="data-container width40 float-left">	
				
				<div>
					<label class="caption width30" >Employee</label>
					<label class="data width50">${LEAVE_MASTER.personName}</label>
				</div>
				<div>
					<label class="caption width30" >Company</label>
					<label class="data width50">${LEAVE_MASTER.companyName}</label>
				</div>
				<div>
					<label class="caption width30" >Department</label>
					<label class="data width50">${LEAVE_MASTER.departmentName}</label>
				</div>
				<div>
					<label class="caption width30" >Location</label>
					<label class="data width50">${LEAVE_MASTER.locationName}</label>
				</div>
			</div>
			<div class="data-container width40 float-left">	
				<div>
					<label class="caption width30" >History From</label>
					<label class="data width20">${LEAVE_MASTER.fromDate}</label>
					<label class="caption width30" >UpTo</label>
					<label class="data width20">${LEAVE_MASTER.toDate}</label>
				</div>
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading">Leave History Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(LEAVE_MASTER.leaveHistoryList)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Leave Type</th>
						<th>Start Date</th>
						<th>End Date</th> 
						<th>Number of Day(s)</th>
						<th>Contact on Leave</th>
						<th>Leave Status</th>
						<th>Request Status</th>
						<th>Requested Date</th>
						<th>Reason</th>
					</tr> 
				</thead> 
				<tbody >	
				
					<c:forEach items="${LEAVE_MASTER.leaveHistoryList}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.leaveType }</td>
							<td>${result3.fromDate}</td>
							<td>${result3.toDate}</td>
							<td>${result3.numberOfDays }</td>
							<td>${result3.contactOnLeave }</td>
							<td>${result3.statusStr}</td>
							<td>${result3.isApprove }</td>
							<td>${result3.createdDate }</td>
							<td>${result3.reason}</td>
						</tr>
					</c:forEach>
				 
				</tbody>
			</table>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Leavs</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>