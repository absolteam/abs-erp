<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
$(document).ready(function (){ 
	
	
	listCall();
	
	
	/* Click event handler */
	$('#JobAssignment tbody tr').live('click', function() {
		if (!$('#JobAssignment tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					
				}
			}
		}
	});
	
	$('#JobAssignment tbody tr').live('dblclick', function() {
		if (!$('#JobAssignment tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					
				}
			}
			 getJobAssignmentFromObject(aData);
			$('#job-common-popup').dialog("close");
		}
		return false;
	});
	
	$('#popup-job-close').click(function () { 
		$('#job-common-popup').dialog("close");
	});
	
});


function listCall(){
	
	
	$('#JobAssignment').dataTable().fnDestroy();
	oTable = $('#JobAssignment')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",
				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sScrollY": $("#main-content").height() - 250,
				"sAjaxSource" : "<%=request.getContextPath()%>/common_job_assignment_json.action",
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								//alert(aData.statusDisplay);
							},
							"aoColumns" : [ {
								"mDataProp" : "jobAssignmentNumber"
							}, {
								"mDataProp" : "personName"
							}, {
								"mDataProp" : "designationName"
							}, {
								"mDataProp" : "locationName"
							}, {
								"mDataProp" : "mobile"
							}, {
								"mDataProp" : "email"
							} ]

						});

	}
</script>

<div id="main-content">

	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Job Assignment
		</div>

		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="JobAssignment">
					<thead>
						<tr>
							<th>Number</th>
							<th>Employee</th>
							<th>Designation</th>
							<th>Location</th>
							<th>Mobile</th>
							<th>Email</th>
						</tr>
					</thead>


				</table>
			</div>

			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="popup-job-close">
					<fmt:message key="common.button.close" />
				</div>
			</div>
		</div>
	</div>
</div>
