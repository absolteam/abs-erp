<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
	
	table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	border:1px solid #8C8C8C;
	min-width:45%;
	}
	th {
	background: #FFC;
	border:1px solid #8C8C8C;
	font-size:13px;
	}
</style>
<script type="text/javascript">
</script>
<body id="offer-letter-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Offer Details
		
		</div>
	
		<div class="portlet-content">
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<table>
					<tbody>
						<tr>
							<td>
								<span style="font-weight: bold;text-decoration: underline;">From:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Company :</span><span> ${CANDIDATE_OFFER.openPosition.cmpDeptLocation.company.companyName},</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Department :</span><span> ${CANDIDATE_OFFER.openPosition.cmpDeptLocation.department.departmentName},</span>
							</td>
						</tr>
						<tr>
							<td style="max-width:10%;">
								<span>${CANDIDATE_OFFER.address}</span>
							</td>
							
						</tr>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;text-decoration: underline;">To:</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Candidate :</span>
								<span>${CANDIDATE_OFFER.personByPersonId.firstName}
												${CANDIDATE_OFFER.personByPersonId.lastName},</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Mobile :</span>
								<span>${CANDIDATE_OFFER.personByPersonId.mobile},</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Email :</span>
								<span>${CANDIDATE_OFFER.personByPersonId.email},</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Address :</span>
								<span>${CANDIDATE_OFFER.personByPersonId.permanentAddress}</span>
							</td>
						</tr>
						
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;text-decoration: underline;">Job Details:</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Designation :</span>
								<span>${CANDIDATE_OFFER.openPosition.designation.designationName}</span>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;">Grade :</span>
								<span>${CANDIDATE_OFFER.openPosition.designation.grade.gradeName}</span>
							</td>
						</tr>
						
						<tr>
							<td>
							</td>
						</tr>
						
						<tr>
							<td>
								<span style="font-weight: bold;text-decoration: underline;">Salary/Wages Details:</span>
							</td>
						</tr>
						<tr>
							<td>
								<table border="1">
								<thead>
									 
									 <tr>
									 	<th>Salary Element</th>
									 	<th>Amount</th>
									 </tr>
								</thead>
								<tbody>
									<c:forEach items="${CANDIDATE_OFFER.salaryElements}" var="sal">
										<tr>
											<td>${sal.elementName}</td>
											<td>${sal.amount}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							</td>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;text-decoration: underline;">Leave Details:</span>
							</td>
						</tr>
						<tr>
							<c:choose>
								<c:when test="${CANDIDATE_OFFER.leaves ne null && fn:length(CANDIDATE_OFFER.leaves)>0}">
									<td>
										<table border="1">
											<thead>
												 <tr>
												 	<th>Leave Type</th>
												 	<th>Days</th>
												 	<th>Term</th>
												 </tr>
											</thead>
											<tbody>
												<c:forEach items="${CANDIDATE_OFFER.leaves}" var="leave">
													<tr>
														<td>${leave.leaveType}</td>
														<td>${leave.days}</td>
														<td>${leave.term}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									<td>
									</c:when>
								<c:otherwise>
									<td>
										No Data
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<td>
								<span style="font-weight: bold;text-decoration: underline;">Work Hours:</span>
							</td>
						</tr>
						<tr>
							<c:choose>
								<c:when test="${CANDIDATE_OFFER.shifts ne null && fn:length(CANDIDATE_OFFER.shifts)>0}">
									<td>
										<table border="1">
											<thead>
												 <tr>
												 	<th>Start Time</th>
												 	<th>End Time</th>
												 	<th>For Specific Day</th>
												 </tr>
											</thead>
											<tbody>
												<c:forEach items="${CANDIDATE_OFFER.shifts}" var="leave">
													<tr>
														<td>${leave.startTime}</td>
														<td>${leave.endTime}</td>
														<td>${leave.irregularDay}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									<td>
								</c:when>
								<c:otherwise>
									<td>
										No Data
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
						</tbody>
					</table>
				<br>
			
			</div>
		</div>
</body>