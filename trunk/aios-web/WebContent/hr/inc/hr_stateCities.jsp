<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
<c:choose>
	<c:when test="${cityList != null }">
		<c:forEach items="${cityList}" var="cityBean" varStatus="status" >
		<option id="${cityBean.cityId}" value="${cityBean.cityId}">${cityBean.cityCode} - ${cityBean.cityName}</option>
	</c:forEach> 
	</c:when>
</c:choose>

<script type="text/javascript">
$(document).ready(function(){ 
$('#city').combobox({ 
	 selected: function(event, ui){ 
	 }
});
});
</script>