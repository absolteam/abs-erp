<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tr class="rowidS" id="fieldrowS_${rowId}">
	<td id="lineIdS_${rowid}" style="display:none;">${rowId}</td>
	<td>
		<%-- <select class="width90 shiftType" id="shiftType_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="shifttyp" items="${SHIFT_TYPE_LIST}" varStatus="statusL">
				<option value="${shifttyp.workingShiftId}">${shifttyp.workingShiftName}</option>
			</c:forEach>
		</select>
		<input type="hidden" id="workingShiftId_${rowid}" style="border:0px;"/> --%>
		<span style="height: 20px;width:65%!important" id="shiftTypeName_${rowId}" class="float-left width20"></span>
		<span class="button float-right" id="shiftType_${rowId}"  style="position: relative; top:4px;">
				<a style="cursor: pointer;" id="shiftpopup_${rowId}" class="btn ui-state-default ui-corner-all width100 shiftType-popup"> 
					<span id="shiftTypeName_${rowId}" class="ui-icon ui-icon-newwin"> 
					</span> 
				</a>
		</span> 
		<input type="hidden" id="workingShiftId_${rowId}" class="workingShiftId" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${rowId}" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${rowId}" style="border:0px;"/>
	</td> 
	<td style="display:none;">
		<input type="hidden" id="jobShiftId_${rowId}" style="border:0px;"/>
	</td>
	 <td style="width:0.01%;" class="opn_td" id="optionS_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>    
</tr>
<script type="text/javascript"> 
$(document).ready(function(){  
	 $('.startDate,.endDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
});
</script>