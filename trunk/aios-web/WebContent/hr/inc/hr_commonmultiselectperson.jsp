<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	var personId = 0;
	var personName = '';
	var personTypes = "";
	$(function() {
		$($($('#common-popup').parent()).get(0)).css('top',
				0 + 'px !important;');
		personTypes = $('#personTypesToList').val();
		$('#Person')
				.dataTable(
						{
							"sAjaxSource" : "common_person_list_json.action?personTypes="
									+ personTypes,
							"sPaginationType" : "full_numbers",
							"bJQueryUI" : true,
							"iDisplayLength" : 25,
							"aoColumns" : [
									{
										"sTitle" : '<fmt:message key="hr.personaldetails.label.recordid"/>',
										"bVisible" : false
									},
									{
										"sTitle" : '<fmt:message key="hr.personaldetails.label.firstname"/>'
									},
									{
										"sTitle" : '<fmt:message key="hr.personaldetails.label.lastname"/>'
									},
									{
										"sTitle" : '<fmt:message key="hr.personaldetails.label.persontype"/>'
									},
									{
										"sTitle" : '<fmt:message key="hr.personaldetails.label.country"/>'
									},
									{
										"sTitle" : '<fmt:message key="hr.personaldetails.label.personid"/>'
									},
									{
										"sTitle" : '<fmt:message key="hr.common.email"/>'
									},
									{
										"sTitle" : '<fmt:message key="hr.person.mobile"/>'
									}, ],
							"sScrollY" : $("#main-content").height() - 250,
							//"bPaginate": false,
							"aaSorting" : [ [ 1, 'desc' ] ],
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex) {
								if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
									$(nRow).addClass('row_selected');
								}
							}
						});
		//Json Grid
		//init datatable
		oTable = $('#Person').dataTable();

		/* Click event handler */
		$('#Person tbody tr').live('click', function() {
 			if ($(this).hasClass('row_multi_selected'))
				$(this).removeClass('row_multi_selected');
			else
				$(this).addClass('row_multi_selected');
			return false;
		});

		$('#person-list-ok').live(
				'click',
				function() {
					var jsonData = [];
					
					$('.row_multi_selected').each(
							function() {
								aData = oTable.fnGetData(this);
								 
								jsonData.push({
									"personId" : Number(aData[0]),
									"personName" : aData[1] + " " + aData[2]
											+ " [" + aData[5] + "]"
								});  
							});
					selectedPersonList(jsonData, $('#currentRowId').val());
					$('#common-popup').dialog('close');
				});
	});
</script>
<div id="main-content">
	<div class="mainhead portlet-header ui-widget-header">
		<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>${POPUP_TITLE}
	</div>
	<input type="hidden" id="personTypesToList" name="personTypesToList"
		value="${PERSON_TYPES}" />
	<div id="trans_combination_accounts">
		<table class="display" id="Person"></table>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right"
				id="person-list-ok">ok</div>
			<div class="portlet-header ui-widget-header float-right"
				id="person-list-close">
				<fmt:message key="common.button.close" />
			</div>
		</div>
		<input type="hidden" id="currentRowId" value="${id}"/>
	</div>
</div>