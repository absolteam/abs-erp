 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<tr style="background-color: #F8F8F8;"
	id="qualificationchildRowId_${requestScope.rowid}">
	<td colspan="7" class="tdqualification"
		id="qualificationchildRowTD_${requestScope.rowid}">
		<div id="errMsg"></div>
		<form name="qualificationform" id="qualificationform" style="position: relative;">
			<input type="hidden" name="addEditFlag" id="qualificationaddEditFlag"
				value="${requestScope.AddEditFlag}" /> <input type="hidden"
				name="id" id="qualificationlineId" value="${requestScope.rowid}" />
			<input type="hidden" name="qualification.academicQualificationsId"
				id="academicQualificationsId" />
			<div class="float-left width50" id="hrm">

				<fieldset>
					<div>
						<label class="width30">Degree<span style="color: red">*</span>
						</label>
						<div>
							<select id="degree"
								name="qualification.lookupDetail.lookupDetailId"
								class="width50 validate[required]">
								<option value="">--Select--</option>
								<c:forEach items="${DEGREES}" var="nltlst">
									<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
									</option>
								</c:forEach>
							</select> <span class="button" id="DEGREE_1"
								style="position: relative;"> <a style="cursor: pointer;"
								id="DEGREE"
								class="btn ui-state-default ui-corner-all recruitment-lookup width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
								type="hidden" name="degreeTemp" id="degreeTemp"
								value="${QUALFIFICATION.lookupDetail.lookupDetailId}" />
						</div>
						<div>
							<label class="width30">Subject<span style="color: red">*</span>
							</label> <input type="text" id="majorSubject"
								class="width50 validate[required]"
								name="qualification.majorSubject"
								value="${QUALFIFICATION.majorSubject}" />
						</div>
						<div>
							<label class="width30">Instution<span style="color: red">*</span>
							</label> <input type="text" id="instution"
								class="width50 validate[required]"
								name="qualification.instution"
								value="${QUALFIFICATION.instution}" />
						</div>
					</div>
				</fieldset>
			</div>
			<div class="float-left width50" id="hrm">
				<fieldset>
					<div>
						<label class="width30">Completion year </label> <input type="text"
							id="completionYear" readonly="readonly" class="width50 "
							name="completionYear" value="${QUALFIFICATION.completionYear}" />
					</div>
					<div>
						<label class="width30">Result </label> <input type="text"
							id="result" class="width50 " name="qualification.result"
							value="${QUALFIFICATION.result}" />
					</div>
					<div>
						<label class="width30">Result Type </label> <input type="text"
							id="resultType" class="width50 " name="qualification.resultType"
							value="${QUALFIFICATION.resultType}" />
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
				style="margin: 20px;">
				<div
					class="portlet-header ui-widget-header float-right cancel_qualification"
					id="cancel_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right add_qualification"
					id="update_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
		</form>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span> </a>
			</div>
			<div id="qualification-common-popup"
				class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="qualification-common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
		</td>
</tr>


<script type="text/javascript">
	var trval = $('.tdqualification').parent().get(0);
	var fromFormat = 'dd-MMM-yyyy';
	var toFormat = 'yyyyMMdd';
	var usrfromFormat = 'dd-MMM-yyyy';
	var usrtoFormat = 'yyyyMMdd';
	var accessCode = null;
	$(function() {
		$jquery("#qualificationform").validationEngine('attach');
		$('#completionYear').datepick();
		clear: false;

				
		$('.success,.error').hide();
		$('#qualification-common-popup').dialog("close");
		$('.recruitment-lookup').click(function() {
			
			$('.ui-dialog-titlebar').remove();
			$('#qualification-common-popup').dialog('open');
			accessCode = $(this).attr("id");
			$.ajax({
				type : "POST",
				url : "<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.qualification-common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.qualification-common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
		 
		
		$('#qualification-common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:450,
			bgiframe: false,
			modal: true 
		});
		  $('.cancel_qualification').click(function(){ 
			  $('.formError').remove();	
			  $('#qualification-common-popup').dialog('destroy');		
				$('#qualification-common-popup').remove(); 
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdqualification').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#qualificationaddEditFlag').val()!=null && $('#qualificationaddEditFlag').val()=='A')
				{
  					$("#qualificationAddImage_"+rowid).show();
  				 	$("#qualificationEditImage_"+rowid).hide();
				}else{
					$("#qualificationAddImage_"+rowid).hide();
  				 	$("#qualificationEditImage_"+rowid).show();
				}
				 $("#qualificationchildRowId_"+rowid).remove();	
				 $("#qualificationDeleteImage_"+rowid).show();
				 $("#qualificationWorkingImage_"+rowid).hide();  
				 
				 tempvar=$('.qualificationtab>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#qualificationDeleteImage_'+rowid).hide(); 
				 $('#openFlag').val(0);
				 return false;
		  });
		  $('.add_qualification').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdqualification').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				
				var aqId=$('#academicQualificationsId').val();
				var degree=$('#degree option:selected').text();
			 	var degreeId=$('#degree option:selected').val();
			 	var majorSubject=$('#majorSubject').val();
			 	var instution=$('#instution').val();
			 	var completionYear=$('#completionYear').val();
			 	var result=$('#result').val();
			 	var resultType=$('#resultType').val();
			 //var tempObject=$(this);
			var addEditFlag=$('#qualificationaddEditFlag').val();
		        if($jquery("#qualificationform").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/person_qualification_save.action", 
					 	async: false, 
					 	data:$("#qualificationform").serialize(),
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 
		        		
		        		 $('#qualification-common-popup').dialog('destroy');		
			 				$('#qualification-common-popup').remove(); 
			        	//Bussiness parameter
			        		$('#academicQualificationsId_'+rowid).val(aqId); 
		        			$('#degree_'+rowid).text(degree); 
		        			$('#degreeId_'+rowid).val(degreeId);
		        			$('#majorSubject_'+rowid).text(majorSubject); 
		        			$('#instution_'+rowid).text(instution); 
		        			$('#completionYear_'+rowid).text(completionYear);
		        			$('#result_'+rowid).text(result);
		        			$('#resultType_'+rowid).text(resultType);
		        			
						//Button(action) hide & show 
							 $("#qualificationchildRowId_"+rowid).remove();	
		    				 $("#qualificationAddImage_"+rowid).hide();
		    				 $("#qualificationEditImage_"+rowid).show();
		    				 $("#qualificationDeleteImage_"+rowid).show();
		    				 $("#qualificationWorkingImage_"+rowid).hide(); 
		        			

		        			if(addEditFlag!=null && addEditFlag=='A')
		     				{
	         					$('.qualificationaddrows').trigger('click');
	         					var qualificationcount=Number($('#qualificationcount').val());
	         					qualificationcount+=1;
	         					$('#qualificationcount').val(qualificationcount);
	         					 tempvar=$('.qualificationtab>tr:last').attr('id'); 
	         					 idval=tempvar.split("_");
	         					 rowid=Number(idval[1]); 
	         					 $('#qualificationDeleteImage_'+rowid).show(); 
		     				}
		        		}		  	 
		      } 
		      else{return false;}
		  });
		  
		//Lookup Data Roload call
		 	$('.save-lookup').live('click',function(){ 
		 		
				if(accessCode=="DEGREE"){
					$('#degree').html("");
					$('#degree').append("<option value=''>--Select--</option>");
					loadLookupList("degree");
					
				}
			});
		 
		 	//Lookup Data Roload call
		 	$('.discard-lookup').live('click',function(){ 
				$('#qualification-common-popup').dialog("close");
				
			});
		 
		  
		return false;	
	 });
	 function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
 </script>	