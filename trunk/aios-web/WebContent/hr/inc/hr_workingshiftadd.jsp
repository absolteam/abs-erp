<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">

#ui-datepicker-div {
	background-color: #cfcfcf !important;
	/* left: 731.5px !important; */
		position: absolute !important;
	/* top: 124.967px !important; */
	width: 231px !important;
	z-index: 751!important;
	
}

.ui-widget-content {
    background: url("images/ui-bg_flat_75_ffffff_20x100.png") repeat-x scroll 50% 50% #FFFFFF;
    color: #444444;
}

.ui-slider-horizontal {
    border: 1px solid #CCCCCC;
    margin: 3px 0 4px 5px;
   
}

</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
$(function (){  
	$('.timePick').timepicker({});
	var workingShiftId=Number($('#workingShiftId').val());
	if(workingShiftId>0){
		$('#irregularDay').val($('#irregularDayTemp').val());
		if($('#isDefaultTemp').val().trim()=='true')
			$('#isDefault').attr("checked","checked");
		
		if($('#isActiveTemp').val().trim()=='true'){
			$('#isActive').attr("checked","checked");
		}
		
		
	}
	$jquery("#ASSET_USAGE").validationEngine('attach');
	
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#ASSET_USAGE").validationEngine('validate')){ 
			var workingShiftId=Number($('#workingShiftId').val());
			var workingShiftName=$('#workingShiftName').val();
			var startTime=$('#startTime').val();
			var endTime=$('#endTime').val();
			var lateAttendance=$('#lateAttendance').val();
			var seviorLateAttendance=$('#seviorLateAttendance').val();
			var bufferTime=$('#bufferTime').val();
			var breakHours=$('#breakHours').val();
			var irregularDay=$('#irregularDay').val();
			
			var isActive=$('#isActive').attr("checked");
			var isDefault=$('#isDefault').attr("checked");
			var breakStart=$('#breakStart').val();
			var breakEnd=$('#breakEnd').val();
			var cmpDeptLocId=Number($('#cmpDeptLocId').val());
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(workingShiftId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/working_shift_save.action",
				data: {
					workingShiftId:workingShiftId,
					workingShiftName:workingShiftName,
					startTime:startTime,
					endTime:endTime,
					lateAttendance:lateAttendance,
					seviorLateAttendance:seviorLateAttendance,
					bufferTime:bufferTime,
					breakHours:breakHours,
					irregularDay:irregularDay,
					isActive:isActive,
					isDefault:isDefault,
					breakStart:breakStart,
					breakEnd:breakEnd,
					cmpDeptLocId:cmpDeptLocId,
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.commonErr').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.commonErr').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		returnCallTOList();
		$('#loading').fadeOut();
	});
   
   $('#breakStart,#breakEnd').change(function(){ 
	   calculateTime();
   });
   
   $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:550,
		bgiframe: false,
		modal: true 
	});
   
   
   $('#removeLocation').click(function(){
	   $('#locationName').val("");
		$('#cmpDeptLocId').val("");
   });
   
   $('#locationpopup').click(function(){
       $('.ui-dialog-titlebar').remove(); 
       $('#common-popup').dialog('open');
       var rowId=-1; 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
             data:{id:rowId},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.common-result').html(result);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	}); 
});
   function returnCallTOList(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/working_shift_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#codecombination-popup').dialog('destroy');
				$('#codecombination-popup').remove();	
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();	
				$("#main-wrapper").html(result); 
			}
	 	});
	}

function calculateTime(){
   /*  var valuestart = $("#breakStart").val();
    var valuestop = $("#breakEnd").val();
      if(valuestart!=null && valuestart!="" && valuestop!=null && valuestop!=""){
	     //create date format          
	     var timeStart = new Date("01/01/2007 " + valuestart).getHours();
	     var timeEnd = new Date("01/01/2007 " + valuestop).getHours();
	     var hourDiff = timeEnd - timeStart; 
	     $("#breakHours").val("0"+hourDiff+":00");
      } */
	var a = $("#breakStart").val();
	var b = $("#breakEnd").val();
	if(a!=null && a!="" && b!=null && b!=""){
		a+=":00";
		b+=":00";
		var difference = Math.abs(toSeconds(a) - toSeconds(b));

		// format time differnece
		var result = [
		    Math.floor(difference / 3600), // an hour has 3600 seconds
		    Math.floor((difference % 3600) / 60), // a minute has 60 seconds
		    difference % 60
		];
		// 0 padding and concatation
		result = result.map(function(v) {
		    return v < 10 ? '0' + v : v;
		}).join(':');
		$("#breakHours").val(result.substring(0,result.lastIndexOf(":")));
	}
}
function toSeconds(time_str) {
    // Extract hours, minutes and seconds
    var parts = time_str.split(':');
    // compute  and return total seconds
    return parts[0] * 3600 + // an hour has 3600 seconds
    parts[1] * 60 + // a minute has 60 seconds
    +
    parts[2]; // seconds
}

function departmentBranchPopupResult(locationId,locationName,commaseparatedValue){
	$('#locationName').val(locationName);
	$('#cmpDeptLocId').val(locationId);
	
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Shift Master Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			
			<form id="ASSET_USAGE" name="ASSET_USAGE" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" name="workingShiftId" id="workingShiftId" value="${WORKING_SHIFT_INFO.workingShiftId}"/>
					<fieldset style="min-height:145px;">
						   <div>
			                  <label class="width30">Shift Name<span style="color:red">*</span></label>
			                  <input type="text" class="width50 validate[required]" name="workingShiftName" id="workingShiftName" value="${WORKING_SHIFT_INFO.workingShiftName}"/>
					       </div>
				      	  <div>
				              <label class="width30">Start Time<span style="color:red">*</span></label>
				              <input type="text" class="width50 timePick validate[required]" name="startTime" id="startTime" value="${WORKING_SHIFT_INFO.startTimeStr}"/>
						  </div>
			              <div>
				              <label class="width30">End Time<span style="color:red">*</span></label>
				              <input type="text" class="width50  timePick validate[required]" name="endTime" id="endTime" value="${WORKING_SHIFT_INFO.endTimeStr}"/>
						  </div>
						  <div>
				              <label class="width30">Late Entry(After)</label>
				              <input type="text" class="width50  timePick " 
				              	name="lateAttendance" id="lateAttendance" value="${WORKING_SHIFT_INFO.lateAttendanceStr}"/>
						  </div>
			              <div>
				              <label class="width30">Severe Late Entry(After)</label>
				              <input type="text" class="width50  timePick " 
				              	name="seviorLateAttendance" id="seviorLateAttendance" value="${WORKING_SHIFT_INFO.seviorLateAttendanceStr}"/>
						  </div>
			             <div>
		                 	<input type="checkbox" id="isActive" class="width8">
		                 	<label class="width30">Active</label>
		                	 <input type="hidden" id="isActiveTemp" class="width8" value="${WORKING_SHIFT_INFO.isActive}">
		            	</div>
				        <div>
		                 	<input type="checkbox" id="isDefault" class="width8">
		                 	<label class="width30">Default</label>
		                	<input type="hidden" id="isDefaultTemp" class="width10" value="${WORKING_SHIFT_INFO.isDefault}">
		            	</div>
				 </fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="min-height:145px;">
					<div>
			              <label class="width30">Buffer Min/Hour(s)</label>
			              <input type="text" class="width50  timePick " 
			              	name="bufferTime" id="bufferTime" value="${WORKING_SHIFT_INFO.bufferTimeStr}"/>
				  </div>
				
				  <div>
		              <label class="width30">Break Start</label>
		              <input type="text" class="width50 timePick " name="breakStart" id="breakStart" value="${WORKING_SHIFT_INFO.breakStartStr}"/>
				  </div>
	              <div>
		              <label class="width30">Break End</label>
		              <input type="text" class="width50  timePick " name="breakEnd" id="breakEnd" value="${WORKING_SHIFT_INFO.breakEndStr}"/>
				  </div>
				  <div>
		              <label class="width30">Break Min/Hour(s)</label>
		              <input type="text" class="width50  timePick " 
		              	name="breakHours" id="breakHours" value="${WORKING_SHIFT_INFO.breakHoursStr}"/>
				  </div>
				  <div>
				  	<label class="width30">Irregular Day</label>
				  	<select class="width51 irregularDay" id="irregularDay" >
						<option value="">Select</option>
						<option value="SUNDAY">SUNDAY</option>
						<option value="MONDAY">MONDAY</option>
						<option value="TUESDAY">TUESDAY</option>
						<option value="WEDNESDAY">WEDNESDAY</option>
						<option value="THURSDAY">THURSDAY</option>
						<option value="FRIDAY">FRIDAY</option>
						<option value="SATURDAY">SATURDAY</option>
					</select>
					<input type="hidden" id="irregularDayTemp" value="${WORKING_SHIFT_INFO.irregularDay}" style="border:0px;"/>
				  </div>
				  <div>
						<label class="width30">Branch</label>
						<input type="text"  readonly="readonly"  id="locationName" 
								class="width40" value="${WORKING_SHIFT_INFO.cmpDeptLocation.location.locationName}"/>
						<span class="button" id="location"  style="position: relative; top:0px; ">
							<a style="cursor: pointer;" id="locationpopup" class="btn ui-state-default ui-corner-all width10"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
							<a id="removeLocation" style="cursor: pointer;margin:10px!important;background-color: red;" class="btn ui-state-default ui-corner-all width10">
								<span class="ui-icon ui-icon-close"> 
								</span> 
							</a>
						</span> 
						<input type="hidden" name="cmpDeptLocId" id="cmpDeptLocId"
							class="cmpDeptLocId"  value="${WORKING_SHIFT_INFO.cmpDeptLocation.cmpDeptLocId}" />  
					</div>
					
		           
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>