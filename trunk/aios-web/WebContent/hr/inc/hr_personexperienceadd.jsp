 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<tr style="background-color: #F8F8F8;"
	id="experiencechildRowId_${requestScope.rowid}">
	<td colspan="8" class="tdexperience"
		id="experiencechildRowTD_${requestScope.rowid}">
		<div id="errMsg"></div>
		<form name="experienceform" id="experienceform" style="position: relative;">
			<input type="hidden" name="addEditFlag" id="experienceaddEditFlag"
				value="${requestScope.AddEditFlag}" /> <input type="hidden"
				name="id" id="experiencelineId" value="${requestScope.rowid}" />
			<input type="hidden" name="experience.experiencesId"
				id="experiencesId" />

			<div class="float-left width50" id="hrm">
				<fieldset>
					<div>

						<div>
							<label class="width30">Previous Experience<span
								style="color: red">*</span> </label> <input type="text"
								id="previousExperience" class="width50 validate[required]"
								name="experience.previousExperience"
								value="${EXPERIENCE.previousExperience}" />
						</div>
						<div>
							<label class="width30">Current Post<span
								style="color: red">*</span> </label> <input type="text" id="currentPost"
								class="width50 validate[required]" name="experience.currentPost"
								value="${EXPERIENCE.currentPost}" />
						</div>
						<div>
							<label class="width30">Job Title<span style="color: red">*</span>
							</label> <input type="text" id="jobTitle"
								class="width50 validate[required]" name="experience.jobTitle"
								value="${EXPERIENCE.jobTitle}" />
						</div>
						<div>
							<label class="width30">Company<span style="color: red">*</span>
							</label> <input type="text" id="companyName"
								class="width50 validate[required]" name="experience.companyName"
								value="${EXPERIENCE.companyName}" />
						</div>
					</div>
				</fieldset>
			</div>
			<div class="float-left width50" id="hrm">
				<fieldset>
					<div>
						<label class="width30">Start Date</label> <input type="text"
							id="startDate" readonly="readonly" class="width50 startDate" name="startDate"
							value="${EXPERIENCE.startDate}" />
					</div>
					<div>
						<label class="width30">End Date</label> <input type="text"
							id="endDate" readonly="readonly" class="width50 endDate" name="endDate"
							value="${EXPERIENCE.endDate}" />
					</div>
					<div>
						<label class="width30">Address</label>
						<textarea id="location" class="width50" name="experience.location"></textarea>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
				style="margin: 20px;">
				<div
					class="portlet-header ui-widget-header float-right cancel_experience"
					id="cancel_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right add_experience"
					id="update_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
		</form>
	</td>

</tr>
<script type="text/javascript">
	 var trval=$('.tdexperience').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';
     var accessCode=null;
	 $(function(){  
		 $jquery("#experienceform").validationEngine('attach');

		 $('.startDate,.endDate').datepick({
			 onSelect: customRanges,showTrigger: '#calImg'});
		 clear:false;
		 
		//Lookup Data Roload call
		 	$('#save-lookup').live('click',function(){ 
		 		
				if(accessCode=="DEGREES"){
					$('#degree').html("");
					$('#degree').append("<option value=''>--Select--</option>");
					loadLookupList("degree");
					
				}
			}); 
		 $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
		 
		
		 
		  $('.cancel_experience').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdexperience').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#experienceaddEditFlag').val()!=null && $('#experienceaddEditFlag').val()=='A')
				{
  					$("#experienceAddImage_"+rowid).show();
  				 	$("#experienceEditImage_"+rowid).hide();
				}else{
					$("#experienceAddImage_"+rowid).hide();
  				 	$("#experienceEditImage_"+rowid).show();
				}
				 $("#experiencechildRowId_"+rowid).remove();	
				 $("#experienceDeleteImage_"+rowid).show();
				 $("#experienceWorkingImage_"+rowid).hide();  
				 
				 tempvar=$('.experiencetab>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#experienceDeleteImage_'+rowid).hide(); 
				 $('#openFlag').val(0);
				 return false;
		  });
		  $('.add_experience').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdexperience').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				
				var expIdId=$('#experiencesId').val();
			 	var previousExperience=$('#previousExperience').val();
			 	var currentPost=$('#currentPost').val();
			 	var jobTitle=$('#jobTitle').val();
			 	var companyName=$('#companyName').val();
			 	var startDate=$('#startDate').val();
			 	var endDate=$('#endDate').val();
			 	var location=$('#location').val();
			 //var tempObject=$(this);
			var addEditFlag=$('#experienceaddEditFlag').val();
		        if($jquery("#experienceform").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/person_experience_save.action", 
					 	async: false, 
					 	data:$("#experienceform").serialize(),
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 
			        	//Bussiness parameter
			        		$('#experiencesId_'+rowid).val(expIdId); 
		        			$('#previousExperience_'+rowid).text(previousExperience); 
		        			$('#currentPost_'+rowid).text(currentPost);
		        			$('#jobTitle_'+rowid).text(jobTitle); 
		        			$('#companyName_'+rowid).text(companyName); 
		        			$('#startDate_'+rowid).text(startDate);
		        			$('#endDate_'+rowid).text(endDate);
		        			$('#location_'+rowid).text(location);
		        			
						//Button(action) hide & show 
							 $("#experiencechildRowId_"+rowid).remove();	
		    				 $("#experienceAddImage_"+rowid).hide();
		    				 $("#experienceEditImage_"+rowid).show();
		    				 $("#experienceDeleteImage_"+rowid).show();
		    				 $("#experienceWorkingImage_"+rowid).hide(); 
		        			

		        			if(addEditFlag!=null && addEditFlag=='A')
		     				{
	         					$('.experienceaddrows').trigger('click');
	         					var experiencecount=Number($('#experiencecount').val());
	         					experiencecount+=1;
	         					$('#experiencecount').val(experiencecount);
	         					 tempvar=$('.experiencetab>tr:last').attr('id'); 
	         					 idval=tempvar.split("_");
	         					 rowid=Number(idval[1]); 
	         					 $('#experienceDeleteImage_'+rowid).show(); 
		     				}
		        		}		  	 
		      } 
		      else{return false;}
		  });
		return false;	
	 });
	 function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
	 function customRanges(dates) {
			if (this.id == 'startDate') {
				$('#endDate').datepick('option', 'minDate', dates[0] || null);  
				if($(this).val()!=""){
					return false;
				} 
				else{
					return false;
				}
			}
			else{
				$('#startDate').datepick('option', 'maxDate', dates[0] || null); 
				if($(this).val()!=""){
					return false;
				} 
				else{
					return false;
				}
			} 
		}
 </script>	