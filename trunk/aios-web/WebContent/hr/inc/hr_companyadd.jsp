<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" /> 
<script type="text/javascript">
var temp;
var tenantCombinationIdOld=0;
var supplierCombinationIdOld=0;
var customerCombinationIdOld=0;
var ownerCombinationIdOld=0;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
} 
$(document).ready(function(){  
	
	//manupulateLastRow();
	
	if('${isSupplier}' == "true") {
		$('#supplierinfo').val(true);
		$('#companyTypeId').val(8);
		$('#companyTypeId').attr('disabled', true);
		if(typeof($('#departmentID')!="undefined")){  
			$('#departmentId').val($('#departmentID').val());
			$('#locationId').val($('#locationID').val()); 
		}
		
	}else if('${isCustomer}' == "true") {
		$('#customerinfo').val(true);
		$('#companyTypeId').val(12);
		$('#companyTypeId').attr('disabled', true);
		if(typeof($('#departmentID')!="undefined")){  
			$('#departmentId').val($('#departmentID').val());
			$('#locationId').val($('#locationID').val()); 
		}
		
	}
	 else{
		$('#customerinfo').val(false);
		$('#supplierinfo').val(false);
		$('.deptloc').remove();   
		
	}	
	
	$("#companyTypeId").change(function(){
				
		var personTypeArray=new Array();
		$('#companyTypeId option:selected').each(function(){
			personTypeArray.push($(this).val());
		});
		//alert(personTypeArray);
		$('#companyTypeId option').each(function(){
			var personTypeId=$(this).val();
				
				
				//Tenant
				if(personTypeId==6 && $.inArray(personTypeId, personTypeArray)>=0){
					tenantShowHideCall(true);
				}else if(personTypeId==6 && $.inArray(personTypeId, personTypeArray)== -1){
					tenantShowHideCall(false);
				}
				
								
				//Supplier
				if(personTypeId==8 && $.inArray(personTypeId, personTypeArray)>=0){
					supplierShowHideCall(true);
					$("#tabs").tabs({
						  selected: 3 
						});
				}else if(personTypeId==8 && $.inArray(personTypeId, personTypeArray)== -1){
					supplierShowHideCall(false);
				}
				
				//Customer
				if(personTypeId==12 && $.inArray(personTypeId, personTypeArray)>=0){
					customerShowHideCall(true);
					$("#tabs").tabs({
						  selected: 4 
						});
				}else if(personTypeId==12 && $.inArray(personTypeId, personTypeArray)== -1){
					customerShowHideCall(false);
				}
		});
	});
    
	$('#issueDate').datepick({ 
		yearRange: 'any', showTrigger: '#calImg'}); 
	
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).get(0);   
	      $('.ui-dialog-titlebar').remove(); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
			return false;
	});

	$('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		height:550,
		bgiframe: false,
		modal: true 
	});
	   

	 $('.common-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();
			actionname=""; 
			actionname=$(this).attr('id');  
			if(actionname=='receive'){ 
				actionname="show_receive_list";
			} 
			else if(actionname=="accountid"){
		    	   bankId=Number(0);
		    	   actionname="getbankaccount_details";
		    }
			else{ 
				actionname="show_creditterm_details";
			} 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+actionname+".action", 
			 	async: false,   
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
				return false;
		});

		$('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		$jquery("#organization_details").validationEngine('attach');
	 
	$('.formError').remove();
	//$('.formErrorContent').remove();
	//$('.formErrorArrow').remove();


		$('.discard').click(function(){
			// alert("discard");
			var supplierinfo=$('#supplierinfo').val();
			var customerinfo=$('#customerinfo').val();
			if(supplierinfo==true || supplierinfo=='true'){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/supplier_list.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$("#main-wrapper").html(result);  
					}
			 	});
			}else if(customerinfo==true || customerinfo=='true'){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_all_customers.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('#codecombination-popup').dialog('destroy');
						$('#codecombination-popup').remove();		
						$("#main-wrapper").html(result);   
					}
			 	});
			}
			else{
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/company_list.action",
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('#codecombination-popup').dialog('destroy');
							$('#codecombination-popup').remove();	
					 		$("#main-wrapper").html(result);
						}	 	
					});	 
			}
			return false;
		 });

		// Ajax Call for Adding Person Details
		$('.save').click(function(){
			$('.error').hide();
			var companytype=$('#companyTypeId option:selected').text(); 
			companytype=$.trim(companytype);
			var combination=Number($('#combinationId').val()); 
			var tenantflag=false;
			 
			if(companytype=="Tenant" &&  combination==0){
				tenantflag=false;
			}
			else{
				tenantflag=true;
			}  
			if($jquery("#organization_details").validationEngine('validate'))
		    {
				if(tenantflag==true){
					var companyId=Number($('#companyId').val());
					var companyName=$('#companyName').val();
					var companyNameArabic=$('#companyNameArabic').val();
					var description=$('#description').val();
					var tradeName=$('#tradeId').val();
					var companyAddress=$('#companyAddress').val();
					var companyPobox=$('#companyPobox').val();
					var companyPhone=$('#companyPhone').val();
					var companyFax=$('#companyFax').val();
					var companyOrigin=$('#companyOrigin').val();
					var issueDate=$('#issueDate').val();
					
					var companyTypeList=new Array();
					$('#companyTypeId option:selected').each(function(){
						companyTypeList.push($(this).val());
					});
					var companyTypes="";
					for(var i=0; i<companyTypeList.length;i++){
						companyTypes+=companyTypeList[i]+",";
					}
					var tenantCombinationId=Number($('#tenantCombinationId').val()); 
					var supplierCombinationId=Number($('#supplierCombinationId').val()); 
					var customerCombinationId=Number($('#customerCombinationId').val()); 
					
					var supplierinfo=$('#supplierinfo').val();
					var customerinfo=$('#customerinfo').val();
					var supplierAcType=Number(0);
					var acClassification=Number(0);
					var paymentTermId=Number(0);
					var creditLimit=Number(0);
					var openingBalance=Number(0);
					var locationId=Number(0);
					var departmentId=Number(0);
					var bankName="";
					var branchName="";
					var iban="";
					var accountNumber="";
					var supplierNumber="";
					var labourCardId=$('#labourCardId').val();
					var supplierId=Number(0);
					var currencyId=Number(0);
					var isSupplierType=$('#supplierAcType').val(); 
					var customerNumber = "";
					var customerAcType =Number(0);
					var creditTermId = Number(0);
					var saleRepresentativeId = Number(0); 
					var billingAddress = "";
					var refferalSource = Number(0);
					var shippingMethod = Number(0); 
					var customerDescription= "";
					var shippingDetails ="";
					var customerId = Number(0);
					var memberCarType = Number(0);
					var cardNumber = Number(0); 
					var customerFlag = true; 
					if(typeof isSupplierType!="undefined"){
						supplierAcType=$('#supplierAcType').val();
						acClassification=$('#acClassification').val();
						paymentTermId=Number($('#paymentTermId').val());
						creditLimit=Number($('#creditLimit').val());
						openingBalance=Number($('#openingBalance').val());
						bankName=$('#bankName').val();
						branchName=$('#branchName').val();
						iban=$('#iban').val();
						accountNumber=$('#accountNumber').val();
						locationId=0;
						departmentId=0;
						currencyId=Number($('#currencyId').val());
						supplierId=Number($('#supplierId').val());
						supplierNumber=$('#supplierNumber').val(); 
						 if(supplierCombinationId==null || supplierCombinationId=='' || supplierCombinationId==0){
							$('#errordiv').hide().html("Please choose supplier account").slideDown();
							return false; 
						} 
					} else if(typeof $('#customerNumber').val() !="undefined") {
						customerNumber = $('#customerNumber').val();
						customerAcType = Number($('#customerAcType').val());
						creditTermId = Number($('#creditTermId').val());
						saleRepresentativeId = Number($('.saleRepresentativeId ').val());
						creditLimit =Number($('#creditLimit').val());
						billingAddress = $('#billingAddress').val();
						refferalSource = Number($('#refferalSource').val());
						shippingMethod = Number($('#shippingMethod').val());
						openingBalance = Number($('#openingBalance').val());
						customerDescription = $('#customerDescription').text();
						memberCarType = Number($('#memberCarType').val()); 
						cardNumber = Number($('#cardNumber').val());
						locationId=0;
						departmentId=0;
						shippingDetails = getShippingDetails();
						customerId = Number($('#customerId').val());
						if(shippingDetails!= null && shippingDetails !=""){
							customerFlag = true;
						} else {
							customerFlag = true;//false
						}
						 if(customerCombinationId==null || customerCombinationId=='' || customerCombinationId==0){
							$('#errordiv').hide().html("Please choose customer account").slideDown();
							return false; 
						} 
					}
					 
					var errorMessage=null;
					var successMessage="Successfully Created";
					if(companyId>0)
						var successMessage="Successfully Modified"; 
					if(customerFlag == true){ 
						 $.ajax({ 
								type: "POST", 
								url: "<%=request.getContextPath()%>/save_company.action", 
								data: {
										companyId:companyId,
										companyName:companyName,
										companyNameArabic: companyNameArabic,
										description:description,
										tradeName:tradeName,
										companyAddress: companyAddress,
										companyPobox: companyPobox,
										companyPhone: companyPhone,
										companyFax: companyFax,
										companyOrigin: companyOrigin,
										tradeNoIssueDate: issueDate,
										isSupplier:supplierinfo,
										supplierAcType:supplierAcType,
										acClassification:acClassification,
										paymentTermId:paymentTermId,
										openingBalance:openingBalance,
										bankName:bankName,
										branchName:branchName,
										ibanNumber:iban,
										accountNumber:accountNumber,
										creditLimit:creditLimit,
										locationId:locationId,
										departmentId:departmentId,
										currencyId:currencyId,
										labourCardId:labourCardId,
										supplierId: supplierId,
										supplierNumber: supplierNumber,
										customerNumber: customerNumber,
										customerAcType: customerAcType,
										creditTermId: creditTermId,
										saleRepresentativeId: saleRepresentativeId, 
										billingAddress: billingAddress,
										refferalSource: refferalSource,
										shippingMethod: shippingMethod, 
										customerDescription: customerDescription,
										shippingDetails: shippingDetails,
										customerId: customerId,
										isCustomer: customerinfo,
										tenantCombinationId:tenantCombinationId,
										supplierCombinationId:supplierCombinationId,
										customerCombinationId:customerCombinationId,
										memberCarType: memberCarType,
										cardNumber: cardNumber,
										companyTypes:companyTypes
								},
								async: false,
								contentType: "application/x-www-form-urlencoded; charset=UTF-8",
								dataType: "html",
								cache: false,
								success: function (result){ 
									$(".tempresult").html(result);
									 var message=$('.tempresult').html(); 
									 if(message.trim()=="SUCCESS"){
										
									 }else{
										 errorMessage=message;
									 }
									if(supplierinfo==true || supplierinfo=='true'){
											returnCallToSupplier();
									} else if (customerinfo==true || customerinfo=='true') {
										returnCallToCustomer();
									} else {
										returnCallToCompany();
									}
									if(errorMessage==null)
										$('.success').hide().html(successMessage).slideDown();
									else
										$('.error').hide().html(errorMessage).slideDown();
								},
								error: function(result){
									$('#codecombination-popup').dialog('destroy');
									$('#codecombination-popup').remove();		
									$("#main-wrapper").html(result);   
								}
							});
					} else {
						$('#errordiv').hide().html("Please add customer shipping details.").slideDown();
						$('#errordiv').delay(3000).slideUp();
						return false;
					}
					return true; 
				}
				else{
					$('#errordiv').hide().html("Please add account to company(tenant)").slideDown();
					$('#errordiv').delay(3000).slideUp();
					return false;
				}
			}		
			else{
				//$('#errordiv').hide().html("Please enter all the mandatory information").slideDown();
				return false;
			} 
			return false;
		});

		var getShippingDetails = function(){
			var shippingInfo = "";
			var shipname=new Array();
			var shipperson=new Array();
			var shipcontact=new Array();
			var shipaddress=new Array();
			var shipid=new Array(); 
			$('.customer-rowid').each(function(){ 
				var rowId=getRowId($(this).attr('id'));  
				var contactName=$('#contactName_'+rowId).val();
				var contactPerson=$('#contactPerson_'+rowId).val();
				var contactNo=$('#contactNo_'+rowId).val();
				var customerAddress=$('#customeraddress_'+rowId).val();  
				var shippingId=Number($('#shippingId_'+rowId).val()); 
				if(typeof contactName != 'undefined' && contactName!=null && contactPerson!="" && contactPerson!=null){
					shipname.push(contactName);
					shipperson.push(contactPerson); 
					shipid.push(shippingId);
					if(contactNo!=null && contactNo!="")
						shipcontact.push(contactNo);
					else
						shipcontact.push("##"); 
					if(customerAddress!=null && customerAddress!="")
						shipaddress.push(customerAddress);
					else
						shipaddress.push("##"); 
				} 
			});
			for(var j=0;j<shipname.length;j++){ 
				shippingInfo+=shipname[j]+"__"+shipperson[j]+"__"+shipcontact[j]+"__"+shipaddress[j]+"__"+shipid[j];
				if(j==shipname.length-1){   
				} 
				else{
					shippingInfo+="#@";
				}
			} 
			return shippingInfo;
		};
		
		$('#company_document_information').click(function(){
			var companyId=Number($("#companyId").val());
			if(companyId>0){
				AIOS_Uploader.openFileUploader("doc","companyDocs","Company",companyId,"CompanyDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","companyDocs","Company","-1","CompanyDocuments");
			}
		});	

		//Get file names which is uploded already
		var companyId=Number($("#companyId").val());
		if(companyId>0){
			//Defalut company type select
			
			//Uplodaded Docs can be visible
			populateUploadsPane("doc","companyDocs","Company",companyId);
			
			//Company Type Selected Items trigger
			companyTypeAutoSelectd();
			
			//Combination Temp Store for Re use to show/hide
			tenantCombinationIdOld=Number($('#tenantCombinationId').val());
			supplierCombinationIdOld=Number($('#supplierCombinationId').val());
			customerCombinationIdOld=Number($('#customerCombinationId').val());
			ownerCombinationIdOld=Number($('#ownerCombinationId').val());
		}
		
		
		 $('.location-popup').live('click',function(){
		        $('.ui-dialog-titlebar').remove();  
		        $('#job-common-popup').dialog('open');
		        var rowId=getRowId($(this).attr('id')); 
		           $.ajax({
		              type:"POST",
		              url:"<%=request.getContextPath()%>/get_location_for_setup.action",
		              data:{id:rowId},
		              async: false,
		              dataType: "html",
		              cache: false,
		              success:function(result){
		                   $('.job-common-result').html(result);
		                   return false;
		              },
		              error:function(result){
		                   $('.job-common-result').html(result);
		              }
		          });
		           return false;
		  }); 
		 
		 $('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:550,
				bgiframe: false,
				modal: true 
			});
		 
		 var tempvar=$('.identitytab>tr:last').attr('id'); 
		 var idval=tempvar.split("_");
		 var rowid=Number(idval[1]); 
		 $('#identityDeleteImage_'+rowid).hide(); 
		 
				 
		//Identity Detail adding
		 $(".identityaddData").click(function(){ 
			slidetab=$(this).parent().parent().get(0);  
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			if($jquery("#organization_details").validationEngine('validate')){
					$("#identityAddImage_"+rowid).hide();
					$("#identityEditImage_"+rowid).hide();
					$("#identityDeleteImage_"+rowid).hide();
					$("#identityWorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/company_identity_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			}
			else{
				return false;
			}	
			 
		});

		$(".identityeditData").click(function(){
			 
			 slidetab=$(this).parent().parent().get(0);  
			 
			 if($jquery("#organization_details").validationEngine('validate')) {
				 
					 
					//Find the Id for fetch the value from Id
						var tempvar=$(this).attr("id");
						var idarray = tempvar.split('_');
						var rowid=Number(idarray[1]); 
						$("#identityAddImage_"+rowid).hide();
						$("#identityEditImage_"+rowid).hide();
						$("#identityDeleteImage_"+rowid).hide();
						$("#identityWorkingImage_"+rowid).show(); 
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/company_identity_add.action",
					 data:{id:rowid,addEditFlag:"E"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){
					
				   $(result).insertAfter(slidetab);

				 //Bussiness parameter
		   			
		   			var identityType= $('#identityType_'+rowid).text();
		   			var identityNumber=$('#identityNumber_'+rowid).text(); 
      				var idendityExpireDate=$('#identityExpireDate_'+rowid).text(); 
      				var identityIssuedPlace=$('#identityIssuedPlace_'+rowid).text();
      				var identityDescription=$('#identityDescription_'+rowid).text();
		   			var identityTypeId= $('#identityTypeId_'+rowid).val(); 
		   			idendityExpireDate=idendityExpireDate.substring(0,11);
      				$('#identityType').val(identityTypeId);
      				$('#identityNumber').val(identityNumber);
      				$('#idendityExpireDate').val(idendityExpireDate);
      				$('#identityIssuedPlace').val(identityIssuedPlace);
      				$('#identityDescription').val(identityDescription);
				    $('#openFlag').val(1);
				  
				},
				error:function(result){
				//	alert("wee"+result);
				}
				
				});
				 			
			 }
			 else{
				 return false;
			 }	
			 
		});
		 
		 $(".identitydelrow").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
				var tempvar=$(this).attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 var lineId=$('#identitylineId_'+rowid).val();
			
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/company_identity_save.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.tempresult').html(result);   
						 if(result!=null){
							 $("#identityfieldrow_"+rowid).remove(); 
							  var identitycount=Number($('#identitycount').val()); 
	         				  identitycount-=1;
	         				  $('#identitycount').val(identitycount);
				        		//To reset sequence 
				        		var i=0;
				     			$('.identityrowid').each(function(){  
									i=i+1;
									$($($(this).children().get(4)).children().get(1)).val(i); 
			   					 }); 
						     }  
					},
					error:function(result){
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 });
				 
			 
		 });


		 $('.identityaddrows').click(function(){
			 var i=0;
			 var lastRowId=$('.identityrowid:last').attr('id');
				if(lastRowId!=null){
					var idarray = lastRowId.split('_');
					//alert(idarray);
					
					rowid=Number(idarray[1]);
					rowid=Number(rowid+1);
				}else{
					rowid=Number(1);
				}
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/company_identity_addrow.action",
					data:{id:rowid}, 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					$(".identitytab").append(result);
					//To reset sequence 
					 $('.identityrowid').each(function(){  
							i=i+1;
							var tempvar=$(this).attr("id");
							var idarray = tempvar.split('_');
							var rowid1=Number(idarray[1]); 
							$('#identitylineId_'+rowid1).val(i);
							
						});  
					} 
	  			
				});
			});

		 var supValue = $('#supplierAccount').val();
		 supValue=supValue.replace(/[\s\n\r]+/g, ' ' ).trim(); 
			$('#supplierAccount').val(supValue);
			
		 supValue = $('#customerAccount').val();
		 supValue=supValue.replace(/[\s\n\r]+/g, ' ' ).trim(); 
			$('#customerAccount').val(supValue);
			
			
		 
});
function companyTypeAutoSelectd() {
	 
	 var companyTypeArray=new Array();
	 var companyTypeTemp=$('#companyTypeIdTemp').val();
	 var companyTypeArray=companyTypeTemp.split(',');
	$('#companyTypeId option').each(function(){
		var txt=$(this).val();
		if($.inArray(txt, companyTypeArray)>=0)
			$(this).attr("selected","selected");
	});
	$('#companyTypeId').trigger('change');

}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tabS>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabS>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabS>tr:last').removeAttr('id');  
	$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
	$($('.tabS>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function setCombination(combinationTreeId,combinationTree){
	tempid=$(tempid).attr('id'); 
	if(typeof tempid!='undefined'&& tempid=="tenantCall"){
		$('#tenantAccount').val(combinationTree);
		$('#tenantCombinationId').val(combinationTreeId);   
	} 
	else if(typeof tempid!='undefined'&& tempid=="supplierCall"){
		$('#supplierAccount').val(combinationTree);
		$('#supplierCombinationId').val(combinationTreeId);   
	} 
	else if(typeof tempid!='undefined'&& tempid=="customerCall"){
		$('#customerAccount').val(combinationTree);
		$('#customerCombinationId').val(combinationTreeId);   
	} 
}
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function returnCallToCompany(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/company_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}
function returnCallToSupplier(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/supplier_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}
function returnCallToCustomer(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_all_customers.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){  
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();		
			$("#main-wrapper").html(result);  
		}
 	});
}
function identityShowHideCall(showFlag){
	if(showFlag==true){
		$("#identitytab").show();
		$("#tabs-2").show();
	}else{
		$("#identitytab").hide();
		$("#tabs-2").hide();
	}
}

function tenantShowHideCall(showFlag){
	if(showFlag==true){
		$("#tenanttab").show();
		$("#tabs-3").show();
		//$("#tenantAccount").addClass('validate[required]');
	}else{
		$("#tenanttab").hide();
		$("#tabs-3").hide();
		$("#tenantAccount").removeClass('validate[required]');
		if(tenantCombinationIdOld==0){
			$("#tenantAccount").val("");
			$("#tenantCombinationId").val("");
		}
	}
}
function supplierShowHideCall(showFlag){
	if(showFlag==true){
		$("#suppliertab").show();
		$("#tabs-4").show();
		//$("#supplierAccount").addClass('validate[required]');
	}else{
		$("#suppliertab").hide();
		$("#tabs-4").hide();
		$("#supplierAccount").removeClass('validate[required]');
		if(supplierCombinationIdOld==0){
			$("#supplierCombinationId").val("");
			$("#supplierAccount").val("");
		}
	}
}
function customerShowHideCall(showFlag){
	if(showFlag==true){
		$("#customertab").show();
		$("#tabs-5").show();
		//$("#customerAccount").addClass('validate[required]');
	}else{
		$("#customertab").hide();
		$("#tabs-5").hide();
		$("#customerAccount").removeClass('validate[required]');
		if(customerCombinationIdOld==0){
			$("#customerAccount").val("");
			$("#customerCombinationId").val("");
		}
	}
}
</script>			
<div id="main-content" class="hr_main_content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="hr.company.companyinfo"/></div>
        <div class="tempresult" style="display:none;"></div>	 
        <div class="portlet-content">
        
        	<div id="errordiv" class="error response-msg ui-corner-all" style="display: none;"></div>
	         <c:if test="${requestScope.errorMsg !=null && requestScope.errorMsg ne ''}">
				<div class="error response-msg ui-corner-all"><c:out value="${requestScope.errorMsg}"/></div>
			</c:if>
			<c:if test="${requestScope.successMsg !=null && requestScope.successMsg ne ''}">
		  	  <div class="success response-msg ui-corner-all"><c:out value="${requestScope.successMsg}"/></div> 
	    	</c:if>
			<div class="tempresult" style="display:none;"></div>
			<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
			<form name="OrganizationDetails" id="organization_details" style="position: relative;">
				<div class="width33 float-left" id="hrm">  
					<fieldset style="min-height:210px;">
            		<input type="hidden" name="companyId" id="companyId" class="width45" value="${COMPANY.companyId}" >
				     <div>
				    	 <label class="width40"><fmt:message key="hr.company.name"/><span style="color:red">*</span></label>
				     	<input type="text" name="companyName" id="companyName" class="width50 validate[required]" value="${COMPANY.companyName}"  maxlength="150" tabindex="1" >
				     </div>
				     <div>
				    	 <label class="width40"><fmt:message key="hr.company.namearabic"/></label>
				     	<input type="text" name="companyNameArabic" id="companyNameArabic" class="width50" value="${COMPANY.companyNameArabic}"  maxlength="150" tabindex="1" >
				     </div>
				     <div>
				     	<label class="width40"><fmt:message key="hr.company.type"/><span style="color:red">*</span></label>
			      		<input type="hidden" name="companyTypeIdTemp" id="companyTypeIdTemp" class="width45" value="${COMPANY.companyTypeDataIds}" >
			      		<select style="width: 51.5%;" name="companyTypeId" multiple class="validate[required]" id="companyTypeId">
								<c:if test="${requestScope.COMPANY_TYPE!=null}">
									<c:forEach items="${requestScope.COMPANY_TYPE}" var="nltlst">
										<option value="${nltlst.dataId}">${nltlst.displayName} </option>
									</c:forEach>
								</c:if>	
                           </select> 
				      </div>
				     				      
				     <div class="deptloc" style="display:none;">
				     	<div>
			    	 		<label class="width40"><fmt:message key="hr.company.department"/><span style="color:red">*</span></label>
			     			<select name="departmentId" id="departmentId" class="width50">
				     			<option value="">Select</option>
				     			<c:choose>
				     				<c:when test="${DEPARTMENT_LIST ne null && DEPARTMENT_LIST ne ''}">
				     					<c:forEach var="dbean" items="${DEPARTMENT_LIST}">
				     						<option value="${dbean.departmentId}">${dbean.departmentName}</option>
				     					</c:forEach>
				     				</c:when>
				     			</c:choose>
			     		</select>
				     </div> 
				      <div>
			    	 	<label class="width40"><fmt:message key="hr.company.location"/><span style="color:red">*</span></label>
			     			<select name="locationId" id="locationId" class="width50">
			     				<option value="">Select</option>
				     			<c:choose>
				     				<c:when test="${LOCATION_LIST ne null && LOCATION_LIST ne ''}">
				     					<c:forEach var="lbean" items="${LOCATION_LIST}">
				     						<option value="${lbean.locationId}">${lbean.locationName}</option>
				     					</c:forEach>
				     				</c:when>
				     			</c:choose>
				     		</select>
					    </div> 
				     </div>
			         <div>
			    	 	<label class="width40"><fmt:message key="hr.company.companyorigin"/></label>
			     		<input type="text" name="companyOrigin" id="companyOrigin" class="width50" value="${COMPANY.companyOrigin}" tabindex="5"  >
				     </div>  
				    </fieldset>
			    </div>
			    <div class="width33 float-left" id="hrm">
			    	 <fieldset style="height:210px;">
				      <div>
				    	 <label class="width30"><fmt:message key="hr.common.phone"/></label>
				     	<input type="text" name="companyPhone" id="companyPhone" class="width60" value="${COMPANY.companyPhone}" tabindex="6"  >
				     </div> 
				      <div>
				    	 <label class="width30"><fmt:message key="hr.common.fax"/></label>
				     	<input type="text" name="companyFax" id="companyFax" class="width60" value="${COMPANY.companyFax}" tabindex="7"  >
				     </div> 
				     <div>
				    	 <label class="width30"><fmt:message key="hr.common.address"/></label>
				     	<input type="text" name="companyAddress" id="companyAddress" class="width60" value="${COMPANY.companyAddress}" tabindex="8"  >
				     </div>  
				      <div>
				    	 <label class="width30"><fmt:message key="hr.common.pobox"/></label>
				     	<input type="text" name="companyPobox" id="companyPobox" class="width60" value="${COMPANY.companyPobox}" tabindex="3"  >
				     </div>   
				      <div>
				    	 <label class="width30"><fmt:message key="common.description"/></label>
				     	<textarea name="description" id="description" style="width: 61.5%;" tabindex="10">${COMPANY.description}</textarea>
				     </div> 
				    
				     </fieldset>
            	</div> 
            	<div class="width33 float-left" id="hrm">
			    	 <fieldset>
			    	 	<div  style="height: 210px;overflow-y:auto;">
						  	<div class="float-left">
								<span id="company_document_information" style="cursor: pointer; color: blue;">
									<u><fmt:message key="hr.company.documentuplode"/></u>
								</span>
							</div>
							<div class="width100 float-left">
								<span id="companyDocs"></span>
							</div>
						</div>
				     </fieldset>
            	</div> 
            	
            	<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
            	<div id="hrm" class="float-left width98">
            	 <fieldset style="margin:0px;" class="width99">
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
						<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1"><fmt:message key="person.tab.trade"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="identitytab"><a href="#tabs-2"><fmt:message key="person.tab.identity"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="tenanttab" style="display: none;"><a href="#tabs-3"><fmt:message key="person.tab.tenant"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="suppliertab" style="display: none;"><a href="#tabs-4"><fmt:message key="person.tab.supplier"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="customertab" style="display: none;"><a href="#tabs-5"><fmt:message key="person.tab.customer"/></a></li>
					</ul>
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1">
					<div id="hrm" class="hastable width50"  >
						<div style="position: relative;top:3px;">
					    	 <label class="width30"><fmt:message key="hr.company.tradenumber"/></label>
					     	<input type="text" name="tradeId" id="tradeId" class="width60" value="${COMPANY.tradeName}" tabindex="3"  >
					     </div> 
					     <div>
					    	 <label class="width30"><fmt:message key="hr.company.issueddate"/></label>
					     	<input type="text" name="issueDate" id="issueDate" value="${COMPANY.tradeNoIssueDate}" tabindex="4" 
					     		class="width60 " readonly="readonly" >
					     </div> 
					      <div id="account_no" style="display: none;">
							<div>
						    	<label class="width40"><fmt:message key="hr.company.labourcard"/></label>
						     	<input type="text" name="labourCardId" id="labourCardId" maxlength="13" class="width60" value="${COMPANY.labourCardId}" tabindex="12"  >
						     </div> 
					     </div>
					 </div>
				</div>
						
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2">
					<div id="hrm" class="hastable width100"  >
					<input type="hidden" id="identitycount" value="${fn:length(IDENTITY_LIST)}"/>
						<table id="identityhastab" class="width100"> 
								<thead>
									<tr>   
										<th class="width10"><fmt:message key="hr.person.identitytype"/></th>
										<th class="width10"><fmt:message key="hr.person.identitynumber"/></th>
										<th class="width10"><fmt:message key="hr.person.expiredate"/></th>
										<th class="width10"><fmt:message key="hr.person.issuedplace"/></th>
										<th class="width20"><fmt:message key="common.description"/></th>
										<th style="width:5%;"><fmt:message key="common.options"/></th>
									</tr>
								</thead>
								<tbody class="identitytab">
								 <c:forEach items="${requestScope.IDENTITY_LIST}" var="result" varStatus="status">                         
							        <tr id="identityfieldrow_${status.index+1}" class="identityrowid">
							    		<td class="width10" id="identityType_${status.index+1}">${result.identityTypeName}</td>
							            <td class="width10" id="identityNumber_${status.index+1}">${result.identityNumber}</td>
							            <td class="width10" id="identityExpireDate_${status.index+1}">${result.expireDate}</br>
							            [<span style="font-weight: bold;color: blue;">${result.daysLeft}</span>] day's left</td>
							           	<td class="width10" id="identityIssuedPlace_${status.index+1}">${result.issuedPlace}</td>
							           <td class="width20" id="identityDescription_${status.index+1}">${result.description}</td>
							             <td style="width:5%;" id="identityoption_${status.index+1}"> 
							             			<input type="hidden" name="identityTypeId_${status.index+1}" id="identityTypeId_${status.index+1}"  value="${result.identityType}"/>
							                		<input type="hidden" name="identityId_${status.index+1}" id="identityId_${status.index+1}"  value="${result.identityId}"/>  
							                		<input type="hidden" name="lineId_${status.index+1}" id="lineId_${status.index+1}" value="${status.index+1}"/>  
						                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddData" id="identityAddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												 	 <span class="ui-icon ui-icon-plus"></span>
												   </a>
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityeditData" id="identityEditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												   </a> 
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrow" id="identityDeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												   </a>
												    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="identityWorkingImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
												   </a>
							                  
										</td> 
							        </tr> 
							    </c:forEach>
							    <c:forEach var="i" begin="${fn:length(IDENTITY_LIST)+1}" end="${fn:length(IDENTITY_LIST)+1}" step="1" varStatus ="status">  
									<tr id="identityfieldrow_${i}" class="identityrowid"> 
										<td class="width10" id="identityType_${i}"></td>
										<td class="width10" id="identityNumber_${i}"></td>
							            <td class="width10" id="identityExpireDate_${i}"></td>
							           	<td class="width10" id="identityIssuedPlace_${i}"></td>
							           	<td class="width20" id="identityDescription_${i}"></td>
								 		<td  style="width:5%;" id="identityoption_${i}">
								 			<input type="hidden" name="identityTypeId_${i}" id="identityTypeId_${i}"  value="0"/>
								 			<input type="hidden" name="identityId_${i}" id="identityId_${i}"  value="0"/>  
								 			<input type="hidden" name="identitylineId_${i}" id="identitylineId_${i}" value="${i}"/>  
										  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddData" id="identityAddImage_${i}"  title="Add Record">
										 	 <span class="ui-icon ui-icon-plus"></span>
										   </a>	
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityeditData"  id="identityEditImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
											</a> 
											<a style="cursor: pointer; display: none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrow"  id="identityDeleteImage_${i}" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="identityWorkingImage_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td> 
										
									</tr>
							 	</c:forEach>
							 </tbody>
						   </table>
						   <div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
								<div class="portlet-header ui-widget-header float-left identityaddrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
							</div>
					</div>
				</div>	
				
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3">
					<div id="hrm" class="float-left width100"  >
						<div class="float-left width100">
	                      	<label class="width20"><fmt:message key="hr.person.tenantAccount"/></label>
	                      	<c:choose>
		                      	<c:when test="${TENANT_ACCOUNT.accountByAnalysisAccountId ne null && TENANT_ACCOUNT.accountByAnalysisAccountId ne ''}">
			                      	<input type="text" class="width50" title="" readonly="readonly" name="tenantAccount" id="tenantAccount" 
			                      	value="${TENANT_ACCOUNT.accountByAnalysisAccountId.account} [${TENANT_ACCOUNT.accountByCompanyAccountId.code}
			                      	.${TENANT_ACCOUNT.accountByCostcenterAccountId.code}
			                      	.${TENANT_ACCOUNT.accountByNaturalAccountId.code}
			                      	.${TENANT_ACCOUNT.accountByAnalysisAccountId.code}]"/>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<input type="text" class="width50" title="" readonly="readonly" name="tenantAccount" id="tenantAccount" value=""/>
		                      	</c:otherwise>
			                 </c:choose>
			                 <c:if test="${TENANT_ACCOUNT.accountByAnalysisAccountId eq null || TENANT_ACCOUNT.accountByAnalysisAccountId eq	''}">
		                      	<span class="button" style="position: relative;top:6px; ">
									<a style="cursor: pointer;" id="tenantCall" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</c:if>
							<input type="hidden" name="tenantCombinationId" id="tenantCombinationId" value="${TENANT_ACCOUNT.combinationId}"/> 
                      	</div>
                      	<div class="width100 float-left" id="tenant-result-div"></div>
					</div>
				</div>
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4">
					<div id="hrm" class="float-left width100"  >
						<div class="float-left width100">
	                      	<label class="width20"><fmt:message key="hr.person.supplierAccount"/><span style="color:red">*</span></label>
	                      	<c:choose>
		                      	<c:when test="${SUPPLIER_ACCOUNT.accountByAnalysisAccountId ne null && SUPPLIER_ACCOUNT.accountByAnalysisAccountId ne ''}">
			                      	<input type="text" class="width50 validate[required]" title="" readonly="readonly" name="supplierAccount" id="supplierAccount" 
			                      	value="${SUPPLIER_ACCOUNT.accountByAnalysisAccountId.account} [${SUPPLIER_ACCOUNT.accountByCompanyAccountId.code}
			                      	.${SUPPLIER_ACCOUNT.accountByCostcenterAccountId.code}
			                      	.${SUPPLIER_ACCOUNT.accountByNaturalAccountId.code}
			                      	.${SUPPLIER_ACCOUNT.accountByAnalysisAccountId.code}]"/>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<input type="text" class="width50 validate[required]" title="" readonly="readonly" name="supplierAccount" id="supplierAccount" value=""/>
		                      	</c:otherwise>
			                 </c:choose>
			                 <c:if test="${SUPPLIER_ACCOUNT.accountByAnalysisAccountId eq null || SUPPLIER_ACCOUNT.accountByAnalysisAccountId eq	''}">
		                      	<span class="button" style="position: relative;top:0px; ">
									<a style="cursor: pointer;" id="supplierCall" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</c:if>
							<input type="hidden" name="supplierCombinationId" id="supplierCombinationId" value="${SUPPLIER_ACCOUNT.combinationId}"/> 
                      	</div>
                      	<div class="width100 float-left" id="supplier-result-div"></div>
					</div>
				</div>
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-5">
					<div id="hrm" class="float-left width100" >
						<div class="float-left width100">
	                      	<label class="width20"><fmt:message key="hr.person.customerAccount"/><span style="color:red">*</span></label>
	                      	<c:choose>
		                      	<c:when test="${CUSTOMER_ACCOUNT.accountByAnalysisAccountId ne null && CUSTOMER_ACCOUNT.accountByAnalysisAccountId ne ''}">
			                      	<input type="text" class="width50 validate[required]" title="" readonly="readonly" name="customerAccount" id="customerAccount" 
			                      	value="${CUSTOMER_ACCOUNT.accountByAnalysisAccountId.account} [${CUSTOMER_ACCOUNT.accountByCompanyAccountId.code}
			                      	.${CUSTOMER_ACCOUNT.accountByCostcenterAccountId.code}
			                      	.${CUSTOMER_ACCOUNT.accountByNaturalAccountId.code}
			                      	.${CUSTOMER_ACCOUNT.accountByAnalysisAccountId.code}]"/>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<input type="text" class="width50 validate[required]" title="" readonly="readonly" name="customerAccount" id="customerAccount" value=""/>
		                      	</c:otherwise>
			                 </c:choose>
			                 <c:if test="${CUSTOMER_ACCOUNT.accountByAnalysisAccountId eq null || CUSTOMER_ACCOUNT.accountByAnalysisAccountId eq	''}">
		                      	<span class="button" style="position: relative;top:0px; ">
									<a style="cursor: pointer;" id="customerCall" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</c:if>
							<input type="hidden" name="customerCombinationId" id="customerCombinationId" value="${CUSTOMER_ACCOUNT.combinationId}"/> 
                      	</div>
                      	<div class="width100 float-left" id="customer-result-div"></div>
					</div>
				</div>
				</fieldset></div>
			</div> 
				<div class="float-right buttons ui-widget-content ui-corner-all">
				<input type="hidden" id="supplierinfo"/>
				<input type="hidden" id="customerinfo"/>
					<div class="portlet-header ui-widget-header float-right discard"><fmt:message key="common.button.cancel"/></div>
					
				 	<div class="portlet-header ui-widget-header float-right save"><fmt:message key="organization.button.save"/></div> 
				</div>
			</form>
		</div>
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 		</div> 
 		
    </div>
</div>

