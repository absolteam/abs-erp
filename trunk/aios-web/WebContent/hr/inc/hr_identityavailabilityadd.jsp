<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
</style>
<script type="text/javascript">
var currentObjectIdentiy="";
var doucmentSplits = [];
$(function (){  
	

	$('.handoverDate').datepick();
	$('.expectedReturnDate').datepick();
	$('.actualReturnDate').datepick();
	 manupulateLastRow();

	var identityAvailabilityMasterId=Number($('#identityAvailabilityMasterId').val());
	if(identityAvailabilityMasterId>0){
		$('#status').val($('#statusTemp').val());
		
	}
	$jquery("#ASSET_USAGE").validationEngine('attach');

	
	 
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#ASSET_USAGE").validationEngine('validate')){ 
			var identityAvailabilityMasterId=Number($('#identityAvailabilityMasterId').val());
			var handoverDate=$('#handoverDate').val();
			var purpose=$('#purpose').val();
			var personId=$('#personId').val();
			var description=$('#description').val();
			var department=$('#department').val();
			var personName=$('#personName').val();
			var status=Number($('#status').val());
			doucmentSplits = getDocumentSplits();
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(identityAvailabilityMasterId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/identity_availability_save.action",
				data: {
					identityAvailabilityMasterId:identityAvailabilityMasterId,
					handoverDate:handoverDate,
					purpose:purpose,
					description:description,
					personId:personId,
					status:status,
					department:department,
					personName:personName,
					doucmentSplits:JSON.stringify(doucmentSplits)
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   var getDocumentSplits = function(){
		 documentSplits = [];
		 $('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var identityId = Number($('#identityId_'+rowId).val());  
			var documentName = $('#documentName_'+rowId).val();  
			var handoverDate =  $('#handoverDate_'+rowId).val(); 
			var expectedReturnDate = $('#expectedReturnDate_'+rowId).val();  
			var actualReturnDate = $('#actualReturnDate_'+rowId).val();  
			var purpose=$('#purpose_'+rowId).val();    
			var identityAvailabilityId = $('#identityAvailabilityId_'+rowId).val();
			if(typeof documentName != 'undefined' && documentName!=''){
				var jsonData = [];
				jsonData.push({
					"identityId" : identityId,
					"documentName" : documentName,
					"handoverDate": handoverDate,
					"expectedReturnDate": expectedReturnDate,
					"actualReturnDate" : actualReturnDate,
					"purpose" : purpose,
					"identityAvailabilityId" : identityAvailabilityId,
				});   
				documentSplits.push({
					"documentSplits" : jsonData
				});
			} 
		});  
		return documentSplits;
	 };
	 
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		returnCallTOList();
		$('#loading').fadeOut();
	});
   
   $('.identityCall').live('click',function(){
       $('.ui-dialog-titlebar').remove(); 
       currentObjectIdentiy=$(this).attr("id");
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/common_identity_list.action",
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.common-result').html(result); 
                  $('#common-popup').dialog('open');
				  $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	}); 
   
   $('#employeepopup').click(function(){
       $('.ui-dialog-titlebar').remove();  
       var rowId=-1; 
       var personTypes="ALL";
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/common_person_list.action",
             data:{id:rowId,personTypes:personTypes},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.common-result').html(result); 
                  $('#common-popup').dialog('open');
				  $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 }); 
	 
 $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:950,
		height:600,
		bgiframe: false,
		modal: true 
	});
 $('#identity-close').live('click',function(){
	 $('#common-popup').dialog('close');
 });
 $('#person-list-close').live('click',function(){
	 $('#common-popup').dialog('close');
 });
 
 
 $('.addrows').click(function(){ 
	  var i=Number(1); 
	  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
	  var handoverDate=$('#handoverDate').val();
	  id=id+1;  
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/identity_availability_addrow.action", 
		 	async: false,
		 	data:{rowId: id,handoverDate:handoverDate},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('.tab tr:last').before(result);
				 if($(".tab").height()>255)
					 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
				 $('.rowid').each(function(){   
		    		 var rowId=getRowId($(this).attr('id')); 
		    		 $('#lineId_'+rowId).html(i);
					 i=i+1; 
		 		 }); 
			}
		});
	  
	  return false;
 }); 
 

//---------------------Delete Row section-----------------	 

 $('.delrow').live('click',function(){ 
	 slidetab=$(this).parent().parent().get(0);  
   	 $(slidetab).remove();  
   	 var i=1;
	 $('.rowid').each(function(){   
		 var rowId=getRowId($(this).attr('id')); 
		 $('#lineId_'+rowId).html(i);
		 i=i+1; 
		 });   
 }); 
 
	 $('.documentName').live('change',function(){ 
		 var rowId=getRowId($(this).attr('id')); 
		 triggerAddRow(rowId);
	}); 

});

function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	

}

 function returnCallTOList(){
	$.ajax({
	type:"POST",
	url:"<%=request.getContextPath()%>/identity_availability_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}


function identityPopupResult(identityId,identityType,extraParam){
	var rowId=getRowId(currentObjectIdentiy); 
	$('#identityType_'+rowId).val(identityType);
	$('#identityId_'+rowId).val(identityId);
	var idval=extraParam.split('@#');
	$('#documentName_'+rowId).val(idval[0] +" ("+idval[1]+")");
	
	$('#common-popup').dialog("close");
	triggerAddRow(rowId);
}

function personPopupResult(personId,personName,commonParam){
	$('#personName').val(personName);
	$('#personId').val(personId);
}


function triggerAddRow(rowId) {
	var documentName= Number($('#documentName_' + rowId).val());
	var nexttab = $('#fieldrow_' + rowId).next();
	if (documentName!=null && documentName!=''&& 
			$(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
	}
}

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Document
			HandOver Information
		</div>
		<div class="tempresult" style="display: none;"></div>
		<div class="response-msg error commonErr ui-corner-all"
			style="display: none;"></div>
		<c:choose>
			<c:when
				test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10"> Comment From :<span>
								${COMMENT_IFNO.name}</span>
						</label> <label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div>
			</c:when>
		</c:choose>
		<form id="ASSET_USAGE" name="ASSET_USAGE" style="position: relative;">
			<div id="hrm" class="float-left width50">
				<input type="hidden" name="identityAvailabilityMasterId"
					id="identityAvailabilityMasterId"
					value="${HANDOVER_INFO.identityAvailabilityMasterId}" /> <input
					type="hidden" name="recordId" id="recordId"
					value="${requestScope.recordId}" /> <input type="hidden"
					name="alertId" id="alertId" value="${requestScope.alertId}" />
				<fieldset style="height: 150px;">
					<div>
						<label class="width30">Reference<span style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required]" 
							name="referenceNumber" id="referenceNumber" readonly="readonly"
							value="${HANDOVER_INFO.referenceNumber}" />
					</div>
					<div>
						<label class="width30">Process<span style="color: red">*</span>
						</label> <input type="hidden" id="statusTemp"
							value="${HANDOVER_INFO.status}" /> <select
							class="width51 validate[required]" name="status" id="status">
							<option value="1">Employer To Employee</option>
							<option value="2">Employee To Employer</option>
							<option value="3">Others</option>
						</select>
					</div>
					<div>
						<label class="width30">HandOver TO<span style="color: red">*</span>
						</label> <input type="text"
							class="width50 validate[required]" title=""
							name="personName" id="personName"
							value="${HANDOVER_INFO.personName}" /> <span class="button"
							style="position: relative; top: 0px;"> <a
							style="cursor: pointer;" id="employeepopup"
							class="btn ui-state-default ui-corner-all width100 common-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
							type="hidden" name="personId" id="personId"
							value="${HANDOVER_INFO.personByHandoverTo.personId}" />
					</div>
					<div>
						<label class="width30">Department
						</label> <input type="text" class="width50" 
							name="department" id="department"
							value="${HANDOVER_INFO.department}" />
					</div>
					<div>
						<label class="width30">HandOver Date<span
							style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required] handoverDate" 
							name="handoverDate" id="handoverDate"
							value="${HANDOVER_INFO.handoverDateStr}" />
					</div>
					

				</fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="height: 150px;">

					<div>
						<label class="width30">Purpose</label>
						<textarea class="width61" id="purpose">${HANDOVER_INFO.purpose}</textarea>
					</div>
					<div>
						<label class="width30">Description</label>
						<textarea class="width61" id="description">${HANDOVER_INFO.description}</textarea>
					</div>
					
				</fieldset>
			</div>
			<div id="hrm" class="hastable width100 float-left">
				<fieldset style="min-height: 100px;">
					<legend>Document Details</legend>
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 10%;">Document</th>
										<th style="width: 15%;">Details</th>
										<th style="width: 5%;">Handover Date</th>
										<th style="width: 5%;">Expected Return Date</th>
										<th style="width: 5%;">Returned Date</th>
										<th style="width: 15%;">Purpose</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${HANDOVER_INFO.identityAvailabilityVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td>
												<input type="text" class="width70"
												readonly="readonly" name="identityType" id="identityType_${status.index+1}"
												value="${DETAIL.identityType}" /> <span class="button"
												style="position: relative; top: 0px;"> <a
												style="cursor: pointer;" id="identityCall_${status.index+1}"
												class="btn ui-state-default ui-corner-all width100 identityCall common-popup">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" name="identityId" id="identityId_${status.index+1}"
												value="${DETAIL.identity.identityId}" />	
												
													
											</td>
											<td><input type="text" name="documentName"
												id="documentName_${status.index+1}" value="${DETAIL.identityName}"
												class="width90"> 
											</td>
											<td><input type="text" name="handoverDate"
												id="handoverDate_${status.index+1}" value="${DETAIL.handoverDateStr}"
												class="width90"> 
											</td>
											<td><input type="text" name="expectedReturnDate"
												id="expectedReturnDate_${status.index+1}" value="${DETAIL.expectedReturnDateStr}"
												class="width90"> 
											</td>
											<td><input type="text" name="actualReturnDate"
												id="actualReturnDate_${status.index+1}" value="${DETAIL.actualReturnDateStr}"
												class="width90"> 
											</td>
											<td><input type="text" name="purpose"
												id="purpose_${status.index+1}"
												value="${DETAIL.purpose}" class="width98"
												maxlength="1000">
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="identityAvailabilityId" id="identityAvailabilityId_${status.index+1}"
												value="${DETAIL.identityAvailabilityId}" />	
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(HANDOVER_INFO.identityAvailabilityVOs)+1}"
										end="${fn:length(HANDOVER_INFO.identityAvailabilityVOs)+2}"
										step="1" varStatus="status">

										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td>
												<input type="text" class="width70"
												readonly="readonly" name="identityType" id="identityType_${i}"
												/> <span class="button"
												style="position: relative; top: 0px;"> <a
												style="cursor: pointer;" id="identityCall_${i}"
												class="btn ui-state-default ui-corner-all width100 identityCall common-popup">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" name="identityId" id="identityId_${i}"
												 />	
												
											</td>
											<td><input type="text" name="documentName"
												id="documentName_${i}" 
												class="width90"> 
											</td>
											<td><input type="text" name="handoverDate"
												id="handoverDate_${i}" 
												class="width90 handoverDate"> 
											</td>
											<td><input type="text" name="expectedReturnDate"
												id="expectedReturnDate_${i}" 
												class="width90 expectedReturnDate"> 
											</td>
											<td><input type="text" name="actualReturnDate"
												id="actualReturnDate_${i}" 
												class="width90 actualReturnDate"> 
											</td>
											<td><input type="text" name="purpose"
												id="purpose_${i}"
												 class="width98"
												maxlength="1000">
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												 <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <input
												type="hidden" name="identityAvailabilityId" id="identityAvailabilityId_${i}"
												/>		
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							
						</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>
		<div
			class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right discard"
				id="discard">
				<fmt:message key="common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right save"
				id="save">
				<fmt:message key="common.button.save" />
			</div>
		</div>
	</div>
	
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
		<div class="portlet-header ui-widget-header float-left addrows"
			style="cursor: pointer; display: none;">
			<fmt:message key="accounts.common.button.addrow" />
		</div>
	</div>
			
	<div
		style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		<div
			class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
			unselectable="on" style="-moz-user-select: none;">
			<span class="ui-dialog-title" id="ui-dialog-title-dialog"
				unselectable="on" style="-moz-user-select: none;">Dialog
				Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
				role="button" unselectable="on" style="-moz-user-select: none;"><span
				class="ui-icon ui-icon-closethick" unselectable="on"
				style="-moz-user-select: none;">close</span>
			</a>
		</div>
		<div id="common-popup" class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>
</div>