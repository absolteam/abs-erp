<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">

</style> 
<script type="text/javascript">
var leaveId=0;var leavePolicyId=0;
$(function (){  
	
	
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($("#LEAVE_MASTER").validationEngine({returnIsValid:true})){ 
			var leaveId=Number($('#leaveId').val());
			var leaveTypeCode=$('#leaveTypeCode').val();
			
			var isDefalut=$('#isDefalut').attr("checked");
			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/attendance_policy_save.action",
				data: {
					leaveId:leaveId,
					policyName:policyName,
					processType:processType,
					allowOt:allowOt,
					allowCompoff:allowCompoff,
					isDefalut:isDefalut,
					otHoursForHalfday:otHoursForHalfday,
					otHoursForFullday:otHoursForFullday,
					compoffHoursForHalfday:compoffHoursForHalfday,
					compoffHoursForFullday:compoffHoursForFullday,
					lopHoursForHalfday:lopHoursForHalfday,
					lopHoursForFullday:lopHoursForFullday
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#reset').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		leaveId=0;
		getLeaveMasterInfo();
		$('#loading').fadeOut();
	});
});

function getLeaveMasterInfo(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/get_leave_master_info.action", 
		data:{leaveId:leaveId},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			$("#LEAVE_MASTER_INFO").html(result); 
		}
 	});
}



</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Master & Policy Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<div>
				<fieldset>
					<legend>Leave Master</legend>
					<form id="LEAVE_MASTER" name="LEAVE_MASTER">
						<div id="hrm" class="float-left width30"  >
							<fieldset>
								   
					       	 
						   </fieldset>
						</div>
						<div id="hrm" class="float-left width70">
							<fieldset>
							<div id="LEAVE_MASTER_INFO">
							
				            </div>
				           <div class="clearfix"></div>  
							<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
								<div class="portlet-header ui-widget-header float-right reset" id="reset" >Reset/New</div>
								<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
							</div>
							</fieldset>
						</div>
					</form>
				</fieldset>
		</div>
		
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>