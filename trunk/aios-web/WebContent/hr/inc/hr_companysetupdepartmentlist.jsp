<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var locationId=0;var locationName=''; var description="";var parentDepartmentId="";
$(function(){ 
	var companyId=Number($("#companyId").val());
	$('#Department').dataTable({ 
		"sAjaxSource": "get_departments_list_json.action?companyId="+companyId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Department ID', "bVisible": false},
			{ "sTitle": 'Department'},
			{ "sTitle": 'Parent Department'},
			{ "sTitle": 'Description'},
			{ "sTitle": 'ParentdepartmentId', "bVisible": false},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Department').dataTable();

	/* Click event handler */
	$('#Department tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1];
	        description=aData[3];
	        parentDepartmentId=aData[4];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1];
	        description=aData[3];
	        parentDepartmentId=aData[4];

	    }
	});
	$('#Department tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        designationId=aData[0];
	        locationName=aData[1];
	        description=aData[3];
	        parentDepartmentId=aData[4];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1];
	        description=aData[3];
	        parentDepartmentId=aData[4];
	    }
		  var rowid=$("#rowId").val();
		$("#department_"+rowid).val(locationName);
		$("#departmentId_"+rowid).val(locationId);
		$("#parentDepartment_"+rowid).val(parentDepartmentId);
		$("#description_"+rowid).val(description);
		$("#department_"+rowid).attr('disabled','disabled');
		$("#description_"+rowid).attr('disabled','disabled');
		$("#parentDepartment_"+rowid).attr('disabled','disabled');
		$('#job-common-popup').dialog("close");

	});
	$('.department-popup-discard').click(function () { 
		$('#job-common-popup').dialog("close");
	});
});
</script>

<div id="main-content"> 
	<input type="hidden" id="rowId" value="${rowId}"/>
 	<div id="trans_combination_accounts">
		<table class="display" id="Department"></table>
	</div> 
	<div class="float-right buttons ui-widget-content ui-corner-all">
		<div class="portlet-header ui-widget-header float-right department-popup-discard"><fmt:message key="common.button.close"/></div>
 	</div>
</div>