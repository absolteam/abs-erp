<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<tr class="rowidC" id="fieldrowC_${rowId}">
	<td id="lineIdC_${rowId}" style="display:none;">${rowId}</td>
	<td>
		<select class="width90 allowance" id="allowance_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
				<option value="${element.payrollElementId}">${element.elementName}[${element.elementType}]</option>
			</c:forEach>
		</select>
	</td> 
	<td>
		<select class="width90 payPolicy" id="payPolicy_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
				<option value="${policy.key}">${policy.value}</option>
			</c:forEach>
		</select>
		<input type="hidden" id="payPolicyId_${rowId}" style="border:0px;"/>
	</td> 
	<td>
		<select class="width90 payType" id="payType_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="mode" items="${PAY_MODE}" varStatus="statusMode">
				<option value="${mode.key}">${mode.value}</option>
			</c:forEach>
		</select>
		<input type="hidden" id="payTypeId_${rowId}" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width80 amount" id="amount_${rowId}" style="border:0px;text-align: right;"/>
	</td> 
	<td>
		<select class="width90 calculationType" id="calculationType_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="mode" items="${CALCULATION_TYPE}" varStatus="statusMode">
				<option value="${mode.key}">${mode.value}</option>
			</c:forEach>
		</select>
		<input type="hidden" id="calculationTypeId_${rowId}" value="" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width80 count" id="count_${rowId}" value="" style="border:0px;text-align: right;"/>
	</td>
	<td>
		<select class="width90 dependentElement" id="dependentElement_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
				<option value="${element.payrollElementId}">${element.elementName}</option>
			</c:forEach>
		</select>
		<input type="hidden" id="dependentElementId_${rowId}" value="" style="border:0px;"/>
	</td> 
	<td style="display:none;">
		<input type="hidden" id="jobPayrollElementId_${rowId}" style="border:0px;"/>
	</td>  
	<td style="width:0.01%;" class="opn_td" id="optionc_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>