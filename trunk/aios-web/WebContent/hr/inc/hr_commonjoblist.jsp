<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var jobId=0;var jobName='';var jobNumber="";commonParam="";
$(function(){ 
	var designationId=Number($('#tempJobDesignationId').val());    
	$('#Job').dataTable({ 
		"sAjaxSource": "common_job_list_json.action?designationId="+designationId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'JobId', "bVisible": false},
			{ "sTitle": 'Job Template',"sWidth": "150px"},
			{ "sTitle": 'Reference No.',"sWidth": "50px"},
			{ "sTitle": 'Salaries'},
			{ "sTitle": 'Leaves'},
			{ "sTitle": 'Shifts'},
			{ "sTitle": 'DesignationId', "bVisible": false},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Job').dataTable();

	/* Click event handler */
	$('#Job tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobName=aData[1]+" ["+aData[2]+"]";
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" ["+aData[2]+"]";
	    }
	});
	$('#Job tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobName=aData[1]+" ["+aData[2]+"]";
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobName=aData[1]+" ["+aData[2]+"]";
	        
	        //TemplateName++Reference++Designation++Grade++DesignationId
	        commonParam=aData[1]+"@&"+aData[2]+"@&"+aData[3]+"@&"+aData[4]+"@&"+aData[9];
	    }
		jobPopupResult(jobId,jobName,commonParam);
		$('#common-popup').dialog("close");
	});
	
	$('#job-list-close').click(function () { 
		$('#common-popup').dialog("close");
	});
});
</script>
<div id="main-content"> 
	<input type="hidden" id="tempJobDesignationId" value="${JOB_DESIGNATION_ID}">
 	<div id="trans_combination_accounts">
		<table class="display" id="Job"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="job-list-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div> 
</div>