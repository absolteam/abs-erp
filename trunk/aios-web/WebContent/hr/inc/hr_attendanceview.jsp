<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.label-data{
	font-weight: normal!important;
	padding: 3px;
} 
</style><script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
$(function(){ 
	$('.formError').remove();
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	personId=Number($('#personId').val());	
	$('#deductionType').val(Number($('#deductionTypeTemp').val()));	

	$('#calcultionMethod').val(Number($('#calcultionMethodTemp').val()));	


$('#discard').click(function(){
	homeredirect();
	return false;
});


//Save & discard process
$('.save').click(function(){ 
	$('#page-error').hide();
	if($("#ATTENDANCEEDIT").validationEngine({returnIsValid:true})){  
		loanLineDetail=""; loanChargesDetail="";
		var attendanceId=Number($('#attendanceId').val());
		var reason=$('#reason').val();
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/attendance_save_employee.action", 
		 	async: false, 
		 	data:{ 
		 		attendanceId:attendanceId,reason:reason
		 	},
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 homeredirect();
				 }else{
					 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				 }
			},
			error:function(result){  
				$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
			}
		});  
	}
	else{
		return false;
	}
	return false;
});
});
function homeredirect(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/attendance_excution_listing.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	return false;
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="hr.attendance.attendancedeviation"/></div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
		<div style="display:none;" class="tempresult">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<div>
			 <div class="error response-msg ui-corner-all" style="display:none;">Sorry, Process Failure!</div>
		  	  <div class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div>
		  <form id="ATTENDANCEEDIT" class="" name="ATTENDANCEEDIT" method="post">
		  		<div class="width100 float-left view-content" id="hrm"> 
		  			<input type="hidden" name="attendanceId" id="attendanceId" value="${ATTENDANCE_PROCESS.attendanceId}" class="width50"/>
		  			<input type="hidden" name="personId" id="personId" value="${ATTENDANCE_PROCESS.person.personId}" class="width50"/>
		  			
		  			<div class="width100 float-left " >
						<fieldset style="min-height:80px;" >
							<legend>Employee Information</legend>
							<div class="width45 float-left">
								<c:set var="jobAssignment" value="${ATTENDANCE_PROCESS.jobAssignment}"/>
								<div class="width100 float-left">
									<label class="width30" >Employee : </label>
									<label class="width50 label-data" >${ATTENDANCE_PROCESS.person.firstName} ${ATTENDANCE_PROCESS.person.lastName}</label>
								</div>  
								<div class="width100 float-left">
									<label class="width30" >Job : </label>
									<label class="width50 label-data" >${jobAssignment.designation.designationName}</label>
								</div> 
								<div class="width100 float-left">
									<label class="width30" >Grade : </label>
									<label class="width50 label-data" >${jobAssignment.designation.grade.gradeName}</label>
								</div> 
							</div>
							<div class="width45 float-left">
								<div class="width100 float-left">
									<label class="width30" >Company : </label>
									<label class="width50 label-data" >${jobAssignment.cmpDeptLocation.company.companyName}</label>
								</div>
								<div class="width100 float-left">
									<label class="width30" >Department : </label>
									<label class="width50 label-data" >${jobAssignment.cmpDeptLocation.department.departmentName}</label>
								</div>
								<div class="width100 float-left">
									<label class="width30" >Location : </label>
									<label class="width50 label-data" >${jobAssignment.cmpDeptLocation.location.locationName}</label>
								</div>
							</div>
						</fieldset>
				 	</div> 
				<div class="width100 float-left">
						<fieldset style="min-height:190px;">
							<legend><fmt:message key="hr.attendance.punchdetail"/></legend>
							<div class="width30 float-left">
								<div class="width100 float-left">
									<label class="width50 " ><fmt:message key="hr.attendance.attendanceday"/> : </label>
									<c:set var="attendanceDate" value="${ATTENDANCE_PROCESS.attendanceDate}"/>  
										<%String attendanceDate = DateFormat.convertDateToString(pageContext.getAttribute("attendanceDate").toString());%>
									<label class="width30 label-data " ><%=attendanceDate%></label>
								</div>
								<div class="width100 float-left">
									<label class="width50 " ><fmt:message key="hr.attendance.timein"/> : </label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.timeIn}</label>
								</div>  
								<div class="width100 float-left">
									<label class="width50 " ><fmt:message key="hr.attendance.timout"/> : </label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.timeOut}</label>
								</div> 
								<div class="width100 float-left">
									<label class="width50 float-left" ><fmt:message key="hr.attendance.latein"/>:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.lateIn} </label>
								</div> 
								<div class="width100 float-left">
									<label class="width50 " ><fmt:message key="hr.attendance.lateout"/>:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.lateOut} </label>
								</div>
								<div class="width100 float-left">
									<label class="width50 " ><fmt:message key="hr.attendance.earlyin"/>:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.earlyIn} </label>
								</div> 
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.earlyout"/>:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.earlyOut} </label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.timeloss"/>:</label>
									<label class="width30 label-data "><span style="background-color: red;color: white;">${ATTENDANCE_PROCESS.timeLoss}</span></label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.timeexcess"/>:</label>
									<label class="width30 label-data " ><span style="background-color: green;color: white;">${ATTENDANCE_PROCESS.timeExcess}</span></label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.totalhours"/>:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.totalHours} </label>
								</div>
							</div>
							<legend><fmt:message key="hr.attendance.shiftinfo"/></legend>
							<div class="width30 float-left">
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.workingshift"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.workingShiftName}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.starttime"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.startTime}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.endtime"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.endTime}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.latetime"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.lateAttendance}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.seviorlatetime"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.seviorLateAttendance}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.buffermins"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.bufferTime}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.breakhours"/> : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.breakHours}</label>
								</div>
							</div>
							<%-- <legend><fmt:message key="hr.attendance.policydetail"/></legend>
							<div class="width30 float-left">
								
								<div class="width100 float-left">
									<c:choose>
										<c:when test="${ATTENDANCE_POLICY.allowOt ne null && ATTENDANCE_POLICY.allowOt eq true}">
											<label class="width50" ><fmt:message key="hr.attendance.overtimehalfday"/> :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.otHoursForHalfday} -(hrs in Minimum)</label>
										</c:when>
										<c:otherwise>
											<label class="width50" ><fmt:message key="hr.attendance.compoffhalfday"/> :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.compoffHoursForHalfday} -(hrs in Minimum)</label>
										</c:otherwise>
									</c:choose>	
								</div>
								<div class="width100 float-left">
									<c:choose>
										<c:when test="${ATTENDANCE_POLICY.allowOt ne null && ATTENDANCE_POLICY.allowOt eq true}">
											<label class="width50" ><fmt:message key="hr.attendance.overtimeoneday"/> :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.otHoursForFullday} -(hrs in Minimum)</label>
										</c:when>
										<c:otherwise>
											<label class="width50" ><fmt:message key="hr.attendance.compoffoneday"/>:</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.compoffHoursForFullday} -(hrs in Minimum)</label>
										</c:otherwise>
									</c:choose>								
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.lophoursoneday"/> :</label>
									<label class="width30 label-data" >${ATTENDANCE_POLICY.lopHoursForFullday} -(hrs in Minimum)</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" ><fmt:message key="hr.attendance.lophourshalfday"/> :</label>
									<label class="width30 label-data" >${ATTENDANCE_POLICY.lopHoursForHalfday} -(hrs in Minimum)</label>
								</div>
								
							</div>
							 --%>
						</fieldset>
				 	</div> 
					<div class="width100 float-left">
						<fieldset style="min-height:150px;">
						<legend>Deviation Information</legend>
							<div class="width45 float-left">
								<div>
									<label class="width30" for="deductionType"><fmt:message key="hr.attendance.processtype"/></label>
									<input type="hidden" name="deductionTypeTemp" id="deductionTypeTemp" value="${ATTENDANCE_PROCESS.deductionType}">
									<select id="deductionType" name="deductionType" disabled="disabled" class="width51 validate[required]">
										<option value="">--Select--</option>
										<option value="1">NoAction</option>
										<option value="2">LOP</option>
										<option value="3">OverTime</option>
										<option value="4">CompOff</option>
										<option value="5">COD-CompOff Deduction</option>
									</select>
								</div>   
								<div>
									<label class="width30">Calculation Method</label>
									<div>
										<select id=calcultionMethod disabled="disabled"
											name="calcultionMethod"
											class="width51 ">
											<option value="">--Select--</option>
											<c:forEach items="${CALCULATION_METHOD}" var="gen">
												<option value="${gen.key}">${gen.value}
												</option>
											</c:forEach>
										</select>  <input
											type="hidden" name="calcultionMethodTemp" id="calcultionMethodTemp"
											value="${ATTENDANCE_PROCESS.calcultionMethod}" />
									</div>
								</div> 
								<c:choose>
									<c:when test="${ATTENDANCE_PROCESS.deviationHours ne null}">
										<label class="float-left width30" for="deviationHours">Deviation Hour's</label>
										<input type="text" name="deviationHours" id="deviationHours" disabled="disabled" value="${ATTENDANCE_PROCESS.deviationHours}" class="width50" >
									</c:when>
								</c:choose>
								<div>
									<label class="float-left width30" for="leaveType">Half a day</label>
									<c:choose>
										<c:when test="${ATTENDANCE_PROCESS.halfDay eq true}">
											<input type="checkbox" name="halfDay" id="halfDay" disabled="disabled" checked="checked" class="float-left width5" >
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="halfDay" id="halfDay" disabled="disabled" class="float-left width5" >
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="width45 float-left">
								<div class="width100 float-left">
									<label class="width30" for="systemSuggesion"><fmt:message key="hr.attendance.systemsuggesion"/> :</label>
									<label class="width60 label-data">${ATTENDANCE_PROCESS.systemSuggesion}</label>
								</div>
								<div class="width100 float-left">
									<label class="width30" for="reason"><fmt:message key="hr.attendance.reason"/></label>
									<label class="width60 label-data">${ATTENDANCE_PROCESS.reason}</label>
								</div>
								<div class="width100 float-left">
									<label class="width30" for="comment">Comment</label>
									<label class="width60 label-data">${ATTENDANCE_PROCESS.comment}</label>
								</div>
							</div>
							<c:if test="${COMMENT_IFNO ne null}">
								<div class="width100 float-left">
									<div class="width45 float-left">	
										<div>
											<label class="width30" style="color: red;"><fmt:message key="hr.attendance.comment"/>:</label>
											<label class="width50">${COMMENT_IFNO.comment}</label>
										</div>
										<div>
											<label class="width30" ><fmt:message key="hr.attendance.commentedby"/> :</label>
											<label class="width50">${COMMENT_IFNO.person.firstName} ${COMMENT_IFNO.person.lastName}</label>
										</div>
										
									</div>
								</div>
							</c:if>
							<div class="clearfix"></div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.close"/></div>
							</div>
						</fieldset>
				 </div> 
 			</div>
 		</form>
	
	</div>
</div>
</div>
