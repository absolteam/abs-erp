<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script src="fileupload/FileUploader.js"></script>
<style type="text/css">

</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
var accessCode=null;
$(function (){  
	
	var assetUsageId=Number($('#assetUsageId').val());
	
	$('#assetcustody_document_information').click(function(){
		if(assetUsageId>0){
			AIOS_Uploader.openFileUploader("doc","assetCustodyDocs","AssetUsage",assetUsageId,"AssetCustodyDocuments");
		}else{
			AIOS_Uploader.openFileUploader("doc","assetCustodyDocs","AssetUsage","-1","AssetCustodyDocuments");
		}
	});
	
	if(assetUsageId>0){
		$('#assetStatus').val($('#assetStatusTemp').val());
		$('#assetType').val($('#assetTypeTemp').val());
		$('#assetColor').val($('#assetColorTemp').val());
		$('#returnDateDiv').show();
		populateUploadsPane("doc","assetCustodyDocs","AssetUsage",assetUsageId);	
	}
	
	$("#allocationDate,#returnedDate,#purchaseDate").datepick();

	$jquery("#ASSET_USAGE").validationEngine('attach');
	

	//pop-up config
    $('.employee-popup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         currentObjectIdentiy=$(this).attr("id");
         var rowId=-1; 
         var personTypes="ALL";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 $('#companyCall').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         currentObjectIdentiy=$(this).attr("id");
         $('#common-popup').dialog('open');
         var rowId=-1; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
               data:{id:rowId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
	 
	 $('#productCall').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         currentObjectIdentiy=$(this).attr("id");
         $('#common-popup').dialog('open');
         var rowId=-1; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/product_list_assetusage.action",
               data:{id:rowId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
	 
	 $('.asset-lookup').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
         accessCode=$(this).attr("id"); 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
   
  
   $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:950,
		height:600,
		bgiframe: false,
		modal: true 
	});
	 
   $('#save').click(function() { 
		$('.success,.error').hide();
		
		if($jquery("#ASSET_USAGE").validationEngine('validate')){ 
			var assetUsageId=Number($('#assetUsageId').val());
			var locationId=Number($('#locationId').val());
			var personId=Number($('#personId').val());
			var productId=Number($('#productId').val());
			var assetId=Number($('#assetId').val());
			var assetName=$('#assetName').val();
			var assignedById=Number($('#assignedById').val());
			var assignedDate=$('#allocationDate').val();
			var returnDate=$('#returnedDate').val();
			var description=$('#description').val();
			var status=Number($('#assetStatus').val());
			var serialNumber=$('#serialNumber').val();
			var assetType=Number($('#assetType').val());
			var assetColor=Number($('#assetColor').val());
			var brand=$('#brand').val();
			var assetModel=$('#assetModel').val();
			var manufacturer=$('#manufacturer').val();
			var assetModel=$('#assetModel').val();
			var usefulLife=Number($('#usefulLife').val());
			var purchaseDate=$('#purchaseDate').val();
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(assetUsageId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/asset_usage_save.action",
				data: {
					assetName:assetName,
					assetUsageId:assetUsageId,
					assetId:assetId,
					locationId:locationId,
					personId:personId,
					productId:productId,
					assignedById:assignedById,
					assignedDate:assignedDate,
					returnDate:returnDate,
					description:description,
					status:status,
					serialNumber:serialNumber,
					assetType:assetType,
					assetColor:assetColor,
					brand:brand,
					assetModel:assetModel,
					manufacturer:manufacturer,
					usefulLife:usefulLife,
					purchaseDate:purchaseDate
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOAssetList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		return false;
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		 returnCallTOAssetList();
		$('#loading').fadeOut();
		return false;
	});
   
 //Lookup Data Roload call
	$('#save-lookup').live('click',function(){ 
		
		if(accessCode=="ASSET_TYPE"){
			$('#assetType').html("");
			$('#assetType').append("<option value=''>--Select--</option>");
			loadLookupList("assetType");
			
		}else if(accessCode=="INTERVIEW_MODE"){
			$('#interviewMode').html("");
			$('#interviewMode').append("<option value=''>--Select--</option>");
			loadLookupList("interviewMode");
		}
		
	});
});
   function returnCallTOAssetList(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/asset_usage_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#codecombination-popup').dialog('destroy');
				$('#codecombination-popup').remove();	
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();	
				$("#main-wrapper").html(result); 
			}
	 	});
		return false;
	}
function personPopupResult(personId,personName){
	
	if(currentObjectIdentiy.trim()=="employeeCall"){
		$('#personName').val(personName);
		$('#personId').val(personId);
	}else{
		$('#assignedByName').val(personName);
		$('#assignedById').val(personId);
	}
}

function departmentBranchPopupResult(locationId,locationName){
	$('#locationName').val(locationName);
	$('#locationId').val(locationId);
}

function productPopupResult(productId,productName){
	$('#productName').val(productName);
	$('#productId').val(productId);
}

function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Asset Custody Form</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<c:choose>
				<c:when
					test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From :<span>
									${COMMENT_IFNO.name}</span>
							</label> <label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div>
				</c:when>
			</c:choose>
			<form id="ASSET_USAGE" name="ASSET_USAGE" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" name="assetUsageId" id="assetUsageId" value="${ASSET_USAGE_INFO.assetUsageId}"/>
					<fieldset>
						
			      	 <div>
	                   	<label class="width30">Person / Employee<span style="color:red">*</span></label>
	                   	<c:choose>
	                    	<c:when test="${ASSET_USAGE_INFO.personByPersonId ne null && ASSET_USAGE_INFO.personByPersonId ne '' }">
	                     		<input type="text" class="masterTooltip width50 validate[required]" title="" readonly="readonly" name="personName" id="personName" 
	                     			value="${ASSET_USAGE_INFO.personByPersonId.firstName} ${ASSET_USAGE_INFO.personByPersonId.lastName}"/>
	                    	</c:when>
	                    	<c:otherwise>
	                    		<input type="text" class="masterTooltip width50 validate[required]" title="" readonly="readonly" name="personName" id="personName" value=""/>
	                    	</c:otherwise>
		                </c:choose>
		                 <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="employeeCall" class="btn ui-state-default ui-corner-all width100 employee-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
						<input type="hidden" name="personId" id="personId" value="${ASSET_USAGE_INFO.personByPersonId.personId}"/> 
			      	  </div>
				         
			       	 <div>
	                   	<label class="width30">Asset<span style="color:red">*</span></label>
                   		<input type="text" class="masterTooltip width50 validate[required]" title="${ASSET_USAGE_INFO.assetName}" name="productName" id="productName" 
                   			value="${ASSET_USAGE_INFO.assetName}"/>
		                 <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="productCall" class="btn ui-state-default ui-corner-all width100 product-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
						<input type="hidden" name="productId" id="productId" value="${ASSET_USAGE_INFO.product.productId}"/> 
			        </div>
			       <div>
		                 <label class="width30">Number<span style="color:red">*</span></label>
		                 <input type="text" name="serialNumber" class="width50 validate[required]" id="serialNumber" value="${ASSET_USAGE_INFO.serialNumber}">
		            </div>
		            <div>
						<label class="width30">Asset Type <span style="color:red">*</span></label>
						<div>
							<select id="assetType"
								name="assetType"
								class="width50 validate[required]">
								<option value="">--Select--</option>
								<c:forEach items="${ASSET_TYPES}" var="nltlst">
									<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
									</option>
								</c:forEach>
							</select>  
							<span class="button" id="ASSET_TYPE_1"
							style="position: relative; "> <a
							style="cursor: pointer;" id="ASSET_TYPE"
							class="btn ui-state-default ui-corner-all asset-lookup width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
							type="hidden" name="assetTypeTemp" id="assetTypeTemp"
							value="${ASSET_USAGE_INFO.lookupDetailByAssetType.lookupDetailId}" />
						</div>
					</div>
		            <div>
	                   	<label class="width30">Allocated By<span style="color:red">*</span></label>
	                   	<c:choose>
	                    	<c:when test="${ASSET_USAGE_INFO.personByCreatedBy ne null && ASSET_USAGE_INFO.personByCreatedBy ne '' }">
	                     		<input type="text" class="masterTooltip width50 validate[required]" title="" readonly="readonly" name="personName" id="personName" 
	                     			value="${ASSET_USAGE_INFO.personByCreatedBy.firstName} ${ASSET_USAGE_INFO.personByCreatedBy.lastName}"/>
	                    	</c:when>
	                    	<c:otherwise>
	                    		<input type="text" class="masterTooltip width50 validate[required]" title="" readonly="readonly" name="assignedByName" id="assignedByName" value=""/>
	                    	</c:otherwise>
		                </c:choose>
		                 <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="assignedByCall" class="btn ui-state-default ui-corner-all width100 employee-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
						<input type="hidden" name="assignedById" id="assignedById" value="${ASSET_USAGE_INFO.personByCreatedBy.personId}"/> 
			       </div>
					<div>
		                 <label class="width30">Allocation Date<span style="color:red">*</span></label>
		                 <input type="text" name="allocationDate" class="width50 validate[required]" id="allocationDate" value="${ASSET_USAGE_INFO.allocationDate}">
		            </div>
		             <div>
		                 <label class="width30">Brand</label>
		                 <input type="text" name="brand" class="width50" id="brand" value="${ASSET_USAGE_INFO.brand}">
		            </div>
		             <div>
		                 <label class="width30">Model</label>
		                 <input type="text" name="assetModel" class="width50" id="assetModel" value="${ASSET_USAGE_INFO.assetModel}">
		            </div>
		             <div>
		                 <label class="width30">Make</label>
		                 <input type="text" name="manufacturer" class="width50" id="manufacturer" value="${ASSET_USAGE_INFO.manufacturer}">
		            </div>
		            <div>
		                 <label class="width30">Useful Life</label>
		                 <input type="text" name="usefulLife" class="width50" id="usefulLife" value="${ASSET_USAGE_INFO.usefulLife}">
		            </div>
			       <div>
	                   	<label class="width30">Department / Branch</label>
	                   	<c:choose>
	                    	<c:when test="${ASSET_USAGE_INFO.cmpDeptLocation ne null && ASSET_USAGE_INFO.cmpDeptLocation ne '' 
	                    			&& ASSET_USAGE_INFO.cmpDeptLocation.department ne null}">
	                     		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="locationName" id="locationName" 
	                     			value="${ASSET_USAGE_INFO.cmpDeptLocation.company.companyName}||
	                     			${ASSET_USAGE_INFO.cmpDeptLocation.department.departmentName}||
	                     			${ASSET_USAGE_INFO.cmpDeptLocation.location.locationName}"/>
	                    	</c:when>
	                    	<c:when test="${ASSET_USAGE_INFO.cmpDeptLocation ne null && ASSET_USAGE_INFO.cmpDeptLocation ne '' 
	                    			&& ASSET_USAGE_INFO.cmpDeptLocation.department eq null}">
	                     		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="locationName" id="locationName" 
	                     			value="${ASSET_USAGE_INFO.cmpDeptLocation.company.companyName}||
	                     			${ASSET_USAGE_INFO.cmpDeptLocation.location.locationName}"/>
	                    	</c:when>
	                    	<c:otherwise>
	                    		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="locationName" id="locationName" value=""/>
	                    	</c:otherwise>
		                </c:choose>
		                 <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="companyCall" class="btn ui-state-default ui-corner-all width100 company-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
						<input type="hidden" name="locationId" id="locationId" value="${ASSET_USAGE_INFO.cmpDeptLocation.cmpDeptLocId}"/> 
			       </div>
				 </fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset>
					<div>
		                 <label class="width30">Purchase Date</label>
		                 <input type="text" name="purchaseDate" class="width50" id="purchaseDate" value="${ASSET_USAGE_INFO.purchaseDateView}">
		            </div>
		            <div>
		                 <label class="width30">Asset Status</label>
		                 <input type="hidden" id="assetStatusTemp" value="${ASSET_USAGE_INFO.assetStatus}">
		                 <select name="assetStatus" class="width51" id="assetStatus">
		                 	<option value="1" selected="selected">Assigned</option>
		                 	<option value="2">Returned</option>
		                 	<option value="3">Damaged</option>
		                 	<option value="4">Lost</option>
		                 </select>
		            </div>
		            <div>
						<label class="width30">Asset Color</label>
						<div>
							<select id="assetColor"
								name="assetColor"
								class="width51">
								<option value="">--Select--</option>
								<c:forEach items="${ASSET_COLORS}" var="nltlst">
									<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
									</option>
								</c:forEach>
							</select>  
							<span class="button" id="ASSET_COLOR_1"
							style="position: relative; "> <a
							style="cursor: pointer;" id="ASSET_COLOR"
							class="btn ui-state-default ui-corner-all asset-lookup width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
							type="hidden" name="assetColorTemp" id="assetColorTemp"
							value="${ASSET_USAGE_INFO.lookupDetailByAssetColor.lookupDetailId}" />
						</div>
					</div>
					<div id="returnDateDiv" style="display: none;">
		                 <label class="width30">Returned Date</label>
		                 <input type="text" name="returnedDate" class="width50" id="returnedDate" value="${ASSET_USAGE_INFO.returnDate}">
		            </div>
		            <div>
		                 <label class="width30">Description</label>
		                 <textarea name="description" class="width51" id="description">${ASSET_USAGE_INFO.description}</textarea>
		            </div>
		            <div class="width90 float-left" style="min-height: 130px;border: 1px solid;">									
						<div id="assetcustody_document_information" class="width90" style="cursor: pointer; color: blue;">
							<u>Upload Documents Here</u>
						</div>
						<div style="padding-top: 10px; margin-top:3px;" class="width90">
							<span id="assetCustodyDocs"></span>
						</div>
					</div>		
				</fieldset>
			</div>
			<div class="clearfix"></div>  
			<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
				<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
			</div>
		</form>
		
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>