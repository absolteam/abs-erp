<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style>
.ui-multiselect .count{
	padding:0!important;
}
</style> 

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 
	
	$jquery("#consultancy-details").validationEngine('attach');


		var advertisingPanelId=Number($("#advertisingPanelId").val());
		if(advertisingPanelId>0){
			autoSelectTheMultiList("taskTemp","task");
			
			
		}else{
			
		}
		
		
		
		$('#RECRUITMENT_RESOURCES').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/common_recruitment_source_list.action",
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){

	                   $('.common-result').html(result);
	                   return false;
	              },
	              error:function(result){

	                   $('.common-result').html(result);
	              }
	          });
	           return false;
	  	}); 
		
	   
	   $('#OPEN_POSITION').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/common_open_position_list.action",
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	  
		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:450,
			bgiframe: false,
			modal: true 
		});
		
		
			
		$('#consultancy-discard').click(function(){ 
			returnCallTOList();
			
		 });

	 $('#consultancy-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
		 	
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(recruitmentSourceId>0)
						successMessage="Successfully Modified";

						if ($jquery("#consultancy-details").validationEngine('validate')) {
							//generateDataToSendToDB("responsibilityList","responsibility");
							
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/advertising_panel_save.action", 
							 	async: false,
							 	data: $("#consultancy-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					

				});
	 
	 
		 $('#postedDate').datepick();
		
		 
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/advertising_panel_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}
	
	function autoSelectTheMultiList(source,target) {
		 var idsArray=new Array();
		 var idlist=$('#'+source).val();
		 idsArray=idlist.split(',');
		$('#'+target+' option').each(function(){
			var txt=$(this).val().trim();
			if($.inArray(txt, idsArray)>=0)
				$(this).attr("selected","selected");
		});

	}

	function selectedOpenPositionResult(openPositionVO){
		$("#openPositionId").val(openPositionVO.openPositionId);
		$("#positionReferenceNumber").val(openPositionVO.positionReferenceNumber);
		$("#numberOfPosition").val(openPositionVO.numberOfPosition);
		$("#job").val(openPositionVO.designationName);
		
	}
	
	function selectedRecruitmentSourceResult(recritmentSourceVO){
		$("#recruitmentSourceId").val(recritmentSourceVO.recruitmentSourceId);
		$("#providerName").val(recritmentSourceVO.providerName);
		$("#sourceType").val(recritmentSourceVO.sourceType);
	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Advertising Panel</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="consultancy-details" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden"
									name="advertisingPanel.advertisingPanelId"
									id="advertisingPanelId"
									value="${ADVERTISING_PANEL.advertisingPanelId}"
									class="width50" />
								<fieldset style="min-height: 200px;">
									<legend>Advertising Information</legend>
									<div id="hrm" class="width50 float-left">
										<div>
											<label class="width30">Reference Number<span
												style="color: red">*</span> </label> <input type="text"
												id=referenceNumber class="width50 validate[required]"
												readonly="readonly" name="advertisingPanel.referenceNumber"
												value="${ADVERTISING_PANEL.referenceNumber}" />
										</div>
										<div>
											<label class="width30">Recruitment Source<span
												style="color: red">*</span> </label> <input type="hidden"
												name="advertisingPanel.recruitmentSource.recruitmentSourceId"
												id="recruitmentSourceId"
												value="${ADVERTISING_PANEL.recruitmentSource.recruitmentSourceId}" />
											<div>
												<input type="text" id="providerName" readonly="readonly"
												class="width50 validate[required]" value="${ADVERTISING_PANEL.recruitmentSource.providerName}"/>
												<span class="button" id="RECRUITMENT_RESOURCES_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="RECRUITMENT_RESOURCES"
													class="btn ui-state-default ui-corner-all width10">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</div>
											
										</div>
										<div>
											<label class="width30">Source Type<span
												style="color: red">*</span> </label>
											<div>
												<input type="text" id="sourceType" readonly="readonly"
													class="width50 validate[required]"
													value="${ADVERTISING_PANEL.recruitmentSource.lookupDetail.displayName}" />
											</div>

										</div>
										<div>
											<label class="width30">Open Position<span
												style="color: red">*</span> </label> <input type="hidden"
												name="advertisingPanel.openPosition.openPositionId"
												id="openPositionId"
												value="${ADVERTISING_PANEL.openPosition.openPositionId}" />
											<div>
												<input type="text" id="positionReferenceNumber" readonly="readonly"
												class="width50 validate[required]"  value="${ADVERTISING_PANEL.openPosition.positionReferenceNumber}"/>
												<span class="button" id="OPEN_POSITION_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="OPEN_POSITION"
													class="btn ui-state-default ui-corner-all width10">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</div>
											<div>
												<label class="width30">No.of Position<span
													style="color: red">*</span> </label>
												<div>
													<input type="text" id="numberOfPosition"
														readonly="readonly" class="width50 validate[required]"
														value="${ADVERTISING_PANEL.openPosition.numberOfPosition}" />
												</div>

											</div>
											<div>
												<label class="width30">Job<span style="color: red">*</span>
												</label>
												<div>
													<input type="text" id="job" readonly="readonly"
														class="width50 validate[required]"
														value="${ADVERTISING_PANEL.openPosition.designation.designationName}" />
												</div>

											</div>
										</div>
										
									</div>
									<div id="hrm" class="width40 float-left">
										<div>
											<label class="width30">Description </label> <textarea 
												id="description" class="width61"
												name="advertisingPanel.description"
												>${ADVERTISING_PANEL.description}</textarea>
										</div>
										<div>
											<label class="width30">Status<span style="color: red">*</span>
											</label>
												<select id="task" name="advertisingPanel.task"
													class="width61 validate[required]">
													<c:forEach items="${STATUS_TYPE}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="taskTemp" id="taskTemp"
													value="${ADVERTISING_PANEL.task}" />
										</div>
										<div>
											<label class="width30">Cost Reference</label> <input
												type="text" class="width60" 
												name="advertisingPanel.costReference" id="costReference"
												value="${ADVERTISING_PANEL.costReference}" />
										</div>
										<div>
											<label class="width30">Posted Date</label> <input
												type="text" class="width60" readonly="readonly"
												name="postedDate" id="postedDate"
												value="${ADVERTISING_PANEL.postedDateDisplay}" />
										</div>
									</div>
								</fieldset>

									
							</div>

							
						</div>

					</form>
					<div class="clearfix"></div>
					
					
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

		
			</div>
		</div>
	</div>
	
</body>
</html>