<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td{
	width:6%;
}
.width35{
	width:35%!important;
}
.width25{
	width:25%!important;
}
.width15{
	width:15%!important;
}
.margintop3{
	margin-top:3px;
}
#common-popup{
	overflow: hidden;
}
.width61{
	width:61% !important;
}
.ui-autocomplete-input {
	width: 52%!important;
}
</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var allowanceLineDetail="";
var leaveLineDetail="";
var shiftLineDetail="";
var assignmentLineDetail="";

$(document).ready(function(){  
	$('.parentrDepatment').combobox({ 
		 selected: function(event, ui){ 
			 
		 }
	 });
	
	$jquery("#JOBDetails").validationEngine('attach');
	//Save & discard process
	$('.save').click(function(){ 
		if($jquery("#JOBDetails").validationEngine('validate')){
			var departmentId=Number($('#departmentId').val());
			var departmentName=$('#department').val();
			var parentDepartmentId=Number($('#parentDepartment').val());
			var description=$('#description').val();
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_department.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		departmentId:departmentId,
			 		departmentName:departmentName,
			 		parentDepartmentId:parentDepartmentId,
			 		description:description,
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('#returnMessage').val(); 
					 if(message.trim()=="SUCCESS"){
						 $('.discard').trigger('click');
						 $('#success_msg').hide().html("Department information successfully updated").slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){
					$(".tempresult").html(result);
					 var message=$('#returnMessage').val(); 
					 if(message.trim()!=""){
						$('#page-error').hide().html(message).slideDown(1000);
						return false;
					 }else{
						 $('#page-error').hide().html("System Error! Contact  administrator.").slideDown(1000);
						 return false;
					 }
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/company_setup.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
		 return false;
	});
	
});

</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Modify Department Information</div>
			 
			<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  		<div class="tempresult" style="display:none;"></div>
			  		<c:choose>
						<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
											&& COMMENT_IFNO.commentId gt 0}">
							<div class="width85 comment-style" id="hrm">
								<fieldset>
									<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
									<label class="width70">${COMMENT_IFNO.comment}</label>
								</fieldset>
							</div> 
						</c:when>
					</c:choose> 
					 <form id="JOBDetails" class="" name="JOBDetails" method="post" style="position: relative;">
						 <div class="width100 float-left" id="hrm"> 
							 <fieldset style="height:150px;">
							 	<legend>Department Information</legend>
						  	<div class="width45 float-left" id="hrm"> 
					  			<div class="width100 float-left">
									<input type="hidden" name="departmentId" id="departmentId" class="departmentId" value="${DEPARTMENT.departmentId}"/>
									<label class="width30" for="company">Company<span class="mandatory">*</span></label>
							 		<input type="text" name="department" id="department" class="department  validate[required] width60" value="${DEPARTMENT.departmentName}"/> 
							 	</div> 
							 	<div class="width100 float-left">
							 		<label class="width30" for="company">Department</label>
									<select class="width60 parentrDepatment" id="parentDepartment" style="border:0px;">
										<option value="0">Select</option>
										<c:forEach var="parentrDepatment" items="${DEPARTMENT_LIST}" varStatus="statusL">
											<c:choose>
												<c:when test="${parentrDepatment.departmentId eq DEPARTMENT.parentDepartmentId}">
													<option value="${parentrDepatment.departmentId}" selected="selected">${parentrDepatment.departmentName}</option>
												</c:when>
												<c:otherwise>
													<option value="${parentrDepatment.departmentId}">${parentrDepatment.departmentName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
							 	</div>
								 
				 			</div>
				 			<div class="width45 float-left" id="hrm"> 
								<div class="width100 float-left">
							 		<label class="width30" for="company">Description</label>
									<textarea id="description" class="description width60">${DEPARTMENT.description}</textarea>
							 	</div>
				 			</div>
					 		<div class="clearfix"></div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right discard" id="discard">Cancel</div>
								<div class="portlet-header ui-widget-header float-right save" id="job_save">Save</div>
							</div>
							</fieldset>
						</div>
			 		</form>
				</div>
			</div>
	</div>
	

