<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<tr class="rowidL" id="fieldrowL_${rowId}">
	<td id="lineIdL_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 <input type="text" class="width80 leavePolicyName" id="leavePolicyName_${i}" value="" style="border:0px;"/>
	</td> 
	<td>
		<input type="checkbox" id="isWeekendExcluded_${rowId}" class="width10 isWeekendExcluded" />
		<input type="hidden" id="isWeekendExcludedTemp_${rowId}" class="width10 isWeekendExcludedTemp" />
	</td>
	<td>
		<input type="checkbox" id="isHolidayExcluded_${rowId}" class="width10 isHolidayExcluded" />
		<input type="hidden" id="isHolidayExcludedTemp_${rowId}" class="width10 isHolidayExcludedTemp" />
	</td>
	<td>
		<input type="checkbox" id="isDefalut_${rowId}" class="width10 isDefalut" />
		<input type="hidden" id="isDefalutTemp_${rowId}" class="width10 isDefalutTemp" />
	</td>
	<td style="display:none;">
		<input type="hidden" id="leavePolicyId_${i}" value="" style="border:0px;"/>
	</td>
	 <td style="width:0.01%;" class="opn_td" id="optionL_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataL" id="AddImageL_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataL" id="EditImageL_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowL" id="DeleteImageL_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageL_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>    
</tr>