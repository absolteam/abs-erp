<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta charset="UTF-8">
<style type="text/css">

body {
	padding: 0px;
	margin: 0px;
}

* {
	font-family: sans-serif;
	font-weight:normal;
	font-size: 12px;
}

tr {
	vertical-align: top;
}

.bold {
	font-weight: bold;
}

table {
    border-collapse: collapse;
}

.border {
	border: 1px solid;
}

td {
	padding: 4px;
}

.htitle {
	font-size: 14px;
	font-weight: bold;
}
</style>

</head>
<body>
<table width="100%" style="margin-top: 10px; border: 0px solid black;">
	<tbody>
		
		<tr>
			<td colspan="4" height="150"></td>
		</tr>
		<tr>
			<td colspan="3" height="20" align="center" class="bold" style="font-size: 18px;font-style: italic;text-decoration: underline;">ADMINISTRATION ORDER</td>
		</tr>
		<tr>
			<td colspan="3" width="40%"  align="center" style="border-bottom: 1px solid"></td>
		</tr>
		
		<tr>
			<td colspan="3" height="0">
				<table width="90%"  align="center" style="margin-top: 10px;">
					<tr>
						<td width="22%" class="bold" align="left">ADMIN ORDER DATE</td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${requestedDate}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">ADMIN ORDER NO </td>
						<td width="0.3%"> : </td>
						<td width="70%" class="bold" align="left">${orderNumber}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">SUBJECT </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${actionType} APPROVED</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">CARD NO    </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${employeeID}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">EMPLOYEE NAME  </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${employee}</td>
					</tr>

					<tr>
						<td width="22%" class="bold" align="left">DATE OF REAPPT  </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${requestedDate}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">DEPT/PROJECT </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${department}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">DESIGNATION </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${designation}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">EFFECTIVE DATE </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${fromDate}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">GROSS SALARY </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left">${grossSalary}</td>
					</tr>
					<tr>
						<td width="22%" class="bold" align="left">DETAILS </td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left"> Your ${actionType} is approved and it effective from <span class="bold" style="font-size: 14px;text-decoration: underline;">${fromDate}</span>
							Details As Follows :  <br>
							${salaryDetails}
						</td>
			
					</tr>
						
					<tr>
						<td width="22%" class="bold" align="left">REMARKS</td>
						<td width="1%"> : </td>
						<td width="70%" class="bold" align="left"> ${remarks}</td>
					</tr>
						
					
				</table/>
			</td>
		</tr>
		<tr>
			<td colspan="3" width="40%"  align="center" style="border-bottom: 1px solid"></td>
		</tr>
		
		<tr>
			<td colspan="3" height="100"  align="center">
				
			</td>
		</tr>
		<tr>
			<td colspan="3"  align="center" style="border-bottom: 1px solid;font-size:18px;font-weight:bold;">
								
							General Manager	<br/>
							المدير العام<br/>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" width="0%"  align="left">SIGNED RECEIPT AND ACCEPTED</td>
		</tr>
		<tr>
			<td colspan="3" width="20%"  align="left">BY THE EMPLOYEE</td>
		</tr>
		<tr>
			<td colspan="3" width="40%"  align="center" style="border-bottom: 1px solid"></td>
		</tr>
		<tr>
			<td colspan="1" width="5%"  align="left">Copy</td>
			<td colspan="3" width="10%"  align="left">EMPLOYEE</td>
		</tr>
		<tr>
			<td colspan="1" width="5%"  align="left">ORGINAL</td>
			<td colspan="3" width="10%"  align="left">PERSONAL FILE</td>
		</tr>
		<tr>
			<td colspan="1" width="5%"  align="left">Copy</td>
			<td colspan="3" width="10%"  align="left">FILE HR</td>
		</tr>
		<tr>
			<td colspan="1" width="5%"  align="left">Copy</td>
			<td colspan="3" width="10%"  align="left">ACCOUNTS</td>
		</tr>
	</tbody>
</table>
</body>
</html>