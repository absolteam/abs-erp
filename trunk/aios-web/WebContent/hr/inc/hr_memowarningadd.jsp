<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.cleditor.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/css/jquery.cleditor.css" />
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style type="text/css">

</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
var accessCode="";
$(function (){  
	

	$jquery("#MEMO_WARNING").validationEngine('attach');
	

	 $('#type').change(function(){
		var typeId= Number($('#type').val());
	     if(typeId>0){
	    	 if(typeId==1)
	    		 accessCode="MEMO_SUBTYPE";
	    	 else
	    		 accessCode="WARNING_SUBTYPE";
	    	 
	    	 $('#subType option').remove();
			 $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/memo_warning_subtype.action",
	             data:{typeId:typeId},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){
	            	 $('#subType').append(result);
	             },
	             error:function(result){
	            	 $('#subType').append(result);
	             }
			 });
	    }else{
	    	
	    }
	     return false;
	 });
	
	 $('#company').change(function(){
			var companyId= Number($('#company').val());
		     if(companyId>0){
		    	 $('#departments option').remove();
		    	 $('#employees option').remove();
				 $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/memo_warning_departments.action",
		             data:{companyId:companyId},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){
		            	 $('#departments').append(result);
		             },
		             error:function(result){
		            	 $('#departments').append(result);
		             }
				 });
				 employeesCall();
		    }
		     return false;
		 });
	 
	 	$('#departments').change(function(){
		    	 $('#employees option').remove();
				 employeesCall();
			return false;	
		 });
		
	 
	
	
	$("#createdDate").datepick();
	
	//pop-up config
    $('.employee-popup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         currentObjectIdentiy=$(this).attr("id");
         var rowId=-1; 
         var personTypes="ALL";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 $('#companyCall').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         currentObjectIdentiy=$(this).attr("id");
         $('#common-popup').dialog('open');
         var rowId=-1; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
               data:{id:rowId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
	 
	 $('#productCall').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         currentObjectIdentiy=$(this).attr("id");
         $('#common-popup').dialog('open');
         var rowId=-1; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/product_list_assetusage.action",
               data:{id:rowId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
	 
   $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:450,
		bgiframe: false,
		modal: true 
	});
   
	 
   $('#save').click(function() { 
		$('.success,.error').hide();
		
		
		if($jquery("#MEMO_WARNING").validationEngine('validate')){ 
			$('#loading').fadeIn();
			var memoWarningId=Number($('#memoWarningId').val());
			var recordId=Number($('#recordId').val());
			var referenceNumber=$('#referenceNumber').val();
			var type=Number($('#type').val());
			var subType=Number($('#subType').val());
			var processType=Number($('#processType').val());
			var title=$('#title').val();
			var subject=$('#subject').val();
			var createdDate=$('#createdDate').val();
			var notes=$('#notes').val();
			var message=$('#message').val();
			var ccList="";
			var employeeList="";
			var departmentIds="";
			var companyId=Number($('#company').val());

			var employeesList=new Array();
			$('#employees option:selected').each(function(){
				employeesList.push($(this).val());
			});
			if(employeesList.length==0){
				$('.error').hide().html("Please select alteast one employee.").slideDown();
			}
				
			for(var i=0; i<employeesList.length;i++){
				if(i==(employeesList.length)-1)
					employeeList+=employeesList[i];
				else
					employeeList+=employeesList[i]+",";
			}
			
			var personList=new Array();
			$('#persons option:selected').each(function(){
				personList.push($(this).val());
			});
			for(var i=0; i<personList.length;i++){
				if(i==(personList.length)-1)
					ccList+=personList[i];
				else
					ccList+=personList[i]+",";
			}
			
			var departmentList=new Array();
			$('#departments option:selected').each(function(){
				departmentList.push($(this).val());
			});
			for(var i=0; i<departmentList.length;i++){
				if(i==(departmentList.length)-1)
					departmentIds+=departmentList[i];
				else
					departmentIds+=departmentList[i]+",";
			}
			
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(memoWarningId>0)
				successMessage="Successfully Modified";

			
			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/memo_warning_save.action",
				data: {
					memoWarningId:memoWarningId,
					referenceNumber:referenceNumber,
					type:type,
					subType:subType,
					title:title,
					subject:subject,
					createdDate:createdDate,
					notes:notes,
					message:message,
					ccList:ccList,
					employeeList:employeeList,
					companyId:companyId,
					departmentIds:departmentIds,
					processType:processType,
					recordId:recordId
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			//$('.error').hide().html("Please enter all the mandatory information").slideDown(); 
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		 returnCallTOList();
		$('#loading').fadeOut();
	});
   
   var memoWarningId=Number($('#memoWarningId').val());
	if(memoWarningId>0){
		$('#type').val($('#typeTemp').val());
		$('#type').trigger('change');
		$('#processType').val($('#processTypeTemp').val());
		$('#subType').val($('#subTypeTemp').val());
		$('#processType').val($('#processTypeTemp').val());
		$('#company').val($('#companyTemp').val());
		$('#company').trigger('change');
		if($('#departmentsTemp').val().trim()!='')
			departmentAutoSelectd();
		employeeAutoSelectd();
		personAutoSelectd();
		$("#employees").multiselect( 'destroy');
		 $("#employees").multiselect();
		 $("#persons").multiselect( 'destroy');
		 $("#persons").multiselect();


	}else{
		$("#employees,#persons").multiselect();
	}
	
	

	 $('.recruitment-lookup').live('click',function(){
		 if(accessCode!=null){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
		 }else{
			 $('.error').hide().html("Choose type to get sub type").slideDown(); 
		 }
   	}); 
   
	 $('#save-lookup').live('click',function(){ 
	 		
		 if(accessCode=="MEMO_SUBTYPE"){
				$('#subType').html("");
				$('#subType').append("<option value=''>--Select--</option>");
				loadLookupList("subType");
			}
		 if(accessCode=="WARNING_SUBTYPE"){
				$('#subType').html("");
				$('#subType').append("<option value=''>--Select--</option>");
				loadLookupList("subType");
		    }
			
		}); 
	 
		setTimeout( function() {  $("#message").cleditor({width:'98%', height:'50%'}); }, 200);


});
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
function employeesCall(){
	var companyId=Number($('#company').val());
	var departmentIds="";

	var personTypeList=new Array();
	$('#departments option:selected').each(function(){
		personTypeList.push($(this).val());
	});
	for(var i=0; i<personTypeList.length;i++){
		if(i==(personTypeList.length)-1)
			departmentIds+=personTypeList[i];
		else
			departmentIds+=personTypeList[i]+",";
	}
	 $.ajax({
         type:"POST",
         url:"<%=request.getContextPath()%>/memo_warning_employees.action",
         data:{companyId:companyId,departmentIds:departmentIds},
         async: false,
         dataType: "html",
         cache: false,
         success:function(result){
        	 $('#employees').append(result);
        	 if(departmentIds==null || departmentIds.trim()==''){
        		$('#persons option').remove();
        	 	$('#persons').append(result);
        	 	$("#persons").multiselect( 'destroy');
        		$("#persons").multiselect();
        	 }
         },
         error:function(result){
        	 $('#employees').append(result);
         }
	 });
	 
	
	 
	 $("#employees").multiselect( 'destroy');
	 $("#employees").multiselect();

}
function departmentAutoSelectd() {
	 
	 var departmentArray=new Array();
	 var departmentsTemp=$('#departmentsTemp').val();
	 departmentArray=departmentsTemp.split(',');
	$('#departments option').each(function(){
		var txt=$(this).val().trim();
		if($.inArray(txt, departmentArray)>=0)
			$(this).attr("selected","selected");
	});
	$('#departments').trigger('change');
}

function employeeAutoSelectd() {
	 
	 var employeeArray=new Array();
	 var employeesTemp=$('#employeesTemp').val();
	 employeeArray=employeesTemp.split(',');
	$('#employees option').each(function(){
		var txt=$(this).val().trim();
		if($.inArray(txt, employeeArray)>=0)
			$(this).attr("selected","selected");
	});

}

function personAutoSelectd() {
	 
	 var personArray=new Array();
	 var personsTemp=$('#personsTemp').val();
	 personArray=personsTemp.split(',');
	$('#persons option').each(function(){
		var txt=$(this).val();
		if($.inArray(txt, personArray)>=0)
			$(this).attr("selected","selected");
	});

}



function returnCallTOList(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/memo_warning_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}
function personPopupResult(personId,personName){
	
	if(currentObjectIdentiy.trim()=="employeeCall"){
		$('#personName').val(personName);
		$('#personId').val(personId);
	}else{
		$('#assignedByName').val(personName);
		$('#assignedById').val(personId);
	}
}

function departmentBranchPopupResult(locationId,locationName){
	$('#locationName').val(locationName);
	$('#locationId').val(locationId);
}

function productPopupResult(productId,productName){
	$('#productName').val(productName);
	$('#productId').val(productId);
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Memo / Warning Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
			<form id="MEMO_WARNING" name="MEMO_WARNING" class="MEMO_WARNING" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" name="memoWarningId" id="memoWarningId" value="${MEMO_WARNING_INFO.memoWarning.memoWarningId}"/>
					<input type="hidden" name="recordId" id="recordId" value="${requestScope.recordId}"/>
					<fieldset>
					<div>
	                   	<label class="width30">Reference Number<span style="color:red">*</span></label>
	                  	<input type="text" class="width60 validate[required]" readonly="readonly" 
		                  	name="referenceNumber" id="referenceNumber" value="${MEMO_WARNING_INFO.memoWarning.referenceNumber}"/>

				     </div>
				     <div>
	                   	<label class="width30">Memo Type<span style="color:red">*</span></label>
						<input type="hidden" name="typeTemp" id="typeTemp" value="${MEMO_WARNING_INFO.memoWarning.type}"/>
	                  	<select id="type" name="type" class="width61 validate[required]">
	                  		<option value="">--Select one--</option>
	                  		<option value="1">Memo</option>
	                  		<option value="2">Warning</option>
	                  	</select>
				     </div>
				     <div>
	                   	<label class="width30">Sub Type<span style="color:red">*</span></label>
	                   	<input type="hidden" name="subTypeTemp" id="subTypeTemp" value="${MEMO_WARNING_INFO.memoWarning.lookupDetail.dataId}"/>
	                  	<select id="subType" name="subType" class="width61 validate[required]">
	                  		
	                  	</select>
	                  	<span class="button" id="SUB_TYPE_1"
							style="position: relative; "> <a
							style="cursor: pointer;" id="SUB_TYPE"
							class="btn ui-state-default ui-corner-all recruitment-lookup width100">
								<span class="ui-icon ui-icon-newwin"> </span></a> 
						</span>
				     </div>
			      	 <div>
	                   	<label class="width30">Title<span style="color:red">*</span></label>
		                  	<input type="text" class="width60 validate[required]" 
		                  	name="title" id="title" value="${MEMO_WARNING_INFO.memoWarning.title}"/>
			      	 </div>
				    <div>
	                   	<label class="width30">Subject<span style="color:red">*</span></label>
		                  	<input type="text" class="masterTooltip width60 validate[required]" 
		                  	name="subject" id="subject" value="${MEMO_WARNING_INFO.memoWarning.subject}"/>
			      	</div> 
			      	<div>
		                 <label class="width30">Date<span style="color:red">*</span></label>
		                 <input type="text" name="createdDate" class="width60 validate[required]" id="createdDate" value="${MEMO_WARNING_INFO.createdDateStr}">
		            </div>
		           
		            <div>
		                 <label class="width30">Company<span style="color:red">*</span></label>
		                 <input type="hidden" name="companyTemp" id="companyTemp" value="${MEMO_WARNING_INFO.companyId}"/>
		              	  <select id="company" name="company" class="width61 validate[required]">
		              	  <option value="">--Select--</option>
		              	  	<c:forEach items="${requestScope.COMPANY_LIST}" var="nltlst">
								<option value="${nltlst.companyId}">${nltlst.companyName} </option>
							</c:forEach>
	                  	 </select>
		            </div>
				 </fieldset>
				</div>
				<div id="hrm" class="float-left width50"  >
					<fieldset>
					
		            <div>
		                 <label class="width30">Department's</label>
		                 <input type="hidden" name="departmentsTemp" id="departmentsTemp" value="${MEMO_WARNING_INFO.memoWarning.department}"/>
		              	  <select id="departments" name="departments" multiple size="7" class="width60">
	                  	 </select>
		            </div>
		            <div>
	                   	<label class="width30">Process Type<span style="color:red">*</span></label>
						<input type="hidden" name="processTypeTemp" id="processTypeTemp" value="${MEMO_WARNING_INFO.memoWarning.processType}"/>
	                  	<select id="processType" name="processType" class="width60 validate[required]">
	                  		<option value="">--Select one--</option>
	                  		<option value="1">Company Specific</option>
	                  		<option value="2">Department Specific</option>
	                  		<option value="3">Employee Specific</option>
	                  	</select>
				     </div>
		            <div>
		                 <label class="width30">Description / Notes</label>
		                 <textarea  name="notes" class="width60 " id="notes">${MEMO_WARNING_INFO.memoWarning.notes}</textarea>
		            </div>
		            
		           </fieldset>
				</div>
				<div id="hrm" class="float-left width50"  >
					<fieldset>
						<legend>Employee's<span style="color:red">*</span></legend>
						<input type="hidden" name="employeesTemp" id="employeesTemp" value="${MEMO_WARNING_INFO.memoPersonIds}"/>
						<div>
							<select id="employees" name="employees" multiple class="width60">
	                  	 	</select>
						</div>
					</fieldset>
				</div>
				<div id="hrm" class="float-left width50"  >
					<fieldset>
						<legend>Corbon Copy Person's</legend>
						<input type="hidden" name="personsTemp" id="personsTemp" value="${MEMO_WARNING_INFO.ccPersonIds}"/>
						<div>
							<select id="persons" name="persons" multiple class="width60">
	                  	 	</select>
						</div>
					</fieldset>
				</div>
				<div id="hrm" class="float-left width100"  >
					<fieldset>
						<legend>Message</legend>
	                 	<textarea id="message" name="message">&nbsp;${MEMO_WARNING_INFO.memoWarning.message}</textarea>
	                </fieldset>
				</div>
		 </form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>
