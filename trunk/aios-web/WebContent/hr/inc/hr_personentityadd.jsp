 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity" id="childRowId_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="periodform" id="periodEditAddValidate">
<input type="hidden" name="addEditFlag" id="addEditFlag"  value="${requestScope.AddEditFlag}"/>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div><label class="width30">Address</label><input type="text" tabindex="1" name="address" id="address" class="width60 validate[required]"></div>
		<div><label class="width30">Email</label><input type="text" tabindex="2" name="email" id="email" class="width60"></div> 
	</fieldset>
</div>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div><label class="width30">Mobile</label><input type="text" tabindex="3" name="mobile" id="mobile" class="width60"></div>
		<div><label class="width30">Phone</label><input type="text" tabindex="4" name="phone" id="phone" class="width60"></div>
	</fieldset> 
</div> 
	<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_address" id="cancel_${requestScope.rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right add_address" id="update_${requestScope.rowid}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
	 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){  
		 $('#periodName').bestupper(); 
		 clear:false;
		  $('.cancel_address').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
  					$("#AddImage_"+rowid).show();
  				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
  				 	$("#EditImage_"+rowid).show();
				}
				 $("#childRowId_"+rowid).remove();	
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 
			     var childCount=Number($('#childCount').val()); 
		          $('#childCount').val(childCount);
					if(childCount<1){
						$('#calendarName').attr("disabled",false);
						$('#sfrom').attr("disabled",false);
						$('#sto').attr("disabled",false);  
					}
					else{
						$('#calendarName').attr("disabled",true);
						$('#sfrom').attr("disabled",true);
						$('#sto').attr("disabled",true);  
					} 
					$('#openFlag').val(0);
		  });
		  $('.add_address').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 
			 //var tempObject=$(this);
			 var personAddressId=$('#personAddressId_'+rowid).val();	
			 var address=$('#address').val();
			 var email=$('#email').val();
			 var mobile=$('#mobile').val();
			 var phone=$('#phone').val();
			 var lineId=$('#lineId_'+rowid).val();
			 var addEditFlag=$('#addEditFlag').val();
			
		        if($("#periodEditAddValidate").validationEngine({returnIsValid:true})){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/person_address_save.action", 
					 	async: false, 
					 	data:{personAddressId:personAddressId,
							 	address:address,
							 	email:email,
							 	mobile:mobile,
							 	phone:phone,
							 	addEditFlag:addEditFlag,
							 	id:lineId
							 },
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 

			        	//Bussiness parameter
		        			$('#address_'+rowid).text(address); 
		        			$('#email_'+rowid).text(email); 
		        			$('#mobile_'+rowid).text(mobile); 
		        			$('#phone_'+rowid).text(phone);

						//Button(action) hide & show 
		    				 $("#AddImage_"+rowid).hide();
		    				 $("#EditImage_"+rowid).show();
		    				 $("#DeleteImage_"+rowid).show();
		    				 $("#WorkingImage_"+rowid).hide(); 
		        			 $("#childRowId_"+rowid).remove();	

		        			
	         				$('.addrows').trigger('click');
	         				
		        		}		  	 
		      } 
		      else{return false;}
		  });
			
	 });
 </script>	