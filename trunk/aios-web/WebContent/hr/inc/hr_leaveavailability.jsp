<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>

<div id="hrm" class="hastable width100"  >  
	<table id="hastab2" class="width100"> 
		<thead>
			<tr> 
				<th style="width:5%">Leave Type</th>
				<th style="width:5%">Provided Days</th> 
				<th style="width:5%">Eligible Days</th>  
			    <th style="width:5%">Taken Days</th>
			    <th style="width:5%">Balance Days</th>
		  </tr>
		</thead> 
		<tbody class="tabS">
			<c:choose>
				<c:when test="${LEAVE_AVAILABILITY ne null && LEAVE_AVAILABILITY ne '' && fn:length(LEAVE_AVAILABILITY)>0}">
					<c:forEach var="leav" items="${LEAVE_AVAILABILITY}" varStatus="status1">
						<tr class="rowidS" id="fieldrowS_${status1.index+1}">
							<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
							<td id="leaveType_${status1.index+1}">${leav.leaveType}</td> 
							<td id="providedDays_${status1.index+1}">${leav.providedDays}</td> 
						<td id="eligibleDays_${status1.index+1}">${leav.eligibleDays}</td> 
							<td id="takenDays_${status1.index+1}">${leav.takenDays}</td> 
							<td id="availableDays_${status1.index+1}" style="font-weight: bold;font-size: 16px;">${leav.availableDays}</td> 
							<td style="display:none;">
								<input type="hidden" id="leaveId_${status1.index+1}" value="${leav.leaveId}"/>
							</td>
						</tr>
					</c:forEach>  
				</c:when>
				<c:otherwise>
					<tr><td colspan="6">No Records</td></tr>
				</c:otherwise>
			</c:choose>  
			 </tbody>
	</table>
</div> 
