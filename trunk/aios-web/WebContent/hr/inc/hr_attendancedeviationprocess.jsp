<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.hr.domain.entity.JobAssignment"%>
<%@page import="java.util.*"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.label-data{
	font-weight: normal!important;
}


</style><script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
$(function(){ 
	$('.formError').remove();
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	personId=Number($('#personId').val());
	attendanceId=Number($('#attendanceId').val());
	$('#deductionType').val(Number($('#deductionTypeTemp').val()));	
	$('#calcultionMethod').val(Number($('#calcultionMethodTemp').val()));	
	$('#history').click(function(){
		checkDataTableExsist();
	});

/* Click event handler */
$('#Attendance tbody tr').live('click', function () { 
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
});

$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.otherErr ').hide();
       }
});


$('#edit').click(function(){
	if(s == null)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/add_job.action",  
     	data: {jobId:recId2},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
});

$('#read').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/attendance_read.action",  
		async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
     	}
	}); 
});
$('#calculate').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/attendance_calculate.action",  
		async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
     	}
	}); 
});
$('.discard').live('click',function(){
	window.location.reload();
	return false;
});

$('.reset').live('click',function(){
	clear_form_elements();
});
$jquery("#LEAVEDET").validationEngine('attach');
//Save & discard process
$('#approveButton,#publishButton').click(function(){ 
	$('#page-error').hide();
	if($jquery("#LEAVEDET").validationEngine('validate')){  
		loanLineDetail=""; loanChargesDetail="";
		var attendanceId=Number($('#attendanceId').val());
		var deductionType=Number($('#deductionType').val());
		var comments=$('#comment').val();
		var calcultionMethod=$('#calcultionMethod').val();
		var halfDay=$('#halfDay').attr('checked');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/attendance_deviation_save.action", 
		 	async: false, 
		 	data:{ 
		 		attendanceId:attendanceId,deductionType:deductionType,
		 		comments:comments,halfDay:halfDay,calcultionMethod:calcultionMethod
		 	},
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 window.location.reload();
				 }else{
					 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				 }
			},
			error:function(result){  
				$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
			}
		});  
		
	}
	else{
		return false;
	}
});

});
function clear_form_elements() {
	$('#page-error').hide();
	$('#LEAVEDET').each(function(){
	    this.reset();
	});
}
function checkDataTableExsist(){
	if(typeof(oTable)!='undefined'){
		oTable = $('#AttendanceApproval').dataTable();
		oTable.fnDestroy();
		$('#AttendanceApproval').remove();
		$('#gridDiv').html("<table class='display' id='AttendanceApproval'></table>");
	}
	attendanceListJsonCall();
}function attendanceListJsonCall(){
	$('#AttendanceApproval').dataTable({ 
		"sAjaxSource": "attendance_listing_approval_json.action?personId="+personId+'&attendanceId='+attendanceId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Attendance ID', "bVisible": false},
			{ "sTitle": 'Date'},
			{ "sTitle": 'Time In'},
			{ "sTitle": 'Time Out'},
			{ "sTitle": 'Late In', "bVisible": false},
			{ "sTitle": 'Late Out', "bVisible": false},
			{ "sTitle": 'Early In', "bVisible": false},
			{ "sTitle": 'Early Out', "bVisible": false},
			{ "sTitle": 'Time Loss'},
			{ "sTitle": 'Time Excess'},
			{ "sTitle": 'Total Hours'},
			{ "sTitle": 'Is Deviated?' , "bVisible": false},
			{ "sTitle": 'Process Status', "bVisible": false},
			{ "sTitle": 'Syestem Suggetion' ,"sWidth": "150px", "bVisible": false},
			{ "sTitle": 'Decision'},
			{ "sTitle": 'Half Day'},
			
		],
		"sScrollY": $("#main-content").height() - 350,
		//"bPaginate": false,
		//"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#AttendanceApproval').dataTable();
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Attendance Deviation</div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
		<div style="display:none;" class="tempresult">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<div>
			 <div class="error response-msg ui-corner-all" style="display:none;">Sorry, Process Failure!</div>
		  	  <div class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div>
		  <form id="LEAVEDET" class="" name="LEAVEDET" method="post" style="position: relative;">
		  	<div class="width100 float-left view-content" id="hrm"> 
		  			<input type="hidden" name="attendanceId" id="attendanceId" value="${ATTENDANCE_PROCESS.attendanceId}" class="width50"/>
		  			<input type="hidden" name="personId" id="personId" value="${ATTENDANCE_PROCESS.person.personId}" class="width50"/>
		  			
		  			<div class="width100 float-left " >
						<fieldset style="min-height:80px;" >
							<legend>Employee Information</legend>
							<div class="width45 float-left">
								<c:set var="jobAssignment" value="${ATTENDANCE_PROCESS.jobAssignment}"/>
								<div class="width100 float-left">
									<label class="width30" >Employee : </label>
									<label class="width50 label-data" >${ATTENDANCE_PROCESS.person.firstName} ${ATTENDANCE_PROCESS.person.lastName}</label>
								</div>  
								<div class="width100 float-left">
									<label class="width30" >Job : </label>
									<label class="width50 label-data" >${jobAssignment.designation.designationName}</label>
								</div> 
								<div class="width100 float-left">
									<label class="width30" >Grade : </label>
									<label class="width50 label-data" >${jobAssignment.designation.grade.gradeName}</label>
								</div> 
							</div>
							<div class="width45 float-left">
								<div class="width100 float-left">
									<label class="width30" >Company : </label>
									<label class="width50 label-data" >${jobAssignment.cmpDeptLocation.company.companyName}</label>
								</div>
								<div class="width100 float-left">
									<label class="width30" >Department : </label>
									<label class="width50 label-data" >${jobAssignment.cmpDeptLocation.department.departmentName}</label>
								</div>
								<div class="width100 float-left">
									<label class="width30" >Location : </label>
									<label class="width50 label-data" >${jobAssignment.cmpDeptLocation.location.locationName}</label>
								</div>
							</div>
						</fieldset>
				 	</div> 
				 	<div class="width100 float-left">
						<fieldset style="min-height:190px;">
							<legend>Deviation Information</legend>
							<div class="width30 float-left">
								<div class="width100 float-left">
									<label class="width50 " >Attendance Date : </label>
									<c:set var="attendanceDate" value="${ATTENDANCE_PROCESS.attendanceDate}"/>  
										<%String attendanceDate = DateFormat.convertDateToString(pageContext.getAttribute("attendanceDate").toString());%>
									<label class="width30 label-data " ><%=attendanceDate%></label>
								</div>
								<div class="width100 float-left">
									<label class="width50 " >Time In : </label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.timeIn}</label>
								</div>  
								<div class="width100 float-left">
									<label class="width50 " >Time Out : </label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.timeOut}</label>
								</div> 
								<div class="width100 float-left">
									<label class="width50 float-left" >Late In:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.lateIn} </label>
								</div> 
								<div class="width100 float-left">
									<label class="width50 " >Late out:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.lateOut} </label>
								</div>
								<div class="width100 float-left">
									<label class="width50 " >Early In:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.earlyIn} </label>
								</div> 
								<div class="width100 float-left">
									<label class="width50" >Early out:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.earlyOut} </label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Time Loss:</label>
									<label class="width30 label-data "><span style="background-color: red;color: white;">${ATTENDANCE_PROCESS.timeLoss}</span></label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Time Excess:</label>
									<label class="width30 label-data " ><span style="background-color: green;color: white;">${ATTENDANCE_PROCESS.timeExcess}</span></label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Total Hours:</label>
									<label class="width30 label-data " >${ATTENDANCE_PROCESS.totalHours} </label>
								</div>
							</div>
							<legend>Shift Information </legend>
							<div class="width30 float-left">
								<div class="width100 float-left">
									<label class="width50" >Working Shift : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.workingShiftName}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Start Time : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.startTime}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >End Time : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.endTime}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Late Time : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.lateAttendance}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Severe Late Time : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.seviorLateAttendance}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Buffer Min(s) : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.bufferTime}</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >Break Hours : </label>
									<label class="width40 label-data" >${ATTENDANCE_PROCESS.jobShift.workingShift.breakHours}</label>
								</div>
							</div>
							<%-- <legend>Policy Details</legend>
							<div class="width30 float-left">
								
								<div class="width100 float-left">
									<c:choose>
										<c:when test="${ATTENDANCE_POLICY.allowOt ne null && ATTENDANCE_POLICY.allowOt eq true}">
											<label class="width50" >Over Time (Half a day) :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.otHoursForHalfday} -(hrs in Minimum)</label>
										</c:when>
										<c:otherwise>
											<label class="width50" >CompOff (Half a day) :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.compoffHoursForHalfday} -(hrs in Minimum)</label>
										</c:otherwise>
									</c:choose>	
								</div>
								<div class="width100 float-left">
									<c:choose>
										<c:when test="${ATTENDANCE_POLICY.allowOt ne null && ATTENDANCE_POLICY.allowOt eq true}">
											<label class="width50" >Over Time (1 day) :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.otHoursForFullday} -(hrs in Minimum)</label>
										</c:when>
										<c:otherwise>
											<label class="width50" >CompOff (1 day) :</label>
											<label class="width30 label-data" >${ATTENDANCE_POLICY.compoffHoursForFullday} -(hrs in Minimum)</label>
										</c:otherwise>
									</c:choose>								
								</div>
								<div class="width100 float-left">
									<label class="width50" >LOP Hour(s) for 1 Day :</label>
									<label class="width30 label-data" >${ATTENDANCE_POLICY.lopHoursForFullday} -(hrs in Minimum)</label>
								</div>
								<div class="width100 float-left">
									<label class="width50" >LOP Hour(s) for Half Day :</label>
									<label class="width30 label-data" >${ATTENDANCE_POLICY.lopHoursForHalfday} -(hrs in Minimum)</label>
								</div>
								
							</div> --%>
							
						</fieldset>
				 	</div> 
					<div class="width100 float-left">
						<fieldset style="min-height:135px;">
							<legend>Deviation Process</legend>
							<div class="width45 float-left">
								<div>
									<label class="width30" for="deductionType">Process Type<span class="mandatory">*</span></label>
									<input type="hidden" name="deductionTypeTemp" id="deductionTypeTemp" value="${ATTENDANCE_PROCESS.deductionType}">
									<select id="deductionType" name="deductionType" class="width51 validate[required]">
										<option value="">--Select--</option>
										<option value="1">NoAction</option>
										<option value="2">LOP</option>
										<option value="3">OverTime</option>
										<option value="4">CompOff</option>
										<option value="5">COD-CompOff Deduction</option>
									</select>
								</div>  
								<div>
									<label class="width30">Calculation Method</label>
									<div>
										<select id=calcultionMethod
											name="calcultionMethod"
											class="width51 ">
											<option value="">--Select--</option>
											<c:forEach items="${CALCULATION_METHOD}" var="gen">
												<option value="${gen.key}">${gen.value}
												</option>
											</c:forEach>
										</select>  <input
											type="hidden" name="calcultionMethodTemp" id="calcultionMethodTemp"
											value="${ATTENDANCE_PROCESS.calcultionMethod}" />
									</div>
								</div> 
								<c:choose>
									<c:when test="${ATTENDANCE_PROCESS.deviationHours ne null}">
										<label class="float-left width30" for="deviationHours">Deviation Hour's</label>
										<input type="text" name="deviationHours" id="deviationHours" value="${ATTENDANCE_PROCESS.deviationHours}" class="width50" >
									</c:when>
								</c:choose>
								<div>
									<label class="float-left width30" for="leaveType">Half Day</label>
								<c:choose>
										<c:when test="${ATTENDANCE_PROCESS.halfDay eq true}">
											<input type="checkbox" name="halfDay" id="halfDay" checked="checked" class="float-left width5" >
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="halfDay" id="halfDay" class="float-left width5" >
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="width45 float-left">
								<div class="width100 float-left">
									<label class="width30" for="systemSuggesion">System Suggestion : </label>
									<label class="width60 label-data">${ATTENDANCE_PROCESS.systemSuggesion}</label>
								</div>
								<div class="width100 float-left" style="margin-top:10px;">
									<label class="width30" for="systemSuggesion">Available Comp-Off: </label>
									<label class="width60 label-data">${COMPOFF_DAYS}</label>
								</div>
								<div class="width100 float-left" style="margin-top:10px;">
									<label class="width30" for="reason">Employee Reason : </label>
									<label class="width60 label-data">${ATTENDANCE_PROCESS.reason}</label>
								</div>
								<div class="width100 float-left" style="margin-top:10px;">
									<label class="width30" for="comment">Comment<span class="mandatory">*</span></label>
									<textarea name="comment" id="comment" class="width60 validate[required]" >${ATTENDANCE_PROCESS.comment}</textarea>
								</div>
							</div>
							
						</fieldset>
				 </div> 
				 <div class="clearfix"></div>
				 <div class="float-left buttons ui-widget-content ui-corner-all">
					<div class="portlet-header ui-widget-header float-right history" id="history">History</div>
				</div>
				<!-- <div class="float-right buttons ui-widget-content ui-corner-all">
					<div class="portlet-header ui-widget-header float-right discard" id="discard">Close</div>
					<div class="portlet-header ui-widget-header float-right save" id="save">Save</div>
				</div> -->
 			</div>
 		</form>
		<div class="width100 float-left" id="hrm"> 
			<div id="gridDiv">
				<table class="display" id="AttendanceApproval"></table>
			</div>
		</div>
	</div>
</div>
</div>
