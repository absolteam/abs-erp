 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
 
<tr style="background-color: #F8F8F8;" id="identitychildRowId_${requestScope.rowid}">
<td colspan="6" class="tdidentity" id="identitychildRowTD_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="periodform" id="periodEditAddValidate" style="position: relative;">
<input type="hidden" name="identityaddEditFlag" id="identityaddEditFlag"  value="${requestScope.AddEditFlag}"/>
<div class="float-left width50" id="hrm" >
	<fieldset>
		<div>
			<label class="width30"><fmt:message key="hr.person.identitytype"/><span style="color:red">*</span></label>
			<input type="hidden" name="identityTypeTemp" id="identityTypeTemp" class="width60">
			<select name="identityType" id="identityType" tabindex="1" class="width60 validate[required]">
				<option value="">--Select--</option>
				<c:if test="${requestScope.IDENTITY_TYPE_LIST!=null}">
				<c:forEach items="${requestScope.IDENTITY_TYPE_LIST}" var="nltlst">
					<option value="${nltlst.dataId}">${nltlst.displayName} </option>
				</c:forEach>
				</c:if>	
			</select>
		</div>
		<div><label class="width30"><fmt:message key="hr.person.identitynumber"/><span style="color:red">*</span></label><input type="text" tabindex="2" name="identityNumber" id="identityNumber" class="width60 validate[required]"></div>
		<div><label class="width30"><fmt:message key="hr.person.issuedplace"/></label><input type="text" tabindex="3" name="identityIssuedPlace" id="identityIssuedPlace" class="width60"></div>
		<div><label class="width30"><fmt:message key="hr.person.expiredate"/><span style="color:red">*</span></label><input type="text" tabindex="4" name="idendityExpireDate" id="idendityExpireDate" readonly="readonly" class="width60 validate[required]"></div> 
	</fieldset>
</div>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div><label class="width30"><fmt:message key="common.description"/></label><textarea tabindex="3" name="identityDescription" id="identityDescription" class="width60"></textarea></div>
	</fieldset> 
</div> 
	<div class="clearfix"></div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_identity" id="cancel_${requestScope.rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right add_identity" id="update_${requestScope.rowid}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
	 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){  
		 $jquery("#periodEditAddValidate").validationEngine('attach');
		 $('#idendityExpireDate').datepick();
		 clear:false;
		  $('.cancel_identity').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#identityaddEditFlag').val()!=null && $('#identityaddEditFlag').val()=='A')
				{
  					$("#identityAddImage_"+rowid).show();
  				 	$("#identityEditImage_"+rowid).hide();
				}else{
					$("#identityAddImage_"+rowid).hide();
  				 	$("#identityEditImage_"+rowid).show();
				}
				 $("#identitychildRowId_"+rowid).remove();	
				 $("#identityDeleteImage_"+rowid).show();
				 $("#identityWorkingImage_"+rowid).hide();  
				 
				 tempvar=$('.identitytab>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#identityDeleteImage_'+rowid).hide(); 
				 $('#openFlag').val(0);
				 return false;
		  });
		  $('.add_identity').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 
			 //var tempObject=$(this);
			 var identityId=$('#identityId_'+rowid).val();	
			 var identityType=$('#identityType :selected').text();
			 var identityTypeId=$('#identityType :selected').val();
			 var identityNumber=$('#identityNumber').val();
			 var identityIssuedPlace=$('#identityIssuedPlace').val();
			 var idendityExpireDate=$('#idendityExpireDate').val();
			 var identityDescription=$('#identityDescription').val();
			 var lineId=$('#identitylineId_'+rowid).val();
			 var addEditFlag=$('#identityaddEditFlag').val();
			
		        if($jquery("#periodEditAddValidate").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/company_identity_save.action", 
					 	async: false, 
					 	data:{identityId:identityId,
							 	identityType:identityTypeId,
							 	identityNumber:identityNumber,
							 	issuedPlace:identityIssuedPlace,
							 	expireDate:idendityExpireDate,
							 	description:identityDescription,
							 	addEditFlag:addEditFlag,
							 	id:lineId
							 },
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 

			        	//Bussiness parameter
		        			$('#identityType_'+rowid).text(identityType); 
		        			$('#identityTypeId_'+rowid).val(identityTypeId); 
		        			$('#identityNumber_'+rowid).text(identityNumber); 
		        			$('#identityExpireDate_'+rowid).text(idendityExpireDate); 
		        			$('#identityIssuedPlace_'+rowid).text(identityIssuedPlace);
		        			$('#identityDescription_'+rowid).text(identityDescription);
						//Button(action) hide & show 
		    				 $("#identityAddImage_"+rowid).hide();
		    				 $("#identityEditImage_"+rowid).show();
		    				 $("#identityDeleteImage_"+rowid).show();
		    				 $("#identityWorkingImage_"+rowid).hide(); 
		        			 $("#identitychildRowId_"+rowid).remove();	

		        			if(addEditFlag!=null && addEditFlag=='A')
		     				{
	         					$('.identityaddrows').trigger('click');
	         					var identitycount=Number($('#identitycount').val());
	         					identitycount+=1;
	         					$('#identitycount').val(identitycount);
	         					 tempvar=$('.identitytab>tr:last').attr('id'); 
	         					 idval=tempvar.split("_");
	         					 rowid=Number(idval[1]); 
	         					 $('.tempresult').html('');
	         					// $('#identityDeleteImage_'+rowid).show(); 
		     				}
		        		}	
		        	$('.tempresult').html('');	  	 
		      } 
		      else{return false;}
		  });
		  return false;	
	 });
 </script>	