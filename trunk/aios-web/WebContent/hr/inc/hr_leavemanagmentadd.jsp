<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<style>
.link-border{
 	border:2px solid #c0c0c0;
    background:#f5f5f5;
    padding:4px;
    position:relative;/* ie6 border fix*/
    top:4px;
    cursor: pointer;
    font-weight: bold;
}
</style>


<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable=null; var selectRow=""; var aData="";var aSelected = [];var leaveProcessId=0;; 
var requestStatus="";
var leaveStatus="";
var totalDays=0;
$(document).ready(function(){  
	$jquery("#LEAVEDET").validationEngine('attach');
	
	$('#leave_document_information').click(function(){
		leaveProcessId=Number($("#leaveProcessId").val());
		if(leaveProcessId>0){
			AIOS_Uploader.openFileUploader("doc","leaveDocs","LeaveProcess",leaveProcessId,"LeaveDocuments");
		}else{
			AIOS_Uploader.openFileUploader("doc","leaveDocs","LeaveProcess","-1","LeaveDocuments");
		}
	});	
	
	//Save & discard process
	$('.save').click(function(){ 
		$('#page-error').hide();
		if($jquery("#LEAVEDET").validationEngine('validate')){  
			loanLineDetail=""; loanChargesDetail="";
			var personId=Number($('#personId').val());
			var jobAssignmentId=Number($('#jobAssignmentId').val());
			var leaveProcessId=Number($('#leaveProcessId').val());
			var jobLeaveId=Number($('#leaveType').val());
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var reason=$('#reason').val();
			var contactOnLeave=$('#contactOnLeave').val();
			var halfDay=$('#halfDay').attr('checked');
			var days=Number($('#totalDaysResult').val());
			var weekendDays=Number($('#excludedWeekendDaysResult').val());
			var holidayDays=Number($('#excludedHolidaysResult').val());
			var encash=$('#encash').attr('checked');
			var settleDues=$('#settleDues').attr('checked');
			var parentLeaveId=0;//Number($('#parentLeaveId').val());
			var holdUntilRejoining=$('#holdUntilRejoining').attr('checked');
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/apply_leave_save_by_hr.action", 
			 	async: false, 
			 	data:{
			 		personId:personId,
			 		jobAssignmentId:jobAssignmentId,
			 		leaveProcessId:leaveProcessId,jobLeaveId:jobLeaveId,
			 		fromDate:fromDate,toDate:toDate,reason:reason,
			 		contactOnLeave:contactOnLeave,halfDay:halfDay,
			 		days:days,weekendDays:weekendDays,
			 		holidayDays:holidayDays,
			 		encash:encash,
			 		parentLeaveId:parentLeaveId,
			 		settleDues:settleDues,
			 		holdUntilRejoining:holdUntilRejoining
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"GET",
								url:"<%=request.getContextPath()%>/employee_leave_list.action",
								async:false,
								dataType:"html",
								cache:false,
								success:function(result){
									$("#main-wrapper").html(result);
								}
							});	
						 $('#success_message').hide().html("Successfully  Proccessed").slideDown(1000);
					 }else{
						 $('#page-error').hide().html(message).slideDown(2000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').click(function(){
		$.ajax({
			type:"GET",
			url:"<%=request.getContextPath()%>/employee_leave_list.action",
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});	
		return false;
	});
	
	$('.reset').click(function(){
		clear_form_elements();
	});
	
     //Date process
     $('#fromDate,#toDate').datepick({
    	 onSelect: customRange,showTrigger: '#calImg'});
     
     $('#fromDate').datepick({
    	 defaultDate: '0', selectDefaultDate: true});   
    
     $('#halfDay').change(function(){
    	 if($('#halfDay').attr("checked"))
			$('#totalDaysResult').val(Number($('#totalDaysResult').val())-0.5);
    	 else
    		 $('#totalDaysResult').val(totalDays); 
     });
     
     //Popup

   //pop-up config
   $('.employee-popup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#job-common-popup').dialog('open');
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/get_employee_for_leavemanagment.action",
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.job-common-result').html(result);
               },
               error:function(result){
                    $('.job-common-result').html(result);
               }
           });
            return false;
   }); 
     
   $('#holdUntilRejoining').click(function(){
	   var jobAssignmentId=Number($("#jobAssignmentId").val());
	   if(jobAssignmentId==null || jobAssignmentId==0 || jobAssignmentId==''){
		   $('#holdUntilRejoining').attr('checked',false);
		   $('#page-error').hide().html("Choose the employee first").slideDown(2000);
		   return false;
	   }
	   
	   if($('#holdUntilRejoining').attr('checked')){
	         $.ajax({
	            type:"POST",
	            url:"<%=request.getContextPath()%>/check_duplicate_rejoining_hold.action",
	            async: false,
	            data : {jobAssignmentId:jobAssignmentId,leaveProcessId:leaveProcessId},
	            dataType: "html",
	            cache: false,
	            success:function(result){
	            	 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()!="SUCCESS"){
	                	 var cnfrm = confirm(message);
	         			if(!cnfrm){
		                	 $('#holdUntilRejoining').attr('checked',false);
	         				return false;
	         			}else{
	         				$('#holdUntilRejoining').attr('checked',true);
	         			}
	                 }
	            },
	            error:function(result){
	                 $('.job-common-result').html(result);
	            }
	        });
	   }else{
		   return true;
	   }
       return true;
 	}); 
   
   

   $('#job-common-popup').dialog({
   	autoOpen: false,
   	minwidth: 'auto',
   	width:800,
   	height:550,
   	bgiframe: false,
   	modal: true 
   });
   
   $("#leave_history_view").click(function(){ 
	 	jobAssignmentId=Number($('#jobAssignmentId').val());
	 	if(jobAssignmentId>0)
			window.open('<%=request.getContextPath()%>/leave_history_view.action?jobAssignmentId='+jobAssignmentId,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	 	else
	 		alert("Select any one Employee!");
		return false;
		
	});
   
   $("#leave_history_print").click(function(){ 
	   jobAssignmentId=Number($('#jobAssignmentId').val());
	 	if(jobAssignmentId>0)
			window.open('<%=request.getContextPath()%>/leave_history_print.action?jobAssignmentId='+jobAssignmentId,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	 	else
	 		alert("Select any one Employee!");
		return false;
		
	});

});
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function leaveTypeCall(personId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getLeaveTypes.action", 
	 	async: false, 
	 	data:{ 
	 		personId:personId
	 	},
	    dataType: "html",
	    cache: false,
		success:function(result){
			 $("#leaveTypeDiv").html(result);
		},
		error:function(result){  
			$("#leaveTypeDiv").html(result);
		}
	});  
	
	check_leave_availability($('#fromDate').val(),$('#toDate').val());
	leaveExcludingCalculation($('#fromDate').val(),$('#toDate').val());
}
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		dateOnchangeCall();
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
		dateOnchangeCall();
	} 
}
function dateOnchangeCall(){
	if($('#toDate').val()!=null && $('#toDate').val()!="" && $('#fromDate').val()!=null && $('#fromDate').val()!=""){ 
		daysDiff();
		check_leave_availability($('#fromDate').val(),$('#toDate').val());
		leaveExcludingCalculation($('#fromDate').val(),$('#toDate').val());
		totalDays=$('#totalDaysResult').val();
		
	}
	return true;
}

function clear_form_elements() {
	$('#page-error').hide();
	$('#employeeName').html("");
	$('#LEAVEDET').each(function(){
	    this.reset();
	});
}
var fromFormat='dd-MMM-yyyy';
var toFormat='MM/dd/y';
function daysDiff() {
	var tempdate1 = formatDate(new Date(getDateFromFormat($("#fromDate").val(),fromFormat)),toFormat);
	var tempdate2 = formatDate(new Date(getDateFromFormat($("#toDate").val(),fromFormat)),toFormat);
	var date1=new Date(tempdate1);
	var date2=new Date(tempdate2);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	 $('#tempDays').val(diffDays);
}

function check_leave_availability(fromDate,toDate) {
	$('#page-error').hide();
	var jobAssignmentId=Number($('#jobAssignmentId').val());

	if(jobAssignmentId>0){ 
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/check_leave_availability.action", 
			 	async: false, 
			 	data:{ 
			 		fromDate:fromDate,toDate:toDate,jobAssignmentId:jobAssignmentId
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#leaveAvailabilityDiv').html(result);
				},
				error:function(result){  
					$('#leaveAvailabilityDiv').html("No Availability").slideDown(1000);
				}
			});  
	 }else{
		 $('#leaveAvailabilityDiv').html("Choose Employee to get Leave Availability").slideDown(1000); 
	 }
	 return false;
}
function leaveExcludingCalculation(fromDate,toDate) {
	$('#page-error').hide();
	var jobAssignmentId=Number($('#jobAssignmentId').val());

	if(jobAssignmentId>0){ 
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/leave_excluding_calculation.action", 
			 	async: false, 
			 	data:{ 
			 		fromDate:fromDate,toDate:toDate,jobAssignmentId:jobAssignmentId
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#LEAVE_EXCLUDING_RESULT').html(result);
				},
				error:function(result){  
					$('#LEAVE_EXCLUDING_RESULT').html("Error In Total Days Calculation").slideDown(1000);
				}
			});  
	 }else{
		 $('#LEAVE_EXCLUDING_RESULT').html("Choose Employee to get Leave Days").slideDown(1000); 
	 }
	 return false;
}

</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Apply Leave</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="LEAVEDET" class="" name="LEAVEDET" method="post" style="position: relative;">
		  	<div class="width100 float-left" id="hrm"> 
					<div class="width100 float-left" id="LeaveInfoContent">
						<fieldset style="min-height:200px;">
							<legend>Leave Information</legend>
							
							<div class="width55 float-left">
								<input type="hidden" name="leaveProcessId" id="leaveProcessId" value="${LEAVE_PROCESS.leaveProcessId}" />
								<div>
								 	<label class="width40" >Employee<span class="mandatory">*</span></label>
									<input type="text" readonly="readonly" id="employeeName" class="float-left width50 validate[required]"/>
									<span class="button" id="employee"  style="position: relative; top:6px;">
										<a style="cursor: pointer;" id="employeepopup" class="btn ui-state-default ui-corner-all width10 employee-popup"> 
											<span class="ui-icon ui-icon-newwin">
											</span> 
										</a>
									</span> 
									<input type="hidden" id="personId" class="personId  validate[required]" />
									<input type="hidden" id="jobAssignmentId" class="jobAssignmentId  validate[required]" />
								</div>
								<div id="leaveTypeDiv">
									<label class="width40" for="leaveType">Leave Type<span class="mandatory">*</span></label>
									<select id="leaveType" name="leaveType" class="width51 validate[required]">
										<option value="">--Select--</option>
										<c:forEach items="${JOB_LEAVE_LIST}" var="jobleave" varStatus="status">
											<option value="${jobleave.jobLeaveId}">${jobleave.leave.lookupDetail.displayName}</option>
										</c:forEach>
									</select>
								</div>   
								<div>
									<label class="width40" for="fromDate">Leave Start Date<span class="mandatory">*</span></label>
									<c:choose>
										<c:when test="${LEAVE_PROCESS.fromDate ne null &&  LEAVE_PROCESS.fromDate  ne ''}">
											<c:set var="fromDate" value="${LEAVE_PROCESS.fromDate}"/>  
											<%String fromDate = DateFormat.convertDateToString(pageContext.getAttribute("fromDate").toString());%>
											<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]" 
											value="<%=fromDate%>"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]"/>
										</c:otherwise>
									</c:choose> 
								</div>   
								<div>
									<label class="width40" for="toDate">Leave End Date<span class="mandatory">*</span></label>
									<c:choose>
										<c:when test="${LEAVE_PROCESS.toDate ne null &&  LEAVE_PROCESS.toDate ne ''}">
											<c:set var="toDate" value="${LEAVE_PROCESS.toDate}"/>  
											<%String toDate = DateFormat.convertDateToString(pageContext.getAttribute("effectiveEndDate").toString());%>
											<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="3" class="width50 validate[required]" 
											value="<%=toDate%>"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="4" class="width50 validate[required]"/>
										</c:otherwise>
									</c:choose> 
								</div> 
								
								<div>
									<input type="checkbox" name="halfDay" id="halfDay" class="float-left width5" >
									<label class="float-left width40" for="leaveType">Half Day</label>
									<label class="float-left width40" for="leaveType" style="display:none;">No. Of Days :</label>
									<input type="hidden" id="tempDays" class="float-left width10" readonly="readonly"/>
									<input type="hidden" id="tempYears"/><input type="hidden" id="tempMonths"/>
								</div>
							</div>
							
							<div class="width45 float-left">
								
								<div>
									<label class="width30" for="reason">Reason</label>
									<textarea name="reason" id="reason" class="width60" ></textarea>
								</div>
								<div>
									<label class="width30" for="contactOnLeave">Contact On Leave</label>
									<textarea name="contactOnLeave" id="contactOnLeave" class="width60" ></textarea>
								</div>
								<div>
									<input type="checkbox" name="settleDues" id="settleDues" class="float-left width5" >
									<label class="float-left width15" for="leaveType">Settle Dues</label>
									
								</div>
								<div>
									<input type="checkbox" name="encash" id="encash" class="float-left width5" >
									<label class="float-left width15" for="leaveType">Encash</label>
								</div>
								<div>
									<input type="checkbox" name="holdUntilRejoining" id="holdUntilRejoining" class="float-left width5" >
									<label class="float-left width25" for="holdUntilRejoining">Service days excluded until rejoining</label>
								</div>
							</div>
							<!-- <div class="width80 float-left">
								<fieldset style="min-height:50px;">
										<legend>Leave</legend>
										<div id="LEAVE_EXCLUDING_RESULT" class="width100 float-left"></div>
								</fieldset>
							</div> -->
							<div class="width100 float-left" id="hrm"> 
				           		 <div class="width55 float-left">
				           			<fieldset style="min-height:50px;">
												<legend>Leave Day's</legend>
												<div id="LEAVE_EXCLUDING_RESULT" class="width100 float-left"></div>
									</fieldset>
								</div>
								<div class="width45 float-left">
								 <fieldset>
						    	 	<div  style="height: 50px;overflow-y:auto;">
									  	<div class="float-left">
											<span id="leave_document_information" style="cursor: pointer; color: blue;">
												<u><fmt:message key="hr.company.documentuplode"/></u>
											</span>
										</div>
										<div class="width100 float-right">
											<span id="leaveDocs"></span>
										</div>
									</div>
							     </fieldset>
							    </div>
				 			</div>
							<div class="clearfix"></div>
							<div class="float-left buttons ui-widget-content ui-corner-all">
								<span class="button float-right link-border" id="leave_history_view">View History</span>
								<span class="button float-right link-border" id="leave_history_print"  
								style="position: relative; top:4px;cursor: pointer;font-weight: bold;">Print History</span>
							</div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
							
								<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.cancel"/></div>
								<div class="portlet-header ui-widget-header float-right reset" id="reset">Reset</div>
								<div class="portlet-header ui-widget-header float-right save" id="job_save">Apply</div>
							</div>
						</fieldset>
				 </div> 
				  <div class="width100 float-left">
					<fieldset>
							<legend>Leave Availability</legend>
							<div id="leaveAvailabilityDiv">
								<div>Select Employee,Leave Type & Leave period to get the leave availability</div>
							</div>
					</fieldset>
				</div>
 			</div>
 		</form>
		</div>
	</div>	
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="job-common-result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>