<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tr class="rowidS" id="fieldrowS_${rowId}">
	<td id="lineIdS_${rowId}" style="display:none;">${rowId}</td>
															
	<td>
		<input type="text" class="width80 attendanceDate" readonly="readonly" name="attendanceDate" id="attendanceDate_${rowId}" value="${ATTENDANCE_DATE}" style="border:0px;"/>
	</td>
	<td>
		<select class="width90 swipeInOutType" id="swipeInOutType_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<option value="IN">IN</option>
			<option value="OUT">OUT</option>
			
		</select>
	</td>  
	<td>
		<input type="text" class="width80 timeInOut"  name="timeInOut" id="timeInOut_${rowId}" style="border:0px;"/>
	</td> 
	<td style="display:none;">
		<input type="hidden" id="swipeInOutId_${rowId}" style="border:0px;"/>
		<input type="hidden" id="swipeId_${rowId}" style="border:0px;"/>
	</td>
	<td style="width:0.01%;" class="opn_td" id="optionS_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>    
</tr>
<script type="text/javascript"> 
$(document).ready(function(){  
	$('.attendanceDate').datepick({});
	$('.timeInOut').timepicker({});
});
</script>