<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<label class="width40" for="leaveType">Leave Type<span class="mandatory">*</span></label>
<select id="leaveType" name="leaveType" class="width50 validate[required]">
	<option value="">--Select--</option>
	<c:forEach items="${JOB_LEAVE_LIST}" var="jobleave" varStatus="status">
		<option value="${jobleave.jobLeaveId}">${jobleave.leave.displayName}</option>
	</c:forEach>
</select>