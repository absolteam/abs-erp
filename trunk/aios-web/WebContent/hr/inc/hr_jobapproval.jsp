<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td{
	width:6%;
}
.width35{
	width:35%!important;
}
.width25{
	width:25%!important;
}
.width15{
	width:15%!important;
}
.margintop3{
	margin-top:3px;
}
#common-popup{
	overflow: hidden;
}
.width61{
	width:61% !important;
}
</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var allowanceLineDetail="";
var leaveLineDetail="";
var shiftLineDetail="";
var assignmentLineDetail="";
var commonRowId=0;
$(document).ready(function(){  
	

//Combination pop-up config
	
	//Last row indentify cal
	manupulateAllowanceLastRow();
	manupulateLeaveLastRow();
	manupulateShiftLastRow();
	//manupulateAssignmentLastRow();
	var jobId=Number($("#jobId").val());
	if(jobId!=0){
		editDataSelectCall();
		totalPaySum();
	}
	//Allowance onchange event
	$('.allowance').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id'));
			dublicatePayEntries(rowId);
			triggerAllowanceAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});
	
	$('.payPolicy').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id'));
			triggerAllowanceAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	$jquery("#JOBDetails").validationEngine('attach');
	

	$('.amount').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAllowanceAddRow(rowId);
			totalPaySum();
			return false;
		} 
		else{
			return false;
		}
	});	
	//Leave onchange events
	
	
	$('.days').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerLeaveAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	
	//Shift on change events
	$('.workingShiftId').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerShiftAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	
	$('.startDate').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerShiftAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	$('.endDate').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerShiftAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	//Assignment
	$('.effectiveStartDate').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAssignmentAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	
	$('.addrowsC').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabC>.rowidC:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_allowance_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabC tr:last').before(result);
					 if($(".tabC").height()>255)
						 $(".tabC").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidC').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdC_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowC').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
   	 $('.rowidC').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineIdC_'+rowId).html(i);
			 i=i+1; 
		 });   
	 	return false;
	 });
	
	$('.addrowsL').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabL>.rowidL:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_leave_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabL tr:last').before(result);
					 if($(".tabL").height()>255)
						 $(".tabL").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidL').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdL_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowL').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
    	 $(slidetab).remove();  
    	 var i=1;
 	 $('.rowidL').each(function(){   
 		 var rowId=getRowId($(this).attr('id')); 
 		 $('#lineIdL_'+rowId).html(i);
			 i=i+1; 
		 });   
	 	return false;
	 });
	
	
	$('.addrowsS').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_shift_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabS tr:last').before(result);
					 if($(".tabS").height()>255)
						 $(".tabS").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidS').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdS_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowS').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
	  	 $(slidetab).remove();  
	  	 var i=1;
		 $('.rowidS').each(function(){   
			 var rowId=getRowId($(this).attr('id')); 
			 $('#lineIdS_'+rowId).html(i);
				 i=i+1; 
			 });  
		 return false;
	 });
	//Assignment
	$('.addrowsA').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabA>.rowidA:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_assignment_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabA tr:last').before(result);
					 if($(".tabA").height()>255)
						 $(".tabA").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidA').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdA_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowA').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
	 	$(slidetab).remove();  
		 var i=1;
		 $('.rowidA').each(function(){   
			 var rowId=getRowId($(this).attr('id')); 
			 $('#lineIdA_'+rowId).html(i);
				 i=i+1; 
			 });  
		 return false;
	 });
	//Save & discard process
	$('.save').click(function(){ 
		if($jquery("#JOBDetails").validationEngine('validate')){  
			 
			var jobId=Number($('#jobId').val());
			var successMsg="";
			if(jobId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var jobNumber=$('#jobNumber').val();
			var jobName=$('#jobName').val();
			var swipeId=$('#swipeId').val();
		 
			var personId=$('#personId').val();
			var designationId=$('#designationId').val();
			var cmpDeptLocId=$('#cmpDeptLocId').val();
			var status=Number($('#status').val());
			allowanceLineDetail=getAllowanceLineDatas(); 
			leaveLineDetail=getLeaveLineDetails();
			shiftLineDetail=getShiftLineDetails();
			//assignmentLineDetail=getAssignmentLineDetails();
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		jobId:jobId,jobNumber:jobNumber,swipeId:swipeId,jobName:jobName,
			 		personId:personId,designationId:designationId,cmpDeptLocId:cmpDeptLocId,
			 		allowanceLineDetail:allowanceLineDetail,leaveLineDetail:leaveLineDetail,
			 		shiftLineDetail:shiftLineDetail,status:status,
			 		//assignmentLineDetail:assignmentLineDetail
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/job_list.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(successMsg).slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/job_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
		 return false;
	});
	

	   $('#designationpopup').click(function() { 
	       $('.ui-dialog-titlebar').remove();  
	       $('#common-popup').dialog('open');
	       var rowId=0; 
	          $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/common_designation_list.action",
	             data:{id:rowId},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){
	                  $('.common-result').html(result);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 }); 
	   
	
	//pop up process area
	//pop-up config
    $('.employees-popup').live('click',function(){
          $('.ui-dialog-titlebar').remove();  
          $('#job-common-popup').dialog('open');
          var rowId=getRowId($(this).attr('id')); 
             $.ajax({
                type:"POST",
                url:"<%=request.getContextPath()%>/get_employee_for_job.action",
                data:{id:rowId},
                async: false,
                dataType: "html",
                cache: false,
                success:function(result){
                     $('.job-common-result').html(result);
                     return false;
                },
                error:function(result){
                     $('.job-common-result').html(result);
                }
            });
             return false;
    }); 
    
    $('.locations-popup').live('click',function(){
        $('.ui-dialog-titlebar').remove();  
        $('#job-common-popup').dialog('open');
        var rowId=getRowId($(this).attr('id')); 
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/get_location_for_job.action",
              data:{id:rowId},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.job-common-result').html(result);
                   return false;
              },
              error:function(result){
                   $('.job-common-result').html(result);
              }
          });
           return false;
  }); 
    
    $('.personBank-popup').live('click',function(){
    	$('.error').hide();
    	 $('.ui-dialog-titlebar').remove();
        var rowId=getRowId($(this).attr('id')); 
        commonRowId=rowId;
    	var personId= Number($('#personId_'+rowId).val());
    	if(personId>0){
        $('#job-common-popup').dialog('open');
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/get_person_bank_list.action",
              data:{id:rowId,personId:personId},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.job-common-result').html(result);
                   return false;
              },
              error:function(result){
                   $('.job-common-result').html(result);
              }
          });
    	}else{
    		$('.error').html("Selete Employee to get Bank Accounts").show();
    	}
           return false;
  }); 
    
    $('.shiftType-popup').live('click',function(){
        $('.ui-dialog-titlebar').remove();  
        $('#job-common-popup').dialog('open');
        var rowId=getRowId($(this).attr('id')); 
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/get_workingshift_for_job.action",
              data:{id:rowId},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.job-common-result').html(result);
                   return false;
              },
              error:function(result){
                   $('.job-common-result').html(result);
              }
          });
           return false;
  }); 
    
    $('.leaves-popup').live('click',function(){
        $('.ui-dialog-titlebar').remove();  
        $('#job-common-popup').dialog('open');
        var rowId=getRowId($(this).attr('id')); 
        commonRowId=rowId;
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/get_active_leaves.action",
              data:{id:rowId},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.job-common-result').html(result);
                   return false;
              },
              error:function(result){
                   $('.job-common-result').html(result);
              }
          });
           return false;
  }); 

    $('#leave-common-close').live('click',function(){
    	 $('#job-common-popup').dialog('close');
    });
                                                 
    
     $('#job-common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:900,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});
     
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

     $('#desigination-close').live('click',function(){ 
     	$('#common-popup').dialog('close');
     });
     
     //Date process
     
     $('.startDate,.endDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
     
     $('.effectiveStartDate').datepick({});
});
function getAllowanceLineDatas(){
	allowanceLineDetail="";
	var jobPayrollElementIds=new Array();
	var amounts=new Array();
	var allowanceIds=new Array();
	var payPolicys=new Array();
	var payModes=new Array();
	var calculationTypeList=new Array();
	var countList=new Array();
	var dependentPayelementList=new Array();
	$('.rowidC').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var jobPayrollElementId=$('#jobPayrollElementId_'+rowId).val();
		var payPolicy=$('#payPolicy_'+rowId).val();
		var payMode=$('#payType_'+rowId).val();
		var calculationType=$('#calculationType_'+rowId).val();
		var count=$('#count_'+rowId).val();
		var dependentPayment=$('#dependentElement_'+rowId).val();
		if(jobPayrollElementId==null || jobPayrollElementId=='' || jobPayrollElementId==0 || jobPayrollElementId=='undefined')
			jobPayrollElementId=-1;
		var allowance=$('#allowance_'+rowId).val();
		
		if(payPolicy==null || payPolicy=='' || payPolicy==0 || payPolicy=='undefined')
			payPolicy=-1;
		
		if(payMode==null || payMode=='' || payMode==0 || payMode=='undefined')
			payMode=-1;
		
		if(calculationType==null || calculationType=='' || calculationType==0 || calculationType=='undefined')
			calculationType=-1;
		
		if(count==null || count=='' || count==0 || count=='undefined')
			count=-1;
		
		if(dependentPayment==null || dependentPayment=='' || dependentPayment==0 || dependentPayment=='undefined')
			dependentPayment=-1;
		
		var amount=$('#amount_'+rowId).val();
		if(typeof allowance != 'undefined' && allowance!=null && allowance!="" && amount!=null && amount!=""){
			jobPayrollElementIds.push(jobPayrollElementId);
			allowanceIds.push(allowance);
			amounts.push(amount);
			payPolicys.push(payPolicy);
			payModes.push(payMode);
			calculationTypeList.push(calculationType);
			countList.push(count);
			dependentPayelementList.push(dependentPayment);
		}
	});
	for(var j=0;j<jobPayrollElementIds.length;j++){ 
		allowanceLineDetail+=jobPayrollElementIds[j]+"@"+allowanceIds[j]+"@"+amounts[j]+"@"+payPolicys[j]+"@"+payModes[j]+"@"+calculationTypeList[j]
		+"@"+countList[j]+"@"+dependentPayelementList[j];
		if(j==jobPayrollElementIds.length-1){   
		} 
		else{
			allowanceLineDetail+="##";
		}
	} 
	return allowanceLineDetail;
}



function getLeaveLineDetails(){
	leaveLineDetail="";
	var jobLeaveIds=new Array();
	var leaveIds=new Array();
	var days=new Array();
	$('.rowidL').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var jobLeaveId=$('#jobLeaveId_'+rowId).val();
		if(jobLeaveId==null || jobLeaveId=='' || jobLeaveId==0 || jobLeaveId=='undefined')
			jobLeaveId=-1;
		var leaveId=$('#leaveId_'+rowId).val().split('#');
		var noofdays=$('#days_'+rowId).val();
		if(typeof leaveId != 'undefined' && leaveId!=null && leaveId!="" && noofdays!=null && noofdays!=""){
			jobLeaveIds.push(jobLeaveId);
			leaveIds.push(leaveId);
			days.push(noofdays);
		}
	});
	for(var j=0;j<jobLeaveIds.length;j++){ 
		leaveLineDetail+=jobLeaveIds[j]+"@"+leaveIds[j]+"@"+days[j];
		if(j==jobLeaveIds.length-1){   
		} 
		else{
			leaveLineDetail+="##";
		}
	} 
	return leaveLineDetail;
}

function getShiftLineDetails(){
	shiftLineDetail="";
	var jobShiftIds=new Array();
	var shiftIds=new Array();
	var startDates=new Array();
	var endDates=new Array();

	$('.rowidS').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var jobShiftId=$('#jobShiftId_'+rowId).val();
		if(jobShiftId==null || jobShiftId=='' || jobShiftId==0 || jobShiftId=='undefined')
			jobShiftId=-1;
		var shiftId=$('#workingShiftId_'+rowId).val();
		var startDate=$('#startDate_'+rowId).val();
		var endDate=$('#endDate_'+rowId).val();
		if(startDate==null || startDate=="")
			startDate="-1";
		if(endDate==null || endDate=="")
			endDate="-1";
		if(typeof shiftId != 'undefined' && shiftId!=null && shiftId!=""){
			jobShiftIds.push(jobShiftId);
			shiftIds.push(shiftId);
			startDates.push(startDate);
			endDates.push(endDate);
			
		}
	});
	for(var j=0;j<jobShiftIds.length;j++){ 
		shiftLineDetail+=jobShiftIds[j]+"@"+shiftIds[j]+"@"+startDates[j]+"@"+endDates[j];
		if(j==jobShiftIds.length-1){   
		} 
		else{
			shiftLineDetail+="##";
		}
	} 
	return shiftLineDetail;
}
/* function getAssignmentLineDetails(){
	assignmentLineDetail="";
	var jobAssignmentIds=new Array();
	var personIds=new Array();
	var designationIds=new Array();
	var cmpDeptLocIds=new Array();
	var assignmentNumber=new Array();
	var swipeIds=new Array();
	var accountNumbers=new Array();
	var routingCodes=new Array();
	var isLocalPays=new Array();
	var payModes=new Array();
	$('.rowidA').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var jobAssignmentId=$('#jobAssignmentId_'+rowId).val();
		if(jobAssignmentId==null || jobAssignmentId=='' || jobAssignmentId==0 || jobAssignmentId=='undefined')
			jobAssignmentId=-1;
		var personId=$('#personId_'+rowId).val();
		var designationId=$('#designationId_'+rowId).val();
		var cmpDeptLocId=$('#cmpDeptLocId_'+rowId).val();
		var jobAssignmentNumber=$('#jobAssignmentNumber_'+rowId).val();
		if(jobAssignmentNumber==null || jobAssignmentNumber=='' || jobAssignmentNumber==0 || jobAssignmentNumber=='undefined')
			jobAssignmentNumber=-1;
		var swipeId=$('#swipeId_'+rowId).val();
		if(swipeId==null || swipeId=='' || swipeId==0 || swipeId=='undefined')
			swipeId=-1;
		var accountNumber=$('#personBankId_'+rowId).val();
		if(accountNumber==null || accountNumber=='' || accountNumber==0 || accountNumber=='undefined')
			accountNumber=-1;
		
		var isLocalPay=$('#isLocalPay_'+rowId).attr("checked");
		
		var payMode=$('#payMode_'+rowId).val();
		if(payMode==null || payMode=='' || payMode==0 || payMode=='undefined')
			payMode=-1;
		if(typeof personId != 'undefined' && personId!=null && personId!="" 
				&& designationId!=null && designationId!="" && cmpDeptLocId!=null && cmpDeptLocId!=""){
			jobAssignmentIds.push(jobAssignmentId);
			personIds.push(personId);
			designationIds.push(designationId);
			cmpDeptLocIds.push(cmpDeptLocId);
			swipeIds.push(swipeId);
			accountNumbers.push(accountNumber);
			isLocalPays.push(isLocalPay);
			payModes.push(payMode);
			assignmentNumber.push(jobAssignmentNumber);
		}
	});
	for(var j=0;j<personIds.length;j++){ 
		assignmentLineDetail+=jobAssignmentIds[j]+"@"+personIds[j]+"@"+designationIds[j]+"@"+cmpDeptLocIds[j]+"@"+swipeIds[j]
		+"@"+accountNumbers[j]+"@"+isLocalPays[j]+"@"+payModes[j]+"@"+assignmentNumber[j];
		if(j==personIds.length-1){   
		} 
		else{
			assignmentLineDetail+="##";
		}
	} 
	return assignmentLineDetail;
} */
function manupulateAllowanceLastRow(){
	var hiddenSize=0;
	$($(".tabC>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabC>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabC>tr:last').removeAttr('id');  
	$('.tabC>tr:last').removeClass('rowidC').addClass('lastrow');  
	$($('.tabC>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabC>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function manupulateLeaveLastRow(){
	var hiddenSize=0;
	$($(".tabL>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabL>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabL>tr:last').removeAttr('id');  
	$('.tabL>tr:last').removeClass('rowidL').addClass('lastrow');  
	$($('.tabL>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabL>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function manupulateShiftLastRow(){
	var hiddenSize=0;
	$($(".tabS>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabS>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabS>tr:last').removeAttr('id');  
	$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
	$($('.tabS>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
/* function manupulateAssignmentLastRow(){
	var hiddenSize=0;
	$($(".tabA>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabA>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabA>tr:last').removeAttr('id');  
	$('.tabA>tr:last').removeClass('rowidA').addClass('lastrow');  
	$($('.tabA>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabA>tr:last').append("<td style=\"height:25px;\"></td>");
	}
} */
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAllowanceAddRow(rowId){  
	var allowance=$('#allowance_'+rowId).val();  
	var amount=$('#amount_'+rowId).val();  
	var nexttab=$('#fieldrowC_'+rowId).next(); 
	if(allowance!=null && allowance!=""
			&& amount!=null && amount!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageC_'+rowId).show();
		$('.addrowsC').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function triggerLeaveAddRow(rowId){  
	var leaveType=$('#leaveType_'+rowId).val();  
	var days=$('#days_'+rowId).val();  
	var nexttab=$('#fieldrowL_'+rowId).next(); 
	if(leaveType!=null && leaveType!=""
			&& days!=null && days!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageL_'+rowId).show();
		$('.addrowsL').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function triggerShiftAddRow(rowId){ 
	var shiftType=$('#workingShiftId_'+rowId).val();  
	var startDate=$('#startDate_'+rowId).val();
	var endDate=$('#endDate_'+rowId).val();
	var nexttab=$('#fieldrowS_'+rowId).next(); 
	if(shiftType!=null && shiftType!=""
			&& startDate!=null && startDate!="" && endDate!=null && endDate!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageS_'+rowId).show();
		$('.addrowsS').trigger('click');
		return false;
	}else{
		return false;
	} 
}


function customRanges(dates) {
	var rowId=getRowId(this.id); 
	if (this.id == 'startDate_'+rowId) {
		$('#endDate_'+rowId).datepick('option', 'minDate', dates[0] || null);  
		if($(this).val()!=""){
			triggerShiftAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	}
	else{
		$('#startDate_'+rowId).datepick('option', 'maxDate', dates[0] || null); 
		if($(this).val()!=""){
			triggerShiftAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	} 
}

function editDataSelectCall(){
	$("#status").val($("#statusTemp").val());
	$('.rowidC').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#allowance_"+rowId).val($("#allowanceId_"+rowId).val());
		$("#payPolicy_"+rowId).val($("#payPolicyId_"+rowId).val());
		$("#payType_"+rowId).val($("#payTypeId_"+rowId).val());
		$("#calculationType_"+rowId).val($("#calculationTypeId_"+rowId).val());
		$("#dependentElement_"+rowId).val($("#dependentElementId_"+rowId).val());
	});
	
	
	
	$('.rowidS').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#shiftType_"+rowId).val($("#workingShiftId_"+rowId).val());
		
	});
	
	/* $('.rowidA').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#payMode_"+rowId).val($("#payModeTemp_"+rowId).val());
		
	}); */
	
	$('.leaveType').each(function(){
		var rowId=getRowId($(this).attr('id'));
		if($("#isMonthly_"+rowId).html().trim()=="true")
			$("#isMonthly_"+rowId).html("YES");
		else
			$("#isMonthly_"+rowId).html("NO");
		
		if($("#isYearly_"+rowId).html().trim()=="true")
			$("#isYearly_"+rowId).html("YES");
		else
			$("#isYearly_"+rowId).html("NO");
		
		if($("#isService_"+rowId).html().trim()=="true")
			$("#isService_"+rowId).html("YES");
		else
			$("#isService_"+rowId).html("NO");
		
	});
}
function dublicatePayEntries(rId){
	var arrPay = [];
	$('.allowance').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var allowance=$('#allowance_'+rowId).val();
		arrPay.push(allowance);
	});
	var sorted_arr = arrPay.sort(); // You can define the comparing function here. 
	for (var i = 0; i < arrPay.length - 1; i++) {
	    if (sorted_arr[i + 1] == sorted_arr[i]) {
	    	$('#allowance_'+rId).val("");
	        alert("Sorry! Duplicate Payroll Element");
	    	return false;
	    }
	}
}
function dublicateLeaveEntries(rId){
	var arrPay = [];
	$('.leaveType').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var allowance=$('#leaveType_'+rowId).val();
		arrPay.push(allowance);
	});
	var sorted_arr = arrPay.sort(); // You can define the comparing function here. 
	                             // JS by default uses a crappy string compare.
	for (var i = 0; i < arrPay.length - 1; i++) {
	    if (sorted_arr[i + 1] == sorted_arr[i]) {
	    	$('#leaveType_'+rId).val("");
	        alert("Sorry! Duplicate Leave");
	    	return false;
	    }
	}
}
/* function dublicateAssignmentEntries(rId){
	var arrPay = [];
	$('.personId').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var personId=$('#personId_'+rowId).val();
		arrPay.push(personId);
	});
	var sorted_arr = arrPay.sort(); // You can define the comparing function here. 
	                             // JS by default uses a crappy string compare.
	for (var i = 0; i < arrPay.length - 1; i++) {
	    if (sorted_arr[i + 1] == sorted_arr[i]) {
	    	$('#personId_'+rId).val("");
	        alert("Sorry! Duplicate Employee");
	    	return false;
	    }
	}
} */
function totalPaySum(){
	var totalPay=Number(0);
	$('.amount').each(function(){
		var rowId=getRowId($(this).attr('id'));
		totalPay+=convertToDouble($('#amount_'+rowId).val());
	});
	$('#totalPay').val(totalPay);
}
function personBankPopupCall(id,name,commaseparatedValue){
	$('#personBank_'+commonRowId).val(name);
	$('#personBankId_'+commonRowId).val(id);
}
function leavePopupResult(id,name,commaseparatedValue){
	$('#leaveType_'+commonRowId).val(name);
	$('#leaveId_'+commonRowId).val(id);
	var idval=commaseparatedValue.split('@#');
	$('#days_'+commonRowId).val(idval[1]);
	$('#isMonthly_'+commonRowId).val(idval[2]);
	$('#isYearly_'+commonRowId).val(idval[3]);
	$('#isService_'+commonRowId).val(idval[4]);
	triggerLeaveAddRow(commonRowId);
}
function designationPopupResult(id,name,commaseparatedValue){
	$('#designationId').val(id);
	var valueArray=new Array();
	valueArray=commaseparatedValue.split('@&');
	$('#designationName').val(valueArray[0]);
	$('#gradeName').val(valueArray[1]);
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Job Template Information</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="JOBDetails" class="" name="JOBDetails" method="post" style="position: relative;">
		  	<input type="hidden" name="jobId" id="jobId" value="${JOB.jobId}" class="width50"/>
		  	<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="height: 100px;">
							<legend>Template Information</legend>
							<div class="width50 float-left">
								<div class="width100 float-left">
									<label class="width30" for="jobNumber">Job Number<span
										class="mandatory">*</span>
									</label> <input type="text" maxlength="100" readonly="readonly"
										name="jobNumber" id="jobNumber" value="${JOB.jobNumber}"
										class="width60 validate[required]" />
								</div>
								<div class="width100 float-left">
									<label class="width30" for="jobName">Template Name<span
										class="mandatory">*</span>
									</label> <input type="text" maxlength="100" name="jobName" id="jobName"
										value="${JOB.jobName}" class="width60 validate[required]" />
								</div>
							</div>
							<div class="width50 float-left">

								<%-- <div class="width90 float-left">
										<label class="width40" for="effectiveStartDate">Effective Start Date<span class="mandatory">*</span></label>
										<c:choose>
											<c:when test="${JOB.effectiveStartDate ne null &&  JOB.effectiveStartDate  ne ''}">
												<c:set var="effectiveStartDate" value="${JOB.effectiveStartDate}"/>  
												<%String effectiveStartDate = DateFormat.convertDateToString(pageContext.getAttribute("effectiveStartDate").toString());%>
												<input type="text" readonly="readonly" name="effectiveStartDate" id="effectiveStartDate" tabindex="1" class="width50 validate[required]" 
												value="<%=effectiveStartDate%>"/>
											</c:when>
											<c:otherwise>
												<input type="text" readonly="readonly" name="effectiveStartDate" id="effectiveStartDate" tabindex="1" class="width50 validate[required]"/>
											</c:otherwise>
										</c:choose> 
									</div>   
									<div class="width90 float-left">
										<label class="width40" for="effectiveEndDate">Effective End Date</label>
										<c:choose>
											<c:when test="${JOB.effectiveEndDate ne null &&  JOB.effectiveEndDate ne ''}">
												<c:set var="effectiveEndDate" value="${JOB.effectiveEndDate}"/>  
												<%String effectiveEndDate = DateFormat.convertDateToString(pageContext.getAttribute("effectiveEndDate").toString());%>
												<input type="text" readonly="readonly" name="effectiveEndDate" id="effectiveEndDate" tabindex="3" class="width50" 
												value="<%=effectiveEndDate%>"/>
											</c:when>
											<c:otherwise>
												<input type="text" readonly="readonly" name="effectiveEndDate" id="effectiveEndDate" tabindex="4" class="width50"/>
											</c:otherwise>
										</c:choose> 
									</div> 
									 --%>
								<div class="width100 float-left">
									<label class="width40">Designation</label> <input type="text"
										readonly="readonly" id="designationName"
										class="masterTooltip width50 "
										value="${JOB.designation.designationName}" /> <span class="button"
										id="designation" style="position: relative;">
										<a style="cursor: pointer;" id="designationpopup"
										class="btn ui-state-default ui-corner-all width100"> <span
											class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" name="designationId" id="designationId"
										value="${JOB.designation.designationId}" />
								</div>
								<div class="width100 float-left">
									<label class="width40">Grade</label> <input type="text"
										readonly="readonly" id=gradeName
										class="masterTooltip width50"
										value="${JOB.designation.grade.gradeName}" /> 
								</div>
								<div class="width100 float-left">
									<label class="width40"><fmt:message key="hr.job.status" />
									</label> <input type="hidden" name="statusTemp" id="statusTemp"
										value="${JOB.status}" /> <select tabindex="10" name="status"
										id="status" class="width51">
										<option value="1">Active</option>
										<option value="2">Inactive</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
 		</form>
			 <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
			 
				<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
					<%-- <li class="ui-state-default ui-corner-top ui-tabs-selected"><a href="#tabs-1"><fmt:message key="hr.job.assignemployees"/></a></li> --%>
					<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-2">Pay Elements</a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected"><a href="#tabs-3"><fmt:message key="hr.job.leaveconfiguration"/></a></li>
					<li class="ui-state-default ui-corner-top ui-tabs-selected"><a href="#tabs-4"><fmt:message key="hr.job.workingshift"/></a></li>
				</ul>
				
			
	<!-- **********************************Job Assignment tab-1********************************************************************************* -->
		<%-- 	<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1">
					<div id="hrm" class="hastable width100"  >  
						<table id="hastab3" class="width100"> 
							<thead>
								<tr> 
									<th style="width:10%"><fmt:message key="hr.attendance.employee"/></th> 
								    <th style="width:10%"><fmt:message key="hr.attendance.job"/></th>  
								    <th style="width:10%"><fmt:message key="hr.attendance.location"/></th>
								    <th style="width:10%">Assignment Number</th>
								    <th style="width:5%"><fmt:message key="hr.attendance.swipeid"/></th>
								    <th style="width:5%"><fmt:message key="hr.job.paymode"/></th>
								    <th style="width:5%"><fmt:message key="hr.job.bank"/></th>
								    <th style="width:1%;" ><fmt:message key="hr.job.isLocalCurrency"/></th>
								    <th style="width:2%;" ><fmt:message key="hr.job.weekendApplicable"/></th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabA">
										<c:choose>
											<c:when test="${JOB.jobAssignments ne null && JOB.jobAssignments ne '' && fn:length(JOB.jobAssignments)>0}">
												<c:forEach var="jobAssignment" items="${JOB.jobAssignments}" varStatus="status1">
													<tr class="rowidA" id="fieldrowA_${status1.index+1}">
														<td id="lineIdA_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<span id="employeeName_${status1.index+1}" class="float-left width20">
															${jobAssignment.person.firstName} ${jobAssignment.person.lastName}
															</span>
															<span class="button float-right" id="employee_${status1.index+1}"  style="position: relative; top:4px;">
																<a style="cursor: pointer;" id="employeepopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 employees-popup"> 
																	<span class="ui-icon ui-icon-newwin">
																	</span> 
																</a>
															</span> 
															<input type="hidden" id="personId_${status1.index+1}" value="${jobAssignment.person.personId}" class="personId" />
														</td> 
														<td>
															<span id="designationName_${status1.index+1}" class="float-left designation">${jobAssignment.designation.designationName}</span>
															<span class="button float-right" id="designation_${status1.index+1}"  style="position: relative; top:6px;">
																<a style="cursor: pointer;" id="designationpopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 designations-popup"> 
																	<span class="ui-icon ui-icon-newwin"> 
																	</span> 
																</a>
															</span> 
															<input type="hidden" name="designationId_${status1.index+1}" id="designationId_${status1.index+1}" class="designationId" value="${jobAssignment.designation.designationId}"/>  
														</td>
														<td>
															<span id="locationName_${status1.index+1}" class="float-left location">
																${jobAssignment.cmpDeptLocation.company.companyName} >> ${jobAssignment.cmpDeptLocation.department.departmentName}
																 >> ${jobAssignment.cmpDeptLocation.location.locationName}
															</span>
															<span class="button float-right" id="location_${status1.index+1}"  style="position: relative; top:6px; ">
																<a style="cursor: pointer;" id="locationpopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 locations-popup"> 
																	<span class="ui-icon ui-icon-newwin"> 
																	</span> 
																</a>
															</span> 
															<input type="hidden" name="cmpDeptLocId_${status1.index+1}" id="cmpDeptLocId_${status1.index+1}"
																class="cmpDeptLocId"  value="${jobAssignment.cmpDeptLocation.cmpDeptLocId}" />  
														</td> 
														<td>
															<input type="text" readonly="readonly" class="width90 jobAssignmentNumber" 
																name="jobAssignmentNumber" id="jobAssignmentNumber_${status1.index+1}" value="${jobAssignment.jobAssignmentNumber}"/>
														</td> 
														<td>
															<input type="text" class="width90 swipeId" name="swipeId" id="swipeId_${status1.index+1}" title="${jobAssignment.swipeId}" value="${jobAssignment.swipeId}" />
														</td> 
														<td>
														
															<select class="width90 payMode" id="payMode_${status1.index+1}">
																<option value="OTHER">OTHER</option>
																<option value="WPS">WPS</option>
																<option value="BANK">BANK</option>
																<option value="EXCHANGE">EXCHANGE</option>
																<option value="CASH">CASH</option>
															</select>
															<input type="hidden" id="payModeTemp_${status1.index+1}" value="${jobAssignment.payMode}"/>
														</td>
														<td>
															<c:choose>
										                      	<c:when test="${jobAssignment.personBank ne null && jobAssignment.personBank ne ''}">
											                      	<input type="text" class="masterTooltip width80" title="" readonly="readonly" name="personBank" id="personBank_${status1.index+1}" 
											                      	value="${jobAssignment.personBank.bankName}[${jobAssignment.personBank.accountNumber}]"/>
											                     	<input type="hidden" name="personBankId" id="personBankId_${status1.index+1}" value="${jobAssignment.personBank.personBankId}"/> 
											                      	
										                      	</c:when>
										                      	<c:otherwise>
										                      		<input type="text" class="masterTooltip width80" title="" readonly="readonly" name="personBank" id="personBank_${status1.index+1}" value=""/>
										                      		<input type="hidden" name="personBankId" id="personBankId_${status1.index+1}" value=""/> 
										                      	</c:otherwise>
											                 </c:choose>
									                      	<span class="button" style="position: relative;top:6px; ">
																<a style="cursor: pointer;" id="personBankCall_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 personBank-popup"> 
																	<span class="ui-icon ui-icon-newwin"> 
																	</span> 
																</a>
															</span>
														</td>
														<td>
															 <c:choose>
															 	<c:when test="${jobAssignment.isLocalPay eq true}">
																	<input type="checkbox" class="width20" checked="checked" id="isLocalPay_${status1.index+1}" />
																</c:when>
																<c:otherwise>
																	<input type="checkbox" class="width20" id="isLocalPay_${status1.index+1}" />
																</c:otherwise>
															</c:choose> 
														</td> 
														<td style="display:none;">
															<input type="hidden" id="jobAssignmentId_${status1.index+1}" value="${jobAssignment.jobAssignmentId}" />
														</td>
														 <td style="width:0.01%;" class="opn_td" id="optionA_${status1.index+1}">
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataA" id="AddImageA_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
									 								<span class="ui-icon ui-icon-plus"></span>
															  </a>	
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataA" id="EditImageA_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																	<span class="ui-icon ui-icon-wrench"></span>
															  </a> 
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowA" id="DeleteImageA_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span>
															  </a>
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageA_${status1.index+1}" style="display:none;" title="Working">
																	<span class="processing"></span>
															  </a>
														</td>
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										<c:forEach var="i" begin="${fn:length(JOB.jobAssignments)+1}" end="${fn:length(JOB.jobAssignments)+2}" step="1" varStatus="status"> 
											<tr class="rowidA" id="fieldrowA_${i}">
												<td id="lineIdA_${i}" style="display: none;">${i}</td>
												<td>
													<span  id="employeeName_${i}" class="float-left width20">
													</span>
													<span class="button float-right" id="employee_${i}"  style="position: relative; top:6px;">
														<a style="cursor: pointer;" id="employeepopup_${i}" class="btn ui-state-default ui-corner-all width100 employees-popup"> 
															<span class="ui-icon ui-icon-newwin">
															</span> 
														</a>
													</span> 
													<input type="hidden" id="personId_${i}" value="" class="personId"  style="border:0px;"/>
												</td> 
												<td>
													<span  id="designationName_${i}" class="float-left designation">
													</span>
													<span class="button float-right" id="designation_${i}"  style="position: relative; top:6px;">
														<a style="cursor: pointer;" id="designationpopup_${i}" class="btn ui-state-default ui-corner-all width100 designations-popup"> 
															<span class="ui-icon ui-icon-newwin"> 
															</span> 
														</a>
													</span> 
													<input type="hidden" name="designationId_${i}" id="designationId_${i}" 
														class="designationId" value=""/>  
												</td>
												<td>
													<span id="locationName_${i}" class="float-left location">
													</span>
													<span class="button float-right" id="location_${i}"  style="position: relative; top:6px;">
														<a style="cursor: pointer;" id="locationpopup_${i}" class="btn ui-state-default ui-corner-all width100 locations-popup"> 
															<span class="ui-icon ui-icon-newwin"> 
															</span> 
														</a>
													</span> 
													<input type="hidden" name="cmpDeptLocId_${i}" id="cmpDeptLocId_${i}"
														class="cmpDeptLocId" value=""/>  
												</td> 
												<td>
													<input type="text" readonly="readonly" class="width90 jobAssignmentNumber" name="jobAssignmentNumber" id="jobAssignmentNumber_${i}" value=""/>
												</td> 
												<td>
													<input type="text" class="width90 swipeId" name="swipeId" id="swipeId_${i}" value=""/>
												</td> 
												<td>
													<select class="width90 payMode" id="payMode_${i}">
														<option value="OTHER">OTHER</option>
														<option value="WPS">WPS</option>
														<option value="BANK">BANK</option>
														<option value="EXCHANGE">EXCHANGE</option>
														<option value="CASH">CASH</option>
													</select>
													<input type="hidden" id="payMode_${i}" value=""/>
												</td>
												<td>
								                    <input type="text" class="masterTooltip width80" title="" readonly="readonly" name="personBank" id="personBank_${i}" value=""/>
								                    <input type="hidden" name="personBankId" id="personBankId_${i}" value=""/> 
							                      	<span class="button float-right" style="position: relative;top:6px; ">
														<a style="cursor: pointer;" id="personBankCall_${i}" class="btn ui-state-default ui-corner-all width100 personBank-popup"> 
															<span class="ui-icon ui-icon-newwin"> 
															</span> 
														</a>
													</span>
												</td>
												<td>
													<input type="checkbox" class="width20" id="isLocalPay_${i}" checked="checked"/>
												</td>  
												<td style="display:none;">
													<input type="hidden" id="jobAssignmentId_${i}" value="" style="border:0px;"/>
												</td>
												 <td style="width:0.01%;" class="opn_td" id="optionA_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataA" id="AddImageA_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataA" id="EditImageA_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowA" id="DeleteImageA_${i}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageA_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>
											</tr>
									</c:forEach>
							 </tbody>
					</table>
				</div> 
			</div> --%>
		<!-- **********************************Job Allowance tab-2********************************************************************************* -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2">
					<div id="hrm" class="hastable width100"  >  
						<table id="hastab1" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.allowance"/></th> 
								    <th style="width:5%">Pay Policy</th>  
								    <th style="width:5%">Pay Mode</th>
								    <th style="width:5%"><fmt:message key="hr.job.amount"/>(Max)</th>
								    <th style="width:5%">Calculation</th>
								    <th style="width:5%">Count</th>
								    <th style="width:5%">Dependent Element</th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabC">
										<c:choose>
											<c:when test="${JOB.jobPayrollElements ne null && JOB.jobPayrollElements ne '' && fn:length(JOB.jobPayrollElements)>0}">
												<c:forEach var="jobpay" items="${JOB.jobPayrollElements}" varStatus="status1">
													<tr class="rowidC" id="fieldrowC_${status1.index+1}">
														<td id="lineIdC_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<select class="width90 allowance" id="allowance_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
																	<option value="${element.payrollElementId}">${element.elementName}[${element.elementType}]</option>
																</c:forEach>
															</select>
															<input type="hidden" id="allowanceId_${status1.index+1}" value="${jobpay.payrollElementByPayrollElementId.payrollElementId}" style="border:0px;"/>
														</td> 
														<td>
															<select class="width90 payPolicy" id="payPolicy_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
																	<option value="${policy.key}">${policy.value}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="payPolicyId_${status1.index+1}" value="${jobpay.payPolicy}" style="border:0px;"/>
															
														</td> 
														<td>
															<select class="width90 payType" id="payType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="mode" items="${PAY_MODE}" varStatus="statusMode">
																	<option value="${mode.key}">${mode.value}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="payTypeId_${status1.index+1}" value="${jobpay.payType}" style="border:0px;"/>
														</td> 
														<td>
															<input type="text" class="width80 amount" id="amount_${status1.index+1}" value="${jobpay.amount}" style="border:0px;text-align: right;"/>
														</td> 
														
														<td>
															<select class="width90 calculationType" id="calculationType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="mode" items="${CALCULATION_TYPE}" varStatus="statusMode">
																	<option value="${mode.key}">${mode.value}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="calculationTypeId_${status1.index+1}" value="${jobpay.calculationType}" style="border:0px;"/>
														</td> 
														<td>
															<input type="text" class="width80 count" id="count_${status1.index+1}" value="${jobpay.count}" style="border:0px;text-align: right;"/>
														</td>
														<td>
															<select class="width90 dependentElement" id="dependentElement_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
																	<option value="${element.payrollElementId}">${element.elementName}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="dependentElementId_${status1.index+1}" value="${jobpay.payrollElementByPercentageElement.payrollElementId}" style="border:0px;"/>
														</td> 
														<td style="display:none;">
															<input type="hidden" id="jobPayrollElementId_${status1.index+1}" value="${jobpay.jobPayrollElementId}" style="border:0px;"/>
														</td>
														 <td style="width:0.01%;" class="opn_td" id="optionC_${status1.index+1}">
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
									 								<span class="ui-icon ui-icon-plus"></span>
															  </a>	
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																	<span class="ui-icon ui-icon-wrench"></span>
															  </a> 
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span>
															  </a>
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${status1.index+1}" style="display:none;" title="Working">
																	<span class="processing"></span>
															  </a>
														</td>
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										<c:forEach var="i" begin="${fn:length(JOB.jobPayrollElements)+1}" end="${fn:length(JOB.jobPayrollElements)+2}" step="1" varStatus="status"> 
											<tr class="rowidC" id="fieldrowC_${i}">
												<td id="lineIdC_${i}" style="display:none;">${i}</td>
												<td>
													<select class="width90 allowance" id="allowance_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
															<option value="${element.payrollElementId}">${element.elementName}[${element.elementType}]</option>
														</c:forEach>
													</select>
												</td> 
												<td>
													<select class="width90 payPolicy" id="payPolicy_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
															<option value="${policy.key}">${policy.value}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="payPolicyId_${i}" style="border:0px;"/>
												</td> 
												<td>
													<select class="width90 payType" id="payType_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="mode" items="${PAY_MODE}" varStatus="statusMode">
															<option value="${mode.key}">${mode.value}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="payTypeId_${i}" style="border:0px;"/>
												</td> 
												<td>
													<input type="text" class="width80 amount" id="amount_${i}" style="border:0px;text-align: right;"/>
												</td> 
												
												<td>
													<select class="width90 calculationType" id="calculationType_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="mode" items="${CALCULATION_TYPE}" varStatus="statusMode">
															<option value="${mode.key}">${mode.value}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="calculationTypeId_${i}" value="" style="border:0px;"/>
												</td> 
												<td>
													<input type="text" class="width80 count" id="count_${i}" value="" style="border:0px;text-align: right;"/>
												</td>
												<td>
													<select class="width90 dependentElement" id="dependentElement_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
															<option value="${element.payrollElementId}">${element.elementName}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="dependentElementId_${i}" value="" style="border:0px;"/>
												</td> 
												<td style="display:none;">
													<input type="hidden" id="jobPayrollElementId_${i}" style="border:0px;"/>
												</td> 
												 <td style="width:0.01%;" class="opn_td" id="optionc_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${i}" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>    
											</tr>
									</c:forEach>
							 </tbody>
					</table>
					<div class="float-right width30" style="display: none;">
						<label class="float-left width30"><span><fmt:message key="hr.job.totalamount"/> :</span></label>
						<input type="text" readonly="readonly" id="totalPay" class="float-left width40"/>
					</div>
				</div> 
			</div>
	<!-- **********************************Job Leave tab-3********************************************************************************* -->
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3">
					<div id="hrm" class="hastable width100"  >  
						<table id="hastab2" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.leavetype"/></th> 
								    <th style="width:5%"><fmt:message key="hr.job.days"/></th> 
								    <th style="width:5%">Monthly</th> 
								    <th style="width:5%">Yearly</th>
								    <th style="width:5%">In Whole Service</th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabL">
										<c:choose>
											<c:when test="${JOB.jobLeaves ne null && JOB.jobLeaves ne '' && fn:length(JOB.jobLeaves)>0}">
												<c:forEach var="jobleave" items="${JOB.jobLeaves}" varStatus="status1">
													<tr class="rowidL" id="fieldrowL_${status1.index+1}">
														<td id="lineIdL_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<%-- <select class="width90 leaveType" id="leaveType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="leavetype" items="${LEAVE_TYPE_LIST}" varStatus="statusL">
																	<c:if test="${leavetype.leaveId eq jobleave.leave.leaveId}">
																		<option value="${leavetype.leaveId}#${leavetype.defaultDays}" selected="selected">${leavetype.leaveType}</option>
																	</c:if>
																	<c:if test="${leavetype.leaveId ne jobleave.leave.leaveId}">
																		<option value="${leavetype.leaveId}#${leavetype.defaultDays}">${leavetype.leaveType}</option>
																	</c:if>
																</c:forEach>
															</select> --%>
															<c:choose>
																<c:when test="${jobleave.leave ne null && jobleave.leave ne '' && jobleave.leave.lookupDetail ne null && jobleave.leave.lookupDetail.lookupDetailId ne null}">
																	<input type="text" readonly="readonly" id="leaveType_${status1.index+1}" value="${jobleave.leave.lookupDetail.displayName}" class="width70 float-left leaveType"/>
																</c:when>
																<c:otherwise>
																	<input type="text" readonly="readonly" id="leaveType_${status1.index+1}" class="width70 float-left leaveType"/>
																</c:otherwise>
															</c:choose>
															<span class="button float-right" id="leave_${status1.index+1}"  style="position: relative; top:6px;">
																<a style="cursor: pointer;" id="leavepopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 leaves-popup"> 
																	<span class="ui-icon ui-icon-newwin"> 
																	</span> 
																</a>
															</span> 
															<c:choose>
																<c:when test="${jobleave.leave ne null && jobleave.leave ne ''}">
																<input type="hidden" id="leaveId_${status1.index+1}" value="${jobleave.leave.leaveId}" style="border:0px;"/>
																</c:when>
																<c:otherwise>
																	<input type="hidden" id="leaveId_${status1.index+1}" value="" style="border:0px;"/>
																	
																</c:otherwise>
															</c:choose>
														</td> 
														<td>
															 <input type="text" readonly="readonly" class="width80 days" id="days_${status1.index+1}" value="${jobleave.days}" style="border:0px;"/>
														</td> 
														<c:choose>
															<c:when test="${jobleave.leave ne null && jobleave.leave ne ''}">
																<td id="isMonthly_${status1.index+1}">${jobleave.leave.isMonthly}</td>
																<td id="isYearly_${status1.index+1}">${jobleave.leave.isYearly}</td>
																<td id="isService_${status1.index+1}">${jobleave.leave.isService}</td>
															</c:when>
															<c:otherwise>
																<td id="isMonthly_${status1.index+1}"></td>
																<td id="isYearly_${status1.index+1}"></td>
																<td id="isService_${status1.index+1}"></td>
															</c:otherwise>
														</c:choose>
														<td style="display:none;">
															<input type="hidden" id="jobLeaveId_${status1.index+1}" value="${jobleave.jobLeaveId}" style="border:0px;"/>
														</td>
														 <td style="width:0.01%;" class="opn_td" id="optionL_${status1.index+1}">
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataL" id="AddImageL_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
									 								<span class="ui-icon ui-icon-plus"></span>
															  </a>	
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataL" id="EditImageL_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																	<span class="ui-icon ui-icon-wrench"></span>
															  </a> 
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowL" id="DeleteImageL_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span>
															  </a>
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageL_${status1.index+1}" style="display:none;" title="Working">
																	<span class="processing"></span>
															  </a>
														</td>
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										<c:forEach var="i" begin="${fn:length(JOB.jobLeaves)+1}" end="${fn:length(JOB.jobLeaves)+2}" step="1" varStatus="status"> 
											<tr class="rowidL" id="fieldrowL_${i}">
												<td id="lineIdL_${i}" style="display:none;">${i}</td>
												<td>
													<%-- <select class="width90 leaveType" id="leaveType_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="leavetype" items="${LEAVE_TYPE_LIST}" varStatus="statusL">
															<option value="${leavetype.leaveId}#${leavetype.defaultDays}">${leavetype.lookupDetail.displayName}</option>
														</c:forEach>
													</select> --%>
													<input type="text"  readonly="readonly" id="leaveType_${i}" value="" class="width70 float-left leaveType"/>
													<span class="button float-right" id="leave_${i}"  style="position: relative; top:6px;">
														<a style="cursor: pointer;" id="leavepopup_${i}" class="btn ui-state-default ui-corner-all width100 leaves-popup"> 
															<span class="ui-icon ui-icon-newwin"> 
															</span> 
														</a>
													</span> 
													<input type="hidden" id="leaveId_${i}" value="" style="border:0px;"/>
												</td> 
												<td>
													<input type="text" readonly="readonly" class="width70 days" id="days_${i}" style="border:0px;"/>
												</td> 
												<td id="isMonthly_${i}">
												</td>
												<td id="isYearly_${i}">
												</td>
												<td id="isService_${i}">
												</td>
												<td style="display:none;">
													<input type="hidden" id="jobLeaveId_${i}" style="border:0px;"/>
												</td> 
												 <td style="width:0.01%;" class="opn_td" id="optionL_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataL" id="AddImageL_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataL" id="EditImageL_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowL" id="DeleteImageL_${i}" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageL_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>    
											</tr>
									</c:forEach>
							 </tbody>
					</table>
				</div> 
			</div>	
		<!-- **********************************Job Shift tab-4********************************************************************************* -->
			
			<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4">
					<div id="hrm" class="hastable width100"  >  
						<table id="hastab2" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.shifttype"/></th> 
								    <th style="width:5%"><fmt:message key="hr.job.periodstartdate"/></th>  
								    <th style="width:5%"><fmt:message key="hr.job.periodenddate"/></th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabS">
										<c:choose>
											<c:when test="${JOB_SHIFT_LIST ne null && JOB_SHIFT_LIST ne '' && fn:length(JOB_SHIFT_LIST)>0}">
												<c:forEach var="jobShift" items="${JOB_SHIFT_LIST}" varStatus="status1">
													<tr class="rowidS" id="fieldrowS_${status1.index+1}">
														<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<%-- <select class="width90 shiftType" id="shiftType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="shifttyp" items="${SHIFT_TYPE_LIST}" varStatus="statusL">
																	<option value="${shifttyp.workingShiftId}">${shifttyp.workingShiftName}</option>
																</c:forEach>
															</select> --%>
															<span style="height: 20px;width:65%!important" id="shiftTypeName_${status1.index+1}" class="float-left width20">
															${jobShift.workingShift.workingShiftName}
															</span>
															<span class="button float-right" id="shiftType_${status1.index+1}"  style="position: relative; top:4px;">
																<a style="cursor: pointer;" id="shiftpopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 shiftType-popup"> 
																	<span class="ui-icon ui-icon-newwin">
																	</span> 
																</a>
															</span> 
															<input type="hidden" id="workingShiftId_${status1.index+1}" value="${jobShift.workingShift.workingShiftId}" class="workingShiftId"  style="border:0px;"/>
														</td> 
														<td>
															 <c:choose>
																	<c:when test="${jobShift.effectiveStartDate ne null &&  jobShift.effectiveStartDate ne ''}">
																		<c:set var="startDate" value="${jobShift.effectiveStartDate}"/>  
																		<%String startDate = DateFormat.convertDateToString(pageContext.getAttribute("startDate").toString());%>
																		<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${status1.index+1}" style="border:0px;" 
																		value="<%=startDate%>"/>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${status1.index+1}" style="border:0px;"/>
																	</c:otherwise>
															</c:choose> 
														</td> 
														<td>
															 <c:choose>
																	<c:when test="${jobShift.effectiveEndDate ne null &&  jobShift.effectiveEndDate ne ''}">
																		<c:set var="endDate" value="${jobShift.effectiveEndDate}"/>  
																		<%String endDate = DateFormat.convertDateToString(pageContext.getAttribute("endDate").toString());%>
																		<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${status1.index+1}" style="border:0px;" 
																		value="<%=endDate%>"/>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${status1.index+1}" style="border:0px;"/>
																	</c:otherwise>
															</c:choose> 
														</td> 
														<td style="display:none;">
															<input type="hidden" id="jobShiftId_${status1.index+1}" value="${jobShift.jobShiftId}" style="border:0px;"/>
														</td>
														 <td style="width:0.01%;" class="opn_td" id="optionS_${status1.index+1}">
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
									 								<span class="ui-icon ui-icon-plus"></span>
															  </a>	
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																	<span class="ui-icon ui-icon-wrench"></span>
															  </a> 
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span>
															  </a>
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${status1.index+1}" style="display:none;" title="Working">
																	<span class="processing"></span>
															  </a>
														</td>
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										<c:forEach var="i" begin="${fn:length(JOB_SHIFT_LIST)+1}" end="${fn:length(JOB_SHIFT_LIST)+2}" step="1" varStatus="status"> 
											<tr class="rowidS" id="fieldrowS_${i}">
												<td id="lineIdS_${i}" style="display:none;">${i}</td>
												<td>
													<%-- <select class="width90 shiftType" id="shiftType_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="shifttyp" items="${SHIFT_TYPE_LIST}" varStatus="statusL">
															<option value="${shifttyp.workingShiftId}">${shifttyp.workingShiftName}</option>
														</c:forEach>
													</select> --%>
													<span style="height: 20px;width:65%!important" id="shiftTypeName_${i}" class="float-left width20"></span>
													<span class="button float-right" id="shiftType_${i}"  style="position: relative; top:4px;">
															<a style="cursor: pointer;" id="shiftpopup_${i}" class="btn ui-state-default ui-corner-all width100 shiftType-popup"> 
																<span class="ui-icon ui-icon-newwin"> 
																</span> 
															</a>
													</span> 
													<input type="hidden" id="workingShiftId_${i}" class="workingShiftId" style="border:0px;"/>
												</td> 
												<td>
													<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${i}" style="border:0px;"/>
												</td> 
												<td>
													<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${i}" style="border:0px;"/>
												</td> 
												
												<td style="display:none;">
													<input type="hidden" id="jobShiftId_${i}" style="border:0px;"/>
												</td>
												 <td style="width:0.01%;" class="opn_td" id="optionS_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${i}" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>    
											</tr>
									</c:forEach>
							 </tbody>
					</table>
					<br>
					<div style="font-weight: bold;">Note: 'Period Start' and 'Period End' date's are to specify the special schedule from the regular schedule.</div>
				</div> 
			</div>
	
		</div>
		<div class="clearfix"></div>
	</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
 	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrowsC" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		<div class="portlet-header ui-widget-header float-left addrowsL" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
		<div class="portlet-header ui-widget-header float-left addrowsS" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
		<div class="portlet-header ui-widget-header float-left addrowsA" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
	</div>
	
</div>
	

