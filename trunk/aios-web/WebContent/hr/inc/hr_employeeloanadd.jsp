<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>
<style>
.container {float: left;padding: 1px;}
.container select{width: 35em;height:10em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 
</style> 

<script type="text/javascript"> 
var j = jQuery.noConflict();
var shiftLineDetail="";

$(document).ready(function(){ 

		var employeeLoanId=Number($("#employeeLoanId").val());
		if(employeeLoanId>0){
			getRepaymentSchdule(0);
			 
		}
		
		$('#repayment').click(function(){
			getRepaymentSchdule(0);
		});
		//pop-up config
		$('#employeepopups').click(function(){
		      $('.ui-dialog-titlebar').remove();  
		      $('#job-common-popup').dialog('open');
		         $.ajax({
		            type:"POST",
		            url:"<%=request.getContextPath()%>/get_employee_designation.action",
		            async: false,
		            dataType: "html",
		            cache: false,
		            success:function(result){
		                 $('.job-common-result').html(result);
		                 return false;
		            },
		            error:function(result){
		                 $('.job-common-result').html(result);
		            }
		        });
		        return false;
		}); 

		$('#job-common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});

		$jquery("#loan-details").validationEngine('attach');
				  
			
			$('#loan-discard').click(function(){ 
				returnCallTOList();
				
			 });

	 $('#loan-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(employeeLoanId>0)
						successMessage="Successfully Modified";

					try {
						if ($jquery("#loan-details").validationEngine('validate')) {
							if($("#jobAssignmentId").val()==null || $("#jobAssignmentId").val()=='' || Number($("#jobAssignmentId").val())==0){
								$('.error').hide().html("Please select employee.").slideDown();
								return false;
							}
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/employee_loan_save.action", 
							 	async: false,
							 	data: $("#loan-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									 if(result.trim()=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=result;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									$('.error').hide().html(result).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					} catch (e) {

					}

				});
					
			
			 
			 if($('#requestedDate').val()!=null && $('#requestedDate').val()!='')
			 	$('#requestedDate').datepick();
			 else
				 $('#requestedDate').datepick();
			 
			 $('#dueStartDate').datepick();
			 
			
			 
	});
	function getRepaymentSchdule(loanId){
		var sanctionedAmount=$("#sanctionedAmount").val();
		var dueStartDate=$("#dueStartDate").val();
		var paymentFrequency=$("#paymentFrequency").val();
		var numberOfRepayment=Number($("#numberOfRepayment").val());
		var intrest=Number($("#intrest").val());
		var fixedInterest=$("#fixedInterest").val();
		var interestPercentage=$("#interestPercentage").val();
		if ($jquery("#loan-details").validationEngine('validate')) {
		if(sanctionedAmount==null || sanctionedAmount=='0' || sanctionedAmount==''){
			$('.error').hide().html("Please enter amount").slideDown(1000);
			return false;
		}
		if(dueStartDate==null ||  dueStartDate==''){
			$('.error').hide().html("Please enter due start date").slideDown(1000);
			return false;
		}
		if(numberOfRepayment==null || numberOfRepayment==0){
			$('.error').hide().html("Please enter number of repayment").slideDown(1000);
			return false;
		}
		if(paymentFrequency==null || paymentFrequency==0){
			$('.error').hide().html("Please select payment frequency").slideDown(1000);
			return false;
		}
		if(intrest==null || intrest==''){
			$('.error').hide().html("Please select interest frequency").slideDown(1000);
			return false;
		}
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/employee_loan_repayment_schedule.action", 
				data:{
					employeeLoanId:loanId,
					sanctionedAmount:sanctionedAmount,
					dueStartDate:dueStartDate,
					numberOfRepayment:numberOfRepayment,
					paymentFrequency:paymentFrequency,
					intrest:intrest,
					fixedInterest:fixedInterest,
					interestPercentage:interestPercentage
					},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#repayment-result").html(result);  
				}
		 });
		}else{
			return false;
		}
	}
	function reload(){
		 $(".low input[type='button']").each(function(){
	    	    var arr = $(this).attr("name").split("2");
	    	    var from = arr[0];
	    	    var to = arr[1];
	    	    $("#" + from + " option:selected").each(function(){
	    	      $("#" + to).append($(this).clone());
	    	      $(this).remove();
	    	    });
	    }); 
	}
	function customRanges(dates) {
		var rowId=getRowId(this.id); 
		if (this.id == 'startDate_'+rowId) {
			$('#endDate_'+rowId).datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				triggerShiftAddRow(rowId);
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#startDate_'+rowId).datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				triggerShiftAddRow(rowId);
				return false;
			} 
			else{
				return false;
			}
		} 
	}

	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/employee_loan.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}
	
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Loan &
			Advance Details
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
				style="min-height: 99%;">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
				<div class="width98 float-left" id="hrm">
					<form id="loan-details" action="employee_loan_save.action" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width35 float-left">
								<input type="hidden" name="employeeLoan.employeeLoanId"
											id="employeeLoanId" value="${EMPLOYEE_LOAN.employeeLoanId}"
											class="width50 validate[required]" />
								<fieldset style="height: 200px;">
									<legend> Employee Information </legend>
									<div class="width100 float-left">
										<c:set var="jobAssignment"
											value="${EMPLOYEE_LOAN.jobAssignment}" />
										<input type="hidden" name="employeeLoan.jobAssignment.jobAssignmentId"
											id="jobAssignmentId" value="${jobAssignment.jobAssignmentId}"
											class="width50 validate[required]" />
										<div class="float-left width100">
											<label class="width30" style="padding-bottom: 6px;"><fmt:message
													key="hr.attendance.employee" /><span
											style="color: red">*</span> : </label> 
											<input type="text" readonly="readonly" id="employeeName"
												class="float-left width50 validate[required]"
												value="${jobAssignment.person.firstName}
												${jobAssignment.person.lastName}"/> <span
												class="button " id="employee"
												style="position: relative; top: 4px;"> <a
												style="cursor: pointer;" id="employeepopups"
												class="btn ui-state-default ui-corner-all width10 employee-popup">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" id="personId" value="" class="personId"
												style="border: 0px;" />
										</div>
										<div class="float-left width100">
											<label class="width30" style="padding-bottom: 6px;"><fmt:message
													key="hr.attendance.job" /> : </label> 
													<input type="text" readonly="readonly"
												class="width60" id="designationName" value="${jobAssignment.designation.designationName}"/>
										</div>
										<div class="float-left width100">
											<label class="width30" style="padding-bottom: 6px;"><fmt:message
													key="hr.attendance.grade" /> : </label> <input type="text" readonly="readonly"
												class="width60 " id="gradeName" value="${jobAssignment.designation.grade.gradeName}"/>
										</div>
										
										<div class="float-left width100">
											<label class="width30" style="padding-bottom: 6px;"><fmt:message
													key="hr.attendance.company" /> : </label> <input type="text" readonly="readonly"
												class="width60 " id="companyName" value="${jobAssignment.cmpDeptLocation.company.companyName}"/>
										</div>
										<div class="float-left width100">
											<label class="width30" style="padding-bottom: 6px;"><fmt:message
													key="hr.attendance.department" /> : </label> <input type="text" readonly="readonly"
												class="width60" id="departmentName" value="${jobAssignment.cmpDeptLocation.department.departmentName}"/>
										</div>
										<div class="float-left width100">
											<label class="width30" style="padding-bottom: 6px;"><fmt:message
													key="hr.attendance.location" /> : </label> <input type="text" readonly="readonly"
												class="width60" id="locationName" value="${jobAssignment.cmpDeptLocation.location.locationName}"/>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="width35 float-left">
								<fieldset style="height: 200px;">
									<legend>Loan Information</legend>
									<div>
										<label class="width30 ">Finance Type<span
											style="color: red">*</span>
										</label> 
										<input type="hidden" name="financeTypeTemp"
											id="financeTypeTemp" value="${EMPLOYEE_LOAN.financeType}" /> 
											<select id="financeType"
											name="employeeLoan.financeType"
											class="width60 validate[required] ">
											<c:forEach items="${FINANCE_TYPE_LIST}" var="ftl"
												varStatus="status">
												<c:choose>
													<c:when test="${EMPLOYEE_LOAN.financeType eq ftl.key}">
														<option value="${ftl.key}" selected="selected">${ftl.value}
															</option>
													</c:when>
													<c:otherwise>
														<option value="${ftl.key}">${ftl.value}
															</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div>
										<label class="width30 ">Repayement Type<span
											style="color: red">*</span>
										</label> 
										<input type="hidden" name="repaymentTypeTemp"
											id="repaymentTypeTemp" value="${EMPLOYEE_LOAN.repaymentType}" /> 
											<select  name="employeeLoan.repaymentType"
											id="repaymentType"
											class="width60 validate[required] ">
											<c:forEach items="${REPAYMENT_TYPE_LIST}" var="ftl" varStatus="status">
												<c:choose>
													<c:when test="${EMPLOYEE_LOAN.repaymentType eq ftl.key}">
														<option value="${ftl.key}" selected="selected">${ftl.value}
															</option>
													</c:when>
													<c:otherwise>
														<option value="${ftl.key}">${ftl.value}
															</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div>
										<label class="width30 ">Security<span
											style="color: red">*</span>
										</label> 
										<input type="hidden" name="securityTemp"
											id="securityTemp" value="${EMPLOYEE_LOAN.repaymentType}" /> 
											<select id="security" 
											name="employeeLoan.security"
											class="width60 validate[required] ">
											<c:forEach items="${SECURITY_TYPE_LIST}" var="ftl"
												varStatus="status">
												<c:choose>
													<c:when test="${EMPLOYEE_LOAN.repaymentType eq ftl.key}">
														<option value="${ftl.key}" selected="selected">${ftl.value}
															</option>
													</c:when>
													<c:otherwise>
														<option value="${ftl.key}">${ftl.value}
															</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div>
										<label class="width30 ">Payment Frequency<span
											style="color: red">*</span>
										</label> 
										<input type="hidden" name="securityTemp"
											id="paymentFrequencyTemp" value="${EMPLOYEE_LOAN.paymentFrequency}" /> 
											<select name="employeeLoan.paymentFrequency"
											id="paymentFrequency"
											class="width60 validate[required] ">
											<c:forEach items="${FREQUENCY_TYPE_LIST}" var="ftl"
												varStatus="status">
												<c:choose>
													<c:when test="${EMPLOYEE_LOAN.paymentFrequency eq ftl.code}">
														<option value="${ftl.code}" selected="selected">${ftl}
															</option>
													</c:when>
													<c:otherwise>
														<option value="${ftl.code}">${ftl}
															</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div>
										<label class="width30 ">Interest Type<span
											style="color: red">*</span>
										</label> 
										
											<select id="intrest"
											name="employeeLoan.intrest"
											class="width60 validate[required] ">
											<c:forEach items="${INTREST_TYPE_LIST}" var="ftl"
												varStatus="status">
												<c:choose>
													<c:when test="${EMPLOYEE_LOAN.repaymentType eq ftl.key}">
														<option value="${ftl.key}" selected="selected">${ftl.value}
															</option>
													</c:when>
													<c:otherwise>
														<option value="${ftl.key}">${ftl.value}
															</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div id="interest_method_div" class="width90 ">
										<label class="width30 float-left">Interest Method
										</label> 
										<div class="width70 float-left">
											<div class="width50 float-left">
												<label class="width40 float-left">Fixed($)</label> <input
													type="text" class="width50 float-left"
													name="employeeLoan.fixedInterest" id="fixedInterest"
													value="${EMPLOYEE_LOAN.fixedInterest}" />
											</div>
											<div class="width40 float-left">
												<label class="width40 float-left">/Per.(%)</label> <input
													type="text" class="width50 float-left"
													name="employeeLoan.interestPercentage"
													id="interestPercentage" value="${EMPLOYEE_LOAN.interestPercentage}" />
											</div>
										</div>
									</div>
									
								</fieldset>
							</div>

							<div class="width30 float-left">
								<fieldset style="height: 200px;">
									<legend>Loan Information</legend>
									<div>
										<label class="width30 ">Requested Date<span
											style="color: red">*</span></label>
										<input type="text" class="width60 validate[required] "
											name="requestedDate" id="requestedDate" 
											value="${EMPLOYEE_LOAN.requestedDateView}"/>
									</div>
									<div>
										<label class="width30 ">Amount<span
											style="color: red">*</span></label>
										<input type="text" class="width60 validate[required] "
											name="employeeLoan.sanctionedAmount" id="sanctionedAmount" 
											value="${EMPLOYEE_LOAN.sanctionedAmount}"/>
									</div>
									<div>
										<label class="width30 ">Due Start Date<span
											style="color: red">*</span></label>
										<input type="text" class="width60 validate[required] "
											name="dueStartDate" id="dueStartDate" 
											value="${EMPLOYEE_LOAN.dueStartDateView}"/>
									</div>
									<div>
										<label class="width30 ">No. of Settlments<span
											style="color: red">*</span></label>
										<input type="text" class="width60 validate[required] "
											name="employeeLoan.numberOfRepayment" id="numberOfRepayment" 
											value="${EMPLOYEE_LOAN.numberOfRepayment}"/>
									</div>
									<div id="repyament-button-div"
										class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
										
										<div
											class="portlet-header ui-widget-header float-right repayment"
											id="repayment">Get Repayment Schedule</div>
									</div>

								</fieldset>
							</div>
						</div>
						<div class="width100 float-left">
							<fieldset>
								<legend>RePayment Schedule</legend>
								<div id="hrm" class="hastable width100">
									<table id="hastab2" class="width100">
										<thead>
											<tr>
												<th style="width: 5%">Period</th>
												<th style="width: 5%">Due Date</th>
												<th style="width: 5%">Pricipal Start</th>
												<th style="width: 5%">Installment</th>
												<th style="width: 5%">Interest</th>
												<th style="width: 5%">Principal</th>
												<th style="width: 5%">Principal End</th>
												<th style="width: 5%">Status</th>
											</tr>
										</thead>
										<tbody class="tabS" id="repayment-result">
										
										</tbody>
									</table>
								</div>
							</fieldset>
						</div>
					</form>
					<div class="clearfix"></div>
					<div
						class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						<div
							class="portlet-header ui-widget-header float-right loan-discard"
							id="loan-discard">cancel</div>
						<div
							class="portlet-header ui-widget-header float-right loan-save"
							id="loan-save">save</div>
					</div>
				</div>

				<div class="clearfix"></div>
				<div id="sub-content" class="width100 float-left"></div>

				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span>
						</a>
					</div>
					<div id="job-common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="job-common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

				<div style="display: none;"
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
					<div class="portlet-header ui-widget-header float-left addrowsS"
						style="cursor: pointer;">
						<fmt:message key="accounts.common.button.addrow" />
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>