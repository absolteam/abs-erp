<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>

</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var allowanceLineDetail="";
var accessCode=null;
var commonRowId=0;
var rowId=null;
$(document).ready(function(){  
	
	//Last row indentify cal
	manupulateAllowanceLastRow();
	//manupulateAssignmentLastRow();
	var houseRentAllowanceId=Number($("#houseRentAllowanceId").val());
	if(houseRentAllowanceId!=0){
		$('#propertyName').val($('#propertyNameTemp').val());
		if($('#isFinanceImpactTemp').val()!=null && $('#isFinanceImpactTemp').val()=='true')
			$('#isFinanceImpact').attr("checked",true);
		else
			$('#isFinanceImpact').attr("checked",false);
	/* 	
		if($('#isFinanceImpact').attr("checked"))
			$('#reciptDiv').show();
		else
			$('#reciptDiv').hide(); */
			
		editDataSelectCall();
		totalPaySum();
	}
	
/* 	$('#isFinanceImpact').live('change',function(){
			if($('#isFinanceImpact').attr("checked"))
				$('#reciptDiv').show();
			else
				$('#reciptDiv').hide();
	});  
	 */
	
	$('#fromDate,#toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});		

	

	$('.amount').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAllowanceAddRow(rowId);
			totalPaySum();
			return false;
		} 
		else{
			return false;
		}
	});	
	//Leave onchange events
	$('#hra_discard').click(function(){
		listLoadCall();
		 return false;
	});
	

	
	$('.addrowsC').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabC>.rowidC:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/hra_payment_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabC tr:last').before(result);
					 if($(".tabC").height()>255)
						 $(".tabC").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidC').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdC_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowC').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
   	 $('.rowidC').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineIdC_'+rowId).html(i);
			 i=i+1; 
		 });   
	 	return false;
	 });
	
	$jquery("#JOBDetails").validationEngine('attach');
	//Save & discard process
	$('#hra_save').click(function(){ 
		if($jquery("#JOBDetails").validationEngine('validate')){  
			 
			var houseRentAllowanceId=Number($('#houseRentAllowanceId').val());
			var successMsg="";
			if(houseRentAllowanceId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var jobAssignmentId=$('#jobAssignmentId').val();
			var jobPayrollElementId=$('#jobPayrollElementId').val();
			var locationId=$('#locationId').val();
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var receiptNumber=$('#receiptNumber').val();
			var isFinanceImpact=$('#isFinanceImpact').attr("checked");
			var amount=$('#amount').val();
			var description=$('#description').val();
			var contractNumber=$('#contractNumber').val();
			var propertyId=$('#propertyName').val();
			var payPeriodTransactionId=$('#payPeriod').val();
			allowanceLineDetail=getAllowanceLineDatas(); 
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/house_rent_allowance_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		houseRentAllowanceId:houseRentAllowanceId,
			 		contractPayments:allowanceLineDetail,
			 		jobAssignmentId:jobAssignmentId,
			 		jobPayrollElementId:jobPayrollElementId,
			 		locationId:locationId,
			 		fromDate:fromDate,
			 		toDate:toDate,
			 		receiptNumber:receiptNumber,
			 		isFinanceImpact:isFinanceImpact,
			 		amount:amount,
			 		description:description,
			 		contractNumber:contractNumber,
			 		propertyId:propertyId,
			 		payPeriodTransactionId:payPeriodTransactionId
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	
	 $('.recruitment-lookup').live('click',function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
        rowId=getRowId($(this).parent().attr('id')); 
         accessCode=$(this).attr("id"); 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
   
	 $('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="PROPERTY_NAME"){
				$('#propertyName').html("");
				$('#propertyName').append("<option value=''>--Select--</option>");
				loadLookupList("propertyName");
			}
			
			if(accessCode=="PAYMENT_TYPE"){
				$('#paymentType_'+rowId).html("");
				$('#paymentType_'+rowId).append("<option value=''>--Select--</option>");
				loadLookupList("paymentType_"+rowId);
			}
		}); 
	 
	 $('.location-popup').live('click',function(){
	    	$('.error,.success').hide();
	    	 var companyId=0;
	    	
	        $('.ui-dialog-titlebar').remove();  
	        $('#job-common-popup').dialog('open');
	        var rowId=getRowId($(this).attr('id'));
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/get_location_for_setup.action",
	              data:{id:rowId,companyId:companyId},
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){
	                   $('.job-common-result').html(result);
	                   return false;
	              },
	              error:function(result){
	                   $('.job-common-result').html(result);
	              }
	          });
	           return false;
	  	}); 

	    $('.location-popup-discard').live('click',function(){
	    	$('#job-common-popup').dialog('close');
	    });
	    
	    $('#add_location').click(function(){
	    	$('.error,.success').hide();
	        	 var companyId=0;
	        	
	        	 $('.ui-dialog-titlebar').remove();  
	             $('#job-common-popup').dialog('open');
	           
			           $.ajax({
			              type:"POST",
			              url:"<%=request.getContextPath()%>/get_add_location.action",
			              data:{companyId:companyId},
			              async: false,
			              dataType: "html",
			              cache: false,
			              success:function(result){
			                   $('.job-common-result').html(result);
			                   return false;
			              },
			              error:function(result){
			                   $('.job-common-result').html(result);
			              }
			          });
			  return false;
	       
	  	}); 
	    

	     $('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:550,
				bgiframe: false,
				modal: true 
			});
    
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

   
     //Date process
     
     $('.fromDate,.toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
     
});
function listLoadCall(){
	var params="id=0";
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var transactionDate=$('#transactionDate').val();
	var payPolicy=$('#payPolicy').val();
	var payPeriodTransactionId=$('#payPeriod').val();
	params+="&jobPayrollElementId="+jobPayrollElementId;
	params+="&jobAssignmentId="+jobAssignmentId;
	params+="&transactionDate="+transactionDate;
	params+="&payPolicy="+payPolicy;
	params+="&payPeriodTransactionId="+payPeriodTransactionId;
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/house_rent_allowance.action?"+params, 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#allowance-manipulation-div").html(result);  
				return false;
			}
	 });
}
function customRanges(dates) {
	 periodTransactionLoad();
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
function getAllowanceLineDatas(){
	allowanceLineDetail="";
	var hraContractPaymentIds=new Array();
	var amounts=new Array();
	var paymentTypes=new Array();
	var descriptions=new Array();
	$('.rowidC').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var hraContractPaymentId=$('#hraContractPaymentId_'+rowId).val();
		var paymentType=$('#paymentType_'+rowId).val();
		var description=$('#description_'+rowId).val();
		var amount=$('#amount_'+rowId).val();
		if(hraContractPaymentId==null || hraContractPaymentId=='' || hraContractPaymentId==0 || hraContractPaymentId=='undefined')
			hraContractPaymentId=-1;
		
		if(paymentType==null || paymentType=='' || paymentType==0 || paymentType=='undefined')
			paymentType=-1;
		if(description==null || description=='' || description==0 || description=='undefined')
			description=-1;
		
		if(typeof amount!=null && amount!=""){
			hraContractPaymentIds.push(hraContractPaymentId);
			paymentTypes.push(paymentType);
			amounts.push(amount);
			descriptions.push(description);
			
		}
	});
	for(var j=0;j<amounts.length;j++){ 
		allowanceLineDetail+=hraContractPaymentIds[j]+"$@"+paymentTypes[j]+"$@"+amounts[j]+"$@"+descriptions[j];
		if(j==amounts.length-1){   
		} 
		else{
			allowanceLineDetail+="#@";
		}
	} 
	return allowanceLineDetail;
}


function manupulateAllowanceLastRow(){
	var hiddenSize=0;
	$($(".tabC>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabC>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabC>tr:last').removeAttr('id');  
	$('.tabC>tr:last').removeClass('rowidC').addClass('lastrow');  
	$($('.tabC>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabC>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[idval.length-1]);
	return rowId;
}
function triggerAllowanceAddRow(rowId){  
	var amount=$('#amount_'+rowId).val();  
	var nexttab=$('#fieldrowC_'+rowId).next(); 
	if(amount!=null && amount!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageC_'+rowId).show();
		$('.addrowsC').trigger('click');
		return false;
	}else{
		return false;
	} 
}

function customRanges(dates) {
	var rowId=getRowId(this.id); 
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		if($(this).val()!=""){
			return false;
		} 
		else{
			return false;
		}
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
		if($(this).val()!=""){
			return false;
		} 
		else{
			return false;
		}
	} 
}

function editDataSelectCall(){
	$('.rowidC').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#paymentType_"+rowId).val($("#paymentTypeId_"+rowId).val());
	});
	
	
	
}
function dublicatePayEntries(rId){
	var arrPay = [];
	$('.paymentType').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var allowance=$('#paymentType_'+rowId).val();
		arrPay.push(allowance);
	});
	var sorted_arr = arrPay.sort(); // You can define the comparing function here. 
	for (var i = 0; i < arrPay.length - 1; i++) {
	    if (sorted_arr[i + 1] == sorted_arr[i]) {
	    	$('#paymentType_'+rId).val("");
	        alert("Sorry! Duplicate Payroll Element");
	    	return false;
	    }
	}
}

function totalPaySum(){
	var totalPay=Number(0);
	$('.amount').each(function(){
		var rowId=getRowId($(this).attr('id'));
		totalPay+=convertToDouble($('#amount_'+rowId).val());
	});
	$('#totalPay').val(totalPay);
}
function locationGenericResultCall(locationId,locationName,commaSeparatedParam){
	$("#locationName").val(locationName);
	$("#locationId").val(locationId);
}
</script>

 
<div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>House Rent Allowance</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="JOBDetails" class="" name="JOBDetails" method="post" style="position: relative;">
		  	<input type="hidden" name="houseRentAllowanceId" id="houseRentAllowanceId" value="${HRA.houseRentAllowanceId}" class="width50"/>
		  	<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="min-height: 150px;">
							<legend>Property Information</legend>
							<div class="width35 float-left">
								<div>
									<label class="width30">Property Name<span
										style="color: red">*</span> </label>
									<div>
										<select id="propertyName"
											name="propertyName"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${PROPERTY_NAME}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="PROPERTY_NAME_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="PROPERTY_NAME"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="propertyNameTemp"
											id="propertyNameTemp"
											value="${HRA.lookupDetail.lookupDetailId}" />
									</div>
								</div>
								<div>
								<label class="width30">Contract Number</label>
									<input type="text" id="contractNumber" class="width50" name="contractNumber" value="${HRA.contractNumber}">
								</div>
								<div>
									<label class="width30">Amount</label>
									<input type="text" id="amount" name="amount" class="width50  validate[required]" value="${HRA.amount}">
								</div>
								<div>
									<label class="width30">Receipt Number</label>
									<input type="text" id="receiptNumber" name="receiptNumber" class="width50" value="${HRA.receiptNumber}">
								</div>
								<div>
									<label class="width30">Finance Impact</label>
									<input type="checkbox" id="isFinanceImpact" name="isFinanceImpact" class="width10" >
									<input type="hidden" id="isFinanceImpactTemp" name="isFinanceImpactTemp" class="width10 " value="${HRA.isFinanceImpact}" >
								</div>
								
							</div>
							<div class="width40 float-left">
							
							
								<div>
									<label class="width30" for="fromDate">From 
										Date<span class="mandatory">*</span>
									</label> <input type="text" readonly="readonly" name="fromDate"
										id="fromDate" value="${HRA.fromDateView}"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="toDate">To 
										Date<span class="mandatory">*</span></label> <input type="text" readonly="readonly"
										name="toDate" id="toDate"
										value="${HRA.toDateView}" class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30">Description</label> <textarea 
										id="description" name="description" class="width50 "
										>${HRA.description}</textarea>
								</div>
							 <div>
								<label class="width30">Location</label>
								<input type="text" readonly="readonly" id="locationName" value="${HRA.location.locationName}" class="width50 location"/>
								<span class="button " id="location"  style="position: relative;">
									<a style="cursor: pointer;" id="locationpopup" class="btn ui-state-default ui-corner-all width10 location-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
							<input type="hidden" name="locationId" id="locationId" class="locationId" value="${HRA.location.locationId}"/> 
							</div>
							<div>
							 	<div class="float-left width100 buttons ui-widget-content ui-corner-all">
									<div class="portlet-header ui-widget-header float-right add_location"  id="add_location"><fmt:message key="hr.department.newlocation"/></div>
								</div>
							 </div> 	
							</div>
							<div class="width20 float-left">
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Pay Elment : </label>
									<label class="width40 ">${JOBPAY_INFO.elementName}</label>
									<input type="hidden" id="jobPayrollElementId" value="${JOBPAY_INFO.jobPayrollElementId}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period From : </label>
									<label class="width40 ">${JOBPAY_INFO.periodStartDate}</label>
									<input type="hidden" id="periodStartDate" value="${JOBPAY_INFO.periodStartDate}"/>
									
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period To : </label>
									<label class="width40 ">${JOBPAY_INFO.periodEndDate}</label>
									<input type="hidden" id="periodEndDate" value="${JOBPAY_INFO.periodEndDate}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 " >Provided Amount(Max) : </label>
									<label class="width40 ">${JOBPAY_INFO.allowedMaixmumAmount}</label>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Available Balance : </label>
									<label class="width40 ">${JOBPAY_INFO.availableAmount}</label>
								</div>
							</div>
						</fieldset>
					</div>
			</div>
 		</form>
		
					<div id="hrm" class="hastable float-left width100"  >  
						<table id="hastab1" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%">Payment Type</th> 
								    <th style="width:5%">Amount</th>  
								    <th style="width:5%">Description</th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabC">
										<c:choose>
											<c:when test="${HRA.hraContractPayments ne null && HRA.hraContractPayments ne '' && fn:length(HRA.hraContractPayments)>0}">
												<c:forEach var="contractDetail" items="${HRA.hraContractPayments}" varStatus="status1">
													<tr class="rowidC" id="fieldrowC_${status1.index+1}">
														<td id="lineIdC_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														
														<td>
															<select id="paymentType_${status1.index+1}"
															name="paymentType_${status1.index+1}}"
															class="width80 validate[required]">
															<option value="">--Select--</option>
															<c:forEach items="${PAYMENT_TYPE}" var="nltls">
																<option value="${nltls.lookupDetailId}">${nltls.displayName}
																</option>
															</c:forEach>
														</select> <span class="button" id="PAYMENT_TYPE_${status1.index+1}"
															style="position: relative; "> <a
															style="cursor: pointer;" id="PAYMENT_TYPE"
															class="btn ui-state-default ui-corner-all recruitment-lookup width100">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
															<input type="hidden" id="paymentTypeId_${status1.index+1}" value="${contractDetail.lookupDetail.lookupDetailId}" style="border:0px;"/>
														</td> 
														<td>
															<input type="text" class="width80 amount" id="amount_${status1.index+1}" value="${contractDetail.amount}" style="border:0px;text-align: right;"/>
														</td> 
														
														<td>
															<textarea class="width80 description" id="description_${status1.index+1}"style="border:0px;">${contractDetail.description}</textarea>
														</td> 
														<td style="display:none;">
															<input type="hidden" id="hraContractPaymentId_${status1.index+1}" value="${contractDetail.hraContractPaymentId}" style="border:0px;"/>
														</td>
														 <td style="width:0.01%;" class="opn_td" id="optionC_${status1.index+1}">
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
									 								<span class="ui-icon ui-icon-plus"></span>
															  </a>	
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																	<span class="ui-icon ui-icon-wrench"></span>
															  </a> 
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span>
															  </a>
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${status1.index+1}" style="display:none;" title="Working">
																	<span class="processing"></span>
															  </a>
														</td>
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										<c:forEach var="i" begin="${fn:length(HRA.hraContractPayments)+1}" end="${fn:length(HRA.hraContractPayments)+2}" step="1" varStatus="status"> 
											<tr class="rowidC" id="fieldrowC_${i}">
												<td id="lineIdC_${i}" style="display:none;">${i}</td>
												<td>
															<select id="paymentType_${i}"
															name="paymentType_${i}"
															class="width80 validate[required]">
															<option value="">--Select--</option>
															<c:forEach items="${PAYMENT_TYPE}" var="nltls">
																<option value="${nltls.lookupDetailId}">${nltls.displayName}
																</option>
															</c:forEach>
														</select> <span class="button" id="PAYMENT_TYPE_${i}"
															style="position: relative; "> <a
															style="cursor: pointer;" id="PAYMENT_TYPE"
															class="btn ui-state-default ui-corner-all recruitment-lookup width100">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
															<input type="hidden" id="paymentTypeId_${i}"  style="border:0px;"/>
														</td> 
														<td>
															<input type="text" class="width80 amount" id="amount_${i}"  style="border:0px;text-align: right;"/>
														</td> 
														
														<td>
															<textarea class="width80 description" id="description_${i}"style="border:0px;"></textarea>
														</td>
														<td style="display:none;">
															<input type="hidden" id="hraContractPaymentId_${i}"  style="border:0px;"/>
														</td>
												 <td style="width:0.01%;" class="opn_td" id="optionc_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${i}" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>    
											</tr>
									</c:forEach>
							 </tbody>
					</table>
					<div class="float-right width30">
						<label class="float-left width30"><span><fmt:message key="hr.job.totalamount"/> :</span></label>
						<input type="text" readonly="readonly" id="totalPay" class="float-left width50"/>
					</div>
				</div> 
			</div>
	
		</div>
		<div class="clearfix"></div>
		<div class="float-right buttons ui-widget-content ui-corner-all">
			<div class="portlet-header ui-widget-header float-right discard" id="hra_discard"><fmt:message key="common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right save" id="hra_save"><fmt:message key="organization.button.save"/></div>
		</div>
		<div id="common-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrowsC" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
		
</div>

 
	

