<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.label-data{
	font-weight: normal!important;
}


</style><script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;var payLineDetail="";
$(function(){ 
	
	$('.formError').remove();
	

	$('.payrollElementIdTemp').each(function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			$('#payrollElementId_'+rowId).val($('#payrollElementIdTemp_'+rowId).val());
		} 
		
	});
	personId=Number($('#personId').val());	
	manupulateLastRow();
	totalPaySum();
	//actualtotalPaySum();

	$('.discard').click(function(){
		listCall();
	});
	
	$('#reject').live('click',function(){
		payrollId=Number($('#payrollId').val());	
		if(payrollId>0)
			workflowRejectCall(payrollId);
	});
//Onchage event
	$('.attendanceDate').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	
	$('.payrollElementId').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	$('.amount').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			totalPaySum();
			return false;
		} 
		else{
			return false;
		}
	});	 
	
	//Add row
	$('.addrowsS').live('click',function(){
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/payroll_transaction_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabS tr:last').before(result);
					 if($(".tabS").height()>255)
						 $(".tabS").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidS').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdS_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowS').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
  	 $(slidetab).remove();  
  		totalPaySum();
  	 var i=1;
	 $('.rowidS').each(function(){   
		 var rowId=getRowId($(this).attr('id')); 
		 $('#lineIdS_'+rowId).html(i);
			 i=i+1; 
		 });   
	 });
	
	
	$jquery("#ATTENDANCEEDIT").validationEngine('attach');
//Save & discard process
$('.save').click(function(){ 
	$('#page-error').hide();
	if($jquery("#ATTENDANCEEDIT").validationEngine('validate')){
		var payrollId=Number($('#payrollId').val());
		var personId=Number($('#personId').val());
		var jobAssignmentId=Number($('#jobAssignmentId').val());
		var netAmount=Number($('#totalPay').val());
		payLineDetail=getLineDetails();
		//alert(payLineDetail);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_payroll_verification.action", 
		 	async: false, 
		 	data:{ 
		 		payrollId:payrollId,
		 		personId:personId,
		 		jobAssignmentId:jobAssignmentId,
		 		payLineDetail:payLineDetail,
		 		netAmount:netAmount
		 	},
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 listCall();
					 $('.success').hide().html("Successfully saved").slideDown(1000);
				 }else{
					 $('#page-error').hide().html(message).slideDown(1000);
				 }
			},
			error:function(result){  
				$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
			}
		});  
	}
	else{
		return false;
	}
	return false;
});

//pop-up config
$('#employeepopups').click(function(){
      $('.ui-dialog-titlebar').remove();  
      $('#job-common-popup').dialog('open');
         $.ajax({
            type:"POST",
            url:"<%=request.getContextPath()%>/get_employee_designation.action",
            async: false,
            dataType: "html",
            cache: false,
            success:function(result){
                 $('.job-common-result').html(result);
                 return false;
            },
            error:function(result){
                 $('.job-common-result').html(result);
            }
        });
        return false;
}); 

$('#job-common-popup').dialog({
	autoOpen: false,
	minwidth: 'auto',
	width:800,
	height:550,
	bgiframe: false,
	modal: true 
});
});
function homeredirect(){
	window.location.reload();
}

var getLineDetails=function(){
	payLineDetail="";
	var payTransactionIds=new Array();
	var payElements=new Array();
	var amountS=new Array();
	var notes=new Array();
	$('.rowidS').each(function(){ 
		var rowId=getRowId($(this).attr('id')); 
		var payrollElementId=$('#payrollElementId_'+rowId).val();  
		var amount=$('#amount_'+rowId).val();
		var payrollTransactionId=$('#payrollTransactionId_'+rowId).val();
		var note=$('#note_'+rowId).val();
		if(payrollTransactionId==null || payrollTransactionId=='' || payrollTransactionId==0 || payrollTransactionId=='undefined')
			payrollTransactionId=-1;
		
		if(note==null || note=='' || note==0 || note=='undefined')
			note=-1;
	
		if(typeof payrollElementId != 'undefined' && payrollElementId!=null && payrollElementId!="" && amount!=null && amount!=""){
			payTransactionIds.push(payrollTransactionId);
			payElements.push(payrollElementId);
			amountS.push(amount);
			notes.push(note);
		}
	});
	for(var j=0;j<payElements.length;j++){ 
		payLineDetail+=payTransactionIds[j]+"@#"+payElements[j]+"@#"+amountS[j]+"@#"+notes[j];
		if(j==payElements.length-1){   
		} 
		else{
			payLineDetail+="##";
		}
	} 
	return payLineDetail;
};
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){
	var payrollElementId=$('#payrollElementId_'+rowId).val();  
	var amount=$('#amount_'+rowId).val();
	var nexttab=$('#fieldrowS_'+rowId).next(); 
	if(payrollElementId!=null && payrollElementId!=""
			&& amount!=null && amount!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageS_'+rowId).show();
		$('.addrowsS').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tabS>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabS>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabS>tr:last').removeAttr('id');  
	$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
	$($('.tabS>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function totalPaySum(){
	var totalPay=Number(0);
	var earningTotal=Number(0);
	var deductionTotal=Number(0);
	$('.amount').each(function(){
		var rowId=getRowId($(this).attr('id'));
		if($('#payrollElementId_'+rowId).val()!=null && $('#payrollElementId_'+rowId).val()!=''){
			var temp=$('#payrollElementId_'+rowId+' option:selected').text();
			var type=temp.substring((temp.indexOf('[')+1), temp.indexOf(']'));
			if(type=="EARNING")
				earningTotal+=convertToDouble($('#amount_'+rowId).val());
			else if(type=="DEDUCTION")
				deductionTotal+=convertToDouble($('#amount_'+rowId).val());
		}
	});
	totalPay=earningTotal-deductionTotal;
	$('#totalPay').val(totalPay);
}
function actualtotalPaySum(){
	var totalPay=Number(0);
	var earningTotal=Number(0);
	var deductionTotal=Number(0);
	$('.actualPayAmount').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var type=$('#actualPayelementNature_'+rowId).text().trim();
		if(type=="EARNING")
			earningTotal+=convertToDouble($(this).text());
		else if(type=="DEDUCTION")
			deductionTotal+=convertToDouble($(this).text());
	});
	totalPay=earningTotal-deductionTotal;
	$('#actualtotalPay').html(totalPay);
}
function listCall(){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/payroll_adjustment.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#main-wrapper").html(result);  
				return false;
			}
	 });
	 return false;
}

function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	if(returnVO.returnStatusName=='DeleteApproved'){
		var payrollId=Number($('#payrollId').val());
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/delete_payroll.action", 
			 	async: false, 
			 	data:{ 
			 		payrollId:payrollId,messageId:messageId
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					return false;
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
		}); 
	}
	return false;
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay Adjustment</div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
		<div style="display:none;" class="tempresult">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<div>
			 <div class="error response-msg ui-corner-all" style="display:none;" id="page-error">Sorry, Process Failure!</div>
		  	  <div class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div>
		  <form id="ATTENDANCEEDIT" class="" name="ATTENDANCEEDIT" method="post" style="position: relative;">
		  	<div class="width100 float-left" id="hrm"> 
		  			<input type="hidden" name="payrollId" id="payrollId" value="${PAYROLL_PROCESS.payrollId}" class="width50"/>
		  			<div class="width50 float-left">
						<fieldset style="height:138px;">
							<legend><fmt:message key="hr.attendance.employeeinfo"/></legend>
							<div class="float-left">
								<c:set var="jobAssignment" value="${PAYROLL_PROCESS.jobAssignment}"/>
								<input type="hidden" name="jobAssignmentId" id="jobAssignmentId" value="${jobAssignment.jobAssignmentId}" class="width50 validate[required]"/>
								<div class="float-left width100" style="padding:2px;">
									<label class="width30" ><fmt:message key="hr.attendance.employee"/> : </label>
									<span  id="employeeName" class="float-left width30">
									${jobAssignment.person.firstName} ${jobAssignment.person.lastName}
									</span>
									<span class="button float-right" id="employee"  style="position: relative; top:6px;display: none;">
										<a style="cursor: pointer;" id="employeepopups" class="btn ui-state-default ui-corner-all width40 employee-popup"> 
											<span class="ui-icon ui-icon-newwin">
											</span> 
										</a>
									</span> 
									<input type="hidden" id="personId" value="" class="personId" value="${jobAssignment.person.personId}" style="border:0px;"/>
								</div>
								<div class="float-left width100" style="padding:2px;">
									<label class="width30" ><fmt:message key="hr.attendance.job"/> : </label>
									<label class="width50 label-data" id="designationName">${jobAssignment.designation.designationName}</label>
								</div> 
								<div class="float-left width100" style="padding:2px;">
									<label class="width30" ><fmt:message key="hr.attendance.grade"/> : </label>
									<label class="width50 label-data" id="gradeName">${jobAssignment.designation.grade.gradeName}</label>
								</div> 
								<div class="float-left width100" style="padding:2px;">
									<label class="width30" ><fmt:message key="hr.attendance.company"/> : </label>
									<label class="width50 label-data" id="companyName">${jobAssignment.cmpDeptLocation.company.companyName}</label>
								</div>
								<div class="float-left width100" style="padding:2px;">
									<label class="width30" ><fmt:message key="hr.attendance.department"/> : </label>
									<label class="width50 label-data" id="departmentName">${jobAssignment.cmpDeptLocation.department.departmentName}</label>
								</div>
								<div class="float-left width100" style="padding:2px;">
									<label class="width30" ><fmt:message key="hr.attendance.location"/> : </label>
									<label class="width50 label-data" id="locationName">${jobAssignment.cmpDeptLocation.location.locationName}</label>
								</div>
							</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset style="height:138px;">
								<legend><fmt:message key="hr.payroll.masterinfo"/></legend>
								<div class="float-left">
									<div class="float-left width100" style="padding:2px;">
										<label class="width30" ><fmt:message key="hr.payroll.paymonth"/> : </label>
										<label class="width50 label-data">${PAYROLL_PROCESS.payMonth}</label>
									</div>
									
									<div class="float-left width100" style="padding:2px;">
										<c:set var="startDate" value="${PAYROLL_PROCESS.startDate}"/>  
										<%String startDate = DateFormat.convertDateToString(pageContext.getAttribute("startDate").toString());%>
										<label class="width30" ><fmt:message key="hr.payroll.periodfrom"/> : </label>
										<label class="width50 label-data"><%=startDate%></label>
									</div>
									<div class="float-left width100" style="padding:2px;">
										<c:set var="endDate" value="${PAYROLL_PROCESS.endDate}"/>  
										<%String endDate = DateFormat.convertDateToString(pageContext.getAttribute("endDate").toString());%>
										<label class="width30" ><fmt:message key="hr.payroll.periodend"/> : </label>
										<label class="width50 label-data"><%=endDate%></label>
									</div>
									<div class="float-left width100" style="padding:2px;">
										<label class="width30" ><fmt:message key="hr.payroll.numberofdays"/> : </label>
										<label class="width50 label-data">${PAYROLL_PROCESS.numberOfDays}</label>
									</div>
									<div class="float-left width100" style="padding:2px;">
										<label class="width30" ><fmt:message key="hr.payroll.deductiondays"/> : </label>
										<label class="width50 label-data">${PAYROLL_PROCESS.lopDays}</label>
									</div>
									<div class="float-left width100" style="background-color:green;color:white;font-weight: bold;padding:2px;">
										<label class="width30" ><fmt:message key="hr.payroll.netpay"/> : </label>
										<label class="width50 label-data">${PAYROLL_PROCESS.netAmount}</label>
									</div>
								</div>
							</fieldset>
						
				 	</div> 
				<div class="width100 float-left">
						<fieldset>
							<legend>Provided Pay Information</legend>
							<div id="hrm" class="hastable width100"  >  
									<table class="width100"> 
										<thead>
											<tr> 
												<th style="width:5%"><fmt:message key="hr.payroll.elementname"/></th> 
												<th style="width:5%">Calculation Type</th> 
											    <th style="width:5%">Value(Max)</th>
											    <th style="width:5%">Amount</th>
										  </tr>
										</thead> 
											<tbody class="tab">
												<c:choose>
													<c:when test="${JOB_PAYROLL_ELEMENTS ne null && JOB_PAYROLL_ELEMENTS ne '' && fn:length(JOB_PAYROLL_ELEMENTS)>0}">
														<c:forEach var="pay1" items="${JOB_PAYROLL_ELEMENTS}" varStatus="status3">
															<tr class="rowid">
																<td>
																	<span>${pay1.payrollElementByPayrollElementId.elementName}</span>
																</td> 
																<td>
																	<span id="actualPayelementNature_${status3.index+1}">${pay1.calculationTypeView}</span>
																</td>
																<td>
																	<span id="actualPayValue_${status3.index+1}" class="actualPayValue">${pay1.amount}</span>
																</td> 
																<td>
																	<span id="actualPayAmount_${status3.index+1}" class="actualPayAmount">${pay1.consolidatedAmount}</span>
																</td> 
															</tr>
														</c:forEach>  
													</c:when>
												</c:choose>  
										 </tbody>
								</table>
								
							</div> 
							
						</fieldset>
				 </div> 
					<div class="width100 float-left">
						<fieldset>
							<legend>Projected Pay Details</legend>
							<div id="hrm" class="hastable width100"  >  
									<table id="hastab2" class="width100"> 
										<thead>
											<tr> 
												<th style="width:15%"><fmt:message key="hr.payroll.elementname"/></th> 
											    <th style="width:5%">Calculated Amount</th>
											     <th style="width:5%">Amount</th>
											    <th style="width:15%">Note</th>
												<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
										  </tr>
										</thead> 
												<tbody class="tabS">
													<c:choose>
														<c:when test="${PAYROLL_PROCESS.payrollTransactions ne null && PAYROLL_PROCESS.payrollTransactions ne '' && fn:length(PAYROLL_PROCESS.payrollTransactions)>0}">
															<c:forEach var="pay" items="${PAYROLL_PROCESS.payrollTransactions}" varStatus="status1">
																<tr class="rowidS" id="fieldrowS_${status1.index+1}">
																	<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
																	<td>
																		<select class="width90 payrollElementId" disabled="disabled" id="payrollElementId_${status1.index+1}"style="border:0px;">
																				<option value="">Select</option>
																			<c:forEach var="payrollElemnt" items="${PAYROLL_ELEMENTS}" varStatus="status">
																				<option value="${payrollElemnt.payrollElementId}">${payrollElemnt.elementName} [${payrollElemnt.elementNature}]</option>
																			</c:forEach>
																		</select>
																		<input type="hidden" class="width80 payrollElementIdTemp"  name="payrollElementIdTemp" id="payrollElementIdTemp_${status1.index+1}" value="${pay.payrollElement.payrollElementId}" style="border:0px;"/>
																	</td> 
																	<td>
																		<input type="text" class="width80 calculatedAmount" disabled="disabled" name="calculatedAmount" id="calculatedAmount_${status1.index+1}" value="${pay.calculatedAmount}" readonly="readonly" style="border:0px;"/>
																	</td> 
																	<td>
																		<input type="text" class="width80 amount" name="amount" id="amount_${status1.index+1}" value="${pay.amount}" style="border:0px;"/>
																	</td> 
																	<td>
																		<input type="text" class="width80 note" name="note" id="note_${status1.index+1}" value="${pay.note}" style="border:0px;"/>
																	</td> 
																	<td style="display:none;">
																		<input type="hidden" id="payrollTransactionId_${status1.index+1}" value="${pay.payrollTransactionId}" style="border:0px;"/>
																	</td>
																	 <td style="width:0.01%;" class="opn_td" id="optionS_${status1.index+1}">
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												 								<span class="ui-icon ui-icon-plus"></span>
																		  </a>	
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																				<span class="ui-icon ui-icon-wrench"></span>
																		  </a> 
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																				<span class="ui-icon ui-icon-circle-close"></span>
																		  </a>
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${status1.index+1}" style="display:none;" title="Working">
																				<span class="processing"></span>
																		  </a>
																	</td>
																</tr>
															</c:forEach>  
														</c:when>
													</c:choose>  
													<c:forEach var="i" begin="${fn:length(PAYROLL_PROCESS.payrollTransactions)+1}" end="${fn:length(PAYROLL_PROCESS.payrollTransactions)+2}" step="1" varStatus="status"> 
														<tr class="rowidS" id="fieldrowS_${i}">
																<td id="lineIdS_${i}" style="display: none;">${i}</td>
																<td>
																	<select class="width90 payrollElementId" id="payrollElementId_${i}"style="border:0px;">
																			<option value="">Select</option>
																		<c:forEach var="payrollElemnt" items="${PAYROLL_ELEMENTS}" varStatus="status">
																			<option value="${payrollElemnt.payrollElementId}">${payrollElemnt.elementName} [${payrollElemnt.elementNature}]</option>
																		</c:forEach>
																	</select>
																	<input type="hidden" class="width80 payrollElementIdTemp" name="payrollElementIdTemp" id="payrollElementIdTemp_${i}" value=""  style="border:0px;"/>
																</td> 
																<td>
																	<input type="text" class="width80 calculatedAmount" disabled="disabled" name="calculatedAmount" id="calculatedAmount_${i}" value="" readonly="readonly" style="border:0px;"/>
																</td> 
																<td>
																	<input type="text" class="width80 amount" name="amount" id="amount_${i}" value="" style="border:0px;"/>
																</td> 
																<td>
																	<input type="text" class="width80 note" name="note" id="note_${i}" value="" style="border:0px;"/>
																</td> 
																<td style="display:none;">
																	<input type="hidden" id="payrollTransactionId_${i}" value="" style="border:0px;"/>
																</td>
																<td style="width:0.01%;" class="opn_td" id="optionS_${i}">
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${i}" style="display:none;cursor:pointer;" title="Add Record">
										 								<span class="ui-icon ui-icon-plus"></span>
																  </a>	
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${i}" style="display:none; cursor:pointer;" title="Edit Record">
																		<span class="ui-icon ui-icon-wrench"></span>
																  </a> 
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${i}" style="display:none;cursor:pointer;" title="Delete Record">
																		<span class="ui-icon ui-icon-circle-close"></span>
																  </a>
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${i}" style="display:none;" title="Working">
																		<span class="processing"></span>
																  </a>
															</td>    
														</tr>
												</c:forEach>
										 </tbody>
								</table>
								<div class="float-right width50">
									<label class="float-left width30"><span><fmt:message key="hr.payroll.totalamount"/> :</span></label>
									<input type="text" readonly="readonly" id="totalPay" class="float-left width40"/>
								</div>
							</div> 
							<div class="clearfix"></div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right discard" id="discard">Cancel</div>
								<div class="portlet-header ui-widget-header float-right save" id="save">Save</div>
							</div>
						</fieldset>
				 </div> 
 			</div>
 			
 		</form>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
	</div>
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrowsS" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
	</div>
</div>
</div>
