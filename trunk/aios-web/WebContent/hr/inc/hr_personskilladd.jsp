 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<tr style="background-color: #F8F8F8;"
	id="skillchildRowId_${requestScope.rowid}">
	<td colspan="7" class="tdskill"
		id="skillchildRowTD_${requestScope.rowid}">
		<div id="errMsg"></div>
		<form name="skillform" id="skillform" style="position: relative;">
			<input type="hidden" name="addEditFlag"
				id="skilladdEditFlag" value="${requestScope.AddEditFlag}" />
			<input type="hidden" name="id"
				id="skilllineId" value="${requestScope.rowid}" />
			<input type="hidden" name="skill.skillsId"
				id="skillsId"  />
			<div class="float-left width50" id="hrm">

				<fieldset>
					<div>
						<label class="width30">Skills<span style="color: red">*</span>
						</label>
						<div>
							<select id="skillType" name="skill.lookupDetail.lookupDetailId"
								class="width50 validate[required]">
								<option value="">--Select--</option>
								<c:forEach items="${SKILLS}" var="nltlst">
									<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
									</option>
								</c:forEach>
							</select> <span class="button" id="SKILLS_1" style="position: relative;">
								<a style="cursor: pointer;" id="SKILLS"
								class="btn ui-state-default ui-corner-all skill-lookup width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
								type="hidden" name="skillTemp" id="skillTemp" />
						</div>
						<div>
							<label class="width30">Skill Level<span
								style="color: red">*</span> </label> <input type="text"
								id="skillLevel" class="width50 validate[required]"
								name="skill.skillLevel" />
						</div>

					</div>
				</fieldset>
			</div>
			<div class="float-left width50" id="hrm">
				<fieldset>
					<div>
						<label class="width30">Description </label>
						<textarea id="skillDescription" class="width50 "
							name="skill.description"></textarea>
					</div>

				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
				style="margin: 20px;">
				<div
					class="portlet-header ui-widget-header float-right cancel_skill"
					id="cancel_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right add_skill"
					id="update_${requestScope.rowid}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
		</form>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span> </a>
			</div>
			<div id="skill-common-popup"
				class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="skill-common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
		</td>
</tr>

<script type="text/javascript">
	 var trval=$('.tdskill').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';
     var accessCode=null;
	 $(function(){  
		 $jquery("#skillform").validationEngine('attach');
		 $('#completionYear').datepick();
		 clear:false;
		 
		//Lookup Data Roload call
		 	$('.save-lookup').live('click',function(){ 
		 		
				if(accessCode=="SKILLS"){
					$('#skillType').html("");
					$('#skillType').append("<option value=''>--Select--</option>");
					loadLookupList("skillType");
					
				}
				
				
			}); 
		
		 	
		 	//Lookup Data Roload call
		 	$('.discard-lookup').live('click',function(){ 
				$('#skill-common-popup').dialog("close");
				$('#common-popup').dialog("close");
			});
		 
		
		 	$('.success,.error').hide();
			$('#skill-common-popup').dialog("close");
			$('.skill-lookup').click(function() {
				
				$('.ui-dialog-titlebar').remove();
				$('#skill-common-popup').dialog('open');
				accessCode = $(this).attr("id");
				$.ajax({
					type : "POST",
					url : "<%=request.getContextPath()%>/add_lookup_detail.action",
		               data:{accessCode:accessCode},
		               async: false,
		               dataType: "html",
		               cache: false,
		               success:function(result){
		                    $('.skill-common-result').html(result);
		                    return false;
		               },
		               error:function(result){
		                    $('.skill-common-result').html(result);
		               }
		           });
		            return false;
		   	}); 
			 
			
			$('#skill-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:450,
				bgiframe: false,
				modal: true 
			});
		 
		  $('.cancel_skill').click(function(){ 
			  $('.formError').remove();	
			  
			  $('#skill-common-popup').dialog('destroy');		
				$('#skill-common-popup').remove(); 
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdskill').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#skilladdEditFlag').val()!=null && $('#skilladdEditFlag').val()=='A')
				{
  					$("#skillAddImage_"+rowid).show();
  				 	$("#skillEditImage_"+rowid).hide();
				}else{
					$("#skillAddImage_"+rowid).hide();
  				 	$("#skillEditImage_"+rowid).show();
				}
				 $("#skillchildRowId_"+rowid).remove();	
				 $("#skillDeleteImage_"+rowid).show();
				 $("#skillWorkingImage_"+rowid).hide();  
				 
				 tempvar=$('.skilltab>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#skillDeleteImage_'+rowid).hide(); 
				 $('#openFlag').val(0);
				 return false;
		  });
		  $('.add_skill').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdskill').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				
				var skillId=$('#skillsId').val();
			 	var skillType=$('#skillType option:selected').text();
			 	var skillTypeId=$('#skillType option:selected').val();
			 	var skillLevel=$('#skillLevel').val();
			 	var description=$('#skillDescription').val();
			 
			 //var tempObject=$(this);
			var addEditFlag=$('#skilladdEditFlag').val();
		        if($jquery("#skillform").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/person_skill_save.action", 
					 	async: false, 
					 	data:$("#skillform").serialize(),
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 
		        		 $('#skill-common-popup').dialog('destroy');		
		 				$('#skill-common-popup').remove(); 
			        	//Bussiness parameter
			        		$('#skillId_'+rowid).val(skillId);
			        		$('#skillTypeId_'+rowid).val(skillTypeId); 
			        		$('#skillType_'+rowid).text(skillType); 
		        			$('#skillLevel_'+rowid).text(skillLevel); 
		        			$('#skillDescription_'+rowid).text(description); 
		        			
		        			
						//Button(action) hide & show 
							 $("#skillchildRowId_"+rowid).remove();	
		    				 $("#skillAddImage_"+rowid).hide();
		    				 $("#skillEditImage_"+rowid).show();
		    				 $("#skillDeleteImage_"+rowid).show();
		    				 $("#skillWorkingImage_"+rowid).hide(); 
		        			

		        			if(addEditFlag!=null && addEditFlag=='A')
		     				{
	         					$('.skilladdrows').trigger('click');
	         					var skillcount=Number($('#skillcount').val());
	         					skillcount+=1;
	         					$('#skillcount').val(skillcount);
	         					 tempvar=$('.skilltab>tr:last').attr('id'); 
	         					 idval=tempvar.split("_");
	         					 rowid=Number(idval[1]); 
	         					 $('#skillDeleteImage_'+rowid).show(); 
		     				}
		        		}		  	 
		      } 
		      else{return false;}
		  });
		return false;	
	 });
	 function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
 </script>	