<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count {
	padding: 0 !important;
}
</style>

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 
	var jobId=$('#jobId').val();
	if(jobId!=null && jobId>0)
		loadOfferInformation();
	
	var jobAssignmentId=$('#jobAssignmentId').val();
	if(jobAssignmentId!=null && jobAssignmentId>0)
		$('#isActive').attr("checked",true);
	
	
	
	autoSelectTheMultiList("statusTemp","candidateStatus");
	
	 $('#expectedJoiningDate').datepick();
 
	 $('#offer-save').click(function() { 
		 var errorMessage=null;
		 var successMessage="Successfully Updated";

			var candidateOfferId=$('#candidateOfferId').val();
			var expectedJoiningDate=$('#expectedJoiningDate').val();
			var comments=$('#comments').val();
			var isActive=$('#isActive').attr("checked");
			var status=$('#candidateStatus option:selected').val();
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/finalise_offer_letter.action",
               data:{candidateOfferId:candidateOfferId,
            	   comments:comments,
            	   expectedJoiningDate:expectedJoiningDate,
            	   isActive:isActive,
            	   status:status},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){ 
					var message=result.trim();
					 if(message=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error:function(result){ 
					errorMessage=result;
					
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				}
           });
   }); 
		
		
		$('#offer-discard').click(function(){ 
			returnCallTOList();
			
		 });
	

		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:450,
			bgiframe: false,
			modal: true 
		});
			
		 //$('#createdDate').datepick();
		//$('#interviewTime').timepicker({});
		 
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/candidate_offer_finalise_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	
	function jobPopupResult(id,name,commaseparatedValue){
		$('#jobName').val(name);
		$('#jobId').val(id);
		loadOfferInformation();
	}
	
	function loadOfferInformation(){
		var jobId=$('#jobId').val();
		var candidateOfferId=$('#candidateOfferId').val();
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_offer_letter.action", 
			 	async: false,
			    dataType: "html",
			    data:{jobId:jobId,candidateOfferId:candidateOfferId},
			    cache: false,
				success:function(result){ 
					$("#offer-letter-div").html(result);  
				}
		 });
	}

	
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	
	function autoSelectTheMultiList(source,target) {
		 var idsArray=new Array();
		 var idlist=$('#'+source).val();
		 idsArray=idlist.split(',');
		$('#'+target+' option').each(function(){
			var txt=$(this).val().trim();
			if($.inArray(txt, idsArray)>=0)
				$(this).attr("selected","selected");
		});

	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Candidate Finalization
		
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10">Comment From   :<span> ${COMMENT_IFNO.person.firstName} ${COMMENT_IFNO.person.lastName}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="interview-details">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
							
								<input type="hidden" name="candidateOfferId"
									id="candidateOfferId" value="${CANDIDATE_OFFER.candidateOfferId}"
									class="width50 " />
									
									<input type="hidden" name="jobAssignmentId"
									id="jobAssignmentId" value="${CANDIDATE_OFFER.candidateOffer.jobAssignment.jobAssignmentId}"
									class="width50 " />
						
								<fieldset style="min-height: 50px;">
									<legend>Candidate Information</legend>
									<div class="width50 float-left">
										<div>
											<label class="width50 float-left">Candidate</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.personByPersonId.firstName}
												${CANDIDATE_OFFER.personByPersonId.lastName}</label>
										</div>
										<div>
											<label class="width50 float-left">Position Reference No. </label> <label
												class="width50 float-left">${CANDIDATE_OFFER.openPosition.positionReferenceNumber}</label>
										</div>
										<div>
											<label class="width50 float-left">Recruitment Source</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.recruitmentSource.providerName}</label>
										</div>
										<div>
											<label class="width50 float-left">Expected Joining Date</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.expectedJoiningDateDisplay}</label>

										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<label class="width50 float-left">Job Template</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.jobVO.jobName}
												[${CANDIDATE_OFFER.jobVO.jobNumber}]</label> <input
												type="hidden" id="jobId"
												value="${CANDIDATE_OFFER.jobVO.jobId}"
												class="jobId" />
										</div>

									</div>

								</fieldset>
								<fieldset style="min-height: 50px;">
									<legend>Offer Information</legend>
									<div class="width50 float-left">
										<div>
											<label class="width30">Status<span style="color: red">*</span>
											</label>
												<select id="candidateStatus" name="candidate.status"
													class="width61 validate[required]">
													<c:forEach items="${STATUS_TYPE}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="statusTemp" id="statusTemp"
													value="${CANDIDATE_OFFER.status}" />
										</div>
										<div>
											<label class="width30">Expected Joining Date</label> <input
												type="text" class="width60" readonly="readonly"
												name="expectedJoiningDate" id="expectedJoiningDate"
												value="${CANDIDATE_OFFER.expectedJoiningDateDisplay}" />
										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<label class="width30">Comments</label> <textarea
												class="width50"
												name="comments" id="comments"
												>${CANDIDATE_OFFER.candidateOffer.comments}</textarea>
										</div>
										<div>
											<label class="width30">Finalise</label> 
											<input type="checkbox" name="isActive" id="isActive" class="width8">
										</div>
									</div>
								</fieldset>
							</div>
							

						</div>
						<div class="clearfix"></div>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
	
							<div
								class="portlet-header ui-widget-header float-right offer-discard"
								id="offer-discard">cancel</div>
							<div
								class="portlet-header ui-widget-header float-right offer-save"
								id="offer-save">save</div>
						</div>
					
					</form>
					<div class="clearfix"></div>
					<div id="offer-letter-div"></div>
					
					
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup" class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

				

			</div>
		</div>
	</div>
</body>
 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
</html>