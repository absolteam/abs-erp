<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui/flick/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.DOMWindow.js"></script> 
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
<link href="${pageContext.request.contextPath}/css/reset.css" rel="stylesheet"  type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/styles/default/ui.css" rel="stylesheet"  type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/forms.css" rel="stylesheet"  type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/tables.css" rel="stylesheet"  type="text/css"  media="all" />
<link href="${pageContext.request.contextPath}/css/messages.css" rel="stylesheet" type="text/css"  media="all" /> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/HRM.css" type="text/css" media="all" /> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%!
	HttpSession session = ServletActionContext.getRequest()
	.getSession();
	Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
%>
<script type="text/javascript">
$(document).ready(function(){
	 window.location.reload();
});

</script>
</head>
 
   File uploaded successfully...!!!
 
   File Name : <s:property value="uploadFileFileName"/> <br>
   Content Type : <s:property value="uploadFileContentType"/> <br>
   Temp File Name : <s:property value="uploadFile"/>
	