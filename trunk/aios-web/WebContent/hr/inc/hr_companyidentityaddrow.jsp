<tr id="identityfieldrow_${requestScope.id}" class="identityrowid"> 
	<td class="width10" id="identityType_${requestScope.id}"></td>
	<td class="width10" id="identityNumber_${requestScope.id}"></td> 
          <td class="width10" id="identityExpireDate_${requestScope.id}"></td>
         	<td class="width10" id="identityIssuedPlace_${requestScope.id}"></td>
         	<td class="width20" id="identityDescription_${requestScope.id}"></td>
		<td  style="width:5%;" id="identityoption_${requestScope.id}">
			<input type="hidden" name="identityId_${requestScope.id}" id="identityId_${requestScope.id}"  value="0"/>  
			<input type="hidden" name="identityTypeId_${requestScope.id}" id="identityTypeId_${requestScope.id}"  value="0"/>
			<input type="hidden" name="identitylineId_${i}" id="identitylineId_${requestScope.id}" value="${requestScope.id}"/>  
	  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddDatanew" id="identityAddImage_${requestScope.id}"  title="Add Record">
	 	 <span class="ui-icon ui-icon-plus"></span>
	   </a>	
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddDatanew"  id="identityEditImage_${requestScope.id}" style="display:none; cursor: pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		</a> 
		<a style="cursor: pointer; display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrownew"  id="identityDeleteImage_${requestScope.id}" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="identityWorkingImage_${requestScope.id}" style="display:none;" title="Working">
			<span class="processing"></span>
		</a>
	</td> 
	
</tr>
<script type="text/javascript"> 
$(function(){
	//Identity Detail adding 
     $('#idendityExpireDate').datepick();
	 clear:false;
	 
	 $(".identityaddDatanew").click(function(){ 
		slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		if($("#editCalendarVal").validationEngine({returnIsValid:true})){
				$("#identityAddImage_"+rowid).hide();
				$("#identityEditImage_"+rowid).hide();
				$("#identityDeleteImage_"+rowid).hide();
				$("#identityWorkingImage_"+rowid).show(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/company_identity_add.action",
					data:{id:rowid,addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $('#openFlag').val(1);
					}
			});
			
		}
		else{
			return false;
		}	
	});

	$(".identityeditDatanew").click(function(){
		 
		 slidetab=$(this).parent().parent().get(0);  
		 
		 if($("#editCalendarVal").validationEngine({returnIsValid:true})) {
			 
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#identityAddImage_"+rowid).hide();
					$("#identityEditImage_"+rowid).hide();
					$("#identityDeleteImage_"+rowid).hide();
					$("#identityWorkingImage_"+rowid).show(); 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/company_identity_add.action",
				 data:{id:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
	   			$('#email_'+rowid).text();
	   			
	   			var identityType= $('#identityType_'+rowid).text(); 
	   			var identityNumber=$('#identityNumber_'+rowid).text(); 
 				var idendityExpireDate=$('#identityExpireDate_'+rowid).text(); 
 				var identityIssuedPlace=$('#identityIssuedPlace_'+rowid).text();
 				var identityDescription=$('#identityDescription_'+rowid).text();
 				$('#identityType').text(identityType);
 				$('#identityNumber').val(identityNumber);
 				$('#idendityExpireDate').val(idendityExpireDate);
 				$('#identityIssuedPlace').val(identityIssuedPlace);
 				$('#identityDescription').val(identityDescription);
			    $('#openFlag').val(1);
			    $('.tempresult').html('');
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			 			
		 }
		 else{
			 return false;
		 }	
	});
	
	 
	 $(".identitydelrownew").click(function(){  slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		 var lineId=$('#identitylineId_'+rowid).val();
		
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/person_identity_save.action", 
				 data:{id:lineId,addEditFlag:"D"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
					 $('.tempresult').html(result);   
					 if(result!=null){
						 $("#identityfieldrow_"+rowid).remove(); 
			        		//To reset sequence 
			        		var identitycount=Number($('#identitycount').val()); 
	         				  identitycount-=1;
	         				  $('#identitycount').val(identitycount);
			        		var i=0;
			     			$('.identityrowid').each(function(){  
								i=i+1;
								$($($(this).children().get(4)).children().get(1)).val(i); 
		   					 }); 
					     }  
				},
				error:function(result){
					 $('.tempresult').html(result);   
					 return false;
				}
					
			 });
			 $('.tempresult').html('');
		 
	 });
//event.preventDefault(); 			 
});	
</script>	