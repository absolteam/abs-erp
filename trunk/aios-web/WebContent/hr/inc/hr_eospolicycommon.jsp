<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="width100 float-left" id="hrm">
	<div class="width50 float-left">
		<fieldset style="min-height: 100px;">
				<legend>Service Details</legend>
			<div class="width100 viewlabel">
				<label class="width30">Notice Period</label>
				<label class="width60" id="noticeDaysTemp">${SERVICE_DETAILS.noticeDays}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Service Start</label>
				<label class="width60">${SERVICE_DETAILS.fromDateView}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Service Ends</label>
				<label class="width60">${SERVICE_DETAILS.toDateView}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Total Days</label>
				<label class="width60">${SERVICE_DETAILS.totalDays}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Number of Months</label>
				<label class="width60">${SERVICE_DETAILS.totalMonths}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Total Years</label>
				<label class="width60">${SERVICE_DETAILS.totalYears}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Gratuity Days </label>
				<label class="width60" id="gratuityDaysTemp">${SERVICE_DETAILS.gratuityDays}</label>
			</div>	
			<div class="width100 viewlabel">
				<label class="width30">Basic Salary</label>
				<label class="width60">${SERVICE_DETAILS.basicAmount}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30" style="font-size: 16px;font-size: bold;">Total Gratuity</label>
				<label class="width60" style="font-size: 16px;font-size: bold;">${SERVICE_DETAILS.totalAmountView}</label>
			</div>
		</fieldset>
	</div>
	<div class="width50 float-left">
		<fieldset style="min-height: 100px;">
				<legend>EOS Policy</legend>
			<div class="width100 viewlabel">
				<label class="width30">Name</label>
				<label class="width60">${SERVICE_DETAILS.eosPolicy.policyName}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Type</label>
				<label class="width60">${SERVICE_DETAILS.typeView}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Policy Service Year Range</label>
				<label class="width60">${SERVICE_DETAILS.eosPolicy.serviceYears}</label>
			</div>
			<div class="width100 viewlabel">
				<label class="width30">Paid Days</label>
				<label class="width60">${SERVICE_DETAILS.eosPolicy.payDays} (Per Year)</label>
			</div>
			
		</fieldset>
	</div>
</div>