<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>

<script type="text/javascript"> 
var shiftLineDetail="";
var globalRowId=null;
$(document).ready(function(){ 
		manupulateShiftLastRow();

		var workingShiftId=Number($("#workingShiftId").val());
		if(workingShiftId>0){
			editDataSelectCall();
		}

	
		  
		$jquery("#shift-details").validationEngine('attach');
		
			$('#shift-discard').click(function(){ 
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/shift_mapping.action", 
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){ 
							$("#main-wrapper").html(result);  
						}
				 });
			 });

			 $('#shift-save').click(function(){
				 var workingShiftId=Number($('#workingShiftId').val()); 
				 if($jquery("#shift-details").validationEngine('validate')){   
					 shiftLineDetail=getShiftLineDetails(); 
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/shift_job_assignment.action", 
							data:{
									workingShiftId:workingShiftId,shiftLineDetail:shiftLineDetail
								},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){ 
								$(".tempresult").html(result);
								 var message=$('.tempresult').html(); 
								 if(message.trim()=="SUCCESS"){
									 $.ajax({
											type:"POST",
											url:"<%=request.getContextPath()%>/shift_mapping.action", 
										 	async: false,
										    dataType: "html",
										    cache: false,
											success:function(result){
												$("#main-wrapper").html(result); 
												$('#success_message').hide().html(message).slideDown(1000);
											}
									 });
								 }
								 else{
									 $('#page-error').hide().html(message).slideDown(1000);
									 return false;
								 }
							}
					 });
				 }
				 else{
					return false;
				} 
			 });
			 
			 $('.jobs-popup').live('click',function(){
			      $('.ui-dialog-titlebar').remove();  
			      $('#job-common-popup').dialog('open');
			      var workingShiftId=$('#workingShiftId').val();
			      var rowId=getRowId($(this).attr('id')); 
			         $.ajax({
			            type:"POST",
			            url:"<%=request.getContextPath()%>/get_jobs_for_shift.action",
			            async: false,
			            data:{id:rowId,workingShiftId:workingShiftId},
			            dataType: "html",
			            cache: false,
			            success:function(result){
			                 $('.job-common-result').html(result);
			                 return false;
			            },
			            error:function(result){
			                 $('.job-common-result').html(result);
			            }
			        });
			        return false;
			}); 
			 
			 $('.jobs-assignment-popup').live('click',function(){
			      $('.ui-dialog-titlebar').remove();  
			      $('#job-common-popup').dialog('open');
			      var rowId=getRowId($(this).attr('id')); 
			      globalRowId=rowId;
			         $.ajax({
			            type:"POST",
			            url:"<%=request.getContextPath()%>/common_job_assignment_list.action",
			            async: false,
			            dataType: "html",
			            cache: false,
			            success:function(result){
			                 $('.job-common-result').html(result);
			                 return false;
			            },
			            error:function(result){
			                 $('.job-common-result').html(result);
			            }
			        });
			        return false;
			}); 
			 
			 

			$('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:900,
				height:600,
				bgiframe: false,
				modal: true 
			});
			
			 $('.startDate,.endDate').datepick({
				 onSelect: customRanges,showTrigger: '#calImg'});
		     
			//On Change Events
			$('.startDate').live('change',function(){
				if($(this).val()!=""){
					var rowId=getRowId($(this).attr('id')); 
					triggerShiftAddRow(rowId);
					return false;
				} 
				else{
					return false;
				}
			});	
			$('.endDate').live('change',function(){
				if($(this).val()!=""){
					var rowId=getRowId($(this).attr('id')); 
					triggerShiftAddRow(rowId);
					return false;
				} 
				else{
					return false;
				}
			});	
			
			$('.addrowsS').click(function(){ 
				  var i=Number(1); 
				  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
				  id=id+1;   
				  $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/shift_job_addrow.action",
														async : false,
														data : {
															id : id
														},
														dataType : "html",
														cache : false,
														success : function(
																result) {
															$('.tabS tr:last')
																	.before(
																			result);
															if ($(".tabS")
																	.height() > 255)
																$(".tabS")
																		.css(
																				{
																					"overflow-x" : "hidden",
																					"overflow-y" : "auto"
																				});
															$('.rowidS')
																	.each(
																			function() {
																				var rowId = getRowId($(
																						this)
																						.attr(
																								'id'));
																				$(
																						'#lineIdS_'
																								+ rowId)
																						.html(
																								i);
																				i = i + 1;
																			});
															return false;
														}
													});
											return false;
										});

						$('.delrowS').live('click', function() {
							slidetab = $(this).parent().parent().get(0);
							$(slidetab).remove();
							var i = 1;
							$('.rowidS').each(function() {
								var rowId = getRowId($(this).attr('id'));
								$('#lineIdS_' + rowId).html(i);
								i = i + 1;
							});
							return false;
						});
					});
	function customRanges(dates) {
		var rowId = getRowId(this.id);
		if (this.id == 'startDate_' + rowId) {
			$('#endDate_' + rowId).datepick('option', 'minDate',
					dates[0] || null);
			if ($(this).val() != "") {
				triggerShiftAddRow(rowId);
				return false;
			} else {
				return false;
			}
		} else {
			$('#startDate_' + rowId).datepick('option', 'maxDate',
					dates[0] || null);
			if ($(this).val() != "") {
				triggerShiftAddRow(rowId);
				return false;
			} else {
				return false;
			}
		}
	}

	function editDataSelectCall() {

		$('.rowidS').each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					

				});

	}
	function manupulateShiftLastRow() {
		var hiddenSize = 0;
		$($(".tabS>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tabS>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tabS>tr:last').removeAttr('id');
		$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');
		$($('.tabS>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function triggerShiftAddRow(rowId) {
		var jobId = $('#jobId_' + rowId).val();
		var jobAssignmentId = $('#jobAssignmentId_' + rowId).val();
		var startDate = $('#startDate_' + rowId).val();
		var endDate = $('#endDate_' + rowId).val();
		var nexttab = $('#fieldrowS_' + rowId).next();
		if (((jobId != null && jobId != "") || (jobAssignmentId!=null && jobAssignmentId!="")) 
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImageS_' + rowId).show();
			$('.addrowsS').trigger('click');
			return false;
		} else {
			return false;
		}
	}
	function getShiftLineDetails() {
		shiftLineDetail = "";
		var jobShiftIds = new Array();
		var shiftIds = new Array();
		var startDates = new Array();
		var endDates = new Array();
		var jobIds = new Array();
		var jobAssignmentIds = new Array();
		$('.rowidS').each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					var jobShiftId = $('#jobShiftId_' + rowId).val();
					if (jobShiftId == null || jobShiftId == ''
							|| jobShiftId == 0 || jobShiftId == 'undefined')
						jobShiftId = -1;
					var shiftId = $('#workingShiftId').val();
					var startDate = $('#startDate_' + rowId).val();
					var endDate = $('#endDate_' + rowId).val();
					if(startDate==null || startDate=="")
						startDate="-1";
					if(endDate==null || endDate=="")
						endDate="-1";
					
					var jobId = $('#jobId_' + rowId).val();
					if (jobId == null || jobId == '' || jobId == 0
							|| jobId == 'undefined')
						jobId = -1;
					
					var jobAssignmentId = $('#jobAssignmentId_' + rowId).val();
					if (jobAssignmentId == null || jobAssignmentId == '' || jobAssignmentId == 0
							|| jobAssignmentId == 'undefined')
						jobAssignmentId = -1;
					//var statusShift=$('#statusShift_'+rowId).val();
					if (typeof shiftId != 'undefined' && shiftId != null
							&& shiftId != "" && (jobAssignmentId>0 || jobId>0)) {
						jobShiftIds.push(jobShiftId);
						shiftIds.push(shiftId);
						startDates.push(startDate);
						endDates.push(endDate);
						jobIds.push(jobId);
						jobAssignmentIds.push(jobAssignmentId);
						//statuses.push(statusShift);
					}
				});
		for ( var j = 0; j < startDates.length; j++) {
			shiftLineDetail += jobShiftIds[j] + "@" + shiftIds[j] + "@"
					+ startDates[j] + "@" + endDates[j] + "@"
					+ jobIds[j]+ "@"
					+ jobAssignmentIds[j];
			
				shiftLineDetail += "##";
		}
		return shiftLineDetail;
	}
	function getJobAssignmentFromObject(jobAssignment){
		$("#employees_"+globalRowId).html(jobAssignment.personName);
		$("#jobAssignmentId_"+globalRowId).val(jobAssignment.jobAssignmentId);
		triggerShiftAddRow(globalRowId);
	}
</script>

<body>
	<div id="main-content">
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
			style="min-height: 99%;">
			<div id="page-error" class="response-msg error ui-corner-all width90"
				style="display: none;">
				<span>Process Failure!</span>
			</div>
			<div class="tempresult" style="display: none;"></div>
		<!-- 	<div id="hrm" class="width98" style="margin: 5px;">
				<fieldset>
					<legend>Assiging Type</legend>
					<div class="width30 float-left">
						<input name="shiftType" id="shift" type="radio" checked="checked">
						Employee Into Shift
					</div>
					<div class="width30 float-left">
						<input name="shiftType" id="employee" type="radio">Shift
						Into Employee
					</div>
				</fieldset>
			</div> -->
			<div class="width98 float-left" id="hrm">
				<form id="shift-details" name="shift-details" style="position: relative;">
					<fieldset style="min-height: 100px;">
						<legend>Shift Information</legend>
						<input name="workingShiftId" id="workingShiftId" type="hidden"
							value="${WORKING_SHIFT.workingShiftId}">
						<div class="width48 float-left">
							<div>
								<label class="width30">Shift Name : </label> <label
									class="width50">${WORKING_SHIFT.workingShiftName}</label>
							</div>
							<div>
								<label class="width30">Start Time : </label> <label
									class="width50">${WORKING_SHIFT.startTime}</label>
							</div>
							<div>
								<label class="width30">End Time : </label> <label
									class="width50">${WORKING_SHIFT.endTime}</label>
							</div>
							<c:if test="${WORKING_SHIFT.irregularDay ne null and WORKING_SHIFT.irregularDay ne ''}">
								<div>
									<label class="width30">Day : </label> <label
										class="width50">${WORKING_SHIFT.irregularDay}</label>
								</div>
							</c:if>
						</div>
						<div class="width48 float-left">
							<div>
								<label class="width30">Late In Time : </label> <label
									class="width50">${WORKING_SHIFT.lateAttendance}</label>
							</div>
							<div>
								<label class="width30">Sevior Late Time : </label> <label
									class="width50">${WORKING_SHIFT.seviorLateAttendance}</label>
							</div>
							<div>
								<label class="width30">Buffer Min(s) : </label> <label
									class="width50">${WORKING_SHIFT.bufferTime}</label>
							</div>
							<div>
								<label class="width30">Break Hour(s) : </label> <label
									class="width50">${WORKING_SHIFT.breakHours}</label>
							</div>

						</div>
					</fieldset>
					<div class="width100 float-left">
						<fieldset>
							<legend>Employee Details</legend>
							<div id="hrm" class="hastable width100">
								<table id="hastab2" class="width100">
									<thead>
										<tr>
											<th style="width: 5%">Job Name</th>
											<th style="width: 5%">Employee(s)</th>
											<th style="width: 5%"><fmt:message
													key="hr.job.periodstartdate" />
											</th>
											<th style="width: 5%"><fmt:message
													key="hr.job.periodenddate" />
											</th>
											<th style="width: 5%;display:none;"><fmt:message
													key="hr.job.irregularday" />
											</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" />
											</th>
										</tr>
									</thead>
									<tbody class="tabS">
										<c:choose>
											<c:when
												test="${JOB_SHIFT_LIST ne null && JOB_SHIFT_LIST ne '' && fn:length(JOB_SHIFT_LIST)>0}">
												<c:forEach var="jobShift" items="${JOB_SHIFT_LIST}"
													varStatus="status1">
													<tr class="rowidS" id="fieldrowS_${status1.index+1}">
														<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td><span id="jobName_${status1.index+1}"
															class="float-left width30">
																${jobShift.job.jobName}(${jobShift.job.jobNumber}) </span> <span
															class="button float-right" id="job_${status1.index+1}"
															style="position: relative; top: 6px;"> <a
																style="cursor: pointer;"
																id="jobpopup_${status1.index+1}"
																class="btn ui-state-default ui-corner-all width40 jobs-popup">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
															type="hidden" name="jobId" id="jobId_${status1.index+1}"
															value="${jobShift.job.jobId}"
															class="width50 validate[required]" /></td>
														<td><span id="employees_${status1.index+1}"> 
															<c:choose>
																<c:when test="${jobShift.job ne null}">
																	<c:forEach
																		var="jobAssignment"
																		items="${jobShift.job.jobAssignments}"
																		varStatus="status2">
																		(${status2.index+1}) ${jobAssignment.person.firstName} ${jobAssignment.person.lastName},
																	</c:forEach> 
																</c:when>
																<c:when test="${jobShift.jobAssignment ne null}">
																	${jobShift.jobAssignment.person.firstName} ${jobShift.jobAssignment.person.lastName}
																</c:when>
															</c:choose>
															</span>
															<span class="button float-right" id="jobAssign_${status1.index+1}"  style="position: relative; top:6px;">
																<a style="cursor: pointer;" id="jobpopupassign_${status1.index+1}" class="btn ui-state-default ui-corner-all width40 jobs-assignment-popup"> 
																	<span class="ui-icon ui-icon-newwin">
																	</span> 
																</a>
															</span> 
															<input type="hidden" name="jobAssignmentId" id="jobAssignmentId_${status1.index+1}" value="${jobShift.jobAssignment.jobAssignmentId}" class="width50"/>
														</td>
														<td><c:choose>
																<c:when
																	test="${jobShift.effectiveStartDate ne null &&  jobShift.effectiveStartDate ne ''}">
																	<c:set var="startDate"
																		value="${jobShift.effectiveStartDate}" />
																	<%
																		String startDate = DateFormat
																									.convertDateToString(pageContext
																											.getAttribute("startDate")
																											.toString());
																	%>
																	<input type="text" class="width80 startDate"
																		readonly="readonly" name="startDate"
																		id="startDate_${status1.index+1}" style="border: 0px;"
																		value="<%=startDate%>" />
																</c:when>
																<c:otherwise>
																	<input type="text" class="width80 startDate"
																		readonly="readonly" name="startDate"
																		id="startDate_${status1.index+1}" style="border: 0px;" />
																</c:otherwise>
															</c:choose></td>
														<td><c:choose>
																<c:when
																	test="${jobShift.effectiveEndDate ne null &&  jobShift.effectiveEndDate ne ''}">
																	<c:set var="endDate"
																		value="${jobShift.effectiveEndDate}" />
																	<%
																		String endDate = DateFormat
																									.convertDateToString(pageContext
																											.getAttribute("endDate")
																											.toString());
																	%>
																	<input type="text" class="width80 endDate"
																		readonly="readonly" name="endDate"
																		id="endDate_${status1.index+1}" style="border: 0px;"
																		value="<%=endDate%>" />
																</c:when>
																<c:otherwise>
																	<input type="text" class="width80 endDate"
																		readonly="readonly" name="endDate"
																		id="endDate_${status1.index+1}" style="border: 0px;" />
																</c:otherwise>
															</c:choose></td>
														<td style="display: none;"><input type="hidden"
															id="jobShiftId_${status1.index+1}"
															value="${jobShift.jobShiftId}" style="border: 0px;" /></td>
														<td style="width: 0.01%;" class="opn_td"
															id="optionS_${status1.index+1}"><a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS"
															id="AddImageS_${status1.index+1}"
															style="display: none; cursor: pointer;"
															title="Add Record"> <span
																class="ui-icon ui-icon-plus"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS"
															id="EditImageS_${status1.index+1}"
															style="display: none; cursor: pointer;"
															title="Edit Record"> <span
																class="ui-icon ui-icon-wrench"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS"
															id="DeleteImageS_${status1.index+1}"
															style="cursor: pointer;" title="Delete Record"> <span
																class="ui-icon ui-icon-circle-close"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
															id="WorkingImageS_${status1.index+1}"
															style="display: none;" title="Working"> <span
																class="processing"></span> </a></td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>
										<c:forEach var="i" begin="${fn:length(JOB_SHIFT_LIST)+1}"
											end="${fn:length(JOB_SHIFT_LIST)+2}" step="1"
											varStatus="status">
											<tr class="rowidS" id="fieldrowS_${i}">
												<td id="lineIdS_${i}" style="display: none;">${i}</td>
												<td><span id="jobName_${i}" class="float-left width30">
												</span> <span class="button float-right" id="job_${i}"
													style="position: relative; top: 6px;"> <a
														style="cursor: pointer;" id="jobpopup_${i}"
														class="btn ui-state-default ui-corner-all width40 jobs-popup">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="jobId" id="jobId_${i}" value=""
													class="width50 validate[required]" /></td>
												<td><span id="employees_${i}"> </span>
													<span class="button float-right" id="jobAssign_${i}"  style="position: relative; top:6px;">
														<a style="cursor: pointer;" id="jobpopupassign_${i}" class="btn ui-state-default ui-corner-all width40 jobs-assignment-popup"> 
															<span class="ui-icon ui-icon-newwin">
															</span> 
														</a>
													</span> 
													<input type="hidden" name="jobAssignmentId" id="jobAssignmentId_${i}" value="" class="width50"/>
												</td>
												<td><input type="text" class="width80 startDate"
													readonly="readonly" name="startDate" id="startDate_${i}"
													style="border: 0px;" /></td>
												<td><input type="text" class="width80 endDate"
													readonly="readonly" name="endDate" id="endDate_${i}"
													style="border: 0px;" /></td>

												<td style="display: none;"><input type="hidden"
													id="jobShiftId_${i}" style="border: 0px;" /></td>
												<td style="width: 0.01%;" class="opn_td" id="optionS_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS"
													id="AddImageS_${i}" style="display: none; cursor: pointer;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS"
													id="EditImageS_${i}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS"
													id="DeleteImageS_${i}"
													style="display: none; cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImageS_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
				</form>
				<div class="clearfix"></div>
				<div
					class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div
						class="portlet-header ui-widget-header float-right shift-discard"
						id="shift-discard">cancel</div>
					<div class="portlet-header ui-widget-header float-right shift-save"
						id="shift-save">save</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div id="sub-content" class="width100 float-left"></div>

			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span>
					</a>
				</div>
				<div id="job-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="job-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>

			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrowsS"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
		</div>
	</div>
</body>
</html>