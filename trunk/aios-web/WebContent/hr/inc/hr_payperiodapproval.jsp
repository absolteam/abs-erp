<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>

<style>
.ui-multiselect .count {
	padding: 0 !important;
}
</style>

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
var payPeriodId=0;
$(document).ready(function(){ 

		payPeriodId=Number($("#payPeriodId").val());
		if(payPeriodId>0){
			periodTransactionLoad();
			$('#yearStartDate').attr("disabled",true);
			$('#yearEndDate').attr("disabled",true);
			if($('#isDefalutTemp').val()=='true')
				$('#isDefalut').attr("checked",true);
			else
				$('#isDefalut').attr("checked",false);
			
			if($('#isActiveTemp').val()=='true')
				$('#isActive').attr("checked","checked");
			else
				$('#isActive').attr("checked",false);
			
		}else{
			
		}
		
		
		$jquery("#interview-details").validationEngine('attach');
		  
			
		$('#interview-discard').click(function(){ 
			returnCallTOList();
			
		 });
		
		
		
		
		

	 $('#interview-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
		 	
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(payPeriodId>0)
						successMessage="Successfully Modified";
					var periodName=$('#periodName').val();
					var startDate=$('#yearStartDate').val();
					var endDate=$('#yearEndDate').val();
					var isDefault=$('#isDefault').attr("checked");
					var isActive=$('#isActive').attr("checked");
					
					try {
						if ($jquery("#interview-details").validationEngine('validate')) {
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/pay_period_save.action", 
							 	async: false,
							 	data: {payPeriodId:payPeriodId,periodName:periodName,
							 		startDate:startDate,endDate:endDate,isDefault:isDefault,isActive:isActive},
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					} catch (e) {

					}

				});
	 
		 $('#getTransaction').click(function(){ 
		 	periodTransactionLoad();
			
		 });
		
		$('#yearStartDate,#yearEndDate').datepick({
			 onSelect: customRanges,showTrigger: '#calImg'});		
		 
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pay_period_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	
	function periodTransactionLoad(){
		var startDate=$('#yearStartDate').val();
		var endDate=$('#yearEndDate').val();
		var payPeriodId=$('#payPeriodId').val();
		if(startDate!=null && startDate!="" && endDate!=null && endDate!=""){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pay_period_transaction_list.action", 
			 	async: false,
			 	data:{payPeriodId:payPeriodId,startDate:startDate,endDate:endDate},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#schedule-result").html(result);  
				}
		 });
		}
	}

	

	function autoSelectTheMultiList(source, target) {
		var idsArray = new Array();
		var idlist = $('#' + source).val();
		idsArray = idlist.split(',');
		$('#' + target + ' option').each(function() {
			var txt = $(this).val().trim();
			if ($.inArray(txt, idsArray) >= 0)
				$(this).attr("selected", "selected");
		});

	}
	 function customRanges(dates) {
		 periodTransactionLoad();
			if (this.id == 'yearStartDate') {
				$('#yearEndDate').datepick('option', 'minDate', dates[0] || null);  
				if($(this).val()!=""){
					return false;
				} 
				else{
					return false;
				}
			}
			else{
				$('#yearStartDate').datepick('option', 'maxDate', dates[0] || null); 
				if($(this).val()!=""){
					return false;
				} 
				else{
					return false;
				}
			} 
		}
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay Period Information
			
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="interview-details" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden" name="payPeriod.payPeriodId"
									id="payPeriodId" value="${PAY_PERIOD_INFO.payPeriodId}"
									class="width50 " />
								<fieldset style="min-height: 100px;">
									<legend>Schedule Information</legend>
									<%-- <div class="width50 float-left">
										<div>
											<label class="width30">Month Day</label> <select
												class="width50 monthDay" id="monthDay">
												<c:forEach var="i" begin="1" end="28" step="1"
													varStatus="status">
													<option value="${i}">${i}</option>
												</c:forEach>
											</select> <input type="hidden" id="monthDayTemp"
												value="${PAY_PERIOD_INFO.monthDay}" style="border: 0px;" />
										</div>
										<div>
									  	<label class="width30">Week Day</label>
									  	<select class="width50 weekDay" id="weekDay" >
											<option value="">Select</option>
											<option value="SUNDAY">SUNDAY</option>
											<option value="MONDAY">MONDAY</option>
											<option value="TUESDAY">TUESDAY</option>
											<option value="WEDNESDAY">WEDNESDAY</option>
											<option value="THURSDAY">THURSDAY</option>
											<option value="FRIDAY">FRIDAY</option>
											<option value="SATURDAY">SATURDAY</option>
										</select>
										<input type="hidden" id="weekDayTemp" value="${PAY_PERIOD_INFO.weekDay}" style="border:0px;"/>
									  </div>
									</div> --%>
									<div class="width50 float-left">
										<div>
											<label class="width30" for="periodName">Period Name
												<span class="mandatory">*</span>
											</label> <input type="text" name="periodName"
												id="periodName" value="${PAY_PERIOD_INFO.periodName}"
												class="width50 validate[required]" />
										</div>
										<div>
											<label class="width30" for="yearStartDate">Year Start
												Date<span class="mandatory">*</span>
											</label> <input type="text" readonly="readonly" name="yearStartDate"
												id="yearStartDate" value="${PAY_PERIOD_INFO.startDate}"
												class="width50 validate[required]" />
										</div>
										<div>
											<label class="width30" for="yearEndDate">Year End
												Date<span class="mandatory">*</span></label> <input type="text" readonly="readonly"
												name="yearEndDate" id="yearEndDate"
												value="${PAY_PERIOD_INFO.endDate}" class="width50 validate[required]" />
										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<input type="checkbox" id="isActive" class="width10" checked="checked">
											<label class="width30">Active</label> <input type="hidden"
												id="isActiveTemp" class="width10"
												value="${PAY_PERIOD_INFO.isActive}">
										</div>
										<div>
											<input type="checkbox" id="isDefault" class="width10">
											<label class="width30">Default</label> <input type="hidden"
												id="isDefaultTemp" class="width10"
												value="${PAY_PERIOD_INFO.isDefault}">
										</div>
										<div id="repyament-button-div" style="display: none;"
											class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">

											<div
												class="portlet-header ui-widget-header float-right getTransaction"
												id="getTransaction">Get Schedule</div>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="width100 float-left">
								<fieldset>
									<legend>Period Details</legend>
									<div id="hrm" class="hastable width100">
										<table id="hastab2" class="width100">
											<thead>
												<tr>
													<th style="width: 5%">S.No.</th>
													<th style="width: 5%">Start Date</th>
													<th style="width: 5%">End Date</th>
													<th style="width: 5%">Pay Month</th>
													<th style="width: 5%">Pay Week</th>
												</tr>
											</thead>
											<tbody class="tabS" id="schedule-result">
											
											</tbody>
										</table>
									</div>
								</fieldset>
							</div>
						</div>

					</form>
					<div class="clearfix"></div>
					<div id="interview_process_div"></div>
					
				</div>

			</div>
		</div>
	</div>
</body>
</html>