<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var designationId=0;var designationName=''; 
$(function(){   
	$('#Designation').dataTable({ 
		"sAjaxSource": "get_job_designation_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Designation ID', "bVisible": false},
			{ "sTitle": 'Designation'},
			{ "sTitle": 'Grade'},
			{ "sTitle": 'Description'},
			
		],
		"sScrollY": $("#main-content").height() - 230,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Designation').dataTable();
	var commonParam=null;
	/* Click event handler */
	$('#Designation tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        designationId=aData[0];
	        designationName=aData[1]+" ["+aData[2]+"]";
	        //Manipulate the addition infomation to send to form
	        commonParam=aData[1]+"@&"+aData[2];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        designationId=aData[0];
	        designationName=aData[1]+" ["+aData[2]+"]";
	        commonParam=aData[1]+"@&"+aData[2];
	    }
	});
	$('#Designation tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        designationId=aData[0];
	        personName=aData[1]+" ["+aData[2]+"]";
	        commonParam=aData[1]+"@&"+aData[2];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        designationId=aData[0];
	        designationName=aData[1]+" ["+aData[2]+"]";
	        commonParam=aData[1]+"@&"+aData[2];
	    }
		  
		  designationPopupResult(designationId,designationName,commonParam);
		$('#common-popup').dialog("close");

	});
	
	$('#desigination-close').click(function () { 
		$('#common-popup').dialog("close");
	});
});
</script>
<div id="main-content">  
 	<div id="trans_combination_accounts">
		<table class="display" id="Designation"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="desigination-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div>  
</div>