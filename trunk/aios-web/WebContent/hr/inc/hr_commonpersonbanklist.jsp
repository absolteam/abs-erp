<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var locationId=0;var locationName=''; 
$(function(){ 
	var personId=Number($("#PERSON_ID").val());
	if(personId>0)
		personId=Number(personId);
	else
		personId=0;
	$('#PersonBank').dataTable({ 
		"sAjaxSource": "get_person_bank_json.action?personId="+personId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'PersonBank ID', "bVisible": false},
			{ "sTitle": 'Person'},
			{ "sTitle": 'Account Number'},
			{ "sTitle": 'Bank Name'},
			{ "sTitle": 'IBAN'},
			{ "sTitle": 'Routing Code'},
			{ "sTitle": 'Branch Name'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#PersonBank').dataTable();

	/* Click event handler */
	$('#PersonBank tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[3]+"["+aData[2]+"]";
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[3]+"["+aData[2]+"]";
	    }
	});
	$('#PersonBank tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[3]+"["+aData[2]+"]";
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[3]+"["+aData[2]+"]";
	    }
		var commaseparatedValue="";
		personBankPopupCall(locationId,locationName,commaseparatedValue);
		$('#job-common-popup').dialog("close");


	});
});
</script>
<input type="hidden" id="rowId" value="${rowId}"/>
<input type="hidden" id="PERSON_ID" value="${PERSON_ID}"/>
<div id="main-content"> 
 	<div id="trans_combination_accounts">
		<table class="display" id="PersonBank"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="hrbank-list-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div> 
</div>