<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tr class="rowidS" id="fieldrowS_${rowId}">
	<td>
		<input type="text" class="width90 startDate" readonly="readonly" name="startDate" id="startDate_${rowId}" style="border:0px;"/>
	</td> 
	<td>
		
		<input type="text" class="width90 endDate" readonly="readonly" name="endDate" id="endDate_${rowId}" style="border:0px;"/>
				
	</td> 
	<td>
		<select class="width90 holidayType" id="holidayType_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach items="${HOLIDAY_TYPE_LIST}" var="offList" varStatus="status4">
				<option value="${offList.holidayTypeId}">${offList.holidayType}</option>
			</c:forEach>
		</select>
		<input type="hidden" id="holidayTypeTemp_${rowId}" value="" style="border:0px;"/>
	</td> 
	<td>
		<textarea id="description_${rowId}" class="width90 " style="border:0px;"></textarea>
	</td>
	<td style="display:none;">
		<input type="hidden" id="employeeCalendarPeriodId_${rowId}" value="" style="border:0px;"/>
	</td>
	 <td style="width:0.01%;" class="opn_td" id="optionS_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>    
</tr>
<script type="text/javascript"> 
$(document).ready(function(){  
	 $('.startDate,.endDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
});
</script>