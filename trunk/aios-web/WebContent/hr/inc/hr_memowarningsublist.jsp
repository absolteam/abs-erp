<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:choose>
	<c:when test="${requestScope.SUB_TYPE!=null}">
		<c:forEach items="${requestScope.SUB_TYPE}" var="nltlst">
			<option value="${nltlst.dataId}">${nltlst.displayName} </option>
		</c:forEach>
	</c:when>
	<c:when test="${requestScope.EMPLOYEE_LIST!=null}">
		<c:forEach items="${requestScope.EMPLOYEE_LIST}" var="emplst">
			<option value="${emplst.personId}">${emplst.firstName} ${emplst.lastName}[${emplst.personNumber}] </option>
		</c:forEach>
	</c:when>	
	<c:when test="${requestScope.DEPARTMENT_LIST!=null}">
		<c:forEach items="${requestScope.DEPARTMENT_LIST}" var="deptLst">
			<option value="${deptLst.departmentId}">${deptLst.departmentName} </option>
		</c:forEach>
	</c:when>	
</c:choose>