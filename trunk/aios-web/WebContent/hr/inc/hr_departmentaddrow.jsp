<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowidA" id="fieldrowA_${rowId}">
	<td id="lineIdA_${rowId}" style="display: none;">${rowId}</td>
	<td>
		<%-- <select class="width90 department" id="department_${i}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="department" items="${DEPARTMENT_LIST}" varStatus="statusL">
				<option value="${department.departmentId}">${department.departmentName}</option>
			</c:forEach>
		</select> --%>
		<input type="text" name="department_${rowId}" id="department_${rowId}" class="department  validate[required] width80" value=""/>  
		<span class="button float-right" id="location_${rowId}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="departmentpopup_${rowId}" class="btn ui-state-default ui-corner-all width100 department-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span> 
		<input type="hidden" name="departmentId_${rowId}" id="departmentId_${rowId}" class="departmentId" value=""/>
		<input type="hidden" name="cmdDeptLocId_${rowId}" id="cmdDeptLocId_${rowId}" class="cmdDeptLocId" value=""/>
	</td> 
	<td>
		<select class="width90 parentrDepatment" id="parentDepartment_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="parentrDepatment" items="${DEPARTMENT_LIST}" varStatus="statusL">
				<option value="${parentrDepatment.departmentId}">${parentrDepatment.departmentName}</option>
			</c:forEach>
		</select>
	</td>
	<td>
		<textarea id="description_${rowId}" class="description width90"></textarea>
	</td>
	<td>
		<span id="locationName_${rowId}" class="float-left location">
		</span>
		<span class="button float-right" id="location_${rowId}"  style="position: relative;">
			<a style="cursor: pointer;" id="locationpopup_${rowId}" class="btn ui-state-default ui-corner-all width100 location-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span> 
		<input type="hidden" name="locationId_${rowId}" id="locationId_${rowId}" class="locationId" value=""/>  
	</td> 
	<td class="codecombination_info" id="codecombination_${rowId}"
		style="cursor: pointer;"><input type="hidden"
		name="combinationId_${rowId}" id="combinationId_${rowId}" /> <input
		type="text" name="codeCombination_${rowId}" readonly="readonly"
		id="codeCombination_${rowId}" class="codeComb width80">
		<span class="button" id="codeID_${rowId}"> <a
			style="cursor: pointer;"
			class="btn ui-state-default ui-corner-all codecombination-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	 <td style="width:0.01%;" class="opn_td" id="optionA_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataA" id="AddImageA_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataA" id="EditImageA_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowA" id="DeleteImageA_${rowId}" style="cursor:pointer;display:none;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageA_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>
</tr>
 <script type="text/javascript"> 
 $(document).ready(function(){ 
$('.parentrDepatment').combobox({ 
		 selected: function(event, ui){ 
			 
		 }
	 });
});
</script>