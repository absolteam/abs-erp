<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<c:choose>
	<c:when
		test="${PAY_PERIOD_TRANSACTION ne null && PAY_PERIOD_TRANSACTION ne '' && fn:length(PAY_PERIOD_TRANSACTION)>0}">
		<c:forEach var="paym"
			items="${PAY_PERIOD_TRANSACTION}" varStatus="status1">
			<tr class="rowidS" id="fieldrowS_${status1.index+1}">
				<td id="lineIdS_${status1.index+1}"
					style="display: none;">${status1.index+1}</td>
				<td>${status1.index+1}</td>
				<td>${paym.startDate}</td>
				<td>${paym.endDate}</td>
				<td>${paym.payMonth}</td>
				<td>${paym.payWeek}</td>
				<c:choose>
					<c:when test="${paym.freeze eq true}">
						<td><input type="checkbox" checked="checked" value="${paym.payPeriodTransactionId}" class="enablePeriod" id="enablePeriod_${status1.index+1}"/> </td>
					</c:when>
					<c:otherwise>
						<td><input type="checkbox" class="enablePeriod" value="${paym.payPeriodTransactionId}" id="enablePeriod_${status1.index+1}"/> </td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
	</c:when>
</c:choose>
	