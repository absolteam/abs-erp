<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style>
.hr_main_content{
	overflow-y: hidden;
}
</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var accessCode=null;
var commonRowId=0;
var rowId=null;
var payLineDetail="";
$(document).ready(function(){  
	

	
	
	$('#effectiveDate,#requestedDate').datepick({
		 onSelect: findGradivityAndServicePeriod,showTrigger: '#calImg'});
	
	
	

	$jquery("#JOBDetails").validationEngine('attach');
	
	$('.promotion_cancel').click(function(){
		listLoadCall();
		 return false;
	});
	
	$('#effectiveDate').change(function(){
		findGradivityAndServicePeriod();

 		 return false;
	});
	
	
	//Save & discard process
	$('.promotion_save').click(function(){ 
		if($jquery("#JOBDetails").validationEngine('validate')){  
			 
			var resignationTerminationId=Number($('#resignationTerminationId').val());
			var successMsg="";
			if(resignationTerminationId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var resignationTerminationId=$('#resignationTerminationId').val();
			var effectiveDate=$('#effectiveDate').val();
			var jobAssignmentId=$('#jobAssignmentId').val();
			var requestedDate=$('#requestedDate').val();
			var requestedPerson=$('#personId').val();
			var type=$('#type').val();
			var reason=$('#reason').val();
			var description=$('#description').val();
			var action=$('#action').val();
			var noticeDays=$('#noticeDays').val();
			var eligibleDays=0; //$('#eligibleDays').val();
			var givenDays=$('#givenDays').val();
			payLineDetail=getLineDetails();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/resignation_termination_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		resignationTerminationId:resignationTerminationId,
			 		jobAssignmentId:jobAssignmentId,
			 		effectiveDate:effectiveDate,
			 		requestedDate:requestedDate,
			 		requestedPerson:requestedPerson,
			 		type:type,
			 		reason:reason,
			 		description:description,
			 		action:action,
			 		noticeDays:noticeDays,
			 		eligibleDays:eligibleDays,
			 		givenDays:givenDays,
			 		payLineDetail:payLineDetail
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	 $('#employeepopup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var rowId=-1; 
         var personTypes="ALL";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 
	 $('#employeepopups').click(function(){
	      $('.ui-dialog-titlebar').remove();  
	      $('#job-common-popup').dialog('open');
	         $.ajax({
	            type:"POST",
	            url:"<%=request.getContextPath()%>/common_job_assignment_list.action",
	            async: false,
	            dataType: "html",
	            cache: false,
	            success:function(result){
	                 $('.job-common-result').html(result);
	                 return false;
	            },
	            error:function(result){
	                 $('.job-common-result').html(result);
	            }
	        });
	        return false;
	}); 

	$('#job-common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:550,
		bgiframe: false,
		modal: true 
	});
	 
	 
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

     $('#current_grade').click(function(){
 		loadCurrentEmployeePolicy();
 		 return false;
 	});
   
 	$('#current_job').click(function(){
		loadCurrentPayTemplate();
		 return false;
	});
 	
 	
	var resignationTerminationId=Number($("#resignationTerminationId").val());
	if(resignationTerminationId!=0){
		$('#type').val($('#typeTemp').val());
		$('#action').val($('#actionTemp').val());
		loadCurrentEmployeePolicy();
		findGradivityAndServicePeriod();

	}
	
     
});

function findGradivityAndServicePeriod(){
	var jobAssignmentId=$('#jobAssignmentId').val();
	var type=$('#type').val();
	var effectiveDate=$('#effectiveDate').val();
	if(jobAssignmentId!=null && type!=null && effectiveDate!=null 
			&& jobAssignmentId!='' && type!='' && effectiveDate!='' ){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/find_service_period.action", 
		 	async: false,
		 	data:{jobAssignmentId:jobAssignmentId,type:type,effectiveDate:effectiveDate},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#eos-policy-div").html(result);  
				$('#givenDays').val($('#gratuityDaysTemp').text());
				$('#noticeDays').val($('#noticeDaysTemp').text());
				return false;
			}
	 });
	}
	findFinalSettlement();
	return false;
}
function findFinalSettlement(){
	var jobAssignmentId=$('#jobAssignmentId').val();
	var resignationTerminationId=Number($("#resignationTerminationId").val());
	var type=$('#type').val();
	var effectiveDate=$('#effectiveDate').val();
	if(jobAssignmentId!=null && type!=null && effectiveDate!=null 
			&& jobAssignmentId!='' && type!='' && effectiveDate!='' ){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/process_final_settlement.action", 
		 	async: false,
		 	data:{resignationTerminationId:resignationTerminationId,jobAssignmentId:jobAssignmentId,type:type,effectiveDate:effectiveDate},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".final_settlement").html(result);  
				return false;
			}
	 });
	}
	return false;
}
function loadCurrentEmployeePolicy(){
	//clearTabs();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var openPositionId=0;
	if(jobAssignmentId!=null && jobAssignmentId>0)
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/grade_view.action", 
		 	async: false,
		 	data:{jobAssignmentId:jobAssignmentId,openPositionId:openPositionId},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".current_grade").html(result);  
				$('.discard').hide();
				return false;
			}
	 });
	else
		return false;
}

function loadCurrentPayTemplate(){
	//clearTabs();
	var jobAssignmentId=$('#jobAssignmentId').val();
	if(jobAssignmentId!=null && jobAssignmentId>0)
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/job_template_view.action", 
		 	async: false,
		 	data:{jobAssignmentId:jobAssignmentId},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".current_job").html(result);  
				$('.discard').hide();
				return false;
			}
	 });
	else
		return false;
}

function listLoadCall(){

	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/resignation_termination_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#main-wrapper").html(result);  
				return false;
			}
	 });
}
function customRanges(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function clearTabs(){
	$('.current_grade').html("<div>Information will be loaded on select the Employee</div>");
	$('.current_job').html("<div>Information will be loaded on select the Employee</div>");
}

function personPopupResult(personId,personName,commonParam){
	$('#employeeName').val(personName);
	$('#personId').val(personId);
}

function selectedOpenPositionResult(openPositionVO){
	$("#openPositionId").val(openPositionVO.openPositionId);
	$("#positionReferenceNumber").val(openPositionVO.positionReferenceNumber);
}

function jobPopupResult(id,name,commaseparatedValue){
	$('#jobName').val(name);
	$('#jobId').val(id);
}

function getJobAssignmentFromObject(aData){
	$('#assignmentName').val(aData.personName);
	$('#jobAssignmentId').val(aData.jobAssignmentId);
	findGradivityAndServicePeriod();
	return false;
}

</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Resignation Termination Information</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
		<c:choose>
			<c:when
				test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10"> Comment From :<span>
								${COMMENT_IFNO.name}</span>
						</label> <label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div>
			</c:when>
		</c:choose>
		  <form id="JOBDetails" class="" name="JOBDetails" method="post">
		  	<input type="hidden" name="resignationTerminationId" id="resignationTerminationId" value="${RESIGNATION.resignationTerminationId}" class="width50"/>
		  	<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="min-height: 150px;">
							<legend>End Of Service Information</legend>
							<div class="width50 float-left">
								<%-- <div class="float-left width100">
									<label class="width30" for="jobAssignmentId"><fmt:message
											key="hr.job.employees" /><span class="mandatory">*</span> </label> <select
										name="jobAssignmentId" id="jobAssignmentId"
										class="width51 validate[required]">
										<option value="">Select</option>
										<c:forEach items="${JOB_ASSIGNMENT_LIST}" var="job"
											varStatus="status">
											<option value="${job.jobAssignmentId}">${job.person.firstName}
												${job.person.lastName} [${job.jobAssignmentNumber}]</option>
										</c:forEach>
									</select>
									
								</div> --%>
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;" ><fmt:message key="hr.attendance.employee"/><span class="mandatory">*</span> : </label>
									<input type="text" readonly="readonly"  id="assignmentName" class="float-left width50 validate[required]" 
									value="${RESIGNATION.jobAssignment.person.firstName} ${RESIGNATION.jobAssignment.person.lastName}"/>
									
									<span class="button" id="employee"  style="position: relative; top:6px;">
										<a style="cursor: pointer;" id="employeepopups" class="btn ui-state-default ui-corner-all width10 employee-popup"> 
											<span class="ui-icon ui-icon-newwin">
											</span> 
										</a>
									</span> 
									<input type="hidden" id="jobAssignmentId"  class="jobAssignmentId"  value="${RESIGNATION.jobAssignment.jobAssignmentId}"/>
									
								</div>  
								
								
								<div class="float-left width100">
									<label class="width30">Type</label>
									<div>
										<select id=type
											name="type"
											class="width51 ">
											<c:forEach items="${RESIGNATION_TYPE}" var="gen">
												<option value="${gen.key}">${gen.value}
												</option>
											</c:forEach>
										</select>  <input
											type="hidden" name="typeTemp" id="typeTemp"
											value="${RESIGNATION.type}" />
									</div>
								</div>
								<div class="float-left width100">
									<label class="width30">Action</label>
									<div>
										<select id=action
											name="action"
											class="width51 ">
											<c:forEach items="${ACTIONS}" var="gen">
												<option value="${gen.key}">${gen.value}
												</option>
											</c:forEach>
										</select>  <input
											type="hidden" name="actionTemp" id="actionTemp"
											value="${RESIGNATION.action}" />
									</div>
							</div>
							<div class="float-left width100">
				      	  		<label class="width30">Requested Person<span style="color:red">*</span></label>
								
								<c:choose>
					      	  		<c:when test="${RESIGNATION.personByRequestedBy ne null}">
										<input type="text" id="employeeName" readonly="readonly" class=" width50 validate[required]"
									value="${RESIGNATION.personByRequestedBy.firstName} ${RESIGNATION.personByRequestedBy.lastName} [${RESIGNATION.personByRequestedBy.personNumber}]"/>
									</c:when>
									<c:otherwise>
										<input type="text" id="employeeName" readonly="readonly" class=" width50 validate[required]"
										value=""/>
									</c:otherwise>
								</c:choose>
								<span class="button" id="employee"  style="position: relative; top:0px;">
									<a style="cursor: pointer;" id="employeepopup" class="btn ui-state-default ui-corner-all width10"> 
										<span class="ui-icon ui-icon-newwin">
										</span> 
									</a>
								</span> 
								<input type="hidden" id="personId" value="${RESIGNATION.personByRequestedBy.personId}" class="personId" />
							</div>
							
							<div>
								<label class="width30" for="requestedDate">Requested Date<span
									style="color: red">*</span> 
								</label> <input type="text" readonly="readonly" name="requestedDate"
									id="requestedDate" value="${RESIGNATION.createdDateView}"
									class="width50 validate[required]" />
							</div>	
							<div>
								<label class="width30" for="effectiveDate">Effective Date<span
									style="color: red">*</span> 
									
								</label> <input type="text" readonly="readonly" name="effectiveDate"
									id="effectiveDate" value="${RESIGNATION.effectiveDateView}"
									class="width50 validate[required]" />
							</div>
						</div>
							
							<div class="width50 float-left">
							
								
								<div>
									<label class="width30" for="noticeDays">Notice Days<span
										style="color: red">*</span> 
										
									</label> <input type="text" name="noticeDays"
										id="noticeDays" value="${RESIGNATION.noticeDays}"
										class="width50 validate[required]" />
								</div>
								
								<div>
									<label class="width30" for="givenDays">Gratuity Days<span
										style="color: red">*</span> 
										
									</label> <input type="text" name="givenDays"
										id="givenDays" value="${RESIGNATION.givenDays}"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30">Reason</label> <textarea 
										id="reason" name="reason" class="width51 "
										>${RESIGNATION.reason}</textarea>
								</div>
								<div>
									<label class="width30">Description</label> <textarea 
										id="description" name="description" class="width51 "
										>${RESIGNATION.description}</textarea>
								</div>
							</div>
							<div class="clearfix"></div>
							<div id="eos-policy-div"></div>
							 <div class="clearfix"></div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right promotion_cancel" id="promotion_cancel"><fmt:message key="common.button.cancel"/></div>
								<div class="portlet-header ui-widget-header float-right promotion_save" id="promotion_save"><fmt:message key="organization.button.save"/></div>
							</div>
						</fieldset>
					</div>
			</div>
			
 		</form>
		
				<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
					
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
					<li class="ui-state-default ui-corner-top ui-tabs-selected promotion-tab"><a href="#tabs-200" id="final_settlement">Final Settlement</a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected promotion-tab"><a href="#tabs-201" id="current_grade">Current Grade</a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected promotion-tab"><a href="#tabs-202" id="current_job">Current Job</a></li>
					</ul>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-200">
						<div id="hrm" class="final_settlement"> 
							<div>Information will be loaded on select the Employee</div>
						</div>
					</div>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-201">
						<div id="hrm" class="current_grade"> 
							<div>Information will be loaded on select the Employee</div>
						</div>
					</div>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-202">
						<div id="hrm" class="current_job"> 
							
						</div>
					</div>
					
			</div>	
	
		</div>
		
		<div id="common-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;display:none;"> 
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
	
</div>

 
	

