<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<option value="">Select</option>
<c:choose>
	<c:when test="${DESIGNATION_LIST ne null && DESIGNATION_LIST ne ''}">
		<c:forEach var="dbean1" items="${DESIGNATION_LIST}">
			<option value="${dbean1.designationId}">${dbean1.designationName}</option>
		</c:forEach>
	</c:when>
</c:choose>