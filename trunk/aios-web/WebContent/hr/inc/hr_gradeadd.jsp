<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">
select{
	width: 51%;
}
</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
var accessCode=null;
$(function (){  
	var gradeView=$('#gradeView').val();
	if(gradeView!=null && gradeView=='true'){
		 $('#save').hide();
	}
	var gradeId=Number($('#gradeId').val());
	if(gradeId>0){
		
		if($('#isDefalutTemp').val().trim()=='true')
			$('#isDefalut').attr("checked","checked");
		
		if($('#isPercentageIncrementTemp').val().trim()=='true')
			$('#isPercentageIncrement').attr("checked","checked");
		
		
		autoSelectTheMultiList("leavePolicyIdTemp","leavePolicyId");
		autoSelectTheMultiList("attendancePolicyIdTemp","attendancePolicyId");
		autoSelectTheMultiList("evaluationPeriodTemp","evaluationPeriod");
		autoSelectTheMultiList("incrementPeriodTemp","incrementPeriod");
		autoSelectTheMultiList("ticketClassTemp","ticketClass");
		autoSelectTheMultiList("insuranceTypeTemp","insuranceType");
		autoSelectTheMultiList("payPeriodIdTemp","payPeriodId");
		
		
	}else{
		$('#leavePolicyId').val(Number('${DEFAULT_LEAVE}'));
		$('#attendancePolicyId').val(Number('${DEFAULT_ATTENDANCE}'));
	}
	
	$jquery("#GRADE_FORM").validationEngine('attach');
	 
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#GRADE_FORM").validationEngine('validate')){ 
			var gradeId=Number($('#gradeId').val());
			var errorMessage=null;
			
			if($('#isPercentageIncrement').attr("checked"))
				$('#isPercentageIncrement').val(true);
			
			if($('#isDefalut').attr("checked"))
				$('#isDefalut').val(true);
			
			var successMessage="Successfully Created";
			if(gradeId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/grade_save.action",
				data: $("#GRADE_FORM").serialize(),
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOGradeList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		returnCallTOGradeList();
		$('#loading').fadeOut();
	});
   
   
   $('.recruitment-lookup').click(function(){
       $('.ui-dialog-titlebar').remove(); 
       $('#common-popup').dialog('open');
       accessCode=$(this).attr("id"); 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode:accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.common-result').html(result);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	}); 
 

	$('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:450,
		bgiframe: false,
		modal: true 
	});
 //Lookup Data Roload call
	$('#save-lookup').live('click',function(){ 
		
		if(accessCode=="INSURANCE_TYPE"){
			$('#insuranceType').html("");
			$('#insuranceType').append("<option value=''>--Select--</option>");
			loadLookupList("insuranceType");
			
		}
		
	});
	
});
   function returnCallTOGradeList(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/grade_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#codecombination-popup').dialog('destroy');
				$('#codecombination-popup').remove();	
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();	
				$("#main-wrapper").html(result); 
			}
	 	});
	}
function personPopupResult(personId,personName){
	
	if(currentObjectIdentiy.trim()=="employeeCall"){
		$('#personName').val(personName);
		$('#personId').val(personId);
	}else{
		$('#assignedByName').val(personName);
		$('#assignedById').val(personId);
	}
}

function departmentBranchPopupResult(locationId,locationName){
	$('#locationName').val(locationName);
	$('#locationId').val(locationId);
}

function productPopupResult(productId,productName){
	$('#productName').val(productName);
	$('#productId').val(productId);
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
function autoSelectTheMultiList(source,target) {
	 var idsArray=new Array();
	 var idlist=$('#'+source).val();
	 idsArray=idlist.split(',');
	$('#'+target+' option').each(function(){
		var txt=$(this).val().trim();
		if($.inArray(txt, idsArray)>=0)
			$(this).attr("selected","selected");
	});

}
</script>
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Grade Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
			<form id="GRADE_FORM" name="GRADE_FORM" style="position: relative;">
			<div id="hrm" class="float-left width50">
				<input type="hidden" name="grade.gradeId" id="gradeId"
					value="${GRADE_INFO.gradeId}" />
					<input type="hidden" name="gradeView" id="gradeView"
					value="${VIEW_FLAG}" />
				<fieldset style="min-height: 290px;">
					<div>
						<label class="width40">Grade Name<span style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required]"
							name="grade.gradeName" id="gradeName"
							value="${GRADE_INFO.gradeName}" />
					</div>
					<div>
						<label class="width40">Sequance<span style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required,custom[onlyNumber]]"
							name="grade.sequence" id="sequance"
							value="${GRADE_INFO.sequence}" />
					</div>
					<div>
						<label class="width40">Pay Period<span style="color: red">*</span></label> <input
							type="hidden" id="payPeriodIdTemp"
							value="${GRADE_INFO.payPeriod.payPeriodId}">
						<select name="grade.payPeriod.payPeriodId" id="payPeriodId" class="validate[required]">
							<option value="">--Select--</option>
							<c:forEach items="${requestScope.PAYPERIOD_LIST}"
								var="nltlst">
								<option value="${nltlst.payPeriodId}">${nltlst.periodName}
								</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width40">Attendance Policy</label> <input
							type="hidden" id="attendancePolicyIdTemp"
							value="${GRADE_INFO.attendancePolicy.attendancePolicyId}">
						<select name="grade.attendancePolicy.attendancePolicyId" id="attendancePolicyId" class="">
							<option value="">--Select--</option>
							<c:forEach items="${requestScope.ATTENDANCE_POLICIES}"
								var="nltlst">
								<option value="${nltlst.attendancePolicyId}">${nltlst.policyName}
								</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width40">Leave Policy</label> <input type="hidden"
							id="leavePolicyIdTemp"
							value="${GRADE_INFO.leavePolicy.leavePolicyId}"> <select
							name="grade.leavePolicy.leavePolicyId"
							id="leavePolicyId" class="">
							<option value="">--Select--</option>
							<c:forEach items="${requestScope.LEAVE_POLICIES}" var="nltls">
								<option value="${nltls.leavePolicyId}">${nltls.leavePolicyName}
								</option>
							</c:forEach>
						</select>
					</div>
					
					<div>
						<label class="width40">Minimum Salary Range </label> <input type="text"
							class="width50" name="grade.minSalary"
							id="minSalary" value="${GRADE_INFO.minSalary}" />
					</div>
					<div>
						<label class="width40">Maximum Salary Range</label> <input type="text"
							class="width50" name="grade.maxSalary"
							id="maxSalary" value="${GRADE_INFO.maxSalary}" />
					</div>
					<div class="width100">
						<label class="width40">Ticket Class</label>
						<div>
							<select id=ticketClass name="grade.ticketClass">
								<option value="">--Select--</option>
								<c:forEach items="${TICKET_CLASS}" var="gen">
									<option value="${gen.key}">${gen.value}</option>
								</c:forEach>
							</select> <input type="hidden" name="ticketClassTemp" id="ticketClassTemp"
								value="${GRADE_INFO.ticketClass}" />
						</div>
					</div>
					<div>
						<label class="width40">No.of Ticket's</label> <input type="text"
							class="width50 validate[optional,custom[onlyNumber]]"
							name="grade.numberOfTicket" id="numberOfTicket"
							value="${GRADE_INFO.numberOfTicket}" />
					</div>
					
				</fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="min-height: 290px;">
					<div>
						<label class="width40">Increment Period</label>
						<div>
							<select id=incrementPeriod name="grade.incrementPeriod">
								<option value="">--Select--</option>
								<c:forEach items="${INCREMENT_PERIOD}" var="gen">
									<option value="${gen.key}">${gen.value}</option>
								</c:forEach>
							</select> <input type="hidden" name="incrementPeriodTemp"
								id="incrementPeriodTemp" value="${GRADE_INFO.incrementPeriod}" />
						</div>
					</div>
					<div>
						<label class="width40">No.of Increment </label> <input type="text"
							class="width50 validate[optional,custom[onlyNumber]]"
							name="grade.numberOfIncrement" id="numberOfIncrement"
							value="${GRADE_INFO.numberOfIncrement}" />
					</div>
					<div class="width100">
						<label class="width40 float-left">Increment Value</label> <input
							type="text" class="width20 float-left"
							name="grade.incrementAmount" id="incrementAmount"
							value="${GRADE_INFO.incrementAmount}" /> <label
							class="width20 float-left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Percentage(%)</label> <input type="checkbox"
							class="width10 float-left" name="grade.isPercentageIncrement"
							id="isPercentageIncrement" /> <input type="hidden"
							id="isPercentageIncrementTemp" class=""
							value="${GRADE_INFO.isPercentageIncrement}">
					</div>
					
					<div>
						<label class="width40">Evaluation Period</label>
						<div>
							<select id=evaluationPeriod name="grade.evaluationPeriod">
								<option value="">--Select--</option>
								<c:forEach items="${EVALUTATION_PERIOD}" var="gen">
									<option value="${gen.key}">${gen.value}</option>
								</c:forEach>
							</select> <input type="hidden" name="evaluationPeriodTemp"
								id="evaluationPeriodTemp" value="${GRADE_INFO.evaluationPeriod}" />
						</div>
					</div>
					<div>
						<label class="width40">No.of Evaluation </label> <input
							type="text" class="width50 validate[optional,custom[onlyNumber]]"
							name="grade.evaluationPeriodCount" id="evaluationPeriodCount"
							value="${GRADE_INFO.evaluationPeriodCount}" />
					</div>
					<div>
						<label class="width40">Insurance Type</label>
						<div>
							<select id="insuranceType"
								name="grade.lookupDetail.lookupDetailId"
								class="">
								<option value="0">--Select--</option>
								<c:forEach items="${INSURANCE_TYPE}" var="nltlst">
									<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
									</option>
								</c:forEach>
							</select> <span class="button" id="INSURANCE_TYPE_1"
								style="position: relative; "> <a
								style="cursor: pointer;" id="INSURANCE_TYPE"
								class="btn ui-state-default ui-corner-all recruitment-lookup width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
								type="hidden" name="insuranceTypeTemp" id="insuranceTypeTemp"
								value="${GRADE_INFO.lookupDetail.lookupDetailId}" />
						</div>
					</div>
					<div class="width100 float-left">
						<label class="width40 float-left">Notice Period(Employee)</label> <input
							type="text" class="float-left validate[optional,custom[onlyNumber]]"
							name="grade.noticeDaysEmployee" id="noticeDaysEmployee"
							value="${GRADE_INFO.noticeDaysEmployee}" style="width: 43%!important"/>
							<label class="width10 float-left" style="margin-top:11px;">Day's</label> 
					</div>
					<div class="width100 float-left">
						<label class="width40 float-left">Notice Period(Employer)</label> <input
							type="text" class="float-left validate[optional,custom[onlyNumber]]"
							name="grade.noticeDaysCompany" id="noticeDaysCompany"
							value="${GRADE_INFO.noticeDaysCompany}" style="width: 43%!important"/>
							<label class="width10 float-left" style="margin-top:11px;">Day's</label>
					</div>
					<div class="width100 float-left">
						<label class="width40">Allowed Dependent's</label> <input
							type="text" class="width50 validate[optional,custom[onlyNumber]]"
							name="grade.allowedDependents" id="allowedDependents"
							value="${GRADE_INFO.allowedDependents}" />
					</div>
					<div id="dependentTicket">
						<label class="width40">No.of Dependent Ticket's</label> <input
							type="text" class="width50 validate[optional,custom[onlyNumber]]"
							name="grade.numberOfDependentTicket" id="numberOfDependentTicket"
							value="${GRADE_INFO.numberOfDependentTicket}" />
					</div>
					
					<div>
						<label class="width40">Default</label> <input type="checkbox"
							id="isDefalut" name="grade.isDefalut"> <input type="hidden"
							id="isDefalutTemp" class="width8"
							value="${GRADE_INFO.isDefalut}">
					</div>
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>