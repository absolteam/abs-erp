<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.label-data{
	font-weight: normal!important;
}


</style><script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;var payLineDetail="";
$(function(){ 
	
$('.formError').remove();
	
$('.payrollElementIdTemp').each(function(){
	if($(this).val()!=""){
		var rowId=getRowId($(this).attr('id')); 
		$('#payrollElementId_'+rowId).val($('#payrollElementIdTemp_'+rowId).val());
	} 
	
});
manupulateLastRow();
	
	
	$('.payrollElementId').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	$('.amount').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			totalPaySum();
			return false;
		} 
		else{
			return false;
		}
	});	 
	
	//Add row
	$('.addrowsS').live('click',function(){
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/payroll_transaction_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabS tr:last').before(result);
					 if($(".tabS").height()>255)
						 $(".tabS").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidS').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdS_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowS').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
  	 $(slidetab).remove();  
  		totalPaySum();
  	 var i=1;
	 $('.rowidS').each(function(){   
		 var rowId=getRowId($(this).attr('id')); 
		 $('#lineIdS_'+rowId).html(i);
			 i=i+1; 
		 });   
	 });
	

$('#job-common-popup').dialog({
	autoOpen: false,
	minwidth: 'auto',
	width:800,
	height:550,
	bgiframe: false,
	modal: true 
});

totalPaySum();
});

var getLineDetails=function(){
	payLineDetail="";
	var payTransactionIds=new Array();
	var payElements=new Array();
	var amountS=new Array();
	var notes=new Array();
	$('.rowidS').each(function(){ 
		var rowId=getRowId($(this).attr('id')); 
		var payrollElementId=$('#payrollElementId_'+rowId).val();  
		var amount=$('#amount_'+rowId).val();
		var payrollTransactionId=$('#payrollTransactionId_'+rowId).val();
		var note=$('#note_'+rowId).val();
		if(payrollTransactionId==null || payrollTransactionId=='' || payrollTransactionId==0 || payrollTransactionId=='undefined')
			payrollTransactionId=-1;
		
		if(note==null || note=='' || note==0 || note=='undefined')
			note=-1;
	
		if(typeof payrollElementId != 'undefined' && payrollElementId!=null && payrollElementId!="" && amount!=null && amount!=""){
			payTransactionIds.push(payrollTransactionId);
			payElements.push(payrollElementId);
			amountS.push(amount);
			notes.push(note);
		}
	});
	for(var j=0;j<payElements.length;j++){ 
		payLineDetail+=payTransactionIds[j]+"@#"+payElements[j]+"@#"+amountS[j]+"@#"+notes[j];
		if(j==payElements.length-1){   
		} 
		else{
			payLineDetail+="##";
		}
	} 
	return payLineDetail;
};
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){
	var payrollElementId=$('#payrollElementId_'+rowId).val();  
	var amount=$('#amount_'+rowId).val();
	var nexttab=$('#fieldrowS_'+rowId).next(); 
	if(payrollElementId!=null && payrollElementId!=""
			&& amount!=null && amount!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageS_'+rowId).show();
		$('.addrowsS').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tabS>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabS>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabS>tr:last').removeAttr('id');  
	$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
	$($('.tabS>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function totalPaySum(){
	var totalPay=Number(0);
	var earningTotal=Number(0);
	var deductionTotal=Number(0);
	$('.amount').each(function(){
		var rowId=getRowId($(this).attr('id'));
		if($('#payrollElementId_'+rowId).val()!=null && $('#payrollElementId_'+rowId).val()!=''){
			var temp=$('#payrollElementId_'+rowId+' option:selected').text();
			var type=temp.substring((temp.indexOf('[')+1), temp.indexOf(']'));
			if(type=="EARNING")
				earningTotal+=convertToDouble($('#amount_'+rowId).val());
			else if(type=="DEDUCTION")
				deductionTotal+=convertToDouble($('#amount_'+rowId).val());
		}
	});
	totalPay=earningTotal-deductionTotal;
	$('#totalPay').val(totalPay);
}

</script>
<div class="width100 float-left">
	<div id="hrm" class="hastable width100"  >  
			<input type="hidden" id="payId" value="${PAYROLL_PROCESS.payrollId}"/>
			<table id="hastab2" class="width100"> 
				<thead>
					<tr> 
						<th style="width:15%"><fmt:message key="hr.payroll.elementname"/></th> 
					    <th style="width:5%">Calculated Amount</th>
					     <th style="width:5%">Amount</th>
					    <th style="width:15%">Note</th>
						<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
				  </tr>
				</thead> 
						<tbody class="tabS">
							<c:choose>
								<c:when test="${PAYROLL_PROCESS.payrollTransactionVOs ne null && PAYROLL_PROCESS.payrollTransactionVOs ne '' && fn:length(PAYROLL_PROCESS.payrollTransactionVOs)>0}">
									<c:forEach var="pay" items="${PAYROLL_PROCESS.payrollTransactionVOs}" varStatus="status1">
										<tr class="rowidS" id="fieldrowS_${status1.index+1}">
											<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
											<td>
												<select class="width90 payrollElementId" disabled="disabled" id="payrollElementId_${status1.index+1}"style="border:0px;">
														<option value="">Select</option>
													<c:forEach var="payrollElemnt" items="${PAYROLL_ELEMENTS}" varStatus="status">
														<option value="${payrollElemnt.payrollElementId}">${payrollElemnt.elementName} [${payrollElemnt.elementNature}]</option>
													</c:forEach>
												</select>
												<input type="hidden" class="width80 payrollElementIdTemp"  name="payrollElementIdTemp" id="payrollElementIdTemp_${status1.index+1}" value="${pay.payrollElement.payrollElementId}" style="border:0px;"/>
											</td> 
											<td>
												<input type="text" class="width80 calculatedAmount" disabled="disabled" name="calculatedAmount" id="calculatedAmount_${status1.index+1}" value="${pay.calculatedAmount}" readonly="readonly" style="border:0px;"/>
											</td> 
											<td>
												<input type="text" class="width80 amount" name="amount" id="amount_${status1.index+1}" value="${pay.amount}" style="border:0px;"/>
											</td> 
											<td>
												<input type="text" class="width80 note" name="note" id="note_${status1.index+1}" value="${pay.note}" style="border:0px;"/>
											</td> 
											<td style="display:none;">
												<input type="hidden" id="payrollTransactionId_${status1.index+1}" value="${pay.payrollTransactionId}" style="border:0px;"/>
											</td>
											 <td style="width:0.01%;" class="opn_td" id="optionS_${status1.index+1}">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${status1.index+1}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
											</td>
										</tr>
									</c:forEach>  
								</c:when>
							</c:choose>  
							<c:forEach var="i" begin="${fn:length(PAYROLL_PROCESS.payrollTransactionVOs)+1}" end="${fn:length(PAYROLL_PROCESS.payrollTransactionVOs)+2}" step="1" varStatus="status"> 
								<tr class="rowidS" id="fieldrowS_${i}">
										<td id="lineIdS_${i}" style="display: none;">${i}</td>
										<td>
											<select class="width90 payrollElementId" id="payrollElementId_${i}"style="border:0px;">
													<option value="">Select</option>
												<c:forEach var="payrollElemnt" items="${PAYROLL_ELEMENTS}" varStatus="status">
													<option value="${payrollElemnt.payrollElementId}">${payrollElemnt.elementName} [${payrollElemnt.elementNature}]</option>
												</c:forEach>
											</select>
											<input type="hidden" class="width80 payrollElementIdTemp" name="payrollElementIdTemp" id="payrollElementIdTemp_${i}" value=""  style="border:0px;"/>
										</td> 
										<td>
											<input type="text" class="width80 calculatedAmount" disabled="disabled" name="calculatedAmount" id="calculatedAmount_${i}" value="" readonly="readonly" style="border:0px;"/>
										</td> 
										<td>
											<input type="text" class="width80 amount" name="amount" id="amount_${i}" value="" style="border:0px;"/>
										</td> 
										<td>
											<input type="text" class="width80 note" name="note" id="note_${i}" value="" style="border:0px;"/>
										</td> 
										<td style="display:none;">
											<input type="hidden" id="payrollTransactionId_${i}" value="${pay.payrollTransactionId}" style="border:0px;"/>
										</td>
										<td style="width:0.01%;" class="opn_td" id="optionS_${i}">
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${i}" style="display:none;cursor:pointer;" title="Add Record">
				 								<span class="ui-icon ui-icon-plus"></span>
										  </a>	
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${i}" style="display:none; cursor:pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
										  </a> 
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${i}" style="display:none;cursor:pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
										  </a>
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
										  </a>
									</td>    
								</tr>
						</c:forEach>
				 </tbody>
		</table>
		<div class="float-right width50">
			<label class="float-left width30"><span><fmt:message key="hr.payroll.totalamount"/> :</span></label>
			<input type="text" readonly="readonly" id="totalPay" class="float-left width40"/>
		</div>
	</div> 
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrowsS" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
	</div>
 </div> 