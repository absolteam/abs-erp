<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count {
	padding: 0 !important;
}
</style>

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 

		var interviewProcessId=$('#interviewProcessId').val();
		autoSelectTheMultiList("statusTemp","status");
		autoSelectTheMultiList("roundTemp","round");
		autoSelectTheMultiList("roomTemp","room");
	//populateUploadsPane("doc","uploadedDocs","InterviewProcess",interviewProcessId);


		$('#dms_document_information').click(function(){
			AIOS_Uploader.openFileUploader("doc","uploadedDocs","InterviewProcess",interviewProcessId,"InterviewInformation");
		});
		
	   
	   $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	  
	   $jquery("#interview-details").validationEngine('attach');
		
			
		$('#interview-discard').click(function(){ 
			returnCallTOList();
			
		 });
		

	 $('#interview-save').click(function(){
		 $('.error,.success').hide();
					var errorMessage=null;
					var successMessage="Successfully updated";
					
					try {
						if ($jquery("#interview-details").validationEngine('validate')) {
							//generateDataToSendToDB("candidateList","candidates");
							//generateDataToSendToDB("interviewerList","interviewers");
							var status=$('#status').val();
							var rating=$('#rating').val();
							var remarks=$('#remarks').val();
							var score=$('#score').val();
							var interviewProcessId=$('#interviewProcessId').val();
							var alertId=$('#alertId').val();
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/interview_update_submit.action", 
							 	async: false,
							 	data: {
							 		status:status,
							 		rating:rating,
							 		remarks:remarks,
							 		score:score,
							 		interviewProcessId:interviewProcessId,
							 		alertId:alertId
							 		},
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
						}
					} catch (e) {

					}

				});
	 
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="INTERVIEW_HALL"){
				$('#room').html("");
				$('#room').append("<option value=''>--Select--</option>");
				loadLookupList("room");
				
			}
			
		});
		
	 	

		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width: '90%',
			height:'600',
			bgiframe: false,
			modal: true 
		});
			
		 //$('#createdDate').datepick();
		//$('#interviewTime').timepicker({});
		
		 $('#view-person').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         var personId=Number($('#personId').val());
			 $.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/common_personal_details_view.action",  
			     	data: {personId:personId,viewType:"CURRENT"},  
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result)
			     	{  
						$('.common-result').html(result);
			     	}
				});
	    	 return false;
	 	});
		 
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/interview_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}

	function generateDataToSendToDB(source, target) {
		var typeList = new Array();
		var ids = "";
		$('#' + source + ' option:selected').each(function() {
			typeList.push($(this).val());
		});
		for ( var i = 0; i < typeList.length; i++) {
			if (i == (typeList.length) - 1)
				ids += typeList[i];
			else
				ids += typeList[i] + ",";
		}

		$('#' + target).val(ids);
	}

	function autoSelectTheMultiList(source, target) {
		var idsArray = new Array();
		var idlist = $('#' + source).val();
		idsArray = idlist.split(',');
		$('#' + target + ' option').each(function() {
			var txt = $(this).val().trim();
			if ($.inArray(txt, idsArray) >= 0)
				$(this).attr("selected", "selected");
		});

	}
	
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	
	function callPersonViewClose(){
		$('.common-result').html("");
	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Interviewing Process
		
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="interview-details" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden" name="alertId"
									id="alertId" value="${alertId}"
									class="width50 " />
								<input type="hidden" name="interviewProcessId"
									id="interviewProcessId" value="${INTERVIEW.interviewProcess.interviewProcessId}"
									class="width50 " />
									
								<fieldset style="min-height: 150px;">
									<legend>Schedule Information</legend>
									<div class="width50 float-left">
										<div>
											<label class="width30">Position Reference<span
												style="color: red">*</span> </label> <input type="hidden"
												name="interview.openPosition.openPositionId"
												id="openPositionId"
												value="${INTERVIEW.openPosition.openPositionId}" />
											<div>
												<input type="text" id="positionReferenceNumber"
													readonly="readonly" class="width50"
													value="${INTERVIEW.openPosition.positionReferenceNumber}" />
											</div>

										</div>
										<div>
											<label class="width30">Question<span
												style="color: red">*</span> </label> <input type="hidden"
												name="interview.questionBank.questionBankId"
												id="questionBankId"
												value="${INTERVIEW.questionBank.questionBankId}" />
											<div>
												<input type="text" id="questionType" readonly="readonly"
													class="width50"
													value="${INTERVIEW.questionBank.questionType}" /> 
											</div>

										</div>
										<div>
											<label class="width30">Question Pattern </label> <input
												type="text" id="questionPattern" class="width50"
												name="questionPattern"
												value="${INTERVIEW.questionPatternView}" />
										</div>
										<div>
											<label class="width30">Round<span style="color: red">*</span>
											</label>
											<div>
												<select id=round name="interview.round" disabled="disabled"
													class="width50">
													<c:forEach items="${ROUNDS}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="roundTemp" id="roundTemp"
													value="${INTERVIEW.round}" />
											</div>
										</div>
										
									</div>
									<div class="width50 float-left">

										<div>
											<label class="width30">Interview Spot</label> <input
												type="text" id="locationName" class="width50"
												name="questionPattern"
												value="${INTERVIEW.location.locationName}" /> <input
												type="hidden" name="interview.location.locationId" id="locationId"
												class="locationId" value="${INTERVIEW.location.locationId}" />
										</div>
										<div>
											<label class="width30">Room/Hall<span
												style="color: red">*</span> </label>
											<div>
												<select id="room" disabled="disabled"
													name="interview.lookupDetail.lookupDetailId"
													style="width: 52.5%" class="">
													<option value="">--Select--</option>
													<c:forEach items="${INTERVIEW_HALL}" var="nltlst">
														<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
														</option>
													</c:forEach>
												</select>  <input
													type="hidden" name="roomTemp" id="roomTemp"
													value="${INTERVIEW.lookupDetail.lookupDetailId}" />
											</div>
										</div>
										<div>
											<label class="width30">Interview Date</label> <input
												type="text" class="width50" readonly="readonly" disabled="disabled"
												name="interviewDate" id="interviewDate"
												value="${INTERVIEW.interviewDateView}" />
										</div>
										<div>
											<label class="width30">Interview Time</label> <input
												type="text" class="width50" readonly="readonly" disabled="disabled"
												name="interviewTime" id="interviewTime"
												value="${INTERVIEW.interviewTimeView}" />
										</div>
										
								
									</div>
								</fieldset>
								<fieldset style="min-height: 100px;">
									<legend>Candidate Information</legend>
									<div class="width50 float-left">
										<input type="hidden" id="personId" value="${INTERVIEW.candidateVO.personByPersonId.personId}"/>
										<div>
											<label class="width30">Candidate Name</label> <input
												type="text" class="width50 " readonly="readonly"
												name="candidateName" id="candidateName"
												value="${INTERVIEW.candidateVO.personByPersonId.firstName} ${INTERVIEW.candidateVO.personByPersonId.lastName}" />
										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<label class="width30">Mobile Number</label> <input
												type="text" class="width50" readonly="readonly"
												name="mobile" id="mobile"
												value="${INTERVIEW.candidateVO.personByPersonId.mobile}" />
										</div>
										<div
											class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
											<div
												class="portlet-header ui-widget-header float-left view-person"
												id="view-person">View Candidate</div>
										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<label class="width30">Email</label> <input
												type="text" class="width50" readonly="readonly"
												name="email" id="email"
												value="${INTERVIEW.candidateVO.personByPersonId.email}" />
										</div>
									</div>
									
								</fieldset>
								<fieldset style="min-height: 50px;">
									<legend>Update Interview</legend>
									<div class="width50 float-left">
									<div>
											<label class="width30">Status<span style="color: red">*</span>
											</label>
											<div>
												<select id=status name="interview.status"
													class="width50 validate[required]">
													<c:forEach items="${STATUS}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="statusTemp" id="statusTemp"
													value="${INTERVIEW.status}" />
											</div>
										</div>
										<div>
											<label class="width30">Score</label> <input
												type="text" class="width50"
												name="score" id="score"
												value="${INTERVIEW.interviewProcess.score}" />
										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<label class="width30">Interview Rating<span style="color: red">*</span>
											</label>
											<div>
												<select id=rating name="interview.rating"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${RATTING}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="ratingTemp" id="ratingTemp"
													value="${INTERVIEW.interviewProcess.rating}" />
											</div>
										</div>
										<div>
											<label class="width30">Remarks</label> <textarea
												class="width50"
												name="remarks" id="remarks"
												>${INTERVIEW.interviewProcess.remarks}</textarea>
										</div>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="width100 float-left">
								<fieldset>
									<legend>Document Uploads</legend>
									<div id="dms_document_information"
										style="cursor: pointer; color: blue;">
										<u><fmt:message key="re.property.info.uploadDocsHere" />
										</u>
									</div>
									<div
										style="padding-top: 10px; margin-top: 3px; height: 150px; overflow: auto;">
										<span id="uploadedDocs"></span>
									</div>
								</fieldset>
							</div>

						</div>

					</form>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">

						<div
							class="portlet-header ui-widget-header float-right interview-discard"
							id="interview-discard">cancel</div>
						<div
							class="portlet-header ui-widget-header float-right interview-save"
							id="interview-save">save</div>
					</div>
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup" class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

				

			</div>
		</div>
	</div>
</body>
</html>