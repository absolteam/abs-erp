<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<tr class="rowidC" id="fieldrowC_${rowId}">
	<td id="lineIdC_${rowId}" style="display:none;">${rowId}</td>
	<td>
		<select id="paymentType_${rowId}"
		name="paymentType_${rowId}"
		class="width80 validate[required]">
		<option value="">--Select--</option>
		<c:forEach items="${PAYMENT_TYPE}" var="nltls">
			<option value="${nltls.lookupDetailId}">${nltls.displayName}
			</option>
		</c:forEach>
	</select> <span class="button" id="PAYMENT_TYPE_${rowId}"
		style="position: relative; "> <a
		style="cursor: pointer;" id="PAYMENT_TYPE"
		class="btn ui-state-default ui-corner-all recruitment-lookup width100">
			<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
		type="hidden" name="paymentTypeIdTemp_${rowId}"
		id="paymentTypeIdTemp_${rowId}"  />
		<input type="hidden" id="paymentTypeId_${rowId}"  style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width80 amount" id="amount_${rowId}"  style="border:0px;text-align: right;"/>
	</td> 
	
	<td>
		<textarea class="width80 description" id="description_${rowId}"style="border:0px;"></textarea>
	</td>
	<td style="display:none;">
		<input type="hidden" id="hraContractPaymentId_${rowId}"  style="border:0px;"/>
	</td>
	<td style="width:0.01%;" class="opn_td" id="optionc_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>


