<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<style>
.ui-multiselect .count{
	padding:0!important;
}
</style> 

<script type="text/javascript"> 
var lineDetail="";
var accessCode=null;
$(document).ready(function(){ 

	manupulateShiftLastRow();
	
		var questionBankId=Number($("#questionBankId").val());
		if(questionBankId>0){
			autoSelectTheMultiList("questionPatternTemp","questionPattern");
			populateUploadsPane("doc","uploadedDocs","QuestionBank",questionBankId);
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","uploadedDocs","QuestionBank",questionBankId,"Questions");
			});
		}else{
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","uploadedDocs","QuestionBank","-1","Questions");
			});
		}
		
		
		
		 $('#designationpopup').click(function() { 
		       $('.ui-dialog-titlebar').remove();  
		       $('#common-popup').dialog('open');
		       var rowId=0; 
		          $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/common_designation_list.action",
		             data:{id:rowId},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){
		                  $('.common-result').html(result);
		                  return false;
		             },
		             error:function(result){
		                  $('.common-result').html(result);
		             }
		         });
		          return false;
		 }); 
	   
	   $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	 
		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
		
		
			
		$('#question-discard').click(function(){ 
			returnCallToList();
			
		 });
		
		$jquery("#question-details").validationEngine('attach');
		
	 $('#question-save').click(function(){
		 $('.error,.success').hide();
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(questionBankId>0)
						successMessage="Successfully Modified";

					try {
						if ($jquery("#question-details").validationEngine('validate')) {
							//generateDataToSendToDB("responsibilityList","responsibility");
							 lineDetail= getLineDetails();
		 					 $("#childLineDetail").val(lineDetail);
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/question_bank_save.action", 
							 	async: false,
							 	data: $("#question-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallToList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
						}
					} catch (e) {

					}

				});
	 
	 $('.questionContent').live('change',function(){
			if($(this).val()!=""){
				var rowId=getRowId($(this).attr('id')); 
				triggerBankAddRow(rowId);
				return false;
			} 
			else{
				return false;
			}
		});	
	 
	 $('.addrowsS').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/question_bank_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabS tr:last').before(result);
					 if($(".tabS").height()>255)
						 $(".tabS").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidS').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdS_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	
	$('.delrowS').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
	  	 $(slidetab).remove();  
	  	 var i=1;
		 $('.rowidS').each(function(){   
			 var rowId=getRowId($(this).attr('id')); 
			 $('#lineIdS_'+rowId).html(i);
				 i=i+1; 
			 });  
		 return false;
	 });
	
	$(".low input[type='button']").click(function(){
	    var arr = $(this).attr("name").split("2");
	    var from = arr[0];
	    var to = arr[1];
	    $("#" + from + " option:selected").each(function(){
	      $("#" + to).append($(this).clone());
	      $(this).remove();
	    });
	}); 
		 
	});

	
	
	
	function returnCallToList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/question_bank_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
	
	function autoSelectTheMultiList(source,target) {
		 var idsArray=new Array();
		 var idlist=$('#'+source).val();
		 idsArray=idlist.split(',');
		$('#'+target+' option').each(function(){
			var txt=$(this).val().trim();
			if($.inArray(txt, idsArray)>=0)
				$(this).attr("selected","selected");
		});

	}
	
	function designationPopupResult(id, name, commaseparatedValue) {
		$('#designationId').val(id);
		var valueArray=new Array();
		valueArray=commaseparatedValue.split('@&');
		$('#designationName').val(valueArray[0]);
		$('#gradeName').val(valueArray[1]);
		
	}
	
	function manupulateShiftLastRow(){
		var hiddenSize=0;
		$($(".tabS>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		var tdSize=$($(".tabS>tr:first").children()).size();
		var actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabS>tr:last').removeAttr('id');  
		$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
		$($('.tabS>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	
	function triggerBankAddRow(rowId){ 
		var questionTitle=$('#questionTitle_'+rowId).val();  
		var questionContent=$('#questionContent_'+rowId).val();  
		var nexttab=$('#fieldrowS_'+rowId).next(); 
		if(questionTitle!=null && questionTitle!="" && questionContent!=null && questionContent!=""
				 && $(nexttab).hasClass('lastrow')){ 
			$('#DeleteImageS_'+rowId).show();
			$('.addrowsS').trigger('click');
			return false;
		}else{
			return false;
		} 
	}
	function getLineDetails(){
		lineDetail="";
		var questionInfoIds=new Array();
		var questionTitles=new Array();
		var questionContents=new Array();
		

		$('.rowidS').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var questionInfoId=$('#questionInfoId_'+rowId).val();
			if(questionInfoId==null || questionInfoId=='' || questionInfoId==0 || questionInfoId=='undefined')
				questionInfoId=-1;
			var questionTitle=$('#questionTitle_'+rowId).val();
			var questionContent=$('#questionContent_'+rowId).val();
			
			//var statusShift=$('#statusShift_'+rowId).val();
			if(typeof questionTitle != 'undefined' && questionTitle!=null && questionTitle!=""){
				questionInfoIds.push(questionInfoId);
				questionTitles.push(questionTitle);
				questionContents.push(questionContent);
				
				//statuses.push(statusShift);
			}
		});
		for(var j=0;j<questionTitles.length;j++){ 
			lineDetail+=questionInfoIds[j]+"@%@"+questionTitles[j]+"@%@"+questionContents[j];
			if(j==questionTitles.length-1){   
			} 
			else{
				lineDetail+="#%#";
			}
		} 
		return lineDetail;
	}
	
	
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Question
			Bank
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="candidate-page-error"
					class="response-msg error ui-corner-all" style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="question-details" method="post">
						<div class="width100 float-left" id="hrm">
							<div class="width50 float-left">
								<input type="hidden" name="questionBank.questionBankId"
									id="questionBankId" value="${QUESTION_BANK.questionBankId}"
									class="width50" />

							<input type="hidden" name="childLineDetail"
									id="childLineDetail" 
									class="width50" />
								<fieldset style="min-height: 100px;">
									<legend>Question Information</legend>
									<div>
										<label class="width30">Pattern<span style="color: red">*</span>
										</label> <select id="questionPattern"
											name="questionBank.questionPattern"
											class="width61 validate[required]">
											<c:forEach items="${QUESTION_PATTERNS}" var="gen">
												<option value="${gen.key}">${gen.value}</option>
											</c:forEach>
										</select> <input type="hidden" name="questionPatternTemp"
											id="questionPatternTemp"
											value="${QUESTION_BANK.questionPattern}" />
									</div>
									<div>
										<label class="width30">Type</label> <input type="text"
											class="width60" 
											name="questionBank.questionType" id="questionType"
											value="${QUESTION_BANK.questionType}" />
									</div>
									<div class="width100 float-left">
										<label class="width30 ">Designation</label> <input type="text"
											readonly="readonly" id="designationName"
											class="masterTooltip width60 "
											value="${QUESTION_BANK.designation.designationName}" /> <span
											class="button " id="designation" style="position: relative;">
											<a style="cursor: pointer;" id="designationpopup"
											class="btn ui-state-default ui-corner-all width10"> <span
												class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="questionBank.designation.designationId"
											id="designationId"
											value="${QUESTION_BANK.designation.designationId}" />
									</div>
									<div>
										<label class="width30">Grade</label> <input type="text"
											readonly="readonly" id="gradeName"
											class="masterTooltip width60"
											value="${QUESTION_BANK.designation.grade.gradeName}" />
									</div>
									<div>
										<label class="width30">Description </label>
										<textarea id="questionBankDescription" class="width61"
											name="questionBank.description">${QUESTION_BANK.description}</textarea>
									</div>
								</fieldset>

							</div>
							<div id="hrm" class="width50 float-left">
								<fieldset>
									<legend>Document Uploads</legend>
									<div id="dms_document_information"
										style="cursor: pointer; color: blue;">
										<u><fmt:message key="re.property.info.uploadDocsHere" />
										</u>
									</div>
									<div
										style="padding-top: 10px; margin-top: 3px; height: 150px; overflow: auto;">
										<span id="uploadedDocs"></span>
									</div>
								</fieldset>
							</div>


						</div>

					</form>
					<div class="width100 float-left">
						<fieldset>
							<legend>Questions</legend>
							<div id="hrm" class="hastable width100">
								<table id="hastab2" class="width100">
									<thead>
										<tr>
											<th>Title</th>
											<th>Content</th>
											<th style="width: 0.01%;"><fmt:message
													key="accounts.common.label.options" />
											</th>
										</tr>
									</thead>
									<tbody class="tabS">
										<c:choose>
											<c:when
												test="${QUESTION_INFO_LIST ne null && QUESTION_INFO_LIST ne '' && fn:length(QUESTION_INFO_LIST)>0}">
												<c:forEach var="quest" items="${QUESTION_INFO_LIST}"
													varStatus="status1">
													<tr class="rowidS" id="fieldrowS_${status1.index+1}">
														<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td><input type="text" class="width100 questionTitle"
															id="questionTitle_${status1.index+1}"
															value="${quest.questionTitle}" /></td>
														<td><textarea id="questionContent_${status1.index+1}"
																class="width100 questionContent">${quest.questionContent}</textarea>
														</td>
														<td style="display: none;"><input type="hidden"
															id="questionInfoId_${status1.index+1}"
															value="${quest.questionInfoId}" style="border: 0px;" />
														</td>
														<td style="width: 0.01%;" class="opn_td"
															id="optionS_${status1.index+1}"><a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS"
															id="AddImageS_${status1.index+1}"
															style="display: none; cursor: pointer;"
															title="Add Record"> <span
																class="ui-icon ui-icon-plus"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS"
															id="EditImageS_${status1.index+1}"
															style="display: none; cursor: pointer;"
															title="Edit Record"> <span
																class="ui-icon ui-icon-wrench"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS"
															id="DeleteImageS_${status1.index+1}"
															style="cursor: pointer;" title="Delete Record"> <span
																class="ui-icon ui-icon-circle-close"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
															id="WorkingImageS_${status1.index+1}"
															style="display: none;" title="Working"> <span
																class="processing"></span> </a></td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>
										<c:forEach var="i" begin="${fn:length(QUESTION_INFO_LIST)+1}"
											end="${fn:length(QUESTION_INFO_LIST)+2}" step="1"
											varStatus="status">
											<tr class="rowidS" id="fieldrowS_${i}">
												<td id="lineIdS_${i}" style="display: none;">${i}</td>
												<td><input type="text" class="width100 questionTitle"
													id="questionTitle_${i}" value="" /></td>
												<td><textarea id="questionContent_${i}"
														class="width100 questionContent"></textarea></td>
												<td style="display: none;"><input type="hidden"
													id="questionInfoId_${i}" value="" /></td>
												<td style="width: 0.01%;" class="opn_td" id="optionS_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS"
													id="AddImageS_${i}" style="display: none; cursor: pointer;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS"
													id="EditImageS_${i}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS"
													id="DeleteImageS_${i}"
													style="display: none; cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImageS_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">

						<div
							class="portlet-header ui-widget-header float-right question-discard"
							id="question-discard">cancel</div>
						<div
							class="portlet-header ui-widget-header float-right question-save"
							id="question-save">save</div>
					</div>
				</div>


				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup" class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>


				<div style="display: none;"
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
					<div class="portlet-header ui-widget-header float-left addrowsS"
						style="cursor: pointer;">
						<fmt:message key="accounts.common.button.addrow" />
					</div>
				</div>

			</div>
		</div>
	</div>

</body>
</html>