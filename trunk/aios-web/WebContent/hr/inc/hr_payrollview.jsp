<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>
<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
<link href="${pageContext.request.contextPath}/css/reset.css"
	rel="stylesheet" type="text/css" media="all" /> 

<link
	href="${pageContext.request.contextPath}/css/styles/default/ui.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/forms.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/tables.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/messages.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/HRM.css" type="text/css"
	media="all" />
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/jquery.ui.button.css"
	rel="stylesheet" type="text/css" media="all" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
.caption {
	padding: 2px;
	padding-top: 5px !important;
}

.data {
	padding: 2px;
	padding-top: 5px !important;
}

.data-container {
	padding: 5px;
}

td {
	padding-top: 8px !important;
}

th {
	font-weight: bold;
	padding-top: 8px !important;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript"> 
 $(document).ready(function (){ 
	 var viewRedirect=$("#viewRedirect").val();
	
	 if(viewRedirect!=null && viewRedirect=='PayrollExecution'){
		 listRedirectCall("payroll_execution");
	 }else if(viewRedirect=='PayAdjustment'){
		 listRedirectCall("payroll_adjustment");
	 }else if(viewRedirect=='Notification'){
		 $('#close').hide();
	 }
	
 });
 function listRedirectCall(urlPath){
	 $('#close').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+urlPath+".action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
		 return false;
	});
 }
 function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
		var payrollId=Number($('#payrollId').val());
		if(returnVO.returnStatusName=='DeleteApproved')
			deletePayrollApproval(payrollId,messageId);
	}
	function deletePayrollApproval(payrollId,messageId){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/delete_payroll.action",  
	     	data: {payrollId:payrollId,messageId: messageId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				return false;
	     	},
	     	error:function(result)
	        {
	     		return false;
	        } 
		}); 
		return false;
	}
 </script>
<div id="main-content" class="hr_main_content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay Information
		</div>
		<div class="width100 float-left" id="hrm">
			<input type="hidden" name="payrollId" id="payrollId"
				value="${PAYROLL_PROCESS.payrollId}" class="width50" />
			<input type="hidden" name="viewRedirect" id="viewRedirect"
				value="${viewRedirect}" class="width50" />
				
			<div class="width50 float-left">
				<fieldset style="height: auto;">
					<legend>
						<fmt:message key="hr.attendance.employeeinfo" />
					</legend>
					<div class="float-left">
						<c:set var="jobAssignment"
							value="${PAYROLL_PROCESS.jobAssignment}" />
						<input type="hidden" name="jobAssignmentId" id="jobAssignmentId"
							value="${jobAssignment.jobAssignmentId}"
							class="width50 validate[required]" />
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.attendance.employee" /> : </label> <span id="employeeName"
								class="float-left width30">
								${jobAssignment.person.firstName}
								${jobAssignment.person.lastName} </span> <span
								class="button float-right" id="employee"
								style="position: relative; top: 6px; display: none;"> <a
								style="cursor: pointer;" id="employeepopups"
								class="btn ui-state-default ui-corner-all width40 employee-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
								type="hidden" id="personId" value="" class="personId"
								value="${jobAssignment.person.personId}" style="border: 0px;" />
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.attendance.job" /> : </label> <label class="width50 label-data"
								id="designationName">${jobAssignment.designation.designationName}</label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.attendance.grade" /> : </label> <label
								class="width50 label-data" id="gradeName">${jobAssignment.designation.grade.gradeName}</label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.attendance.company" /> : </label> <label
								class="width50 label-data" id="companyName">${jobAssignment.cmpDeptLocation.company.companyName}</label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.attendance.department" /> : </label> <label
								class="width50 label-data" id="departmentName">${jobAssignment.cmpDeptLocation.department.departmentName}</label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.attendance.location" /> : </label> <label
								class="width50 label-data" id="locationName">${jobAssignment.cmpDeptLocation.location.locationName}</label>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="width50 float-left">
				<fieldset style="height: auto;">
					<legend>
						<fmt:message key="hr.payroll.masterinfo" />
					</legend>
					<div class="float-left">
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.payroll.paymonth" /> : </label> <label
								class="width50 label-data">${PAYROLL_PROCESS.payMonth}</label>
						</div>

						<div class="float-left width100" style="padding: 2px;">
							<c:set var="startDate" value="${PAYROLL_PROCESS.payPeriodTransaction.startDate}" />
							<%String startDate = DateFormat.convertDateToString(pageContext.getAttribute("startDate").toString());%>
							<label class="width30"><fmt:message
									key="hr.payroll.periodfrom" /> : </label> <label
								class="width50 label-data"><%=startDate%></label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<c:set var="endDate" value="${PAYROLL_PROCESS.payPeriodTransaction.endDate}" />
							<%String endDate = DateFormat.convertDateToString(pageContext.getAttribute("endDate").toString());%>
							<label class="width30"><fmt:message
									key="hr.payroll.periodend" /> : </label> <label
								class="width50 label-data"><%=endDate%></label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.payroll.numberofdays" /> : </label> <label
								class="width50 label-data">${PAYROLL_PROCESS.numberOfDays}</label>
						</div>
						<div class="float-left width100" style="padding: 2px;">
							<label class="width30"><fmt:message
									key="hr.payroll.deductiondays" /> : </label> <label
								class="width50 label-data">${PAYROLL_PROCESS.lopDays}</label>
						</div>
						<div class="float-left width50"
							style="background-color: #006699; color: white; font-weight: bold; padding:5px;">
							<label class="width50"><fmt:message
									key="hr.payroll.netpay" /> : </label> <label class="width50 label-data">${PAYROLL_MASTER.netAmount}</label>
						</div>
					</div>
				</fieldset>

			</div>
			<div class="width100 float-left">
				<fieldset style="height: auto;">
					<legend>Provided Pay Information</legend>
					<div id="hrm" class="hastable width100">
						<table class="width100">
							<thead>
								<tr>
									<th style="width: 5%"><fmt:message
											key="hr.payroll.elementname" />
									</th>
									<th style="width: 5%">Calculation Type</th>
									<th style="width: 5%">Value(Max)</th>
									<th style="width: 5%">Amount</th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:choose>
									<c:when
										test="${JOB_PAYROLL_ELEMENTS ne null && JOB_PAYROLL_ELEMENTS ne '' && fn:length(JOB_PAYROLL_ELEMENTS)>0}">
										<c:forEach var="pay1" items="${JOB_PAYROLL_ELEMENTS}"
											varStatus="status3">
											<tr class="rowid">
												<td><span>${pay1.payrollElementByPayrollElementId.elementName}</span>
												</td>
												<td><span
													id="actualPayelementNature_${status3.index+1}">${pay1.calculationTypeView}</span>
												</td>
												<td><span id="actualPayValue_${status3.index+1}"
													class="actualPayValue">${pay1.amount}</span></td>
												<td><span id="actualPayAmount_${status3.index+1}"
													class="actualPayAmount">${pay1.consolidatedAmount}</span></td>
											</tr>
										</c:forEach>
									</c:when>
								</c:choose>
							</tbody>
						</table>

					</div>

				</fieldset>
			</div>
			<div class="width100 float-left">
				<fieldset style="height: auto;">
					<legend>Projected Pay Details</legend>
					<div id="hrm" class="hastable width100">
						<table id="hastab2" class="width100">
							<thead>
								<tr>
									<th style="width: 15%"><fmt:message
											key="hr.payroll.elementname" />
									</th>
									<th style="width: 5%">Calculated Amount</th>
									<th style="width: 5%">Earning</th>
									<th style="width: 5%">Deduction</th>
									<th style="width: 15%">Note</th>
								</tr>
							</thead>
							<tbody class="tabS">
								<c:choose>
									<c:when
										test="${PAYROLL_PROCESS.payrollTransactions ne null && PAYROLL_PROCESS.payrollTransactions ne '' && fn:length(PAYROLL_PROCESS.payrollTransactions)>0}">
										<c:forEach var="pay"
											items="${PAYROLL_PROCESS.payrollTransactions}"
											varStatus="status1">
											<tr class="rowidS" id="fieldrowS_${status1.index+1}">
												<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
												<td>${pay.payrollElement.elementName}</td>
												<c:set var="amount" value="${pay.amount}" />
												<c:set var="calculatedAmount" value="${pay.calculatedAmount}" />
												<td><%=AIOSCommons.formatAmount(pageContext
															.getAttribute("calculatedAmount"))
														%></td>
												<c:choose>
													<c:when test="${pay.payrollElement.elementNature eq 'EARNING'}">
										
														<td><%=AIOSCommons.formatAmount(pageContext
															.getAttribute("amount"))
														%></td>
														<td></td>
													</c:when>
													<c:otherwise>
														<td></td>
														<td><%=AIOSCommons.formatAmount(pageContext
															.getAttribute("amount"))
														%></td>
													</c:otherwise>
												</c:choose>
												<td>${pay.note}</td>

											</tr>
										</c:forEach>
									</c:when>
								</c:choose>

							</tbody>
						</table>
						<div class="width60 float-right" style="padding:5px;border-bottom: 1px solid #755800;">
							<label class="caption width10 float-left" >Total : </label>
							<label class="data width20 float-left">Earning : ${PAYROLL_MASTER.earningTotal}</label>
							<label class="data width20 float-left">Deduction : ${PAYROLL_MASTER.deductionTotal}</label>
							<label class="data width30 float-left">&nbsp;&nbsp;&nbsp;</label>
						</div>
					</div>

				</fieldset>
			</div>
		</div>

		<!-- LOP code -->
		<c:choose>
			<c:when test="${fn:length(LOP_LIST)gt 0}">
				<div class="width100 float-left">
					<span class="side-heading">LOP(Loss Of Pay) Information</span>
				</div>
				<div class="data-list-container width60 float-left"
					style="margin-top: 10px;">
					<table class="width100 float-left">
						<thead>
							<tr>
								<th>Period</th>
								<th>Other Detail</th>
								<th>Amount</th>

							</tr>
						</thead>
						<tbody>

							<c:forEach items="${LOP_LIST}" var="result3" varStatus="status1">
								<tr>
									<td>From : ${result3.fromDate } upto: ${result3.toDate } - ${result3.noOfDays}(days)</td>
									<td>${result3.details }</td>
									<td>${result3.amount }</td>

								</tr>
							</c:forEach>

						</tbody>
					</table>
					<div class="width15 float-right"
						style="padding: 5px; border-bottom: 1px solid #755800;">
						<label class="caption width40 float-left">Total : </label> <label
							class="data width50 float-left">${LOP_MASTER}</label>
					</div>
				</div>
			</c:when>
		</c:choose>
		<!-- Over Time code -->
		<c:choose>
			<c:when test="${fn:length(OT_LIST)gt 0}">
				<div class="width100 float-left">
					<span class="side-heading">OT(Over Time) Information</span>
				</div>
				<div class="data-list-container width60 float-left"
					style="margin-top: 10px;">
					<table class="width100 float-left">
						<thead>
							<tr>
								<th>Period</th>
								<th>No. Of Days</th>
								<th>Other Detail</th>
								<th>Amount</th>

							</tr>
						</thead>
						<tbody>

							<c:forEach items="${OT_LIST}" var="result3" varStatus="status1">
								<tr>
									<td>From : ${result3.fromDate } upto: ${result3.toDate }</td>
									<td>${result3.noOfDays }</td>
									<td>${result3.details }</td>
									<td>${result3.amount }</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
					<div class="width15 float-right"
						style="padding: 5px; border-bottom: 1px solid #755800;">
						<label class="caption width40 float-left">Total : </label> <label
							class="data width50 float-left">${OT_MASTER}</label>
					</div>
				</div>
			</c:when>
		</c:choose>
	</div>
	<br>
	<br>
	

</div>
