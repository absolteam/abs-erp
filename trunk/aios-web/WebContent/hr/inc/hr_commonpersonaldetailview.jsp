<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>

<style>
.fileinput-preview {
  position: relative;
  float: right;
  top: 35px;
  right:60px;
}
.fileinput-button {
  position: relative;
  overflow: hidden;
  float: right;
  right: 100px;
  top: 5px;
}
.fileinput-button input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  filter: alpha(opacity=0);
  transform: translate(-300px, 0) scale(4);
  font-size: 23px;
  direction: ltr;
  cursor: pointer;
}
</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var temp;
var page;
var employeeCombinationIdOld=0;
var tenantCombinationIdOld=0;
var supplierCombinationIdOld=0;
var customerCombinationIdOld=0;
var ownerCombinationIdOld=0;
var fileUpload=null;
var j = jQuery.noConflict();
$(function(){  
	$("#tradeExpireDate").datepick();
	//added by sudipta 
	
	$("#personType").change(function(){
		var personTypeArray=new Array();
		$('#personType option:selected').each(function(){
			personTypeArray.push($(this).val());
		});
		//alert(personTypeArray);
		$('#personType option').each(function(){
			var personTypeId=$(this).val();
				//alert($(this).text()+"("+$(this).val()+") In array---->"+$.inArray(personTypeId, personTypeArray));
				//Employeement
				if(personTypeId==1 && $.inArray(personTypeId, personTypeArray)>=0 ){
					employeeShowHideCall(true);
				}else if(personTypeId==1 && $.inArray(personTypeId, personTypeArray)== -1){
					employeeShowHideCall(false);
				}
				
				//Tenant
				if(personTypeId==2 && $.inArray(personTypeId, personTypeArray)>=0){
					tenantShowHideCall(true);
				}else if(personTypeId==2 && $.inArray(personTypeId, personTypeArray)== -1){
					tenantShowHideCall(false);
				}
				
				//Owner
				if(personTypeId==3 && $.inArray(personTypeId, personTypeArray)>=0){
					ownerShowHideCall(true);
				}else if(personTypeId==3 && $.inArray(personTypeId, personTypeArray)== -1){
					ownerShowHideCall(false);
				}
				
				//Supplier
				if(personTypeId==7 && $.inArray(personTypeId, personTypeArray)>=0){
					supplierShowHideCall(true);
				}else if(personTypeId==7 && $.inArray(personTypeId, personTypeArray)== -1){
					supplierShowHideCall(false);
				}
				
				//Customer
				if(personTypeId==8 && $.inArray(personTypeId, personTypeArray)>=0){
					customerShowHideCall(true);
				}else if(personTypeId==8 && $.inArray(personTypeId, personTypeArray)== -1){
					customerShowHideCall(false);
				}
		});
			
	});
	
	
	 var tempvar=$('.identitytab>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#identityDeleteImage_'+rowid).hide(); 
	 
	 tempvar=$('.tab>tr:last').attr('id'); 
	 idval=tempvar.split("_");
	 rowid=Number(idval[1]); 
	 $('#DeleteImage_'+rowid).hide(); 

	
	$('#birthdate').datepick({ 
	    onSelect:CallAge,maxDate: 0,yearRange: 'any', showTrigger: '#calImg'}); 

	$("#close").click(function(){ // discard add
		if (typeof callPersonViewClose == 'function'){
			$('#common-popup').dialog("close");
			callPersonViewClose();

		}
	});

   		
	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	
	$('.startPicker').click(function(){
		startpick=($(this).attr('class'));
		$($($($(this).parent().parent()).children().get(3)).children().get(0)).val("");
	});
	 
	$('.startPicker,.endPicker').datepick({
		onSelect: customRange,yearRange: 'any', showTrigger: '#calImg'});
    
	$('#effectiveFromDate').datepick({ 
		yearRange: 'any', showTrigger: '#calImg'}); 

	
	
		 
	// This call only applicable for at the time of hiring process. 
		var dates=$('#birthdate').val();
		if(dates!=null && dates!= ""){
			CallAge();
		}	
		

	

		if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
			$('#themeRollerSelect option:not(:selected)').remove();
		}

		 $('#fname').focus();


	//Identity Detail adding
		 $(".identityaddData").click(function(){ 
			slidetab=$(this).parent().parent().get(0);  
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 if($jquery("#periodEditAddValidate").validationEngine('validate')){      
					$("#identityAddImage_"+rowid).hide();
					$("#identityEditImage_"+rowid).hide();
					$("#identityDeleteImage_"+rowid).hide();
					$("#identityWorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/person_identity_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			}
			else{
				return false;
			}	
			 
		});

		$(".identityeditData").click(function(){
			 
			 slidetab=$(this).parent().parent().get(0);  
			 
			 if($jquery("#periodEditAddValidate").validationEngine('validate')){      
				 
					 
					//Find the Id for fetch the value from Id
						var tempvar=$(this).attr("id");
						var idarray = tempvar.split('_');
						var rowid=Number(idarray[1]); 
						$("#identityAddImage_"+rowid).hide();
						$("#identityEditImage_"+rowid).hide();
						$("#identityDeleteImage_"+rowid).hide();
						$("#identityWorkingImage_"+rowid).show(); 
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/person_identity_add.action",
					 data:{id:rowid,addEditFlag:"E"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){
					
				   $(result).insertAfter(slidetab);

				 //Bussiness parameter
		   			
		   			var identityType= $('#identityType_'+rowid).text();
		   			var identityNumber=$('#identityNumber_'+rowid).text(); 
      				var idendityExpireDate=$('#identityExpireDate_'+rowid).text(); 
      				var identityIssuedPlace=$('#identityIssuedPlace_'+rowid).text();
      				var identityDescription=$('#identityDescription_'+rowid).text();
		   			var identityTypeId= $('#identityTypeId_'+rowid).val(); 
		   			idendityExpireDate=idendityExpireDate.substring(0,11);
      				$('#identityType').val(identityTypeId);
      				$('#identityNumber').val(identityNumber);
      				$('#idendityExpireDate').val(idendityExpireDate);
      				$('#identityIssuedPlace').val(identityIssuedPlace);
      				$('#identityDescription').val(identityDescription);
				    $('#openFlag').val(1);
				  
				},
				error:function(result){
				//	alert("wee"+result);
				}
				
				});
				 			
			 }
			 else{
				 return false;
			 }	
			 
		});
		 
		 $(".identitydelrow").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
				var tempvar=$(this).attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 var lineId=$('#identitylineId_'+rowid).val();
			
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/person_identity_save.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.tempresult').html(result);   
						 if(result!=null){
							 $("#identityfieldrow_"+rowid).remove(); 
							  var identitycount=Number($('#identitycount').val()); 
	         				  identitycount-=1;
	         				  $('#identitycount').val(identitycount);
				        		//To reset sequence 
				        		var i=0;
				     			$('.identityrowid').each(function(){  
									i=i+1;
									$($($(this).children().get(4)).children().get(1)).val(i); 
			   					 }); 
						     }  
					},
					error:function(result){
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 });
				 
			 
		 });


		 $('.identityaddrows').click(function(){
			 var i=0;
			 var lastRowId=$('.identityrowid:last').attr('id');
				if(lastRowId!=null){
					var idarray = lastRowId.split('_');
					//alert(idarray);
					
					rowid=Number(idarray[1]);
					rowid=Number(rowid+1);
				}else{
					rowid=Number(1);
				}
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/person_identity_addrow.action",
					data:{id:rowid}, 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					$(".identitytab").append(result);
					//To reset sequence 
					 $('.identityrowid').each(function(){  
							i=i+1;
							var tempvar=$(this).attr("id");
							var idarray = tempvar.split('_');
							var rowid1=Number(idarray[1]); 
							$('#identitylineId_'+rowid1).val(i);
							
						});  
					} 
	  			
				});
			});

			
		 //Dependent Detail adding
		 $(".addData").click(function(){ 
			slidetab=$(this).parent().parent().get(0);  
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 if($jquery("#periodEditAddValidate").validationEngine('validate')){      
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/person_dependent_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			}
			else{
				return false;
			}	
			 
		});

		$(".editData").click(function(){
			 
			 slidetab=$(this).parent().parent().get(0);  
			 
			 if($jquery("#periodEditAddValidate").validationEngine('validate')){      
				 
					 
					//Find the Id for fetch the value from Id
						var tempvar=$(this).attr("id");
						var idarray = tempvar.split('_');
						var rowid=Number(idarray[1]); 
						$("#AddImage_"+rowid).hide();
						$("#EditImage_"+rowid).hide();
						$("#DeleteImage_"+rowid).hide();
						$("#WorkingImage_"+rowid).show(); 
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/person_dependent_add.action",
					 data:{id:rowid,addEditFlag:"E"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){
					
				   $(result).insertAfter(slidetab);

				 //Bussiness parameter
		   			var dependentName=$('#dependentName_'+rowid).text();
		   			var relationship=$('#dependentRelationship_'+rowid).text();
		   			var relationshipId=$('#dependentRelationshipId_'+rowid).val(); 
        			var dependentMobile=$('#dependentMobile_'+rowid).text(); 
        			var dependentPhone=$('#dependentPhone_'+rowid).text(); 
        			var dependentMail=$('#dependentEmail_'+rowid).text();
        			var emergency=$('#emergency_'+rowid).text();
        			var dependent=$('#dependent_'+rowid).text();
        			var benefit=$('#benefit_'+rowid).text();
        			var dependentAddress=$('#dependentAddress_'+rowid).text();
        			var dependentDescription=$('#dependentDescription_'+rowid).val();
        			
		   			
        			$('#relationship').val(relationshipId);
        			$('#dependentName').val(dependentName);
        			$('#dependentMobile').val(dependentMobile);  
        			$('#dependentPhone').val(dependentPhone);
        			$('#dependentMail').val(dependentMail);
        			$('#dependentAddress').val(dependentAddress);
        			$('#dependentDescription').val(dependentDescription);
        			if(emergency.trim()=='YES')
        				$('#emergency').attr("checked","checked");
        			if(dependent.trim()=='YES')	
        				$('#dependent').attr("checked","checked");
        			if(benefit.trim()=='YES')	
        				$('#benefit').attr("checked","checked");
        				
				    $('#openFlag').val(1);
				   
				},
				error:function(result){
				//	alert("wee"+result);
				}
				
				});
				 			
			 }
			 else{
				 return false;
			 }	
			 
		});
		 
		 $(".delrow").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 var lineId=$('#lineId_'+rowid).val();
			var flag=false;
			
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/person_dependent_save.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
					 success:function(result){ 
						 $('.tempresult').html(result);   
						 if(result!=null){
							 $("#fieldrow_"+rowid).remove();  
				        		//To reset sequence 
				        		var i=0;
				     			$('.rowid').each(function(){  
									i=i+1;
									$($($(this).children().get(4)).children().get(1)).val(i); 
			   					 }); 
						     }  
					},
					error:function(result){
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 });
				 
			 
		 });
		 
		 $('.addrows').click(function(){
			 var i=0;
			 var lastRowId=$('.rowid:last').attr('id');
				if(lastRowId!=null){
					var idarray = lastRowId.split('_');
					//alert(idarray);
					
					rowid=Number(idarray[1]);
					rowid=Number(rowid+1);
				}else{
					rowid=Number(1);
				}
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/person_dependent_addrow.action",
					data:{id:rowid}, 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
						$(".tab").append(result);
					//To reset sequence 
					 $('.rowid').each(function(){
							i=i+1;
							var tempvar=$(this).attr("id");
							var idarray = tempvar.split('_');
							var rowid1=Number(idarray[1]); 
							$('#lineId_'+rowid1).val(i);
						});  
					} 
	  			
				});
			});
		
		 // File upload
		 $('#person_document_information').click(function(){
			if(personId>0){
				AIOS_Uploader.openFileUploader("doc","personDocs","Person",personId,"PersonDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","personDocs","Person","-1","PersonDocuments");
			}
		});
			
		 $('#person_image_information').click(function(){
			if(personId>0){
				AIOS_Uploader.openFileUploader("img","personImages", "Person", personId,"PersonImages");
			}else{
				AIOS_Uploader.openFileUploader("img","personImages","Person","-1","PersonImages");
			}
		});

		(document.PersonalDetails.personType.value=(document.getElementById('personTypeTemp').value)).selected='true';
		(document.PersonalDetails.countryBirth.value=(document.getElementById('countryBirthTemp').value)).selected='true';	 	 
		(document.PersonalDetails.gender.value=(document.getElementById('genderTemp').value)).selected='true';
		(document.PersonalDetails.registerDisabled.value=(document.getElementById('registerDisabledTemp').value)).selected='true';
		(document.PersonalDetails.maritalStatus.value=(document.getElementById('maritalTemp').value)).selected='true';
		(document.PersonalDetails.nationality.value=(document.getElementById('nationalityTemp').value)).selected='true';	 
		(document.PersonalDetails.personGroup.value=(document.getElementById('nationalityTypeIdTemp').value)).selected='true';
		//(document.PersonalDetails.nationalityTypeName.value=(document.getElementById('nationalityTypeNameTemp').value)).selected='true';	 
		//Default Date Picker
		 var personId=Number($("#personId").val());
		 if(personId==0){  
			 var curDate=new Date().toString();
				var splitDate=curDate.split(" ");
				var day=splitDate[2];
				var mon=splitDate[1];
				var year=splitDate[3];
				fromDate=day+"-"+mon+"-"+year;
				 $('#effectiveFromDate').val(fromDate);
		  
		 }else{

			 //Person Type Selected Items trigger
				personTypeAutoSelectd();
			 
			//Document Load
				populateUploadsPane("img","personImages","Person",personId);
				populateUploadsPane("doc","personDocs","Person",personId);	
				
				
			//Combination Temp Store for Re use to show/hide
				employeeCombinationIdOld=Number($('#employeeCombinationId').val());
				tenantCombinationIdOld=Number($('#tenantCombinationId').val());
				supplierCombinationIdOld=Number($('#supplierCombinationId').val());
				customerCombinationIdOld=Number($('#customerCombinationId').val());
				ownerCombinationIdOld=Number($('#ownerCombinationId').val());
				
				//alert(employeeCombinationIdOld+"--"+tenantCombinationIdOld+"--"+customerCombinationIdOld);
				
		 }
		 
		//Combination pop-up config
			$('.codecombination-popup').click(function(){ 
			      tempid=$(this).get(0);   
			      $('.ui-dialog-titlebar').remove(); 
				 	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/combination_treeview.action", 
					 	async: false,  
					    dataType: "html",
					    cache: false,
						success:function(result){   
							 $('.codecombination-result').html(result);  
						},
						error:function(result){ 
							 $('.codecombination-result').html(result); 
						}
					});  
			});
		
			$('#codecombination-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800, 
				bgiframe: false,
				modal: true 
			});
			
			$('#next').click(function(){
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/personal_details_view.action",  
			     	data: {personId:personId,viewType:"NEXT"},  
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(data)
			     	{  
						$("#main-wrapper").html(data); //gridDiv main-wrapper 
			     	}
				});
			});
				
			$('#previous').click(function(){
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/personal_details_view.action",  
			     	data: {personId:personId,viewType:"PREVIOUS"},  
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(data)
			     	{  
						$("#main-wrapper").html(data); //gridDiv main-wrapper 
			     	}
				});
			});

			 
		
});

			

//Prevent selection of invalid dates through the select controls 
function checkLinkedDays() { 
    var daysInMonth = $.datepick.daysInMonth(
		$('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) { 
        $('#selectedDay').val(daysInMonth); 
    } 
}


function customRange(dates) {
	 
	if (($(this).attr('class')) == startpick) {
			$('.endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$(startpick).datepick('option', 'maxDate', dates[0] || null);
		}
}

function personTypeAutoSelectd() {
	 
	 var personTypeArray=new Array();
	 var personTypeTemp=$('#personTypeIdTemp').val();
	  personTypeArray=personTypeTemp.split(',');
	$('#personType option').each(function(){
		var txt=$(this).val();
		if($.inArray(txt, personTypeArray)>=0)
			$(this).attr("selected","selected");
	});
	$('#personType').trigger('change');

}

function CallAge() {
	
	fromDate=$('#birthdate').val();
		var curDate=new Date().toString();
		var splitDate=curDate.split(" ");
		var day=splitDate[2];
		var mon=splitDate[1];
		var year=splitDate[3];
	toDate=day+"-"+mon+"-"+year;
	putYears=$('#age');
	putMonths=$('#tempMon');
	putDays=$('#tempDays');
	calculateDate(fromDate,toDate,putYears,putMonths,putDays);
	if($('#age').val()=="NaN" && " "){
		$('#age').val("0");
	}

		
}

// Ajax Call for Adding Person Details
function personDetailsAjaxCall(flag) {  
	$('.error').hide();
	
	 if($jquery("#periodEditAddValidate").validationEngine('validate')){      
				var recId=document.PersonalDetails.personId.value;
				var firstName=document.PersonalDetails.firstName.value;
				var firstNameArabic=document.PersonalDetails.firstNameArabic.value;
				var lastName=document.PersonalDetails.lastName.value;
				var lastNameArabic=document.PersonalDetails.lastNameArabic.value;
				var prefix=document.PersonalDetails.prefix.value;
				var suffix=document.PersonalDetails.suffix.value;
				var middleName=document.PersonalDetails.middleName.value;
				var middleNameArabic=document.PersonalDetails.middleNameArabic.value;
				var personTypes=document.PersonalDetails.personType.value;
				var identification=document.PersonalDetails.identification.value;
				var effectiveDatesFrom=document.PersonalDetails.effectiveDatesFrom.value;
				var effectiveDatesTo=document.PersonalDetails.effectiveDatesTo.value;
				var birthDate=document.PersonalDetails.birthDate.value;
				var townBirth=document.PersonalDetails.townBirth.value;
				var regionBirth=document.PersonalDetails.regionBirth.value;
				var countryBirth=document.PersonalDetails.countryBirth.value;
				var gender=document.PersonalDetails.gender.value;
				var maritalStatus=document.PersonalDetails.maritalStatus.value;
				var nationality=document.PersonalDetails.nationality.value;
				var personGroup=document.PersonalDetails.personGroup.value;
				var personFlag=Number($('#registerDisabled :selected').val()); //4;  			//document.PersonalDetails.disabled.value;
				
				
				var personTypeList=new Array();
				$('#personType option:selected').each(function(){
					personTypeList.push($(this).val());
				});
				var personTypes="";
				for(var i=0; i<personTypeList.length;i++){
					personTypes+=personTypeList[i]+",";
				}
				var residentialAddress=$("#residentialAddress").val();
				var permanentAddress=$("#permanentAddress").val();
				var mobile=$("#mobile").val();
				var alternateMobile=$("#alternateMobile").val();
				var telephone=$("#telephone").val();
				var alternateTelephone=$("#alternateTelephone").val();
				var postBoxNumber=$("#postBoxNumber").val();
				var email=$("#email").val();
		
				var tradeLicenseNumber=$("#tradeLicenseNumber").val();
				var tradeIssuedPlace=$("#tradeIssuedPlace").val();
				var tradeExpireDate=$("#tradeExpireDate").val();
				var tradeDescription=$("#tradeDescription").val();
		
				var combinationId=null; 
				var employeeCombinationId=Number($('#employeeCombinationId').val()); 
				var tenantCombinationId=Number($('#tenantCombinationId').val()); 
				var supplierCombinationId=Number($('#supplierCombinationId').val()); 
				var customerCombinationId=Number($('#customerCombinationId').val()); 

				var supplierinfo=$('#supplierinfo').val();
				var customerinfo=$('#customerinfo').val();
				var supplierAcType=Number(0);
				var acClassification=Number(0);
				var paymentTermId=Number(0);
				var creditLimit=Number(0);
				var openingBalance=Number(0);
				var locationId=Number(0);
				var departmentId=Number(0);  
				var bankName="";
				var branchName="";
				var iban="";
				var accountNumber="";
				var currencyId=Number(0);
				var supplierId=Number(0);
				var supplierNumber="";
				var customerNumber = "";
				var customerAcType =Number(0);
				var creditTermId = Number(0);
				var saleRepresentativeId = Number(0); 
				var billingAddress = "";
				var refferalSource = Number(0);
				var shippingMethod = Number(0); 
				var customerDescription= "";
				var shippingDetails ="";
				var customerId = Number(0);
				var customerFlag = true; 
				var isSupplierType=$('#supplierAcType').val();
				if(typeof isSupplierType!="undefined"){
					supplierAcType=$('#supplierAcType').val();
					acClassification=$('#acClassification').val();
					paymentTermId=Number($('#paymentTermId').val());
					creditLimit=$('#creditLimit').val();
					openingBalance=Number($('#openingBalance').val());
					bankName=$('#bankName').val();
					branchName=$('#branchName').val();
					iban=$('#iban').val();
					accountNumber=$('#accountNumber').val();  
					currencyId=Number($('#currencyId').val());
					supplierId=$('#supplierId').val();
					supplierNumber=$('#supplierNumber').val(); 
					if(supplierCombinationId==null || supplierCombinationId=='' || supplierCombinationId==0){
						$('.commonErr').hide().html("Please choose supplier account").slideDown();
						return false; 
					}
				}  else if(typeof $('#customerNumber').val() !="undefined") {
					customerNumber = $('#customerNumber').val();
					customerAcType = Number($('#customerAcType').val());
					creditTermId = Number($('#creditTermId').val());
					saleRepresentativeId = Number($('.saleRepresentativeId ').val());
					creditLimit =Number($('#creditLimit').val());
					billingAddress = $('#billingAddress').val();
					refferalSource = Number($('#refferalSource').val());
					shippingMethod = Number($('#shippingMethod').val());
					openingBalance = Number($('#openingBalance').val());
					customerDescription = $('#customerDescription').text();
					locationId=$('#locationId').val();
					departmentId=$('#departmentId').val();
					shippingDetails = getShippingDetails();
					customerId = Number($('#customerId').val());
					if(shippingDetails!= null && shippingDetails !=""){
						customerFlag = true;
					} else {
						customerFlag = false;
					}
					if(customerCombinationId==null || customerCombinationId=='' || customerCombinationId==0){
						$('#errordiv').hide().html("Please choose customer account").slideDown();
						return false; 
					}
				}  
				
			
				var errorMessage=null;
				var successMessage="Successfully Created";
				if(recId>0)
					var successMessage="Successfully Modified"; 
				    $.ajax({  
						type: "POST", 
						url: "<%=request.getContextPath()%>/personal_details_add.action", 
						data: {	recId:recId,identification:identification,
								firstName: firstName,firstNameArabic: firstNameArabic, lastName: lastName,lastNameArabic: lastNameArabic,prefix: prefix,suffix: suffix, middleName: middleName,middleNameArabic: middleNameArabic,personTypes: personTypes, 
								effectiveDatesFrom: effectiveDatesFrom,effectiveDatesTo: effectiveDatesTo,birthDate: birthDate, townBirth: townBirth,regionBirth: regionBirth, 
								countryBirth: countryBirth, gender: gender, maritalStatus: maritalStatus,nationality: nationality,
								personGroup: personGroup,personFlag:personFlag,
								residentialAddress:residentialAddress,
								permanentAddress:permanentAddress,
								mobile:mobile,
								supplierId:supplierId,
								supplierNumber:supplierNumber,
								alternateMobile:alternateMobile,
								telephone:telephone,
								alternateTelephone:alternateTelephone,
								postBoxNumber:postBoxNumber,
								email:email,
								tradeLicenseNumber:tradeLicenseNumber,
								tradeIssuedPlace:tradeIssuedPlace,
								tradeExpireDate:tradeExpireDate,
								tradeDescription:tradeDescription,
								//combinationId:combinationId,
								isSupplier:supplierinfo,
								supplierAcType:supplierAcType,
								acClassification:acClassification,
								paymentTermId:paymentTermId,
								openingBalance:openingBalance,
								bankName:bankName,
								branchName:branchName,
								ibanNumber:iban,
								accountNumber:accountNumber,
								creditLimit:creditLimit,
								currencyId:currencyId,
								customerNumber: customerNumber,
								customerAcType: customerAcType,
								creditTermId: creditTermId,
								saleRepresentativeId: saleRepresentativeId, 
								billingAddress: billingAddress,
								refferalSource: refferalSource,
								shippingMethod: shippingMethod, 
								customerDescription: customerDescription,
								shippingDetails: shippingDetails,
								customerId: customerId,
								isCustomer: customerinfo,
								employeeCombinationId:employeeCombinationId,
								tenantCombinationId:tenantCombinationId,
								supplierCombinationId:supplierCombinationId,
								customerCombinationId:customerCombinationId
							},
						async: false,
						dataType: "html",
						cache: false,
						success:function(result){
							$(".tempresult").html(result);
							 var message=$('.tempresult').html(); 
							 if(message.trim()=="SUCCESS"){
								
							 }else{
								 errorMessage=message;
							 }
							if(supplierinfo==true || supplierinfo=='true'){
									returnCallToSupplier();
							} else if (customerinfo==true || customerinfo=='true') {
								returnCallToCustomer();
							} else {
								returnCallToPerson();
							}
							if(errorMessage==null)
								$('.success').hide().html(successMessage).slideDown();
							else
								$('.error').hide().html(errorMessage).slideDown();
						},  
						error:function(result){
								$('#codecombination-popup').dialog('destroy');		
						  		$('#codecombination-popup').remove(); 
								$("#main-wrapper").html(result); 
						 	} 
				
					});
					return true; 
			
	}		
	else
	{
		return false;
	} 
}
function returnCallToPerson(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/hr_personal_details_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}
function returnCallToSupplier(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/supplier_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}
function returnCallToCustomer(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_all_customers.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){  
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();		
			$("#main-wrapper").html(result);  
		}
 	});
}
function identityShowHideCall(showFlag){
	if(showFlag==true){
		$("#identitytab").show();
		$("#tabs-2").show();
	}else{
		$("#identitytab").hide();
		$("#tabs-2").hide();
	}
}
function dependentShowHideCall(showFlag){
	if(showFlag==true){
		$("#dependenttab").show();
		$("#tabs-3").show();
	}else{
		$("#dependenttab").hide();
		$("#tabs-3").hide();
	}
}
function employeeShowHideCall(showFlag){
	if(showFlag==true){
		$("#employeement-account-div").show();
	}else{
		$("#employeement-account-div").hide();
		$("#employeeAccount").removeClass('validate[required]');
		if(employeeCombinationIdOld==0)
			$("#employeeCombinationId").val("");
	}
}
function ownerShowHideCall(showFlag){
	if(showFlag==true){
		$("#ownertab").show();
		$("#tabs-4").show();
	}else{
		$("#ownertab").hide();
		$("#tabs-4").hide();
		$("#tradeLicenseNumber").val("");
		$("#tradeIssuedPlace").val("");
		$("#tradeExpireDate").val("");
		$("#tradeDescription").val("");
	}
}
function tenantShowHideCall(showFlag){
	if(showFlag==true){
		$("#tenanttab").show();
		$("#tabs-5").show();
		//$("#tenantAccount").addClass('validate[required]');
	}else{
		$("#tenanttab").hide();
		$("#tabs-5").hide();
		$("#tenantAccount").removeClass('validate[required]');
		if(tenantCombinationIdOld==0){
			$("#tenantAccount").val("");
			$("#tenantCombinationId").val("");
		}
	}
}
function supplierShowHideCall(showFlag){
	if(showFlag==true){
		$("#suppliertab").show();
		$("#tabs-6").show();
		//$("#supplierAccount").addClass('validate[required]');
	}else{
		$("#suppliertab").hide();
		$("#tabs-6").hide();
		$("#supplierAccount").removeClass('validate[required]');
		if(supplierCombinationIdOld==0){
			$("#supplierCombinationId").val("");
			$("#supplierAccount").val("");
		}
	}
}
function customerShowHideCall(showFlag){
	if(showFlag==true){
		$("#customertab").show();
		$("#tabs-7").show();
		//$("#customerAccount").addClass('validate[required]');
	}else{
		$("#customertab").hide();
		$("#tabs-7").hide();
		$("#customerAccount").removeClass('validate[required]');
		if(customerCombinationIdOld==0){
			$("#customerAccount").val("");
			$("#customerCombinationId").val("");
		}
	}
}
function getShippingDetails(){
	var shippingInfo = "";
	var shipname=new Array();
	var shipperson=new Array();
	var shipcontact=new Array();
	var shipaddress=new Array();
	var shipid=new Array(); 
	$('.customer-rowid').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var contactName=$('#contactName_'+rowId).val();
		var contactPerson=$('#contactPerson_'+rowId).val();
		var contactNo=$('#contactNo_'+rowId).val();
		var customerAddress=$('#customeraddress_'+rowId).val();  
		var shippingId=Number($('#shippingId_'+rowId).val()); 
		if(typeof contactName != 'undefined' && contactName!=null && contactPerson!="" && contactPerson!=null){
			shipname.push(contactName);
			shipperson.push(contactPerson); 
			shipid.push(shippingId);
			if(contactNo!=null && contactNo!="")
				shipcontact.push(contactNo);
			else
				shipcontact.push("##"); 
			if(customerAddress!=null && customerAddress!="")
				shipaddress.push(customerAddress);
			else
				shipaddress.push("##"); 
		} 
	});
	for(var j=0;j<shipname.length;j++){ 
		shippingInfo+=shipname[j]+"__"+shipperson[j]+"__"+shipcontact[j]+"__"+shipaddress[j]+"__"+shipid[j];
		if(j==shipname.length-1){   
		} 
		else{
			shippingInfo+="#@";
		}
	} 
	return shippingInfo;
}

function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}

function setCombination(combinationTreeId,combinationTree){
	tempid=$(tempid).attr('id'); 
	if(typeof tempid!='undefined'&& tempid=="employeeCall"){
		$('#employeeAccount').val(combinationTree);
		$('#employeeCombinationId').val(combinationTreeId); 
	}
	else if(typeof tempid!='undefined'&& tempid=="tenantCall"){
		$('#tenantAccount').val(combinationTree);
		$('#tenantCombinationId').val(combinationTreeId);   
	} 
	else if(typeof tempid!='undefined'&& tempid=="supplierCall"){
		$('#supplierAccount').val(combinationTree);
		$('#supplierCombinationId').val(combinationTreeId);   
	} 
	else if(typeof tempid!='undefined'&& tempid=="customerCall"){
		$('#customerAccount').val(combinationTree);
		$('#customerCombinationId').val(combinationTreeId);   
	} 
}
function readURL(input) {
	fileUpload =input.files[0];
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        	//alert(e.target.result);
            $('#preview')
                .attr('src', e.target.result)
                .width(125)
                .height(125);
        };

        reader.readAsDataURL(input.files[0]);
    }
	var form_data = new FormData();
	form_data.append("fileUpload",fileUpload);
    j.ajax({
      type: "POST",
      url:"<%=request.getContextPath()%>/save_profile_picture.action",
      data: form_data,
      processData: false,  // tell jQuery not to process the data
      contentType: false   // tell jQuery not to set contentType
    });
}
</script>
<!-- <script>
function customRange(dates) {
    if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
 </script> -->
 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="person.label.header"/></div>
		<div class="tempresult" style="display:none;"></div>	 
		<div class="portlet-content">
		<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
		  <form id="personal_details" class="" name="PersonalDetails" method="post">
		  	
		  	<div id="hrm" class="width35 float-left">
				<fieldset style="height: 200px;">
					<legend><fmt:message key="person.label.name"/></legend>
					<div><label class="width30"><fmt:message key="person.label.first"/><span style="color:red">*</span></label><input name="firstName" type="text"  tabindex="1" value="${bean.firstName}" id="fname" class="width60 validate[required,custom[onlyLetter]]" maxlength="150" readonly="readonly"></div>
					<div style="position: relative;top:3px;"><label class="width30"><fmt:message key="person.label.firstarabic"/></label><input name="firstNameArabic" type="text"  tabindex="2" value="${bean.firstNameArabic}" id="fnameArabic" class="width60" maxlength="150" readonly="readonly"></div>
					
					<div style="position: relative;top:5px;"><label class="width30"><fmt:message key="person.label.middle"/></label><input name="middleName" tabindex="3" id="mname"  value="${bean.middleName}" class="width60 validate[optional,custom[onlyLetter]]" type="text" maxlength="60" readonly="readonly"></div>
					<div style="position: relative;top:3px;"><label class="width30"><fmt:message key="person.label.middlearabic"/></label><input name="middleNameArabic" tabindex="4" id="mnameArabic"  value="${bean.middleNameArabic}" class="width60" type="text" maxlength="60" readonly="readonly"></div>
					
					<div style="position: relative;top:5px;"><label class="width30"><fmt:message key="person.label.last"/><span style="color:red">*</span></label><input  name="lastName" type="text"  tabindex="5" value="${bean.lastName}" id="lname" class="width60 validate[required,custom[onlyLetter]]" maxlength="150" readonly="readonly"></div>
					<div style="position: relative;top:3px;"><label class="width30"><fmt:message key="person.label.lastarabic"/></label><input  name="lastNameArabic" type="text"  tabindex="6" value="${bean.lastNameArabic}" id="lnameArabic" class="width60" maxlength="150" readonly="readonly"></div>
				
				</fieldset>
			 </div>
		  	 <div id="hrm" class="width35 float-left">
			 	<fieldset style="height: 200px;">
					<div><label class="width30"><fmt:message key="person.label.prefix"/></label><input name="prefix"  type="text" tabindex="7" value="${bean.prefix}" class="width60" maxlength="30"></div>
					<div><label class="width30"><fmt:message key="person.label.suffix"/></label><input name="suffix" type="text" tabindex="8" value="${bean.suffix}" class="width60" maxlength="30"></div>	
                     <label class="width30"><fmt:message key="person.label.persontypes"/><span style="color:red">*</span></label>
                      <input name="personTypes" type="hidden" class="width40 float-right" id="personTypeTxt" value="${bean.personTypes}" readonly="readonly">
                      <input  name="personId" id="personId" type="hidden" value="${bean.personId}" class="width40 float-right">
                      <input  name="identification" id="identification" type="hidden" value="${bean.identification}" class="width40 float-right">
                      <div>
                      <input type="hidden" name="personTypeTemp" id="personTypeTemp" value="${bean.personTypes}" />
                      <input type="hidden" name="personTypeIdTemp" id="personTypeIdTemp" value="${bean.personTypeIds}" />
                           <select  tabindex="9" name="personType" multiple class="width60 validate[required]" id="personType" disabled="disabled">
								<c:if test="${requestScope.PERSONTYPELIST!=null}">
									<c:forEach items="${requestScope.PERSONTYPELIST}" var="nltlst">
										<option value="${nltlst.dataId}">${nltlst.displayName} </option>
									</c:forEach>
								</c:if>	
                           </select> 
                      </div> 
                      <div style="display: none;" id="employeement-account-div">
                      	<label class="width30"><fmt:message key="hr.person.accountPart"/></label>
                      	<c:choose>
	                      	<c:when test="${EMPLOYEE_ACCOUNT.accountByAnalysisAccountId ne null && EMPLOYEE_ACCOUNT.accountByAnalysisAccountId ne ''}">
		                      	<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="employeeAccount" id="employeeAccount" 
		                      	value="${EMPLOYEE_ACCOUNT.accountByAnalysisAccountId.account} [${EMPLOYEE_ACCOUNT.accountByCompanyAccountId.code}
		                      	.${EMPLOYEE_ACCOUNT.accountByCostcenterAccountId.code}
		                      	.${EMPLOYEE_ACCOUNT.accountByNaturalAccountId.code}
		                      	.${EMPLOYEE_ACCOUNT.accountByAnalysisAccountId.code}]"/>
	                      	</c:when>
	                      	<c:otherwise>
	                      		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="employeeAccount" id="employeeAccount" value=""/>
	                      	</c:otherwise>
			             </c:choose>
		                
						<input type="hidden" name="employeeCombinationId" id="employeeCombinationId" value="${EMPLOYEE_ACCOUNT.combinationId}"/> 
                      </div>
                      <div style="position: relative;top:3px;">
                    	<label class="width30"><fmt:message key="hr.person.status"/></label>
						<input  type="hidden" name="registerDisabledTemp" id="registerDisabledTemp" value="${bean.personFlag}"/>
						<select tabindex="10" name="registerDisabled" id="registerDisabled" class="width60"  disabled="disabled">
							<option value="" > - Select - </option>
							<option value="1" selected="selected">Active</option>
							<option value="2">Inactive</option>
						</select>
					</div>
					<div>
						<label class="width30"><fmt:message key="hr.person.effectivedate"/><span style="color:red">*</span></label>
						<input name="effectiveDatesFrom" type="text" tabindex="11" value="${bean.effectiveDatesFrom}" class="width60 startPicker validate[required] from " id="effectiveFromDate" readonly="readonly"  disabled="disabled"/>
					</div>
					<div style="display:none">
						<label class="width30">Closed Date</label>
						<input type="text" name="effectiveDatesTo" readonly="readonly"   style="margin-top: 2px;" value="${bean.effectiveDatesTo}" class="width33 endPicker to"/>
					</div>
              	</fieldset>
			</div>
			<div class="width25 float-left" id="hrm">
	 			<fieldset style="height: 200px;">
	 				<span class="fileinput-button">
						<input type="file" name="fileUpload" id="photoimg" onchange="readURL(this);"/>
					</span>
				 	 <span class="fileinput-preview">
				 	 	<c:choose>
				 	 		<c:when test="${bean.profilePic ne null}">
				 	 			<img name="preview" id="preview" height="125" width="125" src="data:image/png;base64,${bean.profilePic}">
				 	 		</c:when>
				 	 		<c:otherwise>
				 	 			<img alt="No Image" name="preview" id="preview" height="125" width="125" src="./images/No_Profile_Picture.jpg">
				 	 		</c:otherwise>
				 	 	</c:choose>
				 	 </span>
					
				</fieldset>
			</div>
			 <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
					
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
						<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1"><fmt:message key="person.tab.personal"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="identitytab"><a href="#tabs-2"><fmt:message key="person.tab.identity"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="dependenttab"><a href="#tabs-3"><fmt:message key="person.tab.dependent"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="ownertab" style="display: none;"><a href="#tabs-4"><fmt:message key="person.tab.trade"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="tenanttab" style="display: none;"><a href="#tabs-5"><fmt:message key="person.tab.tenant"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="suppliertab" style="display: none;"><a href="#tabs-6"><fmt:message key="person.tab.supplier"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="customertab" style="display: none;"><a href="#tabs-7"><fmt:message key="person.tab.customer"/></a></li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected" id="documenttab"><a href="#tabs-8">Files / Documents</a></li>
					</ul>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1">
						<div id="hrm" class="width50 float-left"> 
							<div><label class="width30"><fmt:message key="hr.person.residentialaddress"/><span style="color:red">*</span></label><textarea name="residentialAddress" tabindex="11" id="residentialAddress" class="width60 validate[required]">${bean.residentialAddress}</textarea></div>
							<div><label class="width30"><fmt:message key="hr.person.permenantaddress"/></label><textarea name="permanentAddress" tabindex="12" id="permanentAddress" class="width60">${bean.permanentAddress}</textarea></div>
							<div><label class="width30"><fmt:message key="hr.person.mobile"/><span style="color:red">*</span></label><input name="mobile" type="text" tabindex="11" value="${bean.mobile}" id="mobile" class="width60 validate[required]]"></div>
							<div><label class="width30"><fmt:message key="hr.person.alternatemobile"/></label><input name="alternateMobile" type="text" tabindex="13" value="${bean.alternateMobile}" id="alternateMobile" class="width60 validate[optional]"></div>
							<div><label class="width30"><fmt:message key="hr.common.phone"/></label><input name="telephone" type="text" tabindex="14" value="${bean.telephone}" id="telephone" class="width60 validate[optional]"></div>
							<div><label class="width30"><fmt:message key="hr.common.fax"/></label><input name="alternateTelephone" type="text" tabindex="15" value="${bean.alternateTelephone}" id="alternateTelephone" class="width60 validate[optional]"></div>
							<div><label class="width30"><fmt:message key="hr.common.pobox"/><span style="color:red">*</span></label><input name="postBoxNumber" type="text" tabindex="16" value="${bean.postBoxNumber}" id="postBoxNumber" class="width60 validate[required]"></div>
							<div><label class="width30"><fmt:message key="hr.common.email"/></label><input name="email" type="text" tabindex="17" value="${bean.email}" id="email" class="width60"></div>
							
						</div>	
						<div id="hrm" class="width50 float-left">
						 	<div><label class="width30"><fmt:message key="person.label.birthdate"/><span style="color:red">*</span></label><input name="birthDate" type="text" tabindex="18" value="${bean.birthDate}" id="birthdate" class="width60 startPicker from validate[required]" readonly="readonly"></div>
						 	<div><label class="width30"><fmt:message key="person.label.age"/></label><input value=""  class="width60 validate[optional,custom[onlyNumber],leastlength[2,2]] text-input" type="text" name="age"  id="age" readonly="readonly"/></div>
							<div><label class="width30"><fmt:message key="person.label.townofbirth"/></label><input name="townBirth" type="text" tabindex="19" id="town_birth" value="${bean.townBirth}" class="width60 validate[optional,custom[onlyLetter]]" maxlength="90"></div>
							<div style="display: none;"><label class="width30"><fmt:message key="person.label.regionofbirth"/></label><input name="regionBirth" type="text" tabindex="20" id="region_birth" value="${bean.regionBirth}" class="width60 validate[optional,custom[onlyLetter]] " maxlength="90"></div>
							<div><label class="width30"><fmt:message key="person.label.countryofbirth"/><span style="color:red">*</span></label>
							<input type="hidden" name="countryBirthTemp" id="countryBirthTemp" value="${bean.countryBirth}"/>
								<select tabindex="21" class="width60 validate[required]" name="countryBirth" id="country_birth" >
									<option value=""> - Select - </option>
										<c:if test="${requestScope.COUNTRYLIST!=null}">
										<c:forEach items="${requestScope.COUNTRYLIST}" var="nltlst">
											<option value="${nltlst.countryId}">${nltlst.countryName} </option>
										</c:forEach>
										</c:if>	
								</select>
							</div>
							<div><label class="width30"><fmt:message key="person.label.gender"/><span style="color:red">*</span></label>
						 	<input type="hidden" name="genderTemp" id="genderTemp" value="${bean.gender}"/>
						 		<select tabindex="22" name="gender" id="gender" class="width60 validate[required]">
						 			<option value=""> - Select - </option>
						 			<option value="M">Male</option>
						 			<option value="F">Female</option>
						 		</select>
						 	</div>
							<div><label class="width30"><fmt:message key="person.label.maritalstatus"/></label>
								<input type="hidden"  name="maritalTemp" id="maritalTemp" value="${bean.maritalStatus}"/>
									<select tabindex="23" name="maritalStatus" id="maritalStatus" class="width60" >
										<option value=""> - Select - </option>
										<option value="M">Married</option>
										<option value="S">Single</option>
										<option value="W">Widow</option>
										<option value="P">Separated</option>
									</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="hr.personaldetails.label.nationalitytype"/><span style="color:red">*</span></label>
								<input type="hidden" name="nationalityTypeIdTemp" id="nationalityTypeIdTemp" value="${bean.personGroup}"/>
								<select tabindex="24" name="personGroup" id="personGroup" class="width60 validate[required]">
									<option value=""> - Select - </option>
									<option value="L">Local</option>
									<option value="A">Arab</option>
									<option value="G">Gulf</option>
									<option value="E">Expatriate</option>
								</select>
							</div>
							<div><label class="width30"><fmt:message key="person.label.nationality"/><span style="color:red">*</span></label>
							<input type="hidden" name="nationalityTemp" id="nationalityTemp" value="${bean.nationality}"/>
								<select tabindex="25" id="nationality" class="width60 validate[required]" name="nationality">
									<option value=""> - Select - </option>
										<c:if test="${requestScope.COUNTRYLIST!=null}">
										<c:forEach items="${requestScope.COUNTRYLIST}" var="nltlst">
											<option value="${nltlst.countryId}">${nltlst.countryName} </option>
										</c:forEach>
										</c:if>	
								</select>
							</div>
							
						 </div>
						
					</div>
						
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2">
					<div id="hrm" class="hastable width100"  >
					<input type="hidden" id="identitycount" value="${fn:length(IDENTITY_LIST)}"/>
						<table id="identityhastab" class="width100"> 
								<thead>
									<tr>   
										<th class="width10"><fmt:message key="hr.person.identitytype"/></th>
										<th class="width10"><fmt:message key="hr.person.identitynumber"/></th>
										<th class="width10"><fmt:message key="hr.person.expiredate"/></th>
										<th class="width10"><fmt:message key="hr.person.issuedplace"/></th>
										<th class="width20"><fmt:message key="common.description"/></th>
										<th style="width:5%;"><fmt:message key="common.options"/></th>
									</tr>
								</thead>
								<tbody class="identitytab">
								 <c:forEach items="${requestScope.IDENTITY_LIST}" var="result" varStatus="status">                         
							        <tr id="identityfieldrow_${status.index+1}" class="identityrowid">
							    		<td class="width10" id="identityType_${status.index+1}">${result.identityTypeName}</td>
							            <td class="width10" id="identityNumber_${status.index+1}">${result.identityNumber}</td>
							            <td class="width10" id="identityExpireDate_${status.index+1}">${result.expireDate}</br>
							            [<span style="font-weight: bold;color: blue;">${result.daysLeft}</span>] day's left</td>
							           	<td class="width10" id="identityIssuedPlace_${status.index+1}">${result.issuedPlace}</td>
							           <td class="width20" id="identityDescription_${status.index+1}">${result.description}</td>
							             <td style="width:5%;" id="identityoption_${status.index+1}"> 
							             			<input type="hidden" name="identityTypeId_${status.index+1}" id="identityTypeId_${status.index+1}"  value="${result.identityType}"/>
							                		<input type="hidden" name="identityId_${status.index+1}" id="identityId_${status.index+1}"  value="${result.identityId}"/>  
							                		<input type="hidden" name="lineId_${status.index+1}" id="lineId_${status.index+1}" value="${status.index+1}"/>  
						                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddData" id="identityAddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												 	 <span class="ui-icon ui-icon-plus"></span>
												   </a>
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityeditData" id="identityEditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												   </a> 
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrow" id="identityDeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												   </a>
												    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="identityWorkingImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
												   </a>
							                  
										</td> 
							        </tr> 
							    </c:forEach>
							    <c:forEach var="i" begin="${fn:length(IDENTITY_LIST)+1}" end="${fn:length(IDENTITY_LIST)+1}" step="1" varStatus ="status">  
									<tr id="identityfieldrow_${i}" class="identityrowid"> 
										<td class="width10" id="identityType_${i}"></td>
										<td class="width10" id="identityNumber_${i}"></td>
							            <td class="width10" id="identityExpireDate_${i}"></td>
							           	<td class="width10" id="identityIssuedPlace_${i}"></td>
							           	<td class="width20" id="identityDescription_${i}"></td>
								 		<td  style="width:5%;" id="identityoption_${i}">
								 			<input type="hidden" name="identityTypeId_${i}" id="identityTypeId_${i}"  value="0"/>
								 			<input type="hidden" name="identityId_${i}" id="identityId_${i}"  value="0"/>  
								 			<input type="hidden" name="identitylineId_${i}" id="identitylineId_${i}" value="${i}"/>  
										  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddData" id="identityAddImage_${i}"  title="Add Record">
										 	 <span class="ui-icon ui-icon-plus"></span>
										   </a>	
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityeditData"  id="identityEditImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
											</a> 
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrow"  id="identityDeleteImage_${i}" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="identityWorkingImage_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td> 
										
									</tr>
							 	</c:forEach>
							 </tbody>
						   </table>
						   <div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
								<div class="portlet-header ui-widget-header float-left identityaddrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
							</div>
					</div>
				</div>	
					
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3">
					<div id="hrm" class="hastable width100"  >
					
						<table id="hastab" class="width100"> 
								<thead>
									<tr>   
										<th class="width10"><fmt:message key="hr.person.name"/></th>
										<th class="width10"><fmt:message key="hr.person.relationship"/></th>
										<th class="width10"><fmt:message key="hr.common.address"/></th>
										<th class="width10"><fmt:message key="hr.person.mobile"/></th>
										<th class="width10"><fmt:message key="hr.common.phone"/></th>
										<th class="width10"><fmt:message key="hr.common.email"/></th>
										<th class="width10"><fmt:message key="hr.person.emergency"/></th>
										<th class="width10"><fmt:message key="hr.person.dependent"/></th>
										<th class="width10"><fmt:message key="hr.person.benifit"/></th>
										<th style="width:5%;"><fmt:message key="common.options"/></th>
									</tr>
								</thead>
								<tbody class="tab">
								 <c:forEach items="${requestScope.DEPENDENT_LIST}" var="result" varStatus="status1">                         
							        <tr id="fieldrow_${status1.index+1}" class="rowid">
							    		<td class="width10" id="dependentName_${status1.index+1}">${result.dependentName}</td>
							            <td class="width10" id="dependentRelationship_${status1.index+1}">${result.relationship}</td>
							            <td class="width10" id="dependentAddress_${status1.index+1}">${result.dependentAddress}</td>
							           	<td class="width10" id="dependentMobile_${status1.index+1}">${result.dependentMobile}</td>
							           	<td class="width10" id="dependentPhone_${status1.index+1}">${result.dependentPhone}</td>
							           	<td class="width10" id="dependentEmail_${status1.index+1}">${result.dependentMail}</td>
							           	<td class="width10" id="emergency_${status1.index+1}">
							           		<c:choose><c:when test="${result.emergency eq true}">YES</c:when><c:otherwise>NO</c:otherwise></c:choose>
							           	</td>
							          	<td class="width10" id="dependent_${status1.index+1}">
							      			<c:choose><c:when test="${result.dependentflag eq true}">YES</c:when><c:otherwise>NO</c:otherwise></c:choose>
							          	</td>
							          	<td class="width10" id="benefit_${status1.index+1}">
							          		<c:choose><c:when test="${result.benefit eq true}">YES</c:when><c:otherwise>NO</c:otherwise></c:choose>
							          	</td>
							             <td style="width:5%;" id="option_${status1.index+1}"> 
							             			<input type="hidden" name="dependentDescription_${status1.index+1}" id="dependentDescription_${status1.index+1}"  value="${result.dependentDescription}"/>
							             			<input type="hidden" name="dependentRelationshipId_${status1.index+1}" id="dependentRelationshipId_${status1.index+1}"  value="${result.relationshipId}"/>
							                		<input type="hidden" name="dependentId_${status1.index+1}" id="dependentId_${status1.index+1}"  value="${result.dependentId}"/>  
							                		<input type="hidden" name="lineId_${status1.index+1}" id="lineId_${status1.index+1}" value="${status1.index+1}"/>  
						                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												 	 <span class="ui-icon ui-icon-plus"></span>
												   </a>
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status1.index+1}" style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												   </a> 
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												   </a>
												    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="WorkingImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
												   </a>
							                  
										</td> 
							        </tr> 
							    </c:forEach>
							    <c:forEach var="i" begin="${fn:length(DEPENDENT_LIST)+1}" end="${fn:length(DEPENDENT_LIST)+1}" step="1" varStatus ="status">  
									<tr id="fieldrow_${i}" class="rowid"> 
										<td class="width10" id="dependentName_${i}"></td>
							            <td class="width10" id="dependentRelationship_${i}"></td>
							            <td class="width10" id="dependentAddress_${i}"></td>
							           	<td class="width10" id="dependentMobile_${i}"></td>
							           	<td class="width10" id="dependentPhone_${i}"></td>
							           	<td class="width10" id="dependentEmail_${i}"></td>
							           	<td class="width10" id="emergency_${i}"></td>
							          	<td class="width10" id="dependent_${i}"></td>
							          	<td class="width10" id="benefit_${i}"></td>
								 		<td  style="width:5%;" id="option_${i}">
								 			<input type="hidden" name="dependentDescription_${i}" id="dependentDescription_${i}"  value=""/>
								 			<input type="hidden" name="dependentRelationshipId_${i}" id="dependentRelationshipId_${i}"  value="0"/>
								 			<input type="hidden" name="dependentId_${i}" id="dependentId_${i}"  value="0"/>  
								 			<input type="hidden" name="lineId_${i}" id="lineId_${i}" value="${i}"/>  
										  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}"  title="Add Record">
										 	 <span class="ui-icon ui-icon-plus"></span>
										   </a>	
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  id="EditImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
											</a> 
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  id="DeleteImage_${i}" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td> 
										
									</tr>
							 	</c:forEach>
							 </tbody>
						   </table>
						   <div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
								<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
							</div>
					</div>
				</div>	
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4">
					<div id="hrm" class="float-left width50"  >
						<div>
							<label class="width30"><fmt:message key="hr.person.tradelicense"/></label>
							<input  class="width60" type="text" name="tradeLicenseNumber"  id="tradeLicenseNumber" value="${bean.tradeLicenseNumber}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="hr.person.issuedplace"/></label>
							<input  class="width60" type="text" name="tradeIssuedPlace"  id="tradeIssuedPlace"  value="${bean.tradeIssuedPlace}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="hr.person.expiredate"/></label>
							<input   class="width60" type="text" name="tradeExpireDate"  id="tradeExpireDate" value="${bean.tradeExpireDate}"/>
						</div>
					</div>
					<div id="hrm" class="float-left width50"  >
						<div>
							<label class="width30"><fmt:message key="common.description"/></label>
							<textarea class="width60" name="tradeDescription"  id="tradeDescription">${bean.tradeDescription}</textarea>
						</div>
					</div>
				</div>
				
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-5">
					<div id="hrm" class="float-left width100"  >
						<div class="float-left width100">
	                      	<label class="width20"><fmt:message key="hr.person.tenantAccount"/></label>
	                      	<c:choose>
		                      	<c:when test="${TENANT_ACCOUNT.accountByAnalysisAccountId ne null && TENANT_ACCOUNT.accountByAnalysisAccountId ne ''}">
			                      	<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="tenantAccount" id="tenantAccount" 
			                      	value="${TENANT_ACCOUNT.accountByAnalysisAccountId.account} [${TENANT_ACCOUNT.accountByCompanyAccountId.code}
			                      	.${TENANT_ACCOUNT.accountByCostcenterAccountId.code}
			                      	.${TENANT_ACCOUNT.accountByNaturalAccountId.code}
			                      	.${TENANT_ACCOUNT.accountByAnalysisAccountId.code}]"/>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="tenantAccount" id="tenantAccount" value=""/>
		                      	</c:otherwise>
			                 </c:choose>
			                 <c:if test="${TENANT_ACCOUNT.accountByAnalysisAccountId eq null || TENANT_ACCOUNT.accountByAnalysisAccountId eq	''}">
		                      	<span class="button" style="position: relative;top:6px; ">
									<a style="cursor: pointer;" id="tenantCall" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</c:if>
							<input type="hidden" name="tenantCombinationId" id="tenantCombinationId" value="${TENANT_ACCOUNT.combinationId}"/> 
                      	</div>
                      	<div class="width100 float-left" id="tenant-result-div"></div>
					</div>
				</div>
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-6">
					<div id="hrm" class="float-left width100"  >
						<div class="float-left width100">
	                      	<label class="width20"><fmt:message key="hr.person.supplierAccount"/></label>
	                      	<c:choose>
		                      	<c:when test="${SUPPLIER_ACCOUNT.accountByAnalysisAccountId ne null && SUPPLIER_ACCOUNT.accountByAnalysisAccountId ne ''}">
			                      	<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="supplierAccount" id="supplierAccount" 
			                      	value="${SUPPLIER_ACCOUNT.accountByAnalysisAccountId.account} [${SUPPLIER_ACCOUNT.accountByCompanyAccountId.code}
			                      	.${SUPPLIER_ACCOUNT.accountByCostcenterAccountId.code}
			                      	.${SUPPLIER_ACCOUNT.accountByNaturalAccountId.code}
			                      	.${SUPPLIER_ACCOUNT.accountByAnalysisAccountId.code}]"/>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="supplierAccount" id="supplierAccount" value=""/>
		                      	</c:otherwise>
			                 </c:choose>
			                 <c:if test="${SUPPLIER_ACCOUNT.accountByAnalysisAccountId eq null || SUPPLIER_ACCOUNT.accountByAnalysisAccountId eq	''}">
		                      	<span class="button" style="position: relative;top:6px; ">
									<a style="cursor: pointer;" id="supplierCall" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</c:if>
							<input type="hidden" name="supplierCombinationId" id="supplierCombinationId" value="${SUPPLIER_ACCOUNT.combinationId}"/> 
                      	</div>
                      	<div class="width100 float-left" id="supplier-result-div"></div>
					</div>
				</div>
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-7">
					<div id="hrm" class="float-left width100" >
						<div class="float-left width100">
	                      	<label class="width20"><fmt:message key="hr.person.customerAccount"/></label>
	                      	<c:choose>
		                      	<c:when test="${CUSTOMER_ACCOUNT.accountByAnalysisAccountId ne null && CUSTOMER_ACCOUNT.accountByAnalysisAccountId ne ''}">
			                      	<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="customerAccount" id="customerAccount" 
			                      	value="${CUSTOMER_ACCOUNT.accountByAnalysisAccountId.account} [${CUSTOMER_ACCOUNT.accountByCompanyAccountId.code}
			                      	.${CUSTOMER_ACCOUNT.accountByCostcenterAccountId.code}
			                      	.${CUSTOMER_ACCOUNT.accountByNaturalAccountId.code}
			                      	.${CUSTOMER_ACCOUNT.accountByAnalysisAccountId.code}]"/>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<input type="text" class="masterTooltip width50" title="" readonly="readonly" name="customerAccount" id="customerAccount" value=""/>
		                      	</c:otherwise>
			                 </c:choose>
			                 <c:if test="${CUSTOMER_ACCOUNT.accountByAnalysisAccountId eq null || CUSTOMER_ACCOUNT.accountByAnalysisAccountId eq	''}">
		                      	<span class="button" style="position: relative;top:6px; ">
									<a style="cursor: pointer;" id="customerCall" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</c:if>
							<input type="hidden" name="customerCombinationId" id="customerCombinationId" value="${CUSTOMER_ACCOUNT.combinationId}"/> 
                      	</div>
                      	<div class="width100 float-left" id="customer-result-div"></div>
					</div>
				</div>
				<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-8">
					<div id="UploadDmsDiv" class="width100">
						
						<div>									
							
							<div id="person_image_information" style="cursor: pointer; color: blue;">
								<u><fmt:message key="hr.person.imageupload"/></u>
							</div>
							<div style="padding-top: 10px; margin-top:3px; ">
								<span id="personImages"></span>
							</div>
							
						</div>	
						<div>									
							
							<div id="person_document_information" style="cursor: pointer; color: blue;">
								<u><fmt:message key="hr.person.documentupload"/></u>
							</div>
							<div style="padding-top: 10px; margin-top:3px;">
								<span id="personDocs"></span>
							</div>
							
						</div>						
					</div>
				</div>
			</div>
		</form>
	</div>
		<div class="clearfix"></div>
		<input type="hidden" id="supplierinfo"/>
		<input type="hidden" id="customerinfo"/>
		
		<div class="float-right buttons ui-widget-content ui-corner-all">
			<div class="portlet-header ui-widget-header float-right discard-personalDetails" id="close">Close</div>
		</div>
	</div>
</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 </div> 
<input type="hidden" id="tempDays"/>
<input type="hidden" id="tempMon"/>


	

