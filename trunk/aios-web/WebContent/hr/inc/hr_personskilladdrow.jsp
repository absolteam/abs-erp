<tr id="skillfieldrow_${requestScope.id}" class="skillrowid">
	<td class="width10" id="skillType_${requestScope.id}"></td>
	<td class="width10" id="skillLevel_${requestScope.id}"></td>
	<td class="width10" id="skillDescription_${requestScope.id}"></td>

	<td style="width: 5%;" id="option_${requestScope.id}"><input
		type="hidden" name="skillId_${requestScope.id}"
		id="skillId_${requestScope.id}" value="" /> <input type="hidden"
		name="skillTypeId_${requestScope.id}" id="skillTypeId_${requestScope.id}" /> <input
		type="hidden" name="skilllineId_${requestScope.id}"
		id="skilllineId_${requestScope.id}" value="" /> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip skilladdDatanew"
		id="skillAddImage_${requestScope.id}" style="cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip skilleditDatanew"
		id="skillEditImage_${requestScope.id}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip skilldelrownew"
		id="skillDeleteImage_${requestScope.id}" style="cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="skillWorkingImage_${requestScope.id}"
		style="display: none; cursor: pointer;" title="Working"> <span
			class="processing"></span> </a>
	</td>
</tr>
<script type="text/javascript">
	$(".skilladdDatanew").click(function() {
		slidetab = $(this).parent().parent().get(0);
		//Find the Id for fetch the value from Id
		var tempvar = $(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid = Number(idarray[1]);
		if ($("#editCalendarVal").validationEngine({
			returnIsValid : true
		})) {
			$("#skillAddImage_" + rowid).hide();
			$("#skillEditImage_" + rowid).hide();
			$("#skillDeleteImage_" + rowid).hide();
			$("#skillWorkingImage_" + rowid).show();

			$.ajax({
				type : "POST",
				url : "
<%=request.getContextPath()%>/person_skill_add.action",
				data:{id:rowid,addEditFlag:"A"},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $('#openFlag').val(1);
				}
		});
		
	}
	else{
		return false;
	}	
	 
});

$(".skilleditDatanew").click(function(){
	 
	 slidetab=$(this).parent().parent().get(0);  
	 
	 if($("#editCalendarVal").validationEngine({returnIsValid:true})) {
		 
			 
			//Find the Id for fetch the value from Id
				var tempvar=$(this).attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				$("#skillAddImage_"+rowid).hide();
				$("#skillEditImage_"+rowid).hide();
				$("#skillDeleteImage_"+rowid).hide();
				$("#skillWorkingImage_"+rowid).show(); 
		 $.ajax({
			 type : "POST",
			 url:"<%=request.getContextPath()%>/person_skill_add.action",
			 data:{id:rowid,addEditFlag:"E"},
			 async: false,
		  dataType: "html",
			 cache: false,
		   success:function(result){
			
		   $(result).insertAfter(slidetab);

		 //Bussiness parameter
		 
		 		 $('#skillsId').val($('#skillId_'+rowid).val()); 
        			$('#skillType').val($('#skillTypeId_'+rowid).val()); 
        			$('#skillLevel').val($('#skillLevel_'+rowid).text()); 
        			$('#skillDescription').val($('#skillDescription_'+rowid).text()); 
        		
		
		    $('#openFlag').val(1);
		  
		},
		error:function(result){
		//	alert("wee"+result);
		}
		
		});
		 			
	 }
	 else{
		 return false;
	 }	
	 
});
 
 $(".skilldelrownew").click(function(){ 
	 slidetab=$(this).parent().parent().get(0); 

	//Find the Id for fetch the value from Id
		var tempvar=$(this).attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
	 var lineId=$('#skilllineId_'+rowid).val();
	
		 $.ajax({
			 type : "POST",
			 url:"<%=request.getContextPath()%>/person_skill_save.action", 
			 data:{id:lineId,addEditFlag:"D"},
			 async: false,
		  dataType: "html",
			 cache: false,
		   success:function(result){ 
				 $('.tempresult').html(result);   
				 if(result!=null){
					 $("#skillfieldrow_"+rowid).remove(); 
					  var skillcount=Number($('#skillcount').val()); 
     				  skillcount-=1;
     				  $('#skillcount').val(skillcount);
		        		//To reset sequence 
		        		var i=0;
		     			$('.skillrowid').each(function(){  
							i=i+1;
							$($($(this).children().get(4)).children().get(1)).val(i); 
	   					 }); 
				     }  
			},
			error:function(result){
				 $('.tempresult').html(result);   
				 return false;
			}
				
		 });
		 
	 
 });
</script>