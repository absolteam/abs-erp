<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tr class="rowidS" id="fieldrowS_${rowid}">
	<td id="lineIdS_${rowid}" style="display:none;">${rowid}</td>
	<td>
		<span  id="jobName_${rowid}" class="float-left width30">
		</span>
		<span class="button float-right" id="job_${rowid}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="jobpopup_${rowid}" class="btn ui-state-default ui-corner-all width40 jobs-popup"> 
				<span class="ui-icon ui-icon-newwin">
				</span> 
			</a>
		</span> 
		<input type="hidden" name="jobId" id="jobId_${rowid}" value="" class="width50"/>
		
	</td> 
	<td>
		<span  id="employees_${rowid}">
		</span>
		<span class="button float-right" id="job_${rowid}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="jobpopupassign_${rowid}" class="btn ui-state-default ui-corner-all width40 jobs-assignment-popup"> 
				<span class="ui-icon ui-icon-newwin">
				</span> 
			</a>
		</span> 
		<input type="hidden" name="jobAssignmentId" id="jobAssignmentId_${rowid}" value="" class="width50"/>
	</td> 
	<td>
		<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${rowid}" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${rowid}" style="border:0px;"/>
	</td> 
	<td style="display:none;">
		<input type="hidden" id="jobShiftId_${rowid}" style="border:0px;"/>
	</td>
	 <td style="width:0.01%;" class="opn_td" id="optionS_${rowid}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${rowid}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${rowid}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${rowid}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${rowid}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>    
</tr>
<script type="text/javascript"> 
$(document).ready(function(){  
	 $('.startDate,.endDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
});
</script>