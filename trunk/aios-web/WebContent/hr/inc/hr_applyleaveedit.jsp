<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>

<script type="text/javascript">
$(document).ready(function(){  
	var leaveProcessId=Number($('#leaveProcessId').val());
	populateUploadsPane("doc","leaveDocs","LeaveProcess",leaveProcessId);
	
	$('#halfDay').change(function(){
   	 dateOnchangeCall();
    });
	dateOnchangeCall();
});
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		dateOnchangeCall();
		
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
		dateOnchangeCall();
	} 
}
function dateOnchangeCall(){
	if($('#toDate').val()!=null && $('#toDate').val()!="" && $('#fromDate').val()!=null && $('#fromDate').val()!=""){ 
		daysDiff();
		if($('#tempDays').val()=="NaN" && " " && ""){
			$('#tempDays').val("0");
		}
		if($('#halfDay').attr('checked'))
			$('#tempDays').val(Number($('#tempDays').val())+0.5);
		else
			$('#tempDays').val(Number($('#tempDays').val())+1);	
			
	}
	return true;
}
var fromFormat='dd-MMM-yyyy';
var toFormat='MM/dd/y';
function daysDiff() {
	var tempdate1 = formatDate(new Date(getDateFromFormat($("#fromDate").val(),fromFormat)),toFormat);
	var tempdate2 = formatDate(new Date(getDateFromFormat($("#toDate").val(),fromFormat)),toFormat);
	var date1=new Date(tempdate1);
	var date2=new Date(tempdate2);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	 $('#tempDays').val(diffDays);
}
</script>
<fieldset style="height:170px;">
	<legend>Leave Information</legend>
	<div class="width45 float-left">
		<input type="hidden" name="leaveProcessId" id="leaveProcessId" value="${LEAVE_PROCESS.leaveProcessId}" class="width50"/>
		<div>
			<label class="width30" for="leaveType">Leave Type<span class="mandatory">*</span></label>
			<select id="leaveType" name="leaveType" class="width50 validate[required]">
				<option value="">--Select--</option>
				<c:forEach items="${JOB_LEAVE_LIST}" var="jobleave" varStatus="status">
				
				<c:choose>
					<c:when test="${LEAVE_PROCESS.jobLeave.jobLeaveId eq jobleave.jobLeaveId}">
						<option value="${jobleave.jobLeaveId}" selected="selected">${jobleave.leave.lookupDetail.displayName}</option>
					</c:when>
					<c:otherwise>
						<option value="${jobleave.jobLeaveId}">${jobleave.leave.lookupDetail.displayName}</option>
					</c:otherwise>
				</c:choose>
				</c:forEach>
			</select>
			<input type="hidden" name="leaveTyp" id="leaveTyp" value="${LEAVE_PROCESS.jobLeave.jobLeaveId}" class="width50"/>
			
		</div>   
		<div>
			<label class="width30" for="fromDate">Leave Start Date<span class="mandatory">*</span></label>
			<c:choose>
				<c:when test="${LEAVE_PROCESS.fromDate ne null &&  LEAVE_PROCESS.fromDate  ne ''}">
					<c:set var="fromDate" value="${LEAVE_PROCESS.fromDate}"/>  
					<%String fromDate = DateFormat.convertDateToString(pageContext.getAttribute("fromDate").toString());%>
					<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]" 
					value="<%=fromDate%>"/>
				</c:when>
				<c:otherwise>
					<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]"/>
				</c:otherwise>
			</c:choose> 
		</div>   
		<div>
			<label class="width30" for="toDate">Leave End Date<span class="mandatory">*</span></label>
			<c:choose>
				<c:when test="${LEAVE_PROCESS.toDate ne null &&  LEAVE_PROCESS.toDate ne ''}">
					<c:set var="toDate" value="${LEAVE_PROCESS.toDate}"/>  
					<%String toDate = DateFormat.convertDateToString(pageContext.getAttribute("toDate").toString());%>
					<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="3" class="width50 validate[required]" 
					value="<%=toDate%>"/>
				</c:when>
				<c:otherwise>
					<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="4" class="width50 validate[required]"/>
				</c:otherwise>
			</c:choose> 
		</div> 
		<div>
			<label class="float-left width30" for="leaveType">Half Day?</label>
			<c:choose>
				<c:when test="${LEAVE_PROCESS.halfDay eq true}">
				<input type="checkbox" name="halfDay" id="halfDay" checked="checked" class="float-left width5" >
				</c:when>
				<c:otherwise>
					<input type="checkbox" name="halfDay" id="halfDay" class="float-left width5" >
				</c:otherwise>
			</c:choose>
			<label class="float-left width20" for="leaveType">No. Of Days :</label>
			<input type="text" id="tempDays" class="float-left width10" readonly="readonly"/>
			<input type="hidden" id="tempYears"/><input type="hidden" id="tempMonths"/>
		</div>
	</div>
	<div class="width45 float-left">
		<div>
			<label class="width20" for="reason">Reason<span class="mandatory">*</span></label>
			<textarea name="reason" id="reason" class="width50" >${LEAVE_PROCESS.reason}</textarea>
		</div>
		<div>
			<label class="width20" for="contactOnLeave">Contact On Leave</label>
			<textarea name="contactOnLeave" id="contactOnLeave" class="width50" >${LEAVE_PROCESS.contactOnLeave}</textarea>
		</div>
	</div>
	<div class="width45 float-left">
		 <fieldset>
    	 	<div  style="height: 50px;overflow-y:auto;">
			  	<div class="float-left">
					<span id="leave_document_information" style="cursor: pointer; color: blue;">
						<u><fmt:message key="hr.company.documentuplode"/></u>
					</span>
				</div>
				<div class="width80 float-right">
					<span id="leaveDocs"></span>
				</div>
			</div>
	     </fieldset>
	</div>
	<div class="clearfix"></div>
	<div class="float-right buttons ui-widget-content ui-corner-all">
		<div class="portlet-header ui-widget-header float-right discard" id="discard">Close</div>
		<div class="portlet-header ui-widget-header float-right reset" id="reset">Reset</div>
		<div class="portlet-header ui-widget-header float-right save" id="job_save">Edit</div>
	</div>
</fieldset>