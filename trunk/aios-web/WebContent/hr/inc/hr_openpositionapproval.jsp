<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count{
	padding:0!important;
}
</style> 

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 

		var openPositionId=Number($("#openPositionId").val());
		if(openPositionId>0){
			
			autoSelectTheMultiList("jobTypeTemp","jobType");
			autoSelectTheMultiList("genderTemp","gender");
			autoSelectTheMultiList("requiredTravelTemp","requiredTravel");
			autoSelectTheMultiList("requiredExperienceTemp","requiredExperience");
			autoSelectTheMultiList("interviewModeTemp","interviewMode");
			autoSelectTheMultiList("statusTemp","status");
			autoSelectTheMultiList("ageLimitTemp","ageLimit");
			
			//Select the selection field
			autoSelectTheMultiList("responsibility","responsibilityList");
			autoSelectTheMultiList("qualification","qualificationList");
			autoSelectTheMultiList("recruitmentResources","resourceList");
			autoSelectTheMultiList("skills","skillList");
			
			
			populateUploadsPane("doc","uploadedDocs","OpenPosition",openPositionId);
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","uploadedDocs","OpenPosition",openPositionId,"VacaniesDocuments");
			});
		}else{
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","uploadedDocs","OpenPosition","-1","VacaniesDocuments");
			});
		}
		
		 $('#locationpopup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         var rowId=-1; 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
	               data:{id:rowId},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
		   
	   $('#designationpopup').click(function() { 
	       $('.ui-dialog-titlebar').remove();  
	       $('#common-popup').dialog('open');
	       var rowId=0; 
	          $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/common_designation_list.action",
	             data:{id:rowId},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){
	                  $('.common-result').html(result);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 }); 
	   
	   $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	  
		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});

		$jquery("#position-details").validationEngine('attach');
	
			
		$('#position-discard').click(function(){ 
			returnCallTOList();
			
		 });
		

	 $('#position-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
		 	
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(openPositionId>0)
						successMessage="Successfully Modified";

					try {
						if ($jquery("#position-details").validationEngine('validate')) {
							generateDataToSendToDB("responsibilityList","responsibility");
							generateDataToSendToDB("qualificationList","qualification");
							generateDataToSendToDB("resourceList","recruitmentResources");
							generateDataToSendToDB("skillList","skills");
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/open_position_save.action", 
							 	async: false,
							 	data: $("#position-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					} catch (e) {

					}

				});
	 
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="JOB_TYPE"){
				$('#jobType').html("");
				$('#jobType').append("<option value=''>--Select--</option>");
				loadLookupList("jobType");
				
			}else if(accessCode=="INTERVIEW_MODE"){
				$('#interviewMode').html("");
				$('#interviewMode').append("<option value=''>--Select--</option>");
				loadLookupList("interviewMode");
			}else if(accessCode=="RESPONSIBILITES"){
				$('#responsibilityList').html("");
				loadLookupList("responsibilityList");
				$("#responsibilityList").multiselect( 'destroy');
				 $("#responsibilityList").multiselect();
			}else if(accessCode=="QUALIFICATIONS"){
				$('#qualificationList').html("");
				loadLookupList("qualificationList");
				$("#qualificationList").multiselect( 'destroy');
				$("#qualificationList").multiselect();
			}else if(accessCode=="SKILLS"){
				$('#skillList').html("");
				loadLookupList("skillList");
				$("#skillList").multiselect( 'destroy');
				$("#skillList").multiselect();
			}else if(accessCode=="RECRUITMENT_RESOURCES"){
				$('#resourceList').html("");
				loadLookupList("resourceList");
				$("#resourceList").multiselect( 'destroy');
				$("#resourceList").multiselect();
			}else if(accessCode=="TRAVEL_TYPE"){
				$('#requiredTravel').html("");
				$('#requiredTravel').append("<option value=''>--Select--</option>");
				loadLookupList("requiredTravel");
				
			}else if(accessCode=="EXPERIENCE_PERIOD"){
				$('#requiredExperience').html("");
				$('#requiredExperience').append("<option value=''>--Select--</option>");
				loadLookupList("requiredExperience");
				
			}else if(accessCode=="AGE_LIMIT"){
				$('#ageLimit').html("");
				$('#ageLimit').append("<option value=''>--Select--</option>");
				loadLookupList("ageLimit");
				
			}
			
		});
		
			
		 $('.startDate,.endDate').datepick({
			 onSelect: customRanges,showTrigger: '#calImg'});
		 
		 $("#responsibilityList,#qualificationList,#resourceList,#skillList").multiselect();
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/open_position_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	function departmentBranchPopupResult(locationId, locationName,
			commaseparatedValue) {
	
		$('#cmpDeptLocId').val(locationId);
		var valueArray=new Array();
		valueArray=commaseparatedValue.split('@&');
		$('#companyName').val(valueArray[0]);
		$('#departmentName').val(valueArray[1]);
		$('#locationName').val(valueArray[2]);
		
		findNumberOfExistingPost();
	}
	function designationPopupResult(id, name, commaseparatedValue) {
		$('#designationId').val(id);
		var valueArray=new Array();
		valueArray=commaseparatedValue.split('@&');
		$('#designationName').val(valueArray[0]);
		$('#gradeName').val(valueArray[1]);
		
		findNumberOfExistingPost();
	}
	
	function findNumberOfExistingPost(){
		var designationId=Number($('#designationId').val());
		var cmpDeptLocId=Number($('#cmpDeptLocId').val());
		if(cmpDeptLocId>0 && designationId>0){
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/find_open_position.action", 
					data:{designationId:designationId,cmpDeptLocId:cmpDeptLocId},
				 	async: false,
				    dataType: "json",
				    cache: false,
					success:function(response){ 
						$('#numberOfCount').val(response.openPositionVO.numberOfCount);
					}
			 });
		}
	}
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
	function customRanges(dates) {
		if (this.id == 'startDate') {
			$('#endDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#startDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
	function generateDataToSendToDB(source,target){
		var typeList=new Array();
		var ids="";
		$('#'+source+' option:selected').each(function(){
			typeList.push($(this).val());
		});
		for(var i=0; i<typeList.length;i++){
			if(i==(typeList.length)-1)
				ids+=typeList[i];
			else
				ids+=typeList[i]+",";
		}
		
		$('#'+target).val(ids);
	}
	
	function autoSelectTheMultiList(source,target) {
		 var idsArray=new Array();
		 var idlist=$('#'+source).val();
		 idsArray=idlist.split(',');
		$('#'+target+' option').each(function(){
			var txt=$(this).val().trim();
			if($.inArray(txt, idsArray)>=0)
				$(this).attr("selected","selected");
		});

	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Open
			Position Details
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="position-details" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden" name="openPosition.openPositionId"
									id="openPositionId" value="${OPEN_POSITION.openPositionId}"
									class="width50 " />
								<fieldset style="min-height: 200px;">
									<legend>Job Information</legend>
									<div class="width50 float-left">
										<div>
											<label class="width30">Reference Number<span
												style="color: red">*</span> </label> <input type="text"
												id=positionReferenceNumber
												class="width50 validate[required]" readonly="readonly"
												name="openPosition.positionReferenceNumber"
												value="${OPEN_POSITION.positionReferenceNumber}" />
										</div>
										<div>
											<label class="width30">Designation<span
												style="color: red">*</span>
											</label> <input type="text" readonly="readonly" id="designationName"
												class="masterTooltip width50 validate[required]"
												value="${OPEN_POSITION.designation.designationName}" /> <span
												class="button" id="designation"
												style="position: relative;"> <a
												style="cursor: pointer;" id="designationpopup"
												class="btn ui-state-default ui-corner-all width100"> <span
													class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" name="openPosition.designation.designationId"
												id="designationId"
												value="${OPEN_POSITION.designation.designationId}" />
										</div>
										<div>
											<label class="width30">Grade</label>
											<input type="text" readonly="readonly" id="gradeName"
												class="masterTooltip width50"
												value="${OPEN_POSITION.designation.grade.gradeName}" /> 
										</div>
										<div>
											<label class="width30">Branch<span style="color: red">*</span>
											</label> <input type="text" readonly="readonly" id="locationName"
												class="masterTooltip width50 validate[required]"
												value="${OPEN_POSITION.cmpDeptLocation.location.locationName}" />
											<span class="button" id="location"
												style="position: relative;"> <a
												style="cursor: pointer;" id="locationpopup"
												class="btn ui-state-default ui-corner-all width100"> <span
													class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden"
												name="openPosition.cmpDeptLocation.cmpDeptLocId"
												id="cmpDeptLocId" class="cmpDeptLocId"
												value="${OPEN_POSITION.cmpDeptLocation.cmpDeptLocId}" />
										</div>

										<div>
											<label class="width30">Department</label>
											<input type="text" readonly="readonly" id="departmentName"
												class="masterTooltip width50"
												value="${OPEN_POSITION.cmpDeptLocation.department.departmentName}" /> 
										</div>
										<div>
											<label class="width30">Company</label>
											<input type="text" readonly="readonly" id="companyName"
												class="masterTooltip width50"
												value="${OPEN_POSITION.cmpDeptLocation.company.companyName}" /> 
										</div>
										<div>
											<label class="width30">Job Type<span
												style="color: red">*</span> </label>
											<div>
												<select id="jobType"
													name="openPosition.lookupDetailByJobType.lookupDetailId"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${JOB_TYPES}" var="nltlst">
														<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="JOB_TYPE_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="JOB_TYPE"
													class="btn ui-state-default ui-corner-all recruitment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="jobTypeTemp" id="jobTypeTemp"
													value="${OPEN_POSITION.lookupDetailByJobType.lookupDetailId}" />
											</div>
										</div>
										<div>
											<label class="width30">No.of Position<span
												style="color: red">*</span>
											</label> <input type="text" id="numberOfPosition"
												class="width50 validate[required]" name="openPosition.numberOfPosition"
												value="${OPEN_POSITION.numberOfPosition}" /> 
										</div>
										<div>
											<label class="width30">Opened Date<span
												style="color: red">*</span> </label> <input type="text"
												class="width50 startDate validate[required]" readonly="readonly"
												name="startDate" id="startDate"
												value="${OPEN_POSITION.dateOpenedDisplay}" />
										</div>
										<div>
											<label class="width30">Target Date<span
												style="color: red">*</span> </label> <input type="text"
												class="width50 startDate validate[required]" readonly="readonly" name="endDate"
												id="endDate" value="${OPEN_POSITION.targetDateDisplay}" />
										</div>
									</div>
									<div class="width50 float-left">
										<div>
											<label class="width30">Required Gender<span
												style="color: red">*</span> </label>
											<div>
												<select id=gender
													name="openPosition.gender"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${GENDER_TYPE}" var="gen">
														<option value="${gen.key}">${gen.value}
														</option>
													</c:forEach>
												</select>  <input
													type="hidden" name="genderTemp" id="genderTemp"
													value="${OPEN_POSITION.gender}" />
											</div>
										</div>
										<div>
											<label class="width30">Required Travel<span
												style="color: red">*</span> </label>
											<div>
												<select id=requiredTravel
													name="openPosition.lookupDetailByTravelRequired.lookupDetailId"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${TRAVEL_TYPE}" var="nltlst">
														<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="TRAVEL_TYPE_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="TRAVEL_TYPE"
													class="btn ui-state-default ui-corner-all recruitment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="requiredTravelTemp" id="requiredTravelTemp"
													value="${OPEN_POSITION.lookupDetailByTravelRequired.lookupDetailId}" />
											</div>
										</div>
										<div>
											<label class="width30">Required Experience<span
												style="color: red">*</span> </label>
											<div>
												<select id=requiredExperience
													name="openPosition.lookupDetailByRequiredExperience.lookupDetailId"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${EXPERIENCE_PERIOD}" var="nltlst">
														<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="EXPERIENCE_PERIOD_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="EXPERIENCE_PERIOD"
													class="btn ui-state-default ui-corner-all recruitment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="requiredExperienceTemp" id="requiredExperienceTemp"
													value="${OPEN_POSITION.lookupDetailByRequiredExperience.lookupDetailId}" />
											</div>
										</div>

										<div>
											<label class="width30">Freqency<span
												style="color: red">*</span> </label>
											<div>
												<select id="interviewMode"
													name="openPosition.lookupDetailByInterviewMode.lookupDetailId"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${INTERVIEW_MODES}" var="nltls">
														<option value="${nltls.lookupDetailId}">${nltls.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="INTERVIEW_MODE_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="INTERVIEW_MODE"
													class="btn ui-state-default ui-corner-all recruitment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="interviewModeTemp"
													id="interviewModeTemp"
													value="${OPEN_POSITION.lookupDetailByInterviewMode.lookupDetailId}" />
											</div>
										</div>
										<div>
											<label class="width30">Status<span
												style="color: red">*</span> </label>
											<div>
												<select id=status
													name="openPosition.status"
													class="width50 validate[required]">
													<c:forEach items="${STATUS_TYPE}" var="gen">
														<option value="${gen.key}">${gen.value}
														</option>
													</c:forEach>
												</select>  <input
													type="hidden" name="statusTemp" id="statusTemp"
													value="${OPEN_POSITION.status}" />
											</div>
										</div>
										<div>
											<label class="width30">Age Limit<span
												style="color: red">*</span> </label>
											<div>
												<select id="ageLimit"
													name="openPosition.ageLimit"
													class="width50 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${AGE_LIMIT}" var="nltls">
														<option value="${nltls.lookupDetailId}">${nltls.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="AGE_LIMIT_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="AGE_LIMIT"
													class="btn ui-state-default ui-corner-all recruitment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="ageLimitTemp"
													id="ageLimitTemp"
													value="${OPEN_POSITION.ageLimit}" />
											</div>
										</div>

									
									<%-- 	<div>
											<label class="width30">Description
											</label> <textarea  name="openPosition.description"  id="description"
												class="width50 ">${OPEN_POSITION.description}</textarea> 
										</div> --%>
										<div>
											<label class="width30">Perk & Privilages
											</label> <textarea  name="openPosition.privilages"  id="privilages"
												class="width50 ">${OPEN_POSITION.privilages}</textarea> 
										</div>
										<div>
											<label class="width30">Video Link
											</label> <input type="text" id="videoLink"
												class="width50" name="openPosition.videoLink"
												value="${OPEN_POSITION.videoLink}" /> 
										</div>

										<div>
											<label class="width30">Created Date </label> <input
												type="text" class="width50" readonly="readonly"
												name="createdDate" id="createdDate"
												value="${OPEN_POSITION.createdDateDisplay}" />
										</div>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="float-left width50">
								<fieldset>
									<legend>
										Responsibilities
									</legend>
									<input type="hidden" name="openPosition.responsibilites"
										id="responsibility" value="${OPEN_POSITION.responsibilites}" />
										
									<div>
										<select id="responsibilityList" name="responsibilityList" multiple
											class="width60">
											<c:forEach items="${RESPONSIBILITES}" var="nltls">
													<option value="${nltls.lookupDetailId}">${nltls.displayName} </option>
											</c:forEach>
										</select>
									</div>
									<div style="margin:5px;"><span
										class="button float-left" id="RESPONSIBILITES_1"
										style="position: relative;"> <a
										style="cursor: pointer;" id="RESPONSIBILITES"
										class="btn ui-state-default ui-corner-all recruitment-lookup width10"> <span
											class="ui-icon ui-icon-newwin"> </span> </a> </span> 
										<label class="width50 float-rigt" style="height:20px;top:5px;">Add More...</label>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="float-left width50">
								<fieldset>
									<legend>Qualifications</legend>
									<input type="hidden" name="openPosition.qualifications" id="qualification"
										value="${OPEN_POSITION.qualifications}" />
									<div>
										<select id="qualificationList" name="qualificationList"
											multiple class="width60">
											<c:forEach items="${QUALIFICATIONS}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName} </option>
											</c:forEach>
										</select>
									</div>
									<div style="margin:5px;"><span
										class="button float-left" id="QUALIFICATIONS_1"
										style="position: relative;"> <a
										style="cursor: pointer;" id="QUALIFICATIONS"
										class="btn ui-state-default ui-corner-all recruitment-lookup width10"> <span
											class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											<label class="width50" style="height:20px;top:5px;">Add More...</label>
									</div>
								</fieldset>
							</div>

							<div id="hrm" class="float-left width50">
								<fieldset>
									<legend>Recruitment Resources</legend>
									<input type="hidden" name="openPosition.recruitmentResources" id="recruitmentResources"
										value="${OPEN_POSITION.recruitmentResources}" />
									<div>
										<select id="resourceList" name="resourceList"
											multiple class="width60">
											<c:forEach items="${RECRUITMENT_RESOURCES}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName} </option>
											</c:forEach>
										</select>
									</div>
									<div style="margin:5px;"><span
										class="button float-left" id="RECRUITMENT_RESOURCES_1"
										style="position: relative;"> <a
										style="cursor: pointer;" id="RECRUITMENT_RESOURCES"
										class="btn ui-state-default ui-corner-all recruitment-lookup width10"> <span
											class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											<label class="width50" style="height:20px;top:5px;">Add More...</label>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="float-left width50">
								<fieldset>
									<legend>Skills</legend>
									<input type="hidden" name="openPosition.skills" id="skills"
										value="${OPEN_POSITION.skills}" />
									<div>
										<select id="skillList" name="skillList"
											multiple class="width60">
											<c:forEach items="${SKILLS}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName} </option>
											</c:forEach>
										</select>
									</div>
									<div style="margin:5px;"><span
										class="button float-left" id="SKILLS_1"
										style="position: relative;"> <a
										style="cursor: pointer;" id="SKILLS"
										class="btn ui-state-default ui-corner-all recruitment-lookup width10"> <span
											class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											<label class="width50" style="height:20px;top:5px;">Add More...</label>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="float-left width100">
								<fieldset>
									<legend>Document Uploads</legend>
									<div id="dms_document_information"
										style="cursor: pointer; color: blue;">
										<u><fmt:message key="re.property.info.uploadDocsHere" />
										</u>
									</div>
									<div
										style="padding-top: 10px; margin-top: 3px; height: 150px; overflow: auto;">
										<span id="uploadedDocs"></span>
									</div>
								</fieldset>

							</div>
						</div>

					</form>
					<div class="clearfix"></div>
					
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

		
			</div>
		</div>
	</div>
</body>
</html>