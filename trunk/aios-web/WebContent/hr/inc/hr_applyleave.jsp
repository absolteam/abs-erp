<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>



<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable=null; var selectRow=""; var aData="";var aSelected = [];var leaveProcessId=0;; 
var requestStatus="";
var leaveStatus="";
var diffDays="";var totalDays=0;
$(document).ready(function(){  
	
	leaveHistoryCall();
	check_leave_availability('${CURRENT_DATE}','${CURRENT_DATE}');
	
	$('#leave_document_information').click(function(){
		leaveProcessId=Number($("#leaveProcessId").val());
		if(leaveProcessId>0){
			AIOS_Uploader.openFileUploader("doc","leaveDocs","LeaveProcess",leaveProcessId,"LeaveDocuments");
		}else{
			AIOS_Uploader.openFileUploader("doc","leaveDocs","LeaveProcess","-1","LeaveDocuments");
		}
	});	

	
	$jquery("#LEAVEDET").validationEngine('attach');
	//Save & discard process
	$('.save').click(function(){ 
		$('#page-error').hide();
		if($jquery("#LEAVEDET").validationEngine('validate')){  
			loanLineDetail=""; loanChargesDetail="";
			var leaveProcessId=Number($('#leaveProcessId').val());
			var jobLeaveId=Number($('#leaveType').val());
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var reason=$('#reason').val();
			var contactOnLeave=$('#contactOnLeave').val();
			var halfDay=$('#halfDay').attr('checked');
			var days=Number($('#totalDaysResult').val());
			var weekendDays=Number($('#excludedWeekendDaysResult').val());
			var holidayDays=Number($('#excludedHolidaysResult').val());
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/apply_leave_save.action", 
			 	async: false, 
			 	data:{ 
			 		leaveProcessId:leaveProcessId,jobLeaveId:jobLeaveId,
			 		fromDate:fromDate,toDate:toDate,reason:reason,
			 		contactOnLeave:contactOnLeave,halfDay:halfDay,
			 		days:days,weekendDays:weekendDays,
			 		holidayDays:holidayDays
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						clear_form_elements();
						checkDataTableExsist();
					 }else{
						 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').click(function(){
		window.location.reload();
	});
	
	$('.reset').click(function(){
		clear_form_elements();
	});
	
     //Date process
     $('#fromDate,#toDate').datepick({
    	 defaultDate: '0', selectDefaultDate: true,onSelect: customRange,showTrigger: '#calImg'});   
   
     
     /* Click event handler */
     $('#LeaveProcess tbody tr').live('click', function () {
    	 $('#page-error').hide();
     	  if ( $(this).hasClass('row_selected') ) {
     		  $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             leaveProcessId=aData[0];
             leaveStatus=aData[9];
             requestStatus=aData[10];
         }
         else {
             oTable.$('tr.row_selected').removeClass('row_selected');
             $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             leaveProcessId=aData[0];
             leaveStatus=aData[9];
             requestStatus=aData[10];

         }
     });
     
     $('#edit').click(function(){
    	 $('#page-error').hide();
    	 if(leaveProcessId >0){
    		 if(requestStatus.trim()=='Pending' && leaveStatus.trim()=='Pending'){
    			 populateUploadsPane("doc","leaveDocs","LeaveProcess",leaveProcessId);
    	    	 $.ajax({
    					type:"POST",
    					url:"<%=request.getContextPath()%>/get_edit_leave_process.action", 
    				 	async: false, 
    				 	data:{ 
    				 		leaveProcessId:leaveProcessId
    				 	},
    				    dataType: "html",
    				    cache: false,
    					success:function(result){
    						 $("#LeaveInfoContent").html(result);
    					},
    					error:function(result){  
    						 $("#LeaveInfoContent").html(result);
    					}
    				});  
        	 }else{
        		 $('#page-error').hide().html("Can't Edit this Leave Information").slideDown(1000); 
        	 }
    	 }else{
    		 $('#page-error').hide().html("Please select a record to edit.").slideDown(1000); 
    	 } 
    	 return false;
 	});
  	
     $('#delete').click(function(){
    	 $('#page-error').hide();
    	 if(leaveProcessId >0){
    		 if((requestStatus.trim()=='Pending')&& leaveStatus.trim()=='Pending'){
        		 var cnfrm = confirm('Selected record will be deleted permanently');
     			if(!cnfrm)
     				return false;
    	    	 $.ajax({
    					type:"POST",
    					url:"<%=request.getContextPath()%>/delete_leave.action", 
    				 	async: false, 
    				 	data:{ 
    				 		leaveProcessId:leaveProcessId
    				 	},
    				    dataType: "html",
    				    cache: false,
    					success:function(result){
    						 $(".tempresult").html(result);
    						 var message=$('.tempresult').html(); 
    						 if(message.trim()=="SUCCESS"){
    							checkDataTableExsist();
    						 }else{
    							 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
    						 }
    					},
    					error:function(result){  
    						$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
    					}
    				});  
        	 }else{
        		 $('#page-error').hide().html("Invalid Process").slideDown(1000); 
        	 }
         }else{
        	 $('#page-error').hide().html("Please select a record to delete.").slideDown(1000); 
         } 
    	 
    	 return false;
 	});
     
     $('#view').click(function(){
    	 $('#page-error').hide();
    	 if(leaveProcessId == null || leaveProcessId==0)
 		{
 			alert("Please select one row to View");
 			return false;
 		}
 		else
 		{	
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_view_leave.action", 
				 	async: false, 
				 	data:{ 
				 		leaveProcessId:leaveProcessId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#main-wrapper").html(result);
					},
					error:function(result){  
						$("#main-wrapper").html(result);
					}
				});  
    	 
	    	 return false;
		}
 	});
     
     $('#cancel').click(function(){
    	 $('#page-error').hide();
    	 if((requestStatus.trim()!='Pending') && (leaveStatus.trim()=='Pending')){
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/cancel_leave.action", 
				 	async: false, 
				 	data:{ 
				 		leaveProcessId:leaveProcessId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							checkDataTableExsist();
						 }else{
							 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					}
				});  
    	 }else{
    		 $('#page-error').hide().html("Invalid Process").slideDown(1000); 
    	 }
    	 return false;
 	});
     $('#halfDay').change(function(){
    	 if($('#halfDay').attr("checked"))
 			$('#totalDaysResult').val(Number($('#totalDaysResult').val())-0.5);
     	 else
     		 $('#totalDaysResult').val(totalDays); 
     });
     
    

});

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		dateOnchangeCall();
		
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
		dateOnchangeCall();
	} 
}
function dateOnchangeCall(){
	if($('#toDate').val()!=null && $('#toDate').val()!="" && $('#fromDate').val()!=null && $('#fromDate').val()!=""){ 
		daysDiff();
		check_leave_availability($('#fromDate').val(),$('#toDate').val());
		leaveExcludingCalculation($('#fromDate').val(),$('#toDate').val());
		totalDays=$('#totalDaysResult').val();
			
	}
	return true;
}

function checkDataTableExsist(){
	oTable = $('#LeaveProcess').dataTable();
	oTable.fnDestroy();
	$('#LeaveProcess').remove();
	$('#gridDiv').html("<table class='display' id='LeaveProcess'></table>");
	leaveHistoryCall();
}
function leaveHistoryCall(){
	$('#page-error').hide();
	$('#LeaveProcess').dataTable({ 
		"sAjaxSource": "get_leave_process_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'LeaveProcess ID', "bVisible": false},
			{ "sTitle": 'Type'},
			{ "sTitle": 'From'},
			{ "sTitle": 'To'},
			{ "sTitle": 'Half Day?', "bVisible": false},
			{ "sTitle": 'No.Of Days'},
			{ "sTitle": 'Reason'},
			{ "sTitle": 'Contact On Leave'},
			{ "sTitle": 'Requested Date'},
			{ "sTitle": 'Leave Status'},
			{ "sTitle": 'Request Status'},
			
		],
		"sScrollY": $("#main-content").height() - 400,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#LeaveProcess').dataTable();
}
function clear_form_elements() {
	$('#page-error').hide();
	$('.leaveTypeformError,.fromDateformError,.toDateformError,.reasonformError').remove(); 
	$('#LEAVEDET').each(function(){
	    this.reset();
	});
}
var fromFormat='dd-MMM-yyyy';
var toFormat='MM/dd/y';
function daysDiff() {
	var tempdate1 = formatDate(new Date(getDateFromFormat($("#fromDate").val(),fromFormat)),toFormat);
	var tempdate2 = formatDate(new Date(getDateFromFormat($("#toDate").val(),fromFormat)),toFormat);
	var date1=new Date(tempdate1);
	var date2=new Date(tempdate2);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	 $('#tempDays').val(diffDays);
}
function check_leave_availability(fromDate,toDate) {
	$('#page-error').hide();
	if(fromDate!=null && fromDate!="" && toDate!=null && toDate!=""){ 
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/check_leave_availability.action", 
			 	async: false, 
			 	data:{ 
			 		fromDate:fromDate,toDate:toDate,jobAssignmentId:0
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#leaveAvailabilityDiv').html(result);
				},
				error:function(result){  
					$('#leaveAvailabilityDiv').html("No Availability").slideDown(1000);
				}
			});  
	 }else{
		 $('#leaveAvailabilityDiv').html("Enter Leave Period to get Leave Availability").slideDown(1000); 
	 }
	 return false;
}
function leaveExcludingCalculation(fromDate,toDate) {
	$('#page-error').hide();

   	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/leave_excluding_calculation.action", 
		 	async: false, 
		 	data:{ 
		 		fromDate:fromDate,toDate:toDate,jobAssignmentId:0
		 	},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#LEAVE_EXCLUDING_RESULT').html(result);
			},
			error:function(result){  
				$('#LEAVE_EXCLUDING_RESULT').html("Error In Total Days Calculation").slideDown(1000);
			}
		});  
	 
	 return false;
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Apply Leave</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="LEAVEDET" class="" name="LEAVEDET" method="post" style="position: relative;">
		  	<div class="width100 float-left" id="hrm"> 
					<div class="width55 float-left" id="LeaveInfoContent">
						<fieldset style="min-height:200px;">
							<legend>Leave Information</legend>
							<div class="width100 float-left">
								<input type="hidden" name="leaveProcessId" id="leaveProcessId" value="${LEAVE_PROCESS.leaveProcessId}" class="width50"/>
								<div>
									<label class="width30" for="leaveType">Leave Type<span class="mandatory">*</span></label>
									<select id="leaveType" name="leaveType" class="width61 validate[required]">
										<option value="">--Select--</option>
										<c:forEach items="${JOB_LEAVE_LIST}" var="jobleave" varStatus="status">
											<option value="${jobleave.jobLeaveId}">${jobleave.leave.lookupDetail.displayName}</option>
										</c:forEach>
									</select>
								</div>   
								<div>
									<label class="width30" for="fromDate">Leave Start Date<span class="mandatory">*</span></label>
									<c:choose>
										<c:when test="${LEAVE_PROCESS.fromDate ne null &&  LEAVE_PROCESS.fromDate  ne ''}">
											<c:set var="fromDate" value="${LEAVE_PROCESS.fromDate}"/>  
											<%String fromDate = DateFormat.convertDateToString(pageContext.getAttribute("fromDate").toString());%>
											<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="1" class="width60 validate[required]" 
											value="<%=fromDate%>"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="1" class="width60 validate[required]"/>
										</c:otherwise>
									</c:choose> 
								</div>   
								<div  class="float-left width100">
									<label class="width30" for="toDate">Leave End Date<span class="mandatory">*</span></label>
									<c:choose>
										<c:when test="${LEAVE_PROCESS.toDate ne null &&  LEAVE_PROCESS.toDate ne ''}">
											<c:set var="toDate" value="${LEAVE_PROCESS.toDate}"/>  
											<%String toDate = DateFormat.convertDateToString(pageContext.getAttribute("effectiveEndDate").toString());%>
											<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="3" class="width60 validate[required]" 
											value="<%=toDate%>"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="4" class="width60 validate[required]"/>
										</c:otherwise>
									</c:choose> 
								</div> 
								<div class="float-left width100">
									<label class="float-left width30" for="leaveType">Half Day</label>
									<input type="checkbox" name="halfDay" id="halfDay" class="float-left width4" >
									<label class="float-left width30" for="leaveType" style="display:none;">No. Of Days :</label>
									<input type="hidden" id="tempDays" class="float-left width10" readonly="readonly"/>
									<input type="hidden" id="tempYears"/><input type="hidden" id="tempMonths"/>
								</div>
								<div  class="float-left width100">
									<label class="width30" for="reason" >Reason<span class="mandatory">*</span></label>
									<textarea name="reason" id="reason" class="width61 validate[required]" ></textarea>
								</div>
								<div>
									<label class="width30" for="contactOnLeave">Contact On Leave</label>
									<textarea name="contactOnLeave" id="contactOnLeave" class="width61" ></textarea>
								</div>
							
							</div>
							
							<div class="clearfix"></div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right reset" id="reset">Reset</div>
								<div class="portlet-header ui-widget-header float-right save" id="job_save">Apply</div>
							</div>
						</fieldset>
				 </div> 
			     <div class="width45 float-left">
					<fieldset style="height:209px;">
							<legend>Leave Availability</legend>
							<div id="leaveAvailabilityDiv">
							</div>
					</fieldset>
				</div>
            </div>
            <div class="width100 float-left" id="hrm"> 
           		 <div class="width55 float-left">
           			<fieldset style="min-height:50px;">
								<legend>Leave Day's</legend>
								<div id="LEAVE_EXCLUDING_RESULT" class="width100 float-left"></div>
					</fieldset>
				</div>
				<div class="width45 float-left">
				 <fieldset>
		    	 	<div  style="height: 50px;overflow-y:auto;">
					  	<div class="float-left">
							<span id="leave_document_information" style="cursor: pointer; color: blue;">
								<u><fmt:message key="hr.company.documentuplode"/></u>
							</span>
						</div>
						<div class="width80 float-right">
							<span id="leaveDocs"></span>
						</div>
					</div>
			     </fieldset>
			    </div>
 			</div>
 		</form>
		</div>
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="LeaveProcess"></table>
			</div>
	 	</div>		
		<div class="vmenu">
			<div class="first_li"><span id="delete">Delete</span></div>
			<div class="first_li"><span id="view">View</span></div>
			<div class="first_li"><span id="edit">Edit</span></div>
			
		</div> 
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
			<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
			<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
	 		<div class="portlet-header ui-widget-header float-right" id="view">View</div> 
 		</div>
	</div>	
</div>
