<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var locationId=0;var locationName=''; 
$(function(){ 
	$('#SHIFT').dataTable({ 
		"sAjaxSource": "get_job_workingshift_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Shift ID', "bVisible": false},
			{ "sTitle": 'Shift Name'},
			{ "sTitle": 'Start Time'},
			{ "sTitle": 'End Time'},
			{ "sTitle": 'Late Entry'},
			{ "sTitle": 'Severe Late Entry'},
			{ "sTitle": 'Buffer Min/Hour(s)'},
			{ "sTitle": 'Break Min/Hour(S)'},
			{ "sTitle": 'Irregurlar Day'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#SHIFT').dataTable();

	/* Click event handler */
	$('#SHIFT tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1];
	    }
	});
	$('#SHIFT tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        designationId=aData[0];
	        locationName=aData[1];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1];
	    }
		  var rowid=Number($("#rec_rowid").val());
		$("#shiftTypeName_"+rowid).html(locationName);
		$("#workingShiftId_"+rowid).val(locationId);
		$('#job-common-popup').dialog("close");

	});
	
	$('#person-list-close').click(function () { 
		$('#job-common-popup').dialog("close");
	});
});
</script>
<div id="main-content"> 
 	<input type="hidden" id="rec_rowid" value="${rowId}">

 	<div id="trans_combination_accounts">
		<table class="display" id="SHIFT"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="person-list-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div> 
</div>