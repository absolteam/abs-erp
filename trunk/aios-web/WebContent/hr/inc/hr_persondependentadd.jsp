 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
 
<tr style="background-color: #F8F8F8;" id="childRowId_${requestScope.rowid}">
<td colspan="10" class="tdidentity" id="childRowIdTD_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="periodform" id="dependentValidate" style="position: relative;">
<input type="hidden" name="addEditFlag" id="addEditFlag"  value="${requestScope.AddEditFlag}"/>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div>
			<label class="width30"><fmt:message key="hr.person.relationship"/><span class="mandatory">*</span></label>
			<input type="hidden" name="relationshipTemp" id="relationshipTemp" class="width60">
			<select name="relationship" id="relationship" tabindex="1" class="width60 validate[required]">
				<option value="">--Select--</option>
				<c:if test="${requestScope.DEPENDENT_TYPE_LIST!=null}">
				<c:forEach items="${requestScope.DEPENDENT_TYPE_LIST}" var="nltlst">
					<option value="${nltlst.relationshipId}">${nltlst.relationship} </option>
				</c:forEach>
				</c:if>	
				
			</select>
		</div>
		<div><label class="width30"><fmt:message key="hr.person.name"/><span class="mandatory">*</span></label><input type="text" tabindex="2" name="dependentName" id="dependentName" class="width60 validate[required]"></div>
		<div><label class="width30"><fmt:message key="hr.person.mobile"/><span class="mandatory">*</span></label><input type="text" tabindex="3" name="dependentMobile" id="dependentMobile" class="width60 validate[required,custom[onlyNumber]]"></div>
		<div><label class="width30"><fmt:message key="hr.common.phone"/></label><input type="text" tabindex="4" name="dependentPhone" id="dependentPhone" class="width60 validate[optional,custom[onlyNumber]]"></div> 
		<div><label class="width30"><fmt:message key="hr.common.email"/></label><input type="text" tabindex="5" name="dependentMail" id="dependentMail" class="width60"></div>
		<div>
			<input type="checkbox" tabindex="6" name="emergency" id="emergency" class="width5 float-left"><label class="width20 float-left"><fmt:message key="hr.person.emergency"/></label>
			<input type="checkbox" tabindex="7" name="dependent" id="dependent" class="width5 float-left"><label class="width20 float-left"><fmt:message key="hr.person.dependent"/></label>
			<input type="checkbox" tabindex="8" name="benefit" id="benefit" class="width5 float-left"><label class="width20 float-left"><fmt:message key="hr.person.benifit"/></label>
		</div> 
	</fieldset>
</div>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div><label class="width30"><fmt:message key="hr.common.address"/></label><textarea tabindex="9" name="dependentAddress" id="dependentAddress" class="width60"></textarea></div>
		<div><label class="width30"><fmt:message key="common.description"/></label><textarea tabindex="10" name="dependentDescription" id="dependentDescription" class="width60"></textarea></div>
	</fieldset> 
</div> 
	<div class="clearfix"></div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_dependent" id="cancel_${requestScope.rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right add_dependent" id="update_${requestScope.rowid}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
	 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){  
		 $jquery("#dependentValidate").validationEngine('attach');
		 $('#idendityExpireDate').datepick();
		 clear:false;
		  $('.cancel_dependent').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
  					$("#AddImage_"+rowid).show();
  				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
  				 	$("#EditImage_"+rowid).show();
				}
				 $("#childRowId_"+rowid).remove();	
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 tempvar=$('.tab>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#DeleteImage_'+rowid).hide(); 
		  });
		  $('.add_dependent').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 
			 //var tempObject=$(this);
			 var personId=Number($('#personId').val());
			 var dependentId=$('#dependentId_'+rowid).val();	
			 var relationship=$('#relationship :selected').text();
			 var relationshipId=$('#relationship :selected').val();
			 var dependentName=$('#dependentName').val();
			 var dependentMobile=$('#dependentMobile').val();
			 var dependentPhone=$('#dependentPhone').val();
			 var dependentMail=$('#dependentMail').val();
			 
			 var emergency=$("#emergency").attr("checked");
			 var dependent=$("#dependent").attr("checked");
			 var benefit=$("#benefit").attr("checked");
			 
			 var dependentAddress=$('#dependentAddress').val();
			 var dependentDescription=$('#dependentDescription').val();
			 
			 var lineId=$('#lineId_'+rowid).val();
			 var addEditFlag=$('#addEditFlag').val();
			
		        if($jquery("#dependentValidate").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/person_dependent_save.action", 
					 	async: false, 
					 	data:{
							 personId:personId,
							 dependentId:dependentId,
							 relationshipId:relationshipId,
							 dependentName:dependentName,
							 dependentMobile:dependentMobile,
							 dependentPhone:dependentPhone,
							 dependentMail:dependentMail,
							 emergency:emergency,
							 dependentflag:dependent,
							 benefit:benefit,
							 dependentAddress:dependentAddress,
							 dependentDescription:dependentDescription,
							 addEditFlag:addEditFlag,
							 id:lineId
							 },
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(dependent); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 

			        	//Bussiness parameter
		        			$('#dependentName_'+rowid).text(dependentName);
		        			$('#dependentRelationship_'+rowid).text(relationship);  
		        			$('#dependentRelationshipId_'+rowid).val(relationshipId); 
		        			$('#dependentMobile_'+rowid).text(dependentMobile); 
		        			$('#dependentPhone_'+rowid).text(dependentPhone); 
		        			$('#dependentEmail_'+rowid).text(dependentMail);
		        			if(emergency==true)
		        				$('#emergency_'+rowid).text("YES");
		        			else
		        				$('#emergency_'+rowid).text("NO");
		        			
		        			if(dependent==true)	
		        				$('#dependent_'+rowid).text("YES");
		        			else
		        				$('#dependent_'+rowid).text("NO");
		        			
		        			if(benefit==true)	
		        				$('#benefit_'+rowid).text("YES");
		        			else
		        				$('#benefit_'+rowid).text("NO");
		        			
		        			
		        			
		        			$('#dependentAddress_'+rowid).text(dependentAddress);
		        			$('#dependentDescription_'+rowid).val(dependentDescription);
						//Button(action) hide & show 
		    				 $("#AddImage_"+rowid).hide();
		    				 $("#EditImage_"+rowid).show();
		    				 $("#DeleteImage_"+rowid).show();
		    				 $("#WorkingImage_"+rowid).hide(); 
		        			 $("#childRowId_"+rowid).remove();	

		        			if(addEditFlag!=null && addEditFlag=='A')
		     				{
	         					$('.addrows').trigger('click');
	         					 tempvar=$('.tab>tr:last').attr('id'); 
	         					 idval=tempvar.split("_");
	         					 rowid=Number(idval[1]); 
	         					 $('#DeleteImage_'+rowid).hide(); 
		     				}
		        			
		        			
		        		}		  	 
		      } 
		      else{return false;}
		  });
			
	 });
 </script>	