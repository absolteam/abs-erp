 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %> 
<tr style="background-color: #F8F8F8;" id="identitychildRowId_${requestScope.rowid}">
<td colspan="6" class="tdidentity" id="identitychildRowTD_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="periodform" id="periodEditAddValidate" style="position: relative;">
<input type="hidden" name="identityaddEditFlag" id="identityaddEditFlag"  value="${requestScope.AddEditFlag}"/>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div>
			<label class="width30"><fmt:message key="hr.person.identitytype"/><span class="mandatory">*</span></label>
			<input type="hidden" name="identityTypeTemp" id="identityTypeTemp" class="width60">
			<select name="identityType" id="identityType" tabindex="1" class="width60 validate[required]">
				<option value="">--Select--</option>
				<c:if test="${requestScope.IDENTITY_TYPE_LIST!=null}">
				<c:forEach items="${requestScope.IDENTITY_TYPE_LIST}" var="nltlst">
					<option value="${nltlst.identityType}">${nltlst.identityTypeName} </option>
				</c:forEach>
				</c:if>	
			</select>
		</div>
		<div><label class="width30"><fmt:message key="hr.person.identitynumber"/><span class="mandatory">*</span></label><input type="text" tabindex="2" name="identityNumber" id="identityNumber" class="width60 validate[required]"></div>
		<div><label class="width30"><fmt:message key="hr.person.issuedplace"/><span class="mandatory">*</span></label><input type="text" tabindex="3" name="identityIssuedPlace" id="identityIssuedPlace" class="width60 validate[required]"></div>
		<div><label class="width30"><fmt:message key="hr.person.expiredate"/></label><input type="text" tabindex="4" name="idendityExpireDate" id="idendityExpireDate" readonly="readonly" class="width60"></div> 
	</fieldset>
</div>
<div class="float-left width50" id="hrm">
	<fieldset>
		<div><label class="width30"><fmt:message key="common.description"/></label><textarea tabindex="3" name="identityDescription" id="identityDescription" class="width60"></textarea></div>
	</fieldset> 
</div> 
	<div class="clearfix"></div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_identity" id="cancel_${requestScope.rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right add_identity" id="update_${requestScope.rowid}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript"> 
 $(function(){  
	 $("#idendityExpireDate").datepick();
	 $jquery("#periodEditAddValidate").validationEngine('attach');
 });
 </script>	 