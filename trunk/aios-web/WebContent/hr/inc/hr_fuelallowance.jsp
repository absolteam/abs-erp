<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>

</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var accessCode=null;
var commonRowId=0;
var rowId=null;
$(document).ready(function(){  
	
	var fuelAllowanceId=Number($("#fuelAllowanceId").val());
	if(fuelAllowanceId!=0){
		$('#vehicleNumber').val($('#vehicleNumberTemp').val());
		$('#fuelCompany').val($('#fuelCompanyTemp').val());
		$('#cardType').val($('#cardTypeTemp').val());
		if($('#isFinanceImpactTemp').val()!=null && $('#isFinanceImpactTemp').val()=='true')
			$('#isFinanceImpact').attr("checked",true);
		else
			$('#isFinanceImpact').attr("checked",false);
	
	}
	
	
	$('#fromDate,#toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});		


	$jquery("#JOBDetails").validationEngine('attach');
	
	//Leave onchange events
	$('.discard').click(function(){
		listLoadCall();
		 return false;
	});
	

	

	
	
	//Save & discard process
	$('.save').click(function(){ 
		if($jquery("#JOBDetails").validationEngine('validate')){  
			 
			var fuelAllowanceId=Number($('#fuelAllowanceId').val());
			var successMsg="";
			if(fuelAllowanceId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var jobAssignmentId=$('#jobAssignmentId').val();
			var jobPayrollElementId=$('#jobPayrollElementId').val();
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var receiptNumber=$('#receiptNumber').val();
			var isFinanceImpact=$('#isFinanceImpact').attr("checked");
			var amount=$('#amount').val();
			var description=$('#description').val();
			var payPeriodTransactionId=$('#payPeriod').val();
			
			var cardNumber=$('#cardNumber').val();
			var vehicleNumber=$('#vehicleNumber').val();
			var fuelCompany=$('#fuelCompany').val();
			var cardType=$('#cardType').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/fuel_allowance_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		fuelAllowanceId:fuelAllowanceId,
			 		jobAssignmentId:jobAssignmentId,
			 		jobPayrollElementId:jobPayrollElementId,
			 		fromDate:fromDate,
			 		toDate:toDate,
			 		receiptNumber:receiptNumber,
			 		isFinanceImpact:isFinanceImpact,
			 		amount:amount,
			 		description:description,
			 		payPeriodTransactionId:payPeriodTransactionId,
			 		cardNumber:cardNumber,
			 		vehicleNumber:vehicleNumber,
			 		fuelCompany:fuelCompany,
			 		cardType:cardType
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	
	 $('.recruitment-lookup').live('click',function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
        rowId=getRowId($(this).parent().attr('id'));
         accessCode=$(this).attr("id"); 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
   
	 $('#save-lookup').live('click',function(){ 
	 		
		 if(accessCode=="VEHICLE_NUMBER"){
				$('#vehicleNumber').html("");
				$('#vehicleNumber').append("<option value=''>--Select--</option>");
				loadLookupList("vehicleNumber");
			}
		 if(accessCode=="FUEL_COMPANY"){
				$('#fuelCompany').html("");
				$('#fuelCompany').append("<option value=''>--Select--</option>");
				loadLookupList("fuelCompany");
		    }
		 if(accessCode=="CARD_TYPE"){
				$('#cardType').html("");
				$('#cardType').append("<option value=''>--Select--</option>");
				loadLookupList("cardType");
		    }	
			
			
		}); 
	 
	
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

   
     //Date process
     
     $('.fromDate,.toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});
     
});
function listLoadCall(){
	var params="id=0";
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var transactionDate=$('#transactionDate').val();
	var payPolicy=$('#payPolicy').val();
	var payPeriodTransactionId=$('#payPeriod').val();
	
	params+="&jobPayrollElementId="+jobPayrollElementId;
	params+="&jobAssignmentId="+jobAssignmentId;
	params+="&transactionDate="+transactionDate;
	params+="&payPolicy="+payPolicy;
	params+="&payPeriodTransactionId="+payPeriodTransactionId;

	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/fuel_allowance.action?"+params, 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#allowance-manipulation-div").html(result);  
				return false;
			}
	 });
}
function customRanges(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}

</script>

 
<div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Fuel Allowance</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="JOBDetails" class="" name="JOBDetails" method="post" style="position: relative;">
		  	<input type="hidden" name="fuelAllowanceId" id="fuelAllowanceId" value="${ALLOWANCE.fuelAllowanceId}" class="width50"/>
		  	<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="min-height: 150px;">
							<legend>Allowance Information</legend>
							<div class="width35 float-left">
								<div>
									<label class="width30">Vehicle Number<span
										style="color: red">*</span> </label>
									<div>
										<select id="vehicleNumber"
											name="vehicleNumber"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${VEHICLE_NUMBER}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="VEHICLE_NUMBER_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="VEHICLE_NUMBER"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="vehicleNumberTemp"
											id="vehicleNumberTemp"
											value="${ALLOWANCE.lookupDetailByVehicleNumber.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Fuel Company</label>
									<div>
										<select id="fuelCompany"
											name="fuelCompany"
											class="width50">
											<option value="">--Select--</option>
											<c:forEach items="${FUEL_COMPANY}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="FUEL_COMPANY_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="FUEL_COMPANY"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="fuelCompanyTemp"
											id="fuelCompanyTemp"
											value="${ALLOWANCE.lookupDetailByFuelCompany.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Card Type</label>
									<div>
										<select id="cardType"
											name="cardType"
											class="width50">
											<option value="">--Select--</option>
											<c:forEach items="${CARD_TYPE}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" id="CARD_TYPE_1"
											style="position: relative; "> <a
											style="cursor: pointer;" id="CARD_TYPE"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="cardTypeTemp"
											id="cardTypeTemp" value="${ALLOWANCE.lookupDetailByCardType.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Card Number</label>
									<input type="text" id="cardNumber" name="cardNumber" class="width50 " value="${ALLOWANCE.cardNumber}">
								</div>
								<div>
									<label class="width30">Amount<span
										style="color: red">*</span></label>
									<input type="text" id="amount" name="amount" class="width50  validate[required]" value="${ALLOWANCE.amount}">
								</div>
								<div>
									<label class="width30">Receipt Number</label>
									<input type="text" id="receiptNumber" name="receiptNumber" class="width50" value="${ALLOWANCE.receiptNumber}">
								</div>
								<div>
									<label class="width30">Finance Impact</label>
									<input type="checkbox" id="isFinanceImpact" name="isFinanceImpact" class="width10" >
									<input type="hidden" id="isFinanceImpactTemp" name="isFinanceImpactTemp" class="width10 " value="${ALLOWANCE.isFinanceImpact}" >
								</div>
								
							</div>
							<div class="width40 float-left">
							
							
								<div>
									<label class="width30" for="fromDate">From 
										Date<span class="mandatory">*</span>
									</label> <input type="text" readonly="readonly" name="fromDate"
										id="fromDate" value="${ALLOWANCE.fromDateView}"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="toDate">To 
										Date<span class="mandatory">*</span></label> <input type="text" readonly="readonly"
										name="toDate" id="toDate"
										value="${ALLOWANCE.toDateView}" class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30">Description</label> <textarea 
										id="description" name="description" class="width50 "
										>${ALLOWANCE.description}</textarea>
								</div>
							
							</div>
							<div class="width20 float-left">
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Pay Elment : </label>
									<label class="width40 ">${JOBPAY_INFO.elementName}</label>
									<input type="hidden" id="jobPayrollElementId" value="${JOBPAY_INFO.jobPayrollElementId}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period From : </label>
									<label class="width40 ">${JOBPAY_INFO.periodStartDate}</label>
									<input type="hidden" id="periodStartDate" value="${JOBPAY_INFO.periodStartDate}"/>
									
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period To : </label>
									<label class="width40 ">${JOBPAY_INFO.periodEndDate}</label>
									<input type="hidden" id="periodEndDate" value="${JOBPAY_INFO.periodEndDate}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 " >Provided Amount(Max) : </label>
									<label class="width40 ">${JOBPAY_INFO.allowedMaixmumAmount}</label>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Available Balance : </label>
									<label class="width40 ">${JOBPAY_INFO.availableAmount}</label>
								</div>
							</div>
						</fieldset>
					</div>
			</div>
 		</form>
		
					
	
		</div>
		<div class="clearfix"></div>
		<div class="float-right buttons ui-widget-content ui-corner-all">
			<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right save" id="job_save"><fmt:message key="organization.button.save"/></div>
		</div>
		<div id="common-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;display:none;"> 
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
		
</div>

 
	

