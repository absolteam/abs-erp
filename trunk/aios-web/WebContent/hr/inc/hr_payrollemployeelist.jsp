<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count{
	padding:0!important;
}
</style> 

<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
$(document).ready(function(){
	setTimeout(multiselectss(), 1000);
});
function multiselectss(){
	$("#jobAssignmentList").multiselect();
}
</script>

  <select name="company" id="jobAssignmentList" class="width40" multiple="multiple">
    <c:forEach items="${PERSON_LIST}" var="per" varStatus="status">
	<option value="${per.jobAssignmentId}">${per.person.firstName}
		${per.person.lastName}[${per.jobAssignmentNumber}]</option>
</c:forEach>
  </select>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
  