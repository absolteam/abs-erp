<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>

<style>
.container {
	float: left;
	padding: 1px;
}

.container select {
	width: 35em;
	height: 15em;
}

input[type="button"] {
	width: 5em;
}

.low {
	position: relative;
	top: 55px;
}

.ui-autocomplete {
	width: 28% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 60%!important;
}

.ui-combobox-button {
	height: 32px;
}
</style> 

<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var requestStatus="";
var status="";
var payMonth=null;
var payYear=null;
var selectedLocationId=0;
$(document).ready(function(){
	$('#locations').combobox({
		selected : function(event, ui) {
			selectedLocationId = $('#locations :selected').val(); 
		}
	});

	
	
	 $("#payView").click(function(){
		 
			payMonth=$('#payMonth option:selected').val();
	 		payYear=$('#payYear option:selected').val();
	 		
    	 $('#page-error').hide();
    	
    		window.open('<%=request.getContextPath()%>/payroll_report_printout.action?payMonth='+payMonth+'&payYear='+payYear+'&locationId='+selectedLocationId+'&printCall='+false,
    				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
		
 		return false;
	
    });
	 
	 $("#wps_view").click(function(){
    	 $('#page-error').hide();
    	
    		window.open('<%=request.getContextPath()%>/wps_execution.action?payMonth='+payMonth+'&payYear='+payYear+'&printCall='+false,
    				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
		
 		return false;
	
    });
	 
	 
	 
	 $('.common-popup').click(function(){ 
			$('.ui-dialog-titlebar').remove();
			actionname=""; 
			actionname=$(this).attr('id');  
			if(actionname=='receive'){ 
				actionname="show_receive_list";
			} 
			else if(actionname=="accountid"){
		    	   bankId=Number(0);
		    	   actionname="getbankaccount_details";
		    }
			else{ 
				actionname="show_creditterm_details";
			} 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+actionname+".action", 
				data : {showPage : "wps"},
			 	async: false,   
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.common-result').html(result);  
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
				return false;
		});
  

		$('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:900, 
			 height:550, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
		
		 $('#update').click(function(){
		        	
		        	 $('.ui-dialog-titlebar').remove();  
		             $('#job-common-popup').dialog('open');
		           
				           $.ajax({
				              type:"POST",
				              url:"<%=request.getContextPath()%>/get_payroll_to_finalize.action",
				              async: false,
				              dataType: "html",
				              cache: false,
				              success:function(result){
				                   $('.job-common-result').html(result);
				                   return false;
				              },
				              error:function(result){
				                   $('.job-common-result').html(result);
				              }
				          });
				           return false;
		       
		  	}); 
		 
		 $('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:900,
				height:550,
				bgiframe: false,
				modal: true 
			});
		 
		 $('#download-wps').live('click',function(){
	    	 $('#page-error').hide();
		    	
	    	 var win1=window.open('downloadWPS.action?payMonth='+payMonth+'&payYear='+payYear+'&format=PDF');
	    	 
	    	 return false;
	 	});
		 
		
	
});

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}


</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay Commit & WPS</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
				<div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="POAYROLLFORM" class="" name="POAYROLLFORM" method="post">
		  	<div class="width100 float-left" id="hrm"> 
					<div class="width100 float-left" id="LeaveInfoContent">
						<fieldset style="min-height:120px;">
							<legend>Payroll Information</legend>
							<div class="width100 float-left">
								
								<div class="width50 float-left">
									<label class="width30" for="accountNumber"><fmt:message key="hr.wps.bankaccount"/></label>
									<input type="text" readonly="readonly" name="accountNumber" tabindex="11"  id="accountNumber" 
										class="width60 validate[required]" value="${COMPANY.accountNumber}"/>
									<span class="button" style="position: relative!important;top:0px!important;">
										<a style="cursor: pointer;" id="accountid" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>
									<input type="hidden" readonly="readonly" name="accountId" id="accountId"
										value="${COMPANY.bankAccountId}"/>
								</div>
								 <div class="width50 float-left">
									<label class="width30" for="payMonth"><fmt:message key="hr.payroll.paymonth"/><span class="mandatory">*</span></label>
									<select id="payMonth" name="payMonth" class="width30 validate[required]">
										<option value="">--Select--</option>
										<c:forEach items="${PAYMONTH_LIST}" var="comp1" varStatus="status">
											<option value="${comp1}">${comp1}</option>
										</c:forEach>
									</select>
									<select id="payYear" name="payYear" class="width30 validate[required]">
										<option value="2015">2015</option>
										<option value="2014">2014</option>
									</select>
								</div> 
								<div class="width50 float-left">
									<label class="width30" for="location">Location</label>
									<select id="locations" class="width60">
										<option value="">Select</option>
										<c:forEach var="location" items="${LOCATION_LIST}">
											<option value="${location.locationId}">${location.locationName}</option>						
										</c:forEach>
									</select>
								</div> 
							</div>
							
							<div class="clearfix"></div>
						
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right download-wps" id="download-wps">Donwload WPS</div>
								<div class="portlet-header ui-widget-header float-right wps_view" id="wps_view">WPS View</div>
								<div class="portlet-header ui-widget-header float-right update" id="update">Pay Status Update</div>
								<div class="portlet-header ui-widget-header float-right pay-view" id="payView">Pay View</div>
							</div>
							
					 		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
								<div class="common-result width100"></div>
								<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
							</div>
							<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
								<div class="job-common-result width100"></div>
								<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
						 	</div>
						</fieldset>
				 </div> 
				 
				
 			</div>
 		</form>
		</div>
		
	
	</div>	
</div>
