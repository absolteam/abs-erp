<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>

</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var accessCode=null;
var rowId=null;
$(document).ready(function(){  
	
	var ticketAllowanceId=Number($("#ticketAllowanceId").val());
	if(ticketAllowanceId!=0){
		$('#source').val($('#sourceTemp').val());
		$('#destination').val($('#destinationTemp').val());
		$('#airline').val($('#airlineTemp').val());
		$('#agency').val($('#agencyTemp').val());
		$('#travelClass').val($('#travelClassTemp').val());
		$('#ticketTypeClass').val($('#ticketTypeTemp').val());
		if($('#isFinanceImpactTemp').val()!=null && $('#isFinanceImpactTemp').val()=='true')
			$('#isFinanceImpact').attr("checked",true);
		else
			$('#isFinanceImpact').attr("checked",false);
		
		
	}
		
	$('#purchaseDate').datepick();
	
	$('#fromDate,#toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'});		


	$jquery("#AllowanceForm").validationEngine('attach');
	
	//Leave onchange events
	$('.discard_allowance').click(function(){
		listLoadCall();
		 return false;
	});
	

	
	
	//Save & discard process
	$('.save').click(function(){ 
		if($jquery("#AllowanceForm").validationEngine('validate')){  
			 
			var ticketAllowanceId=Number($('#ticketAllowanceId').val());
			var successMsg="";
			if(ticketAllowanceId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
			var jobAssignmentId=$('#jobAssignmentId').val();
			var jobPayrollElementId=$('#jobPayrollElementId').val();
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var receiptNumber=$('#receiptNumber').val();
			var isFinanceImpact=$('#isFinanceImpact').attr("checked");
			var amount=$('#amount').val();
			var description=$('#description').val();
			
			var source=$('#source').val();
			var destination=$('#destination').val();
			var airline=$('#airline').val();
			var agency=$('#agency').val();
			var travelClass=$('#travelClass').val();
			var ticketType=$('#ticketType').val();
			var numberOfTicket=$('#numberOfTicket').val();
			var purchaseDate=$('#purchaseDate').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/ticket_allowance_save.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		ticketAllowanceId:ticketAllowanceId,
			 		jobAssignmentId:jobAssignmentId,
			 		jobPayrollElementId:jobPayrollElementId,
			 		fromDate:fromDate,
			 		toDate:toDate,
			 		receiptNumber:receiptNumber,
			 		isFinanceImpact:isFinanceImpact,
			 		amount:amount,
			 		description:description,
			 		
			 		source:source,
			 		destination:destination,
			 		airline:airline,
			 		agency:agency,
			 		travelClass:travelClass,
			 		ticketType:ticketType,
			 		numberOfTicket:numberOfTicket,
			 		purchaseDate:purchaseDate
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	
	 $('.recruitment-lookup').live('click',function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
       	 rowId=getRowId($(this).attr('id')); 
         accessCode=getAccessCode($(this).attr("id"));
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
   
	 $('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="PORT" && rowId==1){
				$('#source').html("");
				$('#source').append("<option value=''>--Select--</option>");
				loadLookupList("source");
			}
			
			if(accessCode=="PORT" && rowId==2){
				$('#destination').html("");
				$('#destination').append("<option value=''>--Select--</option>");
				loadLookupList("destination");
			}
			

			if(accessCode=="AIRLINE"){
				$('#airline').html("");
				$('#airline').append("<option value=''>--Select--</option>");
				loadLookupList("airline");
			}
			if(accessCode=="AGENCY"){
				$('#agency').html("");
				$('#agency').append("<option value=''>--Select--</option>");
				loadLookupList("agency");
			}
		}); 
	 
    
     $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			overflow: 'hidden',
			height:600,
			bgiframe: false,
			modal: true 
		});

   
     //Date process
     
     //$(".fromDate").datepicker('dateFormat', 'dd-MMM-yyyy',"option", "minDate", new Date(Date.parse($("#periodStartDate").val())));
     //$(".toDate").datepicker('dateFormat', 'dd-MMM-yyyy',"option", "maxDate", new Date(Date.parse($("#periodEndDate").val())));
     
      $('.fromDate,.toDate').datepick({
		 onSelect: customRanges,showTrigger: '#calImg'}); 
     
      loadEmployeePolicy();

});
function listLoadCall(){
	var params="id=0";
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var transactionDate=$('#transactionDate').val();
	var payPolicy=$('#payPolicy').val();
	var payPeriodTransactionId=$('#payPeriod').val();

	params+="&jobPayrollElementId="+jobPayrollElementId;
	params+="&jobAssignmentId="+jobAssignmentId;
	params+="&transactionDate="+transactionDate;
	params+="&payPolicy="+payPolicy;
	params+="&payPeriodTransactionId="+payPeriodTransactionId;

	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/ticket_allowance.action?"+params, 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#allowance-manipulation-div").html(result);  
				return false;
			}
	 });
}
function loadEmployeePolicy(){
	var jobAssignmentId=$('#jobAssignmentId').val();
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/grade_view.action", 
		 	async: false,
		 	data:{jobAssignmentId:jobAssignmentId,openPositionId:0},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#grade-div").html(result);  
				$('.discard').hide();
				return false;
			}
	 });
}
function customRanges(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function getAccessCode(id){
	var idval=id.split('_');
	var acccessCode=idval[0];
	return acccessCode;
}

function customRanges(dates) {
	var rowId=getRowId(this.id); 
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		if($(this).val()!=""){
			return false;
		} 
		else{
			return false;
		}
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null); 
		if($(this).val()!=""){
			return false;
		} 
		else{
			return false;
		}
	} 
}

function editDataSelectCall(){
	$('.rowidC').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#paymentType_"+rowId).val($("#paymentTypeId_"+rowId).val());
});
	
	
	
}

</script>

<div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Ticket
			Allowance
		</div>

		<div class="portlet-content">
			<div id="page-error" class="response-msg error ui-corner-all width90"
				style="display: none;">
				<span>Process Failure!</span>
			</div>
			<div class="tempresult" style="display: none;"></div>
			<form id="AllowanceForm" class="" name="AllowanceForm" method="post" style="position: relative;">
				<input type="hidden" name="ticketAllowanceId"
					id="ticketAllowanceId" value="${ALLOWANCE.ticketAllowanceId}"
					class="width50" />
				<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="min-height: 150px;">
							<legend>Allowance Details </legend>
							<div class="width35 float-left">
								<div>
									<label class="width30">Source<span
										style="color: red">*</span> </label>
									<div>
										<select id="source" name="source"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${PORT}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" 
											style="position: relative;"> <a
											style="cursor: pointer;" id="PORT_1"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="sourceTemp" id="sourceTemp"
											value="${ALLOWANCE.lookupDetailByFromDestination.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Destination<span
										style="color: red">*</span> </label>
									<div>
										<select id="destination" name="destination"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${PORT}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" 
											style="position: relative;"> <a
											style="cursor: pointer;" id="PORT_2"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="destinationTemp" id="destinationTemp"
											value="${ALLOWANCE.lookupDetailByToDestination.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Airline<span
										style="color: red">*</span> </label>
									<div>
										<select id="airline" name="airline"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${AIRLINE}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" 
											style="position: relative;"> <a
											style="cursor: pointer;" id="AIRLINE"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="airlineTemp" id="airlineTemp"
											value="${ALLOWANCE.lookupDetailByAirline.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30">Agency<span
										style="color: red">*</span> </label>
									<div>
										<select id="agency" name="agency"
											class="width50 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${AGENCY}" var="nltls">
												<option value="${nltls.lookupDetailId}">${nltls.displayName}
												</option>
											</c:forEach>
										</select> <span class="button" 
											style="position: relative;"> <a
											style="cursor: pointer;" id="AGENCY"
											class="btn ui-state-default ui-corner-all recruitment-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" name="agencyTemp" id="agencyTemp"
											value="${ALLOWANCE.lookupDetailByAgency.lookupDetailId}" />
									</div>
								</div>
								<div>
									<label class="width30" for="numberOfTicket">Number Of
										Ticket</label> <input type="text" name="numberOfTicket"
										id="numberOfTicket" value="${ALLOWANCE.numberOfTicket}"
										class="width50" />
								</div>
								
								<div>
									<label class="width30">Total Amount<span
										style="color: red">*</span></label> <input type="text"
										id="amount" name="amount" class="width50  validate[required]"
										value="${ALLOWANCE.amount}">
								</div>
								<div>
									<label class="width30 float-left">Voucher/Receipt Number</label> 
									<input
										type="text" id="receiptNumber" name="receiptNumber"
										class="width50 float-left" value="${ALLOWANCE.receiptNumber}">
								</div>
								<div class="float-left">
									<label class="width30">Finance Impact</label> <input
										type="checkbox" id="isFinanceImpact" name="isFinanceImpact"
										class="width10"> <input type="hidden"
										id="isFinanceImpactTemp" name="isFinanceImpactTemp"
										class="width10 float-left" value="${ALLOWANCE.isFinanceImpact}"> <label
										class="width50 float-left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
								</div>
								
							</div>
							<div class="width40 float-left">
								<div>
									<label class="width30">Travel Class</label>
									<div>
										<select id=travelClass
											name="travelClass"
											class="width50 ">
											<c:forEach items="${TRAVEL_CLASS}" var="gen">
												<option value="${gen.key}">${gen.value}
												</option>
											</c:forEach>
										</select>  <input
											type="hidden" name="travelClassTemp" id="travelClassTemp"
											value="${ALLOWANCE.travelClass}" />
									</div>
								</div>
								<div>
									<label class="width30">Ticket Type</label>
									<div>
										<select id="ticketType" name="ticketType" class="width50 ">
											<option value="ONE-WAY">ONE-WAY</option>
											<option value="RETURN">RETURN</option>
										</select> <input type="hidden" name="ticketTypeTemp"
											id="ticketTypeTemp" value="${ALLOWANCE.ticketType}" />
									</div>
								</div>
								
								<div>
									<label class="width30" for="purchaseDate">Pruchase Date<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" name="purchaseDate" id="purchaseDate"
										value="${ALLOWANCE.purchaseDateView}" class="width50" />
								</div>
								<div>
									<label class="width30" for="fromDate">From Date<span
										class="mandatory">*</span> </label> <input type="text"
										readonly="readonly" name="fromDate" id="fromDate"
										value="${ALLOWANCE.fromDateView}" class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30" for="toDate">To Date<span
										class="mandatory">*</span>
									</label> <input type="text" readonly="readonly" name="toDate"
										id="toDate" value="${ALLOWANCE.toDateView}"
										class="width50 validate[required]" />
								</div>
								<div>
									<label class="width30">Description</label> <textarea 
										id="description" name="description" class="width50 "
										>${ALLOWANCE.description}</textarea>
								</div>
							</div>
							<div class="width20 float-left">
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Pay Elment : </label>
									<label class="width40 ">${JOBPAY_INFO.elementName}</label>
									<input type="hidden" id="jobPayrollElementId" value="${JOBPAY_INFO.jobPayrollElementId}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period From : </label>
									<label class="width40 ">${JOBPAY_INFO.periodStartDate}</label>
									<input type="hidden" id="periodStartDate" value="${JOBPAY_INFO.periodStartDate}"/>
									
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Period To : </label>
									<label class="width40 ">${JOBPAY_INFO.periodEndDate}</label>
									<input type="hidden" id="periodEndDate" value="${JOBPAY_INFO.periodEndDate}"/>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 " >Provided Amount(Max) : </label>
									<label class="width40 ">${JOBPAY_INFO.allowedMaixmumAmount}</label>
								</div>
								<div class="width100 float-left viewlabel">
									<label class="width60 ">Available Balance : </label>
									<label class="width40 ">${JOBPAY_INFO.availableAmount}</label>
								</div>
							</div>
							
							<div class="clearfix"></div>
								<div class="float-right buttons ui-widget-content ui-corner-all">
									<div class="portlet-header ui-widget-header float-right discard_allowance"
										id="discard_allowance">
										<fmt:message key="common.button.cancel" />
									</div>
									<div class="portlet-header ui-widget-header float-right save"
										id="job_save">
										<fmt:message key="organization.button.save" />
									</div>
								</div>
								<div id="common-popup" class="ui-dialog-content ui-widget-content"
									style="height: auto; min-height: 48px; width: auto;">
									<div class="common-result width100"></div>
									<div
										class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
										<button type="button" class="ui-state-default ui-corner-all">Ok</button>
										<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
									</div>
								</div>
						</fieldset>
						
					</div>
					
				</div>
			</form>
			<div class="width100 float-left" id="grade-div">
			</div>
		</div>
		

	</div>

</div>


