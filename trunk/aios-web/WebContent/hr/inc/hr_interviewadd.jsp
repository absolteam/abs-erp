<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count {
	padding: 0 !important;
}
</style>

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 

		var interviewId=Number($("#interviewId").val());
		if(interviewId>0){
			autoSelectTheMultiList("statusTemp","status");
			autoSelectTheMultiList("roomTemp","room");
			autoSelectTheMultiList("roundTemp","round");

			autoSelectTheMultiList("candidates","candidateList");
			autoSelectTheMultiList("interviewers","interviewerList");
		}else{
			
		}
		
		  $('#OPEN_POSITION').click(function(){
		         $('.ui-dialog-titlebar').remove(); 
		         $('#common-popup').dialog('open');
		            $.ajax({
		               type:"POST",
		               url:"<%=request.getContextPath()%>/common_open_position_list.action",
		               async: false,
		               dataType: "html",
		               cache: false,
		               success:function(result){
		                    $('.common-result').html(result);
		                    return false;
		               },
		               error:function(result){
		                    $('.common-result').html(result);
		               }
		           });
		            return false;
		   	}); 
		  
		  $('#QUESTION').click(function(){
		         $('.ui-dialog-titlebar').remove(); 
		         $('#common-popup').dialog('open');
		            $.ajax({
		               type:"POST",
		               url:"<%=request.getContextPath()%>/common_question_bank_list.action",
		               async: false,
		               dataType: "html",
		               cache: false,
		               success:function(result){
		                    $('.common-result').html(result);
		                    return false;
		               },
		               error:function(result){
		                    $('.common-result').html(result);
		               }
		           });
		            return false;
		   	}); 
		  
		  
		  
		
		    $('.location-popup').live('click',function(){
		    	$('.error,.success').hide();
		    	 var companyId=0;
		    	
		        $('.ui-dialog-titlebar').remove();  
		        $('#job-common-popup').dialog('open');
		        var rowId=0;
		           $.ajax({
		              type:"POST",
		              url:"<%=request.getContextPath()%>/get_location_for_setup.action",
		              data:{id:rowId,companyId:companyId},
		              async: false,
		              dataType: "html",
		              cache: false,
		              success:function(result){
		                   $('.job-common-result').html(result);
		                   return false;
		              },
		              error:function(result){
		                   $('.job-common-result').html(result);
		              }
		          });
		           return false;
		  	});
	   
	   
	   $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	  

		
		$jquery("#interview-details").validationEngine('attach');
			
		$('#interview-discard').click(function(){ 
			returnCallTOList();
			
		 });
		

	 $('#interview-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
		 	
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(interviewId>0)
						successMessage="Successfully Modified";

					try {
						if ($jquery("#interview-details").validationEngine('validate')) {
							generateDataToSendToDB("candidateList","candidates");
							generateDataToSendToDB("interviewerList","interviewers");
						
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/interview_save.action", 
							 	async: false,
							 	data: $("#interview-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					} catch (e) {

					}

				});
	 
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="INTERVIEW_HALL"){
				$('#room').html("");
				$('#room').append("<option value=''>--Select--</option>");
				loadLookupList("room");
				
			}
			
		});
		
	 	

		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
		
		 $('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:550,
				bgiframe: false,
				modal: true 
			});
		
			
		 $('#createdDate').datepick();
		 $('#interviewDate').datepick();
		 $('#interviewTime').timepicker({});
		 
		 $("#candidateList,#interviewerList").multiselect();
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/interview_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}

	function generateDataToSendToDB(source, target) {
		var typeList = new Array();
		var ids = "";
		$('#' + source + ' option:selected').each(function() {
			typeList.push($(this).val());
		});
		for ( var i = 0; i < typeList.length; i++) {
			if (i == (typeList.length) - 1)
				ids += typeList[i];
			else
				ids += typeList[i] + ",";
		}

		$('#' + target).val(ids);
	}

	function autoSelectTheMultiList(source, target) {
		var idsArray = new Array();
		var idlist = $('#' + source).val();
		idsArray = idlist.split(',');
		$('#' + target + ' option').each(function() {
			var txt = $(this).val().trim();
			if ($.inArray(txt, idsArray) >= 0)
				$(this).attr("selected", "selected");
		});

	}
	function questionPopupResult(id, type, questionBankVO) {
		$('#questionBankId').val(id);
		$('#questionType').val(questionBankVO.questionType);
		$('#questionPattern').val(questionBankVO.questionPatternView);

	}

	function locationPopupResult(id, type, commaseparatedValue) {
		$('#locationId').val(id);
		var valueArray = new Array();
		valueArray = commaseparatedValue.split('@&');
		var locationVO = valueArray[0];
		$('#locationName').val(locationVO.locationName);
		$('#fullAddress').val(locationVO.fullAddress);

	}

	function locationGenericResultCall(locationId, locationName,
			commaSeparatedParam) {
		var valueArray = new Array();
		valueArray = commaSeparatedParam.split('@&');
		$("#locationName").val(locationName);
		$("#locationId").val(locationId);
	}

	function selectedOpenPositionResult(openPositionVO) {
		$("#openPositionId").val(openPositionVO.openPositionId);
		$("#positionReferenceNumber").val(
				openPositionVO.positionReferenceNumber);
		$("#numberOfPosition").val(openPositionVO.numberOfPosition);
		$("#job").val(openPositionVO.designationName);

	}
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Interview
			Details
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="interview-details" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden" name="interview.interviewId"
									id="interviewId" value="${INTERVIEW.interviewId}"
									class="width50 " />
								<fieldset style="min-height: 200px;">
									<legend>Schedule Information</legend>
									<div class="width50 float-left">
										<div>
											<label class="width30">Position Reference<span
												style="color: red">*</span> </label> <input type="hidden"
												name="interview.openPosition.openPositionId"
												id="openPositionId"
												value="${INTERVIEW.openPosition.openPositionId}" />
											<div>
												<input type="text" id="positionReferenceNumber"
													readonly="readonly" class="width50 validate[required]"
													value="${INTERVIEW.openPosition.positionReferenceNumber}" />
												<span class="button" id="OPEN_POSITION_1"
													style="position: relative;"> <a
													style="cursor: pointer;" id="OPEN_POSITION"
													class="btn ui-state-default ui-corner-all width10"> <span
														class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</div>

										</div>
										<div>
											<label class="width30">Question<span
												style="color: red">*</span> </label> <input type="hidden"
												name="interview.questionBank.questionBankId"
												id="questionBankId"
												value="${INTERVIEW.questionBank.questionBankId}" />
											<div>
												<input type="text" id="questionType" readonly="readonly"
													class="width50 validate[required]"
													value="${INTERVIEW.questionBank.questionType}" /> <span
													class="button" id="QUESTION_1" style="position: relative;">
													<a style="cursor: pointer;" id="QUESTION"
													class="btn ui-state-default ui-corner-all width10"> <span
														class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</div>

										</div>
										<div>
											<label class="width30">Question Pattern </label> <input
												type="text" id="questionPattern" class="width50"
												name="questionPattern"
												value="${INTERVIEW.questionPatternView}" />
										</div>

										<div>
											<label class="width30">Interview Spot<span
												style="color: red">*</span></label> <input
												type="text" id="locationName" class="width50 validate[required]"
												name="questionPattern"
												value="${INTERVIEW.location.locationName}" /> <span
												class="button" id="location" style="position: relative;">
												<a style="cursor: pointer;" id="locationpopup"
												class="btn ui-state-default ui-corner-all width10 location-popup">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" name="interview.location.locationId" id="locationId"
												class="locationId" value="${INTERVIEW.location.locationId}" />
										</div>

										<div>
											<label class="width30">Room/Hall<span
												style="color: red">*</span> </label>
											<div>
												<select id="room"
													name="interview.lookupDetail.lookupDetailId"
													style="width: 51%" class="validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${INTERVIEW_HALL}" var="nltlst">
														<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
														</option>
													</c:forEach>
												</select> <span class="button" id="INTERVIEW_HALL_1"
													style="position: relative;"> <a
													style="cursor: pointer;" id="INTERVIEW_HALL"
													class="btn ui-state-default ui-corner-all recruitment-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
													type="hidden" name="roomTemp" id="roomTemp"
													value="${INTERVIEW.lookupDetail.lookupDetailId}" />
											</div>
										</div>

									</div>
									<div class="width50 float-left">

										<div>
											<label class="width30">Round<span style="color: red">*</span>
											</label>
											<div>
												<select id=round name="interview.round"
													class="width51 validate[required]">
													<c:forEach items="${ROUNDS}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="roundTemp" id="roundTemp"
													value="${INTERVIEW.round}" />
											</div>
										</div>
										
										<div>
											<label class="width30">Interview Date<span style="color: red">*</span></label> <input
												type="text" class="width50 validate[required]" readonly="readonly"
												name="interviewDate" id="interviewDate"
												value="${INTERVIEW.interviewDateView}" />
										</div>
										<div>
											<label class="width30">Interview Time<span style="color: red">*</span></label> <input
												type="text" class="width50 validate[required]" 
												name="interviewTime" id="interviewTime"
												value="${INTERVIEW.interviewTimeView}" />
										</div>
										<div>
											<label class="width30">Status<span style="color: red">*</span>
											</label>
											<div>
												<select id=status name="interview.status"
													class="width51 validate[required]">
													<c:forEach items="${STATUS}" var="gen">
														<option value="${gen.key}">${gen.value}</option>
													</c:forEach>
												</select> <input type="hidden" name="statusTemp" id="statusTemp"
													value="${INTERVIEW.status}" />
											</div>
										</div>
										<div>
											<label class="width30">Created Date </label> <input
												type="text" class="width50 createdDate" readonly="readonly"
												name="createdDate" id="createdDate"
												value="${INTERVIEW.createdDateView}" />
										</div>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="float-left width50">
								<fieldset>
									<legend>Candidates</legend>
									<input type="hidden" name="candidates" id="candidates"
										value="${INTERVIEW.candidatesArray}" />

									<div>
										<select id="candidateList" name="candidateList" multiple
											class="width60">
											<c:forEach items="${CANDIDATES}" var="nltls">
												<option value="${nltls.candidateId}">${nltls.personVO.personName}[${nltls.personVO.personNumber}]
												</option>
											</c:forEach>
										</select>
									</div>
								</fieldset>
							</div>
							<div id="hrm" class="float-left width50">
								<fieldset>
									<legend>Interviewers</legend>
									<input type="hidden" name="interview.interviewersGroup" id="interviewers"
										value="${INTERVIEW.interviewersGroup}" />
									<div>
										<select id="interviewerList" name="interviewerList" multiple
											class="width60">
											<c:forEach items="${INTERVIWERS}" var="nltls">
												<option value="${nltls.jobAssignmentId}">${nltls.personName}-${nltls.designationName}
												</option>
											</c:forEach>
										</select>
									</div>
								</fieldset>
							</div>

						</div>

					</form>
					<div class="clearfix"></div>
					<div id="interview_process_div"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">

						<div
							class="portlet-header ui-widget-header float-right interview-discard"
							id="interview-discard">cancel</div>
						<div
							class="portlet-header ui-widget-header float-right interview-save"
							id="interview-save">save</div>
					</div>
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup" class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="job-common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="job-common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>