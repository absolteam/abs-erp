<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var employeeName="";var personId=0;var designationName="";
var gradeName="";var companyName="";var departmentName="";
var locationName="";var swipeId="";
$(function(){ 
	$('#Employee').dataTable({ 
		"sAjaxSource": "attendance_employee_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'RecordId', "bVisible": false},
			{ "sTitle": 'PersonId', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Designation'},
			{ "sTitle": 'Grade'},
			{ "sTitle": 'Company'},
			{ "sTitle": 'Department'},
			{ "sTitle": 'Location'},
			{ "sTitle": 'Swipe Id'},
			{ "sTitle": 'JobId', "bVisible": false},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Employee').dataTable();

	/* Click event handler */
	$('#Employee tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	        personId=aData[1];
	        employeeName=aData[2];
	        designationName=aData[3];
	        gradeName=aData[4];
	        companyName=aData[5];
	        departmentName=aData[6];
	        locationName=aData[7];
	        swipeId=aData[8];
	        
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	        personId=aData[1];
	        employeeName=aData[2];
	        designationName=aData[3];
	        gradeName=aData[4];
	        companyName=aData[5];
	        departmentName=aData[6];
	        locationName=aData[7];
	        swipeId=aData[8];
	    }
	});
	$('#Employee tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	        personId=aData[1];
	        employeeName=aData[2];
	        designationName=aData[3];
	        gradeName=aData[4];
	        companyName=aData[5];
	        departmentName=aData[6];
	        locationName=aData[7];
	        swipeId=aData[8];
	        jobId=aData[9];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	        personId=aData[1];
	        employeeName=aData[2];
	        designationName=aData[3];
	        gradeName=aData[4];
	        companyName=aData[5];
	        departmentName=aData[6];
	        locationName=aData[7];
	        swipeId=aData[8];
	    }
		$("#jobAssignmentId").val(jobAssignmentId);
		$("#personId").val(personId);
		$("#employeeName").val(employeeName);
		$("#designationName").val(designationName);
		$("#gradeName").val(gradeName);
		$("#companyName").val(companyName);
		$("#departmentName").val(departmentName);
		$("#locationName").val(locationName);
		$("#swipeId").val(swipeId);
		
		$('#job-common-popup').dialog("close");
	});
});
</script>
<div id="main-content"> 
 	<div id="trans_combination_accounts">
		<table class="display" id="Employee"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="employee-list-close">Close</div>
		</div>
	</div> 
</div>