<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">

</style> 
<script type="text/javascript">
var isServiceCountTemp="";
var fullPaidDaysTemp="";
var halfPaidDaysTemp="";
var carryForwardCountTemp="";
var yearlyDaysTemp="";
var monthlyDaysTemp="";
$(function (){  
	var leaveId=Number($('#leaveId').val());
	if(leaveId>0){
		fullPaidDaysTemp=$('#fullPaidDays').val();
		halfPaidDaysTemp=$('#halfPaidDays').val();
		isServiceCountTemp=$('#isServiceCount').val();
		carryForwardCountTemp=$('#carryForwardCount').val();
	
		
		
		$('#leaveType').val($('#leaveTypeTemp').val());
		
		if($('#isMonthlyTemp').val().trim()=='true'){
			$('#isMonthly').attr("checked","checked");
			$('#isWeekly').attr("checked",false);
			$('#isYearly').attr("checked",false);
		}
		if($('#isYearlyTemp').val().trim()=='true'){
			$('#isMonthly').attr("checked",false);
			$('#isYearly').attr("checked","checked");
			$('#isWeekly').attr("checked",false);
		}
		
		$('#isYearly').trigger('change');
		$('#isService').trigger('change');

		if($('#isServiceTemp').val().trim()=='true'){
			$('#isService').attr("checked","checked");
			$('#YEARLY_CARRY_FORWARD').hide();
			$('#isWeekly').attr("checked",false);
		}
			
		
		if($('#isPaidTemp').val().trim()=='true'){
			$('#isPaid').attr("checked","checked");
			$('#fullPaidDays').val(fullPaidDaysTemp);
			$('#halfPaidDays').val(halfPaidDaysTemp);
			$('#PAID-DIV').show();
		}else{
			$('#PAID-DIV').hide();
			$('#fullPaidDays').val("");
			$('#halfPaidDays').val("");
		}
		
		
		if($('#isWeeklyTemp').val().trim()=='true'){
			$('#isWeekly').attr("checked","checked");
			
			$('#isMonthly').attr("checked",false);
			$('#isYearly').attr("checked",false);
		}

		if($('#isActiveTemp').val().trim()=='true')
			$('#isActive').attr("checked","checked");
		
		if($('#canEncashTemp').val().trim()=='true')
			$('#canEncash').attr("checked","checked");
	}
	
	$jquery("#ASSET_USAGE").validationEngine('attach');
	
	$('#isService').change(function() { 
		if($('#isService').attr("checked")){
			$('#isServiceCount').show();
			$('#isServiceCount').val(isServiceCountTemp);
			
			
			$('#CARRY_FORWORD_DIV').hide();
			$('#carryForwardCount').val("");
			$('#YEARLY_CARRY_FORWARD').hide();
		}else{
			
			$('#isServiceCount').hide();
			$('#isServiceCount').val("");
			
			
			$('#CARRY_FORWORD_DIV').show();
			$('#carryForwardCount').val(carryForwardCountTemp);
			$('#YEARLY_CARRY_FORWARD').show();
		}
	});
	
	$('#isPaid').change(function() { 
		if($('#isPaid').attr("checked")){
			$('#fullPaidDays').val(fullPaidDaysTemp);
			$('#halfPaidDays').val(halfPaidDaysTemp);
			$('#PAID-DIV').show();
		}else{
			$('#PAID-DIV').hide();
			$('#fullPaidDays').val("");
			$('#halfPaidDays').val("");
		}
	});
	
		
	
	
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#ASSET_USAGE").validationEngine('validate')){ 
			if(!$('#isWeekly').attr("checked") && !$('#isMonthly').attr("checked") && !$('#isYearly').attr("checked") && !$('#isService').attr("checked")){
				$('.error').hide().html("Either Monthly,Yearly,Weekly or Service should be selected.").slideDown();
				return false;
			}
				
				
			var leaveId=Number($('#leaveId').val());
			var displayName=$('#displayName').val();
			var leaveType=$('#leaveType').val();
			var isMonthly=$('#isMonthly').attr("checked");
			var isYearly=$('#isYearly').attr("checked");
			var isWeekly=$('#isWeekly').attr("checked");
			var isService=$('#isService').attr("checked");
			
			
			var isServiceCount=$('#isServiceCount').val();
			var defaultDays=$('#defaultDays').val();
			var isPaid=$('#isPaid').attr("checked");
			var fullPaidDays=$('#fullPaidDays').val();
			var halfPaidDays=$('#halfPaidDays').val();
			var carryForwardCount=$('#carryForwardCount').val();
			var canEncash=$('#canEncash').attr("checked");
			var isActive=$('#isActive').attr("checked");
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(leaveId>0)
				successMessage="Successfully Modified";
			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/leave_save.action",
				data: {
					leaveId:leaveId,
					displayName:displayName,
					leaveType:leaveType,
					isMonthly:isMonthly,
					isYearly:isYearly,
					isService:isService,
					isWeekly:isWeekly,
					isServiceCount:isServiceCount,
					defaultDays:defaultDays,
					isPaid:isPaid,
					fullPaidDays:fullPaidDays,
					halfPaidDays:halfPaidDays,
					carryForwardCount:carryForwardCount,
					canEncash:canEncash,
					isActive:isActive,
					
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		returnCallTOList();
		$('#loading').fadeOut();
	});
   
   
	$('#isYearly').trigger('change');
	$('#isService').trigger('change');

});
function returnCallTOList(){
	$.ajax({
	type:"POST",
	url:"<%=request.getContextPath()%>/leave_list.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){ 
			$('#codecombination-popup').dialog('destroy');
			$('#codecombination-popup').remove();	
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();	
			$("#main-wrapper").html(result); 
		}
 	});
}



</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<c:choose>
				<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
									&& COMMENT_IFNO.commentId gt 0}">
					<div class="width85 comment-style" id="hrm">
						<fieldset>
							<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
							<label class="width70">${COMMENT_IFNO.comment}</label>
						</fieldset>
					</div> 
				</c:when>
			</c:choose> 
			<form id="ASSET_USAGE" name="ASSET_USAGE" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" name="leaveId" id="leaveId" value="${LEAVE_INFO.leaveId}"/>
					<fieldset style="height:160px;">
						   <div>
			                  <label class="width30">Display Name<span style="color:red">*</span></label>
			                  <input type="text" class="width50 validate[required]" name="displayName" id="displayName" value="${LEAVE_INFO.displayName}"/>
					       </div>
				      	 <div>
		                 	<label class="width30">Leave Type<span style="color:red">*</span></label>
			                 <input type="hidden" id="leaveTypeTemp" value="${LEAVE_INFO.leaveCode}">
			                 <select name="leaveType" class="validate[required]" id="leaveType" style="width: 51.5%;">
			                 	<c:if test="${requestScope.LEAVE_TYPES!=null}">
									<c:forEach items="${requestScope.LEAVE_TYPES}" var="nltlst">
										<option value="${nltlst.accessCode}">${nltlst.displayName} </option>
									</c:forEach>
								</c:if>	
			                 </select>
			             </div>
			             <div>
			                  <label class="width30">Number Of Days<span style="color:red">*</span></label>
			                  <input type="text" class="width50 validate[required,custom[number]]" name="defaultDays" id="defaultDays" value="${LEAVE_INFO.defaultDays}"/>
					       </div>
			             
				      	  <div id="MONTHLY_DIV">
		                 	<label class="width30">Monthly</label>
		                 	<input type="radio" name="periodType" id="isMonthly" checked="checked">
		               	 	 <input type="hidden" id="isMonthlyTemp" class="width10" value="${LEAVE_INFO.isMonthly}">
		               	 	 
		           		 </div>
					     <div id="YEARLY_DIV">
		                 	<label class="width30">Yearly</label>
		                 	<input type="radio" name="periodType" id="isYearly" checked="checked">
		                	 <input type="hidden" id="isYearlyTemp" class="width10" value="${LEAVE_INFO.isYearly}">
		                	 
		            	</div>
		            	 <div id="WEEKLY_DIV">
		                 	<label class="width30">Weekly</label>
		                 	<input type="radio" name="periodType" id="isWeekly" checked="checked">
		               	 	 <input type="hidden" id="isWeeklyTemp" class="width10" value="${LEAVE_INFO.isWeekly}">
		           		 </div>
				        <div>
		                 	<label class="width30">In Whole Service</label>
		                 	<input type="radio" id="isService" name="periodType">
		                	<input type="hidden" id="isServiceTemp" class="width10" value="${LEAVE_INFO.isService}">
		                	<input type="text" id="isServiceCount" class="width20" value="${LEAVE_INFO.isServiceCount}" style="display:none;">
		            	</div>
		            	
			       	 	
				 </fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="height:160px;">
					<div>
	                 	<label class="width30">Paid</label>
	                 	<input type="checkbox" id="isPaid">
	               	 	 <input type="hidden" id="isPaidTemp" class="width10" value="${LEAVE_INFO.isPaid}">
	           		 </div>
	           		 <div id="PAID-DIV" style="display: none;">
					     <div>
		                 	<label class="width30">Full Paid Days</label>
		                 	<input type="text" id="fullPaidDays" class="width30 validate[optional,custom[number]]" value="${LEAVE_INFO.fullPaidDays}">
		            	</div>
		            	<div>
		                 	<label class="width30">Half Paid Days</label>
		                 	<input type="text" id="halfPaidDays" class="width30 validate[optional,custom[number]]" value="${LEAVE_INFO.halfPaidDays}">
		            	</div>
		            </div>
		          
		       	 	<div id="CARRY_FORWORD_DIV">
		       	 		<label class="width30">Count of carry forward</label>
		       	 		<input type="text" id="carryForwardCount" class="width20 validate[optional,custom[onlyNumber]]" value="${LEAVE_INFO.carryForwardCount}">
		       	 	</div>
		            <div>
	                 	<label class="width30">Encash</label>
	                 	<input type="checkbox" id="canEncash">
	               	 	 <input type="hidden" id="canEncashTemp" class="width10" value="${LEAVE_INFO.canEncash}">
	           		 </div>
	           		 <div>
	                 	<label class="width30">Active</label>
	                 	<input type="checkbox" id="isActive">
	               	 	 <input type="hidden" id=isActiveTemp class="width10" value="${LEAVE_INFO.isActive}">
	           		 </div>
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>