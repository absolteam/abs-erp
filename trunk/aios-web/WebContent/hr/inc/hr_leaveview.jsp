<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<style>
.link-border{
 	border:2px solid #c0c0c0;
    background:#f5f5f5;
    padding:4px;
    position:relative;/* ie6 border fix*/
    top:4px;
    cursor: pointer;
    font-weight: bold;
}
</style>
<script type="text/javascript">
$(document).ready(function(){  
	var leaveProcessId=Number($('#leaveProcessId').val());
	populateUploadsPane("doc","leaveDocs","LeaveProcess",leaveProcessId);
	$('#halfDay').change(function(){
	   	 dateOnchangeCall();
	    });
	setTimeout(function(){
		dateOnchangeCall();
	 },50); 
	//Save & discard process
	$('.save').click(function(){ 
		$('#page-error').hide();
		if($("#LEAVEDET").validationEngine({returnIsValid:true})){  
			loanLineDetail=""; loanChargesDetail="";
			var personId=Number($('#personId').val());
			var jobAssignmentId=Number($('#jobAssignmentId').val());
			var leaveProcessId=Number($('#leaveProcessId').val());
			var jobLeaveId=Number($('#leaveType').val());
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var reason=$('#reason').val();
			var contactOnLeave=$('#contactOnLeave').val();
			var halfDay=$('#halfDay').attr('checked');
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/apply_leave_save_by_hr.action", 
			 	async: false, 
			 	data:{
			 		personId:personId,
			 		jobAssignmentId:jobAssignmentId,
			 		leaveProcessId:leaveProcessId,jobLeaveId:jobLeaveId,
			 		fromDate:fromDate,toDate:toDate,reason:reason,
			 		contactOnLeave:contactOnLeave,halfDay:halfDay
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"GET",
								url:"<%=request.getContextPath()%>/employee_leave_list.action",
								async:false,
								dataType:"html",
								cache:false,
								success:function(result){
									$("#main-wrapper").html(result);
								}
							});	
					 }else{
						 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').click(function(){
		$.ajax({
			type:"GET",
			url:"<%=request.getContextPath()%>/applay_leave.action",
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});	
	});
	
	$('.reset').click(function(){
		clear_form_elements();
	});
	
     //Date process
     $('#fromDate,#toDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   
	
	
	
	 //pop-up config
	   $('.employee-popup').live('click',function(){
	         $('.ui-dialog-titlebar').remove();  
	         $('#job-common-popup').dialog('open');
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/get_employee_for_leavemanagment.action",
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.job-common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.job-common-result').html(result);
	               }
	           });
	            return false;
	   }); 

	   $('#job-common-popup').dialog({
	   	autoOpen: false,
	   	minwidth: 'auto',
	   	width:800,
	   	height:450,
	   	bgiframe: false,
	   	modal: true 
	   });

});
function clear_form_elements() {
	$('#page-error').hide();
	$('#employeeName').html("");
	$('#LEAVEDET').each(function(){
	    this.reset();
	});
}
function leaveTypeCall(personId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getLeaveTypes.action", 
	 	async: false, 
	 	data:{ 
	 		personId:personId
	 	},
	    dataType: "html",
	    cache: false,
		success:function(result){
			 $("#leaveTypeDiv").html(result);
		},
		error:function(result){  
			$("#leaveTypeDiv").html(result);
		}
	});  
}
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		dateOnchangeCall();
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
		dateOnchangeCall();
	} 
}
function dateOnchangeCall(){
	if($('#toDate').val()!=null && $('#toDate').val()!="" && $('#fromDate').val()!=null && $('#fromDate').val()!=""){ 
		daysDiff();
		if($('#tempDays').val()=="NaN" && " " && ""){
			$('#tempDays').val("0");
		}
		if($('#halfDay').attr('checked'))
			$('#tempDays').val(Number($('#tempDays').val())+0.5);
		else
			$('#tempDays').val(Number($('#tempDays').val())+1);	
			
	}
	return true;
}
var fromFormat='dd-MMM-yyyy';
var toFormat='MM/dd/y';
function daysDiff() {
	var tempdate1 = formatDate(new Date(getDateFromFormat($("#fromDate").val(),fromFormat)),toFormat);
	var tempdate2 = formatDate(new Date(getDateFromFormat($("#toDate").val(),fromFormat)),toFormat);
	var date1=new Date(tempdate1);
	var date2=new Date(tempdate2);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	 $('#tempDays').val(diffDays);
}
</script>
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>View Leave</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="LEAVEDET" class="" name="LEAVEDET" method="post">
		  	<div class="width100 float-left" id="hrm"> 
					<div class="width100 float-left" id="LeaveInfoContent">
						<fieldset style="height:220px;">
							<legend>Leave Information</legend>
							<div class="width45 float-left">
								<input type="hidden" name="leaveProcessId" id="leaveProcessId" value="${LEAVE_PROCESS.leaveProcessId}" class="width50"/>
								<div class="float-left width90">
								 	<label class="width30" >Employee</label>
									<span id="employeeName" class="float-left width50">
									${LEAVE_PROCESS.jobAssignment.person.firstName} ${LEAVE_PROCESS.jobAssignment.person.lastName}
									</span>
									<span class="button float-right" id="employee"  style="position: relative; top:4px;display:none;">
										<a style="cursor: pointer;" id="employeepopup" class="btn ui-state-default ui-corner-all width100 employee-popup"> 
											<span class="ui-icon ui-icon-newwin">
											</span> 
										</a>
									</span> 
									<input type="hidden" id="personId" class="personId  validate[required]" value="${LEAVE_PROCESS.person.personId}" />
									<input type="hidden" id="jobAssignmentId" value="${LEAVE_PROCESS.jobAssignment.jobAssignmentId}" class="jobAssignmentId  validate[required]" />
								</div>
								<div id="leaveTypeDiv">
								<label class="width30" for="leaveType">Leave Type<span class="mandatory">*</span></label>
								<select id="leaveType" name="leaveType" disabled="disabled" class="width50 validate[required]">
									<option value="">--Select--</option>
									<c:forEach items="${JOB_LEAVE_LIST}" var="jobleave" varStatus="status">
									<c:choose>
										<c:when test="${LEAVE_PROCESS.jobLeave.jobLeaveId eq jobleave.jobLeaveId}">
											<option value="${jobleave.jobLeaveId}" selected="selected">${jobleave.leave.lookupDetail.displayName}</option>
										</c:when>
										<c:otherwise>
											<option value="${jobleave.jobLeaveId}">${jobleave.leave.lookupDetail.displayName}</option>
										</c:otherwise>
									</c:choose>
									</c:forEach>
									
								</select>
							</div>   
							<div>
								<label class="width30" for="fromDate">Leave Start Date<span class="mandatory">*</span></label>
								<c:choose>
									<c:when test="${LEAVE_PROCESS.fromDate ne null &&  LEAVE_PROCESS.fromDate  ne ''}">
										<c:set var="fromDate" value="${LEAVE_PROCESS.fromDate}"/>  
										<%String fromDate = DateFormat.convertDateToString(pageContext.getAttribute("fromDate").toString());%>
										<input type="text" readonly="readonly" disabled="disabled" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]" 
										value="<%=fromDate%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" disabled="disabled" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div>   
							<div>
								<label class="width30" for="toDate">Leave End Date<span class="mandatory">*</span></label>
								<c:choose>
									<c:when test="${LEAVE_PROCESS.toDate ne null &&  LEAVE_PROCESS.toDate ne ''}">
										<c:set var="toDate" value="${LEAVE_PROCESS.toDate}"/>  
										<%String toDate = DateFormat.convertDateToString(pageContext.getAttribute("toDate").toString());%>
										<input type="text" readonly="readonly" disabled="disabled" name="toDate" id="toDate" tabindex="3" class="width50 validate[required]" 
										value="<%=toDate%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" disabled="disabled" name="toDate" id="toDate" tabindex="4" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div>
								<label class="float-left width30" for="leaveType">Half Day?</label>
								<c:choose>
									<c:when test="${LEAVE_PROCESS.halfDay eq true}">
									<input type="checkbox" name="halfDay" disabled="disabled" id="halfDay" checked="checked" class="float-left width5" >
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="halfDay" disabled="disabled" id="halfDay" class="float-left width5" >
									</c:otherwise>
								</c:choose>
								<label class="float-left width20" for="leaveType">No. Of Days :</label>
								<input type="text" id="tempDays" class="float-left width10" readonly="readonly"/>
								<input type="hidden" id="tempYears"/><input type="hidden" id="tempMonths"/>
							</div>
						</div>
						<div class="width45 float-left">
							<div>
								<label class="width20" for="reason">Reason</label>
								<textarea name="reason" id="reason" disabled="disabled" class="width70" >${LEAVE_PROCESS.reason}</textarea>
							</div>
							<div>
								<label class="width20" for="contactOnLeave">Contact On Leave</label>
								<textarea name="contactOnLeave" disabled="disabled" id="contactOnLeave" class="width70" >${LEAVE_PROCESS.contactOnLeave}</textarea>
							</div>
						</div>
						<c:if test="${COMMENT_IFNO ne null}">
							<div class="width100 float-left">
								<div class="width45 float-left">	
									<div>
										<label class="width30" style="color: red;">Comment / Remarks :</label>
										<label class="width50">${COMMENT_IFNO.comment}</label>
									</div>
									<%-- <div>
										<label class="width30" >Commented By :</label>
										<label class="width50">${COMMENT_IFNO.person.firstName} ${COMMENT_IFNO.person.lastName}</label>
									</div> --%>
									
								</div>
							</div>
						</c:if>
						<div class="width45 float-left">
						 <fieldset>
				    	 	<div  style="height: 50px;overflow-y:auto;">
							  	<div class="float-left">
									<span id="leave_document_information" style="cursor: pointer; color: blue;">
										<u><fmt:message key="hr.company.documentuplode"/></u>
									</span>
								</div>
								<div class="width80 float-right">
									<span id="leaveDocs"></span>
								</div>
							</div>
					     </fieldset>
					    </div>
						<div class="clearfix"></div>
						<div class="float-left buttons ui-widget-content ui-corner-all">
							<span class="button float-right link-border" id="leave_history_view">View History</span>
							<span class="button float-right link-border" id="leave_history_print">Print History</span>
						</div>
						<div class="float-right buttons ui-widget-content ui-corner-all">
							<div class="portlet-header ui-widget-header float-right discard" id="discard">Close</div>
						</div>
					</fieldset>
 				</div> 
 			</div>
 		</form>
		</div>
	</div>	
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="job-common-result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>