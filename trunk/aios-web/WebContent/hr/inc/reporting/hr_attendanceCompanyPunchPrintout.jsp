<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Attendance Punch</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Attendance Punch</span></div>
	<div class="width70 float-left"><span class="side-heading">Filtered By</span></div>
	<div class="width30 float-right"><span class="side-heading">Printed on :${ATTENDANCE_MASTER.createdDate}</span></div>
	<div class="width100 float-left">
			<div class="data-container width40 float-left">	
				<div>
					<label class="caption width30" >Date From  :</label>
					<label class="data width20">${ATTENDANCE_MASTER.fromDate}</label>
					<label class="caption width30" >UpTo  :</label>
					<label class="data width20">${ATTENDANCE_MASTER.toDate}</label>
				</div>
			</div>
		</div>
		
		
	<!-- <div class="width100 float-left"><span class="side-heading">Punch Information</span></div> -->
	
	
	<c:forEach var="EMP" items="${ATTENDANCE_MASTER.attendanceHistoryList}">
	
	&nbsp;
		&nbsp;
		<br/>
		&nbsp;
		&nbsp;
		<br/>
		&nbsp;
	<div class="width100 float-left">
		<div class="data-container width40 float-left">	
				
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.employee"/>  :</label>
					<label class="data width50">${EMP.personName}</label>
				</div>
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.company"/>  :</label>
					<label class="data width50">${EMP.companyName}</label>
				</div>
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.department"/>  :</label>
					<label class="data width50">${EMP.departmentName}</label>
				</div>
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.location"/> :</label>
					<label class="data width50">${EMP.locationName}</label>
				</div>
			</div>
			<div class="data-container width40 float-left">	
				<%-- <div>
					<label class="data width20">${EMP.fromDate} :</label>
					<label class="caption width30" ><fmt:message key="hr.report.historyfrom"/></label>
				</div>
				<div>
					<label class="data width20">${EMP.toDate} :</label>
					<label class="caption width30" ><fmt:message key="hr.report.upto"/> </label>
				</div> --%>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.lateindays"/> :</label>
					<label class="data width50">${EMP.lateInDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.lateoutdays"/> :</label>
					<label class="data width50">${EMP.lateOutDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.earlyindays"/> :</label>
					<label class="data width50">${EMP.earlyInDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.earlyoutdays"/> :</label>
					<label class="data width50">${EMP.earlyOutDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.timelossdays"/> :</label>
					<label class="data width50">${EMP.timeLossDyas}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.timeexcessdays"/> :</label>
					<label class="data width50">${EMP.timeExcessDays}</label>
				</div>
				<%-- <div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.numberofdays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.numberOfDays}</label>
				</div> --%>
				<div class="width40 float-left">
					<label class="caption width30" style="color:red;">Absent Days :</label>
					<label class="data width50" style="color:red;">${EMP.numberOfDays}</label>
				</div>
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading"><fmt:message key="hr.attendance.punchinformation"/></span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(EMP.attendanceHistoryList)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th><fmt:message key="hr.attendance.attendanceday"/></th>
						<th><fmt:message key="hr.attendance.timein"/></th>
						<th><fmt:message key="hr.attendance.timout"/></th>
						<th><fmt:message key="hr.attendance.intype"/></th>
						<th><fmt:message key="hr.attendance.outtype"/></th>
						<th><fmt:message key="hr.attendance.timelossorexcess"/></th>
						<th><fmt:message key="hr.attendance.decision"/></th>
						<th><fmt:message key="hr.attendance.totalhours"/></th>
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${EMP.attendanceHistoryList}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.attedanceDate}</td>
							<td>${result3.inTime}</td>
							<td>${result3.outTime}</td>
							<td>${result3.inPart}</td>
							<td>${result3.outPart}</td>
							<td>${result3.timeLossOrExcess}</td>
							<td>${result3.decision}</td>
							<td>${result3.totalHours }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="width20 float-right" style="padding:5px;border-bottom: 1px solid #755800;">
				<label class="caption width15 float-left" ><fmt:message key="hr.attendance.totalhours"/>: </label>
				<label class="data width60 float-left">${EMP.totalHours}</label>
				
			</div>
			
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid"><fmt:message key="hr.attendance.norecord"/></tr>
		  </c:otherwise>
		  </c:choose>
		  	
		</div>	
	
	
	</c:forEach>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<%-- <div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(ATTENDANCE_MASTER.attendanceHistoryList)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Employee</th>
						<th>Designation</th>
						<th>Location</th>
						<th>Date</th>
						<th>In Time</th>
						<th>Out Time</th>
						<th>In(Early/Late)</th>
						<th>Out(Early/Late)</th>
						<th>Time (Loss/Excess)</th>
						<th>HR Decision</th>
						<th>Total Hours</th>
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${ATTENDANCE_MASTER.attendanceHistoryList}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.personName}</td>
					 		<td>${result3.designationName}</td>
					 		<td>${result3.companyName} >> ${result3.departmentName} >> ${result3.locationName}</td>
					 		<td>${result3.attedanceDate}</td>
							<td>${result3.inTime}</td>
							<td>${result3.outTime}</td>
							<td>${result3.inPart}</td>
							<td>${result3.outPart}</td>
							<td>${result3.timeLossOrExcess}</td>
							<td>${result3.decision}</td>
							<td>${result3.totalHours }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="width20 float-right" style="padding:5px;border-bottom: 1px solid #755800;">
				<label class="caption width15 float-left" >Total Hours: </label>
				<label class="data width60 float-left">${ATTENDANCE_MASTER.totalHours}</label>
			</div>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Punch Entries</tr>
		  </c:otherwise>
		  </c:choose>	
		</div> --%>	
		
	</body>
</html>