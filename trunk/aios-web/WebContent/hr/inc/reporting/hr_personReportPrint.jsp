<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="hr.report.attendancepunch" /></title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
td {
	background-color: white !important;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading">Person </span>
	</div>

	<div class="width100 float-left">
		<span class="side-heading">Employee Details </span>
	</div>
	<div class="data-list-container width100 float-left"
		style="margin-top: 10px;">
		<c:choose>
			<c:when test="${fn:length(PERSON_LIST)gt 0}">
				<table class="width100">

					<tbody>
						<c:forEach items="${PERSON_LIST}" var="per" varStatus="status1">
							<tr>
								<td width="35%;">
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Name</label><label
											style="width: 70%; float: left;">
											:&nbsp;&nbsp;${per.personName}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Number</label><label
											style="width: 70%; float: left;">
											:&nbsp;&nbsp;${per.personNumber}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Designation</label><label
											style="width: 70%; float: left;">
											:&nbsp;&nbsp;${per.jobAssignmentVO.designationName}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Department</label><label
											style="width: 70%; float: left;">
											:&nbsp;&nbsp;${per.jobAssignmentVO.departmentName}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Nationality</label><label
											style="width: 70%; float: left;">
											:&nbsp;&nbsp;${per.nationalityCaption}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Gender</label><label
											style="width: 70%; float: left;">
											:&nbsp;&nbsp;${per.genderCaption}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 30%; float: left; font-weight: bold;">Joining
											Date </label><label style="width: 70%; float: left;">:&nbsp;&nbsp;${per.effectiveDatesFrom}</label>
									</div></td>
								<td width="55%;">
									<div style="width: 100%; float: left;">
										<label style="width: 20%; float: left; font-weight: bold;">Mobile</label><label
											style="width: 80%; float: left;">
											:&nbsp;&nbsp;${per.mobile}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 20%; float: left; font-weight: bold;">D.O.B
										</label><label style="width: 80%; float: left;">:&nbsp;&nbsp;${per.birthDateStr}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 20%; float: left; font-weight: bold;">Marital
											Status </label><label style="width: 80%; float: left;">:&nbsp;&nbsp;${per.maritalStatusName}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 20%; float: left; font-weight: bold;">Salary
											Details</label><label style="width: 80%; float: left;">
											:&nbsp;&nbsp;${per.jobAssignmentVO.jobVO.payList}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 20%; float: left; font-weight: bold;">Leave
											Details</label><label style="width: 80%; float: left;">
											:&nbsp;&nbsp;${per.jobAssignmentVO.jobVO.leaveList}</label>
									</div>
									<div style="width: 100%; float: left;">
										<label style="width: 20%; float: left; font-weight: bold;">Work
											Schedule</label><label style="width: 80%; float: left;">
											:&nbsp;&nbsp;${per.jobAssignmentVO.jobVO.shiftList}</label>
									</div></td>
								<td width="10%;"><c:choose>
										<c:when
											test="${per.profilePicDisplay ne null && per.profilePicDisplay ne ''}">
											<div>
												<img name="preview" id="preview" height="125" width="125"
													src="data:image/png;base64,${per.profilePicDisplay}">
											</div>
										</c:when>
										<c:otherwise>
											<div>
												<img alt="No Image" name="preview" id="preview" height="125"
													width="125" src="./images/No_Profile_Picture.jpg">
											</div>
										</c:otherwise>
									</c:choose></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</c:when>
			<c:otherwise>
				<tr class="even rowid">
					<fmt:message key="hr.attendance.norecord" />
				</tr>
			</c:otherwise>
		</c:choose>
	</div>

</body>
</html>