<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
 
<script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
var personId=0;var fromDate="";var toDate="";var params="";
$(function(){ 
	$('.formError').remove();
	
	jsonCall();
	
$(".print-call").click(function(){ 
	if(s > 0)
		{
			manupulateParams();
			personId=s;
			window.open('<%=request.getContextPath()%>/person_detail_report_printout.action?personId='+s,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		}
	else
		{
			manupulateParams();
			window.open('<%=request.getContextPath()%>/person_report_printout.action?'+params,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		}
		
	return false;
	
});
$(".pdf-download-call").click(function(){ 
	manupulateParams();
	if(s > 0)
	{
		personId=s;
		window.open('<%=request.getContextPath()%>/person_detail_report_download_pdf.action?personId='+s,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	}
	else
		{
		
			
			window.open('<%=request.getContextPath()%>/person_report_download_pdf.action?'+params,
				'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
		}
	
	
	return false;
	
}); 
$(".xls-download-call").click(function(){
	manupulateParams();
	if(s > 0)
	{
		personId=s;
		window.open('<%=request.getContextPath()%>/person_detail_report_download_xls.action?personId='+s,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	}
	else
		{
			
			window.open('<%=request.getContextPath()%>/person_report_download_xls.action?'+params,
					'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');
		}
	
	return false;
}); 

$("#Go").click(function(){
	alreadyExsistList();
	return false;
}); 
$('#reset').click(function(){
	clear_form_elements();
});
//pop-up config
$('.employee-popup').click(function(){
      $('.ui-dialog-titlebar').remove();  
      $('#job-common-popup').dialog('open');
         $.ajax({
            type:"POST",
            url:"<%=request.getContextPath()%>/get_employee_for_punch_report.action",
            async: false,
            dataType: "html",
            cache: false,
            success:function(result){
                 $('.job-common-result').html(result);
                 return false;
            },
            error:function(result){
                 $('.job-common-result').html(result);
            }
        });
         return false;
}); 

/* $('#job-common-popup').dialog({
	autoOpen: false,
	minwidth: 'auto',
	width:800,
	height:450,
	bgiframe: false,
	modal: true 
});
 */
//Date process

$('#effectiveStartDate,#effectiveEndDate').datepick({
	 onSelect: customRange,showTrigger: '#calImg'}); 

 if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
/* Click event handler */
	 $('#Person tbody').on( 'click', 'tr', function () {
        	oTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            aData =oTable.fnGetData( this );
            s=aData[0];
	});
 }else{
	 $('#Person tbody tr').live('click', function () { 
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		        aData =oTable.fnGetData( this );
		        s=aData[0];
		    }
		    else {
		        oTable.$('tr.row_selected').removeClass('row_selected');
		        $(this).addClass('row_selected');
		        aData =oTable.fnGetData( this );
		        s=aData[0];
		    }
		}); 
 }



$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.otherErr ').hide();
       }
});


});
function customRange(dates) {
	if (this.id == 'effectiveStartDate') {
		$('#effectiveEndDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#effectiveStartDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}
function alreadyExsistList(){
	oTable = $('#Person').dataTable();
	oTable.fnDestroy();
	$('#Person').remove();
	$('#gridDiv').html("<table class='display' id='Person'></table>");
	jsonCall();
}
function jsonCall(){
	
		manupulateParams();
		$('#Person').dataTable({ 
			"sAjaxSource": "person_report_json.action?"+params,
			"bJQueryUI" : true,
			"bPaginate" : true,
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : false,
			"bLengthChange" : false,
			"bProcessing" : true,
			"bAutoWidth": false,
			"sServerMethod": 'POST',
			"sScrollY": "auto",
			"bDestroy" : true,
			"aoColumns": [
				{ "sTitle": '<fmt:message key="hr.personaldetails.label.recordid"/>', "bVisible": false},
				{ "sTitle": '<fmt:message key="hr.personaldetails.label.firstname"/>'},
				{ "sTitle": '<fmt:message key="hr.personaldetails.label.lastname"/>'},
				{ "sTitle": '<fmt:message key="hr.personaldetails.label.persontype"/>'},
				{ "sTitle": '<fmt:message key="hr.personaldetails.label.country"/>'},
				{ "sTitle": '<fmt:message key="hr.personaldetails.label.personid"/>'},
			],
			
		});	
		//Json Grid
		//init datatable
		oTable = $('#Person').dataTable();
}
function manupulateParams(){
	params="personFlag="+1;
	var personTypeList=new Array();
	$('#personType option:selected').each(function(){
		personTypeList.push($(this).val());
	});
	var personTypes="";
	for(var i=0; i<personTypeList.length;i++){
		if(i==(personTypeList.length-1))
			personTypes+=personTypeList[i];
		else
			personTypes+=personTypeList[i]+",";
	}
	params+="&personTypes="+personTypes;
	
	var status=$('#status').val();
	status=(status>0?status:0);
		
	params+="&disabled="+status;
	
	var gender=$('#gender').val();
	params+="&gender="+gender;
	
	var maritalStatus=$('#maritalStatus').val();
	params+="&maritalStatus="+maritalStatus;
	
	var personGroup=$('#personGroup').val();
	params+="&personGroup="+personGroup;
	
	var nationality=$('#nationality').val();
	params+="&nationality="+nationality;
	
	fromDate=$('#effectiveStartDate').val();
	params+="&effectiveDatesFrom="+fromDate;
	
	toDate=$('#effectiveEndDate').val();
	params+="&effectiveDatesTo="+toDate;
	
}
function clear_form_elements() {
	$('#page-error').hide();
	$('#PERSON').each(function(){
	    this.reset();
	});
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Employee Details</div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
		<div style="display:none;" class="tempresultMsg">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<div>
			 <div class="error response-msg ui-corner-all" style="display:none;">Sorry, Process Failure!</div>
		  	 <div class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div>
	<form id="PERSON" class="" name="PERSON" method="post">	
		<div class="width100 float-left" id="hrm" style="padding:5px;"> 
			<div class="width50 float-left">
				<label class="width30"><fmt:message key="person.label.persontypes"/></label>
		         <select  tabindex="1" name="personType" multiple class="width60 validate[required]" id="personType">
					<c:if test="${requestScope.PERSONTYPELIST!=null}">
						<c:forEach items="${requestScope.PERSONTYPELIST}" var="nltlst">
							<option value="${nltlst.lookupDetailId}">${nltlst.displayName} </option>
						</c:forEach>
					</c:if>	
                 </select> 
            </div>  
             <div class="width50 float-left">
                 <label class="width30"><fmt:message key="hr.person.status"/></label>
				<select tabindex="10" name="status" id="status" class="width60" >
					<option value="" > - Select - </option>
					<option value="1">Active</option>
					<option value="2">Inactive</option>
				</select>
			</div>
			<div class="width50 float-left">
				<label class="width30"><fmt:message key="person.label.gender"/></label>
		 		<select tabindex="22" name="gender" id="gender" class="width60 validate[required]">
		 			<option value="X"> - Select - </option>
		 			<option value="M">Male</option>
		 			<option value="F">Female</option>
		 		</select>
		 	</div>
			<div class="width50 float-left">
				<label class="width30"><fmt:message key="person.label.maritalstatus"/></label>
				<select tabindex="23" name="maritalStatus" id="maritalStatus" class="width60" >
					<option value="X"> - Select - </option>
					<option value="M">Married</option>
					<option value="S">Single</option>
					<option value="W">Widow</option>
					<option value="P">Separated</option>
				</select>
			</div>
			<div class="width50 float-left">
				<label class="width30"><fmt:message key="hr.personaldetails.label.nationalitytype"/></label>
				<select tabindex="24" name="personGroup" id="personGroup" class="width60 validate[required]">
					<option value="X"> - Select - </option>
					<option value="L">Local</option>
					<option value="A">Arab</option>
					<option value="G">Gulf</option>
					<option value="E">Expatriate</option>
				</select>
			</div>
			<div class="width50 float-left">
				<label class="width30"><fmt:message key="person.label.nationality"/></label>
				<select tabindex="25" id="nationality" class="width60 validate[required]" name="nationality">
					<option value=""> - Select - </option>
						<c:if test="${requestScope.COUNTRYLIST!=null}">
						<c:forEach items="${requestScope.COUNTRYLIST}" var="nltlst">
							<option value="${nltlst.countryId}">${nltlst.countryName} </option>
						</c:forEach>
						</c:if>	
				</select>
			</div>
			<div class="width50 float-left">
				<label class="width30" for="effectiveStartDate"><fmt:message key="hr.attendance.startDate"/></label>
				<input type="text" readonly="readonly" name="effectiveStartDate" id="effectiveStartDate" tabindex="2" class="width60"/>
			</div>   
			<div class="width50 float-left">
				<label class="width30" for="effectiveEndDate"><fmt:message key="hr.attendance.endDate"/></label>
				<input type="text" readonly="readonly" name="effectiveEndDate" id="effectiveEndDate" tabindex="3" class="width60"/>
			</div> 
			<div class="float-right buttons ui-widget-content ui-corner-all"> 
				<div class="portlet-header ui-widget-header float-right" id="reset" ><fmt:message key="hr.attendance.reset"/></div>
				<div class="portlet-header ui-widget-header float-right" id="Go" ><fmt:message key="hr.attendance.go"/></div>
			</div>
		</div>
	</form>
	<div id="rightclickarea">
		<div id="gridDiv">
			<table class="display table" id="Person"></table>
		</div>
 	</div>		
	<div class="vmenu">
		<div class="first_li"><span id="view">View</span></div>
		<div class="sep_li"></div>		 
	       	<div class="first_li"><span class="print-call">Print</span></div>
		<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
		<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
	</div>
	</div>
</div>
</div>
<c:choose>
<c:when test="${IS_MOBILE eq true}">
	<div class="process_buttons">
	<div id="hrm" class="width100 float-right">
	  	<div class="width10 float-right height60" title="Download as PDF"><img width="60" height="60" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width10 float-right height60" title="Download as XLS"><img width="60" height="60" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width10 float-right height60" title="Print"><img width="60" height="60" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
	</div> 
</c:when>
<c:otherwise>
	<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
	</div> 
</c:otherwise>
</c:choose>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="job-common-result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>
