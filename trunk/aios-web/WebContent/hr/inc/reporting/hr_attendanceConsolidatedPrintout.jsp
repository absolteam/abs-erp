<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Attendance Punch</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Attendance Punch</span></div>
	<div class="width70 float-left"><span class="side-heading">Filtered By</span></div>
	<div class="width30 float-right"><span class="side-heading">Printed on :${ATTENDANCE_MASTER.createdDate}</span></div>
	<div class="width100 float-left">
			<div class="data-container width40 float-left">	
				<div>
					<label class="caption width30" >Date From  :</label>
					<label class="data width20">${ATTENDANCE_MASTER.fromDate}</label>
					<label class="caption width30" >UpTo  :</label>
					<label class="data width20">${ATTENDANCE_MASTER.toDate}</label>
				</div>
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading">Punch Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(ATTENDANCE_MASTER.attendanceHistoryList)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Employee</th>
						<th>Designation</th>
						<th>Location</th>
						<th>Late In Day's</th>
						<th>Late Out Day's</th>
						<th>Early In Day's</th>
						<th>Early Out Day's</th>
						<th>Time Loss Day's</th>
						<th>Time Excess Day's</th>
						<th>Absent Day''s</th>
						<th>Total Hours</th>
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${ATTENDANCE_MASTER.attendanceHistoryList}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.personName}</td>
					 		<td>${result3.designationName}</td>
					 		<td>${result3.companyName} >> ${result3.departmentName} >> ${result3.locationName}</td>
					 		<td>${result3.lateInDays}</td>
							<td>${result3.lateOutDays}</td>
							<td>${result3.earlyInDays}</td>
							<td>${result3.earlyOutDays}</td>
							<td>${result3.timeLossDyas}</td>
							<td>${result3.timeExcessDays}</td>
							<td>${result3.absentDays}</td>
							<td>${result3.totalHours }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Punch Entries</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>