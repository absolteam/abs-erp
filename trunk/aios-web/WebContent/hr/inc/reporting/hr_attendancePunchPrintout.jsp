<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><fmt:message key="hr.report.attendancepunch"/></title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading"><fmt:message key="hr.report.attendancepunch"/></span></div>
	<div class="width70 float-left"><span class="side-heading"><fmt:message key="hr.attendance.employeeinfo"/></span></div>
	<div class="width40 float-right"><span class="side-heading" ><span>${ATTENDANCE_MASTER.createdDate}</span>:<span><fmt:message key="hr.attendance.printedon"/></span> </span></div>
	<div class="width100 float-left">
		<div class="data-container width40 float-left">	
				
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.employee"/>  :</label>
					<label class="data width50">${ATTENDANCE_MASTER.personName}</label>
				</div>
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.company"/>  :</label>
					<label class="data width50">${ATTENDANCE_MASTER.companyName}</label>
				</div>
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.department"/>  :</label>
					<label class="data width50">${ATTENDANCE_MASTER.departmentName}</label>
				</div>
				<div>
					<label class="caption width30" ><fmt:message key="hr.attendance.location"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.locationName}</label>
				</div>
			</div>
			<div class="data-container width40 float-left">	
				<div>
					<label class="data width20">${ATTENDANCE_MASTER.fromDate} :</label>
					<label class="caption width30" ><fmt:message key="hr.report.historyfrom"/></label>
				</div>
				<div>
					<label class="data width20">${ATTENDANCE_MASTER.toDate} :</label>
					<label class="caption width30" ><fmt:message key="hr.report.upto"/> </label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.lateindays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.lateInDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.lateoutdays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.lateOutDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.earlyindays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.earlyInDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.earlyoutdays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.earlyOutDays}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.timelossdays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.timeLossDyas}</label>
				</div>
				<div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.timeexcessdays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.timeExcessDays}</label>
				</div>
				<%-- <div class="width40 float-left">
					<label class="caption width30" ><fmt:message key="hr.attendance.numberofdays"/> :</label>
					<label class="data width50">${ATTENDANCE_MASTER.numberOfDays}</label>
				</div> --%>
				<div class="width40 float-left">
					<label class="caption width30" style="color:red;">Absent Days :</label>
					<label class="data width50" style="color:red;">${ATTENDANCE_MASTER.numberOfDays}</label>
				</div>
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading"><fmt:message key="hr.attendance.punchinformation"/></span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(ATTENDANCE_MASTER.attendanceHistoryList)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th><fmt:message key="hr.attendance.attendanceday"/></th>
						<th><fmt:message key="hr.attendance.timein"/></th>
						<th><fmt:message key="hr.attendance.timout"/></th>
						<th>Shift Schedule</th>
						<th><fmt:message key="hr.attendance.intype"/></th>
						<th><fmt:message key="hr.attendance.outtype"/></th>
						<th><fmt:message key="hr.attendance.timelossorexcess"/></th>
						<th><fmt:message key="hr.attendance.totalhours"/></th>
						<th><fmt:message key="hr.attendance.decision"/></th>
						<th>Reason</th>
						
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${ATTENDANCE_MASTER.attendanceHistoryList}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.attedanceDate}</td>
							<td>${result3.inTime}</td>
							<td>${result3.outTime}</td>
							<td>&nbsp;${result3.jobShift.workingShift.startTime}&nbsp;&nbsp; - &nbsp;&nbsp;${result3.jobShift.workingShift.endTime}</td>
							<td>${result3.inPart}</td>
							<td>${result3.outPart}</td>
							<td>${result3.timeLossOrExcess}</td>
							<td>${result3.totalHours }</td>
							<td>${result3.decision}</td>
							<td><a href="#" title="${result3.systemSuggesion}">Hover On</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="width20 float-right" style="padding:5px;border-bottom: 1px solid #755800;">
				<label class="caption width15 float-left" ><fmt:message key="hr.attendance.totalhours"/>: </label>
				<label class="data width60 float-left">${ATTENDANCE_MASTER.totalHours}</label>
			</div>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid"><fmt:message key="hr.attendance.norecord"/></tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>