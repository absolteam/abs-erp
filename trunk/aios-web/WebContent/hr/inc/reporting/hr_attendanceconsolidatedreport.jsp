<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
var companyId=0;var fromDate="";var toDate="";
$(function(){ 
	$('.formError').remove();
	
	attendancePunchReportCall();

$(".print-call").click(function(){ 
	companyId=$('#companyId').val();
 	fromDate=$('#effectiveStartDate').val();
 	toDate=$('#effectiveEndDate').val();
	window.open('<%=request.getContextPath()%>/attendance_consolidated_printout.action?companyId='+companyId+'&fromDate='+fromDate+'&toDate='+toDate,
			'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	return false;
	
});
$(".pdf-download-call").click(function(){ 
	companyId=$('#companyId').val();
	fromDate=$('#effectiveStartDate').val();
 	toDate=$('#effectiveEndDate').val();
	window.open('<%=request.getContextPath()%>/consolidated_punch_report_pdf.action?companyId='+companyId+'&fromDate='+fromDate+'&toDate='+toDate,
			'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	return false;
	
}); 
$(".xls-download-call").click(function(){
	companyId=$('#companyId').val();
	fromDate=$('#effectiveStartDate').val();
 	toDate=$('#effectiveEndDate').val();
	window.open('<%=request.getContextPath()%>/get_consolidiated_punch_xls.action?companyId='+companyId+'&fromDate='+fromDate+'&toDate='+toDate,
			'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	
	return false;
}); 

$("#Go").click(function(){
	attendancePunchAlreadyExsist();
	return false;
}); 
$('#reset').click(function(){
	clear_form_elements();
});
//pop-up config
$('.company-popup').click(function(){
      $('.ui-dialog-titlebar').remove();  
      $('#job-common-popup').dialog('open');
         $.ajax({
            type:"POST",
            url:"<%=request.getContextPath()%>/get_company_for_punch_report.action",
            async: false,
            dataType: "html",
            cache: false,
            success:function(result){
                 $('.job-common-result').html(result);
                 return false;
            },
            error:function(result){
                 $('.job-common-result').html(result);
            }
        });
         return false;
}); 

$('#job-common-popup').dialog({
	autoOpen: false,
	minwidth: 'auto',
	width:800,
	height:450,
	bgiframe: false,
	modal: true 
});

//Date process

$('#effectiveStartDate,#effectiveEndDate').datepick({
	 onSelect: customRange,showTrigger: '#calImg'}); 

/* Click event handler */
$('#Attendance tbody tr').live('click', function () { 
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
});

$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.otherErr ').hide();
       }
});

$('#add').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/add_attendance_by_hr.action",  
     	data: {attendanceId:0},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
});

$('#edit').click(function(){
	if(s == null)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		var attendanceId = Number(s);
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/add_attendance_by_hr.action",  
	     	data: {attendanceId:attendanceId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	}
		}); 
	}
});
$('#view').click(function(){
	if(s == null)
	{
		alert("Please select one row to view");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/view_attendance.action",  
     	data: {attendanceId:recId2},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
});
$('#read').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/attendance_read.action",  
		async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
     	}
	}); 
});
$('#calculate').click(function(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/attendance_calculate.action",  
		async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
     	}
	}); 
});
});
function customRange(dates) {
	if (this.id == 'effectiveStartDate') {
		$('#effectiveEndDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#effectiveStartDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}
function attendancePunchAlreadyExsist(){
	oTable = $('#Attendance').dataTable();
	oTable.fnDestroy();
	$('#Attendance').remove();
	$('#gridDiv').html("<table class='display' id='Attendance'></table>");
	attendancePunchReportCall();
}
function attendancePunchReportCall(){
	fromDate=$('#effectiveStartDate').val();
	toDate=$('#effectiveEndDate').val();
	companyId=Number($('#companyId').val());
		$('#Attendance').dataTable({ 
			"sAjaxSource": "attendance_consolidated_json.action?companyId="+companyId+"&fromDate="+fromDate+"&toDate="+toDate,
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": 'Attendance ID', "bVisible": false},
				{ "sTitle": 'Employee',"sWidth": "150px"},
				{ "sTitle": 'Designation'},
				{ "sTitle": 'Location',"sWidth": "200px"},
				{ "sTitle": 'Late In Day(s)'},
				{ "sTitle": 'Late Out Day(s)'},
				{ "sTitle": 'Early In Day(s)'},
				{ "sTitle": 'Early Out Day(s)'},
				{ "sTitle": 'Time Loss Day(s)'},
				{ "sTitle": 'Time Excess Day(s)'},
				{ "sTitle": 'Absent Day(s)'},
				{ "sTitle": 'Total Hours'},
			],
			"sScrollY": $("#main-content").height() - 290,
			//"bPaginate": false,
			"aaSorting": [[0, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	
		//Json Grid
		//init datatable
		oTable = $('#Attendance').dataTable();
}
function clear_form_elements() {
	$('#page-error').hide();
	$('#LEAVEDET').each(function(){
	    this.reset();
	});
}
$('#companyId').combobox({
	selected : function(event, ui) {
	}
});
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Attendance Information</div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
		<div style="display:none;" class="tempresultMsg">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<div>
			 <div class="error response-msg ui-corner-all" style="display:none;">Sorry, Process Failure!</div>
		  	 <div class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div>
	<form id="LEAVEDET" class="" name="LEAVEDET" method="post">	
		<div class="width100 float-left" id="hrm" style="padding:1px;">  
			 <div class="width30 float-left">
			 	<label class="width40" for="companyId">Company</label>
				 <select class="width50" id="companyId" name="companyId">
				 	<option value="">--Select--</option>
					<c:forEach items="${COMPANY_LIST}" var="company" varStatus="status">
						<option value="${company.companyId}">${company.companyName}</option>
					</c:forEach>
				 </select>
			 </div>
			 <div class="width30 float-left">
				<label class="width40" for="effectiveStartDate">Start Date</label>
				<input type="text" readonly="readonly" name="effectiveStartDate" id="effectiveStartDate" tabindex="1" class="width50"/>
			</div>   
			<div class="width30 float-left">
				<label class="width40" for="effectiveEndDate">End Date</label>
				<input type="text" readonly="readonly" name="effectiveEndDate" id="effectiveEndDate" tabindex="2" class="width50"/>
			</div> 
			<div class="float-right buttons ui-widget-content ui-corner-all"> 
				<div class="portlet-header ui-widget-header float-right" id="reset" >Reset</div>
				<div class="portlet-header ui-widget-header float-right" id="Go" >Go</div>
			</div>
		</div>
	</form>
	<div id="rightclickarea">
		<div id="gridDiv">
			<table class="display" id="Attendance"></table>
		</div>
 	</div>		
	<div class="vmenu">
		<div class="first_li"><span id="view">View</span></div>
		<div class="sep_li"></div>		 
	       	<div class="first_li"><span class="print-call">Print</span></div>
		<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
		<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
	</div>
	</div>
</div>
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="job-common-result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>