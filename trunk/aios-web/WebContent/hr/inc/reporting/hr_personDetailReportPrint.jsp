<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="hr.report.attendancepunch" />
</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading">Employee
		</span>
	</div>
	
	<div class="width100 float-left">
		<span class="side-heading">Employee Details
		</span>
	</div>
	<div class="width100 float-left"
		style="margin-top: 10px;">
		<table class="width100">
			<tbody>
				<tr>
					<td colspan="2">
						<div><label>Name :</label><span>${PERSON.personName}</span></div>
						<div><label>Number :</label><span>${PERSON.personNumber}</span></div>
						<div><label>Designation :</label><span>${PERSON.jobAssignmentVO.designationName}</span></div>
						<div><label>Department :</label><span>${PERSON.jobAssignmentVO.departmentName}</span></div>
						<div><label>Person Group :</label><span>${PERSON.personTypes}</span></div>
						<div><label>Type :</label><span>${PERSON.personGroupCaption}</span></div>
						<div><label>Status :</label><span>${PERSON.statusCaption}</span></div>
						<div><label>Joining Date :</label><span>${PERSON.effectiveDatesFrom}</span></div>
					</td>
					<td colspan="2">
						<c:choose>
							<c:when test="${PERSON.profilePicDisplay ne null && PERSON.profilePicDisplay ne ''}">
								<div><img name="preview" id="preview" height="125" width="125" src="data:image/png;base64,${PERSON.profilePicDisplay}"></div>
							</c:when>
							<c:otherwise>
		 	 					<div><img alt="No Image" name="preview" id="preview" height="125" width="125" src="./images/No_Profile_Picture.jpg"></div>
		 	 				</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div><label>Salary Details :</label><span>${PERSON.jobAssignmentVO.jobVO.payList}</span></div>
						<div><label>Leave Details :</label><span>${PERSON.jobAssignmentVO.jobVO.leaveList}</span></div>
						<div><label>Work schedule :</label><span>${PERSON.jobAssignmentVO.jobVO.shiftList}</span></div>
					</td>
					
				</tr>
			</table>
			<table class="width100">
			<tr>
				<td class="width20"> 
					<label>
						<fmt:message key="hr.person.residentialaddress" /> : 
					</label>
				</td>
				<td class="width30">
					${PERSON.residentialAddress}
				</td>
				<td class="width20">
					<label><fmt:message	key="person.label.birthdate" /> : </label>
				</td class="width30">
				<td>${PERSON.birthDate}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message	key="hr.person.permenantaddress" /> : </label>
				</td>
				<td>${PERSON.permanentAddress}</td>
				<td>
					<label><fmt:message key="person.label.age" /> : </label>
				</td>
				<td>${PERSON.age}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message key="hr.person.mobile" /> : </label>
				</td>
				<td>${PERSON.mobile}</td>
				<td>
					<label><fmt:message key="person.label.townofbirth" /> : </label>
				</td>
				<td>${bean.townBirth}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message key="hr.person.alternatemobile" /> : </label>
				</td>
				<td>${PERSON.alternateMobile}</td>
				<td>
					<label><fmt:message key="person.label.regionofbirth" /> : </label>
				</td>
				<td>${PERSON.regionBirth}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message key="hr.common.phone" /> : </label>
				</td>
				<td>${PERSON.telephone}</td>
				<td>
					<label><fmt:message key="person.label.countryofbirth" /> : </label>
				</td>
				<td>${PERSON.countryBirth}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message key="hr.common.fax" /> : </label>
					
				</td>
				<td>${PERSON.alternateTelephone}</td>
				<td>
					<label><fmt:message key="person.label.gender" /> : </label>
				</td>
				<td>${PERSON.genderCaption}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message key="hr.common.pobox" /> : </label>
				</td>
				<td>${PERSON.postBoxNumber}</td>
				<td>
					<label><fmt:message key="person.label.maritalstatus" /> : </label>
				</td>
				<td>${PERSON.maritalStatusName}</td>
			</tr>
			<tr>
				<td>
					<label><fmt:message key="hr.common.email" /> : </label>
				</td>
				<td>${PERSON.email}</td>
				<td>
					<label><fmt:message key="hr.personaldetails.label.nationalitytype" /> : </label> 
				</td>
				<td>${PERSON.personGroupCaption}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>
					<label class="width30"><fmt:message key="person.label.nationality" /> : </label>
				</td>
				<td>${PERSON.nationalityCaption}</td>
			</tr>
			</tbody>
		</table>

		<br><br>
		<h2>Identity</h2>
				<table class="width100">
					<thead>
						<tr>
							<th class="width10"><fmt:message key="hr.person.identitytype" /></th>
							<th class="width10"><fmt:message key="hr.person.identitynumber" /></th>
							<th class="width10"><fmt:message key="hr.person.expiredate" /></th>
							<th class="width10"><fmt:message key="hr.person.issuedplace" /></th>
							<th class="width20"><fmt:message key="common.description" /></th>
						</tr>
					</thead>
					<tbody class="identitytab">
						<c:forEach items="${requestScope.IDENTITY_LIST}" var="result"
							varStatus="status">
							<tr id="identityfieldrow_${status.index+1}" class="identityrowid">
								<td class="width10" id="identityType_${status.index+1}">${result.identityTypeName}</td>
								<td class="width10" id="identityNumber_${status.index+1}">${result.identityNumber}</td>
								<td class="width10" id="identityExpireDate_${status.index+1}">${result.expireDate}</br>
									[<span style="font-weight: bold; color: blue;">${result.daysLeft}</span>]
									day's left
								</td>
								<td class="width10" id="identityIssuedPlace_${status.index+1}">${result.issuedPlace}</td>
								<td class="width20" id="identityDescription_${status.index+1}">${result.description}</td>
								<td style="width: 5%;" id="identityoption_${status.index+1}">
									<input type="hidden" name="identityTypeId_${status.index+1}"
											id="identityTypeId_${status.index+1}" value="${result.identityType}" /> 
									
									<input type="hidden" name="identityId_${status.index+1}"
											id="identityId_${status.index+1}" value="${result.identityId}" />
									
									<input type="hidden" name="lineId_${status.index+1}" id="lineId_${status.index+1}" value="${status.index+1}" /> 
									
									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddData"
											id="identityAddImage_${status.index+1}" style="display: none; cursor: pointer;" title="Add Record">
										<span class="ui-icon ui-icon-plus"></span>
									</a> 
									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityeditData"
											id="identityEditImage_${status.index+1}" style="cursor: pointer;" title="Edit Record"> 
										<span class="ui-icon ui-icon-wrench"></span>
									</a>
									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrow"
											id="identityDeleteImage_${status.index+1}" style="cursor: pointer;" title="Delete Record"> 
									<span class="ui-icon ui-icon-circle-close"></span>
									</a> 
									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="identityWorkingImage_${status.index+1}" style="display: none; cursor: pointer;" title="Working"> 
										<span class="processing"></span>
									</a>

								</td>
							</tr>
						</c:forEach>
						<c:forEach var="i" begin="${fn:length(IDENTITY_LIST)+1}"
							end="${fn:length(IDENTITY_LIST)+1}" step="1" varStatus="status">
							<tr id="identityfieldrow_${i}" class="identityrowid">
								<td class="width10" id="identityType_${i}"></td>
								<td class="width10" id="identityNumber_${i}"></td>
								<td class="width10" id="identityExpireDate_${i}"></td>
								<td class="width10" id="identityIssuedPlace_${i}"></td>
								<td class="width20" id="identityDescription_${i}"></td>
								
								<td style="width: 5%;" id="identityoption_${i}">
									<input type="hidden" name="identityTypeId_${i}" id="identityTypeId_${i}" value="0" /> 
									<input type="hidden" name="identityId_${i}" id="identityId_${i}" value="0" /> 
									<input type="hidden" name="identitylineId_${i}" id="identitylineId_${i}" value="${i}" /> 
									
									<a style="cursor: pointer;"
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityaddData"
										id="identityAddImage_${i}" title="Add Record"> 
										<span class="ui-icon ui-icon-plus"></span>
									</a> 
									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identityeditData"
										id="identityEditImage_${i}" style="display: none; cursor: pointer;" title="Edit Record">
											<span class="ui-icon ui-icon-wrench"></span>
									</a> 
									<a style="cursor: pointer;"
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip identitydelrow"
										id="identityDeleteImage_${i}" title="Delete Record"> 
											<span class="ui-icon ui-icon-circle-close"></span>
									</a> 
									<a href="#"
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
										id="identityWorkingImage_${i}" style="display: none;" title="Working"> 
											<span class="processing"></span>
									</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		<br><br>
		<h2>Dependent</h2>
				<table id="hastab" class="width100">
					<thead>
						<tr>
							<th class="width10"><fmt:message key="hr.person.name" /></th>
							<th class="width10"><fmt:message
									key="hr.person.relationship" /></th>
							<th class="width10"><fmt:message key="hr.common.address" /></th>
							<th class="width10"><fmt:message key="hr.person.mobile" /></th>
							<th class="width10"><fmt:message key="hr.common.phone" /></th>
							<th class="width10"><fmt:message key="hr.common.email" /></th>
							<th class="width10"><fmt:message key="hr.person.emergency" /></th>
							<th class="width10"><fmt:message key="hr.person.dependent" /></th>
							<th class="width10"><fmt:message key="hr.person.benifit" /></th>
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach items="${requestScope.DEPENDENT_LIST}" var="result"
							varStatus="status1">
							<tr id="fieldrow_${status1.index+1}" class="rowid">
								<td class="width10" id="dependentName_${status1.index+1}">${result.dependentName}</td>
								<td class="width10"
									id="dependentRelationship_${status1.index+1}">${result.relationship}</td>
								<td class="width10" id="dependentAddress_${status1.index+1}">${result.dependentAddress}</td>
								<td class="width10" id="dependentMobile_${status1.index+1}">${result.dependentMobile}</td>
								<td class="width10" id="dependentPhone_${status1.index+1}">${result.dependentPhone}</td>
								<td class="width10" id="dependentEmail_${status1.index+1}">${result.dependentMail}</td>
								<td class="width10" id="emergency_${status1.index+1}"><c:choose>
										<c:when test="${result.emergency eq true}">YES</c:when>
										<c:otherwise>NO</c:otherwise>
									</c:choose></td>
								<td class="width10" id="dependent_${status1.index+1}"><c:choose>
										<c:when test="${result.dependentflag eq true}">YES</c:when>
										<c:otherwise>NO</c:otherwise>
									</c:choose></td>
								<td class="width10" id="benefit_${status1.index+1}"><c:choose>
										<c:when test="${result.benefit eq true}">YES</c:when>
										<c:otherwise>NO</c:otherwise>
									</c:choose></td>
								
							</tr>
						</c:forEach>
						<c:forEach var="i" begin="${fn:length(DEPENDENT_LIST)+1}"
							end="${fn:length(DEPENDENT_LIST)+1}" step="1" varStatus="status">
							<tr id="fieldrow_${i}" class="rowid">
								<td class="width10" id="dependentName_${i}"></td>
								<td class="width10" id="dependentRelationship_${i}"></td>
								<td class="width10" id="dependentAddress_${i}"></td>
								<td class="width10" id="dependentMobile_${i}"></td>
								<td class="width10" id="dependentPhone_${i}"></td>
								<td class="width10" id="dependentEmail_${i}"></td>
								<td class="width10" id="emergency_${i}"></td>
								<td class="width10" id="dependent_${i}"></td>
								<td class="width10" id="benefit_${i}"></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			<br><br>
			<h2>Qualifications</h2>
			<table id="qualificationhastab" class="width100">
				<thead>
					<tr>
						<th class="width10">Degree</th>
						<th class="width10">Subject</th>
						<th class="width10">Instution</th>
						<th class="width10">Completion Year</th>
						<th class="width10">Result</th>
						<th class="width10">Result Type</th>
						</th>
					</tr>
				</thead>
				<tbody class="qualificationtab">
					<c:forEach items="${PERSON.academicQualificationses}"
						var="result" varStatus="status1">
						<tr class="qualificationrowid">
							<td class="width10">${result.lookupDetail.displayName}</td>
							<td class="width10">${result.majorSubject}</td>
							<td class="width10">${result.instution}</td>
							<td class="width10">${result.completionYear}</td>
							<td class="width10">${result.result}</td>
							<td class="width10">${result.resultType}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<br><br>
			<h2>Experiences</h2>
			<table id="experiencehastab" class="width100">
				<thead>
					<tr>
						<th class="width10">Previous Experience</th>
						<th class="width10">Current Post</th>
						<th class="width10">Job Title</th>
						<th class="width10">Company Name</th>
						<th class="width10">Start Date</th>
						<th class="width10">End Date</th>
						<th class="width10">Address</th>
					</tr>
				</thead>
				<tbody class="experiencetab">
					<c:forEach items="${PERSON.experienceses}" var="result"
						varStatus="status1">
						<tr class="experiencerowid">
							<td class="width10">${result.previousExperience}</td>
							<td class="width10">${result.currentPost}</td>
							<td class="width10">${result.jobTitle}</td>
							<td class="width10">${result.companyName}</td>
							<td class="width10">${result.startDate}</td>
							<td class="width10">${result.endDate}</td>
							<td class="width10">${result.location}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<br><br>
			<h2>Skills</h2>
			<table id="skillhastab" class="width100">
				<thead>
					<tr>
						<th class="width10">Skill Type</th>
						<th class="width10">Skill Level</th>
						<th class="width10">Description</th>
					</tr>
				</thead>
				<tbody class="skilltab">
					<c:forEach items="${PERSON.skillses}" var="result"
						varStatus="status1">
						<tr class="skillrowid">
							<td class="width10">${result.lookupDetail.displayName}</td>
							<td class="width10">${result.skillLevel}</td>
							<td class="width10">${result.description}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<br><br><br><br>
	</div>
</body>
</html>