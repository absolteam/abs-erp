<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.label-data{
	font-weight: normal!important;
}

#ui-datepicker-div {
	background-color: #cfcfcf !important;
	/* left: 731.5px !important; */
		position: absolute !important;
	/* top: 124.967px !important; */
	width: 231px !important;
	z-index: 751!important;
	
}
.ui-widget-content {
    background: url("images/ui-bg_flat_75_ffffff_20x100.png") repeat-x scroll 50% 50% #FFFFFF;
    color: #444444;
}

.ui-slider-horizontal {
    border: 1px solid #CCCCCC;
    margin: 3px 0 4px 5px;
   
}
</style><script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;var swipeLineDetail="";
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
$(function(){ 
	$('#attendanceDate').datepick({});
	$('.attendanceDate').datepick({});
	$('.timeInOut').timepicker({});
	$('.formError').remove();
	if($('#attendanceDate').attr("checked"))
		$('#att-date-div').show();
	else
		$('#att-date-div').hide();
	
	personId=Number($('#personId').val());	
	manupulateLastRow();

	$('.discard').click(function(){
		homeredirect();
	});
	
	$('#isAbsent').click(function(){
		if($(this).attr("checked"))
			$('#att-date-div').show();
		else
			$('#att-date-div').hide();
			
			
	});
	

//Onchage event
	$('.attendanceDate').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	
	$('.swipeInOutType').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	$('.timeInOut').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	

	 $('#employee-list-close').live('click',function() { 
		   $('#job-common-popup').dialog('close');
	   });
	
	//Add row
	$('.addrowsS').live('click',function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
		  var attendanceDate=$('#attendanceDate_'+id).val();
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/swipe_inout_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabS tr:last').before(result);
					 if($(".tabS").height()>255)
						 $(".tabS").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidS').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdS_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowS').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
  	 $(slidetab).remove();  
  	 var i=1;
	 $('.rowidS').each(function(){   
		 var rowId=getRowId($(this).attr('id')); 
		 $('#lineIdS_'+rowId).html(i);
			 i=i+1; 
		 });   
	 });
	
	$jquery("#ATTENDANCEEDIT").validationEngine('attach');
	
//Save & discard process
$('.save').click(function(){ 
	$('#page-error').hide();
	if($jquery("#ATTENDANCEEDIT").validationEngine('validate')){
		var attendanceId=Number($('#attendanceId').val());
		var jobAssignmentId=Number($('#jobAssignmentId').val());
		var isAbsent=$('#isAbsent').attr("checked");
		var attendanceDate=$('#attendanceDate').val();
		if(isAbsent=='true')
			isAbsent=true;
		if(jobAssignmentId > 0){
			swipeLineDetail=getSwipeLineDetails(); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/attendance_save_hr.action", 
			 	async: false, 
			 	data:{ 
			 		attendanceId:attendanceId,
			 		swipeLineDetail:swipeLineDetail,
			 		jobAssignmentId:jobAssignmentId,
			 		absent:isAbsent,
			 		attendanceDate:attendanceDate
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 homeredirect();
						 $('#success_message').hide().html("Successfully  Processed").slideDown(1000);
					 }else{
						 $('#page-error').hide().html("Attendance Failure. Please check the attendance policy/shift.").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Attendance Failure. Please check the attendance policy/shift.").slideDown(1000);
				}
			});
		}else{
			$('#page-error').hide().html("Please select an employee.").slideDown(1000); 
		} 
	}
	else{
		return false;
	}
	return false;
});

//pop-up config
$('#employeepopups').click(function(){
      $('.ui-dialog-titlebar').remove();  
      $('#job-common-popup').dialog('open');
         $.ajax({
            type:"POST",
            url:"<%=request.getContextPath()%>/get_employee_designation.action",
            async: false,
            dataType: "html",
            cache: false,
            success:function(result){
                 $('.job-common-result').html(result);
                 return false;
            },
            error:function(result){
                 $('.job-common-result').html(result);
            }
        });
        return false;
}); 

$('#job-common-popup').dialog({
	autoOpen: false,
	minwidth: 'auto',
	width:800,
	height:550,
	bgiframe: false,
	modal: true 
});
});
function homeredirect(){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/attendance_excution_listing.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	return false;
}

var getSwipeLineDetails=function(){
	swipeLineDetail="";
	var swipeInOutIds=new Array();
	var attendanceDates=new Array();
	var swipeInOutTypeS=new Array();
	var timeInOutS=new Array();
	var swipeIds=new Array();
	$('.rowidS').each(function(){ 
		var rowId=getRowId($(this).attr('id')); 
		var swipeInOutId=$('#swipeInOutId_'+rowId).val();
		var attedanceDate=$('#attendanceDate_'+rowId).val();
		var swipeInOutType=$('#swipeInOutType_'+rowId).val();  
		var timeInOut=$('#timeInOut_'+rowId).val();		
		var swipeId=$('#swipeId').val();

		if(swipeInOutId==null || swipeInOutId=='' || swipeInOutId==0 || swipeInOutId=='undefined')
			swipeInOutId=-1;
		
		if(swipeId==null || swipeId=='' || swipeId==0 || swipeId=='undefined')
			swipeId=-1;
	
		if(typeof timeInOut != 'undefined' && timeInOut!=null && timeInOut!="" && swipeInOutType!=null && swipeInOutType!=""){
			swipeInOutIds.push(swipeInOutId);
			attendanceDates.push(attedanceDate);
			swipeInOutTypeS.push(swipeInOutType);
			timeInOutS.push(timeInOut);
			swipeIds.push(swipeId);

		}
	});
	for(var j=0;j<timeInOutS.length;j++){ 
		swipeLineDetail+=swipeInOutIds[j]+"@"+attendanceDates[j]+"@"+swipeInOutTypeS[j]+"@"+timeInOutS[j]+"@"+swipeIds[j];
		if(j==timeInOutS.length-1){   
		} 
		else{
			swipeLineDetail+="##";
		}
	} 
	return swipeLineDetail;
};
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){ 
	var swipeInOutType=$('#swipeInOutType_'+rowId).val();  
	var attedanceDate=$('#attendanceDate_'+rowId).val();
	var timeInOut=$('#timeInOut_'+rowId).val();
	var nexttab=$('#fieldrowS_'+rowId).next(); 
	if(swipeInOutType!=null && swipeInOutType!=""
			&& attedanceDate!=null && attedanceDate!="" && timeInOut!=null && timeInOut!="" && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageS_'+rowId).show();
		$('.addrowsS').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tabS>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabS>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabS>tr:last').removeAttr('id');  
	$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
	$($('.tabS>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="hr.attendance.attendanceinfo"/></div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="display:none;"></div>
		<div style="display:none;" class="tempresult">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<!-- <div>
			 <div class="error response-msg ui-corner-all" style="display:none;">Sorry, Process Failure!</div>
		  	  <div class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div> -->
		  <form id="ATTENDANCEEDIT" class="" name="ATTENDANCEEDIT" method="post" style="position: relative;">
		  	<div class="width100 float-left" id="hrm"> 
		  			<input type="hidden" name="attendanceId" id="attendanceId" value="${ATTENDANCE_PROCESS.attendanceId}" class="width50"/>
		  			<input type="hidden" name="personId" id="personId" value="${ATTENDANCE_PROCESS.person.personId}" class="width50 validate[required]"/>
		  			<div class="width100 float-left">
						<fieldset style="min-height:110px;">
							<legend><fmt:message key="hr.attendance.employeeinfo"/></legend>
							<div class="width45 float-left">
								<c:set var="jobAssignment" value="${ATTENDANCE_PROCESS.jobAssignment}"/>
								<input type="hidden" name="jobAssignmentId" id="jobAssignmentId" value="${jobAssignment.jobAssignmentId}" class="width50 validate[required]"/>
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;" ><fmt:message key="hr.attendance.employee"/><span class="mandatory">*</span> : </label>
									<input type="text" readonly="readonly"  id="employeeName" class="float-left width50 validate[required]" 
									value="${ATTENDANCE_PROCESS.person.firstName} ${ATTENDANCE_PROCESS.person.lastName}"/>
									
									<span class="button" id="employee"  style="position: relative; top:6px;">
										<a style="cursor: pointer;" id="employeepopups" class="btn ui-state-default ui-corner-all width10 employee-popup"> 
											<span class="ui-icon ui-icon-newwin">
											</span> 
										</a>
									</span> 
									<input type="hidden" id="personId" value="" class="personId"  style="border:0px;"/>
								</div>  
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;"><fmt:message key="hr.attendance.job"/> : </label>
									<input type="text" readonly="readonly" class="width50" id="designationName" value="${jobAssignment.designation.designationName}"/>
								</div> 
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;"><fmt:message key="hr.attendance.grade"/> : </label>
									<input type="text" readonly="readonly" class="width50" id="gradeName" value="${jobAssignment.designation.grade.gradeName}"/>
								</div> 
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;" ><fmt:message key="hr.attendance.swipeid"/><span class="mandatory">*</span>  : </label>
									<input type="text" readonly="readonly" class="width50 validate[required]" id="swipeId" value="${jobAssignment.swipeId}"/>
								</div>
							</div>
							<div class="width45 float-left">
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;"><fmt:message key="hr.attendance.company"/> : </label>
									<input type="text" readonly="readonly" class="width50" id="companyName" value="${jobAssignment.cmpDeptLocation.company.companyName}"/>
								</div>
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;"><fmt:message key="hr.attendance.department"/> : </label>
									<input type="text" readonly="readonly" class="width50" id="departmentName" value="${jobAssignment.cmpDeptLocation.department.departmentName}"/>
								</div>
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;"><fmt:message key="hr.attendance.location"/> : </label>
									<input type="text" readonly="readonly" class="width50" id="locationName" value="${jobAssignment.cmpDeptLocation.location.locationName}"/>
								</div>
								<div class="float-left width100">
									<label class="width30" style="padding-bottom:6px;">Absent : </label>
									<c:choose>
									<c:when test="${ATTENDANCE_PROCESS.isAbsent eq true}">
										<input type="checkbox" class="width5" id="isAbsent" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" class="width5" id="isAbsent"/>
									</c:otherwise>
									</c:choose>
								</div>
								<div class="float-left width100" style="display:none;" id="att-date-div">
									<label class="width30" style="padding-bottom:6px;">Absent Date </label>
									<input type="text" readonly="readonly" class="width50" id="attendanceDate" value="${ATTENDANCE_PROCESS.attendanceDate}"/>
								</div>
								
							</div>
							
						</fieldset>
				 	</div> 
				
					<div class="width100 float-left">
						<fieldset>
							<legend><fmt:message key="hr.attendance.swipeentries"/></legend>
							<div id="hrm" class="hastable width100"  >  
									<table id="hastab2" class="width100"> 
										<thead>
											<tr> 
												<th style="width:5%"><fmt:message key="hr.attendance.date"/></th> 
											    <th style="width:5%"><fmt:message key="hr.attendance.inouttype"/></th>  
											    <th style="width:5%"><fmt:message key="hr.attendance.inouttime"/></th>
												<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
										  </tr>
										</thead> 
												<tbody class="tabS">
													<c:choose>
														<c:when test="${ATTENDANCE_SWIPE_LIST ne null && ATTENDANCE_SWIPE_LIST ne '' && fn:length(ATTENDANCE_SWIPE_LIST)>0}">
															<c:forEach var="swp" items="${ATTENDANCE_SWIPE_LIST}" varStatus="status1">
																<tr class="rowidS" id="fieldrowS_${status1.index+1}">
																	<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
																	<td>
																		 <c:choose>
																				<c:when test="${swp.attendanceDate ne null &&  swp.attendanceDate ne ''}">
																					<c:set var="attendanceDate" value="${swp.attendanceDate}"/>  
																					<%String attendanceDate = DateFormat.convertDateToString(pageContext.getAttribute("attendanceDate").toString());%>
																					<input type="text" class="width80 attendanceDate" readonly="readonly" name="attendanceDate" id="attendanceDate_${status1.index+1}" style="border:0px;" 
																					value="<%=attendanceDate%>"/>
																				</c:when>
																				<c:otherwise>
																					<input type="text" class="width80 attendanceDate" readonly="readonly" name="attendanceDate" id="attendanceDate_${status1.index+1}" style="border:0px;"/>
																				</c:otherwise>
																		</c:choose> 
																	</td> 
																	<td>
																		<select class="width90 swipeInOutType" id="swipeInOutType_${status1.index+1}"style="border:0px;">
																		<c:choose>
																				<c:when test="${swp.timeInOutType ne null &&  swp.timeInOutType eq 'IN'}">
																					<option value="">Select</option>
																					<option value="IN" selected="selected">IN</option>
																					<option value="OUT">OUT</option>
																				</c:when>
																				<c:when test="${swp.timeInOutType ne null &&  swp.timeInOutType eq 'OUT'}">
																					<option value="">Select</option>
																					<option value="IN">IN</option>
																					<option value="OUT" selected="selected">OUT</option>
																				</c:when>
																		</c:choose>
																		</select>
																	</td> 
																	<td>
																		 <c:choose>
																				<c:when test="${swp.timeInOut ne null &&  swp.timeInOut ne ''}">
																					<c:set var="timeInOut" value="${swp.timeInOut}"/>  
																					<%String timeInOut = DateFormat.convertTimeToString(pageContext.getAttribute("timeInOut").toString());%>
																					<input type="text" class="width80 timeInOut" readonly="readonly" name="timeInOut" id="timeInOut_${status1.index+1}" style="border:0px;" 
																					value="<%=timeInOut%>"/>
																				</c:when>
																				<c:otherwise>
																					<input type="text" class="width80 timeInOut" readonly="readonly" name="timeInOut" id="timeInOut_${status1.index+1}" style="border:0px;"/>
																				</c:otherwise>
																		</c:choose> 
																	</td> 
																	<td style="display:none;">
																		<input type="hidden" id="swipeInOutId_${status1.index+1}" value="${swp.swipeInOutId}" style="border:0px;"/>
																		<input type="hidden" id="swipeId_${status1.index+1}" value="${swp.swipeId}" style="border:0px;"/>
																	</td>
																	 <td style="width:0.01%;" class="opn_td" id="optionS_${status1.index+1}">
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												 								<span class="ui-icon ui-icon-plus"></span>
																		  </a>	
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																				<span class="ui-icon ui-icon-wrench"></span>
																		  </a> 
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																				<span class="ui-icon ui-icon-circle-close"></span>
																		  </a>
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${status1.index+1}" style="display:none;" title="Working">
																				<span class="processing"></span>
																		  </a>
																	</td>
																</tr>
															</c:forEach>  
														</c:when>
													</c:choose>  
													<c:forEach var="i" begin="${fn:length(ATTENDANCE_SWIPE_LIST)+1}" end="${fn:length(ATTENDANCE_SWIPE_LIST)+2}" step="1" varStatus="status"> 
														<tr class="rowidS" id="fieldrowS_${i}">
															<td id="lineIdS_${i}" style="display:none;">${i}</td>
															
															<td>
																<input type="text" class="width80 attendanceDate" readonly="readonly" name="attendanceDate" id="attendanceDate_${i}" style="border:0px;"/>
															</td>
															<td>
																<select class="width90 swipeInOutType" id="swipeInOutType_${i}"style="border:0px;">
																	<option value="">Select</option>
																	<option value="IN">IN</option>
																	<option value="OUT">OUT</option>
																	
																</select>
															</td>  
															<td>
																<input type="text" class="width80 timeInOut"  name="timeInOut" id="timeInOut_${i}" style="border:0px;"/>
															</td> 
															<td style="display:none;">
																<input type="hidden" id="swipeInOutId_${i}" style="border:0px;"/>
																<input type="hidden" id="swipeId_${i}" style="border:0px;"/>
															</td>
															<td style="width:0.01%;" class="opn_td" id="optionS_${i}">
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${i}" style="display:none;cursor:pointer;" title="Add Record">
										 								<span class="ui-icon ui-icon-plus"></span>
																  </a>	
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${i}" style="display:none; cursor:pointer;" title="Edit Record">
																		<span class="ui-icon ui-icon-wrench"></span>
																  </a> 
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${i}" style="display:none;cursor:pointer;" title="Delete Record">
																		<span class="ui-icon ui-icon-circle-close"></span>
																  </a>
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${i}" style="display:none;" title="Working">
																		<span class="processing"></span>
																  </a>
															</td>    
														</tr>
												</c:forEach>
										 </tbody>
								</table>
							</div> 
							<div class="clearfix"></div>
							<div class="float-right buttons ui-widget-content ui-corner-all">
								<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.cancel"/></div>
								<div class="portlet-header ui-widget-header float-right save" id="save"><fmt:message key="organization.button.save"/></div>
							</div>
						</fieldset>
				 </div> 
 			</div>
 			
 		</form>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
	</div>
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrowsS" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
	</div>
</div>
</div>
