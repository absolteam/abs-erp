<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td{
	width:6%;
}
.width35{
	width:35%!important;
}
.width25{
	width:25%!important;
}
.width15{
	width:15%!important;
}
.margintop3{
	margin-top:3px;
}
#common-popup{
	overflow: hidden;
}
.width61{
	width:61% !important;
}
</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var allowanceLineDetail="";
var leaveLineDetail="";
var shiftLineDetail="";
var assignmentLineDetail="";
var commonRowId=0;
$(document).ready(function(){  
	
	editDataSelectCall();
	totalPaySum();
		
	$('.allowance').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id'));
			dublicatePayEntries(rowId);
			triggerAllowanceAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});
	
	$('.payPolicy').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id'));
			triggerAllowanceAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	$("#JOBDetails").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});  

	$('.amount').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAllowanceAddRow(rowId);
			totalPaySum();
			return false;
		} 
		else{
			return false;
		}
	});	
	//Leave onchange events
	
	
	$('.days').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerLeaveAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});	
	
	//Shift on change events
	$('.workingShiftId').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerShiftAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	

});
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}


function editDataSelectCall(){
	$("#status").val($("#statusTemp").val());
	$('.rowidC').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#allowance_"+rowId).val($("#allowanceId_"+rowId).val());
		$("#payPolicy_"+rowId).val($("#payPolicyId_"+rowId).val());
		$("#payType_"+rowId).val($("#payTypeId_"+rowId).val());
		$("#calculationType_"+rowId).val($("#calculationTypeId_"+rowId).val());
		$("#dependentElement_"+rowId).val($("#dependentElementId_"+rowId).val());
	});
	
	
	
	$('.rowidS').each(function(){
		var rowId=getRowId($(this).attr('id'));
		$("#shiftType_"+rowId).val($("#workingShiftId_"+rowId).val());
		
	});
	

	
	$('.leaveType').each(function(){
		var rowId=getRowId($(this).attr('id'));
		if($("#isMonthly_"+rowId).html().trim()=="true")
			$("#isMonthly_"+rowId).html("YES");
		else
			$("#isMonthly_"+rowId).html("NO");
		
		if($("#isYearly_"+rowId).html().trim()=="true")
			$("#isYearly_"+rowId).html("YES");
		else
			$("#isYearly_"+rowId).html("NO");
		
		if($("#isService_"+rowId).html().trim()=="true")
			$("#isService_"+rowId).html("YES");
		else
			$("#isService_"+rowId).html("NO");
		
	});
}
function dublicatePayEntries(rId){
	var arrPay = [];
	$('.allowance').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var allowance=$('#allowance_'+rowId).val();
		arrPay.push(allowance);
	});
	var sorted_arr = arrPay.sort(); // You can define the comparing function here. 
	for (var i = 0; i < arrPay.length - 1; i++) {
	    if (sorted_arr[i + 1] == sorted_arr[i]) {
	    	$('#allowance_'+rId).val("");
	        alert("Sorry! Duplicate Payroll Element");
	    	return false;
	    }
	}
}
function dublicateLeaveEntries(rId){
	var arrPay = [];
	$('.leaveType').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var allowance=$('#leaveType_'+rowId).val();
		arrPay.push(allowance);
	});
	var sorted_arr = arrPay.sort(); // You can define the comparing function here. 
	                             // JS by default uses a crappy string compare.
	for (var i = 0; i < arrPay.length - 1; i++) {
	    if (sorted_arr[i + 1] == sorted_arr[i]) {
	    	$('#leaveType_'+rId).val("");
	        alert("Sorry! Duplicate Leave");
	    	return false;
	    }
	}
}

function totalPaySum(){
	var totalPay=Number(0);
	$('.amount').each(function(){
		var rowId=getRowId($(this).attr('id'));
		totalPay+=convertToDouble($('#amount_'+rowId).val());
	});
	$('#totalPay').val(totalPay);
}
function personBankPopupCall(id,name,commaseparatedValue){
	$('#personBank_'+commonRowId).val(name);
	$('#personBankId_'+commonRowId).val(id);
}
function leavePopupResult(id,name,commaseparatedValue){
	$('#leaveType_'+commonRowId).val(name);
	$('#leaveId_'+commonRowId).val(id);
	var idval=commaseparatedValue.split('@#');
	$('#days_'+commonRowId).val(idval[1]);
	$('#isMonthly_'+commonRowId).val(idval[2]);
	$('#isYearly_'+commonRowId).val(idval[3]);
	$('#isService_'+commonRowId).val(idval[4]);
	triggerLeaveAddRow(commonRowId);
}
function designationPopupResult(id,name,commaseparatedValue){
	$('#designationId').val(id);
	var valueArray=new Array();
	valueArray=commaseparatedValue.split('@&');
	$('#designationName').val(valueArray[0]);
	$('#gradeName').val(valueArray[1]);
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Job Template</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="JOBDetails" class="" name="JOBDetails" method="post">
		  	<div class="width100 float-left" id="hrm">
					<div class="width100 float-left">
						<fieldset style="height: 100px;">
							<legend>Template Information</legend>
							<div class="width50 float-left">
								<div class="width100 float-left">
									<label class="width30" for="jobNumber">Reference Number<span
										class="mandatory">*</span>
									</label> <input type="text" maxlength="100" readonly="readonly"
										name="jobNumber" id="jobNumber" value="${JOB.jobNumber}"
										class="width60 validate[required]" />
								</div>
								<div class="width100 float-left">
									<label class="width30" for="jobName">Template Name<span
										class="mandatory">*</span>
									</label> <input type="text" maxlength="100" name="jobName" id="jobName"
										value="${JOB.jobName}" class="width60 validate[required]" />
								</div>
							</div>
							<div class="width50 float-left">

								
								<div class="width100 float-left">
									<label class="width40">Designation<span
										style="color: red">*</span> </label> <input type="text"
										readonly="readonly" id="designationName"
										class="masterTooltip width50 validate[required]"
										value="${JOB.designation.designationName}" />  <input
										type="hidden" name="designationId" id="designationId"
										value="${JOB.designation.designationId}" />
								</div>
								<div class="width100 float-left">
									<label class="width40">Grade<span
										style="color: red">*</span> </label> <input type="text"
										readonly="readonly" id=gradeName
										class="masterTooltip width50"
										value="${JOB.designation.grade.gradeName}" /> 
								</div>
								<div class="width100 float-left">
									<label class="width40"><fmt:message key="hr.job.status" />
									</label> <input type="hidden" name="statusTemp" id="statusTemp"
										value="${JOB.status}" /> <select tabindex="10" name="status"
										id="status" class="width50">
										<option value="1">Active</option>
										<option value="2">Inactive</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
 		</form>
			<fieldset style="min-height: 100px;">
					<legend>Pay Elements</legend>
					<div id="hrm" class="hastable width100 float-left"  >  
						<table id="hastab1" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.allowance"/></th> 
								    <th style="width:5%">Pay Policy</th>  
								    <th style="width:5%">Pay Mode</th>
								    <th style="width:5%"><fmt:message key="hr.job.amount"/>(Max)</th>
								    <th style="width:5%">Calculation</th>
								    <th style="width:5%">Count</th>
								    <th style="width:5%">Dependent Element</th>
							  </tr>
							</thead> 
									<tbody class="tabC">
										<c:choose>
											<c:when test="${JOB.jobPayrollElements ne null && JOB.jobPayrollElements ne '' && fn:length(JOB.jobPayrollElements)>0}">
												<c:forEach var="jobpay" items="${JOB.jobPayrollElements}" varStatus="status1">
													<tr class="rowidC" id="fieldrowC_${status1.index+1}">
														<td id="lineIdC_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<select class="width90 allowance" id="allowance_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
																	<option value="${element.payrollElementId}">${element.elementName}[${element.elementType}]</option>
																</c:forEach>
															</select>
															<input type="hidden" id="allowanceId_${status1.index+1}" value="${jobpay.payrollElementByPayrollElementId.payrollElementId}" style="border:0px;"/>
														</td> 
														<td>
															<select class="width90 payPolicy" id="payPolicy_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
																	<option value="${policy.key}">${policy.value}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="payPolicyId_${status1.index+1}" value="${jobpay.payPolicy}" style="border:0px;"/>
															
														</td> 
														<td>
															<select class="width90 payType" id="payType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="mode" items="${PAY_MODE}" varStatus="statusMode">
																	<option value="${mode.key}">${mode.value}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="payTypeId_${status1.index+1}" value="${jobpay.payType}" style="border:0px;"/>
														</td> 
														<td>
															<input type="text" class="width80 amount" id="amount_${status1.index+1}" value="${jobpay.amount}" style="border:0px;text-align: right;"/>
														</td> 
														
														<td>
															<select class="width90 calculationType" id="calculationType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="mode" items="${CALCULATION_TYPE}" varStatus="statusMode">
																	<option value="${mode.key}">${mode.value}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="calculationTypeId_${status1.index+1}" value="${jobpay.calculationType}" style="border:0px;"/>
														</td> 
														<td>
															<input type="text" class="width80 count" id="count_${status1.index+1}" value="${jobpay.count}" style="border:0px;text-align: right;"/>
														</td>
														<td>
															<select class="width90 dependentElement" id="dependentElement_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="element" items="${PAYROLL_ELEMENT_LIST}" varStatus="statusPay">
																	<option value="${element.payrollElementId}">${element.elementName}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="dependentElementId_${status1.index+1}" value="${jobpay.payrollElementByPercentageElement.payrollElementId}" style="border:0px;"/>
														</td> 
														<td style="display:none;">
															<input type="hidden" id="jobPayrollElementId_${status1.index+1}" value="${jobpay.jobPayrollElementId}" style="border:0px;"/>
														</td>
														 
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										
							 </tbody>
					</table>
					<div class="float-right width30" style="display: none;">
						<label class="float-left width30"><span><fmt:message key="hr.job.totalamount"/> :</span></label>
						<input type="text" readonly="readonly" id="totalPay" class="float-left width40"/>
					</div>
				</div> 
			</fieldset>
			<fieldset style="min-height: 100px;">
					<legend>Leaves</legend>
				<div id="hrm" class="hastable width100 float-left"  >  
						<table id="hastab2" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.leavetype"/></th> 
								    <th style="width:5%"><fmt:message key="hr.job.days"/></th> 
								    <th style="width:5%">Monthly</th> 
								    <th style="width:5%">Yearly</th>
								    <th style="width:5%">In Whole Service</th>
							  </tr>
							</thead> 
									<tbody class="tabL">
										<c:choose>
											<c:when test="${JOB.jobLeaves ne null && JOB.jobLeaves ne '' && fn:length(JOB.jobLeaves)>0}">
												<c:forEach var="jobleave" items="${JOB.jobLeaves}" varStatus="status1">
													<tr class="rowidL" id="fieldrowL_${status1.index+1}">
														<td id="lineIdL_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<%-- <select class="width90 leaveType" id="leaveType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="leavetype" items="${LEAVE_TYPE_LIST}" varStatus="statusL">
																	<c:if test="${leavetype.leaveId eq jobleave.leave.leaveId}">
																		<option value="${leavetype.leaveId}#${leavetype.defaultDays}" selected="selected">${leavetype.leaveType}</option>
																	</c:if>
																	<c:if test="${leavetype.leaveId ne jobleave.leave.leaveId}">
																		<option value="${leavetype.leaveId}#${leavetype.defaultDays}">${leavetype.leaveType}</option>
																	</c:if>
																</c:forEach>
															</select> --%>
															<input type="text" readonly="readonly" id="leaveType_${status1.index+1}" value="${jobleave.leave.lookupDetail.displayName}" class="width70 float-left leaveType"/>
															<span class="button float-right" id="leave_${status1.index+1}"  style="position: relative; top:6px;">
																<a style="cursor: pointer;" id="leavepopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 leaves-popup"> 
																	<span class="ui-icon ui-icon-newwin"> 
																	</span> 
																</a>
															</span> 
															<input type="hidden" id="leaveId_${status1.index+1}" value="${jobleave.leave.leaveId}" style="border:0px;"/>
														</td> 
														<td>
															 <input type="text" readonly="readonly" class="width80 days" id="days_${status1.index+1}" value="${jobleave.days}" style="border:0px;"/>
														</td> 
														<td id="isMonthly_${status1.index+1}">${jobleave.leave.isMonthly}</td>
														<td id="isYearly_${status1.index+1}">${jobleave.leave.isYearly}</td>
														<td id="isService_${status1.index+1}">${jobleave.leave.isService}</td>
														<td style="display:none;">
															<input type="hidden" id="jobLeaveId_${status1.index+1}" value="${jobleave.jobLeaveId}" style="border:0px;"/>
														</td>
														
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										
							 </tbody>
					</table>
				</div> 
				</fieldset>
		<!-- **********************************Job Shift tab-4********************************************************************************* -->
			<fieldset style="min-height: 100px;">
					<legend>Shifts</legend>
					<div id="hrm" class="hastable width100 float-left"  >  
						<table id="hastab2" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.shifttype"/></th> 
								    <th style="width:5%"><fmt:message key="hr.job.periodstartdate"/></th>  
								    <th style="width:5%"><fmt:message key="hr.job.periodenddate"/></th>
							  </tr>
							</thead> 
									<tbody class="tabS">
										<c:choose>
											<c:when test="${JOB_SHIFT_LIST ne null && JOB_SHIFT_LIST ne '' && fn:length(JOB_SHIFT_LIST)>0}">
												<c:forEach var="jobShift" items="${JOB_SHIFT_LIST}" varStatus="status1">
													<tr class="rowidS" id="fieldrowS_${status1.index+1}">
														<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															<%-- <select class="width90 shiftType" id="shiftType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach var="shifttyp" items="${SHIFT_TYPE_LIST}" varStatus="statusL">
																	<option value="${shifttyp.workingShiftId}">${shifttyp.workingShiftName}</option>
																</c:forEach>
															</select> --%>
															<span style="height: 20px;width:65%!important" id="shiftTypeName_${status1.index+1}" class="float-left width20">
															${jobShift.workingShift.workingShiftName}
															</span>
															<span class="button float-right" id="shiftType_${status1.index+1}"  style="position: relative; top:4px;">
																<a style="cursor: pointer;" id="shiftpopup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 shiftType-popup"> 
																	<span class="ui-icon ui-icon-newwin">
																	</span> 
																</a>
															</span> 
															<input type="hidden" id="workingShiftId_${status1.index+1}" value="${jobShift.workingShift.workingShiftId}" class="workingShiftId"  style="border:0px;"/>
														</td> 
														<td>
															 <c:choose>
																	<c:when test="${jobShift.effectiveStartDate ne null &&  jobShift.effectiveStartDate ne ''}">
																		<c:set var="startDate" value="${jobShift.effectiveStartDate}"/>  
																		<%String startDate = DateFormat.convertDateToString(pageContext.getAttribute("startDate").toString());%>
																		<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${status1.index+1}" style="border:0px;" 
																		value="<%=startDate%>"/>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="width80 startDate" readonly="readonly" name="startDate" id="startDate_${status1.index+1}" style="border:0px;"/>
																	</c:otherwise>
															</c:choose> 
														</td> 
														<td>
															 <c:choose>
																	<c:when test="${jobShift.effectiveEndDate ne null &&  jobShift.effectiveEndDate ne ''}">
																		<c:set var="endDate" value="${jobShift.effectiveEndDate}"/>  
																		<%String endDate = DateFormat.convertDateToString(pageContext.getAttribute("endDate").toString());%>
																		<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${status1.index+1}" style="border:0px;" 
																		value="<%=endDate%>"/>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="width80 endDate" readonly="readonly" name="endDate" id="endDate_${status1.index+1}" style="border:0px;"/>
																	</c:otherwise>
															</c:choose> 
														</td> 
														<td style="display:none;">
															<input type="hidden" id="jobShiftId_${status1.index+1}" value="${jobShift.jobShiftId}" style="border:0px;"/>
														</td>
														 
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										
							 </tbody>
					</table>
				</div> 
		</fieldset>
		</div>
		<%-- <div class="clearfix"></div>
		<div class="float-right buttons ui-widget-content ui-corner-all">
			<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right save" id="job_save"><fmt:message key="organization.button.save"/></div>
		</div> --%>
	</div>
	
</div>
	

