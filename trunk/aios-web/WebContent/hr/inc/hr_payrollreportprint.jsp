<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script
	src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"
	type="text/javascript">
	
</script>

<script
	type="text/javascript">
	$(function(){
		$(".report-print").click(function(){
			window.close();

			 window.open('<%=request.getContextPath()%>/payroll_report_printout.action?printCall='+true,
	    				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		});
		
	});
	
</script>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>${PAYROLL_MASTER.companyName} - SALARY REPORT</title>
 	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
@media print{
	.hideWhilePrint {display: none;}
}
</style>
	<c:if test="${requestScope.printCall eq true}">
		<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	</c:if>
	<c:if test="${requestScope.printCall eq false}">
		<body style="margin-top: 15px; font-size: 12px;">
	</c:if>
	<div class="width100 float-left">
		<span class="heading">${PAYROLL_MASTER.companyName}</span>
		<div style="float: right;"> <a target="_blank" class="hideWhilePrint" style="padding: 0px 15px;cursor: pointer;"><img
			alt="" title="Print" class="report-print"
			src="./images/print_icon.png" height="35" width="35" />
		</a>
		</div>
	</div>
	<div class="width30 float-right"><span class="side-heading">Printed on :${PAYROLL_MASTER.createdDateStr}</span></div>
	
	<div class="width100 float-left"><span class="side-heading">Salary Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:set var="countCol" value="10"></c:set>
	<c:choose>
		<c:when test="${fn:length(PAYROLL_DETAILS)gt 0}">
			<c:forEach var="entry" items="${PAYROLL_MASTER.payrollCodeMaps}" varStatus="status9">
				<c:set var="countCol" value="${countCol+status9.index+1}"></c:set>
			</c:forEach>
			<table class="width100">	
				<thead>		
					<tr>
						<th colspan="${countCol}">Month : ${selectedMonth},${selectedYear}</th>
					</tr> 							
					<tr>
						<th>S.No.</th>
						<th>Employee Name</th>
						<th>Designation</th>
						<th>Basic</th>
						<c:forEach var="entry" items="${PAYROLL_MASTER.payrollCodeMaps}">
						 <th> <c:out value="${entry.value}"/></th>
						</c:forEach>
						<th>Gross Pay</th>
						<th>Total days</th>
						<th>Total Earning</th>
						<th>Total Deduction</th>
						<th>Net Pay</th>
						<th style="width:15%;">Signature</th>
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${PAYROLL_DETAILS}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${status1.index+1}</td>
					 		<td>${result3.employeeName}</td>
					 		<td>${result3.designation}</td>
					 		<td>${result3.basic}</td>
					 		<c:forEach var="entry" items="${PAYROLL_MASTER.payrollCodeMaps}">
					 			<td>
					 			<c:forEach items="${result3.jobPayrollElements}" var="elmCod" varStatus="status5" >
					 				<c:if test="${entry.key eq elmCod.payrollElementByPayrollElementId.elementCode}">
					 					${elmCod.amount}
					 				</c:if>
					 			</c:forEach>
					 			</td>
							</c:forEach>
							<td>${result3.grossPay}</td>
							<td>${result3.numberOfDays}</td>
							<td>${result3.otherEarning}</td>
							<td>${result3.otherDeduction}</td>
							<td>${result3.netPay}</td>
							<td></td>
						</tr>
					</c:forEach>
						<tr style="font-weight: bold;border: 4px;">
							<td></td>
							<td colspan="1" style="font-weight: bold;font-size: 14px;">Total Salary:</td>
					 		<td></td>
					 		<td style="font-weight: bold;font-size: 14px;">${PAYROLL_MASTER.totalBasic}</td>
					 		<c:forEach var="entry" items="${PAYROLL_MASTER.payrollCodeMaps}">
					 			<td></td>
							</c:forEach>
							<td style="font-weight: bold;font-size: 14px;">${PAYROLL_MASTER.totalGross}</td>
							<td></td>
							<td style="font-weight: bold;font-size: 14px;">${PAYROLL_MASTER.totalEarning}</td>
							<td style="font-weight: bold;font-size: 14px;">${PAYROLL_MASTER.totalDeduction}</td>
							<td style="font-weight: bold;font-size: 14px;">${PAYROLL_MASTER.totalNet}</td>
							<td></td>
						</tr>
				</tbody>
			</table>
			<table class="width100">
					<tr>
						<td style="height: 100px;" colspan="4"></td>
					<tr>		
					<tr>
						<td style="border-top: 1px;margin-right: 2%;font-weight: bold; ">General Manager</td>
						<td style="border-top: 1px;margin-right: 2%;font-weight: bold; ">Operation Manager</td>
						<td style="border-top: 1px;margin-right: 2%;font-weight: bold; ">HR Manager</td>
						<td style="border-top: 1px;margin-right: 2%;font-weight: bold; ">Finance Manager</td>
					<tr>	
			</table>		
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Records Found</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>