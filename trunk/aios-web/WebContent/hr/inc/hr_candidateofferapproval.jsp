<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count {
	padding: 0 !important;
}
</style>

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 
	var jobId=$('#jobId').val();
	if(jobId!=null && jobId>0)
		loadOfferInformation();
	
	 $('#jobpopup').click(function() { 
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var designationId=$('#designationId').val();
         var rowId=0; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_job_list.action",
               data:{id:rowId,designationId:designationId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
 
	 $('#publishButton').click(function() { 
		
   }); 
		
		
	 

		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:450,
			bgiframe: false,
			modal: true 
		});
			
		 //$('#createdDate').datepick();
		//$('#interviewTime').timepicker({});
		 
	});
	
function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	if(returnVO.returnStatusName=='AddApproved' 
			|| returnVO.returnStatusName=='ModificationApproved'
			|| returnVO.returnStatusName=='DeleteApproved'){
		 var errorMessage=null;
		 var successMessage="Successfully Created";

			var candidateOfferId=$('#candidateOfferId').val();
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/approve_offe_letter.action",
               data:{candidateOfferId:candidateOfferId},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){ 
					var message=result.trim();
					 if(message=="SUCCESS"){
						 //returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error:function(result){ 
					errorMessage=result.trim();
					
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				}
           });
	}
}	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/candidate_offer_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	
	function jobPopupResult(id,name,commaseparatedValue){
		$('#jobName').val(name);
		$('#jobId').val(id);
		loadOfferInformation();
	}
	
	function loadOfferInformation(){
		var jobId=$('#jobId').val();
		var candidateOfferId=$('#candidateOfferId').val();
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_offer_letter.action", 
			 	async: false,
			    dataType: "html",
			    data:{jobId:jobId,candidateOfferId:candidateOfferId},
			    cache: false,
				success:function(result){ 
					$("#offer-letter-div").html(result);  
				}
		 });
	}

	
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Offer Letter Approval
		
		</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="interview-details">
						<div class="width100 float-left view-content" id="hrm">
							<div class="width100 float-left">
							
								<input type="hidden" name="candidateOfferId"
									id="candidateOfferId" value="${CANDIDATE_OFFER.candidateOfferId}"
									class="width50 " />
						
								<fieldset style="min-height: 50px;">
									<legend>Candidate Information</legend>
									<div class="width50 float-left">
										<div class="width100 float-left">
											<label class="width50 float-left">Candidate:</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.personByPersonId.firstName}
												${CANDIDATE_OFFER.personByPersonId.lastName}</label>
										</div>
										<div class="width100 float-left">
											<label class="width50 float-left">Position Reference No. :</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.openPosition.positionReferenceNumber}</label>
										</div>
										<div class="width100 float-left">
											<label class="width50 float-left">Recruitment Source:</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.recruitmentSource.providerName}</label>
										</div>
										
									</div>
									<div class="width50 float-left">
										<div class="width100 float-left">
											<label class="width30 float-left">Expected Joining Date:</label> <label
												class="width50 float-left">${CANDIDATE_OFFER.expectedJoiningDateDisplay}</label>

										</div>
									</div>
									<div class="width50 float-left">
									<div class="width100 float-left">
						      	  		<label class="width30 float-left">Job Template:</label>
						      	  		<label class="width50 float-left">${CANDIDATE_OFFER.candidateOffer.job.jobName} [${CANDIDATE_OFFER.candidateOffer.job.jobNumber}]</label>
										
										
										<input type="hidden" id="jobId" value="${CANDIDATE_OFFER.candidateOffer.job.jobId}" class="jobId" />
									</div>
										
									</div>
									<div class="width50 float-left">
										
										<div class="width100 float-left">
											<label class="width30 float-left">Comments:</label>
											<label class="width50 float-left">${CANDIDATE_OFFER.candidateOffer.comments}</label>
										</div>
									</div>
								</fieldset>
							
							</div>
							

						</div>

					</form>
					
					<div class="clearfix"></div>
					<div id="offer-letter-div" class="width100 float-left"></div>
					
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup" class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

				

			</div>
		</div>
	</div>
</body>
 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
</html>