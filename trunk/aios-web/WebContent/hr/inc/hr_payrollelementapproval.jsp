<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>

<script type="text/javascript"> 

$(document).ready(function(){ 

		var payrollElementId=Number($("#payrollElementId").val());
		
		if(payrollElementId!=null && payrollElementId>0){
			$("#reverseElementId").val($("#reverseElementIdTemp").val());
		}

		$jquery("#payroll-details").validationEngine('attach');
		
		//Combination pop-up config
		$('.codecombination-popup').click(function(){ 
		      tempid=$(this).parent().get(0);  
		      $('.ui-dialog-titlebar').remove();   
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/combination_treeview.action", 
				 	async: false,  
				    dataType: "html",
				    cache: false,
					success:function(result){   
						 $('.codecombination-result').html(result);  
					},
					error:function(result){ 
						 $('.codecombination-result').html(result); 
					}
				});  
		}); 
		
		 $('#codecombination-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800, 
				bgiframe: false,
				modal: true 
			});
			
			$('#discard').click(function(){ 
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/payroll_elements.action", 
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){ 
							$("#main-wrapper").html(result);  
						}
				 });
				 return false;
			 });

			 $('#save').click(function(){
				 $('.error,.success').hide();
				 if($jquery("#payroll-details").validationEngine('validate')){   
					 var successMessage="Successfully Created";
					 if(payrollElementId>0)
						 successMessage="Successfully Modified";
					 var combinationId=Number($("#combinationId").val());
					 
					 if(combinationId==0 || combinationId=='' || combinationId=='NaN'){
						 $('#page-error').hide().html("Please choose the account code").slideDown(1000);
						 return false;
					 }
						 
					 
					
					 var elementName=$("#elementName").val();
					 var elementNature=$("#elementNature").val();
					 var elementType=$("#elementType").val();
					 var reverseElementId=$("#reverseElementId").val();
						
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/save_payroll_elements.action", 
							data:{
									payrollElementId:payrollElementId,combinationId:combinationId,
									elementName:elementName,elementNature:elementNature,elementType:elementType,
									reverseElementId:reverseElementId
								},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){ 
								$(".tempresult").html(result);
								 var message=$('.tempresult').html(); 
								 if(message.trim()=="SUCCESS"){
									 $.ajax({
											type:"POST",
											url:"<%=request.getContextPath()%>/payroll_elements.action", 
										 	async: false,
										    dataType: "html",
										    cache: false,
											success:function(result){
												$("#main-wrapper").html(result); 
												$('.success').hide().html(successMessage).slideDown(1000);
											}
									 });
								 }
								 else{
									 $('#page-error').hide().html(message).slideDown(1000);
									 return false;
								 }
							}
					 });
				 }
				 else{
					return false;
				}
				 return false;
			 });
			 
			
		    
	});
function setCombination(combinationTreeId,combinationTree){
	$('#combinationId').val(combinationTreeId);
	$('.teneant_account').val(combinationTree);
}
</script>

<body>
	<div id="main-content">	
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Payroll Element Information</div>
		<div class="portlet-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div id="page-error" class="response-msg error ui-corner-all" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
			
			<div class="width98 float-left" id="hrm">
			 <form id="payroll-details" name="payroll-details" style="position: relative;">
			 	<div class="width100 float-left" id="hrm"> 
			 		<fieldset style="height:140px;">
							<legend>Payroll Element Information</legend>
						
							<div class="width50 float-left">
								<input type="hidden" id="payrollElementId" value="${PAYROLL_ELEMENT.payrollElementId}"/>
								<div>
									<label class="width30" for="elementCode">Element Code<span class="mandatory">*</span></label>
									<input type="text" id="elementCode" class="width60 validate[required]" disabled="disabled" value="${PAYROLL_ELEMENT.elementCode}"/>
								</div> 
								<div>
									<label class="width30" for="elementName">Element Name<span class="mandatory">*</span></label>
									<input type="text" id="elementName" maxlength="150" class="width60 validate[required]" value="${PAYROLL_ELEMENT.elementName}"/>
								</div>  
								<div>
									<label class="width30" for="elementNature">Element Nature<span class="mandatory">*</span></label>
									<input type="text" id="elementNature" maxlength="150" disabled="disabled" class="width60 validate[required]" value="${PAYROLL_ELEMENT.elementNature}"/>
								</div>   
								<div>
									<label class="width30" for="elementType">Element Type<span class="mandatory">*</span></label>
									<input type="text" id="elementType" maxlength="150" disabled="disabled" class="width60 validate[required]" value="${PAYROLL_ELEMENT.elementType}"/>
								</div>
								
							</div>
							<div class="width48 float-left">
								
								<div>
									<label class="width30" for="reverseElementId">Conversion Element</label>
									<select class="width61 reverseElementId" id="reverseElementId" >
										<option value="">Select</option>
										<c:forEach var="policy" items="${REVERSE_ELEMENT}" varStatus="statusPolicy">
											<option value="${policy.payrollElementId}">${policy.elementName}</option>
										</c:forEach>
									</select>
									<input type="hidden" id="reverseElementIdTemp" value="${PAYROLL_ELEMENT.payrollElement.payrollElementId}" />
								</div>
								<div>
									<label class="width30" for="accountCode">Account Code<span class="mandatory">*</span></label>
									<input type="text" class="width60 teneant_account validate[required]"
			                      		value="${COMBINATION_NAME}"/>
				                      	<span class="button" style="position: relative;">
											<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width10 codecombination-popup"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span>
										<input type="hidden" name="combinationId" id="combinationId" value="${COMBINATION_ID}"/> 
								</div>  
								
							</div>
						</fieldset>
				 	</div> 
				</form>
				<div class="clearfix"></div> 
			
			</div>
					
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="codecombination-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 		</div> 
		</div>
	</div>
</div>
</body>
</html>