<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count{
	padding:0!important;
}
</style> 

<script type="text/javascript"> 
var shiftLineDetail="";
var accessCode=null;
$(document).ready(function(){ 

		var recruitmentSourceId=Number($("#recruitmentSourceId").val());
		if(recruitmentSourceId>0){
			
			autoSelectTheMultiList("sourceType","resourceList");
			
			
			populateUploadsPane("doc","uploadedDocs","RecruitmentSource",recruitmentSourceId);
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","uploadedDocs","RecruitmentSource",recruitmentSourceId,"ConsultancyDocuments");
			});
		}else{
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","uploadedDocs","RecruitmentSource","-1","ConsultancyDocuments");
			});
		}
		
		$('#add-location').click(function(){
	      	 $('.ui-dialog-titlebar').remove();  
	           $('#job-common-popup').dialog('open');
	         
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/get_add_location.action",
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){

	                   $('.job-common-result').html(result);
	                   return false;
	              },
	              error:function(result){
	                   $('.job-common-result').html(result);
	              }
	          });
	           return false;
	       
	  	}); 
		
		$('.location-popup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
	         $('#job-common-popup').dialog('open');
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/get_location_for_setup.action",
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){

	                   $('.job-common-result').html(result);
	                   return false;
	              },
	              error:function(result){

	                   $('.job-common-result').html(result);
	              }
	          });
	           return false;
	  	}); 
		
	   
	   $('.recruitment-lookup').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
	  
		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
		
		 $('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:550,
				bgiframe: false,
				modal: true 
			});

	
		$jquery("#consultancy-details").validationEngine('attach');
			
		$('#consultancy-discard').click(function(){ 
			returnCallTOList();
			
		 });
		

	 $('#consultancy-save').click(function(){
		 $('.error,.success').hide();
		 //$("#loan-form-submit").trigger('click');
		 //$("#loan-details").ajaxForm({url: 'employee_loan_save.action', type: 'post'});
		 	
	 				var errorMessage=null;
					var successMessage="Successfully Created";
					if(recruitmentSourceId>0)
						successMessage="Successfully Modified";

					try {
						if ($jquery("#consultancy-details").validationEngine('validate')) {
							//generateDataToSendToDB("responsibilityList","responsibility");
							
						
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/recruitment_source_save.action", 
							 	async: false,
							 	data: $("#consultancy-details").serialize(),
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									var message=result.trim();
									 if(message=="SUCCESS"){
										 returnCallTOList();
									 }else{
										 errorMessage=message;
									 }
									 
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								},
								error:function(result){ 
									errorMessage=result.trim();
									
									 if(errorMessage==null)
											$('.success').hide().html(successMessage).slideDown();
										else
											$('.error').hide().html(errorMessage).slideDown();
								}
						 });
						} else {
							//$('.error').hide().html("Please enter all required information").slideDown(1000);
							//event.preventDefault();
							return false;
						}
					} catch (e) {

					}

				});
	 
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="RECRUITMENT_RESOURCES"){
				$('#resourceList').html("");
				$('#resourceList').append("<option value=''>--Select--</option>");
				loadLookupList("resourceList");
				
			}
			
		});
		
			
		 $('.startDate,.endDate').datepick({
			 onSelect: customRanges,showTrigger: '#calImg'});
		 
	});
	
	
	function returnCallTOList(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/recruitment_source_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
				}
		 });
	}

	
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
				data:{accessCode:accessCode},
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(response){
					
					$(response.lookupDetails)
							.each(
									function(index) {
										$('#'+id)
												.append(
														'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
						});
				}
			});
	}
	function customRanges(dates) {
		if (this.id == 'startDate') {
			$('#endDate').datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#startDate').datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				return false;
			} 
			else{
				return false;
			}
		} 
	}
	function generateDataToSendToDB(source,target){
		var typeList=new Array();
		var ids="";
		$('#'+source+' option:selected').each(function(){
			typeList.push($(this).val());
		});
		for(var i=0; i<typeList.length;i++){
			if(i==(typeList.length)-1)
				ids+=typeList[i];
			else
				ids+=typeList[i]+",";
		}
		
		$('#'+target).val(ids);
	}
	
	function autoSelectTheMultiList(source,target) {
		 var idsArray=new Array();
		 var idlist=$('#'+source).val();
		 idsArray=idlist.split(',');
		$('#'+target+' option').each(function(){
			var txt=$(this).val().trim();
			if($.inArray(txt, idsArray)>=0)
				$(this).attr("selected","selected");
		});

	}
	
	function locationGenericResultCall(locationId,locationName,multipleValues){
		$('#locationId').val(locationId);
		var valueArray=new Array();
		valueArray=multipleValues.split('@&');
		$('#address').val(valueArray[1]);
		
	}
</script>

<body>
	<div id="main-content">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Recruitment Source</div>
		<div class="portlet-content">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div id="page-error" class="response-msg error ui-corner-all"
					style="display: none;">
					<span>Process Failure!</span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<div class="width100 float-left" id="hrm">
					<form id="consultancy-details" method="post" style="position: relative;">
						<div class="width100 float-left" id="hrm">
							<div class="width100 float-left">
								<input type="hidden"
									name="recruitmentSource.recruitmentSourceId"
									id="recruitmentSourceId"
									value="${RECRUITMENT_SOURCE.recruitmentSourceId}"
									class="width50" />
									<input type="hidden"
									name="companyId"
									id="companyId"
									class="width50" />
								<fieldset style="min-height: 200px;">
									<legend>Consultancy Information</legend>
									<div id="hrm" class="width50 float-left">
										<div>
											<label class="width30">Provider Name<span
												style="color: red">*</span></label> <input
												type="text" id="providerName" class="width60 validate[required]"
												name="recruitmentSource.providerName"
												value="${RECRUITMENT_SOURCE.providerName}" />
										</div>
										<div>
											<label class="width30">Source Type<span
												style="color: red">*</span> </label> <input type="hidden"
												name="sourceType"
												id="sourceType"
												value="${RECRUITMENT_SOURCE.lookupDetail.lookupDetailId}" />
											<div>
												<select id="resourceList" name="recruitmentSource.lookupDetail.lookupDetailId"
													class="width60 validate[required]">
													<option value="">--Select--</option>
													<c:forEach items="${RECRUITMENT_RESOURCES}" var="nltls">
														<option value="${nltls.lookupDetailId}">${nltls.displayName}
														</option>
													</c:forEach>
												</select>
												<span class="button" id="RECRUITMENT_RESOURCES_1"
													style="position: relative; "> <a
													style="cursor: pointer;" id="RECRUITMENT_RESOURCES"
													class="btn ui-state-default ui-corner-all recruitment-lookup width10">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</div>
											
										</div>
										<div>
											<label class="width30">Address </label> <textarea id="address"
												class="width60" >${RECRUITMENT_SOURCE.fullAddress}</textarea><span
												class="button float-right" id="location"
												style="position: relative;top:10px;left:-10px;"> <a
												style="cursor: pointer;" id="locationpopup"
												class="btn ui-state-default ui-corner-all width10 location-popup">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
												type="hidden" name="recruitmentSource.location.locationId" id="locationId"
												class="locationId" value="${RECRUITMENT_SOURCE.location.locationId}" />
										</div>
										<div>
											<div 
												class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
												<div
													class="portlet-header ui-widget-header float-right add-location"
													id="add-location">Add More Address</div>
											</div>
										</div>
									</div>
									<div id="hrm" class="width40 float-left">
										<div>
											<label class="width30">Manager </label> <input type="text"
												id="manager" class="width60"
												name="recruitmentSource.manager"
												value="${RECRUITMENT_SOURCE.manager}" />
										</div>
										
										<div>
											<label class="width30">Website </label> <input type="text"
												id="website" class="width60"
												name="recruitmentSource.website"
												value="${RECRUITMENT_SOURCE.website}" />
										</div>
										<div>
											<label class="width30">Login </label> <input type="text"
												id="loginId" class="width60"
												name="recruitmentSource.loginId"
												value="${RECRUITMENT_SOURCE.loginId}" />
										</div>
										<div>
											<label class="width30">Password </label> <input type="text"
												id="password" class="width60"
												name="recruitmentSource.password"
												value="${RECRUITMENT_SOURCE.password}" />
										</div>
										<div>
											<label class="width30">Active </label>
											<c:choose>
											<c:when test="${RECRUITMENT_SOURCE.isActive eq true}">
												<input type="checkbox" id="isActive" class="width8"
													name="recruitmentSource.isActive" checked="checked"
													/>
											</c:when>
											<c:otherwise>
												<input type="checkbox" id="isActive" class="width8"
													name="recruitmentSource.isActive"
													 />
											</c:otherwise>
											</c:choose>
										</div>
									</div>
								</fieldset>

									
							</div>

							<div id="hrm" class="float-left width100">
								<fieldset>
									<legend>Document Uploads</legend>
									<div id="dms_document_information"
										style="cursor: pointer; color: blue;">
										<u><fmt:message key="re.property.info.uploadDocsHere" />
										</u>
									</div>
									<div
										style="padding-top: 10px; margin-top: 3px; height: 150px; overflow: auto;">
										<span id="uploadedDocs"></span>
									</div>
								</fieldset>

							</div>
						</div>

					</form>
					<div class="clearfix"></div>
					
					
				</div>



				<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>

		
			</div>
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
</body>
</html>