<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" /> 
<style>
.ui-autocomplete-input {
	width: 55%!important;
}
</style>
<script type="text/javascript">

$(document).ready(function(){  
	$('.autocompletecode').combobox({ 
		 selected: function(event, ui){ 
			 
		 }
	 });
	//apply validation required in auto complete
	 $("#country").next().addClass("validate[required]");
	 $("#state").next().addClass("validate[required]");
	 $("#city").next().addClass("validate[required]");
	//$('#country').next().find('input').addClass("validate[required]");
	
		var locationId=Number($('#loc_LocationId').val());
		if(locationId!=null && locationId>0){
			$('#state').val("");
			$('#city').val("");
		}
	
		$('.discard-location').click(function(){
			$('#job-common-popup').dialog("close");
		 });
		
		$jquery("#SAVE_LOCATION").validationEngine('attach');

		// Ajax Call for Adding Person Details
		$('.save-location').click(function(){
			 $('.error,.success').hide();
		
			if($jquery("#SAVE_LOCATION").validationEngine('validate'))
		    {
				var companyId=Number($('#companyId').val());
				var locationName=$('#locationName').val();
				var countryId=Number($('#country').val());
				var stateId=Number($('#state').val());
				var cityId=Number($('#city').val());
				var address=$('#addressLine1').val();
				var area=$('#uae_area').val();
				if($('#area_other').val()!=null && $('#area_other').val().trim()!='')
					area=$('#area_other').val();
				var zone=$('#uae_zone').val();
				if($('#zone_other').val()!=null && $('#zone_other').val().trim()!='')
					zone=$('#zone_other').val();
				var postalCode=$('#postalCode').val();
				var contactNumber=$('#contactNumber').val();
				var description=$('#description').val();
				var locationId=Number($('#loc_LocationId').val());
				$.ajax({ 
					type: "POST", 
					url: "<%=request.getContextPath()%>/save_location.action", 
					data: {
							locationId:locationId,
							companyId:companyId,
							locationName:locationName,
							countryId:countryId,
							stateId:stateId,
							cityId:cityId,
							address:address,
							area:area,
							zone:zone,
							postalCode:postalCode,
							contactNumber:contactNumber,
							description:description
					},
					async: false,  
				    dataType: "html",
				    cache: false,
					success: function(result){ 
						 $(".tempresult-popup").html(result);
						 var message=$('.tempresult-popup').html(); 
						 if(message.trim()=="SUCCESS"){
							 $('#job-common-popup').dialog("close");
							 $('.success').hide().html("Location saved successfully.").slideDown(1000);
						 }else{
							 if(message.trim()!='')
							 	$('.error').hide().html(message).slideDown(1000);
							 else
								$('.error').hide().html("Save failure, Location Information not saved").slideDown(1000); 
						 }
					},
					error: function(result){
						$('.error').hide().html("Save failure, Location Information not saved").slideDown(1000);   
					}
				});
			
		    }else{
				return false;
			}
		});
		
		///---------------------------country , state & city process
		$('#uae_zone').change(function(){
			if ($('#uae_zone').val() == "other") {
				
				$('#zone_other_div').show();				
			} else {

				$('#zone_other_div').hide();
			}

			//if(editing != "true") {
				if($('#uae_zone').val() != "" && $('#uae_zone').val() != "other" ) {
		
					
					
				} else if($('#uae_zone').val() == "other") {
		
					
				}
		});
		
		$('#uae_area').change(function(){
			if($('#city').val() == "6764") {

				$('#area_other_div').hide();
				$('#zone_other_div').hide();
				
				if($('#uae_area').val() == "KCA - Khalifa City A") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="SE - 4">SE - 4</option>').val('SE - 4');
					
				} else if ($('#uae_area').val() == "MBZ - Mohammad Bin Zayyed City") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="Z5">Z5</option>').val('Z5');
					
				} else if ($('#uae_area').val() == "SH - Shahama") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="S 9/10">S 9/10</option>').val('S 9/10');
					
				} else if ($('#uae_area').val() == "MBZ- Mussafah Shabia") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="ME - 9">ME - 9</option>').val('ME - 9');
					$('#uae_zone').append('<option value="ME - 10">ME - 10</option>');
					$('#uae_zone').append('<option value="ME - 11">ME - 11</option>');
					$('#uae_zone').append('<option value="ME - 12">ME - 12</option>');
					
				} else if ($('#uae_area').val() == "ALM - Al Mushrif") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="AUH W-22">AUH W-22</option>').val('AUH W-22');
					
				} else if ($('#uae_area').val() == "ALK - Al Khubirat Villa") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="AUH W24-01 PLOT 25">AUH W24-01 PLOT 25</option>').val('AUH W24-01 PLOT 25');
					
				} else if ($('#uae_area').val() == "MRC - Marina Royal Compound") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="AUH W42">AUH W42</option>').val('AUH W42');
					
				} else if ($('#uae_area').val() == "EL - Eldarado") {
					
					$('#uae_zone').find('option').remove().end().append('<option value="AUH E-10">AUH E-10</option>').val('AUH E-10');
					
				}else {

					$('#uae_zone').find('option').remove().end().append('<option value="Zone 1">Zone 1</option>').val('Zone 1');
					$('#uae_zone').append('<option value="Zone 2">Zone 2</option>');
					$('#uae_zone').append('<option value="Zone 3">Zone 3</option>');
				}
				$('#uae_zone').append('<option value="other">Other</option>');
			} else if ($('#city').val() == "55000") {

				$('#area_other_div').hide();
				$('#zone_other_div').hide();
				
				$('#uae_zone').find('option').remove().end().append('<option value="Zone 1">Zone 1</option>').val('Zone 1');
				$('#uae_zone').append('<option value="Zone 2">Zone 2</option>');
				$('#uae_zone').append('<option value="Zone 3">Zone 3</option>');
				$('#uae_zone').append('<option value="other">Other</option>');			
			}		

			if ($('#uae_area').val() == "other") {			
				$('#area_other_div').show();							
			}
		});
		
		$('.city').combobox({ 
			 selected: function(event, ui){ 
			
			$('#area_other_div').hide();
			$('#zone_other_div').hide();
			
			if($('#city').val() == "6764") {

				$('#uae_area').find('option').remove().end().append('<option value="KCA - Khalifa City A">KCA - Khalifa City A  / ????? ????? </option>').val('KCA - Khalifa City A');
				$('#uae_area').append('<option value="MBZ - Mohammad Bin Zayyed City">MBZ - Mohammad Bin Zayyed City / ???? ?? ???? </option>');
				$('#uae_area').append('<option value="SH - Shahama">SH - Shahama / ??????? </option>');
				$('#uae_area').append('<option value="RH - Rahba">RH - Rahba / ?????? </option>');
				$('#uae_area').append('<option value="MBZ- Mussafah Shabia">MBZ- Mussafah Shabia / ?????? ?????? </option>');
				$('#uae_area').append('<option value="ALM - Al Mushrif">ALM - Al Mushrif / ?????? </option>');
				$('#uae_area').append('<option value="ALK - Al Khubirat Villa">ALK - Al Khubairat Villa / ?? ???????? ???? </option>');
				$('#uae_area').append('<option value="MRC - Marina Royal Compound">MRC - Marina Royal Compound / ?????? ????? ?????? </option>');
				$('#uae_area').append('<option value="EL - Eldorado">EL - Eldorado / ???????? </option>');
				
				$('#uae_area').val("${addressLine1}");
				$('#uae_area').trigger('change');
				
				$('#uae_zone').val("${addressLine2}");
				$('#uae_area').append('<option value="other">Other</option>');
			    
			} else if($('#city').val() == "55000") {

				$('#uae_area').find('option').remove().end().append('<option value="ADI - Abu Dhabi Island">ADI - Abu Dhabi Island / ????? ??? ??? </option>').val('ADI - Abu Dhabi Island');
				$('#uae_area').append('<option value="ALR - AL Reem Island">ALR - AL Reem Island / ????? ????? </option>');

				$('#uae_area').val("${addressLine1}");
				$('#uae_area').trigger('change');
				
				$('#uae_zone').val("${addressLine2}");
				$('#uae_area').append('<option value="other">Other</option>');
			}
			}
		});

		$('#city').trigger('change');
		
		//Get State List On Change Country List
		$('#country').combobox({ 
			 selected: function(event, ui){ 
			countryId=Number($('#country').val());
			if(countryId == 252) {
				
				//$('#address1').hide();	
				//$('#address2').hide();
				$('#area_combo').show();
				$('#zone_combo').show();
				$('#addressLine1').removeClass("validate[required]");
				//$('#addressLine2').removeClass("validate[required]");
				
			} else {

				$('#address1').show();	
				$('#address2').show();
				$('#area_combo').hide();
				$('#zone_combo').hide();
				$('#area_other_div').hide();
				$('#zone_other_div').hide();
				$('#addressLine1').addClass("validate[required]");
			}
			
			$.ajax({
				type: "GET", 
				url: "<%=request.getContextPath()%>/get_country_states.action", 
		     	async: false,
		     	data:{countryId: countryId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#state").html(result); 
				
				}, 
				error: function() {
				}
			}); 	
			
		
		}
	}); 
});

</script>			
<div id="main-content" class="hr_main_content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Location Information</div>
             
        <div class="portlet-content">
        
        	<div id="page-error-popup" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult-popup" style="display:none;"></div>
			<form name="SAVE_LOCATION" id="SAVE_LOCATION" style="position: relative;">
				<div class="width90 float-left" id="hrm">  
				<input type="hidden" name="locationId" id="loc_LocationId" class="width60" value="${LOCATION.locationId}"  maxlength="150" tabindex="1" >
				
					<fieldset style="min-height:175px;">
				     <div class="width100 float-left">
				    	 <label class="width30">Name<span style="color:red">*</span></label>
				     	<input type="text" name="locationName" id="locationName" class="width60 validate[required]" value="${LOCATION.locationName}"  maxlength="150" tabindex="1" >
				     </div>
				     <div class="width100 float-left"><label class="width30"><fmt:message
						key="re.property.info.country" /><span class="mandatory">*</span></label> <select
						id="country" class="validate[required] country autocompletecode width50" TABINDEX=4
						>
						<option value="">- Select -</option>
					<c:choose>
						<c:when test="${COUNTRIES ne null && COUNTRIES ne ''}">
							<c:forEach items="${COUNTRIES}" var="country" varStatus="status">
								<c:choose>
									<c:when test="${ country.countryId ne LOCATION.countryId }">
										<option id="${country.countryId}" value="${country.countryId}">${country.countryCode}-${country.countryName}</option>
									</c:when>
						
									<c:otherwise>
										<option id="${country.countryId}" value="${country.countryId}" selected="selected">${country.countryCode}-${country.countryName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
					</select></div> 
					
					<div class="width100 float-left">
						<label class="width30"><fmt:message
						key="re.property.info.state" /><span class="mandatory">*</span></label> <select
						id="state" class="validate[required] autocompletecode state width50" TABINDEX=5>
						<option value="">- Select -</option>
						<c:choose>
							<c:when test="${stateId ne null && stateId ne '' && stateId ne 0 && LOCATION.stateName ne ''} ">
								<option id="${stateId}" value="${stateId}" selected="selected">${LOCATION.stateName}</option>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
						</select>
					</div>
					<div class="width100 float-left"><label class="width30"><fmt:message
						key="re.property.info.city" /><span class="mandatory">*</span></label> <select
						id="city" class="validate[required] autocompletecode city width50" TABINDEX=6 >
						<option value="">-Select-</option>
						<c:choose>
							<c:when test="${cityId ne null && cityId ne '' && cityId ne 0 && LOCATION.cityName ne '' && LOCATION.cityName ne null}">
								<option id="${cityId}" value="${cityId}" selected="selected">${LOCATION.cityName}</option>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
						</select>
					</div> 
				    
					<div id="area_combo" style="display: none;" class="width100 float-left">
						<label class="width30">Area</label> 
						<select id="uae_area" TABINDEX=9 style="width:60.5%" class="uae_area">
							<option value="Area 1">Area 1</option>
							<option value="Area 2">Area 2</option>
							<option value="Area 3">Area 3</option>
							<option value="other">Other</option>
						</select>
					</div>
						
					<div id="area_other_div" style="display: none;" class="width100 float-left"><label class="width30">
						Area</label> <input type="text"
						id="area_other" name="area_other" value=""
						class="width60" TABINDEX=8></div>
					
					<div id="zone_combo" style="display: none;" class="width100 float-left">
						<label class="width30">Zone/Sector</label> 
						<select id="uae_zone" TABINDEX=10 style="width:60.5%" class="uae_zone">
							<option value="Zone 1">Zone 1</option>
							<option value="Zone 2">Zone 2</option>
							<option value="Zone 3">Zone 3</option>
							<option value="other">Other</option>
						</select>
					</div>
				
					<div id="zone_other_div" style="display: none;" class="width100 float-left"><label class="width30">
						Zone</label> <input type="text"
						id="zone_other" name="zone_other" value=""
						class="width60" TABINDEX=8>
					</div>
					<div id="address1"><label class="width30">Address<span class="mandatory">*</span></label>
						<input type="text" id="addressLine1" name="addressLine1"
						value="${LOCATION.address}" class="width60 validate[required]" TABINDEX=7>
						<div class="clearfix"></div>
					</div>
					<div>
						<label class="width30">Postal Code</label> 
						<input type="text" id="postalCode" name="postalCode" value="" class="width60" TABINDEX=10>
					</div>
					<div>
						<label class="width30">Contact Number</label> 
						<input type="text" id="contactNumber" name="contactNumber" value="" class="width60" TABINDEX=11>
					</div>
					<div>
						<label class="width30">Description</label> 
						<textarea id="description" name="description" style="width:60.5%" class="" TABINDEX=12></textarea>
					</div>
            	</fieldset>
            	</div>
				<div class="float-right buttons ui-widget-content ui-corner-all">
				<input type="hidden" id="supplierinfo"/>
					<div class="portlet-header ui-widget-header float-right discard-location" id="discard-location"><fmt:message key="organization.button.close"/></div>
					
				 	<div class="portlet-header ui-widget-header float-right save-location" id="save-location"><fmt:message key="organization.button.save"/></div> 
				</div>
			</form>
			
		</div>
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 		</div> 
 		
    </div>
</div>

