<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>

<script type="text/javascript">
var oTable=null; var selectRow=""; var aData="";var aSelected = [];var leaveProcessId=0;; 

$(document).ready(function(){ 
	dateOnchangeCall();
	$('#halfDay').change(function(){
   	 dateOnchangeCall();
    });
	leaveProcessId=Number($('#leaveProcessId').val());
	
	//Save & discard process
	$('.save').click(function(){ 
		$('#page-error').hide();
		if($("#LEAVEDET").validationEngine({returnIsValid:true})){  
			var leaveProcessId=Number($('#leaveProcessId').val());
			var jobLeaveId=Number($('#leaveType').val());
			var comments=$('#comment').val();
			var halfDay=$('#halfDay').attr('checked');
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/approve_leave.action", 
			 	async: false, 
			 	data:{ 
			 		leaveProcessId:leaveProcessId,jobLeaveId:jobLeaveId,
			 		comments:comments,halfDay:halfDay
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/workflowProcess.action", 
							 	async: false, 
							 	data:{ 
							 		recordId:leaveProcessId,finalyzed:1,processFlag:1
							 	},
							    dataType: "html",
							    cache: false,
								success:function(result){
									 window.location.reload();
								},
								error:function(result){  
									$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
								}
							});  
					 }else{
						 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').live('click',function(){
		window.location.reload();
	});
	var limit=0;
	window.setInterval(function() {	
		if(limit==0){
			leaveHistoryCall();
			limit+=1;
		}
	},500);
});
function checkDataTableExsist(){
	oTable = $('#LeaveProcess').dataTable();
	oTable.fnDestroy();
	$('#LeaveProcess').remove();
	$('#gridDiv').html("<table class='display' id='LeaveProcess'></table>");
	leaveHistoryCall();
	
}
function leaveHistoryCall(){
	$('#page-error').hide();
	$('#LeaveProcess').dataTable({ 
		"sAjaxSource": "get_leave_process_history_json.action?leaveProcessId="+leaveProcessId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'LeaveProcess ID', "bVisible": false},
			{ "sTitle": 'Leave Type'},
			{ "sTitle": 'From'},
			{ "sTitle": 'To'},
			{ "sTitle": 'Half Day?'},
			{ "sTitle": 'No.Of Days'},
			{ "sTitle": 'Reason'},
			{ "sTitle": 'Contact On Leave'},
			{ "sTitle": 'Requested Date'},
			{ "sTitle": 'Leave Status'},
			{ "sTitle": 'Request Status'},
			
		],
		"sScrollY": $("#main-content").height() - 400,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#LeaveProcess').dataTable();
}
function dateOnchangeCall(){
	if($('#toDate').val()!=null && $('#toDate').val()!="" && $('#fromDate').val()!=null && $('#fromDate').val()!=""){ 
		putYears=$('#tempYears');
		putMonths=$('#tempMonths');
		putDays=$('#tempDays');
		fromDate=$('#fromDate').val();
		toDate=$('#toDate').val();
		calculateDate(fromDate,toDate,putYears,putMonths,putDays);
		if($('#tempDays').val()=="NaN" && " " && ""){
			$('#tempDays').val("0");
		}
		if($('#halfDay').attr('checked'))
			$('#tempDays').val(Number($('#tempDays').val())+0.5);
		else
			$('#tempDays').val(Number($('#tempDays').val())+1);	
	}
	return true;
}
function dateOnchangeCall(){
	if($('#toDate').val()!=null && $('#toDate').val()!="" && $('#fromDate').val()!=null && $('#fromDate').val()!=""){ 
		putYears=$('#tempYears');
		putMonths=$('#tempMonths');
		putDays=$('#tempDays');
		fromDate=$('#fromDate').val();
		toDate=$('#toDate').val();
		calculateDate(fromDate,toDate,putYears,putMonths,putDays);
		if($('#tempDays').val()=="NaN" && " " && ""){
			$('#tempDays').val("0");
		}
		if($('#halfDay').attr('checked'))
			$('#tempDays').val(Number($('#tempDays').val())+0.5);
		else
			$('#tempDays').val(Number($('#tempDays').val())+1);	
	}
	return true;
}
</script>
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Employee Absent Process</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="LEAVEDET" class="" name="LEAVEDET" method="post">
		  	<div class="width100 float-left" id="hrm">
				<fieldset style="height:170px;">
					<legend>Leave Information</legend>
					<div class="width45 float-left">
						<input type="hidden" name="leaveProcessId" id="leaveProcessId" value="${LEAVE_PROCESS.leaveProcessId}" class="width50"/>
						<div>
							<label class="width30" for="leaveType">Leave Type<span class="mandatory">*</span></label>
							<select id="leaveType" name="leaveType" class="width50 validate[required]">
								<option value="">--Select--</option>
								<c:forEach items="${JOB_LEAVE_LIST}" var="jobleave" varStatus="status">
								
								<c:choose>
									<c:when test="${LEAVE_PROCESS.jobLeave.jobLeaveId eq jobleave.jobLeaveId}">
										<option value="${jobleave.jobLeaveId}" selected="selected">${jobleave.leave.leaveType}</option>
									</c:when>
									<c:otherwise>
										<option value="${jobleave.jobLeaveId}">${jobleave.leave.leaveType}</option>
									</c:otherwise>
								</c:choose>
								</c:forEach>
								
							</select>
						</div>   
						<div>
							<label class="width30" for="fromDate">Leave Start Date<span class="mandatory">*</span></label>
							<c:choose>
								<c:when test="${LEAVE_PROCESS.fromDate ne null &&  LEAVE_PROCESS.fromDate  ne ''}">
									<c:set var="fromDate" value="${LEAVE_PROCESS.fromDate}"/>  
									<%String fromDate = DateFormat.convertDateToString(pageContext.getAttribute("fromDate").toString());%>
									<input type="text" disabled="disabled" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]" 
									value="<%=fromDate%>"/>
								</c:when>
								<c:otherwise>
									<input type="text" disabled="disabled" name="fromDate" id="fromDate" tabindex="1" class="width50 validate[required]"/>
								</c:otherwise>
							</c:choose> 
						</div>   
						<div>
							<label class="width30" for="toDate">Leave End Date<span class="mandatory">*</span></label>
							<c:choose>
								<c:when test="${LEAVE_PROCESS.toDate ne null &&  LEAVE_PROCESS.toDate ne ''}">
									<c:set var="toDate" value="${LEAVE_PROCESS.toDate}"/>  
									<%String toDate = DateFormat.convertDateToString(pageContext.getAttribute("toDate").toString());%>
									<input type="text" disabled="disabled" name="toDate" id="toDate" tabindex="3" class="width50 validate[required]" 
									value="<%=toDate%>"/>
								</c:when>
								<c:otherwise>
									<input type="text" disabled="disabled" name="toDate" id="toDate" tabindex="4" class="width50 validate[required]"/>
								</c:otherwise>
							</c:choose> 
						</div> 
						<div>
							<label class="float-left width30" for="leaveType">Half Day?</label>
							<c:choose>
								<c:when test="${LEAVE_PROCESS.halfDay eq true}">
								<input type="checkbox" name="halfDay" id="halfDay" checked="checked" class="float-left width5" >
								</c:when>
								<c:otherwise>
									<input type="checkbox" name="halfDay" id="halfDay" class="float-left width5" >
								</c:otherwise>
							</c:choose>
							<label class="float-left width20" for="leaveType">No. Of Days :</label>
							<input type="text" id="tempDays" class="float-left width10" readonly="readonly"/>
							<input type="hidden" id="tempYears"/><input type="hidden" id="tempMonths"/>
						</div>
					</div>
					<div class="width45 float-left">
						<div class="width100 float-left">
							<label class="width20" for="reason">Reason</label>
							<label class="width70" for="reason">${LEAVE_PROCESS.reason}</label>
						</div>
						<div class="width100 float-left">
							<label class="width20" for="contactOnLeave">Contact On Leave</label>
							<label class="width70" for="reason">${LEAVE_PROCESS.contactOnLeave}</label>
						</div>
						<div class="width100 float-left">
							<label class="width20" for="comment">Comment</label>
							<textarea name="comment" id="comment" class="width50" ></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="float-right buttons ui-widget-content ui-corner-all">
						<div class="portlet-header ui-widget-header float-right discard" id="discard">Close</div>
						<div class="portlet-header ui-widget-header float-right save" id="job_save">Submit</div>
					</div>
				</fieldset>
				</div>
			</form>
			<div class="width100 float-left" id="hrm">
				<fieldset>
					<legend>Leave History</legend>
					<div id="rightclickarea">
						<div id="gridDiv">
						<table class="display" id="LeaveProcess"></table>
						</div>
		 			</div>	
		 		</fieldset>
	 		</div>
		</div>
	</div>
</div>
