<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td{
	width:6%;
}
.width35{
	width:35%!important;
}
.width25{
	width:25%!important;
}
.width15{
	width:15%!important;
}
.margintop3{
	margin-top:3px;
}
#common-popup{
	overflow: hidden;
}
.width61{
	width:61% !important;
}

</style>
<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var allowanceLineDetail="";
var leaveLineDetail="";
var shiftLineDetail="";
var assignmentLineDetail="";

$(document).ready(function(){  
	

	$('.parentrDepatment').combobox({ 
		 selected: function(event, ui){ 
			 
		 }
	 });
//Combination pop-up config
	
	//Last row indentify cal
	manupulateAssignmentLastRow();

	/* //Allowance onchange event
	$('.allowance').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id'));
			dublicatePayEntries(rowId);
			triggerAllowanceAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	$('.amount').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerAllowanceAddRow(rowId);
			totalPaySum();
			return false;
		} 
		else{
			return false;
		}
	});	 */
	
	$('#companyId').change(function(){
		var companyId=Number($('#companyId').val());
    	var rowId=getRowId($('#hastab3 tr:last').prev().attr('id')); 
    	//alert(companyId+"---"+rowId);
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_parentdepartment_for_setup.action", 
			 	async: false,
			 	data:{companyId:companyId},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('#parentDepartment_'+rowId).html(result);
					$('#department_'+rowId).html(result);
					//applyAutoComplete(rowId);
					return false;
				}
			});
		return false;
		
	});
	
	//Assignment
	$('.addrowsA').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabA>.rowidA:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/company_setup_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabA tr:last').before(result);
					 if($(".tabA").height()>255)
						 $(".tabA").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidA').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdA_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	$('.delrowA').live('click',function(){ 
		 var rowId1=getRowId($(this).attr('id')); 
			deleteDepartment(rowId1,$(this));
		
		 return false;
	 });
	
	$jquery("#JOBDetails").validationEngine('attach');
	
	//Save & discard process
	$('.save').click(function(){ 
		 $('.error,.success').hide();
		if($jquery("#JOBDetails").validationEngine('validate')){  
			var rowId=getRowId($('#hastab3 tr:last').prev().attr('id')); 
			var companyId=Number($('#companyId').val());
			var departmentName=$('#department_'+rowId).val();
			var parentDepartmentId=Number($('#parentDepartment_'+rowId).val());
			var description=$('#description_'+rowId).val();
			var locationId=Number($('#locationId_'+rowId).val());
			var combinationId=Number($('#combinationId_'+rowId).val());
			
			if(departmentName==null || departmentName=="" || locationId==null || locationId=="" || locationId==0){
				 $('#page-error').hide().html("Enter the required department infomation").slideDown(1000);
				 return false;
			}
			var departmentId=Number($('#departmentId_'+rowId).val());	
			//allowanceLineDetail=getAllowanceLineDatas(); 
			
			//loanChargesDetail=getLoanChargesDetails();
			//alert(allowanceLineDetail+"----"+leaveLineDetail+"----"+shiftLineDetail);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_department.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		companyId:companyId,
			 		departmentName:departmentName,
			 		parentDepartmentId:parentDepartmentId,
			 		description:description,
			 		locationId:locationId,
			 		departmentId:departmentId,
			 		combinationId:combinationId
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('#returnMessage').val(); 
					 if(message.trim()=="SUCCESS"){
						 $("#departmentId_"+rowId).val($('#departmentIdTemp').val());
						 $("#cmdDeptLocId_"+rowId).val($('#cmdDeptLocIdTemp').val());
						 triggerAddRow(rowId);
						 $('#success_msg_add').hide().html("Successfully added").slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html("Can not add the information.").slideDown(1000);
						 return false;
					 }
				},
				error:function(result){
					$(".tempresult").html(result);
					 var message=$('#returnMessage').val(); 
					 if(message.trim()!=""){
						$('#page-error').hide().html("Can't Delete information is in use already.").slideDown(1000);
						return false;
					 }else{
						 $('#page-error').hide().html("System Error! Contact  administrator.").slideDown(1000);
						 return false;
					 }
				}
			});  
		}
		else{
			return false;
		}
		return false;
	});
	
	$('.discard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/company_setup.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
		 return false;
	});
	

    $('.location-popup').live('click',function(){
    	$('.error,.success').hide();
    	 var companyId=0;
    	 companyId=Number($('#companyId').val());
    	 if(companyId==0){
         	alert("Select Company.");
         	return false;
    	 }
        $('.ui-dialog-titlebar').remove();  
        $('#job-common-popup').dialog('open');
        var rowId=getRowId($(this).attr('id'));
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/get_location_for_setup.action",
              data:{id:rowId,companyId:companyId},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.job-common-result').html(result);
                   return false;
              },
              error:function(result){
                   $('.job-common-result').html(result);
              }
          });
           return false;
  	}); 

    $('.location-popup-discard').live('click',function(){
    	$('#job-common-popup').dialog('close');
    });
    
    $('.department-popup').live('click',function(){
    	$('.error,.success').hide();
    	 var companyId=0;
    	 companyId=Number($('#companyId').val());
    	 if(companyId==0){
         	alert("Select Company.");
         	return false;
    	 }
        $('.ui-dialog-titlebar').remove();  
        $('#job-common-popup').dialog('open');
        var rowId=getRowId($(this).attr('id')); 
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/get_departments_for_setup.action",
              data:{id:rowId,companyId:companyId},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.job-common-result').html(result);
                   return false;
              },
              error:function(result){
                   $('.job-common-result').html(result);
              }
          });
           return false;
  	}); 
    
    $('#add_location').click(function(){
    	$('.error,.success').hide();
        if($jquery("#JOBDetails").validationEngine('validate')){
        	 var companyId=0;
        	 companyId=Number($('#companyId').val());
        	 if(companyId==0){
             	alert("Select Company.");
             	return false;
        	 }
        	 $('.ui-dialog-titlebar').remove();  
             $('#job-common-popup').dialog('open');
           
		           $.ajax({
		              type:"POST",
		              url:"<%=request.getContextPath()%>/get_add_location.action",
		              data:{companyId:companyId},
		              async: false,
		              dataType: "html",
		              cache: false,
		              success:function(result){
		                   $('.job-common-result').html(result);
		                   return false;
		              },
		              error:function(result){
		                   $('.job-common-result').html(result);
		              }
		          });
		           return false;
        }else{
        	return false;
        }
  	}); 
    
   
     $('#job-common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});
     
   //Combination pop-up config
  	$('.codecombination-popup').live('click',function(){ 
  	    tempid=$(this).parent().get(0);  
  	    $('.ui-dialog-titlebar').remove();   
  	 	$.ajax({
  			type:"POST",
  			url:"<%=request.getContextPath()%>/show_costcenter_combination.action", 
  		 	async: false, 
  		    dataType: "html",
  		    cache: false,
  			success:function(result){   
  				 $('.codecombination-result').html(result);  
  			},
  			error:function(result){ 
  				 $('.codecombination-result').html(result); 
  			}
  		});  
  		return false;
  	});
  	
  	 $('#codecombination-popup').dialog({
  			autoOpen: false,
  			minwidth: 'auto',
  			width:800, 
  			bgiframe: false,
  			modal: true
  	 });

});

function manupulateAssignmentLastRow(){
	var hiddenSize=0;
	$($(".tabA>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabA>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabA>tr:last').removeAttr('id');  
	$('.tabA>tr:last').removeClass('rowidA').addClass('lastrow');  
	$($('.tabA>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabA>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}


function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){ 
	var departmentId=$('#departmentId_'+rowId).val();  
	var nexttab=$('#fieldrowA_'+rowId).next(); 
	if(departmentId!=null && departmentId!="" && departmentId!=0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageA_'+rowId).show();
		$('.addrowsA').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function deleteDepartment(rowId,deleteObject){
	$('.error,.success').hide();
	var departmentId = Number($("#departmentId_"+rowId).val()); 
			
	var cnfrm = confirm('Selected Details will be deleted permanently');
	if(!cnfrm)
		return false;
			
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/delete_department.action",  
	    	data: {departmentId:departmentId},  
	    	async: false,
		dataType: "html",
		cache: false,
		success: function(result)
	    	{  
			$(".tempresult").html(result);
			 var message=$('.tempresult').html(); 
				 $('#success_msg_add').hide().html("Information deleted successfully").slideDown(1000);
				 $('#DeleteImageA_'+rowId).show();
				
				 slidetab=$(deleteObject).parent().parent().get(0);  
			 	$(slidetab).remove();  
				 var i=1;
				 $('.rowidA').each(function(){   
					 var rowId=getRowId($(deleteObject).attr('id')); 
					 $('#lineIdA_'+rowId).html(i);
						 i=i+1; 
					 });  
	    	},
	    	error:function(result)
	          {
	    		$('#page-error').hide().html(result).slideDown(1000);
	          } 
	}); 


}
function locationGenericResultCall(locationId,locationName,commaSeparatedParam){
	var valueArray = new Array();
	valueArray = commaSeparatedParam.split('@&');
	var rowId = valueArray[0];
	$("#locationName_"+rowId).html(locationName);
	$("#locationId_"+rowId).val(locationId);
}
function setCombination(combinationTreeId, combinationTree) {
	var idVals = $(tempid).attr('id').split("_");
	var rowids = Number(idVals[1]);
	$('#combinationId_' + rowids).val(combinationTreeId);
	$('#codeCombination_' + rowids).val(combinationTree);
	$('#codeCombination_' + rowids).val().replace(/[\s\n\r]+/g, ' ').trim();
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Department & Branchs Information</div>
			 
		<div class="portlet-content">
				<div id="page-error" class="response-msg error ui-corner-all" style="display:none;"><span>Process Failure!</span></div> 
				<div id="success_msg_add" class="success response-msg ui-corner-all" style="display: none;"></div>
				 
			  	<div class="tempresult" style="display:none;"></div>
					  <form id="JOBDetails" class="" name="JOBDetails" method="post">
					  	<div class="width100 float-left" id="hrm"> 
					  		<fieldset style="height:50px;">
								<div class="width60 float-left">
									<div class="width60 float-left" style="margin:3px;">
										<label class="width30" for="company" style="margin-top:7px;"><fmt:message key="hr.department.company"/><span class="mandatory">*</span></label>
										<select id="companyId" name="companyId" class="width60 validate[required]">
											<option value="">--Select--</option>
											<c:forEach items="${COMPANY_LIST}" var="comp" varStatus="status">
												<option value="${comp.companyId}">${comp.companyName}</option>
											</c:forEach>
										</select>
									</div> 
							 	</div>
							 <div class="width40 float-left">
							 	<div class="float-left buttons ui-widget-content ui-corner-all" style="margin: 0px;">
									<div class="portlet-header ui-widget-header float-right add_location"  id="add_location"><fmt:message key="hr.department.newlocation"/></div>
								</div>
							 </div> 
							</fieldset>
			 			</div>
			 		</form>
			 
					<div id="hrm" class="hastable width100 float-left"  >  
						<table id="hastab3" class="width100 float-left"> 
							<thead>
								<tr> 
									<th style="width:20%"><fmt:message key="hr.department.department"/><span class="mandatory">*</span></th> 
								    <th style="width:20%"><fmt:message key="hr.department.parent"/></th>  
								    <th style="width:15%"><fmt:message key="hr.department.description"/></th>
								    <th style="width:15%"><fmt:message key="hr.department.location"/><span class="mandatory">*</span></th>
								     <th style="width:15%">Account Code</th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabA">
										<c:forEach var="i" begin="1" end="2" step="1" varStatus="status"> 
											<tr class="rowidA" id="fieldrowA_${i}">
												<td id="lineIdA_${i}" style="display: none;">${i}</td>
												<td>
													<%-- <select class="width90 department" id="department_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="department" items="${DEPARTMENT_LIST}" varStatus="statusL">
															<option value="${department.departmentId}">${department.departmentName}</option>
														</c:forEach>
													</select> --%>
													<input type="text" name="department_${i}" id="department_${i}" class="department  validate[required] width80" value=""/>  
													<span class="button" id="location_${i}"  style="position: relative; top:0px;">
														<a style="cursor: pointer;" id="departmentpopup_${i}" class="btn ui-state-default ui-corner-all width100 department-popup"> 
															<span class="ui-icon ui-icon-newwin"> 
															</span> 
														</a>
													</span> 
													<input type="hidden" name="departmentId_${i}" id="departmentId_${i}" class="departmentId" value=""/>
													<input type="hidden" name="cmdDeptLocId_${i}" id="cmdDeptLocId_${i}" class="cmdDeptLocId" value=""/>
												</td> 
												<td>
													<select class="width90 parentrDepatment" id="parentDepartment_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach var="parentrDepatment" items="${DEPARTMENT_LIST}" varStatus="statusL">
															<option value="${parentrDepatment.departmentId}">${parentrDepatment.departmentName}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<textarea id="description_${i}" class="description width90"></textarea>
												</td>
												<td>
													<span id="locationName_${i}" class="float-left location">
													</span>
													<span class="button float-right" id="location_${i}"  style="position: relative;">
														<a style="cursor: pointer;" id="locationpopup_${i}" class="btn ui-state-default ui-corner-all width100 location-popup"> 
															<span class="ui-icon ui-icon-newwin"> 
															</span> 
														</a>
													</span> 
													<input type="hidden" name="locationId_${i}" id="locationId_${i}" class="locationId" value=""/>  
												</td> 
												<td class="codecombination_info" id="codecombination_${i}"
													style="cursor: pointer;"><input type="hidden"
													name="combinationId_${i}" id="combinationId_${i}" /> <input
													type="text" name="codeCombination_${i}" readonly="readonly"
													id="codeCombination_${i}" class="codeComb width80">
													<span class="button" id="codeID_${i}"> <a
														style="cursor: pointer;"
														class="btn ui-state-default ui-corner-all codecombination-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												 <td style="width:0.01%;" class="opn_td" id="optionA_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataA" id="AddImageA_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataA" id="EditImageA_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowA" 
													  	style="display: none;" id="DeleteImageA_${i}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageA_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>
											</tr>
									</c:forEach>
							 </tbody>
					</table>
				</div> 
				<div class="clearfix"></div>
				<div class="float-right buttons ui-widget-content ui-corner-all">
					<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="common.button.cancel"/></div>
					<div class="portlet-header ui-widget-header float-right save" id="job_save"><fmt:message key="common.button.save"/></div>
				</div>
			</div>
		</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="job-common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div> 
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrowsA" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div>
	<div id="codecombination-popup"
		class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div
			class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>
	

