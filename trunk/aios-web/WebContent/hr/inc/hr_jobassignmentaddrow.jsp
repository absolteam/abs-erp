<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tr class="rowidA" id="fieldrowA_${rowId}">
	<td id="lineIdA_${rowId}" style="display: none;">${rowId}</td>
	<td>
		<span  id="employeeName_${rowId}" class="float-left width20">
		</span>
		<span class="button float-right" id="employee_${rowId}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="employeepopup_${rowId}" class="btn ui-state-default ui-corner-all width100 employees-popup"> 
				<span class="ui-icon ui-icon-newwin">
				</span> 
			</a>
		</span> 
		<input type="hidden" id="personId_${rowId}" value="" class="personId"  style="border:0px;"/>
	</td> 
	<td>
		<span  id="designationName_${rowId}" class="float-left designation">
		</span>
		<span class="button float-right" id="designation_${rowId}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="designationpopup_${rowId}" class="btn ui-state-default ui-corner-all width100 designations-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span> 
		<input type="hidden" name="designationId_${rowId}" id="designationId_${rowId}" 
			class="designationId" value=""/>  
	</td>
	<td>
		<span  id="locationName_${rowId}" class="float-left location">
		</span>
		<span class="button float-right" id="location_${rowId}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="locationpopup_${rowId}" class="btn ui-state-default ui-corner-all width100 locations-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span> 
		<input type="hidden" name="cmpDeptLocId_${rowId}" id="cmpDeptLocId_${rowId}"
			class="cmpDeptLocId" value=""/>  
	</td> 
	<td>
		<input type="text" readonly="readonly" class="width90 jobAssignmentNumber" name="jobAssignmentNumber" id="jobAssignmentNumber_${rowId}" value=""/>
	</td> 
	<td>
		<input type="text" class="width90 swipeId" name="swipeId" id="swipeId_${rowId}" value=""/>
	</td>
	<td>
		<select class="width90 payMode" id="payMode_${rowId}">
			<option value="OTHER">OTHER</option>
			<option value="WPS">WPS</option>
			<option value="BANK">BANK</option>
			<option value="EXCHANGE">EXCHANGE</option>
			<option value="CASH">CASH</option>
		</select>
		<input type="hidden" id="payMode_${rowId}" value=""/>
	</td>  
	<td>
	     <input type="text" class="masterTooltip width80" title="" readonly="readonly" name="personBank" id="personBank_${rowId}" value=""/>
	     <input type="hidden" name="personBankId" id="personBankId_${rowId}" value=""/> 
	     <span class="button" style="position: relative;top:6px; ">
			<a style="cursor: pointer;" id="personBankCall_${rowId}" class="btn ui-state-default ui-corner-all width100 personBank-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span>
	</td>
	<td>
		<input type="checkbox" class="width20" id="isLocalPay_${rowId}" checked="checked"/>
	</td>
	<td style="display:none;">
		<input type="hidden" id="jobAssignmentId_${rowId}" value="" style="border:0px;"/>
	</td>
	 <td style="width:0.01%;" class="opn_td" id="optionA_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataA" id="AddImageA_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataA" id="EditImageA_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowA" id="DeleteImageA_${rowId}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageA_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>
</tr>
 