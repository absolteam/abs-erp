<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<option value="">Select</option>
<c:forEach var="parentDepatment" items="${DEPARTMENT_LIST}" varStatus="statusL">
	<option value="${parentDepatment.departmentId}">${parentDepatment.departmentName}</option>
</c:forEach>