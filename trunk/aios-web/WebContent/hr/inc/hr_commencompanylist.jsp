<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">

</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var companyId=0;var companyName='';var companyTypes="";
$(function(){ 
	companyTypes=$('#companyTypesToList').val();
	$('#CompanyList').dataTable({ 
		"sAjaxSource": "common_company_list_json.action?companyTypes="+companyTypes,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Company ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="hr.company.name"/>'},
 			{ "sTitle": '<fmt:message key="hr.company.type"/>'},
 			{ "sTitle": '<fmt:message key="common.description"/>'},
 			{ "sTitle": '<fmt:message key="hr.company.tradenumber"/>'},
 			
 		],
 		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#CompanyList').dataTable();

	/* Click event handler */
	$('#CompanyList tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        companyId=aData[0];
	        companyName=aData[1];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        companyId=aData[0];
	        companyName=aData[1];
	    }
	});
	$('#CompanyList tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        companyId=aData[0];
	        companyName=aData[1];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        companyId=aData[0];
	        companyName=aData[1];
	    }
		 var commonParam="";
		companyPopupResult(companyId,companyName,commonParam);
		$('#common-popup').dialog("close");
	});
	
	$('#popup-grid-close').click(function () { 
		$('#common-popup').dialog("close");
	});
});
</script>
<div id="main-content"> 
	<input type="hidden" id="companyTypesToList" name="companyTypesToList" value="${COMPANY_TYPES}"/>
 	<div id="trans_company_data">
		<table class="display" id="CompanyList"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="popup-grid-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div>  
</div>