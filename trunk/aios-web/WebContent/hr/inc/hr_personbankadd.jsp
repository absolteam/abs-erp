<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">

</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
$(function (){  
	
	var personBankId=Number($('#personBankId').val());
	if(personBankId>0){
		if($('#isActiveTemp').val().trim()=='true')
			$('#isActive').attr("checked","checked");
	}
	
	//pop-up config
    $('.employee-popup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         currentObjectIdentiy=$(this).attr("id");
         var rowId=-1; 
         var personTypes="";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   }); 
	 
	 
	 
   $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:550,
		bgiframe: false,
		modal: true 
	});

   $('#person-list-close').live('click',function(){
	   $('#common-popup').dialog('close');
   });
   $jquery("#PERSON_BANK").validationEngine('attach');
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#PERSON_BANK").validationEngine('validate')){ 
			var personBankId=Number($('#personBankId').val());
			var personId=Number($('#personId').val());
			var accountTitle=$('#accountTitle').val();
			var accountNumber=$('#accountNumber').val();
			var bankName=$('#bankName').val();
			var routingCode=$('#routingCode').val();
			var iban=$('#iban').val();
			var branchName=$('#branchName').val();
			var bankCode=$('#bankCode').val();
			var branchCity=$('#branchCity').val();
			var description=$('#description').val();
			var isActive=$('#isActive').attr("checked");
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(personBankId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/person_bank_save.action",
				data: {
					personBankId:personBankId,
					personId:personId,
					accountNumber:accountNumber,
					bankName:bankName,
					routingCode:routingCode,
					iban:iban,
					branchName:branchName,
					bankCode:bankCode,
					branchCity:branchCity,
					description:description,
					accountTitle:accountTitle,
					isActive:isActive
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOAssetList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.error').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.error').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		 returnCallTOAssetList();
		$('#loading').fadeOut();
	});
});
   function returnCallTOAssetList(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/person_bank_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();	
				$("#main-wrapper").html(result); 
			}
	 	});
	}
function personPopupResult(personId,personName){
	
	$('#personName').val(personName);
	$('#personId').val(personId);
	
}


</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Person Bank Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<form id="PERSON_BANK" name="PERSON_BANK" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" name="personBankId" id="personBankId" value="${PERSON_BANK_INFO.personBankId}"/>
					<fieldset style="height:190px;">
						
			      	 <div>
	                   	<label class="width30">Person / Employee<span style="color:red">*</span></label>
	                   	<c:choose>
	                    	<c:when test="${PERSON_BANK_INFO.person ne null && PERSON_BANK_INFO.person ne '' }">
	                     		<input type="text" class="width60 validate[required]" title="" readonly="readonly" name="personName" id="personName" 
	                     			value="${PERSON_BANK_INFO.person.firstName} ${PERSON_BANK_INFO.person.lastName}"/>
	                    	</c:when>
	                    	<c:otherwise>
	                    		<input type="text" class="width60 validate[required]" title="" readonly="readonly" name="personName" id="personName" value=""/>
	                    	</c:otherwise>
		                </c:choose>
		                 <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="employeeCall" class="btn ui-state-default ui-corner-all width10 employee-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
						<input type="hidden" name="personId" id="personId" value="${PERSON_BANK_INFO.person.personId}"/> 
			      	 </div>
			      	<div>
		                 <label class="width30">Account Title<span style="color:red">*</span></label>
		                 <input type="text" name="accountTitle" class="width60 validate[required]" id="accountTitle" value="${PERSON_BANK_INFO.accountTitle}">
		            </div>
				    <div>
		                 <label class="width30">Account Number<span style="color:red">*</span></label>
		                 <input type="text" name="accountNumber" class="width60 validate[required]" id="accountNumber" value="${PERSON_BANK_INFO.accountNumber}">
		            </div>
		            <div>
		                 <label class="width30">Bank Name<span style="color:red">*</span></label>
		                 <input type="text" name="bankName" class="width60 validate[required]" id="bankName" value="${PERSON_BANK_INFO.bankName}">
		            </div>  
		            <div>
		                 <label class="width30">IBAN<span style="color:red">*</span></label>
		                 <input type="text" name="iban" class="width60 validate[required]" id="iban" value="${PERSON_BANK_INFO.iban}">
		            </div>    
			       	<div>
		                 <label class="width30">Branch Name</label>
		                 <input type="text" name="branchName" class="width60" id="branchName" value="${PERSON_BANK_INFO.branchName}">
		            </div> 
				 </fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset style="height:190px;">
					
		            <div>
		                 <label class="width30">Routing Code</label>
		                 <input type="text" name="routingCode" class="width60" id="routingCode" value="${PERSON_BANK_INFO.routingCode}">
		            </div>  
		            <div>
		                 <label class="width30">Bank Code</label>
		                 <input type="text" name="bankCode" class="width60" id="bankCode" value="${PERSON_BANK_INFO.bankCode}">
		            </div>
		            <div>
		                 <label class="width30">Address</label>
		                 <textarea name="branchCity" class="width60" id="branchCity">${PERSON_BANK_INFO.branchCity}</textarea>
		            </div>
		            <div>
		                 <label class="width30">Description</label>
		                 <textarea name="description" class="width60" id="description">${PERSON_BANK_INFO.description}</textarea>
		            </div>
		            <div>
	                 	<label class="width30">Active</label>
	                 	<input type="checkbox" id="isActive" class="width8">
	               	 	 <input type="hidden" id=isActiveTemp value="${PERSON_BANK_INFO.isActive}">
	           		 </div>
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>