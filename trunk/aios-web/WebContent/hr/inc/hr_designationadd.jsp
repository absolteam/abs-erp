<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" /> 
<script type="text/javascript">
var designationId=0;
$(document).ready(function(){  
	designationId=Number($('#designationId').val());
	if(designationId>0){
		$('#companyId').val($('#tempCompanyId').val());
		$('#gradeId').val($('#tempGradeId').val());
		$('#parentDesignationId').val($('#tempParentDesignationId').val());
	}
	 $('.formError').remove();
	
	 $('#companyId').change(function(){
			var companyId=Number($('#companyId').val());
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_parent_designation.action", 
				 	async: false,
				 	data:{companyId:companyId},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('#parentDesignationId').html(result);
						return false;
					}
				});
			return false;
			
		});
	 
		$('.discard').click(function(){
			// alert("discard");
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/designation_list.action",
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
						
				 		$("#main-wrapper").html(result);
					}	 	
				});
			 return false; 
		 });

		$('#designationName').change(function(){
			//$('#designationName').addClass('validate[required,custom[letterandSpclchar]]');
		});
		
		$jquery("#DESIGNATION_DETAILS").validationEngine('attach');

		// Ajax Call for Adding Person Details
		$('.save').click(function(){
			$('#page-success').hide();
			$('#page-error').hide();
			var designationName=$('#designationName').val();
			if(designationName==''){
				//$('#designationName').removeClass('validate[required,custom[letterandSpclchar]]').addClass('validate[required]'); 
			}
			if($jquery("#DESIGNATION_DETAILS").validationEngine('validate'))
		    {
				
					var companyId=Number($('#companyId').val());
					var gradeId=Number($('#gradeId').val());
					var parentDesignationId=Number($('#parentDesignationId').val());
					var gradeId=Number($('#gradeId').val());
					var designationNameArabic=$('#designationNameArabic').val();
					var description=$('#description').val();
					var successMessage="Successfully Created";
					if(designationId>0)
						successMessage="Successfully Updated";
				    $.ajax({ 
						type: "POST", 
						url: "<%=request.getContextPath()%>/save_designation.action", 
						data: {
								designationId:designationId,
								companyId:companyId,
								designationName:designationName,
								designationNameArabic:designationNameArabic,
								gradeId:gradeId,
								parentDesignationId:parentDesignationId,
								description:description
						},
						async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							 $(".tempresult").html(result);
							 var message=$(".tempresult").html(); 
							 if(message.trim()=="SUCCESS"){
								$('.discard').trigger('click'); 
								 $('.success').hide().html(successMessage).slideDown(1000);
							 }
							 else{
								 $('#page-error').hide().html(message).slideDown(1000);
								 return false;
							 }
						},
						error: function(result){
									
							 $("#main-wrapper").html(result);   
						}
					});
					return true; 
			}		
			else
			{
				return false;
			} 
		});
		
		
		
		
		 $('.location-popup').live('click',function(){
		        $('.ui-dialog-titlebar').remove();  
		        $('#job-common-popup').dialog('open');
		        var rowId=getRowId($(this).attr('id')); 
		           $.ajax({
		              type:"POST",
		              url:"<%=request.getContextPath()%>/get_location_for_setup.action",
		              data:{id:rowId},
		              async: false,
		              dataType: "html",
		              cache: false,
		              success:function(result){
		                   $('.job-common-result').html(result);
		                   return false;
		              },
		              error:function(result){
		                   $('.job-common-result').html(result);
		              }
		          });
		           return false;
		  }); 
		 
		 $('#job-common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:450,
				bgiframe: false,
				modal: true 
			});
		 
		 $('.reset').click(function(){
				clear_form_elements();
			});
});
function clear_form_elements() {
	$('#page-error').hide();
	$('#DESIGNATION_DETAILS').each(function(){
	    this.reset();
	}); 
}

</script>			
<div id="main-content" class="hr_main_content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Designation Information</div>
             
        <div class="portlet-content">
        
        	 <div id="page-error" class="response-msg error ui-corner-all width100" style="display:none;"><span>Process Failure!</span></div>  
        	  <div id="page-success" class="response-msg success ui-corner-all width100" style="display:none;"><span>Success!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
	        <c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
			<div class="tempresult" style="display:none;"></div>
			<form name="DESIGNATION_DETAILS" id="DESIGNATION_DETAILS" style="position: relative;">
			<div class="width100 float-left" id="hrm">
				<fieldset style="min-height:125px;">
					<div class="width50 float-left" id="hrm">  
	            		<input type="hidden" name="designationId" id="designationId" value="${DESIGNATION.designationId}" >
					     <div>
					     	<label class="width30">Company<span style="color:red">*</span></label>
				      		<select style="width: 51%;" name="companyId" id="companyId" class="validate[required]" tabindex="2" >
				      			<option value=""> - Select - </option>
				      			<c:forEach items="${requestScope.COMPANY_LIST}" var="typLst">
				      				<option value="${typLst.companyId}">${typLst.companyName}</option>
				      			</c:forEach>
				      		</select>
				      		<input type="hidden" name="tempCompanyId" id="tempCompanyId" class="width45" value="${DESIGNATION.company.companyId}" >
					      </div>
					     <div>
					    	 <label class="width30">Designation Name<span style="color:red">*</span></label>
					     	<input type="text" name="designationName" id="designationName" class="width50 validate[required]" value="${DESIGNATION.designationName}"  maxlength="150" tabindex="1" >
					     </div>
					     <div>
					    	 <label class="width30">Designation Name(Arabic)<span style="color:red">*</span></label>
					     	<input type="text" name="designationNameArabic" id="designationNameArabic" class="width50" value="${DESIGNATION_NAME_ARABIC.designationNameArabic}"  maxlength="150" tabindex="3" >
					     </div>
					     <div>
			    	 		<label class="width30">Grade<span style="color:red">*</span></label>
			     			<select name="gradeId" id="gradeId" class="validate[required]" style="width: 51%;">
				     			<option value="">Select</option>
				     			<c:choose>
				     				<c:when test="${GRADE_LIST ne null && GRADE_LIST ne ''}">
				     					<c:forEach var="dbean" items="${GRADE_LIST}">
				     						<option value="${dbean.gradeId}">${dbean.gradeName}</option>
				     					</c:forEach>
				     				</c:when>
				     			</c:choose>
			     			</select>
			     			<input type="hidden" name="tempGradeId" id="tempGradeId" class="width45" value="${DESIGNATION.grade.gradeId}" >
				     	</div> 
				    </div>
				    <div class="width40 float-left" id="hrm">
				    	   <div>
			    	 		<label class="width30">Parent Designation</label>
			     			<select name="parentDesignationId" id="parentDesignationId" style="width: 61.5%;">
				     			<option value="">Select</option>
				     			<c:choose>
				     				<c:when test="${DESIGNATION_LIST ne null && DESIGNATION_LIST ne ''}">
				     					<c:forEach var="dbean1" items="${DESIGNATION_LIST}">
				     						<option value="${dbean1.designationId}">${dbean1.designationName}</option>
				     					</c:forEach>
				     				</c:when>
				     			</c:choose>
			     			</select> 
			     			<input type="hidden" name="tempParentDesignationId" id="tempParentDesignationId" class="width45" value="${DESIGNATION.designation.designationId}">
							</div> 
					      <div>
					    	 <label class="width30">Description</label>
					     	<textarea name="description" id="description" style="width: 61.5%;" tabindex="4" >${DESIGNATION.description}</textarea>
					     </div> 
	            	</div> 
	            	            
					<div class="float-right buttons ui-widget-content ui-corner-all">
						<div class="portlet-header ui-widget-header float-right discard"><fmt:message key="organization.button.cancel"/></div>
					 	<div class="portlet-header ui-widget-header float-right save"><fmt:message key="organization.button.save"/></div> 
					</div>
				</fieldset>
			</div>
			</form>
		</div>
    </div>
</div>

