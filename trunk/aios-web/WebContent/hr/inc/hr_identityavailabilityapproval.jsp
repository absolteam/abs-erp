<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
</style>
<script type="text/javascript">
var currentObjectIdentiy="";
$(function (){  
	
	$('#handoverDate,#expectedReturnDate,#actualReturnDate').datepick();
	
	var identityAvailabilityId=Number($('#identityAvailabilityId').val());
	if(identityAvailabilityId>0){
		$('#status').val($('#statusTemp').val());
		if($('#isActiveTemp').val().trim()=='true')
			$('#isActive').attr("checked","checked");
	}
	
	 
	/* $('.discard').live('click',function(){
		window.location.reload();
	});
	
	$('#disapprove').click(function(){
		identityAvailabilityId=Number($('#identityAvailabilityId').val());	
		if(identityAvailabilityId>0)
			workflowRejectCall(identityAvailabilityId);
		else
			$('.error').html("Disapproval Failured").show();
		
		return false;
	});
	
	$('#approve').click(function(){ 
		$('#page-error').hide();
		identityAvailabilityId=Number($('#identityAvailabilityId').val());	
		if(identityAvailabilityId>0)
			workflowApproveCall(identityAvailabilityId);
		else
			$('.error').html("Approval Failured").show();
		
		return false;
	}); */
});
 
function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	var identityAvailabilityId=Number($('#identityAvailabilityId').val());
	if(returnVO.returnStatusName=='DeleteApproved')
		deleteIdentityApproval(identityAvailabilityId,messageId);
}
function deleteIdentityApproval(identityAvailabilityId,messageId){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/identity_availability_delete.action",  
     	data: {identityAvailabilityId:identityAvailabilityId,messageId: messageId},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result)
     	{  
			return false;
     	},
     	error:function(result)
        {
     		return false;
        } 
	}); 
	return false;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Document
			HandOver Information
		</div>
		<div class="tempresult" style="display: none;"></div>
		<div class="response-msg error commonErr ui-corner-all"
			style="display: none;"></div>
		<form id="ASSET_USATE" name="ASSET_USAGE">
			<div id="hrm" class="float-left width50">
				<input type="hidden" name="identityAvailabilityId"
					id="identityAvailabilityId"
					value="${HANDOVER_INFO.identityAvailabilityId}" />
				<fieldset>
					<div>
						<label class="width30">Document<span style="color: red">*</span>
						</label> <input type="text"
							class="masterTooltip width50 validate[required]" title=""
							readonly="readonly" name="identityType" id="identityType"
							value="${HANDOVER_INFO.identityType}" /> <input type="hidden"
							name="identityId" id="identityId"
							value="${HANDOVER_INFO.identity.identityId}" />
					</div>
					<div>
						<label class="width30">Document Of<span style="color: red">*</span>
						</label> <input type="text" class="width50" disabled="disabled"
							name="identityName" id="identityName"
							value="${HANDOVER_INFO.identityName}" />
					</div>
					<div>
						<label class="width30">Document Number<span
							style="color: red">*</span>
						</label> <input type="text" class="width50" disabled="disabled"
							name="identityNumber" id="identityNumber"
							value="${HANDOVER_INFO.identity.identityNumber}" />
					</div>
					<div>
						<label class="width30">HandOver To<span style="color: red">*</span>
						</label> <input type="text" class="width50" disabled="disabled"
							name="personName" id="personName"
							value="${HANDOVER_INFO.personName}" />
					</div>
					<div>
						<label class="width30">HandOver Date<span
							style="color: red">*</span>
						</label> <input type="text" class="width50 validate[required]"
							disabled="disabled" name="handoverDate" id="handoverDate"
							value="${HANDOVER_INFO.handoverDateStr}" />
					</div>
					<div>
						<label class="width30">Expected Return Date</label> <input
							type="text" class="width50" disabled="disabled"
							name="expectedReturnDate" id="expectedReturnDate"
							value="${HANDOVER_INFO.expectedReturnDateStr}" />
					</div>
					<div>
						<label class="width30">Process<span style="color: red">*</span>
						</label> <input type="hidden" id="statusTemp"
							value="${HANDOVER_INFO.status}" /> <select
							class="width50 validate[required]" name="status" id="status"
							disabled="disabled">
							<option value="1">Employer To Employee</option>
							<option value="2">Employee To Employer</option>
						</select>
					</div>

				</fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset>

					<div>
						<label class="width30">Purpose<span style="color: red">*</span>
						</label>
						<textarea class="width60 validate[required]" id="purpose"
							disabled="disabled">${HANDOVER_INFO.purpose}</textarea>
					</div>
					<div>
						<label class="width30">Description</label>
						<textarea class="width60" id="description" disabled="disabled">${HANDOVER_INFO.description}</textarea>
					</div>
					<div>
						<label class="width30">Actual Return Date</label> <input
							type="text" class="width60" id="actualReturnDate"
							disabled="disabled" value="${HANDOVER_INFO.actualReturnDateStr}" />
					</div>
					<div>
						<label class="width30">Is Active</label> <input type="checkbox"
							id="isActive" disabled="disabled" class="width10"> <input
							type="hidden" id="isActiveTemp" class="width10"
							value="${HANDOVER_INFO.isActive}">
					</div>

					<div>
						<label class="width30">Created Date</label> <input type="text"
							id="createdDate" disabled="disabled" class="width60"
							value="${HANDOVER_INFO.createdDateStr}">
					</div>
					<div>
						<label class="width30">Created By</label> <input type="text"
							id="createdPerson" disabled="disabled" class="width60"
							value="${HANDOVER_INFO.createdPerson}">
					</div>
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>
		<%-- <div
			class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right discard"
				id="discard">
				<fmt:message key="common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right disapprove"
				id="disapprove">DisApprove</div>
			<div class="portlet-header ui-widget-header float-right approve"
				id="approve">Approve</div>
		</div> --%>
	</div>
	<div
		style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		<div
			class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
			unselectable="on" style="-moz-user-select: none;">
			<span class="ui-dialog-title" id="ui-dialog-title-dialog"
				unselectable="on" style="-moz-user-select: none;">Dialog
				Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
				role="button" unselectable="on" style="-moz-user-select: none;"><span
				class="ui-icon ui-icon-closethick" unselectable="on"
				style="-moz-user-select: none;">close</span>
			</a>
		</div>
		<div id="common-popup" class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>
</div>