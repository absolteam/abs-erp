<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<tr class="rowidL" id="fieldrowL_${rowId}">
	<td id="lineIdL_${rowId}" style="display:none;">${rowId}</td>
	<td>
		<%-- <select class="width90 leaveType" id="leaveType_${i}"style="border:0px;">
			<option value="">Select</option>
			<c:forEach var="leavetype" items="${LEAVE_TYPE_LIST}" varStatus="statusL">
				<option value="${leavetype.leaveId}#${leavetype.defaultDays}">${leavetype.lookupDetail.displayName}</option>
			</c:forEach>
		</select> --%>
		<input type="text"  readonly="readonly" id="leaveType_${rowId}" value="" class="width70 float-left leaveType"/>
		<span class="button float-right" id="leave_${rowId}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" id="leavepopup_${rowId}" class="btn ui-state-default ui-corner-all width100 leaves-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span> 
		<input type="hidden" id="leaveId_${rowId}" value="" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" readonly="readonly" class="width70 days" id="days_${rowId}" style="border:0px;"/>
	</td> 
	<td id="isMonthly_${rowId}">
	</td>
	<td id="isYearly_${rowId}">
	</td>
	<td id="isService_${rowId}">
	</td>
	<td style="display:none;">
		<input type="hidden" id="jobLeaveId_${rowId}" style="border:0px;"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="optionL_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataL" id="AddImageL_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataL" id="EditImageL_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowL" id="DeleteImageL_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageL_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>    
</tr>