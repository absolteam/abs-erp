<tr id="fieldrow_${requestScope.id}" class="rowid"> 
<td class="width10" id="dependentName_${requestScope.id}"></td>
     <td class="width10" id="dependentRelationship_${requestScope.id}"></td>
     <td class="width10" id="dependentAddress_${requestScope.id}"></td>
    	<td class="width10" id="dependentMobile_${requestScope.id}"></td>
    	<td class="width10" id="dependentPhone_${requestScope.id}"></td>
    	<td class="width10" id="dependentEmail_${requestScope.id}"></td>
    	<td class="width10" id="emergency_${requestScope.id}"></td>
   	<td class="width10" id="dependent_${requestScope.id}"></td>
   	<td class="width10" id="benefit_${requestScope.id}"></td>
<td  style="width:5%;" id="option_${requestScope.id}">
<input type="hidden" name="dependentDescription_${i}" id="dependentDescription_${requestScope.id}"  value=""/>
<input type="hidden" name="dependentRelationshipId_${requestScope.id}" id="dependentRelationshipId_${requestScope.id}"  value="0"/>
<input type="hidden" name="dependentId_${requestScope.id}" id="dependentId_${requestScope.id}"  value="0"/>  
<input type="hidden" name="lineId_${requestScope.id}" id="lineId_${requestScope.id}" value="${requestScope.id}"/>  
<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${requestScope.id}"  title="Add Record">
<span class="ui-icon ui-icon-plus"></span>
</a>	
<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  id="EditImage_${requestScope.id}" style="display:none; cursor: pointer;" title="Edit Record">
	<span class="ui-icon ui-icon-wrench"></span>
</a> 
<a style="cursor: pointer;display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  id="DeleteImage_${requestScope.id}" title="Delete Record">
	<span class="ui-icon ui-icon-circle-close"></span>
</a>
<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${requestScope.id}" style="display:none;" title="Working">
			<span class="processing"></span>
		</a>
	</td> 
	
</tr>
<%-- <script type="text/javascript"> 
$(function(){ //Dependent Detail adding
	 $(".addDatanew").click(function(){ 
			slidetab=$(this).parent().parent().get(0);  
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			if($("#editCalendarVal").validationEngine({returnIsValid:true})){
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/person_dependent_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			}
			else{
				return false;
			}	
			 
		});

		$(".editDatanew").click(function(){
			 
			 slidetab=$(this).parent().parent().get(0);  
			 
			 if($("#editCalendarVal").validationEngine({returnIsValid:true})) {
				 
					 
					//Find the Id for fetch the value from Id
						var tempvar=$(this).attr("id");
						var idarray = tempvar.split('_');
						var rowid=Number(idarray[1]); 
						$("#AddImage_"+rowid).hide();
						$("#EditImage_"+rowid).hide();
						$("#DeleteImage_"+rowid).hide();
						$("#WorkingImage_"+rowid).show(); 
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/person_dependent_add.action",
					 data:{id:rowid,addEditFlag:"E"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){
					
				   $(result).insertAfter(slidetab);

				 //Bussiness parameter
		   			var dependentName=$('#dependentName_'+rowid).text();
		   			var relationship=$('#dependentRelationship_'+rowid).text();
		   			var relationshipId=$('#dependentRelationshipId_'+rowid).val(); 
     			var dependentMobile=$('#dependentMobile_'+rowid).text(); 
     			var dependentPhone=$('#dependentPhone_'+rowid).text(); 
     			var dependentMail=$('#dependentEmail_'+rowid).text();
     			var emergency=$('#emergency_'+rowid).text();
     			var dependent=$('#dependent_'+rowid).text();
     			var benefit=$('#benefit_'+rowid).text();
     			var dependentAddress=$('#dependentAddress_'+rowid).text();
     			var dependentDescription=$('#dependentDescription_'+rowid).val();
     			
		   			
     			$('#relationship').val(relationshipId);
     			$('#dependentName').val(dependentName);
     			$('#dependentMobile').val(dependentMobile);  
     			$('#dependentPhone').val(dependentPhone);
     			$('#dependentMail').val(dependentMail);
     			$('#dependentAddress').val(dependentAddress);
     			$('#dependentDescription').val(dependentDescription);
     			if(emergency=='true')
     				$('#emergency').attr("checked","checked")
     			if(dependent=='true')	
     				$('#dependent').attr("checked","checked")
     			if(benefit=='true')	
     				$('#benefit').attr("checked","checked")
     				
				    $('#openFlag').val(1);
				   
				},
				error:function(result){
				//	alert("wee"+result);
				}
				
				});
				 			
			 }
			 else{
				 return false;
			 }	
			 
		});
		 
		 $(".delrownew").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 var lineId=$('#lineId_'+rowid).val();
			var flag=false;
			
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/person_dependent_save.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
					 success:function(result){ 
						 $('.tempresult').html(result);   
						 if(result!=null){
							 $("#fieldrow_"+rowid).remove(); 
				        		//To reset sequence 
				        		var i=0;
				     			$('.rowid').each(function(){  
									i=i+1;
									$($($(this).children().get(4)).children().get(1)).val(i); 
			   					 }); 
						     }  
					},
					error:function(result){
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 });
				 
			 
		 });
		 
});	
</script>	 --%>