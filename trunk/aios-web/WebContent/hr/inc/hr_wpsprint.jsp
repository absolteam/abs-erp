<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WPS File</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
</style>
<body>
	<div class="width100 float-left">
		<span class="side-heading">WPS Information</span>
	</div>
	<div class="data-list-container width100 float-left"
		style="margin-top: 10px;">
		<c:choose>
			<c:when test="${fn:length(PAYROLL_EDR)gt 0}">
				<table class="width100">
					<thead>
						<tr>
							<th>Employee Name</th>
							<th>Record Type</th>
							<th>Labour Card</th>
							<th>Routing Code</th>
							<th>IBAN</th>
							<th>Period Start</th>
							<th>Period End</th>
							<th>No.of Days</th>
							<th>Net Pay</th>
							<th>Variable</th>
							<th>LOP Days</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${PAYROLL_EDR}" var="result"
							varStatus="status1">
							<tr>
								<td>${result.employeeName}</td>
								<td>${result.recordType}</td>
								<td>${result.labourCardId}</td>
								<td>${result.routingCode}</td>
								<td>${result.ibanNumber}</td>
								<td>${result.periodStart}</td>
								<td>${result.periodEnd}</td>
								<td>${result.numberOfDays}</td>
								<td>${result.netAmountStr}</td>
								<td>${result.variableSalary}</td>
								<td>${result.lopDays}</td>
							</tr>
						</c:forEach>
						<tr>
							<td></td>
							<td>${PAYROLL_SCR.recordType}</td>
							<td>${PAYROLL_SCR.labourCardId}</td>
							<td>${PAYROLL_SCR.routingCode}</td>
							<td>${PAYROLL_SCR.createdDateStr}</td>
							<td>${PAYROLL_SCR.createdTime}</td>
							<td>${PAYROLL_SCR.payMonth}</td>
							<td>${PAYROLL_SCR.numberOfRecord}</td>
							<td>${PAYROLL_SCR.netAmountStr}</td>
							<td>${PAYROLL_SCR.currency}</td>
							<td>${PAYROLL_SCR.documentryField}</td>
						</tr>
					</tbody>
				</table>
			</c:when>
			<c:otherwise>
				<tr class="even rowid">No Records Found
				</tr>
			</c:otherwise>
		</c:choose>
	</div>
	<%-- <c:choose>
			<c:when test="${fn:length(PAYROLL_EDR)gt 0}">
					<c:forEach items="${PAYROLL_EDR}" var="result" varStatus="status1" >
					 	${result.recordType},${result.labourCardId},${result.routingCode}
					 	,${result.ibanNumber},${result.periodStart},${result.periodEnd}
					 	,${result.numberOfDays},${result.netAmountStr},${result.variableSalary}
					 	,${result.lopDays}--${result.employeeName}<br>
					</c:forEach>
			</c:when>
		</c:choose>
			${PAYROLL_SCR.recordType},${PAYROLL_SCR.labourCardId},${PAYROLL_SCR.routingCode},
			${PAYROLL_SCR.createdDateStr},${PAYROLL_SCR.createdTime},${PAYROLL_SCR.payMonth},
			${PAYROLL_SCR.numberOfRecord},${PAYROLL_SCR.netAmountStr},${PAYROLL_SCR.currency},
			${PAYROLL_SCR.documentryField} --%>
</body>
</html>