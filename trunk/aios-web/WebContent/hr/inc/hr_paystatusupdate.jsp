<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>

<script type="text/javascript"> 
var payLineDetail="";

$(document).ready(function(){ 

		
		editDataSelectCall();

		
		  
			
			$('#shift-discard').click(function(){ 
				$('#job-common-popup').dialog('close');
				return false;
			 });
			
			$('#globalCheckBox').change(function(){ 
				if($('#globalCheckBox').attr("checked"))
					$('.checkedPay').attr("checked",true);
				else
					$('.checkedPay').attr("checked",false);
					
			 });
			
			$jquery("#shift-details").validationEngine('attach');

			 $('#shift-save').click(function(){

				 if($jquery("#shift-details").validationEngine('validate')){   
					 payLineDetail=getPayLineDetails();
					 var exchangeRate=$('#exchangeRate').val();
		 			 var accountId=$('#accountId').val();
		 			 if(payLineDetail==null || payLineDetail==""){
		 				$('#page-error').hide().html("Atleast one pay information is required to update.").slideDown(1000);
		 			 }
		 				 
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/update_pay_status.action", 
							data:{
								accountId:accountId,
								exchangeRate:exchangeRate,
								payLineDetail:payLineDetail
								},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){ 
								$(".tempresult").html(result);
								 var message=$('.tempresult').html(); 
								 if(message.trim()=="SUCCESS"){
										$('.success').hide().html("Successfully Updated").slideDown(1000);
										$('#job-common-popup').dialog('close');
								 }
								 else{
									 $('#page-error').hide().html("Update Failure! Please check the employee/payroll element account information").slideDown(1000);
									 $('#job-common-popup').dialog('close');
									 return false;
								 }
							}
					 });
				 }
				 else{
					return false;
				}
				 return false;
			 });
			 
			 
			 
			
});
	
	function editDataSelectCall(){
		
		$('.rowidS').each(function(){
			var rowId=getRowId($(this).attr('id'));
			$("#salaryStatus_"+rowId).val($("#salaryStatusTemp_"+rowId).val());
			
		});
		
	}
	function manupulateShiftLastRow(){
		var hiddenSize=0;
		$($(".tabS>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		var tdSize=$($(".tabS>tr:first").children()).size();
		var actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabS>tr:last').removeAttr('id');  
		$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
		$($('.tabS>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	
	
	function getPayLineDetails(){
		payLineDetail="";
		var payrollIds=new Array();
		var payStatuses=new Array();
		

		$('.rowidS').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var payrollId=$('#payrollId_'+rowId).val();
			if(payrollId==null || payrollId=='' || payrollId==0 || payrollId=='undefined')
				payrollId=-1;
			
			var payStatus=$('#salaryStatus_'+rowId).val();
			if(payStatus==null || payStatus=='' || payStatus=='undefined')
				payStatus=-1;
			
			
			//var statusShift=$('#statusShift_'+rowId).val();
			if(typeof payrollId != 'undefined' && payrollId!=null && payrollId!=0 
					&& payStatus!=null && payStatus!="" && $('#checkedPay_'+rowId).is(':checked')){
				payrollIds.push(payrollId);
				payStatuses.push(payStatus);
				
			}
		});
		for(var j=0;j<payrollIds.length;j++){ 
			payLineDetail+=payrollIds[j]+"@"+payStatuses[j];
			if(j==payrollIds.length-1){   
			} 
			else{
				payLineDetail+="##";
			}
		} 
		return payLineDetail;
	}
</script>

<body>
	<div id="main-content">	
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"><span>Process Failure!</span></div>  
			 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
			
			<div class="width98 float-left" id="hrm">
			 <form id="shift-details" name="shift-details">
			 	<fieldset style="min-height:20px;">
			 		<legend>Exchange rate</legend> 
					<div class="width100 float-left">
						<label class="width40 float-left">Note : Use exchange rate if foreign currency pay.</label>
						<input type="text" id="exchangeRate" class="width20 float-left">
						
					</div>
				</fieldset>
				<div class="width100 float-left">
						<fieldset>
							<legend>Pay Details</legend>
							<div id="hrm" class="hastable width100"  >  
						<table id="hastab2" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><input type="checkbox" class="width10 checkedPay" 
									name="checkedPay" id="globalCheckBox" style="border:0px;"/></th>
									<th style="width:5%">Employee</th>
								    <th style="width:5%">Location</th>  
								    <th style="width:5%">PayMonth</th>
								    <th style="width:5%">Period Start</th>
								    <th style="width:5%">Period End</th>
								    <th style="width:5%">Net Amount</th>
								    <th style="width:5%">Number of Days</th>
								    <th style="width:25%">Salary Status</th>
							  </tr>
							</thead> 
									<tbody class="tabS">
										<c:choose>
											<c:when test="${PAYROLL_LIST ne null && PAYROLL_LIST ne '' && fn:length(PAYROLL_LIST)>0}">
												<c:forEach var="payrol" items="${PAYROLL_LIST}" varStatus="status1">
														<tr class="rowidS" id="fieldrowS_${status1.index+1}">
															<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
															<td>
																<input type="checkbox" class="width10 checkedPay" name="checkedPay" id="checkedPay_${status1.index+1}" style="border:0px;"/> 
															</td> 
															<td>
																 ${payrol.jobAssignment.person.firstName} ${payrol.jobAssignment.person.lastName} 
																 (${payrol.jobAssignment.designation.designationName})
															</td> 
														
															<td>
																 ${payrol.jobAssignment.cmpDeptLocation.company.companyName} >> 
																 ${payrol.jobAssignment.cmpDeptLocation.department.departmentName}
															</td> 
															<td>
																 ${payrol.payMonth}
															</td>
															<td>
																 ${payrol.periodStart}
															</td> 
															<td>
																${payrol.periodEnd}
															</td>  
															<td>
																 ${payrol.netAmountStr}
															</td> 
															<td>
																 ${payrol.numberOfDays}
															</td> 
															<td style="width:25%">
																<select class="salaryStatus" id="salaryStatus_${status1.index+1}" style="border:0px;">
																	<option value="1">InProcess</option>
																	<option value="2">Commit</option>
																	<option value="3">Deposit</option>
																	<option value="4">Cancel</option>
																	<option value="5">Hold</option>
																	<option value="6">Pending</option>
																</select>
																<input type="hidden" id="salaryStatusTemp_${status1.index+1}" value="${payrol.status}"/>
															</td> 
															<td style="display:none;">
																<input type="hidden" id="payrollId_${status1.index+1}" value="${payrol.payrollId}"/>
															</td>
														</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										
							 </tbody>
					</table>
				</div> 
						</fieldset>
				 </div> 
				</form>
				<div class="clearfix"></div> 
				<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right shift-discard" id="shift-discard" >cancel</div>
					<div class="portlet-header ui-widget-header float-right shift-save" id="shift-save" >save</div> 
				</div>
			</div>
			
			<div class="clearfix"></div> 
			<div id="sub-content" class="width100 float-left"> 
			</div> 
			
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="job-common-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		 	</div>
		 	
		 	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
				<div class="portlet-header ui-widget-header float-left addrowsS" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
			</div>
		</div>
	</div>
</body>
</html>