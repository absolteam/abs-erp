<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>
.container {float: left;padding: 1px;}
.container select{width: 30em;height:10em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 30px;} 
</style> 
<script type="text/javascript"> 
var shiftLineDetail="";

$(document).ready(function(){ 
		manupulateShiftLastRow();

		var employeeCalendarId=Number($("#employeeCalendarId").val());
		if(employeeCalendarId>0){
			editDataSelectCall();
		}

		$jquery("#shift-details").validationEngine('attach');
		  
			
			$('#shift-discard').click(function(){ 
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/employee_calendar.action", 
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){ 
							$("#main-wrapper").html(result);  
						}
				 });
			 });

			 $('#shift-save').click(function(){
				 $('.error,.success').hide();
				 if($jquery("#shift-details").validationEngine('validate')){   
					 shiftLineDetail=getShiftLineDetails();
					 var employeeCalendarId=Number($('#employeeCalendarId').val());
					 var successMessage="Successfully Created";
					 if(employeeCalendarId>0)
						 successMessage="Successfully Modified";
					 
					 var jobAssignmentId=$('#jobAssignmentId').val();
					 var calendarName=$('#calendarName').val();
					 var locationId=$('#locationId').val();
					 
					 if((jobAssignmentId==null || jobAssignmentId=='' || jobAssignmentId==0)
							 && (locationId==null || locationId=='' || locationId==0) ){
						 $('#page-error').hide().html("Select either Location or Employee").slideDown(1000);
						 return false;
					 }
						 
					 var weekend='';
						$('#weekend option:selected').each(function(){ 
							if($(this).val()!='')
								weekend = weekend+$(this).val()+","; 
						});
					 var locationGroup='';
						$('#right option').each(function(){ 
							if($(this).val()!='')
								locationGroup = locationGroup+$(this).val()+","; 
						});
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/save_employee_calendar.action", 
							data:{
									employeeCalendarId:employeeCalendarId,calendarLineDetail:shiftLineDetail,jobAssignmentId:jobAssignmentId,
									locationId:locationId,weekend:weekend,locationGroup:locationGroup,calendarName:calendarName
								},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){ 
								$(".tempresult").html(result);
								 var message=$('.tempresult').html(); 
								 if(message.trim()=="SUCCESS"){
									 $.ajax({
											type:"POST",
											url:"<%=request.getContextPath()%>/employee_calendar.action", 
										 	async: false,
										    dataType: "html",
										    cache: false,
											success:function(result){
												$("#main-wrapper").html(result); 
												$('.success').hide().html(successMessage).slideDown(1000);
											}
									 });
								 }
								 else{
									 $('#page-error').hide().html(message).slideDown(1000);
									 return false;
								 }
							}
					 });
				 }
				 else{
					return false;
				}
			 });
			 
			 			
			 $('.startDate,.endDate').datepick({
				 onSelect: customRanges,showTrigger: '#calImg'});

			 $('#locationId').change(function(){
				 $('#jobAssignmentId').val('');
			 });

			 $('#jobAssignmentId').change(function(){
				 $('#locationId').val('');
			 });
			 
			//On Change Events
			$('.startDate').live('change',function(){
				if($(this).val()!=""){
					var rowId=getRowId($(this).attr('id')); 
					triggerShiftAddRow(rowId);
					return false;
				} 
				else{
					return false;
				}
			});	
			$('.endDate').live('change',function(){
				if($(this).val()!=""){
					var rowId=getRowId($(this).attr('id')); 
					triggerShiftAddRow(rowId);
					return false;
				} 
				else{
					return false;
				}
			});	
			
			$('.holidayType').live('change',function(){
				if($(this).val()!=""){
					var rowId=getRowId($(this).attr('id')); 
					triggerShiftAddRow(rowId);
					return false;
				} 
				else{
					return false;
				}
			});	
			
			$('#locationId').change(function(){
				if($(this).val()!="" && $(this).val()>0){
					$('#locationGroupDiv').show();
				} 
				else{
					$('#locationGroupDiv').hide();
				}
				return false;

			});	
			
			$('.addrowsS').click(function(){ 
				  var i=Number(1); 
				  var id=Number(getRowId($(".tabS>.rowidS:last").attr('id'))); 
				  id=id+1;   
				  $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/employee_calendar_addrow.action", 
					 	async: false,
					 	data:{id: id},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
							$('.tabS tr:last').before(result);
							 if($(".tabS").height()>255)
								 $(".tabS").css({"overflow-x":"hidden","overflow-y":"auto"}); 
							 $('.rowidS').each(function(){   
					    		 var rowId=getRowId($(this).attr('id')); 
					    		 $('#lineIdS_'+rowId).html(i);
								 i=i+1; 
					 		 }); 
							 return false;
						}
					});
				  return false;
			 }); 
			
			$('.delrowS').live('click',function(){ 
				 slidetab=$(this).parent().parent().get(0);  
			  	 $(slidetab).remove();  
			  	 var i=1;
				 $('.rowidS').each(function(){   
					 var rowId=getRowId($(this).attr('id')); 
					 $('#lineIdS_'+rowId).html(i);
						 i=i+1; 
					 });  
				 return false;
			 });
			
			$(".low input[type='button']").click(function(){
	    	    var arr = $(this).attr("name").split("2");
	    	    var from = arr[0];
	    	    var to = arr[1];
	    	    $("#" + from + " option:selected").each(function(){
	    	      $("#" + to).append($(this).clone());
	    	      $(this).remove();
	    	    });
	    	}); 
		    
	});
	function reload(){
		 $(".low input[type='button']").each(function(){
	    	    var arr = $(this).attr("name").split("2");
	    	    var from = arr[0];
	    	    var to = arr[1];
	    	    $("#" + from + " option:selected").each(function(){
	    	      $("#" + to).append($(this).clone());
	    	      $(this).remove();
	    	    });
	    }); 
	}
	function customRanges(dates) {
		var rowId=getRowId(this.id); 
		if (this.id == 'startDate_'+rowId) {
			$('#endDate_'+rowId).datepick('option', 'minDate', dates[0] || null);  
			if($(this).val()!=""){
				triggerShiftAddRow(rowId);
				return false;
			} 
			else{
				return false;
			}
		}
		else{
			$('#startDate_'+rowId).datepick('option', 'maxDate', dates[0] || null); 
			if($(this).val()!=""){
				triggerShiftAddRow(rowId);
				return false;
			} 
			else{
				return false;
			}
		} 
	}

	function editDataSelectCall(){
		
		$('.rowidS').each(function(){
			var rowId=getRowId($(this).attr('id'));
			$("#holidayType_"+rowId).val($("#holidayTypeTemp_"+rowId).val());
			
		});
		
		$("#locationId").val($("#locationIdTemp").val());
		$("#jobAssignmentId").val($("#jobAssignmentIdTemp").val());
		
		var locationGroupTemp=$("#lecationGroupTemp").val();
		var locGrop=locationGroupTemp.split(',');
		for(var i=0;i<locGrop.length;i++){
			$('.locationGroup option[value='+locGrop[i]+']').attr("selected","selected");
		}
		
		reload();
		var weekendTemp=$("#weekendTemp").val();
		var weekendGrop=weekendTemp.split(',');
		for(var i=0;i<weekendGrop.length;i++){
			$('#weekend option[value='+weekendGrop[i]+']').attr("selected","selected");
		}
		
		//Location Group show/hide option
		if($("#locationId").val()!="" && $("#locationId").val()>0){
			$('#locationGroupDiv').show();
		} 
		else{
			$('#locationGroupDiv').hide();
		}
		return false;
	}
	function manupulateShiftLastRow(){
		var hiddenSize=0;
		$($(".tabS>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		var tdSize=$($(".tabS>tr:first").children()).size();
		var actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabS>tr:last').removeAttr('id');  
		$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
		$($('.tabS>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	
	function triggerShiftAddRow(rowId){ 
		var holidayType=$('#holidayType_'+rowId).val();  
		var startDate=$('#startDate_'+rowId).val();
		var endDate=$('#endDate_'+rowId).val();
		var nexttab=$('#fieldrowS_'+rowId).next(); 
		if(holidayType!=null && holidayType!=""
				&& startDate!=null && startDate!="" && endDate!=null && endDate!="" && $(nexttab).hasClass('lastrow')){ 
			$('#DeleteImageS_'+rowId).show();
			$('.addrowsS').trigger('click');
			return false;
		}else{
			return false;
		} 
	}
	function getShiftLineDetails(){
		shiftLineDetail="";
		var jobShiftIds=new Array();
		var startDates=new Array();
		var endDates=new Array();
		var irregulars=new Array();
		var jobAssignmentIds=new Array();

		$('.rowidS').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var employeeCalendarPeriodId=$('#employeeCalendarPeriodId_'+rowId).val();
			if(employeeCalendarPeriodId==null || employeeCalendarPeriodId=='' || employeeCalendarPeriodId==0 || employeeCalendarPeriodId=='undefined')
				employeeCalendarPeriodId=-1;
			var startDate=$('#startDate_'+rowId).val();
			var endDate=$('#endDate_'+rowId).val();
			var holidayType=$('#holidayType_'+rowId).val();
			if(holidayType==null || holidayType=='' || holidayType=='undefined')
				holidayType=-1;
			
			var description=$('#description_'+rowId).val().trim();
			if(description==null || description=='' ||  description=='undefined')
				description=-1;
			
			var jobId=$('#jobId_'+rowId).val();
			if(jobId==null || jobId=='' || jobId==0 || jobId=='undefined')
				jobId=-1;
			//var statusShift=$('#statusShift_'+rowId).val();
			if(typeof holidayType != 'undefined' && holidayType!=null && holidayType!="" && startDate!=null && startDate!=""){
				jobShiftIds.push(employeeCalendarPeriodId);
				startDates.push(startDate);
				endDates.push(endDate);
				irregulars.push(holidayType);
				jobAssignmentIds.push(description);
				//statuses.push(statusShift);
			}
		});
		for(var j=0;j<jobShiftIds.length;j++){ 
			shiftLineDetail+=jobShiftIds[j]+"@"+startDates[j]+"@"+endDates[j]+"@"+irregulars[j]+"@"+jobAssignmentIds[j];
			if(j==jobShiftIds.length-1){   
			} 
			else{
				shiftLineDetail+="##";
			}
		} 
		return shiftLineDetail;
	}
</script>

<body>
	<div id="main-content">	
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Holiday & Work Off Information</div>
		<div class="portlet-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div id="page-error" class="response-msg error ui-corner-all" style="display:none;"><span>Process Failure!</span></div>  
			  	<div class="tempresult" style="display:none;"></div>
			<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
			<div class="width98 float-left" id="hrm">
			 <form id="shift-details" name="shift-details" style="position: relative;">
			 	<div class="width100 float-left" id="hrm"> 
					<div class="width60 float-left">
						<fieldset style="height:150px;">
							<legend>General Information</legend>
							<div class="width100 float-left">
								<input type="hidden" id="employeeCalendarId" value="${EMPLOYEE_CALENDAR.employeeCalendarId}"/>
								<div>
									<label class="width30" for="calendarNumber">Calendar Number<span class="mandatory">*</span></label>
									<input type="text" id="calendarNumber" class="width60 validate[required]" readonly="readonly" value="${EMPLOYEE_CALENDAR.calendarNumber}"/>
									
								</div> 
								<div>
									<label class="width30" for="calendarName">Calendar Name<span class="mandatory">*</span></label>
									<input type="text" id="calendarName" maxlength="150" class="width60 validate[required]" value="${EMPLOYEE_CALENDAR.calendarName}"/>
								</div>  
								<div  id="locationDiv">
									<label class="width30" for="locationId">Location</label>
									<select id="locationId" name="locationId" class="width61 validate[groupRequired[temp]">
										<option value="">--Select--</option>
										<c:forEach items="${LOCATIONS}" var="comp" varStatus="status">
											<option value="${comp.locationId}">${comp.company.companyName}-${comp.locationName}</option>
										</c:forEach>
									</select>
									<input type="hidden" id="locationIdTemp" value="${EMPLOYEE_CALENDAR.location.locationId}"/>
								</div>  
								<div id="jobAssignmentDiv">
									<label class="width30" for="jobAssignmentId">Employee</label>
									<select id="jobAssignmentId" name="jobAssignmentId" class="width61 validate[groupRequired[temp]">
										<option value="">--Select--</option>
										<c:forEach items="${JOB_ASSIGNMENT_LIST}" var="job" varStatus="status">
											<option value="${job.jobAssignmentId}">${job.person.firstName} ${job.person.lastName}</option>
										</c:forEach>
									</select>
									<input type="hidden" id="jobAssignmentIdTemp" value="${EMPLOYEE_CALENDAR.jobAssignment.jobAssignmentId}"/>
									
								</div>  
								<div>
									<label class="width60" style="font-weight: bold;top:6px;">Note<span class="mandatory">*</span> : Select either Location or Employee.</label>
								</div>
							</div>
						</fieldset>
				 </div> 
				  <div class="width40 float-left">
					<fieldset style="height:155px;">
						<legend>Weekend Day(s)</legend>
						  <div>
						    <select class="width100" name="weekend" id="weekend" size="7" multiple="multiple">
						    	<option value="">NONE</option>
								<option value="SUNDAY">SUNDAY</option>
								<option value="MONDAY">MONDAY</option>
								<option value="TUESDAY">TUESDAY</option>
								<option value="WEDNESDAY">WEDNESDAY</option>
								<option value="THURSDAY">THURSDAY</option>
								<option value="FRIDAY">FRIDAY</option>
								<option value="SATURDAY">SATURDAY</option>
						    </select>
						    <input type="hidden" id="weekendTemp" value="${EMPLOYEE_CALENDAR.weekend}"/>
						    
						  </div>
					</fieldset>
				</div>
				 <div class="width100 float-left" id="locationGroupDiv" style="display: none;">
					<fieldset style="height:150px;">
					<legend>Location Group</legend>
						<input type="hidden" id="lecationGroupTemp" value="${EMPLOYEE_CALENDAR.locationGroup}"/>
						  <div class="container">
						    <select name="locationGroup" id="left" size="4" multiple="multiple" class="locationGroup">
						      <c:forEach items="${LOCATIONS}" var="comp" varStatus="status">
								<option value="${comp.locationId}">${comp.company.companyName}-${comp.locationName}</option>
							</c:forEach>
						    </select>
						  </div>
						  
						  <div class="low container">
						    <input name="left2right" value="add" type="button"><br>
						    <input name="right2left" value="remove" type="button">
						  </div>
						  
						  <div class="container">
						    <select name="itemsToAdd" id="right" size="8" multiple="multiple">
						    </select>
						 </div> 
					</fieldset>
				</div>
				</div>
				<div class="width100 float-left">
						<fieldset>
							<legend>Holiday/Off Details</legend>
							<div id="hrm" class="hastable width100"  >  
						<table id="hastab2" class="width100"> 
							<thead>
								<tr> 
									<th style="width:5%"><fmt:message key="hr.job.periodstartdate"/></th>  
								    <th style="width:5%"><fmt:message key="hr.job.periodenddate"/></th>
								    <th style="width:5%">Holiday/Off Type</th>
								    <th style="width:5%">Description</th>
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
									<tbody class="tabS">
										<c:choose>
											<c:when test="${EMPLOYEE_CALENDAR_LIST ne null && EMPLOYEE_CALENDAR_LIST ne '' && fn:length(EMPLOYEE_CALENDAR_LIST)>0}">
												<c:forEach var="empCalPeriod" items="${EMPLOYEE_CALENDAR_LIST}" varStatus="status1">
													<tr class="rowidS" id="fieldrowS_${status1.index+1}">
														<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>
															 <c:choose>
																	<c:when test="${empCalPeriod.fromDate ne null &&  empCalPeriod.fromDate ne ''}">
																		<c:set var="startDate" value="${empCalPeriod.fromDate}"/>  
																		<%String startDate = DateFormat.convertDateToString(pageContext.getAttribute("startDate").toString());%>
																		<input type="text" class="width90 startDate" readonly="readonly" name="startDate" id="startDate_${status1.index+1}" style="border:0px;" 
																		value="<%=startDate%>"/>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="width90 startDate" readonly="readonly" name="startDate" id="startDate_${status1.index+1}" style="border:0px;"/>
																	</c:otherwise>
															</c:choose> 
														</td> 
														<td>
															 <c:choose>
																	<c:when test="${empCalPeriod.toDate ne null &&  empCalPeriod.toDate ne ''}">
																		<c:set var="endDate" value="${empCalPeriod.toDate}"/>  
																		<%String endDate = DateFormat.convertDateToString(pageContext.getAttribute("endDate").toString());%>
																		<input type="text" class="width90 endDate" readonly="readonly" name="endDate" id="endDate_${status1.index+1}" style="border:0px;" 
																		value="<%=endDate%>"/>
																	</c:when>
																	<c:otherwise>
																		<input type="text" class="width90 endDate" readonly="readonly" name="endDate" id="endDate_${status1.index+1}" style="border:0px;"/>
																	</c:otherwise>
															</c:choose> 
														</td> 
														<td>
															<select class="width90 holidayType" id="holidayType_${status1.index+1}"style="border:0px;">
																<option value="">Select</option>
																<c:forEach items="${HOLIDAY_TYPE_LIST}" var="offList" varStatus="status4">
																	<option value="${offList.holidayTypeId}">${offList.holidayType}</option>
																</c:forEach>
															</select>
															<input type="hidden" id="holidayTypeTemp_${status1.index+1}" value="${empCalPeriod.holidayType}" style="border:0px;"/>
														</td> 
														<td>
															<textarea id="description_${status1.index+1}" class="width90 " style="border:0px;">${empCalPeriod.description}</textarea>
														</td>
														<td style="display:none;">
															<input type="hidden" id="employeeCalendarPeriodId_${status1.index+1}" value="${empCalPeriod.employeeCalendarPeriodId}" style="border:0px;"/>
														</td>
														 <td style="width:0.01%;" class="opn_td" id="optionS_${status1.index+1}">
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
									 								<span class="ui-icon ui-icon-plus"></span>
															  </a>	
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																	<span class="ui-icon ui-icon-wrench"></span>
															  </a> 
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span>
															  </a>
															  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${status1.index+1}" style="display:none;" title="Working">
																	<span class="processing"></span>
															  </a>
														</td>
													</tr>
												</c:forEach>  
											</c:when>
										</c:choose>  
										<c:forEach var="i" begin="${fn:length(EMPLOYEE_CALENDAR_LIST)+1}" end="${fn:length(EMPLOYEE_CALENDAR_LIST)+2}" step="1" varStatus="status"> 
											<tr class="rowidS" id="fieldrowS_${i}">
												<td>
													<input type="text" class="width90 startDate" readonly="readonly" name="startDate" id="startDate_${i}" style="border:0px;"/>
												</td> 
												<td>
													
													<input type="text" class="width90 endDate" readonly="readonly" name="endDate" id="endDate_${i}" style="border:0px;"/>
															
												</td> 
												<td>
													<select class="width90 holidayType" id="holidayType_${i}"style="border:0px;">
														<option value="">Select</option>
														<c:forEach items="${HOLIDAY_TYPE_LIST}" var="offList" varStatus="status4">
															<option value="${offList.holidayTypeId}">${offList.holidayType}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="holidayTypeTemp_${i}" value="" style="border:0px;"/>
												</td> 
												<td>
													<textarea id="description_${i}" class="width90 " style="border:0px;"></textarea>
												</td>
												<td style="display:none;">
													<input type="hidden" id="employeeCalendarPeriodId_${i}" value="" style="border:0px;"/>
												</td>
												 <td style="width:0.01%;" class="opn_td" id="optionS_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataS" id="AddImageS_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataS" id="EditImageS_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowS" id="DeleteImageS_${i}" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageS_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>    
											</tr>
									</c:forEach>
							 </tbody>
					</table>
				</div> 
						</fieldset>
				 </div> 
				</form>
				<div class="clearfix"></div> 
				<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right shift-discard" id="shift-discard" >cancel</div>
					<div class="portlet-header ui-widget-header float-right shift-save" id="shift-save" >save</div> 
				</div>
			</div>
			
			<div class="clearfix"></div> 
			<div id="sub-content" class="width100 float-left"> 
			</div> 
			
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="job-common-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		 	</div>
		 	
		 	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
				<div class="portlet-header ui-widget-header float-left addrowsS" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
			</div>
		</div>
	</div>
</div>
</body>
</html>