<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var jobName="";var jobId=0;var employees="";
$(function(){ 
	var workingShiftId="${workingShiftId}";
	$('#Job').dataTable({ 
		"sAjaxSource": "shift_job_json.action?workingShiftId="+workingShiftId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'JobId', "bVisible": false},
			{ "sTitle": 'Job Number'},
			{ "sTitle": 'Job Name'},
			{ "sTitle": 'Employee(s)',"sWidth": "400px"},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Job').dataTable();

	/* Click event handler */
	$('#Job tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobNumber=aData[1];
	        jobName=aData[2];
	        employees=aData[3];        
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobNumber=aData[1];
	        jobName=aData[2];
	        employees=aData[3]; 
	    }
	});
	$('#Job tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobNumber=aData[1];
	        jobName=aData[2];
	        employees=aData[3]; 
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobId=aData[0];
	        jobNumber=aData[1];
	        jobName=aData[2];
	        employees=aData[3]; 
	    }
		var rowId= $("#rowId").val();
		$("#jobId_"+rowId).val(jobId);
		$("#jobName_"+rowId).html(jobName+"("+jobNumber+")");
		$("#employees_"+rowId).html(employees);
		/* $("#designationName").html(designationName);
		$("#gradeName").html(gradeName);
		$("#companyName").html(companyName);
		$("#departmentName").html(departmentName);
		$("#locationName").html(locationName);
		$("#swipeId").html(swipeId); */
		
		$('#job-common-popup').dialog("close");
	});
});
</script>
<div id="main-content"> 
	<input type="hidden" id="rowId" value="${rowid}"/>
 	<div id="trans_combination_accounts">
		<table class="display" id="Job"></table>
	</div> 
</div>