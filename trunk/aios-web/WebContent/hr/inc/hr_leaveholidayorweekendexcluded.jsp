<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="width30 float-left">
	<label class="width55">Total Leave Days</label>
	<input type="text" readonly="readonly" class="width35" id="totalDaysResult" value="${LEAVE_DAYS.totalDays}"/>
</div>
<div class="width35 float-left">
	<label class="width55">Holiday Excluded Days</label>
	<input type="text" readonly="readonly" class="width35" id="excludedHolidaysResult" value="${LEAVE_DAYS.excludedHolidays}"/>
</div>
<div class="width35 float-left">
	<label class="width55">Weekend Excluded Days</label>
	<input type="text" readonly="readonly" class="width35" id="excludedWeekendDaysResult" value="${LEAVE_DAYS.excludedWeekendDays}"/>
</div>
