<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var locationId=0;var locationName=''; 
$(function(){ 
	$('#LocationT').dataTable({ 
		"sAjaxSource": "get_job_location_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Location ID', "bVisible": false},
			{ "sTitle": 'Company'},
			{ "sTitle": 'Department'},
			{ "sTitle": 'Location'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#LocationT').dataTable();
	var commonParam=null;
	/* Click event handler */
	$('#LocationT tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1]+" >> "+aData[2]+" >> "+aData[3];
	        commonParam=aData[1]+"@&"+aData[2]+"@&"+aData[3];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1]+" >> "+aData[2]+" >> "+aData[3];
	        commonParam=aData[1]+"@&"+aData[2]+"@&"+aData[3];
	    }
	});
	$('#LocationT tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1]+" >> "+aData[2]+" >> "+aData[3];
	        commonParam=aData[1]+"@&"+aData[2]+"@&"+aData[3];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        locationId=aData[0];
	        locationName=aData[1]+" >> "+aData[2]+" >> "+aData[3];
	        commonParam=aData[1]+"@&"+aData[2]+"@&"+aData[3];
	    }
		 
		departmentBranchPopupResult(locationId,locationName,commonParam);
		$('#common-popup').dialog("close");

	});
	$('#department-list-close').live('click',function () { 
		$('#common-popup').dialog("close");
		return false;
	});
});
</script>
<input type="hidden" id="rowId" value="${rowId}"/>
<div id="main-content"> 
	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="hr.department.location"/></div>
 	<div id="trans_combination_accounts">
		<table class="display" id="LocationT"></table>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="department-list-close"><fmt:message key="common.button.close"/></div>
		</div>
	</div> 
</div>