<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">

var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var personId=0;var personName=''; 
$(function(){ 
	
	$('#Person').dataTable({ 
		"sAjaxSource": "job_employee_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.recordid"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.firstname"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.lastname"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.persontype"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.country"/>'},
			{ "sTitle": '<fmt:message key="hr.personaldetails.label.personid"/>'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Person').dataTable();

	/* Click event handler */
	$('#Person tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2];
	    }
	});
	$('#Person tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        personId=aData[0];
	        personName=aData[1]+" "+aData[2];
	    }
		$("#employeeName_"+$("#rowId").val()).html(personName);
		$("#personId_"+$("#rowId").val()).val(personId);
		$('#job-common-popup').dialog("close");
	});
});
</script>
<div id="main-content"> 
	<input type="hidden" id="rowId" value="${rowId}"/>
 	<div id="trans_combination_accounts">
		<table class="display" id="Person"></table>
	</div> 
</div>