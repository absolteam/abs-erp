<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">
#ui-datepicker-div {
	background-color: #cfcfcf !important;
	/* left: 731.5px !important; */
	/* top: 124.967px !important; */
	width: 231px !important;
	z-index: 751!important;
	
}
.ui-widget-content {
    background: url("images/ui-bg_flat_75_ffffff_20x100.png") repeat-x scroll 50% 50% #FFFFFF;
    color: #444444;
}

.ui-slider-horizontal {
    border: 1px solid #CCCCCC;
    margin: 3px 0 4px 5px;
   
}
</style> 
<script type="text/javascript">
var currentObjectIdentiy="";
var oldEnteredDays="";
var oldEnteredHours="";

$(function (){  
	$('.timePick').timepicker({});
	var attendancePolicyId=Number($('#attendancePolicyId').val());
	if(attendancePolicyId>0){
		//Get already configured days against the record
		oldEnteredDays=$('#attendanceDays').val();
		oldEnteredHours=$('#attendanceHours').val();
		
		$('#processType').val($('#processTypeTemp').val());
		$('#attendanceType').val($('#attendanceTypeTemp').val());
		if($('#isDefalutTemp').val().trim()=='true')
			$('#isDefalut').attr("checked","checked");
		
		if($('#hourlyProcessTemp').val().trim()=='true')
			$('#hourlyProcess').attr("checked","checked");
		
		if($('#allowLopTemp').val().trim()=='true'){
			$('#LOP-DIV').show();
			$('#allowLop').attr("checked","checked");
		}
		
		if($('#allowOtTemp').val().trim()=='true'){
			$('#OT-DIV').show();
			$('#allowOt').attr("checked","checked");
		}
		
		if($('#allowCompoffTemp').val().trim()=='true'){
			$('#COMPOFF-DIV').show();
			$('#allowCompoff').attr("checked","checked");
		}
		
		if($('#attendanceType').val()==1){
			$('#attendanceDays').attr("disabled",true);
			$('#attendanceHours').attr("disabled",true);
		}else{
			$('#attendanceDays').attr("disabled",false);
			$('#attendanceHours').attr("disabled",false);
		}
	}else{
		$('#attendanceDays').val("1");
		if($('#attendanceType').val()==1){
			$('#attendanceHours').val("");
			$('#attendanceDays').attr("disabled",true);
			$('#attendanceHours').attr("disabled",true);
		}else{
			$('#attendanceDays').attr("disabled",false);
			$('#attendanceHours').attr("disabled",false);
		}
	}
	
	
	$('#hourlyProcess').change(function() { 
		$('#lopHoursForHalfday').val("");
		$('#lopHoursForFullday').val("");
		
		$('#otHoursForHalfday').val("");
		$('#otHoursForFullday').val("");
		
		$('#compoffHoursForHalfday').val("");
		$('#compoffHoursForFullday').val("");
		if($('#hourlyProcess').attr("checked")){
		
			$('#lopHoursForHalfday').attr("disabled",true);
			$('#lopHoursForFullday').attr("disabled",true);
			
			$('#otHoursForHalfday').attr("disabled",true);
			$('#otHoursForFullday').attr("disabled",true);
			
			$('#compoffHoursForHalfday').attr("disabled",true);
			$('#compoffHoursForFullday').attr("disabled",true);
			$('#OT-DIV').hide();
			$('#LOP-DIV').hide();
			$('#COMPOFF-DIV').hide();
		}else{
			$('#lopHoursForHalfday').attr("disabled",false);
			$('#lopHoursForFullday').attr("disabled",false);
			
			$('#otHoursForHalfday').attr("disabled",false);
			$('#otHoursForFullday').attr("disabled",false);
			
			$('#compoffHoursForHalfday').attr("disabled",false);
			$('#compoffHoursForFullday').attr("disabled",false);
			$('#OT-DIV').show();
			$('#LOP-DIV').show();
			$('#COMPOFF-DIV').show();
		}
	});
	$('#allowLop').change(function() { 
		if($('#allowLop').attr("checked")){
			$('#LOP-DIV').show();
		}else{
			$('#LOP-DIV').hide();
			$('#lopHoursForHalfday').val("");
			$('#lopHoursForFullday').val("");
		}
		$('#hourlyProcess').trigger("change");
	});
	
	$('#allowOt').change(function() { 
		if($('#allowOt').attr("checked")){
			$('#OT-DIV').show();
			$('#allowCompoff').attr("checked",false);
		}else{
			$('#OT-DIV').hide();
			$('#otHoursForHalfday').val("");
			$('#otHoursForFullday').val("");
		}
		$('#hourlyProcess').trigger("change");
	});
	
	$('#allowCompoff').change(function() { 
		if($('#allowCompoff').attr("checked")){
			$('#COMPOFF-DIV').show();
			$('#allowOt').attr("checked",false);
		}else{
			$('#COMPOFF-DIV').hide();
			$('#compoffHoursForHalfday').val("");
			$('#compoffHoursForFullday').val("");
		}
		$('#hourlyProcess').trigger("change");
	});
	
	$('#attendanceType').change(function() {
		if($('#attendanceType').val()==1){
			$('#attendanceDays').val("1");
			$('#attendanceHours').val("");
			$('#attendanceDays').attr("disabled",true);
			$('#attendanceHours').attr("disabled",true);
			$('#attendanceDays').removeClass("validate[groupRequired[temp],custom[number],min[1],max[7]]");
			$('#attendanceDays').removeClass("validate[groupRequired[temp],custom[number],min[1],max[30]]");
		}else if($('#attendanceType').val()==2){
			$('#attendanceDays').addClass("validate[groupRequired[temp],custom[number],min[1],max[7]]");
			$('#attendanceDays').removeClass("validate[groupRequired[temp],custom[number],min[1],max[30]]");
			$('#attendanceDays').attr("disabled",false);
			$('#attendanceHours').attr("disabled",false);
			$('#attendanceDays').val(oldEnteredDays);
			$('#attendanceHours').val(oldEnteredHours);
		}else if($('#attendanceType').val()==3){
			$('#attendanceDays').removeClass("validate[groupRequired[temp],custom[number],min[1],max[7]]");
			$('#attendanceDays').addClass("validate[groupRequired[temp],custom[number],min[1],max[30]]");
			$('#attendanceDays').attr("disabled",false);
			$('#attendanceHours').attr("disabled",false);
			$('#attendanceDays').val(oldEnteredDays);
			$('#attendanceHours').val(oldEnteredHours);
		}else{
			$('#attendanceDays').removeClass("validate[groupRequired[temp],custom[number],min[1],max[7]]");
			$('#attendanceDays').removeClass("validate[groupRequired[temp],custom[number],min[1],max[30]]");
			$('#attendanceDays').attr("disabled",false);
			$('#attendanceHours').attr("disabled",false);
			$('#attendanceDays').val(oldEnteredDays);
			$('#attendanceHours').val(oldEnteredHours);
		}
	});
	
	$jquery("#ASSET_USAGE").validationEngine('attach');
	
   $('#save').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		
		if($jquery("#ASSET_USAGE").validationEngine('validate')){ 
			var attendancePolicyId=Number($('#attendancePolicyId').val());
			var policyName=$('#policyName').val();
			var processType=$('#processType').val();
			var allowOt=$('#allowOt').attr("checked");
			var allowCompoff=$('#allowCompoff').attr("checked");
			var otHoursForHalfday=$('#otHoursForHalfday').val();
			var otHoursForFullday=$('#otHoursForFullday').val();
			
			var compoffHoursForHalfday=$('#compoffHoursForHalfday').val();
			var compoffHoursForFullday=$('#compoffHoursForFullday').val();
			
			var lopHoursForHalfday=$('#lopHoursForHalfday').val();
			var lopHoursForFullday=$('#lopHoursForFullday').val();
			var isDefalut=$('#isDefalut').attr("checked");
			var allowLop=$('#allowLop').attr("checked");
			var hourlyProcess=$('#hourlyProcess').attr("checked");
			
			var attendanceType=$('#attendanceType').val();
			var attendanceDays=$('#attendanceDays').val();
			var attendanceHours=$('#attendanceHours').val();
			if(attendanceHours!=null && attendanceHours!='' && Number(attendanceHours)>24){
				$('.commonErr').hide().html("Hours must with in range of 24hrs").slideDown();
				return false;
			}
			var errorMessage=null;
			var successMessage="Successfully Created";
			if(attendancePolicyId>0)
				successMessage="Successfully Modified";

			$.ajax({
				type: "POST", 
				url:"<%=request.getContextPath()%>/attendance_policy_save.action",
				data: {
					attendancePolicyId:attendancePolicyId,
					policyName:policyName,
					processType:processType,
					allowOt:allowOt,
					allowCompoff:allowCompoff,
					isDefalut:isDefalut,
					otHoursForHalfday:otHoursForHalfday,
					otHoursForFullday:otHoursForFullday,
					compoffHoursForHalfday:compoffHoursForHalfday,
					compoffHoursForFullday:compoffHoursForFullday,
					lopHoursForHalfday:lopHoursForHalfday,
					lopHoursForFullday:lopHoursForFullday,
					attendanceType:attendanceType,
					attendanceDays:attendanceDays,
					attendanceHours:attendanceHours,
					allowLop:allowLop,
					hourlyProcess:hourlyProcess
				},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 returnCallTOList();
					 }else{
						 errorMessage=message;
					 }
					 
					 if(errorMessage==null)
							$('.success').hide().html(successMessage).slideDown();
						else
							$('.commonErr').hide().html(errorMessage).slideDown();
				},
				error: function(result) { 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					$('.commonErr').hide().html(message).slideDown(); 
				} 
			});
		}else{
			return false;
		}
		$('#loading').fadeOut();
	});
   
   $('#discard').click(function() { 
		$('.success,.error').hide();
		$('#loading').fadeIn();
		var cnfrm = confirm('You want to close/cancel this?');
		if(!cnfrm)
			return false;
		returnCallTOList();
		$('#loading').fadeOut();
	});
});
   function returnCallTOList(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/attendance_policy_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#codecombination-popup').dialog('destroy');
				$('#codecombination-popup').remove();	
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();	
				$("#main-wrapper").html(result); 
			}
	 	});
	}



</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Attendance Policy Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
			<form id="ASSET_USAGE" name="ASSET_USAGE" style="position: relative;">
				<div id="hrm" class="float-left width50"  >
					<input type="hidden" name="attendancePolicyId" id="attendancePolicyId" value="${POLICY_INFO.attendancePolicy.attendancePolicyId}"/>
					<fieldset>
						   <div>
			                  <label class="width30">Policy Name<span style="color:red">*</span></label>
			                  <input type="text" class="width50 validate[required]" name="policyName" id="policyName" value="${POLICY_INFO.attendancePolicy.policyName}"/>
					       </div>
				      	 <div>
		                 	<label class="width30">Process Type</label>
			                 <input type="hidden" id="processTypeTemp" value="${POLICY_INFO.attendancePolicy.processType}">
			                 <select name="processType" class="width51" id="processType">
			                 	<option value="FLS">First In & Last Out Swipe Only</option>
			                 	<option value="GLS">All Swipe</option>
			                 </select>
			             </div>
			             <div>
		                 	<label class="width30">Attendance Mode</label>
			                 <input type="hidden" id="attendanceTypeTemp" value="${POLICY_INFO.attendancePolicy.attendanceType}">
			                 <select name="attendanceType" class="width51" id="attendanceType">
			                 	<option value="1">Daily</option>
			                 	<option value="2">Weekly</option>
			                 	<option value="3">Monthly</option>
			                 </select>
			             </div>
			             <div>
		                 	<label class="width30">Attendance Days</label>
			                 <input type="text" id="attendanceDays" class="width50  " value="${POLICY_INFO.attendancePolicy.attendanceDays}">
			             </div>
			              <div>
		                 	<label class="width30">Attendance Hours</label>
			                 <input type="text" id="attendanceHours" class="width50 validate[groupRequired[temp],custom[number],min[1],max[300]]" value="${POLICY_INFO.attendancePolicy.attendanceHours}">
			             </div>
				      	  
					   
				        <div>
		                 	<label class="width30">Default</label>
		                 	<input type="checkbox" id="isDefalut" class="width8">
		                	<input type="hidden" id="isDefalutTemp" class="width8" value="${POLICY_INFO.attendancePolicy.isDefalut}">
		            	</div>
			       	 
				 </fieldset>
			</div>
			<div id="hrm" class="float-left width50">
				<fieldset>
						 <div>
		                 	<label class="width30">Hourly Process</label>
		                 	<input type="checkbox" id="hourlyProcess" class="width8">
		                	<input type="hidden" id="hourlyProcessTemp" class="width8" value="${POLICY_INFO.attendancePolicy.hourlyProcess}">
		            	</div>
		            <div>
		                 <label class="width30">LOP(Loss Of Pay)</label>
		                 	<input type="checkbox" id="allowLop" class="width8">
		               	 	 <input type="hidden" id="allowLopTemp" class="width8" value="${POLICY_INFO.attendancePolicy.allowLop}">
		           	 </div>
		           	<div id="LOP-DIV" style="display: none;">
				  		<div>
			              <label class="width30">LOP for Half a day</label>
			              <input type="text" class="width50 timePick " name="lopHoursForHalfday" id="lopHoursForHalfday" value="${POLICY_INFO.lopHoursForHalfdayStr}"/>
					  </div>
		              <div>
			              <label class="width30">LOP for full day</label>
			              <input type="text" class="width50  timePick " name="lopHoursForFullday" id="lopHoursForFullday" value="${POLICY_INFO.lopHoursForFulldayStr}"/>
					  </div>
					</div>
					<div>
		                 <label class="width30">Over Time</label>
		                 	<input type="checkbox" id="allowOt" class="width8">
		               	 	 <input type="hidden" id="allowOtTemp" class="width8" value="${POLICY_INFO.attendancePolicy.allowOt}">
		           		 </div>
					<div id="OT-DIV" style="display: none;">
		             <div>
			              <label class="width30">OT for Half a day</label>
			              <input type="text" class="width50 timePick" name="otHoursForHalfday" id="otHoursForHalfday" value="${POLICY_INFO.otHoursForHalfdayStr}"/>
					  </div>
		              <div>
			              <label class="width30">OT for full day</label>
			              <input type="text" class="width50  timePick " name="otHoursForFullday" id="otHoursForFullday" value="${POLICY_INFO.otHoursForFulldayStr}"/>
					  </div>
					 </div>
					   <div>
		                 	<label class="width30">CompOff</label>
		                 	<input type="checkbox" id="allowCompoff" class="width8">
		                	 <input type="hidden" id="allowCompoffTemp" class="width8" value="${POLICY_INFO.attendancePolicy.allowCompoff}">
		            	</div>
					<div id="COMPOFF-DIV" style="display: none;">
						 <div>
				              <label class="width30">CompOff for Half a day</label>
				              <input type="text" class="width50 timePick " name="compoffHoursForHalfday" id="compoffHoursForHalfday" value="${POLICY_INFO.compoffHoursForHalfdayStr}"/>
						  </div>
			              <div>
				              <label class="width30">CompOff for full day</label>
				              <input type="text" class="width50  timePick " name="compoffHoursForFullday" id="compoffHoursForFullday" value="${POLICY_INFO.compoffHoursForFulldayStr}"/>
						  </div>
					</div>
					
		           
				</fieldset>
			</div>
		</form>
		<div class="clearfix"></div>  
		<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div> 
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>