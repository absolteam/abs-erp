<tr id="qualificationfieldrow_${requestScope.id}" class="qualificationrowid">
	<td class="width10" id="degree_${requestScope.id}"></td>
	<td class="width10" id="majorSubject_${requestScope.id}"></td>
	<td class="width10" id="instution_${requestScope.id}"></td>
	<td class="width10" id="completionYear_${requestScope.id}"></td>
	<td class="width10" id="result_${requestScope.id}"></td>
	<td class="width10" id="resultType_${requestScope.id}"></td>
	<td style="width: 5%;" id="option_${requestScope.id}"><input type="hidden"
		name="degreeId_${requestScope.id}" id="degreeId_${requestScope.id}" value="" /> <input
		type="hidden" name="academicQualificationsId_${requestScope.id}"
		id="academicQualificationsId_${requestScope.id}" value="" /> <input type="hidden"
		name="qualificationlineId_${requestScope.id}" id="qualificationlineId_${requestScope.id}" value="${requestScope.id}" /> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip qualificationaddDatanew"
		id="qualificationAddImage_${requestScope.id}"
		style=" cursor: pointer;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip qualificationeditDatanew"
		id="qualificationEditImage_${requestScope.id}" style="display: none;cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip qualificationdelrownew"
		id="qualificationDeleteImage_${requestScope.id}" style="cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="qualificationWorkingImage_${requestScope.id}"
		style="display: none; cursor: pointer;" title="Working"> <span
			class="processing"></span> </a></td>
</tr>
<script type="text/javascript">
$(function() {
	//*********************************************** Qualification Add/Edit Script content ****************************************
	$(".qualificationaddDatanew").click(function(){ 
		slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		if($("#editCalendarVal").validationEngine({returnIsValid:true})){
				$("#qualificationAddImage_"+rowid).hide();
				$("#qualificationEditImage_"+rowid).hide();
				$("#qualificationDeleteImage_"+rowid).hide();
				$("#qualificationWorkingImage_"+rowid).show(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/person_qualification_add.action",
					data:{id:rowid,addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $('#openFlag').val(1);
					}
			});
			
		}
		else{
			return false;
		}	
		 
	});

	$(".qualificationeditDatanew").click(function(){
		 
		 slidetab=$(this).parent().parent().get(0);  
		 
		 if($("#editCalendarVal").validationEngine({returnIsValid:true})) {
			 
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#qualificationAddImage_"+rowid).hide();
					$("#qualificationEditImage_"+rowid).hide();
					$("#qualificationDeleteImage_"+rowid).hide();
					$("#qualificationWorkingImage_"+rowid).show(); 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/person_qualification_add.action",
				 data:{id:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
  				$('#academicQualificationsId').val($('#academicQualificationsId_'+rowid).val()); 
    			$('#degree').val($('#degreeId_'+rowid).val()); 
    			$('#majorSubject').val($('#majorSubject_'+rowid).text()); 
    			$('#instution').val($('#instution_'+rowid).text()); 
    			$('#completionYear').val($('#completionYear_'+rowid).text());
    			$('#result').val($('#result_'+rowid).text());
    			$('#resultType').val($('#resultType_'+rowid).text());
			    $('#openFlag').val(1);
			  
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			 			
		 }
		 else{
			 return false;
		 }	
		 
	});
	 
	 $(".qualificationdelrownew").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		 var lineId=$('#qualificationlineId_'+rowid).val();
		
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/person_qualification_save.action", 
				 data:{id:lineId,addEditFlag:"D"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
					 $('.tempresult').html(result);   
					 if(result!=null){
						 $("#qualificationfieldrow_"+rowid).remove(); 
						  var qualificationcount=Number($('#qualificationcount').val()); 
         				  qualificationcount-=1;
         				  $('#qualificationcount').val(qualificationcount);
			        		//To reset sequence 
			        		var i=0;
			     			$('.qualificationrowid').each(function(){  
								i=i+1;
								$($($(this).children().get(4)).children().get(1)).val(i); 
		   					 }); 
					     }  
				},
				error:function(result){
					 $('.tempresult').html(result);   
					 return false;
				}
					
			 });
			 
		 
	 });


});	
</script>	