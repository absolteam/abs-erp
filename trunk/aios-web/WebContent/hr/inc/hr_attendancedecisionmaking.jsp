<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<html>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>

<script type="text/javascript"> 
var payLineDetail="";

$(document).ready(function(){ 

		
		editDataSelectCall();

		
			$('#shift-discard').click(function(){ 
				$('#job-common-popup').dialog('close');
				return false;
			 });
		
		$('#deductionType_Master').change(function(){ 
			var cnfrm = confirm('Do you want to update in all the deduction type?');
			if(!cnfrm)
				return false;
			
			var masterVal=$(this).val();
			$('.deductionType').each(function(){
				$(this).val(masterVal);
			});
			return false;
		 });
		
		$('#calcultionMethod_Master').change(function(){ 
			var cnfrm = confirm('Do you want to update in all the calculation method?');
			if(!cnfrm)
				return false;
			
			var masterVal=$(this).val();
			$('.calcultionMethod').each(function(){
				$(this).val(masterVal);
			});
			return false;
		 });
			
			$('#globalCheckBox').change(function(){ 
				if($('#globalCheckBox').attr("checked"))
					$('.checkedPay').attr("checked",true);
				else
					$('.checkedPay').attr("checked",false);
					
			 });
			
			$jquery("#shift-details").validationEngine('attach');

			 $('#shift-save').click(function(){

					 payLineDetail=getPayLineDetails();
		 				 
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/update_attendance_decision.action", 
							data:{
								
								swipeLineDetail:payLineDetail
								},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){ 
								$(".tempresult").html(result);
								 var message=$('.tempresult').html(); 
								 if(message.trim()=="SUCCESS"){
										$('.success').hide().html("Successfully Updated").slideDown(1000);
										homeredirect();
								 }
								 else{
									 $('#page-error').hide().html("Update Failure! Please check the employee & attendnace information").slideDown(1000);
									 $('#job-common-popup').dialog('close');
									 return false;
								 }
							}
					 });
			
				 return false;
			 });
			 
			 

			 $('#discard').click(function(){
			 	homeredirect();
			 	return false;
			 });
			
});
	
	function editDataSelectCall(){
		
		$('.rowidS').each(function(){
			var rowId=getRowId($(this).attr('id'));
			$('#deductionType_'+rowId).val(Number($('#deductionTypeTemp_'+rowId).val()));	
			$('#calcultionMethod_'+rowId).val(Number($('#calcultionMethodTemp_'+rowId).val()));	
			
		});
		
	}
	function manupulateShiftLastRow(){
		var hiddenSize=0;
		$($(".tabS>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		var tdSize=$($(".tabS>tr:first").children()).size();
		var actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabS>tr:last').removeAttr('id');  
		$('.tabS>tr:last').removeClass('rowidS').addClass('lastrow');  
		$($('.tabS>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabS>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function getRowId(id){
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	
	
	function getPayLineDetails(){
		payLineDetail="";
		var payrollIds=new Array();
		var payStatuses=new Array();
		var calcultionMethods=new Array();
		var comments=new Array();

		$('.rowidS').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var attendanceId=$('#attendanceId_'+rowId).val();
			if(attendanceId==null || attendanceId=='' || attendanceId==0 || attendanceId=='undefined')
				attendanceId=-1;
			
			var payStatus=$('#salaryStatus_'+rowId).val();
			if(payStatus==null || payStatus=='' || payStatus=='undefined')
				payStatus=-1;
			
			var deductionType=$('#deductionType_'+rowId).val();
			if(deductionType==null || deductionType=='' || deductionType=='undefined')
				deductionType=-1;
			
			var calcultionMethod=$('#calcultionMethod_'+rowId).val();
			if(calcultionMethod==null || calcultionMethod=='' || calcultionMethod=='undefined')
				calcultionMethod=-1;
			
			var comment=$('#comment_'+rowId).val();
			if(comment==null || comment=='' || comment=='undefined')
				comment=-1;
			//var statusShift=$('#statusShift_'+rowId).val();
			if(typeof attendanceId != 'undefined' && attendanceId!=null && attendanceId!=0){
				payrollIds.push(attendanceId);
				payStatuses.push(deductionType);
				calcultionMethods.push(calcultionMethod);
				comments.push(comment);
			}
		});
		for(var j=0;j<payrollIds.length;j++){ 
			payLineDetail+=payrollIds[j]+"@"+payStatuses[j]+"@"+calcultionMethods[j]+"@"+comments[j];
			if(j==payrollIds.length-1){   
			} 
			else{
				payLineDetail+="##";
			}
		} 
		return payLineDetail;
	}
	function homeredirect(){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/attendance_excution_listing.action",  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	}
		}); 
		return false;
	}
</script>

<body>
	<div id="main-content">
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
			style="min-height: 99%;">
			<div id="page-error" class="response-msg error ui-corner-all width90"
				style="display: none;">
				<span>Process Failure!</span>
			</div>
			<div class="success response-msg ui-corner-all"
				style="display: none;"></div>
			<div class="tempresult" style="display: none;"></div>

			<div class="width98 float-left" id="hrm">
				<form id="shift-details" name="shift-details">
					<div class="width100 float-left">
						<fieldset>
							<legend>Global Decision</legend>
							<div class="width40 float-left">
								<label class="width30">Deduction Type</label>
								<select
									id="deductionType_Master" 
									name="deductionType_Master" style="width: 65%;">
										<option value="">--Select--</option>
										<option value="1">NoAction</option>
										<option value="2">LOP</option>
										<option value="3">OverTime</option>
										<option value="4">CompOff</option>
										<option value="5">COD-CompOff Deduction</option>
								</select> 
							</div>
							<div class="width40 float-left">
								<label class="width30">Calculation Method</label>
								 <select id="calcultionMethod_Master" 
									name="calcultionMethod_Master" style="width: 65%;">
										<option value="">--Select--</option>
										<c:forEach items="${CALCULATION_METHOD}" var="gen">
											<option value="${gen.key}">${gen.value}</option>
										</c:forEach>
								</select> 
							</div>
						</fieldset>
					</div>
					<div class="width100 float-left">
						<fieldset>
							<legend>Attendance Decision Update</legend>
							<div id="hrm" class="hastable width100">
								<table id="hastab2" class="width100">
									<thead>
										<tr>
											
											<th style="width: 15%">Employee</th>
											<th style="width: 10%">Designation</th>
											<th style="width: 10%">Date</th>
											<th style="width: 3%">In</th>
											<th style="width: 3%">Out</th>
											<th style="width: 5%">Time Deviation</th>
											<th style="width: 5%">Total Hours</th>
											<th style="width: 10%">Scedule</th>
											<th style="width: 10%">System Suggestion</th>
											<th style="width: 10%">Action</th>
											<th style="width: 15%">Comments</th>
										</tr>
									</thead>
									<tbody class="tabS">
										<c:choose>
											<c:when
												test="${ATTENDNACE_LIST ne null && ATTENDNACE_LIST ne '' && fn:length(ATTENDNACE_LIST)>0}">
												<c:forEach var="att" items="${ATTENDNACE_LIST}"
													varStatus="status1">
													<tr class="rowidS" id="fieldrowS_${status1.index+1}">
														<td id="lineIdS_${status1.index+1}" style="display: none;">${status1.index+1}</td>
														<td>${att.personName}</td>
														<td>${att.designationName}</td>
														<td>${att.attedanceDate}</td>
														<td>${att.inTime}</td>
														<td>${att.outTime}</td>
														<td>${att.timeLossOrExcess}</td>
														<td>${att.totalHours}</td>
														<td>Start : ${att.jobShift.workingShift.startTime} &
															End : ${att.jobShift.workingShift.endTime}</td>
														<td>${att.systemSuggesion}</td>

														<td style="width: 25%"><input type="hidden"
															name="deductionTypeTemp_${status1.index+1}"
															id="deductionTypeTemp_${status1.index+1}"
															value="${att.deductionType}"> <select
															id="deductionType_${status1.index+1}" class="deductionType"
															name="deductionType_${status1.index+1}" style="width: 45%;">
																<option value="">--Select--</option>
																<option value="1">NoAction</option>
																<option value="2">LOP</option>
																<option value="3">OverTime</option>
																<option value="4">CompOff</option>
																<option value="5">COD-CompOff Deduction</option>
														</select> <select id="calcultionMethod_${status1.index+1}" class="calcultionMethod"
															name="calcultionMethod_${status1.index+1}" style="width: 45%;">
																<option value="">--Select--</option>
																<c:forEach items="${CALCULATION_METHOD}" var="gen">
																	<option value="${gen.key}">${gen.value}</option>
																</c:forEach>
														</select> <input type="hidden"
															name="calcultionMethodTemp_${status1.index+1}"
															id="calcultionMethodTemp_${status1.index+1}"
															value="${att.calcultionMethod}" />
														</td>
														<td><input type="text" id="comment_${status1.index+1}" value="${att.comment}"/></td>
														<td style="display: none;"><input type="hidden"
															id="attendanceId_${status1.index+1}"
															value="${att.attendanceId}" /></td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>

									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
				</form>
				<div class="clearfix"></div>
				<div
					class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div
						class="portlet-header ui-widget-header float-right discard"
						id="discard">cancel</div>
					<div class="portlet-header ui-widget-header float-right shift-save"
						id="shift-save">save</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div id="sub-content" class="width100 float-left"></div>

			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span>
					</a>
				</div>
				<div id="job-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="job-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>

			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrowsS"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
		</div>
	</div>
</body>
</html>