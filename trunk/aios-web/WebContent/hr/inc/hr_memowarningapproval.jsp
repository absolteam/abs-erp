<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.cleditor.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/css/jquery.cleditor.css" />
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style type="text/css">
.subject-heading{
	color:#green;font-weight: bold;font-size: 14px;
}
</style> 
<script type="text/javascript">
var memoWarningId="";
$(function (){  

/* 	$('.discard').live('click',function(){
		window.location.reload();
	});
	
	$('#disapprove').click(function(){
		memoWarningId=Number($('#memoWarningId').val());	
		if(memoWarningId>0)
			workflowRejectCall(memoWarningId);
		else
			$('.error').html("Disapproval Failured").show();
		
		return false;
	});
	
	$('#approve').click(function(){ 
		$('#page-error').hide();
		memoWarningId=Number($('#memoWarningId').val());	
		if(memoWarningId>0)
			workflowApproveCall(memoWarningId);
		else
			$('.error').html("Approval Failured").show();
		
		return false;
	}); */
	
});

function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	var memoWarningId=Number($('#memoWarningId').val());
	if(returnVO.returnStatusName=='DeleteApproved')
		deleteApproval(memoWarningId,messageId);
}
function deleteApproval(memoWarningId,messageId){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/memo_warning_delete.action",  
     	data: {memoWarningId:memoWarningId,messageId:messageId},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result)
     	{  
			return false;
     	},
     	error:function(result)
        {
     		return false;
        } 
	}); 
	return false;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Memo / Warning Information</div>
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
			<form id="MEMO_WARNING" name="MEMO_WARNING">
				<div id="hrm" class="width100"  >
					<input type="hidden" name="memoWarningId" id="memoWarningId" value="${MEMO_WARNING_INFO.memoWarning.memoWarningId}"/>
				<div class="clearfix"></div>  	
				<div>${MEMO_WARNING_INFO.messageHtml}</div> 
			</div>
			<div id="hrm" class="float-left width50" >
				<div>
					<fieldset>
						<legend>TO</legend>
						<ol>
							<c:forEach items="${MEMO_WARNING_INFO.memoWarningPersonVOs}" var="empList">
								<li>
								${empList.employeeName}
								[${empList.employeeDesignation}-${empList.employeeDepartment}]
								</li>
							</c:forEach>
						</ol>
					</fieldset>
				</div>
			</div>	
			<div id="hrm" class="float-left width50" >
				<div>
					<fieldset>
						<legend>CC List</legend>
						<ol>
							<c:forEach items="${MEMO_WARNING_INFO.memoWarningPersonCCVOs}" var="perList">
								<li>
								${perList.employeeName}
								[${perList.employeeDesignation}-${perList.employeeDepartment}]
								</li>
							</c:forEach>
						</ol>
					</fieldset>
				</div>
			</div>	
			<div id="hrm" class="float-left width100" >
				<div>
					<fieldset>
							<legend>Notes From Requester</legend>
							<span>${MEMO_WARNING_INFO.memoWarning.notes}</span>
					</fieldset>
				</div>
			</div>
		 </form>
		<div class="clearfix"></div>  
		<%-- <div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel" /></div>
			<div class="portlet-header ui-widget-header float-right disapprove" id="disapprove" >DisApprove</div> 
			<div class="portlet-header ui-widget-header float-right approve" id="approve" >Approve</div> 
		</div> --%>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</div>
