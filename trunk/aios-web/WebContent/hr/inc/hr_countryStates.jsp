<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<option value="">Select</option>
<c:choose>
	<c:when test="${stateList != null }">
		<c:forEach items="${stateList}" var="stateBean" varStatus="status" >
		<option id="${stateBean.stateId}" value="${stateBean.stateId}">${stateBean.stateCode} - ${stateBean.stateName}</option>
	</c:forEach> 
	</c:when>
</c:choose>
<script type="text/javascript">
var stateId="";
//Get City List On Change State List
$(document).ready(function(){ 
	$('#state').combobox({ 
			 selected: function(event, ui){ 
				 stateSelectedCall();
				 
	}

});
	
});
function stateSelectedCall(){
	stateId=Number($('#state').val());
	$.ajax({
		type: "GET", 
		url: "${pageContext.request.contextPath}/get_state_cities.action", 
     	async: false,
     	data:{stateId: stateId},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#city").html(result); 
			setComboBoxValue("#city",$('#hiddenCity').val());

		}, 
		error: function() {
		}
	}); 
}
</script>