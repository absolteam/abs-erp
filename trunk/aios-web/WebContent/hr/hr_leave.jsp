<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var leaveId=null;var leaveLineDetail="";

$(document).ready(function (){ 

	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	manupulateLeaveLastRow();
	leaveSelectedEntry();
	/* Click event handler */
	$('#Leave tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	    }
	});
	$('#Leave tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/leave_add.action",
		data: {leaveId : 0,recordId:0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
	 $('.response-msg').hide(); 
		if(leaveId == null || leaveId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			leaveId = Number(leaveId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/leave_add.action",  
	     	data: {leaveId:leaveId,recordId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('.response-msg').hide(); 
	 $('#page-error').hide();
		if(leaveId == null || leaveId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			leaveId = Number(leaveId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/leave_delete.action",  
	     	data: {leaveId:leaveId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
					 leaveId=null;
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

$('.addrowsL').click(function(){ 
	  var i=Number(1); 
	  var id=Number(getRowId($(".tabL>.rowidL:last").attr('id'))); 
	  id=id+1;   
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/leave_policy_addrow.action", 
		 	async: false,
		 	data:{id: id},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('.tabL tr:last').before(result);
				 if($(".tabL").height()>255)
					 $(".tabL").css({"overflow-x":"hidden","overflow-y":"auto"}); 
				 $('.rowidL').each(function(){   
		    		 var rowId=getRowId($(this).attr('id')); 
		    		 $('#lineIdL_'+rowId).html(i);
					 i=i+1; 
		 		 }); 
				 return false;
			}
		});
	  return false;
}); 
$('.delrowL').click(function(){ 
	 slidetab=$(this).parent().parent().get(0);  
	 $(slidetab).remove();  
	 var i=1;
$('.rowidL').each(function(){   
	 var rowId=getRowId($(this).attr('id')); 
	 $('#lineIdL_'+rowId).html(i);
		 i=i+1; 
	 });   
});

$('.leavePolicyName').live('change',function(){
	if($(this).val()!=""){
		var rowId=getRowId($(this).attr('id')); 
		triggerLeaveAddRow(rowId);
		return false;
	} 
	else{
		return false;
	}
	return false;
});	

$('#save').click(function() { 
	$('.success,.error').hide();
	$('#loading').fadeIn();
	
	if($jquery("#ASSET_USAGE").validationEngine('validate')){ 
		
		leaveLineDetail=getLeaveLineDetails();
		
		var errorMessage=null;
		var successMessage="Successfully Saved";
		

		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/leave_policy_save.action",
			data: {
				leaveLineDetail:leaveLineDetail
			},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 
				 }else{
					 errorMessage=message;
				 }
				 
				 if(errorMessage==null)
						$('.success').hide().html(successMessage).slideDown();
					else
						$('.error').hide().html(errorMessage).slideDown();
				 $("#main-content").scrollTo(0,300);
			},
			error: function(result) { 
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				$('.error').hide().html(message).slideDown(); 
				$("#main-content").scrollTo(0,300);
			} 
		});
	}else{
		return false;
	}
	$('#loading').fadeOut();
});

$('#leave-cancel').click(function() { 
	$.ajax({
		type: "POST", 
		url:"<%=request.getContextPath()%>/leave_list.action", 
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result) { 
			$("#main-wrapper").html(result);
		} 
	});
});

});

function alreadyListCall(){
	oTable = $('#Leave').dataTable();
	oTable.fnDestroy();
	$('#Leave').remove();
	$('#gridDiv').html("<table class='display' id='Leave'></table>");
	listCall();
}
function listCall(){
	$('#Leave').dataTable({ 
		"sAjaxSource": "leave_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Leave Id', "bVisible": false},
			{ "sTitle": 'Leave Type'},
			{ "sTitle": 'Leave Code', "bVisible": false},
			{ "sTitle": 'Display Name'},
			{ "sTitle": 'Days'},
			{ "sTitle": 'Paid'},
			{ "sTitle": 'Full Paid Days'},
			{ "sTitle": 'Half Paid Days'},
			{ "sTitle": 'Yearly Process'},
			{ "sTitle": 'Carry Forward Count', "bVisible": false},
			{ "sTitle": 'No.Of Time in Service', "bVisible": false}

		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Leave').dataTable();
}
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function leaveSelectedEntry(){
	
	$('.leavePolicyName').each(function(){
		var rowId=getRowId($(this).attr('id'));
		if($("#isWeekendExcludedTemp_"+rowId).val().trim()=="true")
			$("#isWeekendExcluded_"+rowId).attr("checked",true);
				
		if($("#isHolidayExcludedTemp_"+rowId).val().trim()=="true")
			$("#isHolidayExcluded_"+rowId).attr("checked",true);
		
		if($("#isDefalutTemp_"+rowId).val().trim()=="true")
			$("#isDefalut_"+rowId).attr("checked",true);
				
	});
    
}
function triggerLeaveAddRow(rowId){  
	var leavePolicyName=$('#leavePolicyName_'+rowId).val().trim();
	if(leavePolicyName!=null && leavePolicyName!=""
			 && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageL_'+rowId).show();
		$('.addrowsL').trigger('click');
		return false;
	}else{
		return false;
	} 
}

function manupulateLeaveLastRow(){
	var hiddenSize=0;
	$($(".tabL>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabL>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabL>tr:last').removeAttr('id');  
	$('.tabL>tr:last').removeClass('rowidL').addClass('lastrow');  
	$($('.tabL>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabL>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}

function getLeaveLineDetails(){
	leaveLineDetail="";
	var leavePolicyIds=new Array();
	var leavePolicyNames=new Array();
	var holidays=new Array();
	var weekends=new Array();
	var defaults=new Array();
	$('.rowidL').each(function(){ 
		var rowId=getRowId($(this).attr('id'));  
		var leavePolicyId=$('#leavePolicyId_'+rowId).val();
		if(leavePolicyId==null || leavePolicyId=='' || leavePolicyId==0 || leavePolicyId=='undefined')
			leavePolicyId=-1;
		var leavePolicyName=$('#leavePolicyName_'+rowId).val().trim();
		var isWeekendExcluded=$('#isWeekendExcluded_'+rowId).attr("checked");
		var isHolidayExcluded=$('#isHolidayExcluded_'+rowId).attr("checked");
		var isDefalut=$('#isDefalut_'+rowId).attr("checked");
		if(typeof leavePolicyName != 'undefined' && leavePolicyName!=null && leavePolicyName!=''){
			leavePolicyIds.push(leavePolicyId);
			leavePolicyNames.push(leavePolicyName);
			weekends.push(isWeekendExcluded);
			holidays.push(isHolidayExcluded);
			defaults.push(isDefalut);
		}
	});
	for(var j=0;j<leavePolicyNames.length;j++){ 
		leaveLineDetail+=leavePolicyIds[j]+"@@"+leavePolicyNames[j]+"@@"+weekends[j]+"@@"+holidays[j]+"@@"+defaults[j];
		if(j==leavePolicyNames.length-1){   
		} 
		else{
			leaveLineDetail+="##";
		}
	} 
	return leaveLineDetail;
}


</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Policy</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Leave"></table>
			</div>	
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
	<div class="clearfix"></div>  
	<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
 	</div>
 </div>
 <div class="clearfix"></div>  
 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
	<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Policy Information</div>
		<div id="hrm" class="hastable width100"  >  
				<table id="hastab2" class="width100"> 
					<thead>
						<tr> 
						    <th style="width:5%">Policy Name</th> 
						    <th style="width:5%">Weeknd Excluded</th> 
						    <th style="width:5%">Holiday Excluded</th>
						    <th style="width:5%">Default</th>
							<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
					  </tr>
					</thead> 
							<tbody class="tabL">
								<c:choose>
									<c:when test="${Leave_Policy ne null && Leave_Policy ne '' && fn:length(Leave_Policy)>0}">
										<c:forEach var="leavePolicy" items="${Leave_Policy}" varStatus="status1">
											<tr class="rowidL" id="fieldrowL_${status1.index+1}">
												<td id="lineIdL_${status1.index+1}" style="display: none;">${status1.index+1}</td>
												 
												<td>
													 <input type="text" class="width80 leavePolicyName" id="leavePolicyName_${status1.index+1}" value="${leavePolicy.leavePolicyName}" style="border:0px;"/>
												</td> 
												<td>
													<input type="checkbox" id="isWeekendExcluded_${status1.index+1}" class="width10 isWeekendExcluded" />
													<input type="hidden" id="isWeekendExcludedTemp_${status1.index+1}" value="${leavePolicy.isWeekendExcluded}" />
													
												</td>
												<td>
													<input type="checkbox" id="isHolidayExcluded_${status1.index+1}" class="width10 isHolidayExcluded" />
													<input type="hidden" id="isHolidayExcludedTemp_${status1.index+1}" value="${leavePolicy.isHolidayExcluded}" />
													
												</td>
												<td>
													<input type="checkbox" id="isDefalut_${status1.index+1}" class="width10 isDefalut" />
													<input type="hidden" id="isDefalutTemp_${status1.index+1}" value="${leavePolicy.isDefalut}" />
													
												</td>
												<td style="display:none;">
													<input type="hidden" id="leavePolicyId_${status1.index+1}" value="${leavePolicy.leavePolicyId}" style="border:0px;"/>
												</td>
												 <td style="width:0.01%;" class="opn_td" id="optionL_${status1.index+1}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataL" id="AddImageL_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataL" id="EditImageL_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowL" id="DeleteImageL_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageL_${status1.index+1}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>
											</tr>
										</c:forEach>  
									</c:when>
								</c:choose>  
								<c:forEach var="i" begin="${fn:length(Leave_Policy)+1}" end="${fn:length(Leave_Policy)+2}" step="1" varStatus="status"> 
									<tr class="rowidL" id="fieldrowL_${i}">
										<td id="lineIdL_${i}" style="display:none;">${i}</td>
										<td>
											 <input type="text" class="width80 leavePolicyName" id="leavePolicyName_${i}" value="" style="border:0px;"/>
										</td> 
										<td>
											<input type="checkbox" id="isWeekendExcluded_${i}" class="width10 isWeekendExcluded" />
											<input type="hidden" id="isWeekendExcludedTemp_${i}" value="" />
										</td>
										<td>
											<input type="checkbox" id="isHolidayExcluded_${i}" class="width10 isHolidayExcluded" />
											<input type="hidden" id="isHolidayExcludedTemp_${i}" class="width10 isHolidayExcludedTemp" />
										</td>
										<td>
											<input type="checkbox" id="isDefalut_${i}" class="width10 isDefalut" />
											<input type="hidden" id="isDefalutTemp_${i}" class="width10 isDefalutTemp" />
										</td>
										<td style="display:none;">
											<input type="hidden" id="leavePolicyId_${i}" value="" style="border:0px;"/>
										</td> 
										 <td style="width:0.01%;" class="opn_td" id="optionL_${i}">
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataL" id="AddImageL_${i}" style="display:none;cursor:pointer;" title="Add Record">
					 								<span class="ui-icon ui-icon-plus"></span>
											  </a>	
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataL" id="EditImageL_${i}" style="display:none; cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											  </a> 
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowL" id="DeleteImageL_${i}" style="display:none;cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											  </a>
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageL_${i}" style="display:none;" title="Working">
													<span class="processing"></span>
											  </a>
										</td>    
									</tr>
							</c:forEach>
					 </tbody>
			</table>
			<div class="clearfix"></div>  
			<div class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" 
			style="margin-right: -5px;margin-top: 12px;"> 
				<div class="portlet-header ui-widget-header float-right leave-cancel" id="leave-cancel" ><fmt:message key="common.button.cancel" /></div> 
				<div class="portlet-header ui-widget-header float-right save" id="save" ><fmt:message key="common.button.save" /></div>  
			</div>
		</div> 
	
	 </div>		
	</div>
</div>

