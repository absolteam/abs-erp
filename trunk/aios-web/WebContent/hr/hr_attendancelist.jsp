<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 

$(function(){ 
	$('.formError').remove();
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
$('#Attendance').dataTable({ 
	"sAjaxSource": "attendance_listing_json.action",
    "sPaginationType": "full_numbers",
    "bJQueryUI": true, 
    "iDisplayLength": 25,
	"aoColumns": [
		{ "sTitle": 'Attendance ID', "bVisible": false},
		{ "sTitle": 'Date'},
		{ "sTitle": 'Absent'},
		{ "sTitle": 'Time In'},
		{ "sTitle": 'Time Out'},
		{ "sTitle": 'Late In'},
		{ "sTitle": 'Late Out'},
		{ "sTitle": 'Early In'},
		{ "sTitle": 'Early Out'},
		{ "sTitle": 'Time Loss/ Excess', "bVisible": false},
		{ "sTitle": 'Total Hours'},
		{ "sTitle": 'Deviated' , "bVisible": false},
		{ "sTitle": 'Process Status', "bVisible": false},
		{ "sTitle": 'Decision', "bVisible": false},
		{ "sTitle": 'Syestem Suggetion' , "bVisible": false},
		{ "sTitle": 'Half Day', "bVisible": false},

	],
	"sScrollY": $("#main-content").height() - 260,
	//"bPaginate": false,
	"aaSorting": [], 
	"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
			$(nRow).addClass('row_selected');
		}
	}
});	
//Json Grid
//init datatable
oTable = $('#Attendance').dataTable();

/* Click event handler */
$('#Attendance tbody tr').live('click', function () { 
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
});

$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.otherErr ').hide();
       }
});


$('#reason').click(function(){
	if(s == null || s==0)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/edit_attendance_by_employee.action",  
     	data: {attendanceId:recId2},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
	return false;

});
$('#view').click(function(){
	if(s == null || s==0)
	{
		alert("Please select one row to view");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/view_attendance_employee.action",  
     	data: {attendanceId:recId2},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
	return false;

});


});

</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>My Attendance</div> 
		<div class="portlet-content">
		<div  class="otherErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
		<div style="display:none;" class="tempresultMsg">
			<c:if test="${requestScope.bean != null}">
				<input type="hidden" id="sqlReturnStatus" value="${bean.sql_return_status}" />
			</c:if>    
		</div>
		
		<div>
			 <div id="page-error" class="error response-msg ui-corner-all" style="display:none;">Sorry, Process Failure!</div>
		  	 <div id="success_message" class="success response-msg ui-corner-all" style="display:none;">Successfully Processed</div> 
    	</div>
		
	
	<div id="rightclickarea">
		<div id="gridDiv">
			<table class="display" id="Attendance"></table>
		</div>
 	</div>		
	<div class="vmenu">
		<div class="first_li"><span>View</span></div>
		<div class="first_li"><span>Reason</span></div>
	</div>
	<div class="float-right buttons ui-widget-content ui-corner-all">
		<div class="portlet-header ui-widget-header float-right"  id="view">View</div> 
		<div class="portlet-header ui-widget-header float-right"  id="reason">Reason</div>
		
	</div>
	</div>
</div>
</div>
