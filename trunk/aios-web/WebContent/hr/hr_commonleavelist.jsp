<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var leaveId=null;var leaveType="";var leaveDays="";var isMonthly=""; var isYearly="";
var isService="";var isPaid="";var isCarryForward="";var extraParam="";
$(document).ready(function (){ 
	
	
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#Leave tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	        leaveType=aData[1];
	        leaveDays=aData[3];
	        isMonthly=aData[4];
	        isYearly=aData[5];
	        isService=aData[6];
	        isPaid=aData[8];
	        isCarryForward=aData[11];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	        leaveType=aData[1];
	        leaveDays=aData[3];
	        isMonthly=aData[4];
	        isYearly=aData[5];
	        isService=aData[6];
	        isPaid=aData[8];
	        isCarryForward=aData[11];
	    }
	});
	$('#Leave tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	        leaveType=aData[1];
	        leaveDays=aData[3];
	        isMonthly=aData[4];
	        isYearly=aData[5];
	        isService=aData[6];
	        isPaid=aData[8];
	        isCarryForward=aData[11];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        leaveId=aData[0];
	        leaveType=aData[1];
	        leaveDays=aData[3];
	        isMonthly=aData[4];
	        isYearly=aData[5];
	        isService=aData[6];
	        isPaid=aData[8];
	        isCarryForward=aData[11];
	    }
		  extraParam+=leaveDays+"@#"+isMonthly+"@#"+isYearly+"@#"+isService+"@#"+isPaid+"@#"+isCarryForward;
		  leavePopupResult(leaveId,leaveType,extraParam);
			$('#job-common-popup').dialog("close");
		
	});


});

function alreadyListCall(){
	oTable = $('#Leave').dataTable();
	oTable.fnDestroy();
	$('#Leave').remove();
	$('#gridDiv').html("<table class='display' id='Leave'></table>");
	listCall();
}
function listCall(){
	$('#Leave').dataTable({ 
		"sAjaxSource": "active_leave_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Leave Id', "bVisible": false},
			{ "sTitle": 'Leave Type'},
			{ "sTitle": 'Leave Code', "bVisible": false},
			{ "sTitle": 'Understanding Name', "bVisible": false},
			{ "sTitle": 'Days'},
			{ "sTitle": 'Monthly'},
			{ "sTitle": 'Yearly'},
			{ "sTitle": 'Service'},
			{ "sTitle": 'No.Of Time in Service'},
			{ "sTitle": 'Is Paid'},
			{ "sTitle": 'Full Paid Days'},
			{ "sTitle": 'Half Paid Days'},
			{ "sTitle": 'Is Carry Forward?'},
			{ "sTitle": 'Carry Forward Count'}
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Leave').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Master</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Leave"></table>
			</div>	
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="leave-common-close"><fmt:message key="common.button.close"/></div>
			</div>
		</div>		
		
	</div>
</div>
</div>
</div>
