<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var openPositionId=null;
$(document).ready(function (){ 
	
	listCall();
	
	
	/* Click event handler */
	$('#OpenPosition tbody tr').live('click', function() {
		if (!$('#OpenPosition tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					openPositionId=aData.openPositionId;
					
				}
			}
		}
	});
	
	$('#OpenPosition tbody tr').live('dblclick', function() {
		if (!$('#OpenPosition tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					openPositionId=aData.openPositionId;
					selectedOpenPositionResult(aData);
					$('#common-popup').dialog("close");
				}
			}
		}
	});
	
	$('#popup-grid-close').click(function () { 
		$('#common-popup').dialog("close");
	});

});


function listCall(){
	
	
	$('#OpenPosition').dataTable().fnDestroy();
	oTable = $('#OpenPosition')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sAjaxSource" : "<%=request.getContextPath()%>/common_open_position_list_json.action",
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				"aoColumns" : [ {
					"mDataProp" : "positionReferenceNumber"
				},{
					"mDataProp" : "designationName"
				}, {
					"mDataProp" : "gradeName"
				}, {
					"mDataProp" : "companyName"
				}, {
					"mDataProp" : "departmentName"
				}, {
					"mDataProp" : "locationName"
				} , {
					"mDataProp" : "numberOfPosition"
				} , {
					"mDataProp" : "statusDisplay"
				} ]

	}); 
	
	/*  oTable=$('#EmployeeLoan').dataTable({ 
		"sAjaxSource": "employee_loan_list.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'EmployeeLoan Id', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Amount',"sWidth": "200px"},
			{ "sTitle": 'Date'},
			{ "sTitle": 'No. Of Due'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	  */
		//Json Grid
		//init datatable
		
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Open Positions</div>
			
		 
    	<div id="rightclickarea">
			 <div id="gridDiv">
				<table class="display" id="OpenPosition">
					<thead>
						<tr>
							<th>Reference No.</th>
							<th>Designation</th>
							<th>Grade</th>
							<th>Company</th>
							<th>Department</th>
							<th>Location</th>
							<th>No of Position's</th>
							<th>Status</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="popup-grid-close"><fmt:message key="common.button.close"/></div>
			</div>
		</div>		
		
		
</div>
</div>
