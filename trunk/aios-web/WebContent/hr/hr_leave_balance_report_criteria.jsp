<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />
<style>
.ui-multiselect .count{
	padding:0!important;
}
</style>
<style type="text/css">
#purchase_print {
	overflow: hidden;
} 
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width:50%!important;
	}
	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}


</style>
<script type="text/javascript"> 

var oTable; var selectRow=""; var aSelected = []; var aData="";
 var jobAssignmentId = 0; var fromDate = ""; var toDate ="";
 var leaveId="";
$(function(){  
		
	 $("#jobAssignmentId").multiselect();
		
		 $(".print-call").click(function(){  
			 fromDate=$('#fromDate').val();
			 toDate=$('#endDate').val(); 
			 leaveId=$('#leaveId').val(); 
			 var selectedPersonId = generateDataToSendToDB("jobAssignmentId"); 
				window.open('<%=request.getContextPath()%>/print_leave_balance_report.action?fromDate='+ fromDate + '&leaveId=' + leaveId + '&selectedPersonId='+ selectedPersonId,
						'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');	
			 }); 
		  
		  
		  
		  $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
			$('#selectedMonth,#linkedMonth').change();
			$('#l10nLanguage,#rtlLanguage').change();
			if ($.browser.msie) {
			        $('#themeRollerSelect option:not(:selected)').remove();
			}
			$('#fromDate,#endDate').datepick({
			 onSelect: customRange, showTrigger: '#calImg'}); 
	}); 
	
function generateDataToSendToDB(source){
	var typeList=new Array();
	var ids="";
	$('#'+source+' option:selected').each(function(){
		typeList.push($(this).val());
	});
	for(var i=0; i<typeList.length;i++){
		if(i==(typeList.length)-1)
			ids+=typeList[i];
		else
			ids+=typeList[i]+",";
	}
	
	return ids;
}
	
		
		//Prevent selection of invalid dates through the select controls
		function checkLinkedDays() {
			var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
					'#selectedMonth').val());
			$('#selectedDay option:gt(27)').attr('disabled', false);
			$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
					true);
			if ($('#selectedDay').val() > daysInMonth) {
				$('#selectedDay').val(daysInMonth);
			}
		}
		function customRange(dates) {
			if (this.id == 'fromDate') {
				$('#endDate').datepick('option', 'minDate', dates[0] || null); 
			} else {
				$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
			}
		}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Employee Leave Balance
		</div>
		<div class="portlet-content">
			<div class="width90 float-right" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> <input type="text"
						name="fromDate" class="width60 fromDate" id="fromDate"
						readonly="readonly">
				</div>
				<div style="display: none;">
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /></label> <input type="text"
						name="toDate" class="width60 toDate" id="endDate"
						readonly="readonly">
				</div> 
				<div>
					<label class="width30" for="leaveId">Leave</label>
					<select id="leaveId" class="leaveId width60" name="leaveId">
						<c:forEach var="detail" items="${LEAVES}">
							<option value="${detail.leaveId}">${detail.displayName}</option>						
						</c:forEach> 
					</select>	
					
				</div>  
				
				
			</div>
			
			<div class="width100 float-left" style="padding: 5px;"> 
				<div>
					<label class="width100" for="jobAssignmentId">Employee</label>
					<select id="jobAssignmentId" class="jobAssignmentId width100" name="jobAssignmentId" multiple="multiple">
						<c:forEach var="detail" items="${JOB_ASSIGNMENTS}">
							<option value="${detail.jobAssignmentId}">${detail.personName}</option>						
						</c:forEach> 
					</select>	
					
				</div>  
				
			</div> 
 			
			
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>