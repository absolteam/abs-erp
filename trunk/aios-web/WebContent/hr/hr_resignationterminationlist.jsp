<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var resignationTerminationId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#ResignationTermination tbody tr').live('click', function() {
		if (!$('#ResignationTermination tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					resignationTerminationId=aData.resignationTerminationId;
				}
			}
		}
	});
	

$('#add').click(function(){

	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/resignation_termination_add.action",
		data: {resignationTerminationId : 0,recordId:0,
			}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){

		if(resignationTerminationId == null || resignationTerminationId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			resignationTerminationId = Number(resignationTerminationId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/resignation_termination_add.action",  
	     	data: {resignationTerminationId:resignationTerminationId,recordId:0
	     		},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
	     		$("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(resignationTerminationId == null || resignationTerminationId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			resignationTerminationId = Number(resignationTerminationId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/resignation_termination_delete.action",  
	     	data: {resignationTerminationId:resignationTerminationId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 listCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

$("#print").click(function() {  
	if(resignationTerminationId>0){	 
		window.open('resignation_termination_printout.action?resignationTerminationId='+ resignationTerminationId+ '&format=HTML', '',
					'width=800,height=800,scrollbars=yes,left=100px');
	}
	else{
		alert("Please select a record to print.");
		return false;
	}
});


$("#eosPrint").click(function() {  
		if(resignationTerminationId>0){	 
			window.open('show_eos_printout.action?resignationTerminationId='+ resignationTerminationId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});

});


function listCall(){
	
	$('#ResignationTermination').dataTable().fnDestroy();
	oTable = $('#ResignationTermination')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sAjaxSource" : "<%=request.getContextPath()%>/resignation_termination_list_json.action",
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				"aoColumns" : [{
					"mDataProp" : "employeeName"
				},{
					"mDataProp" : "jobNumber"
				},{
					"mDataProp" : "currentPosition"
				},{
					"mDataProp" : "typeView"
				},{
					"mDataProp" : "requestedBy"
				}, {
					"mDataProp" : "effectiveDateView"
				}, {
					"mDataProp" : "actionView"
				}, {
					"mDataProp" : "resignationStatus"
				}]

	}); 
	
}
</script>
<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Resignation Termination</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	<div id="rightclickarea">
			 <div id="gridDiv">
				<table class="display" id="ResignationTermination">
					<thead>
						<tr>
							<th>Employee</th>
							<th>Job Number</th>
							<th>Current Position</th>
							<th>EOS Type</th>
							<th>Requested By</th>
							<th>Effective Date</th>
							<th>Action</th>
							<th>Status</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
			
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="eosPrint">EOS Print</div>
		<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
</div>