<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var recruitmentSourceId=null;
$(document).ready(function (){ 
	
	
	listCall();
	
	
	/* Click event handler */
	$('#RecruitmentSource tbody tr').live('click', function() {
		if (!$('#RecruitmentSource tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					recruitmentSourceId=aData.recruitmentSourceId;
	
					
				}
			}
		}
	});
	
	$('#RecruitmentSource tbody tr').live('dblclick', function() {
		if (!$('#RecruitmentSource tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					recruitmentSourceId=aData.recruitmentSourceId;
					selectedRecruitmentSourceResult(aData);
					$('#common-popup').dialog("close");
				}
			}
		}
	});


});


function listCall(){
	
	
	$('#RecruitmentSource').dataTable().fnDestroy();
	oTable = $('#RecruitmentSource')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sAjaxSource" : "<%=request.getContextPath()%>/common_recruitment_source_list_json.action",
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				"aoColumns" : [ {
					"mDataProp" : "providerName"
				}, {
					"mDataProp" : "sourceType"
				}, {
					"mDataProp" : "manager"
				}, {
					"mDataProp" : "website"
				}, {
					"mDataProp" : "loginId"
				}, {
					"mDataProp" : "password"
				}, {
					"mDataProp" : "fullAddress"
				} ]

	}); 
	
	
		
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Recruitment Source</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			 <div id="gridDiv">
				<table class="display" id="RecruitmentSource">
					<thead>
						<tr>
							<th>Provider Name</th>
							<th>Source Type</th>
							<th>Manager</th>
							<th>Web-site</th>
							<th>Login ID</th>
							<th>Password</th>
							<th>Address</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
			<!-- <div id="gridDiv">
				<table class="display" id=EmployeeLoan></table>
			</div>	 -->
		</div>		
		
	</div>
</div>
</div>
</div>
