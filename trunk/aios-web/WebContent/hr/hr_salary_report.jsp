
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var productId =0;
var oTable;

var fromDate = null;
var toDate = null;
var payMonth=null;
var payYear=null;
var payrollId=0; 

	loadDetData();

					
	$(".xls-download-call").click(function(){
		
		<%-- var personType=Number($('#person').val());
 		var companyType=Number($('#company').val());
 		var status=$('#status').val();
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/get_person_bank_details_report_xls.action?personType='+personType+'&companyType='+companyType+'&status='+status,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;	 --%>		
	
	});
	
 	$(".print-call").click(function(){ 
 		payMonth=$('#payMonth option:selected').val();
 		payYear=$('#payYear option:selected').val();
				window.open('<%=request.getContextPath()%>/get_salary_report_printout.action?printCall=true&selectedMonth='+payMonth+'&selectedYear='+payYear,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
				
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		
 		<%-- 
				
 		var personType=Number($('#person').val());
 		var companyType=Number($('#company').val());
 		var status=$('#status').val();
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/get_person_bank_details_report_pdf.action?personType='+personType+'&companyType='+companyType+'&status='+status,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;	 --%>

	});

	
	$('#payMonth').change(function(){
			var payYear=$('#payYear option:selected').val();
			if(payYear!=null && payYear!='')
				getData();
	});
	
	$('#payYear').change(function(){
			var payMonth=$('#payMonth option:selected').val();
			if(payMonth!=null && payMonth!='')
				getData();
	});
	 
	function getData(){
		oTable = $('#Payroll').dataTable();
		oTable.fnDestroy();
		$('#Payroll').remove();
		$('#gridDiv').html("<table class='display' id='Payroll'></table>");
		loadDetData();
	}
	function loadDetData(){
		payMonth=$('#payMonth option:selected').val();
 		payYear=$('#payYear option:selected').val();
		$('#page-error').hide();
		$('#Payroll').dataTable({ 
			"sAjaxSource": "get_salary_report_table_data.action?selectedMonth="
				+ payMonth+"&selectedYear="+payYear,
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": 'Payroll ID', "bVisible": false},
				{ "sTitle": '<fmt:message key="hr.attendance.employee"/>'},
				{ "sTitle": '<fmt:message key="hr.attendance.company"/>', "bVisible": false},
				{ "sTitle": '<fmt:message key="hr.attendance.department"/>'},
				{ "sTitle": '<fmt:message key="hr.attendance.location"/>', "bVisible": false},
				{ "sTitle": '<fmt:message key="hr.payroll.periodfrom"/>'},
				{ "sTitle": '<fmt:message key="hr.payroll.periodend"/>'},
				{ "sTitle": '<fmt:message key="hr.payroll.paymonth"/>'},
				{ "sTitle": '<fmt:message key="hr.payroll.netpay"/>'},
				{ "sTitle": '<fmt:message key="hr.payroll.executeddate"/>'},
				{ "sTitle": '<fmt:message key="hr.payroll.salarystatus"/>', "bVisible": false},
				{ "sTitle": '<fmt:message key="hr.payroll.requeststatus"/>'},
				
			],
			"sScrollY": $("#main-content").height() - 350,
			//"bPaginate": false,
			"aaSorting": [[0, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	
		//Json Grid
		//init datatable
		oTable = $('#Payroll').dataTable();
	}
</script>

<div id="main-content">
	
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Consolidated Wages

		</div>
		<div class="portlet-content">
			<div class="width45 float-left" style="padding: 5px;">
				 <div class="width60 float-left">
						<label class="width30" for="payMonth"><fmt:message key="hr.payroll.paymonth"/><span class="mandatory">*</span></label>
						<select id="payMonth" name="payMonth" class="width30 validate[required]">
							<option value="">--Select--</option>
							<c:forEach items="${PAYMONTH_LIST}" var="comp1" varStatus="status">
								<option value="${comp1}">${comp1}</option>
							</c:forEach>
						</select>
						<select id="payYear" name="payYear" class="width30 validate[required]">
							<option value="2015">2015</option>
							<option value="2014">2014</option>
							
						</select>
					</div> 

			</div>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="gridDiv">
					<table class="display" id="Payroll"></table>
				</div>
		 	</div>		
			<div class="vmenu">
						<div class="first_li"><span>View</span></div>
						<div class="first_li"><span>View Payslip</span></div>
						<div class="first_li"><span>Download</span></div>
						<div class="first_li"><span>Delete</span></div>
				
			</div> 
			<div class="float-right buttons ui-widget-content ui-corner-all" style="display:none;">
					<div class="portlet-header ui-widget-header float-right delete" id="delete">Delete</div>
					<div class="portlet-header ui-widget-header float-right download" id="download">Download</div>
					<div class="portlet-header ui-widget-header float-right view" id="view">View Payslip</div>
					<div class="portlet-header ui-widget-header float-right view-payroll" id="view-payroll">View</div>
			</div>
		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
