<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<style>
.container {float: left;padding: 1px;}
.container select{width: 33em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 
</style> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script src="fileupload/FileUploader.js"></script>



<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable=null; var selectRow=""; var aData="";var aSelected = [];var payrollId=0; 
var requestStatus="";
var status="";
$(document).ready(function(){
	$jquery("#POAYROLLFORM").validationEngine('attach');
	leaveHistoryCall();
	//$(".employees").multiselect();
	//Save & discard process
	$('.process').click(function(){ 
		$('#page-error').hide();
		if($jquery("#POAYROLLFORM").validationEngine('validate')){
			 var cnfrm = confirm('Please make sure that Leave & Attendance Deviation Request has been processed by Approver?');
	 		 if(!cnfrm)
	 			return false;
			var payPolicy=Number($('#payPolicy').val());
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			var persons = generateDataToSendToDB("jobAssignmentList"); 

			var payMonth=$('#payMonth option:selected').val();
	 		var payYear=$('#payYear option:selected').val();
	 		
			//persons=persons.replace(/(\s+)?.$/, "");
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/payroll_execution_process.action", 
			 	async: false, 
			 	data:{ 
			 		payPolicy:payPolicy,
			 		fromDate:fromDate,toDate:toDate,
			 		persons:persons,
			 		payMonth:payMonth,
			 		payYear:payYear
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						clear_form_elements();
						checkDataTableExsist();
					 }else{
						 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('#discard').click(function(){
		window.location.reload();
	});
			
	{
		var payPolicy=Number($("#payPolicy").val());
		if(payPolicy>0){
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_payroll_employee_list.action", 
				 	async: false, 
				 	data:{ 
				 		payPolicy:payPolicy,
				 		toDate:toDate
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $("#person_result").html(result);
					},
					error:function(result){  
						 $("#person_result").html(result);
					}
				});  
   	 	}
	}
	
	$('.reset').click(function(){
		$('#payPolicy').trigger('change');
		clear_form_elements();
		return false;
	});
	
     //Date process
     $('#fromDate,#toDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   
     
     /* Click event handler */
     $('#Payroll tbody tr').live('click', function () {
    	 $('#page-error').hide();
     	  if ( $(this).hasClass('row_selected') ) {
     		  $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];
         }
         else {
             oTable.$('tr.row_selected').removeClass('row_selected');
             $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];

         }
     	 return false;
     });
     
     $('#edit').click(function(){
    	 $('#page-error').hide();
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_edit_payroll.action", 
				 	async: false, 
				 	data:{ 
				 		payrollId:payrollId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#main-wrapper").html(result);
					},
					error:function(result){  
						$("#main-wrapper").html(result);
					}
				});  
    	
    	 return false;
 	});
     $('#delete').click(function(){
    	 $('#page-error').hide();
    	 var cnfrm = confirm('Selected record will be deleted permanently');
 		 if(!cnfrm)
 			return false;
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/delete_payroll.action", 
				 	async: false, 
				 	data:{ 
				 		payrollId:payrollId,messageId:0
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							checkDataTableExsist();
						 }else{
							 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					}
				});  
    	
    	 return false;
 	});
    
   
     $('#deleteAll').click(function(){
    	 $('#page-error').hide();
    	 var cnfrm = confirm('Selected records will be deleted permanently');
 		 if(!cnfrm)
 			return false;
 		 
 		 var payMonth=$('#payMonth option:selected').val();
    	 var payYear=$('#payYear option:selected').val();
    	 var payLineDetail='';
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/delete_payroll_all.action", 
				 	async: false, 
				 	data:{ 
				 		payMonth:payMonth,payYear:payYear,payLineDetail:payLineDetail
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							checkDataTableExsist();
						 }else{
							 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					}
				});  
    	
    	 return false;
 	});
     
     $('#payslip').click(function(){
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0){
    		 var win1=window.open('getPayslip.action?format=HTML&recordId='
		 		 		+ payrollId+'&approvalFlag=Y','WorkFlowApprove','width=800,height=800,scrollbars=yes,left=100px');
    	 }else{
    		 $('#page-error').hide().html("Select any one record").slideDown(1000); 
    	 }
    	 return false;
 	});
     
     $('#view').click(function(){
  		
  		if(payrollId!=null){
  	    	 $.ajax({
  					type:"POST",
  					url:"<%=request.getContextPath()%>/get_payroll_view.action", 
  				 	async: false, 
  				 	data:{ 
  				 		payrollId:payrollId,viewRedirect:"PayrollExecution"
  				 	},
  				    dataType: "html",
  				    cache: false,
  					success:function(output){
  						var myWindow=window.open('printing/sys_empty_template.jsp','_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
						myWindow.document.write(output);
						myWindow.focus();
						//myWindow.location.reload();
  					},
  					error:function(result){  
  						var myWindow=window.open('printing/sys_empty_template.jsp','_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
						myWindow.document.write(output);
						myWindow.focus();
						//myWindow.location.reload();
  					}
  				});  
     	 	}
  		return false;
  	});
     $('#download,.download').click(function(){
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0){
    		 var win1=window.open('downloadPayslip.action?recordId='
		 		 		+ payrollId+'&approvalFlag=N&format=PDF');
    	 }else{
    		 $('#page-error').hide().html("Select any one record").slideDown(1000); 
    	 }
    	 return false;
 	});
     
     $("#payView").click(function(){
    	 var selectedLocationId=0;
    	 var payMonth=$('#payMonth option:selected').val();
    	 var payYear=$('#payYear option:selected').val();
	 		
 		window.open('<%=request.getContextPath()%>/payroll_report_printout.action?payMonth='+payMonth+'&payYear='+payYear+'&locationId='+selectedLocationId+'&printCall='+false,
 				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
		
		return false;
	
 	});
});
function generateDataToSendToDB(source){
	var typeList=new Array();
	var ids="";
	$('#'+source+' option:selected').each(function(){
		typeList.push($(this).val());
	});
	for(var i=0; i<typeList.length;i++){
		if(i==(typeList.length)-1)
			ids+=typeList[i];
		else
			ids+=typeList[i]+",";
	}
	
	return ids;
}
function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}

function checkDataTableExsist(){
	oTable = $('#Payroll').dataTable();
	oTable.fnDestroy();
	$('#Payroll').remove();
	$('#gridDiv').html("<table class='display' id='Payroll'></table>");
	leaveHistoryCall();
}
function leaveHistoryCall(){
	$('#page-error').hide();
	$('#Payroll').dataTable({ 
		"sAjaxSource": "payroll_execution_listing_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Payroll ID', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.employee"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.company"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.department"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.location"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.payroll.periodfrom"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.periodend"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.paymonth"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.netpay"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.executeddate"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.salarystatus"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.payroll.requeststatus"/>'},
			
		],
		"sScrollY": $("#main-content").height() - 450,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Payroll').dataTable();
}
function clear_form_elements() {
	$('#page-error').hide();
	
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="hr.payroll.payrollexecution"/></div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="POAYROLLFORM" class="" name="POAYROLLFORM" method="post">
		  	<div class="width100 float-left" id="hrm"> 
					<div class="width100 float-left" id="LeaveInfoContent">
						<fieldset style="min-height:20px;display: none;">
							<legend><fmt:message key="hr.attendance.executioninformation"/></legend>
							<div class="width100 float-left">
								<div class="float-left width50">
									<label class="width30" for="payPolicy">Pay Policy<span class="mandatory">*</span></label>
									<select class="payPolicy width60 validate[required]" id="payPolicy">
										<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
											<%-- <option value="${policy.key}">${policy.value}</option> --%>
											<option value="3">Monthly</option>
										</c:forEach>
									</select>
								</div> 
							</div>
						</fieldset>
						<fieldset style="min-height:150px;">
							<div class="width50 float-left">
								<label class="width30" for="payMonth"><fmt:message key="hr.payroll.paymonth"/></label>
								<select id="payMonth" name="payMonth" class="width30">
									<option value="">--Select--</option>
									<c:forEach items="${PAYMONTH_LIST}" var="comp1" varStatus="status">
										<option value="${comp1}">${comp1}</option>
									</c:forEach>
								</select>
								<select id="payYear" name="payYear" class="width30">
									<option value="2015">2015</option>
									<option value="2014">2014</option>
									<option value="2016">2016</option>
								</select>
							</div> 
							
						 	<div class="width100 float-left" id="person_result">
							</div>
						</fieldset>
						
				 </div> 
				<div class="clearfix"></div>
				<div class="float-right buttons ui-widget-content ui-corner-all">
					<div class="portlet-header ui-widget-header float-right delete" id="deleteAll">Delete All</div>
					<div class="portlet-header ui-widget-header float-right" id="payView">Pay View</div>
					<div class="portlet-header ui-widget-header float-right reset" id="reset"><fmt:message key="common.button.reset"/></div>
					<div class="portlet-header ui-widget-header float-right process" id="process_save"><fmt:message key="common.button.run"/></div>
				</div>
 			</div>
 		</form>
		</div>
		
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Payroll"></table>
			</div>
	 	</div>		
		<div class="vmenu">
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Payslip</span></div>
					<div class="first_li"><span>Delete</span></div>
			
		</div> 
		<div class="float-right buttons ui-widget-content ui-corner-all">
					<div class="portlet-header ui-widget-header float-right delete" id="delete">Delete</div>
					<div class="portlet-header ui-widget-header float-right payslip" id="payslip">Payslip</div>
					<div class="portlet-header ui-widget-header float-right view" id="view">View</div>
		</div>
	</div>	
</div>
