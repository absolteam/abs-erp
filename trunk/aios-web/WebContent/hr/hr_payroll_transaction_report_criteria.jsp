
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var productId =0;
var oTable;
 
 getDate();
function getDate() {
 
	var year=Number($('#payYear').val());
	var month=$('#payMonth').val();
	var element=Number($('#elementId').val());
 
	 $('#example').dataTable(
				{
					"sAjaxSource" : "payroll_transactions_json.action?selectedType="
							+ element + "&selectedMonth=" + month+ "&selectedYear="
							+ year,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [ {
						"sTitle" : "Employee Name"
					}, {
						"sTitle" : "Designation"
					}, {
						"sTitle" : "Pay Element"
					}, {
						"sTitle" : 'Element Type'
					}, {
						"sTitle" : 'Pay Month'
					}, {
						"sTitle" : 'Amount' 
					}, ],
					"sScrollY" : $("#main-content").height() - 235,
					//"bPaginate": false,
					"aaSorting" : [ [ 1, 'desc' ] ],
					"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
						/* if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
							$(nRow).addClass('row_selected');
						} */
					}
				});
	 
	 oTable = $('#example').dataTable();
}

var selectedType = null;

$(".print-call").click(function(){ 
	 
	 
	var year=Number($('#payYear').val());
	var month=$('#payMonth').val();
	var element=Number($('#elementId').val());
		 //alert(fromDate + fromDate);
			window.open('<%=request.getContextPath()%>/show_payroll_transaction_printout.action?selectedType='+element+'&selectedMonth='+month+'&selectedYear='+year,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;			
	
	 
	 });
	 
$(".pdf-download-call").click(function(){ 
	 
	 
	var year=Number($('#payYear').val());
	var month=$('#payMonth').val();
	var element=Number($('#elementId').val());
		 //alert(fromDate + fromDate);
			window.open('<%=request.getContextPath()%>/show_payroll_transaction_PDF.action?selectedType='+element+'&selectedMonth='+month+'&selectedYear='+year,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;			
	
	 
	 });
$(".xls-download-call").click(function(){ 
	 
	 
	var year=Number($('#payYear').val());
	var month=$('#payMonth').val();
	var element=Number($('#elementId').val());
		 //alert(fromDate + fromDate);
			window.open('<%=request.getContextPath()%>/show_payroll_transaction_XLS.action?selectedType='+element+'&selectedMonth='+month+'&selectedYear='+year,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;			
	
	 
	 });

	$('#example tbody tr').live('click', function() {

		if ($(this).hasClass('row_selected')) {
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);

			purchaseId = aData[0];
		} else {
			oTable.$('tr.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected');
			aData = oTable.fnGetData(this);
			productId = aData[0];
		}

	});

	$("select[name='itemType']").change(function() {
		// multipleValues will be an array
		/* multipleValues = $(this).val() || []; */
		selectedType = $(this).val();

	});
	
	$('#payYear').combobox({
		selected : function(event, ui) {
			getDate();
		}
	});
	
	$('#payMonth').combobox({
		selected : function(event, ui) {
			getDate();
		}
	});
	
	$('#elementId').combobox({
		selected : function(event, ui) {
			getDate();
		}
	});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>payroll
			transaction report

		</div>
		<div class="portlet-content">





			<div class="width45 float-left" style="padding: 5px;">

				<div class="width15 float-left">
					<label class="width30" for="supplierName">Payroll Element</label> 
				</div>
				<div class="width40 float-left">	
					<select
						id="elementId">
						<option value="0">Select</option>
						<c:forEach var="element" items="${ELEMENTS}">
							<option value="${element.payrollElementId}">${element.elementName}</option>
						</c:forEach>

					</select>
				</div>

			</div>


			<div class="width50 float-right">
				<div class="width10 float-left">
					<label class="width30" for="payMonth">Pay Month</label> 
				</div>
				<div class="width20 float-left">
					<select
						id="payMonth" name="payMonth" 
						class="width30 validate[required]">
						<option value="0">--Select--</option>
						<c:forEach items="${PAYMONTH_LIST}" var="comp1" varStatus="status">
							<option value="${comp1}">${comp1}</option>
						</c:forEach>
					</select> 
				</div>
				<div class="width20 float-left" style="margin-left: 50px;">
					<select id="payYear" name="payYear"
						class="width30 validate[required]">
						<option value="2015">2015</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
					</select>
				</div>
			</div>
		</div>





		<div class="tempresult" style="display: none;"></div>

		<div id="rightclickarea">
			<div id="ledgerFiscals">
				<table class="display" id="example">

				</table>
			</div>
		</div>


	</div>
</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>

	</div>
</div>