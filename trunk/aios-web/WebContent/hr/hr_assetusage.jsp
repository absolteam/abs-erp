<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var assetUsageId=null;
$(document).ready(function (){ 
	 
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#AssetUsage tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        assetUsageId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        assetUsageId=aData[0];
	    }
	});
	$('#AssetUsage tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        assetUsageId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        assetUsageId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/asset_usage_add.action",
		data: {assetUsageId : 0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
		if(assetUsageId == null || assetUsageId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			assetUsageId = Number(assetUsageId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/asset_usage_add.action",  
	     	data: {assetUsageId:assetUsageId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(assetUsageId == null || assetUsageId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			assetUsageId = Number(assetUsageId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/delete_asset_usage.action",  
	     	data: {assetUsageId:assetUsageId,messageId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

$("#print").click(function() {  
	   
	 var allVals = [];
	   $('.asset-group').each(function() {
		   if($(this).attr("checked"))
	      	 allVals.push($(this).val());
	     });
	   var stringArray="";
	   for(var j=0;j<allVals.length;j++){ 
		   if(j==0)
			   stringArray+=allVals[j];
		   else
			   stringArray+=","+allVals[j];
		} 
		if(allVals.length>0){	
			window.open('asset_allocation_printout.action?assetIds='+ stringArray+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});

});

function alreadyListCall(){
	oTable = $('#AssetUsage').dataTable();
	oTable.fnDestroy();
	$('#AssetUsage').remove();
	$('#gridDiv').html("<table class='display' id='AssetUsage'></table>");
	listCall();
}
function listCall(){
	
	$('#AssetUsage').dataTable({ 
		"sAjaxSource": "asset_usage_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'AssetUsageId', "bVisible": false},
			{ "sTitle": 'Employee Name',"sWidth": "250px"},
			{ "sTitle": 'Asset Name',"sWidth": "250px"},
			{ "sTitle": 'Serial Number'},
			{ "sTitle": 'Type'},
			{ "sTitle": 'Brand'},
			{ "sTitle": 'Make'},
			{ "sTitle": 'Model'},
			{ "sTitle": 'Custody Date'},
			{ "sTitle": 'Asset Status'},
			{ "sTitle": 'Allocated By',"sWidth": "100px"},
			{ "sTitle": 'Department / Branch',"sWidth": "150px"},
			{ "sTitle": 'Description'},
			{ "sTitle": 'Group To Print',"mDataProp": function(aData) {
				return '<td><input type="checkbox" class="asset-group" name="asset-group[]" value="'+aData[0]+'"/></td>';

			}
			}
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#AssetUsage').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Asset Custody</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="AssetUsage"></table>
			</div>	
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Print</span></div>
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
