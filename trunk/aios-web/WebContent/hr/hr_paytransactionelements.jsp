<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script>
	$(function() {

		$('#tabs').tabs('destroy').tabs();
		$("#tabs")
				.tabs(
						{
							select : function(event, ui) {
								for ( var i = 1; i <= 1000; i++) {
									$('#ui-tabs-' + i).empty();
								}
							},
							beforeLoad : function(event, ui) {
								$(".ui-tabs-hide").empty();
								ui.jqXHR
										.error(function() {
											ui.panel
													.html("Couldn't load this tab. We'll try to fix this as soon as possible. ");
										});
							}

						});

	});
</script>
<style>
/* .ui-tabs-vertical { width: 100%; min-height:250px; border:1px solid #DDDDDD;}
  .ui-tabs-vertical .ui-tabs-nav { padding-top: 5px;padding-bottom: 5px; float: left;position:relative; width: 17%;min-height:250px; }
  .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
  .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
  .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
  .ui-tabs-vertical .ui-tabs-panel { 
  float: left; width: 40em;border:1px solid #DDDDDD;padding-top: 5px;padding-bottom: 5px;
  min-height:250px;width:79%;position:relative;
  } */
.ui-tabs .ui-tabs-panel {
	list-style: none outside none;
	padding: 0.2em 0.2em 0;
	position: relative;
	min-height: 250px;
	border: 1px solid #DDDDDD;
	width: 100%;
	margin-top: -4px;
	margin-left: 3px;
}
</style>
<c:choose>
	<c:when test="${fn:length(JOB_PAY_ELEMENTS)>0}">
		<div id="tabs">
			<ul>
				<c:forEach var="elment" items="${JOB_PAY_ELEMENTS}"
					varStatus="status">
					<li>
						<%-- <a 
				href="ajax/${elment.allowanceUrl}.action?jobPayrollElementId=${elment.jobPayrollElementId}&jobAssignmentId=${elment.jobAssignmentId}">
				${elment.payrollElement.elementName}</a> --%> <a
						href="<%=request.getContextPath()%>/${elment.allowanceUrl}.action?jobPayrollElementId=${elment.jobPayrollElementId}">${elment.payrollElementByPayrollElementId.elementName}</a>
					</li>
				</c:forEach>

			</ul>
			<%-- <c:forEach var="elment" items="${JOB_PAY_ELEMENTS}" varStatus="status">
			<div id="tabs-${status.index+1}"></div>
		</c:forEach> --%>
		</div>

	</c:when>
	<c:otherwise>
		<div class="width100">
			<div>According the configuration there is no Variance or Count
				based pay for this employee.</div>
		</div>
	</c:otherwise>
</c:choose>

