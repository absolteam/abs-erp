<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>

<style>
.container {float: left;padding: 1px;}
.container select{width: 31em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 

#allowance-manipulation-div>#main-content{
	overflow-y: hidden;
	border:none;
}
#allowance-manipulation-div>#main-content #main-content{
	overflow-y: hidden;
	border:none;
}
.ui-autocomplete-input{
	width: 65%!important;
}
</style> 

<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
var payrollId=0;; 
var requestStatus="";
var status="";
var payLineDetail="";
$(function() {
	

	$jquery("#POAYROLLFORM").validationEngine('attach');
	//Save & discard process
	$('.process').click(function(){ 
		$('#page-error').hide();
		if($jquery("#POAYROLLFORM").validationEngine('validate')){
			
		
			var jobAssignmentId = $('#jobAssignmentId option:selected').val(); 
			var payPolicy = $('#payPolicy option:selected').val(); 
			var payPeriod = $('#payType option:selected').val(); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pay_transaction_elements.action", 
			 	async: false, 
			 	data:{ 
			 		jobAssignmentId:jobAssignmentId,
			 		payPolicy:payPolicy,
			 		payPeriod:payPeriod
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $("#element-div-load").html(result);
					 
					 
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	
	
	
	$('#payPolicy').change(function(){
		getPayPeriodList();
		return false;
	});
	
	$('.reset').live('click',function(){
		clear_form_elements();
	});
	
	$('#transactionDate').datepick();
     //Date process
     $('#fromDate,#toDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   
     
     /* Click event handler */
     $('#Payroll tbody tr').live('click', function () {
    	 $('#page-error').hide();
     	  if ( $(this).hasClass('row_selected') ) {
     		  $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];
         }
         else {
             oTable.$('tr.row_selected').removeClass('row_selected');
             $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];

         }
     });
     
     
	 $('.jobs-assignment-popup').live('click',function(){
	      $('.ui-dialog-titlebar').remove();  
	      $('#job-common-popup').dialog('open');
	         $.ajax({
	            type:"POST",
	            url:"<%=request.getContextPath()%>/common_job_assignment_list.action",
	            async: false,
	            dataType: "html",
	            cache: false,
	            success:function(result){
	                 $('.job-common-result').html(result);
	                 return false;
	            },
	            error:function(result){
	                 $('.job-common-result').html(result);
	            }
	        });
	        return false;
	}); 
	 
	 

	$('#job-common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:900,
		height:600,
		bgiframe: false,
		modal: true 
	});
	
	$('#payPeriod').combobox({
		selected : function(event, ui) {
			getPeriodBasedTransaction();
		}
	});
	
	$('#save_payroll').click(function(){ 
		if($jquery("#POAYROLLFORM").validationEngine('validate')){  
			var payrollId=Number($('#payId').val());

			var successMsg="";
			if(payrollId>0)
				successMsg="Successfully Updated";
			else
				successMsg="Successfully Created";
				
		
			var jobAssignmentId=Number($('#jobAssignmentId').val());
			var payPeriodId=Number($('#payPeriod').val());
	
			payLineDetail=getLineDetails();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_payperiod_transaction.action", 
			 	async: false, 
			    dataType: "html",
			 	data:{ 
			 		payrollId:payrollId,
			 		jobAssignmentId:jobAssignmentId,
			 		payPeriodId:payPeriodId,
			 		payLineDetail:payLineDetail
			 	},
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 listLoadCall();
						 $('#success_message').hide().html(successMsg).slideDown(1000);
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	

});
function listLoadCall(){
  	 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/pay_transaction.action", 
	 	async: false, 
	    dataType: "html",
	    cache: false,
		success:function(result){
			if(typeof($('#job-common-popup')!="undefined")){ 
				$('#job-common-popup').dialog('destroy');		
				$('#job-common-popup').remove(); 
			} 
			if(typeof($('#common-popup')!="undefined")){  
				 $('#common-popup').dialog('destroy');		
				 $('#common-popup').remove(); 
			 }
			$("#main-wrapper").html(result);  
		},
		error:function(result){  
			$("#main-wrapper").html(result);  
		}
	});  
}

function getPayPeriodList(){
	var jobAssignmentId = Number($('#jobAssignmentId').val()); 
	var payPolicy=Number($('#payPolicy option:selected').val());
	if(payPolicy!=null && jobAssignmentId!=null && jobAssignmentId>0){
    	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_active_pay_period_list.action", 
			 	async: false, 
			 	data:{ 
			 		jobAssignmentId:jobAssignmentId,
			 		payPolicy:payPolicy
			 	},
			    dataType: "json",
			    cache: false,
				success:function(result){
					$('#payPeriod').html("");
					$('#payPeriod').append(
							'<option value="">-Select-</option>');
					$(result.periods)
					.each(
						function(index) {
								$('#payPeriod')
										.append(
												'<option value='
														+ result.periods[index].payPeriodTransactionId
														+ '>'
														+ result.periods[index].payMonth+" - "+result.periods[index].startDate+" >> "+result.periods[index].endDate
														+ '</option>');
						});
					
					$('#payPeriod').combobox({
						selected : function(event, ui) {
							getPeriodBasedTransaction();
						}
					});
				},
				error:function(result){  
					$('#payPeriod').html("");
				}
			});  
	 	}
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}

function getPeriodBasedTransaction(){
	var jobAssignmentId=Number($('#jobAssignmentId').val());
	var payPeriodId=Number($('#payPeriod').val());
	if(jobAssignmentId!=null && payPeriodId!=null 
			&& jobAssignmentId!='' && payPeriodId!='' ){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/period_based_transaction.action", 
		 	async: false,
		 	data:{jobAssignmentId:jobAssignmentId,payPeriodId:payPeriodId},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#Payroll-div").html(result);  
				return false;
			}
	 });
	}
	return false;
}

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function clear_form_elements() {
	$('#page-error').hide();
	$('#POAYROLLFORM').each(function(){
	    this.reset();
	});
}
function getJobAssignmentFromObject(jobAssignment){
	$("#employees").html(jobAssignment.personName);
	$("#jobAssignmentId").val(jobAssignment.jobAssignmentId);
	getPayPeriodList();
}

</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay Transaction</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="success_message" class="response-msg success ui-corner-all" style="display:none;"></div>
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="POAYROLLFORM" class="" name="POAYROLLFORM" method="post" style="position: relative;">
				<div class="width100 float-left" id="hrm">
					<div class="width100 float-left" id="LeaveInfoContent">
						<fieldset style="min-height: 100px;">
							<legend>
								Employee Information
							</legend>
								<div class="width40 float-left">
								
								<div class="float-left width100">
									<label class="width30" for="jobAssignmentId"><fmt:message
											key="hr.job.employees" /><span class="mandatory">*</span> </label> <%-- <select
										name="jobAssignmentId" id="jobAssignmentId"
										class="width60 validate[required]">
										<option value="">Select</option>
										<c:forEach items="${JOB_ASSIGNMENT_LIST}" var="job"
											varStatus="status">
											<option value="${job.jobAssignmentId}">${job.person.firstName}
												${job.person.lastName} [${job.jobAssignmentNumber}]</option>
										</c:forEach>
									</select> --%>
									<span id="employees" class="width50"> </span>
									<span class="button float-right" id="jobAssign"  style="position: relative; top:6px;">
										<a style="cursor: pointer;" id="jobpopupassign" class="btn ui-state-default ui-corner-all width40 jobs-assignment-popup"> 
											<span class="ui-icon ui-icon-newwin">
											</span> 
										</a>
									</span> 
									<input type="hidden" name="jobAssignmentId" id="jobAssignmentId" value="" class="width50"/>
								</div>
								<div class="float-left width100" style="display:none;">
									<label class="width30" for="payPolicy">Pay Policy<span class="mandatory">*</span></label>
									<select class="payPolicy width60 validate[required]" id="payPolicy">
										<option value="">Select</option>
										<c:forEach var="policy" items="${PAY_POLICY}" varStatus="statusPolicy">
											<c:if test="${policy.key eq 3}">
												<option value="${policy.key}" selected="selected">${policy.value}</option>
											</c:if>
											<c:if test="${policy.key ne 3}">
												<option value="${policy.key}">${policy.value}</option>
											</c:if>
										</c:forEach>
									</select>
								</div> 
							</div>
							<div class="width50 float-left">
								<div class="float-left width100">
									<label class=" width30" for="payPeriod">Pay Period<span
										class="mandatory">*</span></label> <select
										class="payPeriod  width70 validate[required]" id="payPeriod">
										<option value="">Select</option>
										<c:forEach var="payPeriod" items="${PAY_PERIOD}"
											varStatus="statusMode">
											<option value="${payPeriod.payPeriodTransactionId}">${payPeriod.startDate}-${payPeriod.endDate}</option>
										</c:forEach>
									</select>
								</div>
								<div class="float-left width100" style="display:none;">
									<label class="width30" for="payType">Transaction Date</label> <input type="text"
										id="transactionDate" name="transactionDate"
										class="width60" />
								</div>
							</div>

							<div class="clearfix"></div>
						<!-- <div class="float-right buttons ui-widget-content ui-corner-all">
							<div class="portlet-header ui-widget-header float-right process"
								id="process_save">
								Get Pay List
							</div>
	
						</div> -->
						</fieldset>
					</div>
				</div>
				</form>
					
					
				<div class="width100 float-left" id="hrm">
					<fieldset>
						<legend>
							Allowance Information
						</legend>
						<div class="width100 float-left" id="element-div-load">
							<div id="tabs">
							  <ul>
							    <li><a href="#tabs-1">Pay Details</a></li>
							  </ul>
							  <div id="tabs-1">
							  	<div id="Payroll-div">
							  	</div>
							  	<div class="float-right buttons ui-widget-content ui-corner-all">
									<div class="portlet-header ui-widget-header float-right save"
										id="save_payroll">
										Save
									</div>
								</div>
							  </div>
							</div>
						</div>
					</fieldset>
					
				</div>
		</div>
		
		<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span>
					</a>
				</div>
				<div id="job-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="job-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
	</div>	
</div>