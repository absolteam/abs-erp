<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
div.border
{
	padding:5px;
	border:3px solid gray;
	margin:0px;
	cursor: pointer;
}

div.border span
{
	text-align: center;
	font-weight: bold;
	color: blue;
}

.container {float: left;padding: 1px;}
.container select{width: 33em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var workingShiftId=0;
var j = jQuery.noConflict();

$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	$('.formError').remove();
	
	listCall();
	
	/* Click event handler */
	$('#JobShift tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
	});
	$('#JobShift tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        workingShiftId=aData[0];
	    }
		
	});
	
	
	
	
	$('#view').click(function(){
		$('.error,.success').hide();

		if($jquery("#ATTENDANCEFORM").validationEngine('validate')){
			var persons = generateDataToSendToDB("jobAssignmentList"); 
			var toDate=$('#toDate').val();
			var fromDate=$('#fromDate').val();
			
			window.open('<%=request.getContextPath()%>/view_working_shit.action?toDate='+toDate+'&fromDate='+fromDate+'&persons='+persons,
    				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;	
		}else{
			return false;

		}
		

		return false;
	});


	$('#add').click(function(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_designation.action",
			data: {designationId : 0}, 
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#main-wrapper").html(result);
			}
		});	
		return false; 
	});
	
	$('#assign').click(function(){
			if(workingShiftId == null || workingShiftId == 0)
			{
				alert("Please select one row to edit");
				return false;
			}
			else
			{			
				workingShiftId = Number(workingShiftId); 
				
		
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/assign_shift_redirect.action",  
		     	data: {workingShiftId:workingShiftId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(data)
		     	{  
					$("#main-wrapper").html(data); //gridDiv main-wrapper 
		     	},
		     	 error:function(result)
	             {
	                 $("#main-wrapper").html(result);
	             }
			}); 
			return false;
			}
			return false;  
	});
	callEmployeeList();
	
	$('#processAttenance').click(function() {
		  $('#attendanceDateDiv').toggle('slow', function() {
		    // Animation complete.
		  });

		});
	 $('#fromDate,#toDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   


});
function alreadyListCall(){
	oTable = $('#JobShift').dataTable();
	oTable.fnDestroy();
	$('#JobShift').remove();
	$('#gridDiv').html("<table class='display' id='JobShift'></table>");
	listCall();
}
function listCall(){
	$('#JobShift').dataTable({ 
		"sAjaxSource": "shift_mapping_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Shift ID', "bVisible": false},
			{ "sTitle": 'Shift Name',"sWidth": "200px"},
			{ "sTitle": 'Start Time'},
			{ "sTitle": 'End Time'},
			{ "sTitle": 'Late In Time'},
			{ "sTitle": 'Seviour Late In Time'},
			{ "sTitle": 'Buffer Min(s)'},
			{ "sTitle": 'Lunch Hour(s)'},
			{ "sTitle": 'Employee(s)',"sWidth": "350px"},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#JobShift').dataTable();
}
function generateDataToSendToDB(source){
	var typeList=new Array();
	var ids="";
	$('#'+source+' option:selected').each(function(){
		typeList.push($(this).val());
	});
	for(var i=0; i<typeList.length;i++){
		if(i==(typeList.length)-1)
			ids+=typeList[i];
		else
			ids+=typeList[i]+",";
	}
	
	return ids;
}

function callEmployeeList(){
	var payPolicy=Number(3);
	if(payPolicy>0){
    	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_payroll_employee_list.action", 
		 	async: false, 
		 	data:{ 
		 		payPolicy:payPolicy,
		 		toDate:toDate
		 	},
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $("#person_result").html(result);
			},
			error:function(result){  
				 $("#person_result").html(result);
			}
		});  
	 }
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
		
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Assign Shift</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
		 <div id="success_message" class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="JobShift"></table>
			</div>	
		</div>		
		<div class="vmenu">
 	       <!-- 	<div class="first_li"><span>Add</span></div>
			<div class="sep_li"></div>	 -->
<!-- 			<div class="first_li"><span>View</span></div>	 
			<div class="first_li"><span>Delete</span></div>
-->			<div class="first_li"><span>Assign</span></div>
	
		</div>
		<form id="ATTENDANCEFORM" class="" name="ATTENDANCEFORM">
			<div class="width50 float-left" style="margin-top:12px;"> 
			 	<div id="processAttenance" class="border width100 float-left">
					  <span>Shift View</span> 
				</div>
					<div id="attendanceDateDiv" class="border width100 float-left" style="display:none;">
						<div>
							<label class="width40 float-left" for="fromDate">From Date</label>
							<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="4" class="width50 validate[required]"/>
						</div>
						<div>
							<label class="width40 float-left" for="toDate">To Date</label>
							<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="4" class="width50 validate[required]"/>
						</div>
						<div class="width100 float-left" id="person_result">
						</div>
						<div class="float-left buttons ui-widget-content ui-corner-all"> 
							<div class="portlet-header ui-widget-header float-right" id="view">View</div>
						</div>		
					</div>
			</div>
		</form> 
		
		<div class="float-right buttons ui-widget-content ui-corner-all"> 

		<div class="portlet-header ui-widget-header float-right" id="assign">Assign</div>
	
	</div>
</div>
</div>
</div>
