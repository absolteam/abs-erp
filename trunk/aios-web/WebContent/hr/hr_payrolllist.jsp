<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>



<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable=null; var selectRow=""; var aData="";var aSelected = [];var payrollId=0;; 
var wfstatus="";
$(document).ready(function(){  
	
	leaveHistoryCall();
	
	//Save & discard process
	$('.process').click(function(){ 
		$('#page-error').hide();
		if($("#Payroll").validationEngine({returnIsValid:true})){  
			loanLineDetail=""; loanChargesDetail="";
			var companyId=Number($('#company').val());
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/payroll_execution_process.action", 
			 	async: false, 
			 	data:{ 
			 		companyId:companyId,
			 		fromDate:fromDate,toDate:toDate
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						clear_form_elements();
						checkDataTableExsist();
					 }else{
						 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	

	
     //Date process
     $('#fromDate,#toDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   
     
     /* Click event handler */
     $('#Payroll tbody tr').live('click', function () {
    	 $('#page-error').hide();
     	  if ( $(this).hasClass('row_selected') ) {
     		  $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             wfstatus=aData[11];
         }
         else {
             oTable.$('tr.row_selected').removeClass('row_selected');
             $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             wfstatus=aData[11];
            

         }
     });
     
     $('#view-payroll').live('click',function(){
 		
 		if(payrollId!=null && payrollId!="" && payrollId!=0){
 	    	 $.ajax({
 					type:"POST",
 					url:"<%=request.getContextPath()%>/get_payroll_view.action", 
 				 	async: false, 
 				 	data:{ 
 				 		payrollId:payrollId
 				 	},
 				    dataType: "html",
 				    cache: false,
 					success:function(result){
 						 $("#main-wrapper").html(result);
 					},
 					error:function(result){  
 						 $("#main-wrapper").html(result);
 					}
 				});  
    	 	}else{
    	 		alert("Select any one record to view");
    	 	}
 		return false;
 	});
 	
    
     $('#view').live('click',function(){
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0){
    		 if(payrollId!=null && payrollId!=0 && wfstatus.trim()=='Approved'){
        		 var win1=window.open('getPayslip.action?format=HTML&recordId='
    		 		 		+ payrollId+'&approvalFlag=Y','WorkFlowApprove','width=800,height=800,scrollbars=yes,left=100px');
        	 }else{
        		 $('#page-error').hide().html("Invalid Process! (OR) Not ready for pay slip").slideDown(1000); 
        	 }
         }else{
    		 $('#page-error').hide().html("Please select a payroll.").slideDown(1000); 
    	 }
    	
    	 return false;
 	});
     $('#download,.download').live('click',function(){
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0 && wfstatus.trim()=='Approved'){
    		 var win1=window.open('downloadPayslip.action?recordId='
		 		 		+ payrollId+'&approvalFlag=N&format=PDF');
    	 }else{
    		 $('#page-error').hide().html("Invalid Process! (OR) Not ready for download").slideDown(1000); 
    	 }
    	 return false;
 	});
});

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}

function checkDataTableExsist(){
	oTable = $('#Payroll').dataTable();
	oTable.fnDestroy();
	$('#Payroll').remove();
	$('#gridDiv').html("<table class='display' id='Payroll'></table>");
	leaveHistoryCall();
}
function leaveHistoryCall(){
	$('#page-error').hide();
	$('#Payroll').dataTable({ 
		"sAjaxSource": "employee_payroll_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Payroll ID', "bVisible": false},
			{ "sTitle": 'Employee', "bVisible": false},
			{ "sTitle": 'Company', "bVisible": false},
			{ "sTitle": 'Department'},
			{ "sTitle": 'Location', "bVisible": false},
			{ "sTitle": 'Period From'},
			{ "sTitle": 'Period UpTo'},
			{ "sTitle": 'Pay Month'},
			{ "sTitle": 'Net Amount'},
			{ "sTitle": 'Executed Date'},
			{ "sTitle": 'Salary Status'},
			{ "sTitle": 'Request Status', "bVisible": false},
			{ "sTitle": 'Download'},
		],
		"sScrollY": $("#main-content").height() - 350,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Payroll').dataTable();
}
function clear_form_elements() {
	$('#page-error').hide();
	$('#POAYROLL').each(function(){
	    this.reset();
	});
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>My Payroll</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		 
		</div>
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Payroll"></table>
			</div>
	 	</div>		
		<div class="vmenu">
			<div class="first_li"><span id="view-payroll">View Pay Details</span></div>
			<div class="first_li"><span id="view">View Paysilp</span></div>
			<div class="first_li"><span id="download">Download Paysilp</span></div>
		</div> 
		
		<div class="float-right buttons ui-widget-content ui-corner-all">
			<div class="portlet-header ui-widget-header float-right view" id="view">View Payslip</div>
			<div class="portlet-header ui-widget-header float-right view-payroll" id="view-payroll">View Pay Details</div>
		</div>
	</div>	
</div>
