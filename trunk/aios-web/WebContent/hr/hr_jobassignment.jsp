<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style>

</style>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var jobAssignmentId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#JobAssignment tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	    }
	});
	$('#JobAssignment tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        jobAssignmentId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/job_assignment_add.action",
		data: {jobAssignmentId : 0,recordId:0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
		if(jobAssignmentId == null || jobAssignmentId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			jobAssignmentId = Number(jobAssignmentId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/job_assignment_add.action",  
	     	data: {jobAssignmentId:jobAssignmentId,recordId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#copy').click(function(){
	if(jobAssignmentId == null || jobAssignmentId == 0)
	{
		alert("Please select one row to copy");
		return false;
	}
	else
	{			
		jobAssignmentId = Number(jobAssignmentId); 
		

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/job_assignment_copy.action",  
     	data: {jobAssignmentId:jobAssignmentId,recordId:0},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	},
     	 error:function(result)
         {
             $("#main-wrapper").html(result);
         }
	}); 
	return false;
	}
	return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(jobAssignmentId == null || jobAssignmentId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			jobAssignmentId = Number(jobAssignmentId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/job_assignment_delete.action",  
	     	data: {jobAssignmentId:jobAssignmentId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});
$('#loadEmployeeFromXL').click(function(){  
	//import_employees_from_sheet
	var showPage=$("#xlFileName").val();
	$.ajax({
		type: "POST", 
		
		url: "<%=request.getContextPath()%>/update_employees_from_sheet.action", 
     	async: false,
     	data:{employeeSheet:showPage},
		dataType: "json",
		cache: false,
		success: function(object){ 
			if(object.returnMessage=='SUCCESS'){
				alert("Successfully Done");
				return false;
			}else{
				alert("Error...");
				return false;
			}
		} 		
	}); 
	return false;
}); 

	$("#print").click(function() {  
		if(jobAssignmentId != null && jobAssignmentId > 0) {	
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/job_assignment_contract_data.action", 
		     	async: false,
		     	data:{jobAssignmentId: jobAssignmentId},
				dataType: "json",
				cache: false,
				success: function(object){ 
					window.open('download_employee_contract.action?caller=jobAssignmentAction&template=EmployeeContract&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
				},
				error: function (result) {
					$('.error').hide().html("Employee information not complete!").slideDown(1000);
				}		
			}); 
			
			
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});

});

function alreadyListCall(){
	oTable = $('#JobAssignment').dataTable();
	oTable.fnDestroy();
	$('#JobAssignment').remove();
	$('#gridDiv').html("<table class='display' id='JobAssignment'></table>");
	listCall();
}
function listCall(){

	$('#JobAssignment').dataTable({ 
		"sAjaxSource": "job_assignment_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'JobAssignment Id', "bVisible": false,"bSearchable": false },
			{ "sTitle": 'Number',"sWidth": "50px" ,  'type':"text", 'sSelector': "#Filter"},
			{ "sTitle": 'Job Template',"sWidth": "40px"},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Designation/Job'},
			{ "sTitle": 'Department & Location'},
			{ "sTitle": 'Swipe Id'},
			{ "sTitle": 'Active'},
			{ "sTitle": 'Start Date'},
			{ "sTitle": 'End Date'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	 
	oTable = $('#JobAssignment').dataTable();
	
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Job Assignment</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="JobAssignment"></table>
			</div>	
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Copy</span></div>
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
		
		<div class="float-left buttons ui-widget-content ui-corner-all" style="display: none;"> 
				<div class="portlet-header ui-widget-header float-left" id="loadEmployeeFromXL">Load Employee's</div>
				<input type="text" id="xlFileName" class="width60">
			</div>		
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="print">Generate Contract</div>
		<div class="portlet-header ui-widget-header float-right" id="copy">Copy</div>
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
