<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 

$(document).ready(function (){ 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	 if(typeof($('#job-common-popup')!="undefined")){  
		 $('#job-common-popup').dialog('destroy');		
		 $('#job-common-popup').remove(); 
	 }
	 if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	listCall();

 	/* Click event handler */
 	$('#Company tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0]; 
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});
});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/add_company.action",
		data: {companyId : 0, isSupplier: false}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false;
});

$('#edit').click(function(){
	$('.error,.success').hide();
		if(s == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			var companyId = Number(s); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/add_company.action",  
	     	data: {companyId : companyId, isSupplier: false},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return true;
		}
		return false;  
});

$('#delete').click(function(){
	 $('.error,.success').hide();
		if(s == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected company will be deleted permanently');
			if(!cnfrm)
				return false;
			var companyId = Number(s); 
	
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_company.action",  
		     	data: {companyId:companyId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
		     	{  
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $('#Company').dataTable().fnDestroy();
						 listCall();
						 $('.success').hide().html("Information Deleted Successfully.").slideDown();
					 }
					 else{
						 $('.error').hide().html(message).slideDown(1000);
						 return false;
					 }
		     	},
		     	error:function(result)
	            {
		     		$('.error').hide().html(message).slideDown(1000);
	            } 
			}); 
			return false;
		}
		return false; 
});

$('#view').click(function(){
	$('.error,.success').hide();
	var myedit  = $("#list2").jqGrid('getGridParam','selrow'); 				
		if(myedit == null)
		{
			alert("Please select one row to view");
			return false;
		}
		else
		{			
			var view = $("#list2").jqGrid('getRowData',myedit ); 
			organizationId = view.organizationId;
			locationId	   = view.locationId;
			
			var pageName=$('#pageName').val();
			//alert(" Visa ID= " + visaId2 );	
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/organization_details_view.action",  
	     	data: {organizationId : organizationId,locationId : locationId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	error:function(result)
            {
                $("#main-wrapper").html(result);
            } 
		}); 
		return true;
		}
		return false; 
});

function listCall(){
	//$('#Company').dataTable().fnDestroy();
	oTable = $('#Company').dataTable({ 
 		"sAjaxSource": "company_list_json.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Company ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="hr.company.name"/>'},
 			{ "sTitle": '<fmt:message key="hr.company.type"/>'},
 			{ "sTitle": '<fmt:message key="common.description"/>'},
 			{ "sTitle": '<fmt:message key="hr.company.tradenumber"/>'},
 			
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	
}
</script>



<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Company</div>
			<div class="tempresult" style="display:none;"></div>
		<div class="portlet-content">
			<div class="error response-msg ui-corner-all" style="display:none;"></div>
	  	  <div class="success response-msg ui-corner-all" style="display:none;"></div> 
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Company"></table>
			</div>	
		</div>		
		<div class="vmenu">
 	       	<div class="first_li"><span>Add</span></div>
			<div class="sep_li"></div>		 
			<div class="first_li"><span>Edit</span></div>
			<!-- <div class="first_li"><span>View</span></div> --> 
			<div class="first_li"><span>Delete</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"><!-- 
		<div class="portlet-header ui-widget-header float-right view">View</div>
		-->
		<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="common.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="common.button.edit"/></div>
		<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="common.button.add"/></div>
		
	</div>
</div>
</div>
</div>
