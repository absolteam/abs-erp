<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var identityId=null;var identityType=null;var identityName=null;var identityNumber=null;
var extraParam="";
$(document).ready(function (){ 
	
	$('.formError').remove();
	listCall();
	
	/* Click event handler */
	$('#Identity tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        identityId=aData[0];
	        identityType=aData[1];
	        identityName=aData[2];
	        identityNumber=aData[3];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        identityId=aData[0];
	        identityType=aData[1];
	        identityName=aData[2];
	        identityNumber=aData[3];
	    }
	});
	$('#Identity tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        identityId=aData[0];
	        identityType=aData[1];
	        identityName=aData[2];
	        identityNumber=aData[3];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        identityId=aData[0];
	        identityType=aData[1];
	        identityName=aData[2];
	        identityNumber=aData[3];
	    }
		  
		  extraParam=identityName+"@#"+identityNumber;
		  identityPopupResult(identityId,identityType,extraParam);
			$('#common-popup').dialog("close");
		
	});


});

function alreadyListCall(){
	oTable = $('#Identity').dataTable();
	oTable.fnDestroy();
	$('#Identity').remove();
	$('#gridDiv').html("<table class='display' id='Identity'></table>");
	listCall();
}
function listCall(){
	$('#Identity').dataTable({ 
		"sAjaxSource": "common_identity_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Id', "bVisible": false},
			{ "sTitle": 'Identity Type'},
			{ "sTitle": 'Identity Of'},
			{ "sTitle": 'Identity Number'},
			{ "sTitle": 'Identity Code'},
			{ "sTitle": 'Expire Date'},
			{ "sTitle": 'Issued Place'},
			{ "sTitle": 'Description'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Identity').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Identity Information</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Identity"></table>
			</div>	
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="identity-close"><fmt:message key="common.button.close"/></div>
			</div>
		</div>		
				
	</div>
</div>
</div>
