
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script> --%>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
.custom-combobox-input {
    width: 82%;
}
.custom-combobox-toggle{
	height:20px;
 }
</style>
 
<script type="text/javascript">
var productId =0;
var oTable;

var fromDate = null;
var toDate = null;
var selectedType = null;
$(function(){ 
	


$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

			
	$(".xls-download-call").click(function(){
	
			window.open('<%=request.getContextPath()%>/document_expiry_report_download_xls.action?fromDate='
									+ fromDate + '&toDate='
									+ toDate, +'_blank',
							'width=800,height=700,scrollbars=yes,left=100px,top=2px');

			return false;
	
	});
	
 	$(".print-call").click(function(){ 
		 
 		
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/document_expiry_printout.action?fromDate='+fromDate+'&toDate='+toDate,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
		 
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		
 		
				
				 percentage=$('#percentage').val();
	
				window.open('<%=request.getContextPath()%>/document_expiry_jasper.action?fromDate='
						+ fromDate + '&toDate='
						+ toDate, +'_blank',
				'width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;
	 		
		});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			getData();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			getData();
		}

	});
	
	$('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:600,
		bgiframe: false,
		modal: true 
	});
	
	
$('#employeepopup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var rowId=-1; 
         var personTypes="ALL";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
               data:{id:rowId,personTypes:personTypes},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });

   });
	getData();
	
	$('#companyId').combobox({
		selected : function(event, ui) {
			getData();
		}
	});
	
});  
function personPopupResult(personId,personName,commonParam){
	$('#personName').val(personName);
	$('#personId').val(personId);
	getData();
}

function getData(){ 
	
	var personId=Number($('#personId').val());
	var companyId=Number($('#companyId').val());
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	
	
	 if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	 }else{
	 }
	
	 $('#example').dataTable(
				{
					"sAjaxSource" : "get_document_expiry_data.action?fromDate="
						+ fromDate+"&toDate="+toDate+"&selectedCompanyId="+companyId+"&selectedEmployeeJobId="+personId,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [/*  {
						"sTitle" : "identityId",
						"bVisible" : false,
						"sDefaultContent": "n/a" ,
					}, */ {
						"sTitle" : "Sr. #",
						"sDefaultContent": "n/a" ,
					}, {
						"sTitle" : "Document Type",
						"sDefaultContent": "n/a"
					}, {
						"sTitle" : "Document From",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Document Of",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Expiry Date",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Days Left",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Document Number",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Issued Place",
						"sDefaultContent": "n/a",
					},
					], 
				
					//"bPaginate": false,
					/* "aaSorting" : [ [ 1, 'desc' ] ], */
					"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
												
						/* $('td:eq(0)', nRow).html(aData.identityId);
						$('td:eq(0)', nRow).hide(); */
						
						$('td:eq(0)', nRow).html(iDisplayIndex+1);
						$('td:eq(1)', nRow).html(aData.lookupDetail.displayName);
						
						
						if (aData.person != null) {
							$('td:eq(2)', nRow).html("Person");
						} else {
							$('td:eq(2)', nRow).html("Company");
						}
						
						
						if (aData.person != null) {
							$('td:eq(3)', nRow).html(aData.person.firstName+" "+aData.person.lastName);
						} else {
							$('td:eq(3)', nRow).html(aData.company.companyName);
						}
						
						var javascriptDate = new Date(aData.expireDate);
			            javascriptDate = javascriptDate.getFullYear()+"-"+javascriptDate.getMonth()+"-"+javascriptDate.getDate();
						$('td:eq(4)', nRow).html(javascriptDate);
						
						$('td:eq(5)', nRow).html(calculateToExpire(aData.expireDate));
						
						$('td:eq(6)', nRow).html(aData.identityNumber);
						$('td:eq(7)', nRow).html(aData.issuedPlace);
						
						
					},
				
					"sScrollY": "auto",
	    
	    
	    
	    
	    
	
	});
	 oTable = $('#example').dataTable();
}

function calculateToExpire(date) {
	var today=new Date();

	date = new Date(date);
	
	var diff=date-today;//unit is milliseconds
	if (diff >0) {
		diff=Math.round(diff/1000/60/60/24); //contains days passed since date
	} else {
		diff = 0;
	}
	return diff;
}
	
function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
	
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>document
			expiry report

		</div>
		<div class="portlet-content">
		
		
			<table width="100%">
				<tr>
					<td class="width10">
						<label style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /> : </label>
					</td>
					<td class="width40">
						<input type="text"
							name="fromDate" class="width60 fromDate" id="startPicker"
							readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td class="width10"><label>Person : </label></td>
					<td class="width40">
						<input type="text" readonly="readonly" id="personName" class="width80"></input>
						<span class="button" id="employee"  style="position: relative; top:0px;">
							<a style="cursor: pointer; padding: 0 0 4px 21px; margin-left:22px;" id="employeepopup" class="btn ui-state-default ui-corner-all width100" > 
								<span class="ui-icon ui-icon-newwin">
								</span> 
							</a>
						</span> 
						<input type="hidden" id="personId" class="personId" />
					</td>
				</tr>
				<tr>
					<td class="width10">
						<label style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /> : </label>
					</td>
					<td class="width40">
						<input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td class="width10">
						<label >Company : </label>
					</td>
					<td class="width40">
						<select id="companyId" style="width:100px;">
							<option value="">Select</option>
							<c:forEach var="company" items="${COMPANIES}">
								<option value="${company.companyId}">${company.companyName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>

			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>