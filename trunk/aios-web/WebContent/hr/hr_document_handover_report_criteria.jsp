
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var productId =0;
var oTable;

var fromDate = null;
var toDate = null;
getData();
function getData(){ 
	
	var personId=Number($('#personId').val());
	var companyId=Number($('#companyId').val());
	
	 $('#example').dataTable(
				{
					"sAjaxSource" : "get_document_handover_data.action?fromDate="
						+ fromDate+"&toDate="+toDate+"&selectedCompanyId="+companyId+"&selectedEmployeeJobId="+personId,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [/*  {
						"sTitle" : "Sr. #",
						"sDefaultContent": "n/a" ,
					},  */{
						"sTitle" : "Document Belongs (Company/Person)",
						"sDefaultContent": "n/a"
					}, {
						"sTitle" : "Document Type",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Given Date",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Expected Return Date",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Actural Return Date",
						"sDefaultContent": "n/a",
					}, {
						"sTitle" : "Purpose",
						"sDefaultContent": "n/a",
					}, 
					], 
				
				
					"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
						
						if (aData.identity.person.firstName != null) {
							$('td:eq(0)', nRow).html(aData.identity.person.firstName+" "+aData.identity.person.lastName);
						} else {
							$('td:eq(0)', nRow).html(aData.identity.company.companyName);
						}
						
						$('td:eq(1)', nRow).html(aData.identity.lookupDetail.displayName);						
						
						$('td:eq(2)', nRow).html(aData.handoverDate);
						
						$('td:eq(3)', nRow).html(aData.expectedReturnDate);
						
						$('td:eq(4)', nRow).html(aData.actualReturnDate);
						$('td:eq(5)', nRow).html(aData.purpose);
						
						
					},
				
				"sScrollY" : $("#main-content").height() - 235,
	    
	    
	
	});
	 oTable = $('#example').dataTable();
 }

function calculateToExpire(date) {
	var today=new Date();

	date = new Date(date);
	
	var diff=date-today;//unit is milliseconds
	if (diff >0) {
		diff=Math.round(diff/1000/60/60/24); //contains days passed since date
	} else {
		diff = 0;
	}
	return diff;
}
	
	
	




var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		
		var personId=Number($('#personId').val());
		var companyId=Number($('#companyId').val());
	
			window.open('<%=request.getContextPath()%>/document_handover_report_XLS.action?fromDate='+fromDate+'&toDate='+toDate+'&selectedEmployeeJobId='+personId+'&selectedCompanyId='+companyId,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	

			return false;
	
	});
	
 	$(".print-call").click(function(){ 
		 
 		var personId=Number($('#personId').val());
 		var companyId=Number($('#companyId').val());
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/document_handover_report_printout.action?fromDate='+fromDate+'&toDate='+toDate+'&selectedEmployeeJobId='+personId+'&selectedCompanyId='+companyId,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;			
		 
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		
 		
				
 		var personId=Number($('#personId').val());
 		var companyId=Number($('#companyId').val());
			 //alert(fromDate + fromDate);
				window.open('<%=request.getContextPath()%>/document_handover_report_PDF.action?fromDate='+fromDate+'&toDate='+toDate+'&selectedEmployeeJobId='+personId+'&selectedCompanyId='+companyId,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
	
				
				return false;
	 		
		});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			getData();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			getData();
		}

	});
	
	$('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:600,
		bgiframe: false,
		modal: true 
	});
	
	
$('#employeepopup').click(function(){
         $('.ui-dialog-titlebar').remove();  
         $('#common-popup').dialog('open');
         var rowId=-1; 
         var personTypes="ALL";
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/common_person_list.action",
			data : {
				id : rowId,
				personTypes : personTypes
			},
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('.common-result').html(result);
				return false;
			},
			error : function(result) {
				$('.common-result').html(result);
			}
		});

	});

	function personPopupResult(personId, personName, commonParam) {
		$('#personName').val(personName);
		$('#personId').val(personId);
		getData();
	}
	
	$('#companyId').combobox({
		selected : function(event, ui) {
			getData();
		}
	});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>document
			handover report

		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr>
					<td class="width10">
						<label  style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /> : </label>
					</td>
					<td class="width40">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td class="width10"><label >Company : </label></td>
					<td class="width40">
						 <select id="companyId"
							style="width: 200px;">
							<option value="">Select</option>
							<c:forEach var="company" items="${COMPANIES}">
								<option value="${company.companyId}">${company.companyName}</option>
							</c:forEach>
	
						</select>
					</td>
				</tr>
				<tr>
					<td class="width10">
						<label  style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /> : </label>
					</td>
					<td class="width40">
						<input type="text"
							name="toDate" class="width60 toDate" id="endPicker"
							readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td class="width10">
						<label>Person : </label>
					</td>
					<td class="width40">
						<input type="text" readonly="readonly" id="personName"
							class="width80"></input> <span class="button" id="employee"
							style="top: 0px;"> <a
							style="cursor: pointer; padding: 0 0 4px 21px; margin-left: 22px;"
							id="employeepopup"
							class="btn ui-state-default ui-corner-all width100"> <span
								class="ui-icon ui-icon-newwin"> </span>
							</a>
						</span> <input type="hidden" id="personId" class="personId" />
					</td>
				</tr>
			</table>

			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>