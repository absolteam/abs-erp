<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>

<style>
.container {float: left;padding: 1px;}
.container select{width: 33em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 
</style> 

<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable=null; var selectRow=""; var aData="";var aSelected = [];var payrollId=0; 
var requestStatus="";
var status="";
$(document).ready(function(){
	$jquery("#POAYROLLFORM").validationEngine('attach');
	leaveHistoryCall();
	
	
     /* Click event handler */
     $('#Payroll tbody tr').live('click', function () {
    	 $('#page-error').hide();
     	  if ( $(this).hasClass('row_selected') ) {
     		  $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];
         }
         else {
             oTable.$('tr.row_selected').removeClass('row_selected');
             $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];

         }
     });
     
    
     
     
     $('.print-call').click(function(){
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0){
    		 window.open('<%=request.getContextPath()%>/get_payslip_print.action?payrollId='+payrollId,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;	
    	 }else{
    		 $('#page-error').hide().html("Select any one record").slideDown(1000); 
    	 }
    	 return false;
 	});
     
     
     $(".pdf-download-call").click(function(){ 
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0){
    		 
    		 window.open('<%=request.getContextPath()%>/downloadPayslip.action?recordId='
		 		 		+ payrollId+'&approvalFlag=N&format=PDF',
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
    	 }else{
    		 $('#page-error').hide().html("Select any one record").slideDown(1000); 
    	 }
    	 return false;
 	});
});

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}

function checkDataTableExsist(){
	oTable = $('#Payroll').dataTable();
	oTable.fnDestroy();
	$('#Payroll').remove();
	$('#gridDiv').html("<table class='display' id='Payroll'></table>");
	leaveHistoryCall();
}
function leaveHistoryCall(){
	$('#page-error').hide();
	$('#Payroll').dataTable({ 
		"sAjaxSource": "payroll_execution_listing_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Payroll ID', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.employee"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.company"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.department"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.location"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.payroll.periodfrom"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.periodend"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.paymonth"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.netpay"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.executeddate"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.salarystatus"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.payroll.requeststatus"/>'},
			
		],
		"sScrollY": $("#main-content").height() - 350,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Payroll').dataTable();
}
function clear_form_elements() {
	$('#page-error').hide();
	$('#POAYROLLFORM').each(function(){
	    this.reset();
	});
}
</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Salary Details</div>
			 
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Payroll"></table>
			</div>
	 	</div>		
	</div>	
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>