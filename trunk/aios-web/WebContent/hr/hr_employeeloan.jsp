<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var employeeLoanId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#EmployeeLoan tbody tr').live('click', function() {
		if (!$('#EmployeeLoan tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					employeeLoanId=aData.employeeLoanId;
	
					
				}
			}
		}
	});
	

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/employee_loan_add.action",
		data: {employeeLoanId : 0,recordId:0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
		if(employeeLoanId == null || employeeLoanId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			employeeLoanId = Number(employeeLoanId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/employee_loan_add.action",  
	     	data: {employeeLoanId:employeeLoanId,recordId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(employeeLoanId == null || employeeLoanId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			employeeLoanId = Number(employeeLoanId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/employee_loan_delete.action",  
	     	data: {employeeLoanId:employeeLoanId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 listCall();
					 $('#success_message').hide().html("Information Deleted Successfully.").slideDown();
				 }
				 else{
					 $('#page-error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

$("#print").click(function() {  
	if(employeeLoanId>0){	 
		window.open('employee_loan_printout.action?employeeLoanId='+ employeeLoanId+ '&format=HTML', '',
					'width=800,height=800,scrollbars=yes,left=100px');
	}
	else{
		alert("Please select a record to print.");
		return false;
	}
});

});


function listCall(){
	<%-- $.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/employee_loan_list.action",  
     	async: false,
		dataType: "html",
		cache: false,
     	success: function(response)
	     	{  
				alert(response);
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
	});     --%>
	
	$('#EmployeeLoan').dataTable().fnDestroy();
	oTable = $('#EmployeeLoan')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sScrollY": "300", 
				"sAjaxSource" : "<%=request.getContextPath()%>/employee_loan_list.action",
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
				},
				"aoColumns" : [ {
					"mDataProp" : "employeeName"
				}, {
					"mDataProp" : "requestedDateView"
				}, {
					"mDataProp" : "sanctionedAmount"
				}, {
					"mDataProp" : "financeTypeView"
				}, {
					"mDataProp" : "repaymentTypeView"
				} 
				, {
					"mDataProp" : "paymentFrequencyView"
				}, {
					"mDataProp" : "dueStartDateView"
				} ]

	}); 
	
	/*  oTable=$('#EmployeeLoan').dataTable({ 
		"sAjaxSource": "employee_loan_list.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'EmployeeLoan Id', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Amount',"sWidth": "200px"},
			{ "sTitle": 'Date'},
			{ "sTitle": 'No. Of Due'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	  */
		//Json Grid
		//init datatable
		
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Employee Loan</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div id="success_message" class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			 <div id="gridDiv">
				<table class="display" id="EmployeeLoan">
					<thead>
						<tr>
							<th>Employee</th>
							<th>Date</th>
							<th>Amount</th>
							<th>Finance Type</th>
							<th>Repayment Type</th>
							<th>Frequency</th>
							<th>Due Starts</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
			<!-- <div id="gridDiv">
				<table class="display" id=EmployeeLoan></table>
			</div>	 -->
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Print</span></div>
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
