
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var jobAssignmentId =0;
var oTable;

var fromDate = null;
var toDate = null;


populateDatatable();
	
function populateDatatable(){

	$('#example').dataTable({ 
		"sAjaxSource": 'get_employee_reconciliation_list_json.action?fromDate='+fromDate+'&toDate='+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'RecordId', "bVisible": false},
			{ "sTitle": 'PersonId', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Designation'},
			{ "sTitle": 'Company'},
			{ "sTitle": 'Department'},
			{ "sTitle": 'Location'},
			{ "sTitle": 'Leave Type'},
			{ "sTitle": 'Provided Days'},
			{ "sTitle": 'Eligible Days'},
			{ "sTitle": 'Taken Days'},
			{ "sTitle": 'Available Days'},

		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			/* if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			} */
		}
	});
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();
}

$('#example tbody tr').live('click', function () {
 	/*  $('#page-error').hide(); */
  	  if ( $(this).hasClass('row_selected') ) {
  		  $(this).addClass('row_selected');
          aData =oTable.fnGetData( this );
          jobAssignmentId=aData[0];
          /* leaveStatus=aData[9];
          requestStatus=aData[10]; */
      }
      else {
          oTable.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
          aData =oTable.fnGetData( this );
          jobAssignmentId=aData[0];
       /*    leaveStatus=aData[9];
          requestStatus=aData[10]; */

      }
  });



var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		
		window.open('<%=request.getContextPath()%>/print_leave_reconciliation_report_XLS.action?selectedEmployeeJobId='+jobAssignmentId, +'_blank',
		'width=800,height=700,scrollbars=yes,left=100px,top=2px');
		
		return false;
	});
 	
 	$(".print-call").click(function(){ 
	 	
 		window.open('<%=request.getContextPath()%>/print_leave_reconciliation_report.action?selectedEmployeeJobId='+jobAssignmentId,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
	$(".pdf-download-call").click(function(){ 
		
		window.open('<%=request.getContextPath()%>/print_leave_reconciliation_report_PDF.action?selectedEmployeeJobId='+jobAssignmentId,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Leave Reconciliation

		</div>
		<div class="portlet-content">
			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /> :</label> <input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /> :</label> <input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>

			</div>

			<div class="width45 float-right" style="padding: 5px;">
					<div>
						<label class="width30" for="supplierName">Leave Type :</label>
						<select id="supplierId" onchange="changeEventHandler()" class="width60">
							<option value="">Select</option>
							<option value="Annual Leave">Annual Leave</option>
							<option value="Sick Leave">Sick Leave</option>
							<option value="Casual Leave">Casual Leave</option>
							<option value="Paid Leave">Paid Leave</option>
							<option value="UnPaid Leave">Un Paid Leave</option>
							
						
						</select>		
					</div>  
			</div>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>