<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<style>
.container {float: left;padding: 1px;}
.container select{width: 33em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 
</style> 

<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var oTable=null; var selectRow=""; var aData="";var aSelected = [];var payrollId=0;; 
var requestStatus="";
var status="";
$(document).ready(function(){
	payrollCall();
     
     
     $('#edit').live('click',function(){
    	 $('#page-error').hide();
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/get_edit_payroll.action", 
				 	async: false, 
				 	data:{ 
				 		payrollId:payrollId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#main-wrapper").html(result);
					},
					error:function(result){  
						$("#main-wrapper").html(result);
					}
				});  
    	
    	 return false;
 	});
     $('#delete').live('click',function(){
    	 $('#page-error').hide();
    	 var cnfrm = confirm('Selected record will be deleted permanently');
 		 if(!cnfrm)
 			return false;
	    	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/delete_payroll.action", 
				 	async: false, 
				 	data:{ 
				 		payrollId:payrollId
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							checkDataTableExsist();
						 }else{
							 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					}
				});  
    	
    	 return false;
 	});
   
     
     $('#delete-all').live('click',function(){
    	 $('#page-error').hide();
    	 var cnfrm = confirm('Selected record will be deleted permanently');
 		 if(!cnfrm)
 			return false;
 		 
	     var allVals = [];
		   $('.payroll-group').each(function() {
			   if($(this).attr("checked"))
		      	 allVals.push($(this).val());
		     });
		   var stringArray="";
		   for(var j=0;j<allVals.length;j++){ 
			   if(j==0)
				   stringArray+=allVals[j];
			   else
				   stringArray+=","+allVals[j];
			} 
		   $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/delete_payroll_all.action", 
			 	async: false, 
			 	data:{ 
			 		payLineDetail:stringArray
			 	},
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						checkDataTableExsist();
					 }else{
						 $('#page-error').hide().html("Transaction goes failure").slideDown(1000);
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Transaction goes failure").slideDown(1000);
				}
			});  
		   return false;
	  });
     
     $(".low input[type='button']").click(function(){
    	    var arr = $(this).attr("name").split("2");
    	    var from = arr[0];
    	    var to = arr[1];
    	    $("#" + from + " option:selected").each(function(){
    	      $("#" + to).append($(this).clone());
    	      $(this).remove();
    	    });
    }); 
     
     
     
     
     $('#payslip').click(function(){
    	 $('#page-error').hide();
    	 if(payrollId!=null && payrollId!=0){
    		 var win1=window.open('getPayslip.action?format=HTML&recordId='
		 		 		+ payrollId+'&approvalFlag=Y','WorkFlowApprove','width=800,height=800,scrollbars=yes,left=100px');
    	 }else{
    		 $('#page-error').hide().html("Select any one record").slideDown(1000); 
    	 }
    	 return false;
 	});
     
     $('#view').click(function(){
  		
  		if(payrollId!=null){
  	    	 $.ajax({
  					type:"POST",
  					url:"<%=request.getContextPath()%>/get_payroll_view.action", 
  				 	async: false, 
  				 	data:{ 
  				 		payrollId:payrollId,viewRedirect:"PayrollExecution"
  				 	},
  				    dataType: "html",
  				    cache: false,
  					success:function(output){
  						var myWindow=window.open('printing/sys_empty_template.jsp','_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
						myWindow.document.write(output);
						myWindow.focus();
						//myWindow.location.reload();
  					},
  					error:function(result){  
  						var myWindow=window.open('printing/sys_empty_template.jsp','_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
						myWindow.document.write(output);
						myWindow.focus();
						//myWindow.location.reload();
  					}
  				});  
     	 	}
  		return false;
  	});
     
    
     /* Click event handler */
     $('#Payroll tbody tr').live('click', function () {
    	 $('#page-error').hide();
     	  if ( $(this).hasClass('row_selected') ) {
     		  $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];
         }
         else {
             oTable.$('tr.row_selected').removeClass('row_selected');
             $(this).addClass('row_selected');
             aData =oTable.fnGetData( this );
             payrollId=aData[0];
             status=aData[9];
             requestStatus=aData[10];

         }
     });
     
     
     $('.payroll-group').click(function() {
    	 var flag=false;
	     $('.payroll-group').each(function() {
			   if($(this).attr("checked"))
		      	 flag=true;
		    });
	     if(flag){
	    	 $('#delete-all').show();
	    	 $('#delete').hide();
	     }else{
	    	 $('#delete-all').hide();
	    	 $('#delete').show();
	     }
     });
});

function getRowId(id){
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	} 
}

function checkDataTableExsist(){
	oTable = $('#Payroll').dataTable();
	oTable.fnDestroy();
	$('#Payroll').remove();
	$('#gridDiv').html("<table class='display' id='Payroll'></table>");
	payrollCall();
}
function payrollCall(){
	$('#page-error').hide();
	$('#Payroll').dataTable({ 
		"sAjaxSource": "payroll_adjustment_listing_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Payroll ID', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.employee"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.company"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.attendance.department"/>'},
			{ "sTitle": '<fmt:message key="hr.attendance.location"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.payroll.periodfrom"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.periodend"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.paymonth"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.netpay"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.executeddate"/>'},
			{ "sTitle": '<fmt:message key="hr.payroll.salarystatus"/>', "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.payroll.requeststatus"/>'},
			{ "sTitle": 'Choose To Delete',"mDataProp": function(aData) {
				return '<td><input type="checkbox" class="payroll-group" name="payroll-group[]" value="'+aData[0]+'"/></td>';

			}
			}
		],
		"sScrollY": $("#main-content").height() - 225,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Payroll').dataTable();
}

</script>

 
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Pay Adjustment</div>
			 
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		 
		</div>
		
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="Payroll"></table>
			</div>
	 	</div>		
		<div class="vmenu">
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Payslip</span></div>
					<div class="first_li"><span id="edit">Edit</span></div>
					<div class="first_li"><span id="delete">Delete</span></div>
			
		</div> 
		<div class="float-right buttons ui-widget-content ui-corner-all">
					<div class="portlet-header ui-widget-header float-right delete-all" id="delete-all">Delete</div>
					<div class="portlet-header ui-widget-header float-right delete" id="delete" style="display:none;">Delete</div>
					<div class="portlet-header ui-widget-header float-right payslip" id="payslip">Payslip</div>
					<div class="portlet-header ui-widget-header float-right view" id="view">View</div>
					<div class="portlet-header ui-widget-header float-right edit" id="edit">Edit</div>
					
		</div>
	</div>	
</div>
