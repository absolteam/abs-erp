
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var jobAssignmentId =0;
var oTable;

var fromDate = null;
var toDate = null;



	
function populateDatatable(){
	var jobAssignmentId=($('#swipeId').val());

	$('#example').dataTable({ 
		"sAjaxSource": 'get_attendance_swipe_list_json.action?fromDate='+fromDate+'&toDate='+toDate+'&selectedEmployeeJobId='+jobAssignmentId,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'RecordId', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Designation'},
			{ "sTitle": 'Swipe Id'},
			{ "sTitle": 'Attendance Date'},
			{ "sTitle": 'Swipe Type(In/Out)'},
			{ "sTitle": 'Time'},
			{ "sTitle": 'Door Number'},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			/* if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			} */
		}
	});
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();
}


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		
		window.open('<%=request.getContextPath()%>/print_attendance_swipe_report_XLS.action', +'_blank',
		'width=800,height=700,scrollbars=yes,left=100px,top=2px');
		
		return false;
	});
 	
 	$(".print-call").click(function(){ 
 		var jobAssignmentId=($('#swipeId').val());
 		window.open('<%=request.getContextPath()%>/print_attendance_swipe_report.action?fromDate='+fromDate+'&toDate='+toDate+'&selectedEmployeeJobId='+jobAssignmentId,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
	$(".pdf-download-call").click(function(){ 
		
		window.open('<%=request.getContextPath()%>/print_attendance_swipe_report_PDF.action',
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
		}

	});
	
	
	$("#Go").click(function(){
		populateDatatable();
		return false;
	}); 
	$('#reset').click(function(){
		clear_form_elements();
	});
	  
	$('#employeepopup').click(function(){
	    $('.ui-dialog-titlebar').remove();  
	    $('#job-common-popup').dialog('open');
	    var rowId=-1; 
	    var personTypes="ALL";
	       $.ajax({
	          type:"POST",
	          url:"<%=request.getContextPath()%>/get_employee_for_leavemanagment.action",
	          
	          async: false,
	          dataType: "html",
	          cache: false,
	          success:function(result){
	               $('.common-result').html(result);
	               return false;
	          },
	          error:function(result){
	               $('.common-result').html(result);
	          }
	      });
	       return false;
	}); 

	$('#job-common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800,
		height:600,
		bgiframe: false,
		modal: true 
	});

	function leaveTypeCall(d){
		
	}
	function clear_form_elements() {
		$('#page-error').hide();
		$('#ATT').each(function() {
			this.reset();
		});
	}
</script>
<div id="main-content">

<input type="hidden" id="personId"/>
	<input type="hidden" id="jobAssignmentId"/>
	<input type="hidden" id="gradeName"/>
	<input type="hidden" id="designationName"/>
	<input type="hidden" id="companyName"/>
	<input type="hidden" id="departmentName"/>
	<input type="hidden" id=locationName/>
	<input type="hidden" id="swipeId"/>
	
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Attendance Swipe Report
		</div>
		<div class="portlet-content">
			<form id="att">
			
			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> <input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /></label> <input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>

			</div>
			
			<div class="width45 float-right" style="padding: 5px;">
			
			
			
				<div class="width100 float-left">
	      	  		<label class="width20 float-left">Employee</label>
					<input type="text" id="employeeName" class="width50 float-left"/>
					
					<span class="button" class="width5 float-left" id="employee"  style="position: relative; top:4px;">
						<a style="cursor: pointer; padding: 0 0 4px 21px; margin-left:60px;" id="employeepopup" class="btn ui-state-default ui-corner-all width100" > 
							<span class="ui-icon ui-icon-newwin">
							</span> 
						</a>
					</span> 
					
				</div>
						
			
			</div>

			<div class="float-right buttons ui-widget-content ui-corner-all">
				<div class="portlet-header ui-widget-header float-right"
					id="reset">Reset</div>
				<div class="portlet-header ui-widget-header float-right" id="Go">Go</div>
			</div>
			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>

			</form>
		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>