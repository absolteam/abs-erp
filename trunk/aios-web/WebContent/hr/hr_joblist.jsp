<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var firstName;
var id;
var personId;
var pageName;
var personTypes;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 

$(function(){ 
	
	jobGridLoad();
	
	$('.formError').remove();
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){ 
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
	} 


/* Click event handler */
$('#Job tbody tr').live('click', function () { 
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(this).addClass('row_selected');
        aData =oTable.fnGetData( this );
        s=aData[0];
    }
});

$('#pageName').change(function(){
	var optionselected=$('#pageName option:selected').val();
	if(optionselected=="personal_details"){
		$('#add_person').show();
		
	}else{
		$('#add_person').hide();
		
	}
			
});

$('#firstName').keyup(function(){
       if($('#firstName').val()!=null){
           $('.otherErr ').hide();
       }
});

$('#view').click(function(){
	$('#loading').fadeIn();
		window.open('.action?format=HTML,width=800,height=800,scrollbars=yes,left=100px');
	$('#loading').fadeOut();
	return true; 
});

$('#add').click(function(){
	$.ajax({
		type:"GET",
		url:"<%=request.getContextPath()%>/add_job.action",
		data: {jobId:0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false;
});
$('#edit').click(function(){
	if(s == null || s=="" || s==0)
	{
		alert("Please select one row to edit");
		return false;
	}
	else
	{			
		var recId2 = Number(s);
		
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/add_job.action",  
     	data: {jobId:recId2},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	}
	}); 
	}
	return false;
});
$('#delete').click(function(){
	if(s == null || s=="" || s==0)
	{
		alert("Please select one row to Delete");
		return false;
	}
	else
	{	
		var recId2 = Number(s);
		var cnfrm = confirm('Selected record will be deleted permanently');
		if(!cnfrm)
			return false;
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/job_delete.action",  
			data: {jobId:recId2}, 
			async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result); 
	     		if($(".tempresult").html().trim()=="SUCCESS"){
		     		$('.success').hide().html("Job Deleted Successfully.").slideDown();
		     		$('.success').delay(2000).slideUp(1000);
		     		checkDataTableExsist();
		     	}else{
		     		$('.error').hide().html(result).slideDown(); 
		     		$('.error').delay(2000).slideUp(2000);
			    }
				$('#loading').fadeOut(); 
	     	}
		}); 
		return false;
	}
	return false;
});

});
function checkDataTableExsist(){
	oTable = $('#Job').dataTable();
	oTable.fnDestroy();
	$('#Job').remove();
	$('#gridDiv').html("<table class='display' id='Job'></table>");
	jobGridLoad();
}
function jobGridLoad(){
	$('#Job').dataTable({ 
		"sAjaxSource": "job_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'JOB ID', "bVisible": false},
			{ "sTitle": 'Job No.',"sWidth": "75px"},
			{ "sTitle": 'Template Name',"sWidth": "150px"},
			{ "sTitle": 'Designation',"sWidth": "150px"},
			{ "sTitle": 'Grade',"sWidth": "150px"},
			{ "sTitle": '<fmt:message key="hr.job.employees"/>'},
			
		],
		"sScrollY": $("#main-content").height() - 230,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#Job').dataTable();
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Job Template</div> 
		<div class="portlet-content">
		<div class="tempresult" style="display:none;"></div>
		
		<div>
			 <div class="error response-msg ui-corner-all" style="display:none;"></div>
		  	  <div id="success_message" class="success response-msg ui-corner-all" style="display:none;"></div> 
    	</div>
		<!--<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width80"> 
			<div class="float-left width80" style="margin-top:10px;margin-left:5px;">
				<label style="font-weight:bold;"><fmt:message key="hr.personaldetails.label.personname"/></label><input type="text" name="firstName" id="firstName"/>
				<label style="font-weight:bold;"><fmt:message key="person.label.persontypes"/></label>
				<select id="personType" class="width20" name="personType">
                    <option value="">- Select -</option>
                    <option value="0">In Active</option>
                    <option value="1">Active</option>
                </select>
			</div>
			<div class="portlet-header ui-widget-header float-right" id="cancel"  style="cursor:pointer;"><fmt:message key="hr.personaldetails.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right" id="search"  style="cursor:pointer;"><fmt:message key="hr.personaldetails.button.search"/></div>			
 		</div>
	-->
	
	<div id="rightclickarea">
		<div id="gridDiv">
			<table class="display" id="Job"></table>
		</div>
 	</div>		
	<div class="vmenu">
	    <div class="first_li"><span>Add</span></div>
		<div class="sep_li"></div>		 
		<div class="first_li"><span>Edit</span></div>
		<div class="first_li"><span>Delete</span></div>
	</div>
	
	<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="common.button.delete"/></div>
		<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="common.button.edit"/></div>
		<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="common.button.add"/></div>
		
	</div>
	<!--<div id="add_person" style="margin-top:17px;float:right;"> 
			<select  name="createApplicant" class="validate[required]" id="pageName">
				<option value="personal_details">Personal Details</option>
				<option value="health_details">Health Details</option>
				<option value="addresses_details">Address Details</option>
				<option value="driving_details">Driving License Details</option>
			</select> 
		</div>
--></div>
</div>
</div>
