
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var jobAssignmentId =0;
var oTable;

var fromDate = null;
var toDate = null;
 
if(typeof($('#common-popup')!="undefined")){  
	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
 }

leaveHistoryCall();
	
function leaveHistoryCall(){

	$('#example').dataTable({ 
		"sAjaxSource": 'get_employee_leave_list_json.action?fromDate='+fromDate+'&toDate='+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'RecordId', "bVisible": false},
			{ "sTitle": 'PersonId', "bVisible": false},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Designation', "bVisible": false},
			{ "sTitle": 'Grade',  "bVisible": false },
			{ "sTitle": 'Company' },
			{ "sTitle": 'Department'},
			{ "sTitle": 'Location'},
			{ "sTitle": 'Swipe Id', "bVisible": false},
			{ "sTitle": 'JobId', "bVisible": false},
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			/* if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			} */
		}
	});
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();
}

$('#example tbody tr').live('click', function () {
 	/*  $('#page-error').hide(); */
  	  if ( $(this).hasClass('row_selected') ) {
  		  $(this).addClass('row_selected');
          aData =oTable.fnGetData( this );
          jobAssignmentId=aData[0];
          /* leaveStatus=aData[9];
          requestStatus=aData[10]; */
      }
      else {
          oTable.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
          aData =oTable.fnGetData( this );
          jobAssignmentId=aData[0];
       /*    leaveStatus=aData[9];
          requestStatus=aData[10]; */

      }
  });
  
  
$('#employeepopup').click(function(){
    $('.ui-dialog-titlebar').remove();  
    $('#job-common-popup').dialog('open');
    var rowId=-1; 
    var personTypes="ALL";
       $.ajax({
          type:"POST",
          url:"<%=request.getContextPath()%>/get_employee_for_leavemanagment.action",
          
          async: false,
          dataType: "html",
          cache: false,
          success:function(result){
               $('.common-result').html(result);
               return false;
          },
          error:function(result){
               $('.common-result').html(result);
          }
      });
       return false;
}); 

$('#job-common-popup').dialog({
	autoOpen: false,
	minwidth: 'auto',
	width:800,
	height:600,
	bgiframe: false,
	modal: true 
});

function leaveTypeCall(personId){
	return false;
}



var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		//var jobAssignmentId=Number($('#jobAssignmentId').val());
		if(jobAssignmentId>0) {
			window.open('<%=request.getContextPath()%>/get_hr_employee_leave_history_xls.action?selectedEmployeeJobId='+jobAssignmentId, +'_blank',
							'width=800,height=700,scrollbars=yes,left=100px,top=2px');

			
	  } else {
		  window.open('<%=request.getContextPath()%>/get_hr_all_employee_leave_history_xls.action','_blank',
			'width=800,height=700,scrollbars=yes,left=100px,top=2px');
		 }
		return false;
	});
 	
 	$(".print-call").click(function(){ 
 		//var jobAssignmentId=Number($('#jobAssignmentId').val());
	 	if(jobAssignmentId>0) {
			window.open('<%=request.getContextPath()%>/get_hr_employee_leave_history_print.action?selectedEmployeeJobId='+jobAssignmentId,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	 	}
	 	else {
	 		window.open('<%=request.getContextPath()%>/get_hr_all_employee_leave_history_print.action','_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	 	}
		return false;
		
	});
 	
	$(".pdf-download-call").click(function(){ 
		//var jobAssignmentId=Number($('#jobAssignmentId').val());
	 	if(jobAssignmentId>0) {
			window.open('<%=request.getContextPath()%>/get_hr_employee_leave_history_pdf.action?selectedEmployeeJobId='+jobAssignmentId,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	 	}
	 	else {
	 		window.open('<%=request.getContextPath()%>/get_hr_all_employee_leave_history_pdf.action','_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
	 	}
		return false;
		
	});
 	

	/* 	$('#example tbody tr').live('click', function() {

	 if ($(this).hasClass('row_selected')) {
	 $(this).addClass('row_selected');
	 aData = oTable.fnGetData(this);

	 purchaseId = aData[0];
	 } else {
	 oTable.$('tr.row_selected').removeClass('row_selected');
	 $(this).addClass('row_selected');
	 aData = oTable.fnGetData(this);
	 productId = aData[0];
	 }
	
	 }); */

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			leaveHistoryCall();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			leaveHistoryCall();
		}

	});
</script>
<div id="main-content">


	<input type="hidden" id="personId"/>
	<input type="hidden" id="jobAssignmentId"/>
	<input type="hidden" id="gradeName"/>
	<input type="hidden" id="designationName"/>
	<input type="hidden" id="companyName"/>
	<input type="hidden" id="departmentName"/>
	<input type="hidden" id=locationName/>
	<input type="hidden" id="swipeId"/>

	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>employee
			leave history

		</div>
		<div class="portlet-content">





			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width20" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /> : </label> <input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>
				<div>
					<label class="width20" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /> : </label> <input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
				</div>

			</div>
			
			

			<div class="width50 float-right" style="padding: 5px;">
			
			
			
				<div>
	      	  		<label class="width20">Employee : </label>
					<input type="text" readonly="readonly" id="employeeName" class="width50"></input>
					
					<span class="button" id="employee"  style="position: relative;">
						<a style="cursor: pointer; padding: 0 0 4px 21px; margin-left:20px;" id="employeepopup" class="btn ui-state-default ui-corner-all width100" > 
							<span class="ui-icon ui-icon-newwin">
							</span> 
						</a>
					</span> 
					
				</div>
						
			
			</div>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>