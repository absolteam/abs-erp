<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var memoWarningId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#MemoWarning tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        memoWarningId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        memoWarningId=aData[0];
	    }
	});
	$('#MemoWarning tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        memoWarningId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        memoWarningId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/memo_warning_add.action",
		data: {memoWarningId : 0,recordId:0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
		if(memoWarningId == null || memoWarningId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			memoWarningId = Number(memoWarningId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/memo_warning_add.action",  
	     	data: {memoWarningId:memoWarningId,recordId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(memoWarningId == null || memoWarningId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			memoWarningId = Number(memoWarningId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/memo_warning_delete.action",  
	     	data: {memoWarningId:memoWarningId,messageId:0},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

$('#view').click(function(){
	if(memoWarningId == null || memoWarningId == 0)
	{
		alert("Please select one row to view");
		return false;
	}
	else
	{			
		memoWarningId = Number(memoWarningId); 
		

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/memo_warning_view.action",  
     	data: {memoWarningId:memoWarningId,recordId:0},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(data)
     	{  
			$("#main-wrapper").html(data); //gridDiv main-wrapper 
     	},
     	 error:function(result)
         {
             $("#main-wrapper").html(result);
         }
	}); 
	return false;
	}
	return false;  
});

$("#print").click(function() {  
	   
	if(memoWarningId>0){	 
		window.open('memo-warning-print.action?memoWarningId='+ memoWarningId+ '&format=HTML', '',
					'width=800,height=800,scrollbars=yes,left=100px');
	}
	else{
		alert("Please select a record to print.");
		return false;
	}
});


});

function alreadyListCall(){
	oTable = $('#MemoWarning').dataTable();
	oTable.fnDestroy();
	$('#MemoWarning').remove();
	$('#gridDiv').html("<table class='display' id='MemoWarning'></table>");
	listCall();
}
function listCall(){
	$('#MemoWarning').dataTable({ 
		"sAjaxSource": "memo_warning_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'MemoWarningId', "bVisible": false},
			{ "sTitle": 'Title            '},
			{ "sTitle": 'Type'},
			{ "sTitle": 'Sub Type'},
			{ "sTitle": 'Reference Number'},
			{ "sTitle": 'Created By'},
			{ "sTitle": 'Created Date'},
			{ "sTitle": 'Status'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[3, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#MemoWarning').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Memo & Warning</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="MemoWarning"></table>
			</div>	
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Print</span></div>
			<div class="first_li"><span>View</span></div>
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="print">Print</div>
		<div class="portlet-header ui-widget-header float-right" id="view">View</div>
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>

</div>
