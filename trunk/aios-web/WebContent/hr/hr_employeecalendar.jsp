<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var employeeCalendarId=null;
$(document).ready(function (){ 
	 
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#EmployeeCalendar tbody tr').live('click', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        employeeCalendarId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        employeeCalendarId=aData[0];
	    }
	});
	$('#EmployeeCalendar tbody tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        employeeCalendarId=aData[0];
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        employeeCalendarId=aData[0];
	    }
		
	});

$('#add').click(function(){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/employee_calendar_add.action",
		data: {employeeCalendarId : 0}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#main-wrapper").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
	 $('.response-msg').hide(); 
		if(employeeCalendarId == null || employeeCalendarId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			employeeCalendarId = Number(employeeCalendarId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/employee_calendar_add.action",  
	     	data: {employeeCalendarId:employeeCalendarId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#main-wrapper").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#main-wrapper").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
	 $('.response-msg').hide(); 
		if(employeeCalendarId == null || employeeCalendarId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			employeeCalendarId = Number(employeeCalendarId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/delete_employee_calendar.action",  
	     	data: {employeeCalendarId:employeeCalendarId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 alreadyListCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
					 $('.success').delay(2000).slideUp(1000);
					 employeeCalendarId=null;
				 }
				 else{
					 $('#page-error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('#page-error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

});

function alreadyListCall(){
	oTable = $('#EmployeeCalendar').dataTable();
	oTable.fnDestroy();
	$('#EmployeeCalendar').remove();
	$('#gridDiv').html("<table class='display' id='EmployeeCalendar'></table>");
	listCall();
}
function listCall(){
	$('#EmployeeCalendar').dataTable({ 
		"sAjaxSource": "employee_calendar_list_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'Emplyee Calendar ID', "bVisible": false},
			{ "sTitle": 'Calendar Number'},
			{ "sTitle": 'Calendar Name'},
			{ "sTitle": 'Location'},
			{ "sTitle": 'Employee'},
			{ "sTitle": 'Weekend'},
			
		],
		"sScrollY": $("#main-content").height() - 250,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#EmployeeCalendar').dataTable();
}
</script>

<div id="main-content">
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Holiday & Work Off</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	
    	<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="EmployeeCalendar"></table>
			</div>	
		</div>		
		<div class="vmenu">
 	       	<div class="first_li"><span>Add</span></div>
			<div class="sep_li"></div>	 
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
	
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
