<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var questionBankId=null;var questionType=null;var questionPattern=null;
$(document).ready(function (){ 
	
	
	listCall();
	
	
	/* Click event handler */
	$('#QuestionBank tbody tr').live('click', function() {
		if (!$('#QuestionBank tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					questionBankId=aData.questionBankId;
					questionType=aData.questionType;
					questionPattern=aData.questionPatternView;
				}
			}
		}
	});
	
	$('#QuestionBank tbody tr').live('dblclick', function() {
		if (!$('#QuestionBank tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					questionBankId=aData.questionBankId;
					questionType=aData.questionType;
					questionPattern=aData.questionPatternView;
					commonParam=aData;//+"@&"+description;
				}
			}
			 questionPopupResult(questionBankId,questionType,commonParam);
				$('#common-popup').dialog("close");
		}
		
	});
	
	$('#popup-grid-close').click(function () { 
		$('#common-popup').dialog("close");
	});
	
});


function listCall(){
	
	
	$('#QuestionBank').dataTable().fnDestroy();
	oTable = $('#QuestionBank')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",
				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sAjaxSource" : "<%=request.getContextPath()%>/common_question_bank_list_json.action",
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								//alert(aData.statusDisplay);
							},
							"aoColumns" : [ {
								"mDataProp" : "questionPatternView"
							}, {
								"mDataProp" : "questionType"
							}, {
								"mDataProp" : "designationName"
							}, {
								"mDataProp" : "gradeName"
							}, {
								"mDataProp" : "description"
							} ]

						});

	}
</script>

<div id="main-content">

	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Question
			Bank
		</div>

		<input type="hidden" name="designationTemp"
			value="${requestScope.designationId}" />
		<div id="rightclickarea">
			<div id="gridDiv">
				<table class="display" id="QuestionBank">
					<thead>
						<tr>
							<th>Question Pattern</th>
							<th>Type</th>
							<th>Designation</th>
							<th>Grade</th>
							<th>Description</th>
						</tr>
					</thead>


				</table>
			</div>

			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="popup-grid-close">
					<fmt:message key="common.button.close" />
				</div>
			</div>
		</div>
	</div>
</div>
