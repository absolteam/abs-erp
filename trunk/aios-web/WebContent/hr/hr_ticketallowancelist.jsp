<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var ticketAllowanceId=null;
$(document).ready(function (){ 
	
	if(typeof($('#job-common-popup')!="undefined")){ 
		$('#job-common-popup').dialog('destroy');		
		$('#job-common-popup').remove(); 
	} 
	if(typeof($('#common-popup')!="undefined")){  
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove(); 
	 }
	$('.formError').remove();
	
	listCall();
	
	
	/* Click event handler */
	$('#TicketAllowance tbody tr').live('click', function() {
		if (!$('#TicketAllowance tbody tr td').hasClass('dataTables_empty')) {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
	
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
	
			}
	
			aData = oTable.fnGetData(this);
			if (aData != undefined) {
				if (aData != null) {
					ticketAllowanceId=aData.ticketAllowanceId;
	
					
				}
			}
		}
	});
	

$('#add').click(function(){
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var transactionDate=$('#transactionDate').val();
	var payPolicy=$('#payPolicy').val();
	var payPeriodTransactionId=$('#payPeriod').val();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/ticket_allowance_add.action",
		data: {ticketAllowanceId : 0,recordId:0,
			jobPayrollElementId:jobPayrollElementId,
			jobAssignmentId:jobAssignmentId,
			transactionDate:transactionDate,
			payPolicy:payPolicy,
			payPeriodTransactionId:payPeriodTransactionId}, 
		async:false,
		dataType:"html",
		cache:false,
		success:function(result){
			$("#allowance-manipulation-div").html(result);
		}
	});	
	return false; 
});

$('#edit').click(function(){
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var transactionDate=$('#transactionDate').val();
	var payPolicy=$('#payPolicy').val();
	var payPeriodTransactionId=$('#payPeriod').val();
		if(ticketAllowanceId == null || ticketAllowanceId == 0)
		{
			alert("Please select one row to edit");
			return false;
		}
		else
		{			
			ticketAllowanceId = Number(ticketAllowanceId); 
			
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/ticket_allowance_add.action",  
	     	data: {
	     		ticketAllowanceId:ticketAllowanceId,recordId:0,
	     		jobPayrollElementId:jobPayrollElementId,
				jobAssignmentId:jobAssignmentId,
				transactionDate:transactionDate,
				payPolicy:payPolicy,
				payPeriodTransactionId:payPeriodTransactionId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(data)
	     	{  
				$("#allowance-manipulation-div").html(data); //gridDiv main-wrapper 
	     	},
	     	 error:function(result)
             {
                 $("#allowance-manipulation-div").html(result);
             }
		}); 
		return false;
		}
		return false;  
});

$('#delete').click(function(){
	 $('#page-error').hide();
		if(ticketAllowanceId == null || ticketAllowanceId == 0)
		{
			alert("Please select one row to delete");
			return false;
		}
		else
		{		
			var cnfrm = confirm('Selected Details will be deleted permanently');
			if(!cnfrm)
				return false;
			ticketAllowanceId = Number(ticketAllowanceId); 
	
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/ticket_allowance_delete.action",  
	     	data: {ticketAllowanceId:ticketAllowanceId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
	     	{  
				$(".tempresult").html(result);
				 var message=$('.tempresult').html(); 
				 if(message.trim()=="SUCCESS"){
					 listCall();
					 $('.success').hide().html("Information Deleted Successfully.").slideDown();
				 }
				 else{
					 $('.error').hide().html(message).slideDown(1000);
					 return false;
				 }
	     	},
	     	error:function(result)
            {
	     		$('.error').hide().html(message).slideDown(1000);
            } 
		}); 
		return false;
		}
		return false; 
});

});


function listCall(){
	var params="id=0";
	var jobPayrollElementId=$('#jobPayrollElementId').val();
	var jobAssignmentId=$('#jobAssignmentId').val();
	var payPolicy=$('#payPolicy').val();
	var transactionDate=$('#transactionDate').val();
	var payPeriodTransactionId=$('#payPeriod').val();

	params+="&jobPayrollElementId="+jobPayrollElementId;
	params+="&jobAssignmentId="+jobAssignmentId;
	params+="&transactionDate="+transactionDate;
	params+="&payPolicy="+payPolicy;
	params+="&payPeriodTransactionId="+payPeriodTransactionId;

	$('#TicketAllowance').dataTable().fnDestroy();
	oTable = $('#TicketAllowance')
	.dataTable(
			{
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",

				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : false,
				"bLengthChange" : false,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				"sAjaxSource" : "<%=request.getContextPath()%>/ticket_allowance_json.action?"+params,
				"fnRowCallback" : function(nRow, aData,
						iDisplayIndex, iDisplayIndexFull) {
					//alert(aData.statusDisplay);
				},
				"aoColumns" : [{
					"mDataProp" : "from"
				},{
					"mDataProp" : "to"
				},{
					"mDataProp" : "airline"
				},{
					"mDataProp" : "agency"
				},{
					"mDataProp" : "numberOfTicket"
				},{
					"mDataProp" : "ticketClass"
				},{
					"mDataProp" : "ticketType"
				}, {
					"mDataProp" : "fromDateView"
				}, {
					"mDataProp" : "toDateView"
				}, {
					"mDataProp" : "amount"
				}, {
					"mDataProp" : "isFinanceView"
				} ]

	}); 
	
}
</script>
<div id="allowance-manipulation-div">
<div>
 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Ticket Allowance</div>
			
		<div class="portlet-content">
		 <div id="page-error" class="response-msg error ui-corner-all" style="display:none;"></div>  
		 <div class="success response-msg ui-corner-all" style="display: none;"></div>
			  	<div class="tempresult" style="display:none;"></div>
    	<input type="hidden" value="${jobPayrollElementId}" id="jobPayrollElementId">
    	<div id="rightclickarea">
			 <div id="gridDiv">
				<table class="display" id="TicketAllowance">
					<thead>
						<tr>
							<th>Source</th>
							<th>Destination</th>
							<th>Airline</th>
							<th>Agency</th>
							<th>No.of Ticket</th>
							<th>Class</th>
							<th>Ticket Type</th>
							<th>From Date</th>
							<th>To Date</th>
							<th>Amount</th>
							<th>Finance Impact</th>
						</tr>
					</thead>
					

				</table>
			</div>	 
			
		</div>		
		<div class="vmenu">
			<div class="first_li"><span>Delete</span></div>
			<div class="first_li"><span>Edit</span></div>
			<div class="first_li"><span>Add</span></div>
		</div>
				
		<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right" id="delete">Delete</div>
		<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
 		<div class="portlet-header ui-widget-header float-right" id="add">Add</div> 
		
	</div>
</div>
</div>
</div>
</div>