<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="main-content" class="hr_main_content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Basic Pay</div>
		<div class="portlet-content" style="padding:0!important;">
				<div id="page-error" class="response-msg error ui-corner-all width95" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
		  <form id="Basic_Form" class="" name="Basic_Form" method="post">
				<div class="width100 float-left" id="hrm">
					<label>Basic</label>
					<label>${basicAmount}</label>
				</div>
		  </form>
		</div>
	</div>
</div>