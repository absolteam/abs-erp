<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="org.apache.struts2.components.Include"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head> 
<meta http-equiv="Content-Type" charset=utf-8 " />
<title>After Login</title>
<link rel="shortcut icon" href="images/adel1.jpg" type="image/x-icon" />
<link href="/css/reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/general.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="/css/styles/default/ui.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="/css/forms.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/tables.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="stylesheet" href="/css/CompanyVScroll.css" type="text/css"
	media="all" />
	
<link href="#" rel="stylesheet" title="style" media="all" />
<script type="text/javascript" src="/js/jquery-1.4.2.js"></script> 
<script type="text/javascript" src="/js/superfish.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.7.2.js"></script>
<script type="text/javascript" src="/js/tooltip.js"></script>
<script type="text/javascript" src="/js/tablesorter.js"></script>
<script type="text/javascript" src="/js/tablesorter-pager.js"></script>
<script type="text/javascript" src="/js/cookie.js"></script>
<script type="text/javascript" src="/js/DateTime.js"></script>
<script type="text/javascript" src="/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!--[if IE 6]>
	<link href="/css/ie6.css" rel="stylesheet" media="all" />
	
	<script src="/js/pngfix.js"></script>
	<script>
	  /* EXAMPLE */
	  DD_belatedPNG.fix('.logo, .other ul#dashboard-buttons li a');

	</script>
	<![endif]-->
<!--[if IE 7]>
	<link href="/css/ie7.css" rel="stylesheet" media="all" />
	<![endif]-->
<script type="text/javascript">
	// execute your scripts when DOM is ready. this is a good habit

	$( function() {

		// initialize scrollable with mousewheel support
		$(".scrollable").scrollable( {
			speed :1000,
			vertical :true,
			size :8,
			clickable :false,
			next :'.forward',
			prev :'.backward'
		}).mousewheel();
		$(".companyList").mouseover( function() {
			$(".vertical").slideDown();
		});
		$(".mainBody").mouseover( function() {
			$(".vertical").slideUp();
		});
	});
</script>

</head>
<body onload="startTime();">
<%@ include file="inc/SelectBeforeCompanyheader.jsp"%>
<div class="mainBody">
	<%@ include file="inc/AfterLoginmainPage.jsp"%> 
	<%@ include	file="inc/RightSideNavigation.jsp"%>
</div>
<div class="clearfix"></div>
<%@ include file="inc/Footer.jsp"%>
</body>
</html>