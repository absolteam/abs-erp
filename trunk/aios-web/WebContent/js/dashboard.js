var msgId = null;
var selectedTab = "Notifications";
var workflowProcessType=null;
$(function() {
	
	$.ajaxSetup({
		// Disable caching of AJAX responses
		cache : false
	});
	$("#approveButton").hide();
	$("#rejectButton").hide();
	$("#publishButton").hide();
	$("#mArButton").hide();

	$("#commentDialog").dialog({
		autoOpen : false,
		modal : true,
		draggable : false,
		resizable : false,

	});
});

/**
 * Overloaded Method to change css class of the active tab on dashboard.
 * 
 * @param curr:
 *            Tab/Link to show as active
 * @param otherTab1:
 *            tab/link to deactive
 * @param otherTab2:
 *            tab/link to deactive
 * 
 * @author Shoaib Ahmad Gondal @ 27-Sep-12
 */
function changeActive(curr, otherTab1, otherTab2) {
	try {
		var currActiveTab = $("[id='" + curr + "']");
		var tab1 = $("[id='" + otherTab1 + "']");
		var tab2 = $("[id='" + otherTab2 + "']");

		currActiveTab.addClass("active");

		tab1.removeClass("active");
		tab2.removeClass("active");
	} catch (e) {
		console.log(e.message);
		// TODO: handle exception
	}
}

function getTabData(tab) {
	try {
		selectedTab = tab;

		$
				.ajax({
					type : "POST",
					dataType : "json",
					url : "${pageContext.request.contextPath}/dashboard.action/getTabData",
					traditional : true,
					data : {

						tabName : tab,

					},
					success : function(response) {

						populateDashboard(response, tab);

					},
					error : function(e) {
						showErrorWindow(e.responseText, false);
					}

				});
	} catch (e) {
		console.log(e.message);
	}

}

function populateDashboard(response, tab) {

	var htmlContent = "";

	wFa = document.getElementById("wFa").value;
	wFp = document.getElementById("wFp").value;
	aN = document.getElementById("aN").value;
	rN = document.getElementById("rN").value;
	pN = document.getElementById("pN").value;
	eN = document.getElementById("eN").value;

	if (tab == "Notifications") {

		for ( var i = 0; i < response.workflowDetails.length; i++) {
			var msg = wFa;

			if ((response.workflowDetails[i].operation == 3)
					|| (response.workflowDetails[i].operation == 4)) {
				msg = wFa;
			} else if (response.workflowDetails[i].operation == 9) {
				msg = wFp;
			} else if (response.workflowDetails[i].operation == 8) {
				msg = aN;
			} else if (response.workflowDetails[i].operation == 2) {
				msg = rN;
			} else if ((response.workflowDetails[i].operation == 1)
					&& (selectedTab != "Alerts")) {
				msg = pN;
			} else if ((response.workflowDetails[i].operation == 1)
					&& (selectedTab == "Alerts")) {
				msg = eN;
			}
			processName = null;
			if (document.getElementById("language").value == 'en') {
				processName = response.workflowDetails[i].workflow.moduleProcess.processDisplayName;
			} else {
				processName = response.workflowDetails[i].workflow.moduleProcess.processDisplayNameAr;
			}

			htmlContent = htmlContent
					+ "<div class='bcklines' onclick='showNotificationDialog("
					+ response.workflowDetails[i].message.messageId + ");'> [ "
					+ response.workflowDetails[i].countryName + " ]   "
					+ processName + " - " + msg + "<div  class='notiTime'>"
					+ response.workflowDetails[i].formatednotificationTime
					+ "</div>" + " </div>";

		}
	} else {

		for ( var i = 0; i < response.workflowDetails.length; i++) {

			if ((response.workflowDetails[i].operation == 3)
					|| (response.workflowDetails[i].operation == 4)) {
				msg = wFa;
			} else if (response.workflowDetails[i].operation == 9) {
				msg = wFp;
			} else if (response.workflowDetails[i].operation == 8) {
				msg = aN;
			} else if (response.workflowDetails[i].operation == 2) {
				msg = rN;
			} else if ((response.workflowDetails[i].operation == 1)
					&& (selectedTab != "Alerts")) {
				msg = pN;
			} else if ((response.workflowDetails[i].operation == 1)
					&& (selectedTab == "Alerts")) {
				msg = eN;
			}

			processName = null;
			if (document.getElementById("language").value == 'en') {
				processName = response.workflowDetails[i].workflow.moduleProcess.processDisplayName;
			} else {
				processName = response.workflowDetails[i].workflow.moduleProcess.processDisplayNameAr;
			}

			htmlContent = htmlContent
					+ "<div class='bcklines' onclick='navigateToDataEntry("
					+ response.workflowDetails[i].message.messageId + ","
					+ response.workflowDetails[i].screenByScreenId.screenId + ");'> [ "
					+ response.workflowDetails[i].countryName + " ]   "
					+ processName + " - " + msg + "<div class='notiTime'>"
					+ response.workflowDetails[i].formatednotificationTime
					+ "</div>" + " </div>";

		}
	}

	$("#rightContent").html(htmlContent);

	$("#wfaNotiCount").text(
			'(' + response.waitingApprovalNotificationCount + ')');
	$("#aNotiCount").text('(' + response.approvedNotificationCount + ')');
	$("#alertsCount").text('(' + response.alertsCount + ')');
}

function onNotificationRowSelect(operation,detailVO) {
	if (detailVO.operation != 2) {
		$(".buttons").hide();
	}
	$("#workflow-reject-button").show();
	$("#approveButton").hide();
	$("#rejectButton").hide();
	$("#publishButton").hide();
	$("#mArButton").hide();
	if (detailVO.operation == 5) {
		$("#approveButton").text(detailVO.approveButtonName);
		$("#rejectButton").text(detailVO.rejectButtonName);
		$("#approveButton").show();
		$("#rejectButton").show();
	} else if (detailVO.operationGroupName=='Preview') {//Inform type notification
		$("#mArButton").show();
	}else if (detailVO.operation == 6) {
		$("#publishButton").text(detailVO.approveButtonName);
		$("#publishButton").show();
	} else {
		$("#mArButton").show();
	}
	workflowProcessType=detailVO.processType;

}

function getApprovalAction(path) {

	var sPage = path.substring(path.lastIndexOf('/') + 1);

	if (sPage.indexOf('.') > 0) {
		sPage = sPage.split('.')[0];
	}

	return sPage;
}

function approveNotification() {

	if (messageId != null) {
		$("#NotificationDialog").dialog("close");

		$.ajax({
			type : "POST",
			dataType : "json",
			url : "${pageContext.request.contextPath}/workflowProcess.action",
			traditional : true,
			data : {
				finalyzed : 1,
				messageId : messageId,
				processFlag : 8,

			},
			success : function(response) {
				/*if (typeof callUseCaseApprovalBusinessProcessFromWorkflow == 'function'){
					callUseCaseApprovalBusinessProcessFromWorkflow(messageId,response.returnVO);
				}*/
				$("#NotificationDialog").dialog("close");
				$("#NotificationDialog").dialog("destroy");
				reloadApprovalToDashboard();
				workflowcontentCall();

			},
			error : function(e) {
				showErrorWindow(e.responseText, false);
			}

		});
	}

}


function openRejectCommentDialog() {

	$('#rejComment').val('');
	$("#commentDialog").dialog("open");
	return false;
}

function rejectNotification() {

	rejectComment = $("#rejComment").val();

	if ((messageId != null) && ($.trim(rejectComment) != "")) {

		$
				.ajax({
					type : "POST",
					dataType : "json",
					url : "${pageContext.request.contextPath}/comment_workflow_reject_save.action",
					traditional : true,
					data : {
						finalyzed : 0,
						messageId : messageId,
						processFlag : 2,
						comment : rejectComment,

					},
					success : function(response) {
						
						$("#commentDialog").dialog("close");
						$("#commentDialog").dialog("destroy");
						$("#NotificationDialog").dialog("close");
						$("#NotificationDialog").dialog("destroy");
						workflowcontentCall();
					},
					error : function(e) {
						showErrorWindow(e.responseText, false);
					}

				});
	} else {
		showErrorMessage(resourceBundle.noComment, "");
	}
	return false;
}

function navigateToDataEntry(messageId, screenId) {
	if (messageId != null) {

		if (screenId == 1) {

			$
					.ajax({
						type : "POST",
						dataType : "json",
						url : "${pageContext.request.contextPath}/dashboard.action/navigateToDataEntry",
						traditional : true,
						data : {
							messageId : messageId,

						},
						success : function(response) {

							var currentLocation = window.location.toString();

							var newLoc = currentLocation
									.split(currentLocation
											.substring(currentLocation
													.lastIndexOf('/')))[0];
							newLoc = newLoc + "/business/"
									+ response.screenPath;

							window.location.href = newLoc;

						},
						error : function(e) {
							showErrorWindow(e.responseText, false);
						}

					});
		} else {
			showNotificationDialog(messageId);
		}
	}
}

function publish() {
	//$("#NotificationDialog").dialog("close");
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "${pageContext.request.contextPath}/workflowProcess.action",
		traditional : true,
		data : {
			messageId : messageId,
			processFlag : 7,
		},
		success : function(response) {
			$("#NotificationDialog").dialog("close");
			workflowcontentCall();
			
			
		},
		error : function(e) {
			alert(e);

			//showErrorWindow(e.responseText, false);
		}

	});
	return false;
}

function markAsRead() {
		
	$.ajax({
		type : "POST",
		url : "${pageContext.request.contextPath}/mark_as_read.action",
		data : {
			messageId : messageId,
		},
		dataType : "html",
		cache: false,
		async: false,
		traditional : true,
		success : function(response) {

			/* getTabData(selectedTab); */
			$("#NotificationDialog").dialog("close");
			reloadApprovalToDashboard();
			workflowcontentCall();

		},
		error : function(e) {
			showErrorWindow(e.responseText, false);
		}

	});

}

function reloadApprovalToDashboard(){
	var url='user_selected_company.action';
	window.location=url;
}
