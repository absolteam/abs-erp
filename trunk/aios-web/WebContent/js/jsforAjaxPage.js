$('.datepicker').datepicker({
			inline: true,
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd-M-yy",
			yearRange: '1930:2030',
			maxDate: '+1M +10D'
		});
		
$('.fromdate').datepicker({
	inline: true,
	changeMonth: true,
	changeYear: true,
	dateFormat: "dd-M-yy",
	yearRange: '1930:2030'
});	
var dt;
$('.fromdate').change(function(){
dt=($(this).datepicker("getDate"));
 
});

$('.todate').datepicker({
			inline: true,
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd-M-yy",
			yearRange: '1930:2030',
			minDate: new Date(2010,9,0,10)
		});			
$('#dialog').dialog({
	autoOpen: false,
	minwidth: 'auto',
	bgiframe: false,
	modal: false,
	buttons: {
		"Ok": function() { 
			$(this).dialog("close"); 
		}, 
		"Cancel": function() { 
			$(this).dialog("close"); 
		} 
	}
});

$('#navigate').dialog({
	autoOpen: false,
	minwidth: 'auto',
	bgiframe: false,
	modal: false,
	buttons: {
		"Ok": function() { 
			$(this).dialog("close"); 
			$.ajax({
				type:"GET",
				url:"hr/inc/"+page+".jsp",
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
					$("#main-wrapper").html(result);
				}
			});
		}, 
		"Cancel": function() { 
			$(this).dialog("close"); 
		} 
	}
});
$('.dialog_link').click(function(){
	$('#dialog').dialog('open');
	return false;
});
$('.load_nav').click(function(){
	$('#navigate').dialog('open');
	return false;
});
$('#tabs, #tabs2, #tabs5, #edittask').tabs();

	$(".column").sortable({
		connectWith: '.column'
	});

	//Sidebar only sortable boxes
	$(".side-col").sortable({
		axis: 'y',
		connectWith: '.side-col'
	});
	
	$(".toggle-div").click(function() {
		$(this).toggleClass("ui-icon-circle-arrow-n");
		$(this).parents(".portlet:first").find(".portlet-content").slideToggle();
	});
	
	$(".column").disableSelection();
	
	$("#sort-table")
	.tablesorter({
		widgets: ['zebra'],
		headers: { 
		            // assign the secound column (we start counting zero) 
		            0: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            }, 
		            // assign the third column (we start counting zero) 
		            6: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            } 
		        } 
	})
	.tablesorterPager({container: $("#pager")}); 
	
	$("#sort-tableback")
	.tablesorter({
		widgets: ['zebra'],
		headers: { 
		            // assign the secound column (we start counting zero) 
		            0: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            }, 
		            // assign the third column (we start counting zero) 
		            6: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            } 
		        } 
	})
	.tablesorterPager({container: $("#pager")}); 

	$(".header").append('<span class="ui-icon ui-icon-carat-2-n-s"></span>');
	
	$('.tooltip').tooltip({
		track: true,
		delay: 0,
		showURL: false,
		showBody: " - ",
		fade: 250
		});
	
	$("#accordion4").accordion({collapsible: true });
	
	$("#accordion_1").accordion({ header: "h4" });
	
	$(function(){ 
		if( $('.error ').html()!=null && $('.error ').html()!=""){ 
			$.scrollTo(0,300); 
		}
		if( $('.success ').html()!=null && $('.success ').html()!=""){ 
			$.scrollTo(0,300); 
		} 
	 });
	
	
	
	
