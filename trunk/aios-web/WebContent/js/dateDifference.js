var FDt;	
var TDt;
var curday;
var curmon;
var curyear; 
var fromDate;
var toDate;
var toDate;
var putMonths;
var putDays;
function calculateDate(){
	FDt		= fromDate.split("-");
	TDt		= toDate.split("-");
	li_arr		= new Array(13)
	li_arr[0]	= "TST";
	li_arr[1]	= "JAN";
	li_arr[2]	= "FEB";
	li_arr[3]	= "MAR";
	li_arr[4]	= "APR";
	li_arr[5]	= "MAY";
	li_arr[6]	= "JUN";
	li_arr[7]	= "JUL";
	li_arr[8]	= "AUG";
	li_arr[9]	= "SEP";
	li_arr[10]	= "OCT";
	li_arr[11]	= "NOV";
	li_arr[12]	= "DEC";
	var a, b;
	ld_date1 = fromDate.toUpperCase();
	ld_date1 = ld_date1.split("-");
	a  = ld_date1[1];

	ld_date2 = toDate.toUpperCase();
	ld_date2 = ld_date2.split("-");
	b  = ld_date2[1];

	
	for(i=1;i<13;i++)
	{
	  if (a == li_arr[i])
	  {
		FDt[1] = i;
		break;
	  }
	} 
	
	for(i=0;i<13;i++)
	{
		if (b == li_arr[i])
		{
			TDt[1]= i;
			break;
		}
	}  
	//alert(FDt[0]+" "+FDt[1]+" "+FDt[2]);
	//alert(TDt[0]+" "+TDt[1]+" "+TDt[2]);
	curday = Number(TDt[0]);
	curmon = Number(TDt[1]);
	curyear = Number(TDt[2]);
	dateCalculation(putYears,putMonths,putDays);
	
}
function checkleapyear(datea)
{
	if(datea.getYear()%4 == 0)
	{
		if(datea.getYear()% 10 != 0)
		{
			return true;
		}
		else
		{
			if(datea.getYear()% 400 == 0)
				return true;
			else
				return false;
		}
	}
  return false;
}
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function datediff(date1, date2) {
    var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	 y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
function dateCalculation()
{
  var calday = Number(FDt[0]);
  var calmon = Number(FDt[1]);
  var calyear = Number(FDt[2]);	 
  var curd = new Date(curyear,curmon-1,curday);
  var cald = new Date(calyear,calmon-1,calday);
  var diff =  Date.UTC(curyear,curmon,curday,0,0,0) - Date.UTC(calyear,calmon,calday,0,0,0);
  var dife = datediff(curd,cald);
  $(putYears).val(dife[0]);
  $(putMonths).val(dife[1]);
  $(putDays).val(dife[2]);
	 
}
