 (function(jQuery)
{
	jQuery.extend({			
		noticeAdd: function(options)
		{	
			var defaults = {
				inEffect: 			{opacity: 'show'},	// in effect
				inEffectDuration: 	600,				// in effect duration in miliseconds
				stayTime: 			3000,				// time in miliseconds before the item has to disappear
				text: 				'',					// content of the item
				headerText:          '', 
				ids:				'', 
				nAction: 			'',
				nType:				'', 
				wfId:				'',
				nFn:				'',
				mpId:				'',
				attr1:				'',
		        attr2:				'',
			    attr3: 				'',
			    attr4:				'',
				attr5:				'',
				attr6:				'',
				attr7:				'',
				attr8:				'',
				attr9:				'',
				attr10:				'',
		                   
				stay: 				false,				// should the notice item stay or not?
				type: 				'notice' 			// could also be error, succes
			}
			
			// declare varaibles
			var options, noticeWrapAll, noticeItemOuter, noticeItemInner, noticeItemClose, notifyId, noticeImg, notificationAction;
								
			options 		= jQuery.extend({}, defaults, options);
			noticeWrapAll	= (!jQuery('.notice-wrap').length) ? jQuery('<div></div>').addClass('notice-wrap').appendTo('body') : jQuery('.notice-wrap');
			noticeItemOuter	= jQuery('<div></div>').addClass('notice-item-wrapper'); 
			noticeItemInner	= jQuery('<div></div>').hide().addClass('notice-item ' + options.type).appendTo(noticeWrapAll).html('<span style="display:none;"id="ijk">'+options.nAction+'</span><span style="display:none;" id="mno">'+options.nType+'</span><span style="display:none"id="xyzA">'+options.wfId+'</span><span style="display:none;" id="yupB">'+options.mpId+'</span><span style="display:none;" id="uioR">'+options.attr1+'</span> <span style="display:none;" id="uyR">'+options.attr2+'</span><span style="display:none;" id="uyrZ">'+options.attr3+'</span><span style="display:none;" id="srZ">'+options.attr4+'</span><span style="display:none;" id="ujuZ">'+options.attr5+'</span><span style="display:none;" id="ukrZ">'+options.attr6+'</span><span style="display:none;" id="u1rZ">'+options.attr7+'</span><span style="display:none;" id="uy2Z">'+options.attr8+'</span><span style="display:none;" id="uwsrZ">'+options.attr9+'</span><span style="display:none;" id="udeZ">'+options.attr10+'</span><span style="display:none;" id="uio">'+options.nFn+'</span><div style="display:block;margin:2px 0 0 80px;text-transform: capitalize;font-weight:bold;font-size:14px;color:#463E41;text-decoration:underline;">'+options.headerText+'</div><p style="display:block; padding:5px 0px 0px 40px; cursor:pointer;"class="processTaskView">'+options.text+'</p><div style="display:none;" id="qrs">'+options.ids+'</div>').animate(options.inEffect, options.inEffectDuration).wrap(noticeItemOuter);
			notifyId 		= options.ids;
			notificationAction=options.nAction;
			notificationType=options.nType;
			workflowId=options.wfId;
			notifyFunctionId=options.nFn;
			mappingId=options.mpId;
			attribute1=options.attr1;
			attribute2=options.attr2;
			attribute3=options.attr3;
			attribute4=options.attr4;
			attribute5=options.attr5;
			attribute6=options.attr6;
			attribute7=options.attr7;
			attribute8=options.attr8;
			attribute9=options.attr9;
			attribute10=options.attr10;
			
			noticeImg	= jQuery('<div></div>').addClass('noticeImg').prependTo(noticeItemInner);
			noticeItemClose	= jQuery('<div></div>').addClass('notice-item-close').prependTo(noticeItemInner).html('X').click(function() { jQuery.noticeRemove(noticeItemInner,notifyId) });
		
			jQuery('.notice-item-close').click(function(){  
				var slidetab=$(this).parent().get(0);  
				var notifyId=$($(slidetab).children().get(19)).text().trim();   
				var fullDomain = "",subDomain="";
				var domainArray = new Array();
				fullDomain = window.location.pathname;
				domainArray = fullDomain.split('/');
				subDomain=domainArray[1];  
					$.ajax({
						type: "POST", 
						url: "/"+subDomain+"/sy_usernotification_close.action", 
						async: false,
						data:{notifyId:notifyId},
						dataType: "html",
						cache: false,
						success: function(result){  
							$(".temp").html(result); 
						},
						error: function(result){ 
							$(".temp").html(result);  
						} 
					});  
			});
			
			jQuery('.processTaskView').click(function(){
				var slidetab=$(this).parent().get(0);  
				var notificationAction=$($(slidetab).children().get(2)).text().trim(); 
				var notificationType=$($(slidetab).children().get(3)).text().trim();
				var workflowId=$($(slidetab).children().get(4)).text().trim();
				var mappingId=$($(slidetab).children().get(5)).text().trim();
				var attribute1=$($(slidetab).children().get(6)).text().trim();
				var attribute2=$($(slidetab).children().get(7)).text().trim();
				var attribute3=$($(slidetab).children().get(8)).text().trim();
				var attribute4=$($(slidetab).children().get(9)).text().trim();
				var attribute5=$($(slidetab).children().get(10)).text().trim();
				var attribute6=$($(slidetab).children().get(11)).text().trim();
				var attribute7=$($(slidetab).children().get(12)).text().trim();
				var attribute8=$($(slidetab).children().get(13)).text().trim();
				var attribute9=$($(slidetab).children().get(14)).text().trim();
				var attribute10=$($(slidetab).children().get(15)).text().trim();
				var notifyFunctionId=$($(slidetab).children().get(16)).text().trim();
				var notificationId=$($(slidetab).children().get(19)).text().trim();  
				var taskViewStatus=1;
				
				var fullDomain = "",subDomain="";   
				var domainArray = new Array();
				fullDomain = window.location.pathname;
				domainArray = fullDomain.split('/');
				subDomain=domainArray[1];  
				$.ajax({
					type: "POST", 
					url: "/"+subDomain+"/"+notificationAction+".action", 
					async: false,
					dataType: "html",
					data: {
							notificationId:notificationId,
							notificationType:notificationType,
							workflowId:workflowId,
							mappingId:mappingId,
							functionId:notifyFunctionId,
							attribute1:attribute1,
							attribute2:attribute2,
							attribute3:attribute3,
							attribute4:attribute4,
							attribute5:attribute5,
							attribute6:attribute6,
							attribute7:attribute7,
							attribute8:attribute8,
							attribute9:attribute9,
							attribute10:attribute10,
							taskViewStatus:taskViewStatus
						}, 
					cache: false,
					success: function(result){  
						$("#main-wrapper").html(result);
						jQuery('.notice-item-close').click();
					},
					error: function(result){ 
						$("#main-wrapper").html(result);
						alert('err'+result);
					} 
				}); 
			});
			// saleem
			if(navigator.userAgent.match(/MSIE 6/i)) 
			{
		    	noticeWrapAll.css({top: document.documentElement.scrollTop});
		    }
			
			if(!options.stay)
			{
				setTimeout(function()
				{
					jQuery.noticeRemove(noticeItemInner); 
				},
				options.stayTime);
			}
		},
		
		noticeRemove: function(obj,notifyId)
		{ 	 
			obj.animate({opacity: '0'}, 600, function()
			{   
				obj.parent().animate({height: '0px'}, 300, function()
				{  
					obj.parent().remove();  
				});
			});
		}
	}); 
})(jQuery);

