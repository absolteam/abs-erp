
function validateUsername(fld,msg) {
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
 
    if (fld.value == "") {
        fld.style.borderColor= 'red'; 
        error = "You didn't enter a "+msg+".\n";
    } else if ((fld.value.length < 5) || (fld.value.length > 15)) {
        fld.style.borderColor = 'red'; 
        error = "The "+msg+" is the wrong length.\n";
    } else if (illegalChars.test(fld.value)) {
        fld.style.borderColor = 'red'; 
        error = "The "+msg+" contains illegal characters.\n";
    } else {
        fld.style.borderColor = '#666666';
    }
    return error;
}

function validateEmpty(fld,msg) {
    var error = "";
 
    if (fld.value.length == 0 || fld.value == "0") {
        fld.style.borderColor = 'red'; 
        //error = "The required field has not been filled in "+msg+"\n";
        error = msg+" should not be blank\n";
    } else {
        fld.style.borderColor = '#666666';
    }
    return error;  
} 

function validateEmptyAndSpace(fld,msg) {
    var error = "";
    var fldTrim = trim(fld.value);
    
    if (fldTrim.length == 0 || fldTrim == "0" || fldTrim == "") {
        fld.style.borderColor = 'red';
        //error = "The required field has not been filled in "+msg+"\n";
        error = msg+ " should not be blank\n";
    } else {
        fld.style.borderColor = '#666666';
    }
    return error;  
}

function validatePassword(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.borderColor = 'red';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 6) || (fld.value.length > 15)) {
        error = "The password length should be 6 to 15 characters \n";
        fld.style.borderColor = 'red';
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.borderColor = 'red';
    } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
        fld.style.borderColor = 'red';
    } else {
        fld.style.borderColor = '#666666';
    }
   return error;
}   



function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}

function validateEmail(fld) {
    var error="";
    var tfld = trim(fld.value);                        // value of field with whitespace trimmed off
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
   
    if (fld.value == "") {
       	fld.style.borderColor = '#666666';
    } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
        fld.style.borderColor = 'red';
        error = "Please enter a valid email address.\n";
    } else if (fld.value.match(illegalChars)) {
        fld.style.borderColor = 'red';
        error = "The email address contains illegal characters.\n";
    } else {
        fld.style.borderColor = '#666666';
    }
    return error;
}



function validatePhone(fld,msg) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    
   
   if (fld.value == "") {
        error = "You didn't enter a "+msg+".\n";
        fld.style.borderColor = 'red';
    } else if (isNaN(parseInt(stripped))) {
        error = "The "+msg+" contains illegal characters.\n";
        fld.style.borderColor = 'red';
    } else if ( stripped.length <12 || stripped.length > 15) {
        error = "The "+msg+" is the wrong length. Make sure you included an area code.\n";
        fld.style.borderColor = 'red';
    }
    return error;
}

function validatePhoneField(fld,msg) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    
     if (isNaN(parseInt(stripped))) {
        error = "The "+msg+" contains illegal characters.\n";
        fld.style.borderColor = 'red';
      }
    return error;
}
function validateSelectOption(fld,msg)
{
	var error="";
	var selectedname=fld.value;
	if(selectedname=="" || selectedname=="0")
	{
		error="Must Select any one of "+msg+" \n";
		 fld.style.borderColor = 'red';
	}
	else{
	 }
	return error;
}
function validateSelectOption1(fld,msg)
{
	
	var error="";
	var selectedname=fld.value;
	if(selectedname=="" || selectedname=="O")
	{
		error="Must Select any one of "+msg+" \n";
		 fld.style.borderColor = 'red';
	}
	else{
		
	 }
	return error;
}
                          
function validateTextArea(fld,msg) {
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
 
    if (fld.value == "") {
        fld.style.borderColor = 'red'; 
        error = "You didn't enter a "+msg+".\n";
    } else if (fld.value.length > 2000) {
        fld.style.borderColor = 'red'; 
        error = "The "+msg+" is the wrong length.\n";
   
    } else {
        fld.style.borderColor = '#666666';
    }
    return error;
}

function validateTextArea_N_Space(fld,msg) {
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
 
    var fldTrim = trim(fld.value);
    if (fldTrim.length == 0 || fldTrim.value == "0" || fldTrim.value == "") {
       	fld.style.borderColor = 'red'; 
        error = "You didn't enter a "+msg+"..\n";
    } else if (fld.value.length > 2000) {
    	fld.style.borderColor = 'red'; 
        error = "The "+msg+" is the wrong length.\n";
   
    } else {
    	fld.style.borderColor = '#666666';
    }
    return error;
}

function validateNumericField(numb,msg) 
{
		var numvalue1=numb.value;
		for(var j=0;j<numvalue1.split('.').length;j++)
		{
			var numvalue=numvalue1.split('.')[j];
			for(var i=0;i<numvalue.length;i++)
			{
				var c=numvalue.charAt(i);
				if(isNaN(parseInt(c)))
				{
					numb.style.borderColor = 'red';
					return msg+" contains illegal character\n";
				}
			}
		}
		if(numvalue1.length==0)
		{
			numb.style.borderColor = 'red';
			return msg+" should not be empty \n";
		}
		else return "";
}
function validatePwd(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.borderColor = 'red';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 6) || (fld.value.length > 15)) {
        error = "The password length should be 6 to 15 characters \n";
        fld.style.borderColor = 'red';
    }else {
        fld.style.borderColor = '#666666';
    }
   return error;
}   

function validateoneOrMoreCheckbox(chkbox,errmsg)
{
	var objectLength=0;
	objectLength="document.forms[0]."+chkbox+".length";
	if(!eval(objectLength))
	{
		var objectChecked="document.forms[0]."+chkbox+".checked";
		if(!eval(objectChecked))
		{
			return errmsg;
		}
	}
	else
	{
		var chkcount=0;
		for(var i=0; i<parseInt(eval(objectLength)); i++)
		{
			var objectChecked1="document.forms[0]."+chkbox+"["+i+"].checked";
			if(!eval(objectChecked1))
				chkcount++;
		}
		if(chkcount==parseInt(eval(objectLength)))
		{
				return errmsg;
		}
	}
	return "";
}

function validateonlyoneCheckbox(chkbox,errmsg)
{
	var objectLength=0;
	objectLength="document.forms[0]."+chkbox+".length";
	if(!eval(objectLength))
	{
		var objectChecked="document.forms[0]."+chkbox+".checked";
		if(eval(objectChecked))
		{
			return "";
		}
		else
		{
			return errmsg;
		}
	}
	else
	{
		var chkcount=0;
		for(var i=0; i<parseInt(eval(objectLength)); i++)
		{
			var objectChecked1="document.forms[0]."+chkbox+"["+i+"].checked";
			if(!eval(objectChecked1))
				chkcount++;
		}
		if(chkcount==1)
		{
				return "";
		}
		else
			return errmsg;
	}
	
}

function validateTextasInteger(numb,errmsg)
{
	if(numb.value!="")
	{
		if(isNaN(parseInt(numb.value)))
		{
			return errmsg;
		}
		else return "";

	}
	else return "";
}


function validateMobileNumber(fld,msg) 
{
	var error = "";
	var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');
	var numvalue=fld.value;
	if (fld.value == "") 
	{
		error = "You didn't enter a "+msg+".\n";
		fld.style.borderColor = 'red';
	}
	for(var i=0;i<numvalue.length;i++){
		var c=numvalue.charAt(i);
		if(isNaN(parseInt(c)))
		{
			fld.style.borderColor = 'red';
			error=msg+" contains illegal character\n";
		}
	}
	if ( stripped.length <7 || stripped.length > 7) 
	{
		error = "The mobile number you have provided is wrong in length.\n";
		fld.style.borderColor = 'red';
	}
	return error;
}

function checkURL(value) {
	var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.[A-Za-z0-9])");
	if(urlregex.test(value))
	{
	return(true);
	}
	return(false);
	}

function validateTextasIntegerPrice(numb,errmsg)
{
	if(numb.value!="")
	{
		if(isNaN(parseInt(numb.value)))
		{
			return errmsg;
		}
		else return "";

	}
	else return errmsg;
}

function validateMobileNumberwithText(fld,msg) 
{
	var error = "";
	var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');
	var illegalChars = /\W/; // allow letters, numbers, and underscores
	if (fld.value == "") 
	{
		error = "You didn't enter a "+msg+".\n";
		fld.style.borderColor = 'red';
	}
	else if (illegalChars.test(fld.value)) 
	{
        fld.style.borderColor = 'red'; 
        error = "The "+msg+" contains illegal characters.\n";
    }
	else if ( stripped.length <7 || stripped.length > 7) 
	{
		error = "The mobile number you have provided is wrong in length.\n";
		fld.style.borderColor = 'red';
	}
	return error;
}

function validatePlateasInteger(numb,errmsg)
{
	if(numb.value!="")
	{
		var numvalue=numb.value;
		for(var i=0;i<numvalue.length;i++){
			var c=numvalue.charAt(i);
			if(isNaN(parseInt(c)))
			{
				numb.style.borderColor = 'red';
				return errmsg+" contains illegal character\n";
			}
		}
		if(numvalue.length>5)
		{
			numb.style.borderColor = 'red';
			return errmsg+" should not exceed 5 Digits\n";
		}
		else return "";
	}		
	else{ 
		numb.style.borderColor = 'red';
		return errmsg+"\n";
	}
}
function validatePriceField(numb,msg) 
{
		var numvalue1=numb.value;
		if(numvalue1.length==0)
		{
			numb.style.borderColor = 'red';
			return msg+" should not be empty \n";
		}
		/*else if(numvalue1.length==1 && numvalue1=='X'||numvalue1=='x')
		{	
			return "";
		}*/
		else
		{
			for(var j=0;j<numvalue1.split('.').length;j++)
			{
				var numvalue=numvalue1.split('.')[j];
				for(var i=0;i<numvalue.length;i++)
				{
					var c=numvalue.charAt(i);
					if(isNaN(parseInt(c)))
					{
						numb.style.borderColor = 'red';
						return msg+" contains illegal character\n";
					}
					
				}
			}
		}	
		
		return "";
}
function validateMobileNumberForBulkSMS(numb,msg) 
{
		var numvalue1=numb.value;
		if(numvalue1.length!=0)
		{
			for(var j=0;j<numvalue1.split(',').length;j++)
			{	
				var numvalue=numvalue1.split(',')[j];
				var k=12;
				for(var i=0;i<numvalue.length;i++)
				{	
					var c=numvalue.charAt(i);
					if(isNaN(parseInt(c)))
					{
						numb.style.borderColor = 'red';
						return msg+" contains illegal character\n";
					}
					else if(i==k){
						  k+=12;
						  if(c!=",")
						  {
							numb.style.borderColor = 'red';
							return msg+"contains invalid mobile number(s)\n";
						  }
					}
				}
				
			}
		}	
		
		return "";
}
function validatePassword1(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.borderColor = 'red';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 6) || (fld.value.length > 15)) {
        error = "The password length should be 6 to 15 characters \n";
        fld.style.borderColor = 'red';
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.borderColor = 'red';
    } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
        fld.style.borderColor = 'red';
    } else {
        fld.style.borderColor = '#666666';
    }
   return error;
} 
function validateMobileNumberSms(fld,msg) 
{
	var error = "";
	var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');
	var numvalue=fld.value;
	if (fld.value == "") 
	{
		error = "You didn't enter a "+msg+".\n";
		fld.style.borderColor = 'red';
	}
	for(var i=0;i<numvalue.length;i++){
		var c=numvalue.charAt(i);
		if(isNaN(parseInt(c)))
		{
			fld.style.borderColor = 'red';
			error=msg+" contains illegal character\n";
		}
	}
	if ( stripped.length <7 || stripped.length > 7) 
	{
		error = "Please provide valid information for "+msg+"\n";
		fld.style.borderColor = 'red';
	}
	return error;
}

function validatePasswordStrength(numb,errmsg)
{
	
	if(numb.value!="")
	{
		if(numb.value==3 || numb.value==4 || numb.value==5)
		{
			return "";
		}
		
		else
			{
			numb.style.borderColor = 'red';
			return errmsg+"Should be medium or strong.Please try different Password";}

	}
	else{ 
		numb.style.borderColor = 'red';
		return errmsg+"Should be medium or strong.Please try different Password";}
}

