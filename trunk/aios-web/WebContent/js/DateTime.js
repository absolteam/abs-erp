function startTime()
{
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	var d = getCalendarDate();
	// add a zero in front of numbers<10
	m=checkTime(m);
	s=checkTime(s);
	document.getElementById('txt').innerHTML=d+" "+h+":"+m+":"+s;
	t=setTimeout('startTime()',500);
	var now = new Date();
}
function checkTime(i)
{
	if (i<10)
	{
		i="0" + i;
	}
	return i;
}
function getCalendarDate()
{
   var months = new Array(13);
   months[0]  = "Jan";
   months[1]  = "Feb";
   months[2]  = "Mar";
   months[3]  = "Apr";
   months[4]  = "May";
   months[5]  = "Jun";
   months[6]  = "Jul";
   months[7]  = "Aug";
   months[8]  = "Sep";
   months[9]  = "Oct";
   months[10] = "Nov";
   months[11] = "Dec";
   var now         = new Date();
   var monthnumber = now.getMonth();
   var monthname   = months[monthnumber];
   var monthday    = now.getDate();
   var year        = now.getYear();
   if(year < 2000) { year = year + 1900; }
   var dateString = monthday +
                    '-' +
                    monthname +
                    '-' +
                    year;
   return dateString;
} 