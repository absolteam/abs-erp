$(function() {
		var $oe_ajax		= $('#ajaxLoader');
		var $main_oe_overlay		= $('#main_oe_overlay');

		$oe_ajax.ajaxStart(function(){
			//$("#main_oe_overlay").css("height",100+"%");
			$("#ajaxLoader").show();
			var $this = $(this);
			$main_oe_overlay.stop(true,true).fadeTo(200, 0.6);
			$main_oe_overlay.css("z-index",1);
			//$("#main-wrapper").css("z-index",-100);
		}).ajaxStop(function(){
			//$("#main_oe_overlay").css("height",0+"%");
			$("#ajaxLoader").hide();
			var $this = $(this);
			$main_oe_overlay.stop(true,true).fadeTo(200, 0);
			$main_oe_overlay.css("z-index",0);
			//$("#main-wrapper").css("z-index",100);
		});
		
		$(document).ajaxError(function(e,jqXHR,ajaxSettings,thrownError) {

			 if(jqXHR.status == '401'){
		      //location.href=contextPath+"/SessionExpired.jsp";
				 window.location.reload(true);
		    }
		});
		

		
		$('.masterTooltip').hover(function(){
	        // Hover over code
	        var title = $(this).attr('title');
	        /*if(title==null || title=='')
	        	title = $(this).attr('value');*/
	        $(this).data('tipText', title).removeAttr('title');
	        $('<p class="tooltip-css"></p>')
	        .text(title)
	        .appendTo('body')
	        .fadeIn('fast');
		}, function() {
		        // Hover out code
		        $(this).attr('title', $(this).data('tipText'));
		        $('.tooltip-css').remove();
		}).mousemove(function(e) {
		        var mousex = e.pageX + 20; //Get X coordinates
		        var mousey = e.pageY + 10; //Get Y coordinates
		        $('.tooltip-css')
		        .css({ top: mousey, left: mousex });
		});
	
	// Navigation menu

	$('ul#navigation').superfish({ 
		delay:       100,
		animation:   {opacity:'show',height:'show'},
		speed:       'fast',
		autoArrows:  true,
		dropShadows: false
	});
	
	$('ul#navigation li').hover(function(){
		$(this).addClass('sfHover2');
	},
	function(){
		$(this).removeClass('sfHover2');
	});
	
	$('ul#navigation li ul li').click(function(){
		$('ul#navigation li ul').fadeOut('slow');
	});
	
	$('ul#menunavigation').superfish({ 
		delay:       100,
		animation:   {opacity:'show',height:'show'},
		speed:       'fast',
		autoArrows:  true,
		dropShadows: false
	});
	
	$('ul#menunavigation li').hover(function(){
		$(this).addClass('sfHover2');
	},
	function(){
		$(this).removeClass('sfHover2');
	});
	

	// Accordion
	$("#accordion, #accordion2, #accordion3").accordion({ header: "h3" });

	// Tabs
	$('#tabs, #tabs2, #tabs5').tabs();

	// Dialog			
	$('#dialog').dialog({
		autoOpen: false,
		width: 600,
		bgiframe: false,
		modal: false,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$(this).dialog("close"); 
			} 
		}
	});
	
	// Login Dialog Link
	$('#login_dialog').click(function(){
		$('#login').dialog('open');
		return false;
	});

	// Login Dialog			
	$('#login').dialog({
		autoOpen: false,
		width: 300,
		height: 230,
		bgiframe: true,
		modal: true,
		buttons: {
			"Login": function() { 
				$(this).dialog("close"); 
			}, 
			"Close": function() { 
				$(this).dialog("close"); 
			} 
		}
	});
	
	// Dialog Link
	$('.dialog_link').click(function(){
		$('#dialog').dialog('open');
		return false;
	});

	// Dialog auto open			
	$('#welcome').dialog({
		autoOpen: true,
		width: 470,
		height: 180,
		bgiframe: true,
		modal: true,
		buttons: {
			"View Admintasia V1.0": function() { 
				$(this).dialog("close"); 
			}			
		}
	});

	// Dialog auto open			
	$('#welcome_login').dialog({
		autoOpen: true,
		width: 370,
		height: 430,
		bgiframe: true,
		modal: true,
		buttons: {
			"Proceed to demo !": function() {
				window.location = "index.html";
			}
		}
	});

	// Datepicker
	$('.datepicker').datepicker({
		inline: true
	});
	
	//Hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);
	//Sidebar
	$('.close-sidebar').click(function(){
		   $('#sidebar').hide('slow');
		   //$('#main-content').css('cssText','margin-right:0px!important;min-height:400px');
		   $('.show-side-bar').show('slow');
		   $("link[title='style']").attr("href","/css/no-sidebar.css");
	 });
	$('.show-side-bar').click(function(){
		   $('.show-side-bar').hide('slow');
		   $('#sidebar').show('slow');
		   $("link[title='style']").attr("href","/css/sidebar.css");
	});
	//Sortable

	$(".column").sortable({
		connectWith: '.column'
	});

	//Sidebar only sortable boxes
	$(".side-col").sortable({
		axis: 'y',
		connectWith: '.side-col'
	});

	$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
		.find(".portlet-header")
			.addClass("ui-widget-header")
			.prepend('<span class="ui-icon ui-icon-circle-arrow-s"></span>')
			.end()
		.find(".portlet-content");

	$(".side-col .portlet-header .ui-icon").click(function() {
		$(this).toggleClass("ui-icon-circle-arrow-n");
		$(this).parents(".portlet:first").find(".portlet-content").slideToggle();
	});

	$(".column").disableSelection();


	/* Table Sorter */
	$("#sort-table")
	.tablesorter({
		widgets: ['zebra'],
		headers: { 
		            // assign the secound column (we start counting zero) 
		            0: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            }, 
		            // assign the third column (we start counting zero) 
		            6: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            } 
		        } 
	})
	
	.tablesorterPager({container: $("#pager")}); 

	$(".header").append('<span class="ui-icon ui-icon-carat-2-n-s"></span>');
	
	$("#sort-tablewrk")
	.tablesorter({
		widgets: ['zebra'],
		headers: { 
		            // assign the secound column (we start counting zero) 
		            0: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            }, 
		            // assign the third column (we start counting zero) 
		            6: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            } 
		        } 
	}) 
	
});

function setComboBoxValue(comboBoxId, value) {

	$(comboBoxId).val(value);

	var selectedText = $(comboBoxId + ' option[value="' + value + '"]').text();

	var autoCompleteInput = $(comboBoxId).next();
	$(autoCompleteInput).val(selectedText);
}
	/* Tooltip */

	$(function() {
		$('.tooltip').tooltip({
			track: true,
			delay: 0,
			showURL: false,
			showBody: " - ",
			fade: 250
			});
		});
		
	/* Theme changer - set cookie */

    $(function() {
    
		$("link[title='style']").attr("href","/css/styles/default/ui.css");
        $('a.set_theme').click(function() {
           	var theme_name = $(this).attr("id");
			$("link[title='style']").attr("href","/css/styles/" + theme_name + "/ui.css");
			$.cookie('theme', theme_name );
			$('a.set_theme').css("fontWeight","normal");
			$(this).css("fontWeight","bold");
        });
		
		var theme = $.cookie('theme');
	    
		if (theme == 'default') {
	        $("link[title='style']").attr("href","/css/styles/default/ui.css");
	    };
	    
		if (theme == 'light_blue') {
	        $("link[title='style']").attr("href","/css/styles/light_blue/ui.css");
	    };
	   
	    if (theme == 'dark_blue') {
	        $("link[title='style']").attr("href","/css/styles/dark_blue/ui.css");
	    };
	
	    if (theme == 'dark_green') {
	        $("link[title='style']").attr("href","/css/styles/dark_green/ui.css");
	    };
		if (theme == 'ui_lightness') {
	        $("link[title='style']").attr("href","/css/styles/ui_lightness/ui.css");
	    };
		if (theme == 'ui_darkness') {
	        $("link[title='style']").attr("href","/css/styles/ui_darkness/ui.css");
	    };
		if (theme == 'start') {
	        $("link[title='style']").attr("href","/css/styles/start/ui.css");
	    };
		if (theme == 'sunny') {
	        $("link[title='style']").attr("href","/css/styles/sunny/ui.css");
	    };
		if (theme == 'redmond') {
	        $("link[title='style']").attr("href","/css/styles/redmond/ui.css");
	    };
		if (theme == 'humanity') {
	        $("link[title='style']").attr("href","/css/styles/humanity/ui.css");
	    };
		if (theme == 'hot_sneaks') {
	        $("link[title='style']").attr("href","/css/styles/hot_sneaks/ui.css");
	    };
		if (theme == 'trontastic') {
	        $("link[title='style']").attr("href","/css/styles/trontastic/ui.css");
	    };
		if (theme == 'pepper_grinder') {
	        $("link[title='style']").attr("href","/css/styles/pepper_grinder/ui.css");
	    };
	/* Layout option - Change layout from fluid to fixed with set cookie */
       
       $("#fluid_layout a").click (function(){
			$("#fluid_layout").hide();
			$("#fixed_layout").show();
			$("#page-wrapper").removeClass('fixed');
			$.cookie('layout', 'fluid' );
       });

       $("#fixed_layout a").click (function(){
			$("#fixed_layout").hide();
			$("#fluid_layout").show();
			$("#page-wrapper").addClass('fixed');
			$.cookie('layout', 'fixed' );
       });

	    var layout = $.cookie('layout');
	    
		if (layout == 'fixed') {
			$("#fixed_layout").hide();
			$("#fluid_layout").show();
	        $("#page-wrapper").addClass('fixed');
	    };

		if (layout == 'fluid') {
			$("#fixed_layout").show();
			$("#fluid_layout").hide();
	        $("#page-wrapper").addClass('fluid');
	    };
	
    });
    
    
 	/* Check all table rows */
	
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
return "check_all"; }
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
return "check_none"; }
}

