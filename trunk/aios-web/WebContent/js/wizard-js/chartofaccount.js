var slidetab="";
var addedCOA = 0;

$(function(){ 

	$jquery("#coavalidate-form").validationEngine('attach'); 
	 
	$('#segmentId').val($('#tempSegmentId').val());
	var tempvar=$('.tabCOA>tr:last').attr('id'); 
	var idval=tempvar.split("_");
	var rowid=Number(idval[1]); 
	$('#DeleteCOAImage_'+rowid).hide();  
	 
	$('.addCOAData').live('click',function(){
		if($jquery("#coavalidate-form").validationEngine('validate')){  
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addadd";
		 $("#AddCOAImage_"+rowid).hide();
		 $("#EditCOAImage_"+rowid).hide();
		 $("#DeleteCOAImage_"+rowid).hide();
		 $("#WorkingCOAImage_"+rowid).show(); 
		 $.ajax({
			type:"POST",
			url:"chartofaccount_addentry.action",
			data:{id:rowid, showPage:showPage},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			 	$('#accountCode').focus();
			 	if($('#segmentId').val()==3){
			 		$('#accountTypeId').addClass('validate[required]');
			 	}
			 	else{
			 		$('#accountTypeId').removeClass('validate[required]');
			 	}
			}
		 });
		}
		else{
			return false;
		}
		return false;		
	});

	$('.editCOAData').live('click',function(){
		if($jquery("#coavalidate-form").validationEngine('validate')){  
			 $('.error').hide(); 
			 slidetab=$(this).parent().parent().get(0);   
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var showPage="addedit";
			 $("#AddCOAImage_"+rowid).hide();
			 $("#EditCOAImage_"+rowid).hide();
			 $("#DeleteCOAImage_"+rowid).hide();
			 $("#WorkingCOAImage_"+rowid).show(); 
			 $.ajax({
				type:"POST",
				url:"chartofaccount_addentry.action",
				data:{id:rowid,showPage:showPage},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 	$(result).insertAfter(slidetab); 
				 	$('#accountCode').val($('#code_'+rowid).text());
				 	$('#accountDescription').val($('#description_'+rowid).text()); 
				 	$('#accountTypeId').val($('#accounttypeid_'+rowid).text());
				 	if($('#segmentId').val()==3){
				 		$('#accountTypeId').addClass('validate[required]');
				 	}
				 	else{
				 		$('#accountTypeId').removeClass('validate[required]');
				 	}
				 	$('#accountCode').focus();
				}
			 });
			}
			else{
				return false;
			}	
		return false;	
	 });

	 $(".delCOArow").live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var code=$('#code_'+rowid).text().trim();
		 if(code!=null && code!=""){
			 var flag=false; 
			 $.ajax({
					 type : "POST",
					 url:"chart_of_account_deletelineentry.action", 
					 data:{id:lineId},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.formError').hide();  
						 $('.tempresult').html(result); 
						 if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null ||$('#returnMsg').html()=="Success")
	                              flag = true;
						    	else{	
							    	flag = false; 
							    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						    	} 
						     }  
					},
					error:function(result){
						$("#lineerror").hide().html($('#returnMsg').html().slideDown());
						$("#lineerror").delay(3000).slideUp(); 
						$('.tempresult').html(result);   
						return false;
					}
						
				 });
				 if(flag==true){ 
		        		 var childCount=Number($('#childCount').val());
		        		 if(childCount > 0){
							childCount=childCount-1;
							$('#childCount').val(childCount); 
		        		 } 
		        		 $(this).parent().parent('tr').remove(); 
		        		//To reset sequence 
		        		var i=0;
		     			$('.rowidCOA').each(function(){  
							i=i+1;
							$($(this).children().get(5)).text(i); 
	   					 });  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }  
		 return false;
	 }); 

	 $('.save_chartofaccounts').click(function(){ 
 		 var accountId=0;
		 var accountCode="";
		 var accountDescription="";
		 var accountTypeId=0;
		 var segmentId=0;
 		 $.ajax({
				type:"POST",
				url:"chart_of_account_save.action", 
			 	async: false,
			 	data:{	accountCode:accountCode,accountDescription:accountDescription,
				 		accountTypeId:accountTypeId,segmentId:segmentId,accountId:accountId
				 	 },
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tempresultfinal").html(result);
					var message=$('#returnMessage').html();  
					if(message=="Success"){ 
						$.ajax({
							type:"POST",
							url:"show_account_setup.action", 
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){
								$("#DOMWindow").html(result);    
								$('#setup_success_message').hide().html(message).slideDown(1000); 
								$('#setup_success_message').delay(2000).slideUp(1000);
								return false;
							} 
					 	});
						return false;
					}else{
						$('#setup_error_message').hide().html(message).slideDown(1000);
						$('#setup_error_message').delay(2000).slideUp(1000);
						return false;
					} 
				}
		   });
	 });

	 $('.discard').click(function(){ 
		 $.ajax({
			type:"POST", 
			url:"show_setup_wizard.action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$("#main-wrapper").html(result);
			}
		});
		return false;
	 }); 

	//add rows manipulations
		$('.addCOArows').click(function(){ 
			var id=Number(1);
			var i=Number(0);
			$('.rowidCOA').each(function(){
				id=id+1; 
			});  
			$.ajax({
				type:"POST",
				url:"chartofaccount_addrow.action", 
			 	async: false,
			 	data:{id:id},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('.tabCOA tr:last').before(result); 
					 if($(".tabCOA").height()>255)
						 $(".tabCOA").css({"overflow-x":"hidden","overflow-y":"auto"});
					i=0;
					 $('.rowidCOA').each(function(){  
							i=i+1;
							var rowid=Number($(this).attr('id').split('_')[1]);
	 						$('#lineId_'+rowid).text(i);
	 					});  
				}
			});
		});
		
		manupulateCOALastRow();
});
function manupulateCOALastRow() {
	var hiddenSize = 0;
	$($(".tabCOA>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	}); 
	var tdSize = $($(".tabCOA>tr:first").children()).size();
	if(hiddenSize == tdSize)
		hiddenSize = 3;
	var actualSize = Number(tdSize - hiddenSize);

	$('.tabCOA>tr:last').removeAttr('id');
	$('.tabCOA>tr:last').removeClass('rowidCOA').addClass('lastCOArow');
	$($('.tabCOA>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tabCOA>tr:last').append("<td style=\"height:20px;\"></td>");
	}
}
function triggerCOAAddRow(rowId) {  
	var nexttab = $('.tabCOA>#fieldrow_' + rowId).next(); 
	if ($(nexttab).hasClass('lastCOArow')) {
		$('#DeleteCOAImage_' + rowId).show();
		$('.addCOArows').trigger('click');
 	}
}