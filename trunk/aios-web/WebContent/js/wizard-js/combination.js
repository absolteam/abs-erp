var combinationId=0;
var segmentId=0; 
$(function(){ 
	 
	$jquery("#combination_treeview").fancytree({ 
 		extensions: ['contextMenu','filter'],  
 		quicksearch: true, 
 		icons: true,  
 		minExpandLevel: 1,
		debugLevel: 0,
		selectMode: 1,
 		source: 
			{url: "show_combination_tree.action"}, 
		        
  		contextMenu: {
 	        menu: { 
 	          'add': { 'name': 'Add', 'icon': 'add' },	
 	          'copy': { 'name': 'Copy', 'icon': 'copy' }, 
 	          'paste': { 'name': 'Paste', 'icon': 'paste' }, 
 	          'delete': { 'name': 'Delete', 'icon': 'delete' },
 	          'sep1': '---------',
 	          'quit': { 'name': 'Quit', 'icon': 'quit' },
 	          'sep2': '---------',
 	        },
 	        actions: function(node, action, options) { 
 	        	combinationId = node.key;
  	        	segmentId = getRowId(node.refKey);
  	        	$("#"+action).trigger('click');
 	        }
 		},
 		filter: {
 	        autoApply: true,
  	        fuzzy: false,  
 	        highlight: true, 
 	        mode: "hide" 
 	    },postProcess: function(event, data) {  
 	    	tree = $jquery("#combination_treeview").fancytree("getTree");
 	    	$('.ui-autocomplete-input').attr('name', 'search'); 
 	    	callRecursiveData(data.response); 
 	  	},
	});   

	$('.ui-autocomplete-input').live('focus',function(){  
		$(this).val(''); 
	});
	
	$('.autoComplete').combobox({ 
       selected: function(event, ui){  
           var combtext=$(".autoComplete option:selected").text().trim(); 
           var result = combtext.lastIndexOf("[")+1;
           var result1= combtext.lastIndexOf("]");
           result=combtext.substring(result,result1);
           var combarray=result.split(".");
           segmentId=combarray.length; 
           combinationId=$(this).val();
           var e=event.which; 
           if(e==13){ 
        		 $('#create').trigger('click'); 
	       }else{
	    	   makeTreeview(combtext); 
		   } 
       }
	}); 

	$('.createcombi').mouseenter(function() { 
		 combinationId=0;
		 segmentId=0; 
	});

	$('.companys').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=1;
		return false;
	});

	$('.costscenter').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val(); 
		segmentId=2;
		return false;
	});

	$('.naturals').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val(); 
		segmentId=3;
		return false;
	});
	
	$('.analysis').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=4;
		return false;
	});
	
	$('.buffer1s').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=5;
		return false;
	});

	$('.buffer2s').live('mouseenter',function(){  
		combinationId=$($($(this).children()).get(0)).val();  
		segmentId=6;
		return false;
	});
	
	$('#create').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		segmentId=segmentId+1; 
		 $.ajax({
			type:"POST",
			url:"getcombination_account_codes.action", 
		 	async: false, 
		 	data:{segmentId:segmentId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.popup-result').html(result);  
			},
			error:function(result){  
				 $('.popup-result').html(result); 
			}
		}); 
		 return false;
	}); 

	 $('.deleteoption').live('click',function(){
          var combinationId = Number($(this).attr('id').split('_')[1]);
          var slidetab = $(this).parent(); 
          $('.combination-error').remove();
           if(combinationId > 0) {
        	  $.ajax({
  				type:"POST",
  				url:"combination_ledger_delete.action",
											async : false,
											dataType : "json",
											data: {combinationId: combinationId},
											cache : false,
											success : function(response) {
												if (response.sqlReturnMessage == "SUCCESS")
													callDiscard("Record Deleted");
												else{
													var htmlString = "<span class='combination-error'></span>";
													$(slidetab).append(htmlString); 
													$('.combination-error').html(response.sqlReturnMessage);  
  												}
											}
										});
							}
							return false;
						});

		$('.code_close').live('click', function() { 
			$('#common-popup').dialog('destroy');		
			$('#common-popup').remove();
			return false;
 		});
	 $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:400,
		height:200,
		bgiframe: false,
		modal: true 
	}); 
});
function callRecursiveData(data){ 
   	$.each(data, function(i, item) {  
  		$('#combinationSearch')
			.append(
					'<option value="'+data[i].key+'">'+data[i].title+'</option>');
  		callRecursiveData(data[i].children);
	});  
} 
function callDiscard(message) {
	$.ajax({
		type : "POST",
		url : "show_account_setup.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){  
			$("#DOMWindow").html(result);    
			$('#setup_success_message').hide().html("Record deleted.").slideDown(1000); 
			$('#setup_success_message').delay(2000).slideUp(); 
		}
	});
	return false;
}
function makeTreeview(combtext) { 
	loadTree();
	combtext=combtext.replace(/[\s\n\r]+/g, ' ' ).trim(); 
	var textpos=combtext.lastIndexOf('[');
	combtext=combtext.substring(0, textpos);
	$('#tree li a').each(function(){
		var treevalue=$(this).text().replace(/[\s\n\r]+/g, ' ' ).trim(); 
		var treepos=treevalue.lastIndexOf('[');
		treevalue=treevalue.substring(0, treepos);
		if(treevalue==combtext){  
			$(this).addClass('hover');  
			var parentsize=Number($(this).parents('li').size()-1);    
			for(var i=0;i<parentsize;i++){ 
				$($($(this).parents('li').children('div')).get(i)).removeClass('expandable-hitarea').addClass('collapsable-hitarea');
			} 
			$(this).parents().show(); 
		} 
	});  
	$('.ui-autocomplete-input').val('');
	return false;
}
function loadTree(){   
	$.ajax({
		type:"POST",
		url:"show_account_setup.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$("#DOMWindow").html(result);    
						$('#setup_success_message').hide().html("Record created.").slideDown(1000); 
						$('#setup_success_message').delay(2000).slideUp(1000);
					}
				});
	return false;
	}