var startpick;
var slidetab;
var availflag=1;
var calendarRecordsAdded = 0;
var periodStatusFlag=false;
$(function(){  
	
	var endDate=$('#endPicker').val();
	if(endDate=="31-Dec-9999"){
		$('#endPicker').val(""); 
	} 
	 
	 $jquery("#editCalendarVal").validationEngine('attach');
	 
	 $('.formError').remove();
	 $('.disableedit').hide();

	
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'}); 
	 var tempvar=$('.tabFinancial>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#DeleteImage_'+rowid).hide(); 
	
	 $(".addFinancialData").live('click',function(){ 
		 var checkflag=validateName($.trim($('#calendarName').val()).toLowerCase());  
		 if(checkflag==true){
		 if($('#openFlag').val() == 0) {
			 slidetab=$(this).parent().parent().get(0);   
			 //Find the Id for fetch the value from Id
			 var tempvar=$(this).parent().attr("id");
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 if($jquery("#editCalendarVal").validationEngine('validate')){  
					$("#AddFinancialImage_"+rowid).hide();
					$("#EditFinancialImage_"+rowid).hide();
					$("#DeleteFinancialImage_"+rowid).hide();
					$("#WorkingFinancialImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"calendar_period_add.action",
						data:{rowId:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
							 $(result).insertAfter(slidetab);
							 $('#name').focus();
							 $('#openFlag').val(1);
							 return false;
						}
				}); 
			 }
			 else{
				return false;
			 }	
			}else{
				return false;
			}
		}else{
			$(".calendarflag").animate({"color": "blue"}, 1000);
			$(".calendarflag").animate({"color": "red"}, 1000); 
			return false;
		}
		 return false;
	});

	$(".editFinancialData").live('click',function(){ 
		var checkflag=validateName($.trim($('#calendarName').val()).toLowerCase());  
		 if(checkflag==true){
		 if($('#openFlag').val() == 0) { 
		 slidetab=$(this).parent().parent().get(0);  
		 
		 if($jquery("#editCalendarVal").validationEngine('validate')){  
			 if($('#availflag').val()==1){
				 
				//Find the Id for fetch the value from Id
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			$("#AddFinancialImage_"+rowid).hide();
			$("#EditFinancialImage_"+rowid).hide();
			$("#DeleteFinancialImage_"+rowid).hide();
			$("#WorkingFinancialImage_"+rowid).show();   
			 $.ajax({
				 type : "POST",
				 url:"calendar_period_edit.action",
				 data:{rowId:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
	   			var name=$('#name_'+rowid).text();
	   			var startTime=$('#startTime_'+rowid).text(); 
	   			var endTime=$('#endTime_'+rowid).text(); 
	   			var extention=$('#extention_'+rowid).text();
				var periodstatus=$('#statusline_'+rowid).val();
	   			$('#name').val(name);
	   			$('#extention').val(extention);
	   			$('#startPicker_').val(startTime);
	   			$('#endPicker_').val(endTime);
	   		 	$('.periodStatus').val(periodstatus);
	   			$('#name').focus();
			    $('#openFlag').val(1);
			    return false;
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			 }
				else{
					$('#temperror').hide().html("Calendar name <span style='font-weight:bold; display: inline;'>\""+name+ "\"</span> is already is use !!!").slideDown(1000);
				}			
		 }
		 else{
			 return false;
		 }	
		 }else{
			 return false;
		 } 
		 }
		 else{
				$(".calendarflag").animate({"color": "blue"}, 1000);
				$(".calendarflag").animate({"color": "red"}, 1000); 
				return false;
			}
		 return false;
	});
	 
	 $(".delFinancialrow").live('click',function(){ 
		 var checkflag=validateName($('#calendarName').val().trim().toLowerCase());
		 if(checkflag==true){
		 slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		var tempvar=$(slidetab).attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		var lineId=$('#lineId_'+rowid).text(); 
		var periodId = $('#periodId_'+rowid).val(); 
		var flag=false; 
			 $.ajax({
				 type : "POST",
				 url:"gl_calendar_period_delete.action", 
				 data:{rowId:lineId, periodId: periodId},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
					 $('.formError').hide(); 
                     $('#temperror').hide();
                     $('#wrgNotice').hide(); 
					 $('.tempresult').html(result);   
					 if(result!=null){
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
                              flag = true;
					    	else{	
						    	flag = false; 
						    	$("#temperror").html($('#returnMsg').html());
						    	 $("#temperror").hide().slideDown(); 
					    	} 
					     }  
				},
				error:function(result){
					 $("#temperror").html($('#returnMsg').html());
					 $('.tempresult').html(result);   
					 return false;
				}
					
			 });
			 if(flag==true){  
	        		 var childCount=Number($('#childCount').val());
	        		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount); 
	        		 }  
	        		if(childCount==0){
	        			 $('#calendarName').attr("disabled",false);
	 					$('#startPicker').attr("disabled",false);
	 					$('#endPicker').attr("disabled",false);  
	        		 } 
	        		 $(this).parent().parent('tr').remove(); 
	        		//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){   
	     				i=i+1;
	     				$($(this).children().get(5)).text(i);
   					 });  
	     			return false; 
			}  
			 else{
			 $("#wrgNotice").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"});   
			 $("#wrgNotice").hide().html("Please insert record of delete.").slideDown(1000); 
			 return false; 
		 }
		 } else{
				$(".calendarflag").animate({"color": "blue"}, 1000);
				$(".calendarflag").animate({"color": "red"}, 1000); 
				return false;
			}	 
		return false; 
	 });

	 $(".save_calendar").unbind().click(function() {
		 var checkflag=validateName($('#calendarName').val().trim().toLowerCase());
		 if(checkflag==true){
		 var calHeaderId=$("#calHeaderId").val(); 
		 var calName=$("#calendarName").val();
		 var frmDate=$("#startPicker").val();
		 var toDate=$("#endPicker").val();
		 var status=$('#status').val(); 
		 if($jquery("#editCalendarVal").validationEngine('validate')){  
 			 $.ajax({
				 type : "POST",
				 url:"gl_calendar_add_save_action.action", 
				 data:{calendarId:calHeaderId, name:calName, startTime:frmDate, endTime:toDate, status:status},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){  
					$('.formError').hide();  
					$(".tempresult").html(result);  
					var tempmessage=$('.tempresult').html().trim(); 
					var temparray=tempmessage.split("(");
					var message=temparray[0];
					tempmessage = temparray[1].substring(0, temparray[1].length-1).toLowerCase(); 
					if(tempmessage=="success"){ 
						$.ajax({
							type:"POST",
							url:"show_account_setup.action", 
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){
								$("#DOMWindow").html(result);    
								$('#setup_success_message').hide().html(message).slideDown(1000); 
								$('#setup_success_message').delay(2000).slideUp(1000);
								return false;
							} 
					 	});
						return false;
					}else{
						$('#setup_error_message').hide().html(message).slideDown(1000);
						$('#setup_error_message').delay(2000).slideUp(1000);
						return false;
					} 
					return false;
			    } 
		   }); 
		 }
		 else{
			 return false;
		 }
		 return false;
	 }	 else{
			$(".calendarflag").animate({"color": "blue"}, 1000);
			$(".calendarflag").animate({"color": "red"}, 1000); 
			return false;
		}	 
		 return false;
	 });

	 $('.delete_calendar').click(function(){
		 var cnfrm = confirm("Warning: Delete Financial Year?");
		 var calHeaderId=$("#calHeaderId").val(); 
			if(!cnfrm) {
				return false;
			}else{
				$.ajax({
					type:"POST",
					url:"gl_calendar_delete.action", 
				 	async: false,
				 	data:{calendarId:calHeaderId},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$(".tempresult").html(result); 
						var message=$('.tempresult').html().trim(); 
						var tempmessage=message; 
						var temparray=tempmessage.split("(");
						message=temparray[0];
						tempmessage = temparray[1].substring(0, temparray[1].length-1).toLowerCase(); 
						if(tempmessage=="success"){
							$.ajax({
								type:"POST",
								url:"show_calendarentry.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(message).slideDown(1000); 
									$('#success_message').delay(2000).slideUp(1000);  
								}
						 	});
						}else{
							$('#error_message').hide().html(message).slideDown(1000);
							$('#error_message').delay(2000).slideUp(1000);  
						}
					}
			 	}); 
			} 
	 });
	 
	 $('.skip_accounts_setup').click(function(){ 
		 $.ajax({
			type: "POST", 
			url: "customise_general_accounts.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#DOMWindow").html(result);  
			} 		
		 });  
	 });
      
	 $(".cancel_calendar").click(function(){
		 $.ajax({
			 type:"POST",
			 url:"show_setup_wizard.action", 
			 async: false,
			 dataType: "html",
			 cache: false,
			 success:function(result){  
				 $('.formError').remove(); 
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
				 $('#main-wrapper').html(result);  
			 } 
		 });
	 });

	 $('.addFinancialrows').click(function(){
			var totalTR = $(".tabFinancial>tr").size();
			totalTR = totalTR -1 ; 
			var tempvar = $('.tabFinancial>#fieldrow_'+totalTR).attr('id');  
			var idval=tempvar.split("_");
			var rowid=Number(idval[1]);   
			rowid = Number(rowid + 1);
			$.ajax({
				type:"POST",
				url:"calendar_addrows.action",
				data:{rowId:rowid}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('.tabFinancial tr:last').before(result);
				 if($(".tabFinancial").height()>255)
					 $(".tabFinancial").css({"overflow-x":"hidden","overflow-y":"auto"});
				//To reset sequence 
				 $('.rowFinancialid').each(function(){  
						i=i+1;
						var rowid=Number($(this).attr('id').split('_')[1]);
 						$('#lineId_'+rowid).text(i);
 					});  
				} 
  			
			});
		});  
	 
	 $('#calendarName').change(function(){  
		 var flag=validateName($('#calendarName').val().toLowerCase().trim()); 
		 if(flag==false){
			 $('.calendarflag').hide().html("Already exits.").slideDown(1);
		 }else{
			 $('.calendarflag').hide();
		 }
	 }); 
	 
	 
	if(Number($('#calHeaderId').val())>0){
		$('#status').val($('#tempstatus').val().trim());
		calendarRecordsAdded=1;
		if(Number($('.tabFinancial tr').length>2)){
			$('#startPicker').attr('disabled',true);
			$('.delete_calendar').remove();
		} 
		var calendarstatus=$('#financial-status').val().trim();	
		if(calendarstatus==false && $('#status').val()!=1){
			$('#status').attr('disabled',true);
		} 
		$('.save_calendar').html('Update');
	}else{
 		$('.ui-combobox-button').attr('style', 'padding-bottom : 6px!important');
		$('.delete_calendar').remove();
	}
			
			
	$('#tempcalendarName').val($.trim($('#calendarName').val()));	
	
	 manupulateFinancialLastRow();
 });
function validateName(name){
	var checkflag=true;
	$('.financialYear option').each(function(){ 
		var tempname=$(this).val().trim().toLowerCase();
		var arrayval=tempname.split('#@');
		var financialname=arrayval[1];
		var tempcalendarName=$('#tempcalendarName').val().trim().toLowerCase();
		if(tempcalendarName!=name){
			if(checkflag==true && name==financialname){
				checkflag=false;
			} 
		}
	}); 
	return checkflag;
} 
//Prevent selection of invalid dates through the select controls
 function checkLinkedDays() {
     var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
     $('#selectedDay option:gt(27)').attr('disabled', false);
     $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
     if ($('#selectedDay').val() > daysInMonth){
         $('#selectedDay').val(daysInMonth);
     }
 } 
 function customRange(dates) {
 	if (this.id == 'startPicker') {
 	 	if($('#startPicker').val()!=""){
 	 	 	$('.startPickerformError').hide();
 	 		addPeriods(); 
 	 	}
 		$('#endPicker').datepick('option', 'minDate', dates[0] || null); 
 		
 	}
 	else{
 		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);  
 	} 
 }
function addPeriods(){
	var date = new Date($('#startPicker').datepick('getDate')[0].getTime());  
	$.datepick.add(date, parseInt(364, 10), 'd');
	$('#endPicker').val($.datepick.formatDate(date)); 
}
function manupulateFinancialLastRow() {
	var hiddenSize = 0;
	$($(".tabFinancial>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tabFinancial>tr:first").children()).size();
	if(hiddenSize == tdSize)
		hiddenSize  = 1;
	var actualSize = Number(tdSize - hiddenSize);

	$('.tabFinancial>tr:last').removeAttr('id');
	$('.tabFinancial>tr:last').removeClass('rowid').addClass('lastrowFinancial');
	$($('.tabFinancial>tr:last').children()).remove();
	
	for ( var i = 0; i < actualSize; i++) { 
		$('.tabFinancial>tr:last').append("<td style=\"height:20px;\"></td>");
	}
}
function triggerFinancialAddRow(rowId) {  
	var nexttab = $('.tabFinancial>#fieldrow_' + rowId).next(); 
	if ($(nexttab).hasClass('lastrowFinancial')) {
		$('#DeleteFinancialImage_' + rowId).show();
		$('.addFinancialrows').trigger('click');
 	}
}