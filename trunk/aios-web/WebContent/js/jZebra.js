/**
	* Automatically gets called when applet has loaded.
	*/
	function qzReady() {
		// Setup our global qz object
		window["qz"] = document.getElementById('qz');
		var title = document.getElementById("title");
		if (qz) {
			try {
				title.innerHTML = title.innerHTML + " " + qz.getVersion();
				document.getElementById("content").style.background = "#F0F0F0";
			} catch(err) { // LiveConnect error, display a detailed meesage
				document.getElementById("content").style.background = "#F5A9A9";
				alert("ERROR:  \nThe applet did not load correctly.  Communication to the " + 
					"applet has failed, likely caused by Java Security Settings.  \n\n" + 
					"CAUSE:  \nJava 7 update 25 and higher block LiveConnect calls " + 
					"once Oracle has marked that version as outdated, which " + 
					"is likely the cause.  \n\nSOLUTION:  \n  1. Update Java to the latest " + 
					"Java version \n          (or)\n  2. Lower the security " + 
					"settings from the Java Control Panel.");
		  }
	  }
	}
	
	/**
	* Returns whether or not the applet is not ready to print.
	* Displays an alert if not ready.
	*/
	function notReady() {
		// If applet is not loaded, display an error
		if (!isLoaded()) {
			return true;
		}
		// If a printer hasn't been selected, display a message.
		else if (!qz.getPrinter()) {
			alert('Please select a printer first by using the "Detect Printer" button.');
			return true;
		}
		return false;
	}
	
	/**
	* Returns is the applet is not loaded properly
	*/
	function isLoaded() {
		if (!qz) {
			//alert('Error:\n\n\tPrint plugin is NOT loaded!');
			return false;
		} else {
			try {
				if (!qz.isActive()) {
					//alert('Error:\n\n\tPrint plugin is loaded but NOT active!');
					return false;
				}
			} catch (err) {
				//alert('Error:\n\n\tPrint plugin is NOT loaded properly!');
				return false;
			}
		}
		return true;
	}
	
	/**
	* Automatically gets called when "qz.print()" is finished.
	*/
	function qzDonePrinting() {
		// Alert error, if any
		if (qz.getException()) {
			alert('Error printing:\n\n\t' + qz.getException().getLocalizedMessage());
			qz.clearException();
			return; 
		}
	}
	
	/***************************************************************************
	* Prototype function for finding the "default printer" on the system
	* Usage:
	*    qz.findPrinter();
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function useDefaultPrinter() {
		if (isLoaded()) {
			// Searches for default printer
			qz.findPrinter();
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Alert the printer name to user
				var printer = qz.getPrinter();
				/*alert(printer !== null ? 'Default printer found: "' + printer + '"':
					'Default printer ' + 'not found');*/
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
	
	/***************************************************************************
	* Prototype function for printing a PDF to a PostScript capable printer.
	* Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendPDF('/path/to/sample.pdf');
	*    window['qzDoneAppending'] = function() { qz.printPS(); };
	***************************************************************************/ 
	function printPDF() {
		if (notReady()) { return; }
		// Append our pdf (only one pdf can be appended per print)
		qz.appendPDF(getPath() + "misc/pdf_sample.pdf");
		
		//qz.setCopies(3);
		qz.setCopies(parseInt(document.getElementById("copies").value));
		
		// Automatically gets called when "qz.appendPDF()" is finished.
		window['qzDoneAppending'] = function() {
			// Tell the applet to print PostScript.
			qz.printPS();
			
			// Remove reference to this function
			window['qzDoneAppending'] = null;
		};
	}
	
	function salesInvoiceTemplatePrint(response){
		try{
 			if (isLoaded()) {
				useDefaultPrinter();  
				qz.appendHTML(response);
				deliveryTemplatePrint();
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				qz.appendHTML(response);
				deliveryTemplatePrint();
		    }, 1000);	
		  } 	
		}catch(e){
			
		} 
	}
	
	function bankReceiptTemplatePrint(response, bankReceiptsId) {   
		try{
 			if (isLoaded()) {
				useDefaultPrinter();  
				qz.appendHTML(response);
				receiptPrintMonitor(bankReceiptsId);
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				qz.appendHTML(response);
				receiptPrintMonitor(bankReceiptsId);
		    }, 1000);	
		  } 	
		}catch(e){
			
		} 
	} 
	
	function receiptPrintMonitor(bankReceiptsId) { 
        var applet = qz; 
         if (applet != null) {
        	 applet.setPaperSize("8.5in", "11.0in"); 
           if (!applet.isDoneAppending()) {
              window.setTimeout('receiptPrintMonitor()', 100);
           } else {  
              applet.printHTML(); // Don't print until all of the image data has been appended
           } 
           if(bankReceiptsId >  0){
        	   setTimeout(function() {  
        		   bankReceiptAcknoPrint(bankReceiptsId);
   		    }, 3000); 
           }
        } else {
            alert("Applet not loaded!");
        }
    }
	
	function bankReceiptAcknoPrint(bankReceiptsId){
		$.ajax({
			type: "POST", 
			url:"${pageContext.request.contextPath}/bank_receipt_direct_ackno_printout.action", 
 	     	async: false, 
	     	data:{selectedReceiptId: bankReceiptsId},
			dataType: "json",
			cache: false,
			success: function(response){ 
				bankReceiptsId = Number(0);
				bankReceiptTemplatePrint(response, bankReceiptsId);
			} 		
		}); 
	}
	
	function taskDeliveryTemplatePrint(response) {   
		try{
			if (isLoaded()) {
				useDefaultPrinter();  
				qz.appendHTML(response);
				deliveryPrintMonitor();
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				qz.appendHTML(response);
				deliveryPrintMonitor();
		    }, 1000);	
		  } 	
		}catch(e){
			
		} 
	} 
	
	function deliveryTemplatePrint(response) {   
		try{
			if (isLoaded()) {
				useDefaultPrinter();  
				qz.appendHTML(response);
				deliveryPrintMonitor();
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				qz.appendHTML(response);
				deliveryPrintMonitor();
		    }, 1000);	
		  } 	
		}catch(e){
			
		} 
	} 
	
	function deliveryPrintMonitor() {
        var applet = qz; 
         if (applet != null) {
        	 applet.setPaperSize("8.5in", "11.0in"); 
           if (!applet.isDoneAppending()) {
              window.setTimeout('deliveryPrintMonitor()', 100);
           } else {  
              applet.printHTML(); // Don't print until all of the image data has been appended
           } 
        } else {
            alert("Applet not loaded!");
        }
    }
	 
	function jobTaskTemplatePrint(response){
		try{
			if (isLoaded()) {
				useDefaultPrinter();  
				qz.appendHTML(response);
				customMonitorAppending3(false);
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				qz.appendHTML(response);
				customMonitorAppending3(false);
		    }, 1000);	
		  } 	
		}catch(e){
			
		} 
	}
	
	function jobTemplatePrint(taskExpense) { 
		try{
			if (isLoaded()) {
				useDefaultPrinter();  
				getJobPrintContent(taskExpense);
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				getJobPrintContent(taskExpense);
		    }, 1000);	
		  } 	
		}catch(e){
			
		} 
	}
	
	function getJobPrintContent(taskExpense){ 
		$.ajax({
			type:"POST",
			url:"${pageContext.request.contextPath}/project_job_print_content.action", 
			async: false,
			data:{taskExpense : taskExpense},
			dataType: "json",
			cache: false,
			success:function(response){
				taskExpense = response.taskExpense; 
				qz.appendHTML(response.format);
			} 
		}); 
		customMonitorAppending3(taskExpense);  
	}
	
	function customMonitorAppending3(taskExpense) {
        var applet = qz; 
         if (applet != null) {
        	 applet.setPaperSize("8.5in", "11.0in");
           if (!applet.isDoneAppending()) {
              window.setTimeout('customMonitorAppending3()', 100);
           } else {  
              applet.printHTML(); // Don't print until all of the image data has been appended
           }
           if(taskExpense ==  true){
        	   setTimeout(function() {  
        		   jobTemplatePrint(taskExpense);
   		    }, 3000); 
           }
        } else {
            alert("Applet not loaded!");
        }
      }
	
	/***************************************************************************
	* Prototype function for printing plain HTML 1.0 to a PostScript capable 
	* printer.  Not to be used in combination with raw printers.
	* Usage:
	*    qz.appendHTML('<h1>Hello world!</h1>');
	*    qz.printPS();
	***************************************************************************/ 
	function printHTML() {  		
		try{
			if (isLoaded()) {
				useDefaultPrinter();  
				sendForPrint();
				return "true";
		  }else{ 
		    setTimeout(function() { 
				useDefaultPrinter();  
				sendForPrint();
				return "true";
		    }, 200);	
		  } 	
		}catch(e){
			return "false";
		} 
	}
	
	function sendForPrint() {
		
		var printPaginationRequired = $("#pos_pagination").val();
		
		if(printPaginationRequired != null 
				&& (printPaginationRequired == false || printPaginationRequired == "false")) {
			
			getPointOfSaleContent(-1);
		} else {
			
			var totalItems = null;
			
			$.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/pos_print_item_list_size.action", 
				async: false,
				dataType: "json",
				cache: false,
				success:function(result){
					totalItems = result.totalItemsToPrint;					
				},
				error:function() {
					alert("Error! unexpected items list size");
				}
			}); 
						
			if(totalItems == null) {			
				getPointOfSaleContent(-1);
				
			} else {		
				var pagesRequired = Math.ceil(totalItems / printPaginationRequired);				
				
				if(pagesRequired > 1.00) {					
					for(var i = 0 ; i < pagesRequired ; i++) {						
						getPointOfSaleContent(printPaginationRequired * i);
					}
				} else {					
					getPointOfSaleContent(-1);
				}
			}
		}
		
	}
	
	function getPointOfSaleContent(paginationValue){
		$.ajax({
			type:"POST",
			url:"${pageContext.request.contextPath}/pos_print_content.action?itemsPrintStart=" + paginationValue, 
			async: false,
			dataType: "json",
			cache: false,
			success:function(result){  
				qz.appendHTML(result);
			} 
		}); 
		monitorAppending3();
	}
	
	function employeePrintHTML() { 
		try{
			useDefaultPrinter(); 
			$.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/pos_employee_print_content.action", 
			 	async: false,
			    dataType: "json",
			    cache: false,
				success:function(result){  
					qz.appendHTML(result);
				} 
			}); 
			monitorAppending3(); 
		}catch(e){
			
		} 
	}
	
	function customPrintHTML(printerName, reciptContent) { 
		try{
			var applet1 = qz;
			if (isLoaded()) {
				applet1.findPrinter(printerName);
				applet1.appendHTML(reciptContent);
				applet1.setPaperSize("8.5in", "11.0in"); 
				applet1.setAutoSize(true);  
				applet1.printHTML(); 
			}
		}catch(e){
			
		} 
	} 
	
	var temp = false;
	
	function monitorAppending3() {
        var applet = qz;
        applet.setPaperSize("8.5in", "11.0in");  // US Letter
		applet.setAutoSize(true);  
        if (applet != null) {
           if (!applet.isDoneAppending()) {
              window.setTimeout('monitorAppending3()', 500);
           } else {  
              applet.printHTML(); // Don't print until all of the image data has been appended
              if(temp == true)
            	  return false;
              else {
            	  temp = true;
            	  monitorAppending3();
              }
            	  
           }
        } else {
            alert("Applet not loaded!");
        }
      }
	
	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter(name) {
		// Get printer name from input box
		var p = name;
		if (name) {
			p.value = name;
		}
		
		if (isLoaded()) {
			// Searches for locally installed printer with specified name
			qz.findPrinter(p.value);
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				var p = name;
				var printer = qz.getPrinter();
				
				// Alert the printer name to user
				alert(printer !== null ? 'Printer found: "' + printer + 
					'" after searching for "' + p.value + '"' : 'Printer "' + 
					p.value + '" not found.');
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
       
	/**
	* Fixes some html formatting for printing. Only use on text, not on tags!
	* Very important!
	*   1.  HTML ignores white spaces, this fixes that
	*   2.  The right quotation mark breaks PostScript print formatting
	*   3.  The hyphen/dash autoflows and breaks formatting  
	*/
	function fixHTML(html) {
		return html.replace(/ /g, "&nbsp;").replace(/�/g, "'").replace(/-/g,"&#8209;"); 
	}
	
	/**
	* Equivelant of VisualBasic CHR() function
	*/
	function chr(i) {
		return String.fromCharCode(i);
	}