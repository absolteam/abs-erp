$(document).ready(function() {
	$('#reject-button-tr').hide();
	$('.escalation-div').hide();
	
	$('#operation_add').click(function() {
		workflowOperationFormWindow();
	});
	
	$('#alert_responsibility_add').click(function() {
		$.ajax({
			type : "POST",
			url : "show_alert_role_mapping.action",
			async: false,
		    dataType: "html",
		    cache: false,
		    success : function(result) {
				$("#main-wrapper").html(result);
			},
			error : function(e) {
				alert(result);
				$("#main-wrapper").html(result);
			}

		});
		
	});
	
	
	$('#isEscalation').click(function() {
		if($('#isEscalation').attr("checked"))
			$('.escalation-div').show();
		else
			$('.escalation-div').hide();
	});
	
	
	
	$('#Operation_Reset').click(function() {
		$('#Operation_Form').each(function() {
			this.reset();
		});
	});
	
	$('#operationGroup').change(function() {
		if($('#operationGroup option:selected').val()==3){
			$('#reject-button-tr').show();
		}else{
			$('#reject-button-tr').hide();
			$('#rejectButton').val("");
		}
		return false;
	});
	

	
	
	
	
	$('#Operation_save').click(function() {
		var postData = $('#Operation_Form').serializeArray();
		if(preSubmissionOperationValidation()){
			$.ajax({
				type : "POST",
				dataType : "json",
				url : "${pageContext.request.contextPath}/saveWorkflowOperation.action",
				traditional : true,
				data :postData,
				success : function(response) {
					if(response.status=='SUCCESS'){
						alertify.alert("Information has been saved successfully.");
						reloadOperations();
						$('#Operation_Reset').trigger("click");
	
					}else{
						alertify.alert("Error!.Information have not been saved.");
					}
				},
				error : function(e) {
					// showErrorWindow(e.responseText, false);
					console.log(e.responseText);
				}
	
			});
		}else{
			return false;
		}
		return false;
	});
	
	$('#Operation_delete').click(function() {
		deleteOperation();
		return false;
	});
	


});

function workflowOperationFormWindow(){
	$('#Operation_Form_Div').dialog({
		autoOpen : false,
		height : 540,
		width : 500,
		modal : true,
		resizable : false

	});
	$('#Operation_Form_Div').dialog('open');
	$('#Operation_Form').each(function() {
		this.reset();
	});
	return false;
}

function reloadOperations(){
	var processId = $('#moduleProcessesList :selected').val();
	if(processId==null || processId=='' || processId=='-9999' || processId=='0')
		processId=0;
	
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "${pageContext.request.contextPath}/reloadOperations.action",
		traditional : true,
		data : {
			processId : processId,
		},
		success : function(response) {
			$("#operations").html("");
			$("#operationList").html("");
			var operations=response.operationList;
			//Operation work
			htmlString = "<option value=''>-Select-</option>";
			// append roles to operation combo
			for ( var i = 0; i < operations.length; i++) {
				htmlString = htmlString + "<option value=" + operations[i].operationMaster.operationMasterId+"&"+operations[i].operationMaster.operationGroup + ">"
						+ operations[i].operationMaster.title + "</option>";
			}

			$("#operations").append(htmlString);
			$("#operationList").append(htmlString);
			$('#operations').combobox();

		},
		error : function(e) {
			// showErrorWindow(e.responseText, false);
			alertify.alert(e.responseText);
		}

	});
	
	return false;
}


function deleteOperation(){
	var operationMasterId= $('#operationList').val();
	alertify.set({
		labels : {
			ok : "Continue",
			cancel : "Cancel"
		}
	});

	alertify.set({
		buttonReverse : true
	});
	alertify
	.confirm(
			"Operation information Will be delete permanantly. <br /> Do you want to continue?",
			function(e) {
				if (e) {
					$.ajax({
						type : "POST",
						dataType : "json",
						url : "${pageContext.request.contextPath}/deleteWorkflowOperation.action",
						traditional : true,
						data : {
							operationMasterId : operationMasterId
						},
						success : function(response) {
							if(response.status=='SUCCESS'){
								reloadOperations();
								$('#Operation_Reset').trigger("click");
							}else{
								alertify.alert("Error!.Information have not been deleted.");
							}
						},
						error : function(e) {
							// showErrorWindow(e.responseText, false);
							console.log(e.responseText);
						}
					});
				} else {
					// user clicked "cancel"
					return false;
				}
			});
	return false;
}


var recentWorkflows = new Array();

$("#moduleProcessesList").combobox({
	selected : function(event, ui) {

		// clear existing data
		$("#processFlows").empty();

		showProcessFlowsDiv();

		onModuleProcessSelect();
	}
});

$('#accessRight').combobox({
	selected : function(event, ui) {
		showUserOrRoleCombo();
	}
});
$('#accessRightEscalation').combobox({
	selected : function(event, ui) {
		showUserOrRoleEscalationCombo();
	}
});


$('#userRoles')
		.combobox(
				{
					selected : function(event, ui) {

						var selectedOperationId = $('#type1 :selected').val();

						var selectedText = $(
								'#userRoles option[value='
										+ $('#userRoles :selected').val() + ']')
								.text();

						// if operation is Entry, validate selected role
						if ((selectedOperationId == '3')
								&& $('#type1Div').is(':visible')) {
							if (!validateSelectedRoleForEntry(selectedText)) {
								event.preventDefault();

								setComboBoxValue("#userRoles", 0);
							}
						}
					}
				});
$('#operations')
.combobox(
		{
		selected : function(event, ui) {
			var operationIds=($('#operations option:selected').val()).split("&");
			if(Number(operationIds[1])==3){
				$('#reject-screen-tr').show();
			}else{
				$('#reject-screen-tr').hide();
				setComboBoxValue("#rejectScreens", '');
			}
			
			
		}
});
$('#userRoles').combobox();
$('#persons').combobox();
$('#screens').combobox();
$('#rejectScreens').combobox();
$('#userRolesEscalation').combobox();
$('#personsEscalation').combobox();

/**
 * Gets call when user selects a process from processes combo.
 * <p>
 * Date: 08-Apr-13
 * 
 * @author Usman Anwar
 */
function onModuleProcessSelect() {

	var id = $('#moduleProcessesList :selected').val();
	
	//Set moduleProcess id for submit the operation form submission.
	$('#moduleProcessId').val(id);
	reloadOperations();
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "aios-web/onModuleProcessSelect.action",
		traditional : true,
		data : {

			processId : id,

		},
		success : function(response) {

			clearWorkflowForm();
			fillWorkflowForm(response);
		},
		error : function(e) {
			// showErrorWindow(e.responseText, false);
			console.log(e.responseText);
		}

	});
	
	return false;
}

/**
 * Resets the fields to their default values as they were when page loads first
 * time.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function clearWorkflowForm() {
	$("#selectedWorkflowId").val(0);
	$("#selectedWorkflowIndex").val(0);
	$("#isRedefine").val(false);
	$("#operationNumber").val(1);
	$("#operationOrder").val(1);

	if ($("#accessRight option").length > 1) {
		$("#accessRight option:last").remove();
	}

	$("#userRoles option:not(:first)").remove();

	$("#recentWorkflowTable tbody").empty();

	$("#recentWorkflowListSize").val(0);
	$("#isEntry").val(0);
	$("#isApproval").val(0);
	$("#isPublish").val(0);
}

/**
 * Displays workflows defined for a process.
 * <p>
 * Date: 10-Apr-13
 * 
 * @param response
 *            the response from the server against the ajax call made after
 *            selection of a process.
 * 
 * @author Shoaib Ahmad Gondal
 */
function fillWorkflowForm(response) {

	try {
		var workflowVOs = JSON.parse(response.workflowVOsJSON);
		var roles = JSON.parse(response.rolesJSON);
		var screens = JSON.parse(response.screensJSON);
		var users = JSON.parse(response.usersJSON);
		var quantum=response.quantum;
		$("#processFlows").html("");
		var htmlString = "";
		var workflow = null;

		// number of workflows defined for selected process
		for ( var i = 0; i < workflowVOs.length; i++) {
			workflow = workflowVOs[i];
			// operations detail for defined workflow
			var workflowDetails = workflow.workflowDetailVOs;
			htmlString="<br/><div style='width:100%;float:left;font-weight:bold;font-size:14px;'>"+workflow.workflowTitle+"</div><br/>";
			htmlString = htmlString+ "<table id='"
				+ i	+ "' style='border-spacing: 0; width: 96%; margin:auto;' class='ui-datatable'> <thead> <tr> " ;
				htmlString = htmlString	+"<th class='ui-state-default' style='padding:3px;'> Operation </th>" ;
				htmlString = htmlString	+" <th class='ui-state-default' style='padding:3px;'> Screen </th>" ;
				htmlString = htmlString	+"<th class='ui-state-default' style='padding:3px;'>  User/Role </th>" ;
				htmlString = htmlString	+" <th class='ui-state-default' style='padding:3px;'> Operation Orders</th>";
				htmlString = htmlString	+" <th class='ui-state-default' style='padding:3px;'> Escalation </th>" ;
				htmlString = htmlString	+" <th class='ui-state-default' style='padding:3px;'> Reminder </th>" ;
				htmlString = htmlString	+" <th class='ui-state-default' style='padding:3px;'> Quantum </th>" ;
				htmlString = htmlString	+"</tr></thead><tbody class='ui-datatable-data ui-widget-content'>";

		for ( var j = 0; j < workflowDetails.length; j++) {

			var workflowDetail = workflowDetails[j];

			htmlString = htmlString
					+ "<tr class='ui-widget-content'><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'>";

			htmlString = htmlString +workflowDetail.operationTitle;
			htmlString = htmlString
					+ "</td><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'>";

			htmlString = htmlString + workflowDetail.screenName +""+(workflowDetail.rejectScreenName!=''?" & Reject screen is - "+workflowDetail.rejectScreenName:"");

			htmlString = htmlString + "</td><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'>";

			if (workflowDetail.role.roleId != null) {
				htmlString = htmlString + workflowDetail.role.roleName;

			} else if (workflowDetail.person.personId != null) {
				htmlString = htmlString + workflowDetail.person.firstName
						+ " " + workflowDetail.person.lastName;
			}

			htmlString = htmlString
					+ "</td><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'>";

			htmlString = htmlString + workflowDetail.operationOrder;

			htmlString = htmlString + "</td><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:20%'>";

			htmlString = htmlString + workflowDetail.escalationInformation;

			htmlString = htmlString + "</td><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:20%'>";

			htmlString = htmlString + workflowDetail.reminderInformation;

			htmlString = htmlString + "</td><td style='border: 1px solid #b1b1b1 !important; padding:3px; width:20%'>";

			htmlString = htmlString + workflowDetail.quantumInformation;

			htmlString = htmlString + "</td></tr>";

		}

		htmlString = htmlString + "</tbody></table></br>";
		console.debug(htmlString);

		// append 'Redefine workflow' button
		htmlString = htmlString
				+ "<div style='margin-left: 20px;'><input type='button' value='Re-Define' style='height:28px; width: 91px;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover' onclick='confirmRedefine("
				+ workflow.workflowId + "," + i
				+ ", true)'/><input type='button' value='Delete' style='height:28px; width: 91px;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover' onclick='confirmDelete("
				+ workflow.workflowId + "," + i
				+ ", true)'/></div><br /><br/>";
	
		$("#processFlows").append(htmlString);
	}
		

	// Append 'Add new workflow' button
	htmlString = "<br /><div style='margin-left: 20px;'><input type='button' value='Add New Workflow' style='height:28px; width: 150px;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover' "
			+ "onclick='redefineWorkflow(-1,-1,0); showAddWorkflowDiv();' /></div><br />";
	$("#processFlows").append(htmlString);

	htmlString = "";
	// append roles to userRoles combo
	for ( var i = 0; i < roles.length; i++) {
		htmlString = htmlString + "<option value=" + roles[i].roleId + ">"
				+ roles[i].roleName + "</option>";
	}

	$("#userRoles").append(htmlString);
	$("#userRolesEscalation").append(htmlString);
	
	/*//Operation work
	htmlString = "";
	// append roles to operation combo
	for ( var i = 0; i < operations.length; i++) {
		htmlString = htmlString + "<option value=" + operations[i].operationMaster.operationMasterId + ">"
				+ operations[i].operationMaster.title + "</option>";
	}

	$("#operations").append(htmlString);
	*/
	//Screens work
	htmlString = "";
	// append roles to screen combo
	for ( var i = 0; i < screens.length; i++) {
		htmlString = htmlString + "<option value=" + screens[i].screenId + ">"
				+ screens[i].screenName + "</option>";
	}
	
	if(quantum!=null && quantum.screen!=null){
		$(".quantum-rule-div").show();
		$("#quantumText").val(" "+quantum.condition+" "+quantum.field);
	}

	$("#screens").append(htmlString);
	$("#rejectScreens").append(htmlString);
	
	//Screens work
	htmlString = "";
	// append roles to screen combo
	for ( var i = 0; i < users.length; i++) {
		htmlString = htmlString + "<option value=" + users[i].personId + ">"
				+ users[i].displayUsername + "</option>";
	}

	
	$("#persons").append(htmlString);
	$("#personsEscalation").append(htmlString);

	} catch (e) {
		console.log(e.message);
	}

}

/**
 * Gets Call When user clicks on Re-Define button.
 * 
 * @param workflowId
 *            id of the workflow to deactivate.
 * @param workflowIndex
 */
function confirmRedefine(workflowId, workflowIndex, isRedefine) {

	try {

		alertify.set({
			labels : {
				ok : "Continue",
				cancel : "Cancel"
			}
		});

		alertify.set({
			buttonReverse : true
		});

		alertify
				.confirm(
						"Adding workflow will terminate all the operations currently being executed under this process. <br /> Do you want to continue?",
						function(e) {
							if (e) {
								// user clicked "ok"
								redefineWorkflow(workflowId, workflowIndex,
										isRedefine);

								showAddWorkflowDiv();
							} else {
								// user clicked "cancel"
							}
						});

	} catch (e) {
		console.log(e.message);
	}

}

/**
 * Gets Call When user clicks on Delete button.
 * 
 * @param workflowId
 *            id of the workflow to deactivate.
 * @param workflowIndex
 */
function confirmDelete(workflowId, workflowIndex, isRedefine) {

	try {

		alertify.set({
			labels : {
				ok : "Continue",
				cancel : "Cancel"
			}
		});

		alertify.set({
			buttonReverse : true
		});

		alertify
				.confirm(
						"Worklfow will be deleted permanantly. <br /> Do you want to continue?",
						function(e) {
							if (e) {
								// user clicked "ok"
								$.ajax({
									type : "POST",
									dataType : "json",
									url : "aios-web/deleteWorkflow.action",
									traditional : true,
									data : {
										workflowId : workflowId,
									},
									success : function(response) {
										if(response.status=='SUCCESS')
											onModuleProcessSelect();
										
									},
									error : function(e) {
										// showErrorWindow(e.responseText, false);
										console.log(e.responseText);
									}

								});
								
							} else {
								// user clicked "cancel"
							}
						});

	} catch (e) {
		console.log(e.message);
	}

}
/**
 * Gets call when user confirms redefining a workflow. Sets some values to get
 * track that which workflow record was redefined.
 * <p>
 * Date: 10-Apr-13
 * 
 * @param workflowId
 *            the id of the workflow record.
 * @param workflowIndex
 * @param isRedefine
 *            false, true
 * 
 * @author Shoaib Ahmad Gondal
 */
function redefineWorkflow(workflowId, workflowIndex, isRedefine) {

	$("#selectedWorkflowId").val(workflowId);
	$("#selectedWorkflowIndex").val(workflowIndex);
	$("#isRedefine").val(isRedefine);
}

/**
 * Shows entry form to define workflow steps and hides existing workflow tables
 * div.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showProcessFlowsDiv() {
	$("#processFlows").show();
	$("#addWorkflow").hide();
}

/**
 * Shows existing workflow tables div and hides entry form to define workflow
 * steps.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showAddWorkflowDiv() {
	$("#addWorkflow").show();
	$("#processFlows").hide();
}

/**
 * Gets call when user is defining workflow and presses 'Add' button after
 * selecting Operation, Access Right, Role.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function addWorkflow() {

	try {
		if(preSubmissionValidation()){
		var operationIds=$('#operations option:selected').val().split("&");
		var operation=operationIds[0];
		var operationGroup=operationIds[1];
		var operationText = $("#operations option:selected").text();
		var screens=Number($("#screens option:selected").val());
		var rejectScreens=Number($("#rejectScreens option:selected").val());
		var screensText=$("#screens option:selected").text();
		var rejectScreensText=$("#rejectScreens option:selected").text();
		var accessRight = Number($("#accessRight").val());
		var userRole = Number($("#userRoles").val());
		var persons = Number($("#persons").val());
		var userRoleText = $("#userRoles option:selected").text();
		var personsText = $("#persons option:selected").text();
		var operationOrder = $("#operationOrder").val();
		var quantumValue=$("#quantumValue").val();
		var quantumText=$("#quantumText").val();
		var isEscalation=$("#isEscalation").attr("checked");
		var validity=Number($("#validity").val());
		var isDays=$("#isDays").attr("checked");
		var accessRightEscalation = Number($("#accessRightEscalation").val());
		var userRolesEscalation = Number($("#userRolesEscalation").val());
		var personsEscalation = Number($("#personsEscalation").val());
		var userRolesEscalationText = $("#userRolesEscalation option:selected").text();
		var personsEscalationText = $("#personsEscalation option:selected").text();
		var isReminder=$("#isReminder").attr("checked");
		var reminderDays=Number($("#reminderDays").val());
		var isReminderDays=$("#isReminderDays").attr("checked");
		
		addRowToRecentWorkflowTable(operation,operationGroup,operationText,screens,rejectScreens,screensText,rejectScreensText,accessRight,userRole,persons
				,personsText,userRoleText,operationOrder,quantumValue,quantumText,isEscalation,validity,
				isDays,accessRightEscalation,userRolesEscalation,personsEscalation,userRolesEscalationText,
				personsEscalationText,isReminder,reminderDays,isReminderDays);

		//showOperationType2Div();

		//onFirstOperationAdd();
		}else{
			return false;
		}
	} catch (e) {
		console.log(e.message);
	}
	return false;
}

function saveWorkflows() {

	if (validateWorklfowForm()) {
		confirmSave();
	}
}

function confirmSave() {

	try {

		alertify.set({
			labels : {
				ok : "Continue",
				cancel : "Cancel"
			}
		});

		alertify.set({
			buttonReverse : true
		});

		alertify.confirm("Do you want to Save the changes?", function(e) {
			if (e) {
				// user clicked "ok", submit form
				$("#workflowManagementForm").submit();

				
		/*		var processId = $('#moduleProcessesList :selected').val();

				$.ajax({
				type : "POST",
				dataType : "html",
					url : "workflow_management.action",
				traditional : true,
				data : {
						selectedWorkflowId : selectedWorkflowId,
						isRedefine : isRedefine,
						processId : processId,
						
					},
				success:function(result){
						$("#main-wrapper").html(result);
					}

				});
		 */
			} else {
				// user clicked "cancel"
			}
		});

	} catch (e) {
		console.log(e.message);
	}
	return false;
}

function validateWorklfowForm(){
	var result = true;
	var workflowTitle=$("#workflowTitle").val();
	if(workflowTitle==null || workflowTitle==''){
		alertify.alert("Please enter WOrkflow Title");
		result=false;
	}
	return result;
}

/**
 * Gets Call when user selects 'Role' or 'User' from Access Right Type combo. If
 * Role is selected, then combo containing roles will be shown, else combo with
 * person's names will be shown.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showUserOrRoleCombo() {
	var selectedValue = $("#accessRight").val();
	if (selectedValue == 0) {
		$("#userRolesDiv").show();
		$("#personsDiv").hide();
	} else if (selectedValue == 1) {
		$("#userRolesDiv").hide();
		$("#personsDiv").show();
	}
}

/**
 * Gets Call when user selects 'Role' or 'User' from Access Right For Escalation Type combo. If
 * Role is selected, then combo containing roles will be shown, else combo with
 * person's names will be shown.
 * <p>
 * Date: 30-March-14
 * 
 * @author Mohamed Rafiq 
 */
function showUserOrRoleEscalationCombo() {
	var selectedValue = $("#accessRightEscalation").val();
	if (selectedValue == 0) {
		$("#userRolesDivEscalation").show();
		$("#personsDivEscalation").hide();
	} else if (selectedValue == 1) {
		$("#userRolesDivEscalation").hide();
		$("#personsDivEscalation").show();
	}
}

/**
 * Validates process entry after entry process.
 * <p>
 * Date: 11-Apr-13
 * 
 * @returns true if validation successful else false.
 * 
 * @author Shoaib Ahmad Gondal
 */
function validateEntry() {
	var result = true;

	// check if user has entered valid operation order
	var operationOrder = $("#operationOrder").val();
	var operationOrderIntValue = parseInt(operationOrder, 0);
	if (isNaN(operationOrderIntValue)
			|| ((operationOrderIntValue <= 0) || operationOrderAlreadyDefined(operationOrderIntValue))) {
		alertify.alert("Please Enter Valid Operation Order");
		$("#operationOrder").val("");
		$("#operationOrder").focus();

		result = false;
	}

	

	return result;
}

/**
 * Gets call to validate that the entered operation order number is not already
 * entered.
 */
function operationOrderAlreadyDefined(operationOrderToCheck) {
	var result = false;

	try {
		$('#recentWorkflowTable tbody tr').each(function(index, element) {
			var operationOrder = this.childNodes[2].textContent;
			var intValue = Number(operationOrder);

			if (operationOrderToCheck === intValue) {
				result = true;
			}
		});

	} catch (e) {
		console.log(e.message);
	}

	return result;
}

/**
 * Returns size of the recently added workflows.
 * <p>
 * Date: 11-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function getRecentWorkflowListSize() {

	try {
		var size = $("#recentWorkflowListSize").val();
		var sizeIntValue = parseInt(size, 0);

		return sizeIntValue;
	} catch (e) {
		console.log(e.message);
	}
}

/**
 * Increments size by one of recently added workflow's list.
 * <p>
 * Date: 11-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function setRecentWorkflowListSize() {
	try {
		var size = $("#recentWorkflowListSize").val();
		var sizeIntValue = parseInt(size, 0);
		sizeIntValue = sizeIntValue + 1;

		$("#recentWorkflowListSize").val(sizeIntValue);
	} catch (e) {
		console.log(e.message);
	}
}

function addRowToRecentWorkflowTable(operation,operationGroup,operationText,screens,rejectScreens,screensText,rejectScreensText,accessRight,userRole,persons
		,personsText,userRoleText,operationOrder,quantumValue,quantumText,isEscalation,validity,
		isDays,accessRightEscalation,userRolesEscalation,personsEscalation,userRolesEscalationText,
		personsEscalationText,isReminder,reminderDays,isReminderDays) {

	var recentWorkflowListSize = getRecentWorkflowListSize();

	var htmlString = "<tr>";
	htmlString = htmlString
		+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'><input type='hidden' name='recentWorkflows["
		+ recentWorkflowListSize + "].operationMaster.operationMasterId' value='" + operation
		+ "' /><input type='hidden' name='recentWorkflows["
		+ recentWorkflowListSize + "].operationMaster.operationGroup' value='" + operationGroup
		+ "' />" + operationText + "</td>";
	
	var scrTemp="";
	if(rejectScreens!=null && rejectScreens!="" && rejectScreens!=0)
		scrTemp=" <\n> Reject screen : "+rejectScreensText;
	else
		rejectScreens="";
	
	htmlString = htmlString
	+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'><input type='hidden' name='recentWorkflows["
	+ recentWorkflowListSize + "].screenByScreenId.screenId' value='" + screens
	+ "' /><input type='hidden' name='recentWorkflows["
		+ recentWorkflowListSize + "].screenByRejectScreen.screenId' value='" + rejectScreens
		+ "' />" +screensText +scrTemp+ "</td>";
	
	if (accessRight==0) {
		htmlString = htmlString
				+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].role.roleName' value='"
				+ userRoleText
				+ "'/><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].role.roleId' value=" + userRole
				+ " /><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].accessRightType' value='"
				+ accessRight + "' />" + userRoleText + "</td>";
	} else {
		htmlString = htmlString
				+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].personName' value='"
				+ personsText
				+ "'/><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].personId' value='"
				+ persons + "' />" + personsText + "</td>";
	}

	htmlString = htmlString
			+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:10%'><input type='hidden' name='recentWorkflows["
			+ recentWorkflowListSize + "].operationOrder' value='"
			+ operationOrder + "' />" + operationOrder + "</td>";

	var escalationText="";
	var escalateTo="";
	if(isEscalation==true){
		escalationText="Validity : "+validity
					+(isDays==true?' Days,':' Hours');
		if (accessRightEscalation==0) {
			escalationText=escalationText+" Escalate To :"+userRolesEscalationText;
			escalateTo=userRolesEscalation;
		}else{
			escalationText=escalationText+" Escalate To :"+personsEscalationText;
			escalateTo=personsEscalation;
		}
	}else{
		escalationText="-NA-";
	}
		htmlString = htmlString
			+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:20%'>" 
			+ "<input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].isEscalation' value='"
				+ isEscalation + "' />" 
			+ "<input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].escalationAccessRightType' value='"
				+ accessRightEscalation + "' />"
			+ "<input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].validity' value='"
				+ validity + "' />" 
			+ "<input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].isDays' value='"
				+ isDays + "' />" 
			+ "<input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].escalateTo' value='"
				+ escalateTo + "' />"
				+escalationText + "</td>";
		
		
		var reminderText="";
		if(isReminder==true){
			reminderText=reminderDays+" "+(isReminderDays==true?' Days':' Hours');
		}else{
			reminderText="-NA-";
		}
		
			htmlString = htmlString
				+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:20%'>" 
				+ "<input type='hidden' name='recentWorkflows["
					+ recentWorkflowListSize + "].isReminder' value='"
					+ isReminder + "' />" 
				+ "<input type='hidden' name='recentWorkflows["
					+ recentWorkflowListSize + "].remindeBefore' value='"
					+ reminderDays + "' />" 
				+ "<input type='hidden' name='recentWorkflows["
					+ recentWorkflowListSize + "].isReminderDays' value='"
					+ isReminderDays + "' />"
					+reminderText + "</td>";
			
		if(quantumValue==null || quantumValue==''){
			quantumText="-NA-";
			quantumValue="";
		}else{
			quantumText=quantumValue+"(value)"+ quantumText;
		}
					
		htmlString = htmlString
			+ "<td style='border: 1px solid #b1b1b1 !important; padding:3px; width:20%'><input type='hidden' name='recentWorkflows["
			+ recentWorkflowListSize + "].quantumValue' value='"
			+ quantumValue + "' />" + quantumText+ "</td>";

	htmlString = htmlString + "</tr>";
	$("#recentWorkflowTable tbody").append(htmlString);
	$("#workflowTiltle-div").html($("#workflowTitle").val());
	$("#recentWorkflowTable").show();
	
	// increment size by one, as one row is added.
	setRecentWorkflowListSize();
	resetWorkflowForm();
}

function resetWorkflowForm(){
	
	setComboBoxValue("#operations", 0);
	setComboBoxValue("#screens", 0);
	setComboBoxValue("#userRoles", 0);
	setComboBoxValue("#persons", 0);
	$("#operationOrder").val(Number($("#recentWorkflowListSize").val())+1);
	$("#quantumValue").val("");
	$("#isEscalation").attr("checked",false);
	$("#validity").val("");
	$("#isDays").attr("checked",false);
	setComboBoxValue("#userRolesEscalation", 0);
	setComboBoxValue("#personsEscalation", 0);
	$("#isReminder").attr("checked",false);
	$("#reminderDays").val("");
	$("#isReminderDays").attr("checked",false);
}



/**
 * Gets call when user selects a role for entry.
 * <p>
 * Validates that the selected role shouldn't have entry right in any other
 * workflow of the same process.
 * <p>
 * Date: 31-May-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function validateSelectedRoleForEntry(selectedRole) {

	var result = true;
	try {

		// iterate existing workflows
		$('#processFlows table').each(function(index, element) {

			/*
			 * In case of redefining, skip that table
			 */
			if (isRedefine()) {
				var tableId = $("#selectedWorkflowIndex").val();

				if (!(this.id == tableId)) {
					var tr = element.childNodes[2].childNodes[0];
					var role = tr.childNodes[0].textContent;

					if (selectedRole == role) {

						result = false;
					}
				}
			} else {
				var tr = element.childNodes[2].childNodes[0];
				var role = tr.childNodes[0].textContent;

				if (selectedRole == role) {

					result = false;
				}
			}

		});

		if (!result) {
			showErrorMessage(
					"Invalid Role",
					"Following role can not be selected as it has Entry right in another workflow of same process");
		}
	} catch (e) {
		console.log(e.message);
	}

	return result;
}

function isRedefine() {
	var result = false;

	if ($("#isRedefine").val() == "true") {
		result = true;
	}

	return result;
}

function setComboBoxValue(comboBoxId, value) {
	// if (value != null) {
	// $(comboBoxId).combobox('autocomplete', value);
	// } else {
	// $(comboBoxId).combobox('autocomplete', "");
	// }
	$(comboBoxId).val(value);

	var selectedText = $(comboBoxId + ' option[value="' + value + '"]').text();

	var autoCompleteInput = $(comboBoxId).next();
	$(autoCompleteInput).val(selectedText);
}

function preSubmissionOperationValidation(){
	var result = true;
	var operationTitle=$("#operationTitle").val();
	if(operationTitle==null || operationTitle==''){
		alertify.alert("Please enter Title");
		result=false;
	}
	var operationGroup=$("#operationGroup option:selected").val();
	if(operationGroup==null || operationGroup=='' || operationGroup==0){
		alertify.alert("Please select operation group");
		result=false;
	}
	return result;
}

function preSubmissionValidation(){
	var result = true;
	var workflowTiltle=$("#workflowTitle").val();
	if(workflowTiltle==null || workflowTiltle==''){
		alertify.alert("Please enter Worklfow Title");
		result=false;
	}
	var operations=$("#operations option:selected").val();
	if(operations==null || operations=='' || operations==0){
		alertify.alert("Please select operation");
		result=false;
	}
	
	var screens=$("#screens option:selected").val();
	if(screens==null || screens=='' || screens==0){
		alertify.alert("Please select screen");
		result=false;
	}
	
	var userRoles=$("#userRoles option:selected").val();
	if(userRoles==null || userRoles=='' || userRoles==0){
		alertify.alert("Please select role");
		result=false;
	}
	return result;
}