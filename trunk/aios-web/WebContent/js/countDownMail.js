var mins;
var secs;

function mailcounter() {
 	mins = 3 * m1("00"); // change minutes here
 	secs = 0 + s1(":59"); // change seconds here (always add an additional second to your total)
 	redo1();
}

function m1(obj) {
 	for(var i = 0; i < obj.length; i++) {
  		if(obj.substring(i, i + 1) == ":")
  		break;
 	}
 	return(obj.substring(0, i));
}

function s1(obj) {
 	for(var i = 0; i < obj.length; i++) {
  		if(obj.substring(i, i + 1) == ":")
  		break;
 	}
 	return(obj.substring(i + 1, obj.length));
}

function dis1(mins,secs) {
 	var dispmail;
 	if(mins <= 9) {
 		dispmail = " 0";
 	} else {
 		dispmail = " ";
 	}
 	dispmail += mins + ":";
 	if(secs <= 9) {
 		dispmail += "0" + secs;
 	} else {
 		dispmail += secs;
 	}
 	return(dispmail);
}

function redo1() {
 	secs--;
 	if(secs == -1) {
  		secs = 59;
  		mins--;
 	}
 	document.mailcounter.dispmail.value = dis1(mins,secs); // setup additional displays here.
 	if((mins == 0) && (secs == 0)) {
		fullDomain = window.location.pathname;
		domainArray = fullDomain.split('/');
		subDomain=domainArray[1];
  		//window.alert("Time is up. Press OK to continue."); // change timeout message as required
  		//window.location.href="/"+subDomain+"/logout.do"; // redirects to specified page once timer ends and ok button is pressed
		confirmLogout();
 	} else {
 		mailcounter = setTimeout("redo1()",1000);
 	}
}
 
