

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"none",
						"alertText":" \u0645\u0646 \u062a\u0627\u0631\u064a\u062e ",
						"alertTextCheckboxMultiple":" Please select an option_ar",
						"alertTextCheckboxe":" This checkbox is required_ar"},
					"length":{
						"regex":"none",
						"alertText":"Between ",
						"alertText2":" and ",
						"alertText3": " characters allowed_ar"},
					"dateFields":{
                        "nname":"dateFields", 
                        "alertText":"This field is required_ar"} ,
				   "limitRange":{
						"alertText":"Between ",
						"alertText2":" and ",
						"alertText3": " Values allowed_ar"},
					"singlelength":{
						"regex":"none", 
						"alertText": "Enter ", 
						"alertText2":" Digits_ar"},	
					"maxCheckbox":{
						"regex":"none",
						"alertText":" Checks allowed Exceeded_ar"},	
					"minCheckbox":{
						"regex":"none",
						"alertText":" Please select ",
						"alertText2":" options_ar"},	
					"confirm":{
						"regex":"none",
						"alertText":" Your field is not matching_ar"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":" Invalid phone number_ar"},
					"faxNumber":{
							"regex":"/^[0-9\-\(\)\ ]+$/",
							"alertText":" Invalid fax number_ar"},
					"email":{
						//"regex":"/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/",

						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+\.([a-zA-Z0-9]{2,4})$/",
						"alertText":" Invalid email address_ar"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":" Invalid date, must be in YYYY-MM-DD format_ar"},
					"onlyNumber":{
						"regex":"/^[0-9\]+$/",
						"alertText":" Numbers only_ar"},
					"onlyFloat":{
						"regex":"/^[0-9]*\.?[0-9]+$/",
						"alertText":"Float Numbers only_ar"},						
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":" No special caracters allowed_ar"},	
					"ajaxUser":{
						"file":"",
						"extraData":"name=eric",
						"alertTextOk":" This user is available_ar",	
						"alertTextLoad":" Loading, please wait_ar",
						"alertText":" This user is already taken_ar"},	
					"ajaxName":{
						"file":"",
						"alertText":" This name is already taken_ar",
						"alertTextOk":" This name is available_ar",	
						"alertTextLoad":" Loading, please wait_ar"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":" Letters only_ar"},
					"validate2fields":{
    					"nname":"validate2fields",
    					"alertText":" You must have a firstname and a lastname_ar"},
    				"letters&Symbols":{
    					"regex" :"/^[a-zA-Z0-9\+\-\.\ ]+$/", 
    					"alertText":"Only certain characters allowed_ar"},
    				"lettersNum&someSymbol":{
        					"regex" :"/^[a-zA-Z0-9\-\ ]+$/", 
        					"alertText":"Contains illegal characters_ar"},
    				"cost":{
    					"regex":"/^[-+]?[0-9]{0,5}\.?[0-9]{1,4}$/",
    					"alertText":"Contains illegal characters_ar"},
    				"letterandSpclchar":{
    					"regex":"/^[A-Za-z\-\(\)\ .\/\,\&\]+$/",
    					"alertText":"Contains illegal characters_ar"},
    				"url":{
    					"regex":"/^(((ht|f){1}(tp:[/][/]){1})|((www.)))[A-Za-z0-9\.-]{3,}\.[A-Za-z]{2}$/",
    					//"regex":"/^[www.\[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}]+$/",		
    					"alertText":"Enter valid Web Address_ar"},
					"letterNumberSpace":{
						"regex":"/^[0-9a-zA-Z\ ]+$/",
						"alertText":" No special characters allowed_ar"},
					"letterNumber":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":" No special characters allowed_ar"},
					"letterAndSpace":{
						"regex":"/^[a-zA-Z\ ]+$/",
						"alertText":" No special characters allowed_ar"}
					} 
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});