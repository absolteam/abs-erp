<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param['lang'] != null}">
<fmt:setLocale value="${param['lang']}" scope="session"/>
</c:if>

<%@page import="com.aiotech.aios.system.domain.entity.Implementation"%><html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<meta http-equiv="Content-Type charset=utf-8" /> 
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<title>AIO-ERP | Mobile Sign In</title>  

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/validationEngine.jquery.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style-mobile.css" type="text/css" media="screen"/>
 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.3.2.js"></script> 
<%-- <script src="${pageContext.request.contextPath}/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.validationEngine.js" type="text/javascript"></script> --%>
 <!--[if IE 6]>
	<link href="css/ie6.css" rel="stylesheet" media="all" />
	
	<script src="js/pngfix.js"></script>
	<script>
	  /* EXAMPLE */
	  DD_belatedPNG.fix('.logo, .other ul#dashboard-buttons li a');

	</script>
	<![endif] ttt-->
	<!--[if IE 7]>
	<link href="css/ie7.css" rel="stylesheet" media="all" />
	<![endif]-->
<script type="text/javascript"> 
$(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	}else{
		if(!$.browser.mozilla){
			//alert("Hi");
			window.location.replace("<%=request.getContextPath()%>/browserRestriction.html");
			return false;
		}
	}
});
var type;

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function validateLogin()
{
	//alert(document);
	//alert(document.getElementById("empid").value);
	
	document.getElementById("empid").value = document.getElementById("temp_empid").value;
	
	if(validateEmail(document.getElementById("empid").value)) {
		// no need to concat implementation
	} else {
		document.getElementById("empid").value = document.getElementById("empid").value+ "_${THIS.implementationId}";
	}
	
	$('.error').hide();
	var result="";
	var uname=document.getElementById("empid").value;
	var pwd=document.getElementById("password").value;
	if(uname!="" && uname!=null)
	{
		$("#unameErr").hide();
		$("#pwdErr").hide();
	}
	else
	{
		$("#empid").focus();
		$("#unameErr").show();
		$("#pwdErr").hide();
		 return false;
	}	
	if(pwd!="" && pwd!=null)
	{
		$("#unameErr").hide();
		$("#pwdErr").hide();
	}
	else
	{
		$("#password").focus();
		$("#pwdErr").show();
		$("#unameErr").hide();
		 return false;
	}
	
	return true;	
}
</script>
</head>
<body id="login"> 
	<div id="wrapper">
		<div class="loginbox">
	    	<div class="loginbox_orignal">
	            <div align="center">
	                <img src="images/logo.png" alt="" />
	            </div>
	            <div class="form">
	            	<c:if test="${not empty param.f}">
						<div class="invalid">
							<span>Invalid user name or password</span> 
						</div>
					</c:if>
	            <form id="login1" action="${pageContext.request.contextPath}/login_user.do" method="post" onsubmit="return validateLogin();">
	                <input type="text" value="User Name" name="temp_username" id="temp_empid" 
	                	onfocus="if(this.value==this.defaultValue){this.value='';}" onblur="if(this.value==''){this.value=this.defaultValue;}" />
	                <input type="hidden" value="User Name" name="j_username" id="empid" 
	                	onfocus="if(this.value==this.defaultValue){this.value='';}" onblur="if(this.value==''){this.value=this.defaultValue;}" />
	                <input type="password" name="j_password" id="password" value="Password"
	                	onfocus="if(this.value==this.defaultValue){this.value='';}" onblur="if(this.value==''){this.value=this.defaultValue;}" />
	                <div class="button"><input type="image" src="images/signin_mobile.png" /></div>
	        	</form>
	            </div>
	            <!-- <div class="beta"><img src="images/beta.png" alt="" /></div> -->
	        </div>
		    <div class="terms">
		    		&copy; 2014 All In One Solutions, All Rights Reserved.
					<br />
		            <a href="#">Terms of Services</a> | <a href="#">Privacy Policy</a> | Build 3.0.39
		    </div>
	    </div>
	</div>
	<div class="termsbot" align="center">
    	<img src="images/shadow.png" alt="" />
    </div>   
  </body>
</html>