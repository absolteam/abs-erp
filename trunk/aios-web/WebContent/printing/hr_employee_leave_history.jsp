<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Leave History</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
</style>
<body style="margin-top: 15px; font-size: 12px;">

	<c:forEach var="MAIN" items="${LEAVE_MASTER}">

		<div class="width100 float-left">
			<span class="heading">Leave History</span>
		</div>
		<!-- <div class="width70 float-left">
			<span class="side-heading">Filter Condition</span>
		</div> -->
		<div class="width30 float-right">
			<span class="side-heading">Printed on :${MAIN.createdDate}</span>
		</div>


		<c:forEach var="EMPLOYEE" items="${MAIN.subList}">
			<div class="width100 float-left">
				<div class="data-container width40 float-left">

					<div>
						<label class="caption width30">Employee</label> <label
							class="data width50">${EMPLOYEE.personName}</label>
					</div>
					<div>
						<label class="caption width30">Company</label> <label
							class="data width50">${EMPLOYEE.companyName}</label>
					</div>
					<div>
						<label class="caption width30">Department</label> <label
							class="data width50">${EMPLOYEE.departmentName}</label>
					</div>
					<div>
						<label class="caption width30">Location</label> <label
							class="data width50">${EMPLOYEE.locationName}</label>
					</div>
				</div>
				<%-- <div class="data-container width40 float-left">
					<div>
						<label class="caption width30">History From</label> <label
							class="data width20">${EMPLOYEE.fromDate}</label> <label
							class="caption width30">UpTo</label> <label class="data width20">${EMPLOYEE.toDate}</label>
					</div>
				</div> --%>
			</div>
			<div class="width100 float-left">
				<span class="side-heading">Leave History Information</span>
			</div>
			<div class="data-list-container width100 float-left"
				style="margin-top: 5px; margin-bottom: 30px">
				<c:choose>
					<c:when test="${fn:length(EMPLOYEE.leaveHistoryList)gt 0}">
						<table class="width100">
							<thead>
								<tr>
									<th>Leave Type</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Number of Day(s)</th>
									<th>Contact on Leave</th>
									<th>Leave Status</th>
									<th>Request Status</th>
									<th>Requested Date</th>
									<th>Reason</th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${EMPLOYEE.leaveHistoryList}"
									var="result3" varStatus="status1">
									<tr>
										<td>${result3.leaveType }</td>
										<td>${result3.fromDate}</td>
										<td>${result3.toDate}</td>
										<td>${result3.numberOfDays }</td>
										<td>${result3.contactOnLeave }</td>
										<td>${result3.statusStr}</td>
										<td>${result3.isApprove }</td>
										<td>${result3.createdDate }</td>
										<td>${result3.reason}</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
						
					</c:when>
					<c:otherwise>
						No Leaves
					</c:otherwise>
					
				</c:choose>
			</div>
			
		</c:forEach>
		<div style="height: 20px;">&nbsp;</div>
	</c:forEach>

</body>
</html>