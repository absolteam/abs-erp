<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
	height: 28px;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}

.align-left {
	text-align: left !important;
}

.lstln td {
	border-top: 3px double #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 1px double #000;
	font-weight: bold;
}
.fstln td {
	border-bottom: 3px double #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-top: 1px double #000;
	font-weight: bold;
}
.width68{
	width: 68%;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"
			style="font-family: book; font-style: italic; text-transform: inherit !important;
				 font-size: 28px !important; color: black;"><strong>${THIS.companyName}</strong>
		</span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading" style="font-family: book;"><span><u>
					SUPPLIER AGEING ANALYSIS </u> </span> </span>
	</div>
	<div style="clear: both;"></div>
	<div class="clear-fix"></div>
	<div class="width100 float_right" style="text-align: right;">
		<div class="width100 float_right">
			<span class="side-heading width25" style="float: right;">
				<span class="width68 float_left">STATEMENT DATE :</span>
				<span style="float: left; margin-left: 5px;"> ${requestScope.PRINT_DATE}</span>
			</span> 
		</div> 
		<div class="width100 float_right">
			<span class="side-heading width25" style="float: right;">
				<span class="width68 float_left">STATEMENT BY :</span>
				<span style="float: left; margin-left: 5px;">  ${requestScope.DUEDATE_INVOICE}</span>
			</span> 
		</div> 
	</div>
	<div class="clear-fix"></div> 
	<c:choose>
		<c:when test="${AGEING_ANALYSIS ne null && AGEING_ANALYSIS.invoiceVOs ne null 
			&& fn:length(AGEING_ANALYSIS.invoiceVOs) > 0}">
			<c:forEach items="${AGEING_ANALYSIS.invoiceVOs}" var="mainData">
				<div
					style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px;
						margin-bottom:10px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
					class="width100 float_left">
					<table style="width: 99%; margin: 0 auto;">
						<tr class="fstln">
							<td style="font-weight: bold;">SUPPLIER NAME</td>
							<td style="font-weight: bold;">PAYMENT TERM</td>
							<td style="font-weight: bold;">INVOICE DATE</td>
							<td style="font-weight: bold;">DUE DATE</td>
							<td style="font-weight: bold;">INVOICE REF</td>
							<td style="font-weight: bold;">INVOICE NUMBER</td>
							<td style="font-weight: bold;">OUTSTANDGING INVOICE</td>
							<td style="font-weight: bold;">CURRENT</td>
							<td style="font-weight: bold;">1-30 AGED</td>
							<td style="font-weight: bold;">31-60 AGED</td>
							<td style="font-weight: bold;">61-90 AGED</td>
							<td style="font-weight: bold;">AGED &gt;90</td>
						</tr>
						<c:forEach items="${mainData.invoiceVOs}" var="ageingData">
							<tr>
								<td>${ageingData.supplierName}</td>
								<td>${ageingData.paymentTermStr}</td>
								<td>${ageingData.invoiceDate}</td>
								<td>${ageingData.dueDate}</td>
								<td>${ageingData.invoiceReference}</td>
								<td>${ageingData.invoiceNumber}</td>
								<td style="text-align: right;">${ageingData.balanceAmount}</td>
								<c:choose>
									<c:when test="${ageingData.group0}">
										<td style="text-align: right;">${ageingData.invoiceAmountStr}</td>
										<c:forEach begin="1" end="4" step="1">
											<td />
										</c:forEach>
									</c:when>
									<c:when test="${ageingData.group1}">
										<td />
										<td style="text-align: right;">${ageingData.invoiceAmountStr}</td>
										<c:forEach begin="1" end="3" step="1">
											<td />
										</c:forEach>
									</c:when>
									<c:when test="${ageingData.group2}">
										<c:forEach begin="1" end="2" step="1">
											<td />
										</c:forEach>
										<td style="text-align: right;">${ageingData.invoiceAmountStr}</td>
										<c:forEach begin="1" end="2" step="1">
											<td />
										</c:forEach>
									</c:when>
									<c:when test="${ageingData.group3}">
										<c:forEach begin="1" end="3" step="1">
											<td />
										</c:forEach>
										<td style="text-align: right;">${ageingData.invoiceAmountStr}</td>
										<td />
									</c:when>
									<c:when test="${ageingData.group4}">
										<c:forEach begin="1" end="4" step="1">
											<td />
										</c:forEach>
										<td style="text-align: right;">${ageingData.invoiceAmountStr}</td>
									</c:when>
								</c:choose>
							</tr>
						</c:forEach> 
						<tr class="lstln">
							<td colspan="6" style="text-align: right; font-weight: bold;">TOTAL</td>
							<td style="text-align: right; font-weight: bold;">${mainData.totalInvoiceStr}</td>
							<td style="text-align: right; font-weight: bold;">${mainData.groupAmount0}</td>
							<td style="text-align: right; font-weight: bold;">${mainData.groupAmount1}</td>
							<td style="text-align: right; font-weight: bold;">${mainData.groupAmount2}</td>
							<td style="text-align: right; font-weight: bold;">${mainData.groupAmount3}</td>
							<td style="text-align: right; font-weight: bold;">${mainData.groupAmount4}</td>
						</tr>
					</table>
				</div>
			</c:forEach>
			<table style="width: 30%; position: relative; left: 8px;">
				<tr class="fstln">
					<td colspan=2>TOTAL SUMMARY</td> 
				</tr>
				<tr>
					<td>OUTSTANDING</td>
					<td style="text-align: right;">${AGEING_ANALYSIS.totalInvoiceStr}</td>
				</tr>
				<tr>
					<td>CURRENT</td>
					<td style="text-align: right;">${AGEING_ANALYSIS.groupAmount0}</td>
				</tr>
				<tr>
					<td>1-30 AGED</td>
					<td style="text-align: right;">${AGEING_ANALYSIS.groupAmount1}</td>
				</tr>
				<tr>	
					<td>31-60 AGED</td>
					<td style="text-align: right;">${AGEING_ANALYSIS.groupAmount2}</td>
				</tr>
				<tr>
					<td>61-90 AGED</td>
					<td style="text-align: right;">${AGEING_ANALYSIS.groupAmount3}</td>
				</tr>
				<tr>
					<td>AGED &gt;90</td>
					<td style="text-align: right;">${AGEING_ANALYSIS.groupAmount4}</td>
				</tr>  
			</table>
		</c:when>
		<c:otherwise>
			<div style="color: red;">No result found</div>
		</c:otherwise>
	</c:choose> 
	<div class="clear-fix"></div> 
</body>
</html>