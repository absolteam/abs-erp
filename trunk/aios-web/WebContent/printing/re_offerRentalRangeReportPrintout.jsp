<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Contract List</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Offer Report</span></div>
	<div class="width100 float-left"><span class="side-heading">Filter Condition</span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Rental Range From: </label>
					<label class="data width50">${startDate}</label>
				</div>
				
		</div>
	</div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">
			<div>
				<label class="caption width30">Rental Range Between: </label>
				<label class="data width50">${endDate}</label>
			</div>	
		</div>
		
	</div>
	<div class="width100 float-left"><span class="side-heading">Offer Details</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Offer Number</th> 
						<th>Tenant</th>
						<th>Offer Date</th>
						<th>Offer Period</th>
						<th>Created By</th>	
						<th>Rent</th>					
						<th>Details</th>
					</tr> 
				</thead> 
				<tbody >	
				<c:choose>
				<c:when test="${fn:length(requestScope.PRINT_OFFER_LIST) gt 0}">
					<c:forEach items="${requestScope.PRINT_OFFER_LIST}" var="OFFER_LIST">
					 	<tr> 
							<td>${OFFER_LIST.offerNumber}</td>
							<td>${OFFER_LIST.tenantName}</td>
							<td>${OFFER_LIST.offerDate}</td>
							<td>${OFFER_LIST.startDate} - ${OFFER_LIST.endDate}</td>
							<td>${OFFER_LIST.createdBy}</td>
							<td>${OFFER_LIST.totalOfferAmount}</td>							
							<td>
								<c:choose>
									<c:when test="${OFFER_LIST.offerDetails ne null}">
										<c:forEach items="${OFFER_LIST.offerDetails}" var="OFFER_DETAILS">
											Offered: <strong>${OFFER_DETAILS.unit.unitName}</strong> of 
													 <strong> ${OFFER_DETAILS.unit.component.property.propertyName}</strong><br/>
											Offer Amount: ${OFFER_DETAILS.offerAmount} <br/>
										</c:forEach>
									</c:when>
									<c:otherwise>
											No Details
									</c:otherwise>
								</c:choose>
							</td>	
						</tr>
					</c:forEach>
				  </c:when>
				  <c:otherwise>
				  		<tr class="even rowid">Offers not found</tr>
				  </c:otherwise>
				  </c:choose>	
				</tbody>
			</table>
		
		</div>	
		
	</body>
</html>