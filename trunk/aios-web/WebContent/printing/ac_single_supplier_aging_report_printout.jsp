<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Supplier Aging Report</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>SUPPLIER AGING REPORT</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>
	<div style="position: relative; top: 10px; height: 100%;"
		class="width98 float_left">
		<table class="width100">

			<tbody>

				<tr>
					<td style="text-align: left;">
						<div style="width:70%;">
							<label>Supplier Name :</label><span>${SUPPLIER.supplierName}</span></br>
						
							<label>Number :</label><span>${SUPPLIER.supplierNumber}</span></br>
						
							<label>Supplier Type :</label><span>${SUPPLIER.type}</span></br>
						
							<label>Branch :</label><span>${SUPPLIER.branchName}</span></br>
						
							<label>Payment Term :</label><span>${SUPPLIER.paymentTermName}</span></br>
						
							<label>Bank Name :</label><span>${SUPPLIER.bankName}</span>
							
						</div>

					</td>
					

				</tr>

				<tr>

					<td style="border:none; text-align: left; font-size: 20px; text-decoration:underline; padding-top: 5px; padding-bottom: 5px;">Payments:</td>
				</tr>

				<c:forEach var="payment" items="${SUPPLIER.paymentsList}">
					<tr>
						<td colspan="5">

							<table style="width: 100%; border-collapse: collapse;">

								<tr>

									<td style="text-align: left; width: 33%; border: none;"><label>Invoice
											#:</label><span>${payment.invoiceNumber}</span></td>
									<td style="text-align: left; width: 33%; border: none;"><label>Payment
											Date :</label><span>${payment.paymentDate}</span></td>
									<td style="text-align: left; width: 34%; border: none;"><label>Cheque
											#:</label><span>${payment.chequeNumber}</span></td>

								</tr>
								<tr>

									<td style="text-align: left; border: none;"><label>Discount
											Received:</label><span>${payment.discountReceived}</span></td>
									<td style="text-align: left; border: none;"><label>Cheque
											Date :</label><span>${payment.chequeDate}</span></td>


								</tr>
								<tr>
									<td style="text-align: left; border: none;" colspan="3"><label>Description:</label><span>${payment.paymentDescription}</span></td>
								</tr>

							</table>

						</td>

					</tr>
					<tr>
						<td colspan="5" style="border:none;padding: 0px;">

							<table style="margin:none;">

								<tr>

									<th style="width: 34%;">Product Name</th>
									<th style="width: 33%;">Quantity</th>
									<th style="width: 33%;">Unit</th>
								</tr>


								<c:forEach items="${payment.paymentsDetailsList}" var="detail"
									varStatus="status1">
									<tr>
										<td>${detail.productName }</td>
										<td>${detail.quantity }</td>
										<td>${detail.unitRate }</td>
									</tr>

								</c:forEach>

							</table>
						</td>
					</tr>
					<tr>
						<td style="border:none;" colspan="5">&nbsp;</td>

					</tr>
					<tr>
						<td style="border:none;" colspan="5">&nbsp;</td>

					</tr>

				</c:forEach>










				<tr>

					<td style="border:none; text-align: left; font-size: 20px; text-decoration:underline; padding-top: 5px; padding-bottom: 5px;">Direct Payments:</td>
				</tr>

				<c:forEach var="payment" items="${SUPPLIER.directPaymentsList}">
					<tr>
						<td colspan="5">

							<table style="width: 100%; border-collapse: collapse;">

								<tr>


									<td style="text-align: left; width: 33%; border: none;"><label>Payment
											#:</label><span>${payment.directPaymentNumber}</span></td>
									<td style="text-align: left; width: 33%; border: none;"><label>Payment
											Date :</label><span>${payment.directPaymentDate}</span></td>
									<td style="text-align: left; width: 34%; border: none;"><label>Payment Cheque Date
											:</label><span>${payment.directPaymentChequeDate}</span></td>

								</tr>
								
								<tr>
									<td style="text-align: left; border: none;" colspan="3"><label>Others:</label><span>${payment.others}</span></td>
								</tr>

							</table>

						</td>

					</tr>
					<tr>
						<td colspan="5" style="border:none;padding: 0px;">

							<table style="margin:none;">

								<tr>

									<th style="width: 34%;">Invoice Number</th>
									<th style="width: 33%;">Amount</th>
									<th style="width: 33%;">Description</th>
								</tr>


								<c:forEach items="${payment.directPaymentsDetailsList}" var="detail"
									varStatus="status1">
									<tr>
										<td>${detail.directPaymentDetailInvoiceNumber }</td>
										<td>${detail.directPaymentAmount }</td>
										<td>${detail.directPaymentDetailDescription }</td>
									</tr>

								</c:forEach>

							</table>
						</td>
					</tr>
					<tr>
						<td style="border:none;" colspan="5">&nbsp;</td>

					</tr>
					<tr>
						<td style="border:none;" colspan="5">&nbsp;</td>

					</tr>

				</c:forEach>




























			</tbody>
		</table>
	</div>
	<div class="clear-fix"></div>
</body>
</html>




