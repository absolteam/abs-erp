<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Item Issuance</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>ITEM ISSUANCE SLIP</span></span></div>
	<div style="clear: both;"></div>	
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: dotted; border-width: 1px; -moz-border-radius: 0px; 
		 -webkit-border-radius:0px; border-radius: 0px;" class="width98 float_left">
		
		<div style="color: #333; float: left; padding: 5px;">
			<div><b>Date:</b> ${ISSUE_REQUISTION.issueDate}</div>
			<div><b>Issuer:</b> ${ISSUE_REQUISTION.person.firstName} (employee-code: ${ISSUE_REQUISTION.person.personNumber})</div>
			<div><b>Requisition Reference:</b> ${ISSUE_REQUISTION.requisition.referenceNumber}</div>
		</div>
		<div style="color: red; float: right; padding: 5px;"><b>No:</b> ${ISSUE_REQUISTION.referenceNumber}</div>
				
		<table style="width: 99%; margin: 0 auto;">
				<tr>  
					<th>S. No.</th>
					<th>Item Code</th>
					<th>Description of Article</th>
					<th>Unit</th>	
					<th>Quantity</th>	
				</tr>
				<c:set var="cnt" value="1" />
				<c:forEach items="${ISSUE_REQUISTION_DETAIL}" var="details">
				<tr> 
					<td>
						${cnt}
						<c:set var="cnt" value="${cnt + 1}" />
					</td>
					<td>
						${details.product.code}
					</td>
					<td>
						${details.product.productName}
					</td>
					<td>
						${details.unitNameStr}
					</td>
					<td>
						${details.quantity}
					</td>							
				</tr>
				</c:forEach>
				<tr> 
					<td>
						&nbsp;
					</td>
					<td>
						
					</td>
					<td>
						
					</td>
					<td>
						
					</td>
					<td>
						
					</td>							
				</tr>
				<tr> 
					<td colspan="4" align="right" style="text-align: right !important; padding-right: 5px;">
						Total Quantity
					</td>						
					<td>
						${TOTAL_QTY}
					</td>							
				</tr>
			</table>
			<br/> 
			<br/>
			<table width="80%">
				<tr valign="bottom">
					<td style="border: none;" width="33%" align="center">
						<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;">Operations Manager</span>
					</td>	
					<td style="border: none;" width="33%" align="center">
						<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;">Issuer / Store Keeper</span>
					</td>
					<td style="border: none; height: 50px;" width="33%" align="center">
						<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;">Received By</span>
					</td>									
				</tr>
			</table>
	</div>  
	<div class="clear-fix"></div>
	</body>
</html>