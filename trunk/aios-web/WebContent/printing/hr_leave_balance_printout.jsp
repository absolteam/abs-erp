<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Leave Balance</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	
	<div class="width40 float-right"><span class="side-heading" ><span><fmt:message key="hr.attendance.printedon"/> : </span><span>${printedOn}</span> </span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(LEAVE_RECONCILIATION)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Name</th>
						<th>Number</th>
						<th>Designation</th>
						<th>Department</th>
						<th>Joining Date</th>
						<th>Leave Type</th>
						<th>Provided Days</th>
						<th>Eligible Days</th>
						<th>Taken Days</th>
						<th>Balance Days</th>
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${LEAVE_RECONCILIATION}" var="result" varStatus="status1" >
					 	<tr> 
					 		<td>${result.personName}</td>
							<td>${result.personNumber}</td>
							<td>${result.designationName}</td>
							<td>${result.departmentName}</td>
							<td>${result.joiningDate}</td>
							<td>${result.leaveType}</td>
							<td align="right">${result.providedDays}</td>
							<td align="right">${result.eligibleDays}</td>
							<td align="right">${result.takenDays}</td>
							<td align="right">${result.availableDays}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid"><fmt:message key="hr.attendance.norecord"/></tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>