<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
@media print {
	.hideWhilePrint {
		display: none;
	}
}
</style>
<script type="text/javascript"> 
 
 </script>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div id="main-content">
		<div class="width100 float-left">
			<span class="heading">Issuance Analysis Report</span>
		</div>
		<div class="width100 float-left">
				
				<table class="width100">
					<tr height="25px;">
						<td>From : ${fromDate}</td>
						<td>To : ${toDate}</td>
						<td>Description :</td>
						<td colspan="2" style="font-weight:bold;width:40%;">${otherInfo}</td>
						<td>Printed On :</td>
						<td style="font-weight:bold;width:10%;">${printedOn}</td>
					</tr>
					<c:choose>
						<c:when test="${fn:length(PRODUCT_LIST)>0}">
							<tr style="margin-top: 10px;">
								<td colspan="8" align="right">
									<table style="width:100%;">
										<tr>
											<th style="font-weight:bold;">Code</th>
											<th style="font-weight:bold;">Description</th>
											<th style="font-weight:bold;">Unit Rate</th>
											<th style="font-weight:bold;">Issued Quantity</th>
											<th style="font-weight:bold;">Issue Sub Total</th>
											<th style="font-weight:bold;">Issue Reference</th>
											<th style="font-weight:bold;">Issue Date</th>
											<c:forEach var="entry" items="${PRODUCT_MASTER}">
											 <th style="font-weight:bold;"> <c:out value="${entry.value}"/></th>
											</c:forEach>
										</tr>
										<c:forEach var="detail" items="${PRODUCT_LIST}"
											varStatus="status1">
											<tr>
												<td>${detail.code}</td>
												<td>${detail.productName}</td>
												<td>${detail.unitRate}</td>
												<td>${detail.issuedQuantity}</td>
												<td>${detail.issuedTotalAmount}</td>
												<td>${detail.issuanceNumbers}</td>
												<td>${detail.issuanceDates}</td>
												<c:forEach var="entry1" items="${PRODUCT_MASTER}">
													<td>
														<c:forEach var="entry2" items="${detail.issueRequistionDetailVOWithoutList}">
															<c:if test="${entry1.key eq entry2.key}">
																${entry2.value.quantity}
															</c:if>
														</c:forEach>
													</td>
												</c:forEach>
											</tr>
										</c:forEach>
									</table>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr style="margin-top: 10px;">
								<td colspan="8" align="left">
									No record found.
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</table>

		</div>
	</div>
</body>