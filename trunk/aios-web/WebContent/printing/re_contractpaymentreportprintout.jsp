<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Contract Payment List</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Contract Payment Report</span></div>
	<div class="width100 float-left"><span class="side-heading">Filter Criteria</span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Payment Status</label>
					<label class="data width50">${PAYMENT_MASTER.paymentStatus}</label>
				</div>
			</div>
			<div class="data-container width50 float-left">
				<div>
					<label class="caption width30">Date From</label>
					<label class="data width50">${PAYMENT_MASTER.fromDate}</label>
				</div>
				<div>
					<label class="caption width30">Date To</label>
					<label class="data width50">${PAYMENT_MASTER.toDate}</label>
				</div>	
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading">Payment Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(PAYMENT_LIST)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Contract Number</th>
						<th style="padding-left: 100px; padding-right: 100px;">Tenant Name</th> 
						<th>Property Name</th>
						<th>Unit</th>
						<th>Payment Type</th>
						<th>Payment Mode</th>
						<th>Amount</th>
						<th>Bank Name</th>
						<th>Cheque Date</th>
						<th>Cheque No.</th>
						<th>Payment Status</th>
					</tr> 
				</thead> 
				<tbody >	
				
					<c:forEach items="${PAYMENT_LIST}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.contractNumber }</td>
							<td>${result3.tenantName }</td>
							<td>${result3.buildingName }</td>
							<td>${result3.flatNumber }</td>
							<td>${result3.paymentType }</td>
							<td>${result3.paymentMethod}</td>
							<td>${result3.chequeAmount }</td>
							<td>${result3.bankName }</td>
							<td>${result3.chequeDate }</td>
							<td>${result3.chequeNumber }</td>
							<td>${result3.paymentVisibility }</td>
						</tr>
					</c:forEach>
				 
				</tbody>
			</table>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Payment</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>