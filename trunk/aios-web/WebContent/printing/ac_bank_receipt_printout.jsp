<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Payment Receipt</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PAYMENT RECEIPT</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width60 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">RECEIPT TYPE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${BANK_RECEIPT.receiptType}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">RECEIPT SOURCE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${BANK_RECEIPT.receiptSourceType}</span>
			</div>
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">SOURCE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${BANK_RECEIPT.receiptSourceName}</span>
			</div>  
		</div>  
		<div class="width40 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">RECEIPT NO. <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					 ${BANK_RECEIPT.receiptsNo}
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">DATE <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					${BANK_RECEIPT.receiptsDate}
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">CURRENCY<span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					 ${BANK_RECEIPT.currencyName}
				</span>
			</div>
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>PAYMENT MODE</th>
				<th>INSTITUTION</th>
				<th>CHEQUE NUMBER</th> 
				<th>CHEQUE DATE</th>
				<th>REFERENCE</th>
				<th>AMOUNT</th>
				<th>STATUS</th>
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:forEach begin="0" end="6" step="1">
						<td/>
					</c:forEach>
				</tr>
				<c:forEach items="${BANK_RECEIPT.bankReceiptsDetailVOs}" var="DETAIL" varStatus="status"> 
					<tr> 
						<td>
							${DETAIL.paymentModeName}
						</td>
						<td>
							${DETAIL.institutionName}
						</td>
						<td>
							${DETAIL.bankRefNo}
						</td>
						<td>
							${DETAIL.strChequeDate}
						</td>
						<td>
							${DETAIL.referenceNo}
						</td>
						<c:set var="linesamount" value="${DETAIL.amount}"/>  
						<td class="right_align">
							<%=AIOSCommons.formatAmount(pageContext.getAttribute("linesamount"))%> 
						</td> 
						<td>${DETAIL.paymentStatusName}</td> 
					</tr>
				</c:forEach> 
				<c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="6" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	</body>
</html>