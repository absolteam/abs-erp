<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sales Profit Report</title>
<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
	padding: 2px;
	width: 12.5%;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
.total {
	font-size: 11px;
	font-weight: bold;
}
</style>


<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<div class="width98">
			<span class="heading"><span
				style="font-size: 25px !important;">${THIS.companyName}</span> </span>
		</div>
	</div>
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span>O2C DAILY SALES PROFIT</span> </span>
	</div>
	<div class="width100">
		<span class="side-heading" style="float: right; padding: 5px;">Printed
			on : ${requestScope.PRINT_DATE}</span>
	</div>
	<div class="clear-fix"></div>
	
	<div class="data-list-container width100 float-left"
			style="margin-top: 10px;">
		<fieldset style="background-color: #F5ECCE;">
			
		<table> 
			
			<c:forEach items="${requestScope.SALE_MAP}" var="sales">	
				
				<c:set var="dayTotalQuantity" value="${0}" />
				<c:set var="dayTotalSale" value="${0}" />
				<c:set var="dayTotalCost" value="${0}" />
				<c:set var="dayTotalGain" value="${0}" />
				<c:set var="dayTotalDiscount" value="${0}" />
				
				<table style="margin-top: 10px;">
					<tr>
						<td style="font-weight: bold; font-size: 13px;">SALE DATE: ${sales.key}</td>
					</tr>
					<tr>
						<td>
							<table>
								<tr>
									<th>Sales Ref.</th>
									<th>Customer</th>
									<th>Product Code</th>
									<th>Product</th>
									<th>Unit Price</th>
									<th>Quantity</th>	
									<th>Discount</th>
									<th>Total</th>
									<th>Cost</th>
									<th>Balance</th>
								</tr>
								<c:forEach items="${sales.value}" var="details">
									<tr>
										<td>${details.salesInvoice.referenceNumber}</td>
										<td>${details.customerName}</td>
										<td>${details.productCode}</td>
										<td>${details.productName}</td>
										<td>${details.salesDeliveryDetail.unitRate}</td>
										<td>${details.invoiceQuantity}</td>	
										<td>${details.discountValue} 
											<c:if test="${details.discountValue ne 0.0}">
												<span style="font-size: 8px; float: right; font-weight: bold;">${details.discountType}</span>
											</c:if>	
										</td>						
										<td>${details.total}</td>
										<td>${details.productCostPrice}</td>
										<td>${details.saleGain}</td>
										
										<c:set var="dayTotalQuantity" value="${dayTotalQuantity + details.invoiceQuantity}" />
										<c:set var="dayTotalSale" value="${dayTotalSale + details.total}" />
										<c:set var="dayTotalCost" value="${dayTotalCost + details.productCostPrice}" />
										<c:set var="dayTotalGain" value="${dayTotalGain + details.saleGain}" />
										<c:set var="dayTotalDiscount" value="${dayTotalDiscount + details.discountValue}" />
									</tr>
								</c:forEach>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td class="total">QUANTITY: <fmt:formatNumber type="number" maxFractionDigits="2" value="${dayTotalQuantity}" /></td>		
									<td class="total">DISCOUNT: <fmt:formatNumber type="number" maxFractionDigits="2" value="${dayTotalDiscount}" /></td>				
									<td class="total">SALE: <fmt:formatNumber type="number" maxFractionDigits="2" value="${dayTotalSale}" /></td>
									<td class="total">COST: <fmt:formatNumber type="number" maxFractionDigits="2" value="${dayTotalCost}" /></td>
									<td class="total">GAIN: <fmt:formatNumber type="number" maxFractionDigits="2" value="${dayTotalGain}" /></td>
								</tr>	
							</table>
						</td>
					</tr>
				</table>																		
			</c:forEach>
			<table style="margin-top: 10px; display:none;">
				<tr style="background: #FFFADE;">
					<td style="font-weight: bold; font-size: 13px; background: #FFFADE;" colspan="8">NET TOTAL</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="total">NET QUANTITY:<br/> ${TOTAL_QTY}</td>	
					<td class="total">NET DISCOUNT:<br/> ${TOTAL_DISCOUNT}</td>						
					<td class="total">NET SALE:<br/> ${TOTAL_SALES_AMOUNT}</td>
					<td class="total">NET COST:<br/> ${TOTAL_COST_AMOUNT}</td>
					<td class="total">NET GAIN:<br/> ${TOTAL_GAIN_AMOUNT}</td>
				</tr>
			</table>
		</table>
		</fieldset>
	</div>
	<div class="clear-fix"></div>
</body>
</html>