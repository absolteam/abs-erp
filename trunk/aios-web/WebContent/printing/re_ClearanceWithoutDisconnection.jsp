﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Offer Details</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />	
	<style type="text/css">
		td {
			border: thin !important;
			border-style: solid !important;
			border-color: black !important;
			text-align: left !important;
		}
	</style>
</head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>		
<script type="text/javascript">
	$(function() {
		var account_Nos = "${accountNo}";
		var accountsLength = account_Nos.split(",");
		//alert(accountsLength.length);
		if(accountsLength.length > 1) {
			$("#single_account").hide();
			$("#multi_account_table").show();
			$("#multi_account").show();
		} else {
			$("#single_account").show();
			$("#multi_account_table").hide();
			$("#multi_account").hide();
		}		
	});
</script>
<body>	
	<div class="AIOTitle">
	</div>

	<div style="margin-top: 50px;" align="right">
		<p> <%= AIOSCommons.getCurrentTimeStamp().split(" ")[0] %>  <span>:التاريخ </span> </p>	
	</div>
	
	<div style="margin-top: 50px; display: block; width: 100%;">			
		<span class="bold" style="float: left; margin-left: 180px;">.. المحترمين </span>
		<span class="bold" style="float: right;">السادة / شركة أبوظبي للتوزيع </span>	
	</div>
	
	<div style="margin-top: 100px; display: block;" align="right">	
		<p class="bold">.. تحية طيبة وبعد </p>	
	</div>
	
	<div style="margin-top: 90px;" align="center">
		<p class="bold"><u> الموضوع : اصدار براءة ذمة بدون فصل </u></p>		
	</div>
	
	<div id="single_account" style="margin-top: 20px; width: 77%; margin: 0 auto;" align="center">
		<p style="direction: rtl; line-height: 2.0; text-align: right;">
	  		<span> بالإشارة إلى الموضوع أعلاه ، يرجى التكرم بعمل اللازم للاشتراك  </span>  <span> (${accountNo}) </span>   <span> 	، وذلك بإصدار براءة ذمة بدون فصل ، للعقار  </span> <span>	${buildingName} - ${generalServiceType}</span> <span> في </span><span>${addressLine1}.</span>	   
		</p> 
	</div>
	
	<div id="multi_account" style="margin-top: 20px; width: 77%; margin: 0 auto;" align="center">
		<p style="direction: rtl; line-height: 2.0; text-align: right;">
	  		<span> بالإشارة إلى الموضوع أعلاه ، يرجى التكرم بعمل اللازم للإشتراكات الموضحة أدناه ، وذلك بإصدار براءة ذمة بدون فصل للتيار  للعقار  </span> <span>	${buildingName} - ${generalServiceType}</span> <span> في </span><span>${addressLine1}.</span>	   
		</p> 
	</div>
	
	<div id="multi_account_table" style="margin-top: 20px; width: 77%; margin: 0 auto;" align="center">
		<p style="text-align: right;"> : أرقام الاشتراكات  </p>
		<br/>
		<%
			String accounts = (String) request.getAttribute("accountNo");
			String[] accountNos;
			int maxColumns = 6, i = 0; 
			if(accounts != null) {
				
				accountNos = accounts.split(","); %>
	
				<table style="width: 100%;">
					<tr>
						<%  for(String accountNo : accountNos) { 
							if(i++ == maxColumns) { 
								i = 1; %>
								</tr>
								<tr>
								
							<% } %>
								<td>
									<%= accountNo %> 
								</td>							
						<% } %>
					</tr>
				</table>
				<% } %>
	</div>
			
	<div style="margin-top: 30px;" align="center">
		<p>.. و تفضلوا بقبول فائق الاحترام والتقدير  </p>
	</div>
	
	<div style="padding: 5px; margin-left: 0px; margin-top: 60px; width: 240px; text-align: right;" >
		<span>: مقدم الطلب  </span>
		<br/><br/>
		<span>  ${ownerName } </span>
	</div>

</body>
</html>