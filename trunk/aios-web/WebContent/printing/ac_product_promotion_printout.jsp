<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Journal Voucher</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PRODUCT PROMOTION REPORT</span></span></div>
	<div style="clear: both;"></div>
		<c:forEach items="${PROMOTION}" var="promotion">
			<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
				 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
				<div class="width60 float_right">
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">REWARD TYPE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${promotion.rewardTypeName}</span>
						</span>  
					 </div>
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">PRICING TYPE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${promotion.priceListName}</span>
						</span>  
					 </div>
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">MINIMUM SALE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${promotion.mininumSalesName}</span>
						</span>  
					 </div>
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">PROMOTION OPTION<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${promotion.promotionOptionName}</span>
						</span>  
					 </div> 
				</div>  
				<div class="width40 float_left">
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">PROMOTION NAME<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
							${promotion.promotionName}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">START DATE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
							${promotion.strStartDate}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">END DATE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
							${promotion.strEndDate}
						</span>
					</div>
				</div> 
			</div>
		
	
		<div class="clear-fix"></div>
		<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
			 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
			<table class="width100">
			<c:if test="${promotion.promotionOptionName == 'Customer'}">
				<tr>  
					<th>CUSTOMER REFERENCE</th>
					<th>CUSTOMER NAME</th>
					<th>MEMBER CARD TYPE</th>  
					<th>MEMBER CARD NO.</th>
				</tr>
			</c:if>
			<c:if test="${promotion.promotionOptionName != 'Customer'}">
				<tr>  
					<th>PRODUCT CODE</th>
					<th>PRODUCT</th>
					<th>CATEGORY</th>  
					<th>PRDUCT SUB CATEGORY</th>
				</tr>
			</c:if>
				<tbody>  
					 <tr style="height: 25px;">
						<c:forEach begin="0" end="5" step="1">
							<td/>
						</c:forEach>
					</tr>
					<c:if test="${promotion.promotionOptionName == 'Customer'}">
						<c:forEach items="${promotion.promotionOptionVOs}" var="option">
							<tr> 
								<td>
									${option.customerReference}
								</td>
								<td>
									${option.customerName}
								</td>
								<td>
									${option.memberCardType}
								</td>
								<td>
									${option.memberCardNo}
								</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${promotion.promotionOptionName != 'Customer'}">
						<c:forEach items="${promotion.promotionOptionVOs}" var="option">
							<tr> 
								<td>
									${option.productCode}
								</td>
								<td>
									${option.productName}
								</td>
								<td>
									${option.productCategoryName}
								</td>
								<td>
									${option.productSubCategory}
								</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:forEach begin="0" end="1" step="1">
						<tr style="height: 25px;">
							<c:forEach begin="0" end="5" step="1">
								<td/>
							</c:forEach>
						</tr>  
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="clear-fix"></div>
		<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
			 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
			<table class="width100">
				<tr>  
					<th>TYPE</th>
					<th>CALCULATION TYPE</th>
					<th>PROMOTION</th>
					<th>COUPON</th>
					<th>PRODUCT POINT</th>  
				</tr>
				<tbody>  
					 <tr style="height: 25px;">
						<c:forEach begin="0" end="5" step="1">
							<td/>
						</c:forEach>
					</tr>
						<c:forEach items="${promotion.promotionMethodVOs}" var="method">
							<tr> 
							<td>
								${method.promotionTypeName}
							</td>
							<td>
								${method.calculationTypeName}
							</td>
							<td>
								${method.promotionAmount}
							</td>
							<td>
								${method.couponName}
							</td>
							<td>
								${method.productPoint}
							</td>
						</tr>
					</c:forEach>
					<c:forEach begin="0" end="1" step="1">
						<tr style="height: 25px;">
							<c:forEach begin="0" end="5" step="1">
								<td/>
							</c:forEach>
						</tr>  
					</c:forEach>
				</tbody>
			</table>
		</div>
		</c:forEach> 
	<div class="clear-fix"></div>
	</body>
</html>