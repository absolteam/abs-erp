<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Material Transfer</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
.width15{width:15%;}
.clear-fix{clear: both;}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>MATERIAL TRANSFER</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 80px; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width48 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width40 text-bold">TRANSFER PERSON<span class="float_right">:</span></span>
				<span class="span_border left_align width50 text_info" style="display: -moz-inline-stack;">${MATERIAL_TRANSFER.transferPerson}</span>  
			 </div>  
		</div>  
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">REFERENCE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					 ${MATERIAL_TRANSFER.referenceNumber}
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">DATE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					${MATERIAL_TRANSFER.date}
				</span>
			</div>
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>PRODUCT CODE</th>
				<th>PRODUCT</th>
				<th>TRANSFER TYPE</th>
				<th>DATE</th>
				<th>PRODUCTION QTY</th>
				<th>TRNSFER QTY</th>
				<th>TRANSFERD FROM</th>
				<th>TRANSFERD TO</th> 
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:forEach begin="0" end="5" step="1">
						<td/>
					</c:forEach>
				</tr>
				<c:forEach items="${MATERIAL_TRANSFER.materialTransferDetailVOs}" var="details">
						<tr> 
							<td>
								${details.product.code}
							</td>
							<td>
								${details.productName}
							</td>
							<td>
								${details.transferType}
							</td>  
							<td>
								${details.storeFrom}
							</td>
							<td>
								${details.storeTo}
							</td> 
							<td>
								${details.quantity}
							</td>
							 <td>
								${details.description}
							</td>
						</tr>
					</c:forEach>
				<c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="5" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	<div class="width98 div_text_info" style="margin-top: 20px;">
		<span class="float_left left_align width15 text-bold">DESCRIPTION<span class="float_right">:</span></span>
		<span class="span_border left_align width80 text_info" style="display: -moz-inline-stack;">
		<c:choose>
			<c:when test="${MATERIAL_TRANSFER.description ne null && MATERIAL_TRANSFER.description ne ''}">
				${MATERIAL_TRANSFER.description}
			</c:when>
			<c:otherwise>
				-N/A-
			</c:otherwise>
		</c:choose> 
		</span>
	</div>
	</body>
</html>