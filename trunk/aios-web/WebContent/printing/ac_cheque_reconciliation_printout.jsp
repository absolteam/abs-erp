<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Petty Cash</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}

.flag {
	background: url(./images/checked.png);
	background-repeat: no-repeat;
	height: 30px;
	width: 20px;
	float: left;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"><span>${THIS.companyName}</span> </span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>PRESENTED CHEQUES</span> </span>
	</div>
	<div class="clear-fix"></div>
	<div style="position: relative; top: 10px; height: 100%;"
		class="width98 float_left">
		<c:choose>
			<c:when
				test="${CHEQUE_TRANSACTION ne null && CHEQUE_TRANSACTION ne ''}">
				<table class="width100">
					<tr>
						<th>REFERENCE NO</th>
						<th>TRANSACTION DATE</th>
						<th>TRANSACTION TYPE</th>
						<th>PAYABLES</th>
						<th>RECEIVABLES</th>
						<th>BANK</th>
						<th>ACCOUNT NO</th>
						<th>CHEQUE DATE</th>
						<th>CHEQUE NO</th>
						<th>AMOUNT</th>
						<th style="display: none;">PASSED DATE</th>
					</tr>
					<tbody>
						<c:forEach items="${CHEQUE_TRANSACTION.chequeTransactionVOs}"
							var="TRANSDT" varStatus="status">
							<tr>
								<td>${TRANSDT.referenceNumber}</td>
								<td>${TRANSDT.transactionDate}</td>
								<td>${TRANSDT.transactionType}</td>
								<td><c:choose>
										<c:when
											test="${TRANSDT.payableName ne null && TRANSDT.payableName ne ''}">
									${TRANSDT.payableName}
								</c:when>
										<c:otherwise>-N/-A</c:otherwise>
									</c:choose>
								</td>
								<td><c:choose>
										<c:when
											test="${TRANSDT.receivableName ne null && TRANSDT.receivableName ne ''}">
									${TRANSDT.receivableName}
								</c:when>
										<c:otherwise>-N/-A</c:otherwise>
									</c:choose>
								</td>
								<td>${TRANSDT.bankName}</td>
								<td>${TRANSDT.accountNumber}</td>
								<td>${TRANSDT.chequeDate}</td>
								<td>${TRANSDT.chequeNumber}</td>
								<td style="text-align: right;"><c:choose>
										<c:when
											test="${TRANSDT.chequePayment ne null && TRANSDT.chequePayment ne ''}">
									${TRANSDT.chequePayment}
								</c:when>
										<c:when
											test="${TRANSDT.chequeReceive ne null && TRANSDT.chequeReceive ne ''}">
									${TRANSDT.chequeReceive}
								</c:when>
										<c:otherwise>-N/-A</c:otherwise>
									</c:choose>
								</td>
								<td style="display: none;">${TRANSDT.passedDateCheque}</td>
							</tr>
						</c:forEach>
						<c:forEach begin="0" end="1" step="1">
							<tr style="height: 25px;">
								<c:forEach begin="0" end="9" step="1">
									<td />
								</c:forEach>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="clear-fix"></div>
				<table class="width30">
					<tr>
						<th>TOTAL PAYBLES</th>
						<th>TOTAL RECEIVABLES</th>
					</tr>
					<tr>
						<td>${CHEQUE_TRANSACTION.paymentTotal}</td>
						<td>${CHEQUE_TRANSACTION.receiveTotal}</td>
					</tr>
				</table>
			</c:when>
			<c:otherwise>
				<span style="color: red">No result found.</span>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="clear-fix"></div>
</body>
</html>