<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Income Statement</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-bottom: 3px #000;
	font-size: 13px;
}

td {
	padding: 8px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	margin: 5px 0 5px 0;
	border: 0px #fff;
}

.accountSubType {
	font-weight: bold;
	font-size: 13px;
	border-bottom: 2px solid;
}

.accountSubTypetd {
	border: 0px;
}

.bottomTD { /*border-bottom: 3px double #000;*/
	
}

th,td {
	border-bottom: 0px;
}

.accountheading {
	font-weight: bold;
	font-size: 19px;
	border-bottom: 2px solid;
	letter-spacing: 4px;
}

.amountcol {
	font-weight: bold;
	text-align: right;
}

.totalcol {
	border-width: 4px;
	border-bottom-style: double;
	font-weight: bold;
}

.subtotalcol {
	border-width: 4px;
	border-bottom: 1px solid;
	border-top: 1px solid;
	font-weight: bold;
}

.lower {
	text-transform: lowercase;
}

.upper {
	text-transform: uppercase;
}

tbody tr:hover td,tbody tr:hover th {
	background-color: #FFF;
}

tbody tr:hover td table tbody tr td,tbody tr:hover th td table tbody tr td
	{
	background-color: #FFF;
}

tbody tr:hover td table tbody tr:hover td,tbody tr:hover th td table tbody tr:hover td
	{
	background-color: #FFF;;
}

.content-tbody>tr {
	width: 80%;
	float: left;
}

.content-tbody>tr>td {
	width: 38%;
	float: left;
}

.netlabel {
	font-weight: bold;
	font-size: 15px;
	border-bottom: 1px solid;
	border-bottom-width: 2px;
	letter-spacing: 2px;
}
</style>
<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<div class="width98">
			<span class="heading"><span class="upper"
				style="font-size: 25px !important; border-bottom: 1px solid; border-bottom-width: 2px;">${THIS.companyName}</span>
			</span>
		</div>
	</div>
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span
			style="font-size: 20px !important; border-bottom: 1px solid; border-bottom-width: 2px;">
				INTERIM STATEMENT OF COMPREHENSIVE INCOME STATEMENT AS AT
				${requestScope.PRINT_DATE}</span> </span>
	</div>
	<div class="clear-fix"></div>
	<div class="width100">
		<span class="side-heading" style="float: right; padding: 5px;">Printed
			on : ${requestScope.PRINT_DATE}</span>
	</div>
	<c:choose>
		<c:when
			test="${CONSOLIDATED_INCOMESTATEMENT ne null && CONSOLIDATED_INCOMESTATEMENT ne ''}">
			<div class="data-list-container width100 float-left"
				style="margin-top: 10px;">
				<table class="width100" style="padding: 2px;">
					<tr>
						<th><span class="accountheading">REVENUES</span>
						</th>
					</tr>
					<tbody class="content-tbody">
						<c:forEach
							items="${CONSOLIDATED_INCOMESTATEMENT.transactionDetailVOs}"
							var="accountDetail" varStatus="status">
							<tr>
								<c:choose>
									<c:when
										test="${accountDetail.transactionDetailVOs ne null && fn:length(accountDetail.transactionDetailVOs)>0}">
										<td class="accountSubTypetd"><span
											class="accountSubType upper">${accountDetail.accountSubType}</span>
										</td>
										<c:forEach items="${accountDetail.transactionDetailVOs}"
											var="account">
											<tr>
												<td>${account.accountDescription}</td>
												<td class="amountcol">${account.revenueAmount}</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td>${account.accountDescription}</td>
										<td class="amountcol">${account.revenueAmount}</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach> 
						<tr>
							<td colspan="3" class="accountSubTypetd"></td>
						</tr>
						<tr>
							<td style="font-weight: bold;">TOTAL REVENUE</td>
							<td style="text-align: right;"><span class="subtotalcol">${CONSOLIDATED_INCOMESTATEMENT.revenueAmount}</span>
							</td>
						</tr>
					</tbody>
				</table>

				<table class="width100" style="padding: 2px;">
					<tr>
						<th><span class="accountheading">EXPENSES</span>
						</th>
					</tr>
					<tbody class="content-tbody">
						<c:forEach
							items="${CONSOLIDATED_INCOMESTATEMENT.transactionDetailVOCs}"
							var="expenseDetail" varStatus="status">
							<tr>
								<c:choose>
									<c:when
										test="${expenseDetail.transactionDetailVOs ne null && fn:length(expenseDetail.transactionDetailVOs)>0}">
										<td class="accountSubTypetd"><span
											class="accountSubType upper">${expenseDetail.accountSubType}</span>
										</td>
										<c:forEach items="${expenseDetail.transactionDetailVOs}"
											var="expense">
											<tr>
												<td>${expense.accountDescription}</td>
												<td class="amountcol">${expense.expenseAmount}</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td>${expenseDetail.accountDescription}</td>
										<td class="amountcol">${expenseDetail.expenseAmount}</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>  
						<tr>
							<td colspan="3" class="accountSubTypetd"></td>
						</tr>
						<tr>
							<td style="font-weight: bold;">TOTAL EXPENSES</td>
							<td style="text-align: right;"><span class="subtotalcol">${CONSOLIDATED_INCOMESTATEMENT.expenseAmount}</span>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="accountSubTypetd"></td>
						</tr>
						<tr>
							<td><span class="netlabel upper">${CONSOLIDATED_INCOMESTATEMENT.label}</span>
							</td>
							<td style="text-align: right;"><span class="totalcol">${CONSOLIDATED_INCOMESTATEMENT.netAmount}</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</c:when>
		<c:otherwise>
			<div class="noRecordFound">No Record found.</div>
		</c:otherwise>
	</c:choose>
</body>
</html>
