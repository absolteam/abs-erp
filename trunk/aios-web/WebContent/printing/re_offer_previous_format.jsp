﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.workflow.domain.entity.User"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Offer Details</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
	<style type="text/css">
	table tbody tr td {
		text-align: left;
	}
	</style>
</head>
<body onload="window.print();" style="margin-top: 150px; font-size: 10px;">	

	<div class="subTitle">
		Offer Letter - عرض سعر
	</div>
	
	<% 
		OfferManagementTO agreementTO = (OfferManagementTO) ServletActionContext.getRequest().getSession().getAttribute("OFFER_DETAILS_INFO"); 
		User loggedUser = (User) ServletActionContext.getRequest().getSession().getAttribute("USER"); 
		String temp = agreementTO.getTenantNameArabic();
		String deposit = agreementTO.getSecurityDeposit().trim();
	%>
	
	<br/>
	<div align="right">
		<label style="text-align: right; padding: 0px !important; margin: 0px !important;">Date: <strong><u><%= agreementTO.getOfferDate() %></u></strong> :التاريخ</label>	
	</div>
	
	<div class="sectionHead">
		<u>Tenant Details</u>		
	</div>
	<div style="font-weight: bolder; font-size: 11px;">		
		<label>Name: </label>	
		<label id="name" >${OFFER_DETAILS_INFO.tenantName}</label>
		<span class="arabic">الاسم: <%= temp %></span>
		<br/>
		
		<label>Address: </label>	
		<label id="name" >${OFFER_DETAILS_INFO.address}</label>
		<span class="arabic">العنوان: </span>
		<br/>
	</div>
	
	<div style="margin-top: 5px;">
		<strong>
			<u>Subject: Proposal for the property</u>		
		</strong>
	</div>
	
	<div class="sectionHead">
		<u>Offer Details</u>		
	</div>
	
	<div>
		<p>We would like to offer you following:</p>
	</div>
	
	<fieldset>
		<legend>Offered</legend>
		<c:choose>
			<c:when test="${OFFER_CANVAS_DETAILS_PRINT.componentFlag ne 0 && OFFER_CANVAS_DETAILS_PRINT.componentFlag eq 1 || OFFER_CANVAS_DETAILS_PRINT.propertyType eq 'villa' }"> 
		
				<label>Property Type: </label>	
				<label id="name" >${OFFER_CANVAS_DETAILS_PRINT.propertyType}</label>
				<span class="arabic">نوع العقار: </span>
				<br/>	
				
				<label>Name / Location: </label>	
				<label id="name" >${OFFER_CANVAS_DETAILS_PRINT.propertyName}</label>
				<span class="arabic">اسم/ عنوان العقار: </span>
				<br/>	
								
				<label>Rent Amount: </label>	
				<label id="name" >${OFFER_CANVAS_DETAILS_PRINT.offerAmount}</label>
				<span class="arabic">قيمة الايجار: </span>
				<br/>
				
				
				<label>Net Total Area: </label>	
				<label id="name" >${OFFER_CANVAS_DETAILS_PRINT.propertySize}</label>
				<span class="arabic">مجموع المساحة الإجمالية:</span>
				<br/>
				<br/>
				<hr/>	
		
			</c:when> 
			<c:otherwise>
				<div>
				<%
					OfferManagementTO offerTO=(OfferManagementTO)ServletActionContext.getRequest().getSession().getAttribute("OFFER_CANVAS_DETAILS_PRINT");
					int i=0;	
					List<OfferManagementTO> componentList= new ArrayList<OfferManagementTO>();
					List<OfferManagementTO> unitList= new ArrayList<OfferManagementTO>();
					componentList=offerTO.getComponentList(); 
					unitList=offerTO.getUnitList();
					if(componentList!=null && !componentList.equals("")){
						for(OfferManagementTO component: componentList){%> 
							<label>Property Type: </label>	
							<label id="name" >Component</label>
							<span class="arabic">نوع العقار: </span>
							<br/>	
							
							<label>Name / Location: </label>	
							<label id="name" ><%= component.getComponentName() %></label>
							<span class="arabic">اسم/ عنوان العقار: </span>
							<br/>	
						
							<label>Rent Amount: </label>	
							<label id="name" ><%= AIOSCommons.formatAmount(component.getComponentRent()) %></label>
							<span class="arabic">قيمة الايجار: </span>
							<br/>
															
							<label>Net Total Area: </label>	
							<label id="name" ><%= component.getPropertySize() %></label>
							<span class="arabic">مجموع المساحة الإجمالية:</span>
							<br/>
							<hr/>									
					<%}
					}
					else if(unitList!=null && !unitList.equals("")){
						for(OfferManagementTO units: unitList){%>
							<label>Property Type: </label>	
							<label id="name" >Unit</label>
							<span class="arabic">نوع العقار: </span>
							<br/>	
							
							<label>Name / Location: </label>	
							<label id="name" ><%= units.getUnitName() %></label>
							<span class="arabic">اسم/ عنوان العقار: </span>
							<br/>	
							
							<label>No. of Bedrooms: </label>	
							<label id="name" ><%= units.getUnitBedrooms() %></label>
							<span class="arabic">عدد غرف النوم: </span>
							<br/>
							
							<label>No. of Bathrooms: </label>	
							<label id="name" ><%= units.getUnitBathrooms() %></label>
							<span class="arabic">عدد الحمامات: </span>
							<br/>
														
							<label>Rent Amount: </label>	
							<label id="name" ><%= AIOSCommons.formatAmount(units.getOfferAmount()) %></label>
							<span class="arabic">قيمة الايجار: </span>
							<br/>
							
							<label>Net Total Area: </label>	
							<label id="name" ><%= units.getPropertySize() %></label>
							<span class="arabic">مجموع المساحة الإجمالية:</span>									
							<br/>
							<br/>
							<hr/>		
						<%}
					}
				%> 
				</div>
			</c:otherwise>
		</c:choose> 
	</fieldset>
	
	<div>
		<label>Contract Period: </label>
		<label>${OFFER_DETAILS_INFO.startDate} - ${OFFER_DETAILS_INFO.endDate}</label>
		<span class="arabic">مدة العقد: </span>
	</div>
	
	<div class="sectionHead">
		<u>Other Details</u>
	</div>
	
	<div>
		<div class="signatureBox">
			<strong>Contractor</strong><br/><br/>
			General maintenance will be provided by the contractor for the first year.
		</div>
	
		<div class="signatureBox" style="float: right; margin-right: 10px;">
			<strong>Tenant</strong><br/><br/>
			General utility (water, gas, & electricity) will be on the tenant.
		</div>
	</div>
	
	<fieldset style="border: none;">
		<div class="sectionHead">
			<u>Payment Cheque (fill in details)</u>
		</div>
	
		<div>
			<table border="1" width="100%" style="border-style: none;">
				<tbody>
					<tr>
						<td>Account Title</td>
						<td>Adel Abdul Hameed Al Hosani</td>
					</tr>
					<tr>
						<td>Account Number</td>
						<td>101-25656104-03</td>
					</tr>
					<% 
						if(!deposit.equals("0") && !deposit.equals("0.0")) { %> 
							<tr>
								<td>Account Number (Security Deposit)</td>
								<td>101-25656104-02</td>
							</tr>
					<% } %>
					<tr>
						<td>IBAN Number</td>
						<td>AE500-26000-101-25656-10403</td>
					</tr>
					<% 
						if(!deposit.equals("0") && !deposit.equals("0.0")) { %> 
							<tr>
								<td>IBAN Number (Security Deposit)</td>
								<td>AE500-26000-101-25656-10402</td>
							</tr>
					<% } %>
					<tr>
						<td>Bank Name</td>
						<td>Emirates NBD</td>
					</tr>
					<tr>
						<td>Branch</td>
						<td>Khalifa</td>
					</tr>
					<tr>
						<td>City</td>
						<td>Abu-Dhabi</td>
					</tr>
				</tbody>
			</table>
		</div>
	</fieldset>
	<div>
		<% 
			if(!deposit.equals("0") && !deposit.equals("0.0")) { %> 
				<label>Security Deposit: </label>	
				<span id="security" > <%= agreementTO.getSecurityDeposit() %> </span>
		<% } %>
	</div>
	<div>
		<p><strong>This offer is valid for <u>${OFFER_DETAILS_INFO.validityDays} Days</u> and subject to Landlord approval.</strong></p>
		<!-- <label><b>Offer Validity: </b></label>
		<label><b>${OFFER_DETAILS_INFO.validityDays} Days</b></label>
		<span class="arabic">صلاحية العرض:</span> -->
	</div>
	
	<div>
		<p>By taking this opportunity we would like to thank you for considering
		   our proposal and we hope a positive feedback.</p> <br/><br/>
		<span>Yours Sincerely,</span>
		<p>Real Estate Department<br/>055-4936542</p>
	</div>
	
	 <% 
	 	temp = loggedUser.getUsername()
	 		.concat("-" + loggedUser.getUsername().toString()
	 				.concat("-" + AIOSCommons.getCurrentTimeStamp()));
	 	temp = AIOSCommons.desEncrypt(temp);
	 %>
	
	<div>
		<label style="padding-left: 0px !important;">Ref #: <%= temp %></label>	
		<label style="float: right; text-align: right;">User: <%= loggedUser.getUsername().split("_")[0] %></label>	
	</div>
</body>
</html>