<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Payment Receipt</title>
</head>
<body>
	<div style="height: 420px; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;">
		<div align="center">
			<% 
				type = "";
				if(SecondCopy) {
					type = "(Tenant Copy)";
				} else {
					type = "(Owner Copy)";
					SecondCopy = true;
				}
			%>
			<h3>Receipt <%= type %></h2>	
			<hr/>
			<h5 style="display: inline;">P.O. Box: 5929 - (9712) 6671191 أبو ظبي - الامارات العربية المتحدة - ص. : 6662800 - فاكس</h5>
			<br/>
			<h5 style="display: inline;">P.O. Box: 5929 - Abu Dhabi - United Arab Emirates - Tel. : (9712) 6662800 - Fax: (9712) 6671191</h5>
			<hr/>
		</div>
		
		<div style="margin-bottom: 110px;">
			<div class="float_left" style="width: 33%;" align="center">
				<p>
					<span style="width: 68px; display: table-cell; border-style:solid; border: 1px;">Dhs. &nbsp&nbsp&nbsp درهم</span>
					<!--<span style="width: 50px; display: table-cell;  border-style:solid; border: 1px;">Fils  فلس</span>
				--></p>
				<p>
					<span style="width: 68px; display: table-cell; border: thin; border-style:solid;"><%= contractAmount %></span>
					<!--<span style="width: 50px; display: table-cell;  border-style:solid; border: 1px;">00</span>
				--></p>
			</div>
			
			<div class="float_left bold" style="width: 33%; margin-top: 20px;" align="center">
				<p style="display: table-cell;">
					استلام قسيمة
				</p>
				<hr/>
				<p style="display: table-cell;">
					RECEIPT VOUCHER
				</p>
			</div>
			
			<div class="float_right" style="width: 33%;" align="center">
				<p style="color: red;">
					No. ${AGREEMENT_DETAILS.contractNumber}
				</p>
				<p>
					Date <u>${AGREEMENT_DETAILS.contractDate}</u> التاريخ
				</p>
			</div>
		</div>
		
		<div>
			<div>
				<span class="detail_text left_align" style="width: 140px;">Received from Mr/M/s.</span>			
				<span class="detail_text center_align span_border" style="width: 492px;"><%= agreementTOTenants.getTenantName() %></span>
				<span class="detail_text right_align" style="width: 115px;">وردت من السيد / السيدة</span>
			</div>
			<div>
				<% temp = AIOSCommons.convertAmountToWords(contractAmount); %>
				<span class="detail_text left_align" style="width: 100px;">Sum of Dhs.</span>			
				<span class="detail_text center_align span_border" style="width: 595px;"><%= temp %></span>
				<span class="detail_text right_align" style="width: 78px;">مجموع درهم</span>
			</div>
			<div>
				
				<span class="detail_text left_align" style="width: 89px;">On Account of</span>			
				<span class="detail_text center_align span_border" style="width: 598px;"></span>
				<span class="detail_text right_align" style="width: 59px;">على حساب</span>
			</div>
			<div>
				<%
					chqNo = "";
					banks = "";
					dates = "";
					
					for(ReContractAgreementTO payment : agreementTOPayment) {
						
						chqNo += (payment.getChequeNumber() != null 
								&& payment.getChequeNumber() != "")? payment.getChequeNumber() 
										+ " (" + payment.getChequeAmount() + ")" + " - " : "Cash (" + payment.getChequeAmount() + ") - ";
						
						banks += (payment.getBankName() != null 
								&& payment.getBankName() != "")? payment.getBankName() + " - " : "";
								
						dates += (payment.getChequeNumber() != null 
								&& payment.getChequeNumber() != "")? payment.getChequeDate() + " - " : "";
					}
					
					if(chqNo == "")
						chqNo = "Cash";
				%>
				<span class="detail_text left_align" style="width: 110px;">Cash/Cheque No.</span>			
				<span class="detail_text center_align span_border" style="width: 563px;"><%= chqNo %></span>
				<span class="detail_text right_align" style="width: 72px;">نقدا / شيك رقم</span>
			</div>
			<div>
				<span class="detail_text left_align" style="width: 300px;">Dated 
					<span class="span_border center_align" style="display: -moz-inline-stack; width: 204px;"><%= dates %></span>
					<span class="right_align"> التاريخ </span> 
				 </span>			
				<span class="detail_text" style="width: 450px;">Bank
					<span class="span_border center_align" style="display: -moz-inline-stack; width: 350px;"><%= banks %></span>
					<span class="right_align"> مصرف </span> 
				</span>				
			</div>
			
			<div style="margin-top: 40px; padding-left: 10px; padding-right: 10px;">
				<div style="width: 220px; text-align: center; display: -moz-inline-stack;">
					<hr/>
					<span style="padding-bottom: 80px;">Receiver's Sign. / المتلقي في التوقيع</span>				
				</div>
				<div style="float: right; width: 220px; text-align: center;">
					<hr/>
					<span style="padding-bottom: 80px;">Accountant's Sign. / المحاسب التوقيع</span>
				</div>
			</div>
		</div>
		
		
	</div>
</body>
</html>