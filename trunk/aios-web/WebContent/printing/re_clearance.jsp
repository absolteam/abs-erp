<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" charset="utf-8" /> 
	<title>Clearance</title>
</head>
<body style="font-size: 14px;">
	<div>
		<c:choose>
			<c:when test="${clearanceType == 'withDis'}">
				<div style="height: 650px; display: block; margin-top:150px;">
					<%@ include file="re_ClearanceWithDisconnection.jsp"%>
				</div>	
			</c:when>
			<c:otherwise>
				<div style="height: 650px; display: block; margin-top:150px;">
					<%@ include file="re_ClearanceWithoutDisconnection.jsp"%>
				</div>	
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>