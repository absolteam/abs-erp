<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" charset="utf-8" /> 
	<title>Contract Details</title>
</head>
<body onload="window.print();" style="font-size: 10px !important;">
	<div>
		<%@ include file="re_contractreceipts.jsp"%>
	</div> 
	<div style="height: 500px; display: block;">
		<%@ include file="re_contractreceiptowner.jsp"%>
	</div>
	<div style="height: 500px; display: block;">
		<%@ include file="re_contractreceiptowner.jsp"%>
	</div>
</body>
</html>