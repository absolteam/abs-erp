<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PettyCash</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />

</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	margin: 5px 0 5px 0;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<div class="width98">
			<span class="heading"><span
				style="font-size: 25px !important;">${THIS.companyName}</span> </span>
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span>PettyCash</span> </span>
	</div> 
	<div class="clear-fix"></div>
	<div class="data-list-container width100 float-left"
			style="margin-top: 10px;">
		<table class="width100">
			<tbody>
				<tr style="width: 40%; float: left;">
					<td>
						<table>
							<tr>
								<td>Voucher</td>
								<td>${PETTYCASH_INFO.pettyCashNo}</td>
							</tr>
							<tr>
								<td>Date</td>
								<td>${PETTYCASH_INFO.voucherDateView}</td>
							</tr>
						</table>
					</td> 
				</tr>
				<tr style="width: 40%; float: left;">
					<td>
						<table>
							<tr>
								<td>Type</td>
								<td>${PETTYCASH_INFO.pettyCashTypeName}</td>
							</tr>
							<tr>
								<td>Payee Name</td>
								<td>${PETTYCASH_INFO.receivedBy}</td>
							</tr>
						</table>
					</td> 
				</tr> 
			</tbody>
		</table>
	</div> 
</body>
</html>