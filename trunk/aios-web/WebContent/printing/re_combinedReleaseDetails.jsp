<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" charset="utf-8" /> 
	<title>Release Details</title>
</head>
<body>

	<br><br><br><br><br><br><br><br><br><br><br>
	<div style="height: 800px; display: block;">
		<%@ include file="re_release.jsp"%>
	</div>
	<br><br><br><br><br><br><br><br><br><br><br>
	<div>
		<c:choose>
			<c:when test="${releaseType == 'withoutDis'} ">
				<div style="height: 800px; display: block;">
					<%@ include file="re_relaseClearanceWithoutDisconnection.jsp"%>
				</div>	
			</c:when>
			<c:otherwise>
				<div style="height: 800px; display: block;">
					<%@ include file="re_relaseClearanceWithDisconnection.jsp"%>
				</div>	
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>