<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Production Receive</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />

</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 11px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	margin: 5px 0 5px 0;
}

td {
	border: 1px solid #000;
	padding: 3px;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.percentage-mode{float: left; font-weight: bold; font-size: 9px; position: relative; top: 3px; left: 3px;}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<div class="width98">
			<span class="heading"><span
				style="font-size: 25px !important;">${THIS.companyName}</span> </span>
		</div>

	</div>
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span>POS DAILY PROFIT LOSS</span> </span>
	</div>
	<div class="width100">
		<span class="side-heading" style="float: right; padding: 5px;">Printed
			on : ${requestScope.PRINT_DATE}</span>
	</div>
	<c:forEach items="${POS}" var="pos">
		<div class="clear-fix"></div>
		<c:set var="totalQuantity" value="${0}" />
		<c:set var="totalAmount" value="${0}" />
		<c:set var="totalDiscount" value="${0}" />
		<c:set var="totalUnitRate" value="${0}" />	
		<c:set var="totalReceipt" value="${0}" />
		<c:set var="totalProfit" value="${0}" />
		<c:set var="totalCostPrice" value="${0}" />
		<div class="data-list-container width100 float-left"
			style="margin-top: 10px;">
			<fieldset style="background-color: #F5ECCE;">
				<div class="width100 float-left">
					<span class="side-heading">SALES DATE: ${pos.key}</span>
				</div>
				<table class="width100">
					<tr>
						<th>REFERENCE</th>
						<th>STORE</th>
						<th>PRODUCT</th>
						<th>UNIT RATE</th>						
						<th>QUANTITY</th>					
						<th>DISCOUNT</th>
						<th>TOTAL SALE</th>
						<th>TOTAL COST</th>
						<th>BALANCE</th>
					</tr>
					<tbody>
						<c:forEach items="${pos.value}" var="posHead">
							<c:forEach items="${posHead.pointOfSaleDetailVOs}"
								var="posdetail">
								<tr>
									<td>${posHead.referenceNumber}</td>
									<td>${posHead.storeName}</td>
									<td>${posdetail.productName}</td>		
									<td style="text-align: right;">${posdetail.unitRate}</td>							
									<td style="text-align: right;">${posdetail.quantity}</td>									
									<td style="text-align: right;"><c:choose>
											<c:when
												test="${posdetail.discountValue ne null && posdetail.discountValue gt 0}">
												<c:choose>
													<c:when test="${posdetail.isPercentageDiscount eq true}">
														<span class="percentage-mode">(Percentage)</span>
													</c:when>
													<c:otherwise><span class="percentage-mode">(Amount)</span></c:otherwise>
												</c:choose>
										${posdetail.discount}
									</c:when>
											<c:otherwise>-N/A-</c:otherwise>
										</c:choose>
									</td>
									<td><span style="text-align: right; float: right;">${posdetail.totalPrice}</span>
									<td style="text-align: right;">${posdetail.totalCost}</td>
									<td><span style="text-align: right; float: right;">${posdetail.totalGain}</span>
									</td>
								</tr> 
								<c:set var="totalProfit" value="${totalProfit + posdetail.totalGain}" />
								<c:set var="totalCostPrice" value="${totalCostPrice + posdetail.totalCost}" />
							</c:forEach>
							<c:set var="totalQuantity" value="${posHead.totalQuantity}" />
							<c:set var="totalAmount" value="${posHead.salesAmount}" />
							<c:set var="totalDiscount" value="${posHead.customerDiscount}" />
							<c:set var="totalUnitRate" value="${posHead.totalDue}" />	
						</c:forEach>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL QUANTITY:</span> ${totalQuantity}</td>							
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL DISCOUNT:</span> ${totalDiscount}</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL SALES:</span> <span style="text-align: right; font-weight: bold;">${totalAmount}</span>
							</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">NET COST:</span> <span style="text-align: right; font-weight: bold;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalCostPrice}" /></span>
							</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">NET GAIN:</span> <span style="text-align: right; font-weight: bold;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalProfit}" /></span>
							</td>
						</tr>
					</tbody>
				</table>
				<%-- <div class="width50 float_left" style="float: left;">
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold"
							style="font-weight: bold;">RECEIPT DETAILS </span>
					</div>
					<table class="width100">
						<tr>
							<th>REFERENCE</th>
							<th>RECEIPT TYPE</th>
							<th>AMOUNT</th>
						</tr>
						<tbody>
							<c:set var="totalAmount" value="${0}" />
							<c:forEach items="${pos.value}" var="posHead">
								<c:if
									test="${posHead.pointOfSaleReceiptVOs ne null && fn:length(posHead.pointOfSaleReceiptVOs)>0}">
									<c:forEach items="${posHead.pointOfSaleReceiptVOs}"
										var="receipt">
										<tr>
											<td>${posHead.referenceNumber}</td>
											<td>${receipt.receiptTypeStr}</td>
											<td><span style="float: right;">${receipt.receiptAmountStr}</span>
											</td>
										</tr>
										<c:set var="totalAmount"
											value="${totalAmount+receipt.receiptAmount}" />
									</c:forEach>
								</c:if>
								<c:set var="totalReceipt" value="${posHead.receivedTotal}" />
							</c:forEach>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td style="text-align: right; font-weight: bold;"><span
									style="font-size: 10px;">TOTAL RECEIPT:</span> <span style="text-align: right; font-weight: bold;">${totalReceipt}</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div> --%>
				<%-- <c:set var="posCharge" value="false" />
				<c:forEach items="${pos.value}" var="posHead">
					<c:if test="${posHead.posCharge eq true}">
						<c:set var="posCharge" value="true" />
					</c:if>
				</c:forEach>
				<c:if test="${posCharge eq true}">
					<div class="float_right" style="float: right; width: 48%;">
						<div class="width100 div_text_info">
							<span class="float_left left_align width28 text-bold"
								style="font-weight: bold; float: left;">ADDITIONAL
								CHARGES </span>
						</div>
						<table class="width100" style="position: relative; top: 5px;">
							<tr>
								<th>REFERENCE</th>
								<th>CHARGES TYPE</th>
								<th>AMOUNT</th>
							</tr>
							<tbody>
								<c:set var="totalAmount" value="${0}" />
								<c:forEach items="${pos.value}" var="posHead">
									<c:if
										test="${posHead.pointOfSaleChargeVOs ne null && fn:length(posHead.pointOfSaleChargeVOs)>0}">
										<c:forEach items="${posHead.pointOfSaleChargeVOs}"
											var="charge">
											<tr>
												<td>${posHead.referenceNumber}</td>
												<td>${charge.chargeType}</td>
												<td><span style="float: right;">${charge.charges}</span>
												</td>
											</tr>
											<c:set var="totalAmount"
												value="${totalAmount+charge.charges}" />
										</c:forEach>
									</c:if>
								</c:forEach>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td style="text-align: right; font-weight: bold;"><span
										style="font-size: 10px;">TOTAL AMOUNT:</span> <span style="text-align: right; font-weight: bold;">${totalAmount}</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				 </c:if> --%>
			</fieldset>
		</div>
	</c:forEach>
	<div class="clear-fix"></div>
</body>
</html>