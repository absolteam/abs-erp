<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
@media print {
	.hideWhilePrint {
		display: none;
	}
}
</style>
<script type="text/javascript"> 
 
 </script>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading">Purchase Analysis Report</span>
	</div>
	<div id="main-content">
		<div class="width100">
			<div>Reference - <span style="color:red;font-weight: bold;">${REFERENCE_NUMBER}</span></div>
			<c:forEach var="prd" items="${PRODUCT_LIST}" varStatus="status1">
				<table class="width100">
					<tr height="25px;">
						<td>Code :</td>
						<td style="font-weight:bold;width:10%;">${prd.code}</td>
						<td>Description :</td>
						<td style="font-weight:bold;width:30%;">${prd.productName}</td>
						<td>Unit Type :</td>
						<td style="font-weight:bold;width:5%;">${prd.lookupDetailByProductUnit.displayName}</td>
						<td>Current Stock :</td>
						<td style="font-weight:bold;width:10%;">${prd.currentStock}</td>
					</tr>
					<c:choose>
						<c:when test="${fn:length(prd.productPurchaseVos)>0}">
							<tr style="margin-top: 10px;">
								<td colspan="8" align="right">
									<table style="width:97%;">
										<tr>
											<th style="font-weight:bold;">Order Ref.</th>
											<th style="font-weight:bold;">Supplier</th>
											<th style="font-weight:bold;">Description</th>
											<th style="font-weight:bold;">Quantity</th>
											<th style="font-weight:bold;">Unit Rate</th>
											<th style="font-weight:bold;">Sub Total</th>
											<th style="font-weight:bold;">Received Date</th>
										</tr>
										<c:forEach var="detail" items="${prd.productPurchaseVos}"
											varStatus="status1">
											<tr>
												<td>${detail.purchaseOrderReferenceNumber}</td>
												<td>${detail.supplierNameAndNumber}</td>
												<td>${detail.description}</td>
												<td>${detail.receivedQty}</td>
												<td>${detail.unitRate}</td>
												<td>${detail.totalRate}</td>
												<td>${detail.receiveDate}</td>
											</tr>
										</c:forEach>
									</table>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr style="margin-top: 10px;">
								<td colspan="8" align="left">
									No purchase found.
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</table>
			</c:forEach>

		</div>
	</div>
</body>