<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 55%!important;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">

var selectedStoreId = 0;
var selectedMonthId = 0; 
var selectedYear = 0;

$(function(){ 
						
		$(".xls-download-call").click(function(){
			if(selectedYear > 0){
				window.open("<%=request.getContextPath()%>/get_yearmonthly_pos_salesreport_XLS.action?store="+selectedStoreId+"&year="+selectedYear+"&month="+selectedMonthId,
						'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			} else{
				alert("Please select sales period.");
			}
			return false; 
		}); 

		$('#stores').combobox({
			selected : function(event, ui) {
				selectedStoreId = $('#stores :selected').val(); 
			}
		});


		$('#months').combobox({
			selected : function(event, ui) {
				selectedMonthId = $('#months :selected').val(); 
			}
		});
		
		$('#years').combobox({
			selected : function(event, ui) {
				selectedYear = $('#years :selected').val(); 
			}
		});
}); 

 
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Point of Sales Yearly Report
		</div>
		<div class="portlet-content" id="hrm">
			<table class="width50">
				<tr>
					<td class="width20">Store </td>
					<td>
						<select id="stores" class="width50">
							<option value="-1">--All--</option>
							<c:forEach var="store" items="${STORE_LIST}">
								<option value="${store.storeId}">${store.storeName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td class="width20">Year<span class="mandatory">*</span></td>
					<td>
						<select id="years" class="width50">
							<option value="-1">--Select--</option>
							<c:forEach var="year" items="${SALE_YEARS}"> 
								<option value="${year}">${year}</option>						
							</c:forEach>
						</select>
					</td>
				</tr> 
				<tr>
					<td class="width20">Month</td>
					<td>
						<select id="months">
							<option value="-1">--Select--</option>
							<c:forEach var="month" items="${SALE_MONTHS}">
								<option value="${month.key}">${month.value}</option>						
							</c:forEach>
						</select>
					</td>
				</tr> 
			</table>
		</div>
	</div> 
</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div> 
	</div>
</div> 