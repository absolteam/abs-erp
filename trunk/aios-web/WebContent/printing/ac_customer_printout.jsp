<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Invoice Report</title>
<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>CUSTOMER</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>



		<div style="position: relative; top: 10px; height: 100%;"
			class="data-list-container width98 float_left">
			
			<table class="width100">
				<c:forEach items="${requestScope.CUSTOMER}" var="customer">
					<thead>
						<tr>
							<th>Customer No</th>
							<th>Customer Name</th>
							<th>Customer Type</th>
							<th>Credit Term</th>
							<th>Refferal Source</th>
							<th>Member Type</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${customer.customerNumber}</td>
							<td>${customer.customerName}</td>
							<td>${customer.strCustomerType}</td>
							<td>${customer.strCreditTerm}</td>
							<td>${customer.strRefferalSource}</td>
							<td>${customer.strMemberType}</td>
						</tr>
						<tr>
							<td colspan="6" align="center" style="padding-right: 0px;">
								<table style="width: 99%;margin-top:10px;margin-bottom:10px;">
									<tr>
										<th>Shipping Name</th>
										<th>Contact Person</th>
										<th>Contact Number</th>
									</tr>
									<c:forEach items="${customer.shippingDetails}" var="shipping">
										<tr>
											<td>${shipping.name}</td>
											<td>${shipping.contactPerson}</td>
											<td>${shipping.contactNo}</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>								
								
					</tbody>
				</c:forEach>
			</table>
		</div>
	<div class="clear-fix"></div>
</body>
</html>