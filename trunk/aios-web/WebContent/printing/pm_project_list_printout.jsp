<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Request History</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>REQUEST HISTORY</span></span></div>
	<div style="clear: both;"></div>
	
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>REFERENCE</th>
				<th>MEMBER</th>
				<th>TITLE</th> 
				<th>PERIOD</th>
				<th>LEAD</th>
				<th>PRIORITY</th>
				<th>STATUS</th>
				<th>CREATED DATE</th>
				<th>CREATED BY</th>
			</tr>
			<tbody>  
				 
				<c:forEach items="${PROJECTS}" var="project" varStatus="status"> 
					<tr> 
						<td>
							${project.referenceNumber}
						</td>
						<td>${project.customerName}</td> 
						<td>${project.projectTitle}</td> 
						<td>${project.fromDate} - ${project.toDate}</td> 
						<td>${project.projectLead}</td> 
						<td>${project.priorityView}</td> 
						<td>${project.statusView}</td> 
						<td>${project.createdDateView}</td>
						<td>${project.createdByView}</td>
					</tr>
				</c:forEach> 
				<%-- <c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="5" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach> --%>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	</body>
</html>