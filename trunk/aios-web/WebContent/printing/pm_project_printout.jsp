<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>${sessionScope.project_projectTitle}</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading" style="text-transform: uppercase;"><span>${sessionScope.project_projectTitle} CARD</span></span></div>
	<div class="width98" ><span  class="heading" style="text-transform: uppercase;text-align: right;font-size: 16px!important;;"><span style="text-align: right;color: red;">${PROJECT_INFO.referenceNumber}</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width60 float_left">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width35 text-bold" style="text-transform: uppercase;">${sessionScope.project_client}<span class="float_right">:</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${PROJECT_INFO.customerName}&nbsp;</span></span>  
			 </div> 
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">REFERENCE NO.<span class="float_right"> :</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;"> 
					 ${PROJECT_INFO.referenceNumber}&nbsp;
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold" style="text-transform: uppercase;">${sessionScope.project_projectTitle} TITLE <span class="float_right"> :</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">  
					${PROJECT_INFO.projectTitle}&nbsp;
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold" style="text-transform: uppercase;">${sessionScope.project_projectTitle} TYPE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_INFO.projectType}&nbsp;
				</span>
			</div>
		</div>  
		<div class="width40 float_right">
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">START DATE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					${PROJECT_INFO.startDate}&nbsp;
				</span>
			</div> 
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">END DATE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					${PROJECT_INFO.endDate}&nbsp;
				</span>
			</div> 
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">TOTAL VALUE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_INFO.projectValue}&nbsp;
				</span>
			</div> 
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">DESCRIPTION<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_INFO.description}&nbsp;
				</span>
			</div> 
		</div> 
	</div>
	<div class="clear-fix"></div>
	<c:choose>
			<c:when test="${PROJECT_EXPENSE_INFO ne null && fn:length(PROJECT_EXPENSE_INFO)>0}">
			<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
				 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
				<table class="width100">
					<tr>  
						<th style="width: 10%;">PARTICULARS</th>
						<th style="width: 10%;">AMOUNT</th>
						<th style="width: 25%;">DESCRIPTION</th>
						<th style="width: 55%;">NOTES</th>
					</tr>
					<tbody>  
						 <tr style="height: 25px;">
							<c:forEach begin="0" end="3" step="1">
								<td/>
							</c:forEach>
						</tr>
						<c:forEach items="${PROJECT_EXPENSE_INFO}" var="detail" varStatus="status"> 
							<tr> 
								<td>
									${detail.lookupDetail.displayName}
								</td>
								<td>${detail.amount}</td> 
								<td>${detail.description}</td> 
								<td></td> 
							</tr>
						</c:forEach> 
						 <tr style="height: 25px;">
							<c:forEach begin="0" end="3" step="1">
								<td/>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>  
		</c:when>
	</c:choose>
	<div class="clear-fix"></div>
	
		<c:choose>
			<c:when test="${TASK_EXPENSES ne null && fn:length(TASK_EXPENSES)>0}">
			<div style="position:relative; top: 10px; height:100%;"
				  class="width100 float_left">
				<table class="width100" border="0" style="margin-top: 20px;">
					<c:set var="taskRow" value="0" />
					<c:forEach begin="0" step="2" items="${TASK_EXPENSES}">
						<tr  style="border: 0!important;margin: 5px!important;">
							<!-- TD 1 -->
							<c:if test="${TASK_EXPENSES[taskRow+0]!= null}">
							<td  style="border: 0!important;">
								<table class="width90" style="margin: 5px!important;">
									<tr>
										<td colspan="3" style="font-size: 16px;font-weight: bold;">${TASK_EXPENSES[taskRow+0].taskTitle}
										<c:if test="${TASK_EXPENSES[taskRow+0].parentTaskTitle !=null}">-(${TASK_EXPENSES[taskRow+0].parentTaskTitle})</c:if>
										</td>
									</tr>
									<tr>  
										<th style="width: 15%;">PARTICULARS</th>
										<th style="width: 15%;">AMOUNT</th>
										<th style="width: 70%;">NOTES</th>
									</tr>
									<tbody>  
										<c:forEach items="${TASK_EXPENSES[taskRow+0].projectExpenseVOs}" var="detail"> 
											<tr> 
												<td>
													${detail.lookupDetail.displayName}
												</td>
												<td>${detail.amount}</td> 
												<td></td> 
											</tr>
										</c:forEach> 
									</tbody>
								</table>
							</td>
							</c:if>
							<!-- TD 2 -->
							<c:if test="${TASK_EXPENSES[taskRow+1]!= null}">
								<td  style="border: 0!important;">
									<table class="width90">
										<tr>
											<td colspan="3" style="font-size: 16px;font-weight: bold;">${TASK_EXPENSES[taskRow+1].taskTitle}
											<c:if test="${TASK_EXPENSES[taskRow+1].parentTaskTitle !=null}">-(${TASK_EXPENSES[taskRow+1].parentTaskTitle})</c:if>
											</td>
										</tr>
										<tr>  
											<th style="width: 15%;">PARTICULARS</th>
											<th style="width: 15%;">AMOUNT</th>
											<th style="width: 70%;">NOTES</th>
										</tr>
										<tbody>  
											
											<c:forEach items="${TASK_EXPENSES[taskRow+1].projectExpenseVOs}" var="detail1"> 
												<tr> 
													<td>
														${detail1.lookupDetail.displayName}
													</td>
													<td>${detail1.amount}</td> 
													<td></td> 
												</tr>
											</c:forEach> 
											
										</tbody>
									</table>
								</td>
							</c:if>
							<c:set var="taskRow" value="${taskRow + 2}" />
						</tr>
					</c:forEach>
				</table> 
			</div>
			</c:when>
		</c:choose> 
		
		<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
				 -webkit-border-radius:6px; border-radius: 6px; margin-top: 20px;" class="width98 float_left">
			<h3 style="margin-left: 5px; display: block; width: 100%;">Following section needs to be filled in by ${sessionScope.project_lead}</h3>
			<table class="width100" border="0">
				<tr>
					<th width="10%">
						Sr.	no
					</th>
					<th width="50%">
						Particular
					</th>
					<th width="40%">
						Quantity - Decription
					</th>
				</tr>
				<tbody>
					<c:forEach begin="0" end="8" step="1">					
						<tr>
							<td>&nbsp;
							</td>
							<td>&nbsp;
							</td>
							<td>&nbsp;
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="width100 float_left div_text_info" style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>${sessionScope.project_createdby}</span> 
			</div> 
			<div class="width40 float_left">
				<span>${sessionScope.project_lead}</span> 
			</div>
			<div class="width30 float_left">
				<span>${sessionScope.project_manager}</span> 
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width40 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width30 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: -5px;">
			
			<div class="width30 float_left">
				<span class="width70">(${PROJECT_INFO.createdByView})</span>
			</div>
			<div class="width40 float_left">
				<%-- <span class="width70">(${PROJECT_INFO.projectLead})</span> --%>
			</div>
			<div class="width30 float_left"> 
			</div>
		</div>
	</div>
	</body>
</html>