<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Contract List</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Contract Report</span></div>
	<div class="width100 float-left"><span class="side-heading">Filter Condition</span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Property Name</label>
					<label class="data width50">${CONTRACT_INFO.buildingName}</label>
				</div>
				<div>
					<label class="caption width30" >Unit Name</label>
					<label class="data width50">${CONTRACT_INFO.flatNumber}</label>
				</div>
			</div>
			<div class="data-container width50 float-left">
				<div>
					<label class="caption width30">Area</label>
					<label class="data width50">${CONTRACT_INFO.buildingArea}</label>
					<label class="caption width30">Plot</label>
					<label class="data width50">${CONTRACT_INFO.buildingPlot}</label>
					<label class="caption width30">Street</label>
					<label class="data width50">${CONTRACT_INFO.buildingStreet}</label>
					<label class="caption width30">City</label>
					<label class="data width50">${CONTRACT_INFO.buildingCity}</label>
					
				</div>	
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading">Active Contracts</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(requestScope.CONTRACT_LIST_ACTIVE)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Contract Number</th> 
						<th>Contract Date</th>
						<th>Deposit Amount</th>
						<th>Contract Fee</th>
						<th>Rent Amount</th>
						<th>Contract Period</th>
						<th>Tenant Name</th>
						<th>Tenant Information</th>
						
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${requestScope.CONTRACT_LIST_ACTIVE}" var="result3" varStatus="status1" >
					 	<tr> 
							<td>${result3.contractNumber }</td>
							<td>${result3.contractDate }</td>
							<td>${result3.depositAmount }</td>
							<td>${result3.contractAmount }</td>
							<td>${result3.offerAmount}</td>
							<td>From (${result3.fromDate}) UpTo (${result3.toDate})</td>
							<td>${result3.tenantName }</td>
							<td>
								<span class="inside-heding">Identity :</span> ${result3.tenantIdentity} ,
								<span class="inside-heding">Nationality :</span> ${result3.tenantNationality},
								<span class="inside-heding">Mobile :</span> ${result3.tenantMobile},
								<span class="inside-heding">Fax : </span>${result3.tenantFax},
							</td>
						</tr>
					</c:forEach>
				  
				</tbody>
			</table>
		</c:when>
	  <c:otherwise>
	  		<tr class="even rowid">No Contracts</tr>
	  </c:otherwise>
	  </c:choose>	
		</div>	
		<div class="width100 float-left"><span class="side-heading">Contract History</span></div>
		<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<c:choose>
				<c:when test="${fn:length(requestScope.CONTRACT_LIST_INACTIVE)gt 0}">
				<table class="width100">	
						<thead>									
						<tr>
							<th>Contract Number</th> 
							<th>Contract Date</th>
							<th>Deposit Amount</th>
							<th>Contract Fee</th>
							<th>Rent Amount</th>
							<th>Contract Period</th>
							<th>Tenant Name</th>
							<th>Tenant Information</th>
							
						</tr> 
					</thead> 
					<tbody >	
				
						<c:forEach items="${requestScope.CONTRACT_LIST_INACTIVE}" var="result" varStatus="status1" >
						 	<tr> 
								<td>${result.contractNumber }</td>
								<td>${result.contractDate }</td>
								<td>${result.depositAmount }</td>
								<td>${result.contractAmount }</td>
								<td>${result.offerAmount}</td>
								<td>From (${result.fromDate}) UpTo (${result.toDate})</td>
								<td>${result.tenantName }</td>
								<td>
									<span class="inside-heding">Identity :</span> ${result.tenantIdentity} ,
									<span class="inside-heding">Nationality :</span> ${result.tenantNationality},
									<span class="inside-heding">Mobile :</span> ${result.tenantMobile},
									<span class="inside-heding">Fax : </span>${result.tenantFax},
								</td>
							</tr>
						</c:forEach>
					  	
					</tbody>
				</table>
			</c:when>
			  <c:otherwise>
			  		<label class="caption width100">No Contracts</label>
			  </c:otherwise>
			  </c:choose>
		</div>	
		
	</body>
</html>