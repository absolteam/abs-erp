<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fiscal Income Statement Report</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>FISCAL INCOME STATEMENT REPORT</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 10px; height: 100%;"
		class="width98 float_left">
		<c:forEach items="${requestScope.ACCOUNTS}" var="account" varStatus="status1">
		<table class="width100">
			<tr>

				<th style="width: 40%; border: none;text-align: left;">Revenue</th>
				<th style="width: 30%; border: none;">&nbsp;</th>
				<th style="width: 30%; border: none;">&nbsp;</th>
			</tr>
			<tbody>
				<c:forEach items="${account.revenueList}" var="revenue"
					varStatus="status2">
					
						<tr>
							<td>${revenue.revenueAccountCode}</td>
							<td>&nbsp;</td>
							<td>${revenue.revenueBalanceAmount}</td>
						</tr>
					</c:forEach>
					<tr>
						<td style="text-align: left;">Total(AED)</td>
						<td></td>
						<td>${account.revenueTotal}</td>
					</tr>
			</tbody>
		</table>
		<br><br><br>
		<table class="width100">
			<tr>

				<th style="width: 40%; border: none;text-align: left;">Expense</th>
				<th style="width: 30%; border: none;">&nbsp;</th>
				<th style="width: 30%; border: none;">&nbsp;</th>
			</tr>
			<tbody>
				<c:forEach items="${account.expenseList}" var="expense"
					varStatus="status2">
					
						<tr>
							<td>${expense.expenseAccountCode}</td>
							<td>&nbsp;</td>
							<td>${expense.expenseBalanceAmount}</td>
						</tr>
					</c:forEach>
					<tr>
						<td style="text-align: left;">Total(AED)</td>
						<td></td>
						<td>${account.expenseTotal}</td>
					</tr>
					<tr>
						<td style="border: none;">&nbsp;</td>
						<td style="border: none;">&nbsp;</td>
						<td style="border: none;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border: none; text-align: left;"><b>${account.netLabel}</b></td>
						<td style="border: none;">&nbsp;</td>
						<td style="border: none;">${account.netAmount}</td>
					</tr>
					
			</tbody>
		</table>
		</c:forEach>
		
		
	</div>
	<div class="clear-fix"></div>
</body>
</html>




