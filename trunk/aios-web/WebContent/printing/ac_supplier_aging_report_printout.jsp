<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Supplier Aging Report</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>SUPPLIER AGING REPORT</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 10px; height: 100%;"
		class="width98 float_left">
		<table class="width100">
			<tr>

				<th style="width: 20%; border: none;">Supplier #</th>
				<th style="width: 20%; border: none;">Supplier Name</th>
				<th style="width: 20%; border: none;">Branch</th>
				<th style="width: 20%; border: none;">Payment Term</th>
				<th style="width: 20%; border: none;">Total Payments</th>
			</tr>
			<tbody>

				<c:forEach items="${requestScope.SUPPLIERS_DATA}" var="result"
					varStatus="status1">
					
					<tr>
					
					<td>${result[1]}</td>
					<td>${result[2]}</td>
					<td>${result[3]}</td>
					<td>${result[4]}</td>
					
					<c:set var="sum" value="0"></c:set>
					<td><c:if test="${not empty result[5]}">
						<c:set var="sum" value="${result[5]}"></c:set>					
					</c:if>
					
					<c:if test="${not empty result[6]}">
						<c:set var="sum" value="${sum + result[6]}"></c:set>					
					</c:if>
					${sum}
					
					</td>
					
					
					</tr>
				</c:forEach>
				
			</tbody>
		</table>
	</div>
	<div class="clear-fix"></div>
</body>
</html>




