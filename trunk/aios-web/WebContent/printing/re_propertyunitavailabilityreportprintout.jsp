<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Property Unit Availability List</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Property Unit Availability Report</span></div>
	<div class="width100 float-left"><span class="side-heading">Filter Criteria</span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Property :</label>
					<label class="data width50">${PROPERTY_MASTER.propertyName}</label>
				</div>
		</div>
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Available From :</label>
					<label class="data width50">${PROPERTY_MASTER.fromDate}</label>
				</div>
		</div>
		</div>
	<div class="width100 float-left"><span class="side-heading">Unit Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(PROPERTY_LIST)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Property</th>
						<th>Component</th>
						<th>Unit</th>
						<th>Status</th>
						<th>Contract</th>
						<th>Tenant</th>
						<th>Period Start's</th>
						<th>Period End's</th>
					</tr> 
				</thead> 
				<tbody >	
				
					<c:forEach items="${PROPERTY_LIST}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.propertyName}</td>
					 		<td>${result3.componentName}</td>
							<td>${result3.unitName}</td>
							<td>${result3.statusStr}</td>
							<td>${result3.contractNumber}</td>
							<td>${result3.tenantName}</td>
							<td>${result3.fromDate}</td>
							<td>${result3.toDate}</td>
						</tr>
					</c:forEach>
				 
				</tbody>
			</table>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Unit</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>