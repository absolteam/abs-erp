<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Deliverables</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:80%;
    float:left;
}
.heading1 {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif; 
    color: #000;
    font-weight:bold;
    width:10%;
    float:left;
}
.width33{width:33%}
.signtable{position:relative; top: 25px;}
.signtable td{border: 0px;}
.noborder{border: 0px;}
</style>
	<body onload="window.print();" style="font-size: 12px;">
	<div style="clear: both;"></div>
	<div class="width98" >
		<span class="heading1">Job : </span>
		<span class="heading"><span>${PROJECT_DELIVERY.projectTitle}--${PROJECT_DELIVERY.projectReference}</span></span></div>
	<div style="clear: both;"></div> 
	<div style="height: 100%; border: 1px solid #F3ECEC; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left"> 
		<div class="width100 float_left">
			<div class="width33 div_text_info float_left">
				<span class="float_left left_align width35 text-bold">CUSTOMER<span class="float_right"> :</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;"> 
					 ${PROJECT_DELIVERY.customerName}&nbsp;
				</span> 
			</div> 
			<div class="width30 div_text_info float_left">
				<span class="float_left left_align width35 text-bold"> DATE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_DELIVERY.date}&nbsp;
				</span>
			</div>
			<div class="width30 div_text_info float_left">
				<span class="float_left left_align width35 text-bold">INVOICE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_DELIVERY.paymentAmount}&nbsp;
				</span>
			</div> 
		</div> 
		<div class="width100 float_left">
			<div class="width33 div_text_info float_left">
				<span class="float_left left_align width35 text-bold">ORDER DATE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_DELIVERY.orderDate}&nbsp;
				</span>
			</div>
			<div class="width30 div_text_info float_left">
				<span class="float_left left_align width35 text-bold">MOBILE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_DELIVERY.customerMobile}&nbsp;
				</span>
			</div>
			<div class="width30 div_text_info float_left">
				<span class="float_left left_align width35 text-bold">ADDRESS<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${PROJECT_DELIVERY.customerAddress}&nbsp;
				</span>
			</div>
		</div>
	</div>
	<table class="noborder signtable"> 
		<tr>
			<td><b>Driver</b></td>
			<td><b>Customer</b></td> 
		</tr>
		<tr> 
			<td>
				<span style="text-decoration: underline;">
					<c:forEach var="i" begin="0" end="15">
						&nbsp;
					</c:forEach> 
				</span>  
			</td> 
			<td>
				<span style="text-decoration: underline;">
					<c:forEach var="i" begin="0" end="15">
						&nbsp;
					</c:forEach> 
				</span>  
			</td> 
		</tr>
		<tr>
			<td><b></b></td>
			<td><b>${PROJECT_DELIVERY.customerName}</b></td> 
		</tr>
	</table> 
	</body>
</html>