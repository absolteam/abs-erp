<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project MileStone</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<span class="title-heading"><span>${THIS.companyName}</span>
		</span> 
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>Depreciation REPORT</span>
		</span>
	</div>
	<div class="clear-fix"></div>
	<c:if test="${ASSET_DATA ne null && fn:length(ASSET_DATA)>0}">
		<div
			style="position: relative; top: 10px; height: 100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; -webkit-border-radius: 6px; border-radius: 6px;"
			class="width98 float_left">
			<table class="width100">
				<tr>
					<th>Asset Number</th>
					<th>Asset Name</th>
					<th>Period</th>
					<th>Amount</th>
 				</tr>
				<tbody>
					<c:forEach
						items="${ASSET_DATA}"
						var="detail">
						<tr>
							<td>${detail.assetNumber}</td>
							<td>${detail.assetName}</td>
							<td>${detail.depreciationDate}</td>
							<td>${detail.description}</td>
 						</tr>
					</c:forEach>
					<c:forEach begin="0" end="1" step="1">
						<tr style="height: 25px;">
							<c:forEach begin="0" end="3" step="1">
								<td />
							</c:forEach>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:if> 
	<div class="clear-fix"></div>
</body>
</html>