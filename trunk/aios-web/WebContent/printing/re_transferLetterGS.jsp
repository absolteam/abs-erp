﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Transfer Letter</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />	
</head>
<body style="direction: rtl;">	

	<div class="AIOTitle">

	</div>

	<div style="margin-top: 50px;" align="right">
		
		<p> <%= AIOSCommons.getCurrentTimeStamp().split(" ")[0] %> <span>:التاريخ </span> </p>	
			
	</div>
	
	<div style="margin-top: 50px; display: block; width: 100%;">			
		<span class="bold" style="float: left; margin-left: 100px;"> المحترمين </span>
		<span class="bold" style="float: right;">السادة / شركة أبوظبي للتوزيع </span>	
	</div>
	
	<div style="margin-top: 100px; display: block;" align="right">	
		<p class="bold">.. تحية طيبة وبعد </p>	
	</div>


	<%
		Map<String, String> transferInfo = (Map<String, String>) session.getAttribute("CLEARANCE_INFO"); 
		String temp1 = transferInfo.get("clearanceType");
		String temp2 = "", temp3 = "";
		if(temp1.equals("Connect and Transfer")) {
			temp1 = "وصل و ";
			temp2 = "";
			temp3 = "بوصل و";
		}
		else {
			temp1 = "";
			temp2 = "بدون قطع التيار ";
			temp3 = "";
		}
	%>
	
	<div style="margin-top: 50px;" align="center">
		<p class="bold" style="direction: rtl;">
		<u> <span style="direction: rtl;"> الموضوع :</span> <span style="direction: ltr;"> <%= temp1 %> </span> <span style="direction: rtl;">   تحويل التيـار بإسم مستهـلك جديد </span> <span style="direction: rtl;"> <%= temp2 %> </span> </u></p>		
	</div>
	
	<div style="margin-top: 20px; margin-bottom: 30px;" align="center">
		<p style="direction: rtl;"> 
		
		 <span> بالإشارة إلى الموضوع أعلاه ، يرجى التكرم بعمل اللازم للاشتراك رقم  </span> <span> (<%= transferInfo.get("accountNo") %>) ، </span> <span> وذلك  </span><span> <%= temp3 %> </span>  تحويل التيار الى المستأجر الحالي للعقار، و ذلك تبعأ للبيانات الموضحة أدناه:  <span>  </span> <span> </span>  <span></span> <span>  </span>  <span> </span>      
	  		</p>
	</div>
			
	<div align="center">
		<table style="width: 60%;">
			<%-- <tr>
				<td style="direction: rtl !important;">
					<span> إلى </span> <span><%= prefix %></span> <span>:</span> <span> <%= agreementTOTenants.getTenantNameArabic() %> </span> 
				</td>
			</tr> --%>
			<tr>
				<td>
				<span>  <%= transferInfo.get("buildingName") %> </span>	<span> المستأجر الحالي للبناية رقم </span>
				</td>
			</tr>
			<tr>
				<td>
				<span>  <%=  transferInfo.get("addressLine1")  %> </span>	<span> في </span>
				</td>
			</tr>
		</table>
	</div>
			
	<div style="margin-top: 30px;" align="center"> 
		<p>... و تفضلوا بقبول فائق الاحترام والتقدير  </p>
	</div>
	
	<div style="padding: 20px; margin-left: 50px; margin-top: 30px; width: 150px; text-align: right;">
		<span>: مقدم الطلب</span>
		<br/><br/>
		<span> <%=  transferInfo.get("ownerName")  %> </span>
	</div>

</body>
</html>