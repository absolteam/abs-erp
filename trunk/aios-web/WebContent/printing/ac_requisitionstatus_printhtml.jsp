<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
	height: 28px;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}

.align-left {
	text-align: left !important;
}

.lstln td {
	border-top: 3px double #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 1px double #000;
	font-weight: bold;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"
			style="font-family: book; font-style: italic; text-transform: inherit !important; font-size: 28px !important; color: black;"><strong>${THIS.companyName}</strong>
		</span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading" style="font-family: book;"><span><u>REQUISITION
					STATUS REPORT </u> </span> </span>
	</div>
	<div style="clear: both;"></div>
	<div class="clear-fix"></div>
	<div style="margin-left: 6px; padding: 2px;">
		<div>REQUISITION NO : ${REQUISITION_CYCLE.referenceNumber}</div>
		<div>DATE : ${REQUISITION_CYCLE.date}</div>
	</div>
	<div
		style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
		class="width100 float_left">
		<table style="width: 99%; margin: 0 auto;">
			<tr>
				<th style="width: 5%;">CODE</th>
				<th style="width: 30%;">PRODUCT</th>
				<th style="width: 3%;">REQUSTED QTY</th>
				<th style="width: 3%;">QUOTATION</th>
				<th style="width: 3%;">ORDERED QTY</th>
				<th style="width: 3%;">RECEIVED QTY</th>
				<th style="width: 3%;">RETURNED QTY</th>
				<th style="width: 3%;">RECEIVE PENDING</th>
				<th style="width: 3%;">ISSUED QTY</th>
				<th style="width: 3%;display:none;">ISSUE RETURNS</th>
			</tr>
			<c:set var="dashsign" value="-" />
			<c:forEach items="${REQUISITION_CYCLE.requisitionDetailVOs}"
				var="cycle">
				<tr>
					<td>${cycle.productCode}</td>
					<td>${cycle.productName}</td>
					<td>${cycle.requisitionQty}</td>
					<td><c:choose>
							<c:when
								test="${cycle.quotationQty ne null && cycle.quotationQty ne ''}">
								${cycle.quotationQty}
							</c:when>
							<c:otherwise>${dashsign}</c:otherwise>
						</c:choose></td>
					<td><c:choose>
							<c:when
								test="${cycle.purchaseQty ne null && cycle.purchaseQty ne ''}">
								${cycle.purchaseQty}
							</c:when>
							<c:otherwise>${dashsign}</c:otherwise>
						</c:choose></td>
					<td><c:choose>
							<c:when
								test="${cycle.receiveQty ne null && cycle.receiveQty ne ''}">
								${cycle.receiveQty}
							</c:when>
							<c:otherwise>${dashsign}</c:otherwise>
						</c:choose></td>
					<td><c:choose>
							<c:when
								test="${cycle.returnQty ne null && cycle.returnQty ne ''}">
								${cycle.returnQty}
							</c:when>
							<c:otherwise>${dashsign}</c:otherwise>
						</c:choose></td>
					<td>${cycle.balanceQty}</td>
					<td><c:choose>
							<c:when
								test="${cycle.issuedQty ne null && cycle.issuedQty ne ''}">
								${cycle.issuedQty}
							</c:when>
							<c:otherwise>${dashsign}</c:otherwise>
						</c:choose></td>
					<td style="display:none;"><c:choose>
							<c:when
								test="${cycle.issueReturnQty ne null && cycle.issueReturnQty ne ''}">
								${cycle.issueReturnQty}
							</c:when>
							<c:otherwise>${dashsign}</c:otherwise>
						</c:choose></td>
				</tr>
			</c:forEach>
			<tr class="lstln">
				<td colspan="2">TOTAL</td>
				<td>${REQUISITION_CYCLE.totalRequisitionQty}</td>
				<td>${REQUISITION_CYCLE.totalQuotationQty}</td>
				<td>${REQUISITION_CYCLE.totalPurchaseQty}</td>
				<td>${REQUISITION_CYCLE.totalReceiveQty}</td>
				<td>${REQUISITION_CYCLE.totalReturnQty}</td>
				<td>${REQUISITION_CYCLE.totalReceivePendingQty}</td>
				<td>${REQUISITION_CYCLE.totalIssuedQty}</td>
				<td style="display:none;">${REQUISITION_CYCLE.totalIssueReturnQty}</td>
			</tr>
		</table>
	</div>
	<div class="clear-fix"></div>
	<c:if
		test="${REQUISITION_CYCLE.purchaseDetailVOs ne null && fn:length(REQUISITION_CYCLE.purchaseDetailVOs) > 0}">
		<div
			style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; margin-top: 5px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
			class="width100 float_left">
			<table style="width: 99%; margin: 0 auto;">
				<tr>
					<td colspan="6" class="align-left"
						style="font-weight: bold; padding: 2px;">PURCHASE DETAILS</td>
				</tr>
				<tr>
					<th style="width: 5%;">REFERENCE</th>
					<th style="width: 3%;">DATE</th>
					<th style="width: 3%;">SUPPLIER</th>
					<th style="width: 3%;">PRODUCT</th>
					<th style="width: 3%;">UNIT RATE</th>
					<th style="width: 3%;">QUANTITY</th>
				</tr>
				<c:forEach items="${REQUISITION_CYCLE.purchaseDetailVOs}"
					var="purchase">
					<tr>
						<td>${purchase.referenceNumber}</td>
						<td>${purchase.purchaseDate}</td>
						<td>${purchase.supplierName}</td>
						<td>${purchase.productName}</td>
						<td>${purchase.unitRate}</td>
						<td>${purchase.quantity}</td>
					</tr>
				</c:forEach>
			</table>
			<div class="clear-fix"></div>
		</div>
	</c:if>
	<c:if
		test="${REQUISITION_CYCLE.receiveDetailVOs ne null && fn:length(REQUISITION_CYCLE.receiveDetailVOs) > 0}">
		<div
			style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; margin-top: 5px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
			class="width100 float_left">
			<table style="width: 99%; margin: 0 auto;">
				<tr>
					<td colspan="7" class="align-left"
						style="font-weight: bold; padding: 2px;">RECEIVE DETAILS</td>
				</tr>
				<tr>
					<th style="width: 5%;">REFERENCE</th>
					<th style="width: 3%;">DATE</th>
					<th style="width: 5%;">PO NUMBER</th>
					<th style="width: 3%;">SUPPLIER</th>
					<th style="width: 3%;">PRODUCT</th>
					<th style="width: 3%;">UNIT RATE</th>
					<th style="width: 3%;">QUANTITY</th>
				</tr>
				<c:forEach items="${REQUISITION_CYCLE.receiveDetailVOs}"
					var="receive">
					<tr>
						<td>${receive.referenceNumber}</td>
						<td>${receive.receiveDate}</td>
						<td>${receive.purchaseNumber}</td>
						<td>${receive.supplierName}</td>
						<td>${receive.productName}</td>
						<td>${receive.unitRate}</td>
						<td>${receive.receiveQty}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div class="clear-fix"></div>
	</c:if>
	<c:if
		test="${REQUISITION_CYCLE.issueRequistionDetailVOs ne null && fn:length(REQUISITION_CYCLE.issueRequistionDetailVOs) > 0}">
		<div
			style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; margin-top: 5px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
			class="width100 float_left">
			<table style="width: 99%; margin: 0 auto;">
				<tr>
					<td colspan="6" class="align-left"
						style="font-weight: bold; padding: 2px;">ISSUANCE DETAILS</td>
				</tr>
				<tr>
					<th style="width: 5%;">REFERENCE</th>
					<th style="width: 3%;">DATE</th>
					<th style="width: 3%;">PERSON</th>
					<th style="width: 3%;">PRODUCT</th>
					<th style="width: 3%;">UNIT RATE</th>
					<th style="width: 3%;">QUANTITY</th>
				</tr>
				<c:forEach items="${REQUISITION_CYCLE.issueRequistionDetailVOs}"
					var="issue">
					<tr>
						<td>${issue.referenceNumber}</td>
						<td>${issue.issueDate}</td>
						<td>${issue.personName}</td>
						<td>${issue.productName}</td>
						<td>${issue.unitRate}</td>
						<td>${issue.quantity}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>
</body>
</html>