<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" charset="utf-8" /> 
	<title>Contract Details</title>
</head>

<body onload="window.print();" style="font-size: 10px !important;">
	<%
		List<ReContractAgreementTO> agreementTOOffer = (List<ReContractAgreementTO>)session
				.getAttribute("AGREEMENT_OFFER_DETAILS");
		ReContractAgreementTO agreementTOTenants = (ReContractAgreementTO)session
				.getAttribute("AGREEMENT_TENANT_DETAILS"); 
		String specifiedProperty = "";
		String prefix = ""; 	
	%>
	<c:choose>
		<c:when test="${contractPrintStyle == 'multiple'}">
			<%
				for(ReContractAgreementTO offered : agreementTOOffer) {
					specifiedProperty =  offered.getFlatNumber(); %> 
					
					<div style="height: 900px; display: block; margin-top: 100px;">
						<%@ include file="re_contract.jsp"%>
					</div>
					<br/><br/>
					<div style="height: 750px; display: block; font-size: 13px;">
						<%@ include file="re_transferLetter.jsp"%>	
					</div>
				
			<% } %>
		</c:when>
		<c:otherwise>
			<%
				for(ReContractAgreementTO offered : agreementTOOffer) {
					specifiedProperty +=  offered.getFlatNumber() + " - "; 
				}
				specifiedProperty = AIOSCommons.removeTrailingSymbols(specifiedProperty);
			%> 
			<div style="height: 900px; display: block; margin-top: 100px;">
				<%@ include file="re_contract.jsp"%>
			</div>
			<br/><br/>
			<div style="height: 750px; display: block; font-size: 13px;">
				<%@ include file="re_transferLetter.jsp"%>	
			</div>
		</c:otherwise>
	</c:choose>

</body>
</html>