<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Requisition</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
	height: 23px;
} 

.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:22px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
@page {
  size: 148mm 210mm ;
  margin-top: 130px;
  margin-bottom: 20px;
}
 @media screen {
       div.divHeader {
            position: fixed;
          top:0;
          clear: both;
       }
   }
 @media print {
 		#data-content-table > div{ page-break-inside: avoid;page-break-before: auto;}
      div.divHeader {
          position: fixed;
          top:0;
          clear: both;
      }
  }
 </style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
var	perPage=Number(650);
var lstSize=0;
var referenceNumber=null;
var headerHeight=Number(150);
var signatoryHeigh=Number(120);
var ht=0;
$(function(){
	var x=$("#data-content-table tr:last").offset();
	var trPosition=Number(x.top);
	if(trPosition>=perPage){
		perPage=Number(740);
		recursivePageBreakCall(perPage,ht,trPosition,lstSize);
	}else{
		if(trPosition>=1){
			ht=Number(Number(perPage-trPosition)-50);
			lstSize=Number(ht/25);
			lstSize=Math.floor(lstSize);
			for(var j=0;j<lstSize;j++){
				var myRow = "<tr style='height:25px;'><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				$("#data-content-table tr:last").after(myRow);
			}
		}
	}
});
function recursivePageBreakCall(perPage,ht,trPosition,lstSize){
	//alert("old pos--"+trPosition);
	trPosition=Number(trPosition-perPage);
	//alert("after sliced a page--"+trPosition);
	if(trPosition>=perPage){
		perPage=Number(850);
		recursivePageBreakCall(perPage,ht,trPosition,lstSize);
	}else{
		if(trPosition>=1){
			ht=Number(650-trPosition);
			lstSize=Number(ht/25);
			lstSize=Math.floor(lstSize);
			for(var j=0;j<lstSize;j++){
				var myRow = "<tr style='height:25px;'><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				$("#data-content-table tr:last").after(myRow);
			}
		}
		
	}
}
</script>
<body onload="window.print();" id="content" style="font-size: 12px;"> 
	<div class="divHeader" style="color: red; float: right;font-size: 15px;"><b>No:</b> ${REQUISITION.referenceNumber}</div>

	<div class="width98" ><span class="heading" style="font-family: book;"><span><u>REQUISITION SLIP</u></span></span></div>
	<div style="clear: both;"></div>	
	<div style="position:relative; top: 20px; border-style: none; border-width: 1px; -moz-border-radius: 0px; 
		 -webkit-border-radius:0px; border-radius: 0px;" class="width100 float_left">
		
		<div style="color: #333; float: right; padding: 5px;">
			<div><b>Date:</b> <span style="border-bottom: 1px dotted;font-size: 15px;">&nbsp;${REQUISITION.requisitionDate}&nbsp;&nbsp;</span></div>
		</div>
		<div style="color: #333; float: left; padding: 5px;">
			<div style=""><b>Supplier/Description:</b> ${REQUISITION.description}</div>
		</div>
		
		<br/>
		
			<table style="width: 99%; margin: 0 auto;" class="print-friendly" id="data-content-table">
				<thead>
					<tr>  
						<th>S. No.</th>
						<th>Item Code</th>
						<th>Description of Article</th>
						<th>Purpose</th>
						<th>Unit</th>
						<th>Quantity Total</th>
						<th>Last P.O Unit Rate</th>
					</tr>
				</thead>
				<c:set var="cnt" value="1" />
				<tbody>
					<c:forEach items="${REQUISITION_DETAIL}" var="details">
						<tr> 
							<td>
								${cnt}
								<c:set var="cnt" value="${cnt + 1}" />
							</td>
							<td>
								${details.product.code}
							</td>
							<td>
								${details.product.productName}
							</td>
							<td>
								<c:if test="${details.description ne null && details.description ne ''}">
									&nbsp;(${details.description})
								</c:if>
							</td>
							<td>
								${details.product.lookupDetailByProductUnit.displayName}
							</td>
							<td>
								${details.quantity} &nbsp;
							</td>
							<td>
								${details.unitRate} &nbsp;
							</td>
						</tr>
					</c:forEach>
				</tbody>
		</table>
		<table width="100%" align="center" style="border: 0px; height:125px;">
			<tr valign="bottom" style="border: 0px;">
				<td style="border: none; height: 50px;" width="25%" align="center">
					<span style="display: block; width: 90%; margin: 0 auto;font-weight: bold;">${REQUISITION.person.firstName} ${REQUISITION.person.lastName}</span>
					<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;font-weight: bold;">Requested By</span>
				</td>	
				<td style="border: none;" width="25%" align="center">
					<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;font-weight: bold;">Store Keeper</span>
				</td>		
				<td style="border: none;" width="25%" align="center">
					<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;font-weight: bold;">Purchaser</span>
				</td>
				<td style="border: none;" width="25%" align="center">
					<span style="border-top: 1px dotted; display: block; width: 90%; margin: 0 auto;font-weight: bold;">Operations Manager</span>
				</td>																					
			</tr>
		</table>
		<table width="100%" style="border: 0px;"class="float-left">
			<tr style="border: 0px;" class="float-left">
				<td style="font-size: 9px;border: 0px;text-align:left;" class="float-left">CC = White=>Purchase, Pink => Finance , Yellow => Store, Green =>Book</td>
			</tr>
		</table>
	</div>  
	</body>
</html>