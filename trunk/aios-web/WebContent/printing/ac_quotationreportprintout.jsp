<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Journal Voucher</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>QUOTATION</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width40 float_right">
			 <div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="hr.supplier.supplierNumber" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					${QUOTATION_INFO.supplier.supplierId}
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="hr.supplier.supplierName" /> <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					${QUOTATION_INFO.supplier.person.firstName} ${QUOTATION_INFO.supplier.person.lastName} &nbsp;
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="accounts.tender.add.description" /><span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					 ${QUOTATION_INFO.description}
				</span>
			</div>
		</div>  
		<div class="width50 float_left">
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="accounts.tender.add.tenderNo" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${QUOTATION_INFO.tender.tenderNumber}</span></span>  
			 </div>
			 <div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="accounts.quotation.quotationNumber" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${QUOTATION_INFO.quotationNumber}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="accounts.quotation.currency" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					 ${QUOTATION_INFO.currency.currencyPool.code}
				</span>  		
			</div> 
			 <div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="accounts.quotation.quotationDate" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:set var="date" value="${QUOTATION_INFO.quotationDate}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%>
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="accounts.quotation.quotationExpiry" /> <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					<c:set var="date" value="${QUOTATION_INFO.expiryDate}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%>
				</span>
			</div>
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th style="width:5%"><fmt:message key="accounts.quotation.itemType" /></th>   
			    <th style="width:5%"><fmt:message key="accounts.quotation.product" /></th> 
			    <th style="width:5%">UOM</th>  
			    <th style="width:5%"><fmt:message key="accounts.quotation.unitRate" /></th> 
			    <th style="width:5%"><fmt:message key="accounts.quotation.quantity" /></th> 
			    <th style="width:5%"><fmt:message key="accounts.quotation.totalAmount" /></th> 
			    <th style="width:5%"><fmt:message key="accounts.quotation.description" /></th>
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:forEach begin="0" end="6" step="1">
						<td/>
					</c:forEach>
				</tr>
				<c:forEach items="${requestScope.QUOTATION_INFO.quotationDetails}" var="result" varStatus="status1"> 
					<tr> 
						<td>${result.product.itemType }</td>
						<td>${result.product.productName}</td>
						<td>${result.product.lookupDetailByProductUnit.displayName}</td>
						<td>${result.unitRate}</td>
						<td>${result.totalUnits}</td>
						<td>${result.unitRate * result.totalUnits}</td>
						<td>${result.description}</td>
					</tr>
				</c:forEach> 
				<c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="6" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	</body>
</html> 