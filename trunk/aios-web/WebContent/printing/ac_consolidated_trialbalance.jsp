<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style>
 .ui-autocomplete-input { width:50%!important;}
	  .ui-autocomplete{height:250px;
	  	overflow-y: auto;
	  	overflow-x: hidden;
	  	}
	  	.ui-combobox-button{height: 32px;}
</style>
<script type="text/javascript">  
$(function(){
	 
	$('.print-call').click(function(){ 
		window.open('consolidated_trialbalance_print.action?format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px'); 
		return false;
	}); 

	$('.pdf-download-call').click(function(){ 
		return false;
	});

	$(".xls-download-call").click(function(){ 
		return false;
	});  
}); 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			 trial balance
		</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left "
					style="margin: 10px; display:none; background: none repeat scroll 0 0 #d14836;">
					<div class="portlet-header ui-widget-header float-right"
						id="list_grid" style="cursor: pointer; color: #fff;">
						list gird
					</div> 
				</div>
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="fiscalstatement">
				<table class="display" id="LedgerFiscalDT"></table>
			</div> 
		</div> 
	</div> 
</div>
<div class="process_buttons">
		<div id="hrm" class="width100 float-right ">
		  	<div class="width5 float-right height30" title="Download as PDF" style="display: none;"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
		 	<div class="width5 float-right height30" title="Download as XLS" style="display: none;"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
		  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
		 </div>
</div> 