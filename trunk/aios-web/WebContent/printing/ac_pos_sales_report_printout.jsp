<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />

</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.percentage-mode{float: left; font-weight: bold; font-size: 9px; position: relative; top: 3px; left: 3px;}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<div class="width98">
			<span class="heading"><span
				style="font-size: 25px !important;">${THIS.companyName}</span> </span>
		</div>

	</div>
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span>POS SALES REPORT</span> </span>
	</div>
	<div class="width100">
		<span class="side-heading" style="float: right; padding: 5px;">Printed
			on : ${requestScope.PRINT_DATE}</span>
	</div>
	<div class="clear-fix"></div>
	<c:forEach var="pointOfSale" items="${POS}">
		<div class="width100 float-left" style="margin-bottom: 10px;">
			<div style="border: 1px solid #dddddd; height: 100%; 
				margin: 5px; padding:1px; background-color: #F5ECCE; float:left; width: 98%;">
				<div class="data-container width40 float-right">
					<div class="data-container width100 float-left">
						<label class="caption width20 float-left">Till :</label> <label
							class="data width40 float-left">${pointOfSale.tillNumber}&nbsp;</label>
					</div>
					<div class="data-container width100 float-left">
						<label class="caption width20 float-left">Cashier :</label> <label
							class="data width40 float-left">${pointOfSale.posUserName}&nbsp;</label>
					</div>
				</div>
				<div class="data-container width50 float-left">
					<div class="data-container width100 float-left">
						<label class="caption width20 float-left">Reference No. :</label>
						<label class="data width40 float-left">${pointOfSale.referenceNumber}&nbsp;</label>
					</div>
					<div class="data-container width100 float-left">
						<label class="caption width20 float-left">Date :</label> <label
							class="data width40 float-left">${pointOfSale.date}&nbsp;</label>
					</div>
					<div class="data-container width100 float-left">
						<label class="caption width20 float-left">Store :</label> <label
							class="data width40 float-left">${pointOfSale.storeName}&nbsp;</label>
					</div>
				</div>
				<div class="data-list-container width100 float-left"
					style="margin-top: 10px;">
					<div class="width100 float-left">
						<span class="side-heading">SALES DETAILS</span>
					</div>
					<c:set var="toalQuantity" value="${0}" />
					<c:set var="toalAmount" value="${0}" />
					<c:set var="toalUnitRate" value="${0}" />
					<c:set var="toalDiscount" value="${0}" />
					<table class="width100 float_left">
						<tr>
							<th>PRODUCT</th>
							<th>QUANTITY</th>
							<th>UNIT RATE</th>
							<th>DISCOUNT</th>
							<th>TOTAL</th>
						</tr>
						<tbody>
							<c:forEach items="${pointOfSale.pointOfSaleDetailVOs}"
								var="sales">
								<tr>
									<td>${sales.productName}</td>
									<td>${sales.quantity}</td>
									<td style="text-align: right;">${sales.unitRate}</td>
									<td style="text-align: right;"><c:choose> 
											<c:when
												test="${sales.discountValue ne null && sales.discountValue gt 0}">
												<c:choose>
													<c:when test="${posdetail.isPercentageDiscount eq true}">
														<span class="percentage-mode">(Percentage)</span>
													</c:when>
													<c:otherwise><span class="percentage-mode">(Amount)</span></c:otherwise>
												</c:choose>
													${sales.discount}
												</c:when>
											<c:otherwise>-N/-A</c:otherwise>
										</c:choose></td>
									<td style="text-align: right;">${sales.totalPrice}</td>
								</tr>
								<c:set var="toalQuantity" value="${toalQuantity+sales.quantity}" />
								<c:set var="toalAmount" value="${toalAmount+sales.totalAmount}" />
								<c:set var="toalDiscount"
									value="${toalDiscount+sales.discountValue}" />
								<c:set var="toalUnitRate" value="${toalUnitRate+sales.unitRate}" />
							</c:forEach>
							<tr>
								<td>&nbsp;</td>
								<td style="text-align: right; font-weight: bold;"><span
									style="font-size: 10px;">TOTAL QUANTITY</span> :&nbsp;&nbsp;
									${toalQuantity}</td>
								<td style="text-align: right; font-weight: bold;"><span
									style="font-size: 10px;">TOTAL AMOUNT :&nbsp;&nbsp;</span>
									${toalUnitRate}</td>
								<td style="text-align: right; font-weight: bold;"><span
									style="font-size: 10px;">TOTAL DISCOUNT :&nbsp;&nbsp;</span>
									${toalDiscount}</td>
								<td style="text-align: right; font-weight: bold;"><span
									style="font-size: 10px;">TOTAL SALES :&nbsp;&nbsp; </span>${toalAmount}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear-fix"></div>
				<c:if
					test="${pointOfSale.creditDetailVOs ne null && fn:length(pointOfSale.creditDetailVOs)>0}">
					<div class="float_left" style="float: right; width: 100%;">
						<div class="width100 div_text_info">
							<span class="side-heading">SALES
								RETURNS </span>
						</div>
						<table class="width100">
							<tr>
								<th>REFERENCE</th>
								<th>DATE</th>
								<th>PRODUCT</th>
								<th>RETURN TYPE</th>
								<th>RETURN QUANTITY</th>
								<th>RETURN AMOUNT</th> 
							</tr>
							<tbody>
								<c:set var="totalAmount" value="${0}" />
								<c:forEach items="${pointOfSale.creditDetailVOs}"
									var="salesReturn">
									<tr>
										<td>${salesReturn.referenceNumber}</td>
										<td>${salesReturn.returnDate}</td>
										<td>${salesReturn.productName}</td>
										<td>${salesReturn.returnType}</td>
										<td>${salesReturn.returnQuantity}</td>
										<td style="text-align: right;">${salesReturn.returnAmount}</td>
									</tr>
									<c:set var="totalAmount" value="${totalAmount+salesReturn.returnAmount}" />
								</c:forEach>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td style="text-align: right; font-weight: bold;"><span
										style="font-size: 10px;">TOTAL AMOUNT:</span>&nbsp;&nbsp;
										${totalAmount}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:if>	
				<div class="clear-fix"></div>
				<c:if
					test="${pointOfSale.pointOfSaleReceiptVOs ne null && fn:length(pointOfSale.pointOfSaleReceiptVOs)>0}">
					<div class="width50 float_left" style="float: left;">
						<div class="width100 div_text_info">
							<span class="float_left left_align width28 text-bold"
								style="font-weight: bold;">RECEIPT DETAILS </span>
						</div>
						<table class="width100">
							<tr>
								<th>RECEIPT TYPE</th>
								<th>AMOUNT</th>
							</tr>
							<tbody>
								<c:set var="totalAmount" value="${0}" />
								<c:forEach items="${pointOfSale.pointOfSaleReceiptVOs}"
									var="receipt">
									<tr>
										<td>${receipt.receiptTypeStr}</td>
										<td><span style="float: right;">${receipt.receiptAmountStr}</span>
										</td>
									</tr>
									<c:set var="totalAmount"
										value="${totalAmount+receipt.receiptAmount}" />
								</c:forEach>
								<tr>
									<td>&nbsp;</td>
									<td style="text-align: right; font-weight: bold;"><span
										style="font-size: 10px;">TOTAL RECEIPT:</span> <span
										style="text-align: right; font-weight: bold;">${totalAmount}</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:if>
				<c:if
					test="${pointOfSale.pointOfSaleChargeVOs ne null && fn:length(pointOfSale.pointOfSaleChargeVOs)>0}">
					<div class="float_right" style="float: right; width: 48%;">
						<div class="width100 div_text_info">
							<span class="float_left left_align width28 text-bold"
								style="font-weight: bold; float: left;">ADDITIONAL
								CHARGES </span>
						</div>
						<table class="width100">
							<tr>
								<th>CHARGES TYPE</th>
								<th>AMOUNT</th>
							</tr>
							<tbody>
								<c:set var="totalAmount" value="${0}" />
								<c:forEach items="${pointOfSale.pointOfSaleChargeVOs}"
									var="charge">
									<tr>
										<td>${charge.chargeType}</td>
										<td style="text-align: right;">${charge.charges}</td>
									</tr>
									<c:set var="totalAmount" value="${totalAmount+charge.charges}" />
								</c:forEach>
								<tr>
									<td>&nbsp;</td>
									<td style="text-align: right; font-weight: bold;"><span
										style="font-size: 10px;">TOTAL AMOUNT:</span>&nbsp;&nbsp;
										${totalAmount}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:if> 
			</div>
		</div>

		<div class="clear-fix"></div>
	</c:forEach>
	<div class="clear-fix"></div>
</body>
</html>