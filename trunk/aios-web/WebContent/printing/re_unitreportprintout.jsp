<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><fmt:message key="re.property.propertyunitreport"/></title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading"><fmt:message key="re.property.propertyunitreport"/></span></div>
	<div class="width100 float-left"><span class="side-heading"><fmt:message key="re.property.filtercriteria"/></span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
			<div>
					<label class="caption width30" ><fmt:message key="re.property.unitstatus"/></label>
					<label class="data width50">${UNIT_MASTER.unitStatus}</label>
				</div>
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading"><fmt:message key="re.property.propertyunitinformation"/></span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(UNIT_LIST)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th><fmt:message key="re.property.propertyname"/></th>
						<th><fmt:message key="re.property.componentname"/></th>
						<th><fmt:message key="re.property.unitname"/></th> 
						<th><fmt:message key="re.property.unitnumber"/></th>
						<th><fmt:message key="re.property.unittype"/></th>
						<th><fmt:message key="re.property.unitsize"/></th>
						<th><fmt:message key="re.property.size"/></th>
						<th><fmt:message key="re.property.unitstatus"/></th>
						<th><fmt:message key="re.property.unitdetails"/></th>
						
					</tr> 
				</thead> 
				<tbody >	
				
					<c:forEach items="${UNIT_LIST}" var="result3" varStatus="status1" >
					 	<tr> 
					 		<td>${result3.propertyName}</td>
							<td>${result3.componentName}</td>
							<td>${result3.unitName}</td>
							<td>${result3.unitNumber}</td>
							<td>${result3.unitType}</td>
							<td>${result3.sizeWithUom}</td>
							<td>${result3.rent}</td>
							<td>${result3.unitStatus}</td>
							<td>
								<span class="inside-heding">Bed room :</span> ${result3.bedroomSize} ,
								<span class="inside-heding">Bath room :</span> ${result3.bathroomSize},
								<span class="inside-heding">Hall :</span> ${result3.hallSize},
								<span class="inside-heding">Kitchen : </span>${result3.kitchenSize}
							</td>
							
						</tr>
					</c:forEach>
				 
				</tbody>
			</table>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid"><fmt:message key="re.property.nounits"/></tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>