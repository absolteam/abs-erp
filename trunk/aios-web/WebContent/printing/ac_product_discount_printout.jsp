<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Journal Voucher</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PRODUCT DISCOUNT REPORT</span></span></div>
	<div style="clear: both;"></div>
		<c:forEach items="${DISCOUNT}" var="discount">
			<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
				 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
				<div class="width60 float_right">
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">DISCOUNT TYPE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${discount.discountTypeName}</span>
						</span>  
					 </div>
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">PRICING TYPE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${discount.priceListName}</span>
						</span>  
					 </div>
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">MINIMUM SALE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${discount.minimumValue}</span>
						</span>  
					 </div>
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">DISCOUNT OPTION<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<span style="color: #CC2B2B;">${discount.discountOptionName}</span>
						</span>  
					 </div> 
				</div>  
				<div class="width40 float_left">
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">DISCOUNT NAME<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
							${discount.discountTo}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">START DATE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
							${discount.validFrom}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">END DATE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
							${discount.validTo}
						</span>
					</div>
				</div> 
			</div>
		
	
		<div class="clear-fix"></div>
		<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
			 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
			<table class="width100">
			<c:if test="${discount.discountOptionName == 'Customer'}">
				<tr>  
					<th>CUSTOMER REFERENCE</th>
					<th>CUSTOMER NAME</th>
					<th>MEMBER CARD TYPE</th>  
					<th>MEMBER CARD NO.</th>
				</tr>
			</c:if>
			<c:if test="${discount.discountOptionName != 'Customer'}">
				<tr>  
					<th>PRODUCT CODE</th>
					<th>PRODUCT</th>
					<th>CATEGORY</th>  
					<th>PRDUCT SUB CATEGORY</th>
				</tr>
			</c:if>
				<tbody>  
					 <tr style="height: 25px;">
						<c:forEach begin="0" end="3" step="1">
							<td/>
						</c:forEach>
					</tr>
					<c:if test="${discount.discountOptionName == 'Customer'}">
						<c:forEach items="${discount.discountOptionVOs}" var="option">
							<tr> 
								<td>
									${option.customerReference}
								</td>
								<td>
									${option.customerName}
								</td>
								<td>
									${option.memberCardType}
								</td>
								<td>
									${option.memberCardNo}
								</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${discount.discountOptionName != 'Customer'}">
						<c:forEach items="${discount.discountOptionVOs}" var="option">
							<tr> 
								<td>
									${option.productCode}
								</td>
								<td>
									${option.productName}
								</td>
								<td>
									${option.productCategoryName}
								</td>
								<td>
									${option.productSubCategory}
								</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:forEach begin="0" end="1" step="1">
						<tr style="height: 25px;">
							<c:forEach begin="0" end="3" step="1">
								<td/>
							</c:forEach>
						</tr>  
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="clear-fix"></div>
		<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
			 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
			<table class="width100">
				<tr>  
					<th>FLAT</th>
					<th>SALES AMOUNT</th>
					<th>DISCOUNT</th>  
				</tr>
				<tbody>  
					 <tr style="height: 25px;">
						<c:forEach begin="0" end="2" step="1">
							<td/>
						</c:forEach>
					</tr>
						<c:forEach items="${discount.discountMethodVOs}" var="method">
							<tr> 
							<td>
								${method.flatAmount}
							</td>
							<td>
								${method.salesAmount}
							</td>
							<td>
								${method.discountAmount}
							</td>
						</tr>
					</c:forEach>
					<c:forEach begin="0" end="1" step="1">
						<tr style="height: 25px;">
							<c:forEach begin="0" end="2" step="1">
								<td/>
							</c:forEach>
						</tr>  
					</c:forEach>
				</tbody>
			</table>
		</div>
		</c:forEach> 
	<div class="clear-fix"></div>
	</body>
</html>