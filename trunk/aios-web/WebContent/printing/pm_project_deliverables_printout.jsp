<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Deliverables</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PROJECT DELIVERABLES REPORT</span></span></div>
	<div style="clear: both;"></div>
	
	<c:if test="${VIEW_TYPE eq 'single'}">
	<c:forEach items="${PROJECTS_DELIVERABLES}" var="delivery" varStatus="status">
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width60 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width35 text-bold">DESCRIPTION<span class="float_right">:</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${delivery.description}&nbsp;</span></span>  
			 </div> 
			
		</div>  
		<div class="width40 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">REFERENCE NO.<span class="float_right"> :</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;"> 
					 ${delivery.referenceNumber}&nbsp;
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">DELIVERY TITLE <span class="float_right"> :</span></span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">  
					${delivery.deliveryTitle}&nbsp;
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">DELIVERY DATE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${delivery.date}&nbsp;
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">${sessionScope.project_projectTitle}<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					 ${delivery.projectTitle}&nbsp;
				</span>
			</div> 
			<div class="width100 div_text_info">
				<span class="float_left left_align width35 text-bold">${sessionScope.project_projectTitle} MILESTONE<span class="float_right"> :</span> </span>
				<span class="span_border left_align width60 text_info" style="display: -moz-inline-stack;">
					${delivery.mileStoneTitle}&nbsp;
				</span>
			</div> 
			
		</div> 
	</div>
	</c:forEach>
	</c:if>
	<div class="clear-fix"></div>
	<c:if test="${VIEW_TYPE eq 'multiple'}">
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>REFERENCE NUMBER</th>
				<th>TITLE</th>
				<th>DELIVERY DATE</th> 
				<th>${sessionScope.project_projectTitle}</th>
				<th>MILESTONE</th>
			</tr>
			<tbody>  
				<c:forEach items="${PROJECTS_DELIVERABLES}" var="delivery" varStatus="status">
					<tr> 
						<td>${delivery.referenceNumber}</td>
						<td>${delivery.deliveryTitle}</td> 
						<td>${delivery.date}</td> 
						<td>${delivery.projectTitle}</td> 
						<td>${delivery.mileStoneTitle}</td> 
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</c:if>  
	<div class="clear-fix"></div>
	</body>
</html>