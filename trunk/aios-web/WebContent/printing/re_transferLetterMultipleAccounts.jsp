﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Offer Details</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />	
</head>
<body style="direction: rtl;">	

	<div class="AIOTitle">

	</div>

	<div style="margin-top: 50px;" align="right">
		
		<p> ${AGREEMENT_DETAILS.contractDate}  <span>:التاريخ </span> </p>	
		<p class="arabic_label">رقم المرجع: ${AGREEMENT_DETAILS.contractNumber}</p>	
			
	</div>
	
	<div style="margin-top: 50px; display: block; width: 100%;">			
		<span class="bold" style="float: left; margin-left: 100px;"> المحترمين </span>
		<span class="bold" style="float: right;">السادة / شركة أبوظبي للتوزيع </span>	
	</div>
	
	<div style="margin-top: 100px; display: block;" align="right">	
		<p class="bold">.. تحية طيبة وبعد </p>	
	</div>


	<%
		String temp1 = (String) session.getAttribute("TRANSFER_SUBJECT"); 
		if(temp1.equals("Connect and Transfer")) {
			temp1 = "وصل و ";
		}
		else {
			temp1 = "";
		}
	%>
	
	<div style="margin-top: 50px;" align="center">
		<p class="bold" style="direction: rtl;">
		<u> <span style="direction: rtl;"> الموضوع :</span> <span> طلب </span> <span style="direction: ltr;"> <%= temp1 %> </span> <span style="direction: rtl;">   تحويل التيار بإسم مستهلك جديد </span>  </u></p>		
	</div>
	
	<div style="margin-top: 20px; margin-bottom: 30px; width: 80%; margin: 0 auto;" align="center">
		<p style="direction: rtl;"> 
		
		 <span>    بالإشارة إلى الموضوع أعلاه ، يرجى من  سيادتكم التكرم بتحويل الاشتراكات الموضحة أدناه ، باسم المستأجر الحالي    </span> <span><%= prefix %></span> <span>:</span> <span> <%= agreementTOTenants.getTenantNameArabic() %> </span> <span> ، و ذلك للعقار </span>  <span>  <%= agreementTOOffer.get(0).getBuildingName() %> </span> <span> في  </span> <span><%=  agreementTOOffer.get(0).getBuildingArea()  %> . </span>  <span> </span>      
	  		</p>
	</div>

	<div align="right">: أرقام الاشتراكات </div>	
	<br/>
	<div align="center">
	
		<%
			String[] accountNos = ((String) session.getAttribute("TRANSFER_ACCOUNT")).split(",");
			int maxColumns = 6, i = 0;
		%>
	
		<table style="width: 100%;">
			<tr>
				<%  for(String accountNo : accountNos) { 
					if(i++ == maxColumns) { 
						i = 1; %>
						</tr>
						<tr>
						
					<% } %>
						<td>
							<%= accountNo %> 
						</td>							
				<% } %>
			</tr>
		</table>
	</div>
			
	<div style="margin-top: 30px;" align="center"> 
		<p>.. و تفضلوا بقبول فائق الاحترام والتقدير  </p>
	</div>
	
	<div style="padding: 20px; margin-left: 0px; margin-top: 30px; width: 150px; text-align: right;">
		<span>: مقدم الطلب</span>
		<br/><br/>
		<span> <%= agreementTOOffer.get(0).getOfferedPropertyOwners_arabic() %> </span>
	</div>

</body>
</html>