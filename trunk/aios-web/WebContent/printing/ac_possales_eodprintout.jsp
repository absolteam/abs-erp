<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Daily Sales Print</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />

</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	margin: 5px 0 5px 0;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.percentage-mode{float: left; font-weight: bold; font-size: 9px; position: relative; top: 3px; left: 3px;}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<div class="width98">
			<span class="heading"><span
				style="font-size: 25px !important;">${THIS.companyName}</span> </span>
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span>EOD SALES REPORT</span> </span>
	</div>
	<div class="width100">
		<span class="side-heading" style="float: right; padding: 5px;">Printed
			on : ${requestScope.PRINT_DATE}</span>
	</div>
	<c:forEach items="${POS}" var="pos">
		<div class="clear-fix"></div>
		<c:set var="totalQuantity" value="${0}" />
		<c:set var="totalAmount" value="${0}" />
		<c:set var="totalDiscount" value="${0}" />
		<c:set var="totalUnitRate" value="${0}" />	
		<c:set var="totalReceipt" value="${0}" />
		<div class="data-list-container width100 float-left"
			style="margin-top: 10px;">
			<div style="border: 1px solid #dddddd; height: 100%; 
				margin: 5px; padding:1px; background-color: #F5ECCE; float:left; width: 98%;">
				<div class="width100 float-left">
					<span class="side-heading">SALES DATE: ${pos.key}</span>
				</div>
				<table class="width100">
					<tr>
						<th>REFERENCE</th>
						<th>STORE</th>
						<th>PRODUCT</th>
						<th>QUANTITY</th>
						<th>UNIT RATE</th>
						<th>DISCOUNT</th>
						<th>TOTAL</th>
					</tr>
					<tbody>
						<c:forEach items="${pos.value}" var="posHead">
							<c:forEach items="${posHead.pointOfSaleDetailVOs}"
								var="posdetail">
								<tr>
									<td>${posHead.referenceNumber}</td>
									<td>${posHead.storeName}</td>
									<td>${posdetail.productName}</td>
									<td>${posdetail.quantity}</td>
									<td style="text-align: right;">${posdetail.unitRate}</td>
									<td style="text-align: right;"><c:choose>
											<c:when
												test="${posdetail.discountValue ne null && posdetail.discountValue gt 0}">
												<c:choose>
													<c:when test="${posdetail.isPercentageDiscount eq true}">
														<span class="percentage-mode">(Percentage)</span>
													</c:when>
													<c:otherwise><span class="percentage-mode">(Amount)</span></c:otherwise>
												</c:choose>
										${posdetail.discount}
									</c:when>
											<c:otherwise>-N/A-</c:otherwise>
										</c:choose>
									</td>
									<td><span style="text-align: right; float: right;">${posdetail.totalPrice}</span>
									</td>
								</tr> 
							</c:forEach>
							<c:set var="totalQuantity" value="${posHead.totalQuantity}" />
							<c:set var="totalAmount" value="${posHead.salesAmount}" />
							<c:set var="totalDiscount" value="${posHead.customerDiscount}" />
							<c:set var="totalUnitRate" value="${posHead.totalDue}" />	
						</c:forEach>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL QUANTITY:</span> ${totalQuantity}</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL AMOUNT:</span> ${totalUnitRate}</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL DISCOUNT:</span> ${totalDiscount}</td>
							<td style="text-align: right; font-weight: bold;"><span
								style="font-size: 10px;">TOTAL SALES:</span> <span style="text-align: right; font-weight: bold;">${totalAmount}</span>
							</td>
						</tr>
					</tbody>
				</table> 
			</div>
		</div>
	</c:forEach>
	<div class="clear-fix"></div>
	<div class="width30 float-left" style="margin-top: 5px;">
	<div style="font-weight: bold;">Receipt Summary</div>
	<div id="hrm" class="hastable width100"> 
		<table id="hastab" class="width100">
			<thead>
				<tr>
					<th class="width5">Receipt Type</th>
					<th class="width5">Amount</th>  
				</tr>
			</thead>
			<tbody class="tab">
				<c:forEach var="detail" items="${RECEIPT_DETAILS}"
					varStatus="status">
					<tr>
						<td>${detail.receiptTypeStr}</td>
						<td style="text-align: right;">${detail.receiptAmountStr}</td>
					</tr>
				</c:forEach>
			</tbody>		
		</table>
	</div> 
</div>  
</body>
</html>