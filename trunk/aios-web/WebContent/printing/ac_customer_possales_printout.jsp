<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Production Receive</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
	
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<div class="width98" ><span class="heading" ><span style="font-size: 25px!important;">${THIS.companyName}</span></span></div> 
	</div>
	<div class="clear-fix"></div>
		<div class="width98" ><span class="heading"><span>CUSTOMER POS REPORT</span></span></div>
	<div class="width100"><span class="side-heading" style="float:right;padding:5px;">Printed on : ${POINT_OF_SALE.date}</span></div>
	<div class="clear-fix"></div>
	<div class="width100 float-left">
			<div class="data-container width50 float-left">	
				<c:if test="${VIEW_TYPE eq 'Single Record'}">
					<div>
						<label class="caption width20 float-left" >Reference No. :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.referenceNumber}&nbsp;</label>
						<label class="caption width20 float-left" >Date :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.date}&nbsp;</label>
						<label class="caption width20 float-left" >Sale Type :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.salesType}&nbsp;</label>
						<label class="caption width20 float-left" >Store  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.storeName}&nbsp;</label>
						<label class="caption width20 float-left" >Till  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.tillNumber}&nbsp;</label>
						<label class="caption width20 float-left" >Cashier  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.posUserName}&nbsp;</label>
					</div>
				</c:if>
				<c:if test="${VIEW_TYPE eq 'Multiple Records'}">
					<label class="caption width20 float-left" >Date :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.fromDateView} upto ${POINT_OF_SALE.toDateView}</label>
					<label class="caption width20 float-left" >Store  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.storeName}&nbsp;</label>
					<label class="caption width20 float-left" >Till  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.tillNumber}&nbsp;</label>
					<label class="caption width20 float-left" >Cashier  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.posUserName}&nbsp;</label>
				</c:if>	
			</div>
			<div class="data-container width50 float-left">	
				<div>
					<label class="caption width20 float-left" >Paid By Cash  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.cashReceived}</label>
					<label class="caption width20 float-left" >Paid By Card  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.cardReceived}</label>
					<c:if test="${POINT_OF_SALE.chequeReceived ne null}">
						<label class="caption width20 float-left" >Paid By Cheque  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.chequeReceived}</label>
					</c:if>
					<c:if test="${POINT_OF_SALE.couponReceived ne null}">
						<label class="caption width20 float-left" >Paid By Coupon  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.couponReceived}</label>
					</c:if>
					<c:if test="${POINT_OF_SALE.exchangeReceived ne null}">
						<label class="caption width20 float-left" >Paid By Exchange  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.exchangeReceived}</label>
					</c:if>
				</div>
			</div>
	</div>
	<div class="clear-fix"></div>
		
		<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<div class="width100 float-left"><span class="side-heading">SALES BY CATEGORY</span></div>
			<c:set var="toalQuantity" value="${0}" />
			<c:set var="toalAmount" value="${0}" />
			<table class="width100 float_left">
				<tr>  
					<th>CATEGORY NAME</th> 
					<th>QUANTITY</th>
					<th>AMOUNT</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				<tbody>  
					<c:forEach items="${POS_CATEGORY_SALES}" var="sales">
						<tr> 
							<td>
								${sales.categoryName}
							</td>
							<td style="text-align: right;">
								${sales.productQuantity}
							</td>
							<td style="text-align: right;">
								${sales.productAmount}
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<c:set var="toalQuantity" value="${toalQuantity+sales.productQuantity}" />
						<c:set var="toalAmount" value="${toalAmount+sales.productAmount}" />
					</c:forEach>
					<tr>
						<td>&nbsp;</td>
						<td style="text-align: right;font-weight: bold;">TOTAL QUANTITY:&nbsp;&nbsp; ${toalQuantity}</td>
						<td style="text-align: right;font-weight: bold;">TOTAL AMOUNT:&nbsp;&nbsp; ${toalAmount}</td>
						<td style="text-align: right;font-weight: bold;">TOTAL DISCOUNT :&nbsp;&nbsp; ${POINT_OF_SALE.totalDiscount}</td>
						<td style="text-align: right;font-weight: bold;">TOTAL SALES :&nbsp;&nbsp; ${toalAmount - POINT_OF_SALE.totalDiscount}</td>
					</tr>
				</tbody>
			</table>
		</div>
			<div class="clear-fix"></div>
		
		<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<div class="width100 float-left"><span class="side-heading">SALES BY PRODUCT</span></div>
				<c:set var="totalQuantity" value="${0}" />
				<c:set var="totalAmount" value="${0}" />
				<table class="width100 float_left">
					<tr>  
						<th>PRODUCT</th>
						<th>QUANTITY</th> 
						<th>AMOUNT</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
					<tbody>  
						<c:forEach items="${POS_PRODUCT_SALES}" var="details">
							<tr> 
								<td>
									${details.productName}
								</td>
								<td style="text-align: right;">
									${details.quantity}
								</td>
								<td style="text-align: right;">
									${details.totalPrice}
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<c:set var="totalQuantity" value="${totalQuantity+details.quantity}" />
							<c:set var="totalAmount" value="${totalAmount+details.totalPrice}" />
						</c:forEach>
						<tr>
							<td>&nbsp;</td>
							<td style="text-align: right;font-weight: bold;">TOTAL QUANTITY:&nbsp;&nbsp; ${totalQuantity}</td>
							<td style="text-align: right;font-weight: bold;">TOTAL AMOUNT:&nbsp;&nbsp; ${totalAmount}</td>
							<td style="text-align: right;font-weight: bold;">TOTAL DISCOUNT :&nbsp;&nbsp; ${POINT_OF_SALE.totalDiscount}</td>
							<td style="text-align: right;font-weight: bold;">TOTAL SALES :&nbsp;&nbsp; ${toalAmount - POINT_OF_SALE.totalDiscount}</td>
						</tr>
					</tbody>
				</table>
			</div>
				<div class="clear-fix"></div>
			
			<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<div class="width100 float-left"><span class="side-heading">ADDITIONAL CHARGES</span></div>
			<table class="width60">
				<tr>  
					<th>CHARGES TYPE</th> 
					<th>AMOUNT</th>
				</tr>
				<tbody>  
					<c:set var="totalAmount" value="${0}" />
					<c:forEach items="${POS}" var="pos">
						<c:forEach items="${pos.pointOfSaleChargeVOs}" var="charge">
							<tr> 
								<td>
									${charge.chargeType}
								</td>
								<td style="text-align: right;">
									${charge.charges}
								</td>
							</tr>
							<c:set var="totalAmount" value="${totalAmount+charge.charges}" />
						</c:forEach>
					</c:forEach>
					<tr>
						<td>&nbsp;</td>
						<td style="text-align: right;font-weight: bold;">TOTAL AMOUNT:&nbsp;&nbsp; ${totalAmount}</td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<c:if test="${VIEW_TYPE eq 'Single Record'}">
			<div class="width100 float-left"><span class="side-heading">CUSTOMER DETAILS</span></div>
			<table class="width60">
				<tr>  
					<th>CUSTOMER NAME</th> 
					<th>CUSTOMER NUMBER</th>
				</tr>
				<tbody>  
					<c:forEach items="${POS}" var="pos">
							<tr> 
								<td>
									${pos.customerName}
								</td>
								<td>
									${pos.customerNumber}
								</td>
							</tr>
					</c:forEach>
				</tbody>
			</table>
			</c:if>
		</div>
	<div class="clear-fix"></div>
	</body>
</html>