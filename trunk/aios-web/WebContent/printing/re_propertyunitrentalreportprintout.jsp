<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><fmt:message key="re.property.propertyunitrentallistreport"/></title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading"><fmt:message key="re.property.propertyunitrentallistreport"/></span></div>
	<div class="width100 float-left"><span class="side-heading"><fmt:message key="re.property.filtercriteria"/></span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" ><fmt:message key="re.property.property"/> :</label>
					<label class="data width50">${PROPERTY_MASTER.buildingName}</label>
				</div>
		</div>
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" ><fmt:message key="re.property.period"/> :</label>
					<label class="data width50">${PROPERTY_MASTER.contractPeriod}</label>
				</div>
		</div>
		</div>
	<div class="width100 float-left"><span class="side-heading"><fmt:message key="re.property.rentalinformation"/></span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(PROPERTY_LIST)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th><fmt:message key="re.property.unit"/></th>
						<th><fmt:message key="re.property.tenant"/></th>
						<th><fmt:message key="re.property.period"/></th>
						<th><fmt:message key="re.property.amount"/></th> 
						
					</tr> 
				</thead> 
				<tbody >	
				
					<c:forEach items="${PROPERTY_LIST}" var="result3" varStatus="status1" >
					 	<tr> 
							<td>${result3.buildingName}</td>
							<td>${result3.tenantName }</td>
							<td>${result3.contractPeriod }</td>
							<td>${result3.contractAmount }</td>
						</tr>
					</c:forEach>
				 
				</tbody>
			</table>
			<div class="width20 float-right" style="padding:5px;border-bottom: 1px solid #755800;">
				<label class="caption width15 float-left" ><fmt:message key="re.property.total"/>:</label>
				<label class="data width60 float-left">${TOTAL}</label>
			</div>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid"><fmt:message key="re.property.noproperty"/></tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>