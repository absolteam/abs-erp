<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Contract List</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Contract Report</span></div>
	<div class="width100 float-left"><span class="side-heading">Filter Condition</span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Amount Greater Then :</label>
					<label class="data width50">${CONTRACT_INFO.fromDate}</label>
				</div>
				
			</div>
			<div class="data-container width50 float-left">
				<div>
					<label class="caption width30">Amount Less Then :</label>
					<label class="data width50">${CONTRACT_INFO.toDate}</label>
				</div>	
					
			</div>
		</div>
		
	<div class="width100 float-left"><span class="side-heading">Contract Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
		<c:choose>
			<c:when test="${fn:length(requestScope.TENANT_CONTRACT_LIST)gt 0}">
				<table class="width100">	
					<thead>									
						<tr>
							<th>Contract Number</th> 
							<th>Contract Date</th>
							<th>Deposit Amount</th>
							<th>Contract Fee</th>
							<th>Tenant Name</th>
							<th>Tenant Information</th>
							<th>Contract Period</th>
							<th>Property Name</th>
							<th>Property Details</th>
							<th>Rent Amount</th>
						</tr> 
					</thead> 
					<tbody >	
				
						<c:forEach items="${requestScope.TENANT_CONTRACT_LIST}" var="result3" varStatus="status1" >
						 	<tr> 
								<td>${result3.contractNumber }</td>
								<td>${result3.contractDate }</td>
								<td>${result3.depositAmount }</td>
								<td>${result3.offerAmount }</td>
								<td>${result3.tenantName }</td>
								<td>
									<span class="inside-heding">Identity :</span> ${result3.tenantIdentity} ,
									<span class="inside-heding">Nationality :</span> ${result3.tenantNationality},
									<span class="inside-heding">Mobile :</span> ${result3.tenantMobile},
									<span class="inside-heding">Fax : </span>${result3.tenantFax},
								</td>
								<td>From (${result3.fromDate}) UpTo (${result3.toDate})</td>
								<td>${result3.buildingName }</td>
								<td>
									<span class="inside-heding">Flat Number :</span> ${result3.flatNumber} ,
									<span class="inside-heding">Area :</span> ${result3.buildingArea},
									<span class="inside-heding">Plot :</span> ${result3.buildingPlot},
									<span class="inside-heding">Street :</span> ${result3.buildingStreet},
									<span class="inside-heding">City : </span>${result3.buildingCity}
								</td>
								<td>${result3.offerAmount}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No Contract's</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>