<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Production Requisition</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<span class="title-heading"><span>${THIS.companyName}</span> </span> 
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>PRODUCTION REQUISITION</span> </span>
	</div>
	<div style="clear: both;"></div>
	<div
		style="height: 100px; border-style: solid; border-width: medium; 
		-moz-border-radius: 6px; -webkit-border-radius: 6px; border-radius: 6px;"
		class="width98 float-left"> 
		 
		<div class="width40 float_right"> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width40 text-bold">DESCRIPTION<span
					class="float_right">:</span> </span> <span
					class="span_border left_align width50 text_info"
					style="display: -moz-inline-stack;"> 
					<c:choose>
						<c:when test="${PRODUCTION_REQUISITION.description ne null && PRODUCTION_REQUISITION.description ne ''}">
							${PRODUCTION_REQUISITION.description}
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose>
					
				</span>
			</div>  
		</div> 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">REFERENCE <span
					class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> 
					${PRODUCTION_REQUISITION.referenceNumber}
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"> DATE <span
					class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> 
					${PRODUCTION_REQUISITION.requisitionDateStr}
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"> PERSON <span
					class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> 
					${PRODUCTION_REQUISITION.person.firstName} ${PRODUCTION_REQUISITION.person.lastName}
				</span>
			</div> 
	</div>
	</div>
	<div
		style="margin-top: 10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; -webkit-border-radius: 6px; border-radius: 6px;"
		class="width98 float-left">
		<table class="width100">
			<tr>
				<th style="width: 2%;">S.NO:</th>
				<th class="width10">PRODUCT</th>
				<th class="width10">STORE</th>
				<th style="width: 2%;">QUANTITY</th>
				<th class="width10">DESCRIPTION</th> 
			</tr>
			<tbody>
				<c:choose>
					<c:when
						test="${PRODUCTION_REQUISITION.productionRequisitionDetailVOs ne null 
							&& PRODUCTION_REQUISITION.productionRequisitionDetailVOs ne ''}">
						<c:set var="totalAmount" value="0" />
						<tr style="height: 25px;">
							<c:forEach begin="0" end="4" step="1">
								<td />
							</c:forEach>
						</tr>
						<c:forEach items="${PRODUCTION_REQUISITION.productionRequisitionDetailVOs}"
							var="REQUISITION_DETAIL" varStatus="status">
							<tr>
								<td>${status.index+1}</td>
								<td>${REQUISITION_DETAIL.product.productName}[${REQUISITION_DETAIL.product.code}]</td> 
								<td>${REQUISITION_DETAIL.storeName}</td>
								<td>${REQUISITION_DETAIL.quantity}</td> 
								<td>${REQUISITION_DETAIL.description}</td> 
							</tr>
						</c:forEach>
						<c:if test="${fn:length(PRODUCTION_REQUISITION.productionRequisitionDetailVOs)<5}">
							<c:set var="emptyLoop"
								value="${3 - fn:length(PRODUCTION_REQUISITION.productionRequisitionDetailVOs)}" />
							<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
								<tr style="height: 25px;">
									<c:forEach begin="0" end="4" step="1">
										<td />
									</c:forEach>
								</tr>
							</c:forEach>
						</c:if> 
					</c:when>
				</c:choose>
			</tbody>
		</table>
	</div> 
	
	<div class="width100 float_left div_text_info"
		style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Prepared by:</span>
			</div>
			<div class="width40 float_left">
				<span>Recommended by:</span>
			</div>
			<div class="width30 float_left">
				<span>Approved by:</span>
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70"
					style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width40 float_left">
				<span class="span_border width70"
					style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width30 float_left">
				<span class="span_border width70"
					style="display: -moz-inline-stack;"></span>
			</div>
		</div>
	</div>
</body>
</html>