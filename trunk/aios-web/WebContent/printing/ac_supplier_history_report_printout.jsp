<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Supplier History Report</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>SUPPLIER HISTORY REPORT</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>


	<c:forEach items="${requestScope.SUPPLIERS}" var="supplier">

		<div style="position: relative; top: 10px; height: 100%;"
			class="width98 float_left">
			<table class="width100">
				<tr>

					<td colspan="2" style="border: none; text-align: left;">Supplier
						#: ${supplier.supplierNumber }</td>
				</tr>
				<tr>

					<td colspan="2" style="border: none; text-align: left;">Supplier
						Name: ${supplier.supplierName }</td>
				</tr>

				<tr>
					<td colspan="2">Receives:</td>
				</tr>

				<tr>
					<td colspan="2">
						<table>

							<tr>
								<td>Receive Number</td>
								<td>Receive Date</td>
								<td>Description</td>
								<td>Product</td>
								<td>Quantity</td>
								<td>Unit Rate</td>

							</tr>
							<c:forEach items="${supplier.receivesList}" var="receive">

								<c:forEach items="${receive.receiveDetails}" var="rd">


									<tr>
										<td>${receive.receiveNumber }</td>
										<td>${receive.receiveDate }</td>
										<td>${receive.description }</td>
										<td>${rd.product.productName }</td>
										<td>${rd.receiveQty }</td>
										<td>${rd.unitRate }</td>

									</tr>


								</c:forEach>
							</c:forEach>
						</table>
					</td>
				</tr>
				<tr>

					<td colspan="2">&nbsp;</td>
				</tr>


				<tr>
					<td colspan="2">Returns:</td>
				</tr>

				<tr>
					<td colspan="2">
						<table>

							<tr>
								<td>Return Number</td>
								<td>Return Date</td>

								<td>Product</td>
								<td>Quantity</td>
								<td>Unit Rate</td>

							</tr>
							<c:forEach items="${supplier.returnsList}" var="return">

								<c:forEach items="${return.goodsDetails}" var="gd">


									<tr>
										<td>${return.returnNumber }</td>
										<td>${return.date }</td>
										<td>${gd.product.productName }</td>
										<td>${gd.receiveQty }</td>
										<td>${gd.unitRate }</td>

									</tr>
								</c:forEach>
							</c:forEach>
						</table>
					</td>
				</tr>
				<tr>

					<td colspan="2">&nbsp;</td>
				</tr>



			</table>
		</div>
	</c:forEach>
	<div class="clear-fix"></div>
</body>
</html>




