<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
.caption {
	padding: 2px;
	padding-top: 5px !important;
}

.data {
	padding: 2px;
	padding-top: 5px !important;
}

.data-container {
	padding: 5px;
}

td {
	padding-top: 8px !important;
}

th {
	font-weight: bold;
	padding-top: 8px !important;
}
@media print{
	.hideWhilePrint {display: none;}
}
</style>
<script type="text/javascript"> 
 
 </script>
<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	
	<div class="width100 float-left">
		<span class="heading">Supplier History</span>
	</div>
 	<div id="main-content" class="hr_main_content">
			<div class="width100 float-left">
						<c:choose>
							<c:when
								test="${SUPPLIER_HISTORY ne null && SUPPLIER_HISTORY ne '' && fn:length(SUPPLIER_HISTORY)>0}">
								
								<c:forEach var="supplier" items="${SUPPLIER_HISTORY}"
									varStatus="status1">
										<div id="hrm" class="width100 mainhead">
											${supplier.supplierName} - ${supplier.supplierNumber}
										</div>
										
										<c:forEach var="invoices" items="${supplier.invoiceVOs}"
														varStatus="status2">
												<div id="hrm" class=" width100" style="padding-top:10px;">
													
													<table class="width100" bgcolor="fffade">
														<tr>
															<td colspan="2" bgcolor="fffade"><span class="side-heading">Purchase ${status2.index+1} : ${invoices.receiveNumber}</span></td>
														
															<td colspan="1" style="font-weight: bold;font-size: 12px;" bgcolor="fffade">Date : ${invoices.receiveDate}</td>
														</tr>
													</table>
												</div>
												<div id="hrm" class=" hastable width100">
															<table class="width100">
																<thead>
																	<tr>
																		<th style="width:20%;">Invoice Number</th>
																		<th style="width:20%;">Date</th>
																		<th style="width:20%;">Total Amount</th>
																		<th style="width:20%;">Invoiced Amount</th>
																		<th style="width:20%;">Balance</th>
																	</tr>
																</thead>
																<tbody class="tab">
														<c:forEach var="invoiceDetail" items="${invoices.invoiceVOs}"
																varStatus="status3">
															<tr class="rowid">
																<td>${invoiceDetail.invoiceNumber}</td>
																<td>${invoiceDetail.invoiceDate}</td>
																<td>${invoiceDetail.totalInvoice}</td>
																<td>${invoiceDetail.invoiceAmount}</td>
																<td>${invoiceDetail.pendingInvoice}</td>
															</tr>
														</c:forEach>
															<tr>
																<td colspan="4" style="font-weight: bold;font-size: 15px;" align="right">Balance</td>
																<td style="font-weight: bold;font-size: 15px;">${invoices.pendingInvoice}</td>
															</tr>
														</tbody>
												</table>
											</div>
									</c:forEach>
									<table class="width100">
										<tbody class="tab">
											<tr>
												<td colspan="4" style="font-weight: bold;font-size: 15px;" align="right">
													Supplier(${supplier.supplierName}) Balance :
												</td>
												<td colspan="1" style="font-weight: bold;font-size: 15px;">
													${supplier.pendingInvoice}
												</td>
											<tr>
										</tbody>
									</table>
								</c:forEach>
								
							</c:when>
						</c:choose>

					</div>
			</div>
			

</body>