<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Inventory Statement Report</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>INVENTORY STATEMENT REPORT</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 10px; height: 100%;"
		class="width98 float_left">
		<table class="width100">
			<tr>

				<th style="width: 70%; border: none;">Account #</th>
				<th style="width: 10%; border: none;">Credit</th>
				<th style="width: 10%; border: none;">Debit</th>
			</tr>
			<tbody>
				<c:set var="totalCredit" value="0" scope="page" />
				<c:set var="totalDebit" value="0" scope="page" />

				<c:forEach items="${requestScope.PRODUCTS}" var="result"
					varStatus="status1">
					
					<c:forEach items="${result.combinationByProductCombinationId.transactionDetails}" var="result2"
					varStatus="statuss">
						<tr>
							<c:choose>
								<c:when test="${statuss.index == 0}">
									<td style="border: none; text-align: left;"> ${result.combinationByProductCombinationId.accountByCompanyAccountId.account}
										<c:if test="${result.combinationByProductCombinationId.accountByCostcenterAccountId != null}">
											.${result.combinationByProductCombinationId.accountByCostcenterAccountId.account}
										</c:if>
										<c:if test="${result.combinationByProductCombinationId.accountByNaturalAccountId != null}">
											.${result.combinationByProductCombinationId.accountByNaturalAccountId.account}
										</c:if>
										<c:if test="${result.combinationByProductCombinationId.accountByAnalysisAccountId != null}">
											.${result.combinationByProductCombinationId.accountByAnalysisAccountId.account}
										</c:if>
									</td>
								</c:when>
								<c:otherwise>
									<td style="border: none;">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							<td style="border: none;">
								<c:choose>
								<c:when test="${!result2.isDebit}">
									${result2.amount}
									<c:set var="totalCredit" value="${totalCredit + result2.amount}" scope="page"/>
								</c:when>
								<c:otherwise>&nbsp;</c:otherwise>
								</c:choose>
							</td>
							<td style="border: none;">
								<c:choose>
								<c:when test="${result2.isDebit}">
									${result2.amount}
									<c:set var="totalDebit" value="${totalDebit + result2.amount}" scope="page"/>
								</c:when>
								<c:otherwise>&nbsp;</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</c:forEach>
				
			</tbody>
		</table>
		<table style="border-collapse: separate;" cellspacing="10px">
			<tr>
				<td style="width: 70%; border: none;">&nbsp;</td>
				<td style="width: 10%; border: none; border-top: solid 1px;"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalCredit}" /></td>
				<td style="width: 10%; border: none; border-top: solid 1px;"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDebit}" /></td>
			</tr>
		</table>
	</div>
	<div class="clear-fix"></div>
</body>
</html>




