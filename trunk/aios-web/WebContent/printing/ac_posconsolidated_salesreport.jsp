<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 55%!important;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">

var selectedStoreId = 0;

$(function(){ 
	
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	}
	$('#fromDate,#toDate').datepick({
	 	onSelect: customRange, showTrigger: '#calImg'}); 
	
	$(".xls-download-call").click(function(){ 
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();  
		window.open("<%=request.getContextPath()%>/get_pos_salesconsolidated_reportXLS.action?store="+selectedStoreId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px'); 
		return false; 
	}); 

	$('#stores').combobox({
		selected : function(event, ui) {
			selectedStoreId = $('#stores :selected').val(); 
		}
	}); 
}); 
function checkLinkedDays() {
	var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
			'#selectedMonth').val());
	$('#selectedDay option:gt(27)').attr('disabled', false);
	$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
			true);
	if ($('#selectedDay').val() > daysInMonth) {
		$('#selectedDay').val(daysInMonth);
	}
}
function customRange(dates) {
	if (this.id == 'fromDate') {
		$('#toDate').datepick('option', 'minDate', dates[0] || null); 
	} else {
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Point of Sales Consolidated Report
		</div>
		<div class="portlet-content" id="hrm">
			<table class="width50">
				<tr>
					<td class="width20">Store </td>
					<td>
						<select id="stores" class="width50">
						<option value="-1">--All--</option>
						<c:forEach var="store" items="${STORE_LIST}">
							<option value="${store.storeId}">${store.storeName}</option>						
						</c:forEach>
					</select>
					</td>
				</tr>
				<tr>
					<td class="width20">From Date</td>
					<td>
						<input type="text" id="fromDate" class="width65" readonly="readonly"/>
					</td>
				</tr>
				<tr>
					<td class="width20">To Date </td>
					<td>
						<input type="text" id="toDate" class="width65" readonly="readonly"/>
					</td>
				</tr> 
			</table>
		</div>
	</div> 
</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div> 
	</div>
</div> 