<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Material Requisition</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}

.width15 {
	width: 15%;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"><span>${THIS.companyName}</span> </span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>Bank Reconciliation Entries</span> </span>
	</div>
	<div style="clear: both;"></div>
	<div style="height: 120px; border: 1px solid;"
		class="width98 float-left">
		 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">
					STATEMENT REF <span class="float_right">:</span> </span> <span
					class="span_border left_align width60 text_info"
					style="display: -moz-inline-stack;"> <c:choose>
						<c:when
							test="${BANK_RECONCILIATION.statementReference ne null && BANK_RECONCILIATION.statementReference ne ''}">
							${BANK_RECONCILIATION.statementReference}
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose> </span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"> START
					DATE <span class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${BANK_RECONCILIATION.startDate} </span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"> END
					DATE <span class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${BANK_RECONCILIATION.endDate} </span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">
					STATEMENT BALANCE <span class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${BANK_RECONCILIATION.statementBalanceStr} </span>
			</div>
		</div>
	</div>
	<div style="margin-top: 10px;" class="width98 float-left">
		<c:choose>
			<c:when
				test="${BANK_RECONCILIATION.bankReconciliationDetailVOs ne null 
						&& fn:length(BANK_RECONCILIATION.bankReconciliationDetailVOs)>0}">
				<input type="hidden" id="processFlag" value="1" />
				<div id="hrm" class="hastable width100">
					<table id="hastab" class="width100">
						<thead>
							<tr>
								<th style="width: 3%">Date</th>
								<th style="width: 3%">Transaction Type</th>
								<th style="width: 3%">Reference</th>
								<th style="width: 5%">Description</th>
								<th style="width: 3%;">Payment</th>
								<th style="width: 3%;">Receipt</th>
							</tr>
						</thead>
						<tbody class="tab">
							<c:forEach var="RECONCILIATION_DETAIL"
								items="${BANK_RECONCILIATION.bankReconciliationDetailVOs}"
								varStatus="status">
								<tr class="rowid" id="fieldrow_${status.index+1}">
									<td>${RECONCILIATION_DETAIL.transactionDateStr}</td>
									<td>${RECONCILIATION_DETAIL.useCase}</td>
									<td>${RECONCILIATION_DETAIL.referenceNumber}</td>
									<td>${RECONCILIATION_DETAIL.description}</td>
									<td style="text-align: right;">
										${RECONCILIATION_DETAIL.paymentAmount}</td>
									<td style="text-align: right;">
										${RECONCILIATION_DETAIL.receiptAmount}</td>
								</tr>
							</c:forEach>
							<tr>
								<td style="text-align: right; font-weight: bold;" colspan=4>Total</td>
								<td style="text-align: right; font-weight: bold;"
									id="paymentTotal">${BANK_RECONCILIATION.paymentTotal}</td>
								<td style="text-align: right; font-weight: bold;"
									id="receiptTotal">${BANK_RECONCILIATION.receiptTotal}</td>
							</tr>
						</tbody>
					</table> 
				</div>
			</c:when>
			<c:otherwise>
				<div id="hrm" class="hastable width100">
					<span style="font-weight: bold; color: #ff255c;">Result not
						found.</span>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>