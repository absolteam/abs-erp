<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Contract List</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left"><span class="heading">Contract Report</span></div>
	<div class="width100 float-left"><span class="side-heading">Tenant Information</span></div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">	
				<div>
					<label class="caption width30" >Tenant Name</label>
					<label class="data width50">${TENANT_INFO.tenantName}</label>
				</div>
				<div>
					<label class="caption width30">Phone No.</label>
					<label class="data width50">${TENANT_INFO.tenantMobile}</label>
				</div>
				<div>
					<label class="caption width30">Fax No.</label>
					<label class="data width50">${TENANT_INFO.tenantFax}</label>
				</div>
				<div>
					<label class="caption width30">Post Box No.</label>
					<label class="data width50">${TENANT_INFO.tenantPOBox}</label>
				</div>
			</div>
			<div class="data-container width50 float-left">
				<div>
					<label class="caption width30">Tenant Identity</label>
					<label class="data width50">${TENANT_INFO.tenantIdentity}</label>
				</div>	
				<div>
					<label class="caption width30">Other Info</label>
					<label class="data width50">${TENANT_INFO.presentAddress}</label>
				</div>	
			</div>
		</div>
	<div class="width100 float-left"><span class="side-heading">Contract Information</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Contract Number</th> 
						<th>Contract Date</th>
						<th>Deposit Amount</th>
						<th>Contract Fee</th>
						<th>Property Name</th>
						<th>Contract Period</th>
						<th>Rent Amount</th>
						<th>Property Details</th>
						
					</tr> 
				</thead> 
				<tbody >	
				<c:choose>
				<c:when test="${fn:length(requestScope.TENANT_CONTRACT_LIST)gt 0}">
					<c:forEach items="${requestScope.TENANT_CONTRACT_LIST}" var="result3" varStatus="status1" >
					 	<tr> 
							<td>${result3.contractNumber }</td>
							<td>${result3.contractDate }</td>
							<td>${result3.depositAmount }</td>
							<td>${result3.contractAmount }</td>
							
							<td>${result3.buildingName }</td>
							<td>From (${result3.fromDate}) UpTo (${result3.toDate})</td>
							<td>${result3.offerAmount}</td>
							<td>
							<span class="inside-heding">Flat Number :</span> ${result3.flatNumber} ,
							<span class="inside-heding">Area :</span> ${result3.buildingArea},
							<span class="inside-heding">Plot :</span> ${result3.buildingPlot},
							<span class="inside-heding">Street :</span> ${result3.buildingStreet},
							<span class="inside-heding">City : </span>${result3.buildingCity}
							</td>
						</tr>
					</c:forEach>
				  </c:when>
				  <c:otherwise>
				  		<tr class="even rowid">No Contracts</tr>
				  </c:otherwise>
				  </c:choose>	
				</tbody>
			</table>
		
		</div>	
		
	</body>
</html>