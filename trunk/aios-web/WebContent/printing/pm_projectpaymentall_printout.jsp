<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><fmt:message key="hr.report.attendancepunch"/></title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">

	
	<div class="width100 float-left"><span class="side-heading">Project Payment</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(PROJECT_PAYMENTS)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Payment Title</th>
						<th>Project</th>
						<th>MileStone</th>
						<th>Payment Date</th>
						<th>Amount</th>
						<th>Description</th>
 					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${PROJECT_PAYMENTS}" var="result" varStatus="status1" >
					 	<tr> 
					 		<td>${result.paymentTitle}</td>
							<td>${result.projectTitle}</td>
							<td>${result.milestoneTitle}</td>
							<td>${result.receiptDate}</td>
							<td>${result.receiptAmount}</td>
							<td>${result.description}</td>
 						</tr>
					</c:forEach>
				</tbody>
			</table>
		 </c:when>
		  <c:otherwise>
		  		<tr class="even rowid">No record found.</tr>
		  </c:otherwise>
		  </c:choose>	
		</div>	
		
	</body>
</html>