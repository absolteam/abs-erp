<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Workflow Users Report</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}

.borderlessTable {
	border: none;
}

.borderlessTable tr {
	border: none;
}

.borderlessTable td {
	border: none;
}

.borderlessTable th {
	border: none;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>WORKFLOW USERS REPORT</span></span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div>

	<c:forEach items="${requestScope.WORKFLOWS}" var="wf"
		varStatus="status">

		<div>
			<span style="font-size: 18px;">Workflow ${status.index+1}</span>
		</div>




		<div
			style="position: relative; top: 0px; height: 100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; -webkit-border-radius: 6px; border-radius: 6px; margin-bottom: 20px;"
			class="width98 float_left">
			<table class="width100 display">


				<tr>

					<th style="width: 25%">Role</th>
					<th style="width: 20%">Username</th>
					<th style="width: 20%">Person Name</th>
					<th style="width: 15%">Designation</th>
					<th style="width: 10%">Operation</th>

				</tr>


				<c:forEach items="${wf.detailVOs}" var="detail" varStatus="status1">

					<tr>

						<td>${detail.roleName}</td>
						<td>${detail.displayUsername}</td>
						<td>${detail.personName}</td>
						<td>${detail.designation}</td>
						<td>${detail.operationName}</td>

					</tr>


				</c:forEach>


			</table>

		</div>

	</c:forEach>
	<div class="clear-fix"></div>


</body>
</html>




