<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Production Receive</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
	
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}

</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<div class="width98" ><span class="heading" ><span style="font-size: 25px!important;">${THIS.companyName}</span></span></div> 
	</div>
	<div class="clear-fix"></div>
		<div class="width98" ><span class="heading"><span>POS SALES REPORT</span></span></div>
	<div class="width100"><span class="side-heading" style="float:right;padding:5px;">Printed on : ${POINT_OF_SALE.date}</span></div>
	<div class="clear-fix"></div>
	<div class="width100 float-left">
			<div class="data-container width50 float-left">	
				<c:if test="${VIEW_TYPE eq 'Single Record'}">
					<div>
						<label class="caption width20 float-left" >Reference No. :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.referenceNumber}&nbsp;</label>
						<label class="caption width20 float-left" >Date :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.date}&nbsp;</label>
						<label class="caption width20 float-left" >Sale Type :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.salesType}&nbsp;</label>
						<label class="caption width20 float-left" >Store  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.storeName}&nbsp;</label>
						<label class="caption width20 float-left" >Till  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.tillNumber}&nbsp;</label>
						<label class="caption width20 float-left" >Cashier  :</label>
						<label class="data width70 float-left">${POINT_OF_SALE.posUserName}&nbsp;</label>
					</div>
				</c:if>
				<c:if test="${VIEW_TYPE eq 'Multiple Records'}">
					<label class="caption width20 float-left" >Date :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.fromDateView} upto ${POINT_OF_SALE.toDateView}</label>
					<label class="caption width20 float-left" >Store  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.storeName}&nbsp;</label>
					<label class="caption width20 float-left" >Till  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.tillNumber}&nbsp;</label>
					<label class="caption width20 float-left" >Cashier  :</label>
					<label class="data width70 float-left">${POINT_OF_SALE.posUserName}&nbsp;</label>
				</c:if>	
			</div>
			<c:if test="${VIEW_TYPE eq 'Single Record'}">
			<div class="data-container width50 float-left">	
				<div>
					<c:forEach items="${POS}" var="pos">
						<label class="caption width30 float-left" >Employee Name :</label>
						<label class="data width60 float-left">${pos.customerName}</label>
						<label class="caption width30 float-left" >Employee Number :</label>
						<label class="data width60 float-left">${pos.customerNumber}</label>
					</c:forEach>
				</div>
			</div>
			</c:if>
	</div>
	<div class="clear-fix"></div>
		
		<div class="data-list-container width70 float-left" style="margin-top:10px;">
				<c:set var="totalQuantity" value="${0}" />
				<c:set var="totalAmount" value="${0}" />
				<table class="width100 float_left">
					<tr>  
						<th>PRODUCT</th>
						<th>QUANTITY</th> 
					</tr>
					<tbody>  
						<c:forEach items="${POS_PRODUCT_SALES}" var="details">
							<tr> 
								<td>
									${details.productName}
								</td>
								<td style="text-align: center;">
									${details.quantity}
								</td>
							</tr>
							<c:set var="totalQuantity" value="${totalQuantity+details.quantity}" />
							<c:set var="totalAmount" value="${totalAmount+details.totalPrice}" />
						</c:forEach>
						<tr>
							<td>&nbsp;</td>
							<td style="font-weight: bold;">TOTAL QUANTITY:&nbsp;&nbsp; ${totalQuantity}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="clear-fix"></div>
	</body>
</html>