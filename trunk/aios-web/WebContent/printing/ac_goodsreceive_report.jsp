<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Goods Receive Note</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
	height: 28px;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"
			style="font-family: book; font-style: italic; text-transform: inherit !important; font-size: 28px !important; color: black;"><strong>${THIS.companyName}</strong>
		</span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading" style="font-family: book;"><span><u>GOODS RECEIVE NOTES</u>
		</span>
		</span>
	</div>
	<div style="clear: both;"></div>
	<div class="clear-fix"></div> 
	<div
		style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
		class="width100 float_left">
		<c:if test="${requestScope.datefrom ne null && requestScope.datefrom ne ''}">
			<div style="padding: 5px; font-weight: bold;">
				Date from ${datefrom} to 
				<c:choose>
					<c:when test="${requestScope.dateto ne null && requestScope.dateto ne ''}">
						${dateto}
					</c:when>
					<c:otherwise>
						-N/-A
					</c:otherwise>
				</c:choose> 
			</div>
		</c:if> 
		<table style="width: 99%; margin: 0 auto;">
			<tr>
				<th>REFERENCE</th>
				<th>DATE</th>
				<th>SUPPLIER</th>
				<th>DELIVERY NO.</th>
				<th>INVOICE NO.</th>
				<th>PRODUCT</th>
				<th>QUANTITY</th>
				<th>UNIT RATE</th>
				<th>TOTAL AMOUNT</th> 
				<th style="display: none;">DESCRIPTION</th> 
				<th>SECTION</th>
			</tr>
 			<c:set var="grnNumber" value=""/>
			<c:forEach items="${RECEIVE_DETAIL}" var="detail">
				<tr> 
					<td>
						<c:choose>
							<c:when test="${grnNumber ne null && grnNumber ne ''}">
								<c:choose>
									<c:when test="${grnNumber ne detail.receiveVO.receiveNumber}">
										${detail.receiveVO.receiveNumber}
										<c:set var="grnNumber" value="${detail.receiveVO.receiveNumber}"/>
									</c:when>
									<c:otherwise>
										&quot;
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<c:set var="grnNumber" value="${detail.receiveVO.receiveNumber}"/>
								${detail.receiveVO.receiveNumber}
							</c:otherwise>
					</c:choose> 
					</td> 
					<td>${detail.receiveVO.strReceiveDate}</td>
					<td>${detail.receiveVO.supplierName}</td>
					<td>${detail.receiveVO.deliveryNo}</td>
					<td>${detail.receiveVO.referenceNo}</td>
					<td>${detail.productName} -- ${detail.productCode}</td>
					<td>${detail.displayQuantity}</td>
 					<td>${detail.displayUnitRate}</td> 
					<td>${detail.displayTotalAmount}</td>  
					<td style="display: none;">${detail.description}</td>
					<td>${detail.section}</td>
				</tr>
			</c:forEach>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>TOTAL QUANTITY</th>
				<th>TOTAL UNIT</th>
				<th>TOTAL AMOUNT</th> 
				<th>&nbsp;</th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="text-align: right; font-weight: bold;">
					${RECEIVE_INFO.displayQuantity}</td>
				<td style="text-align: right; font-weight: bold;">
					${RECEIVE_INFO.displayUnitRate}</td>
 				<td style="text-align: right; font-weight: bold;">
 						${RECEIVE_INFO.displayTotalAmount}
				</td>
				<td>&nbsp;</td>
			</tr>
		</table> 
	</div>
	<div class="clear-fix"></div>
</body>
</html>