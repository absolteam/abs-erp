<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Leave History</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
</style>
<body style="margin-top: 15px; font-size: 12px;">


		<div class="width100 float-left">
			<span class="heading">Bank Reconciliation</span>
		</div>
		<!-- <div class="width70 float-left">
			<span class="side-heading">Filter Condition</span>
		</div> -->
		<div class="width30 float-right">
			<span class="side-heading">Printed on :</span>
		</div>


		<c:forEach var="jobAssignment" items="${JOB_ASSIGNMENTS}">
			
			<div class="width100 float-left" style="margin-top: 20px;">
				<div class="data-container width40 float-left">

					<div>
						<label class="caption width30">Employee : </label> <label
							class="data width50">${jobAssignment.personName}</label>
					</div>
					<div>
						<label class="caption width30">Designation : </label> <label
							class="data width50">${jobAssignment.designationName}</label>
					</div>
					<div>
						<label class="caption width30">Company : </label> <label
							class="data width50">${jobAssignment.companyName}</label>
					</div>
					<div>
						<label class="caption width30">Department : </label> <label
							class="data width50">${jobAssignment.departmentName}</label>
					</div>
					<div>
						<label class="caption width30">Location : </label> <label
							class="data width50">${jobAssignment.locationName}</label>
					</div>
				</div>
			</div>
			<div class="width100 float-left" style="margin-top:5px;">
				<span class="side-heading">Leave Reconciliation</span>
			</div>
			<div class="data-list-container width100 float-left"
				style="margin-top: 10px;">
				<c:choose>
					<c:when test="${fn:length(jobAssignment.leaveReconciliations) gt 0}">
						<table class="width100">
							<thead>
								<tr>
									<th>Leave Type</th>
									<th>Provided Days</th>
									<th>Eligible Days</th>
									<th>Taken Days</th>
									<th>Available Days</th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${jobAssignment.leaveReconciliations}"
									var="result3" varStatus="status1">
									<tr>
										<td>${result3.leave.lookupDetail.displayName }</td>
										<td>${result3.providedDays}</td>
										<td>${result3.eligibleDays}</td>
										<td>${result3.takendDays}</td>
										<td>${result3.eligibleDays - result3.takendDays}</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<tr class="even rowid"><td>No Leaves</td>
						</tr>
					</c:otherwise>
					
				</c:choose>
				&nbsp;
			&nbsp;&nbsp;&nbsp;
			</div>
		</c:forEach>

</body>
</html>