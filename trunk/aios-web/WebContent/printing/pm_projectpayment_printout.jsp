<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project MileStone</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<span class="title-heading"><span>${THIS.companyName}</span>
		</span> 
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>PROJECT PAYMENT REPORT</span>
		</span>
	</div>
	<div style="clear: both;"></div>
	<div
		style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; -webkit-border-radius: 6px; border-radius: 6px;"
		class="width98 float_left">
		<div class="width50 float_right">
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">PROJECT
					<span class="float_right">:</span>
				</span> <span class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${PROJECT_PAYMENTS.projectTitle}</span>
			</div>
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">CLIENT
					<span class="float_right">:</span>
				</span> <span class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">${PROJECT_PAYMENTS.clientName}</span>
			</div>  
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">STATUS
					<span class="float_right">:</span>
				</span> <span class="span_border left_align width40 text_info"
					style="display: -moz-inline-stack;">
					${PROJECT_PAYMENTS.receiptStatus}</span>
			</div>
			<c:if test="${PROJECT_PAYMENTS.description ne null && PROJECT_PAYMENTS.description ne ''}">
					<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">DESCRIPTION<span
					class="float_right">:</span>
				</span> <span class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">${PROJECT_PAYMENTS.description}</span>
			</div>
			</c:if> 
		</div>
		<div class="width48 float_left"> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">TITLE
					<span class="float_right">:</span>
				</span> <span class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${PROJECT_PAYMENTS.paymentTitle}</span>
			</div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">MILESTONE
					<span class="float_right">:</span>
				</span> <span class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${PROJECT_PAYMENTS.milestoneTitle}</span>
			</div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">DATE
					<span class="float_right">:</span>
				</span> <span class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;">
					${PROJECT_PAYMENTS.receiptDate}</span>
			</div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">AMOUNT
					<span class="float_right">:</span>
				</span> <span class="span_border right_align width65 text_info"
					style="display: -moz-inline-stack;">
					${PROJECT_PAYMENTS.receiptAmount}</span>
			</div> 
		</div>
		
	</div>
	<div class="clear-fix"></div> 
</body>
</html>