<script type="text/javascript" 
src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jZebra.js"></script>
<script type="text/javascript">
var closeWin = 1;
var printer = [];
var posPrintReciptContent = "";
$(function() {   
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/sendorder_toprinters.action",
		async : false, 
		dataType : "json",
		cache : false,
		success : function(response) { 
			if(null!=response.printers && response.printers !=""){
				printer = response.printers.split(',');
				if(printer.length > 1)
					closeWin = 0;
				posPrintReciptContent = response.kitchenOrders;
				customPrintHTML();  
			} 
		}
	}); 
}); 
</script>
<div align="center" id="content">
	<h1 id="title" style="display: none;"></h1> 
	<applet id="qz" code="qz.PrintApplet.class"
		archive="${pageContext.request.contextPath}/printing/applet/qz-print.jar" 
		width="0" height="0" align="left"> 
	</applet>
</div>