<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>

</style>
<script type="text/javascript"> 
 
 </script>
<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	
	<div class="width100 float-left">
		<span class="heading">Customer History</span>
	</div>
 	<div id="main-content" class="hr_main_content">
			<div class="width100 float-left">
						<c:choose>
							<c:when
								test="${CUSTOMER_HISTORY ne null && CUSTOMER_HISTORY ne '' && fn:length(CUSTOMER_HISTORY)>0}">
								
								<c:forEach var="customer" items="${CUSTOMER_HISTORY}"
									varStatus="status1">
										<div id="hrm" class="width100 mainhead">
											${customer.customerName} - ${customer.customerNumber}
										</div>
										<c:choose>
											<c:when test="${customer.salesInvoiceVOs ne null}">
												<fieldset>
												<legend>Credit Sales</legend>
												<c:forEach var="invoices" items="${customer.salesInvoiceVOs}"
																varStatus="status2">
														<div id="hrm" class=" width100 float-right" style="padding-top:10px;">
															<table class="width100" bgcolor="fffade">
																<tr>
																	<td colspan="2" bgcolor="fffade"><span class="side-heading">Sales ${status2.index+1} : ${invoices.salesDeliveryNumber}</span></td>
																
																	<td colspan="1" style="font-weight: bold;font-size: 12px;" bgcolor="fffade">Date : ${invoices.salesDate}</td>
																</tr>
															</table>
															<div id="hrm" class=" hastable width100">
																		<table class="width100">
																			<thead>
																				<tr>
																					<th style="width:20%;">Invoice Number</th>
																					<th style="width:20%;">Date</th>
																					<th style="width:20%;">Total Amount</th>
																					<th style="width:20%;">Invoiced Amount</th>
																					<th style="width:20%;">Balance</th>
																				</tr>
																			</thead>
																			<tbody class="tab">
																	<c:forEach var="invoiceDetail" items="${invoices.salesInvoiceVOs}"
																			varStatus="status3">
																		<tr class="rowid">
																			<td>${invoiceDetail.referenceNumber}</td>
																			<td>${invoiceDetail.salesInvoiceDate}</td>
																			<td>${invoiceDetail.totalInvoice}</td>
																			<td>${invoiceDetail.invoiceAmount}</td>
																			<td>${invoiceDetail.pendingInvoice}</td>
																		</tr>
																	</c:forEach>
																		<tr>
																			<td colspan="4" style="font-weight: bold;font-size: 15px;" align="right">Balance :</td>
																			<td style="font-weight: bold;font-size: 15px;">${invoices.pendingInvoice}</td>
																		</tr>
																	</tbody>
															</table>
														</div>
													</div>
													<br>
													</c:forEach>
													</fieldset>
												</c:when>
											</c:choose>
											<c:choose>
											<c:when test="${customer.projectSalesInvoiceVOs ne null}">
												<fieldset>
												<legend>Job Order Sales</legend>
													<c:forEach var="invoices" items="${customer.projectSalesInvoiceVOs}"
																	varStatus="status2">
															<div id="hrm" class=" width100 float-right">
																	<table class="width100">
																		<tr>
																			<td colspan="2" bgcolor="fffade"><span class="side-heading">Sales ${status2.index+1} : ${invoices.salesDeliveryNumber}</span></td>
																		
																			<td colspan="1" style="font-weight: bold;font-size: 12px;" bgcolor="fffade">Date : ${invoices.salesDate}</td>
																		</tr>
																	</table>
															
																<div id="hrm" class=" hastable width100">
																			<table class="width100">
																				<thead>
																					<tr>
																						<th style="width:20%;">Invoice Number</th>
																						<th style="width:20%;">Date</th>
																						<th style="width:20%;">Total Amount</th>
																						<th style="width:20%;">Invoiced Amount</th>
																						<th style="width:20%;">Balance</th>
																					</tr>
																				</thead>
																				<tbody class="tab">
																		<c:forEach var="invoiceDetail" items="${invoices.salesInvoiceVOs}"
																				varStatus="status3">
																			<tr class="rowid">
																				<td>${invoiceDetail.referenceNumber}</td>
																				<td>${invoiceDetail.salesInvoiceDate}</td>
																				<td>${invoiceDetail.totalInvoice}</td>
																				<td>${invoiceDetail.invoiceAmount}</td>
																				<td>${invoiceDetail.pendingInvoice}</td>
																			</tr>
																		</c:forEach>
																			<tr>
																				<td colspan="4" style="font-weight: bold;font-size: 15px;" align="right">Balance :</td>
																				<td style="font-weight: bold;font-size: 15px;">${invoices.pendingInvoice}</td>
																			</tr>
																		</tbody>
																</table>
															</div>
														</div>
														<br>
														</c:forEach>
												</fieldset>
												</c:when>
											</c:choose>
											<c:choose>
												<c:when test="${customer.pointOfSaleInvoiceVOs ne null}">
													<fieldset>
													<legend>Counter Sales</legend>
														<div id="hrm" class=" hastable width100 float-right">
															<table class="width100">
															<thead>
																<tr>
																	<th style="width:20%;">Invoice Number</th>
																	<th style="width:20%;">Date</th>
																	<th style="width:20%;">Total Amount</th>
																	<th style="width:20%;">Invoiced Amount</th>
																	<th style="width:20%;">Balance</th>
																</tr>
															</thead>
															<tbody class="tab">
															<c:forEach var="invoiceDetail" items="${customer.pointOfSaleInvoiceVOs}"
																	varStatus="status3">
																<tr class="rowid">
																	<td>${invoiceDetail.salesDeliveryNumber}</td>
																	<td>${invoiceDetail.salesDate}</td>
																	<td>${invoiceDetail.totalInvoice}</td>
																	<td>${invoiceDetail.invoiceAmount}</td>
																	<td>${invoiceDetail.pendingInvoice}</td>
																</tr>
															</c:forEach>
															</tbody>
															</table>
														</div>
													</fieldset>
												</c:when>
											</c:choose>
									<table class="width100">
										<tbody class="tab">
											<tr>
												<td colspan="4" style="font-weight: bold;font-size: 15px;" align="right">
													Customer(${customer.customerName}) Balance :
												</td>
												<td colspan="1" style="font-weight: bold;font-size: 15px;">
													${customer.pendingInvoice}
												</td>
											<tr>
										</tbody>
									</table>
								</c:forEach>
								
							</c:when>
						</c:choose>
					

					</div>
			</div>
			

</body>