﻿<%@page import="java.text.DecimalFormat"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.realestate.to.ReContractAgreementTO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="com.aiotech.aios.workflow.domain.entity.User"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Contract</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
</head>
<body>	
<div>
	<% 
		ReContractAgreementTO agreementTO = (ReContractAgreementTO)session.getAttribute("AGREEMENT_DETAILS"); 
		//ReContractAgreementTO agreementTOTenants = (ReContractAgreementTO)session.getAttribute("AGREEMENT_TENANT_DETAILS"); 
		//List<ReContractAgreementTO> agreementTOOffer = (List<ReContractAgreementTO>)session.getAttribute("AGREEMENT_OFFER_DETAILS");
		List<ReContractAgreementTO> agreementTOPayment = (List<ReContractAgreementTO>)session.getAttribute("AGREEMENT_PAYMENT_DETAILS");
		User loggedUser = (User) session.getAttribute("USER"); 
		String temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantName());
		Boolean SecondCopy = false;
		String type = "";
		String chqNo = "";
		String banks = "";
		String dates = "";
	%>
	
	<div>
		<span style="font-weight: bold; display: -moz-inline-block; width: 200px;">Contract No.  <u>${AGREEMENT_DETAILS.contractNumber}</u>  رقم العقد</span>
		<span style="font-weight: bold; display: -moz-inline-block; float: right; text-align: right; width: 340px;">Contract Issue Date  <u>${AGREEMENT_DETAILS.contractDate}</u>  تاريخ تحرير العقد</span>
	</div>
	
	<div align="center" style="height: 10px;">
		
	</div>
	
	<div class="sectionHead">
		First Party
		<span class="sectionHead_arabic">الطرف اﻻول</span>	
	</div>
	<%
		temp = agreementTOOffer.get(0).getOfferedPropertyOwners();
	%>
	<div>
		<span class="para">
		The Private Office of Mr. Adel Abdul Hameed Al
		Hosani the Representative of the Landlord "<strong><%= temp %></strong>"and is referred here in after
		as “landlord”</span>
		
		<span class="para_arabic">
			
			<% 
				temp = agreementTOOffer.get(0).getOfferedPropertyOwners_arabic();
			%>	
		
		المكتب الخاص بالسيد عادل عبد الحميد الحوسني  بصفته
مفوضا من مالك العقار "<strong> <%= temp %> </strong>"
ويشار اليه فيما بعد ب "المؤجر""
		
		
		</span>
	</div>
	
	<div class="sectionHead">
		Second Party
		<span class="sectionHead_arabic">الطرف الثاني</span>	
	</div>
	<%
		temp = agreementTOTenants.getTenantName();
	%>
	<div>
		<div>
			<span class="detail_text left_align" style="width: 170px;">Referred here in after as "Tenant":</span>			
			<span class="detail_text center_align span_border" style="width: 440px;"><%= temp %></span>
			<span class="detail_text right_align" style="width: 140px;">:ويشار اليه فيما بعد بالمستأجر</span>
		</div>
		<% 	
			try {
				temp = agreementTOTenants.getTenantIssueDate().split(" ")[0];
			} catch(Exception e){
				temp = agreementTOTenants.getTenantIssueDate();
			}
		%>
		<div>
			<span class="detail_text left_align" style="width: 164px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span> :Iss.Date / تاريخ اﻻصدار</span>
			<% 	
				temp = agreementTOTenants.getTenantIdentity();
			%>
			<span class="detail_text right_align" style="width: 300px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :ID Type - No. / نوع ورقم الهوية</span>
			<% 	
				temp = agreementTOTenants.getTenantNationality();
			%>
			<span class="detail_text right_align" style="width: 285px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Nationality / الجنسية</span>
		</div>
		
		<div>
			<% 	
				temp = agreementTOTenants.getTenantFax();
			%>
			<span class="detail_text left_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Fax / فاكس</span>
			<% 	
				temp = agreementTOTenants.getTenantMobile();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Mobile / الهاتف المتحرك</span>
			<% 	
				temp = agreementTOTenants.getTenantPOBox();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :P.O.Box / ص.ب</span>
		</div>
	</div>
	
	<div class="sectionHead">
		Property
		<span class="sectionHead_arabic">العقار</span>	
	</div>
	
	<div>
		<label><strong>Property is the Building Located at</strong></label>
		<span class="arabic"><strong>ويقصد به المبنى المقام على</strong></span>
		<br/>
		
		<div>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingStreet();
			%>
			<span class="detail_text right_align" style="width: 140px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Sector/ قطاع</span>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingCity();
			%>
			<span class="detail_text right_align" style="width: 260px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :City/ المدينة</span>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingArea();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Area/ منطقة</span>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingPlot();
			%>
			<span class="detail_text right_align" style="width: 137px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Plot No./ قطعة رقم</span>
		</div>
		
		<div>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingName();
			%>
			<span class="detail_text center_align span_border" style="width: 635px;"><%= temp %></span>
			<span class="detail_text right_align" style="width: 145px;">:Building Name /  اسم البناية</span>
		</div>
	</div>
	
	<div class="sectionHead">
		Specified Property
		<span class="sectionHead_arabic">العين المؤجرة بالعقار</span>	
	</div>
	
	<div>
		<label><strong>Referred here in after as "The Specified Property"</strong></label>
		<span class="arabic"><strong>ويشار اليها فيما بعد ب " العين المؤجرة</strong></span>
		<br/>
		
		<div>
			<% 	
				temp = "";
				//specifiedProperty = "";
				String specifiedComponent = "";
				double tempRent = 0.0;
				for(ReContractAgreementTO offered1 : agreementTOOffer) {
					//specifiedProperty +=  offered.getFlatNumber() + " - ";
					if(offered1.getComponentName() != null && offered1.getComponentName() != "")
						if(!specifiedComponent.contains(offered1.getComponentName())) 
							specifiedComponent += offered1.getComponentName() + " - ";
				}
				specifiedComponent = AIOSCommons.removeTrailingSymbols(specifiedComponent);
				temp = specifiedProperty;
				 /*if(AIOSCommons.removeTrailingSymbols(specifiedProperty)
						.equals(agreementTOOffer.get(0).getBuildingName()))
					specifiedProperty = ""; */	
				
				prefix = agreementTOTenants.getPrefix();
				if(prefix.equals(""))
					prefix = "السادة";
				else 
					prefix = AIOSCommons.translatetoArabic(agreementTOTenants.getPrefix());
			%>
			<span class="detail_text right_align" style="width: 376px;"><span class="span_border"> <%= (temp == null) ? "n/a" : temp %></span>  :Prop. No./ رقم العقار</span>
			<span class="detail_text right_align" style="width: 376px;"><span class="span_border"><%= (specifiedComponent.trim() == "") ? temp : specifiedComponent %></span>  :Floor / الطابق</span>
		</div>
		
		<div>
			<span class="detail_text left_align" style="width: 215px;">Purpose of Leasing is used for:</span>
			<span class="detail_text center_align span_border" style="width: 610px;"><%= (agreementTO.getLeasedFor() != null) ? agreementTO.getLeasedFor() + " / " + AIOSCommons.translatetoArabic(agreementTO.getLeasedFor()) : ""  %></span>
			<span class="detail_text right_align" style="width: 300px;">:الغرض من اﻻيجار : استخدام العين المؤجرة فقط كا</span>
		</div>
		
		<div>
			<% 	
				temp = agreementTOTenants.getToDate();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Expiry / تنتهي في</span>
			<% 	
				temp = agreementTOTenants.getFromDate();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Starts In/ تبدأ بـ</span>
			<% 	
				int period_years = 0;
				int period_months = 0;
				String period_years_s = "";
				String period_months_s = "";
				try {
					Date to = new Date(agreementTOTenants.getToDate());
					Date from = new Date(agreementTOTenants.getFromDate());
					period_months = DateFormat.monthsBetween(to, from);
				} catch(Exception e) { e.printStackTrace();}
				
				if((period_months / 12) != 0)
					period_years_s = (period_months / 12) + " Year(s)";
				if((period_months % 12) != 0)
					period_months_s = (period_months % 12) + " Month(s)";
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= period_years_s + " " + period_months_s%> </span> :Period / مدة اﻻيجار </span>
		</div>			
		<%
			Double contractAmount = 0.0;
			for(ReContractAgreementTO payment : agreementTOPayment) {
				
				contractAmount += payment.getChequeAmount();
			}
			temp = AIOSCommons.formatAmount(contractAmount);
			String wordAmount = AIOSCommons.convertAmountToWords(contractAmount);
		%>			
		<div>	
			<span class="detail_text right_align span_border" style="width: 470px;">(  <%= wordAmount %> )</span>
			<span class="detail_text center_align span_border" style="width: 185px;"> <%= temp %> </span>
			<span class="detail_text right_align" style="width: 110px;">  :Amount / قيمة الايجار</span>
		</div>
		<%
			temp = AIOSCommons.formatAmount(agreementTO.getDepositAmount());
		%>
		<div>
			<span class="detail_text left_align" style="width: 125px;">Security Deposit Amount:</span>
			<span class="detail_text center_align span_border" style="width: 595px;"><%= temp %></span>
			<span class="detail_text right_align" style="width: 35px;">:التأمين</span>
		</div>
	</div>
	
	<div class="sectionHead">
		Term of Payment
		<span class="sectionHead_arabic">طريقة السداد</span>	
	</div>
	
	<div>
		<table>
			<thead>
				<tr>
					<th>Payment Type <br/> نوع الدفعة</th>
					<th>Payment Method <br/> طريقة الدفع</th>
					<th>Amount <br/> المبلغ</th>
					<th>Cheque No. <br/> رقم الشيك</th>
					<th>Bank Name <br/> اسم البنك</th>
					<th>Due Date <br/> تاريخ التحصيل</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${AGREEMENT_PAYMENT_DETAILS}" var="PAYMENTS">
					<c:choose>
						<c:when test="${PAYMENTS.chequeNumber ne null}">
							<tr>
								<td>
									Cheque
								</td>
								<td>
									Cheque
								</td>
								<td>
									${PAYMENTS.chequeAmount}									
								</td>
								<td>
									${PAYMENTS.chequeNumber}
								</td>
								<td>
									${PAYMENTS.bankName}
								</td>
								<td>
									${PAYMENTS.chequeDate}
								</td>
							</tr>	
						</c:when>
						<c:when test="${PAYMENTS.chequeNumber eq null and PAYMENTS.bankName ne null and PAYMENTS.chequeDate ne null}">
							<tr>
								<td>
									Direct Transfer
								</td>
								<td>
									Direct Transfer
								</td>
								<td>
									${PAYMENTS.chequeAmount}									
								</td>
								<td>
									---
								</td>
								<td>
									${PAYMENTS.bankName}
								</td>
								<td>
									${PAYMENTS.chequeDate}
								</td>
							</tr>	
						</c:when>
						<c:otherwise>
							<tr>
								<td>
									Cash
								</td>
								<td>
									Cash
								</td>
								<td>
									${PAYMENTS.chequeAmount}									
								</td>
								<td>
									---
								</td>
								<td>
									---
								</td>
								<td>
									---
								</td>
							</tr>	
						</c:otherwise>
					</c:choose>
				</c:forEach>						
			</tbody>
		</table>
	</div>
	
	<div>
		<label style="color: #FCB514;">Remarks</label>
		<span style="color: #FCB514;" class="arabic">المﻼحظات</span>
		<div style="width: 100%; display: block;">
			<span class="span_border" style="display: block; direction: rtl; text-align: center; font-weight: bolder; font-size: 11px;">
				<%= (agreementTO.getRemarks() != null) ? AIOSCommons.translatetoArabic(agreementTO.getRemarks()) : ""  %>
			</span>	
			<span class="span_border" style="display: block; direction: rtl; text-align: center; height: 20px;">
				
			</span>	
		</div>		
	</div>

	<div class="footer" align="center">		
		يلتزم المستأجر بشروط العقد المشار اليها في خلف العقد
		<br/>
		Tenant is Obliged by the Terms and Conditions at the Back of the Contract
	</div>
	<div style="width: 220px; text-align: center; display: -moz-inline-stack;">
		<span style="padding-bottom: 80px;">Tenant Signature / توقيع المستأجر</span>
		<br/><br/><br/>
		<hr/>
	</div>
	<div style="float: right; width: 220px; text-align: center;">
		<span style="padding-bottom: 80px;">Landlord Signature / توقيع المؤجر</span>
		<br/><br/><br/>
		<hr/>
	</div>
	
	
	 <% 
	 	temp = loggedUser.getUsername()
	 		.concat("-" + loggedUser.getUsername().toString()
	 				.concat("-" + AIOSCommons.getCurrentTimeStamp()));
	 	temp = AIOSCommons.desEncrypt(temp);
	 %>
	<br/>
	<div>
		<label>Ref #: <%= temp %></label>	
	</div>
</div>
</body>
<style>
	td {
		border-style: solid; border-width: thin; border-color: black;
	}
	th {
		border-style: solid; border-width: thin; border-color: black;
	}
</style>
</html>