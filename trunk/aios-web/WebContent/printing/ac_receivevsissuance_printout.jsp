<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>	
<style>
@media print {
	.hideWhilePrint {
		display: none;
	}
}
</style>

<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div id="main-content">
		<div class="width100 float-left">
			<span class="heading">Receive VS Issuance Report</span>
		</div>
		<div class="width100 float-left">
				
				<table class="width100">
					<tr height="25px;">
						<td>From : ${fromDate}</td>
						<td>To : ${toDate}</td>
						<td>Notes :</td>
						<td colspan="2" style="font-weight:bold;width:40%;">${otherInfo}</td>
						<td>Printed On :</td>
						<td style="font-weight:bold;width:10%;">${printedOn}</td>
					</tr>
					<c:choose>
						<c:when test="${fn:length(PRODUCT_LIST)>0}">
							<tr style="margin-top: 10px;">
								<td colspan="8" align="right">
									<table style="width:100%;">
										<tr>
											<th style="font-weight:bold;">Code</th>
											<th style="font-weight:bold;">Description</th>
											<th style="font-weight:bold;">Unit Rate</th>
											<th style="font-weight:bold;">Opening Balance</th>
											<th style="font-weight:bold;">Requisitions</th>
											<th style="font-weight:bold;">Received Quantity</th>
											<th style="font-weight:bold;">Receive Sub Total</th>
											<th style="font-weight:bold;">Receive Reference</th>
											<th style="font-weight:bold;">Receive Date</th>
											<th style="font-weight:bold;">Issued Quantity</th>
											<th style="font-weight:bold;">Issue Sub Total</th>
											<th style="font-weight:bold;">Issue Reference</th>
											<th style="font-weight:bold;">Issue Date</th>
											<th style="font-weight:bold;">Balance Stock</th>
										</tr>
										<c:forEach var="detail" items="${PRODUCT_LIST}"
											varStatus="status1">
											<tr>
												<td>${detail.code}</td>
												<td>${detail.productName}</td>
												<td>${detail.unitRate}</td>
												<td>${detail.quantity}</td>
												<td>${detail.requisitionNumbers}</td>
												<td>${detail.receivedQuantity}</td>
												<td class="receiveTotalAmount">${detail.receivedTotalAmount}</td>
												<td>${detail.receiveNumbers}</td>
												<td>${detail.receiveDates}</td>
												<td>${detail.issuedQuantity}</td>
												<td class="issueTotalAmount">${detail.issuedTotalAmount}</td>
												<td>${detail.issuanceNumbers}</td>
												<td>${detail.issuanceDates}</td>
												<td style="font-weight: bold;">${detail.balanceQuantity}</td>
											</tr>
										</c:forEach>
										<tr height="25px;"><td colspan="6" align="right" style="font-weight: bold;">Receive Total: &nbsp;&nbsp;</td>
											<td id="finalTotalReceive" style="font-weight: bold;"></td>
											<td colspan="3" align="right" style="font-weight: bold;">Issuance Total : &nbsp;&nbsp;</td>
											<td id="finalTotalIssue" style="font-weight: bold;"></td>
											<td colspan="3">&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr style="margin-top: 10px;">
								<td colspan="8" align="left">
									No record found.
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</table>

		</div>
	</div>
</body>
<script type="text/javascript"> 
{  
	var total=Number(0);

	$('.receiveTotalAmount').each(function(){
		var temp=Number($(this).html());
		total+=temp;
	});
	$('#finalTotalReceive').html(total);
	
	total=Number(0);
	$('.issueTotalAmount').each(function(){
		var temp=Number($(this).html());
		total+=temp;
	});
	
	$('#finalTotalIssue').html(total);
}
 </script>