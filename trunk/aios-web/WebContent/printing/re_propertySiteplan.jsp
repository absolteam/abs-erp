<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Offer Details</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />	
</head>
<body class="arabic_label">	

	<div class="AIOTitle">

	</div>

	<div style="margin-top: 200px;">
		
		<p style="display: inline-block;" class="arabic_label">التاريخ: <p style="direction: ltr ! important; display: inline;"> <%= AIOSCommons.getCurrentTimeStamp().split(" ")[0] %> </p></p>	
			
	</div>
	
	<div style="margin-top: 50px; display: block;" align="right">	
		<p class="bold"><span style="float: left; margin-left: 100px;">المحترمين </span> السادة / دائرة الشؤون البلدية- أبوظبي </p>	
	</div>
	
	<div style="margin-top: 50px; display: block;" align="right">	
		<p class="bold"> تحية طيبة وبعد ..  </p>	
	</div>
	
	<div style="margin-top: 40px;" align="center">
		<p class="bold"><u> الموضوع: اصدار مخطط أرض </u></p>		
	</div>
	
	<!-- <div style="margin-top: 20px; " align="center">
		<p>  بالإشارة إلى الموضوع أعلاه ، يرجى التكرم بعمل اللازم و اصدار مخطط للأرض  <span>${ buildingName }</span> في <span>${ addressLine1 }</span>.
			<span> ،  وتسليمها  </span><span>${ siteplanReceiver }</span>  </p> 
	</div> -->
	
	<div style="margin-top: 20px; " align="center">
		<p>  بالإشارة إلى الموضوع أعلاه ، يرجى التكرم بعمل اللازم و اصدار مخطط للأرض  <span>${ addressLine2 } - ${ plotNo }</span> في <span>${ addressLine1 }</span>.
			<span> ،  وتسليمها الى   </span><span>${ siteplanReceiver }</span>  </p> 
	</div>
	
	<div style="margin-top: 40px;" align="center">
		<p class="bold"> <strong> و تفضلوا بقبول فائق الاحترام والتقدير  .. </strong> </p>		
	</div>
	
	<div style="padding: 20px; margin-left: 50px; margin-top: 30px;" align="left">
		<span>  مقدم الطلب:   </span>
		<br/><br/>
		<span> ${ownerNameArabic} </span>
	</div>

</body>
</html>