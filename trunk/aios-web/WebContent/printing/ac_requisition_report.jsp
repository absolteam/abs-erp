<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Requisition</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
	height: 28px;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"
			style="font-family: book; font-style: italic; text-transform: inherit !important; font-size: 28px !important; color: black;"><strong>${THIS.companyName}</strong>
		</span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading" style="font-family: book;"><span><u>REQUISITION</u>
		</span>
		</span>
	</div>
	<div style="clear: both;"></div>
	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; -moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
		class="width100 float_left">
		<c:if test="${requestScope.datefrom ne null && requestScope.datefrom ne ''}">
			<div style="padding: 5px; font-weight: bold;">
				Date from ${datefrom} to 
				<c:choose>
					<c:when test="${requestScope.dateto ne null && requestScope.dateto ne ''}">
						${dateto}
					</c:when>
					<c:otherwise>
						-N/-A
					</c:otherwise>
				</c:choose> 
			</div>
		</c:if> 
		<table style="width: 99%; margin: 0 auto;">
			<tr>
				<th>Reference</th>
				<th>Date</th>
				<th>Person</th>
				<th>Location</th> 
				<th>Product</th>
				<th>Quantity</th> 
				<th>Status</th>
			</tr>
 			<c:forEach items="${REQUISITION_DETAIL}" var="detail">
				<tr> 
					<td>${detail.requisitionVO.referenceNumber}</td>
					<td>${detail.requisitionVO.date}</td>
					<td>${detail.requisitionVO.personName}</td> 
					<td>${detail.requisitionVO.locationName}</td> 
					<td>${detail.product.productName} -- ${detail.product.code}</td>
 					<td>${detail.quantity}</td> 
					<td>${detail.requisitionVO.statusStr}</td> 
				</tr>
			</c:forEach>
		</table> 
	</div>
	<div class="clear-fix"></div>
</body>
</html>