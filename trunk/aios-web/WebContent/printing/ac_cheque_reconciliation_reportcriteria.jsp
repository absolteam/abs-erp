<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var oTable;

var fromDate = null;
var toDate = null;

$(function(){
	$(".xls-download-call").click(function(){ 
		var issuedChecked = $('#issuedCheques').attr('checked');
		var receivedChecked = $('#receivedCheques').attr('checked');
		fromDate = $('#fromDate').val();
		toDate = $('#endDate').val();
		window.open("<%=request.getContextPath()%>/get_issuereceived_cheque_reportxls.action?issuedChecked="+issuedChecked+"&receivedChecked="+receivedChecked+"&toDate="+toDate+"&fromDate="+fromDate
				+"&format=HTML",'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px'); 
		return false;  
	});
 	
 	$(".print-call").click(function(){ 
 		var issuedChecked = $('#issuedCheques').attr('checked');
		var receivedChecked = $('#receivedCheques').attr('checked');
		fromDate = $('#fromDate').val();
		toDate = $('#endDate').val();
 		window.open("<%=request.getContextPath()%>/get_issuereceived_cheque_printout.action?issuedChecked="+issuedChecked+"&receivedChecked="+receivedChecked+"&toDate="+toDate+"&fromDate="+fromDate
				+"&format=HTML",'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});  
 	 
 	
 	$('#fromDate,#endDate').datepick({
		 onSelect: customRange, showTrigger: '#calImg'});  
	
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	}  
	
	$('#issuedCheques,#receivedCheques').click(function(){
		$('#allCheques').attr('disabled', false);
		$('#allCheques').attr('checked', false);
		var ichecked = $('#issuedCheques').attr('checked');
		var rchecked = $('#receivedCheques').attr('checked');
		if(ichecked == true && rchecked == true){ 
 			$('#allCheques').attr('disabled', true); 
 			$('#allCheques').attr('checked', true);
		}
	});
	
	$('#allCheques').click(function(){
		var checked = $(this).attr('checked');
		$('#issuedCheques,#receivedCheques').attr('disabled', false);
		if(checked == true){
			$('#issuedCheques,#receivedCheques').attr('disabled', true);
			$('#issuedCheques,#receivedCheques').attr('checked', true);
 		}else{
			$('#issuedCheques,#receivedCheques').attr('checked', false);
 		}
	});
 	//populateDatatable();
});

	
function populateDatatable(){
	
	
} 

function checkLinkedDays() {
	var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
			'#selectedMonth').val());
	$('#selectedDay option:gt(27)').attr('disabled', false);
	$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
			true);
	if ($('#selectedDay').val() > daysInMonth) {
		$('#selectedDay').val(daysInMonth);
	}
}
function customRange(dates) {
 	if (this.id == 'fromDate') {
		$('#endDate').datepick('option', 'minDate',
				dates[0] || null);
	} else {
		$('#fromDate').datepick('option', 'maxDate',
				dates[0] || null);
	}
}
 
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Presented Cheque Report
		</div>
		<div class="portlet-content"> 
			<div class="width50 float-left" style="padding: 5px;"> 
				<div class="float-left width100"> 
					<span class="float-left width25">
						<label class="width70" for="issuedCheques"
							style="position: relative; top: 3px; left: 2px;">Issued Cheque</label> 
						<input type="checkbox" name="issuedCheques" id="issuedCheques" class="width5 float-left"/>
					</span>
					<span class="float-left width25"> 
						<label class="width80" for="receivedCheques"
							style="position: relative; top: 3px; left: 2px;">Received Cheque</label> 
						<input type="checkbox" name="receivedCheques" id="receivedCheques" class="width5 float-left"/>
					</span>
					<span class="float-left width25">
						<label class="width70" for="allCheques"
							style="position: relative; top: 3px; left: 2px;">Both</label> 
						<input type="checkbox" name="allCheques" id="allCheques" class="width5 float-left"/>
					</span>
				</div> 
				<div style="position: relative; top: 5px;" class="float-left width100">
					<label class="width15">From Date</label>
					<input type="text" name="fromDate" id="fromDate" class="width40"/>
				</div>
				<div style="position: relative; top: 5px;" class="float-left width100">
					<label class="width15">End Date</label>
					<input type="text" name="endDate" id="endDate" class="width40"/>
				</div>
			</div> 
			<br><br> 
			<div class="tempresult" style="display: none;"></div> 
			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="ChequeTransRT"> 
					</table>
				</div>
			</div> 
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF" style="display: none;">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div> 