<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Journal Voucher</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PURCAHSE ORDER INVOICE</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width40 float_right">
			<c:if test="${PURCHASE_INFO.supplier ne null && PURCHASE_INFO.supplier != null &&  PURCHASE_INFO.supplier.person ne null}">
				 <div class="width100 div_text_info">
					<span class="float_left left_align width28 text-bold">Supplier Number<span class="float_right">:</span></span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
						${PURCHASE_INFO.supplier.supplierNumber}
					</span> 
				</div>
				<div class="width100 div_text_info">
					<span class="float_left left_align width28 text-bold">Supplier Name<span class="float_right">:</span></span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
						${PURCHASE_INFO.supplier.person.firstName} ${PURCHASE_INFO.supplier.person.lastName} &nbsp;
					</span>
				</div>
			
				<div class="width100 div_text_info">
					<span class="float_left left_align width28 text-bold">Payment Terms<span class="float_right">:</span> </span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
						${PURCHASE_INFO.creditTerm.name} &nbsp;
					</span>
				</div>
			</c:if>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">Currency<span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					${PURCHASE_INFO.currency.currencyPool.code}
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">Description<span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					${PURCHASE_INFO.description}
				</span>
			</div>
		</div>  
		<div class="width50 float_left">
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">Purchase Number<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${PURCHASE_INFO.purchaseNumber}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">Quotation<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					${PURCHASE_INFO.quotation.quotationNumber} &nbsp;
				</span>  		
			</div> 
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">Date<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:set var="date" value="${PURCHASE_INFO.date}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%>
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"> Expiry Date <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					<c:set var="date" value="${PURCHASE_INFO.expiryDate}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%>
				</span>
			</div>
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">Status<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					${PURCHASE_INFO.status}
				</span>  		
			</div>
			 
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th style="width:5%">Item Type</th>   
			    <th style="width:5%">Product</th> 
			    <th style="width:5%">UOM</th>  
			    <th style="width:5%">Unit Rate</th> 
			    <th style="width:5%">Quantity</th> 
			    <th style="width:5%">Total Amount</th> 
			    <th style="width:5%">Description</th>
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:forEach begin="0" end="6" step="1">
						<td/>
					</c:forEach>
				</tr>
				<c:forEach items="${requestScope.PURCHASE_INFO.purchaseDetails}" var="result" varStatus="status1"> 
					<tr> 
						<td>${result.product.itemType }</td>
						<td>${result.product.productName}</td>
						<td>${result.product.lookupDetailByProductUnit.displayName}</td>
						<td>${result.unitRate }</td>
						<td>${result.quantity }</td>
						<td>${result.unitRate * result.quantity}</td>
						<td>${result.description }</td>
					</tr>
				</c:forEach> 
				<c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="6" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	</body>
</html>

















<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="re.contract.contractreport" /></title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading"><fmt:message key="ac.tender.tenderReport" /></span>
	</div>
	<div class="width100 float-left">
		<span class="side-heading"><fmt:message
				key="re.property.filtercriteria" /></span>
	</div>
	<div class="width100 float-left">
		<div class="data-container width50 float-left">
			<div>
				<label class="data width50">${TENDER_INFO.tenderNumber}:</label> <label
					class="caption width30"><fmt:message
						key="accounts.tender.list.tenderNumber" /></label>
			</div>
			<div>
				<label class="data width50">${TENDER_INFO.tenderDate}:</label> <label
					class="caption width30"><fmt:message
						key="accounts.tender.list.date" /></label>
			</div>
		</div>
		<div class="data-container width50 float-left">
			<div>
				<label class="caption width30"><fmt:message
						key="accounts.tender.list.expiry" /> :</label> <label
					class="data width50">${TENDER_INFO.tenderExpiry}</label>
			</div>
			<div>
				<label class="caption width30"><fmt:message
						key="accounts.tender.currency" /> :</label> <label class="data width50">${TENDER_INFO.tenderExpiry}</label>
			</div>
		</div>
	</div>

	<div class="width100 float-left">
		<span class="side-heading"><fmt:message
				key="accounts.tender.generalInfo" /></span>
	</div>
	<div class="data-list-container width100 float-left"
		style="margin-top: 10px;">
		<table class="width100">
			<thead>
				<tr>
					<th><fmt:message key="accounts.tender.add.tenderNo" /></th>
					<th><fmt:message key="accounts.tender.amount" /></th>
					<th><fmt:message key="accounts.tender.currency" /></th>
					<th><fmt:message key="accounts.tender.list.date" /></th>
					<th><fmt:message key="accounts.tender.list.expiry" /></th>
					<th><fmt:message key="accounts.tender.add.description" /></th>
					
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>${TENDER_INFO.tenderNumber }</td>
					<td></td>
					<td></td>
					<td>${TENDER_INFO.tenderDate }</td>
					<td>${TENDER_INFO.tenderExpiry }</td>
					<td>${TENDER_INFO.description }</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="width100 float-left">
		<span class="side-heading"><fmt:message
				key="accounts.tender.add.productInfo" /></span>
	</div>
	<div class="data-list-container width100 float-left"
		style="margin-top: 10px;">
		<c:choose>
			<c:when test="${fn:length(requestScope.TENDER_DETAIL_INFO)gt 0}">
				<table class="width100">
					<thead>
						<tr>
							<th style="width:5%"><fmt:message key="accounts.tender.add.itemType" /></th>   
						    <th style="width:5%"><fmt:message key="accounts.tender.add.product" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.uom" /></th>  
						    <th style="width:5%"><fmt:message key="accounts.tender.add.unitRate" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.quantity" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.totalAmount" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.description" /></th> 
						</tr>
					</thead>
					<tbody>

						<c:forEach items="${requestScope.TENDER_DETAIL_INFO}" var="result"
							varStatus="status1">
							<tr>
								<td>${result.product.itemType }</td>
								<td>${result.product.productName}</td>
								<td>${result.product.lookupDetailByProductUnit.displayName }</td>
								<td>${result.unitRate }</td>
								<td>${result.quantity }</td>
								<td>${result.unitRate * result.quantity}</td>
								<td>${result.description }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:when>
			<c:otherwise>
				<tr class="even rowid">
					<fmt:message key="re.contract.nocontracts" />
				</tr>
			</c:otherwise>
		</c:choose>
	</div>
	
	
	
	--------------------------------------------------------------------
	
	<div class="data-list-container width100 float-left"
		style="margin-top: 10px;">
		
				<table class="width100">
					<thead>
						<tr>
							<th style="width:5%"><fmt:message key="accounts.tender.add.itemType" /></th>   
						    <th style="width:5%"><fmt:message key="accounts.tender.add.product" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.uom" /></th>  
						    <th style="width:5%"><fmt:message key="accounts.tender.add.unitRate" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.quantity" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.totalAmount" /></th> 
						    <th style="width:5%"><fmt:message key="accounts.tender.add.description" /></th> 
						</tr>
					</thead>
					<tbody>

						<c:forEach items="${requestScope.TENDER_INFO_SET}" var="result2"
							varStatus="status2">
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td>${result2.tenderNumber }</td>
								<td>${result2.tenderDate }</td>
								<td>${result2.tenderExpiry}</td>
								<td>${result2.description }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			
	</div>

</body>
</html> --%>