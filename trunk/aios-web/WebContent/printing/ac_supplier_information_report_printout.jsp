<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="hr.report.attendancepunch" />
</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
td {
	background-color:white !important;
	}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading">Supplier
		</span>
	</div>
	
	<div class="width100 float-left">
		<span class="side-heading">Supplier Information
		</span>
	</div>
	<div class="data-list-container width100 float-left"
		style="margin-top: 10px;">
		<c:choose>
			<c:when
				test="${fn:length(SUPPLIERS_LIST)gt 0}">
				<table class="width100">
					
					<tbody>
						<c:forEach items="${SUPPLIERS_LIST}"
							var="per" varStatus="status1">
							<tr>
								<td>
									<div><label>Supplier Name :</label><span>${per.supplierName}</span></div>
									<div><label>Number :</label><span>${per.supplierNumber}</span></div>
									<div><label>Supplier Type :</label><span>${per.type}</span></div>
									<div><label>Branch :</label><span>${per.branchName}</span></div>
									
								</td>
								<td>
									<div><label>Payment Term :</label><span>${per.paymentTermName}</span></div>
									<div><label>Bank Name :</label><span>${per.bankName}</span></div>
									
								</td>
								
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
			</c:when>
			<c:otherwise>
				<tr class="even rowid">
					<fmt:message key="hr.attendance.norecord" />
				</tr>
			</c:otherwise>
		</c:choose>
	</div>

</body>
</html>