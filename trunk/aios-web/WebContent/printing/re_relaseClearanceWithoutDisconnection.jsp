<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Offer Details</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />	
</head>
<body>	

	<div class="AIOTitle">
	</div>

	<div style="margin-top: 50px;">
		
		<p> <%= AIOSCommons.getCurrentTimeStamp().split(" ")[0] %>  <span>: التاريخ </span> </p>	
		<p class="arabic_label">رقم: ${AGREEMENT_DETAILS.contractNumber}</p>	
			
	</div>
	
	<div style="margin-top: 50px; display: block; width: 100%;">			
		<span class="bold" style="float: left; margin-left: 100px;"> المحترمين </span>
		<span class="bold" style="float: right;">السادة /شركة أبوظبي للتوزيع </span>	
	</div>
	
	<div style="margin-top: 100px; display: block;" align="right">	
		<p class="bold">.. تحية طيبة وبعد </p>	
	</div>
	
	<div style="margin-top: 90px;" align="center">
		<p class="bold"><u> الموضوع : اصدار براءة ذمة بدون فصل </u></p>		
	</div>
	
	<div style="margin-top: 20px;" align="center">
		<p> 
		<%=  agreementTOOffer.get(0).getBuildingArea()  %>	في	<%= agreementTOOffer.get(0).getBuildingName() %>	<span>   بالإشارة إلى الموضوع أعلاه ، يرجى التكرم بعمل اللازم للاشتراك رقم  </span>	(${transferAccount }) <span> ، وذلك بإصدار براءة ذمة بدون فصل  للعقار </span>  
		
	  	</p>
	</div>
			
	<div style="margin-top: 30px;" align="center">
		<p>.. و تفضلوا بقبول فائق الاحترام والتقدير  </p>
	</div>
	
	<div style="padding: 20px; margin-left: 50px; margin-top: 30px;" >
		<span>: مقدم الطلب  </span>
		<br/><br/>
		<span> <%= AIOSCommons.translatetoArabic(agreementTOOffer.get(0).getOfferedPropertyOwners()) %> </span>
	</div>

</body>
</html>