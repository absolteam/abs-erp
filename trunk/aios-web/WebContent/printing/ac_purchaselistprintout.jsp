<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Purchase Orders</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PURCAHSE ORDER</span></span></div>

	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100" border="0">
			<tr>  
				<th style="width:5%">Ref No.</th> 
				<th style="width:5%">Date</th> 
				<c:if test="${sessionScope.THIS.companyKey eq 'maghaweer'}">
					<th style="width:5%">Requisition No.</th>
					<th style="width:5%">Department</th>
				</c:if>
				<th style="width:15%">Supplier</th>  
			    <th style="width:20%">Product</th> 
			    <th style="width:5%">UOM</th>  
			    <th style="width:5%">Unit Rate</th> 
			    <th style="width:5%">Quantity</th> 
			    <th style="width:5%">Total Amount</th> 
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:choose>
						<c:when test="${sessionScope.THIS.companyKey eq 'maghaweer'}">
							<td colspan="10"></td>
						</c:when>
						<c:otherwise>
							<td colspan="8"></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<c:forEach items="${PURCHASE_LIST}" var="result" varStatus="status1"> 
					<tr> 
						<td>${result.purchaseVO.purchaseNumber}</td>
						<td>${result.purchaseVO.fromDate}</td>
						<c:if test="${sessionScope.THIS.companyKey eq 'maghaweer'}">
							<td>${result.purchaseVO.requisitionNumber}</td>
							<td>${result.purchaseVO.departmentName}</td>
						</c:if>
						<td>${result.purchaseVO.supplierName}</td>
						<td>${result.product.productName}</td>
						<td>${result.product.lookupDetailByProductUnit.displayName}</td>
						<td>${result.unitRate}</td>
						<td>${result.quantity}</td>
						<td>${result.totalRate}</td>
					</tr>
				</c:forEach> 
				<tr style="height: 25px;font-weight: bold;">
					<c:choose>
						<c:when test="${sessionScope.THIS.companyKey eq 'maghaweer'}">
							<td colspan="7"></td>
						</c:when>
						<c:otherwise>
							<td colspan="5"></td>
						</c:otherwise>
					</c:choose>
					<td colspan="2">TOTAL : </td>
					<td colspan="1">${TOTAL_PURCHASE}</td>
				</tr>  
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	</body>
</html>

