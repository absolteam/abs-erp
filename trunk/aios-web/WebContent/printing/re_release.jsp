<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.realestate.to.ReContractAgreementTO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Release Details</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript">
		window.print();
	</script>
	<style>
	.final-amount{
		float:left;
		border:1px;
		border:1px solid #7c7c77;
		min-height:30px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		background: #f0f0f0;
		width: 100%;
	}
	</style>
</head>
<body>	
	<div>
	<% 
		ReContractAgreementTO agreementTO = (ReContractAgreementTO)session.getAttribute("AGREEMENT_DETAILS"); 
		ReContractAgreementTO agreementTOTenants = (ReContractAgreementTO)session.getAttribute("AGREEMENT_TENANT_DETAILS"); 
		List<ReContractAgreementTO> agreementTOOffer = (List<ReContractAgreementTO>)session.getAttribute("AGREEMENT_OFFER_DETAILS");
		String temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantName());
		Boolean SecondCopy = false;
		String type = "";
		String chqNo = "";
		String banks = "";
		String dates = "";
		String depositAmount="";
	%> 

	<div>
		<span style="font-weight: bold; display: -moz-inline-block; width: 200px;">Contract No.<u>   ${AGREEMENT_DETAILS.contractNumber}    </u>رقم العقد</span>
		<span style="font-weight: bold; display: -moz-inline-block; float: right; text-align: right; width: 340px;">Contract Issue Date<u>   ${AGREEMENT_DETAILS.contractDate}   </u>تاريخ تحرير العقد</span>
	</div>
	
	<div class="sectionHead">
		Tenant Details
		<span class="sectionHead_arabic">الطرف الثاني</span>	
	</div>
	
	<div>
		<div>
			<span class="detail_text left_align" style="width: 250px;">Referred here in after as "Tenant":</span>			
			<span class="detail_text center_align span_border" style="width: 250px;"><%= temp %></span>
			<span class="detail_text right_align" style="width: 250px;">:ويشار اليه فيما بعد بالمستأجر</span>
		</div>
		<% 	
			try {
				temp = agreementTOTenants.getTenantIssueDate().split(" ")[0];
			} catch(Exception e){
				temp = agreementTOTenants.getTenantIssueDate();
			}
		%>
		<div>
			<span class="detail_text left_align" style="width: 275px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span> :Iss.Date / تاريخ اﻻصدار</span>
			<% 	
				temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantIdentity());
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :ID Type - No. / نوع ورقم الهوية</span>
			<% 	
				temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantNationality());
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Nationality / الجنسية</span>
		</div>
		
		<div>
			<% 	
				temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantFax());
			%>
			<span class="detail_text left_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Fax/Mob./ متحرك / فاكس</span>
			<% 	
				temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantMobile());
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Tel./ هاتف</span>
			<% 	
				temp = AIOSCommons.translatetoArabic(agreementTOTenants.getTenantPOBox());
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :P.O.Box / ب . ص</span>
		</div>
	</div>
	
	<div class="sectionHead">
		Property
		<span class="sectionHead_arabic">العقار</span>	
	</div>
	
	<div>
		<label><strong>Property is the Building Located at</strong></label>
		<span class="arabic"><strong>ويقصد به المبنى المقام على</strong></span>
		<br/>
		
		<div>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingStreet();
			%>
			<span class="detail_text left_align" style="width: 186px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Street/ شارع</span>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingCity();
			%>
			<span class="detail_text right_align" style="width: 186px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :City/ مدينة</span>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingArea();
			%>
			<span class="detail_text right_align" style="width: 186px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Area/ منطقة</span>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingPlot();
			%>
			<span class="detail_text right_align" style="width: 186px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Plot No./ قطعة رقم</span>
		</div>
		
		<div>
			<% 	
				temp = agreementTOOffer.get(0).getBuildingName();
			%>
			<span class="detail_text center_align span_border" style="width: 544px;"><%= temp %></span>
			<span class="detail_text right_align" style="width: 200px;">:Building Name /  اسم البناية</span>
		</div>
	</div>
	
	<div class="sectionHead">
		Specified Property
		<span class="sectionHead_arabic">العقار</span>	
	</div>
	
	<div>
		<label><strong>Referred here in after as "The Specified Property"</strong></label>
		<span class="arabic"><strong>ويشار اليها فيما بعد ب " العين المؤجرة</strong></span>
		<br/>
		
		<div>
			<% 	
				temp = "";
				double tempRent = 0.0;
				for(ReContractAgreementTO offered : agreementTOOffer) {
					temp +=  offered.getFlatNumber() + " - ";
				}
				temp = temp.trim();
			%>
			<span class="detail_text right_align" style="width: 376px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Prop. No./ رقم الشقة</span>
			<span class="detail_text right_align" style="width: 376px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Prop. / الطابق</span>
		</div>
			
		<div>
			<% 	
				temp = agreementTOTenants.getToDate();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Ends At/ تنتهي في</span>
			<% 	
				temp = agreementTOTenants.getFromDate();
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border"><%= (temp == null) ? "n/a" : temp %></span>  :Starts In/ تبدأ بـ</span>
			<% 	
				int period = 0;
				try {
					Date to = new Date(agreementTOTenants.getToDate());
					Date from = new Date(agreementTOTenants.getFromDate());
					period = to.getYear() - from.getYear();
				} catch(Exception e) { e.printStackTrace();}
			%>
			<span class="detail_text right_align" style="width: 250px;"><span class="span_border">Yearly</span> :Period / مدة اﻻيجار </span>
		</div>			
		
		<%
			temp = AIOSCommons.formatAmount(agreementTO.getDepositAmount());
			depositAmount = AIOSCommons.formatAmount(agreementTO.getDepositAmount());
		%>
		<div>
			<span class="detail_text left_align" style="width: 250px;">Deposit Amount:</span>
			<span class="detail_text center_align span_border" style="float:left;width: 20%;"><%= temp %></span>
			<span class="detail_text right_align" style="width: 250px;">:التأمين</span>
		</div>
	</div>
	
	<div class="sectionHead">
		Damage Details
		<span class="sectionHead_arabic">العقار</span>	
	</div>
	
	<div>
	<c:choose>
		<c:when test="${RELEASE_DETAILS!= null && (fn:length(RELEASE_DETAILS) > 0)}" >	
		<table>
			<thead>
				<tr>
					<th>Asset Name <br/> الأصول اسم</th>
					<th>Damage <br/> تلف</th>
					<th>Amount <br/> كمية</th>
					
				</tr>
			</thead>
			<tbody>
			
					<c:forEach items="${RELEASE_DETAILS}" var="DAMAGES">
						<tr>
							<td>${DAMAGES.assetName}</td>
							<td>${DAMAGES.damage}</td>
							<td>${DAMAGES.balanceAmount}</td>
						</tr>
					</c:forEach>
									
			</tbody>
		</table>
		<span class="detail_text center_align span_border" style="float:left;width: 95%;"></span>
		<div style="float:left;width: 95%;">
			<span class="detail_text right_align" style="float:left;width: 78%;">Total/مجموع:</span>
			<span class="detail_text right_align" style="float:left;width: 15%;">${DAMAGE_TOTAL}</span>
		</div>
		</c:when>
		<c:otherwise>
			<tr>No Damage, Assets are good condition / لا ضرر، الأصول هي حالة جيدة</tr>
		</c:otherwise>	
	</c:choose>		
	</div>
	<div class="sectionHead">
		Refund Amount
		<span class="sectionHead_arabic">استرداد المبلغ</span>	
	</div>
	<div style="float:left;width: 45%;">
		<span class="detail_text left_align" style="float:left;width: 50%;">Damage Total/مجموع:</span>
		<span class="detail_text left_align" style="float:left;width: 40%;">${DAMAGE_TOTAL}</span>
		
		<span class="detail_text left_align" style="float:left;width: 50%;">Other Charges/رسوم أخرى:</span>
		<span class="detail_text left_align" style="float:left;width: 40%;">${OTHER_CHARGES}</span>
		
		<span class="detail_text right_align span_border" style="float:left;width: 95%;"></span>
		<span class="detail_text left_align" style="float:left;width: 50%;">Total Deduction/مجموع الخصم:</span>
		<span class="detail_text left_align" style="float:left;width: 40%;">${TOTAL_DEDUCTION}</span>
	</div>
	<div style="float:left;width: 45%;">	
		<span class="detail_text left_align" style="float:left;width: 50%;">Deposit Amount/إيداع المبلغ:</span>
		<span class="detail_text left_align" style="float:left;width: 40%;"><%= temp %></span>
		
		<span class="detail_text left_align" style="float:left;width: 50%;">Total Deduction/مجموع الخصم:</span>
		<span class="detail_text left_align" style="float:left;width: 40%;">${TOTAL_DEDUCTION}</span>
		
		<span class="detail_text right_align span_border" style="float:left;width: 95%;"></span>
		<span class="detail_text left_align" style="float:left;width: 50%;">Refund Amount/استرداد المبلغ:</span>
		<span class="detail_text left_align" style="float:left;width: 40%;">${REFUND_AMOUNT}</span>
		
	</div>
	<div class="final-amount">
			<span class="detail_text right_align" style="float:left;width: 25%;margin:5px 0;">Refund Amount/استرداد المبلغ :</span>
			<span class="detail_text center_align " style="float:left;width: 20%;font:bold;margin:5px 0;">${REFUND_AMOUNT} </span>
			<span class="detail_text left_align " style="float:left;width: 50%;font:bold;margin:5px 0;"> (${REFUND_AMOUNT_WORDS})</span>
	</div>
	
</div>	
</body>
</html>