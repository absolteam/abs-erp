<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>LEDGER REPORT</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
	<link href="${pageContext.request.contextPath}/css/tables.css" rel="stylesheet"  type="text/css"  media="all" />
</head>
<style type="text/css">  
 
.hastable tr td{
	border-left: 0px;
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;">
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>DIRECT PAYMENT TRANSACTION REPORT</span></span></div>
	<div style="clear: both;"></div> 
	<div style="position:relative; top: 10px; height:100%;" class="width98 float_left">
		<div class="hastable width100">  
			<table id="hastab" class="width100"> 
			<tr>  
				<th>VOUCHER NO.</th>
				<th>DESCRIPTION</th>
				<th>DEBIT</th>
				<th>CREDIT</th> 
			</tr>
			<tbody>   
				<c:forEach items="${DIRECT_PAYMENT}" var="TRANSACTION" varStatus="status"> 
					<tr>
						<td colspan="4" class="left_align">${TRANSACTION.accountCode}</td> 
					</tr> 
					<tr/>
					<c:forEach items="${TRANSACTION.descriptionList}" var="LEDGER_DETAIL">
						<tr>
							<td>${LEDGER_DETAIL.voucherNumber}</td> 
							<td>${LEDGER_DETAIL.lineDescription}</td> 
							<c:choose>
								<c:when test="${LEDGER_DETAIL.debitBalance ne null && LEDGER_DETAIL.debitBalance ne ''}">
									<td>${LEDGER_DETAIL.debitBalance}</td><td/>
								</c:when>
								<c:otherwise>
									<td/><td>${LEDGER_DETAIL.creditBalance}</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach> 
					<tr/>
					<tr> 
						<td colspan="2" class="left_align">Closing Balance</td>  
						<c:choose> 
							<c:when test="${TRANSACTION.debitBalanceTotal ne null && TRANSACTION.debitBalanceTotal ne ''}"> 
								<td>${TRANSACTION.debitBalanceTotal}</td><td/>
							</c:when>
							<c:otherwise>
								<td/><td>${TRANSACTION.creditBalanceTotal}</td>
							</c:otherwise>
						</c:choose> 
					</tr><tr/><tr/>
				</c:forEach>  
			</tbody>
		</table>
		</div>
	</div>   
	</body>
</html>