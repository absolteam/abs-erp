  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <script type="text/javascript">
 $(function(){
	 $("#password").focus();
 });
 </script>
 <body id="login">
 
				<form name="login1" method="post"> 
					<div class="notification information png_bg">
						<div>
							Login here.
						</div>
					</div>
					<c:if test="${requestScope.errorMsg !=null && requestScope.errorMsg ne ''}">
					<div class="response-msg error ui-corner-all">
									<span><fmt:message key="${requestScope.errorMsg}"/></span> 
					</div>
					</c:if>
					<p>
						<label>Username</label>
						<input class="text-input" type="text" name="empid" id="empid" value="${requestScope.username }"/>
					</p>
					<div class="clear"></div>
						<div id="unameErr" class="response-msg error ui-corner-all">
								<span>Enter the User Name.</span> 
						</div>
					<div class="clear"></div>
					<c:if test="${requestScope.userError !=null && requestScope.userError ne ''}">
					<div class="response-msg error ui-corner-all">
									<span>${requestScope.userError}</span> 
					</div>
					</c:if>
					<p>
						<label>Password</label>
						<input class="text-input" type="password" name="password" id="password" /> 
					</p>
					<div class="clear"></div>
						<div id="pwdErr" class="response-msg error ui-corner-all">
								<span>Enter the Password.</span> 
						</div>
					<div class="clear"></div>
					<c:if test="${requestScope.passError !=null && requestScope.passError ne ''}">
					<div class="response-msg error ui-corner-all">
									<span>${requestScope.passError}</span> 
					</div>
					</c:if>
						<div class="clear"></div>
					<p>
						<input class="button" type="button" value="Sign In" onclick="loginAjaxCall();"/>
					</p> 
				</form> 
 </body>