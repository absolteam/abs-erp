<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/msgbar/skins/vista/skin.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/dhtmlTree/dhtmlxtree.css" />
<script type="text/javascript" src="msgbar/jquery.messagebar.js"></script>
<script type="text/javascript" src="dhtmlTree/dhtmlxcommon.js"></script>
<script type="text/javascript" src="dhtmlTree/dhtmlxtree.js"></script> <script
	type="text/javascript" src="dhtmlTree/ext/dhtmlxtree_json.js"></script>

<div id="uploaderDiv" class="" style="height: 190px;"> 
<div id="errorDiv" class="response-msg error ui-corner-all width70" style="display:none;"></div> 
<form id="file_upload" action="uploadTemplateFile.action" method="POST"
	enctype="multipart/form-data"><input type="hidden"
	name="fileType" id="fileType"></input> <input type="hidden"
	name="displayPane" id="displayPane"></input> <input type="hidden"
	name="mode" id="mode"></input> <input type="hidden" name="recordId"
	id="recordId"></input> <input type="hidden" name="tableName"
	id="tableName"></input> <input type="hidden" name="parentNodeText"
	id="parentNodeText" /> <input id="file_1" type="file"
	name="fileToUpload" style="visibility: hidden;" accept="image/jpg">	 

<div id="drop_zone_1" style="display: none;">
<div>Upload/Drop files</div>
</div>
<button>Upload</button> 
<div style="overflow: auto; width: 100%; margin-top: -28px; height: 202px;" class="float-left">
	<table id="files_1" cellpadding="5" cellspacing="5" class="" style="max-height: 357px; width: 100%;">
		<tr>
			<td colspan="4" style="height: 190px;" id="tree_td">	
			<div style="text-align: left; margin-top: 0px; margin-bottom: 2px;" id = "toolBar">
			<img
				src="${pageContext.request.contextPath}/dhtmlTree/imgs/addFolder.png"
				id="addFolder" height="25" width="100" /> <img
				src="${pageContext.request.contextPath}/dhtmlTree/imgs/deleteFolder.png"
				id="deleteFolder" height="25" width="100" /> <img
				src="${pageContext.request.contextPath}/images/add-icon.png"
				id="uploadFile" height="25" width="25" style="vertical-align: middle;" /> Add New Template</div>
			<div id="treeboxbox_tree"
				style="width: 100%; height: 160px; background-color: #f5f5f5; 
						border: 1px solid Silver; overflow: auto;"></div> 
			</td>
		</tr>
		<tr style="border: 1px solid black; background-color: #EEEEEE;"
			height="30" class="tableHeader" id="uploaderTableHeader">
			<td width="25%">File Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td width="50%">Progress</td>
			<td width="20%">Size(KB)</td>
			<td width="5%">Action(s)</td>
		</tr>	
	</table>
</div> 
</form>
</div>

<script>
 
 	var CurrentlyUploadedTemplates = new Array();
 	
 	function getUploadedTemplates(){
 		 $.ajax({
 			    type : "POST",
 			    url : "populateUploadedTemplateFile.action",
 			    async : false,
 			    dataType : "json",
 			    cache : false,
 			    success : function(result) {
 				    var list = result.fileList;
 				    for ( var itr = 0; itr < list.length; itr++) {
 					    var rec = list[itr];
 						var row = $('<tr id='
								+ rec.fileName
								+ '> '
								+ '<td>'
								+ rec.fileName.split("_")[0]
								+ '</td>'
								+ '<td>'
								+ '<img src="fileupload/success.png"/> File uploaded successfully. </td>'
								+ '<td >'
								+ rec.fileSize
								+ '</td>'
								+ '<td id = '
								+ (itr + 1)
								+ ' style="text-align: center;" >'
								+ "<img id=" 
								+ rec.fileName + " src ='${pageContext.request.contextPath}/images/delete.gif' height='10' width='10' style='cursor: pointer; text-align: center;' onclick = 'delRow(this,"
								+ "this.id);' />" + '</td>' + '</tr>');
 						CurrentlyUploadedTemplates[itr] = rec.fileName.split("_")[0];
						$($("#files_1")[0]).append(row[0]); 
 					}
 			    }
 		});
 	};
 	getUploadedTemplates();
 	
	var rootNodeLabel = null;
	var tName = null;
	var dirs = new Array();
	var files = new Array();
	var treeTable = new Array();
	tree = new dhtmlXTreeObject("treeboxbox_tree", "100%", "100%", 0);
	tree.setSkin('dhx_skyblue');
	tree.setImagePath("dhtmlTree/imgs/");
	tree.setXMLAutoLoading("getTreeFile.action");
	tree.setDataMode("json");
	tree.loadJSON("getTreeFile.action?fileType=" + usrFileType + "&recordId="
			+ usrRecordId + "&displayPane=" + usrDisplayPane + "&tableName="
			+ userTableName + "&rootLabel=" + root);
	rootNodeLabel = root;
	var qryStrArray = new Array();
	var delRow = function(ctrl, fileName) {
		var cnfrm = confirm('Selected template will be deleted');
		if(cnfrm) {
			$($($(ctrl).parents()[0]).parents()[0]).remove();
			var index = jQuery.inArray(fileName, CurrentlyUploadedTemplates);
			if(index != -1)
				CurrentlyUploadedTemplates.splice(index, 1);
			deleteTemplateFromServer(fileName);
		}
	};
	 
	var deleteTemplateFromServer = function (fileName) {
		
		$.ajax({
			url : "deleteUploadedTemplateFile.action?nameofFileToDelete=" + fileName,
			async : false,
			dataType : "html",
			success : function(result) {
				
			},
			failure : function(result) {
				alert(result);
			}
		});   
	};
	
	var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
			: $("#displayPane").val();
	$
			.ajax( {
				type : "POST",
				url : "populateUploadedFile.action?fileType=" + usrFileType
						+ "&recordId=" + usrRecordId + "&displayPane="
						+ usrDisplayPane + "&tableName=" + userTableName
						+ "&rootLabel=" + root,
				async : false,
				dataType : "json",
				cache : false,
				success : function(result) {
					if (usrFileType == "img"){
						$("#treeboxbox_tree").css({"display":"none"});						
						$("#addFolder").css({"display":"none"});
						$("#deleteFolder").css({"display":"none"});
						$("#tree_td").css({"height":"5px"});
					}
										
					var list = result.fileList;
					var idToAppend = "files_1";
					for ( var itr = 0; itr < list.length; itr++) {
						var rec = list[itr];
						var ran = Math.floor(Math.random() * 1100);
						var ran2 = Math.floor(Math.random() * 1500);
						var delImg = $("<img/>");
						$(delImg[0]).attr( {
							"src" : "fileupload/deleteFile.png",
							"id" : ran2
						});
						$(delImg[0]).addClass(rec.fileName);
						var qryStr = "hashedName="+rec.hashedName+"&nodeText=" + rec.fileName + "&docId="
								+ rec.docId + "&imgId=" + rec.imgId
								+ "&recordId=" + rec.recId + "&tableName="
								+ result.tableName + "&fileType=" + usrFileType
								+ "&fileToUploadFileName=" + rec.fileName;

						var row = $('<tr id='
								+ itr
								+ '> '
								+ '<td>'
								+ rec.fileName
								+ '</td>'
								+ '<td>'
								+ rec.progressMsg
								+ ' File uploaded successfully. </td>'
								+ '<td >'
								+ rec.size
								+ '</td>'
								+ '<td id = '
								+ ran
								+ '>'
								+ "<img src ='${pageContext.request.contextPath}/images/delete.gif' height='10' width='10' style='cursor: pointer;' onclick = 'delRow(this,"
								+ itr + ");' />" + '</td>' + '</tr>');
						$($("#" + idToAppend)[0]).append(row[0]); 
						qryStrArray[itr] = qryStr;
						$("#displayPane").val(result.displayPane);
						usrDisplayPane = result.displayPane;
						treeTable[rec.fileName] = row;
						$('.filesSearch').append($('<option>', { value : rec.fileName }).text(rec.fileName)); 
					}
				}
			});

	var goToServerToDel = function(qryStr) {
		var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
				: $("#displayPane").val();
		try {
			var fName = qryStr.substring(
					qryStr.indexOf("fileToUploadFileName"), (qryStr.length)); 
			var nameArray = fName.split("="); 
			$
					.ajax( {
						url : "deleteUploadedFile.action?displayPane="
								+ usrDisplayPane,
						data : qryStr,//"tableName=re_property&fileToUploadFileName="+fileName+ "&fileType="+$("#fileType").val()+"&docId="+docId+"&imgId"+imgId+"&recordId"+recId,
						async : false,
						dataType : "json",
						success : function(result) {
							$("#displayPane").val(result.displayPane);
							usrDisplayPane = result.displayPane;
							removeFileFromPan(nameArray[1]);
							tree.findItem(nameArray[1], 0, 1);
							tree.deleteItem(tree.getSelectedItemId(), true); 
						},
						failure : function() {
						}
					});   
			$('.filesSearch option[value='+nameArray[1]+']').remove(); 
		} catch (e) {
		}
	};
	
	var showUploadServerFile = function(qryStr) {
		var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
				: $("#displayPane").val();
		try {
			var fName = qryStr.substring(
					qryStr.indexOf("fileToUploadFileName"), (qryStr.length)); 
			var nameArray = fName.split("=");   
			$
					.ajax({
						url : "showUploadedFile.action?displayPane="
								+ usrDisplayPane,
						data : qryStr,
						async : false,
						dataType : "html",
						success : function(result) {  
							window.open('<%=request.getContextPath()%>/fileupload/showimage.jsp?result='+result+'','_blank', 
							'width=400,height=400,scrollbars=no,left=300px,top=200px');  
						},
						error : function(result) {
							$('#errorDiv').hide().html("File not found").slideDown(1000);
							$('#errorDiv').delay(2000).slideUp(1000);
							return false;
						}
					});

		} catch (e) {
		}
	}; 
	 

	var getFileNameFromQryStr = function(stringQry) {
		var fName = stringQry.substring(stringQry
				.indexOf("fileToUploadFileName"), (stringQry.length - 1));

		var nameArray = fName.split('=');
		return nameArray[1];
	};

	$(function() { 
		
		$("#rootSpan").html(root);		
		$('.documents').live('click',function(){
			var docId=$(this).attr('id'); 
			var fileName=""; 
			if(typeof($(docId)!="undefined")){  
				fileName=$(this).html();  
				var idarray = docId.split('_'); 
				var rowid=idarray[1];  
				if(isNaN(rowid)){ 
					showTempUploadFile(rowid); 
				}
				else{ 
					showUploadServerFile(qryStrArray[rowid]);
				}
				return false;
			}  
		});
		
		var showTempUploadFile = function(fileName) {
			var tempTableName = tName == null ? userTableName : tName;
			var img = tree.getItemImage(tree.getSelectedItemId(),0, false);
			if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") == -1){
				nodeType = "dir"; 	
			} else if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") != -1){
				nodeType = "file";
			} else if (img.indexOf("folder")!= -1 ){
				nodeType = "dir";
			}
			try {
				$.ajax( {
					url : "showUploadedFile.action",
					data : "fileToUploadFileName=" + fileName + "&fileType="
							+ $("#fileType").val() + "&tableName=" + tempTableName
							+ "&nodeText=" + fileName + "&nodeType="+nodeType+"&displayPane=" + usrDisplayPane+"&recordId=" + usrRecordId,
					async : false,
					success : function(result) {
						window.open('<%=request.getContextPath()%>/fileupload/showimage.jsp?result='+result+'','_blank', 
						'width=400,height=400,scrollbars=no,left=300px,top=200px');  
					}
				});
			} catch (e) {
				alert(e);
			}
		};
		
		var initFileUpload = function(suffix) {
			$('#file_upload')
					.fileUploadUI(
							{
								namespace : 'file_upload_' + suffix,
								fileInputFilter : '#file_' + suffix,
								dropZone : $('#drop_zone_' + suffix),
								uploadTable : $('#files_' + suffix),
								downloadTable : $('#files_' + suffix),
								buildUploadRow : function(files, index) {
									
									if(!checkFileExtension(files[index].name, usrFileType)) {
										showErrorMsg("Incorrect file type, please select another file.");
										return false;
									}
									
									if (jQuery.inArray(files[index].name, CurrentlyUploadedTemplates) != -1)	{
										showErrorMsg("File already exists.");
										return false;
									} else 
										CurrentlyUploadedTemplates.push(files[index].name);
									
									return $('<tr><td width = '
											+ files[index].name.length
											+ '>'
											+ files[index].name
											+ '<\/td>'
											+ '<td class="file_upload_progress"><div></div><\/td>'
											+ '<td class="file_upload_cancel">'
											+ '<button class="ui-state-default ui-corner-all" title="Cancel">'
											+ '<span class="ui-icon ui-icon-cancel">Cancel<\/span>'
											+ '<\/button><\/td>'
											+ '<td>&nbsp;</td>' + '<\/tr>');

								},
								buildDownloadRow : function(file) { 
									try {
										
										if(!checkFileExtension(file.name, usrFileType))
											return false;
											
										$("#displayPane").val(file.displayPane);
										usrDisplayPane = file.displayPane;
										var uploadedFiles = $(
												"#" + $("#displayPane").val())
												.html();
										if (uploadedFiles != null
												&& uploadedFiles != "")
											$("#" + $("#displayPane").val())
													.html(
															uploadedFiles
																	+ " , "
																	+ file.name);
										else
											$("#" + $("#displayPane").val())
													.html(
															uploadedFiles + " "
																	+ file.name);
									} catch (exception) {
									}
									tName = file.tableName;
									$("#fileType").val(file.fileType);
									var v = file.progressMsg;									
									var randomNumber = file.name;
									if (v.indexOf("warning.png") != -1){										
										showErrorMsg("File with same name is already uploaded.");	
									}									
									else {
										var node = addFileNode(file.name);										
										tree.changeItemId(node.id, randomNumber);
										files[file.name] = tree.getItemText(tree.getParentId(node.id));
										var ran2 = Math.floor(Math.random() * 1500);
										var rowToReturn = $("<tr id = "
												+ randomNumber
												+ "><td>"
												+ file.name
												+ "<\/td><td>"
												+ file.progressMsg
												+ "</td><td>"
												+ file.size
												+ "</td><td style='text-align: center;'><img id=' " + file.name + "' src = '${pageContext.request.contextPath}/images/delete.gif'"
												+ "'height='10' width='10' style='cursor: pointer;' class = "
												+ file.name
												+ " onmoouseover = 'setPointerAsHand();' onclick = 'delRow(this,this.id); '/></td><\/tr>");
										treeTable[file.name] = rowToReturn; 
										$('.filesSearch').append($('<option>', { value : file.name }).text(file.name)); 
										return rowToReturn;
									} 
								}
							});
		};
		initFileUpload(1);
	});

	var checkFileExtension = function(fileName, fileType) {
			
		 if (fileType == "img")			
			return (fileName.indexOf(".doc") != -1);
		
		return false;
	};
	
	var deleteUploadedFile = function(ctrl) {
		var cnfrm = confirm('Selected file will be deleted');
		if(!cnfrm)	return false;
		var fileName = $(ctrl).attr("class");
		tree.findItem(fileName, 0, 1);
		$("#deleteFolder").trigger("click");
	};

	var deleteFromServer = function(fileName) {
		var tempTableName = tName == null ? userTableName : tName;
		var img = tree.getItemImage(tree.getSelectedItemId(),0, false);	 
		var nodeType = null;
		if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") == -1){
			nodeType = "dir"; 	
		} else if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") != -1){
			nodeType = "file";
		} else if (img.indexOf("folder")!= -1 ){
			nodeType = "dir";
		}	 
		try {
			$.ajax( {
				url : "deleteUploadedFile.action",
				data : "fileToUploadFileName=" + fileName + "&fileType="
						+ $("#fileType").val() + "&tableName=" + tempTableName
						+ "&nodeText=" + fileName + "&nodeType="+nodeType+"&displayPane=" + usrDisplayPane+"&recordId=" + usrRecordId,
				async : false,
				success : function() {
				
				}
			});
			$('.filesSearch option[value='+fileName+']').remove(); 
		} catch (e) {
			alert(e);
		}
	};

	var showUploadFile = function(fileName) {
		var tempTableName = tName == null ? userTableName : tName;
		var img = tree.getItemImage(tree.getSelectedItemId(),0, false);
		if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") == -1){
			nodeType = "dir"; 	
		} else if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") != -1){
			nodeType = "file";
		} else if (img.indexOf("folder")!= -1 ){
			nodeType = "dir";
		}
		try {
			$.ajax( {
				url : "showUploadedFile.action",
				data : "fileToUploadFileName=" + fileName + "&fileType="
						+ $("#fileType").val() + "&tableName=" + tempTableName
						+ "&nodeText=" + fileName + "&nodeType="+nodeType+"&displayPane=" + usrDisplayPane+"&recordId=" + usrRecordId,
				async : false,
				success : function() {
				}
			});
		} catch (e) {
			alert(e);
		}
	};
	var removeFileFromPan = function(fileName) {
		try {
			var uploadedFiles = $("#" + $("#displayPane").val()).html();
			if (uploadedFiles.indexOf(", " + fileName) != -1) {
				uploadedFiles = uploadedFiles.replace(", " + fileName, "");
			} else if (uploadedFiles.indexOf(fileName) != -1) {
				uploadedFiles = uploadedFiles.replace(fileName, "");
			}
			$("#" + $("#displayPane").val()).html(uploadedFiles);
		} catch (e) {
		}
	};

	var setPointerAsHand = function(ctrl) {
		$(ctrl).css( {
			"cursor" : "pointer"
		});
	};
 	var addFolderNode = function(folderName) {
		var d = new Date();
		var node = tree.insertNewItem(tree.getSelectedItemId(), d.valueOf(),
				folderName, 0, 0, 0, 0, 'SELECT');
		tree.setItemImage2(tree.getSelectedItemId(), 'folderClosed.gif',
				'folderOpen.gif', 'folderClosed.gif');
		dirs[folderName] = tree.getItemText(tree.getParentId(node.id));
		saveNodeFolderInSession(folderName, tree.getItemText(tree
				.getParentId(node.id)));
		tree.setItemText(node.id, tree.getItemText(node.id), "Directory");

	}; 

	var addFileNode = function(fileName) {
		var d = new Date();
		var node = tree.insertNewItem(tree.getSelectedItemId(), d.valueOf(),
				fileName, 0, 0, 0, 0);
		tree.setItemText(node.id, tree.getItemText(node.id), "File");
		return node;
	}; 

	var isFolderDialogOpen = false;

	var closeDialog = function(ctrl) {
		$(ctrl).dialog("destroy");
		$(ctrl).remove();
		isFolderDialogOpen = false;
	};
 
	$("#addFolder").bind("click", function() {
		if (isFolderDialogOpen == false) {
			isFolderDialogOpen = true;
			setParentNode();
			var folderDivObj = getFolderDiv();
			$(folderDivObj).appendTo(document);

			$(folderDivObj).dialog( {
				draggable : true,
				resizable : false,
				width : 400,
				title : "Folder Name",
				zindex : 10000,
				buttons : {
					"Cancel" : function() {
						$(this).dialog("close");
					},
					" Add Folder / Dir " : function() {
						if (addFolder()) {
							$(this).dialog("close");
						}
					}

				},
				close : function() {
					closeDialog(this);
					isFolderDialogOpen = false;
				}
			});
		}
	}); 
 
	var addFolder = function() {
		try {
			if ($("#folderName").val() == "") {
				$('#message_bar_folderName').displayMessage( {
					message : "Please provide Folder/Dir Name.",
					speed : 'slow',
					skin : 'vista',
					background : '#FF0000',
					color : '#FFFFFF',
					autohide : true
				});
				return false;
			} else {
				var originalItemId = tree.getSelectedItemId();	
																								
				if (findTreeNode(rootNodeLabel,$("#folderName").val()) == false){					
					tree.selectItem(originalItemId,true,false);
					addFolderNode($("#folderName").val());
					$("#folderName").focus();						
					return true;
					
				} else {
					tree.selectItem(originalItemId,true,false);
					showFolderError("Folder/Directory with same name exists already");
					return false;
				}
			}

		} catch (e) {
			alert(e);
			return false;
		}
	}; 

 	var findTreeNode = function (rootText, itemText){
		var rootIndex =  tree.findItemIdByLabel(rootText, 0, 1);
		
		var subItems = ""+tree.getAllSubItems(rootIndex);
		if (subItems.length > 0){
			return iterateOverNode(subItems,itemText);
		} else {
			return false;
		}
	}; 

 	var iterateOverNode = function (subItem,itemText){			
		var itemArray = subItem.split(",");
		//alert("ItemArray; After SPLIT : "+itemArray + " : ArrayItem.length" + itemArray.length) ;
		var isNode = false;		
		for (var itr =0; itr<itemArray.length; itr++){

			//alert("debug ; itr : itemTwext : "+tree.getItemText(itemArray[itr])+  " newFolderName : " + itemText);
			if (tree.getItemText(itemArray[itr]) == itemText){
				isNode =  true;
				return true;
			}

			//alert("debug: itr : " + tree.getItemText(itemArray[itr]));			
			var subs = tree.getAllSubItems(itemArray[itr]);
			if  (subs != null && subs != "" ){
				isNode = iterateOverNode(""+subs,itemText);
			}	
		}
		return isNode;
	}; 

	var showFolderError = function (msg){
		$('#message_bar_folderName').displayMessage( {
			message : msg,
			speed : 'slow',
			skin : 'vista',
			background : '#FF0000',
			color : '#FFFFFF',
			autohide : true
		});
	};

	$("#deleteFolder").bind(
			"click",
			function() {

				if (tree.getSelectedItemId() != "" && rootNodeLabel != tree.getItemText(tree.getSelectedItemId())) {

					delTreeNode();
					var initialImg = tree.getItemImage(
							tree.getSelectedItemId(), 0, false);

					$("#" + tree.getIndexById(tree.getSelectedItemId()))
							.remove();
					var fileName = tree.getItemText(tree.getSelectedItemId());
					var rowCtrl = treeTable[fileName];
					removeChildFiles();
					$(rowCtrl).remove();

					deleteFromServer(fileName); 
					removeFileFromPan(fileName);
					tree.deleteItem(tree.getSelectedItemId(), true);
					var afterImg = tree.getItemImage(tree.getSelectedItemId(),
							0, false);
					if ((initialImg != afterImg)
							&& (initialImg.indexOf("folder") != -1)) {
						tree.setItemImage2(tree.getSelectedItemId(),
								'folderClosed.gif', 'folderOpen.gif',
								'folderClosed.gif');
					}
				} else {
					showErrorMsg("Pleased select any folder/directory or other than root node.");
				}
			});

	var removeChildFiles = function() {
		var subItems = "" + tree.getAllSubItems(tree.getSelectedItemId());
		//alert("Sub ITems List: " + subItems);
		if (subItems != null && subItems != "") {
			var fileNames = subItems.split(",");
			for (itr = 0; itr < fileNames.length; itr++) {
				var nodeImg = tree.getItemImage(tree.getSelectedItemId(), 0,
						false);
				if (nodeImg.indexOf("folder") != -1) {
					tree.selectItem(fileNames[itr], true, false);
					removeChildFiles();
					tree.deleteItem(fileNames[itr], true);
				}
				var fName = tree.getItemText(fileNames[itr]) == 0 ? fileNames[itr]
						: tree.getItemText(fileNames[itr]);
				deleteFromServer(fName);
				removeFileFromPan(fName);
				var rowCtrl = treeTable[fName];
				$(rowCtrl).remove();

			}
		}
	}; 

	$("#uploadFile").bind(
			"click",
			function() {
				if (tree.getSelectedItemId() != ""
						|| tree.getSelectedItemId() != null) {
					setParentNode();
					$("#file_1").trigger("click");
				} else {
					showErrorMsg("Pleased select any folder/directory.");
				}
			});

	var showErrorMsg = function(msg) {
		$('#message_bar').displayMessage( {
			message : msg,
			speed : 'slow',
			skin : 'vista',
			background : '#FF0000',
			color : '#FFFFFF',
			autohide : true
		});
	};

 	var delTreeNode = function() {
		for ( var key in files) {
			if (files[key] == tree.getItemText(tree.getSelectedItemId())) {
				delete files[key];
			}
		}
		delete dirs[tree.getItemText(tree.getSelectedItemId())];
	}; 

	$("#addFolder").bind("mouseover", function(e) {
		$("#addFolder").css( {
			"cursor" : "pointer"
		});
	});

	$("#deleteFolder").bind("mouseover", function(e) {
		$("#deleteFolder").css( {
			"cursor" : "pointer"
		});
	});

	$("#uploadFile").bind("mouseover", function(e) {
		$("#uploadFile").css( {
			"cursor" : "pointer"
		});
	});

	var saveNodeFolderInSession = function(nodeText, parentNodeText) {
		$.ajax( {
			type : "POST",
			url : "saveInSessionNodeFile.action?fileType=" + usrFileType
					+ "&recordId=" + usrRecordId + "&displayPane="
					+ usrDisplayPane + "&tableName=" + userTableName
					+ "&nodeText=" + nodeText + "&parentNodeText="
					+ parentNodeText,
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
			}
		});
	};

	var setParentNode = function() {
		var selectedId = tree.getSelectedItemId();
		var img = tree.getItemImage(tree.getSelectedItemId(), 0, false);
		if (img != null && img.indexOf("folder") != -1) {
			$("#parentNodeText")
					.val(tree.getItemText(tree.getSelectedItemId()));
		} else if (img != null && img.indexOf("leaf") != -1) {
			if (rootNodeLabel == tree.getItemText(tree.getSelectedItemId())) {
				$("#parentNodeText").val(
						tree.getItemText(tree.getSelectedItemId()));
			} else if (rootNodeLabel != tree.getItemText(tree
					.getSelectedItemId())
					&& tree.getItemText(tree.getSelectedItemId()).indexOf(".") != -1) {
				$("#parentNodeText").val(
						tree.getItemText(tree.getParentId(tree
								.getSelectedItemId())));
				tree.selectItem(tree.getParentId(tree.getSelectedItemId()),
						false, false);
			} else {
				$("#parentNodeText").val(
						tree.getItemText(tree.getSelectedItemId()));
			}
		}

	}; 

	var doWhenEnter = function(evt) {
		evt = (evt) ? evt : ((window.event) ? window.event : "");
		if (evt) {
			if (evt.keyCode == 13 || evt.which == 13) { // Enter Key Capture
				addFolder();
			}
		}
	};

	var getFolderDiv = function() {
		return $('<div id="folderNameDiv" style="display: none">'
				+ '<div>&nbsp;</div> '
				+ '<div id="message_bar_folderName"></div>'
				+ ' Folder/Dir Name: <input type="text" name="folderName" id="folderName" />'
				+ '</div>');
	};
</script>

<div id="message_bar"></div>

<style>
	.ui-button { margin-left: -1px; }
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:85%}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	/* button{
		height:18px;
		position: absolute!important;
		float: right!important;
		margin:4px 0 0 -36px!important;
	}*/
	.ui-autocomplete{
		width:194px!important;
	}
	td {
		padding-left: 2px;
		padding-right: 2px;
	}
</style>