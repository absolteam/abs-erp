<%@ page language="java" import="java.io.OutputStream" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<body>
<div> 
 <c:choose>
	<c:when test="${param.result eq 'Image'}"> 
		<img src="data:image/gif;base64,${sessionScope.DecodedBytes}">  
	</c:when>
	<c:otherwise>
		<% 
			OutputStream outStream =null;
			try{
			   byte[] bytes= (byte[])session.getAttribute("DecodedBytes"); 
			   response.setContentType("application/pdf");  
		       response.setContentLength(bytes.length);  
		       outStream = response.getOutputStream();  
		       outStream.write(bytes, 0, bytes.length);   
		       outStream.flush(); 
			}catch(Exception e){e.printStackTrace();}  
		%>
	</c:otherwise>
</c:choose>  --
</div>
</body>
</html>