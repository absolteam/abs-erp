
var usrFileType = null;
var usrRecordId = null;
var userTableName = null;
var usrDisplayPane = null;
var root = null;
var uploaderDialog = null;
var width = null;
var height = null;
AIOS_Uploader = {
		loadDialogHTML : function (fileType,displayPane,refTable,recId,rootLabel) {			
			usrFileType = fileType;
			usrRecordId = recId;
			userTableName = refTable;	
			usrDisplayPane = displayPane;			
			root = rootLabel;			
			var uploadDivObj = $("<div/>");	
			$(uploadDivObj[0]).attr({"id":"fileUploadDiv"});
			$(uploadDivObj[0]).css({"display":"none"});
			$('body').append(uploadDivObj[0]);	
			if(rootLabel == "Templates") {
				uploaderDialog = 'fileupload/TemplateUploaderDialog.jsp';
				width = 500;
				height = 340;
			}
			else {
				uploaderDialog = 'fileupload/FileUploaderDialog.jsp';
				width = 1000;
				height = 500;
			}
			$("#fileUploadDiv").load(uploaderDialog, 
				function (response,status, httpXMLRequest){							
				$("#fileType").val(fileType);				
				$("#displayPane").val(displayPane);
				$("#recordId").val(usrRecordId);
				$("#tableName").val(userTableName);				
				$("#fileUploadDiv").dialog(
						{
							autoOpe: false,		
							draggable: true,
							resizable: false,
							modal: true,
							width: width,
							height: height,
							show: "blind",
							hide: "fold",
							title: "Upload File",
							buttons: 
							{
								"Close": function() {	
									$(this).dialog("close");
									$(this).dialog("destroy");
									//$("#fileUploadDiv").remove();	
									//Added by rafiq for release use case certificate check 
									checkboxCheckEven();									
								}							
							},
							close : function() {
								if(rootLabel == "Templates")
									$(".print_templates").trigger('click');
								$(this).dialog("destroy");
								$("#fileUploadDiv").remove();								
							}						
						}
					);				
										
				}
			);					
		},		
		openFileUploader : function (fileType,displayPane,refTable,recId,rootLabel){			
			AIOS_Uploader.loadDialogHTML(fileType,displayPane,refTable,recId, rootLabel);
		}
	};

function startCallback() {
    // make something useful before submit (onStart)
    return true;
}

function completeCallback(response) {    
    $("#ajaxLoaderImg").replaceWith("<span>"+response+"</span>");
}
function populateUploadsPane(usrFileType, usrDisplayPane, userTableName, usrRecordId){
	 $.ajax({
		    type : "POST",
		    url : "populateUploadedFile.action?fileType=" + usrFileType + "&recordId=" + usrRecordId + "&displayPane="
		    	+ usrDisplayPane + "&tableName=" + userTableName,
		    async : false,
		    dataType : "json",
		    cache : false,
		    success : function(result) {

			    var list = result.fileList;
			    var idToAppend = "files_001_"+usrFileType;
		    	$("#" + usrDisplayPane).html('<table id="'+idToAppend+'" cellpadding="5" cellspacing="5" class="" style="max-height: 357px; padding: 3px; width: 100%;"></table>');
		    	var firstrow= "<tr style='border: 1px solid black; background-color: #EEEEEE;'"
			    +"height='30' class='tableHeader' id='uploaderTableHeader'>"
				+"<td width='50%'>File Name&nbsp;&nbsp;&nbsp;&nbsp;</td>"
				+"<td width='10%'>Size(KB)</td><td width='25%'>Action(s)</td></tr>";
		    	$($("#" + idToAppend)[0]).append(firstrow); 
			    for ( var itr = 0; itr < list.length; itr++) {
				    var rec = list[itr];
				    var qryStr = "hashedName="+rec.hashedName+"&nodeText=" + rec.fileName + "&docId="
					+ rec.docId + "&imgId=" + rec.imgId
					+ "&recordId=" + usrRecordId + "&tableName="
					+ result.tableName + "&fileType=" + usrFileType
					+ "&fileToUploadFileName=" + rec.fileName
				    + "&usrDisplayPane=" + usrDisplayPane;
			    	//var strl="<img src ='./images/download.png' title='Download File' onclick = 'downloadRow("+itr+")'>";
				    //$("#" + usrDisplayPane).append(rec.fileName + " "+strl+"<br/>");
				   
			    	var row = $('<tr id=useCaseDocument'
							+ itr
							+ '> '
							+ '<td id='+itr+'_fNameListUsecase>'
							+ rec.fileName
							+ '</td>'
							+ '<td >'
							+ rec.size
							+ '</td>'
							+ '<td >'
							+ "<input type='hidden' id='useCase_"+usrDisplayPane+'_'+itr+"' value='"+qryStr+"'/>"
							+ "<img class='document-action-image' src ='./images/view.png' title='View Document/Image' onclick = 'viewRow(\""+ usrDisplayPane+"_"+itr + "\");' />" 
							+ "<img class='document-action-image' src ='./images/download.png' title='Download File' onclick = 'downloadRow(\""+ usrDisplayPane+"_"+itr + "\");' />"
							+ "<img class='document-action-image' src ='./images/edit.png' title='Edit File Name' onclick = 'editRow(\""+ usrDisplayPane+"_"+itr + "\");' />"
							+ "<img class='document-action-image' src ='fileupload/deleteFile.png' title='Delete File' onclick = 'delRow(\""+ usrDisplayPane+"_"+itr + "\");' />"
							+ '</td>' + '</tr>');
					
					$($("#" + idToAppend)[0]).append(row[0]); 
				}

		    }
	});
};
var downloadRow = function(usrDisplayPane) {
	var queryString=$('#useCase_'+usrDisplayPane).val();
	window.open('showUploadedFile.action?' 
			+  queryString,
			'_blank', 
			'width=400,height=400,scrollbars=no,left=300px,top=200px');  
	 return false;
};

var viewRow = function(usrDisplayPane) {
	var queryString=$('#useCase_'+usrDisplayPane).val();
	window.open('viewUploadedFile.action?'+queryString,
			'_blank', 
			'width=900,height=600,scrollbars=no,left=50px,top=50px');  
	
	 return false;
};

var delRow = function(usrDisplayPane) {
	var queryString=$('#useCase_'+usrDisplayPane).val();
	var cnfrm = confirm('Selected file will be permanently deleted');
	if(!cnfrm)
		return false;
	goToServerToDel(queryString);
};

var goToServerToDel = function(qryStr) {
	var fileType=getParameterByName("fileType",qryStr);
	var displayPane=getParameterByName("usrDisplayPane",qryStr);
	var tableName=getParameterByName("tableName",qryStr);
	var recordId=getParameterByName("recordId",qryStr);
	var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
			: displayPane;
	try {
		var fName = qryStr.substring(
				qryStr.indexOf("fileToUploadFileName"), (qryStr.length)); 
		var nameArray = fName.split("="); 
		//alert(qryStr);
		$
				.ajax( {
					url : "deleteUploadedFile.action?displayPane="
							+ usrDisplayPane,
					data : qryStr,//"tableName=re_property&fileToUploadFileName="+fileName+ "&fileType="+$("#fileType").val()+"&docId="+docId+"&imgId"+imgId+"&recordId"+recId,
					async : false,
					dataType : "json",
					success : function(result) {
						//alert(result.displayPane);
						populateUploadsPane(fileType,displayPane,tableName,recordId);
					},
					error: function() {
						//alert("err");
					},
					failure : function() {
					}
				});   
	} catch (e) {
	}
};

var editRow = function(usrDisplayPane) {
	var queryString=$('#useCase_'+usrDisplayPane).val();
	var valueArry=new Array();
	valueArry=queryString.split("_");
	  var fileName = prompt("Please enter file name", "");
	    if (fileName != null && fileName!="") {
	    	goToServerToEdit(queryString,valueArry[1],fileName);
	    }else{
	    	alert("Sorry can not edit file name!!");
	    }
	
};

var goToServerToEdit = function(qryStr,index,fileName) {
	var fileHash=getParameterByName("hashedName",qryStr);
	var oldFileName=getParameterByName("nodeText",qryStr);	
	var fileType=getParameterByName("fileType",qryStr);
	var displayPane=getParameterByName("usrDisplayPane",qryStr);
	var tableName=getParameterByName("tableName",qryStr);
	var recordId=getParameterByName("recordId",qryStr);
	var ext = oldFileName.substr(oldFileName.lastIndexOf('.') + 1);
	fileName=fileName+"."+ext;
	$.ajax( {
		url : "renameDocumentOrImageFile.action",
		data : "fileToUploadFileName="+fileName+ "&fileType="+fileType+"&fileHash="+fileHash,
		async : false,
		dataType : "json",
		success : function(result) {
			populateUploadsPane(fileType,displayPane,tableName,recordId);
		},
		error: function() {
			//alert("err");
		},
		failure : function() {
		}
	});   
	
	
};

function getParameterByName(name,params) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec("?"+params);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
