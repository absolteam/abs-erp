<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/msgbar/skins/vista/skin.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/dhtmlTree/dhtmlxtree.css" />
<script type="text/javascript" src="msgbar/jquery.messagebar.js"></script>
<script type="text/javascript" src="dhtmlTree/dhtmlxcommon.js"></script>
<script type="text/javascript" src="dhtmlTree/dhtmlxtree.js"></script> 
<script type="text/javascript" src="dhtmlTree/ext/dhtmlxtree_json.js"></script>
<style>

</style>
<div id="uploaderDiv" class="" style="height: 362px;"> 
	<div id="errorDiv" class="response-msg error ui-corner-all width70" style="display:none;"></div> 
	<form id="file_upload" action="uploadFile.action" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="fileType" id="fileType"></input> 
		<input type="hidden" name="displayPane" id="displayPane"></input> 
		<input type="hidden" name="mode" id="mode"></input> 
		<input type="hidden" name="recordId" id="recordId"></input> 
		<input type="hidden" name="tableName" id="tableName"></input> 
		<input type="hidden" name="parentNodeText" id="parentNodeText" /> 
		<input id="file_1" type="file" multiple="" name="fileToUpload" style="visibility: hidden;">	 
		 <div id="radioAction">
			<input type="radio" class="radioAction" id="viewRowButton" name="radioAction"><label for="viewRowButton">View</label>
			<input type="radio" class="radioAction" id="downloadRowButton" name="radioAction"><label for="downloadRowButton">Download</label>
			<input type="radio" class="radioAction" id="editRowButton" name="radioAction"><label for="editRowButton">Edit</label>
			<input type="radio" class="radioAction" id="deleteRowButton" name="radioAction"><label for="deleteRowButton">Delete</label>
		</div>
		<button>Upload</button> 
		<div style="height: 358px; overflow: auto;" class="round-border width70 float-left">
			<table id="files_1" cellpadding="5" cellspacing="5" class="" 
					style="max-height: 357px; padding: 8px; width: 100%;">
				<tr>
					<td colspan="4" style="height: 190px;" id="tree_td">	
						<div style="text-align: left; margin-top: -5px; margin-bottom: 2px;" id = "toolBar">
							<img src="${pageContext.request.contextPath}/dhtmlTree/imgs/addFolder.png"
								id="addFolder" height="25" width="100" />
							<img src="${pageContext.request.contextPath}/dhtmlTree/imgs/deleteFolder.png"
								id="deleteFolder" height="25" width="100" /> 
							<img src="${pageContext.request.contextPath}/dhtmlTree/imgs/uploadFile.png" 
								id="uploadFile" height="25" width="100" />
							<%-- <img src="${pageContext.request.contextPath}/dhtmlTree/imgs/addFolder.png"
								id="renameFolder" height="25" width="100" />  --%>
						</div>
						<div id="treeboxbox_tree" style="width: 100%; height: 160px; background-color: #f5f5f5; 
								border: 1px solid Silver; overflow: auto;">
						</div> 
					</td>
				</tr>
				<tr style="border: 1px solid black; background-color: #EEEEEE;"
					height="30" class="tableHeader" id="uploaderTableHeader">
					<td width="45%">File Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width="25%">Progress</td>
					<td width="10%">Size(KB)</td>
					<td width="20%">Action(s)</td>
				</tr>	
			</table>
		</div> 
		<!-- <div>
			<object data="test.pdf" type="application/pdf" width="300" height="200">
			alt : <a href="test.pdf">test.pdf</a>
			</object>
		</div> -->
	</form>
	
	<div class="float-right round-border" style="width: 27% !important; 
			margin-top: -28px; height: 348px; padding: 5px; overflow: auto;">
		<div class="width100 float-left" style="position: relative;">
			<select name="filesSearch" class="filesSearch"> 
				<option value=""></option>   
			</select>
			<span style="margin-left: 10px; cursor: pointer; position: absolute; margin-top: 6px; right: 35px;" class="float-left">
				<img width="24" height="24" src="images/icons/Glass.png">
			</span>
		</div> 
		<div class="float-left" id="selectedValues" style="padding:5px; margin-top:33px; 
				position: absolute; height: 306px; width: 254px; overflow: auto; overflow-x: hidden;"></div> 
	</div>
</div>

<script>
	$( "#radioAction" ).hide();
	var rootNodeLabel = null;
	$("#testDB").bind("click", function() {
		$.ajax( {
			type : "POST",
			url : "saveDirsNDocsFile.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
			}
		});
	});

	var tName = null;

	var dirs = new Array();
	var files = new Array();
	var treeTable = new Array();
	loadTree();
	/* tree = new dhtmlXTreeObject("treeboxbox_tree", "100%", "100%", 0);
	tree.setSkin('dhx_skyblue');
	tree.setImagePath("dhtmlTree/imgs/");
	tree.setXMLAutoLoading("getTreeFile.action");
	tree.setDataMode("json");
	tree.loadJSON("getTreeFile.action?fileType=" + usrFileType + "&recordId="
			+ usrRecordId + "&displayPane=" + usrDisplayPane + "&tableName="
			+ userTableName + "&rootLabel=" + root); */
	rootNodeLabel = root;
	var qryStrArray = new Array();
	var delRow = function(ctrl, index) {
		$( "#radioAction" ).hide();
		var cnfrm = confirm('Selected file will be permanently deleted');
		if(!cnfrm)
			return false;
		$($($(ctrl).parents()[0]).parents()[0]).remove();
		goToServerToDel(qryStrArray[index]);
		$('#selectedValues').html(""); 
		$('.ui-autocomplete-input').val("");
	};
	
	var editRow = function(ctrl, index) {
		  var fileName = prompt("Please enter file name", "");
		    if (fileName != null && fileName!="") {
		    	goToServerToEdit(qryStrArray[index],index,fileName);
		    }else{
		    	alert("Sorry can not edit file name!!");
		    }
		
	};
	
	var downloadRow = function(ctrl, index) {
		$( "#radioAction" ).hide();
		var queryString=qryStrArray[index];
		window.open('<%=request.getContextPath()%>/showUploadedFile.action?' 
				+  queryString,
				'_blank', 
				'width=400,height=400,scrollbars=no,left=300px,top=200px');  
	};
	var viewRow = function(ctrl, index) {
		$( "#radioAction" ).hide();
		var queryString=qryStrArray[index];
		window.open('viewUploadedFile.action?'+queryString,
				'_blank', 
				'width=800,height=600,scrollbars=no,left=100px,top=100px');  
		
		 return false;
	};
	var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
			: $("#displayPane").val();
	//Initial Load of document list
	loadUploadedList(usrFileType,usrRecordId,usrDisplayPane,userTableName,root);

	var goToServerToDel = function(qryStr) {
		var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
				: $("#displayPane").val();
		
		try {
			var fName = qryStr.substring(
					qryStr.indexOf("fileToUploadFileName"), (qryStr.length)); 
			var nameArray = fName.split("="); 
			//alert(qryStr);
			$
					.ajax( {
						url : "deleteUploadedFile.action?displayPane="
								+ usrDisplayPane,
						data : qryStr,//"tableName=re_property&fileToUploadFileName="+fileName+ "&fileType="+$("#fileType").val()+"&docId="+docId+"&imgId"+imgId+"&recordId"+recId,
						async : false,
						dataType : "json",
						success : function(result) {
							//alert(result.displayPane);
							populateUploadsPane($("#fileType").val(),$("#displayPane").val(),$("#tableName").val(),$("#recordId").val());
							loadUploadedList($("#fileType").val(),$("#recordId").val(),$("#displayPane").val(),$("#tableName").val(),root);
							loadTree();
						},
						error: function() {
							//alert("err");
						},
						failure : function() {
						}
					});   
			$('.filesSearch option[value='+nameArray[1]+']').remove(); 
		} catch (e) {
		}
	};
	
	var goToServerToEdit = function(qryStr,index,fileName) {
		var fileHash=getParameterByName("hashedName",qryStr);
		var oldFileName=getParameterByName("nodeText",qryStr);	
		var fileType=getParameterByName("fileType",qryStr);
		var ext = oldFileName.substr(oldFileName.lastIndexOf('.') + 1);
		fileName=fileName+"."+ext;
		$.ajax( {
			url : "renameDocumentOrImageFile.action",
			data : "fileToUploadFileName="+fileName+ "&fileType="+fileType+"&fileHash="+fileHash,
			async : false,
			dataType : "json",
			success : function(result) {
				$("#"+index+"_fNameList").text(fileName);
				populateUploadsPane($("#fileType").val(),$("#displayPane").val(),$("#tableName").val(),$("#recordId").val());
				loadUploadedList($("#fileType").val(),$("#recordId").val(),$("#displayPane").val(),$("#tableName").val(),root);
				loadTree();
			},
			error: function() {
				//alert("err");
			},
			failure : function() {
			}
		});   
		
		
	};
	
	
	var showUploadServerFile = function(qryStr) {
		var pane = usrDisplayPane != null && usrDisplayPane != "" ? usrDisplayPane
				: $("#displayPane").val();
		try {
			var fName = qryStr.substring(
					qryStr.indexOf("fileToUploadFileName"), (qryStr.length)); 
			var nameArray = fName.split("=");   
			
			
			window.open('<%=request.getContextPath()%>/showUploadedFile.action?displayPane='
					+ usrDisplayPane + '&' + qryStr, '_blank', 
					'width=400,height=400,scrollbars=no,left=300px,top=200px');  

		} catch (e) {
			//alert(e);
			//$('#errorDiv').hide().html("File not found").slideDown(1000);
			//$('#errorDiv').delay(2000).slideUp(1000);
			return false;
		}
		return true;
	}; 
	 

	var getFileNameFromQryStr = function(stringQry) {
		var fName = stringQry.substring(stringQry
				.indexOf("fileToUploadFileName"), (stringQry.length - 1));

		var nameArray = fName.split('=');
		return nameArray[1];
	};

	$(function() {
		$('.filesSearch').combobox({ 
		       selected: function(event, ui){ 
		    	   //alert($(this).val());
		    	   $('.ui-autocomplete-input').val($(this).val());
		    	   $('.ui-autocomplete-input').trigger('change');
		    	   return false;
		   } 
		}); 
		$("#rootSpan").html(root);
		$('.ui-autocomplete-input').change(function(){
			 var slidetab=$('.ui-menu');
			 var liSize=$(slidetab).children().size();
			 var listValue=""; var finalValue=""; var macthWords="";
			 for (var i=0; i<liSize; i++) {
				 listValue= $($($($($($($(slidetab).children()).get(i)).children()).get(0)).children()).get(0)).html();   
			 } 
			 $('.filesSearch option').each(function(){ 
				 var contactLike=$(this).text();  
				 var matchPosition =  contactLike.search(listValue);   
				 if(matchPosition!=-1){  
					 macthWords=macthWords+","+contactLike;
				 }  
			 });   
			var listArray= new Array();
			listArray=macthWords.split(',');
			$('#selectedValues').html(""); 
			for(var j=1;j<listArray.length;j++){ 
				var tempfileId = -1;
				var filename=listArray[j];  
				$('#files_1 tr').each(function(){ 
					var tempfile=$($($(this).children()).get(0)).text();
					if(filename == tempfile){ 
						 tempfileId = $(this).attr('id');
					} 
				});
				//alert("tempfileId: " + tempfileId);
				if(tempfileId != -1) {
					$('#selectedValues').append('<span id=docId_' 
							+ tempfileId + ' ' 
							+ 'class=\'width100 documents float-left\'style=\'position:relative;padding:3px;cursor:pointer;\'>'
							+ listArray[j] 
							+ '</span>');
				}
			}
			$('.ui-autocomplete-input').val("");
			$('.ui-menu').css('z-index','0');
		});
		
		$('.documents').live('click',function(){
			var docId=$(this).attr('id');
			var docname=$(this).html();
			var fileName=""; 
			if(typeof($(docId)!="undefined")){  
				fileName=$(this).html(); 				
				var idarray = docId.split('_'); 
				var rowid=idarray[1];
					
				if(isNaN(rowid)) {
					if((fileName.indexOf(" ") != -1 && fileName.indexOf(rowid) != -1)
							|| (fileName.indexOf("_") != -1 && fileName.indexOf(rowid) != -1)) {
						showTempUploadFile(fileName);
					} else {
						showTempUploadFile(rowid);
					}
				}
				else { 					
					showUploadServerFile(qryStrArray[rowid]);						
				}
				return false;
			}  
		});
		
		var showTempUploadFile = function(fileName) {
			var tempTableName = tName == null ? userTableName : tName;
			var img = tree.getItemImage(tree.getSelectedItemId(),0, false);
			if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") == -1){
				nodeType = "dir"; 	
			} else if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") != -1){
				nodeType = "file";
			} else if (img.indexOf("folder")!= -1 ){
				nodeType = "dir";
			}
			try {
				
				window.open('<%=request.getContextPath()%>/showUploadedFile.action?' 
						+ "fileToUploadFileName=" + fileName + "&fileType="
						+ $("#fileType").val() + "&tableName=" + tempTableName
						+ "&nodeText=" + fileName + "&nodeType="+nodeType+"&displayPane=" + usrDisplayPane+"&recordId=" + usrRecordId,
						'_blank', 
						'width=400,height=400,scrollbars=no,left=300px,top=200px');  
								
			} catch (e) {
				//alert(e);
				$('#errorDiv').hide().html("File not found").slideDown(1000);
				$('#errorDiv').delay(2000).slideUp(1000);
				return false;
			}
		};
		
		var initFileUpload = function(suffix) {
			$('#file_upload')
					.fileUploadUI(
							{
								namespace : 'file_upload_' + suffix,
								fileInputFilter : '#file_' + suffix,
								dropZone : $('#drop_zone_' + suffix),
								uploadTable : $('#files_' + suffix),
								downloadTable : $('#files_' + suffix),
								buildUploadRow : function(files, index) {
									
									if(!checkFileNameForSymbols(files[index].name, usrFileType)) {
										showErrorMsg("Filename must NOT contain & or # symbol.");
										return false;
									}	
									
									if(!checkFileExtension(files[index].name, usrFileType)) {
										showErrorMsg("Incorrect file type, please select another file.");
										return false;
									}										
										
									return $('<tr><td width = '
											+ files[index].name.length
											+ '>'
											+ files[index].name
											+ '<\/td>'
											+ '<td class="file_upload_progress"><div></div><\/td>'
											+ '<td class="file_upload_cancel">'
											+ '<button class="ui-state-default ui-corner-all" title="Cancel">'
											+ '<span class="ui-icon ui-icon-cancel">Cancel<\/span>'
											+ '<\/button><\/td>'
											+ '<td>&nbsp;</TD>' + '<\/tr>');

								},
								buildDownloadRow : function(file) { 
									try {
										if(!checkFileNameForSymbols(file.name, usrFileType))
											return false;
										
										if(!checkFileExtension(file.name, usrFileType))
											return false;
											
										$("#displayPane").val(file.displayPane);
										usrDisplayPane = file.displayPane;
										var uploadedFiles = $(
												"#" + $("#displayPane").val())
												.html();
										if (uploadedFiles != null
												&& uploadedFiles != "")
											$("#" + $("#displayPane").val())
													.html(
															uploadedFiles
																	+ " , "
																	+ file.name);
										else
											$("#" + $("#displayPane").val())
													.html(
															uploadedFiles + " "
																	+ file.name);
									} catch (exception) {
									}
									tName = file.tableName;
									$("#fileType").val(file.fileType);
									var v = file.progressMsg;									
									var randomNumber = file.name;//tree.getIndexById(node.id);//tree.getSelectedItemId();
									if (v.indexOf("warning.png") != -1){										
										showErrorMsg("File (" + file.name + ") with same name is already uploaded.");	
									}									
									else {
										var node = addFileNode(file.name);										
										tree.changeItemId(node.id, randomNumber);
										files[file.name] = tree.getItemText(tree.getParentId(node.id));
										var ran2 = Math.floor(Math.random() * 1500);
										var rowToReturn = $("<tr id = "
												+ randomNumber
												+ "><td>"
												+ file.name
												+ "<\/td><td>"
												+ file.progressMsg
												+ "</td><TD>"
												+ file.size
												+ "</TD><td><img src = 'fileupload/deleteFile.png' title='Delete File' class = "
												+ file.name
												+ " onmoouseover = 'setPointerAsHand();' onclick = 'deleteUploadedFile(this); '/></td><\/tr>");
										treeTable[file.name] = rowToReturn; 
										$('.filesSearch').append($('<option>', { value : file.name }).text(file.name)); 
										return rowToReturn;
									} 
								}
							});
		};
		initFileUpload(1);
	});

	var checkFileNameForSymbols = function(fileName, fileType) {
		
		fileName = fileName.toLowerCase();
		if(fileName.indexOf("#") == -1 && fileName.indexOf("&") == -1)
			return true;
		else
			return false;
	};
	
	var checkFileExtension = function(fileName, fileType) {
		fileName=fileName.toLowerCase();
		if(fileType == "doc")
			
			return ((fileName.indexOf(".doc") != -1) || (fileName.indexOf(".docx") != -1) || (fileName.indexOf(".pdf") != -1)
					|| (fileName.indexOf(".xps") != -1) || (fileName.indexOf(".rtf") != -1) || (fileName.indexOf(".dot") != -1)
					|| (fileName.indexOf(".dotx") != -1) || (fileName.indexOf(".xls") != -1) || (fileName.indexOf(".xlsx") != -1)
					|| (fileName.indexOf(".jpg") != -1) || (fileName.indexOf(".jpeg") != -1) || (fileName.indexOf(".gif") != -1) 
					|| (fileName.indexOf(".png") != -1) || (fileName.indexOf(".bmp") != -1)  
					|| (fileName.indexOf(".txt") != -1) || (fileName.indexOf(".csv") != -1));
			
		 else if (fileType == "img")
			
			return ((fileName.indexOf(".jpg") != -1) || (fileName.indexOf(".jpeg") != -1) || (fileName.indexOf(".gif") != -1) 
					|| (fileName.indexOf(".png") != -1) || (fileName.indexOf(".bmp") != -1) || (fileName.indexOf(".jpe") != -1)
					|| (fileName.indexOf(".jfif") != -1) || (fileName.indexOf(".tif") != -1) || (fileName.indexOf(".tiff") != -1));
		
		return false;
	};
	
	var deleteUploadedFile = function(ctrl) {
		//var cnfrm = confirm('Selected file will be permanently deleted');
	//	if(!cnfrm)	return false;
		var fileName = $(ctrl).attr("class");
		tree.findItem(fileName, 0, 1);
		$("#deleteFolder").trigger("click");
		$('#selectedValues').html(""); 
		$('.ui-autocomplete-input').val("");
	};

	var deleteFromServer = function(fileName) {
		var tempTableName = tName == null ? userTableName : tName;
		var img = tree.getItemImage(tree.getSelectedItemId(),0, false);	 
		var nodeType = null;
		if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") == -1){
			nodeType = "dir"; 	
		} else if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") != -1){
			nodeType = "file";
		} else if (img.indexOf("folder")!= -1 ){
			nodeType = "dir";
		}	 
		try {
			$.ajax( {
				url : "deleteUploadedFile.action",
				data : "fileToUploadFileName=" + fileName + "&fileType="
						+ $("#fileType").val() + "&tableName=" + tempTableName
						+ "&nodeText=" + fileName + "&nodeType="+nodeType+"&displayPane=" + usrDisplayPane+"&recordId=" + usrRecordId,
				async : false,
				success : function() {
				
				}
			});
			$('.filesSearch option[value='+fileName+']').remove(); 
		} catch (e) {
			alert(e);
		}
	};

	var showUploadFile = function(fileName) {
		var tempTableName = tName == null ? userTableName : tName;
		var img = tree.getItemImage(tree.getSelectedItemId(),0, false);
		if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") == -1){
			nodeType = "dir"; 	
		} else if (img.indexOf("leaf")!= -1  && fileName.indexOf(".") != -1){
			nodeType = "file";
		} else if (img.indexOf("folder")!= -1 ){
			nodeType = "dir";
		}
		try {
						
			window.open('<%=request.getContextPath()%>/showUploadedFile.action?'
					+ "fileToUploadFileName=" + fileName + "&fileType="
					+ $("#fileType").val() + "&tableName=" + tempTableName
					+ "&nodeText=" + fileName + "&nodeType="+nodeType+"&displayPane=" + usrDisplayPane+"&recordId=" + usrRecordId,
					'_blank', 
					'width=400,height=400,scrollbars=no,left=300px,top=200px'); 
		} catch (e) {
			alert(e);
		}
	};
	var removeFileFromPan = function(fileName) {
		try {
			var uploadedFiles = $("#" + $("#displayPane").val()).html();
			//alert(uploadedFiles);
			if (uploadedFiles.indexOf(", " + fileName) != -1) {
				uploadedFiles = uploadedFiles.replace(", " + fileName, "");
			} else if (uploadedFiles.indexOf(fileName + "</br>") != -1) {
				uploadedFiles = uploadedFiles.replace(fileName + "</br>", "");
			}
			else if (uploadedFiles.indexOf(fileName) != -1) {
				uploadedFiles = uploadedFiles.replace(fileName, "");
			}
			$("#" + $("#displayPane").val()).html(uploadedFiles);
		} catch (e) {
		}
	};

	var setPointerAsHand = function(ctrl) {
		$(ctrl).css( {
			"cursor" : "pointer"
		});
	};
	var addFolderNode = function(folderName) {
		var d = new Date();
		var node = tree.insertNewItem(tree.getSelectedItemId(), d.valueOf(),
				folderName, 0, 0, 0, 0, 'SELECT');
		tree.setItemImage2(tree.getSelectedItemId(), 'folderClosed.gif',
				'folderOpen.gif', 'folderClosed.gif');
		dirs[folderName] = tree.getItemText(tree.getParentId(node.id));
		saveNodeFolderInSession(folderName, tree.getItemText(tree
				.getParentId(node.id)));
		tree.setItemText(node.id, tree.getItemText(node.id), "Directory");

	};

	var addFileNode = function(fileName) {
		var d = new Date();
		var node = tree.insertNewItem(tree.getSelectedItemId(), d.valueOf(),
				fileName, 0, 0, 0, 0);
		tree.setItemText(node.id, tree.getItemText(node.id), "File");
		return node;
	};

	var isFolderDialogOpen = false;

	var closeDialog = function(ctrl) {
		$(ctrl).dialog("destroy");
		$(ctrl).remove();
		isFolderDialogOpen = false;
	};
	
	$( "#radioAction" ).hide();
	var rowId=null;
	$(".standartTreeRow").bind("dblclick", function() {
		$( "#radioAction" ).buttonset();
		$( "#radioAction" ).hide();
		rowId=null;
		var fileName = tree.getItemText(tree.getSelectedItemId());
		var rowCtrl = treeTable[fileName];
		$('.viewfileName').each(function() {
			var currentElement = $(this).text();
			if(currentElement==fileName){
				rowId=$($(this).parent().get(0)).attr('id');
				$( "#radioAction" ).dialog();
				$( "#radioAction" ).show();
			}
		});
		
		return false;
		
	});
	
	$(".radioAction").bind("click", function() {
		var selectedAction=$('input[name=radioAction]:checked').attr('id');
		if(selectedAction=='downloadRowButton')
			downloadRow(null,rowId);
		else if(selectedAction=='viewRowButton')
			viewRow(null,rowId);
		else if(selectedAction=='editRowButton')
			editRow(null,rowId);
		else if(selectedAction=='deleteRowButton')
			delRow(null,rowId);
		
		$( "#radioAction" ).hide();
		$( "#radioAction" ).dialog('close');
		return false;
	});
	
	$("#addFolder").bind("click", function() {
		if (isFolderDialogOpen == false) {
			isFolderDialogOpen = true;
			setParentNode();
			var folderDivObj = getFolderDiv();
			$(folderDivObj).appendTo(document);  
			$(folderDivObj).dialog( {
				draggable : true,
				resizable : false,
				width : 400,
				title : "Folder Name", 
				zindex : 10000, 
				buttons : {
					"Cancel" : function() {
						$(this).dialog("close");
					},
					" Add Folder / Dir " : function() {
						if (addFolder()) {
							$(this).dialog("close");
						}
					}

				},
				close : function() {
					closeDialog(this);
					isFolderDialogOpen = false;
				}
			});
			$($($(folderDivObj).parent()).get(0)).css('border','1px solid #DDDDDD');
		}
	});

	var addFolder = function() {
		try {
			if ($("#folderName").val() == "") {
				$('#message_bar_folderName').displayMessage( {
					message : "Please provide Folder/Dir Name.",
					speed : 'slow',
					skin : 'vista',
					background : '#FF0000',
					color : '#FFFFFF',
					autohide : true
				});
				return false;
			} else {
				var originalItemId = tree.getSelectedItemId();	
																								
				if (findTreeNode(rootNodeLabel,$("#folderName").val()) == false){					
					tree.selectItem(originalItemId,true,false);
					addFolderNode($("#folderName").val());
					$("#folderName").focus();						
					return true;
					
				} else {
					tree.selectItem(originalItemId,true,false);
					showFolderError("Folder/Directory with same name exists already");
					return false;
				}
			}

		} catch (e) {
			alert(e);
			return false;
		}
	};


	var findTreeNode = function (rootText, itemText){
		var rootIndex =  tree.findItemIdByLabel(rootText, 0, 1);
		
		var subItems = ""+tree.getAllSubItems(rootIndex);
		if (subItems.length > 0){
			return iterateOverNode(subItems,itemText);
		} else {
			return false;
		}
		 
	};

	var iterateOverNode = function (subItem,itemText){			
		var itemArray = subItem.split(",");
		//alert("ItemArray; After SPLIT : "+itemArray + " : ArrayItem.length" + itemArray.length) ;
		var isNode = false;		
		for (var itr =0; itr<itemArray.length; itr++){

			//alert("debug ; itr : itemTwext : "+tree.getItemText(itemArray[itr])+  " newFolderName : " + itemText);
			if (tree.getItemText(itemArray[itr]) == itemText){
				isNode =  true;
				return true;
			}

			//alert("debug: itr : " + tree.getItemText(itemArray[itr]));			
			var subs = tree.getAllSubItems(itemArray[itr]);
			if  (subs != null && subs != "" ){
				isNode = iterateOverNode(""+subs,itemText);
			}
				

					
		}
		return isNode;
	};

	var showFolderError = function (msg){
		$('#message_bar_folderName').displayMessage( {
			message : msg,
			speed : 'slow',
			skin : 'vista',
			background : '#FF0000',
			color : '#FFFFFF',
			autohide : true
		});
	};

	$("#deleteFolder").bind(
			"click",
			function() {

				var cnfrm = confirm('Selected folder and its contents will be permanently deleted');
				if(!cnfrm)
					return false;
				
				if (tree.getSelectedItemId() != "" && rootNodeLabel != tree.getItemText(tree.getSelectedItemId())) {

					delTreeNode();
					var initialImg = tree.getItemImage(
							tree.getSelectedItemId(), 0, false);

					$("#" + tree.getIndexById(tree.getSelectedItemId()))
							.remove();
					var fileName = tree.getItemText(tree.getSelectedItemId());
					var rowCtrl = treeTable[fileName];
					removeChildFiles();
					$(rowCtrl).remove();

					deleteFromServer(fileName);
					removeFileFromPan(fileName);
					tree.deleteItem(tree.getSelectedItemId(), true);
					var afterImg = tree.getItemImage(tree.getSelectedItemId(),
							0, false);
					if ((initialImg != afterImg)
							&& (initialImg.indexOf("folder") != -1)) {
						tree.setItemImage2(tree.getSelectedItemId(),
								'folderClosed.gif', 'folderOpen.gif',
								'folderClosed.gif');
					}
					
					$('#selectedValues').html(""); 
					$('.ui-autocomplete-input').val("");
				} else {
					showErrorMsg("Pleased select any folder/directory or other than root node.");
				}
			});

	var removeChildFiles = function() {
		var subItems = "" + tree.getAllSubItems(tree.getSelectedItemId());
		//alert("Sub ITems List: " + subItems);
		if (subItems != null && subItems != "") {
			var fileNames = subItems.split(",");
			for (itr = 0; itr < fileNames.length; itr++) {
				var nodeImg = tree.getItemImage(tree.getSelectedItemId(), 0,
						false);
				if (nodeImg.indexOf("folder") != -1) {
					tree.selectItem(fileNames[itr], true, false);
					removeChildFiles();
					tree.deleteItem(fileNames[itr], true);
				}
				var fName = tree.getItemText(fileNames[itr]) == 0 ? fileNames[itr]
						: tree.getItemText(fileNames[itr]);
				deleteFromServer(fName);
				removeFileFromPan(fName);
				var rowCtrl = treeTable[fName];
				$(rowCtrl).remove();

			}
		}
	};

	$("#uploadFile").bind(
			"click",
			function() {
				if (tree.getSelectedItemId() != ""
						|| tree.getSelectedItemId() != null) {
					setParentNode();
					$("#file_1").trigger("click");
				} else {
					showErrorMsg("Pleased select any folder/directory.");
				}
			});

	var errorDisplayedOnce = false;
	var showErrorMsg = function(msg) {
		if($('#message_bar').css('display') == 'none' && errorDisplayedOnce == true) {
			$('#message_bar').displayMessage( {
				message : msg,
				speed : 'slow',
				skin : 'vista',
				background : '#FF0000',
				color : '#FFFFFF',
				autohide : true
			});
		} else if(errorDisplayedOnce == false) {
			$('#message_bar').displayMessage( {
				message : msg,
				speed : 'slow',
				skin : 'vista',
				background : '#FF0000',
				color : '#FFFFFF',
				autohide : true
			});
			errorDisplayedOnce = true;
		}
	};

	var delTreeNode = function() {
		for ( var key in files) {
			if (files[key] == tree.getItemText(tree.getSelectedItemId())) {
				delete files[key];
			}
		}
		delete dirs[tree.getItemText(tree.getSelectedItemId())];
	};

	$("#addFolder").bind("mouseover", function(e) {
		$("#addFolder").css( {
			"cursor" : "pointer"
		});
	});

	$("#deleteFolder").bind("mouseover", function(e) {
		$("#deleteFolder").css( {
			"cursor" : "pointer"
		});
	});

	$("#uploadFile").bind("mouseover", function(e) {
		$("#uploadFile").css( {
			"cursor" : "pointer"
		});
	});
	
	$("#renameFolder").bind("mouseover", function(e) {
		$("#renameFolder").css( {
			"cursor" : "pointer"
		});
	});

	var saveNodeFolderInSession = function(nodeText, parentNodeText) {
		$.ajax( {
			type : "POST",
			url : "saveInSessionNodeFile.action?fileType=" + usrFileType
					+ "&recordId=" + usrRecordId + "&displayPane="
					+ usrDisplayPane + "&tableName=" + userTableName
					+ "&nodeText=" + nodeText + "&parentNodeText="
					+ parentNodeText,
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
			}
		});
	};

	var setParentNode = function() {
		var selectedId = tree.getSelectedItemId();
		var img = tree.getItemImage(tree.getSelectedItemId(), 0, false);
		if (img != null && img.indexOf("folder") != -1) {
			$("#parentNodeText")
					.val(tree.getItemText(tree.getSelectedItemId()));
		} else if (img != null && img.indexOf("leaf") != -1) {
			if (rootNodeLabel == tree.getItemText(tree.getSelectedItemId())) {
				$("#parentNodeText").val(
						tree.getItemText(tree.getSelectedItemId()));
			} else if (rootNodeLabel != tree.getItemText(tree
					.getSelectedItemId())
					&& tree.getItemText(tree.getSelectedItemId()).indexOf(".") != -1) {
				$("#parentNodeText").val(
						tree.getItemText(tree.getParentId(tree
								.getSelectedItemId())));
				tree.selectItem(tree.getParentId(tree.getSelectedItemId()),
						false, false);
			} else {
				$("#parentNodeText").val(
						tree.getItemText(tree.getSelectedItemId()));
			}
		}

	};

	var doWhenEnter = function(evt) {
		evt = (evt) ? evt : ((window.event) ? window.event : "");
		if (evt) {
			if (evt.keyCode == 13 || evt.which == 13) { // Enter Key Capture
				addFolder();
			}
		}
	};

	var getFolderDiv = function() {
		return $('<div id="folderNameDiv" style="display: none">'
				+ '<div>&nbsp;</div> '
				+ '<div id="message_bar_folderName"></div>'
				+ ' Folder/Dir Name: <input type="text" name="folderName" id="folderName" />'
				+ '</div>');
	};
	
	function loadTree(){
		tree = new dhtmlXTreeObject("treeboxbox_tree", "100%", "100%", 0);
		tree.setSkin('dhx_skyblue');
		tree.setImagePath("dhtmlTree/imgs/");
		tree.setXMLAutoLoading("getTreeFile.action");
		tree.setDataMode("json");
		tree.loadJSON("getTreeFile.action?fileType=" + usrFileType + "&recordId="
				+ usrRecordId + "&displayPane=" + usrDisplayPane + "&tableName="
				+ userTableName + "&rootLabel=" + root);
	}
	
	function getParameterByName(name,params) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec("?"+params);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function populateUploadsPane(usrFileType, usrDisplayPane, userTableName, usrRecordId){
		 $.ajax({
			    type : "POST",
			    url : "populateUploadedFile.action?fileType=" + usrFileType + "&recordId=" + Number(usrRecordId) + "&displayPane="
			    	+ usrDisplayPane + "&tableName=" + userTableName,
			    async : false,
			    dataType : "json",
			    cache : false,
			    success : function(result) {
					$("#" + usrDisplayPane).html("");
				    var list = result.fileList;
				    for ( var itr = 0; itr < list.length; itr++) {
					    var rec = list[itr];
					    $("#" + usrDisplayPane).append(rec.fileName + "<br/>");
					}
			    }
		});
	};
	
	function loadUploadedList(usrFileType,usrRecordId,usrDisplayPane,userTableName,root){
		$('#files_1 tr').each(function(){
			var ids=$(this).attr('id');
			if(ids!=""&& ids>=0)
				$(this).remove();
		});
		
		$
		.ajax( {
			type : "POST",
			url : "populateUploadedFile.action?fileType=" + usrFileType
					+ "&recordId=" + usrRecordId + "&displayPane="
					+ usrDisplayPane + "&tableName=" + userTableName
					+ "&rootLabel=" + root,
			async : false,
			dataType : "json",
			cache : false,
			success : function(result) {
				if (usrFileType == "img"){
					$("#treeboxbox_tree").css({"display":"none"});						
					$("#addFolder").css({"display":"none"});
					$("#deleteFolder").css({"display":"none"});
					$("#tree_td").css({"height":"5px"});
				}
									
				var list = result.fileList;
				var idToAppend = "files_1";
				for ( var itr = 0; itr < list.length; itr++) {
					var rec = list[itr];
					var ran = Math.floor(Math.random() * 1100);
					var ran2 = Math.floor(Math.random() * 1500);
					var delImg = $("<img/>");
					$(delImg[0]).attr( {
						"src" : "fileupload/deleteFile.png",
						"id" : ran2,
						"title" : "Delete File"
					});
					$(delImg[0]).addClass(rec.fileName);
					var qryStr = "hashedName="+rec.hashedName+"&nodeText=" + rec.fileName + "&docId="
							+ rec.docId + "&imgId=" + rec.imgId
							+ "&recordId=" + usrRecordId + "&tableName="
							+ result.tableName + "&fileType=" + usrFileType
							+ "&fileToUploadFileName=" + rec.fileName;

					var row = $('<tr id='
							+ itr
							+ '> '
							+ '<td class="viewfileName" id='+itr+'_fNameList>'
							+ rec.fileName
							+ '</td>'
							+ '<td>'
							+ rec.progressMsg
							+ ' File uploaded successfully. </td>'
							+ '<td >'
							+ rec.size
							+ '</td>'
							+ '<td id = '
							+ ran
							+ '>'
							+ "<img class='document-action-image' src ='./images/view.png' title='View Document/Image' onclick = 'viewRow(this,"
							+ itr + ");' />" 
							+ "<img class='document-action-image' src ='./images/download.png' title='Download File' onclick = 'downloadRow(this,"
							+ itr + ");' />"
							+ "<img class='document-action-image' src ='./images/edit.png' title='Edit File Name' onclick = 'editRow(this,"
							+ itr + ");' />" 
							+ "<img class='document-action-image' src ='fileupload/deleteFile.png' title='Delete File' onclick = 'delRow(this,"
							+ itr + ");' />" 
							+ '</td>' + '</tr>');
					
					$($("#" + idToAppend)[0]).append(row[0]); 
					qryStrArray[itr] = qryStr;
					$("#displayPane").val(result.displayPane);
					usrDisplayPane = result.displayPane;
					treeTable[rec.fileName] = row;
					$('.filesSearch').append($('<option>', { value : rec.fileName }).text(rec.fileName)); 
				}
			}
		});
	}
	
</script>

<div id="message_bar"></div>

<style>
	.ui-autocomplete-input{
		min-height:28px!important;
	}
</style>