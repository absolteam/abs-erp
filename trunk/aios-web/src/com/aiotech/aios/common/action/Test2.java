package com.aiotech.aios.common.action;

import java.util.Calendar;
import java.util.Date;

public class Test2 {

	public static void main(String argu[]) {

		/*
		 * System.out.println("ahsan.m@aio.ae".split("_")[0]);
		 * 
		 * 
		 * Calendar calendar = Calendar.getInstance(); calendar.clear();
		 * calendar.set(Calendar.DATE,(1)); calendar.set(Calendar.MONTH, 3);
		 * calendar.set(Calendar.YEAR, 2014); Date date = calendar.getTime();
		 * System.out.println("Date--"+date.toString());
		 */

		System.out.println("is Plain 1331: " + ispalin(1331));

	}

	static boolean ispalin(Integer toConvert) {

		int a = toConvert;
		StringBuilder toReturn = new StringBuilder();

		while (a >= 10) {
			toReturn.append(a % 10);
			a /= 10;
		}

		toReturn.append(a);
		return Integer.parseInt(toReturn.toString()) == toConvert;
	}
}
