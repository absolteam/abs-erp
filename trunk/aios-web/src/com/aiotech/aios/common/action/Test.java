package com.aiotech.aios.common.action;

//STEP 1. Import required packages
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.csvreader.CsvReader;

public class Test {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/aios";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "root";

	public static void main(String[] args) {

		char[] chars = new char[] { '\u0097' };
		String str = new String(chars);
		byte[] bytes = str.getBytes();
		System.out.println(Arrays.toString(bytes));

		return;

		/*Connection conn = null;
		Statement stmt = null;
		List<String> dbnames = null;
		List<String> file1 = null;
		List<String> file2 = null;

		//
		List<Integer> ids = null;
		List<Integer> line1 = null;
		List<Integer> line2 = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM names";
			ResultSet rs = stmt.executeQuery(sql);

			// STEP 5: Extract data from result set
			
			 * while(rs.next()){ //Retrieve by column name // int id =
			 * rs.getInt("id"); // int age = rs.getInt("age"); String first =
			 * rs.getString("username"); // String last = rs.getString("last");
			 * 
			 * //Display values // System.out.print("ID: " + id); //
			 * System.out.print(", Age: " + age); System.out.print(", First: " +
			 * first); // System.out.println(", Last: " + last); }
			 
			file1 = fileReader();

			ids = new ArrayList<Integer>();
			line1 = new ArrayList<Integer>();

			while (rs.next()) {
				int counter = 0;
				for (String string : file1) {
					String[] filenames = string.split(",");
					String filename = filenames[0];
					if (filename != null
							&& filename.equalsIgnoreCase(rs.getString("name"))) {
						ids.add(rs.getInt("id"));
						line1.add(counter);
					}
					counter++;
				}

			}
			file2 = fileReader2();

			// ids=new ArrayList<Integer>();
			line2 = new ArrayList<Integer>();

			while (rs.next()) {
				int counter = 0;
				for (String string : file1) {
					String[] filenames = string.split(",");
					String filename = filenames[0];
					if (filename != null
							&& filename.equalsIgnoreCase(rs.getString("name"))) {
						// ids.add(rs.getInt("id"));
						line2.add(counter);
					}
					counter++;
				}
				int count = 0;
				for (Integer integer : ids) {
					// line1.get(count);
					// line2.get(count);
					int count2 = 0;
					String nameline1 = file1.get(line1.get(count));
					String nameline2 = file2.get(line2.get(count));
					String[] file1names = nameline1.split(",");
					String[] file2names = nameline2.split(",");
					for (String filename : file1names) {
						Calendar calendar = Calendar.getInstance();
						calendar.clear();
						calendar.set(Calendar.DATE, (count2 + 1));
						calendar.set(Calendar.MONTH, 3);
						calendar.set(Calendar.YEAR, 2014);
						Date date = (Date) calendar.getTime();
						// 4,2014,count2+1
						// Insert with date Insert INTO NAME
						// (ids,filename,file2names[count2],date)
						String sqlQry = "INSERT INTO NAME " + "VALUES (" + ids
								+ ", '" + filename + "', '"
								+ file2names[count2] + "', " + date.toString()
								+ ")";
						stmt.executeUpdate(sqlQry);
					}

				}
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}// nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}// end finally try
		}// end try
		System.out.println("Goodbye!");
	}// end main

	public static List<String> fileReader() {
		File file = null;
		String fileName = null;
		CsvReader reader = null;
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			// File file=new File(attendancePath);
			boolean exists = file.exists();
			if (exists) {
				line = br.readLine();
				files.add(line);
				
				 * //reader = new CsvReader(file.toString());
				 * reader.getTrimWhitespace(); reader.setDelimiter(' '); while
				 * (reader.readRecord()) { if (reader.getCurrentRecord() > 0) {
				 * String[] datas = new String[reader.get(0)
				 * .split(",").length]; datas = reader.get(0).split(","); } }
				 
			}
			return files;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}*/
	}

	public static List<String> fileReader2() {
		File file = null;
		String fileName = null;
		CsvReader reader = null;
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			// File file=new File(attendancePath);
			boolean exists = file.exists();
			if (exists) {
				line = br.readLine();
				files.add(line);
				/*
				 * //reader = new CsvReader(file.toString());
				 * reader.getTrimWhitespace(); reader.setDelimiter(' '); while
				 * (reader.readRecord()) { if (reader.getCurrentRecord() > 0) {
				 * String[] datas = new String[reader.get(0)
				 * .split(",").length]; datas = reader.get(0).split(","); } }
				 */
			}
			return files;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}// end FirstExample