package com.aiotech.aios.common.action;

import java.io.IOException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class WebServiceClient {
	public static final String url = "http://localhost:8080/aios-web/service/rest/getPointOfSaleSync";

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception, IOException {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource webResource = client.resource(UriBuilder.fromUri(url)
				.build());
		MultivaluedMap formData = new MultivaluedMapImpl();
		String name1 = "param";
		formData.add(name1, new String("Mohamed"));
		ClientResponse response = webResource.type(
				MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(
				ClientResponse.class, formData);
		System.out.println("Response " + response.getEntity(String.class));

		try {

			client = Client.create();

			webResource = client
					.resource("http://localhost:8081/aios-web/service/rest/getPointOfSaleSync");
			SalesInvoiceVO vo = webResource.type(MediaType.APPLICATION_JSON)
					.get(SalesInvoiceVO.class);

			client = Client.create();
			webResource = client
					.resource("http://localhost:8081/aios-web/service/rest/sendpointOfSaleSync");
			response = webResource.type(MediaType.APPLICATION_XML).post(
					ClientResponse.class, vo);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			String output = response.getEntity(String.class);
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}