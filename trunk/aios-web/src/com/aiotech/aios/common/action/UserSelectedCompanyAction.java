package com.aiotech.aios.common.action;

/*******************************************************************************
 *
 * Create Log
 * -----------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 Description
 * -----------------------------------------------------------------------------
 * 1.0		31 Aug 2010 	Nagarajan T.	 		   	Initial Version
 * 2.0		18 Nov 2010		Nagarajan T.				Menu Hierarchy
 ******************************************************************************/

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.common.to.UserSelectedCompanyTO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.vo.JobShiftVO;
import com.aiotech.aios.hr.service.bl.JobAssignmentBL;
import com.aiotech.aios.hr.service.bl.JobShiftBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.vo.Configuration;
import com.aiotech.aios.system.domain.entity.vo.Configurations;
import com.aiotech.aios.system.domain.entity.vo.MapEntryType;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.web.interceptor.AIOSContextListner;
import com.aiotech.aios.web.locale.I18NInterface;
import com.aiotech.aios.web.locale.I18NSingleton;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class UserSelectedCompanyAction extends ActionSupport {
	private static final Logger LOGGER = LogManager
			.getLogger(UserSelectedCompanyAction.class);
	private String com;
	private String sortType = "desc";
	private String sort = "leave_id";
	private int pageNum = 0;
	private int count = 0;
	private int counting;
	private String title;
	private String link;

	private String menuId;
	private int workflowId;

	private String lookUpId;
	private String lookUpName;
	private String lookUpDescription;
	private String lookUpTypeId;
	private String status;

	private int userId;
	private String userName;
	private int userStatus;
	private int personId;
	private int notifyId;

	private String description;
	private String successRedirectUrl;
	private String errorRedirectUrl;
	private String transactionPassword;
	private int tasknewId;
	private String taskSubject;

	private int id;
	private int companyId;
	private Integer applicationId;
	private String functionId;
	private long uniqueCode;
	private String attribut8;
	private int organizationId;
	private Long newImplementationId;
	private Implementation implementation;
	public SystemService systemService;
	private String storeLogin;
	private String key = "";
	private JobAssignmentBL jobAssignmentBL;

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	StringBuffer sb = new StringBuffer();

	public String selectCompany() throws SQLException, IOException {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		// Call from email
		if (user == null) {
			Long messageId = null;
			if (ServletActionContext.getRequest().getParameter("processKey") != null) {
				messageId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("processKey"));
			}

			WorkflowEnterpriseService serviceObject = jobAssignmentBL
					.getWorkflowEnterpriseService();
			Message message = serviceObject.getGenerateNotificationsBL()
					.getMessageService().getMessageInfoByMessageId(messageId);
			List<UserRole> userRoles = serviceObject
					.getGenerateNotificationsBL()
					.getMessageService()
					.getAllUserRoleByRoleId(
							message.getWorkflowDetailProcess()
									.getWorkflowDetail().getRole());
			for (UserRole userRole : userRoles) {
				if ((long) userRole.getUser().getPersonId() == (long) message
						.getPersonId()) {
					user = userRole.getUser();

				}
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("USER", user);
			sessionObj.put("USER", user);
			try {
				Implementation implementation = new Implementation();
				implementation.setImplementationId(user.getImplementationId());
				implementation = systemService
						.getImplementation(implementation);

				ServletActionContext.getRequest().getSession()
						.setAttribute("THIS", implementation);
				sessionObj.put("THIS", implementation);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		User user2 = systemService.getUser(user.getUsername());

		session.setAttribute("DASHBOARD_ACCESS", user.getDashboardAccess());

		try {
			storeLogin = "valid";
			// Get implementation group
			List<Long> implementationIds = new ArrayList<Long>();
			if (user.getUserGroup() != null
					&& !user.getUserGroup().trim().equals("")) {
				String[] str = user.getUserGroup().split(",");
				for (String string : str) {
					implementationIds.add(Long.valueOf(string));

				}
			}
			List<Implementation> implementationGroups = null;
			if (implementationIds != null && implementationIds.size() > 0)
				implementationGroups = systemService
						.getImplementationGroup(implementationIds);

			ServletActionContext.getRequest().setAttribute(
					"implementationGroups", implementationGroups);
			newImplementationId = getImplementation().getImplementationId();
			boolean admin = false;
			String BIAccessString = "";
			List<UserRole> userRoles = systemService.getUserRoles(user
					.getUsername());

			if (userRoles != null && userRoles.size() > 0) {

				for (UserRole userRole : userRoles) {

					if (userRole.getRole().getIsAdmin() != null
							&& userRole.getRole().getIsAdmin()) {
						admin = userRole.getRole().getIsAdmin();
						BIAccessString += (userRole.getRole().getBIAccess() != null) ? userRole
								.getRole().getBIAccess() + ","
								: ",";
					} else {

						BIAccessString += (userRole.getRole().getBIAccess() != null) ? userRole
								.getRole().getBIAccess() + ","
								: ",";
					}
				}
			}
			session.setAttribute("ADMIN", admin);

			if (BIAccessString.contains("GL")) {
				session.setAttribute("GL_BI", true);
			}
			if (BIAccessString.contains("SL")) {
				session.setAttribute("SL_BI", true);
			}
			if (BIAccessString.contains("INV")) {
				session.setAttribute("INV_BI", true);
			}
			if (BIAccessString.contains("HR")) {
				session.setAttribute("HR_BI", true);
			}
			if (BIAccessString.contains("PASS_RESET")) {
				session.setAttribute("PASS_RESET", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (null != session.getAttribute("POS_USERTILL_SESSION"
				+ sessionObj.get("jSessionId"))) {
			// Property Load
			Properties prop = new Properties();
			InputStream input = null;
			input = AIOSContextListner.class
					.getResourceAsStream("/dbconfig.properties");
			prop.load(input);
			FileInputStream inputStream = new FileInputStream(prop.getProperty(
					"SYNCFILE-LOCATION").trim());
			Properties posprops = new Properties();
			posprops.load(inputStream);
			String offlineEnv = posprops.getProperty("OFFLINE-MODE").trim();
			if (offlineEnv.equalsIgnoreCase("Y")) {
				session.removeAttribute("SHIFT_SESSION");
				long storeId = Long.valueOf(posprops.getProperty("STORE-ID")
						.trim());
				POSUserTill posUserSession = (POSUserTill) (session
						.getAttribute("POS_USERTILL_SESSION"
								+ sessionObj.get("jSessionId")));
				if ((long) posUserSession.getStore().getStoreId() == storeId) {
					List<JobShiftVO> jobShiftVOs = jobAssignmentBL
							.fetchLocationBasedShifts(posUserSession.getStore()
									.getCmpDeptLocation().getLocation()
									.getLocationId(), Calendar.getInstance()
									.getTime(), Calendar.getInstance()
									.getTime());
					String startTime = "";
					String endTime = "";
					Calendar c1 = Calendar.getInstance();
					Calendar c2 = Calendar.getInstance();
					DateTime currentTime = new DateTime(Calendar.getInstance()
							.getTime());
					List<JobShiftVO> finalShiftVO = new ArrayList<JobShiftVO>();
					for (JobShiftVO jobShiftVO : jobShiftVOs) {
						startTime = jobShiftVO.getStartTime();
						endTime = jobShiftVO.getEndTime();
						c1.set(Calendar.HOUR_OF_DAY,
								Integer.parseInt(startTime.split(":")[0]));
						c1.set(Calendar.MINUTE,
								Integer.parseInt(startTime.split(":")[1]));
						c2.set(Calendar.HOUR_OF_DAY,
								Integer.parseInt(endTime.split(":")[0]));
						c2.set(Calendar.MINUTE,
								Integer.parseInt(endTime.split(":")[1]));
						DateTime shiftStart = new DateTime(c1.getTime());
						DateTime shiftEnd = new DateTime(c2.getTime());
						long checkMin = Minutes.minutesBetween(currentTime,
								shiftEnd).getMinutes();
						if (checkMin > 0) {
							checkMin = Minutes.minutesBetween(currentTime,
									shiftStart).getMinutes();
							if (checkMin <= 0)
								finalShiftVO.add(jobShiftVO);
						}
					}
					session.setAttribute("SHIFT_SESSION", finalShiftVO);
					return "posuser";
				} else {
					storeLogin = "invalid";
					Authentication auth = SecurityContextHolder.getContext()
							.getAuthentication();
					if (auth != null) {
						new SecurityContextLogoutHandler().logout(
								ServletActionContext.getRequest(),
								ServletActionContext.getResponse(), auth);
					}
					SecurityContextHolder.getContext().setAuthentication(null);
					return "posusererror";
				}
			} else {
				inputStream.close();
				return "posuser";
			}
		} else
			return SUCCESS;
	}

	public String workflowApprovalDashboard() throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		// Call from email
		if (user == null) {
			Long messageId = null;
			if (ServletActionContext.getRequest().getParameter("processKey") != null) {
				messageId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("processKey"));
			}

			WorkflowEnterpriseService serviceObject = jobAssignmentBL
					.getWorkflowEnterpriseService();
			Message message = serviceObject.getGenerateNotificationsBL()
					.getMessageService().getMessageInfoByMessageId(messageId);
			List<UserRole> userRoles = serviceObject
					.getGenerateNotificationsBL()
					.getMessageService()
					.getAllUserRoleByRoleId(
							message.getWorkflowDetailProcess()
									.getWorkflowDetail().getRole());
			for (UserRole userRole : userRoles) {
				if ((long) userRole.getUser().getPersonId() == (long) message
						.getPersonId()) {
					user = userRole.getUser();

				}
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("USER", user);
			sessionObj.put("USER", user);
			try {
				Implementation implementation = new Implementation();
				implementation.setImplementationId(user.getImplementationId());
				implementation = systemService
						.getImplementation(implementation);

				ServletActionContext.getRequest().getSession()
						.setAttribute("THIS", implementation);
				sessionObj.put("THIS", implementation);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return SUCCESS;
	}

	public String showPOSOrderingSystem() throws Exception {

		Implementation implementation = systemService
				.getImplementationByCompanyKey(key);

		if (implementation == null)
			throw new Exception("COMPANY KEY UNKNOWN");

		ServletActionContext.getRequest().getSession()
				.setAttribute("THIS", implementation);

		return SUCCESS;
	}

	public String resetCompany() throws SQLException, IOException {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		try {
			User user = (User) sessionObj.get("USER");
			Implementation newImplementation = new Implementation();
			newImplementation.setImplementationId(newImplementationId);
			newImplementation = systemService
					.getImplementation(newImplementation);

			if (user.getUsername().contains("@"))
				throw new Exception("Email based username detected!!");

			String[] newUserNameArray = user.getUsername().split("_");
			User newUser = systemService.getUser(newUserNameArray[0] + "_"
					+ newImplementation.getImplementationId());
			if (newUser == null || newUser.getUsername().equals("")) {
				newUser = systemService.getUserByAdminFlag(newImplementationId);
			}
			session.removeAttribute("POS_USERTILL_SESSION"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("MENU_MAP");
			session.setAttribute("USER", newUser);
			session.setAttribute("THIS", newImplementation);
			session.setAttribute("COMPANY_NAME",
					newImplementation.getCompanyName());

			// Configuration File Reader
			Configurations configurations = null;
			List<Configuration> list = null;
			try {
				JAXBContext jaxbContext = JAXBContext
						.newInstance(Configurations.class);

				Unmarshaller jaxbUnmarshaller = jaxbContext
						.createUnmarshaller();
				configurations = (Configurations) jaxbUnmarshaller
						.unmarshal(LoginAction.class
								.getResourceAsStream("/META-INF/configurations.xml"));

				list = configurations.getConfigurations();

				Configuration configuration = null;
				if (list != null && list.size() > 0) {
					for (Configuration conf : list) {
						if ((long) conf.getImplementation() == (long) 200)
							configuration = conf;
						else if ((long) conf.getImplementation() == (long) newImplementation
								.getImplementationId())
							configuration = conf;
					}
				}

				// Set into session
				HttpSession sSession = ServletActionContext.getRequest()
						.getSession();
				if (configuration != null) {
					for (MapEntryType mapEntryType : configuration.getMap()
							.getEntry()) {
						sSession.setAttribute(mapEntryType.getKey(),
								mapEntryType.getValue());
						ServletActionContext
								.getRequest()
								.getSession()
								.setAttribute(mapEntryType.getKey(),
										mapEntryType.getValue());
					}
					for (MapEntryType mapEntryType : configuration
							.getAccessMap().getEntry()) {
						sSession.setAttribute(mapEntryType.getKey(),
								mapEntryType.getValue());
						ServletActionContext
								.getRequest()
								.getSession()
								.setAttribute(mapEntryType.getKey(),
										mapEntryType.getValue());
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			ServletActionContext.getRequest().setAttribute("jsonlist",
					"SUCCESS");
			ServletActionContext.getRequest().setAttribute("COMPANY_NAME",
					newImplementation.getCompanyName());

			List<JobAssignment> jobAssignments = systemService
					.getJobAssignmentDAO().findByNamedQuery(
							"getJobAssignmentByPerson", user.getPersonId());
			if (jobAssignments != null && jobAssignments.size() > 0)
				session.setAttribute("SESSION_JOB_ASSIGNEMNT_OBJECT"
						+ sessionObj.get("jSessionId"), jobAssignments.get(0));

			if (ServletActionContext.getRequest().getHeader("User-Agent")
					.indexOf("Mobile") != -1) {
				boolean isMobile = true;

				ServletActionContext.getRequest().getSession()
						.setAttribute("IS_MOBILE", isMobile);
				ActionContext.getContext().getSession()
						.put("IS_MOBILE", isMobile);
				String returnStirng = "mobile";
				ServletActionContext.getRequest().setAttribute("IS_MOBILE",
						isMobile);
				LOGGER.info("returnStirng====>" + returnStirng + "--"
						+ isMobile);

			}
		} catch (Exception e) {
			newImplementationId = getImplementation().getImplementationId();
			ServletActionContext.getRequest().setAttribute("jsonlist", "ERROR");
			e.printStackTrace();
		}
		return SUCCESS;

	}

	// Get Online User List
	public String checkingOnlineUsers() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		// LOGGER.info("Module: System : method selectCompany() : user= "+((session
		// != null && session.get("employee_ID") != null) ?
		// session.get("employee_ID").toString().trim() :
		// "null")+" : loggedin? = "+((session != null &&
		// session.get("logged-in") != null) ?
		// session.get("logged-in").toString().trim() :
		// "false")+" Action Started ");
		// Internalization code
		String lan = "";
		if (session != null && (session.get("logged-in") != null)
				&& (session.get("logged-in")).equals("true")
				&& session.get("lang").toString() != null
				&& !session.get("lang").toString().equalsIgnoreCase(""))
			lan = session.get("lang").toString();
		else
			lan = "en";
		ResourceBundle bundle = new I18NInterface().callgetSingleton(lan,
				"all", systemService);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		List<UserSelectedCompanyTO> userList = null;
		UserSelectedCompanyTO companyTO = new UserSelectedCompanyTO();
		// UserSelectedCompanyDAO companyDAO=new UserSelectedCompanyDAO(); //DAO
		try {
			if (session != null && (session.get("logged-in") != null)) { // Session
																			// Validation
				if ((session.get("logged-in")).equals("true")) {
					companyTO.setUserId((String) session.get("emp_CODE"));
					companyTO.setOrganizationId(organizationId);
					// userList=companyDAO.getUserList(companyTO); //User List
					ServletActionContext.getRequest().setAttribute("userList",
							userList);
					return SUCCESS;

				} else {
					// LOGGER.info("Session Expired");
					ServletActionContext.getRequest().setAttribute(
							"sessionErrMsg", "Session Expired !!!");
					return ERROR;
				}
			} else {
				// LOGGER.info("Session Expired");
				ServletActionContext.getRequest().setAttribute("sessionErrMsg",
						"Session Expired !!!");
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// to retrieve notification
	public String retrieveNotification() {
		LOGGER.info("retrieveNotification");
		Map<String, Object> session = ActionContext.getContext().getSession();

		// Internalization code
		ResourceBundle bundle = new I18NInterface().callgetSingleton(session
				.get("lang").toString(), "all", systemService);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		List<UserSelectedCompanyTO> menuLists = null; // Bean List for Menus
		UserSelectedCompanyTO menuList = new UserSelectedCompanyTO(); // Bean
																		// for
																		// Menu
		List<UserSelectedCompanyTO> userList = null;
		// UserSelectedCompanyDAO companyDAO=new UserSelectedCompanyDAO(); //DAO
		if (session != null && (session.get("logged-in") != null)) { // Session
																		// Validation
			if ((session.get("logged-in")).equals("true")) {
				try {
					int flag = 0;
					if (session.get("emp_CODE") != null) {
						menuList.setUserId((String) session.get("emp_CODE"));
					} else {
						LOGGER.info("Session Expired");
						ServletActionContext.getRequest().setAttribute(
								"sessionErrMsg", "Session Expired !!!");
						return LOGIN;
					}
					if ((session.get("personId") != null)) {
						LOGGER.info("Session Person Id--->"
								+ (Integer) session.get("personId"));
						menuList.setPersonId((Integer) session.get("personId"));
					} else {
						LOGGER.info("Session Expired");
						ServletActionContext.getRequest().setAttribute(
								"sessionErrMsg", "Session Expired !!!");
						return LOGIN;
					}

					// Notification coding added by rafiq
					menuList.setWorkflowId(workflowId);
					// List<UserSelectedCompanyTO>
					// notifyLst=companyDAO.getNotificationList(menuList);
					List<UserSelectedCompanyTO> notifyLst = new ArrayList<UserSelectedCompanyTO>();

					LOGGER.info("Notification List size >>>" + notifyLst.size());
					ServletActionContext.getRequest().setAttribute(
							"notifylist", notifyLst);
					return SUCCESS;
					// Notification coding end here
				} catch (Exception e) {
					// throw new RuntimeException(e);
					e.printStackTrace();
					return ERROR;
				}
			} else {
				LOGGER.info("Session Expired");
				ServletActionContext.getRequest().setAttribute("sessionErrMsg",
						"Session Expired !!!");
				return LOGIN;
			}
		} else {
			LOGGER.info("Session Expired");
			ServletActionContext.getRequest().setAttribute("sessionErrMsg",
					"Session Expired !!!");
			return LOGIN;
		}
	}

	// to retrieve notification pop-up
	public String retrievePopupNotifications() {

		Map<String, Object> session = ActionContext.getContext().getSession();// Session
																				// making

		// LOGGER.info("Module: CM - UserSelectedCompanyAction : method retrievePopupNotifications() : user= "+((session
		// != null && session.get("employee_ID") != null) ?
		// session.get("employee_ID").toString().trim() :
		// "null")+" : loggedin? = "+((session != null &&
		// session.get("logged-in") != null) ?
		// session.get("logged-in").toString().trim() : "false")+
		// " action started ...");
		try {
			if (session != null
					&& session.get("logged-in") != null
					&& session.get("logged-in").toString().trim()
							.equalsIgnoreCase("true")) {
				// LOGGER.info("Module: CM - UserSelectedCompanyAction : method retrievePopupNotifications() : user= "+((session
				// != null && session.get("employee_ID") != null) ?
				// session.get("employee_ID").toString().trim() :
				// "null")+" : loggedin? = "+((session != null &&
				// session.get("logged-in") != null) ?
				// session.get("logged-in").toString().trim() : "false")+
				// " : "+"Session Success....");
				UserSelectedCompanyTO userTO = new UserSelectedCompanyTO();
				// UserSelectedCompanyDAO userDAO=new UserSelectedCompanyDAO();
				List<UserSelectedCompanyTO> notifyUser;
				notifyUser = new ArrayList<UserSelectedCompanyTO>();
				personId = (Integer) session.get("personId");
				userTO.setPersonId(personId);
				// notifyUser=userDAO.fetchNotifications(userTO);
				ServletActionContext.getRequest().setAttribute("userNotify",
						notifyUser);
				ServletActionContext.getRequest().setAttribute(
						"userNotifysize", notifyUser.size());
				return SUCCESS;
			} else {
				// LOGGER.info("Module: CM - UserSelectedCompanyAction : method retrievePopupNotifications() : user= "+((session
				// != null && session.get("employee_ID") != null) ?
				// session.get("employee_ID").toString().trim() :
				// "null")+" : loggedin? = "+((session != null &&
				// session.get("logged-in") != null) ?
				// session.get("logged-in").toString().trim() : "false")+
				// " : "+"Session expired....");
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// to close notification pop-up on user close
	public String closePopupNotifications() {
		// Internalization code
		ResourceBundle bundle = I18NSingleton.getBundleValue();
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		Map<String, Object> session = ActionContext.getContext().getSession();// Session
																				// making
		LOGGER.info("Module: CM - UserSelectedCompanyAction : method closePopupNotifications() : user= "
				+ ((session != null && session.get("employee_ID") != null) ? session
						.get("employee_ID").toString().trim()
						: "null")
				+ " : loggedin? = "
				+ ((session != null && session.get("logged-in") != null) ? session
						.get("logged-in").toString().trim()
						: "false") + " action started ...");
		try {
			if (session != null
					&& session.get("logged-in") != null
					&& session.get("logged-in").toString().trim()
							.equalsIgnoreCase("true")) {
				LOGGER.info("Module: CM - UserSelectedCompanyAction : method closePopupNotifications() : user= "
						+ ((session != null && session.get("employee_ID") != null) ? session
								.get("employee_ID").toString().trim()
								: "null")
						+ " : loggedin? = "
						+ ((session != null && session.get("logged-in") != null) ? session
								.get("logged-in").toString().trim()
								: "false") + " : " + "Session Success....");
				UserSelectedCompanyTO userTO = new UserSelectedCompanyTO();
				// UserSelectedCompanyDAO userDAO=new UserSelectedCompanyDAO();

				personId = (Integer) session.get("personId");
				userTO.setPersonId(personId);
				userTO.setNotifyId(notifyId);

				// int updateStatus=userDAO.closeNotifications(userTO);
				ServletActionContext.getRequest().setAttribute("userNotify",
						userTO);
				return SUCCESS;
			} else {
				LOGGER.info("Module: CM - UserSelectedCompanyAction : method closePopupNotifications() : user= "
						+ ((session != null && session.get("employee_ID") != null) ? session
								.get("employee_ID").toString().trim()
								: "null")
						+ " : loggedin? = "
						+ ((session != null && session.get("logged-in") != null) ? session
								.get("logged-in").toString().trim()
								: "false") + " : " + "Session expired....");
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// work flow checkisvaliduser
	public String checkisvaliduser() {
		Map<String, Object> session = ActionContext.getContext().getSession();// Session
																				// making
		// Internalization code
		ResourceBundle bundle = new I18NInterface().callgetSingleton(session
				.get("lang").toString(), "all", systemService);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		try {

			if (session != null && (session.get("logged-in") != null)
					&& (session.get("logged-in")).equals("true")) {
				// transactionPassword
				LOGGER.info("User Name-->"
						+ (String) session.get("employee_ID"));
				LOGGER.info("Id-->" + (String) session.get("emp_CODE"));
				// LoginDAO loginDAO = new LoginDAO();
				/*
				 * boolean
				 * returnStatus=loginDAO.secondLevelAuthentication((String
				 * )session.get("emp_CODE"),transactionPassword);
				 * if(returnStatus){
				 */
				return SUCCESS;
				// }
				/*
				 * else{
				 * ServletActionContext.getRequest().setAttribute("errMsg",
				 * "You dont have access for this functionality !!!"); return
				 * ERROR; }
				 */
			} else {
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Session Expired !!!");
				LOGGER.info("-Session Expired in UserSelectedCompanyAction.checkisvaliduser()-");
				return ERROR;
			}
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("errMsg",
					"You dont have access for this functionality !!!");
			LOGGER.error(e);
			return ERROR;
		}
	}

	// to retrieve announcements for log-in user
	public String retrieveAnnoncements() {
		Map<String, Object> session = ActionContext.getContext().getSession();// Session
																				// making
		// Internalization code
		ResourceBundle bundle = new I18NInterface().callgetSingleton(session
				.get("lang").toString(), "all", systemService);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		// LOGGER.info("Module: CM - UserSelectedCompanyAction : method retrieveAnnoncements() : user= "+((session
		// != null && session.get("employee_ID") != null) ?
		// session.get("employee_ID").toString().trim() :
		// "null")+" : loggedin? = "+((session != null &&
		// session.get("logged-in") != null) ?
		// session.get("logged-in").toString().trim() : "false")+
		// " action started ...");
		try {
			if (session != null
					&& session.get("logged-in") != null
					&& session.get("logged-in").toString().trim()
							.equalsIgnoreCase("true")) {
				// LOGGER.info("Module: CM - UserSelectedCompanyAction : method retrieveAnnoncements() : user= "+((session
				// != null && session.get("employee_ID") != null) ?
				// session.get("employee_ID").toString().trim() :
				// "null")+" : loggedin? = "+((session != null &&
				// session.get("logged-in") != null) ?
				// session.get("logged-in").toString().trim() : "false")+
				// " : "+"Session Success....");
				UserSelectedCompanyTO userTO = new UserSelectedCompanyTO();
				// UserSelectedCompanyDAO userDAO=new UserSelectedCompanyDAO();
				List<UserSelectedCompanyTO> annoUser;
				annoUser = new ArrayList<UserSelectedCompanyTO>();
				personId = (Integer) session.get("personId");
				userTO.setPersonId(personId);
				// annoUser=userDAO.getAnnouncementsList(userTO);
				ServletActionContext.getRequest().setAttribute("userAnnoc",
						annoUser);
				ServletActionContext.getRequest().setAttribute("userAnnocsize",
						annoUser.size());
				return SUCCESS;
			} else {
				// LOGGER.info("Module: CM - UserSelectedCompanyAction : method retrieveAnnoncements() : user= "+((session
				// != null && session.get("employee_ID") != null) ?
				// session.get("employee_ID").toString().trim() :
				// "null")+" : loggedin? = "+((session != null &&
				// session.get("logged-in") != null) ?
				// session.get("logged-in").toString().trim() : "false")+
				// " : "+"Session expired....");
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// to retrieve dashboard view
	public String viewDashBoard() {

		Map<String, Object> session = ActionContext.getContext().getSession();// Session
																				// making
		// Internalization code
		/*
		 * ResourceBundle bundle = new
		 * I18NInterface().callgetSingleton(session.get("lang").toString(),
		 * "all");
		 * javax.servlet.jsp.jstl.core.Config.set(ServletActionContext.getRequest
		 * (), Config.FMT_LOCALIZATION_CONTEXT, new LocalizationContext(bundle,
		 * getLocale()));
		 */
		// I18N ended
		LOGGER.info("Module: CM - UserSelectedCompanyAction : method viewDashBoard() : user= "
				+ ((session != null && session.get("employee_ID") != null) ? session
						.get("employee_ID").toString().trim()
						: "null")
				+ " : loggedin? = "
				+ ((session != null && session.get("logged-in") != null) ? session
						.get("logged-in").toString().trim()
						: "false") + " action started ...");
		try {
			if (session != null
					&& session.get("logged-in") != null
					&& session.get("logged-in").toString().trim()
							.equalsIgnoreCase("true")) {
				LOGGER.info("Module: CM - UserSelectedCompanyAction : method viewDashBoard() : user= "
						+ ((session != null && session.get("employee_ID") != null) ? session
								.get("employee_ID").toString().trim()
								: "null")
						+ " : loggedin? = "
						+ ((session != null && session.get("logged-in") != null) ? session
								.get("logged-in").toString().trim()
								: "false") + " : " + "Session Success....");
				UserSelectedCompanyTO userTO = new UserSelectedCompanyTO();
				// UserSelectedCompanyDAO userDAO=new UserSelectedCompanyDAO();
				List<UserSelectedCompanyTO> taskView;
				taskView = new ArrayList<UserSelectedCompanyTO>();

				personId = (Integer) session.get("personId");
				userTO.setTaskId(tasknewId);
				userTO.setTaskSubject(taskSubject);
				userTO.setPersonId(personId);

				ServletActionContext.getRequest().setAttribute("personId",
						personId);

				/*
				 * if(attribut8!=null && attribut8.equalsIgnoreCase("ANOCMT")){
				 * taskView=userDAO.getAnnocementDetails(userTO); } else{
				 * taskView=userDAO.getTaskDetails(userTO); }
				 */

				for (int l = 0; l < taskView.size(); l++) {
					String dateSplit = new String();
					String tempDate = ((UserSelectedCompanyTO) taskView.get(l))
							.getTaskFromDate();
					LOGGER.info(" tempDate" + " ======== " + tempDate);
					int noOfPosition = 2;
					dateSplit = getDateFormat(tempDate, noOfPosition); // dateSplit+tempDate.split(" ");
					LOGGER.info(" dateSplit ======== " + dateSplit);
					((UserSelectedCompanyTO) taskView.get(l))
							.setTaskFromDate(dateSplit.trim());
				}

				for (int j = 0; j < taskView.size(); j++) {
					String tempdesc = new String();
					String desc = ((UserSelectedCompanyTO) taskView.get(j))
							.getTaskDesc();
					for (int k = 0; k < desc.length(); k++) {
						tempdesc = tempdesc + desc.charAt(k);
						// LOGGER.info("Task description--->"+tempdesc);
						if (k == 20) {
							tempdesc += "...";
							break;
						}
					}
					((UserSelectedCompanyTO) taskView.get(j))
							.setTaskSubDesc(tempdesc.trim());
				}

				ServletActionContext.getRequest().setAttribute("taskView",
						taskView);
				return SUCCESS;
			} else {
				LOGGER.info("Module: CM - UserSelectedCompanyAction : method viewDashBoard() : user= "
						+ ((session != null && session.get("employee_ID") != null) ? session
								.get("employee_ID").toString().trim()
								: "null")
						+ " : loggedin? = "
						+ ((session != null && session.get("logged-in") != null) ? session
								.get("logged-in").toString().trim()
								: "false") + " : " + "Session expired....");
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	private String getDateFormat(String dateString, int noOfPosition) { // Mar 4
																		// 2011
																		// 12:00AM
																		// -->
																		// for
																		// eg
																		// only
																		// Mar 4
		String dateVal = "";
		String[] dateArr = dateString.split("\\s+");

		LOGGER.info("dateArr len = " + dateArr.length);
		for (int i = 0; i < dateArr.length; i++) {
			LOGGER.info("dateArr val = " + dateArr[i]);
		}
		for (int i = 0; i < noOfPosition; i++) {
			LOGGER.info(dateArr[i]);
			dateVal = dateVal + dateArr[i] + " ";
		}

		return dateVal.trim();
	}

	// to back to dashboard
	public String returnDashBoard() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		LOGGER.info("Module: System : method selectCompany() : user= "
				+ ((session != null && session.get("employee_ID") != null) ? session
						.get("employee_ID").toString().trim()
						: "null")
				+ " : loggedin? = "
				+ ((session != null && session.get("logged-in") != null) ? session
						.get("logged-in").toString().trim()
						: "false") + " Action Started ");
		// Internalization code
		ResourceBundle bundle = new I18NInterface().callgetSingleton(session
				.get("lang").toString(), "all", systemService);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		// UserSelectedCompanyDAO companyDAO=new UserSelectedCompanyDAO(); //DAO
		UserSelectedCompanyTO menuList = new UserSelectedCompanyTO();
		if (session != null && (session.get("logged-in") != null)) { // Session
																		// Validation
			if ((session.get("logged-in")).equals("true")) {
				try {
					menuList.setCompanyId(id);
					personId = (Integer) session.get("personId");
					menuList.setPersonId(personId);
					/*
					 * List<UserSelectedCompanyTO>
					 * workflowList=companyDAO.getWorkFlowList(menuList);
					 * List<UserSelectedCompanyTO>
					 * taskList=companyDAO.getTaskListRetrieve(menuList);
					 */
					List<UserSelectedCompanyTO> workflowList = new ArrayList<UserSelectedCompanyTO>();
					List<UserSelectedCompanyTO> taskList = new ArrayList<UserSelectedCompanyTO>();
					ServletActionContext.getRequest().setAttribute(
							"workflowList", workflowList);
					ServletActionContext.getRequest().setAttribute("taskList",
							taskList);
					return SUCCESS;
				} catch (Exception e) {
					LOGGER.error("Action execption " + e);
					return ERROR;
				}
			} else {
				LOGGER.info("Session Expired");
				ServletActionContext.getRequest().setAttribute("sessionErrMsg",
						"Session Expired !!!");
				return LOGIN;

			}
		} else {
			LOGGER.info("Session Expired");
			ServletActionContext.getRequest().setAttribute("sessionErrMsg",
					"Session Expired !!!");
			return LOGIN;
		}
	}

	// Print Log Entry
	public String printLogEntry() {
		Map<String, Object> session = ActionContext.getContext().getSession();// Session
																				// making
		// Internalization code
		ResourceBundle bundle = new I18NInterface().callgetSingleton(session
				.get("lang").toString(), "all", systemService);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, getLocale()));
		// I18N ended
		LOGGER.info("Inside printLogEntry() : user= "
				+ ((session != null && session.get("employee_ID") != null) ? session
						.get("employee_ID").toString().trim()
						: "null"));
		UserSelectedCompanyTO companyTO = new UserSelectedCompanyTO();
		// UserSelectedCompanyDAO companyDAO=new UserSelectedCompanyDAO();
		UserSelectedCompanyTO to = new UserSelectedCompanyTO();
		try {
			if (session != null && (session.get("logged-in") != null)) {
				if ((session.get("logged-in")).equals("true")) {

					companyTO.setCompanyId(companyId);
					companyTO.setFunctionId(functionId);
					companyTO.setApplicationId(applicationId);

					// DB Call for submit the period details
					// to=companyDAO.printLogEntry(companyTO);

					if (to.getSqlReturnStatus() == 1) {
						to.setSqlReturnMsg("");
						ServletActionContext.getRequest().setAttribute("bean",
								to);
						return SUCCESS;
					} else if (to.getSqlReturnStatus() == 0) {
						ServletActionContext.getRequest().setAttribute("bean",
								to);
						return SUCCESS;
					} else if (to.getSqlReturnStatus() == 2) {
						ServletActionContext.getRequest().setAttribute("bean",
								to);
						return SUCCESS;
					} else if (to.getSqlReturnStatus() == 50) {
						to.setSqlReturnMsg("Internal error, Please contact Adminsistrator");
						ServletActionContext.getRequest().setAttribute("bean",
								to);
						return SUCCESS;
					} else {
						to.setSqlReturnMsg("Internal error, Please contact Adminsistrator");
						ServletActionContext.getRequest().setAttribute("bean",
								to);
						return SUCCESS;
					}
				} else {
					LOGGER.info("Module: System : method printLogEntry() : Session Expired");
					ServletActionContext.getRequest().setAttribute("errMsg",
							"Session Expired !!!");
					return ERROR;
				}
			} else {
				LOGGER.info("Module: System : method printLogEntry() : Session Expired");
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Session Expired !!!");
				return ERROR;
			}
		} catch (Exception e) {
			to.setSqlReturnMsg("Internal error, Please contact Adminsistrator");
			ServletActionContext.getRequest().setAttribute("bean", to);
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
					"Tansaction not successfull");
			return ERROR;
		}
	}

	public String getLookUpId() {
		return lookUpId;
	}

	public void setLookUpId(String lookUpId) {
		this.lookUpId = lookUpId;
	}

	public String getLookUpName() {
		return lookUpName;
	}

	public void setLookUpName(String lookUpName) {
		this.lookUpName = lookUpName;
	}

	public String getLookUpDescription() {
		return lookUpDescription;
	}

	public void setLookUpDescription(String lookUpDescription) {
		this.lookUpDescription = lookUpDescription;
	}

	public String getLookUpTypeId() {
		return lookUpTypeId;
	}

	public void setLookUpTypeId(String lookUpTypeId) {
		this.lookUpTypeId = lookUpTypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPageNum() {
		return pageNum;
	}

	public int getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(int workflowId) {
		this.workflowId = workflowId;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCounting() {
		return counting;
	}

	public void setCounting(int counting) {
		this.counting = counting;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getCom() {
		return com;
	}

	public void setCom(String com) {
		this.com = com;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public int getUserStatus() {
		return userStatus;
	}

	public int getNotifyId() {
		return notifyId;
	}

	public void setNotifyId(int notifyId) {
		this.notifyId = notifyId;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public String getTransactionPassword() {
		return transactionPassword;
	}

	public void setTransactionPassword(String transactionPassword) {
		this.transactionPassword = transactionPassword;
	}

	public String getSuccessRedirectUrl() {
		return successRedirectUrl;
	}

	public void setSuccessRedirectUrl(String successRedirectUrl) {
		this.successRedirectUrl = successRedirectUrl;
	}

	public String getErrorRedirectUrl() {
		return errorRedirectUrl;
	}

	public void setErrorRedirectUrl(String errorRedirectUrl) {
		this.errorRedirectUrl = errorRedirectUrl;
	}

	public int getTasknewId() {
		return tasknewId;
	}

	public void setTasknewId(int tasknewId) {
		this.tasknewId = tasknewId;
	}

	public String getTaskSubject() {
		return taskSubject;
	}

	public void setTaskSubject(String taskSubject) {
		this.taskSubject = taskSubject;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public long getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(long uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAttribut8() {
		return attribut8;
	}

	public void setAttribut8(String attribut8) {
		this.attribut8 = attribut8;
	}

	public int getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	public Long getNewImplementationId() {
		return newImplementationId;
	}

	public void setNewImplementationId(Long newImplementationId) {
		this.newImplementationId = newImplementationId;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getStoreLogin() {
		return storeLogin;
	}

	public void setStoreLogin(String storeLogin) {
		this.storeLogin = storeLogin;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	@Autowired
	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}
}
