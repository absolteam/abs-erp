package com.aiotech.aios.common.action;

import javax.mail.*;
import javax.mail.internet.*;

import java.security.Security;
import java.util.Properties;

class Main {
	public static void main(String[] args) throws Exception {

		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");

		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.debug", "true");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {

					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"rafiqabdul10@gmail.com", "rafiq@abdul10");
					}
				});

		session.setDebug(true);

		InternetAddress addressFrom = new InternetAddress("rafiqabdul10@gmail.com");

		MimeMessage message = new MimeMessage(session);
		message.setSender(addressFrom);
		message.setSubject("Testing javamail plain");
		message.setContent("This is a test", "text/plain");
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				"rafiq.hbs@gmail.com"));
		Transport transport = session.getTransport("smtps");

		transport.connect("smtp.gmail.com", 465, "rafiqabdul10@gmail.com", "rafiq@abdul10");
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}
}