package com.aiotech.aios.common.action;

/*********************************************************************************************
 *
 * Create Log
 * -------------------------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 Description
 * -------------------------------------------------------------------------------------------
 * 1.0		30 Aug 2010 	Mohamed rafiq.S 		    Initial Version
 * 1.0		28 Oct 2010  	Nagarajan T.				URN Number - Mail service

 *********************************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.common.to.LoginTO;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.IdGenerator;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.SystemVersion;
import com.aiotech.aios.system.domain.entity.vo.Configuration;
import com.aiotech.aios.system.domain.entity.vo.Configurations;
import com.aiotech.aios.system.domain.entity.vo.MapEntryType;
import com.aiotech.aios.system.domain.entity.vo.MapType;
import com.aiotech.aios.system.domain.entity.vo.UserVO;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.web.interceptor.AIOSContextListner;
import com.aiotech.aios.web.locale.I18NSingleton;
import com.aiotech.aios.web.locale.SerializableResourceBundle;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.notify.enterprise.NotifyEnterpriseService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {

	private static final long serialVersionUID = -1841857629125037351L;
	private static final Logger LOGGER = LogManager
			.getLogger(LoginAction.class);
	private String empID;
	private String password;
	private String username;
	private String sessionErrMsg;
	private String currentDate;
	private String language;
	private String redirectValue;
	private String url;
	private int personId;
	private String personName;
	private String lastLoginDate;
	private Integer urnNumber;
	private String loginType;
	private String companyName;
	private int companyId;
	private long implementationId;
	private String passwordFlag;
	private String redirectPage;
	private String storeLogin;

	private String projectTitle;
	private String projectReference;
	private String projectCustomer;
	private String projectCreatedBy;
	private String projectLead;
	private String projectManager;
	private String projectPaymentTerms;
	private String penalityDescriptive;
	private String contractTerms;
	private String simpleSaveButton;
	private String updateSaveButton;
	private String sendRequisitionButton;
	private String projectAccessArea;

	private String productUnifiedPrice;
	private String posPrintOrder;
	private String posNextInvoice;
	private String posSaveOrderButton;
	private String posPrintOrderButton;
	private String penaltyMonths;
	private String pdcAlertDays;
	private String releaseNoticePeriod;
	private String imageSliderAccessArea;

	private String systemUrnOption;
	private String notifyUser;
	private String urnVerificationResult;
	private String jsonResponseString;
	private Integer currentPasswordAge;
	public Boolean isURNResendInEmail = null;

	LoginTO loginBean = null;

	private SystemService systemService;
	private NotifyEnterpriseService notifyEnterpriseService;

	private static final Properties COMMON_MESSAGE = new Properties();
	private static final String String = null;
	static {
		try {
			COMMON_MESSAGE.load(LoginAction.class.getClassLoader()
					.getResourceAsStream("common.message.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getHomePage() {
		storeLogin = null;
		return SUCCESS;
	}

	public String showConfigurationSetup() {
		try {
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Configurations configurations = getConfiguration(confProps);
			List<Configuration> list = configurations.getConfigurations();

			Configuration configuration = null;
			Configuration implconfiguration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == 200)
						configuration = conf;
					else if ((long) conf.getImplementation() == (long) getImplementation()
							.getImplementationId())
						implconfiguration = conf;
				}
			}
			List<MapEntryType> entryType = configuration.getAccessMap()
					.getEntry();
			projectAccessArea = "";
			for (MapEntryType entry : entryType) {
				if (entry.getKey().contains("project_access_area")) {
					projectAccessArea = entry.getValue();
					break;
				}
			}
			String[] accessAreas = projectAccessArea.split(",");
			StringBuilder sb = null;
			List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
			MapEntryType entry = null;
			for (int i = 0; i < accessAreas.length; i++) {
				sb = new StringBuilder();
				entry = new MapEntryType();
				for (int k = 0; k < accessAreas[i].length(); k++) {
					char ch = accessAreas[i].charAt(k);
					if (k != 0 && Character.isUpperCase(ch)) {
						sb.append(' ');
						sb.append(ch);
					} else
						sb.append(ch);
				}
				entry.setKey(accessAreas[i]);
				entry.setValue(WordUtils.capitalize(sb.toString()));
				entryTypes.add(entry);
			}
			ServletActionContext.getRequest().setAttribute(
					"PROJECT_ACCESS_AREAS", entryTypes);

			entryType = implconfiguration.getAccessMap().getEntry();
			projectAccessArea = "";
			for (MapEntryType mapentry : entryType) {
				if (mapentry.getKey().contains("project_access_area")) {
					projectAccessArea = mapentry.getValue();
					break;
				}
			}
			accessAreas = projectAccessArea.split(",");
			Map<String, String> accessAreaMap = new HashMap<String, String>();
			for (int i = 0; i < accessAreas.length; i++) {
				accessAreaMap.put(accessAreas[i], accessAreas[i]);
			}
			ServletActionContext.getRequest().setAttribute(
					"PROJECT_ACCESS_AREAMAP", accessAreaMap);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private Configurations getConfiguration(Properties confProps)
			throws Exception {
		Configurations configurations = null;

		JAXBContext jaxbContext = JAXBContext.newInstance(Configurations.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		configurations = (Configurations) jaxbUnmarshaller
				.unmarshal((new File(confProps.getProperty("file.drive")
						+ "aios/configurations.xml")));
		return configurations;
	}

	private void updateConfigurations(Properties confProps,
			Configurations configurations, Configuration configuration)
			throws Exception {
		JAXBContext jaxbContext1 = JAXBContext
				.newInstance(Configurations.class);

		Marshaller jaxbMarshaller = jaxbContext1.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		FileOutputStream os = new FileOutputStream(
				(new File(confProps.getProperty("file.drive")
						+ "aios/configurations.xml")));

		jaxbMarshaller.marshal(configurations, os);

		// Set into session
		HttpSession sSession = ServletActionContext.getRequest().getSession();
		if (configuration != null) {
			for (MapEntryType mapEntryType : configuration.getMap().getEntry()) {
				sSession.setAttribute(mapEntryType.getKey(),
						mapEntryType.getValue());
				ServletActionContext
						.getRequest()
						.getSession()
						.setAttribute(mapEntryType.getKey(),
								mapEntryType.getValue());
			}
			for (MapEntryType mapEntryType : configuration.getAccessMap()
					.getEntry()) {
				sSession.setAttribute(mapEntryType.getKey(),
						mapEntryType.getValue());
				ServletActionContext
						.getRequest()
						.getSession()
						.setAttribute(mapEntryType.getKey(),
								mapEntryType.getValue());
			}
		}
	}

	public String saveProjectConfiguration() {
		try {

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configurations configurations = getConfiguration(confProps);

			List<Configuration> list = configurations.getConfigurations();

			Configuration configuration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == (long) getImplementation()
							.getImplementationId())
						configuration = conf;
				}
			}

			MapType object = configuration.getMap();
			List<MapEntryType> entryType = object.getEntry();
			Map<String, String> configurationMap = new HashMap<String, String>();
			for (MapEntryType mapEntryType : entryType)
				configurationMap.put(mapEntryType.getKey(),
						mapEntryType.getValue());

			object = projectConfigurationSetKeyValue(configurationMap);

			configuration.setMap(object);

			MapType accessObject = configuration.getAccessMap();
			List<MapEntryType> entryAccessType = accessObject.getEntry();
			Map<String, String> accessConfigurationMap = new HashMap<String, String>();
			for (MapEntryType accessEntryType : entryAccessType)
				accessConfigurationMap.put(accessEntryType.getKey(),
						accessEntryType.getValue());

			accessObject = projectConfigurationSetAccess(accessConfigurationMap);

			configuration.setAccessMap(accessObject);

			configurations = new Configurations();

			configurations.setConfigurations(list);
			updateConfigurations(confProps, configurations, configuration);
			redirectValue = "Success";
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private MapType projectConfigurationSetKeyValue(
			Map<java.lang.String, java.lang.String> configurationMap)
			throws Exception {
		MapType mapType = new MapType();
		List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
		MapEntryType obj = null;
		for (Entry<String, String> entry : configurationMap.entrySet()) {
			obj = new MapEntryType();
			if (entry.getKey().contains("project_projectTitle")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectTitle);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_reference")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectReference);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_client")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectCustomer);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_createdby")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectCreatedBy);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_lead")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectLead);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_manager")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectManager);
				entryTypes.add(obj);
			} else {
				obj.setKey(entry.getKey());
				obj.setValue(entry.getValue());
				entryTypes.add(obj);
			}
		}
		mapType.setEntry(entryTypes);
		return mapType;
	}

	private MapType projectConfigurationSetAccess(
			Map<java.lang.String, java.lang.String> configurationMap)
			throws Exception {
		MapType mapType = new MapType();
		List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
		MapEntryType obj = null;
		for (Entry<String, String> entry : configurationMap.entrySet()) {
			obj = new MapEntryType();
			if (entry.getKey().contains("project_access_paymentTerms")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectPaymentTerms);
				entryTypes.add(obj);
			} else if (entry.getKey().contains(
					"project_access_penalityDescriptive")) {
				obj.setKey(entry.getKey());
				obj.setValue(penalityDescriptive);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_access_contractTerms")) {
				obj.setKey(entry.getKey());
				obj.setValue(contractTerms);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_simple_save_button")) {
				obj.setKey(entry.getKey());
				obj.setValue(simpleSaveButton);
				entryTypes.add(obj);
			} else if (entry.getKey()
					.contains("project_update_and_save_button")) {
				obj.setKey(entry.getKey());
				obj.setValue(updateSaveButton);
				entryTypes.add(obj);
			} else if (entry.getKey().contains(
					"project_send_requisition_button")) {
				obj.setKey(entry.getKey());
				obj.setValue(sendRequisitionButton);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("project_access_area")) {
				obj.setKey(entry.getKey());
				obj.setValue(projectAccessArea);
				entryTypes.add(obj);
			} else {
				obj.setKey(entry.getKey());
				obj.setValue(entry.getValue());
				entryTypes.add(obj);
			}
		}
		mapType.setEntry(entryTypes);
		return mapType;
	}

	public String saveAccountConfiguration() {
		try {

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configurations configurations = getConfiguration(confProps);

			List<Configuration> list = configurations.getConfigurations();

			Configuration configuration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == (long) getImplementation()
							.getImplementationId())
						configuration = conf;
				}
			}

			MapType object = configuration.getMap();
			List<MapEntryType> entryType = object.getEntry();
			Map<String, String> configurationMap = new HashMap<String, String>();
			for (MapEntryType mapEntryType : entryType)
				configurationMap.put(mapEntryType.getKey(),
						mapEntryType.getValue());

			object = accountsConfigurationSetKeyValue(configurationMap);

			configuration.setMap(object);

			MapType accessObject = configuration.getAccessMap();
			List<MapEntryType> entryAccessType = accessObject.getEntry();
			Map<String, String> accessConfigurationMap = new HashMap<String, String>();
			for (MapEntryType accessEntryType : entryAccessType)
				accessConfigurationMap.put(accessEntryType.getKey(),
						accessEntryType.getValue());

			accessObject = accountConfigurationSetAccess(accessConfigurationMap);

			configuration.setAccessMap(accessObject);

			configurations = new Configurations();

			configurations.setConfigurations(list);
			updateConfigurations(confProps, configurations, configuration);
			redirectValue = "Success";
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private MapType accountsConfigurationSetKeyValue(
			Map<java.lang.String, java.lang.String> configurationMap)
			throws Exception {
		MapType mapType = new MapType();
		List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
		MapEntryType obj = null;
		for (Entry<String, String> entry : configurationMap.entrySet()) {
			obj = new MapEntryType();
			if (entry.getKey().contains("pos_printorder")) {
				obj.setKey(entry.getKey());
				obj.setValue(posPrintOrder);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("pos_movetoinvoice")) {
				obj.setKey(entry.getKey());
				obj.setValue(posNextInvoice);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("unified_price")) {
				obj.setKey(entry.getKey());
				obj.setValue(productUnifiedPrice);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("pdc_alert_days")) {
				obj.setKey(entry.getKey());
				obj.setValue(pdcAlertDays);
				entryTypes.add(obj);
			} else {
				obj.setKey(entry.getKey());
				obj.setValue(entry.getValue());
				entryTypes.add(obj);
			}
		}
		mapType.setEntry(entryTypes);
		return mapType;
	}

	private MapType accountConfigurationSetAccess(
			Map<java.lang.String, java.lang.String> configurationMap)
			throws Exception {
		MapType mapType = new MapType();
		List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
		MapEntryType obj = null;
		for (Entry<String, String> entry : configurationMap.entrySet()) {
			obj = new MapEntryType();
			if (entry.getKey().contains("pos_printorder_button")) {
				obj.setKey(entry.getKey());
				obj.setValue(posPrintOrderButton);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("pos_saveorder_button")) {
				obj.setKey(entry.getKey());
				obj.setValue(posSaveOrderButton);
				entryTypes.add(obj);
			} else {
				obj.setKey(entry.getKey());
				obj.setValue(entry.getValue());
				entryTypes.add(obj);
			}
		}
		mapType.setEntry(entryTypes);
		return mapType;
	}

	public String saveRealEstateConfiguration() {
		try {

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configurations configurations = getConfiguration(confProps);

			List<Configuration> list = configurations.getConfigurations();

			Configuration configuration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == (long) getImplementation()
							.getImplementationId())
						configuration = conf;
				}
			}

			MapType object = configuration.getMap();
			List<MapEntryType> entryType = object.getEntry();
			Map<String, String> configurationMap = new HashMap<String, String>();
			for (MapEntryType mapEntryType : entryType)
				configurationMap.put(mapEntryType.getKey(),
						mapEntryType.getValue());

			object = realEstateConfigurationSetKeyValue(configurationMap);

			configuration.setMap(object);

			configurations = new Configurations();

			configurations.setConfigurations(list);
			updateConfigurations(confProps, configurations, configuration);
			redirectValue = "Success";
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private MapType realEstateConfigurationSetKeyValue(
			Map<java.lang.String, java.lang.String> configurationMap)
			throws Exception {
		MapType mapType = new MapType();
		List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
		MapEntryType obj = null;
		for (Entry<String, String> entry : configurationMap.entrySet()) {
			obj = new MapEntryType();
			if (entry.getKey().contains("penalty_months")) {
				obj.setKey(entry.getKey());
				obj.setValue(penaltyMonths);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("release_notice_period")) {
				obj.setKey(entry.getKey());
				obj.setValue(releaseNoticePeriod);
				entryTypes.add(obj);
			} else if (entry.getKey().contains("image_slider_access_area")) {
				obj.setKey(entry.getKey());
				obj.setValue(imageSliderAccessArea);
				entryTypes.add(obj);
			} else {
				obj.setKey(entry.getKey());
				obj.setValue(entry.getValue());
				entryTypes.add(obj);
			}
		}
		mapType.setEntry(entryTypes);
		return mapType;
	}

	public String saveSystemConfiguration() {
		try {

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configurations configurations = getConfiguration(confProps);

			List<Configuration> list = configurations.getConfigurations();

			Configuration configuration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == (long) getImplementation()
							.getImplementationId())
						configuration = conf;
				}
			}

			MapType object = configuration.getAccessMap();
			List<MapEntryType> entryType = object.getEntry();
			Map<String, String> configurationMap = new HashMap<String, String>();
			for (MapEntryType mapEntryType : entryType)
				configurationMap.put(mapEntryType.getKey(),
						mapEntryType.getValue());

			object = systemConfigurationSetAccess(configurationMap);

			configuration.setAccessMap(object);

			configurations = new Configurations();

			configurations.setConfigurations(list);
			updateConfigurations(confProps, configurations, configuration);
			redirectValue = "Success";
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private MapType systemConfigurationSetAccess(
			Map<java.lang.String, java.lang.String> configurationMap)
			throws Exception {
		MapType mapType = new MapType();
		List<MapEntryType> entryTypes = new ArrayList<MapEntryType>();
		MapEntryType obj = null;
		for (Entry<String, String> entry : configurationMap.entrySet()) {
			obj = new MapEntryType();
			if (entry.getKey().contains("system_urn_option")) {
				obj.setKey(entry.getKey());
				obj.setValue(systemUrnOption);
				entryTypes.add(obj);
			} else {
				obj.setKey(entry.getKey());
				obj.setValue(entry.getValue());
				entryTypes.add(obj);
			}
		}
		mapType.setEntry(entryTypes);
		return mapType;
	}

	public String methodRedirect() {

		Map<String, Object> session = ActionContext.getContext().getSession();
		HttpSession sessionObj = ServletActionContext.getRequest().getSession();
		redirectPage = "user_verification.action";
		UserDetails user = (UserDetails) ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication().getPrincipal();
		User user2 = systemService.getUser(user.getUsername());

		Implementation implementation = new Implementation();
		implementation.setImplementationId(user2.getImplementationId());
		Boolean isManagementLogin = false;
		Boolean isMobile = false;
		Boolean isFireFox = false;
		Boolean isPOSOrderSystem = false;
		Boolean forcePasswordUpdate = false;
		Boolean isPOSUser = false;
		Boolean isAdmin = false;

		try {
			List<UserRole> userRoles = systemService.getUserRoles(user
					.getUsername());
			if (userRoles != null && userRoles.size() > 0) {

				for (UserRole userRole : userRoles) {

					if (userRole.getRole().getIsAdmin() != null
							&& userRole.getRole().getIsAdmin()) {
						isAdmin = userRole.getRole().getIsAdmin();
						break;

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			implementation = systemService.getImplementation(implementation);
			Person currentPerson = systemService.getPerson(user2.getPersonId());
			String currentPersonName = WordUtils.capitalize(AIOSCommons
					.returnCompleteName(currentPerson.getFirstName(),
							currentPerson.getMiddleName(),
							currentPerson.getLastName()));
			ServletActionContext.getRequest().getSession()
					.setAttribute("THIS", implementation);
			ServletActionContext.getRequest().getSession()
					.setAttribute("USER", user2);
			ServletActionContext.getRequest().getSession()
					.setAttribute("USER_PERSON_NAME", currentPersonName);
			removeURNFromSession();

			try {
				List<UserRole> userRoles = systemService.getUserRoles(user2
						.getUsername());
				Boolean active = false;
				Boolean management = false;

				for (UserRole userRole : userRoles) {

					active = userRole.getRole().getIsActive();
					management = userRole.getRole().getIsManagement();

					if ((null != active && active == true)
							&& (null != management && management == true)) {

						isManagementLogin = true;
						break;
					}

					if (userRole.getRole().getRoleName()
							.equalsIgnoreCase("OrderingSystem"))
						isPOSOrderSystem = true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			loginBean = new LoginTO();
			LoginTO Beans = null;

			IdGenerator generator = new IdGenerator();
			String jSessionId = generator.generateId(16);
			long sessionId = System.currentTimeMillis();
			int urnNumber = (int) ((Math.floor(Math.random() * 900000)) + 100000);

			loginBean.setJSessionId(jSessionId);
			loginBean.setSessionId(sessionId);
			loginBean.setUrnNumber(urnNumber);
			Beans = new LoginTO();
			Beans.setSqlReturnStatus(1);
			Person person = null;
			if (null != user2.getPersonId()) {
				person = systemService.getPerson(user2.getPersonId());
			}
			Beans.setPersonId(8149);
			if (Beans.getSqlReturnStatus() == 1) {

				session.put("logged-in", "true");
				session.put("employee_ID", 15);
				session.put("emp_CODE", "15");
				session.put("roleID", "1");
				session.put("roleName", "RoleName");
				session.put("User_Name", "UserName");
				session.put("lang", "en");
				session.put("date", "2/1/2012");
				session.put("cu" + "rrentDate", "2/1/2012");
				session.put("personId", 8149);
				session.put("PERSON_ID", 8149);
				session.put("personName", Beans.getPersonName());
				session.put("PERSON_NAME", null != person ? person
						.getFirstName().concat(" " + person.getLastName())
						: Beans.getPersonName());
				session.put("jSessionId", jSessionId);
				session.put("lastLoginDate", Beans.getLastLoginDate());
				session.put("sessionId", sessionId);
				session.put("SESSION_ID", sessionId);
				session.put("emailId", Beans.getPersonName());
				session.put("mobileNo", Beans.getMobileNo());
				// Property Load
				Properties prop = new Properties();
				InputStream input = null;
				input = AIOSContextListner.class
						.getResourceAsStream("/dbconfig.properties");
				prop.load(input);

				Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				FileInputStream inputStream = new FileInputStream(prop
						.getProperty("SYNCFILE-LOCATION").trim());
				Properties posprops = new Properties();
				posprops.load(inputStream);
				inputStream.close();
				String keyValue = posprops.getProperty("OFFLINE-MODE").trim();
				if (keyValue.equalsIgnoreCase("Y"))
					session.put("offline", true);
				else
					session.put("offline", false);
				sessionObj.setAttribute(
						"POS_USERTILL_SESSION" + session.get("jSessionId"),
						null);
				sessionObj.removeAttribute("MENU_MAP");
				List<POSUserTill> posUserSession = systemService
						.getpOSUserTillDAO()
						.findByNamedQuery("getPosUserTillByPersonId",
								user2.getPersonId());

				if (null != posUserSession && posUserSession.size() > 0) {
					sessionObj.setAttribute(
							"POS_USERTILL_SESSION" + session.get("jSessionId"),
							posUserSession.get(0));
					isPOSUser = true;
				}

				LOGGER.info("Session ID in jSessionId	: ---->"
						+ (String) session.get("jSessionId"));
				LOGGER.info("Session ID for Transaction : ---->" + sessionId);
				loginBean.setPersonId((Integer) session.get("personId"));

				LOGGER.info("Module: System : method userVerification() : user= "
						+ ((session != null && session.get("employee_ID") != null) ? session
								.get("employee_ID").toString().trim()
								: "null")
						+ " : loggedin? = "
						+ ((session != null && session.get("logged-in") != null) ? session
								.get("logged-in").toString().trim()
								: "false") + " Action Success");
			} else {
				ServletActionContext.getRequest().setAttribute("aDMUserBean",
						loginBean);
				ServletActionContext.getRequest().setAttribute("errorMsg",
						"login.incorrect.user");
				ServletActionContext.getRequest().setAttribute("username",
						empID);
			}

			List<JobAssignment> jobAssignments = systemService
					.getJobAssignmentDAO().findByNamedQuery(
							"getJobAssignmentByPerson", user2.getPersonId());
			if (jobAssignments != null && jobAssignments.size() > 0)
				sessionObj.setAttribute("SESSION_JOB_ASSIGNEMNT_OBJECT"
						+ session.get("jSessionId"), jobAssignments.get(0));

			if (ServletActionContext.getRequest().getHeader("User-Agent")
					.indexOf("Firefox") != -1
					|| ServletActionContext.getRequest()
							.getHeader("User-Agent").indexOf("firefox") != -1) {

				isFireFox = true;
			}

			if (ServletActionContext.getRequest().getHeader("User-Agent")
					.indexOf("Mobile") != -1) {
				isMobile = true;

				ServletActionContext.getRequest().getSession()
						.setAttribute("IS_MOBILE", isMobile);
				ActionContext.getContext().getSession()
						.put("IS_MOBILE", isMobile);
				String returnStirng = "mobile";
				ServletActionContext.getRequest().setAttribute("IS_MOBILE",
						isMobile);
				LOGGER.info("returnStirng====>" + returnStirng + "--"
						+ isMobile);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Configuration File Reader
		Configurations configurations = null;
		List<Configuration> list = null;
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(Configurations.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			configurations = (Configurations) jaxbUnmarshaller
					.unmarshal((new File(confProps.getProperty("file.drive")
							+ "aios/configurations.xml")));

			list = configurations.getConfigurations();

			Configuration configuration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == (long) 200)
						configuration = conf;
					else if ((long) conf.getImplementation() == (long) implementation
							.getImplementationId())
						configuration = conf;
				}
			}

			// Set into session
			HttpSession sSession = ServletActionContext.getRequest()
					.getSession();
			if (configuration != null) {
				for (MapEntryType mapEntryType : configuration.getMap()
						.getEntry()) {
					sSession.setAttribute(mapEntryType.getKey(),
							mapEntryType.getValue());
					ServletActionContext
							.getRequest()
							.getSession()
							.setAttribute(mapEntryType.getKey(),
									mapEntryType.getValue());
				}
				for (MapEntryType mapEntryType : configuration.getAccessMap()
						.getEntry()) {
					sSession.setAttribute(mapEntryType.getKey(),
							mapEntryType.getValue());
					ServletActionContext
							.getRequest()
							.getSession()
							.setAttribute(mapEntryType.getKey(),
									mapEntryType.getValue());
				}
			}

			if (user2.getPasswordUpdateDate() == null) {

				try {
					user2.setPasswordUpdateDate(Calendar.getInstance()
							.getTime());
					systemService.saveOrUpdateUser(user2);
				} catch (Exception e) {
					e.printStackTrace();
				}
				forcePasswordUpdate = false;
			} else {
				forcePasswordUpdate = isForcePasswordUpdateRequired();
			}

			String showURNScreen = (java.lang.String) sessionObj
					.getAttribute("system_urn_option");

			if (isManagementLogin)
				redirectPage = "mobile_dashboard.action";

			else if (!isManagementLogin && !isFireFox)
				redirectPage = "logout.do";

			else if (isPOSOrderSystem)
				redirectPage = "pos_ordering_system.action";

			else if (((showURNScreen.equals("hide")) && !forcePasswordUpdate)
					|| isPOSUser || isAdmin)
				redirectPage = "user_selected_company.action";

			else if ((showURNScreen.equals("hide")) && forcePasswordUpdate)
				redirectPage = "forceUpdatePasswordRedirect.action?passwordFlag=C";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private void handleURNScreen(User user, Boolean isResendRequestedInEmail) {

		String URN = AIOSCommons.generateRandomURN();
		Person person = systemService.getPerson(user.getPersonId());
		String[] emailToShow = null;
		Boolean trySendingEmail = false;
		Boolean isEmailPriority = false;

		HttpSession sessionObj = ServletActionContext.getRequest().getSession();
		String sendURNPriorityMethod = (java.lang.String) sessionObj
				.getAttribute("urn_send_priority");

		if ((sendURNPriorityMethod != null
				&& sendURNPriorityMethod.equalsIgnoreCase("email") && isResendRequestedInEmail == null)
				|| (isResendRequestedInEmail != null && isResendRequestedInEmail)) {
			isEmailPriority = true;
		} else
			isEmailPriority = false;

		List<String> sendToMobile = new ArrayList<String>();
		sendToMobile.add(person.getMobile());

		List<String> sendToEmail = new ArrayList<String>();
		sendToEmail.add(person.getEmail());

		Boolean isMobileAvailable = !AIOSCommons.isNullOrEmptyString(person
				.getMobile());
		Boolean isEmailAvailable = !AIOSCommons.isNullOrEmptyString(person
				.getEmail());

		if (!isMobileAvailable && !isEmailAvailable) {

			LOGGER.warn("Unable to send URN, email or mobile number not available");
			notifyUser = "Mobile & Email not found for "
					+ person.getFirstName() + " (" + person.getPersonNumber()
					+ ") [FATAL ERROR: UNABLE TO LOGIN!]";

			return;
		}

		if (isMobileAvailable && !isEmailPriority) {

			Boolean smsSentSuccess = isMobileAvailable ? notifyEnterpriseService
					.sendSMS("Your ERP verification code is " + URN,
							sendToMobile, "en", getImplementation()
									.getImplementationId(), user.getPersonId(),
							isResendRequestedInEmail != null, null) : false;

			if (smsSentSuccess) {
				ServletActionContext.getRequest().getSession()
						.setAttribute("URN$#" + user.getUsername(), URN);
				notifyUser = "Verification code is sent in SMS to "
						+ person.getMobile().substring(0, 4)
						+ " xx xxxx"
						+ ((person.getMobile().length() >= 10) ? person
								.getMobile().substring(10) : "");

			} else {

				LOGGER.warn("SMS sending failed for URN, trying to send email instead");
				trySendingEmail = true;
			}

		}

		if (trySendingEmail || isEmailPriority) {

			Boolean emailSentSuccess = isEmailAvailable ? notifyEnterpriseService
					.sendEmail("ERP Login Code",
							"Your ERP verification code is " + URN,
							sendToEmail, "en", getImplementation()
									.getImplementationId(), user.getPersonId(),
							isResendRequestedInEmail != null, null) : false;

			if (emailSentSuccess) {

				ServletActionContext.getRequest().getSession()
						.setAttribute("URN$#" + user.getUsername(), URN);

				emailToShow = person.getEmail().split("@");
				notifyUser = "Verification code is sent in email to "
						+ emailToShow[0].substring(0,
								(emailToShow[0].length() / 2))
						+ AIOSCommons.returnAestarics(emailToShow[0].length()
								- (emailToShow[0].length() / 2)) + "@"
						+ emailToShow[1];

				if (trySendingEmail) {
					LOGGER.warn("SMS sending failed or mobile number not found for "
							+ person.getFirstName()
							+ " ("
							+ person.getPersonNumber()
							+ "), URN is sent in email.");
					notifyUser += "<br/>SMS sending failed or mobile number not found for "
							+ person.getFirstName()
							+ " ("
							+ person.getPersonNumber()
							+ "), URN is sent in email.";
				}
			} else {

				LOGGER.warn("Email sending failed for URN. Email address available: "
						+ isEmailAvailable
						+ " Person: "
						+ person.getFirstName()
						+ " - "
						+ person.getPersonNumber());

				if (isEmailPriority) {

					Boolean smsSentSuccess = isMobileAvailable ? notifyEnterpriseService
							.sendSMS("Your ERP verification code is " + URN,
									sendToMobile, "en", getImplementation()
											.getImplementationId(), user
											.getPersonId(),
									isResendRequestedInEmail != null, null)
							: false;

					if (smsSentSuccess) {
						ServletActionContext
								.getRequest()
								.getSession()
								.setAttribute("URN$#" + user.getUsername(), URN);
						notifyUser = "Verification code is sent in SMS to "
								+ person.getMobile().substring(0, 4)
								+ " xx xxxx" + person.getMobile().substring(10);
					} else {
						notifyUser = "Unable to send URN code (either by email or sms) for "
								+ person.getFirstName()
								+ " ("
								+ person.getPersonNumber()
								+ ") [FATAL ERROR: UNABLE TO LOGIN!]";
					}
				}
			}
		}
	}

	public String confirmLogout() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = null;
		removeURNFromSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		implementationId = implementation.getImplementationId();
		invalidateCurrentUser();
		return SUCCESS;
	}

	private void invalidateCurrentUser() {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(
					ServletActionContext.getRequest(),
					ServletActionContext.getResponse(), auth);
		}
		SecurityContextHolder.getContext().setAuthentication(null);
	}

	private void removeURNFromSession() {
		try {
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute("USER");
			ServletActionContext.getRequest().getSession()
					.setAttribute("URN$#" + user.getUsername(), null);
			ServletActionContext.getRequest().getSession()
					.setAttribute("URN_MSG$#" + user.getUsername(), notifyUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String resendURNCode() throws Exception {

		this.removeURNFromSession();
		return this.userVerification();
	}

	public String switchLanguage() {

		Map<String, Object> session = ActionContext.getContext().getSession();
		System.out.println("Current Locale --> " + session.get("lang"));
		session.put("lang", language);
		System.out.println("Switched to Locale --> " + session.get("lang"));

		String locale = language;
		SerializableResourceBundle bundle;
		I18NSingleton singletonObj = I18NSingleton.getInstance();

		if (language.equals("ar"))
			bundle = singletonObj.getBundleValueAr();
		else
			bundle = singletonObj.getBundleValueEn();

		ServletActionContext.getRequest().getSession()
				.setAttribute("bundle", bundle);

		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, new Locale(locale)));
		return SUCCESS;
	}

	public Implementation getImplementation() {
		return (Implementation) ServletActionContext.getRequest().getSession()
				.getAttribute("THIS");
	}

	public String defaultLogin() throws Exception {

		String returnStirng = "desktop";

		try {
			SystemVersion systemVersion = systemService.getSystemVersion();
			ServletActionContext
					.getRequest()
					.getSession()
					.setAttribute(
							"SYSTEM_VERSION",
							systemVersion == null ? "n/a" : systemVersion
									.getVersion());
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Map<String, Object> session = ActionContext.getContext()
					.getSession();
			loginBean = new LoginTO();
			Implementation implementation = null;
			String requestedAction = ServletActionContext.getRequest()
					.getRequestURI();
			String companyKey;

			if (requestedAction.contains("-login")) {

				try {
					String[] uriSplit = requestedAction.split("/");
					String uri = uriSplit[uriSplit.length - 1];
					companyKey = uri.split("-")[0];

					if (companyKey != null) {

						implementation = systemService
								.getImplementationByCompanyKey(companyKey);

						if (implementation != null) {

							ServletActionContext.getRequest().getSession()
									.setAttribute("THIS", implementation);

							ServletActionContext
									.getRequest()
									.getSession()
									.setAttribute(
											"bundle",
											ServletActionContext
													.getServletContext()
													.getAttribute("bundle"));

						} else {
							returnStirng = "bad_company";
						}
					} else {
						returnStirng = "bad_company";
					}
				} catch (Exception e) {
					returnStirng = "bad_company";
				}

			} else if (ServletActionContext.getRequest().getParameter("KEY") != null) {

				implementation = new Implementation();
				implementation.setImplementationId(Long
						.parseLong(ServletActionContext.getRequest()
								.getParameter("KEY")));
				implementation = systemService
						.getImplementation(implementation);

				ServletActionContext.getRequest().getSession()
						.setAttribute("THIS", implementation);

				ServletActionContext
						.getRequest()
						.getSession()
						.setAttribute(
								"bundle",
								ServletActionContext.getServletContext()
										.getAttribute("bundle"));

				if (ServletActionContext.getRequest().getHeader("User-Agent")
						.indexOf("Firefox") != -1
						|| ServletActionContext.getRequest()
								.getHeader("User-Agent").indexOf("firefox") != -1) {

					// nothing
				} else {
					returnStirng = "restricted_browser";
				}

			} else if (ServletActionContext.getRequest().getSession()
					.getAttribute("THIS") == null) {

				returnStirng = "bad_company";
			}

			if (session != null && (session.get("logged-in") != null)) {
				if ((session.get("logged-in")).equals("true")) {

					loginBean.setCompanyName((String) session
							.get("COMPANY_NAME"));
					ServletActionContext.getRequest().setAttribute("loginBean",
							loginBean);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnStirng = "bad_company";
		}
		LOGGER.info("Login Action Exit ====>");
		return returnStirng;
	}

	public String userVerification() throws Exception {

		LOGGER.info("URN Handler Invoked **");

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute("USER");

		if (ServletActionContext.getRequest().getSession()
				.getAttribute("URN$#" + user.getUsername()) == null) {

			handleURNScreen(user, isURNResendInEmail);
		}

		if (ServletActionContext.getRequest().getSession()
				.getAttribute("URN_MSG$#" + user.getUsername()) == null) {

			ServletActionContext.getRequest().getSession()
					.setAttribute("URN_MSG$#" + user.getUsername(), notifyUser);
		} else {

			notifyUser = (String) ServletActionContext.getRequest()
					.getSession()
					.getAttribute("URN_MSG$#" + user.getUsername());
		}

		return SUCCESS;
	}

	private String oldPassword;
	private String newPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getPasswordFlag() {
		return passwordFlag;
	}

	public void setPasswordFlag(String passwordFlag) {
		this.passwordFlag = passwordFlag;
	}

	private Boolean isForcePasswordUpdateRequired() {

		try {
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute("USER");
			HttpSession sessionObj = ServletActionContext.getRequest()
					.getSession();

			if (user.getPasswordUpdateDate() != null) {

				Double passwordAge = DateFormat.dayDifference(user
						.getPasswordUpdateDate(), Calendar.getInstance()
						.getTime());
				currentPasswordAge = passwordAge.intValue();

				String passwordUpdateDays = (java.lang.String) sessionObj
						.getAttribute("password_update_days");

				if (passwordUpdateDays != null) {

					Integer passwordDaysAgeLimit = Integer
							.parseInt(passwordUpdateDays);

					if (passwordDaysAgeLimit > 0
							&& currentPasswordAge > passwordDaysAgeLimit)

						return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public String returnSuccess() throws Exception {

		ServletActionContext.getRequest().setAttribute("passwordFlag",
				passwordFlag);

		try {
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute("USER");
			Double passwordAge = DateFormat.dayDifference(user
					.getPasswordUpdateDate(), Calendar.getInstance().getTime());
			currentPasswordAge = passwordAge.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (passwordFlag.equals("R")) {

			ServletActionContext.getRequest().setAttribute(
					"USERS_LIST",
					UserVO.convertToUserVO(systemService
							.getAllUsers(getImplementation())));
		}
		return SUCCESS;
	}

	public String changePassword() throws Exception {

		UserDetails user = (UserDetails) ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication().getPrincipal();
		User loggedUser = systemService.getUser(user.getUsername(),
				getImplementation());

		if (passwordFlag.equals("R")) {

			User user_ = systemService.getUser(username);
			return this.resetPassword(user_);
		}

		if (AIOSCommons.encrypt(oldPassword).equals(loggedUser.getPassword())) {

			loggedUser.setPassword(AIOSCommons.encrypt(newPassword));
			loggedUser.setPasswordUpdateDate(Calendar.getInstance().getTime());
			systemService.saveOrUpdateUser(loggedUser);
		} else {
			throw new Exception("INCORRECT OLD PASSWORD");
		}

		return SUCCESS;
	}

	public String changePasswordJson() throws Exception {

		UserDetails user = (UserDetails) ((SecurityContext) SecurityContextHolder
				.getContext()).getAuthentication().getPrincipal();
		User loggedUser = systemService.getUser(user.getUsername(),
				getImplementation());

		if (AIOSCommons.encrypt(oldPassword).equals(loggedUser.getPassword())) {

			loggedUser.setPassword(AIOSCommons.encrypt(newPassword));
			loggedUser.setPasswordUpdateDate(Calendar.getInstance().getTime());
			systemService.saveOrUpdateUser(loggedUser);

			ServletActionContext.getRequest().getSession()
					.setAttribute("USER", loggedUser);

			jsonResponseString = "success";
		} else {
			jsonResponseString = "incorrect_old";
		}

		return SUCCESS;
	}

	public String resetPassword(User user) throws Exception {

		user.setPassword(AIOSCommons.encrypt(newPassword));
		user.setPasswordUpdateDate(Calendar.getInstance().getTime());
		systemService.saveOrUpdateUser(user);

		return SUCCESS;
	}

	public String urnVerification() {

		if (ServletActionContext.getRequest().getHeader("User-Agent")
				.indexOf("Mobile") != -1) {
			Boolean isMobile = true;
			ServletActionContext.getRequest().setAttribute("IS_MOBILE",
					isMobile);
		}

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute("USER");
		String correctURN = (String) ServletActionContext.getRequest()
				.getSession().getAttribute("URN$#" + user.getUsername());

		if (correctURN != null && correctURN.equals(urnNumber + "")) {
			ServletActionContext.getRequest().getSession()
					.setAttribute("URN$#" + user.getUsername(), null);

			if (isForcePasswordUpdateRequired())
				urnVerificationResult = "FORCE_PASSWORD_UPDATE";
			else
				urnVerificationResult = "PASS";
		} else
			urnVerificationResult = "FAIL";

		return SUCCESS;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getUrl() {
		return url;
	}

	public String getRedirectValue() {
		return redirectValue;
	}

	public void setRedirectValue(String redirectValue) {
		this.redirectValue = redirectValue;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSessionErrMsg() {
		return sessionErrMsg;
	}

	public void setSessionErrMsg(String sessionErrMsg) {
		this.sessionErrMsg = sessionErrMsg;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getEmpid() {
		return empID;
	}

	public void setEmpid(String s) {
		this.empID = s;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String p) {
		this.password = p;
	}

	public String getEmpID() {
		return empID;
	}

	public void setEmpID(String empID) {
		this.empID = empID;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getUrnNumber() {
		return urnNumber;
	}

	public void setUrnNumber(Integer urnNumber) {
		this.urnNumber = urnNumber;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getImplementationId() {
		return implementationId;
	}

	public void setImplementationId(long implementationId) {
		this.implementationId = implementationId;
	}

	public String getRedirectPage() {
		return redirectPage;
	}

	public void setRedirectPage(String redirectPage) {
		this.redirectPage = redirectPage;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getProjectReference() {
		return projectReference;
	}

	public void setProjectReference(String projectReference) {
		this.projectReference = projectReference;
	}

	public String getProjectCustomer() {
		return projectCustomer;
	}

	public void setProjectCustomer(String projectCustomer) {
		this.projectCustomer = projectCustomer;
	}

	public String getProjectCreatedBy() {
		return projectCreatedBy;
	}

	public void setProjectCreatedBy(String projectCreatedBy) {
		this.projectCreatedBy = projectCreatedBy;
	}

	public String getProjectLead() {
		return projectLead;
	}

	public void setProjectLead(String projectLead) {
		this.projectLead = projectLead;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public String getProductUnifiedPrice() {
		return productUnifiedPrice;
	}

	public void setProductUnifiedPrice(String productUnifiedPrice) {
		this.productUnifiedPrice = productUnifiedPrice;
	}

	public String getPosPrintOrder() {
		return posPrintOrder;
	}

	public void setPosPrintOrder(String posPrintOrder) {
		this.posPrintOrder = posPrintOrder;
	}

	public String getPosNextInvoice() {
		return posNextInvoice;
	}

	public void setPosNextInvoice(String posNextInvoice) {
		this.posNextInvoice = posNextInvoice;
	}

	public String getPenaltyMonths() {
		return penaltyMonths;
	}

	public void setPenaltyMonths(String penaltyMonths) {
		this.penaltyMonths = penaltyMonths;
	}

	public String getReleaseNoticePeriod() {
		return releaseNoticePeriod;
	}

	public void setReleaseNoticePeriod(String releaseNoticePeriod) {
		this.releaseNoticePeriod = releaseNoticePeriod;
	}

	public String getProjectPaymentTerms() {
		return projectPaymentTerms;
	}

	public void setProjectPaymentTerms(String projectPaymentTerms) {
		this.projectPaymentTerms = projectPaymentTerms;
	}

	public String getPenalityDescriptive() {
		return penalityDescriptive;
	}

	public void setPenalityDescriptive(String penalityDescriptive) {
		this.penalityDescriptive = penalityDescriptive;
	}

	public String getSimpleSaveButton() {
		return simpleSaveButton;
	}

	public void setSimpleSaveButton(String simpleSaveButton) {
		this.simpleSaveButton = simpleSaveButton;
	}

	public String getUpdateSaveButton() {
		return updateSaveButton;
	}

	public void setUpdateSaveButton(String updateSaveButton) {
		this.updateSaveButton = updateSaveButton;
	}

	public String getSendRequisitionButton() {
		return sendRequisitionButton;
	}

	public void setSendRequisitionButton(String sendRequisitionButton) {
		this.sendRequisitionButton = sendRequisitionButton;
	}

	public String getContractTerms() {
		return contractTerms;
	}

	public void setContractTerms(String contractTerms) {
		this.contractTerms = contractTerms;
	}

	public String getPosSaveOrderButton() {
		return posSaveOrderButton;
	}

	public void setPosSaveOrderButton(String posSaveOrderButton) {
		this.posSaveOrderButton = posSaveOrderButton;
	}

	public String getPosPrintOrderButton() {
		return posPrintOrderButton;
	}

	public void setPosPrintOrderButton(String posPrintOrderButton) {
		this.posPrintOrderButton = posPrintOrderButton;
	}

	public String getSystemUrnOption() {
		return systemUrnOption;
	}

	public void setSystemUrnOption(String systemUrnOption) {
		this.systemUrnOption = systemUrnOption;
	}

	public String getProjectAccessArea() {
		return projectAccessArea;
	}

	public void setProjectAccessArea(String projectAccessArea) {
		this.projectAccessArea = projectAccessArea;
	}

	public String getImageSliderAccessArea() {
		return imageSliderAccessArea;
	}

	public void setImageSliderAccessArea(String imageSliderAccessArea) {
		this.imageSliderAccessArea = imageSliderAccessArea;
	}

	public String getNotifyUser() {
		return notifyUser;
	}

	public void setNotifyUser(String notifyUser) {
		this.notifyUser = notifyUser;
	}

	public String getUrnVerificationResult() {
		return urnVerificationResult;
	}

	public void setUrnVerificationResult(String urnVerificationResult) {
		this.urnVerificationResult = urnVerificationResult;
	}

	public String getJsonResponseString() {
		return jsonResponseString;
	}

	public void setJsonResponseString(String jsonResponseString) {
		this.jsonResponseString = jsonResponseString;
	}

	public Integer getCurrentPasswordAge() {
		return currentPasswordAge;
	}

	public void setCurrentPasswordAge(Integer currentPasswordAge) {
		this.currentPasswordAge = currentPasswordAge;
	}

	public Boolean getIsURNResendInEmail() {
		return isURNResendInEmail;
	}

	public void setIsURNResendInEmail(Boolean isURNResendInEmail) {
		this.isURNResendInEmail = isURNResendInEmail;
	}

	public String getStoreLogin() {
		return storeLogin;
	}

	public void setStoreLogin(String storeLogin) {
		this.storeLogin = storeLogin;
	}

	public NotifyEnterpriseService getNotifyEnterpriseService() {
		return notifyEnterpriseService;
	}

	public void setNotifyEnterpriseService(
			NotifyEnterpriseService notifyEnterpriseService) {
		this.notifyEnterpriseService = notifyEnterpriseService;
	}

	public String getPdcAlertDays() {
		return pdcAlertDays;
	}

	public void setPdcAlertDays(String pdcAlertDays) {
		this.pdcAlertDays = pdcAlertDays;
	}
}
