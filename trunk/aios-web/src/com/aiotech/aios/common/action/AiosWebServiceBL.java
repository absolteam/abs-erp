package com.aiotech.aios.common.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.service.bl.PointOfSaleBL;
import com.aiotech.aios.hr.service.bl.AttendanceBL;

public class AiosWebServiceBL {
	@Autowired
	private PointOfSaleBL pointOfSaleBL;
	
	@Autowired
	private AttendanceBL attendanceBL;

	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public AttendanceBL getAttendanceBL() {
		return attendanceBL;
	}

	public void setAttendanceBL(AttendanceBL attendanceBL) {
		this.attendanceBL = attendanceBL;
	}
}
