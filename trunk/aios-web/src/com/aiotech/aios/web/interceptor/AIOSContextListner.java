package com.aiotech.aios.web.interceptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.aiotech.aios.accounts.service.bl.PosStagingSyncBL;
import com.aiotech.aios.accounts.service.bl.ProductBL;
import com.aiotech.aios.reporting.service.bl.AccountsJob;
import com.aiotech.aios.reporting.service.bl.AccountsJobBL;
import com.aiotech.aios.reporting.service.bl.HrBL;
import com.aiotech.aios.reporting.service.bl.HrJob;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.AlertJob;
import com.aiotech.aios.web.locale.I18NInterface;
import com.aiotech.aios.web.locale.SerializableResourceBundle;

public class AIOSContextListner implements ServletContextListener {

	private final String PROD = "production";
	private static final Logger LOGGER = LogManager
			.getLogger(AIOSContextListner.class);

	/**
	 * Default constructor.
	 */
	public AIOSContextListner() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent event) {

		ServletContext context = event.getServletContext();

		try {
			WebApplicationContext ctx = WebApplicationContextUtils
					.getWebApplicationContext(context);

			SystemService service = (SystemService) ctx
					.getBean("systemService");
			SerializableResourceBundle bundle = new I18NInterface()
					.callgetSingleton("en", "all", service);
			context.setAttribute("bundle", bundle);

			context.setAttribute("COUNTRIES", service.getAllCountries());
			context.setAttribute("CITIES", service.getAllCities());
			context.setAttribute("STATES", service.getAllStates());

			Properties props = new Properties();
			props.load(event.getServletContext().getResourceAsStream(
					File.separator + "WEB-INF" + File.separator + "aios.conf"));
			event.getServletContext().setAttribute("confProps", props);
			event.getServletContext().setAttribute("SERVER_ID", "n/a");
			event.getServletContext().setAttribute("UP_SINCE",
					Calendar.getInstance().getTime().toString());

			Boolean isProdServer = false;

			{
				/*
				 * AccountsJobBL accountsJobBL = (AccountsJobBL) ctx
				 * .getBean("accountsJobBL");
				 * accountsJobBL.getProductPricingBL()
				 * .updateProductSellingPrice();
				 */
			}
			// POS Configs

			Properties prop = new Properties();
			InputStream input = null;
			input = AIOSContextListner.class
					.getResourceAsStream("/dbconfig.properties");
			prop.load(input);

			FileInputStream inputStream = new FileInputStream(prop.getProperty(
					"SYNCFILE-LOCATION").trim());
			Properties posprops = new Properties();
			posprops.load(inputStream);
			inputStream.close();

			try {
				FileInputStream inputStream2 = new FileInputStream(prop
						.getProperty("SERVER-IDENTITY-LOCATION").trim());
				Properties server_identity_props = new Properties();
				server_identity_props.load(inputStream2);
				inputStream2.close();

				if ((null != server_identity_props.getProperty("env"))
						&& server_identity_props.getProperty("env").trim()
								.equalsIgnoreCase(PROD)) {
					isProdServer = true;
					LOGGER.info("Production Enviornment Detected!");
				}

				if (null != server_identity_props.getProperty("id")) {
					event.getServletContext().setAttribute("SERVER_ID",
							server_identity_props.getProperty("id"));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			String keyValue = posprops.getProperty("POSENV").trim();

			// Rafiq Code - for alert
			// -------------Contract Payment
			// reminder-------------------------------------
			if (keyValue.equalsIgnoreCase("N")) {

				// Ahsan M. - Only configure alert on production live server
				if (isProdServer) {

					{
						AlertBL alertBL = (AlertBL) ctx.getBean("alertBL");
						// specify sceduler task details
						JobDetail job = new JobDetail();
						job.setName("alertJob");
						job.setJobClass(AlertJob.class);
						job.setGroup("CCD");
						Map dataMap = job.getJobDataMap();
						dataMap.put("contractChequeDeposit", alertBL);

						// configure the scheduler time
						CronTrigger trigger = new CronTrigger();
						trigger.setName("alertJob");

						// That'll run a job at 10am and 1pm. That's twice a day
						trigger.setCronExpression("* 0 8 * * ?");
						trigger.setGroup("CCD");

						// schedule it
						Scheduler scheduler = new StdSchedulerFactory()
								.getScheduler();
						scheduler.start();
						scheduler.scheduleJob(job, trigger);
					}
					// Rent Payment Schedule
					{
						AlertBL alertBL = (AlertBL) ctx.getBean("alertBL");
						// specify sceduler task details
						JobDetail job = new JobDetail();
						job.setName("alertJob");
						job.setJobClass(AlertJob.class);
						job.setGroup("RPS");
						Map dataMap = job.getJobDataMap();
						dataMap.put("rentPaymentSchedule", alertBL);

						// configure the scheduler time
						CronTrigger trigger = new CronTrigger();
						trigger.setName("alertJob");

						// Run daily day time @ 12PM (noon)
						trigger.setCronExpression("* 0 12 * * ?");//
						trigger.setGroup("RPS");

						// schedule it
						Scheduler scheduler = new StdSchedulerFactory()
								.getScheduler();
						scheduler.start();
						scheduler.scheduleJob(job, trigger);

						// POS CONFIGS

					}
					// ----------------------HR Daily Morning Schedule
					// Configuration------------------------------
					{
						HrBL hrBL = (HrBL) ctx.getBean("hrBL");
						// specify sceduler task details
						JobDetail hrjob = new JobDetail();
						hrjob.setName("hrBL");
						hrjob.setJobClass(HrJob.class);
						hrjob.setGroup("hrDailyMorningSchedule");
						Map hrMap = hrjob.getJobDataMap();
						hrMap.put("hrDailyMorningSchedule", hrBL);

						// configure the scheduler time
						CronTrigger hrtrigger = new CronTrigger();
						hrtrigger.setName("hrBL");

						// Run daily day time @ 12PM (noon)
						hrtrigger.setCronExpression("* 0 09 * * ?");//
						hrtrigger.setGroup("hrDailyMorningSchedule");

						// schedule it
						Scheduler hrscheduler = new StdSchedulerFactory()
								.getScheduler();
						hrscheduler.start();
						hrscheduler.scheduleJob(hrjob, hrtrigger);
					}

					// ----------------------Accounts Daily Morning Schedule
					// Configuration------------------------------
					{
						AccountsJobBL accountsJobBL = (AccountsJobBL) ctx
								.getBean("accountsJobBL");
						// specify sceduler task details
						JobDetail accountsjob = new JobDetail();
						accountsjob.setName("accountsJobBL");
						accountsjob.setJobClass(AccountsJob.class);
						accountsjob.setGroup("accountsDailyMorningSchedule");
						Map accountsMap = accountsjob.getJobDataMap();
						accountsMap.put("accountsDailyMorningSchedule",
								accountsJobBL);

						// POS Staging Sync - Sending data from Production to
						// Staging database
						PosStagingSyncBL posStagingSyncBL = (PosStagingSyncBL) ctx
								.getBean("posStagingSyncBL");
						posStagingSyncBL.syncPosTablesToStaging();
						accountsMap
								.put("dailyPOSStagingSync", posStagingSyncBL);

						// configure the scheduler time
						CronTrigger accountstrigger = new CronTrigger();
						accountstrigger.setName("accountsJobBL");

						// Run daily day time @ 12PM (noon)
						accountstrigger.setCronExpression("* 0 12 * * ?");
						accountstrigger
								.setGroup("accountsDailyMorningSchedule");

						// schedule it
						Scheduler accountsscheduler = new StdSchedulerFactory()
								.getScheduler();
						accountsscheduler.start();
						accountsscheduler.scheduleJob(accountsjob,
								accountstrigger);

						/*
						 * accountsJobBL.getAccountBL().updateSegments();
						 * accountsJobBL.getReceiveBL().updateReceiveDetailPO();
						 * //
						 * accountsJobBL.getLedgerFiscalBL().processLedgerFiscal
						 * ();
						 * 
						 * /* accountsJobBL.getProductPricingBL().getProductBL()
						 * .updateProductUnit();/
						 * accountsJobBL.getSalesUpdateJob().getPointOfSaleBL
						 * ().updatePointOfSaleReference();
						 * 
						 * accountsJobBL.getProductPricingBL().getProductBL()
						 * .updateExpenseProductCombination();
						 * accountsJobBL.getSalesUpdateJob
						 * ().getPointOfSaleBL().updatePointOfSaleReference();
						 * accountsJobBL
						 * .getProductPricingBL().getProductBL().getCombinationBL
						 * ().createAnalysisAccountFromFile();
						 * accountsJobBL.getProjectBL
						 * ().getPettyCashBL().updatePettyCashTransaction();
						 */

					}

					{
						// ------------------Work flow notification
						// scheduler-----------------------------------
						/*
						 * WorkFlowBL workFlowBL = (WorkFlowBL)
						 * ctx.getBean("workFlowBL"); // specify sceduler task
						 * details JobDetail workflowjob = new JobDetail();
						 * workflowjob.setName("workFlowBL");
						 * workflowjob.setJobClass(WorkFlowJob.class);
						 * workflowjob.setGroup("Workflow"); Map workflowdataMap
						 * = workflowjob.getJobDataMap();
						 * workflowdataMap.put("Workflow", workFlowBL);
						 * 
						 * // configure the scheduler time CronTrigger
						 * workflowTrigger = new CronTrigger();
						 * workflowTrigger.setName("workflowJob");
						 * 
						 * // Evary 5 mins once
						 * workflowTrigger.setCronExpression("0 0/15 * * * ?");
						 * workflowTrigger.setGroup("Workflow");
						 * 
						 * // schedule it Scheduler workflowScheduler = new
						 * StdSchedulerFactory() .getScheduler();
						 * workflowScheduler.start();
						 * workflowScheduler.scheduleJob(workflowjob,
						 * workflowTrigger);
						 */
					}
					/*
					 * { // ------------------Hr Table Update tigger-- Temporary
					 * method // scheduler-----------------------------------
					 * JobBL jobBL = (JobBL) ctx.getBean("jobBL");
					 * jobBL.hrBuildUpdateScript();
					 * 
					 * // ------------------Real Estate Table Update tigger--
					 * Temporary method RealEstateBL realEstateBL=(RealEstateBL)
					 * ctx.getBean("realEstateBL");
					 * realEstateBL.realEstateBuildUpdateScript();
					 * 
					 * // ------------------- Accounts Update script
					 * -------------------------- AccountsBL accountsBL =
					 * (AccountsBL) ctx.getBean("accountsBL");
					 * accountsBL.accountsBuildUpdateScript(); }
					 */

					// Extract product menu images to local context, if not
					// already
					// present

					String overwriteImages = props
							.getProperty("overwrite.menu.images");

					ProductBL productBL = (ProductBL) ctx.getBean("productBL");
					productBL.extractImagesForOrderingMenu(props, context,
							overwriteImages.equalsIgnoreCase("true"), null);

				} else {
					LOGGER.warn("Initiation scripts are skipped; as this is not a production server!");
				}

			} else {
				AccountsJobBL accountsJobBL = (AccountsJobBL) ctx
						.getBean("accountsJobBL");
				// accountsJobBL.getSalesUpdateJob().updateSalesSync();
				// specify sceduler task details
				JobDetail accountsjob = new JobDetail();
				accountsjob.setName("accountsJobBL");
				accountsjob.setJobClass(AccountsJob.class);
				accountsjob.setGroup("everyTwoHrsPOSSalesSync");
				Map accountsMap = accountsjob.getJobDataMap();
				accountsMap.put("everyTwoHrsPOSSalesSync", accountsJobBL);

				// configure the scheduler time
				CronTrigger accountstrigger = new CronTrigger();
				accountstrigger.setName("accountsJobBL");

				// Every two hrs
				accountstrigger.setCronExpression("0 0 0/2 * * ?");
				accountstrigger.setGroup("everyTwoHrsPOSSalesSync");

				// schedule it
				Scheduler accountsscheduler = new StdSchedulerFactory()
						.getScheduler();
				accountsscheduler.start();
				accountsscheduler.scheduleJob(accountsjob, accountstrigger);

			}

			// Onload Status Update For paid receipt and payments
			/*
			 * { DirectPaymentBL directPaymentBL = (DirectPaymentBL)
			 * ctx.getBean("directPaymentBL");
			 * directPaymentBL.paymentStatusUpdateCall();
			 * 
			 * BankReceiptsBL bankReceiptsBL = (BankReceiptsBL)
			 * ctx.getBean("bankReceiptsBL");
			 * bankReceiptsBL.receiptStatusUpdateCall(); }
			 * accountsJobBL.getDirectPaymentBL
			 * ().getTransactionBL().updateReceiptReference();
			 */

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
