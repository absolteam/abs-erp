package com.aiotech.aios.web.interceptor;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LocaleInterceptor implements Interceptor {

	private static final long serialVersionUID = 7600945200636332046L;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();
			Map<String, Object> session = ActionContext.getContext()
					.getSession();

			if (session != null && session.get("lang") != null) {

				String locale = request.getParameter("locale");
				locale = (session.get("lang").toString() != null && !session
						.get("lang").toString().equalsIgnoreCase("")) ? session
						.get("lang").toString() : "ar";

				ResourceBundle bundle = (ResourceBundle) ServletActionContext
						.getRequest().getSession().getAttribute("bundle");

				if (bundle == null)
					bundle = (ResourceBundle) ServletActionContext
							.getServletContext().getAttribute("bundle");

				javax.servlet.jsp.jstl.core.Config.set(
						ServletActionContext.getRequest(),
						Config.FMT_LOCALIZATION_CONTEXT,
						new LocalizationContext(bundle, new Locale(locale)));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return invocation.invoke();
	}
}
