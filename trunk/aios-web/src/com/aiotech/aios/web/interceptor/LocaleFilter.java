package com.aiotech.aios.web.interceptor;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import org.apache.struts2.ServletActionContext;

import sun.print.resources.serviceui;

import com.opensymphony.xwork2.ActionContext;

public class LocaleFilter implements Filter {

	private ServletContext context;

	public LocaleFilter() {
		// TODO Auto-generated constructor stub
	}

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		Map<String, Object> session = ActionContext.getContext().getSession();
		String locale = request.getParameter("locale");

		locale = (session.get("lang").toString() != null && !session
				.get("lang").toString().equalsIgnoreCase("")) ? session.get(
				"lang").toString() : "en";

		System.out.println("Getting Resource Bundle");
		ResourceBundle bundle = (ResourceBundle) ServletActionContext
				.getRequest().getSession().getAttribute("bundle");

		if (bundle == null)
			bundle = (ResourceBundle) ServletActionContext.getServletContext()
					.getAttribute("bundle");

		System.out.println(bundle);
		javax.servlet.jsp.jstl.core.Config.set(ServletActionContext
				.getRequest(), Config.FMT_LOCALIZATION_CONTEXT,
				new LocalizationContext(bundle, new Locale(locale)));

		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		context = fConfig.getServletContext();
	}
}
