package com.aiotech.aios.web.interceptor;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.notify.enterprise.NotifyEnterpriseService;

public class CustomFilter implements Filter {

	FilterConfig config;
	private static final Logger LOGGER = LogManager
			.getLogger(CustomFilter.class);

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse httpResponse = (HttpServletResponse) res;
		User user = (User) request.getSession().getAttribute("USER");

		if (user != null && request.getRequestURI().contains(".action")) {

			String urnPresent = (String) request.getSession().getAttribute(
					"URN$#" + user.getUsername());

			// if urn is not yet passed and other than allowed action is
			// requested; log the by-pass effort and redirect to login page
			if (urnPresent != null
					&& !request.getRequestURI().contains(
							"user_verification.action")
					&& !request.getRequestURI().contains(
							"urn_resend_verification.action")
					&& !request.getRequestURI().contains(
							"urn_verification.action")
					&& !request.getRequestURI().contains(
							"confirm_logout.action")) {

				String warn = "URN by-passing effort detected; re-login will be requested - User: "
						+ user.getUsername()
						+ " | IP: "
						+ request.getRemoteAddr()
						+ " | Time: "
						+ new Date().toString();
				LOGGER.warn(warn);

				try {
					WebApplicationContext ctx = WebApplicationContextUtils
							.getWebApplicationContext(getConfig()
									.getServletContext());
					((NotifyEnterpriseService) ctx
							.getBean("notifyEnterpriseService"))
							.logSystemMessage(
									CustomFilter.class.getSimpleName(),
									"Security", warn,
									getImplementationId(request));
				} catch (Exception e) {
					e.printStackTrace();
				}

				httpResponse.sendRedirect("/" + request.getContextPath()
						+ "/user_verification.action");

			}
		}
		chain.doFilter(req, res);
	}

	public void init(FilterConfig config) throws ServletException {
		this.setConfig(config);
	}

	public void destroy() {
		// add code to release any resource
	}

	public FilterConfig getConfig() {
		return config;
	}

	public void setConfig(FilterConfig config) {
		this.config = config;
	}

	private Long getImplementationId(HttpServletRequest request) {
		if (request.getSession().getAttribute("THIS") != null)
			return ((Implementation) request.getSession().getAttribute("THIS"))
					.getImplementationId();
		else
			return null;
	}
}
