package com.aiotech.aios.web.interceptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;

public class WorkflowApprovalFilter implements Filter {
	FilterConfig config;
	private static final Logger LOGGER = LogManager
			.getLogger(WorkflowApprovalFilter.class);

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse httpResponse = (HttpServletResponse) res;
		try {

			WebApplicationContext ctx = WebApplicationContextUtils
					.getWebApplicationContext(getConfig().getServletContext());

			Long messageId = Long.valueOf(request.getParameter("processKey")
					.toString());
			WorkflowEnterpriseService serviceObject = (WorkflowEnterpriseService) ctx
					.getBean("workflowEnterpriseService");
			Message message = serviceObject.getGenerateNotificationsBL()
					.getMessageService().getMessageInfoByMessageId(messageId);
			if ((byte) message.getStatus() == 0
					&& (byte) message.getWorkflowDetailProcess()
							.getProcessType() == 2) {
				List<UserRole> userRoles = serviceObject
						.getGenerateNotificationsBL()
						.getMessageService()
						.getAllUserRoleByRoleId(
								message.getWorkflowDetailProcess()
										.getWorkflowDetail().getRole());
				User user = null;
				for (UserRole userRole : userRoles) {
					if ((long) userRole.getUser().getPersonId() == (long) message
							.getPersonId()) {
						user = userRole.getUser();
					}
				}

				if (user != null && autoLogin(request, user)) {
					httpResponse.sendRedirect(request.getContextPath()
							+ "/workflow_approval_email.action?processKey="
							+ messageId);
				} else {
					httpResponse.sendRedirect(request.getContextPath()
							+ "/index_main.action");
				}
			} else {
				httpResponse.sendRedirect(request.getContextPath()
						+ "/WorkflowError.jsp");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		chain.doFilter(req, res);
	}

	public boolean autoLogin(HttpServletRequest request, User user) {
		try {
			String username = user.getUsername();
			String password = user.getPassword();
			UserDetails userDetail = new org.springframework.security.core.userdetails.User(
					username, password, true, true, true, true,
					getAuthorities());
			Authentication authentication = new UsernamePasswordAuthenticationToken(
					userDetail, null, userDetail.getAuthorities());
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
			HttpSession session = request.getSession(true);
			session.setAttribute("SPRING_SECURITY_CONTEXT", authentication);
		} catch (Exception e) {
			SecurityContextHolder.getContext().setAuthentication(null);
			LOGGER.error("Exception", e);
			return false;
		}
		return true;
	}

	public Collection<GrantedAuthority> getAuthorities() {
		// make everyone ROLE_USER
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		GrantedAuthority grantedAuthority = new GrantedAuthority() {
			// anonymous inner type
			public String getAuthority() {
				return "ROLE_USER";
			}
		};
		grantedAuthorities.add(grantedAuthority);
		return grantedAuthorities;
	}

	public void init(FilterConfig config) throws ServletException {
		this.setConfig(config);
	}

	public void destroy() {
		// add code to release any resource
	}

	public FilterConfig getConfig() {
		return config;
	}

	public void setConfig(FilterConfig config) {
		this.config = config;
	}

}
