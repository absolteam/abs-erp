package com.aiotech.aios.web.locale;

import org.apache.log4j.Logger;
import com.aiotech.aios.system.service.SystemService;

public class I18NInterface {

	@SuppressWarnings("unused")
	private static final Logger LOGGERS = Logger.getLogger(I18NInterface.class);
	I18NSingleton singletonObj;

	@SuppressWarnings("static-access")
	public SerializableResourceBundle callgetSingleton(String lang,
			String modulcode, SystemService systemService) {

		try {
			singletonObj = I18NSingleton.getInstance();
			singletonObj.setBundleValue(lang, modulcode, systemService);

			if (lang != null && lang.equals("ar"))
				return singletonObj.getBundleValueAr();
			else
				return singletonObj.getBundleValueEn();
		} catch (Exception e) {
			e.printStackTrace();
			return singletonObj.getBundleValue();
		}
	}

	public void callSetSingleton(String lang, String modulcode,
			SystemService systemService) {

		try {
			singletonObj = I18NSingleton.getInstance();
			singletonObj.setBundleValue(lang, modulcode, systemService);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
