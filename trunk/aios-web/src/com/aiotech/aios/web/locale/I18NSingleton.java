package com.aiotech.aios.web.locale;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

import com.aiotech.aios.system.service.SystemService;

public class I18NSingleton {

	private static final I18NSingleton singleton = new I18NSingleton();
	private static SerializableResourceBundle finalBundle = null;
	private SerializableResourceBundle bundle = null;
	private SerializableResourceBundle arbundle = null;
	private SerializableResourceBundle finalBundles = null;
	private Locale locale;

	private I18NSingleton() {

	}

	public void setBundleValue(String langcode, String modulecode,
			SystemService systemService) throws IllegalAccessException,
			InstantiationException, IOException, InvocationTargetException {

		String lan = "", module = "";

		if (modulecode != null && !modulecode.trim().equalsIgnoreCase(""))
			module = modulecode;
		else
			module = "all";

		{
			lan = "ar";
			locale = new Locale(lan, module);
			Locale.setDefault(locale);
			ClassLoader loader = null;
			arbundle = (SerializableResourceBundle) DbResourceBundle
					.getMyControl(systemService).newBundle("BaseNames", locale,
							"db", loader, true);
			finalBundles = arbundle;
		}

		{
			lan = "en";
			locale = new Locale(lan, module);
			Locale.setDefault(locale);
			ClassLoader loader = null;
			bundle = (SerializableResourceBundle) DbResourceBundle
					.getMyControl(systemService).newBundle("BaseNames", locale,
							"db", loader, true);
			finalBundle = bundle;

		}
	}

	public static I18NSingleton getInstance() {
		return singleton;
	}

	public static SerializableResourceBundle getBundleValue() {
		return finalBundle;
	}

	public SerializableResourceBundle getBundleValues() {
		return finalBundles;
	}

	public SerializableResourceBundle getBundleValueEn() {
		return bundle;
	}

	public SerializableResourceBundle getBundleValueAr() {
		return arbundle;
	}
}
