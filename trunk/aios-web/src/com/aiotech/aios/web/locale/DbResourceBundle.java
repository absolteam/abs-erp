package com.aiotech.aios.web.locale;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import com.aiotech.aios.system.domain.entity.I18NMessages;
import com.aiotech.aios.system.service.SystemService;

public class DbResourceBundle extends SerializableResourceBundle {

	private static final long serialVersionUID = 2389480790714406457L;
	private Properties properties;

	private static final Logger LOGGERS = Logger
			.getLogger(DbResourceBundle.class);

	public static ResourceBundle.Control getMyControl(
			final SystemService systemService) {

		return new ResourceBundle.Control() {
			@Override
			public List<String> getFormats(String baseName) {
				if (baseName == null) {
					throw new NullPointerException();
				}
				return Arrays.asList("db");
			}

			public SerializableResourceBundle newBundle(String baseName,
					Locale locale, String format, ClassLoader loader,
					boolean reload) throws IllegalAccessException,
					InstantiationException, IOException {

				SerializableResourceBundle bundle = null;
				List<I18NMessages> i18nMessages = null;
				if ((baseName == null) || (locale == null) || (format == null))
					throw new NullPointerException();

				if (format.equals("db")) {

					Properties FMT_MESSAGE_PROPERTIES = new Properties();

					try {
						if (locale.getLanguage() != null
								&& locale.getCountry() != null
								&& !locale.getCountry().equals("")
								&& !locale.getLanguage().equals("")) {

							if (locale.getCountry() != null
									&& !locale.getCountry().equalsIgnoreCase(
											"All")
									&& !locale.getCountry().equals(""))

								i18nMessages = systemService
										.getI18NMessages(locale.getLanguage());

							else
								i18nMessages = systemService
										.getI18NMessages(locale.getLanguage());

							LOGGERS.info("I18NMessages Fetched Size -> "
									+ i18nMessages.size());

							if (i18nMessages.size() > 0) {

								for (int i = 0; i < i18nMessages.size(); i++) {
									FMT_MESSAGE_PROPERTIES
											.setProperty(i18nMessages.get(i)
													.getMessageKey(),
													i18nMessages.get(i)
															.getMessageValue());
								}
							} else {

								LOGGERS.info("i18n record is empty");
							}
						}

					} catch (Exception e) {

						i18nMessages = null;
						e.printStackTrace();
						throw new RuntimeException("FMT Properties: " + e);
					}

					bundle = new DbResourceBundle(FMT_MESSAGE_PROPERTIES);
				}
				return bundle;
			}

			@Override
			public long getTimeToLive(String baseName, Locale locale) {
				return 1000 * 60 * 30;
			}

			@Override
			public boolean needsReload(String baseName, Locale locale,
					String format, ClassLoader loader, ResourceBundle bundle,
					long loadTime) {
				return true;
			}
		};
	}

	public DbResourceBundle(Properties inProperties) {
		properties = inProperties;
	}

	@SuppressWarnings("unchecked")
	public Enumeration<String> getKeys() {
		return properties != null ? ((Enumeration<String>) properties
				.propertyNames()) : null;
	}

	protected Object handleGetObject(String key) {
		return properties.getProperty(key);
	}
}
