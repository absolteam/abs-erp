package com.aiotech.aios.web.webservices;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.action.EODBalancingAction;
import com.aiotech.aios.accounts.domain.entity.EODBalancing;
import com.aiotech.aios.accounts.domain.entity.EODBalancingDetail;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.service.bl.EODBalancingBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

public class WebServiceHelperAction extends ActionSupport {

	private static final long serialVersionUID = -6930039403924778262L;
	private static final Logger log = Logger
			.getLogger(WebServiceHelperAction.class);
	private String eodDetails;
	private String currentDate;
	private String returnMessage;
	private String description;
	private EODBalancingBL eodBalancingBL;

	public String processEODWebService() {
		try {
			log.info("Inside Module: Accounts : Method: processEODWebService"); 
			Properties prop = new Properties();
			InputStream input = null;
			input = EODBalancingAction.class
					.getResourceAsStream("/dbconfig.properties");
			prop.load(input);
			FileInputStream inputStream = new FileInputStream(prop.getProperty(
					"SYNCFILE-LOCATION").trim());
			Properties props = new Properties();
			props.load(inputStream);
			inputStream.close();
			String keyValue = props.getProperty("SYNCFLAG").trim();
			if (keyValue.equalsIgnoreCase("N")) {
				/*EODBalancingServicesProxy eodBalancingServicesProxy = new EODBalancingServicesProxy();
				returnMessage = eodBalancingServicesProxy
						.processEODBalacing(eodDetails);*/
				if (null != returnMessage
						&& returnMessage.equalsIgnoreCase("success")) {
					JSONParser parser = new JSONParser();
					Object eodObject = parser.parse(eodDetails);
					JSONObject jsonObject = (JSONObject) eodObject;
					JSONArray arrayObject = (JSONArray) jsonObject
							.get("eodDetails");
					EODBalancing eodBalancing = new EODBalancing();
					POSUserTill posUserTill = new POSUserTill();
					Implementation implementation = new Implementation();
					posUserTill.setPosUserTillId((Long) jsonObject
							.get("posUserTillId"));
					implementation.setImplementationId((Long) jsonObject
							.get("implementationId"));
					eodBalancing.setDescription(jsonObject.get("description")
							.toString());
					eodBalancing.setDate(DateFormat
							.convertStringToDate(jsonObject.get("currentDate")
									.toString()));
					eodBalancing.setReferenceNumber(SystemBL.getReferenceStamp(
							EODBalancing.class.getName(),
							eodBalancingBL.getImplementation())); 
					eodBalancing.setImplementation(implementation);
					EODBalancingDetail eodBalancingDetail = null;
					List<EODBalancingDetail> eodBalancingDetails = new ArrayList<EODBalancingDetail>();
					 
 					eodBalancingBL.saveOfflineEODBalancing(eodBalancing,
								eodBalancingDetails);
					returnMessage = "SUCCESS";
				} else {
					returnMessage = "Live server doesn't responding.";
					return SUCCESS;
				}
			} else {
				returnMessage = "Processing updates, please try again later.";
				return SUCCESS;
			}
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: processEODWebService: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getEodDetails() {
		return eodDetails;
	}

	public void setEodDetails(String eodDetails) {
		this.eodDetails = eodDetails;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public EODBalancingBL getEodBalancingBL() {
		return eodBalancingBL;
	}

	public void setEodBalancingBL(EODBalancingBL eodBalancingBL) {
		this.eodBalancingBL = eodBalancingBL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
