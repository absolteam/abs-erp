package com.aiotech.aios.web.webservices;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.common.action.AiosWebServiceBL;
import com.aiotech.aios.hr.domain.entity.vo.SwipeVO;

@Component
@Path("/rest")
public class WebServiceServer {
	@Autowired
	private AiosWebServiceBL aiosWebServiceBL;

	@GET
	@Path("/getPointOfSaleSync")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public SalesInvoiceVO getList(@Context ServletContext servletContext) {

		// get Spring application context
		ApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		aiosWebServiceBL = (AiosWebServiceBL) ctx.getBean("aiosWebServiceBL",
				AiosWebServiceBL.class);
		SalesInvoiceVO salesInvoiceVO = aiosWebServiceBL.getPointOfSaleBL()
				.objectCall();
		return salesInvoiceVO;

	}

	@POST
	@Path("/sendpointOfSaleSync")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response sendList(@Context ServletContext servletContext,
			SalesInvoiceVO vo) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		aiosWebServiceBL = (AiosWebServiceBL) ctx.getBean("aiosWebServiceBL",
				AiosWebServiceBL.class);

		String result = aiosWebServiceBL.getPointOfSaleBL().testCall(vo);

		return Response.status(200).entity(result).build();

	}

	// Actual Business Calls

	@POST
	@Path("/pointOfSaleSync")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response posUpdate(@Context ServletContext servletContext,
			PointOfSaleVO vo) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		aiosWebServiceBL = (AiosWebServiceBL) ctx.getBean("aiosWebServiceBL",
				AiosWebServiceBL.class);

		CustomerVO result = aiosWebServiceBL.getPointOfSaleBL()
				.postOfflineTransaction(vo);

		return Response.status(200).entity(result.getType()).build();
	}

	@POST
	@Path("/attendanceSync")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response attendanceSync(@Context ServletContext servletContext,
			SwipeVO vo) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		aiosWebServiceBL = (AiosWebServiceBL) ctx.getBean("aiosWebServiceBL",
				AiosWebServiceBL.class);

		String result = aiosWebServiceBL.getAttendanceBL().saveSwipes(vo);

		return Response.status(200).entity(result).build();
	}
}