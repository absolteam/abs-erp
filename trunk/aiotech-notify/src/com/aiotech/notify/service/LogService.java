package com.aiotech.notify.service;

import java.util.List;

import com.aiotech.notify.domain.entity.Email;
import com.aiotech.notify.domain.entity.EmailLog;
import com.aiotech.notify.domain.entity.SMS;
import com.aiotech.notify.domain.entity.SMSLog;
import com.aiotech.notify.domain.entity.SystemLog;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LogService {

	private AIOTechGenericDAO<Email> emailDAO;
	private AIOTechGenericDAO<EmailLog> emailLogDAO;
	private AIOTechGenericDAO<SMS> smsDAO;
	private AIOTechGenericDAO<SMSLog> smsLogDAO;
	private AIOTechGenericDAO<SystemLog> systemLogDAO;

	public void saveSystemLog(SystemLog systemLog) {
		systemLogDAO.save(systemLog);
	}

	public void saveOrUpdateEmailLogs(List<EmailLog> emailLogs) {
		emailLogDAO.saveOrUpdateAll(emailLogs);
	}

	public void saveOrUpdateEmail(Email email) {
		emailDAO.saveOrUpdate(email);
	}

	public void saveOrUpdateSMS(SMS sms) {
		smsDAO.saveOrUpdate(sms);
	}

	public void saveOrUpdateSMSLogs(List<SMSLog> smsLogs) {
		smsLogDAO.saveOrUpdateAll(smsLogs);
	}

	public AIOTechGenericDAO<Email> getEmailDAO() {
		return emailDAO;
	}

	public void setEmailDAO(AIOTechGenericDAO<Email> emailDAO) {
		this.emailDAO = emailDAO;
	}

	public AIOTechGenericDAO<EmailLog> getEmailLogDAO() {
		return emailLogDAO;
	}

	public void setEmailLogDAO(AIOTechGenericDAO<EmailLog> emailLogDAO) {
		this.emailLogDAO = emailLogDAO;
	}

	public AIOTechGenericDAO<SMS> getSmsDAO() {
		return smsDAO;
	}

	public void setSmsDAO(AIOTechGenericDAO<SMS> smsDAO) {
		this.smsDAO = smsDAO;
	}

	public AIOTechGenericDAO<SMSLog> getSmsLogDAO() {
		return smsLogDAO;
	}

	public void setSmsLogDAO(AIOTechGenericDAO<SMSLog> smsLogDAO) {
		this.smsLogDAO = smsLogDAO;
	}

	public AIOTechGenericDAO<SystemLog> getSystemLogDAO() {
		return systemLogDAO;
	}

	public void setSystemLogDAO(AIOTechGenericDAO<SystemLog> systemLogDAO) {
		this.systemLogDAO = systemLogDAO;
	}
}
