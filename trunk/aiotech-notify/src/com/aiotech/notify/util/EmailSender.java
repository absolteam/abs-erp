package com.aiotech.notify.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.aiotech.notify.domain.entity.Email;
import com.aiotech.notify.domain.entity.EmailLog;
import com.aiotech.notify.service.LogService;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGrid.Response;

public class EmailSender {

	static final Logger LOGGER = LogManager.getLogger(EmailSender.class);

	public static Boolean sendEmail(String subject, String body,
			String recipientEmail, String lang, Properties confProps,
			Long implementationId, Long personId, Boolean isResent,
			LogService logService) {

		List<String> list = new ArrayList<String>();
		list.add(recipientEmail);

		return sendEmail(subject, body, list, lang, confProps,
				implementationId, personId, isResent, logService);
	}

	public static Boolean sendEmail(String subject, String body,
			List<String> recipientEmails, String lang, Properties confProps,
			Long implementationId, Long personId, Boolean isResent,
			LogService logService) {

		Boolean result = null;
		List<String> alreadySent = new ArrayList<String>();
		List<EmailLog> emailLogs = new ArrayList<EmailLog>();

		Email emailToSend = new Email();

		if (lang.equalsIgnoreCase(Constant.EN)) {
			emailToSend.setSubject(subject);
			emailToSend.setBodyEn(body);

		} else if (lang.equalsIgnoreCase(Constant.AR)) {
			emailToSend.setSubjectAr(subject);
			emailToSend.setBodyAr(body);
		}

		emailToSend.setDate(Calendar.getInstance().getTime());
		emailToSend.setTime(Calendar.getInstance().getTime());
		emailToSend.setImplementationId(implementationId);

		logService.saveOrUpdateEmail(emailToSend);

		try {
			SendGrid sendGrid = new SendGrid(
					confProps.getProperty("email.gateway.username"),
					confProps.getProperty("email.gateway.password"));
			Response response = null;
			com.sendgrid.SendGrid.Email sendGridEmail = null;

			for (String emailAddress : recipientEmails) {

				if (alreadySent.contains(emailAddress))
					continue;
				else
					alreadySent.add(emailAddress);

				sendGridEmail = new com.sendgrid.SendGrid.Email();

				sendGridEmail.setFrom(confProps
						.getProperty("email.gateway.from.email"));
				sendGridEmail.setFromName(confProps
						.getProperty("email.gateway.from.name"));
				sendGridEmail.setReplyTo(sendGridEmail.getFrom());

				sendGridEmail.addTo(emailAddress);
				sendGridEmail.addToName(emailAddress);

				sendGridEmail.setSubject(subject);
				sendGridEmail.setHtml(body);

				try {
					response = sendGrid.send(sendGridEmail);

					if (response.getStatus()) {
						result = true;
						LOGGER.info("Email with subject ["
								+ subject
								+ "] SENT to email ending with: "
								+ emailAddress.substring(emailAddress.length() / 2));
						emailLogs.add(new EmailLog(emailToSend, personId,
								emailAddress, Constant.SENT_SUCCESS, Calendar
										.getInstance().getTime(), Calendar
										.getInstance().getTime(), lang, result,
								isResent));
					} else {
						result = false;
						LOGGER.error("Email with subject ["
								+ subject
								+ "] FAILED for email ending with: "
								+ emailAddress.substring(emailAddress.length() / 2)
								+ " - reason: " + response.getMessage());
						emailLogs.add(new EmailLog(emailToSend, personId,
								emailAddress, Constant.FAILED_WITH_MSG
										.concat(response.getMessage()),
								Calendar.getInstance().getTime(), Calendar
										.getInstance().getTime(), lang, result,
								isResent));
					}
				} catch (Exception e) {
					e.printStackTrace();
					result = false;
					LOGGER.error("Email with subject [" + subject
							+ "] FAILED for email ending with: "
							+ emailAddress.substring(emailAddress.length() / 2)
							+ " - reason: " + e.getMessage());
					emailLogs.add(new EmailLog(emailToSend, personId,
							emailAddress, Constant.FAILED_WITH_MSG.concat(e
									.getMessage()), Calendar.getInstance()
									.getTime(), Calendar.getInstance()
									.getTime(), lang, result, isResent));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			LOGGER.error("Email with subject [" + subject
					+ "] FAILED - reason: " + e.getMessage());
			emailLogs.add(new EmailLog(emailToSend, personId, null,
					Constant.FAILED_WITH_MSG.concat(e.getMessage()), Calendar
							.getInstance().getTime(), Calendar.getInstance()
							.getTime(), lang, result, isResent));
		}

		logService.saveOrUpdateEmailLogs(emailLogs);
		return result;
	}
}