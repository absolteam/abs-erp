package com.aiotech.notify.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.catalina.tribes.util.Logs;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.aiotech.notify.domain.entity.SMS;
import com.aiotech.notify.domain.entity.SMSLog;
import com.aiotech.notify.service.LogService;

public class SMSSender {

	static final Logger LOGGER = LogManager.getLogger(SMSSender.class);
	static final String SUCCESS_MSG = "ACCEPTED";
	static final String LOW = "LOW";
	static final String NORMAL = "NORMAL";
	static final String ERROR = "ERROR";
	static final String TRUE = "TRUE";
	static final String EN = "en";
	static final String AR = "ar";

	public static Boolean sendSMS(String body, String recipientNumber,
			String lang, Properties confProps, Long implementationId,
			Long personId, Boolean isResent, LogService logService) {

		List<String> list = new ArrayList<String>();
		list.add(recipientNumber);

		return sendSMS(body, list, lang, confProps, implementationId, personId,
				isResent, logService);
	}

	public static Boolean sendSMS(String body, List<String> recipientNumbers,
			String lang, Properties confProps, Long implementationId,
			Long personId, Boolean isResent, LogService logService) {

		Boolean result = null;
		String smsSendResponse = null;
		List<String> alreadySent = new ArrayList<String>();
		List<SMSLog> smsLogs = new ArrayList<SMSLog>();

		SMS smsToSend = new SMS();

		if (lang.equalsIgnoreCase(EN)) {
			smsToSend.setBodyEn(body);

		} else if (lang.equalsIgnoreCase(AR)) {
			smsToSend.setBodyAr(body);
		}

		smsToSend.setDate(Calendar.getInstance().getTime());
		smsToSend.setTime(Calendar.getInstance().getTime());
		smsToSend.setImplementationId(implementationId);

		logService.saveOrUpdateSMS(smsToSend);

		try {
			String remainingCredit = isRemainingCreditLow(
					confProps.getProperty("bulkSMS.username"),
					confProps.getProperty("bulkSMS.password"),
					confProps.getProperty("notify.low.credit.limit"));

			if (remainingCredit.contains(LOW)
					&& confProps.getProperty("notify.low.sms.credit")
							.equalsIgnoreCase(TRUE))

				sendLowSMSCreditAlertEmail(confProps,
						remainingCredit.split("#")[1], implementationId,
						personId, logService);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Unable to notify low SMS credit - error: "
					+ e.getMessage());
		}

		try {

			for (String mobile : recipientNumbers) {

				if (alreadySent.contains(mobile))
					continue;
				else
					alreadySent.add(mobile);

				try {
					smsSendResponse = sendSmsViaJSON(
							confProps.getProperty("bulkSMS.username"),
							confProps.getProperty("bulkSMS.password"), mobile,
							body);

					if (smsSendResponse.toUpperCase().contains(SUCCESS_MSG)) {
						result = true;
						LOGGER.info("SMS SENT to number ending with: "
								+ mobile.substring(mobile.length() / 2));
						smsLogs.add(new SMSLog(smsToSend, personId, mobile,
								Constant.SENT_SUCCESS, Calendar.getInstance()
										.getTime(), Calendar.getInstance()
										.getTime(), result, lang, isResent));
					} else {
						result = false;
						LOGGER.error("SMS Sending FAILED for number ending with: "
								+ mobile.substring(mobile.length() / 2)
								+ " - reason: " + smsSendResponse);
						smsLogs.add(new SMSLog(smsToSend, personId, mobile,
								Constant.FAILED_WITH_MSG
										.concat(smsSendResponse), Calendar
										.getInstance().getTime(), Calendar
										.getInstance().getTime(), result, lang,
								isResent));
					}

				} catch (Exception e) {
					e.printStackTrace();
					result = false;
					LOGGER.error("SMS Sending FAILED for number ending with: "
							+ mobile.substring(mobile.length() / 2)
							+ " - reason: " + e.getMessage());
					smsLogs.add(new SMSLog(smsToSend, personId, mobile,
							Constant.FAILED_WITH_MSG.concat(e.getMessage()),
							Calendar.getInstance().getTime(), Calendar
									.getInstance().getTime(), result, lang,
							isResent));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			LOGGER.error("SMS Sending FAILED - reason: " + e.getMessage());
			smsLogs.add(new SMSLog(smsToSend, personId, null,
					Constant.FAILED_WITH_MSG.concat(e.getMessage()), Calendar
							.getInstance().getTime(), Calendar.getInstance()
							.getTime(), result, lang, isResent));
		}

		logService.saveOrUpdateSMSLogs(smsLogs);
		return result;
	}

	static String sendSmsViaJSON(String user, String password, String number,
			String body) throws Exception {

		JSONObject json = new JSONObject();
		json.put("to", number);
		json.put("body", body);
		json.put("longMessageMaxParts", 2);

		String authString = user + "@bulksms.vsms.net:" + password;
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String responseString = "";
		String reader = "";

		org.apache.http.client.HttpClient httpClient = new DefaultHttpClient();

		HttpPost post = new HttpPost(
				"https://api.bulksms.com/v1/messages?auto-unicode=true");
		post.setHeader("Authorization", "Basic " + new String(authEncBytes));

		StringEntity input = new StringEntity(json.toString(),
				Charset.forName("utf-8"));
		input.setContentEncoding("utf-8");
		input.setContentType("application/json");

		post.setEntity(input);

		try {
			HttpResponse response = httpClient.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			while ((reader = rd.readLine()) != null) {
				responseString += reader;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseString = "";
		}

		return responseString;
	}

	static String isRemainingCreditLow(String username, String password,
			String limit) {

		try {
			URL url = new URL(
					"https://bulksms.vsms.net/eapi/user/get_credits/1/1.1?username="
							+ username + "&password=" + password);
			URLConnection conn = null;
			BufferedReader rd = null;
			String responseLine = null;
			String toConvert = "";

			conn = url.openConnection();
			rd = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));

			while ((responseLine = rd.readLine()) != null) {
				toConvert += responseLine;
			}

			rd.close();

			Double remainingCredit = Double.parseDouble(toConvert.substring(2));
			LOGGER.warn("Remaining BULKSMS credit = " + remainingCredit);

			if (remainingCredit <= Double.parseDouble(limit))
				return LOW + "#" + remainingCredit;
			else
				return NORMAL + "#" + remainingCredit;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR + " : " + e.getMessage();
		}
	}

	static void sendLowSMSCreditAlertEmail(Properties confProps,
			String currentBalance, Long implementationId, Long personId,
			LogService logService) throws Exception {

		List<String> adminEmailList = new ArrayList<String>();
		String adminEmails = confProps.getProperty("notify.low.credit.email");

		if (adminEmails != null && !adminEmails.isEmpty()) {
			for (String address : adminEmails.split(","))
				adminEmailList.add(address);
		} else {
			adminEmailList.add("ahsan.m@aio.ae");
		}

		EmailSender
				.sendEmail(

						"ERP | Low SMS credit (" + currentBalance + ")",
						"This is to notify that your remaining bulksms account ("
								+ confProps.getProperty("bulkSMS.username")
								+ ") balance is running low.<br/><br/>Current Balance = "
								+ currentBalance
								+ " (minimum balance required = "
								+ confProps
										.getProperty("notify.low.credit.limit")
								+ ")"
								+ "<br/><br/><h5>To stop this alert,"
								+ " please refule your account credit or adjust the minimum credit"
								+ " limit in configuration.</h5>",
						adminEmailList, "en", confProps, implementationId,
						personId, false, logService);
	}
}