package com.aiotech.notify.util;

public class Constant {

	static final String EN = "en";
	static final String AR = "ar";
	static final String SENT_SUCCESS = "Sent Successfully";
	static final String FAILED_WITH_MSG = "Failed - reason: ";
}
