package com.aiotech.notify.enterprise;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiotech.notify.bl.ExecuteNotificationBL;

@Service
public class NotifyEnterpriseService {

	ExecuteNotificationBL executeNotificationBL;

	public Boolean sendEmail(String subject, String body,
			String recipientEmail, String lang, Long implementationId,
			Long personId, Boolean isResent, Properties confProps) {

		List<String> list = new ArrayList<String>();
		list.add(recipientEmail);

		return this.sendEmail(subject, body, list, lang, implementationId,
				personId, isResent, confProps);
	}

	public Boolean sendEmail(String subject, String body,
			List<String> recipientEmails, String lang, Long implementationId,
			Long personId, Boolean isResent, Properties confProps) {

		return executeNotificationBL.sendEmail(subject, body, recipientEmails,
				lang, implementationId, personId, isResent, confProps);
	}

	public Boolean sendSMS(String body, String recipientNumber, String lang,
			Long implementationId, Long personId, Boolean isResent,
			Properties confProps) {

		List<String> list = new ArrayList<String>();
		list.add(recipientNumber);

		return this.sendSMS(body, list, lang, implementationId, personId,
				isResent, confProps);
	}

	public Boolean sendSMS(String body, List<String> recipientNumbers,
			String lang, Long implementationId, Long personId,
			Boolean isResent, Properties confProps) {

		return executeNotificationBL.sendSMS(body, recipientNumbers, lang,
				implementationId, personId, isResent, confProps);
	}

	public void logSystemMessage(String reportingClassName, String type,
			String message, Long implementationId) {
		executeNotificationBL.logSystemMessage(
				reportingClassName + "::" + type, message, implementationId);
	}

	public ExecuteNotificationBL getExecuteNotificationBL() {
		return executeNotificationBL;
	}

	@Autowired
	public void setExecuteNotificationBL(
			ExecuteNotificationBL executeNotificationBL) {
		this.executeNotificationBL = executeNotificationBL;
	}
}
