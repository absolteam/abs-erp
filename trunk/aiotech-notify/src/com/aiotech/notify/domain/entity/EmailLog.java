package com.aiotech.notify.domain.entity;

// Generated Aug 18, 2015 12:22:34 PM by Hibernate Tools 3.2.5.Beta

import java.util.Date;

/**
 * EmailLog generated by hbm2java
 */
public class EmailLog implements java.io.Serializable {

	private Long id;
	private Email email;
	private Long personId;
	private String emailAddress;
	private String message;
	private Date date;
	private Date time;
	private String lang;
	private Boolean status;
	private Boolean isResent;

	public EmailLog() {
	}

	public EmailLog(Email email, Long personId) {
		this.email = email;
		this.personId = personId;
	}

	public EmailLog(Email email, Long personId, String emailAddress,
			String message, Date date, Date time, String lang, Boolean status,
			Boolean isResent) {
		this.email = email;
		this.personId = personId;
		this.emailAddress = emailAddress;
		this.message = message;
		this.date = date;
		this.time = time;
		this.lang = lang;
		this.status = status;
		this.isResent = isResent;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Email getEmail() {
		return this.email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Long getPersonId() {
		return this.personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Boolean getIsResent() {
		return this.isResent;
	}

	public void setIsResent(Boolean isResent) {
		this.isResent = isResent;
	}

}
