package com.aiotech.notify.bl;

import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aiotech.notify.domain.entity.SystemLog;
import com.aiotech.notify.service.LogService;
import com.aiotech.notify.util.EmailSender;
import com.aiotech.notify.util.SMSSender;

@Service
@Transactional
public class ExecuteNotificationBL {

	LogService logService;

	public Boolean sendEmail(String subject, String body,
			List<String> recipientEmails, String lang, Long implementationId,
			Long personId, Boolean isResent, Properties confProps) {

		if (null == confProps)
			confProps = (Properties) ServletActionContext.getServletContext()
					.getAttribute("confProps");

		return EmailSender.sendEmail(subject, body, recipientEmails, lang,
				confProps, implementationId, personId, isResent, logService);
	}

	public Boolean sendSMS(String body, List<String> recipientNumbers,
			String lang, Long implementationId, Long personId,
			Boolean isResent, Properties confProps) {

		if (null == confProps)
			confProps = (Properties) ServletActionContext.getServletContext()
					.getAttribute("confProps");

		return SMSSender.sendSMS(body, recipientNumbers, lang, confProps,
				implementationId, personId, isResent, logService);
	}

	public void logSystemMessage(String type, String message,
			Long implementationId) {

		SystemLog log = new SystemLog(type, message, Calendar.getInstance()
				.getTime(), Calendar.getInstance().getTime(), implementationId);
		logService.saveSystemLog(log);
	}

	public LogService getLogService() {
		return logService;
	}

	@Autowired
	public void setLogService(LogService logService) {
		this.logService = logService;
	}
}
