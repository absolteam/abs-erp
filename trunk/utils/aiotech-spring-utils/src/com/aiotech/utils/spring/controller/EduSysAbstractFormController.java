/**
 * @creation date: Jul 4, 2011
 * All Rights Reserved to EduSys Pakistan (www.edusyspk.com)
 *
 * This software is the confidential and proprietary information of 
 * EduSys Lahore Pakistan. You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with
 * EduSys Pakistan (www.edusyspk.com)
 */
package com.aiotech.utils.spring.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractFormController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * @author Haris
 * 
 */
public abstract class EduSysAbstractFormController extends
		AbstractFormController {

	protected Map<String, Object> model = new HashMap<String, Object>();
	private String contentPage;
	private String pageTitle;
	private String template = "Template-Default";

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {

		/* Default implementation, Child can override this method */
		Object object = super.formBackingObject(request);
		request.setAttribute("command", object);
		return object;
	}

	@Override
	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) throws Exception {

		super.initBinder(request, binder);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, null, editor);
		binder.bind(request);
	}

	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException bindException)
			throws Exception {

		model.put("CONTENT_PAGE", contentPage);
		model.put("PAGE_TITLE", pageTitle);

		/* if no errors were found during validation */
		if (!bindException.hasErrors()) {

			/* if a redirection is specified */
			if (request.getAttribute("REDIRECT_VIEW") != null) {

				String redirectURL = (String) request
						.getAttribute("REDIRECT_VIEW");
				return new ModelAndView(new RedirectView(redirectURL));

			} else {
				/* show the template page */
				referenceData(request, null, null);
				return new ModelAndView(template, "model", model);
			}

		} else {
			referenceData(request, null, null);
			request.setAttribute("model", model);
			return new ModelAndView(template, bindException.getModel());
		}
	}

	public String getContentPage() {
		return contentPage;
	}

	public void setContentPage(String contentPage) {
		this.contentPage = contentPage;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

}
