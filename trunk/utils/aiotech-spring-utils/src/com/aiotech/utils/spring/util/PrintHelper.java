package com.aiotech.utils.spring.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletOutputStream;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.struts2.ServletActionContext;

public class PrintHelper {

	private static final String commonPath = "aios/PrintTemplate";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Boolean generateDocument(HashMap<String, String> tagList,
			String templateFile, String implementationString) {

		final Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");

		POIFSFileSystem fs;
		HWPFDocument hwpfDocument = null;
		final String targetDoc = templateFile.concat(".doc");
		final String implementation = implementationString;
		Iterator tagIterator = tagList.entrySet().iterator();

		File file = (File) AccessController
				.doPrivileged(new PrivilegedAction() {
					public Object run() {
						try {
							return new File(confProps.getProperty("file.drive")
									.concat(commonPath)
									+ "/templates/"
									+ implementation + "/" + targetDoc);
						} catch (Exception e) {
							e.printStackTrace();
						}
						return null;
					}
				});

		try {
			fs = new POIFSFileSystem(new FileInputStream(file));
			hwpfDocument = new HWPFDocument(fs);
			Range range = hwpfDocument.getRange();

			while (tagIterator.hasNext()) {

				try {
					Map.Entry pairs = (Map.Entry) tagIterator.next();
					range.replaceText(pairs.getKey().toString(), pairs
							.getValue().toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			OutputStream out = new FileOutputStream(new File(confProps
					.getProperty("file.drive").concat(commonPath)
					+ "/server/"
					+ tagList.get("tempFileName")));

			hwpfDocument.write(out);
			out.flush();
			out.close();

			return true;
		} catch (Exception e) {
			System.out.println("PRINT HELPER");
			e.printStackTrace();
			return false;
		}
	}

	public static boolean sendResponseToApplet(String fileName, String path)
			throws Exception {

		File temporaryFile = new File(path + fileName);
		BufferedInputStream buf = new BufferedInputStream(new FileInputStream(
				temporaryFile));
		ServletOutputStream servletOutputStream = ServletActionContext
				.getResponse().getOutputStream();
		int readBytes = 0;

		try {
			while ((readBytes = buf.read()) != -1)
				servletOutputStream.write(readBytes);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		} finally {
			if (buf != null)
				buf.close();

			if (temporaryFile.exists())
				temporaryFile.delete();

			if (servletOutputStream != null)
				servletOutputStream.close();
		}
		return true;
	}

}
