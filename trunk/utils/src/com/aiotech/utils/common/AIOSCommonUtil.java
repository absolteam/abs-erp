package com.aiotech.utils.common;

import java.text.NumberFormat;
import java.util.Locale;

public class AIOSCommonUtil {

	public static String formatAmount(Object amount) {

		try {
			NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
			return n.format(amount).substring(1);
		} catch (Exception e) {
			e.printStackTrace();
			return amount.toString();
		}
	}
}