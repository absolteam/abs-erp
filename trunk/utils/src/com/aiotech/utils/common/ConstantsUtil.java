package com.aiotech.utils.common;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public class ConstantsUtil {

	public static final class Accounts {

		public static enum ProjectStatus {
			Open((byte) 1), Requested((byte) 2), Delivered((byte) 3), Closed(
					(byte) 99);
			private static final Map<Byte, ProjectStatus> lookup = new HashMap<Byte, ProjectStatus>();
			static {
				for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum Priority {
			Normal((byte) 1), Low((byte) 2), High((byte) 3), Immediate((byte) 4);
			private static final Map<Byte, Priority> lookup = new HashMap<Byte, Priority>();
			static {
				for (Priority e : EnumSet.allOf(Priority.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private Priority(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static Priority get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectPaymentStatus {
			Pending((byte) 1), Paid((byte) 2), Hold((byte) 3), Cancelled(
					(byte) 4);
			private static final Map<Byte, ProjectPaymentStatus> lookup = new HashMap<Byte, ProjectPaymentStatus>();
			static {
				for (ProjectPaymentStatus e : EnumSet
						.allOf(ProjectPaymentStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectPaymentStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectPaymentStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectResourceType {
			Labour((byte) 2);
			private static final Map<Byte, ProjectResourceType> lookup = new HashMap<Byte, ProjectResourceType>();
			static {
				for (ProjectResourceType e : EnumSet
						.allOf(ProjectResourceType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectResourceType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectResourceType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectExpenseMode {
			Estimated_Expense((byte) 1), General_Expense((byte) 2);
			private static final Map<Byte, ProjectExpenseMode> lookup = new HashMap<Byte, ProjectExpenseMode>();
			static {
				for (ProjectExpenseMode e : EnumSet
						.allOf(ProjectExpenseMode.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectExpenseMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectExpenseMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectDeliveryStatus {
			Delivered((byte) 1), Pending((byte) 2), Hold((byte) 3), Cancelled(
					(byte) 4);
			private static final Map<Byte, ProjectDeliveryStatus> lookup = new HashMap<Byte, ProjectDeliveryStatus>();
			static {
				for (ProjectDeliveryStatus e : EnumSet
						.allOf(ProjectDeliveryStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectDeliveryStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectDeliveryStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectPriority {
			High((byte) 1), Medium((byte) 2), Low((byte) 3);
			private static final Map<Byte, ProjectPriority> lookup = new HashMap<Byte, ProjectPriority>();
			static {
				for (ProjectPriority e : EnumSet.allOf(ProjectPriority.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectPriority(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectPriority get(Byte code) {
				return lookup.get(code);
			}
		}
	}

	public static final class Business {
		// Register all the parent and child record information here
		// This is being used only in MOFA but the same method need in
		// ERP as well in order to maintain the generic logic
		public static enum ChildRecord {
			EventsDetails("Events");

			private static final Map<String, ChildRecord> lookup = new HashMap<String, ChildRecord>();

			static {
				for (ChildRecord s : EnumSet.allOf(ChildRecord.class)) {
					lookup.put(s.getChild(), s);
				}
			}

			private String child;

			private ChildRecord(String child) {
				this.child = child;
			}

			public String getChild() {
				return child;
			}

			public static ChildRecord get(String child) {
				return lookup.get(child);
			}
		}
	}

	public static enum ReturnProcessType {
		Exchange((byte) 1), DirectPayment((byte) 2), FreeReturn((byte) 3);
		private static final Map<Byte, ReturnProcessType> lookup = new HashMap<Byte, ReturnProcessType>();
		static {
			for (ReturnProcessType e : EnumSet.allOf(ReturnProcessType.class))
				lookup.put(e.getCode(), e);
		}

		private Byte code;

		private ReturnProcessType(Byte code) {
			this.code = code;
		}

		public Byte getCode() {
			return code;
		}

		public static ReturnProcessType get(Byte code) {
			return lookup.get(code);
		}
	}
}
