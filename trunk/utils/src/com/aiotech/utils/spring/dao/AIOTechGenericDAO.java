/**
 * @creation date: Jul 4, 2011
 * All Rights Reserved to EduSys Pakistan (www.edusyspk.com)
 *
 * This software is the confidential and proprietary information of 
 * EduSys Lahore Pakistan. You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with
 * EduSys Pakistan (www.edusyspk.com)
 */
package com.aiotech.utils.spring.dao;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Haris
 * 
 */
public class AIOTechGenericDAO<T> extends HibernateDaoSupport {

	private Class<T> type;

	public AIOTechGenericDAO(Class<T> type) {
		super();
		this.type = type;
	}

	/* Initialize static resources */
	private static final Log log = LogFactory.getLog(AIOTechGenericDAO.class);

	public Class getUnderlyingType() {
		return type;
	}

	public Query getNamedQuery(String queryName) {

		return getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(queryName);
	}

	public Criteria createCriteria() {
		return getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createCriteria(type);
	}
	
	public Criteria createCriteria(String aliasName) {
		return getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createCriteria(type,aliasName);
	}

	public List<T> getAll() {
		// System.out.println("Getting All Records");
		log.debug("listing all");
		try {
			List<T> results = getHibernateTemplate().loadAll(type);
			log.debug("listing successful");
			// System.out.println("No. of Records: "+results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("listing failed", e);
			throw e;
		}
	}

	public void saveOrUpdateAll(Collection<T> transientInstances) {
		log.debug("persisting instance");
		try {
			/*
			 * Get the HibernateTemplate for this DAO, pre-initialized with the
			 * SessionFactory or set explicitly
			 */
			getHibernateTemplate().saveOrUpdateAll(transientInstances);
			log.debug("saveOrUpdateAll successful");
		} catch (RuntimeException re) {
			log.error("saveOrUpdateAll Failed", re);
			throw re;
		}
	}

	public void persist(T transientInstance) {
		log.debug("persisting instance");
		try {
			/*
			 * Get the HibernateTemplate for this DAO, pre-initialized with the
			 * SessionFactory or set explicitly
			 */
			getHibernateTemplate().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void saveOrUpdate(T transientInstance) {
		log.debug("saving instance");
		try {
			/*
			 * Get the HibernateTemplate for this DAO, pre-initialized with the
			 * SessionFactory or set explicitly
			 */
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(T instance) {
		log.debug("attaching dirty instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(T instance) {
		log.debug("attaching clean instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE); 
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public void evict(T instance) {
		log.debug("attaching clean evict");
		try {
			getHibernateTemplate().evict(instance);
			log.debug("attach evict");
		} catch (RuntimeException re) {
			log.error("attach evict", re);
			throw re;
		}
	}
	
	public void refresh(T instance) {
		log.debug("attaching clean evict");
		try {
			getHibernateTemplate().refresh(instance);
			log.debug("attach evict");
		} catch (RuntimeException re) {
			log.error("attach evict", re);
			throw re;
		}
	}

	public void delete(T persistentInstance) {
		log.debug("deleting instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public void deleteAll(List<T> persistentInstances) {
		log.debug("deleting instances");
		try {
			getHibernateTemplate().deleteAll(persistentInstances);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public T merge(T detachedInstance) {
		log.debug("merging instance");
		try {
			T result = (T) getHibernateTemplate().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public T findById(Number id) {
		log.debug("getting instance with id: " + id);
		try {
			T instance = (T) getHibernateTemplate().get(type, id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public T findById(String id) {
		log.debug("getting instance with id: " + id);
		try {
			T instance = (T) getHibernateTemplate().get(type, id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<T> findByExample(T instance) {
		log.debug("finding instance by example");
		try {
			List<T> results = (List<T>) createCriteria().add(
					Example.create(instance)).list();
			/* FIX-ME: there might be some missing code */
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	/**
	 * Method to get records based on the given Named Query which is defined
	 * com.e2esp.dinnersms.domain.queries package
	 * 
	 * */
	public List<T> findByNamedQuery(String queryName, Object value) {
		log.debug("finding instance by named query");
		try {
			List<T> results;
			if (value != null) {
				results = getHibernateTemplate().findByNamedQuery(queryName,
						value);
			} else {
				results = getHibernateTemplate().findByNamedQuery(queryName);
			}
			log.debug("find by named query, result size: " + results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("find by named query failed", e);
			throw e;
		}
	}

	/**
	 * Method to get records based on the given Named Query and given parameter
	 * values
	 * 
	 * @param queryName
	 *            name of the query defined in package-info.java
	 * @param values
	 *            an array of Object containing query parameter values
	 * @return List of Entity objects
	 */
	public List<T> findByNamedQuery(String queryName, Object... values) {
		log.debug("finding instance by named query");
		try {
			List<T> results = getHibernateTemplate().findByNamedQuery(
					queryName, values);

			log.debug("find by named query successful, result size: "
					+ results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("find by named query failed", e);
			throw e;
		}
	}

	/**
	 * Method to get records based on the given Named Query and given parameter
	 * values
	 * 
	 * @param queryName
	 *            name of the query defined in package-info.java
	 * @param values
	 *            an array of Object containing query parameter values
	 * @return List of Entity objects
	 */
	public List<T> findByNamedQuery(String queryName, int maxResults,
			Object... values) {
		log.debug("finding instance by named query");
		try {
			HibernateTemplate template = getHibernateTemplate();

			template.setMaxResults(maxResults);
			List<T> results = template.findByNamedQuery(queryName, values);

			log.debug("find by named query successful, result size: "
					+ results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("find by named query failed", e);
			throw e;
		}
	}

	public List<T> findByExample(T instance, int firstResult, int maxResults) {
		log.debug("finding instance by named query");
		try {
			HibernateTemplate template = getHibernateTemplate();

			// template.setMaxResults(maxResults);
			List<T> results = template.findByExample(instance, firstResult,
					maxResults);
			log.debug("find by named query successful, result size: "
					+ results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("find by named query failed", e);
			throw e;
		}
	}

	public List<T> findByNamedParam(String query, String[] paramNames,
			Object[] values) {
		log.debug("finding instance by named param");
		try {
			HibernateTemplate template = getHibernateTemplate();

			List<T> results = template.findByNamedParam(query, paramNames,
					values);
			log.debug("find by named param successful, result size: "
					+ results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("find by named query failed", e);
			throw e;
		}
	}

	public List<T> find(String query) {
		log.debug("finding instance by query");
		try {
			HibernateTemplate template = getHibernateTemplate();

			List<T> results = template.find(query);
			log.debug("find by query successful, result size: "
					+ results.size());
			return results;
		} catch (DataAccessException e) {
			log.error("find by query failed", e);
			throw e;
		}
	}

	public void save(T transientInstance) {
		log.debug("saving instance");
		try {
			/*
			 * Get the HibernateTemplate for this DAO, pre-initialized with the
			 * SessionFactory or set explicitly
			 */
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}
	
	public void evict(Collection<T> instance) {
		log.debug("attaching clean evict");
		try {
			getHibernateTemplate().evict(instance);
			log.debug("attach evict");
		} catch (RuntimeException re) {
			log.error("attach evict", re);
			throw re;
		}
	}
	
	
	//Added by rafiq from MOFA GenericDAO in order to follow the generic wrokflow implementation
	public List<T> findByRelativeId(Object id) {

		Criteria criteria = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(type);
		criteria.add(Restrictions.eq("relativeId", id));
		return criteria.list();

	}
	
	//Added by rafiq from MOFA GenericDAO in order to follow the generic wrokflow implementation
	public List<T> findByParentId(String method,Object object) {

		Criteria criteria = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(type);
		criteria.add(Restrictions.eq(method, object));
		return criteria.list();

	}

	public Session getHibernateSession() {
		return getSession();
	}

}
