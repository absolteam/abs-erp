/**
 * @creation date: Jul 4, 2011
 * All Rights Reserved to EduSys Pakistan (www.edusyspk.com)
 *
 * This software is the confidential and proprietary information of 
 * EduSys Lahore Pakistan. You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with
 * EduSys Pakistan (www.edusyspk.com)
 */
package com.aiotech.utils.spring.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * @author Haris
 *
 */
public class EduSysAbstractController extends AbstractController {

	protected Map<String, Object> model;
	private String contentPage;
	private String pageTitle;
	private String template = "Template-Default";

	@Override
	public ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		model = new HashMap<String, Object>();
		performAction(request, response);
		
		if (request.getAttribute("REDIRECT_VIEW") == null) {
			if (request.getAttribute("FORWARD_VIEW") == null) {
				model.put("CONTENT_PAGE", contentPage);
				model.put("PAGE_TITLE", pageTitle);
				return new ModelAndView(template, "model", model);
			} else {
				return new ModelAndView((String) request.getAttribute("FORWARD_VIEW"), "model", model);
			}
		} else {			
			return new ModelAndView((RedirectView) request.getAttribute("REDIRECT_VIEW")); 
		}
	}

	/* will be overridden by children */
	public void performAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//System.out.println("in parent");
	}

	public String getContentPage() {
		return contentPage;
	}

	public void setContentPage(String contentPage) {
		this.contentPage = contentPage;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
}
