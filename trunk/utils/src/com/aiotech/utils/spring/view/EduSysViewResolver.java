/**
 * @creation date: Jul 4, 2011
 * All Rights Reserved to EduSys Pakistan (www.edusyspk.com)
 *
 * This software is the confidential and proprietary information of 
 * EduSys Lahore Pakistan. You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with
 * EduSys Pakistan (www.edusyspk.com)
 */
package com.aiotech.utils.spring.view;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.AbstractCachingViewResolver;

/**
 * @author Haris
 *
 */
public class EduSysViewResolver extends AbstractCachingViewResolver {

	private String ajaxPrefix;
	private String imagePrefix;
	private View jsonView;
	private View imageView;

	@Override
	protected View loadView(String viewName, Locale locale) throws Exception {
		View view = null;

		if (viewName.startsWith(ajaxPrefix)) {
			view = jsonView;
		} else if (imagePrefix != null && viewName.startsWith(imagePrefix)) {
			view = imageView;
		}

		return view;
	}

	public View getImageView() {
		return imageView;
	}

	public void setImageView(View imageView) {
		this.imageView = imageView;
	}

	public String getAjaxPrefix() {
		return ajaxPrefix;
	}

	public void setAjaxPrefix(String ajaxPrefix) {
		this.ajaxPrefix = ajaxPrefix;
	}

	public String getImagePrefix() {
		return imagePrefix;
	}

	public void setImagePrefix(String imagePrefix) {
		this.imagePrefix = imagePrefix;
	}

	public View getJsonView() {
		return jsonView;
	}

	public void setJsonView(View jsonView) {
		this.jsonView = jsonView;
	}
}
