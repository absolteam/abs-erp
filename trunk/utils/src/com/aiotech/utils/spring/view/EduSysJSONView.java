/**
 * @creation date: Jul 4, 2011
 * All Rights Reserved to EduSys Pakistan (www.edusyspk.com)
 *
 * This software is the confidential and proprietary information of 
 * EduSys Lahore Pakistan. You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with
 * EduSys Pakistan (www.edusyspk.com)
 */
package com.aiotech.utils.spring.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import flexjson.JSONSerializer;

/**
 * @author Haris
 * 
 */
public class EduSysJSONView extends AbstractView {

	@Override
	protected void renderMergedOutputModel(Map map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		List<String> excludes = new ArrayList<String>();

		if (request.getAttribute("EXCLUDES") != null) {
			excludes = (List<String>) request.getAttribute("EXCLUDES");
		}

		List<String> includes = new ArrayList<String>();
		if (request.getAttribute("INCLUDES") != null) {
			includes = (List<String>) request.getAttribute("INCLUDES");
		}

		JSONSerializer serializer = new JSONSerializer();

		for (String exclude : excludes) {
			serializer.exclude(exclude);
		}

		for (String include : includes) {
			serializer.include(include);
		}

		String jsonString = serializer.serialize(map);

		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.getOutputStream().write(jsonString.getBytes("UTF-8"));
	}
}
