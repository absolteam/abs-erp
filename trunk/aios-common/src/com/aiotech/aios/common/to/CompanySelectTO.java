package com.aiotech.aios.common.to;

/*******************************************************************************
*
* Create Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		21 Feb 2011 	Nagarajan T.	 		   Initial Version

******************************************************************************/

import java.io.Serializable;

public class CompanySelectTO implements Serializable{
	
	private int organizationId;
	private String organizationName;
	
	public int getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	
	

}
