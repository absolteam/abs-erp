package com.aiotech.aios.common.to;

public class SMSTO {
	
	private String messageCode;
	private String mobileNumber;
	private String messageText;
	private long batchNumber;
	private String processFlag;
	private String createdBy;
	private String requestedDate;
	private String reason;
	private String messageId;
	private int companyId;
	private int functionId;
	private int personId;
	private String personName;
	private int applicationId;
	private int processStatus;
	private int toPersonId;
	private String pushMode;
	private String expectedPushDate;
	public int getToPersonId() {
		return toPersonId;
	}
	public void setToPersonId(int toPersonId) {
		this.toPersonId = toPersonId;
	}
	public String getPushMode() {
		return pushMode;
	}
	public void setPushMode(String pushMode) {
		this.pushMode = pushMode;
	}
	public String getExpectedPushDate() {
		return expectedPushDate;
	}
	public void setExpectedPushDate(String expectedPushDate) {
		this.expectedPushDate = expectedPushDate;
	}
	public int getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(int processStatus) {
		this.processStatus = processStatus;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	 
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public long getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(long batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
