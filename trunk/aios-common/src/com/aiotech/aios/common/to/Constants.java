package com.aiotech.aios.common.to;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final class Accounts {
		public final String asset = "Assets";
		public final String liabilites = "Liabilites";
		public final String revenue = "Revenue";
		public final String expenses = "Expenses";
		public final String ownersEquity = "Owners Equity";
		public final Boolean debit = true;
		public final Boolean credit = false;

		public static enum AccountType {
			Assets(1), Liabilites(2), Revenue(3), Expenses(4), OwnersEquity(5);
			private static final Map<Integer, AccountType> lookup = new HashMap<Integer, AccountType>();

			static {
				for (AccountType e : EnumSet.allOf(AccountType.class))
					lookup.put(e.getCode(), e);
			}

			private Integer code;

			private AccountType(Integer code) {
				this.code = code;
			}

			public Integer getCode() {
				return code;
			}

			public static AccountType get(Integer code) {
				return lookup.get(code);
			}
		}

		public static enum Segments {
			Company(1), CostCenter(2), Natural(3), Analysis(4), Buffer1(5), Buffer2(
					6);
			private static final Map<Integer, Segments> lookup = new HashMap<Integer, Segments>();

			static {
				for (Segments e : EnumSet.allOf(Segments.class))
					lookup.put(e.getCode(), e);
			}

			private Integer code;

			private Segments(Integer code) {
				this.code = code;
			}

			public Integer getCode() {
				return code;
			}

			public static Segments get(Integer code) {
				return lookup.get(code);
			}
		}

		public static enum TransactionType {
			Debit((boolean) true), Credit((boolean) false);
			private static final Map<Boolean, TransactionType> lookup = new HashMap<Boolean, TransactionType>();
			static {
				for (TransactionType e : EnumSet.allOf(TransactionType.class))
					lookup.put(e.getCode(), e);
			}

			private Boolean code;

			private TransactionType(Boolean code) {
				this.code = code;
			}

			public boolean getCode() {
				return code;
			}

			public static TransactionType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ReceiptsUseCase {
			SalesInvoice((byte) 1), ProjectPaymentSchedule((byte) 2);
			private static final Map<Byte, ReceiptsUseCase> lookup = new HashMap<Byte, ReceiptsUseCase>();
			static {
				for (ReceiptsUseCase e : EnumSet.allOf(ReceiptsUseCase.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ReceiptsUseCase(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ReceiptsUseCase get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum WorkinProcessStatus {
			Issue_Materials((byte) 1), Finshed_Goods((byte) 2), Altered_Finshed_Goods(
					(byte) 3);
			private static final Map<Byte, WorkinProcessStatus> lookup = new HashMap<Byte, WorkinProcessStatus>();
			static {
				for (WorkinProcessStatus e : EnumSet
						.allOf(WorkinProcessStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private WorkinProcessStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static WorkinProcessStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum InvoiceStatus {
			Active((byte) 1), Inactive((byte) 2), Hold((byte) 3), Paid((byte) 4);
			private static final Map<Byte, InvoiceStatus> lookup = new HashMap<Byte, InvoiceStatus>();
			static {
				for (InvoiceStatus e : EnumSet.allOf(InvoiceStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private InvoiceStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static InvoiceStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ItemType {
			Asset('A'), Inventory('I'), Expense('E');
			private static final Map<Character, ItemType> lookup = new HashMap<Character, ItemType>();
			static {
				for (ItemType e : EnumSet.allOf(ItemType.class))
					lookup.put(e.getCode(), e);
			}

			private Character code;

			private ItemType(Character code) {
				this.code = code;
			}

			public Character getCode() {
				return code;
			}

			public static ItemType get(Character code) {
				return lookup.get(code);
			}
		}

		public static enum ItemAccountType {
			Inventory((byte) 1), Expense((byte) 2), Revenue((byte) 3);
			private static final Map<Byte, ItemAccountType> lookup = new HashMap<Byte, ItemAccountType>();
			static {
				for (ItemAccountType e : EnumSet.allOf(ItemAccountType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ItemAccountType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ItemAccountType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CostingType {
			FIFO((byte) 1), LIFO((byte) 2), Average((byte) 3);
			private static final Map<Byte, CostingType> lookup = new HashMap<Byte, CostingType>();
			static {
				for (CostingType e : EnumSet.allOf(CostingType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private CostingType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CostingType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum SalesInvoiceType {
			Cash((byte) 1), Credit((byte) 2);
			private static final Map<Byte, SalesInvoiceType> lookup = new HashMap<Byte, SalesInvoiceType>();
			static {
				for (SalesInvoiceType e : EnumSet.allOf(SalesInvoiceType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private SalesInvoiceType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static SalesInvoiceType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ReceiptSource {
			Customer((byte) 1), Supplier((byte) 2), Employee((byte) 3), Others(
					(byte) 4);
			private static final Map<Byte, ReceiptSource> lookup = new HashMap<Byte, ReceiptSource>();
			static {
				for (ReceiptSource e : EnumSet.allOf(ReceiptSource.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private ReceiptSource(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ReceiptSource get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProductionReqDetailStatus {
			Open((byte) 1), Processing((byte) 2), Closed((byte) 3);
			private static final Map<Byte, ProductionReqDetailStatus> lookup = new HashMap<Byte, ProductionReqDetailStatus>();
			static {
				for (ProductionReqDetailStatus e : EnumSet
						.allOf(ProductionReqDetailStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProductionReqDetailStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProductionReqDetailStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PaymentStatus {
			Paid((byte) 1), Pending((byte) 2), PDC((byte) 3);
			private static final Map<Byte, PaymentStatus> lookup = new HashMap<Byte, PaymentStatus>();
			static {
				for (PaymentStatus e : EnumSet.allOf(PaymentStatus.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private PaymentStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PaymentStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum GeneralStatus {
			Open((byte) 1), Pending((byte) 2), Completed((byte) 9);
			private static final Map<Byte, GeneralStatus> lookup = new HashMap<Byte, GeneralStatus>();
			static {
				for (GeneralStatus e : EnumSet.allOf(GeneralStatus.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private GeneralStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static GeneralStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum InvoiceSalesType {
			Dine_In((byte) 1), Take_Away((byte) 2), Delivery((byte) 3), Employee_Meal(
					(byte) 4);
			private static final Map<Byte, InvoiceSalesType> lookup = new HashMap<Byte, InvoiceSalesType>();
			static {
				for (InvoiceSalesType e : EnumSet.allOf(InvoiceSalesType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private InvoiceSalesType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static InvoiceSalesType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProductPricingDetail {
			Combo((byte) 1), Default((byte) 2);
			private static final Map<Byte, ProductPricingDetail> lookup = new HashMap<Byte, ProductPricingDetail>();
			static {
				for (ProductPricingDetail e : EnumSet
						.allOf(ProductPricingDetail.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private ProductPricingDetail(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProductPricingDetail get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ManagementStatus {
			Approve((byte) 1), Reject((byte) 2), Hold((byte) 3), Amended(
					(byte) 4);
			private static final Map<Byte, ManagementStatus> lookup = new HashMap<Byte, ManagementStatus>();
			static {
				for (ManagementStatus e : EnumSet.allOf(ManagementStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ManagementStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ManagementStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RequestStatus {
			Completed((byte) 1), Inprogress((byte) 2), Pending((byte) 3);
			private static final Map<Byte, RequestStatus> lookup = new HashMap<Byte, RequestStatus>();
			static {
				for (RequestStatus e : EnumSet.allOf(RequestStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private RequestStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RequestStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PaymentRequestStatus {
			Active((byte) 1), Inactive((byte) 2);
			private static final Map<Byte, PaymentRequestStatus> lookup = new HashMap<Byte, PaymentRequestStatus>();
			static {
				for (PaymentRequestStatus e : EnumSet
						.allOf(PaymentRequestStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PaymentRequestStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PaymentRequestStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RequestDetailStatus {
			Approve((byte) 1), Reject((byte) 2), Hold((byte) 3), Amended(
					(byte) 4);
			private static final Map<Byte, RequestDetailStatus> lookup = new HashMap<Byte, RequestDetailStatus>();
			static {
				for (RequestDetailStatus e : EnumSet
						.allOf(RequestDetailStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private RequestDetailStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RequestDetailStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PaymentRequestMode {
			Cheque((byte) 1), Transfer((byte) 2), Direct_Debit((byte) 3), Cash(
					(byte) 4), Credit_card((byte) 5);
			private static final Map<Byte, PaymentRequestMode> lookup = new HashMap<Byte, PaymentRequestMode>();
			static {
				for (PaymentRequestMode e : EnumSet
						.allOf(PaymentRequestMode.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PaymentRequestMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PaymentRequestMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum EiborType {
			OneMonth('O'), ThreeMonth('T'), SixMonth('S');
			private static final Map<Character, EiborType> lookup = new HashMap<Character, EiborType>();

			static {
				for (EiborType e : EnumSet.allOf(EiborType.class))
					lookup.put(e.getCode(), e);
			}

			private char code;

			private EiborType(char code) {
				this.code = code;
			}

			public char getCode() {
				return code;
			}

			public static EiborType get(char code) {
				return lookup.get(code);
			}
		}

		public static enum InventorySubCategory {
			Raw_Material((byte) 1), Under_Production((byte) 2), Finished_Goods(
					(byte) 3), Unfinished_Goods((byte) 4), Packaging_Materials(
					(byte) 5), Supplies((byte) 6), Office_Product((byte) 7), Other_Expense(
					(byte) 8), Tangible((byte) 9), InTangible((byte) 10), Services(
					(byte) 11), Craft0Manufacturer((byte) 12);
			private static final Map<Byte, InventorySubCategory> lookup = new HashMap<Byte, InventorySubCategory>();
			static {
				for (InventorySubCategory e : EnumSet
						.allOf(InventorySubCategory.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private InventorySubCategory(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static InventorySubCategory get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum InventoryCategory {
			Raw_Material((byte) 1), Under_Production((byte) 2), Finished_Goods(
					(byte) 3), Unfinished_Goods((byte) 4), Packaging_Materials(
					(byte) 5), Supplies((byte) 6), Services((byte) 11), Craft0Manufacturer(
					(byte) 12);
			private static final Map<Byte, InventoryCategory> lookup = new HashMap<Byte, InventoryCategory>();
			static {
				for (InventoryCategory e : EnumSet
						.allOf(InventoryCategory.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private InventoryCategory(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static InventoryCategory get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ExpenseCategory {
			Office_Product((byte) 7), Other_Expense((byte) 8);
			private static final Map<Byte, ExpenseCategory> lookup = new HashMap<Byte, ExpenseCategory>();
			static {
				for (ExpenseCategory e : EnumSet.allOf(ExpenseCategory.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ExpenseCategory(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ExpenseCategory get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetCategory {
			Tangible((byte) 9), InTangible((byte) 10);
			private static final Map<Byte, AssetCategory> lookup = new HashMap<Byte, AssetCategory>();
			static {
				for (AssetCategory e : EnumSet.allOf(AssetCategory.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private AssetCategory(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetCategory get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PenaltyType {
			Daily((byte) 1), Weekly((byte) 2), Monthly((byte) 3);
			private static final Map<Byte, PenaltyType> lookup = new HashMap<Byte, PenaltyType>();
			static {
				for (PenaltyType e : EnumSet.allOf(PenaltyType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PenaltyType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PenaltyType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PaymentTermBaseDay {
			Invoice((byte) 1), System((byte) 2), Transaction((byte) 3);
			private static final Map<Byte, PaymentTermBaseDay> lookup = new HashMap<Byte, PaymentTermBaseDay>();
			static {
				for (PaymentTermBaseDay e : EnumSet
						.allOf(PaymentTermBaseDay.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PaymentTermBaseDay(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PaymentTermBaseDay get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ReconciliationAdjustmentType {
			Payment((byte) 1), Receipt((byte) 2);
			private static final Map<Byte, ReconciliationAdjustmentType> lookup = new HashMap<Byte, ReconciliationAdjustmentType>();
			static {
				for (ReconciliationAdjustmentType e : EnumSet
						.allOf(ReconciliationAdjustmentType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ReconciliationAdjustmentType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ReconciliationAdjustmentType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum SupplierType {
			Goverment((byte) 1), Private((byte) 2);
			private static final Map<Byte, SupplierType> lookup = new HashMap<Byte, SupplierType>();
			static {
				for (SupplierType e : EnumSet.allOf(SupplierType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private SupplierType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static SupplierType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum SupplierClassification {
			RelatedParty((byte) 1), SME((byte) 2);
			private static final Map<Byte, SupplierClassification> lookup = new HashMap<Byte, SupplierClassification>();
			static {
				for (SupplierClassification e : EnumSet
						.allOf(SupplierClassification.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private SupplierClassification(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static SupplierClassification get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProductCalculationType {
			Fixed_Price((byte) 1), Markup((byte) 2), Margin((byte) 3), Fixed_Price_Markup(
					(byte) 4), Fixed_Price_Margin((byte) 5);
			private static final Map<Byte, ProductCalculationType> lookup = new HashMap<Byte, ProductCalculationType>();
			static {
				for (ProductCalculationType e : EnumSet
						.allOf(ProductCalculationType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private ProductCalculationType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProductCalculationType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProductCalculationSubType {
			Percentage((byte) 1), Amount((byte) 2);
			private static final Map<Byte, ProductCalculationSubType> lookup = new HashMap<Byte, ProductCalculationSubType>();
			static {
				for (ProductCalculationSubType e : EnumSet
						.allOf(ProductCalculationSubType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private ProductCalculationSubType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProductCalculationSubType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PaymentMode {
			Cash((byte) 1), Bank((byte) 2);
			private static final Map<Byte, PaymentMode> lookup = new HashMap<Byte, PaymentMode>();
			static {
				for (PaymentMode e : EnumSet.allOf(PaymentMode.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PaymentMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PaymentMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PeriodStatus {
			Open((byte) 1), Close((byte) 2), Inactive((byte) 3), Temporary_Closed(
					(byte) 4);
			private static final Map<Byte, PeriodStatus> lookup = new HashMap<Byte, PeriodStatus>();
			static {
				for (PeriodStatus e : EnumSet.allOf(PeriodStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PeriodStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PeriodStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CalendarStatus {
			Active((byte) 1), Inactive((byte) 2);
			private static final Map<Byte, CalendarStatus> lookup = new HashMap<Byte, CalendarStatus>();
			static {
				for (CalendarStatus e : EnumSet.allOf(CalendarStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private CalendarStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CalendarStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ModeOfPayment {
			Cash((byte) 1), Cheque((byte) 2), Debit_Card((byte) 3), Credit_Card(
					(byte) 4), Wire_Transfer((byte) 5), Inter_Transfer((byte) 6);
			private static final Map<Byte, ModeOfPayment> lookup = new HashMap<Byte, ModeOfPayment>();
			static {
				for (ModeOfPayment e : EnumSet.allOf(ModeOfPayment.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ModeOfPayment(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ModeOfPayment get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum StoreBinType {
			High((byte) 1), Medium((byte) 2), Low((byte) 3);
			private static final Map<Byte, StoreBinType> lookup = new HashMap<Byte, StoreBinType>();
			static {
				for (StoreBinType e : EnumSet.allOf(StoreBinType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private StoreBinType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static StoreBinType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PettyCashTypes {
			Advance_issued((byte) 2), Carry_forward((byte) 7), Cash_transfer(
					(byte) 1), Cash_given_settlement((byte) 5), Cash_received_settlement(
					(byte) 4), Reimbursed((byte) 6), Spend((byte) 3);
			private static final Map<Byte, PettyCashTypes> lookup = new HashMap<Byte, PettyCashTypes>();
			static {
				for (PettyCashTypes e : EnumSet.allOf(PettyCashTypes.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PettyCashTypes(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PettyCashTypes get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PettyCashTypeCode {
			AIPC((byte) 2), CFRD((byte) 7), CTPC((byte) 1), CGFS((byte) 5), CRFS(
					(byte) 4), RFPC((byte) 6), SFAI((byte) 3);
			private static final Map<Byte, PettyCashTypeCode> lookup = new HashMap<Byte, PettyCashTypeCode>();
			static {
				for (PettyCashTypeCode e : EnumSet
						.allOf(PettyCashTypeCode.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PettyCashTypeCode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PettyCashTypeCode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProductionRequisitionStatus {
			Open((byte) 1), Processing((byte) 2), Closed((byte) 3);
			private static final Map<Byte, ProductionRequisitionStatus> lookup = new HashMap<Byte, ProductionRequisitionStatus>();
			static {
				for (ProductionRequisitionStatus e : EnumSet
						.allOf(ProductionRequisitionStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProductionRequisitionStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProductionRequisitionStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum POStatus {
			Open((byte) 1), Received((byte) 2), Cancelled((byte) 3), Others(
					(byte) 4), InProgress((byte) 5);
			private static final Map<Byte, POStatus> lookup = new HashMap<Byte, POStatus>();
			static {
				for (POStatus e : EnumSet.allOf(POStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private POStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static POStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RequisitionProcessStatus {
			Open((byte) 1), Issued((byte) 2), InProgress((byte) 3);
			private static final Map<Byte, RequisitionProcessStatus> lookup = new HashMap<Byte, RequisitionProcessStatus>();
			static {
				for (RequisitionProcessStatus e : EnumSet
						.allOf(RequisitionProcessStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private RequisitionProcessStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RequisitionProcessStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectStatus {
			Open((byte) 1), Requested((byte) 2), Delivered((byte) 3), Closed(
					(byte) 99);
			private static final Map<Byte, ProjectStatus> lookup = new HashMap<Byte, ProjectStatus>();
			static {
				for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum Priority {
			Normal((byte) 1), Low((byte) 2), High((byte) 3), Immediate((byte) 4);
			private static final Map<Byte, Priority> lookup = new HashMap<Byte, Priority>();
			static {
				for (Priority e : EnumSet.allOf(Priority.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private Priority(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static Priority get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectPaymentStatus {
			Pending((byte) 1), Paid((byte) 2), Hold((byte) 3), Cancelled(
					(byte) 4);
			private static final Map<Byte, ProjectPaymentStatus> lookup = new HashMap<Byte, ProjectPaymentStatus>();
			static {
				for (ProjectPaymentStatus e : EnumSet
						.allOf(ProjectPaymentStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectPaymentStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectPaymentStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectResourceType {
			Labour((byte) 2);
			private static final Map<Byte, ProjectResourceType> lookup = new HashMap<Byte, ProjectResourceType>();
			static {
				for (ProjectResourceType e : EnumSet
						.allOf(ProjectResourceType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectResourceType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectResourceType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectExpenseMode {
			Estimated_Expense((byte) 1), General_Expense((byte) 2);
			private static final Map<Byte, ProjectExpenseMode> lookup = new HashMap<Byte, ProjectExpenseMode>();
			static {
				for (ProjectExpenseMode e : EnumSet
						.allOf(ProjectExpenseMode.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectExpenseMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectExpenseMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectDeliveryStatus {
			Delivered((byte) 1), Pending((byte) 2), Hold((byte) 3), Cancelled(
					(byte) 4);
			private static final Map<Byte, ProjectDeliveryStatus> lookup = new HashMap<Byte, ProjectDeliveryStatus>();
			static {
				for (ProjectDeliveryStatus e : EnumSet
						.allOf(ProjectDeliveryStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectDeliveryStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectDeliveryStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ProjectPriority {
			High((byte) 1), Medium((byte) 2), Low((byte) 3);
			private static final Map<Byte, ProjectPriority> lookup = new HashMap<Byte, ProjectPriority>();
			static {
				for (ProjectPriority e : EnumSet.allOf(ProjectPriority.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ProjectPriority(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ProjectPriority get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PaymentTerms {
			Daily((byte) 1), Weekly((byte) 2), Monthly((byte) 3), Quaterly(
					(byte) 4), SemiAnually((byte) 5), Anually((byte) 6);
			private static final Map<Byte, PaymentTerms> lookup = new HashMap<Byte, PaymentTerms>();
			static {
				for (PaymentTerms e : EnumSet.allOf(PaymentTerms.class))
					lookup.put(e.getCode(), e);
			}
			private Byte code;

			private PaymentTerms(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PaymentTerms get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum LoanType {
			Islamic('I'), Commercial('R'), Personal('P'), Car('C'), Auto('A'), Overdraft(
					'O');
			private static final Map<Character, LoanType> lookup = new HashMap<Character, LoanType>();

			static {
				for (LoanType e : EnumSet.allOf(LoanType.class))
					lookup.put(e.getCode(), e);
			}

			private char code;

			private LoanType(char code) {
				this.code = code;
			}

			public char getCode() {
				return code;
			}

			public static LoanType get(char code) {
				return lookup.get(code);
			}
		}

		public static enum LoanFrequency {
			Daily('D'), Weekly('W'), Monthly('M'), Quaterly('Q'), SemiAnually(
					'H'), Anually('Y');
			private static final Map<Character, LoanFrequency> lookup = new HashMap<Character, LoanFrequency>();
			static {
				for (LoanFrequency e : EnumSet.allOf(LoanFrequency.class))
					lookup.put(e.getCode(), e);
			}
			private char code;

			private LoanFrequency(char code) {
				this.code = code;
			}

			public char getCode() {
				return code;
			}

			public static LoanFrequency get(char code) {
				return lookup.get(code);
			}
		}

		public static enum AssetInsurancePeriod {
			Daily((byte) 1), Weekly((byte) 2), Monthly((byte) 3), Quaterly(
					(byte) 4), SemiAnually((byte) 5), Anually((byte) 6);
			private static final Map<Byte, AssetInsurancePeriod> lookup = new HashMap<Byte, AssetInsurancePeriod>();
			static {
				for (AssetInsurancePeriod e : EnumSet
						.allOf(AssetInsurancePeriod.class))
					lookup.put(e.getCode(), e);
			}
			private Byte code;

			private AssetInsurancePeriod(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetInsurancePeriod get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetDetail {
			BMW(1L), Mercedez(2L), Lamborghini(3L);
			private static final Map<Long, AssetDetail> lookup = new HashMap<Long, AssetDetail>();
			static {
				for (AssetDetail e : EnumSet.allOf(AssetDetail.class))
					lookup.put(e.getCode(), e);
			}

			private Long code;

			private AssetDetail(Long code) {
				this.code = code;
			}

			public Long getCode() {
				return code;
			}

			public static AssetDetail get(Long code) {
				return lookup.get(code);
			}
		}

		public static enum CustomerType {
			Wholesale(1), Special(2), Distributor(3), Main(4), Retail(5), Individual(
					7), Private(8), Goverment(9), Others(6);
			private static final Map<Integer, CustomerType> lookup = new HashMap<Integer, CustomerType>();
			static {
				for (CustomerType e : EnumSet.allOf(CustomerType.class))
					lookup.put(e.getCode(), e);
			}

			private Integer code;

			private CustomerType(Integer code) {
				this.code = code;
			}

			public Integer getCode() {
				return code;
			}

			public static CustomerType get(Integer code) {
				return lookup.get(code);
			}
		}

		public static enum WastageTypes {
			Production_Wastage((byte) 1), General_Wastage((byte) 2);
			private static final Map<Byte, WastageTypes> lookup = new HashMap<Byte, WastageTypes>();
			static {
				for (WastageTypes e : EnumSet.allOf(WastageTypes.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private WastageTypes(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static WastageTypes get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetCondition {
			Good((byte) 1), New((byte) 2), Old((byte) 3), Damaged((byte) 4), Others(
					(byte) 5);
			private static final Map<Byte, AssetCondition> lookup = new HashMap<Byte, AssetCondition>();
			static {
				for (AssetCondition e : EnumSet.allOf(AssetCondition.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private AssetCondition(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetCondition get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetColor {
			Black((byte) 1), White((byte) 2), Gray((byte) 3);
			private static final Map<Byte, AssetColor> lookup = new HashMap<Byte, AssetColor>();
			static {
				for (AssetColor e : EnumSet.allOf(AssetColor.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private AssetColor(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetColor get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PlateType {
			Private((byte) 1), Goverment((byte) 2), SemiGoverment((byte) 3);
			private static final Map<Byte, PlateType> lookup = new HashMap<Byte, PlateType>();
			static {
				for (PlateType e : EnumSet.allOf(PlateType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private PlateType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PlateType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetCheckOutDue {
			Unlimited((byte) 1), Service((byte) 2), Date((byte) 3);
			private static final Map<Byte, AssetCheckOutDue> lookup = new HashMap<Byte, AssetCheckOutDue>();
			static {
				for (AssetCheckOutDue e : EnumSet.allOf(AssetCheckOutDue.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private AssetCheckOutDue(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetCheckOutDue get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CustomerNature {
			Person((byte) 1), Company((byte) 2), CmpDeptLocation((byte) 3);
			private static final Map<Byte, CustomerNature> lookup = new HashMap<Byte, CustomerNature>();
			static {
				for (CustomerNature e : EnumSet.allOf(CustomerNature.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CustomerNature(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CustomerNature get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetServiceLevel {
			Annually((byte) 1), Monthly((byte) 2), Weekly((byte) 3);
			private static final Map<Byte, AssetServiceLevel> lookup = new HashMap<Byte, AssetServiceLevel>();
			static {
				for (AssetServiceLevel e : EnumSet
						.allOf(AssetServiceLevel.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private AssetServiceLevel(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetServiceLevel get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum AssetPriorityType {
			High((byte) 1), Medium((byte) 2), Low((byte) 3);
			private static final Map<Byte, AssetPriorityType> lookup = new HashMap<Byte, AssetPriorityType>();
			static {
				for (AssetPriorityType e : EnumSet
						.allOf(AssetPriorityType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private AssetPriorityType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static AssetPriorityType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DepreciationMethod {
			Straight_Line((byte) 1), Declinig_Balance((byte) 2), Declinig_Balance_Percentage(
					(byte) 3), Double_Declinig_Balance((byte) 4), Sum_Of_Year_Digit(
					(byte) 5);
			private static final Map<Byte, DepreciationMethod> lookup = new HashMap<Byte, DepreciationMethod>();
			static {
				for (DepreciationMethod e : EnumSet
						.allOf(DepreciationMethod.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DepreciationMethod(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DepreciationMethod get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DepreciationConvention {
			Full_Month((byte) 1), Half_Year((byte) 2), Full_Year((byte) 3);
			private static final Map<Byte, DepreciationConvention> lookup = new HashMap<Byte, DepreciationConvention>();
			static {
				for (DepreciationConvention e : EnumSet
						.allOf(DepreciationConvention.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DepreciationConvention(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DepreciationConvention get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ValuationType {
			Information((byte) 1), Revaluation((byte) 2);
			private static final Map<Byte, ValuationType> lookup = new HashMap<Byte, ValuationType>();
			static {
				for (ValuationType e : EnumSet.allOf(ValuationType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private ValuationType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ValuationType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RevaluationType {
			Increment((byte) 1), Impairment((byte) 2);
			private static final Map<Byte, RevaluationType> lookup = new HashMap<Byte, RevaluationType>();
			static {
				for (RevaluationType e : EnumSet.allOf(RevaluationType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private RevaluationType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RevaluationType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RevaluationMethod {
			RNV((byte) 1), WDRC((byte) 2);
			private static final Map<Byte, RevaluationMethod> lookup = new HashMap<Byte, RevaluationMethod>();
			static {
				for (RevaluationMethod e : EnumSet
						.allOf(RevaluationMethod.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private RevaluationMethod(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RevaluationMethod get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RevalautionSource {
			Internal((byte) 1), External((byte) 2);
			private static final Map<Byte, RevalautionSource> lookup = new HashMap<Byte, RevalautionSource>();
			static {
				for (RevalautionSource e : EnumSet
						.allOf(RevalautionSource.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private RevalautionSource(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RevalautionSource get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum SourceInternalDetail {
			Evidence_Of_Obsolscence((byte) 1), Physical_Damage((byte) 2), Asset_Economics_Performance(
					(byte) 3);
			private static final Map<Byte, SourceInternalDetail> lookup = new HashMap<Byte, SourceInternalDetail>();
			static {
				for (SourceInternalDetail e : EnumSet
						.allOf(SourceInternalDetail.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private SourceInternalDetail(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static SourceInternalDetail get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum SourceExternalDetail {
			Fall_Market_Value((byte) 1), Technological_Changes((byte) 2), Market_Intrest_Increases(
					(byte) 3);
			private static final Map<Byte, SourceExternalDetail> lookup = new HashMap<Byte, SourceExternalDetail>();
			static {
				for (SourceExternalDetail e : EnumSet
						.allOf(SourceExternalDetail.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private SourceExternalDetail(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static SourceExternalDetail get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RevalautionApproach {
			Income((byte) 1), Market((byte) 2);
			private static final Map<Byte, RevalautionApproach> lookup = new HashMap<Byte, RevalautionApproach>();
			static {
				for (RevalautionApproach e : EnumSet
						.allOf(RevalautionApproach.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private RevalautionApproach(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RevalautionApproach get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DisposalMethod {
			Voluntary_Disposal((byte) 1), Sales_Exchange((byte) 2), Abondment_Scrap(
					(byte) 3), Involuntry_Conversion((byte) 4), Charity(
					(byte) 5), Destroyed((byte) 6), Others((byte) 7);
			private static final Map<Byte, DisposalMethod> lookup = new HashMap<Byte, DisposalMethod>();
			static {
				for (DisposalMethod e : EnumSet.allOf(DisposalMethod.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DisposalMethod(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DisposalMethod get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DisposalType {
			Simple_Disposal((byte) 1), Disposal_Cash_Proceed((byte) 2), Disposal_TradeIn(
					(byte) 3), Disposal_TradeIn_Cash_Proceed((byte) 4);
			private static final Map<Byte, DisposalType> lookup = new HashMap<Byte, DisposalType>();
			static {
				for (DisposalType e : EnumSet.allOf(DisposalType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DisposalType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DisposalType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CommissionBase {
			COGP((byte) 1), COTS((byte) 2), Fixed((byte) 3), None((byte) 4);
			private static final Map<Byte, CommissionBase> lookup = new HashMap<Byte, CommissionBase>();
			static {
				for (CommissionBase e : EnumSet.allOf(CommissionBase.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CommissionBase(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CommissionBase get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CalculationMethod {
			Flat((byte) 1), Range((byte) 2), Fixed_Amount((byte) 3), Flat_and_Range(
					(byte) 4), Flat_and_Amount((byte) 5);
			private static final Map<Byte, CalculationMethod> lookup = new HashMap<Byte, CalculationMethod>();
			static {
				for (CalculationMethod e : EnumSet
						.allOf(CalculationMethod.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CalculationMethod(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CalculationMethod get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DiscountCalcMethod {
			Flat_Percentage((byte) 1), Flat_Amount((byte) 2), Fixed_Amount_and_Fixed_Percentage(
					(byte) 3), Fixed_Amount_and_Variable_Percentage((byte) 4), Variable_Amount_and_Fixed_Percentage(
					(byte) 5), Variable_Amount_and_Variable_Percentage((byte) 6);
			private static final Map<Byte, DiscountCalcMethod> lookup = new HashMap<Byte, DiscountCalcMethod>();
			static {
				for (DiscountCalcMethod e : EnumSet
						.allOf(DiscountCalcMethod.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DiscountCalcMethod(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DiscountCalcMethod get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DiscountOptions {
			Single_Product((byte) 1), Multi_Product((byte) 2), Customer(
					(byte) 3);
			private static final Map<Byte, DiscountOptions> lookup = new HashMap<Byte, DiscountOptions>();
			static {
				for (DiscountOptions e : EnumSet.allOf(DiscountOptions.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DiscountOptions(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DiscountOptions get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum DiscountOnPrice {
			Sale_Price((byte) 1), Regular_Price((byte) 2), Discounted_Price(
					(byte) 3), Employees((byte) 4), Retail_Price((byte) 5);
			private static final Map<Byte, DiscountOnPrice> lookup = new HashMap<Byte, DiscountOnPrice>();
			static {
				for (DiscountOnPrice e : EnumSet.allOf(DiscountOnPrice.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private DiscountOnPrice(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static DiscountOnPrice get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CouponType {
			Free_Standing((byte) 1), On_Shelf((byte) 2), CheckOut((byte) 3), Online(
					(byte) 4), Mobile((byte) 5);
			private static final Map<Byte, CouponType> lookup = new HashMap<Byte, CouponType>();
			static {
				for (CouponType e : EnumSet.allOf(CouponType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CouponType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CouponType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum MemberCardType {
			Member((byte) 1), Sliver((byte) 2), Gold((byte) 3), Platinum(
					(byte) 4);
			private static final Map<Byte, MemberCardType> lookup = new HashMap<Byte, MemberCardType>();
			static {
				for (MemberCardType e : EnumSet.allOf(MemberCardType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private MemberCardType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static MemberCardType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum InstitutionType {
			Bank((byte) 1), Building_Socities((byte) 2), Check_Buisness(
					(byte) 3), Check_Cashier((byte) 4), Credit_Unions((byte) 5), Insurance_Company(
					(byte) 6), Trust_Company((byte) 7), Others((byte) 8);
			private static final Map<Byte, InstitutionType> lookup = new HashMap<Byte, InstitutionType>();
			static {
				for (InstitutionType e : EnumSet.allOf(InstitutionType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private InstitutionType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static InstitutionType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum BankAccountType {
			Savings((byte) 1), Current((byte) 2), Salary((byte) 3);
			private static final Map<Byte, BankAccountType> lookup = new HashMap<Byte, BankAccountType>();
			static {
				for (BankAccountType e : EnumSet.allOf(BankAccountType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private BankAccountType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static BankAccountType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum BankCardType {
			Debit((byte) 1), Credit((byte) 2);
			private static final Map<Byte, BankCardType> lookup = new HashMap<Byte, BankCardType>();
			static {
				for (BankCardType e : EnumSet.allOf(BankCardType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private BankCardType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static BankCardType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CreditTermDueDay {
			Invoice((byte) 1), System((byte) 2), Transaction((byte) 3);
			private static final Map<Byte, CreditTermDueDay> lookup = new HashMap<Byte, CreditTermDueDay>();
			static {
				for (CreditTermDueDay e : EnumSet.allOf(CreditTermDueDay.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CreditTermDueDay(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CreditTermDueDay get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CreditTermDiscountMode {
			Amount((byte) 1), Percentage((byte) 2);
			private static final Map<Byte, CreditTermDiscountMode> lookup = new HashMap<Byte, CreditTermDiscountMode>();
			static {
				for (CreditTermDiscountMode e : EnumSet
						.allOf(CreditTermDiscountMode.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CreditTermDiscountMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CreditTermDiscountMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum O2CProcessStatus {
			Awaiting_Response((byte) 1), Awarded((byte) 2), Closed((byte) 3), Delivered(
					(byte) 4), Follow_up((byte) 5), In_Progress((byte) 6), Open(
					(byte) 7), Invoiced((byte) 8), Paid((byte) 9);
			private static final Map<Byte, O2CProcessStatus> lookup = new HashMap<Byte, O2CProcessStatus>();
			static {
				for (O2CProcessStatus e : EnumSet.allOf(O2CProcessStatus.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private O2CProcessStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static O2CProcessStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RequisitionStatus {
			Open((byte) 1), Closed((byte) 2), Completed((byte) 3), Processing(
					(byte) 4), Order_Ready((byte) 5);
			private static final Map<Byte, RequisitionStatus> lookup = new HashMap<Byte, RequisitionStatus>();
			static {
				for (RequisitionStatus e : EnumSet
						.allOf(RequisitionStatus.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private RequisitionStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RequisitionStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CreditTermPenaltyType {
			Daily((byte) 1), Weekly((byte) 2), Monthly((byte) 3), Yearly(
					(byte) 4);
			private static final Map<Byte, CreditTermPenaltyType> lookup = new HashMap<Byte, CreditTermPenaltyType>();
			static {
				for (CreditTermPenaltyType e : EnumSet
						.allOf(CreditTermPenaltyType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CreditTermPenaltyType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CreditTermPenaltyType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum RewardType {
			Member((byte) 1), Sliver((byte) 2), Gold((byte) 3), Platinum(
					(byte) 4), Open((byte) 5);
			private static final Map<Byte, RewardType> lookup = new HashMap<Byte, RewardType>();
			static {
				for (RewardType e : EnumSet.allOf(RewardType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private RewardType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static RewardType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PromotionRewardName {
			Price_deal((byte) 1), Loyal_reward_program((byte) 2), Price_pack_deal(
					(byte) 3), Coupons((byte) 4), Buy_X_get_Y_off((byte) 5);
			private static final Map<Byte, PromotionRewardName> lookup = new HashMap<Byte, PromotionRewardName>();
			static {
				for (PromotionRewardName e : EnumSet
						.allOf(PromotionRewardName.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private PromotionRewardName(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PromotionRewardName get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PACType {
			Percentage((byte) 1), Amount((byte) 2), Coupon((byte) 3), Promotion_Points(
					(byte) 4);
			private static final Map<Byte, PACType> lookup = new HashMap<Byte, PACType>();
			static {
				for (PACType e : EnumSet.allOf(PACType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private PACType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PACType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum POSPaymentType {
			Cash((byte) 1), Cheque((byte) 2), Card((byte) 3), Coupon((byte) 4), Exchange(
					(byte) 5), Voucher((byte) 6);
			private static final Map<Byte, POSPaymentType> lookup = new HashMap<Byte, POSPaymentType>();
			static {
				for (POSPaymentType e : EnumSet.allOf(POSPaymentType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private POSPaymentType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static POSPaymentType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CurrencyMode {
			Cash((byte) 1), Cheque((byte) 2), Card((byte) 3);
			private static final Map<Byte, CurrencyMode> lookup = new HashMap<Byte, CurrencyMode>();
			static {
				for (CurrencyMode e : EnumSet.allOf(CurrencyMode.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CurrencyMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CurrencyMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CurrencyType {
			Paper((byte) 1), Coin((byte) 2), Card((byte) 3);
			private static final Map<Byte, CurrencyType> lookup = new HashMap<Byte, CurrencyType>();
			static {
				for (CurrencyType e : EnumSet.allOf(CurrencyType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private CurrencyType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CurrencyType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ReceiptType {
			Receipts((byte) 1), POS((byte) 2);
			private static final Map<Byte, ReceiptType> lookup = new HashMap<Byte, ReceiptType>();
			static {
				for (ReceiptType e : EnumSet.allOf(ReceiptType.class)) {
					lookup.put(e.getCode(), e);
				}
			}
			private Byte code;

			private ReceiptType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ReceiptType get(Byte code) {
				return lookup.get(code);
			}
		}
	}

	public static final class PatientCare {

		public static enum ScheduleDays {
			Sunday((byte) 1), Monday((byte) 2), Tuesday((byte) 3), Wednesday(
					(byte) 4), Thursday((byte) 5), Friday((byte) 6), Saturday(
					(byte) 7);
			private static final Map<Byte, ScheduleDays> lookup = new HashMap<Byte, ScheduleDays>();
			static {
				for (ScheduleDays e : EnumSet.allOf(ScheduleDays.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ScheduleDays(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ScheduleDays get(Byte code) {
				return lookup.get(code);
			}
		}

	}

	public static final class RealEstate {
		/*
		 * public final String asset = "Assets"; public final String liabilites
		 * = "Liabilites"; public final String revenue = "Revenue"; public final
		 * String expenses = "Expenses"; public final String ownersEquity =
		 * "Owners Equity";
		 */

		public static enum ContractFeeType {
			Fee((byte) 1), Deposit((byte) 2), Rent((byte) 3);
			private static final Map<Byte, ContractFeeType> lookup = new HashMap<Byte, ContractFeeType>();
			static {
				for (ContractFeeType e : EnumSet.allOf(ContractFeeType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ContractFeeType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ContractFeeType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PropertyExpenseType {
			Maintainance((byte) 1), Utility((byte) 2), Others((byte) 3);
			private static final Map<Byte, PropertyExpenseType> lookup = new HashMap<Byte, PropertyExpenseType>();
			static {
				for (PropertyExpenseType e : EnumSet
						.allOf(PropertyExpenseType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PropertyExpenseType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PropertyExpenseType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum Status {
			Confirm(4), // approval (waiting for approval)
			Deny(2), NoDecession(3), // Entry
			Publish(9), // approval (waiting for publish)
			Saved(5), Post(6), Posted(7), Approved(8), Published(1), DeleteApproval(
					10), // waiting for delete approval
			Deleted(11), // finally deleted the specified record (i.e. set
							// record's status = 11)
			DeleteRejected(12), // delete request has been rejected
			DeleteApprovalReadOnly(13), // used to show mark as read message
										// pop-up for
										// "delete request has been sent"
			OldPublished(50);// being used to differentiate the record from
								// edited and old published.
			private static final Map<Integer, Status> lookup = new HashMap<Integer, Status>();

			static {
				for (Status s : EnumSet.allOf(Status.class))
					lookup.put(s.getCode(), s);
			}

			private int code;

			private Status(int code) {
				this.code = code;
			}

			public int getCode() {
				return code;
			}

			public static Status get(int code) {
				return lookup.get(code);
			}
		}

		public static enum PropertyType {
			Villa(1), Shop(6), Flat(7);
			private static final Map<Integer, PropertyType> lookup = new HashMap<Integer, PropertyType>();

			static {
				for (PropertyType p : EnumSet.allOf(PropertyType.class))
					lookup.put(p.getCode(), p);
			}

			private int code;

			private PropertyType(int code) {
				this.code = code;
			}

			public int getCode() {
				return code;
			}

			public static PropertyType get(int code) {
				return lookup.get(code);
			}
		}

		public static enum UnitStatus {
			NOTAPPLICABLE(0), AVAILABLE(1), PENDING(2), RENTED(3), SOLD(4), RENOVATION(
					5), OFFERED(6);
			private static final Map<Integer, UnitStatus> lookup = new HashMap<Integer, UnitStatus>();

			static {
				for (UnitStatus s : EnumSet.allOf(UnitStatus.class))
					lookup.put(s.getCode(), s);
			}

			private int code;

			private UnitStatus(int code) {
				this.code = code;
			}

			public int getCode() {
				return code;
			}

			public static UnitStatus get(int code) {
				return lookup.get(code);
			}
		}
	}

	public static final class HR {

		public static enum LeaveStatus {
			Cancelled(0), Pending(1), Taken(2), NotTaken(3), OnLeave(4);

			private static final Map<Integer, LeaveStatus> lookup = new HashMap<Integer, LeaveStatus>();

			static {
				for (LeaveStatus s : EnumSet.allOf(LeaveStatus.class))
					lookup.put(s.getCode(), s);
			}

			private int code;

			private LeaveStatus(int code) {
				this.code = code;
			}

			public int getCode() {
				return code;
			}

			public static LeaveStatus get(int code) {
				return lookup.get(code);
			}
		}

		public static enum InterviewStatus {
			Initiated(1), Scheduled(2), Interviewed(3), ShortListed(4), Offered(
					5), OfferAccepted(6), JobAssigned(7), OfferDenied(8), Holded(
					9), Rejected(10);

			private static final Map<Integer, InterviewStatus> lookup = new HashMap<Integer, InterviewStatus>();

			static {
				for (InterviewStatus s : EnumSet.allOf(InterviewStatus.class))
					lookup.put(s.getCode(), s);
			}

			private int code;

			private InterviewStatus(int code) {
				this.code = code;
			}

			public int getCode() {
				return code;
			}

			public static InterviewStatus get(int code) {
				return lookup.get(code);
			}
		}

		public static enum Rattings {
			Insufficient(1), Ok(2), Good(3), VeryGood(4), Excellent(5);

			private static final Map<Integer, Rattings> lookup = new HashMap<Integer, Rattings>();

			static {
				for (Rattings s : EnumSet.allOf(Rattings.class))
					lookup.put(s.getCode(), s);
			}

			private int code;

			private Rattings(int code) {
				this.code = code;
			}

			public int getCode() {
				return code;
			}

			public static Rattings get(int code) {
				return lookup.get(code);
			}
		}

		public static enum PayMode {
			Salary((byte) 1), Cash((byte) 2), Transfer((byte) 3), Other(
					(byte) 4);
			private static final Map<Byte, PayMode> lookup = new HashMap<Byte, PayMode>();
			static {
				for (PayMode e : EnumSet.allOf(PayMode.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PayMode(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PayMode get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PayPolicy {
			Daily((byte) 1), Weekly((byte) 2), Monthly((byte) 3), Yearly(
					(byte) 4);
			private static final Map<Byte, PayPolicy> lookup = new HashMap<Byte, PayPolicy>();
			static {
				for (PayPolicy e : EnumSet.allOf(PayPolicy.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PayPolicy(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PayPolicy get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum CalculationType {
			Fixed((byte) 1), Count((byte) 2), WorkDay((byte) 3), Variance(
					(byte) 4), Percentage((byte) 5);
			private static final Map<Byte, CalculationType> lookup = new HashMap<Byte, CalculationType>();
			static {
				for (CalculationType e : EnumSet.allOf(CalculationType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private CalculationType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static CalculationType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PayTransactionStatus {
			New((byte) 1), LedgerPosted((byte) 2), Suspended((byte) 3);
			private static final Map<Byte, PayTransactionStatus> lookup = new HashMap<Byte, PayTransactionStatus>();
			static {
				for (PayTransactionStatus e : EnumSet
						.allOf(PayTransactionStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PayTransactionStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PayTransactionStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PayStatus {
			InProcess((byte) 1), Commit((byte) 2), Deposited((byte) 3), Cancelled(
					(byte) 4), Holded((byte) 5), Pending((byte) 6), Other(
					(byte) 0);
			private static final Map<Byte, PayStatus> lookup = new HashMap<Byte, PayStatus>();
			static {
				for (PayStatus e : EnumSet.allOf(PayStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PayStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PayStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum LoanFinanceType {
			Loan((byte) 1), Advance((byte) 2);
			private static final Map<Byte, LoanFinanceType> lookup = new HashMap<Byte, LoanFinanceType>();
			static {
				for (LoanFinanceType e : EnumSet.allOf(LoanFinanceType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private LoanFinanceType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static LoanFinanceType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PromotionType {
			Promotion((byte) 1), Demotion((byte) 2), PayIncrement((byte) 3), PayDecrement(
					(byte) 4);
			private static final Map<Byte, PromotionType> lookup = new HashMap<Byte, PromotionType>();
			static {
				for (PromotionType e : EnumSet.allOf(PromotionType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PromotionType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PromotionType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PromotionStatus {
			Activated((byte) 1), Approved((byte) 2), Holded((byte) 3), Created(
					(byte) 4);
			private static final Map<Byte, PromotionStatus> lookup = new HashMap<Byte, PromotionStatus>();
			static {
				for (PromotionStatus e : EnumSet.allOf(PromotionStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private PromotionStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static PromotionStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ResignationStatus {
			InActivated((byte) 1), Approved((byte) 2), Requested((byte) 3);
			private static final Map<Byte, ResignationStatus> lookup = new HashMap<Byte, ResignationStatus>();
			static {
				for (ResignationStatus e : EnumSet
						.allOf(ResignationStatus.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ResignationStatus(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ResignationStatus get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ResignationType {
			Resignation((byte) 1), Termination((byte) 2), EmployeeTransfer(
					(byte) 3);
			private static final Map<Byte, ResignationType> lookup = new HashMap<Byte, ResignationType>();
			static {
				for (ResignationType e : EnumSet.allOf(ResignationType.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ResignationType(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ResignationType get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum ResignationActions {
			NoticeByEmployer((byte) 1), NoticeByEmployee((byte) 2), NoAction(
					(byte) 3);
			private static final Map<Byte, ResignationActions> lookup = new HashMap<Byte, ResignationActions>();
			static {
				for (ResignationActions e : EnumSet
						.allOf(ResignationActions.class))
					lookup.put(e.getCode(), e);
			}

			private Byte code;

			private ResignationActions(Byte code) {
				this.code = code;
			}

			public Byte getCode() {
				return code;
			}

			public static ResignationActions get(Byte code) {
				return lookup.get(code);
			}
		}

		public static enum PayElementCode {
			BasicPay("BASIC"), HouseRentAllowance("HRA"), DependentAllowance(
					"DA"), TravelAllowance("TA");
			private static final Map<String, PayElementCode> lookup = new HashMap<String, PayElementCode>();
			static {
				for (PayElementCode e : EnumSet.allOf(PayElementCode.class))
					lookup.put(e.getCode(), e);
			}

			private String code;

			private PayElementCode(String code) {
				this.code = code;
			}

			public String getCode() {
				return code;
			}

			public static PayElementCode get(String code) {
				return lookup.get(code);
			}
		}

		public static enum AttendanceProcess {
			NoAction((Integer) 0), TimeAndQuarter((Integer) 25), TimeAndHalf(
					(Integer) 50), FullTime((Integer) 100), OneAndHalf(
					(Integer) 150), DoubleTime((Integer) 200);
			private static final Map<Integer, AttendanceProcess> lookup = new HashMap<Integer, AttendanceProcess>();
			static {
				for (AttendanceProcess e : EnumSet
						.allOf(AttendanceProcess.class))
					lookup.put(e.getCode(), e);
			}

			private Integer code;

			private AttendanceProcess(Integer code) {
				this.code = code;
			}

			public Integer getCode() {
				return code;
			}

			public static AttendanceProcess get(Integer code) {
				return lookup.get(code);
			}
		}
	}

	public static final class Business {
		// Register all the parent and child record information here
		// This is being used only in MOFA but the same method need in
		// ERP as well in order to maintain the generic logic
		public static enum ChildRecord {
			EventsDetails("Events");

			private static final Map<String, ChildRecord> lookup = new HashMap<String, ChildRecord>();

			static {
				for (ChildRecord s : EnumSet.allOf(ChildRecord.class)) {
					lookup.put(s.getChild(), s);
				}
			}

			private String child;

			private ChildRecord(String child) {
				this.child = child;
			}

			public String getChild() {
				return child;
			}

			public static ChildRecord get(String child) {
				return lookup.get(child);
			}
		}
	}

	public static enum ReturnProcessType {
		Exchange((byte) 1), DirectPayment((byte) 2), FreeReturn((byte) 3);
		private static final Map<Byte, ReturnProcessType> lookup = new HashMap<Byte, ReturnProcessType>();
		static {
			for (ReturnProcessType e : EnumSet.allOf(ReturnProcessType.class))
				lookup.put(e.getCode(), e);
		}

		private Byte code;

		private ReturnProcessType(Byte code) {
			this.code = code;
		}

		public Byte getCode() {
			return code;
		}

		public static ReturnProcessType get(Byte code) {
			return lookup.get(code);
		}
	}
}
