package com.aiotech.aios.common.to;

/***************************************************************************************
*
* Modified Log
* --------------------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* --------------------------------------------------------------------------------------
* 1.0		18 Oct 2010 	Prabhu			 		   Initial Version
* 1.0		28 Oct 2010 	Nagarajan T.	 		   Change the Action to Service

****************************************************************************************/

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class AlertMailTO implements Serializable {
	protected Logger logger = LogManager.getLogger(this.getClass());
	private String userMail;
	private String userPassword;
	private String toMailId;
	private String toMailSubject;
	private String toMailContent;
	private String type;
	private int companyId;
	private int applicationId;
	private int functionId;
	private String functionType;
	private String ccMailId;
	private String bccMailId;
	
	public String getCcMailId() {
		return ccMailId;
	}
	public void setCcMailId(String ccMailId) {
		this.ccMailId = ccMailId;
	}
	public String getBccMailId() {
		return bccMailId;
	}
	public void setBccMailId(String bccMailId) {
		this.bccMailId = bccMailId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public String getFunctionType() {
		return functionType;
	}
	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}
	public String getUserMail() {
		return userMail;
	}
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getToMailId() {
		return toMailId;
	}
	public void setToMailId(String toMailId) {
		this.toMailId = toMailId;
	}
	public String getToMailSubject() {
		return toMailSubject;
	}
	public void setToMailSubject(String toMailSubject) {
		this.toMailSubject = toMailSubject;
	}
	public String getToMailContent() {
		return toMailContent;
	}
	public void setToMailContent(String toMailContent) {
		this.toMailContent = toMailContent;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}
