package com.aiotech.aios.common.to;

/*******************************************************************************
*
* Create Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		31 Aug 2010 	Nagarajan T.	 		   Initial Version

******************************************************************************/
import java.io.Serializable;
import java.util.List;


public class UserSelectedCompanyTO implements Serializable{
	private int roleId;
	private int approvalLevel;
	private int employeeId;
	private String employeeName;
	private String leaveId;
	private String fromDate;
	private String toDate;
	private String noOfDays;
	private String reason;
	private String sortType = "desc";
	private String sort = "leave_id";
	private int pageNum = 0;
	private String functionId;
	private String actionId;
	private String functionName;
	private List<UserSelectedCompanyTO> functionArray;
	private String lookUpId;
	private String lookUpName;
	private String lookUpDescription;
	private String lookUpTypeId;
	private String status;
	private int notifyId;
	private String notifyName;
	private int ledgerId;
	
	private String menuId;
	private String subMenuId;
	private String prompt;
	private String level;
	private String childDetails;
	
	private String fileName;
	private String userId;
	private int personId;
	
	private String userName;
	private int userStatus;
	
	private int organizationId;
	
	//Notification Entries
	private String notificationId;
	private String notificationName;
	private String assignBy;
	private String createdDate;
	public String getAssignBy() {
		return assignBy;
	}
	public void setAssignBy(String assignBy) {
		this.assignBy = assignBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	private String notificationType;
	private String notificationAction;
	private String notificationFlag;
	private int notificationPersonId;
	private int notificationJobId;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String attribute4;
	private String attribute5;
	private String attribute6;
	private String attribute7;
	private String attribute8;
	private String attribute9;
	private String attribute10;
	private int workflowId;
	private int mappingId;
	private int companyId;
	private int applicationId;
	private int notifyFunctionId;
	
	private String workflowName;
	private String description;
	private int securedFlow; 
	
	private int taskId;
	private String taskSubject;
	private String taskFromDate;
	private String taskToDate;
	private int ownerId;
	private String taskPriority;
	private String taskNotifyFlag;
	private String taskCreatedDate;
	private String notifyAction;
	private String ownerName;
	private int taskDateDiff;
	private String taskDesc;
	private String taskResponse;
	private String taskSubDesc; 
	private int taskViewStatus;
	private String taskNotifyType;
	
	private long uniqueCode;
	
	private String firstName;
	private String lastName; 

	private String sqlProgramName;
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;

	public long getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(long uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getTaskViewStatus() {
		return taskViewStatus;
	}
	public void setTaskViewStatus(int taskViewStatus) {
		this.taskViewStatus = taskViewStatus;
	}
	public String getTaskNotifyType() {
		return taskNotifyType;
	}
	public void setTaskNotifyType(String taskNotifyType) {
		this.taskNotifyType = taskNotifyType;
	}
	public String getTaskSubDesc() {
		return taskSubDesc;
	}
	public void setTaskSubDesc(String taskSubDesc) {
		this.taskSubDesc = taskSubDesc;
	}
	public String getNotifyAction() {
		return notifyAction;
	}
	public void setNotifyAction(String notifyAction) {
		this.notifyAction = notifyAction;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getNotifyName() {
		return notifyName;
	}
	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}
	public int getSecuredFlow() {
		return securedFlow;
	}
	public void setSecuredFlow(int securedFlow) {
		this.securedFlow = securedFlow;
	}
	public String getWorkflowName() {
		return workflowName;
	}
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getNotifyFunctionId() {
		return notifyFunctionId;
	}
	public void setNotifyFunctionId(int notifyFunctionId) {
		this.notifyFunctionId = notifyFunctionId;
	}
	public int getMappingId() {
		return mappingId;
	}
	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public int getWorkflowId() {
		return workflowId;
	}
	public void setWorkflowId(int workflowId) {
		this.workflowId = workflowId;
	}
	public int getNotificationPersonId() {
		return notificationPersonId;
	}
	public void setNotificationPersonId(int notificationPersonId) {
		this.notificationPersonId = notificationPersonId;
	}
	public int getNotificationJobId() {
		return notificationJobId;
	}
	public void setNotificationJobId(int notificationJobId) {
		this.notificationJobId = notificationJobId;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	public String getAttribute2() {
		return attribute2;
	}
	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}
	public String getAttribute3() {
		return attribute3;
	}
	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}
	public String getAttribute4() {
		return attribute4;
	}
	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}
	public String getAttribute5() {
		return attribute5;
	}
	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}
	public String getAttribute6() {
		return attribute6;
	}
	public void setAttribute6(String attribute6) {
		this.attribute6 = attribute6;
	}
	public String getAttribute7() {
		return attribute7;
	}
	public void setAttribute7(String attribute7) {
		this.attribute7 = attribute7;
	}
	public String getAttribute8() {
		return attribute8;
	}
	public void setAttribute8(String attribute8) {
		this.attribute8 = attribute8;
	}
	public String getAttribute9() {
		return attribute9;
	}
	public void setAttribute9(String attribute9) {
		this.attribute9 = attribute9;
	}
	public String getAttribute10() {
		return attribute10;
	}
	public void setAttribute10(String attribute10) {
		this.attribute10 = attribute10;
	}
	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	public String getNotificationName() {
		return notificationName;
	}
	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getNotificationAction() {
		return notificationAction;
	}
	public void setNotificationAction(String notificationAction) {
		this.notificationAction = notificationAction;
	}
	public String getNotificationFlag() {
		return notificationFlag;
	}
	public void setNotificationFlag(String notificationFlag) {
		this.notificationFlag = notificationFlag;
	}
	
	//Notification field ended
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getChildDetails() {
		return childDetails;
	}
	public void setChildDetails(String childDetails) {
		this.childDetails = childDetails;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getSubMenuId() {
		return subMenuId;
	}
	public void setSubMenuId(String subMenuId) {
		this.subMenuId = subMenuId;
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getLookUpId() {
		return lookUpId;
	}
	public void setLookUpId(String lookUpId) {
		this.lookUpId = lookUpId;
	}
	public String getLookUpName() {
		return lookUpName;
	}
	public void setLookUpName(String lookUpName) {
		this.lookUpName = lookUpName;
	}
	public String getLookUpDescription() {
		return lookUpDescription;
	}
	public void setLookUpDescription(String lookUpDescription) {
		this.lookUpDescription = lookUpDescription;
	}
	public String getLookUpTypeId() {
		return lookUpTypeId;
	}
	public void setLookUpTypeId(String lookUpTypeId) {
		this.lookUpTypeId = lookUpTypeId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getApprovalLevel() {
		return approvalLevel;
	}
	public void setApprovalLevel(int approvalLevel) {
		this.approvalLevel = approvalLevel;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(String leaveId) {
		this.leaveId = leaveId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public String getActionId() {
		return actionId;
	}
	public void setActionId(String actionId) {
		this.actionId = actionId;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public List<UserSelectedCompanyTO> getFunctionArray() {
		return functionArray;
	}
	public void setFunctionArray(List<UserSelectedCompanyTO> functionArray) {
		this.functionArray = functionArray;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}
	public int getNotifyId() {
		return notifyId;
	}
	public void setNotifyId(int notifyId) {
		this.notifyId = notifyId;
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getTaskSubject() {
		return taskSubject;
	}
	public void setTaskSubject(String taskSubject) {
		this.taskSubject = taskSubject;
	}
	public String getTaskFromDate() {
		return taskFromDate;
	}
	public void setTaskFromDate(String taskFromDate) { 
		this.taskFromDate = taskFromDate;
	}
	public String getTaskToDate() {
		return taskToDate;
	}
	public void setTaskToDate(String taskToDate) {
		this.taskToDate = taskToDate;
	} 
	public int getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	public String getTaskPriority() {
		return taskPriority;
	}
	public void setTaskPriority(String taskPriority) {
		this.taskPriority = taskPriority;
	}
	public String getTaskNotifyFlag() {
		return taskNotifyFlag;
	}
	public void setTaskNotifyFlag(String taskNotifyFlag) {
		this.taskNotifyFlag = taskNotifyFlag;
	}
	public String getTaskCreatedDate() {
		return taskCreatedDate;
	}
	public void setTaskCreatedDate(String taskCreatedDate) {
		this.taskCreatedDate = taskCreatedDate;
	}
	public int getTaskDateDiff() {
		return taskDateDiff;
	}
	public void setTaskDateDiff(int taskDateDiff) {
		this.taskDateDiff = taskDateDiff;
	}
	public String getTaskDesc() {
		return taskDesc;
	}
	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}
	public String getTaskResponse() {
		return taskResponse;
	}
	public void setTaskResponse(String taskResponse) {
		this.taskResponse = taskResponse;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public int getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	public int getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}  
	
}
