package com.aiotech.aios.common.to;

public class FileVO {
	
	private String hashedName;
	private int compressedSize;

	private String fileName;
	
	public String getHashedName() {
		return hashedName;
	}
	public void setHashedName(String hashedName) {
		this.hashedName = hashedName;
	}
	public int getCompressedSize() {
		return compressedSize;
	}

	public void setCompressedSize(int compressedSize) {
		this.compressedSize = compressedSize;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	 

}
