package com.aiotech.aios.common.util;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

public class JodaDateFormat {
	
	public static List<LocalDate> findWeekEndFromPeriod(String weekend,LocalDate start,
			LocalDate end){
		List<LocalDate> localdates=new ArrayList<LocalDate>();
		String[] weekends=weekend.split(",");
		for (String wend : weekends) {
			DayOfWeekIterator it =null;
			
			switch (Day.valueOf(wend))
			  {
			      case MONDAY:  
						it = new DayOfWeekIterator(start, end, DateTimeConstants.MONDAY);
			       break;
			       
			      case TUESDAY:
						it = new DayOfWeekIterator(start, end, DateTimeConstants.TUESDAY);
			       break;
			       
			      case WEDNESDAY:
						it = new DayOfWeekIterator(start, end, DateTimeConstants.WEDNESDAY);
			       break;
			       
			      case THURSDAY:
						it = new DayOfWeekIterator(start, end, DateTimeConstants.THURSDAY);
			       break;
			       
			      case FRIDAY:
						it = new DayOfWeekIterator(start, end, DateTimeConstants.FRIDAY);
			       break;
			       
			      case SATURDAY:
						it = new DayOfWeekIterator(start, end, DateTimeConstants.SATURDAY);
			       break;
			       
			      case SUNDAY:
						it = new DayOfWeekIterator(start, end, DateTimeConstants.SUNDAY);
			       break;
			  } 
				
			while (it.hasNext()) {
	        	localdates.add(it.next());
	        }
		}
		
		return localdates;
		
	}
	
	 public static enum Day
	 {
	     SUNDAY, MONDAY, TUESDAY, WEDNESDAY, 
	     THURSDAY, FRIDAY, SATURDAY; 
	 }

}
