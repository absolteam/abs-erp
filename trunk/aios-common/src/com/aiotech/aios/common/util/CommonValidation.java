package com.aiotech.aios.common.util;

/*******************************************************************************
*
* Create Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		30 Aug 2010 	Mohamed rafiq.S 		   Initial Version

******************************************************************************/
import java.util.regex.*;
public class CommonValidation {
	boolean returnStatus=false;
  public boolean validateEmpty(String input)
  {
	  if (input == null || "".equals(input.toString().trim())) {
		  returnStatus=false;
	  }
	  else
	  {
		  returnStatus=true;  
	  }
	  return returnStatus;
  }
  
  public boolean validateInt(int input)
  {
	  returnStatus = false; // input empty
	 try
	 {
		if(input == 0){
		returnStatus=false;  
		}
	else{
		returnStatus = true;
		}
	 }
	 catch(Exception e)
	 {
		 returnStatus = false; // input empty
	 }
	 return returnStatus;
  }
  public boolean emailValidation(String input)
  {
	  //Checks for email addresses starting with
      //inappropriate symbols like dots or @ signs.
      Pattern p = Pattern.compile("^\\.|^\\@");
      Matcher m = p.matcher(input);
      if (m.find())
         System.err.println("Email addresses don't start" +
                            " with dots or @ signs.");
      //Checks for email addresses that start with
      //www. and prints a message if it does.
      p = Pattern.compile("^www\\.");
      m = p.matcher(input);
      if (m.find()) {
        System.out.println("Email addresses don't start" +
                " with \"www.\", only web pages do.");
      }
      p = Pattern.compile("[^A-Za-z0-9\\.\\@_\\-~#]+");
      m = p.matcher(input);
      StringBuffer sb = new StringBuffer();
      boolean result = m.find();
      boolean deletedIllegalChars = false;

      while(result) {
         deletedIllegalChars = true;
         m.appendReplacement(sb, "");
         result = m.find();
      }

      // Add the last segment of input to the new String
      m.appendTail(sb);

      input = sb.toString();

      if (deletedIllegalChars) {
         System.out.println("It contained incorrect characters" +
                           " , such as spaces or commas.");
         returnStatus=false;
      }
      else
      {
    	  returnStatus=true;
      }
      return returnStatus;
   }
	  public boolean validateNumeric(String input)  
	  {  
		  if (input.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {  
	           System.out.println("Is a number");  
	           returnStatus=true;
	       } else {  
	          System.out.println("Is not a number");  
	          returnStatus=false;
	       } 
	     return returnStatus;
	  }  
	  
	  //validation for a name field so it dosen't contain any special chars (only alphabets and space) 
	  private boolean validateOnlyAlphabetsAndSpace(String input)
	  {
	  char [] str = new char[input.length()];
	  input.getChars(0, input.length(), str, 0);
		  for(int i = 0; i<str.length; i++)
		  {
			  if((Character.getNumericValue(str[i])<10 || Character.getNumericValue(str[i])>35)&& Character.isSpaceChar(str[i])==false)
			  {
				  i=str.length;
				  returnStatus=false;
				  break;
			  }
			  else
			  {
				  returnStatus=true;
			  }
		  }
	  return returnStatus;
	  }
	  
	  	//Convert double value according to the required precission - by Sudipta
	  	//originalVal -> original double value
	  	//precisionNeeded -> required precission, e.g., 1, 2, 3, ... etc.
	  	public double makeRoundOfDouble(double originalVal, int precisionNeeded) 
		{
			double p = (double)Math.pow(10,precisionNeeded);
			originalVal = originalVal * p;
			double tmp = Math.round(originalVal);
			return (double)tmp/p;
		}
	 	  	
}
