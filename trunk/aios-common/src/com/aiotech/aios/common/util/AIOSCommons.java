package com.aiotech.aios.common.util;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;

import com.aiotech.aios.common.to.Constants.Accounts.CreditTermDueDay;
import com.aiotech.aios.common.to.Constants.Accounts.CustomerType;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountCalcMethod;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountOptions;
import com.aiotech.aios.common.to.Constants.Accounts.MemberCardType;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ReceiptSource;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.google.api.GoogleAPI;
import com.google.api.translate.Language;
import com.google.api.translate.Translate;

public class AIOSCommons {

	private static DesEncrypter desEncrypter = new DesEncrypter();

	public static String returnCompleteName(String first, String middle,
			String last) {

		String name = "";

		if (first != null && !"".equals(first) && !"undefined".equals(first))
			name += first;

		if (middle != null && !"".equals(middle) && !"undefined".equals(middle))
			name += " " + middle;

		if (last != null && !"".equals(last) && !"undefined".equals(last))
			name += " " + last;

		return name;
	}

	public static boolean isNullOrEmptyString(String str) {
		if (str != null && !"".equals(str) && !"undefined".equals(str)) {
			return false;
		} else
			return true;
	}

	public static String fixNullToEmptyString(String str) {
		return str == null ? "" : str;
	}

	public static String fixEmptyStringToNull(String str) {
		return "".equals(str) ? null : str;
	}

	public static int getRandom() {
		Random random = new Random();
		return random.nextInt(1100);
	}

	public static String encrypt(String plaintext) throws Exception {

		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(plaintext.getBytes("UTF-8"));
		byte raw[] = md.digest();
		return (new String(new Base64().encode(raw)));
	}

	public static String formatAmount(Object amount) {

		try {
			NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
			return n.format(amount).substring(1);
		} catch (Exception e) {
			e.printStackTrace();
			return amount.toString();
		}
	}

	public static String formatAmountIncludingNegative(Object amount) {

		try {
			DecimalFormat format = new DecimalFormat("#,##0.00;-#,##0.00");
			String formatted = format.format(amount);
			return formatted;
		} catch (Exception e) {
			e.printStackTrace();
			return amount.toString();
		}
	}

	public static Double formatAmountToDouble(Object amount) {

		try {
			Double value = new Double(amount.toString().replace(",", ""));
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return 0.0;
		}
	}

	public static String translatetoArabic(String text) {

		GoogleAPI.setHttpReferrer("http://erp.aio.ae");
		GoogleAPI.setKey("AIzaSyAGIObxrGpPfNNLc1_ToEHJLjy5-3rlFX8");
		String translatedText = "";
		try {
			translatedText = Translate.DEFAULT.execute(text, Language.ENGLISH,
					Language.ARABIC);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return (translatedText.equals("") || translatedText == null) ? text
				: translatedText;
	}

	public static void removeUploaderSession(HttpSession session,
			String displayPane, Long recordId, String entityName) {
		session.removeAttribute("AIOS-file-structure" + "-" + displayPane + "-"
				+ recordId);
		session.removeAttribute("AIOS-dir-structure" + "-" + displayPane + "-"
				+ recordId);
		session.removeAttribute("AIOS-dirNode-structure" + "-" + displayPane
				+ "-" + recordId);
		session.removeAttribute("AIOS-tree-mod" + "-" + displayPane + "-"
				+ recordId);

		session.removeAttribute("AIOS-img-" + entityName + "-"
				+ session.getId() + "-" + displayPane + "-" + recordId);
		session.removeAttribute("AIOS-doc-" + entityName + "-"
				+ session.getId() + "-" + displayPane + "-" + recordId);

		if (recordId != -1) {
			session.removeAttribute("AIOS-img-" + entityName + "-"
					+ session.getId() + "-" + displayPane + "-" + "-1");
			session.removeAttribute("AIOS-doc-" + entityName + "-"
					+ session.getId() + "-" + displayPane + "-" + "-1");
		}
	}

	public static String convertAmountToWords(Object amount) {

		try {
			return AmountToWordsConverter.convert(
					(int) Double.parseDouble(amount.toString()))
					.concat(" Only");
		} catch (Exception e) {
			e.printStackTrace();
			return amount.toString();
		}
	}

	public static String removeTrailingSymbols(String text) {

		String trimmed = text.trim();
		return (trimmed.length() != 0) ? trimmed.substring(0,
				trimmed.length() - 1) : trimmed;
	}

	public static String intToString(int num, int digits) {
		// create variable length array of zeros
		char[] zeros = new char[digits];
		Arrays.fill(zeros, '0');
		// format number as String
		DecimalFormat df = new DecimalFormat(String.valueOf(zeros));
		return df.format(num);
	}

	public static String longToString(long num, int digits) {
		// create variable length array of zeros
		char[] zeros = new char[digits];
		Arrays.fill(zeros, '0');
		// format number as String
		DecimalFormat df = new DecimalFormat(String.valueOf(zeros));
		return df.format(num);
	}

	public static String getCurrentTimeStamp() {
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat(
				"dd-MMMM-yyyy HH:mm:ss");
		String dateTime = formatter.format(currentDate.getTime());
		return dateTime;
	}

	public static String getCurrentYear() {
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		String year = formatter.format(currentDate.getTime());
		return year;
	}

	public static String generateRandomURN() {
		Random ran = new Random(Calendar.getInstance().getTimeInMillis());
		return (100000 + ran.nextInt(900000)) + "";
	}

	public static String returnAestarics(int size) {
		String result = "";
		for (int i = 0; i < size; i++)
			result += "x";
		return result;
	}

	public static String bytesToUTFString(Byte[] bytes) {

		try {
			if (bytes == null)
				return "";
			byte[] newBytes = new byte[bytes.length];
			for (int i = 0; i < newBytes.length; i++)
				newBytes[i] = bytes[i];
			return new String(newBytes, "utf8").trim();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static Byte[] stringToUTFBytes(String toConvert) {

		try {
			if (toConvert == null)
				return null;
			byte[] bytes = toConvert.trim().getBytes("utf8");
			Byte[] finalBytes = new Byte[bytes.length];
			for (int i = 0; i < finalBytes.length; i++)
				finalBytes[i] = bytes[i];
			return finalBytes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String desEncrypt(String plainText) {
		if (plainText != null)
			return desEncrypter.encrypt(plainText);
		else
			return null;
	}

	public static String desDecrypt(String cipherText) {

		return desEncrypter.decrypt(cipherText);
	}

	public static String roundTwoDecimals(Double d) {
		if (d != null && (d > 0 || d < 0)) {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			return String.valueOf(twoDForm.format(d));
		} else if (d == 0) {
			return "0.0";
		} else {
			return null;
		}
	}

	public static String roundTwoDecimalsWPS(Double d) {
		if (d != null && d > 0) {
			DecimalFormat twoDForm = new DecimalFormat("#");
			return String.valueOf(twoDForm.format(d) + ".00");
		} else if (d == 0) {
			return "0.00";
		} else {
			return null;
		}
	}

	public static String roundLeaveDays(String str) {
		String fromatedStr = "";
		if (str != null) {
			StringTokenizer st2 = new StringTokenizer(str, ".");
			int i = 0;
			while (st2.hasMoreElements()) {
				if (i == 0) {
					fromatedStr += st2.nextElement().toString();
				} else if (i == 2) {
					String temp = st2.nextElement().toString();
					if (!temp.equals("0")) {
						fromatedStr += "." + temp;
					}

				}
				i++;
			}
		}
		return fromatedStr;
	}

	public static Double roundThreeDecimals(Double d) {
		if (d != null && d > 0) {
			DecimalFormat threeDForm = new DecimalFormat("#.###");
			return Double.valueOf(threeDForm.format(d));
		} else if (d == 0) {
			return 0.0;
		} else {
			return null;
		}
	}

	public static String roundThreeDecimalsStr(Double d) {
		if (d != null && d > 0) {
			DecimalFormat threeDForm = new DecimalFormat("#.###");
			return String.valueOf(threeDForm.format(d));
		} else if (d == 0) {
			return "0.0";
		} else {
			return null;
		}
	}
	
	public static String convertExponential(Double d) {
		return BigDecimal.valueOf(d).toPlainString();
	}
	
	public static Double roundDecimals(Double d) {
		if (d != null && d > 0) {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			return Double.valueOf(twoDForm.format(d));
		} else {
			return 0.0;
		}
	}

	public static Map<Byte, CreditTermDueDay> getDueDays() {
		Map<Byte, CreditTermDueDay> dueDays = new HashMap<Byte, CreditTermDueDay>();
		for (CreditTermDueDay dueDay : EnumSet.allOf(CreditTermDueDay.class))
			dueDays.put(dueDay.getCode(), dueDay);
		return dueDays;
	}

	public static Map<Integer, CustomerType> getCustomerTypes() {
		Map<Integer, CustomerType> customerType = new HashMap<Integer, CustomerType>();
		for (CustomerType e : EnumSet.allOf(CustomerType.class)) {
			customerType.put(e.getCode(), e);
		}
		return customerType;
	}

	public static Map<Byte, MemberCardType> getMemberCardTypes() {
		Map<Byte, MemberCardType> memberCardTypes = new HashMap<Byte, MemberCardType>();
		for (MemberCardType e : EnumSet.allOf(MemberCardType.class)) {
			memberCardTypes.put(e.getCode(), e);
		}
		return memberCardTypes;
	}

	public static Map<Byte, String> getO2CProcessStatus() throws Exception {
		Map<Byte, String> o2CProcessStatuses = new HashMap<Byte, String>();
		for (O2CProcessStatus o2CProcessStatus : EnumSet
				.allOf(O2CProcessStatus.class))
			o2CProcessStatuses.put(o2CProcessStatus.getCode(), o2CProcessStatus
					.name().replaceAll("_", " "));
		return o2CProcessStatuses;
	}

	public static Map<String, ReceiptSource> getReceiptSource()
			throws Exception {
		Map<String, ReceiptSource> receiptSources = new HashMap<String, ReceiptSource>();
		String key = "";
		for (ReceiptSource receiptSource : EnumSet.allOf(ReceiptSource.class)) {
			if ((byte) ReceiptSource.Customer.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Customer.getCode().toString().concat("@@")
						.concat("CST");
			else if ((byte) ReceiptSource.Supplier.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Supplier.getCode().toString().concat("@@")
						.concat("SPL");
			else if ((byte) ReceiptSource.Employee.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Employee.getCode().toString().concat("@@")
						.concat("EMP");
			else if ((byte) ReceiptSource.Others.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Others.getCode().toString().concat("@@")
						.concat("OT");
			receiptSources.put(key, receiptSource);
		}
		return receiptSources;
	}

	public static Map<String, String> getModeOfPayment() throws Exception {
		Map<String, String> modeOfPayments = new HashMap<String, String>();
		String key = "";
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class)) {
			if ((byte) ModeOfPayment.Cash.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cash.getCode().toString().concat("@@")
						.concat("CSH");
			else if ((byte) ModeOfPayment.Cheque.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cheque.getCode().toString().concat("@@")
						.concat("CHQ");
			else if ((byte) ModeOfPayment.Credit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Credit_Card.getCode().toString()
						.concat("@@").concat("CCD");
			else if ((byte) ModeOfPayment.Debit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Debit_Card.getCode().toString()
						.concat("@@").concat("DCD");
			else if ((byte) ModeOfPayment.Wire_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Wire_Transfer.getCode().toString()
						.concat("@@").concat("WRT");
			else if ((byte) ModeOfPayment.Inter_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Inter_Transfer.getCode().toString()
						.concat("@@").concat("IAT");
			modeOfPayments.put(key, modeOfPayment.name().replaceAll("_", " "));
		}
		return modeOfPayments;
	}

	public static Map<String, PaymentStatus> getPaymentStatus()
			throws Exception {
		Map<String, PaymentStatus> paymentStatuses = new HashMap<String, PaymentStatus>();
		String key = "";
		for (PaymentStatus paymentStatus : EnumSet.allOf(PaymentStatus.class)) {
			if ((byte) PaymentStatus.Paid.getCode() == (byte) paymentStatus
					.getCode())
				key = PaymentStatus.Paid.getCode().toString().concat("@@")
						.concat("PAID");
			else if ((byte) PaymentStatus.Pending.getCode() == (byte) paymentStatus
					.getCode())
				key = PaymentStatus.Pending.getCode().toString().concat("@@")
						.concat("PND");
			else if ((byte) PaymentStatus.PDC.getCode() == (byte) paymentStatus
					.getCode())
				key = PaymentStatus.PDC.getCode().toString().concat("@@")
						.concat("PDC");
			paymentStatuses.put(key, paymentStatus);
		}
		return paymentStatuses;
	}

	public static Map<Byte, String> getDiscountCalcMethods() throws Exception {
		Map<Byte, String> discountMethods = new HashMap<Byte, String>();
		for (DiscountCalcMethod e : EnumSet.allOf(DiscountCalcMethod.class)) {
			discountMethods.put(e.getCode(), e.name().replaceAll("_", " "));
		}
		return discountMethods;
	}

	public static Map<Byte, String> getDiscountOptions() throws Exception {
		Map<Byte, String> discountOptions = new HashMap<Byte, String>();
		for (DiscountOptions e : EnumSet.allOf(DiscountOptions.class)) {
			discountOptions.put(e.getCode(), e.name().replaceAll("_", " "));
		}
		return discountOptions;
	}

	public static Map<Byte, String> getRequisitionStatus() throws Exception {
		Map<Byte, String> requisitionStatus = new HashMap<Byte, String>();
		for (RequisitionStatus e : EnumSet.allOf(RequisitionStatus.class)) {
			requisitionStatus.put(e.getCode(), e.name().replaceAll("_", " "));
		}
		return requisitionStatus;
	}
}