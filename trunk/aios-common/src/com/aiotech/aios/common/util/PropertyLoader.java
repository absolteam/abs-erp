package com.aiotech.aios.common.util;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
	
	//	 To load SMPP properties file
	private static final Properties SMPP_PROPERTIES =
        new Properties();
    static {
        try {
        	SMPP_PROPERTIES.load(
        			PropertyLoader.class.getClassLoader().getResourceAsStream("alert.application.jmx.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
	} 
    
    public static String getProperty(String propertyName){
    	return SMPP_PROPERTIES.getProperty(propertyName);
    }

}
