package com.aiotech.aios.common.util;
/*

 

public class DUSMSSender {
	
/*	public String messageCodeValue=null;
	public long batchNumber =0;
	protected Logger logger = LogManager.getLogger(this.getClass());
	
	Connection myConnection = null;
	
	public DUSMSSender(String messageCode,long batchNumber){
		
		messageCodeValue = messageCode;
		try {
            myConnection = new Connection(PropertyLoader.getProperty("du.conn.hostName"), 
            		Integer.parseInt(PropertyLoader.getProperty("du.conn.port")));
            myConnection.autoAckLink(true);
            myConnection.autoAckMessages(true);
            
            logger.info("Binding to the SMSC");

            BindResp resp = myConnection.bind(
                    Connection.TRANSMITTER,
                    PropertyLoader.getProperty("du.conn.systemID"),
                    PropertyLoader.getProperty("du.conn.password"),
                    PropertyLoader.getProperty("du.conn.systemType"),
                    Integer.parseInt(PropertyLoader.getProperty("du.conn.source.ton")),
                    Integer.parseInt(PropertyLoader.getProperty("du.conn.source.npi")),
                    messageCode);

            if (resp.getCommandStatus() != 0) {
                logger.info("SMSC bind failed. for"+messageCode);
                //System.exit(1);
            }
		} catch (Exception x) {
            logger.info("An exception occurred. in binding");
            x.printStackTrace(System.err);
        }
	}
	
	public synchronized String sendSMS(SMSTO smsBean){
		
		String returnValue = "";
		try {
						 
            UCS2Encoding ucs2Encoding = new UCS2Encoding();
                       
            String msg = smsBean.getMessageText();
            logger.info("msg ="+msg);
            String[] msgArray = null;
            int numOfSplits = 0;
            int maxChars = 60;
            if(msg!=null&&msg.length()>maxChars){
            	
            	numOfSplits = msg.length()/maxChars;
            	numOfSplits = (msg.length()%maxChars>0)?++numOfSplits:numOfSplits;
            	
            	logger.info("no of splits ="+numOfSplits);
            	msgArray = new String[numOfSplits];
            	for(int i=0;i<numOfSplits;i++){
            		
            		if((i+1)*maxChars>msg.length()){
            			msgArray[i]=msg.substring(i*maxChars,msg.length());
            			break;
            		}
            		msgArray[i] = msg.substring(i*maxChars, (i+1)*maxChars);
            	}
            	
            }else{
            	msgArray = new String[1];
            	msgArray[0]=msg;
            }
                      
            logger.info("msg array length="+msgArray.length);
            if(msgArray.length==1){
            	System.out.println("msg array value="+msgArray[0]);
            	SubmitSM sm = (SubmitSM) myConnection.newInstance(SMPPPacket.SUBMIT_SM);
            	sm.setSource(new Address(0,0,PropertyLoader.getProperty("push.msgcode")));
            	sm.setDestination(new Address(0, 0, smsBean.getMobileNumber()));
            	
            	//sm.setMessageText(smsBean.getMessageText(), ucs2Encoding);
            	
            	sm.setDataCoding(ucs2Encoding.getDataCoding());
            	sm.setOptionalParameter(Tag.MESSAGE_PAYLOAD, ucs2Encoding.encodeString(msgArray[0]));
            	SubmitSMResp smr = (SubmitSMResp) myConnection.sendRequest(sm);
            	logger.info("Message array = "+msgArray[0]);
            	logger.info("command status ="+smr.getCommandStatus());
            	logger.info("error status ="+smr.getErrorCode());
            	
            	if(smr.getCommandStatus()!=0)
            		returnValue=null;
            	logger.info("Submitted message ID: " + smr.getMessageId());

            }
            else
            {
	            for(int i=0;i<msgArray.length;i++){
	            	
	            	System.out.println("msg array value="+msgArray[i]);
	            	SubmitSM sm = (SubmitSM) myConnection.newInstance(SMPPPacket.SUBMIT_SM);
	            	sm.setSource(new Address(0,0,PropertyLoader.getProperty("push.msgcode")));
	            	sm.setDestination(new Address(0, 0, smsBean.getMobileNumber()));
	            	
	            	//sm.setMessageText(smsBean.getMessageText(), ucs2Encoding);
	            	sm.setDataCoding(ucs2Encoding.getDataCoding());
	
	            	sm.setOptionalParameter(Tag.MESSAGE_PAYLOAD, ucs2Encoding.encodeString(msgArray[i]));
	            	sm.setOptionalParameter(Tag.SAR_TOTAL_SEGMENTS, new Integer(msgArray.length));
	            	sm.setOptionalParameter(Tag.SAR_MSG_REF_NUM, 101);
	            	sm.setOptionalParameter(Tag.SAR_SEGMENT_SEQNUM, new Integer(i+1));
	            	
	            	SubmitSMResp smr = (SubmitSMResp) myConnection.sendRequest(sm);
	           	
	            	logger.info("command status ="+smr.getCommandStatus());
	            	logger.info("error status ="+smr.getErrorCode());
	            	
	            	if(smr.getCommandStatus()!=0)
	            		returnValue=null;
	            	logger.info("Submitted message ID: " + smr.getMessageId());
	
	            }	
            }
        } catch (java.net.SocketTimeoutException x) {
            // ah well...
        	returnValue=null;
        }  catch (Exception x) {
            logger.info("An exception occurred. in Read Packet ="+x.getMessage());
            x.printStackTrace(System.err);
            returnValue=null;
        }
        
        return returnValue;
	}
	
	
	public synchronized String sendSMS(SMSBean smsBean){
		
		String returnValue = "";
		try {
			
			// Submit a simple message
            SubmitSM sm = (SubmitSM) myConnection.newInstance(SMPPPacket.SUBMIT_SM);
            sm.setSource(new Address(0,0,PropertyLoader.getProperty("push.msgcode")));
            sm.setDestination(new Address(0, 0, smsBean.getMobileNumber()));
            sm.setMessageText(smsBean.getMessageText());
            SubmitSMResp smr = (SubmitSMResp) myConnection.sendRequest(sm);
            
            System.out.println("command status ="+smr.getCommandStatus());
            System.out.println("error status ="+smr.getErrorCode());
            if(smr.getCommandStatus()!=0)
            	returnValue=null;
            logger.info("Submitted message ID: " + smr.getMessageId());

            // API should be automatically acking deliver_sm and
            // enquire_link packets...
        } catch (java.net.SocketTimeoutException x) {
            // ah well...
        }  catch (Exception x) {
            logger.info("An exception occurred. in Read Packet");
            x.printStackTrace(System.err);
        }
        
        return returnValue;
	}
	
	public void unBind(){
		
		try {
			// Unbind.
	        UnbindResp ubr = myConnection.unbind();
	
	        if (ubr.getCommandStatus() == 0) {
	            logger.info("Successfully unbound from the SMSC");
	        } else {
	        	 logger.info("There was an error unbinding.");
	        }
		}
        catch (Exception x) {
            logger.info("An exception occurred.");
            x.printStackTrace(System.err);
        }
	}
}*/
