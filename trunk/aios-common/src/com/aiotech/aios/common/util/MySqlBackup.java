package com.aiotech.aios.common.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class MySqlBackup {

	private static final Logger log = Logger.getLogger(MySqlBackup.class
			.getName());

	/**
	 * Backup live database on local machine
	 * 
	 * @param dumpExePath
	 * @param host
	 * @param port
	 * @param user
	 * @param password
	 * @param database
	 * @param backupPath
	 * @param localDBName
	 * @param localDBPassword
	 * @param localDBUserName
	 * @return
	 */
	public static boolean dataBaseBackupAndRestore(String dumpExePath, String host,
			String port, String user, String password, String database,
			String backupPath, String localDBUserName, String localDBPassword,
			String sqlExepath) {
		boolean status = false;
		try {

			File directory = new File(backupPath);

			// deleteOldBackupFiles(directory);

			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

			String filepath = "Backup Database " + database + "("
					+ directory.listFiles().length + ")" + " -" + host + "-("
					+ dateFormat.format(Calendar.getInstance().getTime())
					+ ").sql";

			String batchCommand = "\"" + dumpExePath + "\"" + " -h " + host
					+ " --port " + port + " -u " + user + " --password="
					+ password + " --add-drop-database -B " + database
					+ " -r \"" + backupPath + "" + filepath + "\"";

			ProcessBuilder pb = new ProcessBuilder(batchCommand);
			Process process = pb.start();
			int processComplete = process.waitFor();

			if (processComplete == 0) {
				status = true;
				log.info("Backup created successfully for DB " + database
						+ "  with" + host + ":" + port);
				status = restoreDatabase(localDBUserName, localDBPassword,
						backupPath, sqlExepath);
				if (status)
					log.info("Backup Restored successfully for  DB " + database
							+ " with " + host + ":" + port);
				else
					log.info("Backup failed to Restore for  DB " + database
							+ " with " + host + ":" + port);
			} else {
				status = false;
				log.error("Could not create the backup for with DB " + database
						+ " in " + host + ":" + port);
			}
			if (directory.exists())
				FileUtils.cleanDirectory(directory);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			log.error( "failed!", ioe );
 		} catch (Exception e) {
			e.printStackTrace();
			log.error( "failed!", e );
 		}

		return status;
	}

	/**
	 * Delete Old Backup files when there is file size > 3
	 * 
	 * @param directory
	 */
	@SuppressWarnings("unused")
	private static void deleteOldBackupFiles(File directory) {
		if (directory.exists()) {
			File[] listFiles = directory.listFiles();
			if (null != listFiles && listFiles.length > 3) {
				Arrays.sort(listFiles, new Comparator<File>() {
					public int compare(File f1, File f2) {
						if (((File) f1).lastModified() > ((File) f2)
								.lastModified())
							return -1;
						else if (((File) f1).lastModified() < ((File) f2)
								.lastModified())
							return +1;
						else
							return 0;
					}
				});
			} else
				return;
			for (int i = 0; i < listFiles.length; i++) {
				if (i > 2)
					listFiles[i].delete();
			}
		} else
			return;

	}

	/**
	 * Get Last Modified file
	 * 
	 * @param directory
	 * @return
	 */
	private static File getLastModifiedFile(File directory) {
		File[] listFiles = directory.listFiles();
		if (listFiles.length == 1)
			return listFiles[0];
		else {
			Arrays.sort(listFiles, new Comparator<File>() {
				public int compare(File f1, File f2) {
					if (((File) f1).lastModified() > ((File) f2).lastModified())
						return -1;
					else if (((File) f1).lastModified() < ((File) f2)
							.lastModified())
						return +1;
					else
						return 0;
				}
			});
			return listFiles[0];
		}
	}

	/**
	 * Restore local database with live database
	 * 
	 * @param dbUserName
	 * @param dbPassword
	 * @param sqlExepath
	 * @param source
	 * @return
	 */
	private static boolean restoreDatabase(String dbUserName, String dbPassword,
			String backUpSource, String sqlExepath) {

		String rootDir = backUpSource;
		File file = getLastModifiedFile(new File(rootDir));
		String[] executeCmd = new String[] { sqlExepath,
				"--user=" + dbUserName, "--password=" + dbPassword, "-e",
				"source " + file.getAbsolutePath() };

		Process runtimeProcess;
		try {
			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();

			if (processComplete == 0) {
				log.info("Backup restored successfully with " + backUpSource);
				return true;
			} else {
				log.error("Could not restore the backup " + backUpSource);
			}
		} catch (Exception ex) {
			log.error(ex.getCause());
		}
		return false;
	}
}