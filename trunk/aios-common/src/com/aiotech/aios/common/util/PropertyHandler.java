package com.aiotech.aios.common.util;


import java.util.PropertyResourceBundle;

public class PropertyHandler {
	
	
public static PropertyResourceBundle getResourceBundle(String fileName){
		
		PropertyResourceBundle bundle = (PropertyResourceBundle)
				PropertyResourceBundle.getBundle(fileName);
		
		return bundle;
	}
	
	public static void main(String ar[]){
		PropertyResourceBundle bundle = getResourceBundle("videodestinationinfo");
		System.out.println("bundle = "+bundle.getString("distinationvideopath"));
	}

}
