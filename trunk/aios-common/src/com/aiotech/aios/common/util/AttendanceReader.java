package com.aiotech.aios.common.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.log4j.Logger;

public class AttendanceReader {
	private static final Logger log = Logger.getLogger(AttendanceReader.class
			.getName());
	private static Properties prop;

	static {
		try {
			prop = new Properties();
			InputStream input = null;
			input = AttendanceReader.class
					.getResourceAsStream("/resources/config.properties");
			prop.load(input);
			//Reload The config file from local drive
			prop.load(new FileInputStream(prop.getProperty(
			"SYNCFILE-LOCATION").trim()));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Properties load execetion");
		}
	}

	private static final String SQL_SERVER_URL = prop.getProperty(
			"SQL-SERVER-URL").trim();
	private static final String MYSQL_URL = prop.getProperty("MYSQL-URL")
			.trim();
	private static final String SQL_SERVER_USERNAME = prop.getProperty(
			"SQL-SERVER-USERNAME").trim();
	private static final String SQL_SERVER_PASSWORD = prop.getProperty(
			"SQL-SERVER-PASSWORD").trim();
	private static final String MYSQL_USERNAME = prop.getProperty(
			"MYSQL-USERNAME").trim();
	private static final String MYSQL_PASSWORD = prop.getProperty(
			"MYSQL-PASSWORD").trim();
	private static final Long implementationId = Long.parseLong(prop.getProperty("APP-KEY"));

	public static boolean startReadData() {

		Connection con = null;
		Connection con2 = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {

			Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
			con = DriverManager.getConnection(SQL_SERVER_URL,
					SQL_SERVER_USERNAME, SQL_SERVER_PASSWORD);
			log.info("CONNECTED sql server");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con2 = DriverManager.getConnection(MYSQL_URL, MYSQL_USERNAME,
					MYSQL_PASSWORD);
			con2.setAutoCommit(false);
			log.info("CONNECTED2 mysql");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info("connection error");
			return false;
		}

		try {
			String file = new SimpleDateFormat("yyyyMMddHHmmss")
					.format(new java.util.Date()) + ".Bak";
			String sql = "backup database unis to disk = '"
					+ prop.getProperty("BACKUP-LOCATION").trim() + file
					+ "' with format, name = 'full backup of unis'";
			con.prepareStatement(sql).executeUpdate();
			log.info("Backup Done in the name of " + file);

			stmt = con.createStatement();
			rs = stmt
					.executeQuery("SELECT [C_Date],[C_Time],[L_TID],[L_Mode],[tUser].[C_Unique] FROM [unis].[dbo].[tEnter] left JOIN [unis].[dbo].[tUser]  "
							+ "ON  [L_TID]=[L_ID] WHERE [L_Result] =0");
			
			//L_Result =0 is Success
			
			log.info("Select executed");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			while (rs.next()) {
				String dateT = rs.getString("C_Date");
				String timeT = rs.getString("C_Time");
				String uid = rs.getString("C_Unique");
				if (uid == null || uid.trim().equals(""))
					continue;

				String mode = rs.getString("L_MODE");
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				java.util.Date parsed = format.parse(dateT);
				java.sql.Date date = new java.sql.Date(parsed.getTime());
				format = new SimpleDateFormat("HHmmss");
				java.util.Date parsedT = format.parse(timeT);
				java.sql.Time time = new java.sql.Time(parsedT.getTime());
				String modeString = "";
				switch (Integer.valueOf(mode)) {
				case 1:
					modeString = "IN";
					break;
				case 2:
					modeString = "OUT";
					break;
				case 3:
					modeString = "VISITOR";
					break;
				case 4:
					modeString = "BREAK-IN";
					break;
				case 5:
					modeString = "BREAK-OUT";
					break;
				}

				String insertSql = "insert into hr_swipe_in_out"
						+ "(swipe_id,attendance_date,time_in_out,time_in_out_type,door_number,implementation_id,status) "
						+ "values(?,?,?,?,?,?,?)";
				PreparedStatement preparedStmt = con2
						.prepareStatement(insertSql);
				preparedStmt.setLong(1, Long.valueOf(uid));
				preparedStmt.setDate(2, date);
				preparedStmt.setTime(3, time);
				preparedStmt.setString(4, modeString);
				preparedStmt.setString(5, "0");
				preparedStmt.setLong(6, implementationId);
				preparedStmt.setByte(7, (byte) 4);//4 = not yet synchronised 1=synchronised,
				preparedStmt.execute();
			}
			log.info("Data inserted into live DB");
			// Truncate Table
			 stmt = con.createStatement();
			 stmt.executeUpdate("TRUNCATE TABLE [unis].[dbo].[tEnter]");

			con2.commit();
			con2.close();
			con.close();
			return true;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

	}

	

}
