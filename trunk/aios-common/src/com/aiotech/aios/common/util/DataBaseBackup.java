package com.aiotech.aios.common.util;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DataBaseBackup {

	private static final long serialVersionUID = 9197348289286040213L;

	private static final Logger log = Logger.getLogger(DataBaseBackup.class
			.getName());
	private static Properties prop;
	private static final String PROGRAM_FILES = System.getenv("programfiles");
 
	static {
		try {
			prop = new Properties();
			InputStream input = null;
			input = DataBaseBackup.class
					.getResourceAsStream("/dbconfig.properties");
			prop.load(input);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Properties load execetion");
		}
	}

	public static boolean synchronizeDatabase() {
		try {
			boolean backupStatus = MySqlBackup.dataBaseBackupAndRestore(PROGRAM_FILES
					.concat(prop.getProperty("MYSQL-DUMPEXE").trim()), prop
					.getProperty("HOST-ADDRESS").trim(),
					prop.getProperty("LIVEDB-PORT").trim(),
					prop.getProperty("LIVEDB-USERNAME").trim(), prop
							.getProperty("LIVEDB-PASSWORD").trim(), prop
							.getProperty("LIVEDB-NAME").trim(), prop
							.getProperty("BACKUP-LOCATION").trim(), prop
							.getProperty("LOCALDB-USERNAME").trim(), prop
							.getProperty("LOCALDB-PASSWORD").trim(),
					PROGRAM_FILES.concat(prop.getProperty("MYSQL-EXE").trim()));
			return backupStatus;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	} 
}
