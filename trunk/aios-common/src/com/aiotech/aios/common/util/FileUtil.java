package com.aiotech.aios.common.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.DataFormatException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aiotech.aios.common.to.FileVO;

public class FileUtil {

	private static final Log log = LogFactory.getLog(FileUtil.class);

	public static byte[] getBytes(File uploadedFile) throws IOException {
		byte[] bytes = null;
		FileInputStream fis = null;
		fis = new FileInputStream(uploadedFile);
		bytes = new byte[fis.available()];
		int read = 0;
		int numRead = 0;

		while (read < bytes.length
				&& (numRead = fis.read(bytes, read, bytes.length - read)) >= 0)
			read = read + numRead;

		if (fis != null)
			fis.close();

		return bytes;
	}

	public static Map<String, FileVO> copyCachedFiles(
			Map<String, byte[]> fileBytesMap, Properties confProps,
			String templateUploadPath) throws UnsupportedEncodingException,
			DataFormatException {

		Map<String, FileVO> filesMap = new HashMap<String, FileVO>();
		if (fileBytesMap != null) {
			String mapKey = null;
			byte[] fileBytes = null;
			FileOutputStream fos = null;
			ByteArrayInputStream bais = null;
			BufferedInputStream bis = null;

			try {
				for (Map.Entry<String, byte[]> entry : fileBytesMap.entrySet()) {
					mapKey = entry.getKey();
					fileBytes = fileBytesMap.get(mapKey);
					if (fileBytes != null) {
						String hashName = String.valueOf(Base64
								.encodeBase64(mapKey.getBytes()));
						FileVO file = new FileVO();
						file.setFileName(mapKey);
						file.setHashedName(hashName);
						file.setCompressedSize(fileBytes.length);
						filesMap.put(mapKey, file);
						if (templateUploadPath == null)
							fos = new FileOutputStream(new File(confProps
									.getProperty("file.upload.path").concat(
											hashName)));
						else
							fos = new FileOutputStream(new File(
									templateUploadPath.concat(hashName)));
						bais = new ByteArrayInputStream(fileBytes);
						bis = new BufferedInputStream(bais);
						int read = 0;
						byte[] bytes = new byte[bais.available()];
						while ((read = bis.read(bytes)) != -1) {
							fos.write(bytes, 0, read);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fos != null) {
						fos.flush();
						fos.close();
					}
					if (bis != null) {
						bis.close();
					}
					if (bais != null) {
						bais.close();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return filesMap;
		} else {
			log.error("Files Are Not Uploaded");
			return filesMap;
		}
	}
	
	public static Map<String, FileVO> copyCachedFiles(
			Map<String, byte[]> fileBytesMap, Properties confProps,
			String templateUploadPath,String extraFolder) throws UnsupportedEncodingException,
			DataFormatException {

		Map<String, FileVO> filesMap = new HashMap<String, FileVO>();
		if (fileBytesMap != null) {
			String mapKey = null;
			byte[] fileBytes = null;
			FileOutputStream fos = null;
			ByteArrayInputStream bais = null;
			BufferedInputStream bis = null;

			try {
				for (Map.Entry<String, byte[]> entry : fileBytesMap.entrySet()) {
					mapKey = entry.getKey();
					fileBytes = fileBytesMap.get(mapKey);
					if (fileBytes != null) {
						String hashName = String.valueOf(Base64
								.encodeBase64(mapKey.getBytes()));
						FileVO file = new FileVO();
						file.setFileName(mapKey);
						file.setHashedName(hashName);
						file.setCompressedSize(fileBytes.length);
						filesMap.put(mapKey, file);
						if (templateUploadPath == null)
							fos = new FileOutputStream(new File(confProps
									.getProperty("file.upload.path").concat(extraFolder+"/").concat(
											hashName)));
						else
							fos = new FileOutputStream(new File(
									templateUploadPath.concat(hashName)));
						bais = new ByteArrayInputStream(fileBytes);
						bis = new BufferedInputStream(bais);
						int read = 0;
						byte[] bytes = new byte[bais.available()];
						while ((read = bis.read(bytes)) != -1) {
							fos.write(bytes, 0, read);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fos != null) {
						fos.flush();
						fos.close();
					}
					if (bis != null) {
						bis.close();
					}
					if (bais != null) {
						bais.close();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return filesMap;
		} else {
			log.error("Files Are Not Uploaded");
			return filesMap;
		}
	}

	public static List<Map<String, Object>> getUploadedTemplates(
			String templateUploadPath) throws IOException {

		List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
		File templatesFolder = new File(templateUploadPath);
		File[] listOfTemplates = templatesFolder.listFiles();

		for (File templatefile : listOfTemplates) {

			if (templatefile.isFile()) {

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("fileName", templatefile.getName());
				map.put("fileData", FileUtil.getBytes(templatefile));
				map.put("fileSize", templatefile.length());
				fileList.add(map);
			}
		}

		return fileList;
	}

	public static void writeExtracedBytesToDisk(byte[] extractedBytes,
			String filePath) throws IOException {
		FileOutputStream fos = null;
		ByteArrayInputStream bais = null;
		BufferedInputStream bis = null;
		if (extractedBytes != null) {
			try {
				bais = new ByteArrayInputStream(extractedBytes);
				File file = new File(filePath);
				fos = new FileOutputStream(file);
				int read = 0;
				bis = new BufferedInputStream(bais);
				byte[] bytes = new byte[bais.available()];
				while ((read = bis.read(bytes)) != -1) {
					fos.write(bytes, 0, read);
				}
			} finally {
				try {
					if (fos != null) {
						fos.flush();
						fos.close();
					}
					if (bis != null) {
						bis.close();
					}
					if (bais != null) {
						bais.close();
					}

				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void createDir(String fullDirPath) {
		File file = new File(fullDirPath);
		file.mkdir();

	}

	public static FileVO copyDocFile(Map<String, byte[]> fileBytesMap,
			String filePath, String fileName)
			throws UnsupportedEncodingException, DataFormatException {

		FileVO file = new FileVO();
		if (fileBytesMap != null) {
			byte[] fileBytes = null;
			FileOutputStream fos = null;
			ByteArrayInputStream bais = null;
			BufferedInputStream bis = null;
			try {
				fileBytes = fileBytesMap.get(fileName);
				if (fileBytes != null) {
					String hashName = String.valueOf(Base64
							.encodeBase64(fileName.getBytes()));
					file.setFileName(fileName);
					file.setHashedName(hashName);
					file.setCompressedSize(fileBytes.length);

					/*
					 * String tmp = getDistinctName(hashName, filePath); String
					 * newHashName = hashName; if (tmp.contains("/"))
					 * newHashName = tmp.substring((tmp.lastIndexOf("/") + 1),
					 * tmp.length()); if (!hashName.equals(newHashName))
					 * file.setHashedName(newHashName);
					 */

					fos = new FileOutputStream(new File(
							filePath.concat(hashName)));
					bais = new ByteArrayInputStream(fileBytes);
					bis = new BufferedInputStream(bais);
					int read = 0;
					byte[] bytes = new byte[bais.available()];
					while ((read = bis.read(bytes)) != -1) {
						fos.write(bytes, 0, read);
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fos != null) {
						fos.flush();
						fos.close();
					}
					if (bis != null) {
						bis.close();
					}
					if (bais != null) {
						bais.close();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return file;
		} else {
			log.error("Files Are Not Uploaded");
			return file;
		}
	}

	private static String getDistinctName(String hashName, String filePath) {

		try {
			File file = new File(filePath.concat(hashName));
			if (file.exists()) {
				while (file.exists()) {
					hashName = hashName + AIOSCommons.getRandom();
					file = new File(filePath.concat(hashName));
				}
			} else
				return hashName;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(hashName + " :: File exist check error.");
		}
		return hashName;
	}

	public boolean deleteFile(String filePath) {
		File file = new File(filePath);
		return file.delete();
	}

	public static void delete(File file) throws IOException {
		if (file.isDirectory()) {
			// directory is empty, then delete it
			if (file.list().length == 0) {
				file.delete();
			} else {
				// list all the directory contents
				String files[] = file.list();
				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);
					// recursive delete
					delete(fileDelete);
				}
				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
				}
			}

		} else {
			// if file, then delete it
			file.delete();
		}
	}

}
