package com.aiotech.aios.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressionUtil {
	public static byte[] compressBytes(byte[] input) throws UnsupportedEncodingException, IOException {
		Deflater df = new Deflater();
		df.setInput(input);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(input.length);
		df.finish();
		byte[] buff = new byte[1024];
		while (!df.finished()) {
			int count = df.deflate(buff);
			baos.write(buff, 0, count);
		}
		baos.close();
		byte[] output = baos.toByteArray();		
		return output;
	}

	public static byte[] extractBytes(byte[] input) throws UnsupportedEncodingException, IOException,DataFormatException {
		Inflater ifl = new Inflater();
		ifl.setInput(input);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(input.length);
		byte[] buff = new byte[1024];
		while (!ifl.finished()) {int count = ifl.inflate(buff);
			baos.write(buff, 0, count);
		}
		baos.close();
		byte[] output = baos.toByteArray();
		return output;
	}
}
