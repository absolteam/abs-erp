package com.aiotech.aios.common.util;

import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GetFormatedDate {
	public static String getFormatedDate(String inputDate, String inputFormat, String format) {  //format is the required long or short format
		String outString = ""; 
		try{
			if(inputDate != null && inputDate.trim().length() > 0){
				//String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"
							
				SimpleDateFormat inFormat = new SimpleDateFormat(inputFormat); // input format "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if(format != null && format.trim().equalsIgnoreCase("long"))
					outFormat= new SimpleDateFormat("dd-MMMM-yyyy");	// output fromat	"dd-MMMM-yyyy" -> long : "dd-MMM-yyyy" -> Short			
				else if(format != null && format.trim().equalsIgnoreCase("sort"))
					outFormat= new SimpleDateFormat("dd-MMM-yyyy");	
				else if(format != null && format.trim().equalsIgnoreCase("number"))
					outFormat= new SimpleDateFormat("yyyy-MM-dd");	
				else if(format != null && format.trim().equalsIgnoreCase("noLanguage"))
                    outFormat= new SimpleDateFormat("dd-MM-yyyy");
				else
					outFormat= new SimpleDateFormat("dd-MMMM-yyyy");	
				
				Date date = inFormat.parse(inputDate);			 
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);
	
				//System.out.println("----------------------------------------------------------------" +  outString);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			return outString;
		}
	}
}

