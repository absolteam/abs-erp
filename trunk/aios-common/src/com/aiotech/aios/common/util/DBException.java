/**
 * DBException.java
 *
 * @Version         :   0.01
 * @Author 			:   
 * @Date created	: 	16/06/2010
 * @Last modified   :   
 * @Updated By 		:   
 *                                                                                                                             
 * Copyright (c) 2002 HBS Services.,
 * Bangalore, India. 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of HBS Services 
 * ("Confidential Information").You shall not disclose such Confidential Information and shall
 * only use it only in accordance with the terms of the license agreement you entered into with
 * HBS Services
*/

package com.aiotech.aios.common.util;

/**
 * This class is used as a Parent Class to the following class which as follows:
 * <pre>
 *	1. UnKnownPROException
 * </pre>
 *
 *	This class does not have methods. It only has constructors.
 */

public class DBException extends Exception {
  
  public DBException() {
    super();
  }

  public DBException(String msg) {
  	super(msg);
  	
  }
}
