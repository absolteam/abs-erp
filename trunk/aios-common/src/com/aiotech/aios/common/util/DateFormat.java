package com.aiotech.aios.common.util;

/*******************************************************************************
 *
 * Create Log
 * -----------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 Description
 * -----------------------------------------------------------------------------
 * 1.0		30 Aug 2010 	Mohamed rafiq.S 		   Initial Version

 ******************************************************************************/

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateFormat {
	public static final String DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss";
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
	public static final String TO_DATE_FORMAT = "yyyy-mm-dd HH:mm:sss";
	public static final String DATE_PICKER_FORMAT = "dd-MMM-yyyy";
	public static final String LONG_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";

	public static String getToday() {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DATE);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		return day + "/" + month + "/" + year;
	}

	/*
	 * Add Day/Month/Year to a Date add() is used to add values to a Calendar
	 * object. You specify which Calendar field is to be affected by the
	 * operation (Calendar.YEAR, Calendar.MONTH, Calendar.DATE).
	 */
	public static String addToDate(String DATE_FORMAT, int addMonth) {
		System.out.println("In the ADD Operation");
		// String DATE_FORMAT = "yyyy-MM-dd";
		// String DATE_FORMAT = "dd-MM-yyyy"; //Refer Java DOCS for formats
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		Date d1 = new Date();
		System.out.println("Todays date in Calendar Format : " + c1);
		System.out.println("c1.getTime() : " + c1.getTime());
		System.out.println("c1.get(Calendar.YEAR): " + c1.get(Calendar.YEAR));
		System.out.println("Todays date in Date Format : " + d1);
		c1.set(1999, 0, 20); // (year,month,date)
		System.out.println("c1.set(1999,0 ,20) : " + c1.getTime());
		c1.add(Calendar.DATE, 40);
		System.out.println("Date + 20 days is : " + sdf.format(c1.getTime()));
		return sdf.format(c1.getTime());
	}

	/*
	 * Substract Day/Month/Year to a Date roll() is used to substract values to
	 * a Calendar object. You specify which Calendar field is to be affected by
	 * the operation (Calendar.YEAR, Calendar.MONTH, Calendar.DATE).
	 * 
	 * Note: To substract, simply use a negative argument. roll() does the same
	 * thing except you specify if you want to roll up (add 1) or roll down
	 * (substract 1) to the specified Calendar field. The operation only affects
	 * the specified field while add() adjusts other Calendar fields. See the
	 * following example, roll() makes january rolls to december in the same
	 * year while add() substract the YEAR field for the correct result
	 */

	public static String subToDate(String DATE_FORMAT, int subtractionMonth) {
		System.out.println("In the SUB Operation");
		// String DATE_FORMAT = "dd-MM-yyyy";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		c1.set(1999, 0, 20);
		System.out.println("Date is : " + sdf.format(c1.getTime()));

		// roll down, substract 1 month
		c1.roll(Calendar.MONTH, false);
		System.out.println("Date roll down 1 month : "
				+ sdf.format(c1.getTime()));

		c1.set(1999, 0, 20);
		System.out.println("Date is : " + sdf.format(c1.getTime()));
		c1.add(Calendar.MONTH, subtractionMonth);
		// substract 1 month
		System.out.println("Date minus 1 month : " + sdf.format(c1.getTime()));
		return sdf.format(c1.getTime());
	}

	public static long daysBetween2Dates() {
		Calendar c1 = Calendar.getInstance(); // new GregorianCalendar();
		Calendar c2 = Calendar.getInstance(); // new GregorianCalendar();
		c1.set(1999, 0, 20);
		c2.set(1999, 0, 22);
		System.out.println("Days Between " + c1.getTime() + "\t" + c2.getTime()
				+ " is");
		System.out.println((c2.getTime().getTime() - c1.getTime().getTime())
				/ (24 * 3600 * 1000));
		return (c2.getTime().getTime() - c1.getTime().getTime())
				/ (24 * 3600 * 1000);
	}

	public static int daysInMonth() {
		Calendar c1 = Calendar.getInstance(); // new GregorianCalendar();
		c1.set(1999, 6, 20);
		int year = c1.get(Calendar.YEAR);
		int month = c1.get(Calendar.MONTH);
		// int days = c1.get(Calendar.DATE);
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		daysInMonths[1] += DateFormat.isLeapYear(year) ? 1 : 0;
		System.out.println("Days in " + month + "th month for year " + year
				+ " is " + daysInMonths[c1.get(Calendar.MONTH)]);
		return daysInMonths[c1.get(Calendar.MONTH)];
	}

	public static String getDayofTheDate() {
		Date d1 = new Date();
		String day = null;
		SimpleDateFormat f = new SimpleDateFormat("EEEE");
		try {
			day = f.format(d1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("The dat for " + d1 + " is " + day);
		return day;
	}

	public static boolean validateAGivenDate(String dt) {
		// String dt = "20011223";
		// String invalidDt = "20031315";
		// String dateformat = "yyyyMMdd";
		Date dt1 = null, dt2 = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			sdf.setLenient(false);
			dt1 = sdf.parse(dt);
			// dt2 = sdf.parse(invalidDt);
			System.out.println("Date is ok = " + dt1 + "(" + dt + ")");
			return true;
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			return false;
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid date");
			return false;
		}
	}

	public static void compare2Dates() {
		SimpleDateFormat fm = new SimpleDateFormat(DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();

		c1.set(2000, 02, 15);
		c2.set(2001, 02, 15);

		System.out.print(fm.format(c1.getTime()) + " is ");
		if (c1.before(c2)) {
			System.out.println("less than " + c2.getTime());
		} else if (c1.after(c2)) {
			System.out.println("greater than " + c2.getTime());
		} else if (c1.equals(c2)) {
			System.out.println("is equal to " + fm.format(c2.getTime()));
		}

	}

	public static boolean isLeapYear(int year) {
		if ((year % 100 != 0) || (year % 400 == 0)) {
			return true;
		}
		return false;
	}

	public static String dateFormater() {
		Date todaysDate = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String formattedDate = formatter.format(todaysDate);
		System.out.println("Today�s date and Time is:" + formattedDate);
		return formattedDate;
	}

	/*
	 * //Added by rafiq public Date convertStringToDate(String str_date) throws
	 * ParseException{
	 * 
	 * Date date = new Date(); SimpleDateFormat formatter ; formatter = new
	 * SimpleDateFormat(DB_DATE_FORMAT); date = (Date)formatter.parse(str_date);
	 * return date; }
	 */
	public static Date convertStringToDate(String inputDate) { // format is the
																// required long
																// or short
																// format
		String format = DB_DATE_FORMAT;
		Date date = null;
		try {
			if (inputDate != null && inputDate.trim().length() > 0) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"
				date = new Date();
				SimpleDateFormat inFormat = new SimpleDateFormat(
						DATE_PICKER_FORMAT); // input format "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if (format != null && format.trim().equalsIgnoreCase("long"))
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				else if (format != null
						&& format.trim().equalsIgnoreCase("sort"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				else if (format != null
						&& format.trim().equalsIgnoreCase("number"))
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				else if (format != null
						&& format.trim().equalsIgnoreCase("noLanguage"))
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				else
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy");

				date = inFormat.parse(inputDate);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return date;
		}
	}

	public static String convertStringToSearchDate(String inputDate) { // format
																		// is
																		// the
																		// required
																		// long
																		// or
																		// short
																		// format
		String format = DB_DATE_FORMAT;
		Date date = null;
		String outString = "";
		try {
			if (inputDate != null && inputDate.trim().length() > 0) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"
				date = new Date();
				SimpleDateFormat inFormat = new SimpleDateFormat(
						DATE_PICKER_FORMAT); // input format "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				outFormat = new SimpleDateFormat("yyyy-MM-dd");

				date = inFormat.parse(inputDate.toString());
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	public static String convertDateToString(String inputDate) { // format is
																	// the
																	// required
																	// long or
																	// short
																	// format
		String outString = "";
		String format = DATE_PICKER_FORMAT;
		try {
			if (inputDate != null && inputDate.trim().length() > 0) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(DB_DATE_FORMAT); // input
																					// format
																					// "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if (format != null && format.trim().equalsIgnoreCase("long"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				else if (format != null
						&& format.trim().equalsIgnoreCase("sort"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				else if (format != null
						&& format.trim().equalsIgnoreCase("number"))
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				else if (format != null
						&& format.trim().equalsIgnoreCase("noLanguage"))
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				else
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");

				Date date = inFormat.parse(inputDate.toString());
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	@SuppressWarnings("finally")
	public static String convertDateToTimeOnly(Date inputDate) {
		String outString = "";
		try {
			if (inputDate != null) {
				LocalTime inDate = new LocalTime(inputDate);
				outString = inDate.toString("HH:mm");

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	@SuppressWarnings("finally")
	public static Date convertStringToDateTimeFormat(String inputDate) {
		Date jdkDate = null;
		try {
			if (inputDate != null) {
				DateTimeFormatter formatter = DateTimeFormat
						.forPattern("dd-MMM-yyyy HH:mm");
				DateTime dt = formatter.parseDateTime(inputDate);
				jdkDate = dt.toDate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return jdkDate;
		}
	}

	@SuppressWarnings("finally")
	public static String convertDateToDate(String inputDate) {
		String outString = "";
		String format = DATE_PICKER_FORMAT;
		String time[];
		time = inputDate.split(" ");
		try {
			if (inputDate != null && inputDate.trim().length() > 0) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(DB_DATE_FORMAT); // input
																					// format
																					// "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if (format != null && format.trim().equalsIgnoreCase("long"))
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				else if (format != null
						&& format.trim().equalsIgnoreCase("sort"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				else if (format != null
						&& format.trim().equalsIgnoreCase("number"))
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				else if (format != null
						&& format.trim().equalsIgnoreCase("noLanguage"))
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				else
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy");

				Date date = inFormat.parse(inputDate);
				outString = outFormat.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString + " " + time[1];
		}
	}

	public static String convertSystemDateToString(Date inputDate) { // format
																		// is
																		// the
																		// required
																		// long
																		// or
																		// short
																		// format
		String outString = "";
		String format = "yyyy/MM/dd HH:mm:ss";
		try {
			if (inputDate != null) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(DB_DATE_FORMAT); // input
																					// format
																					// "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if (format != null && format.trim().equalsIgnoreCase("long"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				else if (format != null
						&& format.trim().equalsIgnoreCase("sort"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				else if (format != null
						&& format.trim().equalsIgnoreCase("number"))
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				else if (format != null
						&& format.trim().equalsIgnoreCase("noLanguage"))
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				else
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");

				Date date = inputDate;
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	public static String convertSystemDateToStringTimeFormat(Date inputDate) { // format
																				// is
																				// the
																				// required
																				// long
																				// or
																				// short
																				// format
		String outString = "";
		String format = "yyyy/MM/dd HH:mm:ss";
		try {
			if (inputDate != null) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss"); // input format "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if (format != null && format.trim().equalsIgnoreCase("long"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				else if (format != null
						&& format.trim().equalsIgnoreCase("sort"))
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				else if (format != null
						&& format.trim().equalsIgnoreCase("number"))
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				else if (format != null
						&& format.trim().equalsIgnoreCase("noLanguage"))
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				else
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");

				Date date = inputDate;
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	public static boolean compareDates(Date fromDate, Date toDate) {
		Calendar c1 = Calendar.getInstance();

		Calendar c2 = Calendar.getInstance();
		c1.setTime(fromDate);
		c1.set(Calendar.HOUR_OF_DAY, 0);
		c1.set(Calendar.MINUTE, 0);
		c1.set(Calendar.SECOND, 0);
		c1.set(Calendar.MILLISECOND, 0);

		c2.setTime(toDate);
		c2.set(Calendar.HOUR_OF_DAY, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.SECOND, 0);
		c2.set(Calendar.MILLISECOND, 0);

		if (c1.before(c2))
			return true;
		else if (c1.after(c2))
			return false;
		else if (c1.equals(c2))
			return true;
		else
			return false;
	}

	public static int monthDifference(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double monthsBetween = 0;
		// difference in month for years
		monthsBetween = (toDate.get(Calendar.YEAR) - fromDate
				.get(Calendar.YEAR)) * 12;
		// difference in month for months
		monthsBetween += toDate.get(Calendar.MONTH)
				- fromDate.get(Calendar.MONTH);
		// difference in month for days
		if (toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			monthsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		return (int) monthsBetween;
	}

	public static double monthDifferenceUnRound(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double monthsBetween = 0;
		// difference in month for years
		monthsBetween = (toDate.get(Calendar.YEAR) - fromDate
				.get(Calendar.YEAR)) * 12;
		// difference in month for months
		monthsBetween += toDate.get(Calendar.MONTH)
				- fromDate.get(Calendar.MONTH);
		// difference in month for days
		if (toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			monthsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		return monthsBetween;
	}

	public static double offerMonthDifference(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		return (toDate.get(Calendar.YEAR) - fromDate.get(Calendar.YEAR)) * 12;
	}

	public static int yearDifference(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double yrsBetween = 0;
		// difference in month for years
		yrsBetween = (toDate.get(Calendar.YEAR) - fromDate.get(Calendar.YEAR)) * 12;
		// difference in month for months
		yrsBetween += toDate.get(Calendar.MONTH) - fromDate.get(Calendar.MONTH);
		// difference in month for days
		if (toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			yrsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		yrsBetween /= 12;
		return (int) yrsBetween;
	}

	public static int halfYearlyVariance(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double yrsBetween = 0;
		// difference in month for years
		yrsBetween = (toDate.get(Calendar.YEAR) - fromDate.get(Calendar.YEAR)) * 12;
		// difference in month for months
		yrsBetween += toDate.get(Calendar.MONTH) - fromDate.get(Calendar.MONTH);
		// difference in month for days
		if (toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			yrsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		yrsBetween /= 6;
		return (int) yrsBetween;
	}

	public static int quaterVariance(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double yrsBetween = 0;
		// difference in month for years
		yrsBetween = (toDate.get(Calendar.YEAR) - fromDate.get(Calendar.YEAR)) * 12;
		// difference in month for months
		yrsBetween += toDate.get(Calendar.MONTH) - fromDate.get(Calendar.MONTH);
		// difference in month for days
		if (toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			yrsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		yrsBetween /= 4;
		return (int) yrsBetween;
	}

	public static Date addMonth(Date currentDate) throws ParseException {
		Calendar nextMonth = Calendar.getInstance();
		nextMonth.setTime(currentDate);
		nextMonth.add(Calendar.MONTH, 1);
		return nextMonth.getTime();
	}

	public static Date addToMonths(Date currentDate, int noOfMonths)
			throws ParseException {
		Calendar newMonth = Calendar.getInstance();
		newMonth.setTime(currentDate);
		newMonth.add(Calendar.MONTH, noOfMonths);
		return newMonth.getTime();
	}

	public static Date addToDays(Date currentDate, int noOfDays)
			throws ParseException {
		Calendar newDate = Calendar.getInstance();
		newDate.setTime(currentDate);
		newDate.add(Calendar.DATE, noOfDays);
		return newDate.getTime();
	}

	public static Date addYear(Date currentDate) throws ParseException {
		Calendar nextMonth = Calendar.getInstance();
		nextMonth.setTime(currentDate);
		nextMonth.add(Calendar.MONTH, 12);
		return nextMonth.getTime();
	}

	public static long dayVariance(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		long milis1 = fromDate.getTimeInMillis();
		long milis2 = toDate.getTimeInMillis();
		long diff = milis2 - milis1;
		return (diff / (24 * 60 * 60 * 1000));
	}

	public static double dayDifference(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		long milis1 = fromDate.getTimeInMillis();
		long milis2 = toDate.getTimeInMillis();
		double diff = milis2 - milis1;
		diff = (diff / (24 * 60 * 60 * 1000));
		return diff;
	}

	public static int weekVariance(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		long milis1 = fromDate.getTimeInMillis();
		long milis2 = toDate.getTimeInMillis();
		long diff = milis2 - milis1;
		long totalDays = diff / (24 * 60 * 60 * 1000);
		return (int) (totalDays / 7);
	}

	public static int monthsBetween(Date minuend, Date subtrahend) {
		Calendar cal = Calendar.getInstance();
		// default will be Gregorian in US Locales
		cal.setTime(minuend);
		int minuendMonth = cal.get(Calendar.MONTH);
		int minuendYear = cal.get(Calendar.YEAR);
		cal.setTime(subtrahend);
		int subtrahendMonth = cal.get(Calendar.MONTH);
		int subtrahendYear = cal.get(Calendar.YEAR);

		// the following will work okay for Gregorian but will not
		// work correctly in a Calendar where the number of months
		// in a year is not constant
		return ((minuendYear - subtrahendYear) * 12)
				+ (minuendMonth - subtrahendMonth);
	}

	public static double monthVariance(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double monthsBetween = 0;
		// difference in month for years
		monthsBetween = (toDate.get(Calendar.YEAR) - fromDate
				.get(Calendar.YEAR)) * 12;
		// difference in month for months
		monthsBetween += toDate.get(Calendar.MONTH)
				- fromDate.get(Calendar.MONTH);
		// difference in month for days
		if (toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			monthsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		return monthsBetween;
	}

	public static Double timeDifferenceReturnDouble(String time1, String time2)
			throws Exception {
		if (time1 != null && time2 != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm");
			Date date1 = formatter.parse(time1);
			Date date2 = formatter.parse(time2);
			// long millis = date1.getTime() - date2.getTime();
			Calendar fromDate = Calendar.getInstance();
			Calendar toDate = Calendar.getInstance();
			fromDate.setTime(date1);
			toDate.setTime(date2);

			long millis = fromDate.getTimeInMillis() - toDate.getTimeInMillis();
			long time = millis / 1000;
			String minutes = Integer.toString(Math
					.abs((int) ((time % 3600) / 60)));
			String hours = Integer.toString((int) (time / 3600));

			if (minutes.length() == 1)
				minutes = "0" + minutes;
			return Double.parseDouble(hours + "." + minutes);
		} else {
			return null;
		}
	}

	public static Double timeDifference(Date time1, Date time2)
			throws Exception {
		if (time1 != null && time2 != null) {

			Calendar fromDate = Calendar.getInstance();
			Calendar toDate = Calendar.getInstance();
			fromDate.setTime(time1);
			toDate.setTime(time2);

			long millis = (fromDate.getTimeInMillis() > 0 ? fromDate
					.getTimeInMillis() : 3600000)
					- (toDate.getTimeInMillis() > 0 ? toDate.getTimeInMillis()
							: 3600000);
			long time = millis / 1000;
			String minutes = Integer.toString(Math
					.abs((int) ((time % 3600) / 60)));
			String hours = Integer.toString((int) (time / 3600));
			if (minutes.length() == 1)
				minutes = "0" + minutes;
			if (time2.after(time1))
				return Math.abs(Double.parseDouble(hours + "." + minutes));
			else
				return -Math.abs(Double.parseDouble(hours + "." + minutes));
		} else {
			return null;
		}
	}

	public static Double timeDifferencePasitive(Date time1, Date time2)
			throws Exception {
		if (time1 != null && time2 != null) {

			Calendar fromDate = Calendar.getInstance();
			Calendar toDate = Calendar.getInstance();
			fromDate.setTime(time1);
			toDate.setTime(time2);

			long millis = (fromDate.getTimeInMillis() > 0 ? fromDate
					.getTimeInMillis() : 3600000)
					- (toDate.getTimeInMillis() > 0 ? toDate.getTimeInMillis()
							: 3600000);
			long time = millis / 1000;
			String minutes = Integer.toString(Math
					.abs((int) ((time % 3600) / 60)));
			String hours = Integer.toString((int) (time / 3600));
			if (minutes.length() == 1)
				minutes = "0" + minutes;

			if (time2.before(time1))
				return Double.parseDouble(hours + "." + minutes);
			else
				return -Math.abs(Double.parseDouble(hours + "." + minutes));
		} else {
			return null;
		}
	}

	public static Date timeDifferenceReturnDate(Date time1, Date time2)
			throws Exception {
		if (time1 != null && time2 != null) {

			Calendar fromDate = Calendar.getInstance();
			Calendar toDate = Calendar.getInstance();
			fromDate.setTime(time1);
			toDate.setTime(time2);

			long millis = fromDate.getTimeInMillis() - toDate.getTimeInMillis();
			Calendar outputDate = Calendar.getInstance();
			outputDate.setTimeInMillis(millis);

			return outputDate.getTime();
		} else {
			return null;
		}
	}

	public static String getTimeOnly(Date time) {
		String shortTimeStr = "00:00:00";
		if (time != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			shortTimeStr = formatter.format(time);
		}
		return shortTimeStr;
	}

	public static String getTimeOnlyWithSeconds(Date time) {
		String shortTimeStr = "00:00:00";
		if (time != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			shortTimeStr = formatter.format(time);
		}
		return shortTimeStr;
	}

	public static Double getTimeOnlyDouble(Date time) {
		double diffHours = 0.0;
		if (time != null) {
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(time);
			long millis = fromDate.getTimeInMillis();
			long tim = millis / 1000;
			String minutes = Integer.toString(Math
					.abs((int) ((tim % 3600) / 60)));
			String hours = Integer.toString((int) (tim / 3600));
			if (minutes.length() == 1)
				minutes = "0" + minutes;
			return Double.parseDouble(hours + "." + minutes);
		}
		return diffHours;
	}

	public static Double convertTimeToDouble(Date time) {
		double fromatedTime = 0.0;
		if (time != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			String shortTimeStr = formatter.format(time);
			StringTokenizer st2 = new StringTokenizer(shortTimeStr, ":");
			int i = 0;
			int hr = 0;
			String minit = "0";

			while (st2.hasMoreElements()) {
				if (i == 0) {
					String temp = st2.nextElement().toString();
					hr = Integer.valueOf(temp);
				} else if (i == 2) {
					String temp = st2.nextElement().toString();
					if (temp.length() > 1) {
						minit = temp.substring(0, 2);
					} else {
						minit = temp.substring(0, 1) + "";
					}
				}
				i++;
			}
			fromatedTime = Double.valueOf(hr + "." + minit);
		}
		return fromatedTime;
	}

	/*
	 * public static Date getTimeOnlyReturnDate(String time){ SimpleDateFormat
	 * formatter = new SimpleDateFormat("HH:mm"); Date date =
	 * formatter.format(time); return date; }
	 */
	public static String getDateOnly(Date date) {
		String shortTimeStr = date.toString();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		shortTimeStr = formatter.format(date);
		return shortTimeStr;
	}

	public static String getDay(Date date) {
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("E");
		simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week
															// spelled out
															// completely
		return simpleDateformat.format(date);
	}

	public static String convertDoubleToTimeStr(Double hours) {
		String fromatedTime = "";
		if (hours != null) {
			StringTokenizer st2 = new StringTokenizer(hours.toString(), ".");
			int i = 0;
			int hr = 0;
			String minit = "0";

			while (st2.hasMoreElements()) {
				if (i == 0) {
					String temp = st2.nextElement().toString();
					hr = Integer.valueOf(temp);
				} else if (i == 2) {
					String temp = st2.nextElement().toString();
					if (temp.length() > 1) {
						minit = temp.substring(0, 2);
						if (Integer.valueOf(minit) >= 60)
							minit = (Integer.valueOf(minit) - 40) + "";
					} else {
						minit = (Integer.valueOf(temp.substring(0, 1)) * 10)
								+ "";
						if (Integer.valueOf(minit) >= 60)
							minit = ((Integer.valueOf(temp.substring(0, 1)) * 10) - 40)
									+ "";
					}
				}
				i++;
			}
			fromatedTime = (hr + "." + minit);
		}
		return fromatedTime;
	}

	public static double roundTwoDecimals(double d) {
		DecimalFormat twoDForm = new DecimalFormat("##.##");
		return Double.parseDouble((twoDForm.format(d)));
	}

	@SuppressWarnings("finally")
	public static Date convertStringToTime(String date) {
		Date time = null;
		try {
			if (date != null && !date.equalsIgnoreCase("")) {
				java.text.DateFormat sdf = new SimpleDateFormat("HH:mm");
				time = sdf.parse(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return time;
		}
	}

	@SuppressWarnings("finally")
	public static String convertTimeToString(String date) {
		String time = null;
		try {
			if (date != null) {
				java.text.DateFormat sdf = new SimpleDateFormat("HH:mm");
				Date dateOut = sdf.parse(date.toString());
				time = sdf.format(dateOut);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return time;
		}
	}

	public static Date convertDoubleToTimeStrFromat(Double hours)
			throws ParseException {
		String fromatedTime = "";
		if (hours != null) {
			StringTokenizer st2 = new StringTokenizer(hours.toString(), ".");
			int i = 0;
			int hr = 0;
			String minit = "0";

			while (st2.hasMoreElements()) {
				if (i == 0) {
					String temp = st2.nextElement().toString();
					hr = Integer.valueOf(temp);
				} else if (i == 2) {
					String temp = st2.nextElement().toString();
					if (temp.length() > 1) {
						minit = temp.substring(0, 2);
						if (Integer.valueOf(minit) >= 60)
							minit = (Integer.valueOf(minit) - 40) + "";
					} else {
						minit = (Integer.valueOf(temp.substring(0, 1)) * 10)
								+ "";
						if (Integer.valueOf(minit) >= 60)
							minit = ((Integer.valueOf(temp.substring(0, 1)) * 10) - 40)
									+ "";
					}
				}
				i++;
			}
			fromatedTime = (hr + ":" + minit);

		}
		return convertStringToTime(fromatedTime);
	}

	/**
	 * This Method compares two dates.
	 * 
	 * @param date1
	 *            :
	 * @param date2
	 *            :
	 * @return: LESS, GREATER, EQUAL
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */
	public static String compare2Dates(Date date1, Date date2) {

		String result = "";

		Calendar c1 = Calendar.getInstance();
		c1.setTime(date1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(date2);

		if (c1.before(c2)) {
			result = "LESS";
		} else if (c1.after(c2)) {
			result = "GREATER";
		} else if (c1.equals(c2)) {
			result = "EQUAL";
		}

		return result;
	}

	/**
	 * This Method adds DATE, HOURS, MONTH etc to the provided date.
	 * 
	 * @param date
	 *            : Actual Date
	 * @param field
	 *            : Calendar field
	 * @param amount
	 *            : amount to add to the field of Actual Date
	 * @return: Modified Date
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */
	public static Date addField(Date date, int field, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, amount);
		return cal.getTime();
	}

	// Get from date with 0hrs
	public static Date getFromDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}

	// Get from date with 0hrs
	public static Date getToDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

}
