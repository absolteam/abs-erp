package com.aiotech.aios.project.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants.Accounts.ProjectDeliveryStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.project.domain.entity.vo.ProjectDeliveryVO;
import com.aiotech.aios.project.service.bl.ProjectDeliveryBL;
import com.aiotech.aios.project.service.bl.ProjectTaskBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionSupport;

public class ProjectDeliveryAction extends ActionSupport {

	private static final long serialVersionUID = 1326110696480032267L;

	private static final Logger log = Logger
			.getLogger(ProjectDeliveryAction.class);

	private ProjectDeliveryBL projectDeliveryBL;
	private ProjectTaskBL projectTaskBL;

	private long projectDeliveryId;
	private long projectTaskId;
	private long alertId;
	private long recordId;
	private byte deliveryStatus;
	private String referenceNumber;
	private String deliveryTitle;
	private String deliveryDate;
	private String description;
	private String returnMessage;
	private List<Object> aaData;
	private long projectId;
	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		return SUCCESS;
	}

	// Show All Projects
	public String showAllProjectDeliveries() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProjectDeliveries");
			aaData = projectDeliveryBL.showAllProjectDeliveries();
			log.info("Module: Accounts : Method: showAllProjectDeliveries: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProjectDeliveries Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProjectDeliveryEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectDeliveryEntry");
			ProjectDelivery projectDelivery = null;
			ProjectDeliveryVO projectDeliveryVO = null;
			if (projectDeliveryId == 0)
				projectDeliveryId = recordId;
			if (projectDeliveryId > 0) {
				projectDelivery = projectDeliveryBL.getProjectDeliveryService()
						.getProjectDeliveryById(projectDeliveryId);
				projectDeliveryVO = projectDeliveryBL
						.convertEntityToVO(projectDelivery);
			} else {
				projectDeliveryVO=new ProjectDeliveryVO();
				referenceNumber = SystemBL.getReferenceStamp(
						ProjectDelivery.class.getName(),
						projectDeliveryBL.getImplementation());
				if(projectTaskId>0){
					projectDeliveryVO = new ProjectDeliveryVO();
					ProjectTask projectTask = projectTaskBL.getProjectTaskService()
							.getProjectTaskById(projectTaskId);
					projectDeliveryVO.setProjectTask(projectTask);
				}else if(projectId>0){
					Project project = projectDeliveryBL
					.getProjectService().getProjectById(
							projectId);
					projectDeliveryVO.setProject(project);
				}
				projectDeliveryVO.setDate(DateFormat
						.convertSystemDateToString(new Date()));
			}
			ServletActionContext.getRequest().setAttribute("DELIVERY_INFO",
					projectDeliveryVO);
			Map<Byte, ProjectDeliveryStatus> deliveryStatus = new HashMap<Byte, ProjectDeliveryStatus>();
			for (ProjectDeliveryStatus status : EnumSet
					.allOf(ProjectDeliveryStatus.class))
				deliveryStatus.put(status.getCode(), status);
			ServletActionContext.getRequest().setAttribute("DELIVERY_STATUS",
					deliveryStatus);
			log.info("Module: Accounts : Method: showProjectDeliveryEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectDeliveryEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveProjectDelivery() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectDeliveryEntry");
			ProjectDelivery projectDelivery = new ProjectDelivery();
			if (projectDeliveryId > 0) {
				projectDelivery.setProjectDeliveryId(projectDeliveryId);
				projectDelivery.setReferenceNumber(referenceNumber);

			} else {
				projectDelivery.setReferenceNumber(SystemBL.getReferenceStamp(
						ProjectDelivery.class.getName(),
						projectDeliveryBL.getImplementation()));

			}
			projectDelivery.setDeliveryStatus(deliveryStatus);
			projectDelivery.setDeliveryDate(DateFormat
					.convertStringToDate(deliveryDate));
			projectDelivery.setDeliveryTitle(deliveryTitle);
			projectDelivery.setDescription(description);
			if (projectTaskId > 0) {
				ProjectTask projectTask = projectDeliveryBL
						.getProjectTaskService().getProjectTaskById(
								projectTaskId);
				projectDelivery
						.setProjectTaskSurrogate(new ArrayList<ProjectTaskSurrogate>(
								projectTask.getProjectTaskSurrogates()).get(0));
			}else if(projectId>0){
				Project project = projectDeliveryBL
				.getProjectService().getProjectById(
						projectId);
				projectDelivery
						.setProjectTaskSurrogate(new ArrayList<ProjectTaskSurrogate>(
								project.getProjectTaskSurrogates()).get(0));
			}
			

			projectDeliveryBL.saveProjectDelivery(projectDelivery);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: showProjectDeliveryEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: showProjectDeliveryEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updateProjectDelivery() {
		try {
			log.info("Inside Module: Accounts : Method: updateProjectDelivery");
			ProjectDelivery projectDelivery = projectDeliveryBL
					.getProjectDeliveryService().getProjectDeliveryById(
							projectDeliveryId);
			projectDelivery.setDeliveryStatus(deliveryStatus);
			if ((byte) projectDelivery.getDeliveryStatus() == (byte) ProjectDeliveryStatus.Delivered
					.getCode())
				projectDelivery.setActualDeliveryDate(Calendar.getInstance()
						.getTime());
			projectDeliveryBL.updateProject(projectDelivery);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updateProjectDelivery: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: updateProjectDelivery Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteProjectDelivery() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectDeliveryEntry");
			ProjectDelivery projectDelivery = projectDeliveryBL
					.getProjectDeliveryService().getProjectDeliveryById(
							projectDeliveryId);
			projectDeliveryBL.deleteProjectDelivery(projectDelivery);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProjectDelivery: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteProjectDelivery Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public long getProjectDeliveryId() {
		return projectDeliveryId;
	}

	public void setProjectDeliveryId(long projectDeliveryId) {
		this.projectDeliveryId = projectDeliveryId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getDeliveryTitle() {
		return deliveryTitle;
	}

	public void setDeliveryTitle(String deliveryTitle) {
		this.deliveryTitle = deliveryTitle;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public ProjectDeliveryBL getProjectDeliveryBL() {
		return projectDeliveryBL;
	}

	public void setProjectDeliveryBL(ProjectDeliveryBL projectDeliveryBL) {
		this.projectDeliveryBL = projectDeliveryBL;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public byte getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(byte deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public long getProjectTaskId() {
		return projectTaskId;
	}

	public void setProjectTaskId(long projectTaskId) {
		this.projectTaskId = projectTaskId;
	}

	public ProjectTaskBL getProjectTaskBL() {
		return projectTaskBL;
	}

	public void setProjectTaskBL(ProjectTaskBL projectTaskBL) {
		this.projectTaskBL = projectTaskBL;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

}
