package com.aiotech.aios.project.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jxl.common.Logger;

import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectPaymentDetail;
import com.aiotech.aios.project.service.bl.ProjectMileStoneBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionSupport;

public class ProjectMileStoneAction extends ActionSupport {

	private static final long serialVersionUID = -194035645268314659L;

	private static final Logger log = Logger
			.getLogger(ProjectMileStoneAction.class);

	private ProjectMileStoneBL projectMileStoneBL;

	private long projectMileStoneId;
	private long projectId;
	private long alertId;
	private long recordId;
	private double estimationCost;
	private double actualCost;
	private int rowId;
	private byte status;
	private String referenceNumber;
	private String milestoneTitle;
	private String startDate;
	private String endDate;
	private String actualStartDate;
	private String actualEndDate;
	private String description;
	private String projectPayments;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		return SUCCESS;
	}

	// Show All Project MileStones
	public String showAllProjectMileStones() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProjectMileStones");
			aaData = projectMileStoneBL.showActiveProjectMileStones();
			log.info("Module: Accounts : Method: showAllProjectMileStones: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProjectMileStones Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	// Show All Project MileStones
	public String getMileStonesByProject() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProjectMileStones");
			aaData = projectMileStoneBL.getActiveProjectMileStonesByProject(projectId);
			log.info("Module: Accounts : Method: showAllProjectMileStones: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProjectMileStones Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	

	// Show Project MileStone by project
	public String showProjectMileStoneByProject() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectMileStoneByProject");
			aaData = projectMileStoneBL
					.showProjectMileStoneByProject(projectId);
			log.info("Module: Accounts : Method: showProjectMileStoneByProject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectMileStoneByProject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show ProjectMileStone entry screen
	public String showProjectMileStoneEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectMileStoneEntry");
			ProjectMileStone projectMileStone = null;
			if (projectMileStoneId == 0)
				projectMileStoneId = recordId;
			if (projectMileStoneId > 0)
				projectMileStone = projectMileStoneBL
						.getProjectMileStoneService().getProjectMileStoneById(
								projectMileStoneId);
			else
				referenceNumber = SystemBL.getReferenceStamp(
						ProjectMileStone.class.getName(),
						projectMileStoneBL.getImplementation());
			Map<Byte, ProjectStatus> projectStatus = new HashMap<Byte, ProjectStatus>();
			for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class)) {
				projectStatus.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("PROJECT_STATUS",
					projectStatus);
			ServletActionContext.getRequest().setAttribute("PROJECT_MILESTONE",
					projectMileStone);
			log.info("Module: Accounts : Method: showProjectMileStoneEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectMileStoneEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveProjectMileStone() {
		try {
			log.info("Inside Module: Accounts : Method: saveProjectMileStone");
			ProjectMileStone projectMileStone = new ProjectMileStone();
			/*projectMileStone.setMilestoneTitle(milestoneTitle);
			projectMileStone.setStatus(ProjectStatus.InProgess.getCode());*/
			projectMileStone.setStartDate(DateFormat
					.convertStringToDate(startDate));
			projectMileStone
					.setEndDate(DateFormat.convertStringToDate(endDate));
			projectMileStone.setDescription(description);
			Project project = new Project();
			project.setProjectId(projectId);
			projectMileStone.setProject(project);
			/*projectMileStone.setEstimationCost(estimationCost);*/
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(projectPayments);
			JSONArray object = (JSONArray) receiveObject;
			ProjectPaymentDetail projectPaymentDetail = null;
			List<ProjectPaymentDetail> projectPaymentDetails = new ArrayList<ProjectPaymentDetail>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("projectPayments");
				if (null != scheduleArray && scheduleArray.size() > 0) { 
					for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
							.hasNext();) {
						JSONObject paymentDetail = (JSONObject) iteratorObject
								.next();
						projectPaymentDetail = new ProjectPaymentDetail();
						projectPaymentDetail.setPaymentTitle(paymentDetail.get(
								"paymentReference").toString());
						projectPaymentDetail.setPaymentDate(DateFormat
								.convertStringToDate(paymentDetail.get(
										"paymentDate").toString()));
						projectPaymentDetail.setPaymentAmount(Double
								.parseDouble(paymentDetail.get("paymentAmount")
										.toString()));
						projectPaymentDetail.setDescription(paymentDetail.get(
								"description").toString());
						projectPaymentDetail.setStatus(PaymentStatus.Pending
								.getCode());
						long paymentId = Long.parseLong(paymentDetail.get(
								"paymentDetailId").toString());
						if (paymentId > 0)
							projectPaymentDetail
									.setProjectPaymentDetailId(paymentId);
						projectPaymentDetails.add(projectPaymentDetail);
					}
				}
			}
			List<ProjectPaymentDetail> deletePaymentDetails = null;
			if (projectMileStoneId > 0) {
			/*	projectMileStone.setProjectMileStoneId(projectMileStoneId);
				projectMileStone.setReferenceNumber(referenceNumber);*/
				deletePaymentDetails = userDeletedPaymentDetails(
						projectPaymentDetails,
						projectMileStoneBL.getProjectMileStoneService()
								.getProjectPaymentDetailByMileStone(
										projectMileStoneId));
			} else
				/*projectMileStone.setReferenceNumber(SystemBL.getReferenceStamp(
						ProjectMileStone.class.getName(),
						projectMileStoneBL.getImplementation()));*/
			projectMileStoneBL.saveProjectMileStone(projectMileStone,
					projectPaymentDetails, deletePaymentDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProjectMileStone: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: saveProjectMileStone Exception "
					+ ex);
			returnMessage = "FAILURE";
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<ProjectPaymentDetail> userDeletedPaymentDetails(
			List<ProjectPaymentDetail> projectPaymentDetails,
			List<ProjectPaymentDetail> projectPaymentDetailOld)
			throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProjectPaymentDetail> hashPaymentDetails = new HashMap<Long, ProjectPaymentDetail>();
		if (null != projectPaymentDetailOld
				&& projectPaymentDetailOld.size() > 0) {
			for (ProjectPaymentDetail list : projectPaymentDetailOld) {
				listOne.add(list.getProjectPaymentDetailId());
				hashPaymentDetails.put(list.getProjectPaymentDetailId(), list);
			}
			for (ProjectPaymentDetail list : projectPaymentDetails) {
				listTwo.add(list.getProjectPaymentDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProjectPaymentDetail> paymentList = new ArrayList<ProjectPaymentDetail>();
		if (null != different && different.size() > 0) {
			ProjectPaymentDetail tempPaymentDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempPaymentDetail = new ProjectPaymentDetail();
					tempPaymentDetail = hashPaymentDetails.get(list);
					paymentList.add(tempPaymentDetail);
				}
			}
		}
		return paymentList;
	}

	public String updateProjectMileStone() {
		try {
			log.info("Inside Module: Accounts : Method: updateProjectMileStone");
			ProjectMileStone projectMileStone = projectMileStoneBL
					.getProjectMileStoneService().getProjectMileStoneById(
							projectMileStoneId);
			/*projectMileStone.setStatus(status);
			if ((byte) projectMileStone.getStatus() == (byte) ProjectStatus.Completed
					.getCode()) {
				if (null != actualStartDate
						&& !("").equals(actualStartDate.trim()))
					projectMileStone.setActualStartDate(DateFormat
							.convertStringToDate(actualStartDate));
				if (null != actualEndDate && !("").equals(actualEndDate.trim()))
					projectMileStone.setActualStartDate(DateFormat
							.convertStringToDate(actualEndDate));
				projectMileStone.setActualCost(actualCost);
			}*/
			projectMileStoneBL.updateProjectMileStone(projectMileStone);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updateProjectMileStone: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: updateProjectMileStone Exception "
					+ ex);
			returnMessage = "FAILURE";
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete
	public String deleteProjectMileStone() {
		try {
			log.info("Inside Module: Accounts : Method: saveProjectMileStone");
			ProjectMileStone projectMileStone = projectMileStoneBL
					.getProjectMileStoneService().getProjectMileStoneById(
							projectMileStoneId);
			projectMileStoneBL.deleteProjectMileStone(projectMileStone);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProjectMileStone: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: deleteProjectMileStone Exception "
					+ ex);
			returnMessage = "FAILURE";
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public ProjectMileStoneBL getProjectMileStoneBL() {
		return projectMileStoneBL;
	}

	public void setProjectMileStoneBL(ProjectMileStoneBL projectMileStoneBL) {
		this.projectMileStoneBL = projectMileStoneBL;
	}

	public long getProjectMileStoneId() {
		return projectMileStoneId;
	}

	public void setProjectMileStoneId(long projectMileStoneId) {
		this.projectMileStoneId = projectMileStoneId;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getMilestoneTitle() {
		return milestoneTitle;
	}

	public void setMilestoneTitle(String milestoneTitle) {
		this.milestoneTitle = milestoneTitle;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getProjectPayments() {
		return projectPayments;
	}

	public void setProjectPayments(String projectPayments) {
		this.projectPayments = projectPayments;
	}

	public double getEstimationCost() {
		return estimationCost;
	}

	public void setEstimationCost(double estimationCost) {
		this.estimationCost = estimationCost;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(String actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public String getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(String actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public double getActualCost() {
		return actualCost;
	}

	public void setActualCost(double actualCost) {
		this.actualCost = actualCost;
	}
}
