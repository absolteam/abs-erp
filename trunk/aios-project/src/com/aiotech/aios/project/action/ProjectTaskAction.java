package com.aiotech.aios.project.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionVO;
import com.aiotech.aios.accounts.service.bl.BankReceiptsBL;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.Priority;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectExpenseMode;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectPaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectResourceType;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectDiscussion;
import com.aiotech.aios.project.domain.entity.ProjectExpense;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectNotes;
import com.aiotech.aios.project.domain.entity.ProjectResource;
import com.aiotech.aios.project.domain.entity.ProjectResourceDetail;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.project.domain.entity.vo.ProjectDeliveryVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectDiscussionVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectNotesVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectTaskVO;
import com.aiotech.aios.project.service.bl.ProjectBL;
import com.aiotech.aios.project.service.bl.ProjectTaskBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ProjectTaskAction extends ActionSupport {

	private static final long serialVersionUID = -5068099892504570168L;

	private static final Logger log = Logger.getLogger(ProjectTaskAction.class);

	// Dependency
	private ProjectBL projectBL;
	private BankReceiptsBL bankReceiptsBL;
	private ProjectTaskBL projectTaskBL;

	// Variables
	private Long projectId;
	private long projectTaskId;
	private long projectType;
	private long creditTermId;
	private long personId;
	private long customerId;
	private long pettyCashId;
	private int rowId;
	private byte status;
	private String referenceNumber;
	private String taskTitle;
	private String startDate;
	private String endDate;
	private String actualStartDate;
	private String actualEndDate;
	private String description;
	private Byte priority;
	private Long supplierId;

	private String projectResources;
	private String projectExpenses;
	private String projectInventories;
	private String returnMessage;
	private List<Object> aaData;
	private Long recordId;
	private Long alertId;
	private Implementation implementation;
	private String message;
	private Boolean isPublic;
	private Long projectMileStoneId;
	private Long parentProjectTaskId;
	private boolean isStockUpdate;
	private String saveType;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		return SUCCESS;
	}

	// Show All Projects
	public String showAllProjectsTask() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProjectsTask");
			aaData = projectTaskBL.showAllProjectTask(projectId);
			log.info("Module: Accounts : Method: showAllProjectsTask: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProjectsTask Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show active projects
	public String showActiveProjectsTask() {
		try {
			log.info("Inside Module: Accounts : Method: showActiveProjects");
			aaData = projectBL.showActiveProject();
			log.info("Module: Accounts : Method: showActiveProjects: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showActiveProjects Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Project Entry
	public String showProjectTaskEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectEntry");
			ProjectTask projectTask = null;
			ProjectTaskVO projectTaskVO = null;
			HttpSession sSession = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			if (alertId != null && alertId > 0 && recordId != null
					&& recordId > 0) {
				ProjectDelivery pd = projectBL.getProjectDeliveryService()
						.getProjectDeliveryById(recordId);
				projectTaskId = pd.getProjectTaskSurrogate().getProjectTask()
						.getProjectTaskId();
			}

			if (projectTaskId > 0) {
				projectTask = projectTaskBL.getProjectTaskService()
						.getProjectTaskById(projectTaskId);

				projectTaskVO = projectTaskBL.convertProjectTaskToVO(
						projectTask, null);

				projectTaskVO.setAlertId(alertId);

				// Petty Cash work
				if (null != projectTask.getPettyCash()
						&& null != projectTask.getPettyCash().getPettyCashId()) {
					PettyCash pettyCash = projectBL
							.getPettyCashBL()
							.getPettyCashService()
							.getPettyCashById(
									projectTask.getPettyCash().getPettyCashId());
					double cashIn = pettyCash.getCashIn();
					double cashOut = projectBL.getPettyCashBL()
							.showTotalCashOutPettyCash(
									new ArrayList<PettyCash>(
											pettyCash.getPettyCashs()),
									projectTaskId,
									ProjectTask.class.getSimpleName());
					projectTaskVO.setPettyCashAmount(cashIn - cashOut);
				}

				// Material Requisition check
				MaterialRequisitionVO mvo = new MaterialRequisitionVO();
				mvo.setRecordId(projectTask.getProjectTaskId());
				mvo.setUseCase(ProjectTask.class.getSimpleName());
				mvo.setImplementation(getImplementation());
				List<MaterialRequisition> requistions = projectBL
						.getMaterialRequisitionBL()
						.getMaterialRequisitionService()
						.getFilteredMaterialRequisition(mvo);
				if (requistions != null && requistions.size() > 0) {
					List<MaterialRequisitionVO> requisitonsVOs = projectBL
							.getMaterialRequisitionBL()
							.convertMaterialRequistionToVOs(requistions);
					ServletActionContext.getRequest().setAttribute(
							"REQUISITION_INFO", requisitonsVOs);
				}

				List<ProjectDeliveryVO> projectDeliveryVOs = projectTaskBL
						.getProjectDeliveryBL().getAllDeliverablesByTask(
								projectTaskId);
				ServletActionContext.getRequest().setAttribute("RESOURCE_INFO",
						projectTaskVO.getProjectResourceVOs());
				ServletActionContext.getRequest().setAttribute("DELIVERY_INFO",
						projectDeliveryVOs);
				ServletActionContext.getRequest().setAttribute(
						"PROJECT_EXPENSE_INFO",
						projectTaskVO.getProjectExpenseVOs());

				ServletActionContext.getRequest().setAttribute(
						"INVENTORY_DETAIL",
						projectTaskVO.getProjectInventoryVOs());
				List<ProjectTaskVO> projectTaskVOs = projectTaskBL
						.showAllProjectTaskByParentTask(projectTaskId);
				ServletActionContext.getRequest().setAttribute("TASKS_INFO",
						projectTaskVOs);

				// Expense Schedule
				ServletActionContext.getRequest().setAttribute(
						"EXPENSE_TYPES",
						projectBL.getLookupMasterBL().getActiveLookupDetails(
								"CASE_EXPENSE_TYPE", true));
				Map<Byte, String> expenseMode = new HashMap<Byte, String>();
				for (ProjectExpenseMode e : EnumSet
						.allOf(ProjectExpenseMode.class))
					expenseMode.put(e.getCode(), e.name().replaceAll("_", " "));
				ServletActionContext.getRequest().setAttribute("EXPENSE_MODES",
						expenseMode);
				// Tabs Access Rights Conditions
				if (projectTask.getProject() != null
						&& projectTask.getProject().getPersonByProjectLead() != null
						&& (long) projectTask.getProject()
								.getPersonByProjectLead().getPersonId() == (long) user
								.getPersonId()) {
					projectTaskVO.setAllowModification(true);

					if (sSession.getAttribute("project_access_area") != null)
						projectTaskVO
								.setProjectAccessRights(sSession.getAttribute(
										"project_access_area").toString());
				} else {
					projectTaskVO.setAllowModification((Boolean) sessionObj
							.get("ADMIN"));
					List<ProjectTaskSurrogate> ptsTemp = new ArrayList<ProjectTaskSurrogate>(
							projectTaskVO.getProjectTaskSurrogates());
					ProjectTaskSurrogate projectTaskSurrogate = ptsTemp.get(0);
					for (ProjectResource projectResouce : projectTaskSurrogate
							.getProjectResources()) {
						if (projectResouce.getAccessRights() != null
								&& !projectResouce.getAccessRights().trim()
										.equals("")
								&& (byte) projectResouce.getResourceType() == (byte) 2) {
							for (ProjectResourceDetail projectResourceDetail : projectResouce
									.getProjectResourceDetails()) {
								if (projectResourceDetail.getPerson() != null
										&& (long) projectResourceDetail
												.getPerson().getPersonId() == (long) user
												.getPersonId()) {

									projectTaskVO
											.setProjectAccessRights(projectResouce
													.getAccessRights());
									break;
								}

							}
						}
					}

				}

			} else {

				projectTaskVO = new ProjectTaskVO();
				if (projectId != null && projectId > 0) {
					projectTaskVO.setProject(projectBL.getProjectService()
							.getProjectById(projectId));
				} else if (parentProjectTaskId != null
						&& parentProjectTaskId > 0) {
					projectTaskVO.setProjectTask(projectTaskBL
							.getProjectTaskService().getProjectTaskById(
									parentProjectTaskId));
				}
				projectTaskVO.setFromDate(DateFormat
						.convertSystemDateToString(new Date()));
				referenceNumber = SystemBL.getReferenceStamp(
						ProjectTask.class.getName(),
						projectBL.getImplementation());
			}

			Map<Byte, ProjectStatus> projectStatus = new HashMap<Byte, ProjectStatus>();
			for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class))
				projectStatus.put(e.getCode(), e);
			Map<Byte, ProjectResourceType> resourceType = new HashMap<Byte, ProjectResourceType>();
			for (ProjectResourceType e : EnumSet
					.allOf(ProjectResourceType.class))
				resourceType.put(e.getCode(), e);

			Map<Byte, Priority> priority = new HashMap<Byte, Priority>();
			for (Priority e : EnumSet.allOf(Priority.class))
				priority.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PRIORITES",
					priority);
			ServletActionContext.getRequest().setAttribute(
					"PROJECT_TYPE",
					projectBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"RESOURCE_DETAIL",
					projectBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_RESOURCE_DETAIL", true));
			ServletActionContext.getRequest().setAttribute("RESOURCE_TYPE",
					resourceType);
			ServletActionContext.getRequest().setAttribute("PROJECT_STATUS",
					projectStatus);

			ServletActionContext.getRequest().setAttribute("PROJECT_TASK_INFO",
					projectTaskVO);

			log.info("Module: Accounts : Method: showProjectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Project Add Row
	public String showProjectAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectAddRow");
			Map<Byte, ProjectResourceType> resourceType = new HashMap<Byte, ProjectResourceType>();
			for (ProjectResourceType e : EnumSet
					.allOf(ProjectResourceType.class))
				resourceType.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("RESOURCE_TYPE",
					resourceType);
			ServletActionContext.getRequest().setAttribute(
					"RESOURCE_DETAIL",
					projectBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_RESOURCE_DETAIL", true));
			log.info("Module: Accounts : Method: showProjectAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Project Add Row
	public String showProjectPaymentScheduleAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectPaymentScheduleAddRow");
			Map<Byte, ProjectPaymentStatus> paymentStatus = new HashMap<Byte, ProjectPaymentStatus>();
			for (ProjectPaymentStatus e : EnumSet
					.allOf(ProjectPaymentStatus.class))
				paymentStatus.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PAYMENT_STATUS",
					paymentStatus);

			Map<Byte, ModeOfPayment> paymentMode = new HashMap<Byte, ModeOfPayment>();
			for (ModeOfPayment e : EnumSet.allOf(ModeOfPayment.class))
				paymentMode.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					paymentMode);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_TYPES",
					projectBL.getLookupMasterBL().getActiveLookupDetails(
							"CASE_PAYMENT_TYPE", true));
			log.info("Module: Accounts : Method: showProjectPaymentScheduleAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectPaymentScheduleAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Project Expense Add Row
	public String showProjectExpenseAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showProjectExpenseAddRow");

			ServletActionContext.getRequest().setAttribute(
					"EXPENSE_TYPES",
					projectBL.getLookupMasterBL().getActiveLookupDetails(
							"CASE_EXPENSE_TYPE", true));
			Map<Byte, String> expenseMode = new HashMap<Byte, String>();
			for (ProjectExpenseMode e : EnumSet.allOf(ProjectExpenseMode.class))
				expenseMode.put(e.getCode(), e.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("EXPENSE_MODES",
					expenseMode);
			log.info("Module: Accounts : Method: showProjectExpenseAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProjectExpenseAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Project
	public String saveProjectTask() {
		try {
			log.info("Inside Module: Accounts : Method: saveProjectTask");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			ProjectTask projectTask = new ProjectTask();
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			PettyCash pettyCashExpense = null;
			projectTask.setTaskTitle(taskTitle);
			projectTask.setStartDate(DateFormat.convertStringToDate(startDate));
			if (null != endDate && !("").equals(endDate))
				projectTask.setEndDate(DateFormat.convertStringToDate(endDate));

			if (status > 0)
				projectTask.setStatus(status);
			else
				projectTask.setStatus(ProjectStatus.Open.getCode());

			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(projectType);
			// Validate if project status to delivered and no inventories
			boolean validateInventory = true;
			if (alertId != null && alertId > 0 && projectTaskId > 0
					&& status == (byte) ProjectStatus.Delivered.getCode()) {
				JSONParser parser = new JSONParser();
				Object inventoryObject = parser.parse(projectInventories);
				JSONArray object = (JSONArray) inventoryObject;
				object = (JSONArray) inventoryObject;
				if (null == object || object.size() == 0)
					validateInventory = false;
			}
			if (!validateInventory) {
				returnMessage = "Task Delivery should have inventory details.";
				log.info("Module: Accounts : Method: saveProject: Action Success");
				return SUCCESS;
			}
			projectTask.setDescription(description);
			if (projectId > 0) {
				Project prj = new Project();
				prj.setProjectId(projectId);
				projectTask.setProject(prj);
			} else if (parentProjectTaskId != null && parentProjectTaskId > 0) {
				ProjectTask pTask = new ProjectTask();
				pTask.setProjectTaskId(parentProjectTaskId);
				projectTask.setProjectTask(pTask);
			}
			if (supplierId != null && supplierId > 0) {
				Supplier supplier = new Supplier();
				supplier.setSupplierId(supplierId);
				projectTask.setSupplier(supplier);
			}
			projectTask.setPriority(priority);
			if (projectMileStoneId != null && projectMileStoneId > 0) {
				ProjectMileStone projectMileStone = new ProjectMileStone();
				projectMileStone.setProjectMileStoneId(projectMileStoneId);
				projectTask.setProjectMileStone(projectMileStone);
			}

			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(projectResources);
			JSONArray object = (JSONArray) receiveObject;
			ProjectResource projectResource = null;
			ProjectResourceDetail projectResourceDetail = null;
			List<ProjectResource> projectResources = new ArrayList<ProjectResource>();
			List<ProjectResourceDetail> projectResourceDetails = new ArrayList<ProjectResourceDetail>();
			List<ProjectResourceDetail> projectResourceDetailsOld = new ArrayList<ProjectResourceDetail>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("projectResources");
				if (null != scheduleArray && scheduleArray.size() > 0) {
					for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
							.hasNext();) {
						JSONObject resourceDetail = (JSONObject) iteratorObject
								.next();
						projectResource = new ProjectResource();
						projectResource.setResourceType(Byte
								.parseByte(resourceDetail.get("resourceType")
										.toString()));
						if ((byte) projectResource.getResourceType() == (byte) ProjectResourceType.Labour
								.getCode()) {
							parser = new JSONParser();
							Object personObject = parser.parse(resourceDetail
									.get("personData").toString());
							JSONArray prObject = (JSONArray) personObject;
							for (Iterator<?> personItrObject = prObject
									.iterator(); personItrObject.hasNext();) {
								projectResourceDetail = new ProjectResourceDetail();
								JSONObject labourDetail = (JSONObject) personItrObject
										.next();
								person = new Person();
								person.setPersonId(Long.parseLong(labourDetail
										.get("personId").toString()));
								projectResourceDetail.setPerson(person);
								projectResourceDetail
										.setProjectResource(projectResource);
								projectResourceDetails
										.add(projectResourceDetail);
							}
						} else {
							lookupDetail = new LookupDetail();
							lookupDetail.setLookupDetailId(Long
									.parseLong(resourceDetail.get(
											"resourceDetail").toString()));
							projectResource.setLookupDetail(lookupDetail);
						}
						projectResource.setCount(Double
								.parseDouble(resourceDetail
										.get("resourceCount").toString()));
						projectResource.setUnitPrice(Double
								.parseDouble(resourceDetail.get("unitPrice")
										.toString()));
						projectResource.setAccessRights(resourceDetail.get(
								"accessRights").toString());
						projectResource.setDescription(resourceDetail.get(
								"description").toString());
						long resourceId = Long.parseLong(resourceDetail.get(
								"resourceId").toString());
						if (resourceId > 0) {
							projectResource.setProjectResourceId(resourceId);
							List<ProjectResourceDetail> projectResourceDetailtemp = projectBL
									.getProjectService()
									.getProjectResourceDetailById(resourceId);
							if (null != projectResourceDetailtemp
									&& projectResourceDetailtemp.size() > 0)
								projectResourceDetailsOld
										.addAll(projectResourceDetailtemp);
						}
						projectResources.add(projectResource);
					}
				}
			}
			List<ProjectResource> deleteResources = null;
			if (projectTaskId > 0) {
				ProjectTask tempproject = projectTaskBL.getProjectTaskService()
						.getProjectTaskById(projectTaskId);
				projectTask.setProjectTaskId(projectTaskId);
				projectTask.setProjectTaskSurrogates(tempproject
						.getProjectTaskSurrogates());
				projectTask
						.setReferenceNumber(tempproject.getReferenceNumber());
				projectTask.setPerson(tempproject.getPerson());
				projectTask.setCreatedDate(tempproject.getCreatedDate());
				deleteResources = userDeletedResource(
						projectResources,
						projectTaskBL
								.getProjectTaskService()
								.getProjectResourcByProjectTaskId(projectTaskId));
				if (null != deleteResources && deleteResources.size() > 0) {
					for (ProjectResource deleteResource : deleteResources) {
						List<ProjectResourceDetail> projectResourceDetailtemp = projectBL
								.getProjectService()
								.getProjectResourceDetailById(
										deleteResource.getProjectResourceId());
						if (null != projectResourceDetailtemp
								&& projectResourceDetailtemp.size() > 0)
							projectResourceDetailsOld
									.addAll(projectResourceDetailtemp);
					}

				}
			} else {
				projectTask.setReferenceNumber(SystemBL.getReferenceStamp(
						ProjectTask.class.getName(),
						projectBL.getImplementation()));
				projectTask.setCreatedDate(Calendar.getInstance().getTime());
				person = new Person();
				person.setPersonId(user.getPersonId());
				projectTask.setPerson(person);
			}

			// Project Expense Coding Starts
			Object expenseObject = parser.parse(projectExpenses);
			object = (JSONArray) expenseObject;
			ProjectExpense projectExpense = null;
			List<ProjectExpense> projectExpenses = new ArrayList<ProjectExpense>();
			List<ProjectExpense> deleteProjectExpenses = null;
			ProjectExpense editProjectExpense = null;
			List<ProjectExpense> pettyCashExpenses = new ArrayList<ProjectExpense>();
			{
				for (Iterator<?> iterator = object.iterator(); iterator
						.hasNext();) {
					JSONObject jsonObject = (JSONObject) iterator.next();
					JSONArray scheduleArray = (JSONArray) jsonObject
							.get("projectExpenses");
					if (null != scheduleArray && scheduleArray.size() > 0) {
						for (Iterator<?> iteratorObject = scheduleArray
								.iterator(); iteratorObject.hasNext();) {
							JSONObject resourceDetail = (JSONObject) iteratorObject
									.next();
							projectExpense = new ProjectExpense();
							lookupDetail = new LookupDetail();
							if (resourceDetail.get("expenseType").toString() != null) {
								lookupDetail.setLookupDetailId(Long
										.valueOf(resourceDetail.get(
												"expenseType").toString()));
								projectExpense.setLookupDetail(lookupDetail);
							}
							projectExpense.setExpenseMode(Byte
									.valueOf(resourceDetail.get("expenseMode")
											.toString()));
							if ((byte) projectExpense.getExpenseMode() == (byte) ProjectExpenseMode.General_Expense
									.getCode()) {
								pettyCashExpenses.add(projectExpense);
							}
							projectExpense.setAmount(Double
									.valueOf(resourceDetail
											.get("expenseAmount").toString()));
							projectExpense.setPaymentDate(DateFormat
									.convertStringToDate(resourceDetail.get(
											"expenseDate").toString()));
							if (resourceDetail.get("costToClient") != null
									&& !resourceDetail.get("costToClient")
											.toString().equals(""))
								projectExpense.setChargableAmount(Double
										.valueOf(resourceDetail.get(
												"costToClient").toString()));

							projectExpense.setReferenceNumber(resourceDetail
									.get("expenseReference").toString());
							projectExpense.setDescription(resourceDetail.get(
									"description").toString());

							long projectExpenseId = 0;
							if (resourceDetail.get("projectExpenseId") != null
									&& !resourceDetail.get("projectExpenseId")
											.toString().equals(""))
								projectExpenseId = Long
										.parseLong(resourceDetail.get(
												"projectExpenseId").toString());
							if (projectExpenseId > 0) {
								projectExpense
										.setProjectExpenseId(projectExpenseId);
								editProjectExpense = projectBL
										.getProjectService()
										.getProjectExpenseById(projectExpenseId);
								projectExpense
										.setImplementation(editProjectExpense
												.getImplementation());
								projectExpense
										.setCreatedDate(editProjectExpense
												.getCreatedDate());
								projectExpense.setPerson(editProjectExpense
										.getPerson());
							} else {
								projectExpense
										.setImplementation(getImplementation());
								projectExpense.setCreatedDate(new Date());
								person = new Person();
								person.setPersonId(user.getPersonId());
								projectExpense.setPerson(person);
							}

							projectExpenses.add(projectExpense);
						}
					}
				}
				if (projectTaskId > 0) {

					deleteProjectExpenses = userDeletedExpenses(
							projectExpenses,
							projectTaskBL.getProjectTaskService()
									.getProjectTaskExpenseByTaskId(
											projectTaskId));

				}
			}

			// Inventories Coding Starts
			Object inventoryObject = parser.parse(projectInventories);
			object = (JSONArray) inventoryObject;
			ProjectInventory projectInventory = null;
			List<ProjectInventory> projectInventories = new ArrayList<ProjectInventory>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("projectInventories");
				if (null != scheduleArray && scheduleArray.size() > 0) {
					for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
							.hasNext();) {
						JSONObject resourceDetail = (JSONObject) iteratorObject
								.next();
						projectInventory = new ProjectInventory();
						lookupDetail = new LookupDetail();
						Product product = new Product();
						product.setProductId(Long.valueOf(resourceDetail.get(
								"productId").toString()));
						projectInventory.setQuantity(Double
								.valueOf(resourceDetail.get("quantity")
										.toString()));
						projectInventory.setProduct(product);
						projectInventory.setUnitRate(Double
								.valueOf(resourceDetail.get("amount")
										.toString()));
						if (resourceDetail.get("discount") != null
								&& !resourceDetail.get("discount").toString()
										.equals(""))
							projectInventory.setDiscount(Double
									.valueOf(resourceDetail.get("discount")
											.toString()));
						if (resourceDetail.get("storeId") != null
								&& !resourceDetail.get("storeId").toString()
										.equals("0")) {
							Shelf store = new Shelf();
							store.setShelfId(Integer.valueOf(resourceDetail
									.get("storeId").toString()));
							projectInventory.setShelf(store);
						}
						projectInventory.setDescription(resourceDetail.get(
								"description").toString());
						projectInventory.setImplementation(getImplementation());
						long projectInventoryId = 0;
						if (resourceDetail.get("projectInventoryId") != null
								&& !resourceDetail.get("projectInventoryId")
										.toString().equals(""))
							projectInventoryId = Long.parseLong(resourceDetail
									.get("projectInventoryId").toString());
						if (projectInventoryId > 0) {
							projectInventory
									.setProjectInventoryId(projectInventoryId);

						}

						if (resourceDetail.get("isPercentage") != null
								&& !resourceDetail.get("isPercentage")
										.toString().equals("")
								&& resourceDetail.get("isPercentage")
										.toString().equals("true")) {
							projectInventory.setIsPercentage(true);
						} else if (resourceDetail.get("isPercentage") != null
								&& !resourceDetail.get("isPercentage")
										.toString().equals("")
								&& resourceDetail.get("isPercentage")
										.toString().equals("false")) {
							projectInventory.setIsPercentage(false);
						}

						if (resourceDetail.get("isPercentage") != null
								&& !resourceDetail.get("isPercentage")
										.toString().equals("")
								&& resourceDetail.get("isPercentage")
										.toString().equals("true")) {
							projectInventory.setIsPercentage(true);
						} else if (resourceDetail.get("isPercentage") != null
								&& !resourceDetail.get("isPercentage")
										.toString().equals("")
								&& resourceDetail.get("isPercentage")
										.toString().equals("false")) {
							projectInventory.setIsPercentage(false);
						}

						if (resourceDetail.get("projectInventoryStatus") != null
								&& !resourceDetail
										.get("projectInventoryStatus")
										.toString().equals("")) {
							projectInventory.setStatus(Byte
									.valueOf(resourceDetail.get(
											"projectInventoryStatus")
											.toString()));
						}
						projectInventories.add(projectInventory);
					}
				}
			}
			List<ProjectInventory> deleteProjectInvenotries = null;
			List<ProjectInventory> editInventories = null;

			if (projectTaskId > 0) {
				editInventories = projectTaskBL.getProjectTaskService()
						.getProjectTaskInventoriesByTaskId(projectTaskId);
				deleteProjectInvenotries = userDeletedInventories(
						projectInventories, editInventories);

			}
			isStockUpdate = true;
			if (saveType != null && saveType.equalsIgnoreCase("SAVE"))
				isStockUpdate = false;
			if (saveType != null && saveType.equalsIgnoreCase("REQUISITION"))
				isStockUpdate = false;
			ProjectTaskVO vo = new ProjectTaskVO();

			if (null != pettyCashExpenses && pettyCashExpenses.size() > 0) {
				if (pettyCashId > 0) {
					PettyCash cash = new PettyCash();
					cash.setPettyCashId(pettyCashId);
					projectTask.setPettyCash(cash);
				} else {
					returnMessage = "Please select a pettycash account.";
					return SUCCESS;
				}
			}

			ProjectTask tempTask = projectTaskBL.getProjectTaskService()
					.getProjectTaskById(projectTask.getProjectTaskId());
			vo.setProjectTask(tempTask);

			// petty cash work
			if (pettyCashId > 0 && null != pettyCashExpenses
					&& pettyCashExpenses.size() > 0) {
				PettyCash voucher = projectBL.getPettyCashBL()
						.getPettyCashService().getPettyCashById(pettyCashId);
				pettyCashExpense = new PettyCash();
				BeanUtils.copyProperties(pettyCashExpense, voucher);
				pettyCashExpense.setPettyCashType((byte) 3);
				pettyCashExpense.setPettyCashId(null);
				pettyCashExpense.setPettyCash(voucher);
				double cashExpense = 0;
				for (ProjectExpense expense : pettyCashExpenses) {
					cashExpense += expense.getAmount();
				}
				pettyCashExpense.setCashOut(cashExpense);
				pettyCashExpense.setExpenseAmount(cashExpense);
				pettyCashExpense.setCashIn(null);
				Combination combination = new Combination();
				combination.setCombinationId(projectBL.getImplementation()
						.getExpenseAccount());
				pettyCashExpense.setCombination(combination);
				pettyCashExpense.setVoucherDate(Calendar.getInstance()
						.getTime());
				pettyCashExpense.setRecordId(projectId);
				pettyCashExpense.setUseCase(Project.class.getSimpleName());
				pettyCashExpense.setPettyCashNo(SystemBL.getReferenceStamp(
						PettyCash.class.getName(),
						projectBL.getImplementation()));
			}
			projectTaskBL.saveProjectTask(projectTask, projectResources,
					projectResourceDetails, projectResourceDetailsOld,
					deleteResources, projectExpenses, deleteProjectExpenses,
					projectInventories, deleteProjectInvenotries,
					editInventories, isStockUpdate, saveType, vo,
					pettyCashExpense);
			returnMessage = "SUCCESS";
			projectTaskId = projectTask.getProjectTaskId();
			log.info("Module: Accounts : Method: saveProjectTask: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveProjectTask Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<ProjectResource> userDeletedResource(
			List<ProjectResource> projectResources,
			List<ProjectResource> projectResourcOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProjectResource> hashResources = new HashMap<Long, ProjectResource>();
		if (null != projectResourcOld && projectResourcOld.size() > 0) {
			for (ProjectResource list : projectResourcOld) {
				listOne.add(list.getProjectResourceId());
				hashResources.put(list.getProjectResourceId(), list);
			}
			for (ProjectResource list : projectResources) {
				listTwo.add(list.getProjectResourceId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProjectResource> resourceList = new ArrayList<ProjectResource>();
		if (null != different && different.size() > 0) {
			ProjectResource tempResource = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempResource = new ProjectResource();
					tempResource = hashResources.get(list);
					resourceList.add(tempResource);
				}
			}
		}
		return resourceList;
	}

	private List<ProjectExpense> userDeletedExpenses(
			List<ProjectExpense> projectResources,
			List<ProjectExpense> projectResourcOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProjectExpense> hashResources = new HashMap<Long, ProjectExpense>();
		if (null != projectResourcOld && projectResourcOld.size() > 0) {
			for (ProjectExpense list : projectResourcOld) {
				listOne.add(list.getProjectExpenseId());
				hashResources.put(list.getProjectExpenseId(), list);
			}
			for (ProjectExpense list : projectResources) {
				listTwo.add(list.getProjectExpenseId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProjectExpense> resourceList = new ArrayList<ProjectExpense>();
		if (null != different && different.size() > 0) {
			ProjectExpense tempResource = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempResource = new ProjectExpense();
					tempResource = hashResources.get(list);
					resourceList.add(tempResource);
				}
			}
		}
		return resourceList;
	}

	private List<ProjectInventory> userDeletedInventories(
			List<ProjectInventory> projectResources,
			List<ProjectInventory> projectResourcOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProjectInventory> hashResources = new HashMap<Long, ProjectInventory>();
		if (null != projectResourcOld && projectResourcOld.size() > 0) {
			for (ProjectInventory list : projectResourcOld) {
				listOne.add(list.getProjectInventoryId());
				hashResources.put(list.getProjectInventoryId(), list);
			}
			for (ProjectInventory list : projectResources) {
				listTwo.add(list.getProjectInventoryId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProjectInventory> resourceList = new ArrayList<ProjectInventory>();
		if (null != different && different.size() > 0) {
			ProjectInventory tempResource = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempResource = new ProjectInventory();
					tempResource = hashResources.get(list);
					resourceList.add(tempResource);
				}
			}
		}
		return resourceList;
	}

	public String deleteProjectTask() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProjectTask");
			projectTaskBL.deleteProjectTask(projectTaskBL
					.getProjectTaskService().getProjectTaskById(projectTaskId));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProjectTask: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteProjectTask Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getAllDiscussions() {
		try {
			log.info("Inside Module: Accounts : Method: getAllDiscussions");
			Map<String, List<ProjectDiscussionVO>> projectDisucssionMaps = new HashMap<String, List<ProjectDiscussionVO>>();

			List<ProjectDiscussionVO> projectDisucssionVOs = projectTaskBL
					.getAllProjectDiscussions(null, projectTaskId);
			Collections.sort(projectDisucssionVOs,
					new Comparator<ProjectDiscussionVO>() {
						public int compare(ProjectDiscussionVO m1,
								ProjectDiscussionVO m2) {
							return m2.getCreatedDate().compareTo(
									m1.getCreatedDate());
						}
					});
			List<ProjectDiscussionVO> tempDisucssionVOs = null;
			for (ProjectDiscussionVO projectDiscussionVO : projectDisucssionVOs) {
				if (projectDisucssionMaps.containsKey(projectDiscussionVO
						.getDate())) {
					tempDisucssionVOs = projectDisucssionMaps
							.get(projectDiscussionVO.getDate());
					tempDisucssionVOs.add(projectDiscussionVO);
					projectDisucssionMaps.put(projectDiscussionVO.getDate(),
							tempDisucssionVOs);
				} else {
					tempDisucssionVOs = new ArrayList<ProjectDiscussionVO>();
					tempDisucssionVOs.add(projectDiscussionVO);
					projectDisucssionMaps.put(projectDiscussionVO.getDate(),
							tempDisucssionVOs);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"DISCUSSION_MAP_LIST", projectDisucssionMaps);
			log.info("Module: Accounts : Method: getAllDiscussions Success ");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: getAllDiscussions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Project Discussion
	public String saveProjectDiscussion() {
		try {
			log.info("Inside Module: Accounts : Method: saveProjectDiscussion");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			ProjectDiscussion projectDiscussion = new ProjectDiscussion();
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<ProjectTaskSurrogate> ptsList = null;
			ProjectTask projectTask = null;
			if (projectTaskId > 0) {
				projectTask = projectTaskBL.getProjectTaskService()
						.getProjectTaskById(projectTaskId);
				ptsList = new ArrayList<ProjectTaskSurrogate>(
						projectTask.getProjectTaskSurrogates());
				if (ptsList != null)
					projectDiscussion.setProjectTaskSurrogate(ptsList.get(0));
			} else {
				returnMessage = "ERROR";
				return ERROR;
			}
			projectDiscussion.setMessage(message);
			projectDiscussion.setCreatedDate(new Date());
			projectDiscussion.setPerson(person);
			projectDiscussion.setImplementation(getImplementation());
			projectBL.saveProjectDiscussion(projectDiscussion);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProjectDiscussion: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveProjectDiscussion Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getAllNotes() {
		try {
			log.info("Inside Module: Accounts : Method: getAllNotes");

			List<ProjectNotesVO> projectNotesVOs = projectTaskBL
					.getAllProjectNotes(null, projectTaskId);
			Collections.sort(projectNotesVOs, new Comparator<ProjectNotesVO>() {
				public int compare(ProjectNotesVO m1, ProjectNotesVO m2) {
					return m2.getCreatedDate().compareTo(m1.getCreatedDate());
				}
			});

			List<ProjectNotesVO> projectNotesVOsTemp = new ArrayList<ProjectNotesVO>();
			projectNotesVOsTemp.addAll(projectNotesVOs);
			for (ProjectNotesVO projectNotesVO : projectNotesVOsTemp) {
				if (projectNotesVO.getIsPrivate() != null
						&& projectNotesVO.getIsPrivate()
						&& !projectNotesVO.isSignedUser())
					projectNotesVOs.remove(projectNotesVO);
			}

			ServletActionContext.getRequest().setAttribute("NOTES_MAP_LIST",
					projectNotesVOs);
			log.info("Module: Accounts : Method: getAllNotes Success ");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: getAllNotes Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Project Notes
	public String saveProjectNotes() {
		try {
			log.info("Inside Module: Accounts : Method: saveProjectNotes");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			ProjectNotes projectNote = new ProjectNotes();
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			List<ProjectTaskSurrogate> ptsList = null;
			ProjectTask projectTask = null;
			if (projectTaskId > 0) {
				projectTask = projectTaskBL.getProjectTaskService()
						.getProjectTaskById(projectTaskId);
				ptsList = new ArrayList<ProjectTaskSurrogate>(
						projectTask.getProjectTaskSurrogates());
				if (ptsList != null)
					projectNote.setProjectTaskSurrogate(ptsList.get(0));
			} else {
				returnMessage = "ERROR";
				return ERROR;
			}
			projectNote.setMessage(message);
			projectNote.setCreatedDate(new Date());
			projectNote.setPerson(person);
			projectNote.setIsPrivate(isPublic);
			projectNote.setImplementation(getImplementation());
			projectBL.saveProjectNote(projectNote);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProjectNotes: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveProjectNotes Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getProjectType() {
		return projectType;
	}

	public void setProjectType(long projectType) {
		this.projectType = projectType;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProjectBL getProjectBL() {
		return projectBL;
	}

	public void setProjectBL(ProjectBL projectBL) {
		this.projectBL = projectBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getProjectResources() {
		return projectResources;
	}

	public void setProjectResources(String projectResources) {
		this.projectResources = projectResources;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public ProjectTaskBL getProjectTaskBL() {
		return projectTaskBL;
	}

	public void setProjectTaskBL(ProjectTaskBL projectTaskBL) {
		this.projectTaskBL = projectTaskBL;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public long getProjectTaskId() {
		return projectTaskId;
	}

	public void setProjectTaskId(long projectTaskId) {
		this.projectTaskId = projectTaskId;
	}

	public String getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(String actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public String getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(String actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public Long getProjectMileStoneId() {
		return projectMileStoneId;
	}

	public void setProjectMileStoneId(Long projectMileStoneId) {
		this.projectMileStoneId = projectMileStoneId;
	}

	public Long getParentProjectTaskId() {
		return parentProjectTaskId;
	}

	public void setParentProjectTaskId(Long parentProjectTaskId) {
		this.parentProjectTaskId = parentProjectTaskId;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public Byte getPriority() {
		return priority;
	}

	public void setPriority(Byte priority) {
		this.priority = priority;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public boolean isStockUpdate() {
		return isStockUpdate;
	}

	public void setStockUpdate(boolean isStockUpdate) {
		this.isStockUpdate = isStockUpdate;
	}

	public String getSaveType() {
		return saveType;
	}

	public void setSaveType(String saveType) {
		this.saveType = saveType;
	}

	public String getProjectExpenses() {
		return projectExpenses;
	}

	public void setProjectExpenses(String projectExpenses) {
		this.projectExpenses = projectExpenses;
	}

	public String getProjectInventories() {
		return projectInventories;
	}

	public void setProjectInventories(String projectInventories) {
		this.projectInventories = projectInventories;
	}

	public long getPettyCashId() {
		return pettyCashId;
	}

	public void setPettyCashId(long pettyCashId) {
		this.pettyCashId = pettyCashId;
	}
}
