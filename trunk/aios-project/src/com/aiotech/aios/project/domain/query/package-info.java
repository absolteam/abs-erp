@org.hibernate.annotations.NamedQueries({

		@org.hibernate.annotations.NamedQuery(name = "getAllProjects", query = "SELECT DISTINCT(p) FROM Project p"
				+ " LEFT JOIN FETCH p.lookupDetail prjType"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH p.personByProjectLead pr"
				+ " WHERE p.implementation = ? AND p.project IS NULL ORDER BY p.projectId DESC "),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProjects", query = "SELECT DISTINCT(p) FROM Project p"
				+ " LEFT JOIN FETCH p.lookupDetail prjType"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH p.personByProjectLead pr"
				+ " LEFT JOIN FETCH p.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " LEFT JOIN FETCH p.projectMileStones pms"
				+ " LEFT JOIN FETCH p.projectTasks pt"
				+ " WHERE p.implementation = ?"
				+ " AND p.projectStatus != ? AND p.project IS NULL ORDER BY p.projectId DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProjectsForSecurityDeposit", query = "SELECT DISTINCT(p) FROM Project p"
				+ " LEFT JOIN FETCH p.lookupDetail prjType"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH p.personByProjectLead pr"
				+ " WHERE  p.projectStatus != ? AND p.depositDate IS NOT NULL AND p.securityDeposit IS NOT NULL AND p.project IS NULL "),

		@org.hibernate.annotations.NamedQuery(name = "getProjectById", query = "SELECT DISTINCT(p) FROM Project p"
				+ " LEFT JOIN FETCH p.lookupDetail prjType"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH cst.combination combination"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH p.personByProjectLead pr"
				+ " LEFT JOIN FETCH p.creditTerm terms"
				+ " LEFT JOIN FETCH p.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH pts.projectDiscussions prDisc"
				+ " LEFT JOIN FETCH pts.projectNoteses notes"
				+ " LEFT JOIN FETCH pts.projectExpenses proExpe"
				+ " LEFT JOIN FETCH pts.projectInventories pInvent"
				+ " LEFT JOIN FETCH pts.projectPaymentSchedules paymentSch"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " LEFT JOIN FETCH rsrd.person rsrpr"
				+ " LEFT JOIN FETCH p.projectMileStones pms"
				+ " LEFT JOIN FETCH p.supplier supplier"
				+ " LEFT JOIN FETCH supplier.person supperson"
				+ " LEFT JOIN FETCH supplier.company supcompany"
				+ " LEFT JOIN FETCH p.projects subProjects"
				+ " LEFT JOIN FETCH p.pettyCash pcash"
				+ " WHERE p.projectId = ?"),
				
			@org.hibernate.annotations.NamedQuery(name = "getProjectForHistory", query = "SELECT DISTINCT(p) FROM Project p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH pts.projectDiscussions prDisc"
				+ " LEFT JOIN FETCH pts.projectNoteses notes"
				+ " LEFT JOIN FETCH pts.projectExpenses proExpe"
				+ " LEFT JOIN FETCH pts.projectInventories pInvent"
				+ " LEFT JOIN FETCH pts.projectPaymentSchedules paymentSch"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " LEFT JOIN FETCH p.projectMileStones pms"
				+ " LEFT JOIN FETCH pts.projectDeliveries pdelivey"
				+ " WHERE p.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBasicProjectById", query = "SELECT DISTINCT(p) FROM Project p"
				+ " LEFT JOIN FETCH p.lookupDetail prjType"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH p.personByProjectLead pr"
				+ " LEFT JOIN FETCH p.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectDiscussions prDisc"
				+ " LEFT JOIN FETCH pts.projectNoteses notes"
				+ " LEFT JOIN FETCH pts.projectExpenses proExpe"
				+ " LEFT JOIN FETCH pts.projectPaymentSchedules paymentSch"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " LEFT JOIN FETCH pts.projectPaymentSchedules pps"
				+ " LEFT JOIN FETCH pts.projectInventories pInvent"
				+ " LEFT JOIN FETCH pts.projectExpenses pExp"
				+ " LEFT JOIN FETCH p.projectMileStones pms"
				+ " LEFT JOIN FETCH p.projectTasks pt"
				+ " LEFT JOIN FETCH p.projects projects"
				+ " WHERE p.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectResourceDetailById", query = "SELECT DISTINCT(p) FROM ProjectResourceDetail p"
				+ " JOIN FETCH p.projectResource rsr"
				+ " WHERE rsr.projectResourceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectResourcByProjectId", query = "SELECT DISTINCT(p) FROM ProjectResource p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProjectMileStones", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " JOIN FETCH p.project prj"
				+ " WHERE p.implementation = ?"
				+ " AND p.status != ?" + " AND p.status != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProjectMileStonesByProject", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " LEFT JOIN FETCH p.projectTasks pts"
				+ " LEFT JOIN FETCH p.project prj" + " WHERE prj.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectMileStones", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " LEFT JOIN FETCH p.projectTasks pts"
				+ " LEFT JOIN FETCH p.project prj" + " WHERE p.implementation = ? AND prj.project IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectMileStonesByEndDate", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " WHERE p.endDate = ?"
				+ " AND p.status != ?"
				+ " AND p.status != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectPaymentsByPaymentDate", query = "SELECT DISTINCT(p) FROM ProjectPaymentDetail p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pro.projectMileStones pms"
				+ " WHERE p.paymentDate = ?"
				+ " AND pms.status != ?"
				+ " AND pms.status != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectMileStoneByProject", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " JOIN FETCH p.project prj" + " WHERE prj.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectMileStoneById", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " LEFT JOIN FETCH p.project prj"
				+ " LEFT JOIN FETCH prj.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectPaymentDetails pay"
				+ " WHERE p.projectMileStoneId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectPaymentDetailById", query = "SELECT DISTINCT(p) FROM ProjectPaymentDetail p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pro.projectMileStones pms"
				+ " JOIN FETCH pro.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " JOIN FETCH cst.combination cbi"
				+ " JOIN FETCH cbi.accountByNaturalAccountId nacc"
				+ " JOIN FETCH cbi.accountByAnalysisAccountId acc"
				+ " WHERE p.projectPaymentDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectPaymentDetailByMileStone", query = "SELECT DISTINCT(p) FROM ProjectPaymentDetail p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pro.projectMileStones pms"
				+ " WHERE pms.projectMileStoneId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectDeliveries", query = "SELECT DISTINCT(pd) FROM ProjectDelivery pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " WHERE pd.implementation = ? AND pro.project IS NULL ORDER BY pd.projectDeliveryId DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectDeliveryById", query = "SELECT DISTINCT(pd) FROM ProjectDelivery pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " LEFT JOIN FETCH ptask.project proParent"
				+ " WHERE pd.projectDeliveryId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getProjectDeliveryDetailById", query = "SELECT DISTINCT(pd) FROM ProjectDelivery pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pro.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId pr"
				+ " LEFT JOIN FETCH cst.company cst" 
				+ " LEFT JOIN FETCH pts.projectPaymentSchedules pps"
				+ " WHERE pd.projectDeliveryId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDeliveriesByTask", query = "SELECT DISTINCT(pd) FROM ProjectDelivery pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " WHERE ptask.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDeliveriesByProject", query = "SELECT DISTINCT(pd) FROM ProjectDelivery pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectMileStoneByProjectId", query = "SELECT DISTINCT(p) FROM ProjectMileStone p"
				+ " LEFT JOIN FETCH p.project pro" + " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectDiscussionByProject", query = "SELECT DISTINCT(pd) FROM ProjectDiscussion pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " LEFT JOIN FETCH pd.person person"
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectDiscussionByTask", query = "SELECT DISTINCT(pd) FROM ProjectDiscussion pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " LEFT JOIN FETCH pd.person person"
				+ " WHERE ptask.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectTaskById", query = "SELECT DISTINCT(pt) FROM ProjectTask pt"
				+ " LEFT JOIN FETCH pt.projectTask ptParent"
				+ " LEFT JOIN FETCH pt.projectMileStone pms"
				+ " LEFT JOIN FETCH pt.project project"
				+ " LEFT JOIN FETCH project.customer custo"
				+ " LEFT JOIN FETCH pt.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectDeliveries pds"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH pts.projectDiscussions prDisc"
				+ " LEFT JOIN FETCH pts.projectNoteses notes"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " LEFT JOIN FETCH rsrd.person resourcePerson"
				+ " LEFT JOIN FETCH pt.supplier supplier"
				+ " LEFT JOIN FETCH supplier.person supperson"
				+ " LEFT JOIN FETCH supplier.company supcompany"
				+ " LEFT JOIN FETCH pt.projectTasks subList"
				+ " LEFT JOIN FETCH pt.pettyCash pcash"
				+ " WHERE pt.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectNotesByProject", query = "SELECT DISTINCT(pd) FROM ProjectNotes pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " LEFT JOIN FETCH pd.person person"
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectNotesByTask", query = "SELECT DISTINCT(pd) FROM ProjectNotes pd"
				+ " LEFT JOIN FETCH pd.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pts.projectTask ptask"
				+ " LEFT JOIN FETCH pd.person person"
				+ " WHERE ptask.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectPaymentScheduleByProjectId", query = "SELECT DISTINCT(p) FROM ProjectPaymentSchedule p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH p.lookupDetail lookupDetail"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectExpenseByProjectId", query = "SELECT DISTINCT(p) FROM ProjectExpense p"
				+ " LEFT JOIN FETCH p.lookupDetail lookupDetail"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectInventoriesByProjectId", query = "SELECT DISTINCT(pi) FROM ProjectInventory pi"
				+ " LEFT JOIN FETCH pi.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.project pro"
				+ " LEFT JOIN FETCH pi.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit unit"
				+ " LEFT JOIN FETCH pi.shelf parentShelf "
				+ " LEFT JOIN FETCH parentShelf.shelf parentRack "
				+ " LEFT JOIN FETCH parentRack.aisle parentAsile "
				+ " LEFT JOIN FETCH parentAsile.store parentStore "
				+ " WHERE pro.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectTaskExpenseByTaskId", query = "SELECT DISTINCT(p) FROM ProjectExpense p"
				+ " LEFT JOIN FETCH p.lookupDetail lookupDetail"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.projectTask task"
				+ " WHERE task.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectTaskInventoriesByTaskId", query = "SELECT DISTINCT(pi) FROM ProjectInventory pi"
				+ " LEFT JOIN FETCH pi.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.projectTask task"
				+ " LEFT JOIN FETCH pi.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit unit"
				+ " LEFT JOIN FETCH pi.shelf parentShelf "
				+ " LEFT JOIN FETCH parentShelf.shelf parentRack "
				+ " LEFT JOIN FETCH parentRack.aisle parentAsile "
				+ " LEFT JOIN FETCH parentAsile.store parentStore "
				+ " WHERE task.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProjectResourcByProjectTaskId", query = "SELECT DISTINCT(p) FROM ProjectResource p"
				+ " LEFT JOIN FETCH p.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pts.projectTask proT"
				+ " WHERE proT.projectTaskId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectTasks", query = "SELECT DISTINCT(pt) FROM ProjectTask pt"
				+ " LEFT JOIN FETCH pt.projectTask ptParent"
				+ " LEFT JOIN FETCH pt.supplier supplier"
				+ " LEFT JOIN FETCH supplier.person supperson"
				+ " LEFT JOIN FETCH supplier.company supcompany"
				+ " LEFT JOIN FETCH pt.projectMileStone pms"
				+ " LEFT JOIN FETCH pt.project project"
				+ " LEFT JOIN FETCH project.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH pt.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectDeliveries pds"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " WHERE pt.implementation = ? ORDER BY pt.projectTaskId DESC "),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectTaskByProjectId", query = "SELECT DISTINCT(pt) FROM ProjectTask pt"
				+ " LEFT JOIN FETCH pt.projectTask ptParent"
				+ " LEFT JOIN FETCH pt.supplier supplier"
				+ " LEFT JOIN FETCH supplier.person supperson"
				+ " LEFT JOIN FETCH supplier.company supcompany"
				+ " LEFT JOIN FETCH pt.projectMileStone pms"
				+ " LEFT JOIN FETCH pt.project project"
				+ " LEFT JOIN FETCH project.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH pt.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectInventories ptsinv"
				+ " LEFT JOIN FETCH pts.projectDeliveries pds"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " LEFT JOIN FETCH pt.projectTasks subTasks"
				+ " LEFT JOIN FETCH subTasks.projectTaskSurrogates subpts"
				+ " LEFT JOIN FETCH subpts.projectResources subrsr"
				+ " LEFT JOIN FETCH subrsr.projectResourceDetails subrsrd"
				+ " WHERE project.projectId = ? ORDER BY pt.projectTaskId DESC "),

		@org.hibernate.annotations.NamedQuery(name = "getAllProjectTaskByParentTaskId", query = "SELECT DISTINCT(pt) FROM ProjectTask pt"
				+ " LEFT JOIN FETCH pt.projectTask ptParent"
				+ " LEFT JOIN FETCH pt.supplier supplier"
				+ " LEFT JOIN FETCH supplier.person supperson"
				+ " LEFT JOIN FETCH supplier.company supcompany"
				+ " LEFT JOIN FETCH pt.projectMileStone pms"
				+ " LEFT JOIN FETCH pt.project project"
				+ " LEFT JOIN FETCH project.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH pt.projectTaskSurrogates pts"
				+ " LEFT JOIN FETCH pts.projectDeliveries pds"
				+ " LEFT JOIN FETCH pts.projectResources rsr"
				+ " LEFT JOIN FETCH rsr.projectResourceDetails rsrd"
				+ " WHERE ptParent.projectTaskId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getProjectPaymentScheduleById", query = "SELECT DISTINCT(pps) FROM ProjectPaymentSchedule pps"
				+ " LEFT JOIN FETCH pps.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pps.lookupDetail look"
				+ " LEFT JOIN FETCH pts.project project"
				+ " LEFT JOIN FETCH project.customer cst"
				+ " LEFT JOIN FETCH cst.combination comb"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " WHERE pps.projectPaymentScheduleId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getProjectPaymentScheduleForScheduler", query = "SELECT DISTINCT(pps) FROM ProjectPaymentSchedule pps"
				+ " LEFT JOIN FETCH pps.projectTaskSurrogate pts"
				+ " LEFT JOIN FETCH pps.lookupDetail look"
				+ " LEFT JOIN FETCH pts.project project"
				+ " LEFT JOIN FETCH project.customer cst"
				+ " LEFT JOIN FETCH cst.combination comb"
				+ " LEFT JOIN FETCH cst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH cst.company cmp"
				+ " LEFT JOIN FETCH pps.implementation implementation"
				+ " WHERE (pps.status = 1 OR pps.status = 0) AND project.project IS NULL "),

})
package com.aiotech.aios.project.domain.query;