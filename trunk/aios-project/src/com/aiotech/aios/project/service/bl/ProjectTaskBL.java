package com.aiotech.aios.project.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.CreditTermBL;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.service.bl.MaterialRequisitionBL;
import com.aiotech.aios.accounts.service.bl.PettyCashBL;
import com.aiotech.aios.accounts.service.bl.ProductBL;
import com.aiotech.aios.accounts.service.bl.SalesInvoiceBL;
import com.aiotech.aios.accounts.service.bl.StockBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.bl.JobAssignmentBL;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDiscussion;
import com.aiotech.aios.project.domain.entity.ProjectExpense;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectNotes;
import com.aiotech.aios.project.domain.entity.ProjectResource;
import com.aiotech.aios.project.domain.entity.ProjectResourceDetail;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.project.domain.entity.vo.ProjectDiscussionVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectExpenseVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectInventoryVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectMileStoneVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectNotesVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectResourceVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectTaskVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectVO;
import com.aiotech.aios.project.service.ProjectService;
import com.aiotech.aios.project.service.ProjectTaskService;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;

public class ProjectTaskBL {

	// Dependencies
	private ProjectTaskService projectTaskService;
	private LookupMasterBL lookupMasterBL;
	private CreditTermBL creditTermBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private DirectPaymentBL directPaymentBL;
	private ProjectService projectService;
	private ProjectDeliveryBL projectDeliveryBL;
	private StockBL stockBL;
	private TransactionBL transactionBL;
	private ProductBL productBL;
	private MaterialRequisitionBL materialRequisitionBL;
	private JobAssignmentBL jobAssignmentBL;
	private SalesInvoiceBL salesInvoiceBL;
	private AlertBL alertBL;
	private PettyCashBL pettyCashBL;

	public ProjectTaskService getProjectTaskService() {
		return projectTaskService;
	}

	public void setProjectTaskService(ProjectTaskService projectTaskService) {
		this.projectTaskService = projectTaskService;
	}

	public List<Object> showAllProjectTask(Long projectId) throws Exception {
		List<ProjectTask> projectTasks = null;
		if (projectId != null && projectId > 0) {
			projectTasks = projectTaskService
					.getAllProjectTaskByProjectId(projectId);
		} else {
			projectTasks = projectTaskService
					.getAllProjectTasks(getImplementation());
		}
		List<Object> projectTaskVOs = new ArrayList<Object>();
		if (null != projectTasks && projectTasks.size() > 0) {
			for (ProjectTask projectTask : projectTasks)
				projectTaskVOs.add(projectTaskVOForJSON(projectTask));
		}
		return projectTaskVOs;
	}

	public ProjectTaskVO projectTaskVOForJSON(ProjectTask projectTask) {
		ProjectTaskVO projectTaskVO = new ProjectTaskVO();
		projectTaskVO.setProjectTaskId(projectTask.getProjectTaskId());
		projectTaskVO.setReferenceNumber(projectTask.getReferenceNumber());
		if (projectTask.getProject() != null)
			projectTaskVO.setProjectTitle(projectTask.getProject()
					.getProjectTitle());

		if (projectTask.getProjectTask() != null)
			projectTaskVO.setParentTaskTitle(projectTask.getProjectTask()
					.getTaskTitle());

		projectTaskVO.setTaskTitle(projectTask.getTaskTitle());
		projectTaskVO.setFromDate(DateFormat.convertDateToString(projectTask
				.getStartDate().toString()));
		if (null != projectTask.getEndDate())
			projectTaskVO.setToDate(DateFormat.convertDateToString(projectTask
					.getEndDate().toString()));

		projectTaskVO.setCurrentStatus(ProjectStatus.get(
				projectTask.getStatus()).name());

		if (projectTask.getProjectMileStone() != null) {
			projectTaskVO.setMileStoneTitle(projectTask.getProjectMileStone()
					.getTitle());
		}
		projectTaskVO.setEditFlag(true);
		if (projectTask.getStatus() != null
				&& (byte) projectTask.getStatus() == (byte) ProjectStatus.Closed
						.getCode())
			projectTaskVO.setEditFlag(false);

		return projectTaskVO;
	}

	public List<ProjectTaskVO> showAllProjectTaskByParentTask(Long projectTaskId)
			throws Exception {
		List<ProjectTask> projectTasks = null;
		if (projectTaskId != null && projectTaskId > 0) {
			projectTasks = projectTaskService
					.getAllProjectTaskByParentTaskId(projectTaskId);
		} else {
			projectTasks = projectTaskService
					.getAllProjectTasks(getImplementation());
		}
		List<ProjectTaskVO> projectTaskVOs = new ArrayList<ProjectTaskVO>();
		if (null != projectTasks && projectTasks.size() > 0) {
			for (ProjectTask projectTask : projectTasks)
				projectTaskVOs.add(convertProjectTaskToVO(projectTask, null));
		}
		return projectTaskVOs;
	}

	public ProjectTaskVO addProjectTaskVO(ProjectTask projectTask) {
		ProjectTaskVO projectTaskVO = new ProjectTaskVO();
		projectTaskVO.setProjectTitle(projectTask.getProject()
				.getProjectTitle());
		projectTaskVO.setProjectTaskId(projectTask.getProjectTaskId());
		projectTaskVO.setReferenceNumber(projectTask.getReferenceNumber());
		projectTaskVO.setTaskTitle(projectTask.getTaskTitle());
		projectTaskVO.setFromDate(DateFormat.convertDateToString(projectTaskVO
				.getStartDate().toString()));
		if (null != projectTask.getEndDate())
			projectTaskVO.setToDate(DateFormat.convertDateToString(projectTask
					.getEndDate().toString()));

		projectTaskVO.setCurrentStatus(ProjectStatus.get(
				projectTask.getStatus()).name());

		return projectTaskVO;
	}

	public ProjectTaskVO convertProjectTaskToVO(ProjectTask projectTask,
			List<ProjectTaskVO> parentVOs) {
		ProjectTaskVO projectTaskVO = new ProjectTaskVO();
		try {
			if (projectTask.getProjectTasks() != null
					&& projectTask.getProjectTasks().size() > 0
					&& parentVOs != null) {
				for (ProjectTask subTask : projectTask.getProjectTasks()) {
					parentVOs.add(convertProjectTaskToVO(projectTaskService
							.getProjectTaskById(subTask.getProjectTaskId()),
							parentVOs));
				}

			}
			BeanUtils.copyProperties(projectTaskVO, projectTask);
			projectTaskVO
					.setFromDate(DateFormat.convertDateToString(projectTask
							.getStartDate().toString()));
			if (null != projectTask.getEndDate())
				projectTaskVO.setToDate(DateFormat
						.convertDateToString(projectTask.getEndDate()
								.toString()));

			projectTaskVO.setCurrentStatus(ProjectStatus.get(
					projectTask.getStatus()).name());

			List<ProjectTaskSurrogate> ptslist = new ArrayList<ProjectTaskSurrogate>(
					projectTask.getProjectTaskSurrogates());
			if (ptslist != null && ptslist.size() > 0
					&& ptslist.get(0).getProjectResources() != null
					&& ptslist.get(0).getProjectResources().size() > 0) {
				List<ProjectResourceVO> projectResourceVOs = new ArrayList<ProjectResourceVO>();
				for (ProjectResource pr : ptslist.get(0).getProjectResources()) {
					projectResourceVOs.add(convertResourceToVO(pr));
				}
				projectTaskVO.setProjectResourceVOs(projectResourceVOs);

			}

			if (projectTask.getSupplier() != null
					&& projectTask.getSupplier().getCompany() != null
					&& projectTask.getSupplier().getCompany().getCompanyId() != null) {
				projectTaskVO.setSupplierName(projectTask.getSupplier()
						.getCompany().getCompanyName());
			} else if (projectTask.getSupplier() != null
					&& projectTask.getSupplier().getPerson() != null
					&& projectTask.getSupplier().getPerson().getPersonId() != null) {
				projectTaskVO.setSupplierName(projectTask.getSupplier()
						.getPerson().getFirstName()
						+ " "
						+ projectTask.getSupplier().getPerson().getLastName());
			}

			if (projectTask.getProjectTask() != null) {
				projectTaskVO.setParentTaskTitle(projectTask.getProjectTask()
						.getTaskTitle());
			}

			// Payment Expense
			List<ProjectExpenseVO> projectExpenseVOs = new ArrayList<ProjectExpenseVO>();
			for (ProjectExpense projectExpense : projectTaskService
					.getProjectTaskExpenseByTaskId(projectTask
							.getProjectTaskId())) {
				projectExpenseVOs.add(convertExpenseToVO(projectExpense));

			}
			projectTaskVO.setProjectExpenseVOs(projectExpenseVOs);

			// Project Inventory
			List<ProjectInventoryVO> projectInventoryVOs = new ArrayList<ProjectInventoryVO>();
			for (ProjectInventory projectInventory : projectTaskService
					.getProjectTaskInventoriesByTaskId(projectTask
							.getProjectTaskId())) {
				projectInventoryVOs.add(convertInventoryToVO(projectInventory));

			}
			projectTaskVO.setProjectInventoryVOs(projectInventoryVOs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectTaskVO;
	}

	public ProjectTaskVO convertProjectTaskToVORecursive(
			ProjectTask projectTask, ProjectTaskVO tempVO) {
		ProjectTaskVO projectTaskVO = new ProjectTaskVO();
		try {

			BeanUtils.copyProperties(projectTaskVO, projectTask);
			projectTaskVO
					.setFromDate(DateFormat.convertDateToString(projectTask
							.getStartDate().toString()));
			if (null != projectTask.getEndDate())
				projectTaskVO.setToDate(DateFormat
						.convertDateToString(projectTask.getEndDate()
								.toString()));

			projectTaskVO.setCurrentStatus(ProjectStatus.get(
					projectTask.getStatus()).name());

			List<ProjectTaskSurrogate> ptslist = new ArrayList<ProjectTaskSurrogate>(
					projectTask.getProjectTaskSurrogates());
			if (ptslist != null && ptslist.size() > 0
					&& ptslist.get(0).getProjectResources() != null
					&& ptslist.get(0).getProjectResources().size() > 0) {
				List<ProjectResourceVO> projectResourceVOs = new ArrayList<ProjectResourceVO>();
				for (ProjectResource pr : ptslist.get(0).getProjectResources()) {
					projectResourceVOs.add(convertResourceToVO(pr));
				}
				projectTaskVO.setProjectResourceVOs(projectResourceVOs);

			}

			if (projectTask.getSupplier() != null
					&& projectTask.getSupplier().getCompany() != null
					&& projectTask.getSupplier().getCompany().getCompanyId() != null) {
				projectTaskVO.setSupplierName(projectTask.getSupplier()
						.getCompany().getCompanyName());
			} else if (projectTask.getSupplier() != null
					&& projectTask.getSupplier().getPerson() != null
					&& projectTask.getSupplier().getPerson().getPersonId() != null) {
				projectTaskVO.setSupplierName(projectTask.getSupplier()
						.getPerson().getFirstName()
						+ " "
						+ projectTask.getSupplier().getPerson().getLastName());
			}

			if (projectTask.getProjectTask() != null) {
				projectTaskVO.setParentTaskTitle(projectTask.getProjectTask()
						.getTaskTitle());
			}

			// Payment Expense
			List<ProjectExpenseVO> projectExpenseVOs = new ArrayList<ProjectExpenseVO>();
			for (ProjectExpense projectExpense : projectTaskService
					.getProjectTaskExpenseByTaskId(projectTask
							.getProjectTaskId())) {
				projectExpenseVOs.add(convertExpenseToVO(projectExpense));

			}
			projectTaskVO.setProjectExpenseVOs(projectExpenseVOs);

			// Project Inventory
			List<ProjectInventoryVO> projectInventoryVOs = new ArrayList<ProjectInventoryVO>();
			for (ProjectInventory projectInventory : projectTaskService
					.getProjectTaskInventoriesByTaskId(projectTask
							.getProjectTaskId())) {
				projectInventoryVOs.add(convertInventoryToVO(projectInventory));

			}
			projectTaskVO.setProjectInventoryVOs(projectInventoryVOs);

			if (projectTask.getProjectTasks() != null
					&& projectTask.getProjectTasks().size() > 0
					&& projectTaskVO != null) {
				List<ProjectTaskVO> parentVOs = new ArrayList<ProjectTaskVO>();
				for (ProjectTask subTask : projectTask.getProjectTasks()) {
					parentVOs.add(convertProjectTaskToVORecursive(
							projectTaskService.getProjectTaskById(subTask
									.getProjectTaskId()), projectTaskVO));
				}
				projectTaskVO.setSubTasks(parentVOs);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectTaskVO;
	}

	// Project Milestone Entity to VO common conversion
	public ProjectMileStoneVO convertMileStoneToVO(ProjectMileStone pms) {
		ProjectMileStoneVO projectMileStoneVO = new ProjectMileStoneVO();

		try {
			BeanUtils.copyProperties(projectMileStoneVO, pms);
			projectMileStoneVO.setFromDate(DateFormat.convertDateToString(pms
					.getStartDate().toString()));
			projectMileStoneVO.setToDate(DateFormat.convertDateToString(pms
					.getEndDate().toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectMileStoneVO;
	}

	// Project Resource Entity to VO common conversion
	public ProjectResourceVO convertResourceToVO(ProjectResource pr) {
		ProjectResourceVO projectResourceVO = new ProjectResourceVO();

		try {
			BeanUtils.copyProperties(projectResourceVO, pr);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectResourceVO;
	}

	// Project Expense Entity to VO common conversion
	public ProjectExpenseVO convertExpenseToVO(ProjectExpense pr) {
		ProjectExpenseVO projectResourceVO = new ProjectExpenseVO();

		try {
			BeanUtils.copyProperties(projectResourceVO, pr);
			projectResourceVO.setPaymentDateView(DateFormat
					.convertDateToString(pr.getPaymentDate().toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectResourceVO;
	}

	// Project Inventory Entity to VO common conversion
	public ProjectInventoryVO convertInventoryToVO(ProjectInventory pr) {
		ProjectInventoryVO projectInventoryVO = new ProjectInventoryVO();

		try {
			BeanUtils.copyProperties(projectInventoryVO, pr);
			if (pr.getIsPercentage() == null && pr.getDiscount() != null
					&& pr.getDiscount() > 0)
				pr.setIsPercentage(false);

			projectInventoryVO.setIsPercentage(pr.getIsPercentage());
			double discount = pr.getDiscount() != null ? pr.getDiscount() : 0.0;
			if (projectInventoryVO.getIsPercentage() != null
					&& projectInventoryVO.getIsPercentage()) {
				discount = (pr.getQuantity() * pr.getUnitRate() * pr
						.getDiscount()) / 100;
			}

			projectInventoryVO.setTotalAmount((pr.getQuantity() * pr
					.getUnitRate()) - (discount));
			projectInventoryVO.setStoreName(pr.getShelf().getShelf().getAisle()
					.getStore().getStoreName()
					+ " >> "
					+ pr.getShelf().getShelf().getAisle().getSectionName()
					+ " >> "
					+ pr.getShelf().getShelf().getName()
					+ " >> "
					+ pr.getShelf().getName());
			projectInventoryVO.setSuggestedRetailPrice(stockBL
					.getProductPricingBL().getSuggestedRetailPriceByProduct(
							pr.getProduct().getProductId()));
			if (pr.getStatus() != null && pr.getStatus() != 0)
				projectInventoryVO.setStatusName(ProjectStatus.get(
						pr.getStatus()).name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectInventoryVO;
	}

	// Save Project
	public void saveProjectTask(ProjectTask projectTask,
			List<ProjectResource> projectResources,
			List<ProjectResourceDetail> projectResourceDetails,
			List<ProjectResourceDetail> projectResourceDetailsOld,
			List<ProjectResource> deleteResources,
			List<ProjectExpense> projectExpenses,
			List<ProjectExpense> deleteProjectExpenses,
			List<ProjectInventory> projectInventories,
			List<ProjectInventory> deleteProjectInventories,
			List<ProjectInventory> editProjectInventories, boolean updateStock,
			String saveType, ProjectTaskVO vo, PettyCash pettyCashExpense)
			throws Exception {
		boolean modify = null == projectTask.getProjectTaskId() ? false : true;
		projectTask.setImplementation(getImplementation());
		ProjectTaskSurrogate pts = null;
		if (!modify)
			SystemBL.saveReferenceStamp(ProjectTask.class.getName(),
					getImplementation());
		else {
			if (null != projectResourceDetailsOld
					&& projectResourceDetailsOld.size() > 0)
				projectTaskService
						.deleteProjectResourceDetail(projectResourceDetailsOld);
			if (null != deleteResources && deleteResources.size() > 0)
				projectTaskService.deleteProjectResource(deleteResources);

			if (null != deleteProjectExpenses
					&& deleteProjectExpenses.size() > 0)
				projectService.deleteProjectExpenses(deleteProjectExpenses);

			if (null != deleteProjectInventories
					&& deleteProjectInventories.size() > 0) {

				projectService
						.deleteProjectInventories(deleteProjectInventories);
			}

			List<ProjectTaskSurrogate> ptses = new ArrayList<ProjectTaskSurrogate>(
					projectTask.getProjectTaskSurrogates());
			pts = ptses.get(0);

		}
		projectTaskService.saveProjectTask(projectTask, projectResources,
				projectResourceDetails, pts, projectExpenses,
				projectInventories);

		savProjectDocuments(projectTask, modify);

		if (saveType != null && saveType.equalsIgnoreCase("REQUISITION")) {
			if (projectInventories != null && projectInventories.size() > 0) {
				createRequisition(projectTask, projectInventories);
			}

		}

		if (vo.getAlertId() != null && vo.getAlertId() > 0) {
			SalesDeliveryNote deliveryNote = manipulateSalesDeliveryByOnlyTask(
					vo.getProjectTask(), projectInventories);
			salesInvoiceBL.getSalesDeliveryNoteBL().saveDeliveryNote(
					deliveryNote,
					new ArrayList<SalesDeliveryDetail>(deliveryNote
							.getSalesDeliveryDetails()), null, null, null,
					null, null, null, null, null, null);
			Alert alert = alertBL.getAlertService().getAlertInfo(
					vo.getAlertId());
			alert.setIsActive(false);
			alertBL.commonSaveAlert(alert);

			// petty cash work
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.getPettyCashByUseCase(
							vo.getProjectTask().getProjectTaskId(),
							ProjectTask.class.getSimpleName());
			if (null != pettyCash)
				pettyCashBL.deletePettyCash(pettyCash);
			if (null != pettyCashExpense)
				pettyCashBL.savePettyCash(pettyCashExpense, null, null, null,
						null, null);
		}

		if (projectTask.getProject() != null) {
			projectTask.setProject(projectService.getProjectById(projectTask
					.getProject().getProjectId()));
		} else {
			projectTask.setProject(projectService
					.getProjectById(getProjectFromTaskRecursive(projectTask)
							.getProject().getProjectId()));

		}
		if (vo != null && vo.getProjectTask() != null) {

			// If status changed into delivered
			if ((byte) ProjectStatus.Delivered.getCode() == (byte) projectTask
					.getStatus()) {
				// Delete old saved entries
				SalesDeliveryNote toDelete = salesInvoiceBL
						.getSalesDeliveryNoteBL()
						.getSalesDeliveryNoteService()
						.getSalesDeliveryByProject(
								vo.getProjectTask().getProjectTaskId());
				if (toDelete != null)
					salesInvoiceBL.getSalesDeliveryNoteBL()
							.deleteSalesDeliveryNoteDirect(toDelete);

				// Save New one
				SalesDeliveryNote deliveryNote = manipulateSalesDelivery(projectTask);
				salesInvoiceBL.getSalesDeliveryNoteBL().saveDeliveryNote(
						deliveryNote,
						new ArrayList<SalesDeliveryDetail>(deliveryNote
								.getSalesDeliveryDetails()), null, null, null,
						null, null, null, null, null, null);
			}

			if ((byte) ProjectStatus.Requested.getCode() == (byte) projectTask
					.getStatus()) {
				// Delete old saved entries
				SalesOrder toDelete = salesInvoiceBL
						.getSalesDeliveryNoteBL()
						.getSalesOrderBL()
						.getSalesOrderService()
						.getSalesOrderByProject(
								vo.getProjectTask().getProjectTaskId());
				if (toDelete != null)
					salesInvoiceBL.getSalesDeliveryNoteBL().getSalesOrderBL()
							.deleteSalesOrderDirect(toDelete);
				// Save New one
				SalesOrder salesOrder = manipulateSalesOrder(projectTask);
				salesInvoiceBL
						.getSalesDeliveryNoteBL()
						.getSalesOrderBL()
						.saveSalesOrderDirect(
								salesOrder,
								new ArrayList<SalesOrderDetail>(salesOrder
										.getSalesOrderDetails()), null, null,
								null);
			}

		}
	}

	public SalesDeliveryNote manipulateSalesDeliveryByOnlyTask(
			ProjectTask task, List<ProjectInventory> projectInventories)
			throws Exception {
		SalesDeliveryNote salesDeliveryNote = new SalesDeliveryNote();
		salesDeliveryNote.setProjectTask(task);
		salesDeliveryNote.setReferenceNumber(salesInvoiceBL
				.getSalesDeliveryNoteBL().generateReferenceNumber());
		salesDeliveryNote.setStatusNote((byte) 1);
		salesDeliveryNote.setDeliveryDate(new Date());
		salesDeliveryNote.setCustomer(task.getProject().getCustomer());
		List<ShippingDetail> shippings = salesInvoiceBL
				.getCustomerBL()
				.getCustomerService()
				.getCustomerShippingSite(
						task.getProject().getCustomer().getCustomerId());
		if (shippings != null && shippings.size() > 0)
			salesDeliveryNote.setShippingDetail(shippings.get(0));
		salesDeliveryNote.setImplementation(getImplementation());
		salesDeliveryNote.setCurrency(transactionBL
				.getCurrency(getImplementation()));
		salesDeliveryNote.setDescription(task.getProject().getReferenceNumber()
				+ "  " + task.getProject().getProjectTitle());
		salesDeliveryNote.setStatus(O2CProcessStatus.Open.getCode());

		List<SalesDeliveryDetail> deliveryDetails = new ArrayList<SalesDeliveryDetail>();
		SalesDeliveryDetail salesDeliveryDetail = null;

		for (ProjectInventory projectInventory : projectInventories) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			salesDeliveryDetail.setProduct(projectInventory.getProduct());
			List<Object> stocks = stockBL
					.showProductStockDetail(projectInventory.getProduct()
							.getProductId());
			if (stocks != null && stocks.size() > 0) {
				StockVO stok = (StockVO) stocks.get(0);
				Shelf shelf = new Shelf();
				shelf.setShelfId(stok.getShelfId());
				salesDeliveryDetail.setShelf(shelf);
			} else {
				try {

				} catch (Exception e) {
					throw new Exception("Stock not available", e);
				}
			}
			salesDeliveryDetail.setIsPercentage(projectInventory
					.getIsPercentage());
			salesDeliveryDetail.setUnitRate(projectInventory.getUnitRate());
			salesDeliveryDetail.setDiscount(projectInventory.getDiscount());
			salesDeliveryDetail.setDeliveryQuantity(projectInventory
					.getQuantity());
			salesDeliveryDetail
					.setOrderQuantity(projectInventory.getQuantity());
			salesDeliveryDetail.setPackageUnit(projectInventory.getQuantity());
			deliveryDetails.add(salesDeliveryDetail);
		}

		if (getImplementation().getServiceProduct() != null
				&& getImplementation().getServiceProduct() > 0) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			Product product = new Product();
			product = productBL.getProductService().getProductById(
					getImplementation().getServiceProduct());
			salesDeliveryDetail.setProduct(product);
			salesDeliveryDetail.setShelf(product.getShelf());
			salesDeliveryDetail.setUnitRate(0.0);
			salesDeliveryDetail.setDeliveryQuantity(1.0);
			deliveryDetails.add(salesDeliveryDetail);
		}

		salesDeliveryNote
				.setSalesDeliveryDetails(new HashSet<SalesDeliveryDetail>(
						deliveryDetails));
		return salesDeliveryNote;
	}

	public SalesDeliveryNote manipulateSalesDelivery(ProjectTask projectTask)
			throws Exception {
		SalesDeliveryNote salesDeliveryNote = new SalesDeliveryNote();
		salesDeliveryNote.setProjectTask(projectTask);
		salesDeliveryNote.setReferenceNumber(salesInvoiceBL
				.getSalesDeliveryNoteBL().generateReferenceNumber());
		salesDeliveryNote.setStatusNote((byte) 1);
		salesDeliveryNote.setDeliveryDate(new Date());
		salesDeliveryNote.setCustomer(projectTask.getProject().getCustomer());
		List<ShippingDetail> shippings = salesInvoiceBL
				.getCustomerBL()
				.getCustomerService()
				.getCustomerShippingSite(
						projectTask.getProject().getCustomer().getCustomerId());
		if (shippings != null && shippings.size() > 0)
			salesDeliveryNote.setShippingDetail(shippings.get(0));
		salesDeliveryNote.setImplementation(getImplementation());
		salesDeliveryNote.setCurrency(transactionBL
				.getCurrency(getImplementation()));
		salesDeliveryNote.setDescription(projectTask.getReferenceNumber()
				+ "  " + projectTask.getTaskTitle());
		salesDeliveryNote.setStatus(O2CProcessStatus.Open.getCode());

		List<SalesDeliveryDetail> deliveryDetails = new ArrayList<SalesDeliveryDetail>();
		SalesDeliveryDetail salesDeliveryDetail = null;
		List<ProjectInventory> pInventories = new ArrayList<ProjectInventory>(
				projectTaskService
						.getProjectTaskInventoriesByTaskId(projectTask
								.getProjectTaskId()));
		for (ProjectInventory projectInventory : pInventories) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			salesDeliveryDetail.setProduct(projectInventory.getProduct());
			List<Object> stocks = stockBL
					.showProductStockDetail(projectInventory.getProduct()
							.getProductId());
			if (stocks != null && stocks.size() > 0) {
				StockVO stok = (StockVO) stocks.get(0);
				Shelf shelf = new Shelf();
				shelf.setShelfId(stok.getShelfId());
				salesDeliveryDetail.setShelf(shelf);
			} else {
				try {

				} catch (Exception e) {
					throw new Exception("Stock not available", e);
				}
			}
			salesDeliveryDetail.setIsPercentage(projectInventory
					.getIsPercentage());
			salesDeliveryDetail.setUnitRate(projectInventory.getUnitRate());
			salesDeliveryDetail.setDiscount(projectInventory.getDiscount());
			salesDeliveryDetail.setDeliveryQuantity(projectInventory
					.getQuantity());
			salesDeliveryDetail
					.setOrderQuantity(projectInventory.getQuantity());
			salesDeliveryDetail.setPackageUnit(projectInventory.getQuantity());
			deliveryDetails.add(salesDeliveryDetail);
		}

		salesDeliveryNote
				.setSalesDeliveryDetails(new HashSet<SalesDeliveryDetail>(
						deliveryDetails));
		return salesDeliveryNote;
	}

	public SalesOrder manipulateSalesOrder(ProjectTask projectTask)
			throws Exception {
		SalesOrder salesOrder = new SalesOrder();
		List<SalesOrderDetail> salesOrderDetails = new ArrayList<SalesOrderDetail>();
		salesOrder.setProjectTask(projectTask);
		salesOrder.setReferenceNumber(SystemBL.getReferenceStamp(
				SalesOrder.class.getName(), getImplementation()));
		salesOrder.setPerson(projectTask.getPerson());
		salesOrder.setOrderDate(projectTask.getStartDate());
		if (projectTask.getEndDate() == null)
			salesOrder.setExpiryDate(DateFormat.addMonth(projectTask
					.getStartDate()));
		else
			salesOrder.setExpiryDate(projectTask.getEndDate());

		Customer customer = salesInvoiceBL
				.getCustomerBL()
				.getCustomerService()
				.getCustomerDetails(
						projectTask.getProject().getCustomer().getCustomerId());
		List<ShippingDetail> shipDetails = new ArrayList<ShippingDetail>(
				customer.getShippingDetails());
		salesOrder.setShippingDetail(shipDetails.get(0));
		salesOrder.setCurrency(transactionBL.getCurrency());
		salesOrder.setImplementation(getImplementation());
		salesOrder.setCustomer(projectTask.getProject().getCustomer());
		salesOrder.setOrderStatus(O2CProcessStatus.Open.getCode());
		salesOrder.setDescription(projectTask.getProject().getReferenceNumber()
				+ " " + projectTask.getProject().getProjectTitle());

		List<ProjectTaskSurrogate> ptses = new ArrayList<ProjectTaskSurrogate>(
				projectTask.getProject().getProjectTaskSurrogates());
		ProjectTaskSurrogate pts = ptses.get(0);
		SalesOrderDetail salesOrderDetail = null;
		for (ProjectInventory projectInventory : pts.getProjectInventories()) {
			salesOrderDetail = new SalesOrderDetail();
			salesOrderDetail.setProduct(projectInventory.getProduct());
			salesOrderDetail
					.setIsPercentage(projectInventory.getIsPercentage());
			salesOrderDetail.setUnitRate(projectInventory.getUnitRate());
			salesOrderDetail.setDiscount(projectInventory.getDiscount());
			salesOrderDetail.setOrderQuantity(projectInventory.getQuantity());
			salesOrderDetail.setPackageUnit(projectInventory.getQuantity());
			salesOrderDetails.add(salesOrderDetail);
		}

		salesOrder.setSalesOrderDetails(new HashSet<SalesOrderDetail>(
				salesOrderDetails));
		return salesOrder;
	}

	public void createRequisition(ProjectTask projectTask,
			List<ProjectInventory> inventories) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		MaterialRequisition materialRequisition = new MaterialRequisition();
		List<MaterialRequisitionDetail> details = new ArrayList<MaterialRequisitionDetail>();
		materialRequisition.setRequisitionDate(new Date());
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		materialRequisition.setPerson(person);
		JobAssignment jobAssignment = jobAssignmentBL.getJobAssignmentService()
				.getJobAssignmentByEmployee(user.getPersonId());
		Store store = stockBL
				.getStoreBL()
				.getStoreService()
				.getStoreByDepartmentLocation(
						jobAssignment.getCmpDeptLocation().getCmpDeptLocId());
		materialRequisition.setStore(store);
		materialRequisition.setRequisitionStatus(RequisitionStatus.Open
				.getCode());
		materialRequisition.setUseCase(projectTask.getClass().getSimpleName());
		materialRequisition.setRecordId(projectTask.getProjectTaskId());
		materialRequisition.setCreatedDate(new Date());
		materialRequisition.setDescription(projectTask.getReferenceNumber());
		materialRequisition.setReferenceNumber(SystemBL.getReferenceStamp(
				MaterialRequisition.class.getName(),
				materialRequisitionBL.getImplementation()));

		MaterialRequisitionDetail materialRequisitionDetail = null;
		for (ProjectInventory projectInventory : inventories) {
			if (projectInventory.getStatus() == null
					|| (byte) projectInventory.getStatus() == (byte) ProjectStatus.Closed
							.getCode())
				continue;

			materialRequisitionDetail = new MaterialRequisitionDetail();
			materialRequisitionDetail.setProduct(projectInventory.getProduct());
			materialRequisitionDetail.setQuantity(projectInventory
					.getQuantity());
			materialRequisitionDetail
					.setMaterialRequisition(materialRequisition);
			details.add(materialRequisitionDetail);
			materialRequisitionDetail.setProjectInventory(projectInventory);
			projectInventory.setStatus(ProjectStatus.Closed.getCode());
		}

		if (details != null && details.size() > 0)
			materialRequisitionBL.saveMaterialRequisition(materialRequisition,
					details, null);
	}

	public void performeJVTransaction(ProjectTask projectTask,
			List<ProjectInventory> inventories) throws Exception {
		Combination combination = productBL
				.getCombinationBL()
				.getCombinationService()
				.getCombinationAllAccountDetail(
						getImplementation().getExpenseAccount());
		// Transaction
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double total = 0.0;
		for (ProjectInventory detail : inventories) {
			double discount = detail.getDiscount() != null ? detail
					.getDiscount() : 0.0;
			if (detail.getIsPercentage() != null && detail.getIsPercentage()) {
				discount = (detail.getQuantity() * detail.getUnitRate() * detail
						.getDiscount()) / 100;
			}
			transactionDetails.add(transactionBL.createTransactionDetail(
					detail.getQuantity() * detail.getUnitRate(),
					productBL.getProductInventory(
							detail.getProduct().getProductId())
							.getCombinationId(), TransactionType.Credit
							.getCode(), "", null, null));
			total += (detail.getQuantity() * detail.getUnitRate()) - (discount);
		}

		transactionDetails.add(transactionBL.createTransactionDetail(total,
				combination.getCombinationId(),
				TransactionType.Debit.getCode(), "", null, null));

		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "", new Date(), transactionBL
				.getCategory("Project Task Inventory"), projectTask
				.getProjectTaskId(), ProjectTask.class.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);

	}

	private void savProjectDocuments(ProjectTask projectTask, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(projectTask,
				"projectTaskDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, projectTask.getProjectTaskId(), "ProjectTask",
					"projectTaskDocs");
			docVO.setDirStruct(dirStucture);
		}
		documentBL.saveUploadDocuments(docVO);
	}

	// Delete Project
	public void deleteProjectTask(ProjectTask projectTask) throws Exception {
		documentBL.getDocumentService().deleteDocuments(
				documentBL.getDocumentService().getChildDocumentsByRecordId(
						projectTask.getProjectTaskId(), "ProjectTask"));
		documentBL.getDocumentService().deleteDocuments(
				documentBL.getDocumentService().getParentDocumentsByRecordId(
						projectTask.getProjectTaskId(), "ProjectTask"));
		List<ProjectResource> projectResources = null;
		List<ProjectResourceDetail> projectResourceDetails = null;

		List<ProjectDiscussion> projectDiscussions = null;
		List<ProjectNotes> projectNotes = null;
		List<ProjectTaskSurrogate> ptslist = new ArrayList<ProjectTaskSurrogate>(
				projectTask.getProjectTaskSurrogates());
		if (ptslist != null && ptslist.size() > 0
				&& ptslist.get(0).getProjectResources() != null
				&& ptslist.get(0).getProjectResources().size() > 0) {
			projectResources = new ArrayList<ProjectResource>(ptslist.get(0)
					.getProjectResources());
			projectResourceDetails = new ArrayList<ProjectResourceDetail>();
			for (ProjectResource projectResource : projectResources) {
				if (null != projectResource.getProjectResourceDetails()
						&& projectResource.getProjectResourceDetails().size() > 0) {
					projectResourceDetails
							.addAll(new ArrayList<ProjectResourceDetail>(
									projectResource.getProjectResourceDetails()));

				}
			}

			projectDiscussions = new ArrayList<ProjectDiscussion>(ptslist
					.get(0).getProjectDiscussions());
			projectNotes = new ArrayList<ProjectNotes>(ptslist.get(0)
					.getProjectNoteses());
		}

		projectTaskService.deleteProjectTask(projectTask, projectResources,
				projectResourceDetails, projectDiscussions, projectNotes);

	}

	public ProjectVO getProjectDepositDirectPaymentTransaction(
			ProjectVO projectVO) throws Exception {
		List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
		Project project = (Project) projectVO.getObject();
		DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
		directPaymentDetail.setAmount(project.getSecurityDeposit());
		directPayDetails.add(directPaymentDetail);
		projectVO.setDirectPayments(directPayDetails);
		projectVO.setCustomer(project.getCustomer());

		return projectVO;
	}

	public List<ProjectDiscussionVO> getAllProjectDiscussions(Long projectId,
			Long projectTaskId) {
		List<ProjectDiscussion> projectDiscussions = null;
		List<ProjectDiscussionVO> projectDiscussionVOs = null;
		try {
			if (projectTaskId != null && projectTaskId > 0 && projectId == null) {
				projectDiscussions = projectTaskService
						.getAllProjectDiscussionByTask(projectTaskId);
			}
			projectDiscussionVOs = new ArrayList<ProjectDiscussionVO>();
			for (ProjectDiscussion projectDiscussion : projectDiscussions) {
				projectDiscussionVOs
						.add(convertProjectDiscussionToVO(projectDiscussion));
			}
		} catch (Exception ex) {

			ex.printStackTrace();
		}
		return projectDiscussionVOs;
	}

	public ProjectDiscussionVO convertProjectDiscussionToVO(ProjectDiscussion pd) {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		ProjectDiscussionVO projectDiscussionVO = new ProjectDiscussionVO();
		User user = (User) sessionObj.get("USER");

		try {
			BeanUtils.copyProperties(projectDiscussionVO, pd);
			if ((long) projectDiscussionVO.getPerson().getPersonId() == (long) user
					.getPersonId())
				projectDiscussionVO.setSignedUser(true);

			projectDiscussionVO.setTime(DateFormat.getTimeOnlyWithSeconds(pd
					.getCreatedDate()));
			projectDiscussionVO.setDate(DateFormat.convertDateToString((pd
					.getCreatedDate().toString())));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectDiscussionVO;
	}

	public void saveProjectDiscussion(ProjectDiscussion projectDiscussion) {
		projectService.saveProjectDiscussion(projectDiscussion);
	}

	public List<ProjectNotesVO> getAllProjectNotes(Long projectId,
			Long projectTaskId) {
		List<ProjectNotes> projectNotesList = null;
		List<ProjectNotesVO> projectNotesVOs = null;
		try {
			if (projectId != null && projectId > 0 && projectTaskId == null) {
				projectNotesList = projectService
						.getAllProjectNotesByProject(projectId);
			} else if (projectTaskId != null && projectTaskId > 0
					&& projectId == null) {
				projectNotesList = projectService
						.getAllProjectNotesByTask(projectTaskId);
			}
			projectNotesVOs = new ArrayList<ProjectNotesVO>();
			for (ProjectNotes projectNote : projectNotesList) {
				projectNotesVOs.add(convertProjectNotesToVO(projectNote));
			}
		} catch (Exception ex) {

			ex.printStackTrace();
		}
		return projectNotesVOs;
	}

	public ProjectNotesVO convertProjectNotesToVO(ProjectNotes pd) {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		ProjectNotesVO projectNotesVO = new ProjectNotesVO();
		User user = (User) sessionObj.get("USER");

		try {
			BeanUtils.copyProperties(projectNotesVO, pd);
			if ((long) projectNotesVO.getPerson().getPersonId() == (long) user
					.getPersonId())
				projectNotesVO.setSignedUser(true);

			projectNotesVO.setTime(DateFormat.getTimeOnlyWithSeconds(pd
					.getCreatedDate()));
			projectNotesVO.setDate(DateFormat.convertDateToString((pd
					.getCreatedDate().toString())));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectNotesVO;
	}

	public void saveProjectNote(ProjectNotes projectNote) {
		projectService.saveProjectNotes(projectNote);
	}

	public List<ProjectTaskVO> manipulateTaskVOByProject(Long projectId) {
		List<ProjectTaskVO> taskVoList = null;
		try {
			List<ProjectTask> projectTasks = projectTaskService
					.getAllProjectTaskByProjectId(projectId);
			if (projectTasks != null && projectTasks.size() > 0)
				taskVoList = new ArrayList<ProjectTaskVO>();
			for (ProjectTask projectTask : projectTasks) {
				taskVoList.add(convertProjectTaskToVO(projectTask, taskVoList));
			}
			if (null != taskVoList && taskVoList.size() > 0) {
				Collections.sort(taskVoList, new Comparator<ProjectTaskVO>() {
					public int compare(ProjectTaskVO m1, ProjectTaskVO m2) {
						return m1.getProjectTaskId().compareTo(
								m2.getProjectTaskId());
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taskVoList;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public ProjectTask getProjectFromTaskRecursive(ProjectTask pt) {
		if (pt.getProject() == null)
			getProjectFromTaskRecursive(pt);
		return pt;
	}

	// Getters & Setters
	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public ProjectDeliveryBL getProjectDeliveryBL() {
		return projectDeliveryBL;
	}

	public void setProjectDeliveryBL(ProjectDeliveryBL projectDeliveryBL) {
		this.projectDeliveryBL = projectDeliveryBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public MaterialRequisitionBL getMaterialRequisitionBL() {
		return materialRequisitionBL;
	}

	public void setMaterialRequisitionBL(
			MaterialRequisitionBL materialRequisitionBL) {
		this.materialRequisitionBL = materialRequisitionBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public SalesInvoiceBL getSalesInvoiceBL() {
		return salesInvoiceBL;
	}

	public void setSalesInvoiceBL(SalesInvoiceBL salesInvoiceBL) {
		this.salesInvoiceBL = salesInvoiceBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public PettyCashBL getPettyCashBL() {
		return pettyCashBL;
	}

	public void setPettyCashBL(PettyCashBL pettyCashBL) {
		this.pettyCashBL = pettyCashBL;
	}
}
