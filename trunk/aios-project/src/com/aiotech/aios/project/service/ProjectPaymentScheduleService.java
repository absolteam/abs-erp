package com.aiotech.aios.project.service;

import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProjectPaymentScheduleService {
	private AIOTechGenericDAO<ProjectPaymentSchedule> projectPaymentScheduleDAO;

	public AIOTechGenericDAO<ProjectPaymentSchedule> getProjectPaymentScheduleDAO() {
		return projectPaymentScheduleDAO;
	}

	public void setProjectPaymentScheduleDAO(
			AIOTechGenericDAO<ProjectPaymentSchedule> projectPaymentScheduleDAO) {
		this.projectPaymentScheduleDAO = projectPaymentScheduleDAO;
	}
}
