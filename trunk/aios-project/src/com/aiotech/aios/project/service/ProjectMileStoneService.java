package com.aiotech.aios.project.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectPaymentDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProjectMileStoneService {

	private AIOTechGenericDAO<ProjectMileStone> projectMileStoneDAO;
	private AIOTechGenericDAO<ProjectPaymentDetail> projectPaymentDetailDAO;

	public List<ProjectMileStone> getActiveProjectMileStones(
			Implementation implementation, Byte completedStatus,
			Byte cancelledStatus) throws Exception {
		return projectMileStoneDAO.findByNamedQuery(
				"getActiveProjectMileStones", implementation, completedStatus,
				cancelledStatus);
	}

	public List<ProjectMileStone> getActiveProjectMileStonesByProject(
			Long projectId) throws Exception {
		return projectMileStoneDAO.findByNamedQuery(
				"getActiveProjectMileStonesByProject", projectId);
	}

	public List<ProjectMileStone> getAllProjectMileStones(
			Implementation implementation) throws Exception {
		return projectMileStoneDAO.findByNamedQuery("getAllProjectMileStones",
				implementation);
	}

	public List<ProjectMileStone> getFilteredProjectMileStone(
			Implementation implementation, Long projectMileStoneId,
			Long clientId, Long projectId, Date fromDate, Date toDate)
			throws Exception {
		Criteria criteria = projectMileStoneDAO.createCriteria();

		criteria.createAlias("project", "prj", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prj.customer", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "cpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("projectPaymentDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("implementation", implementation));

		if (projectMileStoneId != null && projectMileStoneId > 0)
			criteria.add(Restrictions.eq("projectMileStoneId",
					projectMileStoneId));

		if (clientId != null && clientId > 0) {
			criteria.add(Restrictions.eq("cst.customerId", clientId));
		}

		if (projectId != null && projectId > 0)
			criteria.add(Restrictions.eq("prj.projectId", projectId));

		if (fromDate != null)
			criteria.add(Restrictions.ge("startDate", fromDate));

		if (toDate != null)
			criteria.add(Restrictions.le("endDate", toDate));

		return criteria.list();
	}

	public List<ProjectPaymentDetail> getFilteredProjectPayment(
			Implementation implementation, Long projectPaymentId,
			Long projectMileStoneId, Long clientId, Long projectId,
			Byte currentStatus, Date fromDate, Date toDate) {

		Criteria criteria = projectPaymentDetailDAO.createCriteria();
		criteria.createAlias("projectMileStone", "mstone",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("mstone.project", "prj",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prj.customer", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "cpr",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("mstone.implementation",
					implementation));

		if (projectMileStoneId != null && projectMileStoneId > 0)
			criteria.add(Restrictions.eq("mstone.projectMileStoneId",
					projectMileStoneId));

		if (clientId != null && clientId > 0) {
			criteria.add(Restrictions.eq("cst.customerId", clientId));
		}

		if (projectId != null && projectId > 0)
			criteria.add(Restrictions.eq("prj.projectId", projectId));

		if (currentStatus != null && currentStatus > 0)
			criteria.add(Restrictions.eq("currentStatus", currentStatus));

		if (fromDate != null)
			criteria.add(Restrictions.ge("paymentDate", fromDate));

		if (toDate != null)
			criteria.add(Restrictions.le("paymentDate", toDate));

		return criteria.list();
	}

	public List<ProjectMileStone> getProjectMileStonesByEndDate(Date endDate,
			Byte cancelledStatus, Byte completedStatus) throws Exception {
		return projectMileStoneDAO.findByNamedQuery(
				"getProjectMileStonesByEndDate", endDate, cancelledStatus,
				completedStatus);
	}

	public List<ProjectPaymentDetail> getProjectPaymentsByPaymentDate(
			Date paymentDate, Byte cancelledStatus, Byte completedStatus)
			throws Exception {
		return projectPaymentDetailDAO.findByNamedQuery(
				"getProjectPaymentsByPaymentDate", paymentDate,
				cancelledStatus, completedStatus);
	}

	public List<ProjectMileStone> getProjectMileStoneByProject(long projectId)
			throws Exception {
		return projectMileStoneDAO.findByNamedQuery(
				"getProjectMileStoneByProject", projectId);
	}

	public ProjectMileStone getProjectMileStoneById(long projectMileStoneId)
			throws Exception {
		return projectMileStoneDAO.findByNamedQuery("getProjectMileStoneById",
				projectMileStoneId).get(0);
	}

	public ProjectPaymentDetail getProjectPaymentDetailById(
			long projectPaymentDetailId) throws Exception {
		return projectPaymentDetailDAO.findByNamedQuery(
				"getProjectPaymentDetailById", projectPaymentDetailId).get(0);
	}

	public List<ProjectPaymentDetail> getProjectPaymentDetailByMileStone(
			long projectPaymentDetailId) throws Exception {
		return projectPaymentDetailDAO.findByNamedQuery(
				"getProjectPaymentDetailByMileStone", projectPaymentDetailId);
	}

	public void saveProjectMileStone(ProjectMileStone projectMileStone,
			List<ProjectPaymentDetail> projectPaymentDetails) throws Exception {
		projectMileStoneDAO.saveOrUpdate(projectMileStone);
		if (null != projectPaymentDetails && projectPaymentDetails.size() > 0) {
			for (ProjectPaymentDetail projectPaymentDetail : projectPaymentDetails)
				/* projectPaymentDetail.setProjectMileStone(projectMileStone); */
				projectPaymentDetailDAO.saveOrUpdateAll(projectPaymentDetails);
		}
	}

	public void saveProjectMileStone(ProjectMileStone projectMileStone)
			throws Exception {
		projectMileStoneDAO.saveOrUpdate(projectMileStone);
	}

	public void deleteProjectMileStone(ProjectMileStone projectMileStone,
			List<ProjectPaymentDetail> projectPaymentDetails) throws Exception {
		if (null != projectPaymentDetails && projectPaymentDetails.size() > 0)
			projectPaymentDetailDAO.deleteAll(projectPaymentDetails);
		projectMileStoneDAO.delete(projectMileStone);
	}

	public void deleteProjectPaymentDetails(
			List<ProjectPaymentDetail> projectPaymentDetails) throws Exception {
		projectPaymentDetailDAO.deleteAll(projectPaymentDetails);
	}

	// Getters & Setters
	public AIOTechGenericDAO<ProjectMileStone> getProjectMileStoneDAO() {
		return projectMileStoneDAO;
	}

	public void setProjectMileStoneDAO(
			AIOTechGenericDAO<ProjectMileStone> projectMileStoneDAO) {
		this.projectMileStoneDAO = projectMileStoneDAO;
	}

	public AIOTechGenericDAO<ProjectPaymentDetail> getProjectPaymentDetailDAO() {
		return projectPaymentDetailDAO;
	}

	public void setProjectPaymentDetailDAO(
			AIOTechGenericDAO<ProjectPaymentDetail> projectPaymentDetailDAO) {
		this.projectPaymentDetailDAO = projectPaymentDetailDAO;
	}
}
