package com.aiotech.aios.project.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.ast.tree.IsNotNullLogicOperatorNode;

import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectDiscussion;
import com.aiotech.aios.project.domain.entity.ProjectExpense;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectNotes;
import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;
import com.aiotech.aios.project.domain.entity.ProjectResource;
import com.aiotech.aios.project.domain.entity.ProjectResourceDetail;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProjectService {

	private AIOTechGenericDAO<Project> projectDAO;
	private AIOTechGenericDAO<ProjectResource> projectResourceDAO;
	private AIOTechGenericDAO<ProjectResourceDetail> projectResourceDetailDAO;
	private AIOTechGenericDAO<ProjectMileStone> projectMileStoneDAO;
	private AIOTechGenericDAO<ProjectTaskSurrogate> projectTaskSurrogateDAO;
	private AIOTechGenericDAO<ProjectDiscussion> projectDiscussionDAO;
	private AIOTechGenericDAO<ProjectNotes> projectNotesDAO;
	private AIOTechGenericDAO<ProjectPaymentSchedule> projectPaymentScheduleDAO;
	private AIOTechGenericDAO<ProjectExpense> projectExpenseDAO;
	private AIOTechGenericDAO<ProjectInventory> projectInventoryDAO;
	private AIOTechGenericDAO<ProjectDelivery> projectDeliveryDAO;

	public List<Project> getAllProjects(Implementation implementation)
			throws Exception {
		return projectDAO.findByNamedQuery("getAllProjects", implementation);
	}

	public List<Project> getActiveProjects(Implementation implementation,
			byte closed) throws Exception {
		return projectDAO.findByNamedQuery("getActiveProjects", implementation,
				closed);
	}

	public List<Project> getActiveProjectsForSecurityDeposit(byte cancelled,
			byte closed) throws Exception {
		return projectDAO.findByNamedQuery(
				"getActiveProjectsForSecurityDeposit", closed);
	}

	public Project findProjectById(long projectId) throws Exception {
		return projectDAO.findById(projectId);
	}

	public Project getBasicProjectById(long projectId) throws Exception {
		return projectDAO.findByNamedQuery("getBasicProjectById", projectId)
				.get(0);
	}

	public Project getProjectById(long projectId) throws Exception {
		return projectDAO.findByNamedQuery("getProjectById", projectId).get(0);
	}

	public Project getProjectForHistory(long projectId) throws Exception {
		return projectDAO.findByNamedQuery("getProjectForHistory", projectId)
				.get(0);
	}

	public ProjectPaymentSchedule getProjectPaymentScheduleById(
			long projectPaymentScheduleId) throws Exception {
		List<ProjectPaymentSchedule> schedules = projectPaymentScheduleDAO
				.findByNamedQuery("getProjectPaymentScheduleById",
						projectPaymentScheduleId);
		if (schedules != null && schedules.size() > 0)
			return schedules.get(0);
		else
			return null;

	}

	public List<ProjectResourceDetail> getProjectResourceDetailById(
			long projectResourceId) throws Exception {
		return projectResourceDetailDAO.findByNamedQuery(
				"getProjectResourceDetailById", projectResourceId);
	}

	public List<ProjectResource> getProjectResourcByProjectId(
			long projectResourceId) throws Exception {
		return projectResourceDAO.findByNamedQuery(
				"getProjectResourcByProjectId", projectResourceId);
	}

	public List<ProjectMileStone> getProjectMileStoneByProjectId(long projectId)
			throws Exception {
		return projectMileStoneDAO.findByNamedQuery(
				"getProjectMileStoneByProjectId", projectId);
	}

	public List<ProjectPaymentSchedule> getProjectPaymentScheduleByProjectId(
			long projectId) throws Exception {
		return projectPaymentScheduleDAO.findByNamedQuery(
				"getProjectPaymentScheduleByProjectId", projectId);
	}

	public List<ProjectPaymentSchedule> getProjectPaymentScheduleForScheduler()
			throws Exception {
		return projectPaymentScheduleDAO
				.findByNamedQuery("getProjectPaymentScheduleForScheduler");
	}

	public List<ProjectExpense> getProjectExpenseByProjectId(long projectId)
			throws Exception {
		return projectExpenseDAO.findByNamedQuery(
				"getProjectExpenseByProjectId", projectId);
	}

	public List<ProjectInventory> getProjectInventoriesByProjectId(
			long projectId) throws Exception {
		return projectInventoryDAO.findByNamedQuery(
				"getProjectInventoriesByProjectId", projectId);
	}

	public ProjectExpense getProjectExpenseById(long projectExpenseId)
			throws Exception {
		return projectExpenseDAO.findById(projectExpenseId);
	}

	public ProjectMileStone getProjectMileStoneById(long projectMileStoneId)
			throws Exception {
		return projectMileStoneDAO.findById(projectMileStoneId);
	}

	public void saveProject(Project project,
			List<ProjectResource> projectResources,
			List<ProjectResourceDetail> projectResourceDetails,
			List<ProjectMileStone> projectMileStones, ProjectTaskSurrogate pts,
			List<ProjectPaymentSchedule> projectPaymentSchedules,
			List<ProjectExpense> projectExpenses,
			List<ProjectInventory> projectInventories,
			List<ProjectNotes> projectNotes,
			List<ProjectDiscussion> projectDiscussions,
			List<ProjectDelivery> projectDeliveries) throws Exception {
		projectDAO.saveOrUpdate(project);
		if (pts == null) {
			pts = new ProjectTaskSurrogate();
			pts.setProject(project);
			projectTaskSurrogateDAO.save(pts);
		}
		if (null != projectResources && projectResources.size() > 0) {

			for (ProjectResource projectResource : projectResources)
				projectResource.setProjectTaskSurrogate(pts);
			projectResourceDAO.saveOrUpdateAll(projectResources);
			if (null != projectResourceDetails
					&& projectResourceDetails.size() > 0) {
				projectResourceDetailDAO
						.saveOrUpdateAll(projectResourceDetails);
			}
		}

		if (null != projectMileStones && projectMileStones.size() > 0) {

			for (ProjectMileStone projectMileStone : projectMileStones)
				projectMileStone.setProject(project);

			projectMileStoneDAO.saveOrUpdateAll(projectMileStones);

		}

		if (null != projectPaymentSchedules
				&& projectPaymentSchedules.size() > 0) {

			for (ProjectPaymentSchedule projectPaymentSchedule : projectPaymentSchedules)
				projectPaymentSchedule.setProjectTaskSurrogate(pts);

			projectPaymentScheduleDAO.saveOrUpdateAll(projectPaymentSchedules);

		}

		if (null != projectExpenses && projectExpenses.size() > 0) {

			for (ProjectExpense projectExpense : projectExpenses)
				projectExpense.setProjectTaskSurrogate(pts);

			projectExpenseDAO.saveOrUpdateAll(projectExpenses);

		}

		if (null != projectInventories && projectInventories.size() > 0) {

			for (ProjectInventory projectinVentory : projectInventories)
				projectinVentory.setProjectTaskSurrogate(pts);

			projectInventoryDAO.saveOrUpdateAll(projectInventories);

		}

		if (null != projectNotes && projectNotes.size() > 0) {

			for (ProjectNotes projectinNote : projectNotes)
				projectinNote.setProjectTaskSurrogate(pts);

			projectNotesDAO.saveOrUpdateAll(projectNotes);

		}

		if (null != projectDiscussions && projectDiscussions.size() > 0) {

			for (ProjectDiscussion projectinDiscussion : projectDiscussions)
				projectinDiscussion.setProjectTaskSurrogate(pts);

			projectDiscussionDAO.saveOrUpdateAll(projectDiscussions);

		}

		if (null != projectDeliveries && projectDeliveries.size() > 0) {

			for (ProjectDelivery projectinDelivery : projectDeliveries)
				projectinDelivery.setProjectTaskSurrogate(pts);

			projectDeliveryDAO.saveOrUpdateAll(projectDeliveries);

		}

	}

	public void deleteProjectResourceDetail(
			List<ProjectResourceDetail> projectResourceDetails)
			throws Exception {
		projectResourceDetailDAO.deleteAll(projectResourceDetails);
	}

	public void deleteProjectResource(List<ProjectResource> projectResources)
			throws Exception {
		projectResourceDAO.deleteAll(projectResources);
	}

	public void deleteProjectMilestones(
			List<ProjectMileStone> deleteProjectMilestones) throws Exception {
		projectMileStoneDAO.deleteAll(deleteProjectMilestones);
	}

	public void deleteProjectPaymentSchedules(
			List<ProjectPaymentSchedule> deleteProjectPaymentSchedule)
			throws Exception {
		projectPaymentScheduleDAO.deleteAll(deleteProjectPaymentSchedule);
	}

	public void deleteProjectExpenses(List<ProjectExpense> deleteProjectExpenses)
			throws Exception {
		projectExpenseDAO.deleteAll(deleteProjectExpenses);
	}

	public void deleteProjectInventories(
			List<ProjectInventory> deleteProjectInventories) throws Exception {
		projectInventoryDAO.deleteAll(deleteProjectInventories);
	}

	public void deleteProject(Project project,
			List<ProjectResource> projectResources,
			List<ProjectResourceDetail> projectResourceDetails,
			List<ProjectMileStone> projectMileStones,
			List<ProjectPaymentSchedule> projectPaymentSchedules,
			List<ProjectExpense> projectExpenses,
			List<ProjectDiscussion> projectDiscussions,
			List<ProjectNotes> projectNotes,
			List<ProjectInventory> projectInventories) throws Exception {
		if (null != projectResources && projectResources.size() > 0) {
			if (null != projectResourceDetails
					&& projectResourceDetails.size() > 0)
				projectResourceDetailDAO.deleteAll(projectResourceDetails);
			projectResourceDAO.deleteAll(projectResources);
		}
		if (projectDiscussions != null && projectDiscussions.size() > 0)
			projectDiscussionDAO.deleteAll(projectDiscussions);
		if (projectNotes != null && projectNotes.size() > 0)
			projectNotesDAO.deleteAll(projectNotes);
		if (projectMileStones != null && projectMileStones.size() > 0)
			projectMileStoneDAO.deleteAll(projectMileStones);

		if (projectPaymentSchedules != null
				&& projectPaymentSchedules.size() > 0)
			projectPaymentScheduleDAO.deleteAll(projectPaymentSchedules);
		if (projectExpenses != null && projectExpenses.size() > 0)
			projectExpenseDAO.deleteAll(projectExpenses);

		if (projectInventories != null && projectInventories.size() > 0)
			projectInventoryDAO.deleteAll(projectInventories);

		projectTaskSurrogateDAO.deleteAll(new ArrayList<ProjectTaskSurrogate>(
				project.getProjectTaskSurrogates()));
		projectDAO.evict(project);
		projectDAO.refresh(project);
		projectDAO.delete(project);
	}

	@SuppressWarnings("unchecked")
	public List<Project> getFilteredProjects(Implementation implementation,
			Long projectId, Long clientId, Long labourId, Long resourceId,
			Long leadId, Byte status, Long projectType, Date fromDate,
			Date toDate) {
		Criteria criteria = projectDAO.createCriteria();

		criteria.createAlias("personByProjectLead", "lead",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("creditTerm", "crd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customer", "cust",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust.personByPersonId", "cust_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust.company", "comp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("projectTaskSurrogates", "surrogate",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("surrogate.projectInventories", "inventory",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inventory.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inventory.shelf", "slf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("slf.shelf", "rack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.aisle", "ae",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ae.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		/*
		 * criteria.createAlias("projectResources", "res",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("res.projectResourceDetails", "det",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("res.lookupDetail", "res_ld",
		 * CriteriaSpecification.LEFT_JOIN); criteria.createAlias("det.person",
		 * "labor", CriteriaSpecification.LEFT_JOIN);
		 */

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (projectId != null && projectId > 0) {
			criteria.add(Restrictions.eq("projectId", projectId));
		}

		if (clientId != null && clientId > 0) {
			criteria.add(Restrictions.eq("cust.customerId", clientId));
		}

		/*
		 * if (labourId != null && labourId > 0) {
		 * criteria.add(Restrictions.eq("labor.personId", labourId)); }
		 * 
		 * if (resourceId != null && resourceId > 0) {
		 * criteria.add(Restrictions.eq("res_ld.lookupDetailId", resourceId)); }
		 */

		if (leadId != null && leadId > 0) {
			criteria.add(Restrictions.eq("lead.personId", leadId));
		}

		if (status != null && status > 0) {
			criteria.add(Restrictions.eq("projectStatus", status));
		}

		if (projectType != null && projectType > 0) {
			criteria.add(Restrictions.eq("priority", projectType));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("startDate", fromDate, toDate));
		}

		Set<Project> uniqueRecords = new HashSet<Project>(criteria.list());
		List<Project> projectList = new ArrayList<Project>(uniqueRecords);
		return projectList;
	}

	@SuppressWarnings("unchecked")
	public List<Project> getFilteredPaymentsByProjects(
			SalesInvoiceVO vo) {
		Criteria criteria = projectDAO.createCriteria();

		criteria.createAlias("projectTaskSurrogates", "surogate",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("surogate.projectPaymentSchedules", "payment",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("payment.lookupDetail", "look",
				CriteriaSpecification.LEFT_JOIN);
		
		criteria.createAlias("customer", "cust",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust.personByPersonId", "person",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust.cmpDeptLocation", "cmpDeptLoc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cmpDeptLoc.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cmpDeptLoc.department", "dept",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cmpDeptLoc.location", "loc",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("cust.bankReceiptses", "receipt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("receipt.bankReceiptsDetails", "receiptDetails",
				CriteriaSpecification.LEFT_JOIN);

		if (vo.getImplementation() != null) {
			criteria.add(Restrictions.eq("implementation",
					vo.getImplementation()));
		}

		if (vo.getCustomerId() != null && vo.getCustomerId() > 0) {
			criteria.add(Restrictions.eq("cust.customerId", vo.getCustomerId()));
		}

		if (vo.getFormDate() != null && vo.getToDate() != null) {
			criteria.add(Restrictions.gt("payment.paymentDate", vo.getFormDate())).add(
					Restrictions.lt("payment.paymentDate", vo.getToDate()));
		}
		
		criteria.add(Restrictions.isNotNull("payment.projectPaymentScheduleId"));

		Set<Project> uniqueRecords = new HashSet<Project>(
				criteria.list());
		List<Project> projectList = new ArrayList<Project>(
				uniqueRecords);
		return projectList;
	}

	public List<ProjectDiscussion> getAllProjectDiscussionByProject(
			Long projectId) throws Exception {
		return projectDiscussionDAO.findByNamedQuery(
				"getAllProjectDiscussionByProject", projectId);
	}

	public List<ProjectDiscussion> getAllProjectDiscussionByTask(
			Long projectTaskId) throws Exception {
		return projectDiscussionDAO.findByNamedQuery(
				"getAllProjectDiscussionByTask", projectTaskId);
	}

	public void saveProjectDiscussion(ProjectDiscussion projectDiscussion) {
		projectDiscussionDAO.saveOrUpdate(projectDiscussion);
	}

	public List<ProjectNotes> getAllProjectNotesByProject(Long projectId)
			throws Exception {
		return projectNotesDAO.findByNamedQuery("getAllProjectNotesByProject",
				projectId);
	}

	public List<ProjectNotes> getAllProjectNotesByTask(Long projectTaskId)
			throws Exception {
		return projectNotesDAO.findByNamedQuery("getAllProjectNotesByTask",
				projectTaskId);
	}

	public void saveProjectNotes(ProjectNotes projectNote) {
		projectNotesDAO.saveOrUpdate(projectNote);
	}

	// Getters & Setters
	public AIOTechGenericDAO<Project> getProjectDAO() {
		return projectDAO;
	}

	public void setProjectDAO(AIOTechGenericDAO<Project> projectDAO) {
		this.projectDAO = projectDAO;
	}

	public AIOTechGenericDAO<ProjectResource> getProjectResourceDAO() {
		return projectResourceDAO;
	}

	public void setProjectResourceDAO(
			AIOTechGenericDAO<ProjectResource> projectResourceDAO) {
		this.projectResourceDAO = projectResourceDAO;
	}

	public AIOTechGenericDAO<ProjectResourceDetail> getProjectResourceDetailDAO() {
		return projectResourceDetailDAO;
	}

	public void setProjectResourceDetailDAO(
			AIOTechGenericDAO<ProjectResourceDetail> projectResourceDetailDAO) {
		this.projectResourceDetailDAO = projectResourceDetailDAO;
	}

	public AIOTechGenericDAO<ProjectMileStone> getProjectMileStoneDAO() {
		return projectMileStoneDAO;
	}

	public void setProjectMileStoneDAO(
			AIOTechGenericDAO<ProjectMileStone> projectMileStoneDAO) {
		this.projectMileStoneDAO = projectMileStoneDAO;
	}

	public AIOTechGenericDAO<ProjectTaskSurrogate> getProjectTaskSurrogateDAO() {
		return projectTaskSurrogateDAO;
	}

	public void setProjectTaskSurrogateDAO(
			AIOTechGenericDAO<ProjectTaskSurrogate> projectTaskSurrogateDAO) {
		this.projectTaskSurrogateDAO = projectTaskSurrogateDAO;
	}

	public AIOTechGenericDAO<ProjectDiscussion> getProjectDiscussionDAO() {
		return projectDiscussionDAO;
	}

	public void setProjectDiscussionDAO(
			AIOTechGenericDAO<ProjectDiscussion> projectDiscussionDAO) {
		this.projectDiscussionDAO = projectDiscussionDAO;
	}

	public AIOTechGenericDAO<ProjectNotes> getProjectNotesDAO() {
		return projectNotesDAO;
	}

	public void setProjectNotesDAO(
			AIOTechGenericDAO<ProjectNotes> projectNotesDAO) {
		this.projectNotesDAO = projectNotesDAO;
	}

	public AIOTechGenericDAO<ProjectPaymentSchedule> getProjectPaymentScheduleDAO() {
		return projectPaymentScheduleDAO;
	}

	public void setProjectPaymentScheduleDAO(
			AIOTechGenericDAO<ProjectPaymentSchedule> projectPaymentScheduleDAO) {
		this.projectPaymentScheduleDAO = projectPaymentScheduleDAO;
	}

	public AIOTechGenericDAO<ProjectExpense> getProjectExpenseDAO() {
		return projectExpenseDAO;
	}

	public void setProjectExpenseDAO(
			AIOTechGenericDAO<ProjectExpense> projectExpenseDAO) {
		this.projectExpenseDAO = projectExpenseDAO;
	}

	public AIOTechGenericDAO<ProjectInventory> getProjectInventoryDAO() {
		return projectInventoryDAO;
	}

	public void setProjectInventoryDAO(
			AIOTechGenericDAO<ProjectInventory> projectInventoryDAO) {
		this.projectInventoryDAO = projectInventoryDAO;
	}

	public AIOTechGenericDAO<ProjectDelivery> getProjectDeliveryDAO() {
		return projectDeliveryDAO;
	}

	public void setProjectDeliveryDAO(
			AIOTechGenericDAO<ProjectDelivery> projectDeliveryDAO) {
		this.projectDeliveryDAO = projectDeliveryDAO;
	}
}
