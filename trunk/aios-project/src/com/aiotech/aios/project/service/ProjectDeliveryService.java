package com.aiotech.aios.project.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProjectDeliveryService {

	private AIOTechGenericDAO<ProjectDelivery> projectDeliveryDAO;

	public List<ProjectDelivery> getAllProjectDeliveries(
			Implementation implementation, Byte cancelledStatus,
			Byte deliverdStaus) throws Exception {
		return projectDeliveryDAO.findByNamedQuery("getAllProjectDeliveries",
				implementation);
	}

	public List<ProjectDelivery> getAllDeliveriesByTask(Long projectTaskId)
			throws Exception {
		return projectDeliveryDAO.findByNamedQuery("getAllDeliveriesByTask",
				projectTaskId);
	}

	public List<ProjectDelivery> getAllDeliverieByProject(Long projectId)
			throws Exception {
		return projectDeliveryDAO.findByNamedQuery("getAllDeliveriesByProject",
				projectId);
	}

	public ProjectDelivery getProjectDeliveryById(long projectDeliveryId)
			throws Exception {
		return projectDeliveryDAO.findByNamedQuery("getProjectDeliveryById",
				projectDeliveryId).get(0);
	}

	public ProjectDelivery getProjectDeliveryDetailById(long projectDeliveryId)
			throws Exception {
		return projectDeliveryDAO.findByNamedQuery(
				"getProjectDeliveryDetailById", projectDeliveryId).get(0);
	}

	public void saveProjectDelivery(ProjectDelivery projectDelivery)
			throws Exception {
		projectDeliveryDAO.saveOrUpdate(projectDelivery);
	}

	public void deleteProjectDelivery(ProjectDelivery projectDelivery)
			throws Exception {
		projectDeliveryDAO.delete(projectDelivery);
	}

	@SuppressWarnings("unchecked")
	public List<ProjectDelivery> getFilteredProjectDelivarables(
			Implementation implementation, Long projectDeliveryId,
			Long clientId, Byte status, Long projectType, Date fromDate,
			Date toDate) {
		Criteria criteria = projectDeliveryDAO.createCriteria();

		criteria.createAlias("projectTaskSurrogate", "pts",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pts.project", "prj",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pts.projectTask", "pt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prj.customer", "cust",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust.personByPersonId", "cust_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust.company", "comp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pt.project.customer", "cust1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust1.personByPersonId", "cust_per1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cust1.company", "comp1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pt.project", "pt1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pt.projectMileStone", "pms",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("prj.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (projectDeliveryId != null && projectDeliveryId > 0) {
			criteria.add(Restrictions
					.eq("projectDeliveryId", projectDeliveryId));
		}

		if (clientId != null && clientId > 0) {
			criteria.add(Restrictions.or(
					Restrictions.eq("cust.customerId", clientId),
					Restrictions.eq("cust1.customerId", clientId)));
		}

		if (status != null && status > 0) {
			criteria.add(Restrictions.eq("prj.projectStatus", status));
		}

		if (projectType != null && projectType > 0) {
			criteria.add(Restrictions.eq("ld.lookupDetailId", projectType));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("deliveryDate", fromDate, toDate));
		}

		return criteria.list();
	}

	// Getters & Setters
	public AIOTechGenericDAO<ProjectDelivery> getProjectDeliveryDAO() {
		return projectDeliveryDAO;
	}

	public void setProjectDeliveryDAO(
			AIOTechGenericDAO<ProjectDelivery> projectDeliveryDAO) {
		this.projectDeliveryDAO = projectDeliveryDAO;
	}
}
