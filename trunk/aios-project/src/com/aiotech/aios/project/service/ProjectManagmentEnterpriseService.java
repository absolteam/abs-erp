package com.aiotech.aios.project.service;


public class ProjectManagmentEnterpriseService {
	private ProjectMileStoneService projectMileStoneService;
	private ProjectService projectService;
	private ProjectDeliveryService projectDeliveryService;
	
	public ProjectService getProjectService() {
		return projectService;
	}
	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
	public ProjectDeliveryService getProjectDeliveryService() {
		return projectDeliveryService;
	}
	public void setProjectDeliveryService(
			ProjectDeliveryService projectDeliveryService) {
		this.projectDeliveryService = projectDeliveryService;
	}
	public ProjectMileStoneService getProjectMileStoneService() {
		return projectMileStoneService;
	}
	public void setProjectMileStoneService(
			ProjectMileStoneService projectMileStoneService) {
		this.projectMileStoneService = projectMileStoneService;
	}
}
