package com.aiotech.aios.project.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDiscussion;
import com.aiotech.aios.project.domain.entity.ProjectExpense;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectNotes;
import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;
import com.aiotech.aios.project.domain.entity.ProjectResource;
import com.aiotech.aios.project.domain.entity.ProjectResourceDetail;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProjectTaskService {

	private AIOTechGenericDAO<ProjectTask> projectTaskDAO;
	private AIOTechGenericDAO<ProjectMileStone> projectMileStoneDAO;
	private AIOTechGenericDAO<ProjectTaskSurrogate> projectTaskSurrogateDAO;
	private AIOTechGenericDAO<ProjectDiscussion> projectDiscussionDAO;
	private AIOTechGenericDAO<ProjectResource> projectResourceDAO;
	private AIOTechGenericDAO<ProjectResourceDetail> projectResourceDetailDAO;
	private AIOTechGenericDAO<ProjectNotes> projectNotesDAO;
	private AIOTechGenericDAO<ProjectPaymentSchedule> projectPaymentScheduleDAO;
	private AIOTechGenericDAO<ProjectExpense> projectExpenseDAO;
	private AIOTechGenericDAO<ProjectInventory> projectInventoryDAO;

	public List<ProjectTask> getAllProjectTasks(Implementation implementation)
		throws Exception {
		return projectTaskDAO.findByNamedQuery("getAllProjectTasks", implementation);
	}


	public ProjectTask getProjectTaskById(long projectTaskId) throws Exception {
		return projectTaskDAO.findByNamedQuery("getProjectTaskById",
				projectTaskId).get(0);
	}

	public List<ProjectResource> getProjectResourcByProjectTaskId(
			long projectTaskId) throws Exception {
		return projectResourceDAO.findByNamedQuery(
				"getProjectResourcByProjectTaskId", projectTaskId);
	}

	public void saveProjectTask(ProjectTask projectTask,
			List<ProjectResource> projectResources,
			List<ProjectResourceDetail> projectResourceDetails,
			ProjectTaskSurrogate pts,List<ProjectExpense> projectExpenses,
			List<ProjectInventory> projectInventories) throws Exception {
		projectTaskDAO.saveOrUpdate(projectTask);
		if (pts == null) {
			pts = new ProjectTaskSurrogate();
			pts.setProjectTask(projectTask);
			pts.setProject(null);
			projectTaskSurrogateDAO.save(pts);
		}
		if (null != projectResources && projectResources.size() > 0) {

			for (ProjectResource projectResource : projectResources)
				projectResource.setProjectTaskSurrogate(pts);
			projectResourceDAO.saveOrUpdateAll(projectResources);
			if (null != projectResourceDetails
					&& projectResourceDetails.size() > 0) {
				projectResourceDetailDAO
						.saveOrUpdateAll(projectResourceDetails);
			}
		}
		
		if (null != projectExpenses && projectExpenses.size() > 0) {

			for (ProjectExpense projectExpense : projectExpenses)
				projectExpense.setProjectTaskSurrogate(pts);

			projectExpenseDAO.saveOrUpdateAll(projectExpenses);

		}

		if (null != projectInventories && projectInventories.size() > 0) {

			for (ProjectInventory projectinVentory : projectInventories)
				projectinVentory.setProjectTaskSurrogate(pts);

			projectInventoryDAO.saveOrUpdateAll(projectInventories);

		}

	}

	public void deleteProjectResourceDetail(
			List<ProjectResourceDetail> projectResourceDetails)
			throws Exception {
		projectResourceDetailDAO.deleteAll(projectResourceDetails);
	}

	public void deleteProjectResource(List<ProjectResource> projectResources)
			throws Exception {
		projectResourceDAO.deleteAll(projectResources);
	}

	public void deleteProjectTask(ProjectTask projectTask,
			List<ProjectResource> projectResources,
			List<ProjectResourceDetail> projectResourceDetails,
			List<ProjectDiscussion> projectDiscussions,
			List<ProjectNotes> projectNotes) throws Exception {
		if (null != projectResources && projectResources.size() > 0) {
			if (null != projectResourceDetails
					&& projectResourceDetails.size() > 0)
				projectResourceDetailDAO.deleteAll(projectResourceDetails);
			projectResourceDAO.deleteAll(projectResources);
		}
		if(projectDiscussions!=null && projectDiscussions.size()>0)
			projectDiscussionDAO.deleteAll(projectDiscussions);
		if(projectNotes!=null && projectNotes.size()>0)
			projectNotesDAO.deleteAll(projectNotes);

		projectTaskSurrogateDAO.deleteAll(new ArrayList<ProjectTaskSurrogate>(
				projectTask.getProjectTaskSurrogates()));
		projectTaskDAO.delete(projectTask);
	}

	public List<ProjectDiscussion> getAllProjectDiscussionByTask(
			Long projectTaskId) throws Exception {
		return projectDiscussionDAO.findByNamedQuery(
				"getAllProjectDiscussionByTask", projectTaskId);
	}

	public List<ProjectNotes> getAllProjectNotesByTask(Long projectTaskId)
			throws Exception {
		return projectNotesDAO.findByNamedQuery("getAllProjectNotesByTask",
				projectTaskId);
	}
	
	public List<ProjectTask> getAllProjectTaskByProjectId(Long projectId)
		throws Exception {
	return projectTaskDAO.findByNamedQuery("getAllProjectTaskByProjectId",
			projectId);
	}

	public List<ProjectTask> getAllProjectTaskByParentTaskId(Long taskId)
		throws Exception {
		return projectTaskDAO.findByNamedQuery("getAllProjectTaskByParentTaskId",
				taskId);
	}
	
	public List<ProjectExpense> getProjectTaskExpenseByTaskId(long taskId)
		throws Exception {
		return projectExpenseDAO.findByNamedQuery(
				"getProjectTaskExpenseByTaskId", taskId);
	}
	
	public List<ProjectInventory> getProjectTaskInventoriesByTaskId(
		long taskId) throws Exception {
		return projectInventoryDAO.findByNamedQuery(
				"getProjectTaskInventoriesByTaskId", taskId);
	}

	// Getter Setter
	public AIOTechGenericDAO<ProjectTask> getProjectTaskDAO() {
		return projectTaskDAO;
	}

	public void setProjectTaskDAO(AIOTechGenericDAO<ProjectTask> projectTaskDAO) {
		this.projectTaskDAO = projectTaskDAO;
	}

	public AIOTechGenericDAO<ProjectMileStone> getProjectMileStoneDAO() {
		return projectMileStoneDAO;
	}

	public void setProjectMileStoneDAO(
			AIOTechGenericDAO<ProjectMileStone> projectMileStoneDAO) {
		this.projectMileStoneDAO = projectMileStoneDAO;
	}

	public AIOTechGenericDAO<ProjectTaskSurrogate> getProjectTaskSurrogateDAO() {
		return projectTaskSurrogateDAO;
	}

	public void setProjectTaskSurrogateDAO(
			AIOTechGenericDAO<ProjectTaskSurrogate> projectTaskSurrogateDAO) {
		this.projectTaskSurrogateDAO = projectTaskSurrogateDAO;
	}

	public AIOTechGenericDAO<ProjectDiscussion> getProjectDiscussionDAO() {
		return projectDiscussionDAO;
	}

	public void setProjectDiscussionDAO(
			AIOTechGenericDAO<ProjectDiscussion> projectDiscussionDAO) {
		this.projectDiscussionDAO = projectDiscussionDAO;
	}

	public AIOTechGenericDAO<ProjectResource> getProjectResourceDAO() {
		return projectResourceDAO;
	}

	public void setProjectResourceDAO(
			AIOTechGenericDAO<ProjectResource> projectResourceDAO) {
		this.projectResourceDAO = projectResourceDAO;
	}

	public AIOTechGenericDAO<ProjectResourceDetail> getProjectResourceDetailDAO() {
		return projectResourceDetailDAO;
	}

	public void setProjectResourceDetailDAO(
			AIOTechGenericDAO<ProjectResourceDetail> projectResourceDetailDAO) {
		this.projectResourceDetailDAO = projectResourceDetailDAO;
	}

	public AIOTechGenericDAO<ProjectNotes> getProjectNotesDAO() {
		return projectNotesDAO;
	}

	public void setProjectNotesDAO(
			AIOTechGenericDAO<ProjectNotes> projectNotesDAO) {
		this.projectNotesDAO = projectNotesDAO;
	}

	public AIOTechGenericDAO<ProjectPaymentSchedule> getProjectPaymentScheduleDAO() {
		return projectPaymentScheduleDAO;
	}

	public void setProjectPaymentScheduleDAO(
			AIOTechGenericDAO<ProjectPaymentSchedule> projectPaymentScheduleDAO) {
		this.projectPaymentScheduleDAO = projectPaymentScheduleDAO;
	}

	public AIOTechGenericDAO<ProjectExpense> getProjectExpenseDAO() {
		return projectExpenseDAO;
	}

	public void setProjectExpenseDAO(
			AIOTechGenericDAO<ProjectExpense> projectExpenseDAO) {
		this.projectExpenseDAO = projectExpenseDAO;
	}


	public AIOTechGenericDAO<ProjectInventory> getProjectInventoryDAO() {
		return projectInventoryDAO;
	}


	public void setProjectInventoryDAO(
			AIOTechGenericDAO<ProjectInventory> projectInventoryDAO) {
		this.projectInventoryDAO = projectInventoryDAO;
	}
}
