package com.aiotech.aios.project.service.bl;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import sun.util.logging.resources.logging;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PettyCashDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.BankReceiptsBL;
import com.aiotech.aios.accounts.service.bl.CreditBL;
import com.aiotech.aios.accounts.service.bl.CreditTermBL;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.service.bl.MaterialRequisitionBL;
import com.aiotech.aios.accounts.service.bl.PettyCashBL;
import com.aiotech.aios.accounts.service.bl.PointOfSaleBL;
import com.aiotech.aios.accounts.service.bl.ProductBL;
import com.aiotech.aios.accounts.service.bl.SalesInvoiceBL;
import com.aiotech.aios.accounts.service.bl.StockBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectPaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.to.Constants.ReturnProcessType;
import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.bl.JobAssignmentBL;
import com.aiotech.aios.hr.service.bl.PersonBL;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectDiscussion;
import com.aiotech.aios.project.domain.entity.ProjectExpense;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectNotes;
import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;
import com.aiotech.aios.project.domain.entity.ProjectResource;
import com.aiotech.aios.project.domain.entity.ProjectResourceDetail;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.project.domain.entity.vo.ProjectDiscussionVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectExpenseVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectInventoryVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectMileStoneVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectNotesVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectPaymentScheduleVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectResourceVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectVO;
import com.aiotech.aios.project.service.ProjectDeliveryService;
import com.aiotech.aios.project.service.ProjectPaymentScheduleService;
import com.aiotech.aios.project.service.ProjectService;
import com.aiotech.aios.project.service.ProjectTaskService;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;

public class ProjectBL {

	// Dependencies
	private ProjectService projectService;
	private LookupMasterBL lookupMasterBL;
	private CreditTermBL creditTermBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private ImageBL imageBL;
	private DirectPaymentBL directPaymentBL;
	private PersonBL personBL;
	private StockBL stockBL;
	private TransactionBL transactionBL;
	private ProductBL productBL;
	private AlertBL alertBL;
	private PointOfSaleBL pointOfSaleBL;
	private BankReceiptsBL bankReceiptsBL;
	private SalesInvoiceBL salesInvoiceBL;
	private CreditBL creditBL;
	private MaterialRequisitionBL materialRequisitionBL;
	private JobAssignmentBL jobAssignmentBL;
	private PettyCashBL pettyCashBL;
	private ProjectTaskService projectTaskService;
	private ProjectPaymentScheduleService projectPaymentScheduleService;
	private ProjectDeliveryService projectDeliveryService;

	public List<Object> showAllProject() throws Exception {
		List<Project> projects = projectService
				.getAllProjects(getImplementation());

		List<Object> projectVOs = new ArrayList<Object>();
		if (null != projects && projects.size() > 0) {
			for (Project project : projects)
				projectVOs.add(addProjectVO(project));
		}
		return projectVOs;
	}

	public List<Object> showActiveProject() throws Exception {
		List<Project> projects = projectService.getActiveProjects(
				getImplementation(), ProjectStatus.Closed.getCode());
		List<Object> projectVOs = new ArrayList<Object>();
		if (null != projects && projects.size() > 0) {
			for (Project project : projects)
				projectVOs.add(addProjectVO(project));
		}
		return projectVOs;
	}

	public ProjectVO addProjectVO(Project project) {
		ProjectVO projectVO = new ProjectVO();
		projectVO.setProjectId(project.getProjectId());
		projectVO.setReferenceNumber(project.getReferenceNumber());
		projectVO.setProjectTitle(project.getProjectTitle());
		projectVO.setFromDate(DateFormat.convertDateToString(project
				.getStartDate().toString()));
		if (null != project.getEndDate())
			projectVO.setToDate(DateFormat.convertDateToString(project
					.getEndDate().toString()));
		if (project.getLookupDetail() != null)
			projectVO
					.setProjectType(project.getLookupDetail().getDisplayName());
		else
			projectVO.setProjectType("");

		projectVO
				.setCustomerName(null != project.getCustomer().getCompany() ? project
						.getCustomer().getCompany().getCompanyName()
						: project
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(project.getCustomer()
										.getPersonByPersonId().getLastName()));
		projectVO.setCoordinator(project.getPersonByProjectLead()
				.getFirstName().concat(" ")
				.concat(project.getPersonByProjectLead().getLastName()));
		projectVO.setCurrentStatus(ProjectStatus
				.get(project.getProjectStatus()).name());
		projectVO.setEditFlag(true);
		if (project.getProjectStatus() != null
				&& (byte) project.getProjectStatus() == (byte) ProjectStatus.Closed
						.getCode())
			projectVO.setEditFlag(false);

		return projectVO;
	}

	public ProjectVO convertProjectToVO(Project project) {
		ProjectVO projectVO = new ProjectVO();
		try {
			BeanUtils.copyProperties(projectVO, project);
			projectVO.setProjectId(project.getProjectId());
			projectVO.setReferenceNumber(project.getReferenceNumber());
			projectVO.setProjectTitle(project.getProjectTitle());
			projectVO.setStatusView(ProjectStatus.get(
					project.getProjectStatus()).name());
			projectVO.setFromDate(DateFormat.convertDateToString(project
					.getStartDate().toString()));
			if (null != project.getEndDate())
				projectVO.setToDate(DateFormat.convertDateToString(project
						.getEndDate().toString()));
			if (project.getLookupDetail() != null)
				projectVO.setProjectType(project.getLookupDetail()
						.getDisplayName());
			else
				projectVO.setProjectType("");

			if (project.getPersonByCreatedBy() != null) {
				projectVO.setCreatedByView(project.getPersonByCreatedBy()
						.getFirstName()
						+ " "
						+ project.getPersonByCreatedBy().getLastName());
			}

			if (project.getPersonByProjectLead() != null) {
				projectVO.setProjectLead(project.getPersonByProjectLead()
						.getFirstName()
						+ " "
						+ project.getPersonByProjectLead().getLastName());
			}

			projectVO.setCustomerName(null != project.getCustomer()
					.getCompany() ? project.getCustomer().getCompany()
					.getCompanyName() : project
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat(" ")
					.concat(project.getCustomer().getPersonByPersonId()
							.getLastName()));
			projectVO.setCustomerMobile(null != project.getCustomer()
					.getCompany() ? project.getCustomer().getCompany()
					.getCompanyPhone() : project.getCustomer()
					.getPersonByPersonId().getMobile());
			projectVO.setCustomerAddress(null != project.getCustomer()
					.getCompany() ? project.getCustomer().getCompany()
					.getCompanyAddress() : project.getCustomer()
					.getPersonByPersonId().getResidentialAddress());
			projectVO.setCoordinator(project.getPersonByProjectLead()
					.getFirstName().concat(" ")
					.concat(project.getPersonByProjectLead().getLastName()));
			projectVO.setCurrentStatus(ProjectStatus.get(
					project.getProjectStatus()).name());
			if (project.getProjectMileStones() != null
					&& project.getProjectMileStones().size() > 0) {
				List<ProjectMileStoneVO> projectMileStoneVOs = new ArrayList<ProjectMileStoneVO>();
				for (ProjectMileStone pms : project.getProjectMileStones()) {
					projectMileStoneVOs.add(convertMileStoneToVO(pms));
				}
				projectVO.setProjectMileStoneVOs(projectMileStoneVOs);
			}
			List<ProjectTaskSurrogate> ptslist = new ArrayList<ProjectTaskSurrogate>(
					project.getProjectTaskSurrogates());
			if (ptslist != null && ptslist.size() > 0
					&& ptslist.get(0).getProjectResources() != null
					&& ptslist.get(0).getProjectResources().size() > 0) {
				List<ProjectResourceVO> projectResourceVOs = new ArrayList<ProjectResourceVO>();
				List<ProjectResource> pRsources = new ArrayList<ProjectResource>(
						ptslist.get(0).getProjectResources());
				Collections.sort(pRsources, new Comparator<ProjectResource>() {
					public int compare(ProjectResource m1, ProjectResource m2) {
						return m1.getProjectResourceId().compareTo(
								m2.getProjectResourceId());
					}
				});
				int index = 0;
				for (ProjectResource pr : pRsources) {
					projectResourceVOs.add(convertResourceToVO(pr));
					if (index < 4) {
						if (null != pr.getProjectResourceDetails()) {
							List<ProjectResourceDetail> tempResourceDetails = new ArrayList<ProjectResourceDetail>(
									pr.getProjectResourceDetails());
							Collections.sort(tempResourceDetails,
									new Comparator<ProjectResourceDetail>() {
										public int compare(
												ProjectResourceDetail m1,
												ProjectResourceDetail m2) {
											return m1
													.getProjectResourceDetailId()
													.compareTo(
															m2.getProjectResourceDetailId());
										}
									});
							if (index == 0) {
								projectVO.setProjectManager(tempResourceDetails
										.get(0)
										.getPerson()
										.getFirstName()
										.concat(" "
												+ tempResourceDetails.get(0)
														.getPerson()
														.getLastName()));
							} else if (index == 1) {
								projectVO
										.setProjectDecorator(tempResourceDetails
												.get(0)
												.getPerson()
												.getFirstName()
												.concat(" "
														+ tempResourceDetails
																.get(0)
																.getPerson()
																.getLastName()));
							} else if (index == 2) {
								projectVO.setProjectHelper(tempResourceDetails
										.get(0)
										.getPerson()
										.getFirstName()
										.concat(" "
												+ tempResourceDetails.get(0)
														.getPerson()
														.getLastName()));
							} else if (index == 3) {
								projectVO.setSalesPerson(tempResourceDetails
										.get(0)
										.getPerson()
										.getFirstName()
										.concat(" "
												+ tempResourceDetails.get(0)
														.getPerson()
														.getLastName()));
							}
						}
					}
					index++;
				}
				projectVO.setProjectResourceVOs(projectResourceVOs);

			}

			// Payment Schedule
			List<ProjectPaymentScheduleVO> projectPaymentScheduleVOs = new ArrayList<ProjectPaymentScheduleVO>();
			List<BankReceipts> bankReceipts = null;
			for (ProjectPaymentSchedule projectPaymentSchedule : projectService
					.getProjectPaymentScheduleByProjectId(project
							.getProjectId())) {
				BankReceiptsVO receiptVO = new BankReceiptsVO();
				receiptVO.setRecordId(projectPaymentSchedule
						.getProjectPaymentScheduleId());
				receiptVO.setUseCase(projectPaymentSchedule.getClass()
						.getSimpleName());
				receiptVO.setImplementation(getImplementation());
				bankReceipts = bankReceiptsBL.getBankReceiptsService()
						.getFilteredBankReceipts(receiptVO);

				projectPaymentScheduleVOs
						.add(convertPaymentScheduleToVO(projectPaymentSchedule));

			}
			projectVO.setProjectPaymentScheduleVOs(projectPaymentScheduleVOs);

			// Payment Expense
			List<ProjectExpenseVO> projectExpenseVOs = new ArrayList<ProjectExpenseVO>();
			for (ProjectExpense projectExpense : projectService
					.getProjectExpenseByProjectId(project.getProjectId())) {
				projectExpenseVOs.add(convertExpenseToVO(projectExpense));

			}
			projectVO.setProjectExpenseVOs(projectExpenseVOs);

			if (project.getSupplier() != null
					&& project.getSupplier().getCompany() != null
					&& project.getSupplier().getCompany().getCompanyId() != null) {
				projectVO.setSupplierName(project.getSupplier().getCompany()
						.getCompanyName());
			} else if (project.getSupplier() != null
					&& project.getSupplier().getPerson() != null
					&& project.getSupplier().getPerson().getPersonId() != null) {
				projectVO
						.setSupplierName(project.getSupplier().getPerson()
								.getFirstName()
								+ " "
								+ project.getSupplier().getPerson()
										.getLastName());
			}

			// Project Inventory
			List<ProjectInventoryVO> projectInventoryVOs = new ArrayList<ProjectInventoryVO>();
			for (ProjectInventory projectInventory : projectService
					.getProjectInventoriesByProjectId(project.getProjectId())) {
				projectInventoryVOs.add(convertInventoryToVO(projectInventory));

			}
			projectVO.setProjectInventoryVOs(projectInventoryVOs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectVO;
	}

	// Project Milestone Entity to VO common conversion
	public ProjectMileStoneVO convertMileStoneToVO(ProjectMileStone pms) {
		ProjectMileStoneVO projectMileStoneVO = new ProjectMileStoneVO();

		try {
			BeanUtils.copyProperties(projectMileStoneVO, pms);
			projectMileStoneVO.setFromDate(DateFormat.convertDateToString(pms
					.getStartDate().toString()));
			projectMileStoneVO.setToDate(DateFormat.convertDateToString(pms
					.getEndDate().toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectMileStoneVO;
	}

	// Project Resource Entity to VO common conversion
	public ProjectResourceVO convertResourceToVO(ProjectResource pr) {
		ProjectResourceVO projectResourceVO = new ProjectResourceVO();

		try {
			BeanUtils.copyProperties(projectResourceVO, pr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectResourceVO;
	}

	// Project Payment Entity to VO common conversion
	public ProjectPaymentScheduleVO convertPaymentScheduleToVO(
			ProjectPaymentSchedule pr) {
		ProjectPaymentScheduleVO projectResourceVO = new ProjectPaymentScheduleVO();

		try {
			BeanUtils.copyProperties(projectResourceVO, pr);
			projectResourceVO.setPaymentDateView(DateFormat
					.convertDateToString(pr.getPaymentDate().toString()));
			if (pr.getReceiptReference() != null)
				projectResourceVO.setDescription(pr.getDescription()
						+ " [Receipt Reference == " + pr.getReceiptReference()
						+ "]");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectResourceVO;
	}

	// Project Expense Entity to VO common conversion
	public ProjectExpenseVO convertExpenseToVO(ProjectExpense pr) {
		ProjectExpenseVO projectResourceVO = new ProjectExpenseVO();

		try {
			BeanUtils.copyProperties(projectResourceVO, pr);
			projectResourceVO.setPaymentDateView(DateFormat
					.convertDateToString(pr.getPaymentDate().toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectResourceVO;
	}

	// Project Inventory Entity to VO common conversion
	public ProjectInventoryVO convertInventoryToVO(ProjectInventory pr) {
		ProjectInventoryVO projectInventoryVO = new ProjectInventoryVO();

		try {
			BeanUtils.copyProperties(projectInventoryVO, pr);
			if (pr.getIsPercentage() == null && pr.getDiscount() != null
					&& pr.getDiscount() > 0)
				pr.setIsPercentage(false);

			projectInventoryVO.setIsPercentage(pr.getIsPercentage());
			double discount = pr.getDiscount() != null ? pr.getDiscount() : 0.0;
			if (projectInventoryVO.getIsPercentage() != null
					&& projectInventoryVO.getIsPercentage()) {
				discount = (pr.getQuantity() * pr.getUnitRate() * pr
						.getDiscount()) / 100;
			}

			projectInventoryVO.setTotalAmount((pr.getQuantity() * pr
					.getUnitRate()) - (discount));
			if (pr.getShelf() != null) {
				projectInventoryVO.setStoreName(pr.getShelf().getShelf()
						.getAisle().getStore().getStoreName()
						+ " >> "
						+ pr.getShelf().getShelf().getAisle().getSectionName()
						+ " >> "
						+ pr.getShelf().getShelf().getName()
						+ " >> "
						+ pr.getShelf().getName());
			} else {
				projectInventoryVO.setStoreName("");
			}
			projectInventoryVO.setSuggestedRetailPrice(stockBL
					.getProductPricingBL().getSuggestedRetailPriceByProduct(
							pr.getProduct().getProductId()));
			if (pr.getStatus() != null && (byte) pr.getStatus() != (byte) 0)
				projectInventoryVO.setStatusName(ProjectPaymentStatus.get(
						pr.getStatus()).name());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectInventoryVO;
	}

	// Save Project
	public void saveProject(Project project,
			List<ProjectResource> projectResources,
			List<ProjectResourceDetail> projectResourceDetails,
			List<ProjectResourceDetail> projectResourceDetailsOld,
			List<ProjectResource> deleteResources,
			List<ProjectMileStone> projectMileStones,
			List<ProjectMileStone> deleteMileStones,
			List<ProjectPaymentSchedule> projectPaymentSchedules,
			List<ProjectPaymentSchedule> deletePaymentSchedules,
			List<ProjectExpense> projectExpenses,
			List<ProjectExpense> deleteProjectExpenses,
			List<ProjectInventory> projectInventories,
			List<ProjectInventory> deleteProjectInventories,
			List<ProjectInventory> editProjectInventories, boolean updateStock,
			String saveType, ProjectVO vo, PettyCash pettyCashExpense,
			List<PettyCashDetail> pettyCashDetails) throws Exception {
		boolean modify = null == project.getProjectId() ? false : true;
		project.setImplementation(getImplementation());
		ProjectTaskSurrogate pts = null;
		List<Stock> increaseStocks = new ArrayList<Stock>();

		// Save all the project history into new system
		if (vo != null
				&& vo.getEditProject() != null
				&& (byte) ProjectStatus.get(project.getProjectStatus())
						.getCode() > (byte) ProjectStatus.get(
						vo.getEditProject().getProjectStatus()).getCode()) {

			vo.getEditProject().setProject(project);
			saveProjectHistory(vo.getEditProject());

		}

		if (!modify)
			SystemBL.saveReferenceStamp(Project.class.getName(),
					getImplementation());
		else {
			if (null != projectResourceDetailsOld
					&& projectResourceDetailsOld.size() > 0)
				projectService
						.deleteProjectResourceDetail(projectResourceDetailsOld);
			if (null != deleteResources && deleteResources.size() > 0)
				projectService.deleteProjectResource(deleteResources);

			if (null != deleteMileStones && deleteMileStones.size() > 0){
				try{
					projectService.deleteProjectMilestones(deleteMileStones);
				}catch(Exception e){
					System.out.println("Milestone can not be deleted");
				}
			}

			if (null != deletePaymentSchedules
					&& deletePaymentSchedules.size() > 0)
				projectService
						.deleteProjectPaymentSchedules(deletePaymentSchedules);

			if (null != deleteProjectExpenses
					&& deleteProjectExpenses.size() > 0) {
				// petty cash work
				PettyCash pettyCash = pettyCashBL.getPettyCashService()
						.getPettyCashByUseCase(project.getProjectId(),
								Project.class.getSimpleName());
				if (null != pettyCash)
					pettyCashBL.deletePettyCashDirect(pettyCash);
				projectService.deleteProjectExpenses(deleteProjectExpenses);
			}

			if (null != deleteProjectInventories
					&& deleteProjectInventories.size() > 0) {

				projectService
						.deleteProjectInventories(deleteProjectInventories);
			}

			List<ProjectTaskSurrogate> ptses = new ArrayList<ProjectTaskSurrogate>(
					project.getProjectTaskSurrogates());
			pts = ptses.get(0);

		}

		projectService.saveProject(project, projectResources,
				projectResourceDetails, projectMileStones, pts,
				projectPaymentSchedules, projectExpenses, projectInventories,
				null, null, null);

		if (null != pettyCashExpense)
			pettyCashBL.savePettyCashDirect(pettyCashExpense, null,
					pettyCashDetails, null, null);

		savProjectDocuments(project, modify);
		saveProjectImages(project, modify);

		// Payment Alert

		if (null != projectPaymentSchedules
				&& projectPaymentSchedules.size() > 0) {
			LocalDate currentDate = DateTime.now().toLocalDate();
			for (ProjectPaymentSchedule projectPaymentDetail : projectPaymentSchedules) {
				LocalDate paymentDate = new DateTime(
						projectPaymentDetail.getPaymentDate()).toLocalDate();
				if (paymentDate.compareTo(currentDate) <= 0
						&& (byte) projectPaymentDetail.getStatus() != (byte) ProjectPaymentStatus.Paid
								.getCode())
					createPaymentAlert(projectPaymentDetail);
			}
		}

		if (saveType != null && saveType.equalsIgnoreCase("REQUISITION")) {
			if (projectInventories != null && projectInventories.size() > 0) {
				createRequisition(project, projectInventories);
			}

		}

		// Save all the project history into new system
		if (vo != null && vo.getEditProject() != null) {

			// If status changed into delivered
			if ((byte) ProjectStatus.Delivered.getCode() == (byte) project
					.getProjectStatus()) {
				// Delete old saved entries
				SalesDeliveryNote toDelete = salesInvoiceBL
						.getSalesDeliveryNoteBL()
						.getSalesDeliveryNoteService()
						.getSalesDeliveryByProject(
								vo.getEditProject().getProjectId());
				if (toDelete != null)
					salesInvoiceBL.getSalesDeliveryNoteBL()
							.deleteSalesDeliveryNoteDirect(toDelete);

				// Save New one
				SalesDeliveryNote deliveryNote = manipulateSalesDelivery(project);
				salesInvoiceBL.getSalesDeliveryNoteBL().saveDeliveryNote(
						deliveryNote,
						new ArrayList<SalesDeliveryDetail>(deliveryNote
								.getSalesDeliveryDetails()), null, null, null,
						null, null, null, null, null, null);
			}

			if ((byte) ProjectStatus.Requested.getCode() == (byte) project
					.getProjectStatus()) {
				// Delete old saved entries
				SalesOrder toDelete = salesInvoiceBL
						.getSalesDeliveryNoteBL()
						.getSalesOrderBL()
						.getSalesOrderService()
						.getSalesOrderByProject(
								vo.getEditProject().getProjectId());
				if (toDelete != null)
					salesInvoiceBL.getSalesDeliveryNoteBL().getSalesOrderBL()
							.deleteSalesOrderDirect(toDelete);
				// Save New one
				SalesOrder salesOrder = manipulateSalesOrder(project);
				salesInvoiceBL
						.getSalesDeliveryNoteBL()
						.getSalesOrderBL()
						.saveSalesOrderDirect(
								salesOrder,
								new ArrayList<SalesOrderDetail>(salesOrder
										.getSalesOrderDetails()), null, null,
								null);
			}

		}

	}

	public SalesDeliveryNote manipulateSalesDeliveryByOnlyProject(
			Project project, List<ProjectInventory> projectInventories)
			throws Exception {
		SalesDeliveryNote salesDeliveryNote = new SalesDeliveryNote();
		salesDeliveryNote.setProject(project);
		salesDeliveryNote.setReferenceNumber(salesInvoiceBL
				.getSalesDeliveryNoteBL().generateReferenceNumber());
		salesDeliveryNote.setStatusNote((byte) 1);
		salesDeliveryNote.setDeliveryDate(new Date());
		salesDeliveryNote.setCustomer(project.getCustomer());
		List<ShippingDetail> shippings = salesInvoiceBL.getCustomerBL()
				.getCustomerService()
				.getCustomerShippingSite(project.getCustomer().getCustomerId());
		if (shippings != null && shippings.size() > 0)
			salesDeliveryNote.setShippingDetail(shippings.get(0));
		salesDeliveryNote.setImplementation(getImplementation());
		salesDeliveryNote.setCurrency(transactionBL
				.getCurrency(getImplementation()));
		salesDeliveryNote.setDescription(project.getReferenceNumber() + "  "
				+ project.getProjectTitle());
		salesDeliveryNote.setStatus(O2CProcessStatus.Open.getCode());

		List<SalesDeliveryDetail> deliveryDetails = new ArrayList<SalesDeliveryDetail>();
		SalesDeliveryDetail salesDeliveryDetail = null;

		for (ProjectInventory projectInventory : projectInventories) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			salesDeliveryDetail.setProduct(projectInventory.getProduct());
			salesDeliveryDetail.setShelf(projectInventory.getShelf());
			List<Object> stocks = stockBL
					.showProductStockDetail(projectInventory.getProduct()
							.getProductId());
			if (stocks != null && stocks.size() > 0) {
				StockVO stok = (StockVO) stocks.get(0);
				Shelf shelf = new Shelf();
				shelf.setShelfId(stok.getShelfId());
				salesDeliveryDetail.setShelf(shelf);
			} else {
				try {

				} catch (Exception e) {
					throw new Exception("Stock not available", e);
				}
			}
			salesDeliveryDetail.setIsPercentage(projectInventory
					.getIsPercentage());
			salesDeliveryDetail.setUnitRate(projectInventory.getUnitRate());
			salesDeliveryDetail.setDiscount(projectInventory.getDiscount());
			salesDeliveryDetail.setDeliveryQuantity(projectInventory
					.getQuantity());
			salesDeliveryDetail.setDeliveryQuantity(projectInventory
					.getQuantity());
			salesDeliveryDetail.setPackageUnit(projectInventory.getQuantity());
			deliveryDetails.add(salesDeliveryDetail);
		}

		if (getImplementation().getServiceProduct() != null
				&& getImplementation().getServiceProduct() > 0) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			Product product = new Product();
			product = productBL.getProductService().getProductById(
					getImplementation().getServiceProduct());
			salesDeliveryDetail.setProduct(product);
			salesDeliveryDetail.setShelf(product.getShelf());
			salesDeliveryDetail.setUnitRate(0.0);
			salesDeliveryDetail.setDeliveryQuantity(1.0);
			salesDeliveryDetail.setPackageUnit(1.0);
			deliveryDetails.add(salesDeliveryDetail);
		}

		salesDeliveryNote
				.setSalesDeliveryDetails(new HashSet<SalesDeliveryDetail>(
						deliveryDetails));
		return salesDeliveryNote;
	}

	public SalesDeliveryNote manipulateSalesDelivery(Project project)
			throws Exception {
		SalesDeliveryNote salesDeliveryNote = new SalesDeliveryNote();
		salesDeliveryNote.setProject(project);
		salesDeliveryNote.setReferenceNumber(salesInvoiceBL
				.getSalesDeliveryNoteBL().generateReferenceNumber());
		salesDeliveryNote.setStatusNote((byte) 1);
		salesDeliveryNote.setDeliveryDate(new Date());
		salesDeliveryNote.setCustomer(project.getCustomer());
		List<ShippingDetail> shippings = salesInvoiceBL.getCustomerBL()
				.getCustomerService()
				.getCustomerShippingSite(project.getCustomer().getCustomerId());
		if (shippings != null && shippings.size() > 0)
			salesDeliveryNote.setShippingDetail(shippings.get(0));
		salesDeliveryNote.setImplementation(getImplementation());
		salesDeliveryNote.setCurrency(transactionBL
				.getCurrency(getImplementation()));
		salesDeliveryNote.setDescription(project.getReferenceNumber() + "  "
				+ project.getProjectTitle());
		salesDeliveryNote.setStatus(O2CProcessStatus.Open.getCode());

		List<ProjectTaskSurrogate> ptses = new ArrayList<ProjectTaskSurrogate>(
				project.getProjectTaskSurrogates());
		ProjectTaskSurrogate pts = ptses.get(0);
		List<SalesDeliveryDetail> deliveryDetails = new ArrayList<SalesDeliveryDetail>();
		SalesDeliveryDetail salesDeliveryDetail = null;

		for (ProjectInventory projectInventory : pts.getProjectInventories()) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			salesDeliveryDetail.setProduct(projectInventory.getProduct());
			salesDeliveryDetail.setShelf(projectInventory.getShelf());
			List<Object> stocks = stockBL
					.showProductStockDetail(projectInventory.getProduct()
							.getProductId());
			if (stocks != null && stocks.size() > 0) {
				StockVO stok = (StockVO) stocks.get(0);
				Shelf shelf = new Shelf();
				shelf.setShelfId(stok.getShelfId());
				salesDeliveryDetail.setShelf(shelf);
			} else {
				try {

				} catch (Exception e) {
					throw new Exception("Stock not available", e);
				}
			}

			salesDeliveryDetail.setIsPercentage(projectInventory
					.getIsPercentage());
			salesDeliveryDetail.setUnitRate(projectInventory.getUnitRate());
			salesDeliveryDetail.setDiscount(projectInventory.getDiscount());
			salesDeliveryDetail.setDeliveryQuantity(projectInventory
					.getQuantity());
			salesDeliveryDetail.setPackageUnit(projectInventory.getQuantity());
			deliveryDetails.add(salesDeliveryDetail);
		}
		if (project.getProjectTasks() != null)
			for (ProjectTask task : project.getProjectTasks()) {
				recursiveProjectTaskForDeliveryDetail(task, deliveryDetails);
			}

		if (getImplementation().getServiceProduct() != null
				&& getImplementation().getServiceProduct() > 0) {
			salesDeliveryDetail = new SalesDeliveryDetail();
			Double amount = 0.0;
			for (SalesDeliveryDetail sd : deliveryDetails) {
				double discount = sd.getDiscount() != null ? sd.getDiscount()
						: 0.0;
				if (sd.getIsPercentage() != null && sd.getIsPercentage()) {
					discount = (sd.getDeliveryQuantity() * sd.getUnitRate() * sd
							.getDiscount()) / 100;

				}
				amount += (sd.getDeliveryQuantity() * sd.getUnitRate())
						- (discount);
			}
			Product product = new Product();
			product = productBL.getProductService().getProductById(
					getImplementation().getServiceProduct());
			salesDeliveryDetail.setProduct(product);
			salesDeliveryDetail.setShelf(product.getShelf());
			salesDeliveryDetail.setUnitRate(project.getProjectValue() - amount);
			salesDeliveryDetail.setDeliveryQuantity(1.0);
			if (salesDeliveryDetail.getUnitRate() > 0.0)
				deliveryDetails.add(salesDeliveryDetail);
		}

		salesDeliveryNote
				.setSalesDeliveryDetails(new HashSet<SalesDeliveryDetail>(
						deliveryDetails));
		return salesDeliveryNote;
	}

	public SalesOrder manipulateSalesOrder(Project project) throws Exception {
		SalesOrder salesOrder = new SalesOrder();
		List<SalesOrderDetail> salesOrderDetails = new ArrayList<SalesOrderDetail>();
		salesOrder.setProject(project);
		salesOrder.setReferenceNumber(SystemBL.getReferenceStamp(
				SalesOrder.class.getName(), getImplementation()));
		salesOrder.setPerson(project.getPersonByCreatedBy());
		salesOrder.setOrderDate(project.getStartDate());
		if (project.getEndDate() == null)
			salesOrder
					.setExpiryDate(DateFormat.addMonth(project.getStartDate()));
		else
			salesOrder.setExpiryDate(project.getEndDate());

		Customer customer = salesInvoiceBL.getCustomerBL().getCustomerService()
				.getCustomerDetails(project.getCustomer().getCustomerId());
		List<ShippingDetail> shipDetails = new ArrayList<ShippingDetail>(
				customer.getShippingDetails());
		salesOrder.setShippingDetail(shipDetails.get(0));
		salesOrder.setCurrency(transactionBL.getCurrency());
		salesOrder.setImplementation(getImplementation());
		salesOrder.setCustomer(project.getCustomer());
		salesOrder.setOrderStatus(O2CProcessStatus.Open.getCode());
		salesOrder.setDescription(project.getReferenceNumber() + " "
				+ project.getProjectTitle());

		List<ProjectTaskSurrogate> ptses = new ArrayList<ProjectTaskSurrogate>(
				project.getProjectTaskSurrogates());
		ProjectTaskSurrogate pts = ptses.get(0);
		SalesOrderDetail salesOrderDetail = null;
		for (ProjectInventory projectInventory : pts.getProjectInventories()) {
			salesOrderDetail = new SalesOrderDetail();
			salesOrderDetail.setProduct(projectInventory.getProduct());
			salesOrderDetail
					.setIsPercentage(projectInventory.getIsPercentage());
			salesOrderDetail.setUnitRate(projectInventory.getUnitRate());
			salesOrderDetail.setDiscount(projectInventory.getDiscount());
			salesOrderDetail.setOrderQuantity(projectInventory.getQuantity());
			salesOrderDetail.setPackageUnit(projectInventory.getQuantity());
			salesOrderDetails.add(salesOrderDetail);
		}

		salesOrder.setSalesOrderDetails(new HashSet<SalesOrderDetail>(
				salesOrderDetails));
		return salesOrder;
	}

	public List<SalesDeliveryDetail> recursiveProjectTaskForDeliveryDetail(
			ProjectTask task, List<SalesDeliveryDetail> deliveryDetails)
			throws Exception {
		SalesDeliveryDetail salesDeliveryDetail = null;
		if (task != null) {
			task = projectTaskService.getProjectTaskById(task
					.getProjectTaskId());
			if (task.getProjectTasks() != null
					&& task.getProjectTasks().size() > 0) {
				for (ProjectTask pTask : task.getProjectTasks()) {
					pTask = projectTaskService.getProjectTaskById(pTask
							.getProjectTaskId());
					recursiveProjectTaskForDeliveryDetail(pTask,
							deliveryDetails);
				}
			}
			List<ProjectTaskSurrogate> ptses = new ArrayList<ProjectTaskSurrogate>(
					task.getProjectTaskSurrogates());
			ProjectTaskSurrogate pts = ptses.get(0);
			for (ProjectInventory projectInventory : pts
					.getProjectInventories()) {
				salesDeliveryDetail = new SalesDeliveryDetail();
				salesDeliveryDetail.setProduct(projectInventory.getProduct());
				List<Object> stocks = stockBL
						.showProductStockDetail(projectInventory.getProduct()
								.getProductId());
				if (stocks != null && stocks.size() > 0) {
					StockVO stok = (StockVO) stocks.get(0);
					Shelf shelf = new Shelf();
					shelf.setShelfId(stok.getShelfId());
					salesDeliveryDetail.setShelf(shelf);
				} else {
					try {

					} catch (Exception e) {
						throw new Exception("Stock not available", e);
					}
				}
				salesDeliveryDetail.setIsPercentage(projectInventory
						.getIsPercentage());
				salesDeliveryDetail.setUnitRate(projectInventory.getUnitRate());
				salesDeliveryDetail.setDiscount(projectInventory.getDiscount());
				salesDeliveryDetail.setDeliveryQuantity(projectInventory
						.getQuantity());
				deliveryDetails.add(salesDeliveryDetail);
			}
		}

		return deliveryDetails;
	}

	public void saveProjectHistory(Project project) throws Exception {
		try {
			project.setProjectId(null);
			ProjectTaskSurrogate pts = new ArrayList<ProjectTaskSurrogate>(
					project.getProjectTaskSurrogates()).get(0);
			
			pts.setProjectTaskSurrogateId(null);
			List<ProjectResource> projectResources = new ArrayList<ProjectResource>(
					pts.getProjectResources());
			List<ProjectResourceDetail> projectResourceDetails = new ArrayList<ProjectResourceDetail>();
			for (ProjectResource projectResource : projectResources) {
				projectResource.setProjectResourceId(null);
				for (ProjectResourceDetail projectResourceDetail : projectResourceDetails) {
					projectResourceDetail.setProjectResourceDetailId(null);
					projectResourceDetail.setProjectResource(projectResource);
					projectResourceDetails.add(projectResourceDetail);
				}
			}

			List<ProjectMileStone> projectMileStones = new ArrayList<ProjectMileStone>(
					project.getProjectMileStones());
			for (ProjectMileStone projectMileStone : projectMileStones) {
				projectMileStone.setProjectMileStoneId(null);
			}

			List<ProjectPaymentSchedule> projectPaymentSchedules = new ArrayList<ProjectPaymentSchedule>(
					pts.getProjectPaymentSchedules());
			for (ProjectPaymentSchedule projectPaymentSchedule : projectPaymentSchedules) {
				projectPaymentSchedule.setProjectPaymentScheduleId(null);
			}

			List<ProjectExpense> projectExpenses = new ArrayList<ProjectExpense>(
					pts.getProjectExpenses());
			for (ProjectExpense projectExpense : projectExpenses) {
				projectExpense.setProjectExpenseId(null);
			}

			List<ProjectInventory> projectInventorys = new ArrayList<ProjectInventory>(
					pts.getProjectInventories());
			for (ProjectInventory projectInventory : projectInventorys) {
				projectInventory.setProjectInventoryId(null);
			}

			List<ProjectNotes> projectNotes = new ArrayList<ProjectNotes>(
					pts.getProjectNoteses());
			for (ProjectNotes projectNote : projectNotes) {
				projectNote.setProjectNotesId(null);
			}

			List<ProjectDiscussion> projectDiscussions = new ArrayList<ProjectDiscussion>(
					pts.getProjectDiscussions());
			for (ProjectDiscussion projectDiscussion : projectDiscussions) {
				projectDiscussion.setProjectDiscussionId(null);
			}

			List<ProjectDelivery> projectDeliverys = new ArrayList<ProjectDelivery>(
					pts.getProjectDeliveries());
			for (ProjectDelivery projectDelivery : projectDeliverys) {
				projectDelivery.setProjectDeliveryId(null);
			}

			projectService.saveProject(project, projectResources,
					projectResourceDetails, projectMileStones, null,
					projectPaymentSchedules, projectExpenses,
					projectInventorys, projectNotes, projectDiscussions,
					projectDeliverys);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createRequisition(Project project,
			List<ProjectInventory> inventories) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		MaterialRequisition materialRequisition = new MaterialRequisition();
		List<MaterialRequisitionDetail> details = new ArrayList<MaterialRequisitionDetail>();
		materialRequisition.setRequisitionDate(new Date());
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		materialRequisition.setPerson(person);
		JobAssignment jobAssignment = jobAssignmentBL.getJobAssignmentService()
				.getJobAssignmentByEmployee(user.getPersonId());
		Store store = stockBL
				.getStoreBL()
				.getStoreService()
				.getStoreByDepartmentLocation(
						jobAssignment.getCmpDeptLocation().getCmpDeptLocId());
		materialRequisition.setStore(store);
		materialRequisition.setRequisitionStatus(RequisitionStatus.Open
				.getCode());
		materialRequisition.setUseCase(project.getClass().getSimpleName());
		materialRequisition.setRecordId(project.getProjectId());
		materialRequisition.setCreatedDate(new Date());
		materialRequisition.setDescription(project.getReferenceNumber());
		materialRequisition.setReferenceNumber(SystemBL.getReferenceStamp(
				MaterialRequisition.class.getName(),
				materialRequisitionBL.getImplementation()));

		MaterialRequisitionDetail materialRequisitionDetail = null;
		for (ProjectInventory projectInventory : inventories) {
			if (projectInventory.getStatus() == null
					|| (byte) projectInventory.getStatus() == (byte) ProjectStatus.Closed
							.getCode())
				continue;

			materialRequisitionDetail = new MaterialRequisitionDetail();
			materialRequisitionDetail.setProduct(projectInventory.getProduct());
			materialRequisitionDetail.setQuantity(projectInventory
					.getQuantity());
			materialRequisitionDetail
					.setMaterialRequisition(materialRequisition);
			details.add(materialRequisitionDetail);
			materialRequisitionDetail.setProjectInventory(projectInventory);
			projectInventory.setStatus(ProjectStatus.Closed.getCode());
		}

		if (details != null && details.size() > 0)
			materialRequisitionBL.saveMaterialRequisition(materialRequisition,
					details, null);
	}

	public void performeJVTransaction(Project project,
			List<ProjectInventory> inventories) throws Exception {
		Combination combination = productBL
				.getCombinationBL()
				.getCombinationService()
				.getCombinationAllAccountDetail(
						getImplementation().getExpenseAccount());
		// Transaction
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double total = 0.0;
		for (ProjectInventory detail : inventories) {
			double discount = detail.getDiscount() != null ? detail
					.getDiscount() : 0.0;
			if (detail.getIsPercentage() != null && detail.getIsPercentage()) {
				discount = (detail.getQuantity() * detail.getUnitRate() * detail
						.getDiscount()) / 100;
			}
			transactionDetails.add(transactionBL.createTransactionDetail(
					detail.getQuantity() * detail.getUnitRate(),
					productBL.getProductInventory(
							detail.getProduct().getProductId())
							.getCombinationId(), TransactionType.Credit
							.getCode(), "", null, null));
			total += (detail.getQuantity() * detail.getUnitRate()) - (discount);
		}

		transactionDetails.add(transactionBL.createTransactionDetail(total,
				combination.getCombinationId(),
				TransactionType.Debit.getCode(), "", null, null));

		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "", new Date(), transactionBL
				.getCategory("Project Inventory"), project.getProjectId(),
				Project.class.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);

	}

	private void savProjectDocuments(Project project, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(project,
				"projectDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, project.getProjectId(), "Project", "projectDocs");
			docVO.setDirStruct(dirStucture);
		}
		documentBL.saveUploadDocuments(docVO);
	}

	public void saveProjectImages(Project project, boolean editFlag)
			throws Exception {
		DocumentVO docVO2 = new DocumentVO();
		docVO2.setDisplayPane("projectImages");
		docVO2.setEdit(editFlag);
		docVO2.setObject(project);
		imageBL.saveUploadedImages(project, docVO2);
	}

	private Image populateImage(Map.Entry<String, FileVO> entry, Object object)
			throws Exception {
		Image image = new Image();
		image.setName(entry.getKey());
		image.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		image.setTableName(name[name.length - 1]);

		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		image.setRecordId((Long) method.invoke(object, null));

		return image;
	}

	private Document populateDocument(Document document,
			Map.Entry<String, FileVO> entry, Object object) throws Exception {
		document.setName(entry.getKey());
		document.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		document.setTableName(name[name.length - 1]);
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		document.setRecordId((Long) method.invoke(object, null));
		return document;
	}

	// Delete Project
	public void deleteProject(Project project) throws Exception {
		if (project.getProjects() != null) {
			for (Project prject : project.getProjects()) {
				deleteProject(this.getProjectService().getBasicProjectById(
						prject.getProjectId()));
			}
		}
		documentBL.getDocumentService().deleteDocuments(
				documentBL.getDocumentService().getChildDocumentsByRecordId(
						project.getProjectId(), "Project"));
		documentBL.getDocumentService().deleteDocuments(
				documentBL.getDocumentService().getParentDocumentsByRecordId(
						project.getProjectId(), "Project"));
		List<ProjectResource> projectResources = null;
		List<ProjectResourceDetail> projectResourceDetails = null;
		List<ProjectMileStone> projectMileStones = null;
		List<ProjectPaymentSchedule> projectPaymentSchedules = null;
		List<ProjectExpense> projectExpenses = null;
		List<ProjectDiscussion> projectDiscussions = null;
		List<ProjectNotes> projectNotes = null;
		List<ProjectTaskSurrogate> ptslist = new ArrayList<ProjectTaskSurrogate>(
				project.getProjectTaskSurrogates());
		List<ProjectInventory> projectInventories = null;
		ProjectTaskSurrogate pts = null;
		if (ptslist != null && ptslist.size() > 0) {
			projectResources = new ArrayList<ProjectResource>(ptslist.get(0)
					.getProjectResources());
			projectResourceDetails = new ArrayList<ProjectResourceDetail>();
			for (ProjectResource projectResource : projectResources) {
				if (null != projectResource.getProjectResourceDetails()
						&& projectResource.getProjectResourceDetails().size() > 0) {
					projectResourceDetails
							.addAll(new ArrayList<ProjectResourceDetail>(
									projectResource.getProjectResourceDetails()));

				}
			}
			projectPaymentSchedules = new ArrayList<ProjectPaymentSchedule>(
					ptslist.get(0).getProjectPaymentSchedules());
			projectExpenses = new ArrayList<ProjectExpense>(ptslist.get(0)
					.getProjectExpenses());
			projectDiscussions = new ArrayList<ProjectDiscussion>(ptslist
					.get(0).getProjectDiscussions());
			projectNotes = new ArrayList<ProjectNotes>(ptslist.get(0)
					.getProjectNoteses());
			projectInventories = new ArrayList<ProjectInventory>(ptslist.get(0)
					.getProjectInventories());
		}
		projectMileStones = new ArrayList<ProjectMileStone>(
				project.getProjectMileStones());

		SalesDeliveryNote toDeleteDelivery = salesInvoiceBL
				.getSalesDeliveryNoteBL().getSalesDeliveryNoteService()
				.getSalesDeliveryByProject(project.getProjectId());
		if (toDeleteDelivery != null) {
			projectService.getProjectDAO().evict(toDeleteDelivery.getProject());
			salesInvoiceBL.getSalesDeliveryNoteBL()
					.deleteSalesDeliveryNoteDirect(toDeleteDelivery);
		}

		SalesOrder toDelete = salesInvoiceBL.getSalesDeliveryNoteBL()
				.getSalesOrderBL().getSalesOrderService()
				.getSalesOrderByProject(project.getProjectId());
		if (toDelete != null) {
			projectService.getProjectDAO().evict(toDelete.getProject());
			salesInvoiceBL.getSalesDeliveryNoteBL().getSalesOrderBL()
					.deleteSalesOrderDirect(toDelete);
		}

		projectService.deleteProject(project, projectResources,
				projectResourceDetails, projectMileStones,
				projectPaymentSchedules, projectExpenses, projectDiscussions,
				projectNotes, projectInventories);

	}

	public void projectDepositDirectPayment(ProjectVO vo) {

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;

			List<Currency> currencyList = null;
			if (vo.getObject() != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplementation());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplementation());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(vo.getRecordId());
				directPayment.setUseCase(vo.getUseCase());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				ProjectVO projectVO = getProjectDepositDirectPaymentTransaction(vo);
				directPayDetails.addAll(projectVO.getDirectPayments());
				directPayment.setCustomer(projectVO.getCustomer());
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public ProjectVO getProjectDepositDirectPaymentTransaction(
			ProjectVO projectVO) throws Exception {
		List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
		Project project = (Project) projectVO.getObject();
		DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
		directPaymentDetail.setAmount(project.getSecurityDeposit());
		directPayDetails.add(directPaymentDetail);
		projectVO.setDirectPayments(directPayDetails);
		projectVO.setCustomer(project.getCustomer());

		return projectVO;
	}

	public List<ProjectDiscussionVO> getAllProjectDiscussions(Long projectId,
			Long projectTaskId) {
		List<ProjectDiscussion> projectDiscussions = null;
		List<ProjectDiscussionVO> projectDiscussionVOs = null;
		try {
			if (projectId != null && projectId > 0 && projectTaskId == null) {
				projectDiscussions = projectService
						.getAllProjectDiscussionByProject(projectId);
			} else if (projectTaskId != null && projectTaskId > 0
					&& projectId == null) {
				projectDiscussions = projectService
						.getAllProjectDiscussionByTask(projectTaskId);
			}
			projectDiscussionVOs = new ArrayList<ProjectDiscussionVO>();
			for (ProjectDiscussion projectDiscussion : projectDiscussions) {
				projectDiscussionVOs
						.add(convertProjectDiscussionToVO(projectDiscussion));
			}
		} catch (Exception ex) {

			ex.printStackTrace();
		}
		return projectDiscussionVOs;
	}

	public ProjectDiscussionVO convertProjectDiscussionToVO(ProjectDiscussion pd) {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		ProjectDiscussionVO projectDiscussionVO = new ProjectDiscussionVO();
		User user = (User) sessionObj.get("USER");

		try {
			BeanUtils.copyProperties(projectDiscussionVO, pd);
			if ((long) projectDiscussionVO.getPerson().getPersonId() == (long) user
					.getPersonId())
				projectDiscussionVO.setSignedUser(true);

			projectDiscussionVO.setTime(DateFormat.getTimeOnlyWithSeconds(pd
					.getCreatedDate()));
			projectDiscussionVO.setDate(DateFormat.convertDateToString((pd
					.getCreatedDate().toString())));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectDiscussionVO;
	}

	public void saveProjectDiscussion(ProjectDiscussion projectDiscussion) {
		projectService.saveProjectDiscussion(projectDiscussion);
	}

	public List<ProjectNotesVO> getAllProjectNotes(Long projectId,
			Long projectTaskId) {
		List<ProjectNotes> projectNotesList = null;
		List<ProjectNotesVO> projectNotesVOs = null;
		try {
			if (projectId != null && projectId > 0 && projectTaskId == null) {
				projectNotesList = projectService
						.getAllProjectNotesByProject(projectId);
			} else if (projectTaskId != null && projectTaskId > 0
					&& projectId == null) {
				projectNotesList = projectService
						.getAllProjectNotesByTask(projectTaskId);
			}
			projectNotesVOs = new ArrayList<ProjectNotesVO>();
			for (ProjectNotes projectNote : projectNotesList) {
				projectNotesVOs.add(convertProjectNotesToVO(projectNote));
			}
		} catch (Exception ex) {

			ex.printStackTrace();
		}
		return projectNotesVOs;
	}

	public ProjectNotesVO convertProjectNotesToVO(ProjectNotes pd) {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		ProjectNotesVO projectNotesVO = new ProjectNotesVO();
		User user = (User) sessionObj.get("USER");

		try {
			BeanUtils.copyProperties(projectNotesVO, pd);
			if ((long) projectNotesVO.getPerson().getPersonId() == (long) user
					.getPersonId())
				projectNotesVO.setSignedUser(true);

			projectNotesVO.setTime(DateFormat.getTimeOnlyWithSeconds(pd
					.getCreatedDate()));
			projectNotesVO.setDate(DateFormat.convertDateToString((pd
					.getCreatedDate().toString())));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectNotesVO;
	}

	public void saveProjectNote(ProjectNotes projectNote) {
		projectService.saveProjectNotes(projectNote);
	}

	public void paymentAlertByJobSheduler() {
		try {
			List<ProjectPaymentSchedule> projectPaymentSchedules = projectService
					.getProjectPaymentScheduleForScheduler();
			LocalDate currentDate = DateTime.now().toLocalDate();
			for (ProjectPaymentSchedule projectPaymentDetail : projectPaymentSchedules) {
				LocalDate paymentDate = new DateTime(
						projectPaymentDetail.getPaymentDate()).toLocalDate();
				if (paymentDate.compareTo(currentDate) <= 0
						&& (byte) projectPaymentDetail.getStatus() != (byte) ProjectPaymentStatus.Paid
								.getCode())
					createPaymentAlert(projectPaymentDetail);
			}

		} catch (Exception e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createPaymentAlert(ProjectPaymentSchedule projectPaymentDetail)
			throws Exception {
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				projectPaymentDetail.getProjectPaymentScheduleId(),
				ProjectPaymentSchedule.class.getSimpleName());
		if (null != alert) {
			alert.setIsActive(true);
			alertBL.commonSaveAlert(alert);
		} else {
			List<ProjectPaymentSchedule> projectPaymentDetails = new ArrayList<ProjectPaymentSchedule>();
			projectPaymentDetails.add(projectPaymentDetail);
			alertBL.alertProjectPayment(projectPaymentDetails);
		}
	}

	public PointOfSaleVO showSalesDetailsByReferece(String referenceNumber) {
		PointOfSaleVO salesVO = null;
		try {
			BankReceipts bankReceipts = bankReceiptsBL.getBankReceiptsService()
					.getReceiptBySalesReference(referenceNumber,
							getImplementation());
			Credit credit = creditBL
					.getSalesReturnBySalesReference(referenceNumber);
			if (credit != null) {
				salesVO = new PointOfSaleVO();
				salesVO.setReferenceNumber("Return is already processed for this reference [ "
						+ referenceNumber + "]");
				return salesVO;
			}
			PointOfSale pos = null;
			if (bankReceipts == null || bankReceipts.getUseCase() == null) {
				pos = pointOfSaleBL.getPointOfSaleService()
						.getPointOfDetailByReference(getImplementation(),
								referenceNumber);
				if (pos != null)
					bankReceipts = null;
			}
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
			if (bankReceipts != null
					&& bankReceipts.getUseCase().equals(
							ProjectPaymentSchedule.class.getSimpleName())) {
				ProjectPaymentSchedule projectPaymentSchedule = projectService
						.getProjectPaymentScheduleById(bankReceipts
								.getRecordId());

				List<ProjectInventory> projectInventories = getProjectInvetoryByProject(projectPaymentSchedule
						.getProjectTaskSurrogate().getProject().getProjectId());
				salesVO = new PointOfSaleVO();
				pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
				PointOfSaleDetailVO detailVO = null;
				for (ProjectInventory projectInventory : projectInventories) {
					detailVO = new PointOfSaleDetailVO();
					detailVO.setQuantity(projectInventory.getQuantity());
					detailVO.setUnitRate(projectInventory.getUnitRate());
					double discount = projectInventory.getDiscount() != null ? projectInventory
							.getDiscount() : 0.0;
					if (projectInventory.getIsPercentage() != null
							&& projectInventory.getIsPercentage()) {
						discount = (projectInventory.getQuantity()
								* projectInventory.getUnitRate() * projectInventory
								.getDiscount()) / 100;
					}
					detailVO.setDiscountValue(discount);
					detailVO.setProductByProductId(projectInventory
							.getProduct());
					detailVO.setRecordId(projectInventory
							.getProjectInventoryId());
					detailVO.setShelfId(projectInventory.getShelf()
							.getShelfId());
					pointOfSaleDetailVOs.add(detailVO);
				}
				salesVO.setCustomer(projectPaymentSchedule
						.getProjectTaskSurrogate().getProject().getCustomer());
				salesVO.setReferenceNumber(projectPaymentSchedule
						.getProjectTaskSurrogate().getProject()
						.getReferenceNumber());
				salesVO.setDate(DateFormat
						.convertDateToString(projectPaymentSchedule
								.getProjectTaskSurrogate().getProject()
								.getCreatedDate()
								+ ""));
				salesVO.setCustomerName(null != salesVO.getCustomer()
						.getCompany() ? salesVO.getCustomer().getCompany()
						.getCompanyName() : salesVO
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(salesVO.getCustomer().getPersonByPersonId()
								.getLastName()));
				salesVO.setSalesReceiptNumber(referenceNumber);
				salesVO.setRecordId(bankReceipts.getBankReceiptsId());
				salesVO.setUseCase(BankReceipts.class.getSimpleName());
				salesVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
			} else if (bankReceipts != null
					&& bankReceipts.getUseCase().equals(
							SalesInvoice.class.getSimpleName())) {
				SalesInvoice salesInvoice = salesInvoiceBL
						.getSalesInvoiceService().getSalesInvoiceById(
								bankReceipts.getRecordId());
				if (salesInvoice.getSalesInvoice() != null
						&& salesInvoice.getSalesInvoice().getSalesInvoiceId() != null)
					salesInvoice = salesInvoiceBL.getSalesInvoiceService()
							.getSalesInvoiceById(
									salesInvoice.getSalesInvoice()
											.getSalesInvoiceId());
				salesVO = new PointOfSaleVO();
				pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
				PointOfSaleDetailVO detailVO = null;
				List<SalesInvoiceDetail> salesInvoiceDetails = null;
				if (salesInvoice.getSalesInvoiceDetails() != null
						&& salesInvoice.getSalesInvoiceDetails().size() > 0)
					salesInvoiceDetails = new ArrayList<SalesInvoiceDetail>(
							salesInvoice.getSalesInvoiceDetails());
				else if (salesInvoice.getSalesInvoice()
						.getSalesInvoiceDetails() != null
						&& salesInvoice.getSalesInvoice()
								.getSalesInvoiceDetails().size() > 0)
					salesInvoiceDetails = new ArrayList<SalesInvoiceDetail>(
							salesInvoice.getSalesInvoice()
									.getSalesInvoiceDetails());

				for (SalesInvoiceDetail salesInvoiceDetail : salesInvoiceDetails) {
					detailVO = new PointOfSaleDetailVO();
					detailVO.setQuantity(salesInvoiceDetail
							.getInvoiceQuantity());
					detailVO.setUnitRate(salesInvoiceDetail.getUnitRate());
					if (salesInvoiceDetail.getSalesDeliveryDetail()
							.getIsPercentage() != null
							&& salesInvoiceDetail.getSalesDeliveryDetail()
									.getIsPercentage())
						detailVO.setDiscountValue((salesInvoiceDetail
								.getUnitRate() * 100)
								/ salesInvoiceDetail.getSalesDeliveryDetail()
										.getDiscount());
					else
						detailVO.setDiscountValue(salesInvoiceDetail
								.getSalesDeliveryDetail().getDiscount());

					detailVO.setProductByProductId(salesInvoiceDetail
							.getSalesDeliveryDetail().getProduct());
					detailVO.setRecordId(salesInvoiceDetail
							.getSalesDeliveryDetail()
							.getSalesDeliveryDetailId());
					detailVO.setShelfId(salesInvoiceDetail
							.getSalesDeliveryDetail().getShelf().getShelfId());
					pointOfSaleDetailVOs.add(detailVO);
				}
				salesVO.setCustomer(salesInvoice.getCustomer());
				salesVO.setReferenceNumber(salesInvoice.getReferenceNumber());
				salesVO.setDate(DateFormat.convertDateToString(salesInvoice
						.getInvoiceDate() + ""));
				salesVO.setCustomerName(null != salesVO.getCustomer()
						.getCompany() ? salesVO.getCustomer().getCompany()
						.getCompanyName() : salesVO
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(salesVO.getCustomer().getPersonByPersonId()
								.getLastName()));
				salesVO.setSalesReceiptNumber(referenceNumber);
				salesVO.setRecordId(bankReceipts.getBankReceiptsId());
				salesVO.setUseCase(BankReceipts.class.getSimpleName());
				salesVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

			} else if (bankReceipts == null && pos != null) {
				salesVO = new PointOfSaleVO();
				pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
				PointOfSaleDetailVO detailVO = null;
				double discount = 0;
				double salesAmount = 0;
				for (PointOfSaleDetail posDetail : pos.getPointOfSaleDetails()) {
					discount = 0;
					salesAmount = 0;
					detailVO = new PointOfSaleDetailVO();
					detailVO.setQuantity(posDetail.getQuantity());
					detailVO.setUnitRate(posDetail.getUnitRate());
					salesAmount = posDetail.getQuantity()
							* posDetail.getUnitRate();
					if (null != posDetail.getIsPercentageDiscount()
							&& null != posDetail.getDiscountValue()) {
						if (posDetail.getIsPercentageDiscount())
							discount = ((salesAmount * posDetail
									.getDiscountValue()) / 100);
						else
							discount = posDetail.getDiscountValue();
					}
					detailVO.setDiscountValue(discount);
					detailVO.setProductByProductId(posDetail
							.getProductByProductId());
					detailVO.setRecordId(posDetail.getPointOfSaleDetailId());
					if (posDetail.getProductByProductId().getShelf() != null)
						detailVO.setShelfId(posDetail.getProductByProductId()
								.getShelf().getShelfId());
					pointOfSaleDetailVOs.add(detailVO);
				}
				salesVO.setReferenceNumber(pos.getReferenceNumber());
				salesVO.setDate(DateFormat.convertDateToString(pos
						.getSalesDate() + ""));
				if (pos.getCustomer() != null) {
					salesVO.setCustomer(pos.getCustomer());

					salesVO.setCustomerName(null != salesVO.getCustomer()
							.getCompany() ? salesVO.getCustomer().getCompany()
							.getCompanyName() : salesVO
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat(" ")
							.concat(salesVO.getCustomer().getPersonByPersonId()
									.getLastName()));
				}
				salesVO.setSalesReceiptNumber(referenceNumber);
				salesVO.setRecordId(pos.getPointOfSaleId());
				salesVO.setUseCase(PointOfSale.class.getSimpleName());
				salesVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
			}
			return salesVO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * public void saveSalesReturn(Credit credit, List<CreditDetail>
	 * creditDetails) throws Exception { try { if (credit.getCreditId() == null
	 * || credit.getCreditId() == 0)
	 * credit.setCreditNumber(SystemBL.saveReferenceStamp(
	 * Credit.class.getName(), credit.getImplementation()));
	 * 
	 * creditBL.getCreditService().saveCreditNote(credit, creditDetails);
	 * 
	 * // Stock Updates // Increase the stock what ever reduced before on save
	 * List<Stock> editStocks = new ArrayList<Stock>(); Stock stock = null;
	 * Shelf shelf = null; for (CreditDetail detail : creditDetails) { stock =
	 * new Stock(); if (detail.getShelf() != null) { shelf =
	 * stockBL.getStoreBL().getStoreService()
	 * .getStoreByShelfId(detail.getShelf().getShelfId());
	 * stock.setShelf(shelf); }
	 * stock.setStore(shelf.getShelf().getAisle().getStore());
	 * stock.setProduct(detail.getProduct());
	 * stock.setQuantity(detail.getReturnQuantity());
	 * stock.setImplementation(getImplementation()); editStocks.add(stock); }
	 * stockBL.updateStockDetails(editStocks, null);
	 * 
	 * // JV Transactions if (null != credit.getIsDirectPayment()) {
	 * performeSalesReturnJVTransaction(credit, creditDetails); } else {
	 * performeSalesReturnJVTransactionForFreeReturn(credit, creditDetails); }
	 * 
	 * // Payment Alert if (null != credit.getIsDirectPayment() &&
	 * credit.getIsDirectPayment()) {
	 * createSalesReturnDirectPaymentAlert(credit); } } catch (IOException e) {
	 * e.printStackTrace(); } }
	 * 
	 * private void createSalesReturnDirectPaymentAlert(Credit credit) throws
	 * Exception { Alert alert = alertBL.getAlertService().getAlertRecordInfo(
	 * credit.getCreditId(), Credit.class.getSimpleName()); if (null != alert) {
	 * alert.setIsActive(true); alertBL.commonSaveAlert(alert); } else {
	 * List<Credit> credits = new ArrayList<Credit>(); credits.add(credit);
	 * alertBL.salesReturnDirectPayment(credits); } }
	 */
	public void salesReturnDirectPayment(Credit credit,
			List<CreditDetail> creditDetails) {

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;
			Combination combination = null;
			if (credit.getCustomer() != null) {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								credit.getCustomer().getCombination()
										.getCombinationId());
			} else {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								getImplementation().getAccountsReceivable());
			}
			List<Currency> currencyList = null;
			if (credit != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplementation());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplementation());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(credit.getCreditId());
				directPayment.setUseCase(Credit.class.getSimpleName());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				DirectPaymentDetail dpDetail = null;
				dpDetail = new DirectPaymentDetail();
				dpDetail.setAmount(credit.getTotalReturn());
				dpDetail.setCombination(combination);
				directPayDetails.add(dpDetail);
				directPayment.setCustomer(credit.getCustomer());
				directPayment.setDescription("Sales Return Reference "
						+ credit.getCreditNumber());
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	/*
	 * public void performeSalesReturnJVTransaction(Credit credit,
	 * List<CreditDetail> creditDetails) throws Exception {
	 * 
	 * // Transaction List<TransactionDetail> transactionDetails = new
	 * ArrayList<TransactionDetail>(); double totalDeduction = 0.0; // Inventory
	 * Debit for (CreditDetail detail : creditDetails) {
	 * transactionDetails.add(transactionBL.createTransactionDetail(
	 * detail.getReturnAmount(), productBL.getProductInventory(
	 * detail.getProduct().getProductId()) .getCombinationId(),
	 * TransactionType.Debit .getCode(), "", null)); totalDeduction +=
	 * (detail.getDeduction() != null ? detail .getDeduction() : 0.0); }
	 * 
	 * // Revenue credit if (totalDeduction > 0)
	 * transactionDetails.add(transactionBL.createTransactionDetail(
	 * totalDeduction, getImplementation().getRevenueAccount(),
	 * TransactionType.Credit.getCode(), "", null));
	 * 
	 * // Customer Credit if (credit.getCustomer() != null)
	 * transactionDetails.add(transactionBL.createTransactionDetail(
	 * credit.getTotalReturn(), credit.getCustomer()
	 * .getCombination().getCombinationId(), TransactionType.Credit.getCode(),
	 * "", null)); else
	 * transactionDetails.add(transactionBL.createTransactionDetail(credit
	 * .getTotalReturn(), getImplementation() .getAccountsReceivable(),
	 * TransactionType.Credit.getCode(), "", null));
	 * 
	 * Transaction transaction = transactionBL.createTransaction(transactionBL
	 * .getCurrency().getCurrencyId(), "", new Date(), transactionBL
	 * .getCategory("Sales Return"), credit.getCreditId(),
	 * Credit.class.getSimpleName()); transactionBL.saveTransaction(transaction,
	 * transactionDetails);
	 * 
	 * }
	 * 
	 * public void performeSalesReturnJVTransactionForFreeReturn(Credit credit,
	 * List<CreditDetail> creditDetails) throws Exception {
	 * 
	 * // Transaction List<TransactionDetail> transactionDetails = new
	 * ArrayList<TransactionDetail>(); double totalRevenue = 0.0; // Inventory
	 * Debit for (CreditDetail detail : creditDetails) {
	 * transactionDetails.add(transactionBL.createTransactionDetail(
	 * detail.getReturnAmount(), productBL.getProductInventory(
	 * detail.getProduct().getProductId()) .getCombinationId(),
	 * TransactionType.Debit .getCode(), "", null)); totalRevenue +=
	 * (detail.getReturnAmount() != null ? detail .getReturnAmount() : 0.0); }
	 * 
	 * // Revenue credit if (totalRevenue > 0)
	 * transactionDetails.add(transactionBL.createTransactionDetail(
	 * totalRevenue, getImplementation().getRevenueAccount(),
	 * TransactionType.Credit.getCode(), "", null));
	 * 
	 * Transaction transaction = transactionBL.createTransaction(transactionBL
	 * .getCurrency().getCurrencyId(), "", new Date(), transactionBL
	 * .getCategory("Sales Return(Free)"), credit.getCreditId(),
	 * Credit.class.getSimpleName()); transactionBL.saveTransaction(transaction,
	 * transactionDetails);
	 * 
	 * }
	 */

	// Get Credit Notes list
	public List<Object> getAllCredits(Implementation implementation)
			throws Exception {
		List<Object> creditNotes = new ArrayList<Object>();
		List<Credit> creditNoteList = creditBL.getCreditService()
				.getAllCreditNotesByReturn(implementation);
		if (null != creditNoteList && creditNoteList.size() > 0) {
			CreditNoteVO creditNoteVO = null;
			for (Credit credit : creditNoteList) {
				creditNoteVO = new CreditNoteVO();
				creditNoteVO.setCreditDate(DateFormat
						.convertDateToString(credit.getDate().toString()));
				creditNoteVO.setCreditId(credit.getCreditId());
				if (credit.getCustomer() != null) {
					creditNoteVO.setCustomerReference(credit.getCustomer()
							.getCustomerNumber());
					creditNoteVO.setCustomerName(null != credit.getCustomer()
							.getPersonByPersonId() ? credit
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat("")
							.concat(credit.getCustomer().getPersonByPersonId()
									.getLastName()) : (null != credit
							.getCustomer().getCompany() ? credit.getCustomer()
							.getCompany().getCompanyName() : credit
							.getCustomer().getCmpDeptLocation().getCompany()
							.getCompanyName()));
				}
				creditNoteVO.setCreditNumber(credit.getCreditNumber());
				creditNoteVO.setDescription(credit.getDescription());
				creditNoteVO.setTotalReturn(credit.getTotalReturn());
				if (credit.getStatus() != null)
					creditNoteVO.setStatusString(ProjectPaymentStatus.get(
							credit.getStatus()).name());

				creditNoteVO.setProcessType(ReturnProcessType.Exchange.name());
				if (credit.getIsDirectPayment() != null
						&& credit.getIsDirectPayment())
					creditNoteVO.setProcessType(ReturnProcessType.DirectPayment
							.name());
				else if (credit.getIsDirectPayment() == null)
					creditNoteVO.setProcessType(ReturnProcessType.FreeReturn
							.name());

				creditNoteVO.setSalesReference(credit.getSalesReference());
				if (credit.getStatus() != null
						&& (byte) credit.getStatus() == (byte) ProjectPaymentStatus.Paid
								.getCode())
					creditNoteVO.setPaymentFlag(true);
				else
					creditNoteVO.setPaymentFlag(false);
				creditNotes.add(creditNoteVO);
			}
		}
		return creditNotes;
	}

	// Manipulate Credit Detail
	public List<CreditDetailVO> manipulateCreditDetail(
			List<CreditDetail> creditDetails) {
		List<CreditDetailVO> creditDetailVOs = new ArrayList<CreditDetailVO>();
		CreditDetailVO creditDetailVO = null;
		double invoiceQuantity = 0;
		for (CreditDetail creditDetail : creditDetails) {
			creditDetailVO = new CreditDetailVO();
			creditDetailVO.setSalesDeliveryDetailId(creditDetail
					.getSalesDeliveryDetail().getSalesDeliveryDetailId());
			creditDetailVO.setCreditDetailId(creditDetail.getCreditDetailId());
			creditDetailVO.setReturnQuantity(creditDetail.getReturnQuantity());
			creditDetailVO.setDeliveryQuantity(creditDetail
					.getSalesDeliveryDetail().getDeliveryQuantity());
			creditDetailVO.setProductId(creditDetail.getSalesDeliveryDetail()
					.getProduct().getProductId());
			creditDetailVO.setProductName(creditDetail.getSalesDeliveryDetail()
					.getProduct().getProductName());
			creditDetailVO.setShelfId(creditDetail.getSalesDeliveryDetail()
					.getShelf().getShelfId());
			creditDetailVO.setShelfName(creditDetail.getSalesDeliveryDetail()
					.getShelf().getShelf().getAisle().getStore().getStoreName()
					+ ">>"
					+ creditDetail.getSalesDeliveryDetail().getShelf()
							.getShelf().getAisle().getSectionName()
					+ ">>"
					+ creditDetail.getSalesDeliveryDetail().getShelf()
							.getShelf().getName()
					+ ">>"
					+ creditDetail.getSalesDeliveryDetail().getShelf()
							.getName());
			creditDetailVO.setUnitRate(creditDetail.getSalesDeliveryDetail()
					.getUnitRate());
			if (null != creditDetail.getSalesDeliveryDetail().getDiscount()
					&& creditDetail.getSalesDeliveryDetail().getDiscount() > 0) {
				double discountAmount = 0;
				if (creditDetail.getSalesDeliveryDetail().getIsPercentage()) {
					discountAmount = ((creditDetail.getSalesDeliveryDetail()
							.getUnitRate() * creditDetail
							.getSalesDeliveryDetail().getDeliveryQuantity()) * creditDetail
							.getSalesDeliveryDetail().getDiscount()) / 100;
				} else {
					discountAmount = creditDetail.getSalesDeliveryDetail()
							.getDiscount();
				}
				creditDetailVO.setUnitRate(creditDetailVO.getUnitRate()
						- discountAmount);
			}
			invoiceQuantity = 0;
			if (null != creditDetail.getSalesDeliveryDetail()
					.getSalesInvoiceDetails()) {
				for (SalesInvoiceDetail salesInvoiceDetail : creditDetail
						.getSalesDeliveryDetail().getSalesInvoiceDetails()) {
					invoiceQuantity += salesInvoiceDetail.getInvoiceQuantity();
				}
				creditDetailVO.setInvoiceQuantity(invoiceQuantity);
			}
			creditDetailVO
					.setInwardedQuantity(creditDetail.getReturnQuantity());
			creditDetailVO.setReturnQuantity(null != creditDetailVO
					.getInwardedQuantity() ? creditDetailVO
					.getInwardedQuantity() - creditDetailVO.getReturnQuantity()
					: creditDetailVO.getReturnQuantity());
			creditDetailVO.setDescription(creditDetail.getDescription());
			creditDetailVO.setCreditDetailId(creditDetail.getCreditDetailId());

			creditDetailVOs.add(creditDetailVO);
		}
		return creditDetailVOs;
	}

	public List<ProjectInventory> getProjectInvetoryByProject(Long projectId) {
		List<ProjectInventory> projectInventories = new ArrayList<ProjectInventory>();
		try {
			List<ProjectTask> projectTasks = projectTaskService
					.getAllProjectTaskByProjectId(projectId);
			projectInventories = projectService
					.getProjectInventoriesByProjectId(projectId);
			for (ProjectTask projectTask : projectTasks) {
				projectInventories.addAll(getProjectInventoryByProjectTasks(
						projectTask, projectInventories));

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Cleaning empty duplicate records
		Set<ProjectInventory> sets = new HashSet<ProjectInventory>(
				projectInventories);
		return new ArrayList<ProjectInventory>(sets);
	}

	public List<ProjectInventory> getProjectInventoryByProjectTasks(
			ProjectTask projectTask, List<ProjectInventory> projectInventories) {
		try {
			projectInventories.addAll(projectTaskService
					.getProjectTaskInventoriesByTaskId(projectTask
							.getProjectTaskId()));
			for (ProjectTask subTask : projectTask.getProjectTasks()) {
				getProjectInventoryByProjectTasks(subTask, projectInventories);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return projectInventories;
	}

	public List<SalesInvoiceVO> getAllProjectPaymentByProject(
			SalesInvoiceVO salesInvoiceVOTemp) throws Exception {
		SalesInvoiceVO salesInvoiceVO = null;
		List<Project> projects = projectService
				.getFilteredPaymentsByProjects(salesInvoiceVOTemp);
		List<SalesInvoiceVO> salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();
		Collections.sort(projects, new Comparator<Project>() {
			public int compare(Project m1, Project m2) {
				return m1.getProjectId().compareTo(m2.getProjectId());
			}
		});
		for (Project project : projects) {
			salesInvoiceVO = new SalesInvoiceVO();
			Double paidTotal = 0.0;
			Double pendingTotal = 0.0;
			Double totalAmount = 0.0;

			List<ProjectTaskSurrogate> taskSurrogats = new ArrayList<ProjectTaskSurrogate>(
					project.getProjectTaskSurrogates());
			List<SalesInvoiceVO> salesInvoiceVOsTemp = new ArrayList<SalesInvoiceVO>();
			SalesInvoiceVO salesInvoiceTempVO = null;
			for (ProjectPaymentSchedule payment : taskSurrogats.get(0)
					.getProjectPaymentSchedules()) {
				salesInvoiceTempVO = new SalesInvoiceVO();
				if ((byte) payment.getStatus() == (byte) ProjectPaymentStatus.Paid
						.getCode())
					paidTotal += payment.getAmount();
				else
					pendingTotal += payment.getAmount();

				totalAmount += payment.getAmount();

				for (BankReceipts receipts : project.getCustomer()
						.getBankReceiptses()) {
					if (receipts.getRecordId() != null
							&& receipts.getUseCase() != null
							&& receipts.getUseCase().equals(
									ProjectPaymentSchedule.class
											.getSimpleName())
							&& (long) payment.getProjectPaymentScheduleId() == (long) receipts
									.getRecordId()) {

						salesInvoiceTempVO.setReferenceNumber(payment
								.getLookupDetail().getDisplayName()
								+ " ( Paid Reference : "
								+ receipts.getReceiptsNo() + " )");
						salesInvoiceTempVO.setSalesInvoiceDate(DateFormat
								.convertDateToString(receipts.getReceiptsDate()
										+ ""));
						salesInvoiceTempVO
								.setInvoiceAmount(payment.getAmount());

					}
				}
				if (salesInvoiceTempVO.getInvoiceAmount() == null)
					salesInvoiceTempVO.setInvoiceAmount(0.0);

				if (salesInvoiceTempVO.getReferenceNumber() == null)
					salesInvoiceTempVO.setReferenceNumber(payment
							.getLookupDetail().getDisplayName());

				if (salesInvoiceTempVO.getSalesInvoiceDate() == null)
					salesInvoiceTempVO
							.setSalesInvoiceDate(DateFormat
									.convertDateToString(payment
											.getPaymentDate() + ""));

				salesInvoiceTempVO.setTotalInvoice(payment.getAmount());

				salesInvoiceTempVO.setPendingInvoice(salesInvoiceTempVO
						.getTotalInvoice()
						- salesInvoiceTempVO.getInvoiceAmount());

				salesInvoiceTempVO.setSalesInvoiceId(payment
						.getProjectPaymentScheduleId());
				salesInvoiceVOsTemp.add(salesInvoiceTempVO);

			}
			salesInvoiceVO.setSalesInvoiceVOs(salesInvoiceVOsTemp);
			salesInvoiceVO.setSalesDeliveryNumber(project.getReferenceNumber());
			salesInvoiceVO.setSalesDate(DateFormat.convertDateToString(project
					.getStartDate() + ""));
			salesInvoiceVO.setInvoiceAmount(paidTotal);
			salesInvoiceVO.setTotalInvoice(totalAmount);
			salesInvoiceVO.setPendingInvoice(totalAmount - paidTotal);
			salesInvoiceVO.setReferenceNumber(project.getReferenceNumber());
			salesInvoiceVO.setSalesInvoiceId(project.getProjectId());
			salesInvoiceVO.setCustomerName(null != project.getCustomer()
					.getPersonByPersonId() ? project
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(project.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != project.getCustomer()
					.getCompany() ? project.getCustomer().getCompany()
					.getCompanyName() : project.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));

			salesInvoiceVO.setCustomerNumber(project.getCustomer()
					.getCustomerNumber());
			salesInvoiceVO.setCustomerId(project.getCustomer().getCustomerId());

			salesInvoiceVO.setSalesInvoiceDate(DateFormat
					.convertDateToString(project.getStartDate() + ""));
			salesInvoiceVO.setSalesFlag("PROJECT");

			salesInvoiceVO.setInvoiceAmount(AIOSCommons
					.roundDecimals(salesInvoiceVO.getInvoiceAmount()));
			salesInvoiceVO.setTotalInvoice(AIOSCommons
					.roundDecimals(salesInvoiceVO.getTotalInvoice()));
			salesInvoiceVO.setPendingInvoice(AIOSCommons
					.roundDecimals(salesInvoiceVO.getPendingInvoice()));

			if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Pending
					.getCode() && salesInvoiceVO.getPendingInvoice() > 0.0)
				salesInvoiceVOs.add(salesInvoiceVO);
			else if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Paid
					.getCode() && salesInvoiceVO.getPendingInvoice() <= 0.0)
				salesInvoiceVOs.add(salesInvoiceVO);
			else if ((byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Pending
					.getCode()
					&& (byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Paid
							.getCode())
				salesInvoiceVOs.add(salesInvoiceVO);

		}

		return salesInvoiceVOs;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public SalesInvoiceBL getSalesInvoiceBL() {
		return salesInvoiceBL;
	}

	public void setSalesInvoiceBL(SalesInvoiceBL salesInvoiceBL) {
		this.salesInvoiceBL = salesInvoiceBL;
	}

	public CreditBL getCreditBL() {
		return creditBL;
	}

	public void setCreditBL(CreditBL creditBL) {
		this.creditBL = creditBL;
	}

	public MaterialRequisitionBL getMaterialRequisitionBL() {
		return materialRequisitionBL;
	}

	public void setMaterialRequisitionBL(
			MaterialRequisitionBL materialRequisitionBL) {
		this.materialRequisitionBL = materialRequisitionBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public ProjectTaskService getProjectTaskService() {
		return projectTaskService;
	}

	public void setProjectTaskService(ProjectTaskService projectTaskService) {
		this.projectTaskService = projectTaskService;
	}

	public ProjectPaymentScheduleService getProjectPaymentScheduleService() {
		return projectPaymentScheduleService;
	}

	public void setProjectPaymentScheduleService(
			ProjectPaymentScheduleService projectPaymentScheduleService) {
		this.projectPaymentScheduleService = projectPaymentScheduleService;
	}

	public PettyCashBL getPettyCashBL() {
		return pettyCashBL;
	}

	public void setPettyCashBL(PettyCashBL pettyCashBL) {
		this.pettyCashBL = pettyCashBL;
	}

	public ProjectDeliveryService getProjectDeliveryService() {
		return projectDeliveryService;
	}

	public void setProjectDeliveryService(
			ProjectDeliveryService projectDeliveryService) {
		this.projectDeliveryService = projectDeliveryService;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}
}
