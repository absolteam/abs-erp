package com.aiotech.aios.project.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectPaymentDetail;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.vo.ProjectMileStoneVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectPaymentDetailVO;
import com.aiotech.aios.project.service.ProjectMileStoneService;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.to.DocumentVO;

public class ProjectMileStoneBL {

	private ProjectBL projectBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private AlertBL alertBL;
	private ProjectMileStoneService projectMileStoneService;

	public List<Object> showActiveProjectMileStones() throws Exception {
		List<ProjectMileStone> projectMileStones = projectMileStoneService
				.getActiveProjectMileStones(getImplementation(),
						ProjectStatus.Closed.getCode(), (byte) 5);
		List<Object> projectMileStoneVOs = new ArrayList<Object>();
		if (null != projectMileStones && projectMileStones.size() > 0) {
			for (ProjectMileStone projectMileStone : projectMileStones)
				projectMileStoneVOs
						.add(addProjectMileStoneVO(projectMileStone));
		}
		return projectMileStoneVOs;
	}

	public List<Object> getActiveProjectMileStonesByProject(Long projectId)
			throws Exception {
		List<ProjectMileStone> projectMileStones = projectMileStoneService
				.getActiveProjectMileStonesByProject(projectId);
		List<Object> projectMileStoneVOs = new ArrayList<Object>();
		if (null != projectMileStones && projectMileStones.size() > 0) {
			for (ProjectMileStone projectMileStone : projectMileStones)
				projectMileStoneVOs
						.add(addProjectMileStoneVO(projectMileStone));
		}
		return projectMileStoneVOs;
	}

	public List<Object> showAllProjectMileStones() throws Exception {
		List<ProjectMileStone> projectMileStones = projectMileStoneService
				.getAllProjectMileStones(getImplementation());
		List<Object> projectMileStoneVOs = new ArrayList<Object>();
		if (null != projectMileStones && projectMileStones.size() > 0) {
			for (ProjectMileStone projectMileStone : projectMileStones)
				projectMileStoneVOs
						.add(addProjectMileStoneVO(projectMileStone));
		}
		return projectMileStoneVOs;
	}

	public List<Object> showProjectMileStoneByProject(long projectId)
			throws Exception {
		List<ProjectMileStone> projectMileStones = projectMileStoneService
				.getProjectMileStoneByProject(projectId);
		List<Object> projectMileStoneVOs = new ArrayList<Object>();
		if (null != projectMileStones && projectMileStones.size() > 0) {
			for (ProjectMileStone projectMileStone : projectMileStones)
				projectMileStoneVOs
						.add(addProjectMileStoneVO(projectMileStone));
		}
		return projectMileStoneVOs;
	}

	private ProjectMileStoneVO addProjectMileStoneVO(
			ProjectMileStone projectMileStone) {
		ProjectMileStoneVO projectMileStoneVO = new ProjectMileStoneVO();
		projectMileStoneVO.setProjectTitle(projectMileStone.getProject()
				.getProjectTitle());
		projectMileStoneVO.setReferenceNumber(projectMileStone.getProject()
				.getReferenceNumber());
		projectMileStoneVO.setMileStoneTitle(projectMileStone.getTitle());
		projectMileStoneVO
				.setFromDate(DateFormat.convertDateToString(projectMileStone
						.getStartDate().toString()));
		projectMileStoneVO.setToDate(DateFormat
				.convertDateToString(projectMileStone.getEndDate().toString()));
		if (projectMileStone.getStatus() != null)
			projectMileStoneVO.setCurrentStatus(ProjectStatus.get(
					projectMileStone.getStatus()).toString());

		projectMileStoneVO.setDescription(projectMileStone.getDescription());
		if (projectMileStone.getProjectTasks() != null
				&& projectMileStone.getProjectTasks().size() > 0) {
			for (ProjectTask task : projectMileStone.getProjectTasks()) {
				projectMileStoneVO.setTaskName(task.getTaskTitle());
				projectMileStoneVO.setTaskReference(task.getTaskTitle());
			}
		} else {
			projectMileStoneVO.setTaskName("");
			projectMileStoneVO.setTaskReference("");
		}
		projectMileStoneVO.setProjectMileStoneId(projectMileStone
				.getProjectMileStoneId());
		return projectMileStoneVO;
	}

	public ProjectMileStoneVO addDetailedProjectMileStoneVO(
			ProjectMileStone projectMileStone) {
		ProjectMileStoneVO projectMileStoneVO = new ProjectMileStoneVO();
		projectMileStoneVO.setProjectTitle(projectMileStone.getProject()
				.getProjectTitle());
		/*
		 * projectMileStoneVO.setMilestoneTitle(projectMileStone
		 * .getMilestoneTitle()); projectMileStoneVO
		 * .setFromDate(DateFormat.convertDateToString(projectMileStone
		 * .getStartDate().toString())); projectMileStoneVO.setToDate(DateFormat
		 * .convertDateToString(projectMileStone.getEndDate().toString()));
		 * projectMileStoneVO.setReferenceNumber(projectMileStone
		 * .getReferenceNumber());
		 * projectMileStoneVO.setCurrentStatus(ProjectStatus.get(
		 * projectMileStone.getStatus()).toString());
		 * projectMileStoneVO.setDescription(projectMileStone.getDescription());
		 * if (null != projectMileStone.getActualStartDate())
		 * projectMileStoneVO.setActualFromDate(DateFormat
		 * .convertDateToString(projectMileStone.getActualStartDate()
		 * .toString())); if (null != projectMileStone.getActualEndDate())
		 * projectMileStoneVO.setActualToDate(DateFormat
		 * .convertDateToString(projectMileStone.getActualEndDate()
		 * .toString())); projectMileStoneVO.setActualFormatCost(null !=
		 * projectMileStone .getActualCost() ?
		 * AIOSCommons.formatAmount(projectMileStone .getActualCost()) : null);
		 * projectMileStoneVO.setEstimationFormatCost(AIOSCommons
		 * .formatAmount(projectMileStone.getEstimationCost()));
		 * projectMileStoneVO.setEstimationCost(projectMileStone
		 * .getEstimationCost());
		 * projectMileStoneVO.setActualCost(projectMileStone.getActualCost());
		 * projectMileStoneVO.setClientName(null !=
		 * projectMileStone.getProject() .getCustomer().getCompany() ?
		 * projectMileStone.getProject()
		 * .getCustomer().getCompany().getCompanyName() : projectMileStone
		 * .getProject() .getCustomer() .getPersonByPersonId() .getFirstName()
		 * .concat(" ") .concat(projectMileStone.getProject().getCustomer()
		 * .getPersonByPersonId().getLastName()));
		 * projectMileStoneVO.setProjectMileStoneId(projectMileStone
		 * .getProjectMileStoneId()); if (null != projectMileStone &&
		 * projectMileStone.getProjectPaymentDetails().size() > 0) {
		 * ProjectPaymentDetailVO projectPaymentDetailVO = null;
		 * List<ProjectPaymentDetailVO> ProjectPaymentDetailVOs = new
		 * ArrayList<ProjectPaymentDetailVO>(); for (ProjectPaymentDetail
		 * projectPaymentDetail : projectMileStone .getProjectPaymentDetails())
		 * { projectPaymentDetailVO = new ProjectPaymentDetailVO();
		 * projectPaymentDetailVO.setPaymentTitle(projectPaymentDetail
		 * .getPaymentTitle()); projectPaymentDetailVO.setReceiptDate(DateFormat
		 * .convertDateToString(projectPaymentDetail
		 * .getPaymentDate().toString()));
		 * projectPaymentDetailVO.setReceiptAmount(AIOSCommons
		 * .formatAmount(projectPaymentDetail.getPaymentAmount()));
		 * projectPaymentDetailVO.setReceiptStatus(PaymentStatus.get(
		 * projectPaymentDetail.getStatus()).name());
		 * projectPaymentDetailVO.setPaymentAmount(projectPaymentDetail
		 * .getPaymentAmount());
		 * ProjectPaymentDetailVOs.add(projectPaymentDetailVO); }
		 * projectMileStoneVO
		 * .setProjectPaymentDetailVOs(ProjectPaymentDetailVOs); }
		 */
		return projectMileStoneVO;
	}

	public ProjectPaymentDetailVO addDetailedProjectPaymentVO(
			ProjectPaymentDetail projectPaymentDetail) {

		ProjectPaymentDetailVO projectPaymentDetailVO = new ProjectPaymentDetailVO();
		/*
		 * projectPaymentDetailVO.setPaymentTitle(projectPaymentDetail
		 * .getPaymentTitle()); projectPaymentDetailVO.setReceiptDate(DateFormat
		 * .convertDateToString(projectPaymentDetail.getPaymentDate()
		 * .toString())); projectPaymentDetailVO.setReceiptAmount(AIOSCommons
		 * .formatAmount(projectPaymentDetail.getPaymentAmount()));
		 * projectPaymentDetailVO.setPaymentAmount(projectPaymentDetail
		 * .getPaymentAmount());
		 * projectPaymentDetailVO.setReceiptStatus(PaymentStatus.get(
		 * projectPaymentDetail.getStatus()).name());
		 * projectPaymentDetailVO.setDescription(projectPaymentDetail
		 * .getDescription()); projectPaymentDetailVO .setClientName(null !=
		 * projectPaymentDetail
		 * .getProjectMileStone().getProject().getCustomer() .getCompany() ?
		 * projectPaymentDetail
		 * .getProjectMileStone().getProject().getCustomer()
		 * .getCompany().getCompanyName() : projectPaymentDetail
		 * .getProjectMileStone() .getProject() .getCustomer()
		 * .getPersonByPersonId() .getFirstName() .concat(" ")
		 * .concat(projectPaymentDetail.getProjectMileStone()
		 * .getProject().getCustomer() .getPersonByPersonId().getLastName()));
		 * projectPaymentDetailVO.setProjectTitle(projectPaymentDetail
		 * .getProjectMileStone().getProject().getProjectTitle());
		 * projectPaymentDetailVO.setMilestoneTitle(projectPaymentDetail
		 * .getProjectMileStone().getMilestoneTitle());
		 * projectPaymentDetailVO.setReceiptStatus(PaymentStatus.get(
		 * projectPaymentDetail.getStatus()).name());
		 * projectPaymentDetailVO.setProjectPaymentDetailId(projectPaymentDetail
		 * .getProjectPaymentDetailId());
		 */
		return projectPaymentDetailVO;
	}

	public void saveProjectMileStone(ProjectMileStone projectMileStone,
			List<ProjectPaymentDetail> projectPaymentDetails,
			List<ProjectPaymentDetail> deletePaymentDetails) throws Exception {
		// projectMileStone.setImplementation(getImplementation());
		boolean modify = null == projectMileStone ? false : true;
		if (!modify)
			SystemBL.saveReferenceStamp(ProjectMileStone.class.getName(),
					getImplementation());
		if (null != deletePaymentDetails && deletePaymentDetails.size() > 0)
			projectMileStoneService
					.deleteProjectPaymentDetails(deletePaymentDetails);
		projectMileStoneService.saveProjectMileStone(projectMileStone,
				projectPaymentDetails);
		savProjectMileStoneDocuments(projectMileStone, modify);

		// MileStone Alert
		LocalDate endDate = new DateTime(projectMileStone.getEndDate())
				.toLocalDate();
		LocalDate currentDate = DateTime.now().toLocalDate();
		if (endDate.compareTo(currentDate) <= 0)
			createMileStoneAlert(projectMileStone, modify);
		if (null != projectPaymentDetails && projectPaymentDetails.size() > 0) {
			for (ProjectPaymentDetail projectPaymentDetail : projectPaymentDetails) {
				LocalDate paymentDate = new DateTime(
						projectPaymentDetail.getPaymentDate()).toLocalDate();
				if (paymentDate.compareTo(currentDate) <= 0)
					createPaymentAlert(projectPaymentDetail);
			}
		}
	}

	public void updateProjectMileStone(ProjectMileStone projectMileStone)
			throws Exception {
		projectMileStoneService.saveProjectMileStone(projectMileStone);
		/*
		 * if ((byte) projectMileStone.getStatus() == ProjectStatus.Completed
		 * .getCode() || (byte) projectMileStone.getStatus() ==
		 * ProjectStatus.Cancelled .getCode()) { Alert alert =
		 * alertBL.getAlertService().getAlertRecordInfo(
		 * projectMileStone.getProjectMileStoneId(),
		 * ProjectMileStone.class.getSimpleName()); alert.setIsActive(false);
		 * alertBL.commonSaveAlert(alert); }
		 */
	}

	private void createMileStoneAlert(ProjectMileStone projectMileStone,
			boolean modify) throws Exception {
		/*
		 * if (modify) { modify = false; Alert alert =
		 * alertBL.getAlertService().getAlertRecordInfo(
		 * projectMileStone.getProjectMileStoneId(),
		 * ProjectMileStone.class.getSimpleName()); if (null != alert) {
		 * alert.setIsActive(true); alertBL.commonSaveAlert(alert); modify =
		 * true; } }
		 */
		if (!modify) {
			List<ProjectMileStone> projectMileStones = new ArrayList<ProjectMileStone>();
			projectMileStones.add(projectMileStone);
			alertBL.alertProjectMileStone(projectMileStones);
		}
	}

	private void createPaymentAlert(ProjectPaymentDetail projectPaymentDetail)
			throws Exception {
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				projectPaymentDetail.getProjectPaymentDetailId(),
				ProjectPaymentDetail.class.getSimpleName());
		if (null != alert) {
			alert.setIsActive(true);
			alertBL.commonSaveAlert(alert);
		} else {
			List<ProjectPaymentDetail> projectPaymentDetails = new ArrayList<ProjectPaymentDetail>();
			projectPaymentDetails.add(projectPaymentDetail);
			// alertBL.alertProjectPayment(projectPaymentDetails);
		}
	}

	public void createMileStoneAlerts() throws Exception {
		LocalDate currentDate = DateTime.now().toLocalDate();
		List<ProjectMileStone> projectMileStones = projectMileStoneService
				.getProjectMileStonesByEndDate(currentDate.toDate(),
						ProjectStatus.Closed.getCode(), (byte) 5);
		if (null != projectMileStones && projectMileStones.size() > 0)
			alertBL.alertProjectMileStone(projectMileStones);
	}

	public void createProjectPaymentAlerts() throws Exception {
		LocalDate currentDate = DateTime.now().toLocalDate();
		List<ProjectPaymentDetail> projectPaymentDetails = projectMileStoneService
				.getProjectPaymentsByPaymentDate(currentDate.toDate(),
						ProjectStatus.Closed.getCode(), (byte) 5);
		/*
		 * if (null != projectPaymentDetails && projectPaymentDetails.size() >
		 * 0) alertBL.alertProjectPayment(projectPaymentDetails);
		 */
	}

	public void createProjectDepositPaymentAlerts() throws Exception {
		LocalDate currentDate = DateTime.now().toLocalDate();
		List<Project> projects = projectBL.getProjectService()
				.getActiveProjectsForSecurityDeposit(
						ProjectStatus.Closed.getCode(), (byte) 5);
		List<Project> projectTemps = new ArrayList<Project>();
		if (null != projects && projects.size() > 0) {
			LocalDate depositDate = null;
			for (Project project : projects) {
				depositDate = new LocalDate(project.getDepositDate());
				if (project.getProjectStatus() != null
						&& (byte) project.getProjectStatus() != (byte) 5
						&& (depositDate.isBefore(currentDate) || depositDate
								.equals(currentDate))) {
					projectTemps.add(project);

				}
			}
		}

		if (null != projectTemps && projectTemps.size() > 0)
			alertBL.alertProjectDepositPayment(projectTemps);
	}

	private void savProjectMileStoneDocuments(
			ProjectMileStone projectMileStone, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(projectMileStone,
				"projectMileStoneDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		/*
		 * if (dirs != null) { Map<String, Directory> dirStucture =
		 * directoryBL.saveDirStructure( dirs,
		 * projectMileStone.getProjectMileStoneId(), "ProjectMileStone",
		 * "projectMileStoneDocs"); docVO.setDirStruct(dirStucture); }
		 */
		documentBL.saveUploadDocuments(docVO);
	}

	public void deleteProjectMileStone(ProjectMileStone projectMileStone)
			throws Exception {
		/*
		 * documentBL.getDocumentService().deleteDocuments(
		 * documentBL.getDocumentService().getChildDocumentsByRecordId(
		 * projectMileStone.getProjectMileStoneId(), "ProjectMileStone"));
		 * documentBL.getDocumentService().deleteDocuments(
		 * documentBL.getDocumentService().getParentDocumentsByRecordId(
		 * projectMileStone.getProjectMileStoneId(), "ProjectMileStone"));
		 * List<ProjectPaymentDetail> projectPaymentDetails = null; if (null !=
		 * projectMileStone.getProjectPaymentDetails() &&
		 * projectMileStone.getProjectPaymentDetails().size() > 0) {
		 * projectPaymentDetails = new ArrayList<ProjectPaymentDetail>();
		 * projectPaymentDetails.addAll(new ArrayList<ProjectPaymentDetail>(
		 * projectMileStone.getProjectPaymentDetails())); }
		 * projectMileStoneService.deleteProjectMileStone(projectMileStone,
		 * projectPaymentDetails);
		 */
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProjectBL getProjectBL() {
		return projectBL;
	}

	public void setProjectBL(ProjectBL projectBL) {
		this.projectBL = projectBL;
	}

	public ProjectMileStoneService getProjectMileStoneService() {
		return projectMileStoneService;
	}

	public void setProjectMileStoneService(
			ProjectMileStoneService projectMileStoneService) {
		this.projectMileStoneService = projectMileStoneService;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}
}
