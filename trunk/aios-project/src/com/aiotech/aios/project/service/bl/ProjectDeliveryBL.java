package com.aiotech.aios.project.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants.Accounts.ProjectDeliveryStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.vo.ProjectDeliveryVO;
import com.aiotech.aios.project.service.ProjectDeliveryService;
import com.aiotech.aios.project.service.ProjectService;
import com.aiotech.aios.project.service.ProjectTaskService;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;

public class ProjectDeliveryBL {

	private ProjectMileStoneBL projectMileStoneBL;
	private AlertBL alertBL;
	private ProjectDeliveryService projectDeliveryService;
	private ProjectService projectService;
	private ProjectTaskService projectTaskService;

	public List<Object> showAllProjectDeliveries() throws Exception {
		List<ProjectDelivery> projectDeliveries = projectDeliveryService
				.getAllProjectDeliveries(getImplementation(),
						ProjectDeliveryStatus.Cancelled.getCode(),
						ProjectDeliveryStatus.Delivered.getCode());
		List<Object> projectDeliverieVOs = new ArrayList<Object>();
		if (null != projectDeliveries && projectDeliveries.size() > 0) {
			for (ProjectDelivery projectDelivery : projectDeliveries)
				projectDeliverieVOs.add(addProjectDeliveryVO(projectDelivery));
		}
		return projectDeliverieVOs;
	}

	private Object addProjectDeliveryVO(ProjectDelivery projectDelivery) {
		ProjectDeliveryVO projectDeliveryVO = new ProjectDeliveryVO();
		projectDeliveryVO.setProjectDeliveryId(projectDelivery
				.getProjectDeliveryId());
		projectDeliveryVO.setDate(DateFormat
				.convertDateToString(projectDelivery.getDeliveryDate()
						.toString()));
		projectDeliveryVO.setReferenceNumber(projectDelivery
				.getReferenceNumber());
		projectDeliveryVO.setDeliveryTitle(projectDelivery.getDeliveryTitle());
		if (projectDelivery.getProjectTaskSurrogate().getProject() != null)
			projectDeliveryVO.setProjectTitle(projectDelivery
					.getProjectTaskSurrogate().getProject().getProjectTitle());
		else
			projectDeliveryVO.setTaskTitle(projectDelivery
					.getProjectTaskSurrogate().getProjectTask().getTaskTitle());

		if (projectDelivery.getDeliveryStatus() != null)
			projectDeliveryVO.setCurrentStatus(ProjectDeliveryStatus.get(
					projectDelivery.getDeliveryStatus()).name());
		return projectDeliveryVO;
	}

	public List<ProjectDeliveryVO> getAllDeliverablesByTask(Long projectTaskId) {
		List<ProjectDeliveryVO> projectDeliveryVOs = new ArrayList<ProjectDeliveryVO>();
		try {
			List<ProjectDelivery> projectDeliveries = projectDeliveryService
					.getAllDeliveriesByTask(projectTaskId);
			for (ProjectDelivery projectDelivery : projectDeliveries) {
				projectDeliveryVOs.add(convertEntityToVO(projectDelivery));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectDeliveryVOs;
	}

	public List<ProjectDeliveryVO> getAllDeliverablesByProject(Long projectId) {
		List<ProjectDeliveryVO> projectDeliveryVOs = new ArrayList<ProjectDeliveryVO>();
		try {
			List<ProjectDelivery> projectDeliveries = projectDeliveryService
					.getAllDeliverieByProject(projectId);
			for (ProjectDelivery projectDelivery : projectDeliveries) {
				projectDeliveryVOs.add(convertEntityToVO(projectDelivery));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectDeliveryVOs;
	}

	public ProjectDeliveryVO convertEntityToVO(ProjectDelivery projectDelivery) {
		ProjectDeliveryVO projectDeliveryVO = new ProjectDeliveryVO();
		try {
			BeanUtils.copyProperties(projectDeliveryVO, projectDelivery);
			projectDeliveryVO.setProjectDeliveryId(projectDelivery
					.getProjectDeliveryId());
			projectDeliveryVO.setDate(DateFormat
					.convertDateToString(projectDelivery.getDeliveryDate()
							.toString()));
			projectDeliveryVO.setReferenceNumber(projectDelivery
					.getReferenceNumber());
			projectDeliveryVO.setDeliveryTitle(projectDelivery
					.getDeliveryTitle());
			if (projectDelivery.getProjectTaskSurrogate().getProject() != null)
				projectDeliveryVO.setProjectTitle(projectDelivery
						.getProjectTaskSurrogate().getProject()
						.getProjectTitle());
			else
				projectDeliveryVO.setTaskTitle(projectDelivery
						.getProjectTaskSurrogate().getProjectTask()
						.getTaskTitle());

			if (projectDelivery.getDeliveryStatus() != null)
				projectDeliveryVO.setCurrentStatus(ProjectDeliveryStatus.get(
						projectDelivery.getDeliveryStatus()).name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectDeliveryVO;
	}

	public void saveProjectDelivery(ProjectDelivery projectDelivery)
			throws Exception {
		projectDelivery.setImplementation(getImplementation());
		boolean modify = false;
		if (null == projectDelivery.getProjectDeliveryId())
			SystemBL.saveReferenceStamp(ProjectDelivery.class.getName(),
					getImplementation());
		else
			modify = true;
		projectDeliveryService.saveProjectDelivery(projectDelivery);

		// Delivery Alert
		if (modify) {
			modify = false;
			Alert alert = alertBL.getAlertService().getAlertRecordInfo(
					projectDelivery.getProjectDeliveryId(),
					ProjectDelivery.class.getSimpleName());
			if (null != alert) {
				alert.setIsActive(true);
				alertBL.commonSaveAlert(alert);
				modify = true;
			}
		}
		if (!modify) {
			ProjectDelivery projectDeliveryTemp = projectDeliveryService
					.getProjectDeliveryById(projectDelivery
							.getProjectDeliveryId());
			List<ProjectDelivery> projectDeliveries = new ArrayList<ProjectDelivery>();
			projectDeliveries.add(projectDeliveryTemp);
			alertBL.alertProjectDelivery(projectDeliveries);
		}
	}

	public void updateProject(ProjectDelivery projectDelivery) throws Exception {
		if ((byte) projectDelivery.getDeliveryStatus() == (byte) ProjectDeliveryStatus.Delivered
				.getCode()
				|| (byte) projectDelivery.getDeliveryStatus() == (byte) ProjectDeliveryStatus.Cancelled
						.getCode()) {
			Alert alert = alertBL.getAlertService().getAlertRecordInfo(
					projectDelivery.getProjectDeliveryId(),
					ProjectDelivery.class.getSimpleName());
			alert.setIsActive(false);
			alertBL.commonSaveAlert(alert);
		}
		projectDeliveryService.saveProjectDelivery(projectDelivery);
	}

	public void deleteProjectDelivery(ProjectDelivery projectDelivery)
			throws Exception {
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				projectDelivery.getProjectDeliveryId(),
				ProjectDelivery.class.getSimpleName());
		if(alert!=null)
			alertBL.getAlertService().deleteAlert(alert);
		projectDeliveryService.deleteProjectDelivery(projectDelivery);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProjectMileStoneBL getProjectMileStoneBL() {
		return projectMileStoneBL;
	}

	public void setProjectMileStoneBL(ProjectMileStoneBL projectMileStoneBL) {
		this.projectMileStoneBL = projectMileStoneBL;
	}

	public ProjectDeliveryService getProjectDeliveryService() {
		return projectDeliveryService;
	}

	public void setProjectDeliveryService(
			ProjectDeliveryService projectDeliveryService) {
		this.projectDeliveryService = projectDeliveryService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public ProjectTaskService getProjectTaskService() {
		return projectTaskService;
	}

	public void setProjectTaskService(ProjectTaskService projectTaskService) {
		this.projectTaskService = projectTaskService;
	}
}
