package com.aiotech.aios.accounts.service.bl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aiotech.aios.accounts.action.EODBalancingAction;
import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Discount;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.ProductPoint;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Promotion;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.RewardPolicy;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.PointOfSaleService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.CustomerType;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceSalesType;
import com.aiotech.aios.common.to.Constants.Accounts.POSPaymentType;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.util.WorkflowConstants;

public class PointOfSaleBL {

	// Dependencies
	private ProductPricingBL productPricingBL;
	private DiscountBL discountBL;
	private PromotionBL promotionBL;
	private ProductPointBL productPointBL;
	private RewardPolicyBL rewardPolicyBL;
	private CustomerBL customerBL;
	private TransactionBL transactionBL;
	private CombinationBL combinationBL;
	private StoreBL storeBL;
	private StockBL stockBL;
	private POSUserTillBL pOSUserTillBL;
	private LookupMasterBL lookupMasterBL;
	private ProductionReceiveBL productionReceiveBL;
	private MerchandiseExchangeBL merchandiseExchangeBL;
	private ProductDefinitionBL productDefinitionBL;
	private PointOfSaleService pointOfSaleService;
	private SystemService systemService;
	private CreditBL creditBL;

	// Show all PointOfSales
	public List<Object> showAllPointOfSales(long pointOfSaleId, byte salesType,
			String salesDate, long customerId) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleService.getAllPointOfSales(
				getImplementation(), pointOfSaleId, salesType, salesDate,
				customerId);
		List<Object> pointOfSaleVOs = new ArrayList<Object>();
		for (PointOfSale pointOfSale : pointOfSales) {
			pointOfSaleVOs.add(addPointOfSaleVO(pointOfSale));
		}
		return pointOfSaleVOs;
	}

	// Show all PointOfSales
	public List<Object> showDispatchedOrders(long posUserTillId,
			boolean dispatchFlag) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleService
				.getDispatchedOrders(posUserTillId, dispatchFlag);
		List<Object> pointOfSaleVOs = new ArrayList<Object>();
		for (PointOfSale pointOfSale : pointOfSales) {
			pointOfSaleVOs.add(addPointOfSaleVO(pointOfSale));
		}
		return pointOfSaleVOs;
	}

	// Show POS By Reference
	public PointOfSaleVO showPointOfDetailByReference(String salesRefNumber)
			throws Exception {
		PointOfSale pointOfSale = pointOfSaleService
				.getPointOfDetailByReference(getImplementation(),
						salesRefNumber);
		PointOfSaleVO pointOfSaleVO = null;
		if (null != pointOfSale && !("").equals(pointOfSale)) {
			if (null != pointOfSale.getDiscountMethod()
					|| null != pointOfSale.getPromotionMethod()) {
				pointOfSaleVO = new PointOfSaleVO();
				pointOfSaleVO.setSalesType("NoReturns");
			} else
				return addPointOfSaleVO(pointOfSale);
		}
		return pointOfSaleVO;
	}

	// Get Product Pricing
	public ProductPricingDetail getDefalutProductPricingDetail(long productId,
			int storeId) throws Exception {
		return productPricingBL.getPricingDetail(productId, storeId);
	}

	// Get Product Pricing
	public ProductPricingDetail getNonUnfiedProductPricingDetail(
			long productId, long storeId) throws Exception {
		return productPricingBL.getNonUnfiedProductPricingDetail(productId,
				storeId);
	}

	// Get Product Pricing

	public ProductPricingDetail getDefalutProductPricingDetail(long productId)
			throws Exception {
		return productPricingBL.getPricingDetail(productId);
	}

	// Get Product Pricing
	public ProductPricingCalc getDefalutProductPricingCalc(long productId,
			int storeId) throws Exception {
		return productPricingBL.getPricingDetails(productId, storeId);
	}

	// Get Product Pricing
	public ProductPricingCalc getComboProductPricingCalc(long productId)
			throws Exception {
		return productPricingBL.getComboPricingDetails(productId);
	}

	// Get Product Points
	public List<ProductVO> getProductPoints(List<ProductVO> productVOs)
			throws Exception {
		for (ProductVO productVO : productVOs) {
			ProductPoint productPoint = productPointBL
					.getProductPointsByProductId(productVO.getProductId());
			if (null != productPoint && !("").equals(productPoint)) {
				if (null != productPoint.getPurchaseAmount()
						&& productPoint.getPurchaseAmount() > 0)
					productVO
							.setProductPurchasePoints((productVO
									.getTotalPrice() / productPoint
									.getPurchaseAmount())
									* productPoint.getStatusPoints());
				else
					productVO.setProductPurchasePoints(productPoint
							.getStatusPoints());
				productVO.setTotalPoints(productVO.getProductPurchasePoints());
			}
		}
		return productVOs;
	}

	public List<ProductVO> getPromotionDetails(List<ProductVO> productVOs,
			long customerId) throws Exception {
		PromotionMethod promotionMethod = null;
		Promotion promotion = null;
		boolean customerPromotion = false;
		for (ProductVO productVO : productVOs) {
			if (customerId > 0) {
				promotion = promotionBL.getPromotionByCustomerId(customerId);
				if (null != promotion) {
					customerPromotion = true;
					productVO.setCustomerPromotion(customerPromotion);
				} else {
					promotion = null;
					promotionMethod = null;
					productVO.setCustomerPromotion(customerPromotion);
				}
			}
			if (null == promotion || ("").equals(promotion)) {
				promotion = promotionBL.getPromotionByProductId(productVO
						.getProductId());
			}
			boolean promotionFlag = false;
			if (null != promotion && !("").equals(promotion)) {
				ProductPricingCalc productPricingCalc = processPromotion(
						promotion, productVO, customerId);
				if (null != promotion.getMiniumSales()
						&& promotion.getMiniumSales() != '\u0000') {
					if (promotion.getMiniumSales() == 'Q') {
						if (productVO.getQuantity() >= promotion
								.getSalesValue())
							promotionFlag = true;
					} else if (promotion.getMiniumSales() == 'A') {
						if (productVO.getTotalPrice() >= promotion
								.getSalesValue())
							promotionFlag = true;
					}
					if (null != productPricingCalc
							&& !("").equals(productPricingCalc)) {
						if (Constants.Accounts.ProductCalculationType
								.get(productPricingCalc.getCalculationType())
								.equals(Constants.Accounts.ProductCalculationType.Margin)
								|| Constants.Accounts.ProductCalculationType
										.get(productPricingCalc
												.getCalculationType())
										.equals(Constants.Accounts.ProductCalculationType.Markup))
							productVO.setFinalUnitPrice(productPricingCalc
									.getSalePrice());
					}
				} else
					promotionFlag = true;
				if (promotionFlag) {
					promotionMethod = new ArrayList<PromotionMethod>(
							promotion.getPromotionMethods()).get(0);
					if (customerId == 0 || !customerPromotion) {
						productVO.setPromotionMethodId(promotionMethod
								.getPromotionMethodId());
						productVO.setPromotionPoints(null != promotionMethod
								.getProductPoint() ? promotionMethod
								.getProductPoint() : 0);
						productVO.setTotalPoints((null != promotionMethod
								.getProductPoint() ? promotionMethod
								.getProductPoint() : 0)
								+ productVO.getProductPurchasePoints());
						if (Constants.Accounts.PACType.Coupon.getCode().equals(
								promotion.getRewardType())) {

							if (null != promotion.getMiniumSales()
									&& promotion.getMiniumSales() != '\u0000') {
								if (promotion.getMiniumSales() == 'Q') {
									if (productVO.getQuantity() >= promotion
											.getSalesValue())
										productVO
												.setNumberOfCoupons((int) (productVO
														.getQuantity() / promotion
														.getSalesValue()));
								} else if (promotion.getMiniumSales() == 'A') {
									if (productVO.getTotalPrice() >= promotion
											.getSalesValue())
										productVO
												.setNumberOfCoupons((int) (productVO
														.getTotalPrice() / promotion
														.getSalesValue()));
								}
							} else
								productVO.setNumberOfCoupons(1);

						} else {
							double promotionAmount = 0;
							if (Constants.Accounts.PACType.Amount.getCode()
									.equals(promotionMethod.getPacType())) {
								promotionAmount = productVO.getFinalUnitPrice()
										- promotionMethod.getPromotionAmount();
								productVO.setCustomerDiscount(productVO
										.getFinalUnitPrice() - promotionAmount);
							}

							else if (Constants.Accounts.PACType.Percentage
									.getCode().equals(
											promotionMethod.getPacType())) {
								promotionAmount = productVO.getFinalUnitPrice()
										- ((productVO.getFinalUnitPrice() * promotionMethod
												.getPromotionAmount()) / 100);
								productVO.setCustomerDiscount(productVO
										.getFinalUnitPrice() - promotionAmount);
							}
						}
					}
				}
			}
			productVO.setPromotionFlag(promotionFlag);
		}
		if (customerId > 0 && null != promotionMethod && customerPromotion) {
			for (ProductVO productVO : productVOs) {
				if (Constants.Accounts.PACType.Coupon.getCode().equals(
						promotion.getRewardType())) {
					if (null != promotion.getMiniumSales()
							&& promotion.getMiniumSales() != '\u0000') {
						if (promotion.getMiniumSales() == 'Q') {
							double salesQuantity = getTotalSaleQuantity(productVOs);
							if (salesQuantity >= promotion.getSalesValue())
								productVO
										.setCustomerCoupons((int) (salesQuantity / promotion
												.getSalesValue()));
						} else if (promotion.getMiniumSales() == 'A') {
							double saleValue = getTotalSaleValue(productVOs);
							if (saleValue >= promotion.getSalesValue())
								productVO
										.setCustomerCoupons((int) (saleValue / promotion
												.getSalesValue()));
						}
					} else
						productVO.setCustomerCoupons(1);
				} else {
					double promotionAmount = 0;
					double saleValue = getTotalSaleValue(productVOs);
					if (Constants.Accounts.PACType.Amount.getCode().equals(
							promotionMethod.getPacType())) {
						promotionAmount = saleValue
								- promotionMethod.getPromotionAmount();
						productVO.setCustomerDiscount(saleValue
								- promotionAmount);
					} else if (Constants.Accounts.PACType.Percentage.getCode()
							.equals(promotionMethod.getPacType())) {
						promotionAmount = saleValue
								- ((saleValue * promotionMethod
										.getPromotionAmount()) / 100);
						productVO.setCustomerDiscount(saleValue
								- promotionAmount);
					}
				}
				productVO.setPromotionMethodId(promotionMethod
						.getPromotionMethodId());
				productVO.setPromotionPoints(null != promotionMethod
						.getProductPoint() ? promotionMethod.getProductPoint()
						: 0);
				break;
			}
		}
		return productVOs;
	}

	public double getTotalSaleValue(List<ProductVO> productVOs) {
		double salesValue = 0;
		for (ProductVO productVO : productVOs)
			salesValue += productVO.getQuantity()
					* productVO.getFinalUnitPrice();
		return salesValue;

	}

	private double getTotalSaleQuantity(List<ProductVO> productVOs) {
		double salesQuantity = 0;
		for (ProductVO productVO : productVOs)
			salesQuantity += productVO.getQuantity();
		return salesQuantity;
	}

	// Get Discount Details
	public List<ProductVO> getDiscountDetails(List<ProductVO> productVOs,
			long customerId) throws Exception {
		if (!productVOs.get(0).isPromotionFlag()) {
			boolean customerPromotion = false;
			Discount discount = null;
			DiscountMethod discountMethod = null;
			for (ProductVO productVO : productVOs) {
				if (customerId > 0) {
					discount = discountBL.getDiscountByCustomerId(customerId);
					if (null != discount) {
						customerPromotion = true;
						productVO.setCustomerPromotion(customerPromotion);
					} else
						productVO.setCustomerPromotion(customerPromotion);
				} else {
					discount = null;
					discountMethod = null;
				}
				if (null == discount) {
					discount = discountBL.getDiscountByProductId(productVO
							.getProductId());
				}

				if (null != discount) {
					double customerDiscount = 0;
					if (null != discount.getLookupDetail()) {
						ProductPricingCalc productPricingCalc = processDiscount(
								discount, productVO, customerId);
						if (null != productPricingCalc) {
							if (Constants.Accounts.ProductCalculationType
									.get(productPricingCalc
											.getCalculationType())
									.equals(Constants.Accounts.ProductCalculationType.Margin)
									|| Constants.Accounts.ProductCalculationType
											.get(productPricingCalc
													.getCalculationType())
											.equals(Constants.Accounts.ProductCalculationType.Markup))
								productVO.setFinalUnitPrice(productPricingCalc
										.getSalePrice());
							else
								productVO.setFinalUnitPrice(productPricingCalc
										.getSalePriceAmount());
							productVO
									.setProductPricingCalcId(productPricingCalc
											.getProductPricingCalcId());
							productVO.setDiscountFlag(true);
							discountMethod = new DiscountMethod();
							discountMethod = new ArrayList<DiscountMethod>(
									discount.getDiscountMethods()).get(0);
							productVO.setDiscountMethodId(discountMethod
									.getDiscountMethodId());
							if (customerId == 0 || !customerPromotion) {
								if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Amount
										.getCode() == (byte) discount
										.getDiscountType()) {
									customerDiscount = productVO
											.getFinalUnitPrice()
											- discountMethod.getFlatAmount();
									productVO.setCustomerDiscount(productVO
											.getFinalUnitPrice()
											- customerDiscount);
								} else if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Percentage
										.getCode() == (byte) discount
										.getDiscountType()) {
									customerDiscount = productVO
											.getFinalUnitPrice()
											- ((productVO.getFinalUnitPrice() * discountMethod
													.getFlatPercentage()) / 100);
									productVO.setCustomerDiscount(productVO
											.getFinalUnitPrice()
											- customerDiscount);
								}
							}
						}
					} else {
						ProductPricingDetail productPricingDetail = processDiscountByPricingDetail(
								discount, productVO);
						if (null != productPricingDetail) {
							productVO.setFinalUnitPrice(productPricingDetail
									.getSellingPrice());
							productVO.setDiscountFlag(true);
							discountMethod = new DiscountMethod();
							discountMethod = new ArrayList<DiscountMethod>(
									discount.getDiscountMethods()).get(0);
							productVO.setDiscountMethodId(discountMethod
									.getDiscountMethodId());
							if (customerId == 0 || !customerPromotion) {
								if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Amount
										.getCode() == (byte) discount
										.getDiscountType()) {
									customerDiscount = productVO
											.getFinalUnitPrice()
											- discountMethod.getFlatAmount();
									productVO.setCustomerDiscount(productVO
											.getFinalUnitPrice()
											- customerDiscount);
								} else if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Percentage
										.getCode() == (byte) discount
										.getDiscountType()) {
									customerDiscount = productVO
											.getFinalUnitPrice()
											- ((productVO.getFinalUnitPrice() * discountMethod
													.getFlatPercentage()) / 100);
									productVO.setCustomerDiscount(productVO
											.getFinalUnitPrice()
											- customerDiscount);
								}
							}
						}
					}
				}
				productVO.setTotalPrice(productVO.getFinalUnitPrice()
						* productVO.getQuantity());
				productVO.setDisplayUnitPrice(AIOSCommons
						.formatAmount(productVO.getFinalUnitPrice()));
				productVO.setDisplayPrice(AIOSCommons.formatAmount(productVO
						.getFinalUnitPrice() * productVO.getQuantity()));
			}
			if (customerId > 0 && null != discountMethod && customerPromotion) {
				double customerDiscount = 0;
				double totalSales = 0;
				for (ProductVO productVO : productVOs) {
					totalSales += productVO.getFinalUnitPrice();
					productVO.setTotalPrice(productVO.getFinalUnitPrice()
							* productVO.getQuantity());
					productVO.setDisplayUnitPrice(AIOSCommons
							.formatAmount(productVO.getFinalUnitPrice()));
					productVO.setDisplayPrice(AIOSCommons
							.formatAmount(productVO.getFinalUnitPrice()
									* productVO.getQuantity()));
				}

				if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Amount
						.getCode() == (byte) discount.getDiscountType()) {
					customerDiscount = totalSales
							- discountMethod.getFlatAmount();
					productVOs.get(0).setCustomerDiscount(
							totalSales - customerDiscount);
				} else if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Percentage
						.getCode() == (byte) discount.getDiscountType()) {
					customerDiscount = totalSales
							- ((totalSales * discountMethod.getFlatPercentage()) / 100);
					productVOs.get(0).setCustomerDiscount(
							totalSales - customerDiscount);
				}
			}
		}
		return productVOs;
	}

	// Get Discount Amount By Calc table
	private ProductPricingCalc processDiscount(Discount discount,
			ProductVO productVO, long customerId) throws Exception {
		ProductPricingCalc productPricingCalc = null;
		if (customerId > 0) {
			productPricingCalc = productPricingBL
					.getProductPricingCalcWithCustomer(
							productVO.getProductId(), customerId, productVO
									.getStoreId(), discount.getLookupDetail()
									.getLookupDetailId());
		}
		if (null == productPricingCalc) {
			productPricingCalc = productPricingBL.getProductPricingCalc(
					productVO.getProductId(), productVO.getStoreId(), discount
							.getLookupDetail().getLookupDetailId());
		}
		if (null != productPricingCalc) {
			boolean discountFlag = false;
			if (null != discount.getMinimumSales()
					&& discount.getMinimumSales() != '\u0000') {
				if (discount.getMinimumSales() == 'Q') {
					if (productVO.getQuantity() >= discount.getMinimumValue()) {
						discountFlag = true;
					}
				} else if (discount.getMinimumSales() == 'A') {
					if (productVO.getTotalPrice() >= discount.getMinimumValue()) {
						discountFlag = true;
					}
				}
			} else
				discountFlag = true;
			if (discountFlag)
				return productPricingCalc;
		}
		return null;
	}

	// Get Discount Amount by Pricing Detail table
	private ProductPricingDetail processDiscountByPricingDetail(
			Discount discount, ProductVO productVO) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		String unifiedPrice = "";
		ProductPricingDetail productPricingDetail = null;
		if (session.getAttribute("unified_price") != null)
			unifiedPrice = session.getAttribute("unified_price").toString();
		if (!unifiedPrice.equals("false"))
			productPricingDetail = productPricingBL
					.getDefalutProductPricingDetail(productVO.getProductId());
		else {
			productPricingDetail = productPricingBL
					.getNonunfiedProductPricingDetail(productVO.getProductId(),
							productVO.getStoreId());
			if (null == productPricingDetail
					|| null == productPricingDetail.getProductPricingDetailId()) {
				productPricingDetail = productPricingBL
						.getDefalutProductPricingDetail(productVO
								.getProductId());
			}
		}

		if (null != productPricingDetail) {
			boolean discountFlag = false;
			if (null != discount.getMinimumSales()
					&& discount.getMinimumSales() != '\u0000') {
				if (discount.getMinimumSales() == 'Q') {
					if (productVO.getQuantity() >= discount.getMinimumValue()) {
						discountFlag = true;
					}
				} else if (discount.getMinimumSales() == 'A') {
					if (productVO.getTotalPrice() >= discount.getMinimumValue()) {
						discountFlag = true;
					}
				}
			} else
				discountFlag = true;
			if (discountFlag)
				return productPricingDetail;
		}
		return null;
	}

	// Get Promotion Amount
	private ProductPricingCalc processPromotion(Promotion promotion,
			ProductVO productVO, long customerId) throws Exception {
		ProductPricingCalc productPricingCalc = null;
		if (customerId > 0) {
			productPricingCalc = productPricingBL
					.getProductPricingCalcWithCustomer(
							productVO.getProductId(), customerId, productVO
									.getStoreId(), promotion.getLookupDetail()
									.getLookupDetailId());
		}
		if (null == productPricingCalc || ("").equals(productPricingCalc)) {
			productPricingCalc = productPricingBL.getProductPricingCalc(
					productVO.getProductId(), productVO.getStoreId(), promotion
							.getLookupDetail().getLookupDetailId());
		}
		if (null != productPricingCalc && !("").equals(productPricingCalc)) {
			boolean promotionFlag = false;
			if (null != promotion.getMiniumSales()
					&& promotion.getMiniumSales() != '\u0000') {
				if (promotion.getMiniumSales() == 'Q') {
					if (productVO.getQuantity() >= promotion.getSalesValue()) {
						promotionFlag = true;
					}
				} else if (promotion.getMiniumSales() == 'A') {
					if (productVO.getTotalPrice() >= promotion.getSalesValue()) {
						promotionFlag = true;
					}
				}
			} else
				promotionFlag = true;
			if (promotionFlag)
				return productPricingCalc;
		}
		return null;
	}

	// Get Coupon Sale Value
	public RewardPolicy calculateCouponReward(int couponNumber, long customerId)
			throws Exception {
		Coupon coupon = rewardPolicyBL.getCouponBL().getCouponByCouponNumber(
				couponNumber);
		if (null != coupon) {
			Customer customer = null;
			if (customerId > 0) {
				customer = customerBL.getCustomerService().getCustomerById(
						customerId);
			}
			List<RewardPolicy> rewardPolicies = rewardPolicyBL
					.getRewardPolicyService().getRewardPolicyByCouponId(
							coupon.getCouponId());
			if (null != rewardPolicies && rewardPolicies.size() > 0) {
				if (null != customer && null != customer.getMemberType()) {
					for (RewardPolicy reward : rewardPolicies)
						if (reward.getRewardType().equals(
								customer.getMemberType())) {
							return reward;

						}
				} else {
					for (RewardPolicy reward : rewardPolicies)
						if (reward.getRewardType().equals(
								Constants.Accounts.RewardType.Open.getCode()))
							return reward;
				}
			}

		}
		return null;
	}

	// Get PointOfSale by Current Date
	public List<PointOfSale> getUnBalancedPointOfSaleByCurrentDate()
			throws Exception {
		return pointOfSaleService.getUnBalancedPointOfSaleByCurrentDate(
				this.getImplementation(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()),
				DateFormat.getToDate(Calendar.getInstance().getTime()));
	}

	// Get PointOfSale by Current Date
	public List<PointOfSale> getBalancedPointOfSaleByCurrentDate()
			throws Exception {
		return pointOfSaleService.getBalancedPointOfSaleByCurrentDate(
				this.getImplementation(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()),
				DateFormat.getToDate(Calendar.getInstance().getTime()));
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSaleVO> getPointOfSaleReceiptsBySalesDate(
			List<Object> aaData, String salesDate) throws Exception {
		JSONObject jsonObject = new JSONObject();
		for (Object object : aaData)
			jsonObject.put("storeObjects", object);
		String storeObjects = jsonObject.getString("storeObjects");
		JSONObject storeObject = new JSONObject(storeObjects);
		JSONArray recordArray = storeObject.getJSONArray("record");
		List<Store> stores = new ArrayList<Store>();
		Store store = null;
		for (int i = 0; i < recordArray.length(); i++) {
			org.json.JSONObject jsonobj = recordArray.getJSONObject(i);
			store = new Store();
			store.setStoreId(jsonobj.getLong("storeId"));
			stores.add(store);
		}
		List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
		for (Store storeObj : stores) {
			List<PointOfSaleReceipt> pointOfSaleReceipt = pointOfSaleService
					.getPointOfSaleReceiptsBySalesDate(
							this.getImplementation(), storeObj.getStoreId(),
							DateFormat.getFromDate((DateFormat
									.convertStringToDate(salesDate))),
							DateFormat.getToDate((DateFormat
									.convertStringToDate(salesDate))),
							Constants.Accounts.POSPaymentType.Coupon.getCode());
			if (null != pointOfSaleReceipt && pointOfSaleReceipt.size() > 0)
				pointOfSaleReceipts.addAll(pointOfSaleReceipt);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			Map<Byte, PointOfSaleVO> pointOfSaleVOs = new HashMap<Byte, PointOfSaleVO>();
			if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
				for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
					addPointOfSaleVO(pointOfSaleReceipt, pointOfSaleVOs);
			}
			return new ArrayList<PointOfSaleVO>(pointOfSaleVOs.values());
		}
		return null;
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSaleVO> getPointOfSaleReceiptsBySalesDate(int storeId,
			String salesDate) throws Exception {
		List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
		List<PointOfSaleReceipt> pointOfSaleReceipt = pointOfSaleService
				.getPointOfSaleReceiptsBySalesDate(this.getImplementation(),
						storeId, DateFormat.getFromDate((DateFormat
								.convertStringToDate(salesDate))), DateFormat
								.getToDate((DateFormat
										.convertStringToDate(salesDate))),
						Constants.Accounts.POSPaymentType.Coupon.getCode());
		if (null != pointOfSaleReceipt && pointOfSaleReceipt.size() > 0)
			pointOfSaleReceipts.addAll(pointOfSaleReceipt);

		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			Map<Byte, PointOfSaleVO> pointOfSaleVOs = new HashMap<Byte, PointOfSaleVO>();
			if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
				Credit credit = null;
				for (PointOfSaleReceipt receipt : pointOfSaleReceipts) {
					credit = creditBL
							.getCreditService()
							.getCreditNoteByRecordIdUseCase(
									receipt.getPointOfSale().getPointOfSaleId(),
									PointOfSale.class.getSimpleName());
					double posQuantity = 0;
					if (null != credit) {
						for (PointOfSaleDetail posDetail : receipt
								.getPointOfSale().getPointOfSaleDetails()) {
							posQuantity += posDetail.getQuantity();
						}
					} else
						addPointOfSaleVO(receipt, pointOfSaleVOs);
				}
			}
			return new ArrayList<PointOfSaleVO>(pointOfSaleVOs.values());
		}
		return null;
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSaleVO> getPointOfSaleReceiptsBySalesDate(
			List<PointOfSale> pointOfSales) throws Exception {
		List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
		Credit credit = null;
		for (PointOfSale pointOfSale : pointOfSales) {
			credit = creditBL.getCreditService()
					.getCreditNoteByRecordIdUseCase(
							pointOfSale.getPointOfSaleId(),
							PointOfSale.class.getSimpleName());
			if (null != credit) {
				double returnAmount = credit.getTotalReturn();
				if (null != pointOfSale.getPointOfSaleReceipts()
						&& pointOfSale.getPointOfSaleReceipts().size() > 0) {
					for (PointOfSaleReceipt saleReceipt : pointOfSale
							.getPointOfSaleReceipts()) {
						double receiptAmount = saleReceipt.getReceipt()
								- (null != saleReceipt.getBalanceDue() ? saleReceipt
										.getBalanceDue() : 0);
						if (returnAmount > receiptAmount) {
							returnAmount -= receiptAmount;
							saleReceipt.setReceipt(0d);
						} else {
							saleReceipt
									.setReceipt(receiptAmount - returnAmount);
							if (saleReceipt.getReceipt() > 0)
								pointOfSaleReceipts.add(saleReceipt);
							break;
						}
					}
				}
			} else {
				if (null != pointOfSale.getPointOfSaleReceipts()
						&& pointOfSale.getPointOfSaleReceipts().size() > 0) {
					pointOfSaleReceipts
							.addAll(new ArrayList<PointOfSaleReceipt>(
									pointOfSale.getPointOfSaleReceipts()));
				}
			}
		}

		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			Map<Byte, PointOfSaleVO> pointOfSaleVOs = new HashMap<Byte, PointOfSaleVO>();
			if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
				for (PointOfSaleReceipt receipt : pointOfSaleReceipts) {
					addPointOfSaleVO(receipt, pointOfSaleVOs);
				}
			}
			return new ArrayList<PointOfSaleVO>(pointOfSaleVOs.values());
		}
		return null;
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSaleVO> getPointOfSaleByEmployeeService(long storeId,
			String salesDate) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleService
				.getPointOfSaleReceiptsByEmployeeService(this
						.getImplementation(), storeId,
						DateFormat.getFromDate((DateFormat
								.convertStringToDate(salesDate))), DateFormat
								.getToDate((DateFormat
										.convertStringToDate(salesDate))),
						InvoiceSalesType.Employee_Meal.getCode());

		if (null != pointOfSales && pointOfSales.size() > 0) {
			Map<Long, PointOfSaleVO> pointOfSaleVOs = new HashMap<Long, PointOfSaleVO>();
			if (null != pointOfSales && pointOfSales.size() > 0) {
				for (PointOfSale sale : pointOfSales)
					addPointOfSaleVO(sale, pointOfSaleVOs);
			}
			return new ArrayList<PointOfSaleVO>(pointOfSaleVOs.values());
		}
		return null;
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSale> getPointOfSaleBySalesDate(List<Object> aaData,
			String salesDate) throws Exception {
		JSONObject jsonObject = new JSONObject();
		for (Object object : aaData)
			jsonObject.put("storeObjects", object);
		String storeObjects = jsonObject.getString("storeObjects");
		JSONObject storeObject = new JSONObject(storeObjects);
		JSONArray recordArray = storeObject.getJSONArray("record");
		List<Store> stores = new ArrayList<Store>();
		Store store = null;
		for (int i = 0; i < recordArray.length(); i++) {
			org.json.JSONObject jsonobj = recordArray.getJSONObject(i);
			store = new Store();
			store.setStoreId(jsonobj.getLong("storeId"));
			stores.add(store);
		}
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
		for (Store storeObj : stores) {
			List<PointOfSale> pointOfSale = pointOfSaleService
					.getPointOfSaleBySalesDate(this.getImplementation(),
							storeObj.getStoreId(), DateFormat
									.getFromDate((DateFormat
											.convertStringToDate(salesDate))),
							DateFormat.getToDate((DateFormat
									.convertStringToDate(salesDate))),
							Constants.Accounts.POSPaymentType.Coupon.getCode());
			if (null != pointOfSale && pointOfSale.size() > 0)
				pointOfSales.addAll(pointOfSale);
		}

		return pointOfSales;
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSale> getEODPointOfSales(int storeId) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleService
				.getPointOfSalesByStore(getImplementation(), storeId,
						InventorySubCategory.Craft0Manufacturer.getCode());
		return pointOfSales;
	}

	// Get Point of Sale Receipts by current date
	public List<PointOfSale> getEODPointOfSaleBySalesDate(long storeId,
			String salesDate) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleService
				.getPointOfSaleBySalesDate(this.getImplementation(), storeId,
						DateFormat.getFromDate((DateFormat
								.convertStringToDate(salesDate))), DateFormat
								.getToDate((DateFormat
										.convertStringToDate(salesDate))),
						Constants.Accounts.POSPaymentType.Coupon.getCode());

		return pointOfSales;
	}

	// Save Point of Sale Session Order
	public PointOfSale savePOSOrderSession(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSale.setImplementation(getImplementation());

		pointOfSaleService.savePOSOrderSession(pointOfSale, pointOfSaleDetails);
		return pointOfSale;
	}

	// Save Point of Sale Session Order
	public void saveEmployeePOSOrder(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSale.setImplementation(getImplementation());
		SystemBL.saveReferenceStamp(PointOfSale.class.getName(),
				this.getImplementation());
		pointOfSaleService.savePOSOrderSession(pointOfSale, pointOfSaleDetails);
	}

	// Save Point of Sale
	public void savePointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetailsTemp,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleVO> returnProductDetails, POSUserTill posUserSession)
			throws Exception {
		// Check sale offline/online
		Properties prop = new Properties();
		InputStream input = null;
		input = EODBalancingAction.class
				.getResourceAsStream("/dbconfig.properties");
		prop.load(input);
		FileInputStream inputStream = new FileInputStream(prop.getProperty(
				"SYNCFILE-LOCATION").trim());
		Properties props = new Properties();
		props.load(inputStream);
		inputStream.close();
		String offlineEnv = props.getProperty("OFFLINE-MODE").trim();
		pointOfSale.setIsEodBalanced(true);
		if (offlineEnv.equalsIgnoreCase("Y")) {
			pointOfSale.setOfflineEntry(true);
			pointOfSale.setIsEodBalanced(false);
		} else
			pointOfSale.setOfflineEntry(false);
		pointOfSale.setImplementation(getImplementation());
		if (null != pointOfSaleDetailsTemp && pointOfSaleDetailsTemp.size() > 0)
			pointOfSaleService.deletePointOfSaleDetails(pointOfSaleDetailsTemp);
		if (null == pointOfSale.getPointOfSaleId())
			SystemBL.saveReferenceStamp(PointOfSale.class.getName(),
					this.getImplementation());
		pointOfSaleService.savePointOfSale(pointOfSale, pointOfSaleDetails,
				pointOfSaleCharges, pointOfSaleReceipts, pointOfSaleOffers);
		updateSaleStockTransaction(pointOfSale, pointOfSaleDetails,
				pointOfSaleReceipts, pointOfSaleCharges, posUserSession, null);

	}

	// Update PointOfSale
	public void updateSaleStockTransaction(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<PointOfSaleCharge> pointOfSaleCharges,
			POSUserTill posUserSession, Implementation implementation)
			throws Exception {
		List<Stock> stocks = new ArrayList<Stock>();
		Store store = posUserSession.getStore();
		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0) {
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails) {
				stocks.add(addStock(pointOfSaleDetail, store,
						getImplementation()));
			}
			stockBL.updateStockDetails(null, stocks);
			if (null == pointOfSale.getOfflineEntry()
					|| !pointOfSale.getOfflineEntry())
				processPointOfSaleTransaction(pointOfSale, pointOfSaleDetails,
						pointOfSaleReceipts, posUserSession, implementation);
		}
	}

	// Update PointOfSale
	public void updateSaleStockTransaction(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			POSUserTill posUserSession) throws Exception {
		List<Stock> stocks = new ArrayList<Stock>();
		Store store = posUserSession.getStore();
		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0) {
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails) {
				stocks.add(addStock(pointOfSaleDetail, store,
						getImplementation()));
			}
			stockBL.updateStockDetails(null, stocks);
			processPointOfSaleTransaction(pointOfSale, pointOfSaleDetails,
					pointOfSaleReceipts, posUserSession, null);
		}
	}

	@SuppressWarnings("unused")
	private TransactionDetail createRevenueTransactionDetail(
			double receiptAmount, boolean creditFlag) throws Exception {
		return transactionBL.createTransactionDetail(receiptAmount,
				getImplementation().getClearingAccount(), creditFlag, null,
				null, null);
	}

	// Update Point of Sale
	public void updateDispatchPointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<PointOfSaleVO> returnProductDetails,
			List<PointOfSaleDetail> pointOfSaleDetails,
			POSUserTill posUserSession) throws Exception {
		pointOfSale.setImplementation(getImplementation());

		// Check sale offline/online
		Properties prop = new Properties();
		InputStream input = null;
		input = EODBalancingAction.class
				.getResourceAsStream("/dbconfig.properties");
		prop.load(input);
		FileInputStream inputStream = new FileInputStream(prop.getProperty(
				"SYNCFILE-LOCATION").trim());
		Properties props = new Properties();
		props.load(inputStream);
		inputStream.close();
		String offlineEnv = props.getProperty("OFFLINE-MODE").trim();
		pointOfSale.setIsEodBalanced(true);
		if (offlineEnv.equalsIgnoreCase("Y")) {
			pointOfSale.setOfflineEntry(true);
			pointOfSale.setIsEodBalanced(false);
		} else
			pointOfSale.setOfflineEntry(false);
		pointOfSaleService.updateDispatchPointOfSale(pointOfSale,
				pointOfSaleReceipts);
		if (null != returnProductDetails && returnProductDetails.size() > 0) {
			List<PointOfSaleDetail> returnPointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			for (PointOfSaleVO returnPOS : returnProductDetails) {
				returnPointOfSaleDetails
						.addAll(createReturnPOSSalesDetail(returnPOS
								.getPointOfSaleDetailVOs()));
			}
			pointOfSaleService.savePointOfSaleDetails(returnPointOfSaleDetails);
		}
		updateSaleStockTransaction(pointOfSale, pointOfSaleDetails,
				pointOfSaleReceipts, posUserSession);
	}

	// Add Product Map
	private Map<Long, ProductVO> addProductVOMap(
			PointOfSaleDetail pointOfSaleDetail, Shelf shelf, double basePrice) {
		Map<Long, ProductVO> productMap = new HashMap<Long, ProductVO>();
		ProductVO productVO = new ProductVO();
		productVO.setProductId(pointOfSaleDetail.getProductByProductId()
				.getProductId());
		productVO.setQuantity(pointOfSaleDetail.getQuantity());
		productVO.setFinalUnitPrice(basePrice);
		productVO.setShelfId(shelf.getShelfId());
		productVO.setCostingType(pointOfSaleDetail.getProductByProductId()
				.getCostingType());
		productMap.put(
				pointOfSaleDetail.getProductByProductId().getProductId(),
				productVO);
		return productMap;
	}

	public void updateEODLocalPointOfSales(List<PointOfSale> pointOfSales)
			throws Exception {
		for (PointOfSale pointOfSale : pointOfSales) {
			pointOfSale.setIsEodBalanced(true);
			pointOfSale.setOfflineEntry(null);
			pointOfSale.setProcessedSession((byte) 2);
		}
		pointOfSaleService.updateEODPointOfSales(pointOfSales);
	}

	public void updateEODProductionSales(List<PointOfSale> pointOfSales)
			throws Exception {
		for (PointOfSale pointOfSale : pointOfSales) {
			pointOfSale.setIsEodBalanced(true);
			pointOfSale.setOfflineEntry(null);
			pointOfSale.setProcessedSession((byte) 2);
		}
		pointOfSaleService.updateEODPointOfSales(pointOfSales);
	}

	// Update PointOfSale
	public void updateEODPointOfSales(List<PointOfSale> pointOfSales)
			throws Exception {
		ProductPricingDetail productPricingDetail = null;
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		List<TransactionDetail> cosTransactionDetails = new ArrayList<TransactionDetail>();
		Implementation implementation = pointOfSales.get(0).getImplementation();
		List<Stock> stocks = new ArrayList<Stock>();
		Map<Long, Map<Long, ProductVO>> salesStock = new HashMap<Long, Map<Long, ProductVO>>();
		for (PointOfSale pointOfSale : pointOfSales) {
			pointOfSale.setIsEodBalanced(true);
			Store store = pointOfSale.getPOSUserTill().getStore();
			Shelf shelf = storeBL.getStoreService().getShelfByStoreId(
					store.getStoreId());
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSale
					.getPointOfSaleDetails()) {
				productPricingDetail = productPricingBL
						.getProductPricingDetail(pointOfSaleDetail
								.getProductByProductId().getProductId(),
								pointOfSale.getPOSUserTill().getStore()
										.getStoreId());
				Map<Long, ProductVO> productMap = null;
				if (null != salesStock && salesStock.size() > 0) {
					if (salesStock.containsKey(pointOfSale.getPOSUserTill()
							.getStore().getStoreId())) {
						productMap = salesStock.get(pointOfSale
								.getPOSUserTill().getStore().getStoreId());
						if (productMap.containsKey(pointOfSaleDetail
								.getProductByProductId().getProductId())) {
							ProductVO productVO = productMap
									.get(pointOfSaleDetail
											.getProductByProductId()
											.getProductId());
							productVO.setQuantity(productVO.getQuantity()
									+ pointOfSaleDetail.getQuantity());
							productMap.put(pointOfSaleDetail
									.getProductByProductId().getProductId(),
									productVO);
							salesStock.put(pointOfSale.getPOSUserTill()
									.getStore().getStoreId(), productMap);
						} else {
							productMap.putAll(addProductVOMap(
									pointOfSaleDetail, shelf,
									productPricingDetail.getBasicCostPrice()));
							salesStock.put(pointOfSale.getPOSUserTill()
									.getStore().getStoreId(), productMap);
						}
					} else {
						salesStock.put(
								pointOfSale.getPOSUserTill().getStore()
										.getStoreId(),
								addProductVOMap(pointOfSaleDetail, shelf,
										productPricingDetail
												.getBasicCostPrice()));
					}
				} else {
					salesStock.put(
							pointOfSale.getPOSUserTill().getStore()
									.getStoreId(),
							addProductVOMap(pointOfSaleDetail, shelf,
									productPricingDetail.getBasicCostPrice()));
				}
				transactionDetails.add(createSalesCreditEntry(
						pointOfSaleDetail, pointOfSale.getPOSUserTill()
								.getStore(), false));

				cosTransactionDetails.add(createCOSEntry(productPricingDetail,
						pointOfSaleDetail, pointOfSale.getPOSUserTill()
								.getStore(), true));
				cosTransactionDetails.add(createInventoryEntry(
						productPricingDetail, pointOfSaleDetail, pointOfSale
								.getPOSUserTill().getStore(), false));
				// Add Stock
				stocks.add(addStock(pointOfSaleDetail, store, implementation));
			}

			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSale
					.getPointOfSaleReceipts()) {
				if (pointOfSaleReceipt.getReceiptType().equals(
						Constants.Accounts.POSPaymentType.Cash.getCode())
						|| pointOfSaleReceipt.getReceiptType() == Constants.Accounts.POSPaymentType.Cash
								.getCode())
					transactionDetails.add(createCashTransactionDetail(
							pointOfSaleReceipt, true));
				else if (pointOfSaleReceipt.getReceiptType().equals(
						Constants.Accounts.POSPaymentType.Card.getCode())
						|| pointOfSaleReceipt.getReceiptType() == Constants.Accounts.POSPaymentType.Card
								.getCode())
					transactionDetails.add(createCardTransactionDetail(
							pointOfSaleReceipt, true));
				else if (pointOfSaleReceipt.getReceiptType().equals(
						Constants.Accounts.POSPaymentType.Cheque.getCode())
						|| pointOfSaleReceipt.getReceiptType() == Constants.Accounts.POSPaymentType.Cheque
								.getCode())
					transactionDetails.add(createChequeTransactionDetail(
							pointOfSaleReceipt, true));
			}
		}

		if (null != salesStock && salesStock.size() > 0) {
			List<ProductionReceiveDetail> productReceiveDetails = new ArrayList<ProductionReceiveDetail>();
			Map<Long, ProductVO> productVOMap = null;
			for (Entry<Long, Map<Long, ProductVO>> iteStore : salesStock
					.entrySet()) {
				productVOMap = iteStore.getValue();
				for (Entry<Long, ProductVO> iteProduct : productVOMap
						.entrySet()) {
					List<StockVO> stockVOs = stockBL.getStockByStoreDetails(
							iteProduct.getKey(), iteStore.getKey());
					double availableQuantity = 0;
					if (null != stockVOs && stockVOs.size() > 0) {
						for (StockVO stockVO : stockVOs)
							availableQuantity += stockVO.getAvailableQuantity();
					}
					ProductVO productVO = iteProduct.getValue();
					if (availableQuantity < productVO.getQuantity()) {
						productVO.setAvailableQuantity(availableQuantity);
						productReceiveDetails.add(productionReceiveBL
								.addProductReceiveDetail(productVO));
					}
				}
			}
			if (null != productReceiveDetails
					&& productReceiveDetails.size() > 0)
				productionReceiveBL.addProductReceive(implementation,
						productReceiveDetails);
		}

		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(createTransaction(
				"POS Sales Entry on ".concat(Calendar.getInstance().getTime()
						.toString()), transactionDetails, implementation));

		transactions.add(createTransaction(
				"POS Inventory Entry on ".concat(Calendar.getInstance()
						.getTime().toString()), cosTransactionDetails,
				implementation));

		transactionBL.saveTransactions(transactions);
		pointOfSaleService.updateEODPointOfSales(pointOfSales);

		stockBL.updateStockDetails(null, stocks);
	}

	// Create Sales Credit Transaction Detail Entry
	private TransactionDetail createSalesCreditEntry(
			PointOfSaleDetail pointOfSaleDetail, Store store, boolean creditFlag)
			throws Exception {
		double salesAmount = pointOfSaleDetail.getUnitRate()
				* pointOfSaleDetail.getQuantity();
		Combination combination = new Combination();
		combination.setCombinationId(getImplementation().getRevenueAccount());
		return transactionBL.createTransactionDetail(salesAmount,
				combination.getCombinationId(), creditFlag, null, null, null);
	}

	// Create COS Debit Transaction Detail Entry
	private TransactionDetail createCOSEntry(
			ProductPricingDetail productPricingDetail,
			PointOfSaleDetail pointOfSaleDetail, Store store, boolean debitFlag)
			throws Exception {
		Combination combination = new Combination();
		combination.setCombinationId(getImplementation().getExpenseAccount());
		return transactionBL.createTransactionDetail((productPricingDetail
				.getBasicCostPrice() * pointOfSaleDetail.getQuantity()),
				combination.getCombinationId(), debitFlag, null, null, null);
	}

	// Create COS Credit Transaction Detail Entry
	private TransactionDetail createInventoryEntry(
			ProductPricingDetail productPricingDetail,
			PointOfSaleDetail pointOfSaleDetail, Store store, boolean creditFlag)
			throws Exception {
		Combination combination = productPricingBL.getProductBL()
				.getProductInventory(
						pointOfSaleDetail.getProductByProductId()
								.getProductId());
		return transactionBL.createTransactionDetail((productPricingDetail
				.getBasicCostPrice() * pointOfSaleDetail.getQuantity()),
				combination.getCombinationId(), creditFlag, null, null, null);
	}

	// Create Cash Transaction Details
	private TransactionDetail createCashTransactionDetail(
			PointOfSaleReceipt pointOfSaleReceipt, boolean debitFlag)
			throws Exception {
		return transactionBL.createTransactionDetail(
				(pointOfSaleReceipt.getReceipt() - (null != pointOfSaleReceipt
						.getBalanceDue() ? pointOfSaleReceipt.getBalanceDue()
						: 0)), getImplementation().getClearingAccount(),
				debitFlag, null, null, null);
	}

	// Create Card Transaction Details
	private TransactionDetail createCardTransactionDetail(
			PointOfSaleReceipt pointOfSaleReceipt, boolean debitFlag)
			throws Exception {
		return transactionBL.createTransactionDetail(
				(pointOfSaleReceipt.getReceipt() - (null != pointOfSaleReceipt
						.getBalanceDue() ? pointOfSaleReceipt.getBalanceDue()
						: 0)), getImplementation().getClearingAccount(),
				debitFlag, null, null, null);
	}

	// Create Cheque Transaction Details
	private TransactionDetail createChequeTransactionDetail(
			PointOfSaleReceipt pointOfSaleReceipt, boolean debitFlag)
			throws Exception {
		return transactionBL.createTransactionDetail(
				(pointOfSaleReceipt.getReceipt() - (null != pointOfSaleReceipt
						.getBalanceDue() ? pointOfSaleReceipt.getBalanceDue()
						: 0)), getImplementation().getClearingAccount(),
				debitFlag, null, null, null);
	}

	// Create Transaction Entry
	private Transaction createTransaction(String transactionDescription,
			List<TransactionDetail> transactionDetails,
			Implementation implementation) throws Exception {
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), transactionDescription,
				Calendar.getInstance().getTime(), transactionBL
						.getCategory("POS ENTRY"), implementation, null, null);
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Add Stock
	public Stock addStock(PointOfSaleDetail pointOfSaleDetail, Store store,
			Implementation implementation) throws Exception {
		Stock stock = new Stock();
		stock.setStore(store);
		stock.setProduct(pointOfSaleDetail.getProductByProductId());
		stock.setQuantity(pointOfSaleDetail.getQuantity());
		stock.setImplementation(implementation);
		return stock;
	}

	// Add Stock
	public Stock addStock(PointOfSaleDetail pointOfSaleDetail, Store store)
			throws Exception {
		Stock stock = new Stock();
		stock.setStore(store);
		stock.setProduct(pointOfSaleDetail.getProductByProductId());
		stock.setQuantity(pointOfSaleDetail.getQuantity());
		stock.setImplementation(getImplementation());
		stock.setUnitRate(pointOfSaleDetail.getUnitRate());
		return stock;
	}

	// Add PointOfSale VO
	private void addPointOfSaleVO(PointOfSale pointOfSale,
			Map<Long, PointOfSaleVO> pointOfSaleVOs) throws Exception {
		PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
		double serveAmount = 0;
		for (PointOfSaleDetail posDetail : pointOfSale.getPointOfSaleDetails()) {
			serveAmount += (posDetail.getQuantity() * posDetail.getUnitRate());
		}

		if (pointOfSaleVOs.containsKey(pointOfSale.getPersonByEmployeeId()
				.getPersonId())) {
			pointOfSaleVO = pointOfSaleVOs.get(pointOfSale
					.getPersonByEmployeeId().getPersonId());
			pointOfSaleVO.setReceivedTotal(AIOSCommons.formatAmount(serveAmount
					+ AIOSCommons.formatAmountToDouble(pointOfSaleVO
							.getReceivedTotal())));
		} else {
			pointOfSaleVO.setReceivedTotal(AIOSCommons
					.formatAmount(serveAmount));
			pointOfSaleVO
					.setReceiptType(pointOfSale
							.getPersonByEmployeeId()
							.getFirstName()
							.concat(" "
									+ pointOfSale.getPersonByEmployeeId()
											.getLastName()));
		}
		pointOfSaleVO
				.setReceiptQuantity(pointOfSaleVO.getReceiptQuantity() + 1);
		pointOfSaleVOs.put(pointOfSale.getPersonByEmployeeId().getPersonId(),
				pointOfSaleVO);
	}

	// Add PointOfSale VO
	private void addPointOfSaleVO(PointOfSaleReceipt pointOfSaleReceipt,
			Map<Byte, PointOfSaleVO> pointOfSaleVOs) throws Exception {
		PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
		if (pointOfSaleVOs.containsKey(pointOfSaleReceipt.getReceiptType())) {
			pointOfSaleVO = pointOfSaleVOs.get(pointOfSaleReceipt
					.getReceiptType());
			pointOfSaleVO
					.setReceivedTotal(AIOSCommons
							.formatAmount((pointOfSaleReceipt.getReceipt() - (null != pointOfSaleReceipt
									.getBalanceDue() ? pointOfSaleReceipt
									.getBalanceDue() : 0))
									+ AIOSCommons
											.formatAmountToDouble(pointOfSaleVO
													.getReceivedTotal())));
		} else {
			pointOfSaleVO
					.setReceivedTotal(AIOSCommons
							.formatAmount((pointOfSaleReceipt.getReceipt() - (null != pointOfSaleReceipt
									.getBalanceDue() ? pointOfSaleReceipt
									.getBalanceDue() : 0))));
			pointOfSaleVO.setReceiptType(Constants.Accounts.POSPaymentType.get(
					pointOfSaleReceipt.getReceiptType()).name());
			pointOfSaleVO.setCurrencyMode(pointOfSaleReceipt.getReceiptType());
		}
		pointOfSaleVO
				.setReceiptQuantity(pointOfSaleVO.getReceiptQuantity() + 1);
		pointOfSaleVOs.put(pointOfSaleReceipt.getReceiptType(), pointOfSaleVO);
	}

	// Add PointOfSale VO
	private PointOfSaleVO addPointOfSaleVO(PointOfSale pointOfSale)
			throws Exception {
		PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
		pointOfSaleVO.setPointOfSaleId(pointOfSale.getPointOfSaleId());
		pointOfSaleVO.setReferenceNumber(pointOfSale.getReferenceNumber());
		pointOfSaleVO
				.setCustomerName(null != pointOfSale.getCustomer() ? (null != pointOfSale
						.getCustomer().getPersonByPersonId() ? pointOfSale
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" "
								+ pointOfSale.getCustomer()
										.getPersonByPersonId().getLastName())
						: pointOfSale.getCustomer().getCompany()
								.getCompanyName()) : "");
		pointOfSaleVO
				.setCustomerId(null != pointOfSale.getCustomer() ? pointOfSale
						.getCustomer().getCustomerId() : 0);
		pointOfSaleVO
				.setSalesType(null != pointOfSale.getCustomer() ? " Customer"
						: "Counter");
		if (null != pointOfSale.getCustomer())
			pointOfSaleVO.setCustomerNumber(pointOfSale.getCustomer()
					.getCustomerNumber());
		pointOfSaleVO.setDate(DateFormat.convertDateToString(pointOfSale
				.getSalesDate().toString()));
		if (null != pointOfSale.getPointOfSaleDetails()
				&& pointOfSale.getPointOfSaleDetails().size() > 0) {
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSale
					.getPointOfSaleDetails()) {
				pointOfSaleDetailVOs
						.add(addPointOfSaleDetails(pointOfSaleDetail));
			}
			pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
		}
		return pointOfSaleVO;
	}

	// Add POS DetailVO
	private PointOfSaleDetailVO addPointOfSaleDetails(
			PointOfSaleDetail pointOfSaleDetail) throws Exception {
		PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO();
		if (null != pointOfSaleDetail.getDiscountMethod()
				|| null != pointOfSaleDetail.getPromotionMethod())
			pointOfSaleDetailVO.setNonExchanged(true);
		double discoutPrice = 0;
		if (null != pointOfSaleDetail.getIsPercentageDiscount()
				&& null != pointOfSaleDetail.getDiscountValue()
				&& pointOfSaleDetail.getDiscountValue() > 0) {
			if (pointOfSaleDetail.getIsPercentageDiscount())
				discoutPrice = ((pointOfSaleDetail.getQuantity() * pointOfSaleDetail
						.getUnitRate()) * pointOfSaleDetail.getDiscountValue()) / 100;

			else
				discoutPrice = (pointOfSaleDetail.getQuantity() * pointOfSaleDetail
						.getUnitRate()) - pointOfSaleDetail.getDiscountValue();
			pointOfSaleDetailVO
					.setTotalPrice(AIOSCommons.formatAmount(((pointOfSaleDetail
							.getQuantity() * pointOfSaleDetail.getUnitRate()) - discoutPrice)));
			pointOfSaleDetailVO.setDiscountValue(discoutPrice);
		} else {
			pointOfSaleDetailVO
					.setProductUnitPrice(AIOSCommons
							.formatAmount((pointOfSaleDetail.getQuantity() * pointOfSaleDetail
									.getUnitRate())));
			double totalAmount = pointOfSaleDetail.getQuantity()
					* pointOfSaleDetail.getUnitRate();
			discoutPrice = (totalAmount)
					- (pointOfSaleDetail.getDiscountValue() != null ? pointOfSaleDetail
							.getDiscountValue() : totalAmount);
			pointOfSaleDetailVO
					.setTotalPrice(AIOSCommons.formatAmount(((pointOfSaleDetail
							.getQuantity() * pointOfSaleDetail.getUnitRate()) - discoutPrice)));
			pointOfSaleDetailVO.setDiscountValue(discoutPrice);
		}
		double totalSales = 0;
		if (null != pointOfSaleDetail.getDiscountMethod()) {
			totalSales = pointOfSaleDetail.getUnitRate()
					* pointOfSaleDetail.getQuantity();
			discoutPrice = getDiscountAmount(pointOfSaleDetail
					.getDiscountMethod().getDiscount(),
					pointOfSaleDetail.getDiscountMethod(), totalSales);
			pointOfSaleDetailVO.setProductUnitPrice(AIOSCommons
					.formatAmount(totalSales));
			pointOfSaleDetailVO.setTotalPrice(AIOSCommons
					.formatAmount(totalSales - discoutPrice));
			pointOfSaleDetailVO.setDiscountValue(discoutPrice);
		}
		if (null != pointOfSaleDetail.getPromotionMethod()) {
			totalSales = pointOfSaleDetail.getUnitRate()
					* pointOfSaleDetail.getQuantity();
			discoutPrice = getPromotionAmount(
					pointOfSaleDetail.getPromotionMethod(), totalSales);
			pointOfSaleDetailVO.setProductUnitPrice(AIOSCommons
					.formatAmount(totalSales));
			pointOfSaleDetailVO.setTotalPrice(AIOSCommons
					.formatAmount(totalSales - discoutPrice));
			pointOfSaleDetailVO.setDiscountValue(discoutPrice);
		}
		pointOfSaleDetailVO.setProductName(pointOfSaleDetail
				.getProductByProductId().getProductName());
		pointOfSaleDetailVO.setPointOfSaleDetailId(pointOfSaleDetail
				.getPointOfSaleDetailId());
		pointOfSaleDetailVO.setIsPercentageDiscount(pointOfSaleDetail
				.getIsPercentageDiscount());
		pointOfSaleDetailVO.setProductUnitPrice(AIOSCommons
				.formatAmount(pointOfSaleDetail.getUnitRate()));
		pointOfSaleDetailVO.setQuantity(pointOfSaleDetail.getQuantity());
		return pointOfSaleDetailVO;
	}

	public double getDiscountAmount(Discount discount,
			DiscountMethod discountMethod, double salesAmount) throws Exception {
		double discountAmount = 0;
		if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Amount.getCode() == (byte) discount
				.getDiscountType()) {
			discountAmount = salesAmount - discountMethod.getFlatAmount();
		} else if ((byte) Constants.Accounts.DiscountCalcMethod.Flat_Percentage
				.getCode() == (byte) discount.getDiscountType()) {
			discountAmount = (salesAmount * discountMethod.getFlatPercentage()) / 100;
		}
		return discountAmount;
	}

	public double getPromotionAmount(PromotionMethod promotionMethod,
			double salesAmount) throws Exception {
		double promotionAmount = 0;
		if (Constants.Accounts.PACType.Amount.getCode().equals(
				promotionMethod.getPacType())) {
			promotionAmount = salesAmount
					- promotionMethod.getPromotionAmount();
		}

		else if (Constants.Accounts.PACType.Percentage.getCode().equals(
				promotionMethod.getPacType())) {
			promotionAmount = salesAmount
					- ((salesAmount * promotionMethod.getPromotionAmount()) / 100);
		}
		return promotionAmount;
	}

	// Create Sales Return Detail
	private List<PointOfSaleDetail> createReturnPOSSalesDetail(
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs) throws Exception {
		PointOfSaleDetail pointOfSaleDetail = null;
		List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();

		for (PointOfSaleDetailVO pointOfSaleDetailVO : pointOfSaleDetailVOs) {
			pointOfSaleDetail = new PointOfSaleDetail();
			BeanUtils.copyProperties(pointOfSaleDetail, pointOfSaleDetailVO);
			pointOfSaleDetail.setQuantity(pointOfSaleDetail.getQuantity()
					- pointOfSaleDetailVO.getReturnQuantity());
			pointOfSaleDetails.add(pointOfSaleDetail);
		}
		return pointOfSaleDetails;
	}

	// Create Stock Returns
	@SuppressWarnings("unused")
	private List<Stock> createStockReturns(
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs,
			PointOfSale pointOfSale) throws Exception {
		List<Stock> stocks = new ArrayList<Stock>();
		Stock stock = null;
		for (PointOfSaleDetailVO pointOfSaleDetailVO : pointOfSaleDetailVOs) {
			stock = new Stock();
			stock.setProduct(null != pointOfSaleDetailVO
					.getProductByProductId() ? pointOfSaleDetailVO
					.getProductByProductId() : pointOfSaleDetailVO
					.getProductByComboProductId());
			stock.setStore(pointOfSale.getPOSUserTill().getStore());
			stock.setQuantity(pointOfSaleDetailVO.getReturnQuantity());
		}
		return stocks;
	}

	// Create Sales Return Transaction
	@SuppressWarnings("unused")
	private Transaction createSalesReturnTransaction(PointOfSaleVO returnPOS,
			Implementation implementation) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		PointOfSaleDetail pointOfSaleDetail = null;
		ProductPricingDetail productPricingDetail = null;
		for (PointOfSaleDetailVO pointOfSaleDetailVO : returnPOS
				.getPointOfSaleDetailVOs()) {
			pointOfSaleDetail = new PointOfSaleDetail();
			BeanUtils.copyProperties(pointOfSaleDetail, pointOfSaleDetailVO);
			pointOfSaleDetail.setQuantity(pointOfSaleDetailVO
					.getReturnQuantity());
			productPricingDetail = productPricingBL.getProductPricingDetail(
					pointOfSaleDetailVO.getProductByProductId().getProductId(),
					returnPOS.getPOSUserTill().getStore().getStoreId());
			transactionDetails.add(createCOSEntry(productPricingDetail,
					pointOfSaleDetail, returnPOS.getPOSUserTill().getStore(),
					false));
			transactionDetails.add(createInventoryEntry(productPricingDetail,
					pointOfSaleDetail, returnPOS.getPOSUserTill().getStore(),
					true));
		}
		Transaction transaction = createTransaction(
				"POS Ref "
						+ returnPOS.getReferenceNumber()
						+ " Sales Return Entry on ".concat(Calendar
								.getInstance().getTime().toString()),
				transactionDetails, implementation);
		return transaction;
	}

	public void deleteSessionPOSOrder(PointOfSale pointOfSale) throws Exception {
		transactionBL.deleteTransaction(pointOfSale.getPointOfSaleId(),
				PointOfSale.class.getSimpleName());
		List<PointOfSaleOffer> pointOfSaleOffers = null;
		List<PointOfSaleReceipt> pointOfSaleReceipts = null;
		List<PointOfSaleCharge> pointOfSaleCharges = null;
		if (null != pointOfSale.getPointOfSaleOffers()) {
			pointOfSaleOffers = new ArrayList<PointOfSaleOffer>(
					pointOfSale.getPointOfSaleOffers());
		}
		if (null != pointOfSale.getPointOfSaleReceipts()) {
			pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>(
					pointOfSale.getPointOfSaleReceipts());
		}
		if (null != pointOfSale.getPointOfSaleCharges()) {
			pointOfSaleCharges = new ArrayList<PointOfSaleCharge>(
					pointOfSale.getPointOfSaleCharges());
		}
		if (null != pointOfSale.getPointOfSaleDetails()
				&& pointOfSale.getPointOfSaleDetails().size() > 0) {
			List<Stock> stocks = new ArrayList<Stock>();
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSale
					.getPointOfSaleDetails())
				stocks.add(addStock(pointOfSaleDetail, pointOfSale
						.getPOSUserTill().getStore(), getImplementation()));
			stockBL.updateStockDetails(stocks, null);
		}

		pointOfSaleService.deleteSessionPOSOrder(
				pointOfSale,
				new ArrayList<PointOfSaleDetail>(pointOfSale
						.getPointOfSaleDetails()), pointOfSaleCharges,
				pointOfSaleReceipts, pointOfSaleOffers);
	}

	private List<Stock> getStockDetails(PointOfSale pos,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		Stock stock = null;
		List<Stock> stocks = new ArrayList<Stock>();
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails) {
			stock = new Stock();
			stock.setProduct(pointOfSaleDetail.getProductByProductId());
			stock.setStore(pos.getPOSUserTill().getStore());
			stock.setQuantity(pointOfSaleDetail.getQuantity());
			stock.setImplementation(getImplementation());
			stocks.add(stock);
		}
		return stocks;
	}

	public void deletePOSOrder(PointOfSale pointOfSale) throws Exception {
		transactionBL.deleteTransaction(pointOfSale.getPointOfSaleId(),
				PointOfSale.class.getSimpleName());
		stockBL.updateStockDetails(
				getStockDetails(pointOfSale, new ArrayList<PointOfSaleDetail>(
						pointOfSale.getPointOfSaleDetails())), null);
		List<PointOfSaleOffer> pointOfSaleOffers = null;
		List<PointOfSaleReceipt> pointOfSaleReceipts = null;
		List<PointOfSaleCharge> pointOfSaleCharges = null;
		if (null != pointOfSale.getPointOfSaleOffers()) {
			pointOfSaleOffers = new ArrayList<PointOfSaleOffer>(
					pointOfSale.getPointOfSaleOffers());
		}
		if (null != pointOfSale.getPointOfSaleReceipts()) {
			pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>(
					pointOfSale.getPointOfSaleReceipts());
		}
		if (null != pointOfSale.getPointOfSaleCharges()) {
			pointOfSaleCharges = new ArrayList<PointOfSaleCharge>(
					pointOfSale.getPointOfSaleCharges());
		}
		pointOfSaleService.deleteSessionPOSOrder(
				pointOfSale,
				new ArrayList<PointOfSaleDetail>(pointOfSale
						.getPointOfSaleDetails()), pointOfSaleCharges,
				pointOfSaleReceipts, pointOfSaleOffers);
	}

	public void updatePointOfSaleDetail(PointOfSaleDetail pointOfSaleDetail)
			throws Exception {

		pointOfSaleService.updatePointOfSaleDetail(pointOfSaleDetail);
	}

	public PointOfSaleDetail insertPointOfSaleDetail(
			PointOfSaleDetail pointOfSaleDetail) throws Exception {

		pointOfSaleService.updatePointOfSaleDetail(pointOfSaleDetail);
		return pointOfSaleDetail;
	}

	public void deletePointOfSaleDetail(PointOfSaleDetail pointOfSaleDetail)
			throws Exception {

		pointOfSaleService.deletePointOfSaleDetail(pointOfSaleDetail);
	}

	public void deletePointOfSale(PointOfSale pointOfSale) throws Exception {

		pointOfSaleService.deletePointOfSale(pointOfSale);
	}

	public void updatePointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetailsTemp,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		if (null != pointOfSaleDetailsTemp && pointOfSaleDetailsTemp.size() > 0)
			pointOfSaleService.deletePointOfSaleDetails(pointOfSaleDetailsTemp);
		if (null == pointOfSale.getPointOfSaleId())
			SystemBL.saveReferenceStamp(PointOfSale.class.getName(),
					getImplementation());
		pointOfSale.setImplementation(getImplementation());
		pointOfSaleService.updatePointOfSale(pointOfSale, pointOfSaleDetails);
	}

	public void dispatchPointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetailsTemp,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges) throws Exception {
		if (null != pointOfSaleDetailsTemp && pointOfSaleDetailsTemp.size() > 0)
			pointOfSaleService.deletePointOfSaleDetails(pointOfSaleDetailsTemp);
		if (null == pointOfSale.getPointOfSaleId())
			SystemBL.saveReferenceStamp(PointOfSale.class.getName(),
					getImplementation());
		pointOfSale.setImplementation(getImplementation());
		pointOfSaleService.dispatchPointOfSale(pointOfSale, pointOfSaleDetails,
				pointOfSaleCharges);
	}

	public void deleteOfflinePointOfSales(List<PointOfSale> pointOfSales,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<MerchandiseExchange> merchandiseExchanges,
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails)
			throws Exception {
		if (null != merchandiseExchanges && merchandiseExchanges.size() > 0)
			merchandiseExchangeBL.deleteMerchandiseExchange(
					merchandiseExchanges, merchandiseExchangeDetails);
		pointOfSaleService.deleteOfflinePointOfSales(pointOfSales,
				pointOfSaleDetails, pointOfSaleCharges, pointOfSaleOffers,
				pointOfSaleReceipts);
	}

	public void deletPointOfSaleFullDetail(PointOfSale pointOfSale)
			throws Exception {
	}

	public CustomerVO postOfflineTransaction(PointOfSaleVO pointOfSaleVO) {
		try {
			List<PointOfSale> pointOfSales = pointOfSaleVO.getPointOfSales();
			Implementation implementation = null;
			if (pointOfSaleVO.getPointOfSales() != null) {
				implementation = pointOfSaleVO.getPointOfSales().get(0)
						.getImplementation();
			}
			PointOfSale pointOfSale = null;
			List<PointOfSaleDetail> pointOfSaleDetails = null;
			List<MerchandiseExchangeDetail> exchangeDetails = null;
			List<PointOfSaleCharge> pointOfSaleCharges = null;
			PointOfSaleCharge pointOfSaleCharge = null;
			List<PointOfSaleOffer> pointOfSaleOffers = null;
			PointOfSaleOffer pointOfSaleOffer = null;
			List<PointOfSaleReceipt> pointOfSaleReceipts = null;
			PointOfSaleReceipt pointOfSaleReceipt = null;
			MerchandiseExchangeDetail exchangeDetail = null;
			MerchandiseExchange merchandiseExchange = null;
			PointOfSaleDetail pointOfSaleDetail = null;
			List<Customer> offlineCustomer = new ArrayList<Customer>();
			implementation = systemService.getImplementation(implementation);
			for (PointOfSale sale : pointOfSales) {
				pointOfSale = new PointOfSale();
				BeanUtils.copyProperties(pointOfSale, sale);
				pointOfSale.setPointOfSaleId(null);
				pointOfSale.setOfflineEntry(null);
				pointOfSale.setDeliveryStatus(null);
				pointOfSale.setProcessedSession((byte) 2);
				if (null != pointOfSale.getCustomer()) {
					if (null != sale.getCustomer().getCustomerRecordId()
							&& sale.getCustomer().getCustomerRecordId() > 0) {
						Customer customer = new Customer();
						customer.setCustomerId(sale.getCustomer()
								.getCustomerRecordId());
						pointOfSale.setCustomer(customer);
					} else if (null != sale.getCustomer().getPosCustomer()
							&& (boolean) sale.getCustomer().getPosCustomer() == true) {
						Customer offlineCst = new Customer();
						Person person = new Person();
						person.setPersonId(sale.getCustomer()
								.getPersonByPersonId().getPersonId());
						person.setFirstName(sale.getCustomer()
								.getPersonByPersonId().getFirstName());
						person.setLastName("");
						person.setGender(sale.getCustomer()
								.getPersonByPersonId().getGender());
						person.setResidentialAddress(sale.getCustomer()
								.getPersonByPersonId().getResidentialAddress());
						person.setMobile(sale.getCustomer()
								.getPersonByPersonId().getMobile());
						person.setIsApprove((byte) WorkflowConstants.Status.Published
								.getCode());
						offlineCst.setCustomerId(sale.getCustomer()
								.getCustomerId());
						offlineCst.setPersonByPersonId(person);
						ShippingDetail shippingDetail = new ShippingDetail();
						shippingDetail.setName(sale.getCustomer()
								.getPersonByPersonId().getResidentialAddress());
						shippingDetail.setAddress(sale.getCustomer()
								.getPersonByPersonId().getResidentialAddress());
						shippingDetail.setContactNo(sale.getCustomer()
								.getPersonByPersonId().getMobile());
						Customer tempCustomer = saveCustomerPerson(person,
								sale.getCustomer(), shippingDetail);
						offlineCst.setCustomerRecordId(tempCustomer
								.getCustomerId());
						offlineCustomer.add(offlineCst);
						pointOfSale.setCustomer(tempCustomer);
					} else
						pointOfSale.setCustomer(sale.getCustomer());
				}

				pointOfSale.setReferenceNumber(sale.getReferenceNumber());

				pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
				for (PointOfSaleDetail saleDetail : sale
						.getPointOfSaleDetails()) {
					pointOfSaleDetail = new PointOfSaleDetail();
					BeanUtils.copyProperties(pointOfSaleDetail, saleDetail);
					pointOfSaleDetail.setPointOfSaleDetailId(null);
					pointOfSaleDetail.setPointOfSale(pointOfSale);
					pointOfSaleDetails.add(pointOfSaleDetail);
					if (null != saleDetail.getMerchandiseExchangeDetails()
							&& saleDetail.getMerchandiseExchangeDetails()
									.size() > 0) {
						if (null == merchandiseExchange) {
							List<MerchandiseExchangeDetail> tempExchangeDetail = new ArrayList<MerchandiseExchangeDetail>(
									saleDetail.getMerchandiseExchangeDetails());
							merchandiseExchange = new MerchandiseExchange();
							if (null != implementation)
								merchandiseExchange
										.setReferenceNumber((SystemBL
												.saveReferenceStamp(
														MerchandiseExchange.class
																.getName(),
														implementation)));
							else
								merchandiseExchange
										.setReferenceNumber((SystemBL
												.saveReferenceStamp(
														MerchandiseExchange.class
																.getName(),
														getImplementation())));
							merchandiseExchange.setDate(tempExchangeDetail
									.get(0).getMerchandiseExchange().getDate());
							merchandiseExchange.setImplementation(sale
									.getImplementation());
						}
						exchangeDetails = new ArrayList<MerchandiseExchangeDetail>();
						for (MerchandiseExchangeDetail merchandiseExchangeDetail : saleDetail
								.getMerchandiseExchangeDetails()) {
							exchangeDetail = new MerchandiseExchangeDetail();
							BeanUtils.copyProperties(exchangeDetail,
									merchandiseExchangeDetail);
							exchangeDetail.setMerchandiseExchangeDetailId(null);
							exchangeDetail
									.setPointOfSaleDetail(pointOfSaleDetail);
							exchangeDetail
									.setMerchandiseExchange(merchandiseExchangeDetail
											.getMerchandiseExchange());
							exchangeDetails.add(exchangeDetail);
						}
					}
					if (null != exchangeDetails && exchangeDetails.size() > 0) {
						pointOfSaleDetail
								.setMerchandiseExchangeDetails(new HashSet<MerchandiseExchangeDetail>(
										exchangeDetails));
					}
				}
				pointOfSale
						.setPointOfSaleDetails(new HashSet<PointOfSaleDetail>(
								pointOfSaleDetails));
				if (null != pointOfSale.getPointOfSaleCharges()
						&& pointOfSale.getPointOfSaleCharges().size() > 0) {
					pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();
					for (PointOfSaleCharge saleCharge : pointOfSale
							.getPointOfSaleCharges()) {
						pointOfSaleCharge = new PointOfSaleCharge();
						BeanUtils.copyProperties(pointOfSaleCharge, saleCharge);
						pointOfSaleCharge.setPointOfSale(pointOfSale);
						pointOfSaleCharge.setPointOfSaleChargeId(null);
						pointOfSaleCharges.add(pointOfSaleCharge);
					}
					pointOfSale
							.setPointOfSaleCharges(new HashSet<PointOfSaleCharge>(
									pointOfSaleCharges));
				}
				if (null != pointOfSale.getPointOfSaleOffers()
						&& pointOfSale.getPointOfSaleOffers().size() > 0) {
					pointOfSaleOffers = new ArrayList<PointOfSaleOffer>();
					for (PointOfSaleOffer saleOffer : pointOfSale
							.getPointOfSaleOffers()) {
						pointOfSaleOffer = new PointOfSaleOffer();
						BeanUtils.copyProperties(pointOfSaleOffer, saleOffer);
						pointOfSaleOffer.setPointOfSale(pointOfSale);
						pointOfSaleOffer.setPointOfSaleOfferId(null);
						pointOfSaleOffers.add(pointOfSaleOffer);
					}
					pointOfSale
							.setPointOfSaleOffers(new HashSet<PointOfSaleOffer>(
									pointOfSaleOffers));
				}
				if (null != pointOfSale.getPointOfSaleReceipts()
						&& pointOfSale.getPointOfSaleReceipts().size() > 0) {
					pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
					for (PointOfSaleReceipt saleReceipt : pointOfSale
							.getPointOfSaleReceipts()) {
						pointOfSaleReceipt = new PointOfSaleReceipt();
						BeanUtils.copyProperties(pointOfSaleReceipt,
								saleReceipt);
						pointOfSaleReceipt.setPointOfSale(pointOfSale);
						pointOfSaleReceipt.setPointOfSaleReceiptId(null);
						pointOfSaleReceipts.add(pointOfSaleReceipt);
					}
					pointOfSale
							.setPointOfSaleReceipts(new HashSet<PointOfSaleReceipt>(
									pointOfSaleReceipts));
				}
				pointOfSaleService.savePointOfSales(pointOfSale,
						pointOfSaleDetails, pointOfSaleCharges,
						pointOfSaleOffers, pointOfSaleReceipts,
						merchandiseExchange, exchangeDetails);
				processPointOfSaleTransaction(pointOfSale, pointOfSaleDetails,
						pointOfSaleReceipts, pointOfSale.getPOSUserTill(),
						implementation);
			}
			CustomerVO customerVO = new CustomerVO();
			customerVO.setType("SUCCESS");
			// customerVO.setCustomers(offlineCustomer);
			return customerVO;
		} catch (Exception ex) {
			CustomerVO customerVO = new CustomerVO();
			customerVO.setType("ERROR");
			// customerVO.setCustomers(offlineCustomer);
			ex.printStackTrace();
			return customerVO;
		}
	}

	public Customer saveCustomerPerson(Person person, Customer customer,
			ShippingDetail shippingDetail) {
		try {
			person.setPersonId(null);
			customer.setCustomerId(null);
			shippingDetail.setShippingId(null);

			Implementation implementation = systemService
					.getImplementation(customer.getImplementation());
			List<Customer> customerList = customerBL.getCustomerService()
					.getAllCustomers(implementation);
			if (null != customerList && customerList.size() > 0) {
				List<Long> customerNoList = new ArrayList<Long>();
				for (Customer list : customerList) {
					customerNoList
							.add(Long.parseLong(list.getCustomerNumber()));
				}
				long customerNo = Collections.max(customerNoList);
				customer.setCustomerNumber(customerNo + "");
			}
			person.setPersonNumber(customer.getCustomerNumber());
			person.setImplementation(implementation);

			Combination combination = getPOSCombinationInfo(
					implementation.getAccountsReceivable(),
					person.getFirstName() + "-" + person.getPersonNumber()
							+ "-CUSTOMER", implementation);
			customer.setCombination(combination);
			pointOfSaleService.savePerson(person);
			customer.setPersonByPersonId(person);
			customer.setCustomerRecordId(null);
			customer.setPosCustomer(null);
			customer.setCustomerType(CustomerType.Retail.getCode());
			customerBL.savePOSCustomer(customer, shippingDetail);
			return customer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Combination getPOSCombinationInfo(Long natualAccountCombinationId,
			String currentName) throws Exception {
		Combination combination = null;
		if (natualAccountCombinationId == null
				|| natualAccountCombinationId == 0)
			return combination;

		Account account = combinationBL.getCombinationService()
				.getAccountDetail(currentName, getImplementation());
		if (account != null) {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationBL.getCombinationService()
					.getCombinationAllAccountDetail(
							combination.getCombinationId());
		} else {

			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationBL.createAnalysisCombination(combination,
					currentName, false);
		}
		return combination;
	}

	public Combination getPOSCombinationInfo(Long natualAccountCombinationId,
			String currentName, Implementation implementation) throws Exception {
		Combination combination = null;
		if (natualAccountCombinationId == null
				|| natualAccountCombinationId == 0)
			return combination;

		Account account = combinationBL.getCombinationService()
				.getAccountDetail(currentName, implementation);
		if (account != null) {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationBL.getCombinationService()
					.getCombinationAllAccountDetail(
							combination.getCombinationId());
		} else {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = combinationBL.createAnalysisCombination(combination,
					currentName, true, implementation);
		}
		return combination;
	}

	public void updatePointOfSale(List<PointOfSale> pointOfSales,
			byte processedSesseion) throws Exception {
		for (PointOfSale pointOfSale : pointOfSales)
			pointOfSale.setProcessedSession(processedSesseion);
		pointOfSaleService.updatePointOfSale(pointOfSales);
	}

	public PointOfSale fetchSalesInformationByUseCase(Long recordId,
			String useCase, Implementation implementation) throws Exception {
		return pointOfSaleService.fetchSalesInformationByUseCase(recordId,
				useCase, implementation);
	}

	public List<SalesInvoiceVO> getAllSalesByCustomer(
			SalesInvoiceVO salesInvoiceVOTemp) throws Exception {
		SalesInvoiceVO salesInvoiceVO = null;
		List<PointOfSale> posList = pointOfSaleService
				.getFilteredPaymentsByCustomer(salesInvoiceVOTemp);
		List<SalesInvoiceVO> salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();
		Collections.sort(posList, new Comparator<PointOfSale>() {
			public int compare(PointOfSale m1, PointOfSale m2) {
				return m1.getPointOfSaleId().compareTo(m2.getPointOfSaleId());
			}
		});
		for (PointOfSale pos : posList) {
			salesInvoiceVO = new SalesInvoiceVO();
			Double totalAmount = 0.0;

			for (PointOfSaleDetail posDetail : pos.getPointOfSaleDetails()) {
				Double discount = 0.0;
				if (posDetail.getDiscountValue() != null
						&& posDetail.getIsPercentageDiscount() != null) {
					discount = posDetail.getIsPercentageDiscount() == false ? posDetail
							.getDiscountValue() : ((posDetail.getUnitRate()
							* posDetail.getQuantity() * posDetail
							.getDiscountValue()) / 100);
				}

				totalAmount += (posDetail.getUnitRate()
						* posDetail.getQuantity() - discount);

			}
			salesInvoiceVO.setSalesDeliveryNumber(pos.getReferenceNumber());
			salesInvoiceVO.setSalesDate(DateFormat.convertDateToString(pos
					.getSalesDate() + ""));
			salesInvoiceVO.setInvoiceAmount(totalAmount);
			salesInvoiceVO.setTotalInvoice(totalAmount);
			salesInvoiceVO.setPendingInvoice(totalAmount - totalAmount);
			salesInvoiceVO.setReferenceNumber(pos.getReferenceNumber());
			salesInvoiceVO.setSalesInvoiceId(pos.getPointOfSaleId());
			salesInvoiceVO.setCustomerName(null != pos.getCustomer()
					.getPersonByPersonId() ? pos
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(pos.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != pos.getCustomer()
					.getCompany() ? pos.getCustomer().getCompany()
					.getCompanyName() : pos.getCustomer().getCmpDeptLocation()
					.getCompany().getCompanyName()));

			salesInvoiceVO.setCustomerNumber(pos.getCustomer()
					.getCustomerNumber());
			salesInvoiceVO.setCustomerId(pos.getCustomer().getCustomerId());

			salesInvoiceVO.setSalesInvoiceDate(DateFormat
					.convertDateToString(pos.getSalesDate() + ""));
			// Child Level Record
			List<SalesInvoiceVO> salesInvoiceVOsTemp = new ArrayList<SalesInvoiceVO>();
			salesInvoiceVOsTemp.add(salesInvoiceVO);
			salesInvoiceVO.setSalesInvoiceVOs(salesInvoiceVOsTemp);

			salesInvoiceVO.setSalesFlag("POS");

			salesInvoiceVO.setInvoiceAmount(AIOSCommons
					.roundDecimals(salesInvoiceVO.getInvoiceAmount()));
			salesInvoiceVO.setTotalInvoice(AIOSCommons
					.roundDecimals(salesInvoiceVO.getTotalInvoice()));
			salesInvoiceVO.setPendingInvoice(AIOSCommons
					.roundDecimals(salesInvoiceVO.getPendingInvoice()));

			if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Pending
					.getCode() && salesInvoiceVO.getPendingInvoice() > 0.0)
				salesInvoiceVOs.add(salesInvoiceVO);
			else if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Paid
					.getCode() && salesInvoiceVO.getPendingInvoice() <= 0.0)
				salesInvoiceVOs.add(salesInvoiceVO);
			else if ((byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Pending
					.getCode()
					&& (byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Paid
							.getCode())
				salesInvoiceVOs.add(salesInvoiceVO);

		}

		return salesInvoiceVOs;
	}

	public void updatePointOfSaleReference() {
		try {
			List<PointOfSale> pointOfSales = this.getPointOfSaleService()
					.getAllPointOfSalesWithImplementation();
			Map<String, List<PointOfSale>> salesMap = new HashMap<String, List<PointOfSale>>();
			List<PointOfSale> tempPointOfSaleTemp = null;
			if (null != pointOfSales && pointOfSales.size() > 0) {
				for (PointOfSale pointOfSale : pointOfSales) {
					if ((long) pointOfSale.getImplementation()
							.getImplementationId() == 6
							|| (long) pointOfSale.getImplementation()
									.getImplementationId() == 14) {
						tempPointOfSaleTemp = new ArrayList<PointOfSale>();
						if (salesMap.containsKey(pointOfSale
								.getReferenceNumber()))
							tempPointOfSaleTemp.addAll(salesMap.get(pointOfSale
									.getReferenceNumber()));
						tempPointOfSaleTemp.add(pointOfSale);
						salesMap.put(pointOfSale.getReferenceNumber(),
								tempPointOfSaleTemp);
					}
				}
			}
			List<PointOfSale> updatePointOfSales = new ArrayList<PointOfSale>();
			for (Entry<String, List<PointOfSale>> entry : salesMap.entrySet()) {
				tempPointOfSaleTemp = new ArrayList<PointOfSale>();
				tempPointOfSaleTemp.addAll(entry.getValue());
				if (tempPointOfSaleTemp.size() > 1) {
					String value = "A";
					int counter = 0;
					for (PointOfSale pointOfSale : tempPointOfSaleTemp) {
						if (counter == 0)
							counter += 1;
						else {
							pointOfSale.setReferenceNumber(pointOfSale
									.getReferenceNumber().concat("-" + value));
							updatePointOfSales.add(pointOfSale);
							int charValue = value.charAt(0);
							value = String.valueOf((char) (charValue + 1));
							counter += 1;
						}
					}
				}
			}
			this.getPointOfSaleService().savePointOfSale(updatePointOfSales);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void updatepointOfSalesAddtionalCharges(
			List<PointOfSale> pointOfSales) throws Exception {
		if (null != pointOfSales && pointOfSales.size() > 0) {
			List<PointOfSaleCharge> pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();
			PointOfSaleCharge pointOfSaleCharge = null;
			for (PointOfSale pointOfSale : pointOfSales) {
				pointOfSaleCharge = processPointOfAddtionalCharges(
						pointOfSale,
						new ArrayList<PointOfSaleDetail>(pointOfSale
								.getPointOfSaleDetails()),
						new ArrayList<PointOfSaleReceipt>(pointOfSale
								.getPointOfSaleReceipts()),
						new ArrayList<PointOfSaleCharge>(pointOfSale
								.getPointOfSaleCharges()));
				if (null != pointOfSaleCharge)
					pointOfSaleCharges.add(pointOfSaleCharge);
			}
			if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0)
				pointOfSaleService.savePointOfSaleCharges(pointOfSaleCharges);
		}
	}

	private PointOfSaleCharge processPointOfAddtionalCharges(
			PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<PointOfSaleCharge> pointOfSaleCharges) throws Exception {
		double addtionalCharges = 0;
		double receiptAmount = 0;
		double inventoryAmount = 0;
		double totalDiscount = null != pointOfSale.getTotalDiscount() ? pointOfSale
				.getTotalDiscount() : 0;
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				addtionalCharges += pointOfSaleCharge.getCharges();
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				receiptAmount += pointOfSaleReceipt.getReceipt()
						- (null != pointOfSaleReceipt.getBalanceDue() ? pointOfSaleReceipt
								.getBalanceDue() : 0);
		}

		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0) {
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
				inventoryAmount += pointOfSaleDetail.getUnitRate()
						* pointOfSaleDetail.getQuantity();
		}

		double totalInventory = inventoryAmount - totalDiscount;
		PointOfSaleCharge saleCharge = null;
		if (receiptAmount > totalInventory) {
			if ((receiptAmount - totalInventory) > addtionalCharges) {
				saleCharge = new PointOfSaleCharge();
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(2237l);
				saleCharge.setCharges((receiptAmount - totalInventory));
				saleCharge.setLookupDetail(lookupDetail);
				saleCharge.setPointOfSale(pointOfSale);
			}
		}
		return saleCharge;
	}

	public void updatePointOfSalesTransaction(List<PointOfSale> pointOfSales,
			Implementation implementation) throws Exception {
		if (null != pointOfSales && pointOfSales.size() > 0) {
			for (PointOfSale pointOfSale : pointOfSales)
				this.processPointOfSaleTransaction(
						pointOfSale,
						new ArrayList<PointOfSaleDetail>(pointOfSale
								.getPointOfSaleDetails()),
						new ArrayList<PointOfSaleReceipt>(pointOfSale
								.getPointOfSaleReceipts()), null,
						implementation);
		}
	}

	public void createPointOfSalesTransactionOnly(
			List<PointOfSale> pointOfSales, Implementation implementation)
			throws Exception {
		if (null != pointOfSales && pointOfSales.size() > 0) {
			for (PointOfSale pointOfSale : pointOfSales)
				this.processPointOfSaleTransactionOnly(
						pointOfSale,
						new ArrayList<PointOfSaleDetail>(pointOfSale
								.getPointOfSaleDetails()),
						new ArrayList<PointOfSaleReceipt>(pointOfSale
								.getPointOfSaleReceipts()), null,
						implementation);
		}
	}

	private void processPointOfSaleTransactionOnly(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			POSUserTill posUserSession, Implementation implementation)
			throws Exception {
		// Create Inventory + Cost Of Goods Sold entry
		List<TransactionDetail> transactionDetails = createInventoryCOGTransactions(
				pointOfSale, pointOfSaleDetails, posUserSession, implementation);
		if (null != transactionDetails && transactionDetails.size() > 0) {
			Transaction transaction = null;
			if (implementation == null)
				transaction = transactionBL.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pointOfSale.getSalesDate()),
						transactionBL.getCurrency().getCurrencyId(),
						"POS COSTOFGOODS SOLD ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL
								.getCategory("POS ENTRY"), pointOfSale
								.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			else
				transaction = transactionBL.createTransaction(
						transactionBL.getCurrency(implementation)
								.getCurrencyId(),
						"POS COSTOFGOODS SOLD ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL.getCategory(
								"POS ENTRY", implementation), implementation,
						pointOfSale.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			transactionBL.saveTransactionOnly(transaction, transactionDetails);
		}
		// Create Sales Revenue + CASH/CARD Transaction
		List<TransactionDetail> revenueTransactionDetails = createSalesRevenueTransactions(
				pointOfSale, pointOfSaleReceipts, implementation);
		if (null != revenueTransactionDetails
				&& revenueTransactionDetails.size() > 0) {
			Transaction transaction = null;
			if (implementation == null)
				transaction = transactionBL.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pointOfSale.getSalesDate()),
						transactionBL.getCurrency().getCurrencyId(),
						"POS REVENUE ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL
								.getCategory("POS ENTRY"), pointOfSale
								.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			else
				transaction = transactionBL.createTransaction(
						transactionBL.getCurrency(implementation)
								.getCurrencyId(),
						"POS REVENUE ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL.getCategory(
								"POS ENTRY", implementation), implementation,
						pointOfSale.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			transactionBL.saveTransactionOnly(transaction,
					revenueTransactionDetails);
		}
	}

	private synchronized void processPointOfSaleTransaction(
			PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			POSUserTill posUserSession, Implementation implementation)
			throws Exception {
		// Create Inventory + Cost Of Goods Sold entry
		List<TransactionDetail> transactionDetails = createInventoryCOGTransactions(
				pointOfSale, pointOfSaleDetails, posUserSession, implementation);
		if (null != transactionDetails && transactionDetails.size() > 0) {
			Transaction transaction = null;
			if (implementation == null)
				transaction = transactionBL.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pointOfSale.getSalesDate()),
						transactionBL.getCurrency().getCurrencyId(),
						"POS COSTOFGOODS SOLD ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL
								.getCategory("POS ENTRY"), pointOfSale
								.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			else
				transaction = transactionBL.createTransaction(
						transactionBL.getCurrency(implementation)
								.getCurrencyId(),
						"POS COSTOFGOODS SOLD ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL.getCategory(
								"POS ENTRY", implementation), implementation,
						pointOfSale.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			transactionBL.saveTransaction(transaction, transactionDetails);
		}
		// Create Sales Revenue + CASH/CARD Transaction
		List<TransactionDetail> revenueTransactionDetails = createSalesRevenueTransactions(
				pointOfSale, pointOfSaleReceipts, implementation);
		if (null != revenueTransactionDetails
				&& revenueTransactionDetails.size() > 0) {
			Transaction transaction = null;
			if (implementation == null)
				transaction = transactionBL.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pointOfSale.getSalesDate()),
						transactionBL.getCurrency().getCurrencyId(),
						"POS REVENUE ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL
								.getCategory("POS ENTRY"), pointOfSale
								.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			else
				transaction = transactionBL.createTransaction(
						transactionBL.getCurrency(implementation)
								.getCurrencyId(),
						"POS REVENUE ENTRY REF: "
								+ pointOfSale.getReferenceNumber(), pointOfSale
								.getSalesDate(), transactionBL.getCategory(
								"POS ENTRY", implementation), implementation,
						pointOfSale.getPointOfSaleId(), PointOfSale.class
								.getSimpleName());
			transactionBL.saveTransaction(transaction,
					revenueTransactionDetails);
		}
	}

	private List<TransactionDetail> createSalesRevenueTransactions(
			PointOfSale pointOfSale,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			Implementation implementation) throws Exception {
		Combination revenueCombination = new Combination();
		if (null == implementation)
			implementation = getImplementation();
		revenueCombination.setCombinationId(implementation.getRevenueAccount());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		Combination cashOrCardClearing = null;
		double revenue = 0;
		double discount = 0;
		if (null != pointOfSale.getTotalDiscount()
				&& pointOfSale.getTotalDiscount() > 0) {
			discount = pointOfSale.getTotalDiscount();
			transactionDetails.add(transactionBL.createTransactionDetail(
					pointOfSale.getTotalDiscount(),
					implementation.getSalesDiscount(),
					TransactionType.Debit.getCode(),
					"POS Sales Discount Entry Ref: "
							+ pointOfSale.getReferenceNumber(), null,
					pointOfSale.getReferenceNumber()));
		}
		double cashReceived = 0;
		for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts) {
			if ((double) pointOfSaleReceipt.getReceipt() > 0) {
				cashReceived = pointOfSaleReceipt.getReceipt()
						- (null != pointOfSaleReceipt.getBalanceDue() ? pointOfSaleReceipt
								.getBalanceDue() : 0);
				if (cashReceived > 0) {
					revenue += cashReceived;
					cashOrCardClearing = new Combination();
					if ((byte) pointOfSaleReceipt.getReceiptType() == (byte) POSPaymentType.Card
							.getCode())
						cashOrCardClearing.setCombinationId(implementation
								.getCreditCardAccount());
					else
						cashOrCardClearing.setCombinationId(implementation
								.getClearingAccount());
					transactionDetails.add(transactionBL
							.createTransactionDetail(
									cashReceived,
									cashOrCardClearing.getCombinationId(),
									TransactionType.Debit.getCode(),
									"POS Entry Ref: "
											+ pointOfSale.getReferenceNumber(),
									null, pointOfSale.getReferenceNumber()));
				}
			}
		}
		revenue += discount;
		transactionDetails.add(transactionBL.createTransactionDetail(revenue,
				revenueCombination.getCombinationId(),
				TransactionType.Credit.getCode(), "POS Entry Ref: "
						+ pointOfSale.getReferenceNumber(), null,
				pointOfSale.getReferenceNumber()));
		return transactionDetails;
	}

	private List<TransactionDetail> createInventoryCOGTransactions(
			PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			POSUserTill posUserSession, Implementation implementation)
			throws Exception {
		Combination expenseCombination = new Combination();
		if (null == implementation)
			implementation = getImplementation();
		expenseCombination.setCombinationId(implementation.getExpenseAccount());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();

		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails) {
			Combination inventoryCombination = null;
			ProductPricingDetail productPricingDetail = null;
			if (null != pointOfSaleDetail.getProductByProductId()
					.getProductPricingDetails()
					&& pointOfSaleDetail.getProductByProductId()
							.getProductPricingDetails().size() > 0)
				productPricingDetail = new ArrayList<ProductPricingDetail>(
						pointOfSaleDetail.getProductByProductId()
								.getProductPricingDetails()).get(0);
			else {
				List<ProductPricing> productPricings = productPricingBL
						.getProductPricingService()
						.getProductPricingWithoutCustomerAndStore(
								pointOfSaleDetail.getProductByProductId()
										.getProductId(),
								Calendar.getInstance().getTime(),
								Calendar.getInstance().getTime());
				if (null != productPricings && productPricings.size() > 0) {
					List<ProductPricingDetail> pricingDetails = new ArrayList<ProductPricingDetail>(
							productPricings.get(0).getProductPricingDetails());
					productPricingDetail = pricingDetails.get(0);
					productPricingBL.getProductPricingService()
							.getProductPricingDetailDAO()
							.evict(productPricingDetail);
					productPricingBL.getProductPricingService()
							.getProductPricingDetailDAO()
							.refresh(productPricingDetail);
					for (ProductPricing pricing : productPricings)
						productPricingBL.getProductPricingService()
								.getProductPricingDAO().refresh(pricing);
				}
			}
			if (null != pointOfSaleDetail.getProductByProductId()
					.getCombinationByInventoryAccount()) {
				inventoryCombination = new Combination();
				inventoryCombination = pointOfSaleDetail
						.getProductByProductId()
						.getCombinationByInventoryAccount();
			} else {
				inventoryCombination = new Combination();
				inventoryCombination = productPricingBL.getProductBL()
						.getProductInventory(
								pointOfSaleDetail.getProductByProductId()
										.getProductId());
			}
			if (null != productPricingDetail && null != inventoryCombination) {
				transactionDetails
						.add(transactionBL.createTransactionDetail(
								(productPricingDetail.getBasicCostPrice() * pointOfSaleDetail
										.getQuantity()),
								expenseCombination.getCombinationId(),
								TransactionType.Debit.getCode(),
								"POS Entry Ref: "
										+ pointOfSale.getReferenceNumber(),
								null, pointOfSale.getReferenceNumber()));
				transactionDetails
						.add(transactionBL.createTransactionDetail(
								(productPricingDetail.getBasicCostPrice() * pointOfSaleDetail
										.getQuantity()),
								inventoryCombination.getCombinationId(),
								TransactionType.Credit.getCode(),
								"POS Entry Ref: "
										+ pointOfSale.getReferenceNumber(),
								null, pointOfSale.getReferenceNumber()));
			}
		}
		return transactionDetails;
	}

	public void deletePointOfSale(List<PointOfSale> deletedPointOfSales)
			throws Exception {
		for (PointOfSale pointOfSale : deletedPointOfSales) {
			transactionBL.deleteTransaction(pointOfSale.getPointOfSaleId(),
					PointOfSale.class.getSimpleName());
			pointOfSaleService.deletePointOfSale(
					pointOfSale,
					new ArrayList<PointOfSaleDetail>(pointOfSale
							.getPointOfSaleDetails()),
					new ArrayList<PointOfSaleCharge>(pointOfSale
							.getPointOfSaleCharges()),
					new ArrayList<PointOfSaleReceipt>(pointOfSale
							.getPointOfSaleReceipts()));
		}
	}

	public String testCall(SalesInvoiceVO vo) {
		return "My name is " + vo.getPaymentMode();
	}

	public SalesInvoiceVO objectCall() {
		SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO();
		salesInvoiceVO.setPaymentMode("Cash");
		salesInvoiceVO.setCustomerName("Rafiq");
		return salesInvoiceVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProductPricingBL getProductPricingBL() {
		return productPricingBL;
	}

	public void setProductPricingBL(ProductPricingBL productPricingBL) {
		this.productPricingBL = productPricingBL;
	}

	public DiscountBL getDiscountBL() {
		return discountBL;
	}

	public void setDiscountBL(DiscountBL discountBL) {
		this.discountBL = discountBL;
	}

	public PromotionBL getPromotionBL() {
		return promotionBL;
	}

	public void setPromotionBL(PromotionBL promotionBL) {
		this.promotionBL = promotionBL;
	}

	public ProductPointBL getProductPointBL() {
		return productPointBL;
	}

	public void setProductPointBL(ProductPointBL productPointBL) {
		this.productPointBL = productPointBL;
	}

	public PointOfSaleService getPointOfSaleService() {
		return pointOfSaleService;
	}

	public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
		this.pointOfSaleService = pointOfSaleService;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public RewardPolicyBL getRewardPolicyBL() {
		return rewardPolicyBL;
	}

	public void setRewardPolicyBL(RewardPolicyBL rewardPolicyBL) {
		this.rewardPolicyBL = rewardPolicyBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public MerchandiseExchangeBL getMerchandiseExchangeBL() {
		return merchandiseExchangeBL;
	}

	public void setMerchandiseExchangeBL(
			MerchandiseExchangeBL merchandiseExchangeBL) {
		this.merchandiseExchangeBL = merchandiseExchangeBL;
	}

	public POSUserTillBL getpOSUserTillBL() {
		return pOSUserTillBL;
	}

	public void setpOSUserTillBL(POSUserTillBL pOSUserTillBL) {
		this.pOSUserTillBL = pOSUserTillBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public ProductionReceiveBL getProductionReceiveBL() {
		return productionReceiveBL;
	}

	public void setProductionReceiveBL(ProductionReceiveBL productionReceiveBL) {
		this.productionReceiveBL = productionReceiveBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public ProductDefinitionBL getProductDefinitionBL() {
		return productDefinitionBL;
	}

	public void setProductDefinitionBL(ProductDefinitionBL productDefinitionBL) {
		this.productDefinitionBL = productDefinitionBL;
	}

	public CreditBL getCreditBL() {
		return creditBL;
	}

	public void setCreditBL(CreditBL creditBL) {
		this.creditBL = creditBL;
	}
}
