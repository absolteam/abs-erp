/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationCharge;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class CustomerQuotationService {

	private AIOTechGenericDAO<CustomerQuotation> customerQuotationDAO;
	private AIOTechGenericDAO<CustomerQuotationCharge> customerQuotationChargeDAO;
	private AIOTechGenericDAO<CustomerQuotationDetail> customerQuotationDetailDAO;

	// Get all Customer Quotation
	public List<CustomerQuotation> getAllCustomerQuotation(
			Implementation implementation) throws Exception {
		return customerQuotationDAO.findByNamedQuery("getAllCustomerQuotation",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get all Customer Quotation by ID
	public CustomerQuotation getCustomerQuotationById(long customerQuotationId)
			throws Exception {
		return customerQuotationDAO.findByNamedQuery(
				"getCustomerQuotationById", customerQuotationId).get(0);
	}

	// Get Customer Quotation
	public CustomerQuotation getCustomerQuotation(long customerQuotationId)
			throws Exception {
		return customerQuotationDAO.findByNamedQuery("getCustomerQuotation",
				customerQuotationId).get(0);
	}

	// Get all Customer Quotation Detail by Quotation ID
	public List<CustomerQuotationDetail> getCustomerQuotationDetailById(
			long customerQuotationId) throws Exception {
		return customerQuotationDetailDAO.findByNamedQuery(
				"getCustomerQuotationDetailById", customerQuotationId);
	}

	// Get all Customer Quotation Charges by Quotation ID
	public List<CustomerQuotationCharge> getCustomerQuotationChargesById(
			long customerQuotationId) throws Exception {
		return customerQuotationChargeDAO.findByNamedQuery(
				"getCustomerQuotationChargesById", customerQuotationId);
	}

	// Save Customer Quotation Details
	public void deleteCustomerQuotationDetails(
			List<CustomerQuotationDetail> deleteQuotationDetails)
			throws Exception {
		customerQuotationDetailDAO.deleteAll(deleteQuotationDetails);
	}

	// Save Customer Quotation Charges
	public void deleteCustomerQuotationCharges(
			List<CustomerQuotationCharge> deleteQuotationCharges)
			throws Exception {
		customerQuotationChargeDAO.deleteAll(deleteQuotationCharges);
	}

	// Save Customer Quotation
	public void saveCustomerQuotation(CustomerQuotation customerQuotation,
			List<CustomerQuotationDetail> customerQuotationDetails,
			List<CustomerQuotationCharge> customerQuotationCharges)
			throws Exception {
		customerQuotationDAO.saveOrUpdate(customerQuotation);
		if (null != customerQuotationCharges
				&& customerQuotationCharges.size() > 0) {
			for (CustomerQuotationCharge customerQuotationCharge : customerQuotationCharges)
				customerQuotationCharge.setCustomerQuotation(customerQuotation);
		}
		for (CustomerQuotationDetail customerQuotationDetail : customerQuotationDetails) 
			customerQuotationDetail.setCustomerQuotation(customerQuotation);
		customerQuotationDetailDAO.saveOrUpdateAll(customerQuotationDetails);
		if (null != customerQuotationCharges
				&& customerQuotationCharges.size() > 0) {
			customerQuotationChargeDAO
					.saveOrUpdateAll(customerQuotationCharges);
		}
	}

	// Delete Customer Quotation
	public void deleteCustomerQuotation(CustomerQuotation customerQuotation) {
		if (null != customerQuotation.getCustomerQuotationCharges()
				&& customerQuotation.getCustomerQuotationCharges().size() > 0) {
			customerQuotationChargeDAO
					.deleteAll(new ArrayList<CustomerQuotationCharge>(
							customerQuotation.getCustomerQuotationCharges()));
		}
		if (null != customerQuotation.getCustomerQuotationDetails()
				&& customerQuotation.getCustomerQuotationDetails().size() > 0) {
			customerQuotationDetailDAO
					.deleteAll(new ArrayList<CustomerQuotationDetail>(
							customerQuotation.getCustomerQuotationDetails()));
		}
		customerQuotationDAO.delete(customerQuotation);
	}

	@SuppressWarnings("unchecked")
	public List<CustomerQuotation> getFilteredCustomerQuotation(
			Implementation implementation, Long quotationId, Long customerId,
			Byte statusId, Date fromDate, Date toDate) throws Exception {
		Criteria criteria = customerQuotationDAO.createCriteria();

		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByShippingTerm", "st",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByShippingMethod", "sm",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customerQuotationDetails", "cqd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customerQuotationCharges", "cqc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.shippingDetails", "c_sd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "c_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.cmpDeptLocation", "c_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.company", "c_cmp",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("cqd.product", "prod",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cqd.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cqc.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (quotationId != null && quotationId > 0) {
			criteria.add(Restrictions.eq("customerQuotationId", quotationId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("c.customerId", customerId));
		}

		if (statusId != null && statusId > 0) {
			criteria.add(Restrictions.eq("status", statusId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date", fromDate, toDate));
		}

		return criteria.list();
	}

	// Getters and Setters
	public AIOTechGenericDAO<CustomerQuotation> getCustomerQuotationDAO() {
		return customerQuotationDAO;
	}

	public void setCustomerQuotationDAO(
			AIOTechGenericDAO<CustomerQuotation> customerQuotationDAO) {
		this.customerQuotationDAO = customerQuotationDAO;
	}

	public AIOTechGenericDAO<CustomerQuotationCharge> getCustomerQuotationChargeDAO() {
		return customerQuotationChargeDAO;
	}

	public void setCustomerQuotationChargeDAO(
			AIOTechGenericDAO<CustomerQuotationCharge> customerQuotationChargeDAO) {
		this.customerQuotationChargeDAO = customerQuotationChargeDAO;
	}

	public AIOTechGenericDAO<CustomerQuotationDetail> getCustomerQuotationDetailDAO() {
		return customerQuotationDetailDAO;
	}

	public void setCustomerQuotationDetailDAO(
			AIOTechGenericDAO<CustomerQuotationDetail> customerQuotationDetailDAO) {
		this.customerQuotationDetailDAO = customerQuotationDetailDAO;
	}

}
