package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetService;
import com.aiotech.aios.accounts.domain.entity.AssetServiceDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetServiceService {

	private AIOTechGenericDAO<AssetService> assetServiceDAO;
	private AIOTechGenericDAO<AssetServiceDetail> assetServiceDetailDAO;

	/**
	 * Get All Asset Services
	 * @param implementation
	 * @return
	 * @throws Exception
	 */
	public List<AssetService> getAllAssetServices(Implementation implementation)
			throws Exception {
		return assetServiceDAO.findByNamedQuery("getAllAssetServices",
				implementation, (byte) WorkflowConstants.Status.Published.getCode());
	}
	
	/**
	 * Get All Asset Service Details
	 * @param implementation
	 * @return
	 */
	public List<AssetService> getAllAssetServiceDetails(
			Implementation implementation) throws Exception {
		return assetServiceDAO.findByNamedQuery("getAllAssetServiceDetails",
				implementation, (byte) WorkflowConstants.Status.Published.getCode());
	}
	
	/**
	 * Get All Asset Service Details + Asset Service Charges
	 * @param implementation
	 * @return
	 */
	public List<AssetServiceDetail> getAllAssetServiceCharges(
			Implementation implementation) throws Exception {
		return assetServiceDetailDAO.findByNamedQuery("getAllAssetServiceCharges",
				implementation, (byte) WorkflowConstants.Status.Published.getCode());
	}
	
	/**
	 * Get Asset Service Details
	 * @param assetServiceId
	 * @return
	 * @throws Exception
	 */
	public AssetService getAssetServiceDetails(long assetServiceId) throws Exception { 
		return assetServiceDAO.findByNamedQuery("getAssetServiceDetails", assetServiceId).get(0);
	}
	
	/**
	 * Get Asset Service Detail By Service Id
	 * @param assetServiceId
	 * @return
	 */
	public List<AssetServiceDetail> getAssetServiceDetailByServiceId(
			long assetServiceId) throws Exception { 
		return assetServiceDetailDAO.findByNamedQuery("getAssetServiceDetailByServiceId", assetServiceId);
	} 
	
	/**
	 * Save Asset Service
	 * @param assetService
	 * @param assetServiceDetails
	 * @param deleteAssetServiceDetails
	 */
	public void saveAssetService(AssetService assetService,
			List<AssetServiceDetail> assetServiceDetails,
			List<AssetServiceDetail> deleteAssetServiceDetails)
			throws Exception {
		assetServiceDAO.saveOrUpdate(assetService);
		if (null != deleteAssetServiceDetails
				&& deleteAssetServiceDetails.size() > 0)
			assetServiceDetailDAO.deleteAll(deleteAssetServiceDetails);
		if (null != assetServiceDetails && assetServiceDetails.size() > 0) {
			for (AssetServiceDetail assetServiceDetail : assetServiceDetails) {
				assetServiceDetail.setAssetService(assetService);
			}
			assetServiceDetailDAO.saveOrUpdateAll(assetServiceDetails);
		}
	}
	
	/**
	 * Delete Asset Service
	 * @param assetService
	 * @param assetServiceDetails
	 * @throws Exception
	 */
	public void deleteAssetService(AssetService assetService,
			List<AssetServiceDetail> assetServiceDetails)
			throws Exception { 
		if (null != assetServiceDetails && assetServiceDetails.size() > 0)
			assetServiceDetailDAO.deleteAll(assetServiceDetails);
		assetServiceDAO.delete(assetService);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetService> getAssetServiceDAO() {
		return assetServiceDAO;
	}

	public void setAssetServiceDAO(
			AIOTechGenericDAO<AssetService> assetServiceDAO) {
		this.assetServiceDAO = assetServiceDAO;
	}

	public AIOTechGenericDAO<AssetServiceDetail> getAssetServiceDetailDAO() {
		return assetServiceDetailDAO;
	}

	public void setAssetServiceDetailDAO(
			AIOTechGenericDAO<AssetServiceDetail> assetServiceDetailDAO) {
		this.assetServiceDetailDAO = assetServiceDetailDAO;
	} 
}
