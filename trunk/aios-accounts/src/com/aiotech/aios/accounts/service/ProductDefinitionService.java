package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductDefinition;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductDefinitionService {

	private AIOTechGenericDAO<ProductDefinition> productDefinitionDAO;

	public List<ProductDefinition> getAllProductDefinitions(
			Implementation implementation) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getAllProductDefinitions", implementation);
	}

	public List<ProductDefinition> getAllProductDefinitionParents(long productId)
			throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getAllProductDefinitionParents", productId);
	}

	public List<ProductDefinition> getProductDefinitionSubCategory(
			long productId) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getProductDefinitionSubCategory", productId);
	}

	public List<ProductDefinition> getAllDefinitionLabels(long productId)
			throws Exception {
		return productDefinitionDAO.findByNamedQuery("getAllDefinitionLabels",
				productId);
	}

	public List<ProductDefinition> getParentProductDefinitions(
			Implementation implementation, long productId) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getParentProductDefinitions", implementation, productId);
	}

	public List<ProductDefinition> getProductDefinitionByProduct(long productId)
			throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getProductDefinitionByProduct", productId);
	}

	public List<ProductDefinition> getParentProductDefinitionByGroup(
			Implementation implementation, long productId) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getParentProductDefinitionByGroup", implementation, productId);
	}

	public List<ProductDefinition> getChildProductDefinitions(
			Long productDefinitionId) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getChildProductDefinitions", productDefinitionId);
	}

	public ProductDefinition getProductDefinitionByDefinitionId(
			long productDefinitionId) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getProductDefinitionByDefinitionId", productDefinitionId).get(
				0);
	}

	public ProductDefinition findProductDefinitionByDefinitionId(
			long productDefinitionId) throws Exception {
		return productDefinitionDAO.findById(productDefinitionId);
	}

	public List<ProductDefinition> getProductDefinitionByGroupByDefinition(
			long productId, String definitionLabel, long productDefinitionId)
			throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getProductDefinitionByGroupByDefinition", productId,
				definitionLabel, productDefinitionId);
	}

	public List<ProductDefinition> getProductDefinitionByGroup(long productId,
			String definitionLabel) throws Exception {
		return productDefinitionDAO.findByNamedQuery(
				"getProductDefinitionByGroup", productId, definitionLabel);
	}

	public ProductDefinition getProductDefinitionByProductAndLabel(
			long productId, String definitionLabel) throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionDAO
				.findByNamedQuery("getProductDefinitionByProductAndLabel",
						productId, definitionLabel);
		return null != productDefinitions && productDefinitions.size() > 0 ? productDefinitions
				.get(0) : null;
	}

	public ProductDefinition getDefinitionBySpecialProductAndProduct(
			long specailProductId, long productId) throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionDAO
				.findByNamedQuery("getDefinitionBySpecialProductAndProduct",
						productId, specailProductId);
		return null != productDefinitions && productDefinitions.size() > 0 ? productDefinitions
				.get(0) : null;
	}

	public void saveProductDefinition(List<ProductDefinition> productDefinitions)
			throws Exception {
		productDefinitionDAO.saveOrUpdateAll(productDefinitions);
	}

	public void deleteProductDefinition(ProductDefinition productDefinition)
			throws Exception {
		productDefinitionDAO.delete(productDefinition);
	}

	public void saveProductDefinition(ProductDefinition productDefinition)
			throws Exception {
		productDefinitionDAO.saveOrUpdate(productDefinition);
	}

	// Getters & Setters
	public AIOTechGenericDAO<ProductDefinition> getProductDefinitionDAO() {
		return productDefinitionDAO;
	}

	public void setProductDefinitionDAO(
			AIOTechGenericDAO<ProductDefinition> productDefinitionDAO) {
		this.productDefinitionDAO = productDefinitionDAO;
	}
}
