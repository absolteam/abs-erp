/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationCharge;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.service.CustomerQuotationService;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * Customer Quotation BL
 * 
 * @author Saleem
 */
public class CustomerQuotationBL {

	private CustomerQuotationService customerQuotationService;
	private LookupMasterBL lookupMasterBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private CreditTermBL creditTermBL;
	private PackageConversionBL packageConversionBL;

	// Get all Customer Quotation
	public List<Object> getAllCustomerQuotation() throws Exception {
		List<CustomerQuotation> customerQuotations = customerQuotationService
				.getAllCustomerQuotation(getImplementation());
		List<Object> quotationObjects = new ArrayList<Object>();
		CustomerQuotationVO customerQuotationVO = null;
		for (CustomerQuotation customerQuotation : customerQuotations) {
			customerQuotationVO = new CustomerQuotationVO();
			customerQuotationVO.setCustomerQuotationId(customerQuotation
					.getCustomerQuotationId());
			customerQuotationVO.setReferenceNo(customerQuotation
					.getReferenceNo());
			customerQuotationVO
					.setOrderDateFormat(DateFormat
							.convertDateToString(customerQuotation.getDate()
									.toString()));
			customerQuotationVO.setExpiryDateFormat(DateFormat
					.convertDateToString(customerQuotation.getExpiryDate()
							.toString()));
			customerQuotationVO.setShippingDateFormat(null != customerQuotation
					.getShippingDate() ? DateFormat
					.convertDateToString(customerQuotation.getShippingDate()
							.toString()) : "");
			customerQuotationVO
					.setCustomerName(null != customerQuotation.getCustomer() ? (null != customerQuotation
							.getCustomer().getPersonByPersonId() ? customerQuotation
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat("")
							.concat(customerQuotation.getCustomer()
									.getPersonByPersonId().getLastName())
							: (null != customerQuotation.getCustomer()
									.getCompany() ? customerQuotation
									.getCustomer().getCompany()
									.getCompanyName() : customerQuotation
									.getCustomer().getCmpDeptLocation()
									.getCompany().getCompanyName()))
							: "");
			customerQuotationVO.setSalesRep(null != customerQuotation
					.getPerson() ? customerQuotation.getPerson().getFirstName()
					.concat(" ")
					.concat(customerQuotation.getPerson().getLastName()) : "");
			customerQuotationVO.setStrStatus(O2CProcessStatus
					.get(customerQuotation.getStatus()).name()
					.replaceAll("_", " "));
			if (customerQuotation.getCreditTerm() != null)
				customerQuotationVO.setCreditTermId(customerQuotation
						.getCreditTerm().getCreditTermId());
			quotationObjects.add(customerQuotationVO);
		}
		return quotationObjects;
	}

	// Show Customer Quotation Info
	public CustomerQuotationVO getCustomerQuotation(long customerQuotationId)
			throws Exception {
		CustomerQuotation customerQuotation = customerQuotationService
				.getCustomerQuotationById(customerQuotationId);
		return convertEntityQuotationVO(customerQuotation);
	}

	// Conversation
	public CustomerQuotationVO convertEntityQuotationVO(
			CustomerQuotation customerQuotation) throws Exception {
		CustomerQuotationVO customerQuotationVO = new CustomerQuotationVO();
		customerQuotationVO.setCustomerQuotationId(customerQuotation
				.getCustomerQuotationId());
		customerQuotationVO.setOrderDateFormat(DateFormat
				.convertDateToString(customerQuotation.getDate().toString()));
		customerQuotationVO.setExpiryDateFormat(DateFormat
				.convertDateToString(customerQuotation.getExpiryDate()
						.toString()));
		customerQuotationVO.setShippingDateFormat(null != customerQuotation
				.getShippingDate() ? DateFormat
				.convertDateToString(customerQuotation.getShippingDate()
						.toString()) : "");
		LookupDetail lookupDetail = null;
		if (null != customerQuotation.getLookupDetailByShippingMethod()
				&& !("").equals(customerQuotation
						.getLookupDetailByShippingMethod())) {
			lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(customerQuotation
					.getLookupDetailByShippingMethod().getLookupDetailId());
			customerQuotationVO.setLookupDetailByShippingMethod(lookupDetail);
		}
		if (null != customerQuotation.getLookupDetailByShippingTerm()
				&& !("").equals(customerQuotation
						.getLookupDetailByShippingTerm())) {
			lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(customerQuotation
					.getLookupDetailByShippingTerm().getLookupDetailId());
			customerQuotationVO.setLookupDetailByShippingTerm(lookupDetail);
		}
		customerQuotationVO.setModeOfPayment(customerQuotation
				.getModeOfPayment());
		if (null != customerQuotation.getPerson()
				&& !("").equals(customerQuotation.getPerson())) {
			Person person = new Person();
			person.setPersonId(customerQuotation.getPerson().getPersonId());
			person.setFirstName(customerQuotation.getPerson().getFirstName());
			person.setLastName(customerQuotation.getPerson().getLastName());
			customerQuotationVO.setPerson(person);
		}

		if (null != customerQuotation.getCustomer()
				&& !("").equals(customerQuotation.getCustomer())) {
			CustomerVO customer = new CustomerVO();
			customer.setCustomerId(customerQuotation.getCustomer()
					.getCustomerId());
			customer.setCustomerName(null != customerQuotation.getCustomer()
					.getPersonByPersonId() ? customerQuotation
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat(" ")
					.concat(customerQuotation.getCustomer()
							.getPersonByPersonId().getLastName())
					: (null != customerQuotation.getCustomer().getCompany() ? customerQuotation
							.getCustomer().getCompany().getCompanyName()
							: customerQuotation.getCustomer()
									.getCmpDeptLocation().getCompany()
									.getCompanyName()));

			ShippingDetail shippingDetail = null;
			List<ShippingDetail> shippingDetails = new ArrayList<ShippingDetail>();
			for (ShippingDetail shipDetail : customerQuotation.getCustomer()
					.getShippingDetails()) {
				shippingDetail = new ShippingDetail();
				shippingDetail.setShippingId(shipDetail.getShippingId());
				shippingDetail.setName(shipDetail.getName());
				shippingDetails.add(shippingDetail);
			}
			customer.setShippingDetails(new HashSet<ShippingDetail>(
					shippingDetails));
			customerQuotationVO.setCustomer(customer);
		}

		List<CustomerQuotationDetailVO> customerQuotationDetails = new ArrayList<CustomerQuotationDetailVO>();
		CustomerQuotationDetailVO customerQuotationDetailVO = null;
		Product product = null;
		for (CustomerQuotationDetail quotationDetail : customerQuotation
				.getCustomerQuotationDetails()) {
			customerQuotationDetailVO = new CustomerQuotationDetailVO();
			product = new Product();
			lookupDetail = new LookupDetail();

			product.setProductId(quotationDetail.getProduct().getProductId());
			product.setProductName(quotationDetail.getProduct()
					.getProductName());
			product.setCostingType(quotationDetail.getProduct()
					.getCostingType());
			customerQuotationDetailVO.setProduct(product);
			customerQuotationDetailVO
					.setQuantity(quotationDetail.getQuantity());
			customerQuotationDetailVO.setBaseQuantity(AIOSCommons
					.convertExponential(quotationDetail.getQuantity()));
			customerQuotationDetailVO.setUnitCode(quotationDetail.getProduct()
					.getLookupDetailByProductUnit().getAccessCode());
			customerQuotationDetailVO
					.setUnitRate(quotationDetail.getUnitRate());
			customerQuotationDetailVO
					.setDiscount(quotationDetail.getDiscount());
			customerQuotationDetailVO.setIsPercentage(quotationDetail
					.getIsPercentage());
			customerQuotationDetailVO.setPackageUnit(quotationDetail
					.getPackageUnit());
			customerQuotationDetailVO.setProductPackageVOs(packageConversionBL
					.getProductPackagingDetail(quotationDetail.getProduct()
							.getProductId()));
			if (null != quotationDetail.getProductPackageDetail()) {
				customerQuotationDetailVO.setPackageDetailId(quotationDetail
						.getProductPackageDetail().getProductPackageDetailId());
				boolean iflag = true;
				for (ProductPackageVO packagevo : customerQuotationDetailVO
						.getProductPackageVOs()) {
					if (iflag) {
						for (ProductPackageDetailVO packageDtvo : packagevo
								.getProductPackageDetailVOs()) {
							if ((long) packageDtvo.getProductPackageDetailId() == (long) quotationDetail
									.getProductPackageDetail()
									.getProductPackageDetailId()) {
								customerQuotationDetailVO
										.setConversionUnitName(packageDtvo
												.getConversionUnitName());
								iflag = true;
								break;
							}
						}
					} else
						break;
				}
				customerQuotationDetailVO.setBaseUnitName(quotationDetail
						.getProduct().getLookupDetailByProductUnit()
						.getDisplayName());
			} else {
				customerQuotationDetailVO.setConversionUnitName(quotationDetail
						.getProduct().getLookupDetailByProductUnit()
						.getDisplayName());
				customerQuotationDetailVO.setPackageDetailId(-1l);
			}
			customerQuotationDetailVO
					.setCustomerQuotationDetailId(quotationDetail
							.getCustomerQuotationDetailId());
			customerQuotationDetails.add(customerQuotationDetailVO);
		}
		Collections.sort(customerQuotationDetails,
				new Comparator<CustomerQuotationDetail>() {
					public int compare(CustomerQuotationDetail o1,
							CustomerQuotationDetail o2) {
						return o1.getCustomerQuotationDetailId().compareTo(
								o2.getCustomerQuotationDetailId());
					}
				});

		customerQuotationVO
				.setCustomerQuotationDetailVOs(customerQuotationDetails);

		// Quotation Charges
		if (null != customerQuotation.getCustomerQuotationCharges()
				&& customerQuotation.getCustomerQuotationCharges().size() > 0) {
			List<CustomerQuotationCharge> customerQuotationCharges = new ArrayList<CustomerQuotationCharge>();
			CustomerQuotationCharge customerQuotationCharge = null;
			for (CustomerQuotationCharge quotationCharge : customerQuotation
					.getCustomerQuotationCharges()) {
				customerQuotationCharge = new CustomerQuotationCharge();
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(quotationCharge
						.getLookupDetail().getLookupDetailId());
				lookupDetail.setDisplayName(quotationCharge.getLookupDetail()
						.getDisplayName());
				customerQuotationCharge.setLookupDetail(lookupDetail);
				customerQuotationCharge.setIsPercentage(quotationCharge
						.getIsPercentage());
				customerQuotationCharge
						.setCharges(quotationCharge.getCharges());
				customerQuotationCharge.setDescription(quotationCharge
						.getDescription());
				customerQuotationCharges.add(customerQuotationCharge);
			}
			customerQuotationVO
					.setCustomerQuotationCharges(new HashSet<CustomerQuotationCharge>(
							customerQuotationCharges));
		}
		return customerQuotationVO;
	}

	// Save Customer Quotation
	public void saveCustomerQuotation(CustomerQuotation customerQuotation,
			List<CustomerQuotationDetail> customerQuotationDetails,
			List<CustomerQuotationCharge> customerQuotationCharges,
			List<CustomerQuotationDetail> deleteQuotationDetails,
			List<CustomerQuotationCharge> deleteQuotationCharges)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		boolean updateFlag = false;
		customerQuotation
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		if (null != customerQuotation.getCustomerQuotationId())
			updateFlag = true;
		customerQuotation.setImplementation(getImplementation());
		if (null != deleteQuotationDetails && deleteQuotationDetails.size() > 0)
			customerQuotationService
					.deleteCustomerQuotationDetails(deleteQuotationDetails);
		if (null != deleteQuotationCharges && deleteQuotationCharges.size() > 0)
			customerQuotationService
					.deleteCustomerQuotationCharges(deleteQuotationCharges);
		if (!updateFlag)
			SystemBL.saveReferenceStamp(CustomerQuotation.class.getName(),
					getImplementation());

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (customerQuotation != null
				&& customerQuotation.getCustomerQuotationId() != null
				&& customerQuotation.getCustomerQuotationId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		customerQuotationService.saveCustomerQuotation(customerQuotation,
				customerQuotationDetails, customerQuotationCharges);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				CustomerQuotation.class.getSimpleName(),
				customerQuotation.getCustomerQuotationId(), user,
				workflowDetailVo);
	}

	public void doCustomerQuotationBusinessUpdate(Long customerQuotationId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				CustomerQuotation customerQuotation = customerQuotationService
						.getCustomerQuotationById(customerQuotationId);
				customerQuotationService
						.deleteCustomerQuotation(customerQuotation);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Customer Quotation
	public void deleteCustomerQuotation(CustomerQuotation customerQuotation)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				CustomerQuotation.class.getSimpleName(),
				customerQuotation.getCustomerQuotationId(), user,
				workflowDetailVo);
		workflowDetailVo.setDeleteFlag(true);
	}

	public Map<Byte, String> getModeOfPayment() throws Exception {
		Map<Byte, String> modeOfPayments = new HashMap<Byte, String>();
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class))
			modeOfPayments.put(modeOfPayment.getCode(), modeOfPayment.name()
					.replaceAll("_", " "));
		return modeOfPayments;
	}

	public Map<Byte, String> getO2CProcessStatus() throws Exception {
		Map<Byte, String> o2CProcessStatuses = new HashMap<Byte, String>();
		for (O2CProcessStatus o2CProcessStatus : EnumSet
				.allOf(O2CProcessStatus.class))
			o2CProcessStatuses.put(o2CProcessStatus.getCode(), o2CProcessStatus
					.name().replaceAll("_", " "));
		return o2CProcessStatuses;
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters and Setters
	public CustomerQuotationService getCustomerQuotationService() {
		return customerQuotationService;
	}

	public void setCustomerQuotationService(
			CustomerQuotationService customerQuotationService) {
		this.customerQuotationService = customerQuotationService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public PackageConversionBL getPackageConversionBL() {
		return packageConversionBL;
	}

	public void setPackageConversionBL(PackageConversionBL packageConversionBL) {
		this.packageConversionBL = packageConversionBL;
	}
}
