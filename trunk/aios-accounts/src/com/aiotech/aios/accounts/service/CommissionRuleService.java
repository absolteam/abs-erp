package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.CommissionMethod;
import com.aiotech.aios.accounts.domain.entity.CommissionPersonCombination;
import com.aiotech.aios.accounts.domain.entity.CommissionRule;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CommissionRuleService {

	private AIOTechGenericDAO<CommissionRule> commissionRuleDAO;
	private AIOTechGenericDAO<CommissionMethod> commissionMethodDAO;
	private AIOTechGenericDAO<CommissionPersonCombination> commissionPersonCombinationDAO;

	// Get Commission Rule Details
	public List<CommissionRule> getCommissionRuleDetails(
			Implementation implementation) throws Exception {
		return commissionRuleDAO.findByNamedQuery("getCommissionRuleDetails",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Commission Rule Details
	public List<CommissionMethod> getCommissionMethodByCommissionRuleId(
			long commissionRuleId) throws Exception {
		return commissionMethodDAO.findByNamedQuery(
				"getCommissionMethodByCommissionRuleId", commissionRuleId);
	}

	// Get Commission Person Combination
	public List<CommissionPersonCombination> getCommissionPersonByCommissionRuleId(
			long commissionRuleId) throws Exception {
		return commissionPersonCombinationDAO.findByNamedQuery(
				"getCommissionPersonByCommissionRuleId", commissionRuleId);
	}

	// Save Commission Rule
	public void saveCommissionRule(CommissionRule commissionRule,
			List<CommissionMethod> commissionDetails,
			List<CommissionPersonCombination> commissionPersons)
			throws Exception {
		commissionRuleDAO.saveOrUpdate(commissionRule);
		for (CommissionMethod commissionMethod : commissionDetails)
			commissionMethod.setCommissionRule(commissionRule);
		for (CommissionPersonCombination commissionPerson : commissionPersons)
			commissionPerson.setCommissionRule(commissionRule);
		commissionMethodDAO.saveOrUpdateAll(commissionDetails);
		commissionPersonCombinationDAO.saveOrUpdateAll(commissionPersons);
	}

	// Delete Commission Rule
	public void deleteCommissionRule(CommissionRule commissionRule,
			List<CommissionMethod> commissionDetails,
			List<CommissionPersonCombination> commissionPersons)
			throws Exception {
		commissionMethodDAO.deleteAll(commissionDetails);
		commissionPersonCombinationDAO.deleteAll(commissionPersons);
		commissionRuleDAO.delete(commissionRule);
	}

	// Get Commission Rule Base
	public CommissionRule getCommissionRuleBase(long commissionRuleId)
			throws Exception {
		return commissionRuleDAO.findByNamedQuery("getCommissionRuleBase",
				commissionRuleId).get(0);
	}

	// Delete Commission Methods
	public void deleteCommissionMethods(List<CommissionMethod> commissionDetails)
			throws Exception {
		commissionMethodDAO.deleteAll(commissionDetails);
	}

	// Delete Commission Persons
	public void deleteCommissionPersons(
			List<CommissionPersonCombination> commissionPersons)
			throws Exception {
		commissionPersonCombinationDAO.deleteAll(commissionPersons);
	}

	// Getters & setters
	public AIOTechGenericDAO<CommissionRule> getCommissionRuleDAO() {
		return commissionRuleDAO;
	}

	public void setCommissionRuleDAO(
			AIOTechGenericDAO<CommissionRule> commissionRuleDAO) {
		this.commissionRuleDAO = commissionRuleDAO;
	}

	public AIOTechGenericDAO<CommissionMethod> getCommissionMethodDAO() {
		return commissionMethodDAO;
	}

	public void setCommissionMethodDAO(
			AIOTechGenericDAO<CommissionMethod> commissionMethodDAO) {
		this.commissionMethodDAO = commissionMethodDAO;
	}

	public AIOTechGenericDAO<CommissionPersonCombination> getCommissionPersonCombinationDAO() {
		return commissionPersonCombinationDAO;
	}

	public void setCommissionPersonCombinationDAO(
			AIOTechGenericDAO<CommissionPersonCombination> commissionPersonCombinationDAO) {
		this.commissionPersonCombinationDAO = commissionPersonCombinationDAO;
	}
}
