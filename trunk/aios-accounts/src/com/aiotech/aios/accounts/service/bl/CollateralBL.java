package com.aiotech.aios.accounts.service.bl;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.aiotech.aios.accounts.domain.entity.CollateralDetail;
import com.aiotech.aios.accounts.service.BankService;
import com.aiotech.aios.accounts.service.CollateralService;
import com.aiotech.aios.accounts.service.LoanService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.domain.entity.Implementation;

public class CollateralBL {
	private long iTotalRecords;
	private long iTotalDisplayRecords;
	private CollateralService collateralService;
	private BankService bankService;
	private LoanService loanService;
	private List<CollateralDetail> collateralList=null;
	//Logger Property
	private static final Logger LOGGER=LogManager.getLogger(CollateralBL.class); 
	public JSONObject getCollateralList(Implementation implementation){
		try {
			collateralList=this.getCollateralService().getAllCollateral(implementation);
			if(null!=collateralList && !collateralList.equals("")){
				iTotalRecords=collateralList.size();
				iTotalDisplayRecords=collateralList.size();
				JSONObject jsonResponse= new JSONObject();
				jsonResponse.put("iTotalRecords", iTotalRecords);
				jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);  
				JSONArray data= new JSONArray();
				for(CollateralDetail list: collateralList){
					JSONArray array= new JSONArray();
					array.add(list.getCollateralId());
					array.add(list.getLoan().getBankAccount().getBank().getBankName());
					array.add(list.getLoan().getBankAccount().getAccountNumber());
					array.add(list.getLoan().getLoanNumber());
					array.add(list.getLoan().getAmount());
					array.add(Constants.Accounts.AssetDetail.get(list.getAssetId()));
					array.add(list.getAmount());
					data.add(array);
				}
				jsonResponse.put("aaData", data);   
				return jsonResponse;
			}
			else{
				return null;
			}
		} catch (Exception ex) { 
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: getCollateralList: throws Exception "+ex);   
			return null;
		} 
	}
	
	public CollateralDetail getCollateral(Long collateralId) throws Exception{
		return this.getCollateralService().getCollateralById(collateralId);
	}
	
	public List<CollateralDetail> getCollateralList(Long collateralId) throws Exception{
		return this.getCollateralService().getCollateralListById(collateralId);
	}
	
	public void saveCollateral(List<CollateralDetail> collateralDetails) throws Exception{
		this.getCollateralService().saveCollateral(collateralDetails);
	}
	public CollateralService getCollateralService() {
		return collateralService;
	}
	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}
	public BankService getBankService() {
		return bankService;
	}
	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	}
	public LoanService getLoanService() {
		return loanService;
	}
	public void setLoanService(LoanService loanService) {
		this.loanService = loanService;
	}
	public List<CollateralDetail> getCollateralList() {
		return collateralList;
	}
	public void setCollateralList(List<CollateralDetail> collateralList) {
		this.collateralList = collateralList;
	}
	public long getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public long getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(long iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
}
