package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class SegmentService {

	private AIOTechGenericDAO<Segment> segmentDAO;

	public List<Segment> getDefaultSegments(Implementation implementation)
			throws Exception {
		return segmentDAO
				.findByNamedQuery("getDefaultSegments", implementation);
	}

	public List<Segment> getAllSegments(Implementation implementation)
			throws Exception {
		return segmentDAO.findByNamedQuery("getAllSegments", implementation);
	}

	public List<Segment> getSegmentChildById(Long segmentId) throws Exception {
		return segmentDAO.findByNamedQuery("getSegmentChildById", segmentId);
	}

	public Segment getSegmentChilds(Long segmentId) throws Exception {
		return segmentDAO.findByNamedQuery("getSegmentChilds", segmentId)
				.get(0);
	}

	public Segment getAllSegmentChilds(Long segmentId) throws Exception {
		return segmentDAO.findByNamedQuery("getAllSegmentChilds", segmentId)
				.get(0);
	}

	public Segment getParentSegment(Implementation implementation,
			Integer accessId) throws Exception {
		return segmentDAO.findByNamedQuery("getParentSegment", implementation,
				accessId).get(0);
	}

	public Segment getParentSegmentSp(Implementation implementation,
			Integer accessId) throws Exception {
		return segmentDAO.findByNamedQuery("getParentSegmentSp",
				implementation, accessId).get(0);
	}

	public Segment getSegmentParentById(long segmentId) throws Exception {
		return segmentDAO.findByNamedQuery("getSegmentParentById", segmentId)
				.get(0);
	}

	public Segment getSegmentById(long segmentId) throws Exception {
		return segmentDAO.findById(segmentId);
	}

	public void saveSegment(Segment segment) throws Exception {
		segmentDAO.saveOrUpdate(segment);
	}
	
	public void saveSegment(List<Segment> segments) throws Exception {
		segmentDAO.saveOrUpdateAll(segments);
	}

	public void deleteSegment(Segment segment) throws Exception {
		segmentDAO.delete(segment);
	}

	// Getters & Setters
	public AIOTechGenericDAO<Segment> getSegmentDAO() {
		return segmentDAO;
	}

	public void setSegmentDAO(AIOTechGenericDAO<Segment> segmentDAO) {
		this.segmentDAO = segmentDAO;
	}
}
