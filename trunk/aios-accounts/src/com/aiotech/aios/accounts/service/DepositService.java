package com.aiotech.aios.accounts.service;

import com.aiotech.aios.accounts.service.bl.TransactionBL;


public class DepositService { 
	 private CurrencyService currencyService;
	 private CalendarService calendarService; 
	 private TransactionBL transactionBL;
	 private GLChartOfAccountService accountService;
	 private GLCombinationService combinationService;
	 private BankService bankService;
	
	
	public CurrencyService getCurrencyService() {
		return currencyService;
	}
	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}
	public CalendarService getCalendarService() {
		return calendarService;
	}
	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
	public TransactionBL getTransactionBL() {
		return transactionBL;
	}
	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}
	public GLChartOfAccountService getAccountService() {
		return accountService;
	}
	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}
	public GLCombinationService getCombinationService() {
		return combinationService;
	}
	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}
	public BankService getBankService() {
		return bankService;
	}
	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	} 
}
