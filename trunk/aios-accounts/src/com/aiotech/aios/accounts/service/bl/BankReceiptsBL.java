package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.BankReceiptsService;
import com.aiotech.aios.accounts.service.PettyCashService;
import com.aiotech.aios.common.to.Constants.Accounts.CurrencyMode;
import com.aiotech.aios.common.to.Constants.Accounts.GeneralStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ReceiptSource;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.vo.ConfigurationVO;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class BankReceiptsBL {

	private Implementation implementation;
	private BankReceiptsService bankReceiptsService;
	private TransactionBL transactionBL;
	private LookupMasterBL lookupMasterBL;
	private SalesInvoiceBL salesInvoiceBL;
	private DebitBL debitBL;
	private AssetClaimInsuranceBL assetClaimInsuranceBL;
	private AssetWarrantyClaimBL assetWarrantyClaimBL;
	private AlertBL alertBL;
	private PettyCashService pettyCashService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private ConfigurationHandler configurationHandler;

	// get all bank receipts
	public JSONObject convertBankReceiptsToJson(List<BankReceipts> bankReceipts)
			throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		double amount = 0;
		for (BankReceipts list : bankReceipts) {
			amount = 0;
			array = new JSONArray();
			array.add(list.getBankReceiptsId());
			array.add(list.getReceiptsNo());
			array.add(list.getLookupDetail().getDisplayName());
			array.add(DateFormat.convertDateToString(list.getReceiptsDate()
					.toString()));
			array.add(list.getCurrency().getCurrencyPool().getCode());
			for (BankReceiptsDetail bankReceiptsDetail : list
					.getBankReceiptsDetails())
				amount += bankReceiptsDetail.getAmount();
			array.add(AIOSCommons.formatAmount(amount));
			array.add(list.getReferenceNo());
			if (null != list.getSupplier()) {
				if (null != list.getSupplier().getPerson())
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					array.add(null != list.getSupplier().getCompany() ? list
							.getSupplier().getCompany().getCompanyName() : list
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName());
			} else if (null != list.getCustomer()) {
				if (null != list.getCustomer().getPersonByPersonId())
					array.add(list
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat(" ")
							.concat(list.getCustomer().getPersonByPersonId()
									.getLastName()));
				else
					array.add(null != list.getCustomer().getCompany() ? list
							.getCustomer().getCompany().getCompanyName() : list
							.getCustomer().getCmpDeptLocation().getCompany()
							.getCompanyName());
			} else if (null != list.getPersonByPersonId()) {
				array.add(list.getPersonByPersonId().getFirstName().concat(" ")
						.concat(list.getPersonByPersonId().getLastName()));
			} else {
				array.add(list.getOthers());
			}
			array.add(null != list.getPersonByCreatedBy() ? list
					.getPersonByCreatedBy().getFirstName()
					+ " "
					+ list.getPersonByCreatedBy().getLastName() : "");
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// Save or update bank receipts
	public void saveBankReceipts(BankReceipts bankReceipts,
			List<BankReceiptsDetail> bankReceiptsDetails,
			List<BankReceiptsDetail> deleteBankReceiptsDetails,
			List<BankReceiptsDetail> reverseBankReceiptsDetails,
			List<BankReceiptsDetail> entryBankReceiptsDetails, long alertId)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		bankReceipts.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (bankReceipts != null && bankReceipts.getBankReceiptsId() != null
				&& bankReceipts.getBankReceiptsId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		boolean updateFlag = false;
		if (null != bankReceipts.getBankReceiptsId())
			updateFlag = true;
		// Save Bank Receipts & Bank Receipts Details
		for (BankReceiptsDetail bankReceiptsDetail : bankReceiptsDetails) {
			if ((byte) bankReceiptsDetail.getPaymentMode() == (byte) ModeOfPayment.Cheque
					.getCode()) {
				Period period = this
						.getTransactionBL()
						.getCalendarBL()
						.transactionPostingPeriod(
								DateFormat.convertStringToDate(DateFormat
										.convertDateToString(DateTime.now()
												.toLocalDate().toString())));
				Period pdcPeriod = this
						.getTransactionBL()
						.getCalendarBL()
						.getCalendarService()
						.getPeriodByPeriodDate(
								bankReceiptsDetail.getChequeDate(),
								this.getImplementation());
				int days = 0;
				if (null != pdcPeriod)
					days = Days.daysBetween(new DateTime(period.getEndTime()),
							new DateTime(pdcPeriod.getEndTime())).getDays();
				else
					days = Days.daysBetween(new DateTime(period.getEndTime()),
							new DateTime(bankReceiptsDetail.getChequeDate()))
							.getDays();
				if (days > 0) {
					bankReceiptsDetail.setPaymentStatus(PaymentStatus.PDC
							.getCode());
					Combination combination = new Combination();
					combination.setCombinationId(getImplementation()
							.getPdcReceived());
					bankReceiptsDetail.setCombination(combination);
					bankReceiptsDetail.setIsPdc(true);
				} else {
					bankReceiptsDetail.setPaymentStatus(PaymentStatus.Paid
							.getCode());
					bankReceiptsDetail.setIsPdc(false);
				}
			} else {
				bankReceiptsDetail.setPaymentStatus(PaymentStatus.Paid
						.getCode());
				bankReceiptsDetail.setIsPdc(false);
			}
		}
		bankReceipts.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
				bankReceiptsDetails));
		if (!updateFlag) {
			SystemBL.saveReferenceStamp(BankReceipts.class.getName(),
					getImplementation());
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			bankReceipts.setPersonByCreatedBy(person);
		} else {
			if (null != deleteBankReceiptsDetails
					&& deleteBankReceiptsDetails.size() > 0) {
				// Delete Bank Receipts Detail
				bankReceiptsService
						.deleteBankReceiptsDetail(deleteBankReceiptsDetails);
			}
			transactionBL.deleteTransaction(bankReceipts.getBankReceiptsId(),
					BankReceipts.class.getSimpleName());
		}
		bankReceiptsService.saveBankReceipts(bankReceipts, bankReceiptsDetails);
		if (bankReceipts.getUseCase() != null
				&& bankReceipts.getRecordId() != null)
			alertBL.commonUpdateUseCaseStatus(bankReceipts.getUseCase(),
					bankReceipts.getRecordId(), GeneralStatus.Pending.getCode());
		for (BankReceiptsDetail bankReceiptsDetail2 : bankReceiptsDetails) {
			if (bankReceiptsDetail2.getUseCase() != null
					&& bankReceiptsDetail2.getRecordId() != null)
				alertBL.commonUpdateUseCaseStatus(
						bankReceiptsDetail2.getUseCase(),
						bankReceiptsDetail2.getRecordId(),
						GeneralStatus.Pending.getCode());
		}
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankReceipts.class.getSimpleName(),
				bankReceipts.getBankReceiptsId(), user, workflowDetailVo);

	}

	public void doBankReceiptsBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		BankReceipts bankReceipts = this.getBankReceiptsService()
				.findByBankReceiptsId(recordId);
		List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>(
				bankReceipts.getBankReceiptsDetails());
		if (workflowDetailVO.isDeleteFlag()) {
			if (bankReceipts.getUseCase() != null
					&& bankReceipts.getRecordId() != null)
				alertBL.commonReverseUpdateUseCaseStatus(
						bankReceipts.getUseCase(), bankReceipts.getRecordId(),
						GeneralStatus.Pending.getCode());

			transactionBL.deleteTransaction(bankReceipts.getBankReceiptsId(),
					BankReceipts.class.getSimpleName());
			// Delete Bank receipts & Details
			bankReceiptsService.deleteBankReceipts(bankReceipts);
		} else {
			// Entry transaction
			Transaction transcation = createBankReceiptsTransaction(
					bankReceipts, bankReceiptsDetails);
			transactionBL.saveTransaction(
					transcation,
					new ArrayList<TransactionDetail>(transcation
							.getTransactionDetails()));

			if (null != bankReceipts.getUseCase()
					&& bankReceipts.getUseCase().equalsIgnoreCase(
							SalesInvoice.class.getSimpleName())) {
				double invoiceAmount = 0;
				for (BankReceiptsDetail receiptDetail : bankReceiptsDetails) {
					invoiceAmount += receiptDetail.getAmount();
				}
				Transaction revenueTransaction = transactionBL
						.createTransaction(
								transactionBL.getCalendarBL()
										.transactionPostingPeriod(
												bankReceipts.getReceiptsDate()),
								bankReceipts.getCurrency().getCurrencyId(),
								"Sales Revenue",
								bankReceipts.getReceiptsDate(), transactionBL
										.getCategory("Sales Receipt"),
								bankReceipts.getBankReceiptsId(),
								BankReceipts.class.getSimpleName());
				List<TransactionDetail> transactionDetails = createSalesRevenueTransaction(
						bankReceipts, invoiceAmount);
				transactionBL.saveTransaction(revenueTransaction,
						transactionDetails);
			}
			if (bankReceipts.getUseCase() != null
					&& bankReceipts.getRecordId() != null)
				alertBL.commonUpdateUseCaseStatus(bankReceipts.getUseCase(),
						bankReceipts.getRecordId(),
						GeneralStatus.Completed.getCode());
			for (BankReceiptsDetail bankReceiptsDetail2 : bankReceiptsDetails) {
				if (bankReceiptsDetail2.getUseCase() != null
						&& bankReceiptsDetail2.getRecordId() != null)
					alertBL.commonUpdateUseCaseStatus(
							bankReceiptsDetail2.getUseCase(),
							bankReceiptsDetail2.getRecordId(),
							GeneralStatus.Pending.getCode());
			}
		}

	}

	private List<TransactionDetail> createSalesRevenueTransaction(
			BankReceipts bankReceipt, double invoiceAmount) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		TransactionDetail debitTransaction = transactionBL
				.createTransactionDetail(invoiceAmount, getImplementation()
						.getUnearnedRevenue(), TransactionType.Debit.getCode(),
						null, null, bankReceipt.getReceiptsNo());
		TransactionDetail creditTransaction = transactionBL
				.createTransactionDetail(invoiceAmount, getImplementation()
						.getRevenueAccount(), TransactionType.Credit.getCode(),
						null, null, bankReceipt.getReceiptsNo());
		transactionDetails.add(debitTransaction);
		transactionDetails.add(creditTransaction);
		return transactionDetails;
	}

	// Update Bank Receipts PDC
	public void updateBankReceiptsPDC(BankReceipts bankReceipts,
			List<BankReceiptsDetail> bankReceiptsDetails, long alertId)
			throws Exception {
		// Transaction
		Transaction transaction = clearPDCTransaction(bankReceipts,
				bankReceiptsDetails, true, false);
		transactionBL.saveTransaction(
				transaction,
				new ArrayList<TransactionDetail>(transaction
						.getTransactionDetails()));
		// Update Bank Receipts Details
		bankReceiptsService.updateBankReceiptsDetail(bankReceiptsDetails);
	}

	public void updateBankReceiptsPDC(BankReceiptsDetail bankReceiptsDetail,
			long alertId) throws Exception {
		Transaction transaction = createPDCReverseEntries(bankReceiptsDetail);
		transactionBL.saveTransaction(
				transaction,
				new ArrayList<TransactionDetail>(transaction
						.getTransactionDetails()));

		transactionBL.saveTransaction(
				transaction,
				new ArrayList<TransactionDetail>(transaction
						.getTransactionDetails()));

		bankReceiptsDetail.setIsPdc(false);
		bankReceiptsDetail.setPaymentStatus(PaymentStatus.Paid.getCode());
		Combination combination = transactionBL.getCombinationService()
				.getCombination(getImplementation().getClearingAccount());
		bankReceiptsDetail.setCombination(combination);

		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				bankReceiptsDetail.getBankReceiptsDetailId(),
				BankReceiptsDetail.class.getSimpleName());
		alert.setIsActive(false);
		alertBL.getAlertService().updateAlert(alert);
		bankReceiptsService.updateBankReceiptsDetail(bankReceiptsDetail);
	}

	private Transaction createPDCReverseEntries(
			BankReceiptsDetail bankReceiptsDetail) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(transactionBL.createTransactionDetail(
				bankReceiptsDetail.getAmount(), getImplementation()
						.getClearingAccount(), TransactionType.Debit.getCode(),
				"Receipt(PDC Clearance)", null, bankReceiptsDetail
						.getBankReceipts().getReceiptsNo()));
		transactionDetails.add(transactionBL.createTransactionDetail(
				bankReceiptsDetail.getAmount(), bankReceiptsDetail
						.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(), "Receipt(PDC Clearance)",
				null, bankReceiptsDetail.getBankReceipts().getReceiptsNo()));
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						java.util.Calendar.getInstance().getTime(),
						getImplementation()), bankReceiptsDetail
						.getBankReceipts().getCurrency().getCurrencyId(),
				"Receipts for reference.no : "
						+ bankReceiptsDetail.getBankReceipts().getReceiptsNo(),
				DateFormat.convertStringToDate(DateFormat
						.convertDateToString(DateTime.now().toLocalDate()
								.toString())), transactionBL
						.getCategory("Bank Receipts"), bankReceiptsDetail
						.getBankReceipts().getBankReceiptsId(),
				BankReceipts.class.getSimpleName(), bankReceiptsDetail
						.getBankReceipts().getExchangeRate(),
				getImplementation());
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Delete bank receipt & details & reverse the transaction
	public void deleteBankReceipts(BankReceipts bankReceipts) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankReceipts.class.getSimpleName(),
				bankReceipts.getBankReceiptsId(), user, workflowDetailVo);
	}

	// Clear PDC Transaction
	private Transaction clearPDCTransaction(BankReceipts bankReceipts,
			List<BankReceiptsDetail> bankReceiptsDetails, Boolean debitFlag,
			Boolean creditFlag) throws Exception {
		double amount = 0;
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		for (BankReceiptsDetail receipts : bankReceiptsDetails) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					receipts.getAmount(), this.getImplementation()
							.getClearingAccount(), debitFlag, receipts
							.getDescription(), null, bankReceipts
							.getReceiptsNo()));
			transactionDetails.add(transactionBL.createTransactionDetail(
					receipts.getAmount(), this.getImplementation()
							.getPdcReceived(), creditFlag, receipts
							.getDescription(), null, bankReceipts
							.getReceiptsNo()));
			amount += receipts.getAmount();
		}
		Transaction transaction = transactionBL.createTransaction(bankReceipts
				.getCurrency().getCurrencyId(), bankReceipts.getDescription(),
				bankReceipts.getReceiptsDate(), transactionBL
						.getCategory("Bank Receipts"), bankReceipts
						.getBankReceiptsId(), BankReceipts.class
						.getSimpleName());
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Create bank receipt transaction
	private Transaction createBankReceiptsTransaction(
			BankReceipts bankReceipts,
			List<BankReceiptsDetail> bankReceiptsDetails) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		String lineDescription = "";
		double totalAmount = 0;
		String description = null != bankReceipts.getDescription() ? bankReceipts
				.getDescription()
				+ " & Receipt Ref: "
				+ bankReceipts.getReferenceNo() : "Receipt Ref: "
				+ bankReceipts.getReferenceNo();
		for (BankReceiptsDetail receipts : bankReceiptsDetails) {
			if (null != receipts.getDescription()
					&& !("").equals(receipts.getDescription().trim()))
				lineDescription = receipts.getDescription()
						+ " & Receipt Ref: " + bankReceipts.getReferenceNo();
			else
				lineDescription = "Receipt Ref:"
						+ bankReceipts.getReferenceNo();

			transactionDetails.add(transactionBL.createTransactionDetail(
					receipts.getAmount(), receipts.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(), lineDescription, null, bankReceipts
							.getReceiptsNo()));
			totalAmount += receipts.getAmount();
		}
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, bankReceipts.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(), description, null,
				bankReceipts.getReceiptsNo()));
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						bankReceipts.getReceiptsDate()), bankReceipts
						.getCurrency().getCurrencyId(), description,
				bankReceipts.getReceiptsDate(), transactionBL
						.getCategory("Bank Receipt"), bankReceipts
						.getBankReceiptsId(), BankReceipts.class
						.getSimpleName(), bankReceipts.getExchangeRate());
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	public Map<String, String> getModeOfPayment() throws Exception {
		Map<String, String> modeOfPayments = new HashMap<String, String>();
		String key = "";
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class)) {
			if ((byte) ModeOfPayment.Cash.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cash.getCode().toString().concat("@@")
						.concat("CSH");
			else if ((byte) ModeOfPayment.Cheque.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cheque.getCode().toString().concat("@@")
						.concat("CHQ");
			else if ((byte) ModeOfPayment.Credit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Credit_Card.getCode().toString()
						.concat("@@").concat("CCD");
			else if ((byte) ModeOfPayment.Debit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Debit_Card.getCode().toString()
						.concat("@@").concat("DCD");
			else if ((byte) ModeOfPayment.Wire_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Wire_Transfer.getCode().toString()
						.concat("@@").concat("WRT");
			else if ((byte) ModeOfPayment.Inter_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Inter_Transfer.getCode().toString()
						.concat("@@").concat("IAT");
			modeOfPayments.put(key, modeOfPayment.name().replaceAll("_", " "));
		}
		return modeOfPayments;
	}

	public Map<String, ReceiptSource> getReceiptSource() throws Exception {
		Map<String, ReceiptSource> receiptSources = new HashMap<String, ReceiptSource>();
		String key = "";
		for (ReceiptSource receiptSource : EnumSet.allOf(ReceiptSource.class)) {
			if ((byte) ReceiptSource.Customer.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Customer.getCode().toString().concat("@@")
						.concat("CST");
			else if ((byte) ReceiptSource.Supplier.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Supplier.getCode().toString().concat("@@")
						.concat("SPL");
			else if ((byte) ReceiptSource.Employee.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Employee.getCode().toString().concat("@@")
						.concat("EMP");
			else if ((byte) ReceiptSource.Others.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Others.getCode().toString().concat("@@")
						.concat("OT");
			receiptSources.put(key, receiptSource);
		}
		return receiptSources;
	}

	public Map<String, PaymentStatus> getPaymentStatus() throws Exception {
		Map<String, PaymentStatus> paymentStatuses = new HashMap<String, PaymentStatus>();
		String key = "";
		for (PaymentStatus paymentStatus : EnumSet.allOf(PaymentStatus.class)) {
			if ((byte) PaymentStatus.Paid.getCode() == (byte) paymentStatus
					.getCode()) {
				key = PaymentStatus.Paid.getCode().toString().concat("@@")
						.concat("PAID");
				paymentStatuses.put(key, paymentStatus);
			} else if ((byte) PaymentStatus.PDC.getCode() == (byte) paymentStatus
					.getCode()) {
				key = PaymentStatus.PDC.getCode().toString().concat("@@")
						.concat("PDC");
				paymentStatuses.put(key, paymentStatus);
			}
		}
		return paymentStatuses;
	}

	public Object getPOSReceiptType() throws Exception {
		Map<Byte, String> posrcTypes = new HashMap<Byte, String>();
		for (CurrencyMode currencyMode : EnumSet.allOf(CurrencyMode.class))
			posrcTypes.put(currencyMode.getCode(), currencyMode.name());
		return posrcTypes;
	}

	public void receiptStatusUpdateCall() {
		try {
			List<Implementation> implementations = alertBL.getSystemService()
					.getAllImplementation();
			for (Implementation implementation : implementations) {
				List<BankReceipts> bankReceiptsList = this
						.getBankReceiptsService().getAllBankReceipts(
								implementation);
				for (BankReceipts bankReceipts : bankReceiptsList) {
					if (bankReceipts.getUseCase() != null
							&& bankReceipts.getRecordId() != null)
						alertBL.commonUpdateUseCaseStatus(
								bankReceipts.getUseCase(),
								bankReceipts.getRecordId(),
								GeneralStatus.Completed.getCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateBankReceiptsTransaction(List<BankReceipts> bankReceipts)
			throws Exception {
		for (BankReceipts bankReceipt : bankReceipts)
			this.processBankReceiptTransaction(
					bankReceipt,
					new ArrayList<BankReceiptsDetail>(bankReceipt
							.getBankReceiptsDetails()));
	}

	private void processBankReceiptTransaction(BankReceipts bankReceipt,
			List<BankReceiptsDetail> bankReceiptsDetails) throws Exception {
		transactionBL.deleteTransaction(bankReceipt.getBankReceiptsId(),
				BankReceipts.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;
		String lineDescription = "";
		String description = null != bankReceipt.getDescription() ? bankReceipt
				.getDescription()
				+ " & Receipt Ref: "
				+ bankReceipt.getReferenceNo() : "Receipt Ref: "
				+ bankReceipt.getReferenceNo();
		for (BankReceiptsDetail list : bankReceiptsDetails) {
			if (null != list.getDescription()
					&& !("").equals(list.getDescription()))
				lineDescription = list.getDescription() + " & Receipt Ref: "
						+ bankReceipt.getReferenceNo();
			else
				lineDescription = "Receipt Ref:" + bankReceipt.getReferenceNo();
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), lineDescription, null,
					bankReceipt.getReceiptsNo()));
			totalAmount += list.getAmount();
		}
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, bankReceipt.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(), description, null,
				bankReceipt.getReceiptsNo()));
		Transaction transaction = transactionBL.createTransaction(bankReceipt
				.getCurrency().getCurrencyId(), description, bankReceipt
				.getReceiptsDate(), transactionBL.getCategory("Bank Receipt"),
				bankReceipt.getBankReceiptsId(), BankReceipts.class
						.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);
		if (null != bankReceipt.getUseCase()
				&& bankReceipt.getUseCase().equalsIgnoreCase(
						SalesInvoice.class.getSimpleName())) {
			double invoiceAmount = 0;
			for (BankReceiptsDetail receiptDetail : bankReceiptsDetails)
				invoiceAmount += receiptDetail.getAmount();
			Transaction revenueTransaction = transactionBL.createTransaction(
					bankReceipt.getCurrency().getCurrencyId(), "Sales Revenue",
					bankReceipt.getReceiptsDate(),
					transactionBL.getCategory("Sales Receipt"),
					bankReceipt.getBankReceiptsId(),
					BankReceipts.class.getSimpleName());
			List<TransactionDetail> revenueTransactionDetails = createSalesRevenueTransaction(
					bankReceipt, invoiceAmount);
			transactionBL.saveTransaction(revenueTransaction,
					revenueTransactionDetails);
			transactionBL.saveTransaction(revenueTransaction,
					revenueTransactionDetails);
		}
	}

	public void alertBankReceiptPDCCheque() {
		try {
			List<Implementation> implementations = alertBL.getSystemService()
					.getAllImplementation();
			ConfigurationVO configurationVO = null;
			LocalDate localeDate = null;
			for (Implementation implementation : implementations) {
				localeDate = DateTime.now().toLocalDate();
				configurationVO = configurationHandler
						.getConfigurationSetup(implementation
								.getImplementationId());
				if (null != configurationVO)
					localeDate = DateTime.now().toLocalDate()
							.plusDays(configurationVO.getAlertDays());
				List<BankReceiptsDetail> bankReceiptsDetails = bankReceiptsService
						.getPDCBankReceipts(DateFormat
								.convertStringToDate(DateFormat
										.convertDateToString(localeDate
												.toString())), implementation,
								ModeOfPayment.Cheque.getCode());
				if (null != bankReceiptsDetails
						&& bankReceiptsDetails.size() > 0)
					alertBL.alertBankReceiptsPDCCheque(bankReceiptsDetails,
							configurationVO);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public BankReceiptsService getBankReceiptsService() {
		return bankReceiptsService;
	}

	public void setBankReceiptsService(BankReceiptsService bankReceiptsService) {
		this.bankReceiptsService = bankReceiptsService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public SalesInvoiceBL getSalesInvoiceBL() {
		return salesInvoiceBL;
	}

	public void setSalesInvoiceBL(SalesInvoiceBL salesInvoiceBL) {
		this.salesInvoiceBL = salesInvoiceBL;
	}

	public AssetClaimInsuranceBL getAssetClaimInsuranceBL() {
		return assetClaimInsuranceBL;
	}

	public void setAssetClaimInsuranceBL(
			AssetClaimInsuranceBL assetClaimInsuranceBL) {
		this.assetClaimInsuranceBL = assetClaimInsuranceBL;
	}

	public AssetWarrantyClaimBL getAssetWarrantyClaimBL() {
		return assetWarrantyClaimBL;
	}

	public void setAssetWarrantyClaimBL(
			AssetWarrantyClaimBL assetWarrantyClaimBL) {
		this.assetWarrantyClaimBL = assetWarrantyClaimBL;
	}

	public DebitBL getDebitBL() {
		return debitBL;
	}

	public void setDebitBL(DebitBL debitBL) {
		this.debitBL = debitBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public PettyCashService getPettyCashService() {
		return pettyCashService;
	}

	public void setPettyCashService(PettyCashService pettyCashService) {
		this.pettyCashService = pettyCashService;
	}

	public ConfigurationHandler getConfigurationHandler() {
		return configurationHandler;
	}

	@Autowired
	public void setConfigurationHandler(
			ConfigurationHandler configurationHandler) {
		this.configurationHandler = configurationHandler;
	}
}
