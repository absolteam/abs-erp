package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductSplit;
import com.aiotech.aios.accounts.domain.entity.ProductSplitDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductSplitService {

	private AIOTechGenericDAO<ProductSplit> productSplitDAO;
	private AIOTechGenericDAO<ProductSplitDetail> productSplitDetailDAO;

	// Get all Production Receive
	public List<ProductSplit> getAllProductSplit(Implementation implementation)
			throws Exception {
		return productSplitDAO.findByNamedQuery("getAllProductSplit",
				implementation);
	}

	// Get all Production Receive
	public ProductSplit getProductSplitById(Long productSplitId)
			throws Exception {
		List<ProductSplit> productSplits = productSplitDAO.findByNamedQuery(
				"getProductSplitById", productSplitId);
		if (productSplits != null && productSplits.size() > 0)
			return productSplits.get(0);
		else
			return null;
	}
	
	public List<ProductSplitDetail> getProductSplitDetailBySplitId(Long productSplitId)
			throws Exception {
		return productSplitDetailDAO.findByNamedQuery(
				"getProductSplitDetailBySplitId", productSplitId);
		
	}
	
	public void saveProductSplit(ProductSplit productSplit,List<ProductSplitDetail> productSplitDetails){
		productSplitDAO.saveOrUpdate(productSplit);
		for (ProductSplitDetail productSplitDetail : productSplitDetails) {
			productSplitDetail.setProductSplit(productSplit);
		}
		productSplitDetailDAO.saveOrUpdateAll(productSplitDetails);
	}
	
	public void deleteProductSplit(ProductSplit productSplit,List<ProductSplitDetail> productSplitDetails){
		productSplitDetailDAO.deleteAll(productSplitDetails);
		productSplitDAO.delete(productSplit);

	}
	
	public void deleteProductSplitDetail(List<ProductSplitDetail> productSplitDetails){
		productSplitDetailDAO.deleteAll(productSplitDetails);

	}

	public AIOTechGenericDAO<ProductSplit> getProductSplitDAO() {
		return productSplitDAO;
	}

	public void setProductSplitDAO(
			AIOTechGenericDAO<ProductSplit> productSplitDAO) {
		this.productSplitDAO = productSplitDAO;
	}

	public AIOTechGenericDAO<ProductSplitDetail> getProductSplitDetailDAO() {
		return productSplitDetailDAO;
	}

	public void setProductSplitDetailDAO(
			AIOTechGenericDAO<ProductSplitDetail> productSplitDetailDAO) {
		this.productSplitDetailDAO = productSplitDetailDAO;
	}
}
