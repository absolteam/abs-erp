package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Emi;
import com.aiotech.aios.accounts.domain.entity.EmiDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class EmiDetailService {
	
	private AIOTechGenericDAO<EmiDetail> emiDetailDAO;
	private AIOTechGenericDAO<Emi> emiDAO;
	
	public List<Emi> getAllEMI(Implementation implementation) throws Exception{
		return emiDAO.findByNamedQuery("getAllEMI", implementation);
	}
	
	public void saveEMI(Emi emi, List<EmiDetail> detailList) throws Exception{
		emiDAO.saveOrUpdate(emi);
		for(EmiDetail list: detailList){
			list.setEmi(emi);
		}
		emiDetailDAO.saveOrUpdateAll(detailList);
	}
	
	public List<EmiDetail> getEMIPayments(long emiId) throws Exception{
		return emiDetailDAO.findByNamedQuery("getEMIPayments", emiId);
	}
	
	public List<Emi>getEMISchedule(long loanId) throws Exception{
		return emiDAO.findByNamedQuery("getEMIScheduleDetails", loanId);
	}

	public AIOTechGenericDAO<EmiDetail> getEmiDetailDAO() {
		return emiDetailDAO;
	}

	public void setEmiDetailDAO(AIOTechGenericDAO<EmiDetail> emiDetailDAO) {
		this.emiDetailDAO = emiDetailDAO;
	}

	public AIOTechGenericDAO<Emi> getEmiDAO() {
		return emiDAO;
	}

	public void setEmiDAO(AIOTechGenericDAO<Emi> emiDAO) {
		this.emiDAO = emiDAO;
	}
	
}
