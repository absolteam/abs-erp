package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DebitDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DebitService {
	private AIOTechGenericDAO<Debit> debitDAO;
	private AIOTechGenericDAO<DebitDetail> debitDetailDAO;
	
	public List<Debit> getAllDebits(Implementation implementation) throws Exception{
		return debitDAO.findByNamedQuery("getAllDebitNotes", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public void saveDebit(Debit debit, List<DebitDetail> detailList) throws Exception{
		debitDAO.saveOrUpdate(debit);
		for(DebitDetail list: detailList)
			list.setDebit(debit);
		debitDetailDAO.saveOrUpdateAll(detailList);
	}
	
	public Debit getDebitById(Long debitId)throws Exception{
		return debitDAO.findById(debitId);
	}
	
	public Debit getDebitNoteInfo(Long debitId)throws Exception{
		return debitDAO.findByNamedQuery("getDebitNoteInfo", debitId).get(0);
	}
	
	public Debit getDebitNotesById(Long debitId)throws Exception{
		return debitDAO.findByNamedQuery("getDebitNoteById",debitId).get(0);
	}
	
	public List<Debit> getDebitByReturnId(Long goodsReturnId) throws Exception{
		return debitDAO.findByNamedQuery("getDebitByReturnId", goodsReturnId);
	}
	
	public List<DebitDetail> getDebitDetailById(Long debitId)throws Exception{
		return debitDetailDAO.findByNamedQuery("getDebitDetailById", debitId);
	}
	
	public void deleteDebits(Debit debit, List<DebitDetail> detailList) throws Exception{
		debitDetailDAO.deleteAll(detailList);
		debitDAO.delete(debit); 
	}
	
	public void deleteDebitDetail(List<DebitDetail> detailList) throws Exception{
		debitDetailDAO.deleteAll(detailList); 
	}
	
	@SuppressWarnings("unchecked")
	public List<Debit> getFilteredDebits(Implementation implementation, Long debitId, Long returnId, Long supplierId, Date fromDate, Date toDate) throws Exception{
		Criteria criteria = debitDAO.createCriteria();
		
		criteria.createAlias("goodsReturn", "r", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("r.supplier", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.person", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "cdl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.company", "sc", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("debitDetails", "dd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("dd.product", "dd_p", CriteriaSpecification.LEFT_JOIN);
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		if (debitId != null && debitId > 0) {
			criteria.add(Restrictions.eq("debitId", debitId));
		}
		
		if (returnId != null && returnId > 0) {
			criteria.add(Restrictions.eq("r.returnId", returnId));
		}
		
		if (supplierId != null && supplierId > 0) {
			criteria.add(Restrictions.eq("s.supplierId", supplierId));
		}
		
		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date",fromDate, toDate));
		}
		Set<Debit> uniqueRecords = new HashSet<Debit>(criteria.list());
		List<Debit> debitList = new ArrayList<Debit>(uniqueRecords); 
		return debitList;
	}
	
	public AIOTechGenericDAO<Debit> getDebitDAO() {
		return debitDAO;
	}
	public void setDebitDAO(AIOTechGenericDAO<Debit> debitDAO) {
		this.debitDAO = debitDAO;
	}
	public AIOTechGenericDAO<DebitDetail> getDebitDetailDAO() {
		return debitDetailDAO;
	}
	public void setDebitDetailDAO(AIOTechGenericDAO<DebitDetail> debitDetailDAO) {
		this.debitDetailDAO = debitDetailDAO;
	}
	
}
