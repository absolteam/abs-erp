package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MaterialTransferService {

	private AIOTechGenericDAO<MaterialTransfer> materialTransferDAO;
	private AIOTechGenericDAO<MaterialTransferDetail> materialTransferDetailDAO;

	// Get All Material Transfer
	public List<MaterialTransfer> getAllMaterialTransfer(
			Implementation implementation) throws Exception {
		return materialTransferDAO.findByNamedQuery("getAllMaterialTransfer",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Material Transfer By Id
	public MaterialTransfer getAllMaterialTransferById(long materialTransferId)
			throws Exception {
		return materialTransferDAO.findByNamedQuery(
				"getAllMaterialTransferById", materialTransferId).get(0);
	}

	// Get Material Transfer By Id
	public MaterialTransfer getMaterialTransferById(long materialTransferId)
			throws Exception {
		return materialTransferDAO.findByNamedQuery("getMaterialTransferById",
				materialTransferId).get(0);
	}

	public MaterialTransferDetail getMaterialTransferDetailById(
			long materialTransferDetailId) throws Exception {
		return materialTransferDetailDAO.findByNamedQuery(
				"getMaterialTransferDetailById", materialTransferDetailId).get(
				0);
	}

	// Save Material Transfer
	public void saveMaterialTransfer(MaterialTransfer materialTransfer,
			List<MaterialTransferDetail> materialTransferDetails)
			throws Exception {
		materialTransferDAO.saveOrUpdate(materialTransfer);
		for (MaterialTransferDetail materialTransferDetail : materialTransferDetails)
			materialTransferDetail.setMaterialTransfer(materialTransfer);
		materialTransferDetailDAO.saveOrUpdateAll(materialTransferDetails);
	}

	// Delete Material Transfer
	public void deleteMaterialTransfer(MaterialTransfer materialTransfer,
			List<MaterialTransferDetail> materialTransferDetails)
			throws Exception {
		materialTransferDetailDAO.deleteAll(materialTransferDetails);
		materialTransferDAO.delete(materialTransfer);
	}

	public List<MaterialTransferDetail> getMaterialTransferDetailByRequisitionId(
			long materialRequisitionDetailId) throws Exception {
		return materialTransferDetailDAO.findByNamedQuery(
				"getMaterialTransferDetailByRequisitionId",
				materialRequisitionDetailId);
	}

	@SuppressWarnings("unchecked")
	public List<MaterialTransfer> getFilteredMaterialTransfer(
			Implementation implementation, Long materialTransferId,
			Long personId, Long productId, Long fromStoreId, Long toStoreId,
			Long typeId, Long reasonId, Date fromDate, Date toDate)
			throws Exception {

		Criteria criteria = materialTransferDAO.createCriteria();

		criteria.createAlias("personByTransferPerson", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("materialTransferDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.lookupDetailByTransferType", "tr_type",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.lookupDetailByTransferReason", "tr_rsn",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.shelfByFromShelfId", "fromslf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("fromslf.shelf", "frack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("frack.aisle", "fasl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("fasl.store", "fstr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.shelfByShelfId", "toslf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("toslf.shelf", "track",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("track.aisle", "tasl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tasl.store", "tstr",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (materialTransferId != null && materialTransferId > 0) {
			criteria.add(Restrictions.eq("materialTransferId",
					materialTransferId));
		}

		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("prs.personId", personId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		if (fromStoreId != null && fromStoreId > 0) {
			criteria.add(Restrictions.eq("from.shelfId", fromStoreId));
		}

		if (toStoreId != null && toStoreId > 0) {
			criteria.add(Restrictions.eq("to.shelfId", toStoreId));
		}

		if (typeId != null && typeId > 0) {
			criteria.add(Restrictions.eq("tr_type.lookupDetailId", typeId));
		}

		if (reasonId != null && reasonId > 0) {
			criteria.add(Restrictions.eq("tr_rsn.lookupDetailId", reasonId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("transferDate", fromDate, toDate));
		}
		Set<MaterialTransfer> uniqueRecords = new HashSet<MaterialTransfer>(
				criteria.list());
		List<MaterialTransfer> transferList = new ArrayList<MaterialTransfer>(
				uniqueRecords);
		return transferList;
	}

	@SuppressWarnings("unchecked")
	public List<MaterialTransferDetail> getMaterialTransferByShelfs(
			Implementation implementation, Set<Integer> shelfIds)
			throws Exception {

		Criteria criteria = materialTransferDetailDAO.createCriteria();

		criteria.createAlias("materialTransfer", "tf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("issueReturnDetails", "isrd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tf.personByCreatedBy", "mrcpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tf.personByTransferPerson", "mrtfpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("product", "prd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelfByFromShelfId", "fromslf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("fromslf.shelf", "frack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("frack.aisle", "fasl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("fasl.store", "fstr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelfByShelfId", "toslf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("toslf.shelf", "track",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("track.aisle", "tasl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tasl.store", "tstr",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("tf.implementation", implementation));

		criteria.add(Restrictions.in("toslf.shelfId", shelfIds));
		Set<MaterialTransferDetail> uniqueRecords = new HashSet<MaterialTransferDetail>(
				criteria.list());
		List<MaterialTransferDetail> transferDtList = new ArrayList<MaterialTransferDetail>(
				uniqueRecords);
		return transferDtList;
	}

	// Getters & Setters
	public AIOTechGenericDAO<MaterialTransferDetail> getMaterialTransferDetailDAO() {
		return materialTransferDetailDAO;
	}

	public void setMaterialTransferDetailDAO(
			AIOTechGenericDAO<MaterialTransferDetail> materialTransferDetailDAO) {
		this.materialTransferDetailDAO = materialTransferDetailDAO;
	}

	public AIOTechGenericDAO<MaterialTransfer> getMaterialTransferDAO() {
		return materialTransferDAO;
	}

	public void setMaterialTransferDAO(
			AIOTechGenericDAO<MaterialTransfer> materialTransferDAO) {
		this.materialTransferDAO = materialTransferDAO;
	}
}
