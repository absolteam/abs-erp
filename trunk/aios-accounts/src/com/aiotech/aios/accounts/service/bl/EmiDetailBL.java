package com.aiotech.aios.accounts.service.bl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Eibor;
import com.aiotech.aios.accounts.domain.entity.Emi;
import com.aiotech.aios.accounts.domain.entity.EmiDetail;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.LoanChargeVO;
import com.aiotech.aios.accounts.service.EiborService;
import com.aiotech.aios.accounts.service.EmiDetailService;
import com.aiotech.aios.accounts.service.LoanService;
import com.aiotech.aios.accounts.to.EmiDetailTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.DocumentVO;

public class EmiDetailBL {

	private List<EmiDetail> emiList;
	private List<EmiDetailTO> emiTOList = new ArrayList<EmiDetailTO>();
	private EmiDetailService emiService;
	private LoanService loanService;
	private EmiDetailTO emiTo = new EmiDetailTO();
	private List<LoanCharge> chargeList = new ArrayList<LoanCharge>();
	private int iTotalDisplayRecords;
	private int iTotalRecords;
	private double loanBalance = 0;
	private double emiCumulative = 0;
	private int varianceDays;
	private int yearlyNoDate;
	private double addtionalCharges;
	private Double interestRate;
	private boolean flag;
	private String sheduleBy;
	private Date currentPaymentDate;
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;
	private ImageBL imageBL;
	private TransactionBL transactionBL;
	private EiborService eiborService;

	public JSONObject getEMIlist(Implementation implementation)
			throws Exception {
		List<Emi> emiEntityList = this.getEmiService()
				.getAllEMI(implementation);
		if (emiEntityList != null & !emiEntityList.equals("")) {
			iTotalRecords = emiEntityList.size();
			iTotalDisplayRecords = emiEntityList.size();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Emi list : emiEntityList) {
				JSONArray array = new JSONArray();
				array.add(list.getEmiId());
				array.add(list.getLoan().getLoanNumber());
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(Constants.Accounts.LoanType.get(list.getLoan()
						.getLoanType()));
				array.add(Constants.Accounts.LoanFrequency.get(list.getLoan()
						.getEmiFrequency()));
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			return jsonResponse;
		} else {
			return null;
		}
	}

	private void buildUnpaidEMI(Loan loan) throws Exception {
		if (loan.getEmiType().equals("P")) {
			predictableEMI(loan);
		} else {
			unPredictableEMI(loan);
		}
	}

	public List<EmiDetailTO> buildSchedule(Loan loan) throws Exception {
		if (loan.getEmiType().equals("P")) {
			predictableEMI(loan);
		} else {
			unPredictableEMI(loan);
		}
		return emiTOList;
	}

	private void predictableEMI(Loan loan) throws Exception {
		doTransaction(loan);
	}

	private void unPredictableEMI(Loan loan) throws Exception {
		unPredictableTransaction(loan);
	}

	private void unPredictableTransaction(Loan loan) throws Exception {
		if (loan.getEmiFrequency() == 'M') {
			processUnPredictEMIMonthly(loan);
		} else if (loan.getEmiFrequency() == 'Y') {
			processUnPredictEMIYearly(loan);
		} else if (loan.getEmiFrequency() == 'D') {
			processUnPredictEMIDaily(loan);
		} else if (loan.getEmiFrequency() == 'W') {
			processUnPredictEMIWeekly(loan);
		} else if (loan.getEmiFrequency() == 'H') {
			processUnPredictEMISemiAunnaul(loan);
		} else if (loan.getEmiFrequency() == 'Q') {
			processUnPredictEMIQuaterly(loan);
		}
	}

	private void doTransaction(Loan loan) throws Exception {
		if (loan.getEmiFrequency() == 'M') {
			processEMIMonthly(loan);
		} else if (loan.getEmiFrequency() == 'Y') {
			processEMIYearly(loan);
		} else if (loan.getEmiFrequency() == 'D') {
			processEMIDaily(loan);
		} else if (loan.getEmiFrequency() == 'W') {
			processEMIWeekly(loan);
		} else if (loan.getEmiFrequency() == 'H') {
			processEMISemiAunnaul(loan);
		} else if (loan.getEmiFrequency() == 'Q') {
			processEMIQuaterly(loan);
		}
	}

	private List<EmiDetailTO> processEMIQuaterly(Loan loan) throws Exception {
		int quaterVariance = DateFormat.quaterVariance(
				loan.getEmiFirstDuedate(), loan.getEmiEndDate());
		Date nextQuater = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		emiCumulative = 0D;
		loanBalance = loan.getAmount();
		for (int i = 0; i <= quaterVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextQuater);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextQuater);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			if (null != currentPaymentDate && !currentPaymentDate.equals("")) {
				if (DateFormat.dayVariance(nextQuater, currentPaymentDate) == 0) {
					emiTo.setPaidStatus((byte) 2);
				} else {
					emiTo.setPaidStatus((byte) 0);
				}
			}
			Date currentMonth = nextQuater;
			nextQuater = DateFormat.addMonth(nextQuater);
			emiSetValues(loan, currentMonth, nextQuater);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processEMISemiAunnaul(Loan loan) throws Exception {
		int semiVariance = DateFormat.halfYearlyVariance(
				loan.getEmiFirstDuedate(), loan.getEmiEndDate());
		Date nextSemi = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		emiCumulative = 0D;
		loanBalance = loan.getAmount();
		for (int i = 0; i <= semiVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextSemi);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextSemi);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			if (null != currentPaymentDate && !currentPaymentDate.equals("")) {
				if (DateFormat.dayVariance(nextSemi, currentPaymentDate) == 0) {
					emiTo.setPaidStatus((byte) 2);
				} else {
					emiTo.setPaidStatus((byte) 0);
				}
			}
			Date currentMonth = nextSemi;
			nextSemi = DateFormat.addMonth(nextSemi);
			emiSetValues(loan, currentMonth, nextSemi);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processEMIWeekly(Loan loan) throws Exception {
		int weekVariance = DateFormat.weekVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextWeek = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		emiCumulative = 0D;
		loanBalance = loan.getAmount();
		for (int i = 0; i <= weekVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextWeek);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextWeek);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			if (null != currentPaymentDate && !currentPaymentDate.equals("")) {
				if (DateFormat.dayVariance(nextWeek, currentPaymentDate) == 0) {
					emiTo.setPaidStatus((byte) 2);
				} else {
					emiTo.setPaidStatus((byte) 0);
				}
			}
			Date currentMonth = nextWeek;
			nextWeek = DateFormat.addMonth(nextWeek);
			emiSetValues(loan, currentMonth, nextWeek);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processEMIDaily(Loan loan) throws Exception {
		long dayVariance = DateFormat.dayVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextDay = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		emiCumulative = 0D;
		loanBalance = loan.getAmount();
		for (int i = 0; i <= dayVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextDay);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextDay);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			if (null != currentPaymentDate && !currentPaymentDate.equals("")) {
				if (DateFormat.dayVariance(nextDay, currentPaymentDate) == 0) {
					emiTo.setPaidStatus((byte) 2);
				} else {
					emiTo.setPaidStatus((byte) 0);
				}
			}
			Date currentMonth = nextDay;
			nextDay = DateFormat.addMonth(nextDay);
			emiSetValues(loan, currentMonth, nextDay);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processEMIYearly(Loan loan) throws Exception {
		int yearVariance = DateFormat.yearDifference(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextYear = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		emiCumulative = 0D;
		loanBalance = loan.getAmount();
		for (int i = 0; i <= yearVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextYear);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextYear);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			if (null != currentPaymentDate && !currentPaymentDate.equals("")) {
				if (DateFormat.dayVariance(nextYear, currentPaymentDate) == 0) {
					emiTo.setPaidStatus((byte) 2);
				} else {
					emiTo.setPaidStatus((byte) 0);
				}
			}
			Date currentMonth = nextYear;
			nextYear = DateFormat.addMonth(nextYear);
			emiSetValues(loan, currentMonth, nextYear);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processEMIMonthly(Loan loan) throws Exception {
		int monthVariance = DateFormat.monthsBetween(loan.getEmiEndDate(),
				loan.getEmiFirstDuedate());
		Date nextMonth = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
			loanBalance = loan.getAmount();
		}
		for (int i = 0; i <= monthVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextMonth);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextMonth);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			if (null != currentPaymentDate && !currentPaymentDate.equals("")) {
				if (DateFormat.dayVariance(nextMonth, currentPaymentDate) == 0) {
					emiTo.setPaidStatus((byte) 2);
				} else {
					emiTo.setPaidStatus((byte) 0);
				}
			}
			Date currentMonth = nextMonth;
			nextMonth = DateFormat.addMonth(nextMonth);
			emiSetValues(loan, currentMonth, nextMonth);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processUnPredictEMIMonthly(Loan loan)
			throws Exception {
		int monthVariance = DateFormat.monthsBetween(loan.getEmiEndDate(),
				loan.getEmiFirstDuedate());
		Date nextMonth = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		for (int i = 0; i <= monthVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextMonth);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextMonth);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			nextMonth = DateFormat.addMonth(nextMonth);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processUnPredictEMIYearly(Loan loan)
			throws Exception {
		int yearVariance = DateFormat.yearDifference(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextYear = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		for (int i = 0; i <= yearVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextYear);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextYear);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			nextYear = DateFormat.addMonth(nextYear);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processUnPredictEMIQuaterly(Loan loan)
			throws Exception {
		int quaterVariance = DateFormat.quaterVariance(
				loan.getEmiFirstDuedate(), loan.getEmiEndDate());
		Date nextQuater = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		for (int i = 0; i <= quaterVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextQuater);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextQuater);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			nextQuater = DateFormat.addMonth(nextQuater);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processUnPredictEMISemiAunnaul(Loan loan)
			throws Exception {
		int semiVariance = DateFormat.halfYearlyVariance(
				loan.getEmiFirstDuedate(), loan.getEmiEndDate());
		Date nextSemi = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		for (int i = 0; i <= semiVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextSemi);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextSemi);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			nextSemi = DateFormat.addMonth(nextSemi);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processUnPredictEMIWeekly(Loan loan)
			throws Exception {
		int weekVariance = DateFormat.weekVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextWeek = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		for (int i = 0; i <= weekVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextWeek);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextWeek);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			nextWeek = DateFormat.addMonth(nextWeek);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private List<EmiDetailTO> processUnPredictEMIDaily(Loan loan)
			throws Exception {
		long dayVariance = DateFormat.dayVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextDay = loan.getEmiFirstDuedate();
		if (null == emiTOList || emiTOList.equals("")) {
			emiTOList = new ArrayList<EmiDetailTO>();
		}
		for (int i = 0; i <= dayVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextDay);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextDay);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			nextDay = DateFormat.addMonth(nextDay);
			emiTOList.add(emiTo);
		}
		return emiTOList;
	}

	private double getInterest(double loanBalance, Loan loan,
			Date currenctDate, Date nextInstalment) {
		double maxRate = 0;
		if (loan.getRateType().equalsIgnoreCase("V")
				|| loan.getRateType().equalsIgnoreCase("M")) {
			maxRate = loan.getRate() + loan.getEibor().getEiborRate();
			if (maxRate > loan.getFinalRate()) {
				maxRate = loan.getFinalRate();
			}
		} else {
			maxRate = loan.getFinalRate();
		}
		getPeriods(currenctDate, nextInstalment);
		return ((loanBalance * (maxRate / 100)) / yearlyNoDate * (varianceDays));
	}

	private double getLoanInsurance(double loanBalance, Date currenctDate,
			Date nextInstalment) {
		double interestRate = 0d;
		for (LoanCharge list : chargeList) {
			if (list.getType() == 'I') {
				interestRate += list.getValue();
			}
		}
		getPeriods(currenctDate, nextInstalment);
		return ((loanBalance * (interestRate / 100)) / yearlyNoDate * (varianceDays));
	}

	private double getEibor(Loan loan, Date currenctDate, Date nextInstalment,
			double loanBalance) throws Exception {
		getPeriods(currenctDate, nextInstalment);
		Eibor eibor = getEiborService().getEiborById(
				loan.getEibor().getEiborId());
		return ((loanBalance * (eibor.getEiborRate() / 100)) / yearlyNoDate * (varianceDays));
	}

	private void getPeriods(Date currenctDate, Date nextInstalment) {
		varianceDays = (int) DateFormat.dayVariance(currenctDate,
				nextInstalment);
		GregorianCalendar calendar = new GregorianCalendar();
		Calendar cal = Calendar.getInstance();
		boolean leapFlag = calendar.isLeapYear(cal.get(Calendar.YEAR));
		if (leapFlag)
			yearlyNoDate = 366;
		else
			yearlyNoDate = 365;
	}

	private double roundValue(double amount) {
		final int precision = 2;
		BigDecimal returnValue = new BigDecimal(amount).setScale(precision,
				BigDecimal.ROUND_HALF_UP);
		return returnValue.doubleValue();
	}

	private double getInstallment(Loan loan) throws Exception {
		double loanInterest = loan.getRate();
		double interestRate = 0d;
		for (LoanCharge list : chargeList) {
			if (list.getType() == 'A') {
				// interestAmount+=list.getValue();
			} else {
				interestRate += list.getValue();
			}
		}
		loanInterest = (loanInterest + interestRate);
		loanInterest /= 100;

		int yearVariance = 0;
		yearVariance = DateFormat.yearDifference(loan.getEmiStartDate(),
				loan.getEmiEndDate());
		if (yearVariance == 0)
			yearVariance = 1;
		return (loanInterest * loan.getAmount() / 12)
				/ (1.0 - Math.pow(((loanInterest / 12) + 1.0),
						(-(12 * yearVariance))));
	}

	public void emiSetValues(Loan loan, Date currentDate, Date nextInstalment)
			throws Exception {
		if ((loan.getRateVariance() == 'F' || loan.getRateVariance() == 'D')
				&& loan.getRateType().equalsIgnoreCase("F")) {
			getInterestFixed(loan, currentDate, nextInstalment);
		} else if ((loan.getRateVariance() == 'F' || loan.getRateVariance() == 'D')
				&& loan.getRateType().equalsIgnoreCase("V")) {
			getInterestVary(loan, currentDate, nextInstalment);
		} else if ((loan.getRateVariance() == 'F' || loan.getRateVariance() == 'D')
				&& loan.getRateType().equalsIgnoreCase("M")) {
			getInterestMixed(loan, currentDate, nextInstalment);
		}
		emiTo.setInstalment(roundValue(getInstallment(loan)));
		emiTo.setLoanCharges(getLoanCharges(loan, currentDate, nextInstalment));
		double loanCharges = 0d;
		for (LoanChargeVO list : emiTo.getLoanCharges()) {
			loanCharges += list.getValue();
		}
		double emiDepreciation = emiTo.getInstalment()
				- (emiTo.getInterest() + emiTo.getEiborAmount() + loanCharges);
		emiTo.setEmiDepreciation(roundValue(emiDepreciation));
		emiCumulative += emiTo.getEmiDepreciation();
		emiTo.setEmiCumulative(roundValue(emiCumulative));
		loanBalance = loanBalance - emiDepreciation;
		emiTo.setAmount(roundValue(loanBalance));
	}

	private List<LoanChargeVO> getLoanCharges(Loan loan, Date currentDate,
			Date nextInstalment) {
		List<LoanChargeVO> chargesVO = new ArrayList<LoanChargeVO>();
		for (LoanCharge list : chargeList) {
			chargesVO.add(convertInterestRate(list, currentDate,
					nextInstalment, loan));
		}
		return chargesVO;
	}

	private LoanChargeVO convertInterestRate(LoanCharge charge,
			Date currentDate, Date nextInstalment, Loan loan) {
		LoanChargeVO chargeVO = new LoanChargeVO();
		double interestRate = 0d;
		getPeriods(currentDate, nextInstalment);
		double amount = loanBalance;
		if (loan.getRateVariance() == 'F')
			amount = loan.getAmount();
		interestRate = (amount * (charge.getValue() / 100)) / yearlyNoDate
				* (varianceDays);
		chargeVO.setValue(interestRate);
		return chargeVO;
	}

	private void getInterestMixed(Loan loan, Date currentDate,
			Date nextInstalment) throws Exception {
		if (loan.getRateVariance() == ('D')) {
			emiTo.setInterest(roundValue(getInterest(loanBalance, loan,
					currentDate, nextInstalment)));
			if (null != chargeList && chargeList.size() > 0)
				emiTo.setLoanCharge(roundValue(getLoanInsurance(loanBalance,
						currentDate, nextInstalment)));
			else
				emiTo.setLoanCharge(0d);
			if (null != loan.getEibor() && !loan.getEibor().equals(""))
				emiTo.setEiborAmount(roundValue(getEibor(loan, currentDate,
						nextInstalment, loanBalance)));
			else
				emiTo.setEiborAmount(0d);
		} else {
			emiTo.setInterest(roundValue(getInterest(loan.getAmount(), loan,
					currentDate, nextInstalment)));
			if (null != chargeList && chargeList.size() > 0)
				emiTo.setLoanCharge(roundValue(getLoanInsurance(loanBalance,
						currentDate, nextInstalment)));
			else
				emiTo.setLoanCharge(0d);
			if (null != loan.getEibor() && !loan.getEibor().equals(""))
				emiTo.setEiborAmount(roundValue(getEibor(loan, currentDate,
						nextInstalment, loan.getAmount())));
			else
				emiTo.setEiborAmount(0d);
		}
	}

	private void getInterestVary(Loan loan, Date currentDate,
			Date nextInstalment) throws Exception {
		if (loan.getRateVariance() == ('D')) {
			emiTo.setInterest(roundValue(getInterest(loanBalance, loan,
					currentDate, nextInstalment)));
			if (null != chargeList && chargeList.size() > 0)
				emiTo.setLoanCharge(roundValue(getLoanInsurance(loanBalance,
						currentDate, nextInstalment)));
			else
				emiTo.setLoanCharge(0d);
			if (null != loan.getEibor() && !loan.getEibor().equals(""))
				emiTo.setEiborAmount(roundValue(getEibor(loan, currentDate,
						nextInstalment, loanBalance)));
			else
				emiTo.setEiborAmount(0d);
		} else {
			emiTo.setInterest(roundValue(getInterest(loan.getAmount(), loan,
					currentDate, nextInstalment)));
			if (null != chargeList && chargeList.size() > 0)
				emiTo.setLoanCharge(roundValue(getLoanInsurance(
						loan.getAmount(), currentDate, nextInstalment)));
			else
				emiTo.setLoanCharge(0d);
			if (null != loan.getEibor() && !loan.getEibor().equals(""))
				emiTo.setEiborAmount(roundValue(getEibor(loan, currentDate,
						nextInstalment, loan.getAmount())));
			else
				emiTo.setEiborAmount(0d);
		}
	}

	private void getInterestFixed(Loan loan, Date currentDate,
			Date nextInstalment) throws Exception {
		if (loan.getRateVariance() == ('D')) {
			emiTo.setInterest(roundValue(getInterest(loanBalance, loan,
					currentDate, nextInstalment)));
			if (null != chargeList && chargeList.size() > 0)
				emiTo.setLoanCharge(roundValue(getLoanInsurance(loanBalance,
						currentDate, nextInstalment)));
			else
				emiTo.setLoanCharge(0d);
			if (null != loan.getEibor() && !loan.getEibor().equals(""))
				emiTo.setEiborAmount(roundValue(getEibor(loan, currentDate,
						nextInstalment, loanBalance)));
			else
				emiTo.setEiborAmount(0d);
		} else {
			emiTo.setInterest(roundValue(getInterest(loan.getAmount(), loan,
					currentDate, nextInstalment)));
			if (null != chargeList && chargeList.size() > 0)
				emiTo.setLoanCharge(roundValue(getLoanInsurance(
						loan.getAmount(), currentDate, nextInstalment)));
			else
				emiTo.setLoanCharge(0d);
			if (null != loan.getEibor() && !loan.getEibor().equals(""))
				emiTo.setEiborAmount(roundValue(getEibor(loan, currentDate,
						nextInstalment, loan.getAmount())));
			else
				emiTo.setEiborAmount(0d);
		}
	}

	public List<EmiDetailTO> buildWithEMI(Loan loan,
			List<EmiDetailTO> detailList) throws Exception {
		emiTOList = new ArrayList<EmiDetailTO>();
		loanBalance = loan.getAmount();
		emiTOList.addAll(detailList);
		int i = 1;
		int emi = 0;
		if (null != sheduleBy && sheduleBy.equalsIgnoreCase("S")) {
			for (EmiDetailTO list : detailList) {
				list.setLineNumber(i++);
				Date datevalue = DateFormat.convertStringToDate(list
						.getEmiDueDate());
				loan.setEmiFirstDuedate(DateFormat.addMonth(datevalue));
				list.setPaidStatus((byte) 1);
				emi = list.getEmiPaymentList().size();
				loanBalance = list.getEmiPaymentList().get(--emi).getAmount();
			}
			buildUnpaidEMI(loan);
		} else {
			for (EmiDetailTO list : detailList) {
				list.setLineNumber(i++);
			}
		}
		return emiTOList;
	}

	public List<EmiDetailTO> calculateSchedule(List<EmiDetailTO> detailList,
			Loan loan) {
		emiCumulative = 0d;
		loanBalance = loan.getAmount();
		for (EmiDetailTO list : detailList) {
			double emiDepreciation = list.getInstalment()
					- (list.getInterest() + list.getEiborAmount());
			list.setEmiDepreciation(roundValue(emiDepreciation));
			emiCumulative += list.getEmiDepreciation();
			list.setEmiCumulative(roundValue(emiCumulative));
			loanBalance = loanBalance - emiDepreciation;
			list.setAmount(roundValue(loanBalance));
		}
		return detailList;
	}

	public List<EmiDetailTO> convertEntity(List<Emi> detailList)
			throws Exception {
		List<EmiDetailTO> emiList = new ArrayList<EmiDetailTO>();
		emiCumulative = 0;
		loanBalance = detailList.get(0).getLoan().getAmount();
		for (Emi list : detailList) {
			emiList.add(convertToEntity(list));
		}
		return emiList;
	}

	public List<EmiDetailTO> convertListEntity(List<Emi> detailList)
			throws Exception {
		List<EmiDetailTO> emiList = new ArrayList<EmiDetailTO>();
		emiCumulative = 0;
		loanBalance = detailList.get(0).getLoan().getAmount();
		for (Emi list : detailList) {
			if (null != list.getStatus() && list.getStatus() == true)
				emiList.add(convertToEntity(list));
		}
		return emiList;
	}

	private EmiDetailTO convertToEntity(Emi detail) throws Exception {
		EmiDetailTO emiTo = new EmiDetailTO();
		EmiDetailTO emiTopay = new EmiDetailTO();
		emiTo.setEmiDueDate(DateFormat.convertDateToString(detail.getDate()
				.toString()));
		List<EmiDetailTO> paymentsList = new ArrayList<EmiDetailTO>();
		List<EmiDetail> emiPaymentDetail = emiService.getEMIPayments(detail
				.getEmiId());
		for (EmiDetail payement : emiPaymentDetail) {
			emiTopay = new EmiDetailTO();
			emiTopay.setName(payement.getName());
			emiTopay.setAmount(payement.getAmount());
			paymentsList.add(emiTopay);
			emiTo.setEmiPaymentList(paymentsList);
		}
		emiTo.setDescription(detail.getDescription());
		emiTo.setStatus(detail.getStatus());
		return emiTo;
	}

	public void saveEMIDetails(Emi emi, List<EmiDetail> detailList)
			throws Exception {
		this.getEmiService().saveEMI(emi, detailList);
		List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
		Loan loan = this.getLoanService()
				.getLoanById(emi.getLoan().getLoanId());
		TransactionDetail transactionDetail = null;
		Transaction transaction = null;
		List<Category> categoryList = this.getTransactionBL().getCategoryBL()
				.getCategoryService()
				.getAllActiveCategories(loan.getImplementation());
		Category category = new Category();
		if (null != categoryList) {
			for (Category list : categoryList) {
				if (list.getName().equalsIgnoreCase("EMI Payment")) {
					category.setCategoryId(list.getCategoryId());
				}
			}
		}
		if (null == category || category.equals("")) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(loan.getImplementation(), "EMI Payment");
		} else if (null == category.getCategoryId()) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(loan.getImplementation(), "EMI Payment");
		}
		for (EmiDetail list : detailList) {
			if (list.getCombination() != null) {
				transactionDetailList = new ArrayList<TransactionDetail>();
				transactionDetail = transactionBL.createTransactionDetail(list
						.getAmount(), list.getCombination().getCombinationId(),
						false, "EMI Debit Payments", null, null);
				transactionDetailList.add(transactionDetail);
				transactionDetail = transactionBL.createTransactionDetail(
						list.getAmount(), loan.getBankAccount()
								.getCombination().getCombinationId(), true,
						"EMI Credit Payments", null, null);
				transactionDetailList.add(transactionDetail);
				transaction = transactionBL.createTransaction(loan
						.getCurrency().getCurrencyId(), "EMI Payments",
						new Date(), category, emi.getEmiId(), Emi.class
								.getSimpleName());
				transactionBL.saveTransaction(transaction,
						transactionDetailList);
			}
		}
	}

	public void saveEMIDocuments(Emi emi, boolean editFlag) throws Exception {

		DocumentVO docVO = documentBL.populateDocumentVO(emi, "personDocs",
				editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, emi.getEmiId(), "Person", "personDocs");
			docVO.setDirStruct(dirStucture);
		}
		documentBL.saveUploadDocuments(docVO);
	}

	public void saveEMIImages(Emi emi, boolean editFlag) throws Exception {
		DocumentVO docVO2 = new DocumentVO();
		docVO2.setDisplayPane("personImages");
		docVO2.setEdit(editFlag);
		docVO2.setObject(emi);
		imageBL.saveUploadedImages(emi, docVO2);
	}

	public List<EmiDetail> getEmiList() {
		return emiList;
	}

	public void setEmiList(List<EmiDetail> emiList) {
		this.emiList = emiList;
	}

	public EmiDetailService getEmiService() {
		return emiService;
	}

	public void setEmiService(EmiDetailService emiService) {
		this.emiService = emiService;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public List<EmiDetailTO> getEmiTOList() {
		return emiTOList;
	}

	public void setEmiTOList(List<EmiDetailTO> emiTOList) {
		this.emiTOList = emiTOList;
	}

	public double getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(double loanBalance) {
		this.loanBalance = loanBalance;
	}

	public double getEmiCumulative() {
		return emiCumulative;
	}

	public void setEmiCumulative(double emiCumulative) {
		this.emiCumulative = emiCumulative;
	}

	public EmiDetailTO getEmiTo() {
		return emiTo;
	}

	public void setEmiTo(EmiDetailTO emiTo) {
		this.emiTo = emiTo;
	}

	public int getVarianceDays() {
		return varianceDays;
	}

	public void setVarianceDays(int varianceDays) {
		this.varianceDays = varianceDays;
	}

	public int getYearlyNoDate() {
		return yearlyNoDate;
	}

	public void setYearlyNoDate(int yearlyNoDate) {
		this.yearlyNoDate = yearlyNoDate;
	}

	public double getAddtionalCharges() {
		return addtionalCharges;
	}

	public void setAddtionalCharges(double addtionalCharges) {
		this.addtionalCharges = addtionalCharges;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public String getSheduleBy() {
		return sheduleBy;
	}

	public void setSheduleBy(String sheduleBy) {
		this.sheduleBy = sheduleBy;
	}

	public List<LoanCharge> getChargeList() {
		return chargeList;
	}

	public void setChargeList(List<LoanCharge> chargeList) {
		this.chargeList = chargeList;
	}

	public Date getCurrentPaymentDate() {
		return currentPaymentDate;
	}

	public void setCurrentPaymentDate(Date currentPaymentDate) {
		this.currentPaymentDate = currentPaymentDate;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public LoanService getLoanService() {
		return loanService;
	}

	public void setLoanService(LoanService loanService) {
		this.loanService = loanService;
	}

	public EiborService getEiborService() {
		return eiborService;
	}

	public void setEiborService(EiborService eiborService) {
		this.eiborService = eiborService;
	}
}
