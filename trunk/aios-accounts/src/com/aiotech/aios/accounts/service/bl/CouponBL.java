package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.domain.entity.vo.CouponVO;
import com.aiotech.aios.accounts.service.CouponService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CouponBL {

	private CouponService couponService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Coupons
	public List<Object> showAllCoupons() throws Exception {
		List<Coupon> coupons = couponService.getAllCoupons(getImplementation());
		List<Object> couponVOs = new ArrayList<Object>();
		if (null != coupons && coupons.size() > 0)
			for (Coupon coupon : coupons)
				couponVOs.add(addCoupontVO(coupon));
		return couponVOs;
	}

	// Show All Active Coupons
	public List<Object> showAllPopupCoupon() throws Exception {
		List<Coupon> coupons = couponService.getAllActiveCoupons(
				getImplementation(), Calendar.getInstance().getTime());
		List<Object> couponVOs = new ArrayList<Object>();
		if (null != coupons && coupons.size() > 0)
			for (Coupon coupon : coupons)
				couponVOs.add(addCoupontVO(coupon));
		return couponVOs;
	}

	// Add Coupon VO
	public CouponVO addCoupontVO(Coupon coupon) {
		CouponVO couponVO = new CouponVO();
		couponVO.setCouponId(coupon.getCouponId());
		couponVO.setCouponName(coupon.getCouponName());
		couponVO.setStartDigit(coupon.getStartDigit());
		couponVO.setEndDigit(coupon.getEndDigit());
		couponVO.setValidFrom(DateFormat.convertDateToString(coupon
				.getFromDate().toString()));
		couponVO.setValidTo(DateFormat.convertDateToString(coupon.getToDate()
				.toString()));
		couponVO.setCouponType(coupon.getCouponType());
		couponVO.setCouponTypeName(Constants.Accounts.CouponType
				.get(coupon.getCouponType()).name().replaceAll("_", " "));
		couponVO.setStatus(coupon.getStatus());
		couponVO.setStatusValue(coupon.getStatus() ? "Active" : "Inactive");
		couponVO.setDescription(coupon.getDescription());
		return couponVO;
	}

	// Gte Coupon By Coupon Number
	public Coupon getCouponByCouponNumber(int couponNumber) throws Exception {
		return couponService.getCouponByCouponNumber(couponNumber, Calendar
				.getInstance().getTime(), Calendar.getInstance().getTime(),
				this.getImplementation());
	}

	// Save Coupon
	public void saveCoupon(Coupon coupon) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		coupon.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (coupon != null && coupon.getCouponId() != null
				&& coupon.getCouponId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		coupon.setImplementation(getImplementation());
		couponService.saveCoupon(coupon);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Coupon.class.getSimpleName(), coupon.getCouponId(), user,
				workflowDetailVo);
	}

	public void doCouponBusinessUpdate(Long couponId,
			WorkflowDetailVO workflowDetailVO) {
		try { 
			if (workflowDetailVO.isDeleteFlag()) {
				Coupon coupon = couponService.getCouponByCouponId(
						couponId);
				couponService.deleteCoupon(coupon);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Delete Coupon
	public void deleteCoupon(Coupon coupon) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						Coupon.class.getSimpleName(), coupon.getCouponId(),
						user, workflowDetailVo);  
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public CouponService getCouponService() {
		return couponService;
	}

	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
