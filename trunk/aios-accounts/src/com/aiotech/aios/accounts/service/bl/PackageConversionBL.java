package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.ProductPackage;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.common.util.AIOSCommons;

public class PackageConversionBL {

	private ProductPackageBL productPackageBL;
	private ProductBL productBL;

	public Stock stockUnitConversion(Stock stock,
			ProductPackageDetail productPackageDetail) throws Exception {
		if (null == productPackageDetail || stock.getQuantity() <= 0)
			return stock;
		productPackageDetail = productPackageBL.getProductPackageService()
				.getProductPackageDetailsByDetailId(
						productPackageDetail.getProductPackageDetailId());
		if ((long) productPackageDetail.getLookupDetail().getLookupDetailId() == (long) productPackageDetail
				.getProductPackage().getProduct()
				.getLookupDetailByProductUnit().getLookupDetailId())
			return stock;

		stock.setQuantity(stock.getQuantity()
				/ productPackageDetail.getUnitQuantity());
		return stock;
	}

	public ProductPackageDetailVO getBaseConversionUnit(
			long productPackageDetailId, double packageQuantity,
			double basePrice) throws Exception {
		ProductPackageDetailVO productPackageDetailVO = new ProductPackageDetailVO();
		ProductPackageDetail productPackageDetail = productPackageBL
				.getProductPackageService().getProductPackageDetailsByDetailId(
						productPackageDetailId);
		productPackageDetailVO.setConversionQuantity(AIOSCommons
				.convertExponential(packageQuantity
						/ productPackageDetail.getUnitQuantity()));
		productPackageDetailVO.setConversionUnitName(productPackageDetail
				.getProductPackage().getProduct()
				.getLookupDetailByProductUnit().getDisplayName());
		productPackageDetailVO.setConversionUnitMeasure(productPackageDetail
				.getLookupDetail().getDisplayName());
		productPackageDetailVO
				.setBasePrice(basePrice > 0 ? ((basePrice * 1) / productPackageDetail
						.getUnitQuantity()) : 0);
		productPackageDetailVO.setUnitQuantity(productPackageDetail
				.getUnitQuantity());
		if (Double.parseDouble(productPackageDetailVO.getConversionQuantity()) >= 1) {
			productPackageDetailVO.setConversionQuantity(AIOSCommons
					.roundThreeDecimalsStr(Double
							.parseDouble(productPackageDetailVO
									.getConversionQuantity())));
			String cvQty[] = productPackageDetailVO.getConversionQuantity()
					.split("\\.");
			if (cvQty.length == 1)
				productPackageDetailVO
						.setConversionQuantity(productPackageDetailVO
								.getConversionQuantity() + ".00");
		}
		return productPackageDetailVO;
	}

	public ProductPackageDetailVO getPackageConversionUnit(
			long productPackageDetailId, double packageQuantity)
			throws Exception {
		ProductPackageDetailVO productPackageDetailVO = new ProductPackageDetailVO();
		ProductPackageDetail productPackageDetail = productPackageBL
				.getProductPackageService().getProductPackageDetailsByDetailId(
						productPackageDetailId);
		productPackageDetailVO.setConversionQuantity(String
				.valueOf(packageQuantity
						* productPackageDetail.getUnitQuantity()));
		return productPackageDetailVO;
	}

	public Double getPackageConversionQuantity(long productPackageDetailId,
			double packageQuantity) throws Exception {
		ProductPackageDetail productPackageDetail = productPackageBL
				.getProductPackageService().getProductPackageDetailsByDetailId(
						productPackageDetailId);
		return packageQuantity * productPackageDetail.getUnitQuantity();
	}

	public Double getPackageBaseQuantity(long productPackageDetailId,
			double packageQuantity) throws Exception {
		ProductPackageDetail productPackageDetail = productPackageBL
				.getProductPackageService().getProductPackageDetailsByDetailId(
						productPackageDetailId);
		return packageQuantity / productPackageDetail.getUnitQuantity();
	}

	public List<ProductPackageVO> getProductPackagingDetail(long productId)
			throws Exception {
		List<ProductPackage> productPackages = productPackageBL
				.getProductPackageService().getProductPackagingsByProduct(
						productId);
		return createProductPackaging(productPackages, productId);
	}

	public List<ProductPackageDetail> getProductPackageDetails(long productId)
			throws Exception {
		List<ProductPackage> productPackages = productPackageBL
				.getProductPackageService().getProductPackagingsByProduct(
						productId);
		List<ProductPackageDetail> productPackageDetails = null;
		if (null != productPackages && productPackages.size() > 0) {
			productPackageDetails = new ArrayList<ProductPackageDetail>();
			for (ProductPackage productPackage : productPackages) {
				for (ProductPackageDetail packageDetail : productPackage
						.getProductPackageDetails())
					productPackageDetails.add(packageDetail);
			}
		}
		return productPackageDetails;
	}

	private List<ProductPackageVO> createProductPackaging(
			List<ProductPackage> productPackages, long productId)
			throws Exception {
		List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
		ProductPackageVO productPackageVO = null;
		ProductPackageDetailVO productPackageDetailVO = null;
		List<ProductPackageDetailVO> productPackageDetailVOs = null;
		if (null != productPackages && productPackages.size() > 0) {
			for (ProductPackage productPackage : productPackages) {
				productPackageVO = new ProductPackageVO();
				productPackageVO.setProductPackageId(productPackage
						.getProductPackageId());
				productPackageVO
						.setPackageName(productPackage.getPackageName());
				productPackageDetailVOs = new ArrayList<ProductPackageDetailVO>();
				for (ProductPackageDetail packageDetail : productPackage
						.getProductPackageDetails()) {
					productPackageDetailVO = new ProductPackageDetailVO();
					productPackageDetailVO
							.setProductPackageDetailId(packageDetail
									.getProductPackageDetailId());
					productPackageDetailVO.setConversionUnitName(packageDetail
							.getLookupDetail().getDisplayName());
					productPackageDetailVOs.add(productPackageDetailVO);
				}
				Collections.sort(productPackageDetailVOs,
						new Comparator<ProductPackageDetailVO>() {
							public int compare(ProductPackageDetailVO o1,
									ProductPackageDetailVO o2) {
								return o1.getProductPackageDetailId()
										.compareTo(
												o2.getProductPackageDetailId());
							}
						});
				productPackageVO
						.setProductPackageDetailVOs(productPackageDetailVOs);
				productPackageVOs.add(productPackageVO);
			}
		}
		if (null != productPackageVOs && productPackageVOs.size() > 0) {
			Collections.sort(productPackageVOs,
					new Comparator<ProductPackageVO>() {
						public int compare(ProductPackageVO o1,
								ProductPackageVO o2) {
							return o1.getProductPackageId().compareTo(
									o2.getProductPackageId());
						}
					});
		}
		return productPackageVOs;
	}

	public ProductPackageBL getProductPackageBL() {
		return productPackageBL;
	}

	public void setProductPackageBL(ProductPackageBL productPackageBL) {
		this.productPackageBL = productPackageBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}
}
