package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.ProductSplit;
import com.aiotech.aios.accounts.domain.entity.ProductSplitDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductSplitVO;
import com.aiotech.aios.accounts.service.ProductSplitService;
import com.aiotech.aios.common.to.Constants.Accounts;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class ProductSplitBL {
	private ProductSplitService productSplitService;
	private StoreBL storeBL;
	private StockBL stockBL;
	private ProductBL productBL;
	private TransactionBL transactionBL;

	public List<Object> showAllProductSplit() throws Exception {
		List<ProductSplit> productionSplits = productSplitService
				.getAllProductSplit(getImplementation());
		List<Object> productionSplitVOs = new ArrayList<Object>();
		if (null != productionSplits && productionSplits.size() > 0) {
			for (ProductSplit productSplit : productionSplits)
				productionSplitVOs.add(addProductSplitVO(productSplit));
		}
		return productionSplitVOs;
	}

	private ProductSplitVO addProductSplitVO(ProductSplit productSplit)
			throws Exception {
		ProductSplitVO productSplitVO = new ProductSplitVO();
		productSplitVO.setProductSplitId(productSplit.getProductSplitId());
		productSplitVO.setCreatedDateView(DateFormat
				.convertDateToString(productSplit.getCreatedDate().toString()));
		productSplitVO.setReferenceNumber(productSplit.getReferenceNumber());
		productSplitVO.setSplitedPerson(productSplit.getPerson().getFirstName()
				.concat(" ").concat(productSplit.getPerson().getLastName()));

		productSplitVO.setProductName(productSplit.getProduct()
				.getProductName());
		productSplitVO.setProductCode(productSplit.getProduct().getCode());
		productSplitVO.setStoreName(productSplit.getShelf().getShelf().getAisle().getStore()
				.getStoreName()
				+ " >> "
				+ productSplit.getShelf().getShelf().getAisle().getSectionName()
				+ " >> "
				+ productSplit.getShelf().getShelf().getName()
				+ " >> "
				+ productSplit.getShelf().getName());
		productSplitVO.setDescription(productSplit.getDescription());
		return productSplitVO;
	}

	public ProductSplitVO convertProductSplitToVO(ProductSplit productSplit) {
		ProductSplitVO productSplitVO = new ProductSplitVO();
		try {
			BeanUtils.copyProperties(productSplitVO, productSplit);
			productSplitVO.setProductSplitId(productSplit.getProductSplitId());
			productSplitVO.setCreatedDateView(DateFormat
					.convertDateToString(productSplit.getCreatedDate()
							.toString()));
			productSplitVO
					.setReferenceNumber(productSplit.getReferenceNumber());
			productSplitVO.setSplitedPerson(productSplit.getPerson()
					.getFirstName().concat(" ")
					.concat(productSplit.getPerson().getLastName()));

			productSplitVO.setProductName(productSplit.getProduct()
					.getProductName());
			productSplitVO.setStoreName(productSplit.getShelf().getShelf().getAisle().getStore()
					.getStoreName()
					+ " >> "
					+ productSplit.getShelf().getShelf().getAisle().getSectionName()
					+ " >> "
					+ productSplit.getShelf().getShelf().getName()
					+ " >> "
					+ productSplit.getShelf().getName());
			productSplitVO.setProductCode(productSplit.getProduct().getCode());
			productSplitVO.setDescription(productSplit.getDescription());
			productSplitVO
					.setSplitDetailVO(convertProductSplitToVO(new ArrayList<ProductSplitDetail>(
							productSplit.getProductSplitDetails())));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return productSplitVO;
	}

	public List<ProductSplitVO> convertProductSplitToVO(
			List<ProductSplitDetail> splitDetails) {
		List<ProductSplitVO> productSplitVOs = null;
		ProductSplitVO productSplitVO = null;
		try {
			if (splitDetails != null && splitDetails.size() > 0) {
				productSplitVOs = new ArrayList<ProductSplitVO>();
				for (ProductSplitDetail detail : splitDetails) {
					productSplitVO = new ProductSplitVO();
					productSplitVO.setProductSplitDetail(detail);

					productSplitVO.setProductName(detail.getProduct()
							.getProductName());
					productSplitVO.setStoreName(detail.getShelf().getShelf().getAisle().getStore()
							.getStoreName()
							+ " >> "
							+ detail.getShelf().getShelf().getAisle().getSectionName()
							+ " >> "
							+ detail.getShelf().getShelf().getName()
							+ " >> "
							+ detail.getShelf().getName());
					productSplitVO
							.setProductCode(detail.getProduct().getCode());
					productSplitVO.setDescription(detail.getDescription());
					productSplitVO.setShelfName(detail.getShelf().getName());
					productSplitVOs.add(productSplitVO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return productSplitVOs;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Save Project
	public void saveProductSplit(ProductSplit productSplit,
			List<ProductSplitDetail> inventories,
			List<ProductSplitDetail> deleteInventories,
			List<ProductSplitDetail> editInventories,
			ProductSplit editSplit) throws Exception {

		boolean modify = null == productSplit.getProductSplitId() ? false
				: true;
		productSplit.setImplementation(getImplementation());
		// Update stock information
		List<Stock> stocks = null;
		List<Stock> deletestocks = new ArrayList<Stock>();
		Stock stock = null;
		if (!modify)
			SystemBL.saveReferenceStamp(ProductSplit.class.getName(),
					getImplementation());
		else {
			//Increase Stock with parent product
			stock = new Stock();
			stock.setShelf(editSplit.getShelf());
			stock.setProduct(editSplit.getProduct());
			stock.setQuantity(1.0);
			stock.setStore(editSplit.getShelf().getShelf().getAisle().getStore());
			stock.setImplementation(getImplementation());
			stocks = new ArrayList<Stock>();
			stocks.add(stock);
			// Decrease Stock by details
			for (ProductSplitDetail projectInventory : editInventories) {

				stock = new Stock();
				stock.setStore(projectInventory.getShelf().getShelf().getAisle().getStore());
				stock.setShelf(projectInventory.getShelf());
				stock.setProduct(projectInventory.getProduct());
				stock.setUnitRate(projectInventory.getUnitRate());
				stock.setQuantity(1.0);
				stock.setImplementation(getImplementation());
				deletestocks.add(stock);
			}
			stockBL.updateStockDetails(stocks, deletestocks);
			// Delete JV
			transactionBL.deleteTransaction(productSplit.getProductSplitId(),
					ProductSplit.class.getSimpleName());
			
			productSplitService.deleteProductSplitDetail(deleteInventories);

		}
		productSplitService.saveProductSplit(productSplit, inventories);

		stocks = new ArrayList<Stock>();
		deletestocks = new ArrayList<Stock>();
		// Parent
		stock = new Stock();
		stock.setStore(productSplit.getShelf().getShelf().getAisle().getStore());
		stock.setShelf(productSplit.getShelf());
		stock.setProduct(productSplit.getProduct());
		stock.setQuantity(1.0);
		stock.setImplementation(getImplementation());
		deletestocks.add(stock);
		// Child
		for (ProductSplitDetail projectInventory : inventories) {

			stock = new Stock();
			stock.setStore(projectInventory.getStore());
			stock.setProduct(projectInventory.getProduct());
			stock.setUnitRate(projectInventory.getUnitRate());
			stock.setQuantity(1.0);
			stock.setShelf(projectInventory.getShelf());
			stock.setImplementation(getImplementation());
 			stocks.add(stock);
		}
		stockBL.updateStockDetails(stocks, deletestocks);

		// JV Transaction
		performeJVTransaction(productSplit, inventories);
	}

	public void performeJVTransaction(ProductSplit productSplit,
			List<ProductSplitDetail> inventories) throws Exception {
		Combination splitCombination = findCombinationInfo(getImplementation()
				.getCompanyName() + "-INVENTORY-SPLIT");
		Combination productsp = productBL.getProductInventory(productSplit
				.getProduct().getProductId());
		// Transaction
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double total = 0.0;
		for (ProductSplitDetail detail : inventories) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					detail.getQuantity() * detail.getUnitRate(),
					productBL.getProductInventory(
							detail.getProduct().getProductId())
							.getCombinationId(), TransactionType.Debit
							.getCode(), "", null, null));
			total += detail.getUnitRate();
		}
		
		// First Transaction with parent information
		List<TransactionDetail> transactionDetailsParet = new ArrayList<TransactionDetail>();
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "", new Date(), transactionBL
				.getCategory("Prodcut Split"), productSplit
				.getProductSplitId(), ProductSplit.class.getSimpleName());

		transactionDetailsParet.add(transactionBL.createTransactionDetail(total,
				splitCombination.getCombinationId(),
				TransactionType.Debit.getCode(), null, null, null));

		transactionDetailsParet.add(transactionBL.createTransactionDetail(total,
				productsp.getCombinationId(), TransactionType.Credit.getCode(),
				null, null, null));
		transactionBL.saveTransaction(transaction, transactionDetailsParet);

		// Second Transaction with child information
		transactionDetails.add(transactionBL.createTransactionDetail(total,
				splitCombination.getCombinationId(),
				TransactionType.Credit.getCode(), null, null, null));

		 transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "", new Date(), transactionBL
				.getCategory("Prodcut Split Details"), productSplit
				.getProductSplitId(), ProductSplit.class.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);

		
	}

	public Combination findCombinationInfo(String currentName) throws Exception {
		Combination combination = null;
		Account account = productBL.getCombinationBL().getCombinationService()
				.getAccountDetail(currentName, getImplementation());
		if (account != null) {
			combination = productBL.getCombinationBL().getCombinationService()
					.getCombinationByNaturalAccount(account.getAccountId());
		} else {
			combination = productBL
					.getCombinationBL()
					.getCombinationService()
					.getCombinationAllAccountDetail(
							getImplementation().getExpenseAccount());
			combination = productBL
					.getCombinationBL()
					.getCombinationService()
					.getCombinationByCostCenter(
							combination.getAccountByCostcenterAccountId()
									.getAccountId());
			combination = productBL.getCombinationBL().createNaturalAccount(
					combination, currentName,
					Accounts.AccountType.Expenses.getCode(), true);
		}
		return combination;
	}

	public boolean deleteProductSplit(ProductSplit productSplit) {
		boolean returnValue = true;
		try {
			// Delete Transaction
			transactionBL.deleteTransaction(productSplit.getProductSplitId(),
					ProductSplit.class.getSimpleName());
			
			List<Stock> stocks = null;
			List<Stock> deletestocks = new ArrayList<Stock>();
			// Delete Stock
			//Increase Stock with parent product
			Stock stock = new Stock();
			stock.setShelf(productSplit.getShelf());
			stock.setStore(productSplit.getShelf().getShelf().getAisle().getStore());
			stock.setProduct(productSplit.getProduct());
			stock.setQuantity(1.0);
			stock.setImplementation(getImplementation());
			stocks = new ArrayList<Stock>();
			stocks.add(stock);
			// Decrease Stock by details
			for (ProductSplitDetail projectInventory : productSplit.getProductSplitDetails()) {

				stock = new Stock();
				stock.setShelf(projectInventory.getShelf());
				stock.setStore(projectInventory.getShelf().getShelf().getAisle().getStore());
				stock.setProduct(projectInventory.getProduct());
				stock.setUnitRate(projectInventory.getUnitRate());
				stock.setQuantity(1.0);
				stock.setImplementation(getImplementation());
				deletestocks.add(stock);
			}
			stockBL.updateStockDetails(stocks, deletestocks);
			// Delete Split
			productSplitService.deleteProductSplit(
					productSplit,
					new ArrayList<ProductSplitDetail>(productSplit
							.getProductSplitDetails()));
		} catch (Exception e) {
			returnValue = false;
			e.printStackTrace();
		}
		return returnValue;
	}

	public ProductSplitService getProductSplitService() {
		return productSplitService;
	}

	public void setProductSplitService(ProductSplitService productSplitService) {
		this.productSplitService = productSplitService;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}
}
