/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.StockPeriodic;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class StockService {

	private AIOTechGenericDAO<Stock> stockDAO;
	private AIOTechGenericDAO<StockPeriodic> stockPeriodicDAO;

	// Get closing stock
	public Stock getStockDetails(long productId, int storeId) throws Exception {
		List<Stock> stockList = stockDAO.findByNamedQuery("getStockDetails",
				productId, storeId);
		Stock stockDetails = new Stock();
		if (stockList.size() > 0)
			stockDetails = stockList.get(0);
		else
			stockDetails = null;
		return stockDetails;
	}

	public List<Stock> getIssuedStoreStockDetails(long productId, int storeId)
			throws Exception {
		return stockDAO.findByNamedQuery("getIssuedStoreStockDetails",
				productId, storeId);

	}

	// Get Stock by Stock Id
	public Stock getStockByStockId(long stockId) throws Exception {
		return stockDAO.findByNamedQuery("getStockByStockId", stockId).get(0);
	}

	// Get Closing Stock
	public List<Stock> getStockFullDetails(long productId, int shelfId)
			throws Exception {
		return stockDAO.findByNamedQuery("getStockFullDetails", productId,
				shelfId);
	}

	// Get Closing Stock
	public List<Stock> getBatchStockFullDetails(long productId, int shelfId,
			String batchNumber, Date productExpriy) throws Exception {
		return stockDAO.findByNamedQuery("getBatchStockFullDetails", productId,
				shelfId, batchNumber, productExpriy);
	}

	@SuppressWarnings("unchecked")
	public List<Stock> getStockFullDetails(int shelfId, long productId,
			long storeId, String batchNumber, Date productExpriy) {

		Criteria stockCriteria = stockDAO.createCriteria();
		stockCriteria.createAlias("store", "str",
				CriteriaSpecification.LEFT_JOIN);
		stockCriteria.createAlias("shelf", "slf",
				CriteriaSpecification.LEFT_JOIN);
		stockCriteria.createAlias("product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		if (shelfId > 0)
			stockCriteria.add(Restrictions.eq("slf.shelfId", shelfId));

		if (storeId > 0)
			stockCriteria.add(Restrictions.eq("str.storeId", storeId));

		if (productId > 0)
			stockCriteria.add(Restrictions.eq("prd.productId", productId));

		if (null != batchNumber && !batchNumber.equals(""))
			stockCriteria.add(Restrictions.eq("batchNumber", batchNumber));

		if (null != productExpriy && !productExpriy.equals(""))
			stockCriteria.add(Restrictions.eq("productExpiry", productExpriy));

		Set<Stock> uniqueRecord = new HashSet<Stock>(stockCriteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Stock>(
				uniqueRecord) : null;
	}

	// Get Closing Stock
	public List<Stock> getStockByStoreDetails(long productId, long storeId)
			throws Exception {
		return stockDAO.findByNamedQuery("getStockByStoreDetails", productId,
				storeId);
	}

	// Get Closing Stock
	public List<Stock> getProductStockFullDetails(long productId)
			throws Exception {
		return stockDAO.findByNamedQuery("getProductStockFullDetails",
				productId);
	}

	// Get Closing Stock
	public List<Stock> getStockWithSelfs(long productId, long storeId,
			int shelfId) throws Exception {
		return stockDAO.findByNamedQuery("getStockWithSelfs", productId,
				storeId, shelfId);
	}

	public Stock getStockByProductId(Long productId) throws Exception {
		List<Stock> stocks = stockDAO.findByNamedQuery("getStockByProductId",
				productId);
		return null != stocks && stocks.size() > 0 ? stocks.get(0) : null;
	}

	// Get Closing Stock by product id
	public List<Stock> getStockFullDetailsByProduct(long productId)
			throws Exception {
		return stockDAO.findByNamedQuery("getStockFullDetailsByProduct",
				productId);
	}

	// Get Product Stock
	public List<Stock> getProductStockDetail(Implementation implemention,
			char itemType) throws Exception {
		return stockDAO.findByNamedQuery("getProductStockDetail", implemention,
				itemType);
	}

	// Get Craft & Service Product Stock
	public List<Stock> getCraftServiceProducts(Implementation implemention,
			byte itemSubType1, byte itemSubType2) throws Exception {
		return stockDAO.findByNamedQuery("getCraftServiceProducts",
				implemention, itemSubType1, itemSubType2);
	}

	// Get Product Stock
	public List<Stock> getProductStockDetail(Implementation implemention)
			throws Exception {
		return stockDAO.findByNamedQuery("getProductStockDetailCommon",
				implemention);
	}

	// Get Product Stock
	public List<Stock> getProductRequisitionStock(long productId)
			throws Exception {
		return stockDAO.findByNamedQuery("getProductRequisitionStock",
				productId);
	}

	// Get Product Stock
	public List<Stock> getStockByStoreId(int storeId) throws Exception {
		return stockDAO.findByNamedQuery("getStockByStoreId", storeId);
	}

	public List<Stock> getStockByProduct(long productId) throws Exception {
		return stockDAO.findByNamedQuery("getStockByProduct", productId);
	}

	// Update Stock Quantity
	public void updateStockDetails(List<Stock> stockList) throws Exception {
		stockDAO.saveOrUpdateAll(stockList);
	}

	public void saveStock(Stock stock) throws Exception {
		stockDAO.saveOrUpdate(stock);
	}

	public List<Stock> getProductsByShelfId(int shelfId) {
		return stockDAO.findByNamedQuery("getProductsByShelfId", shelfId);
	}

	// Update Stock Quantity
	public void mergeStockDetails(List<Stock> stockList) throws Exception {
		for (Stock stock : stockList) {
			stockDAO.merge(stock);
		}
	}

	// Delete Stock
	public void deleteStocks(List<Stock> stockList) throws Exception {
		stockDAO.deleteAll(stockList);
	}

	// Delete Stock
	public void deleteStocks(List<Stock> stockList, Session session)
			throws Exception {
		for (Stock stock : stockList)
			session.delete(stock);
	}

	// Delete Stock
	public void deleteStock(Stock stock) throws Exception {
		stockDAO.delete(stock);
	}

	// Get Stock Store Details
	@SuppressWarnings("unchecked")
	public List<Stock> getStockReport(Implementation implementation,
			long storeId, long productId) {

		Criteria stockCriteria = stockDAO.createCriteria();
		stockCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("implementation", implementation)));
		if (storeId > 0) {
			stockCriteria.add(Restrictions.conjunction().add(
					Restrictions.eq("store.storeId", storeId)));
		}
		if (productId > 0) {
			stockCriteria.add(Restrictions.conjunction().add(
					Restrictions.eq("product.productId", productId)));
		}

		stockCriteria
				.createAlias("store", "s", CriteriaSpecification.LEFT_JOIN);
		stockCriteria.createAlias("shelf", "slf",
				CriteriaSpecification.LEFT_JOIN);
		stockCriteria.createAlias("slf.shelf", "rack",
				CriteriaSpecification.LEFT_JOIN);
		stockCriteria.createAlias("rack.aisle", "ae",
				CriteriaSpecification.LEFT_JOIN);
		stockCriteria.setFetchMode("product", FetchMode.JOIN);
		stockCriteria.setFetchMode("product.productUnit", FetchMode.JOIN);
		stockCriteria.setFetchMode("product.lookupDetail", FetchMode.JOIN);
		return stockCriteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Stock> getFilteredStockDetails(Implementation implementation,
			Long stockId, Integer storeId, Long productId) throws Exception {
		Criteria criteria = stockDAO.createCriteria();

		criteria.createAlias("product", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("store", "s", CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (stockId != null && stockId > 0) {
			criteria.add(Restrictions.eq("stockId", stockId));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("s.storeId", storeId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("p.productId", productId));
		}

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Stock> getStockByStoreProducts(Set<Long> products, Long storeId)
			throws Exception {
		Criteria criteria = stockDAO.createCriteria();

		criteria.createAlias("product", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("store", "s", CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.in("p.productId", products));

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("s.storeId", storeId));
		}
		criteria.add(Restrictions.gt("quantity", 0d));

		Set<Stock> uniqueRecord = new HashSet<Stock>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Stock>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Stock> getStockByShelfs(long productId, long storeId,
			Integer[] shelfIds) throws Exception {
		Criteria criteria = stockDAO.createCriteria();

		criteria.createAlias("product", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("store", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelf", "slf", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("slf.shelf", "rack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.aisle", "ae",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ae.store", "slfstr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("p.productId", productId));

		criteria.add(Restrictions.eq("s.storeId", storeId));

		if (shelfIds != null && shelfIds.length > 0) {
			criteria.add(Restrictions.in("slf.shelfId", shelfIds));
		}

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Stock> getStockByProducts(Long[] productIds) throws Exception {
		Criteria criteria = stockDAO.createCriteria();
		criteria.createAlias("product", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("store", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelf", "slf", CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("p.productId", productIds));
		return criteria.list();
	}

	public List<StockPeriodic> getStockPeriodicByMonth(
			Implementation implementation, String month) throws Exception {
		return stockPeriodicDAO.findByNamedQuery("getStockPeriodicByMonth",
				implementation, month);
	}

	public void updateStockPeriodic(List<StockPeriodic> stockList)
			throws Exception {
		stockPeriodicDAO.saveOrUpdateAll(stockList);
	}

	public void deleteStockPeriodicAll(List<StockPeriodic> stockList)
			throws Exception {
		stockPeriodicDAO.deleteAll(stockList);
	}

	// Getters and Setters
	public AIOTechGenericDAO<Stock> getStockDAO() {
		return stockDAO;
	}

	public void setStockDAO(AIOTechGenericDAO<Stock> stockDAO) {
		this.stockDAO = stockDAO;
	}

	public AIOTechGenericDAO<StockPeriodic> getStockPeriodicDAO() {
		return stockPeriodicDAO;
	}

	public void setStockPeriodicDAO(
			AIOTechGenericDAO<StockPeriodic> stockPeriodicDAO) {
		this.stockPeriodicDAO = stockPeriodicDAO;
	}

}
