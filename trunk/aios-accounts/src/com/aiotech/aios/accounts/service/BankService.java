package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class BankService {
	private AIOTechGenericDAO<Bank> bankDAO;
	private AIOTechGenericDAO<BankAccount> bankAccountDAO;

	// Bank List
	public List<Bank> getAllBanks(Implementation implementation,
			byte institutionType) throws Exception {
		return bankDAO.findByNamedQuery("getAllBanks", implementation,
				institutionType);
	}

	// Bank List
	public List<Bank> getAllInsitutions(Implementation implementation)
			throws Exception {
		return bankDAO.findByNamedQuery("getAllInsitutions", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Bank> getAllDebitCreditCard(Implementation implementation,
			byte insituationType, byte cardType) throws Exception {
		return this.getBankDAO().findByNamedQuery("getAllDebitCreditCard",
				implementation, insituationType, cardType);
	}

	// Get all bank accounts using implementation
	public List<BankAccount> getAllBankAccounts(Implementation implementation)
			throws Exception {
		return bankAccountDAO.findByNamedQuery("getAllBankAccounts",
				implementation);
	}

	// Get bank account using bankAccountId
	public BankAccount getBankAccountInfo(long bankAccountId) throws Exception {
		return bankAccountDAO.findById(bankAccountId);
	}

	// Get bank account using bankAccountId
	public BankAccount getBankAccountDetails(long bankAccountId)
			throws Exception {
		return bankAccountDAO.findByNamedQuery("getBankAccountDetails",
				bankAccountId).get(0);
	}

	// Get all bank accounts using bank
	public List<BankAccount> getAllBankAccount(Bank bank) throws Exception {
		return bankAccountDAO.findByNamedQuery("getAllBankAccountsUsingBank",
				bank);
	}

	// Get bank account using bankAccountId
	public Bank getBankInfo(long bankId) throws Exception {
		return bankDAO.findByNamedQuery("getBankInfo", bankId).get(0);
	}

	public void saveBank(Bank bank, List<BankAccount> bankAccounts)
			throws Exception {
		bankDAO.saveOrUpdate(bank);
		if (bankAccounts != null) {
			for (BankAccount ul : bankAccounts) {
				ul.setBank(bank);
			}
			bank.setBankAccounts(new HashSet<BankAccount>(bankAccounts));
			bankAccountDAO.saveOrUpdateAll(bankAccounts);
		}

	}

	@SuppressWarnings("unchecked")
	public List<BankAccount> getBankAccountByCombination(
			Set<Long> combinationIds) {
		Criteria criteria = bankAccountDAO.createCriteria();
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combinationByClearingAccount", "cl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.or(
				Restrictions.in("cb.combinationId", combinationIds),
				Restrictions.in("cl.combinationId", combinationIds)));
		Set<BankAccount> uniqueRecord = new HashSet<BankAccount>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<BankAccount>(
				uniqueRecord) : null;
	}

	public List<Bank> getAllBankWithAccounts(Implementation implementation)
			throws Exception {
		return bankDAO.findByNamedQuery("getAllBanksWithAccounts",
				implementation);
	}

	public void deleteBank(Bank bank, List<BankAccount> bankAccounts)
			throws Exception {
		if (bankAccounts != null)
			bankAccountDAO.deleteAll(bankAccounts);
		bankDAO.delete(bank);
	}

	public void deleteBankAccount(BankAccount bankAccounts) throws Exception {
		bankAccountDAO.delete(bankAccounts);
	}

	public AIOTechGenericDAO<Bank> getBankDAO() {
		return bankDAO;
	}

	public void setBankDAO(AIOTechGenericDAO<Bank> bankDAO) {
		this.bankDAO = bankDAO;
	}

	public AIOTechGenericDAO<BankAccount> getBankAccountDAO() {
		return bankAccountDAO;
	}

	public void setBankAccountDAO(AIOTechGenericDAO<BankAccount> bankAccountDAO) {
		this.bankAccountDAO = bankAccountDAO;
	}
}
