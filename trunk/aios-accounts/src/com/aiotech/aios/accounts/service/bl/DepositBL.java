package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.action.DepositAction;
import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.DepositService;
import com.aiotech.aios.accounts.to.DepositTO;
import com.aiotech.aios.accounts.to.converter.GLJournalVoucherTOConverter;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.InstitutionType;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DepositBL {
	private DepositService depositService;
	private List<DepositTO> depositList;
	private TransactionBL transactionBL;
	private AIOTechGenericDAO<ContractPayment> contractPaymentDAO;
	private AIOTechGenericDAO<Contract> contractDAO;

	public List<Bank> getBankList(Implementation implementation)
			throws Exception {
		List<Bank> bankList = depositService.getBankService().getAllBanks(
				implementation, InstitutionType.Bank.getCode());
		return bankList;
	}

	public List<DepositTO> getBankTransactionEntries(
			Implementation implementation) throws Exception {
		List<BankAccount> accountList = depositService.getBankService()
				.getAllBankAccounts(implementation);
		depositList = new ArrayList<DepositTO>();
		if (null != accountList && !accountList.equals("")) {
			depositList = GLJournalVoucherTOConverter
					.convertBankDepositList(accountList);
			return depositList;
		} else {
			return null;
		}
	}

	public List<TransactionDetail> createReceiptTransactionDetail(
			Combination customerCombination, String[] detailString,
			String propertyName, Implementation implementation)
			throws Exception {
		TransactionDetail transactionDetail = new TransactionDetail();
		List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
		ContractPayment contractPayment = this.getContractPaymentDAO()
				.findById(Long.parseLong(detailString[1]));
		if (null == contractPayment.getIsDeposited()) {
			String contractFeeType = Constants.RealEstate.ContractFeeType.get(
					contractPayment.getFeeType()).toString();
			if (contractFeeType.equalsIgnoreCase("Fee")
					|| contractFeeType.equalsIgnoreCase("Deposit")) {
				if (Boolean.parseBoolean(detailString[3])) {
					contractPayment.setIsDeposited(false);
					this.getContractPaymentDAO().saveOrUpdate(contractPayment);
				}
				return null;
				/*
				 * transactionDetail = createFeeDepositTransactionDetail(
				 * contractPayment, detailString, contractFeeType);
				 * transactionDetailList.add(transactionDetail);
				 * transactionDetail =
				 * createTenantTransactionDetail(contractPayment,
				 * customerCombination, contractFeeType);
				 * transactionDetailList.add(transactionDetail);
				 */
			} else {
				if (!Boolean.parseBoolean(detailString[2])) {
					/*
					 * transactionDetail = createDefferedIncomeDetail(
					 * contractPayment, contractFeeType, implementation);
					 * transactionDetailList.add(transactionDetail);
					 * transactionDetail =
					 * createRentIncomeDetail(contractPayment, propertyName,
					 * contractFeeType, implementation);
					 * transactionDetailList.add(transactionDetail);
					 */
				} else {
					transactionDetail = createPDCReceivableDetail(
							contractPayment, implementation, contractFeeType);
					transactionDetailList.add(transactionDetail);
					transactionDetail = createTenantTransactionDetail(
							contractPayment, customerCombination,
							contractFeeType);
					transactionDetailList.add(transactionDetail);
					contractPayment.setIsPdc(true);
					this.getContractPaymentDAO().saveOrUpdate(contractPayment);
				}
				if (Boolean.parseBoolean(detailString[3])) {
					contractPayment.setIsDeposited(false);
					this.getContractPaymentDAO().saveOrUpdate(contractPayment);
				}
				return transactionDetailList;
			}
		} else {
			if (Boolean.parseBoolean(detailString[3])) {
				contractPayment.setIsDeposited(false);
				this.getContractPaymentDAO().saveOrUpdate(contractPayment);
			}
			return null;
		}
	}

	private TransactionDetail createPDCReceivableDetail(
			ContractPayment contractPayment, Implementation implementaion,
			String contractFeeType) throws Exception {
		TransactionDetail transactionDetail = this.getTransactionBL()
				.createTransactionDetail(contractPayment.getAmount(),
						implementaion.getPdcReceived(), true,
						"PDC Receivables(" + contractFeeType + ")", null, null);
		return transactionDetail;
	}

	/*
	 * private TransactionDetail createRentIncomeDetail( ContractPayment
	 * contractPayment, String propertyName, String contractFeeType,
	 * Implementation implementaion) throws Exception { Account account =
	 * this.getTransactionBL().getCombinationService()
	 * .getAccountDetail(propertyName, implementaion); Combination combination =
	 * this.getTransactionBL() .getCombinationService()
	 * .getCombinationByNaturalAccount(account.getAccountId());
	 * TransactionDetail transactionDetail = this.getTransactionBL()
	 * .createTransactionDetail(contractPayment.getAmount(),
	 * combination.getCombinationId(), false, "Rent Income(" + contractFeeType +
	 * ")", null); return transactionDetail; }
	 * 
	 * private TransactionDetail createDefferedIncomeDetail( ContractPayment
	 * contractPayment, String contractFeeType, Implementation implementation)
	 * throws Exception { TransactionDetail transactionDetail =
	 * this.getTransactionBL()
	 * .createTransactionDetail(contractPayment.getAmount(),
	 * implementation.getUnearnedRevenue(), true, "UnEarned Revenue(" +
	 * contractFeeType + ")", null); return transactionDetail; }
	 */

	private TransactionDetail createTenantTransactionDetail(
			ContractPayment contractPayment, Combination combination,
			String contractFeeType) throws Exception {
		TransactionDetail transactionDetail = this.getTransactionBL()
				.createTransactionDetail(contractPayment.getAmount(),
						combination.getCombinationId(), false,
						"Customer Credited (" + contractFeeType + ")", null, null);
		return transactionDetail;
	}

	@SuppressWarnings("unused")
	private TransactionDetail createFeeDepositTransactionDetail(
			ContractPayment contractPayment, String[] accountDetail,
			String contractFeeType) throws Exception {
		TransactionDetail transactionDetail = new TransactionDetail();
		if (null != contractPayment.getBankName()
				&& !contractPayment.getBankName().equals("")) {
			transactionDetail = this.getTransactionBL()
					.createTransactionDetail(contractPayment.getAmount(),
							Long.parseLong(accountDetail[0]), true,
							"By Bank Debited(" + contractFeeType + ")", null, null);
		} else {
			transactionDetail = this.getTransactionBL()
					.createTransactionDetail(contractPayment.getAmount(),
							Long.parseLong(accountDetail[0]), true,
							"By Cash Debited(" + contractFeeType + ")", null, null);
		}
		return transactionDetail;
	}

	public Transaction createReceiptTransaction(Double amount,
			Implementation implementation) throws Exception {
		List<Category> categoryList = this.getTransactionBL().getCategoryBL()
				.getCategoryService().getAllActiveCategories(implementation);
		Category categoryReceipts = new Category();
		if (null != categoryList) {
			for (Category list : categoryList) {
				if (list.getName().equalsIgnoreCase("Contract Receipts")) {
					categoryReceipts.setCategoryId(list.getCategoryId());
					break;
				}
			}
		}
		if (null == categoryReceipts || categoryReceipts.equals("")) {
			categoryReceipts = this.getTransactionBL().getCategoryBL()
					.createCategory(implementation, "Contract Receipts");
		} else if (null == categoryReceipts.getCategoryId()) {
			categoryReceipts = this.getTransactionBL().getCategoryBL()
					.createCategory(implementation, "Contract Receipts");
		}
		Transaction transaction = this.getTransactionBL().createTransaction(
				transactionBL.getCurrency().getCurrencyId(),
				"CONTRACT RECEIPTS", new Date(), categoryReceipts, null, null);
		return transaction;
	}

	public void saveTransaction(Transaction transaction,
			List<TransactionDetail> transactionList) throws Exception {
		depositService.getTransactionBL().saveTransaction(transaction,
				transactionList);
	}

	public void updateTransaction(List<TransactionDetail> transactionList)
			throws Exception {
		// depositService.getTransactionBL().updateTransaction(transactionList);
	}

	public Period getCalendar(Implementation implementation) throws Exception {
		com.aiotech.aios.accounts.domain.entity.Calendar calendar = null;
		Period period = null;
		try {
			calendar = depositService.getCalendarService()
					.getAllCalendar(implementation).get(0);
			period = depositService.getCalendarService()
					.getPeriodDetails(calendar.getCalendarId()).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return period;
	}

	public TransactionDetail createDepositCredit(Double amount,
			long combinationId) {
		TransactionDetail transactionDetail = new TransactionDetail();
		Combination combination = new Combination();
		combination.setCombinationId(combinationId);
		transactionDetail.setCombination(combination);
		transactionDetail.setDescription("Bank account credited");
		transactionDetail.setIsDebit(false);
		transactionDetail.setAmount(amount);
		transactionDetail.setIsreceipt(null);
		return transactionDetail;
	}

	public TransactionDetail createCreditDetail(Double amount,
			String accountName, Implementation implementation) throws Exception {
		Combination combination = null;
		combination = new Combination();
		TransactionDetail transactionDetail = new TransactionDetail();
		Account account = depositService.getAccountService().getAccountDetails(
				accountName, implementation);
		combination = getCombinationByAccount(account.getAccountId());
		transactionDetail.setCombination(combination);
		transactionDetail.setDescription("Tenant a/c credited");
		transactionDetail.setAmount(amount);
		transactionDetail.setIsDebit(false);
		transactionDetail.setIsreceipt(false);
		return transactionDetail;
	}

	public List<TransactionDetail> createDebitDetail(
			List<TransactionDetail> transactionList, String accountDetails)
			throws Exception {
		Combination combination = null;
		TransactionDetail transactionDetail = null;
		String[] debitSide = DepositAction.splitValues(accountDetails, "#");
		for (int i = 0; i < debitSide.length; i++) {
			transactionDetail = new TransactionDetail();
			combination = new Combination();
			String[] debitData = DepositAction.splitValues(debitSide[i], "@");
			combination.setCombinationId(Long.parseLong(debitData[0]));
			transactionDetail.setCombination(combination);
			transactionDetail.setAmount(Double.parseDouble(debitData[1]));
			transactionDetail.setDescription("a/c debited");
			transactionDetail.setIsDebit(true);
			transactionDetail.setIsreceipt(false);
			transactionList.add(transactionDetail);
		}
		return transactionList;
	}

	public Combination getCombinationByAccount(Long accountId) throws Exception {
		Combination combination = depositService.getCombinationService()
				.getCombinationByAccount(accountId);
		return combination;
	}

	public DepositService getDepositService() {
		return depositService;
	}

	public void setDepositService(DepositService depositService) {
		this.depositService = depositService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public AIOTechGenericDAO<ContractPayment> getContractPaymentDAO() {
		return contractPaymentDAO;
	}

	public void setContractPaymentDAO(
			AIOTechGenericDAO<ContractPayment> contractPaymentDAO) {
		this.contractPaymentDAO = contractPaymentDAO;
	}

	public AIOTechGenericDAO<Contract> getContractDAO() {
		return contractDAO;
	}

	public void setContractDAO(AIOTechGenericDAO<Contract> contractDAO) {
		this.contractDAO = contractDAO;
	}
}
