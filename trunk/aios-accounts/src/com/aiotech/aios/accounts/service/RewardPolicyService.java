package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.RewardPolicy;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class RewardPolicyService {

	private AIOTechGenericDAO<RewardPolicy> rewardPolicyDAO;

	// Get all Reward Policies
	public List<RewardPolicy> getAllRewardPolicies(Implementation implementation)
			throws Exception {
		return rewardPolicyDAO.findByNamedQuery("getAllRewardPolicies",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Reward Policy by policy Id
	public RewardPolicy getRewardPolicyByPolicyId(long rewardPolicyId)
			throws Exception {
		return rewardPolicyDAO.findByNamedQuery("getRewardPolicyByPolicyId",
				rewardPolicyId).get(0);
	}
	
	// Get Reward Policy by coupon Id
	public List<RewardPolicy> getRewardPolicyByCouponId(long couponId)
			throws Exception {
		return rewardPolicyDAO.findByNamedQuery("getRewardPolicyByCouponId",
				couponId);
	}

	// Save Reward Policy
	public void saveRewardPolicy(RewardPolicy rewardPolicy) throws Exception {
		rewardPolicyDAO.saveOrUpdate(rewardPolicy);
	}

	// Delete Reward Policy
	public void deleteRewardPolicy(RewardPolicy rewardPolicy) throws Exception {
		rewardPolicyDAO.delete(rewardPolicy);
	}

	// Getters & Setters
	public AIOTechGenericDAO<RewardPolicy> getRewardPolicyDAO() {
		return rewardPolicyDAO;
	}

	public void setRewardPolicyDAO(
			AIOTechGenericDAO<RewardPolicy> rewardPolicyDAO) {
		this.rewardPolicyDAO = rewardPolicyDAO;
	}
}
