package com.aiotech.aios.accounts.service.bl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class SalesUpdateJob {

	private PointOfSaleBL pointOfSaleBL;
	private Properties prop;

	private static final Logger log = Logger.getLogger(SalesUpdateJob.class
			.getName());

	public void updateSalesSync() {
		try {
			prop = new Properties();
			InputStream input = null;
			input = SalesUpdateJob.class
					.getResourceAsStream("/dbconfig.properties");
			prop.load(input);
			// Reload The config file from local drive
			prop.load(new FileInputStream(prop.getProperty("SYNCFILE-LOCATION")
					.trim()));
			String keyValue = prop.getProperty("SYNCFLAG").trim();
			String offlineMode = prop.getProperty("OFFLINE-MODE").trim();
			if (keyValue.equalsIgnoreCase("N")
					&& offlineMode.equalsIgnoreCase("Y")) {
				if (liveConnection())
					performSalesUpdates();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			log.warn("File Not found or config file is locked.....");
			e.printStackTrace();
		}
	}

	private boolean liveConnection() {
		try {
			String applicationUrl = prop.getProperty("APPLCIATION-URL").trim()
					.concat(prop.getProperty("APP-KEY").trim());
			String userAgent = "Mozilla/5.0";
			URL obj = new URL(applicationUrl);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", userAgent);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setDoOutput(true);
			if (con.getResponseCode() == 200)
				return true;
			else
				log.warn("Server was down...");
		} catch (Exception e) {
			log.warn("Server was down...");
		}
		return false;
	}

	private void performSalesUpdates() {
		try {
			updateSync("Y");
			Implementation implementation = new Implementation();
			implementation.setImplementationId(Long.parseLong(prop.getProperty(
					"APP-KEY").trim()));

			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getOfflinePOSTransactions(
							implementation);
			List<PointOfSale> localSales = null;
			Person person = null;
			List<Customer> customers = new ArrayList<Customer>();
			if (null != pointOfSales && pointOfSales.size() > 0) {
				PointOfSale localSale = null;
				localSales = new ArrayList<PointOfSale>();
				for (PointOfSale pointOfSale : pointOfSales) {
					localSale = new PointOfSale();

					// Manipulate customer
					localSale.setCustomer(manipulateCustomer(pointOfSale));
					if (pointOfSale.getCustomer() != null) {
						customers.add(pointOfSale.getCustomer());
					}

					localSale.setImplementation(implementation);
					localSale.setReferenceNumber(pointOfSale
							.getReferenceNumber());
					localSale.setDeliveryOptionName(pointOfSale
							.getDeliveryOptionName());
					localSale
							.setDeliveryStatus(pointOfSale.getDeliveryStatus());
					if (pointOfSale.getLookupDetail() != null) {
						LookupDetail lookupDetail = new LookupDetail();
						lookupDetail.setLookupDetailId(pointOfSale
								.getLookupDetail().getLookupDetailId());
						localSale.setLookupDetail(lookupDetail);
					}
					if (pointOfSale.getPOSUserTill() != null) {
						POSUserTill posTill = new POSUserTill();
						posTill.setPosUserTillId(pointOfSale.getPOSUserTill()
								.getPosUserTillId());
						localSale.setPOSUserTill(posTill);
					}
					localSale.setSalesDate(pointOfSale.getSalesDate());
					localSale.setSaleType(pointOfSale.getSaleType());

					if (pointOfSale.getPersonByDriverId() != null) {
						person = new Person();
						person.setPersonId(pointOfSale.getPersonByDriverId()
								.getPersonId());
						localSale.setPersonByDriverId(person);
					}

					if (pointOfSale.getPersonByEmployeeId() != null) {
						person = new Person();
						person.setPersonId(pointOfSale.getPersonByEmployeeId()
								.getPersonId());
						localSale.setPersonByDriverId(person);
					}
					localSale
							.setDiscountMethod(pointOfSale.getDiscountMethod());
					localSale.setPromotionMethod(pointOfSale
							.getPromotionMethod());
					localSale.setTotalDiscount(pointOfSale.getTotalDiscount());

					// Details
					localSale
							.setPointOfSaleDetails(new HashSet<PointOfSaleDetail>(
									manipulatePosDetails(pointOfSale)));

					// Receipts
					localSale
							.setPointOfSaleReceipts(new HashSet<PointOfSaleReceipt>(
									manipulateReceipts(pointOfSale)));

					// Offers
					if (null != pointOfSale.getPointOfSaleOffers()
							&& pointOfSale.getPointOfSaleOffers().size() > 0) {
						List<PointOfSaleOffer> offers = new ArrayList<PointOfSaleOffer>();
						PointOfSaleOffer offerTemp = null;
						for (PointOfSaleOffer offer : pointOfSale
								.getPointOfSaleOffers()) {
							offerTemp = new PointOfSaleOffer();
							offerTemp.setPointOfSaleOfferId(offer
									.getPointOfSaleOfferId());
							offers.add(offerTemp);
						}
						localSale
								.setPointOfSaleOffers(new HashSet<PointOfSaleOffer>(
										offers));
					}

					// Charges
					if (null != pointOfSale.getPointOfSaleCharges()
							&& pointOfSale.getPointOfSaleCharges().size() > 0) {
						List<PointOfSaleCharge> charges = new ArrayList<PointOfSaleCharge>();
						PointOfSaleCharge chargeTemp = null;
						LookupDetail lookupDetail = null;
						for (PointOfSaleCharge charge : pointOfSale
								.getPointOfSaleCharges()) {
							chargeTemp = new PointOfSaleCharge();
							lookupDetail = new LookupDetail();
							chargeTemp.setPointOfSaleChargeId(charge
									.getPointOfSaleChargeId());
							chargeTemp.setCharges(charge.getCharges());
							lookupDetail.setLookupDetailId(charge
									.getLookupDetail().getLookupDetailId());
							chargeTemp.setLookupDetail(lookupDetail);
							chargeTemp.setDescription(charge.getDescription());
							charges.add(chargeTemp);
						}
						localSale
								.setPointOfSaleCharges(new HashSet<PointOfSaleCharge>(
										charges));
					}

					localSales.add(localSale);
				}

				PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
				pointOfSaleVO.setPointOfSales(localSales);
				Client client = Client.create();
				WebResource webResource = client.resource(prop.getProperty(
						"WEBSERVICE-URL").trim()
						+ "pointOfSaleSync");
				ClientResponse response = webResource.type(
						MediaType.APPLICATION_XML).post(ClientResponse.class,
						pointOfSaleVO);
				String output = response.getEntity(String.class);
				log.info("sales synchronization return status..." + output);
				if (response.getStatus() == 200 || output.equals("SUCCESS")) {
					if (null != customers && customers.size() > 0) {
						for (Customer customer : customers) {
							customer.setPosCustomer(null);
						}
						pointOfSaleBL.getCustomerBL()
								.updateCustomers(customers);
					}
					for (PointOfSale pointOfSale2 : pointOfSales) {
						pointOfSale2.setOfflineEntry(null);
						pointOfSale2.setProcessedSession((byte) 2);
					}

					pointOfSaleBL.updatePointOfSale(pointOfSales, (byte) 2);
				} else {
					log.info("Error in sales synchronization..."
							+ response.getStatus());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				updateSync("N");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void updateSync(String syncFlag) throws IOException {
		FileInputStream inputStream = new FileInputStream(prop.getProperty(
				"SYNCFILE-LOCATION").trim());
		Properties props = new Properties();
		props.load(inputStream);
		inputStream.close();

		FileOutputStream outStream = new FileOutputStream(prop.getProperty(
				"SYNCFILE-LOCATION").trim());
		props.setProperty("SYNCFLAG", syncFlag);
		props.store(outStream, null);
	}

	private Customer manipulateCustomer(PointOfSale pos) {
		Customer customer = new Customer();
		if (pos.getCustomer() != null) {
			if (pos.getCustomer().getPosCustomer() != null
					&& pos.getCustomer().getPosCustomer()) {
				Person person = new Person();
				person.setFirstName(pos.getCustomer().getPersonByPersonId()
						.getFirstName());
				person.setLastName(pos.getCustomer().getPersonByPersonId()
						.getLastName());
				person.setMobile(pos.getCustomer().getPersonByPersonId()
						.getMobile());
				person.setResidentialAddress(pos.getCustomer()
						.getPersonByPersonId().getResidentialAddress());
				person.setGender(pos.getCustomer().getPersonByPersonId()
						.getGender());
				person.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
				customer.setPersonByPersonId(person);
				customer.setPosCustomer(pos.getCustomer().getPosCustomer());
				Implementation implementation = new Implementation();
				implementation.setImplementationId(pos.getImplementation()
						.getImplementationId());
				customer.setImplementation(implementation);
			} else {
				customer.setCustomerId(pos.getCustomer().getCustomerId());
				customer.setPosCustomer(pos.getCustomer().getPosCustomer());
				customer.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
				Implementation implementation = new Implementation();
				implementation.setImplementationId(pos.getImplementation()
						.getImplementationId());
				customer.setImplementation(implementation);
				// Object
				if (pos.getCustomer().getPersonByPersonId() != null) {
					Person person = new Person();
					person.setPersonId(pos.getCustomer().getPersonByPersonId()
							.getPersonId());
					person.setFirstName(pos.getCustomer().getPersonByPersonId()
							.getFirstName());
					person.setLastName(pos.getCustomer().getPersonByPersonId()
							.getLastName());
					person.setMobile(pos.getCustomer().getPersonByPersonId()
							.getMobile());
					person.setResidentialAddress(pos.getCustomer()
							.getPersonByPersonId().getResidentialAddress());
					person.setGender(pos.getCustomer().getPersonByPersonId()
							.getGender());
					customer.setPersonByPersonId(person);
				} else {
					Company company = new Company();
					company.setCompanyId(pos.getCustomer().getCompany()
							.getCompanyId());
					company.setCompanyName(pos.getCustomer().getCompany()
							.getCompanyName());
					company.setCompanyPhone(pos.getCustomer().getCompany()
							.getCompanyPhone());
					company.setCompanyAddress(pos.getCustomer().getCompany()
							.getCompanyAddress());
					company.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
					customer.setCompany(pos.getCustomer().getCompany()); 
				}
			}

		} else
			customer = null;
		return customer;
	}

	private List<PointOfSaleReceipt> manipulateReceipts(PointOfSale pos) {
		List<PointOfSaleReceipt> receipts = new ArrayList<PointOfSaleReceipt>();
		if (pos.getPointOfSaleReceipts() != null) {
			PointOfSaleReceipt recceipt = null;
			for (PointOfSaleReceipt pointOfSaleReceipt : pos
					.getPointOfSaleReceipts()) {
				recceipt = new PointOfSaleReceipt();
				recceipt.setReceiptType(pointOfSaleReceipt.getReceiptType());
				recceipt.setReceipt(pointOfSaleReceipt.getReceipt());
				recceipt.setBalanceDue(pointOfSaleReceipt.getBalanceDue());
				recceipt.setTransactionNumber(pointOfSaleReceipt
						.getTransactionNumber());
				receipts.add(recceipt);
			}
		} else {
			receipts = null;
		}
		return receipts;
	}

	private List<PointOfSaleDetail> manipulatePosDetails(PointOfSale pos) {
		List<PointOfSaleDetail> details = new ArrayList<PointOfSaleDetail>();
		if (pos.getPointOfSaleDetails() != null) {
			PointOfSaleDetail recceipt = null;
			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				recceipt = new PointOfSaleDetail();
				recceipt.setQuantity(detail.getQuantity());
				recceipt.setUnitRate(detail.getUnitRate());
				recceipt.setIsPercentageDiscount(detail
						.getIsPercentageDiscount());
				recceipt.setDiscountValue(detail.getDiscountValue());
				recceipt.setActualPoints(detail.getActualPoints());
				recceipt.setPromotionPoints(detail.getActualPoints());
				recceipt.setSpecialProduct(detail.getSpecialProduct());
				recceipt.setItemOrder(detail.getItemOrder());
				recceipt.setComboItemOrder(detail.getComboItemOrder());
				if (detail.getDiscountMethod() != null) {
					DiscountMethod dis = new DiscountMethod();
					dis.setDiscountMethodId(detail.getDiscountMethod()
							.getDiscountMethodId());
					recceipt.setDiscountMethod(dis);
				}
				if (detail.getProductByProductId() != null) {
					Product pro = new Product();
					pro.setProductId(detail.getProductByProductId()
							.getProductId());
					recceipt.setProductByProductId(pro);
				}

				if (detail.getProductByComboProductId() != null) {
					Product pro = new Product();
					pro.setProductId(detail.getProductByComboProductId()
							.getProductId());
					recceipt.setProductByComboProductId(pro);
				}

				if (detail.getPromotionMethod() != null) {
					PromotionMethod promo = new PromotionMethod();
					promo.setPromotionMethodId(detail.getPromotionMethod()
							.getPromotionMethodId());
					recceipt.setPromotionMethod(promo);
				}

				if (detail.getProductPricingCalc() != null) {
					ProductPricingCalc cal = new ProductPricingCalc();
					cal.setProductPricingCalcId(detail.getProductPricingCalc()
							.getProductPricingCalcId());
					recceipt.setProductPricingCalc(cal);
				}

				details.add(recceipt);
			}
		} else {
			details = null;
		}
		return details;
	}

	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}
}
