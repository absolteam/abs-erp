package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.ProductDefinition;
import com.aiotech.aios.accounts.domain.entity.vo.ProductDefinitionVO;
import com.aiotech.aios.accounts.service.ProductDefinitionService;
import com.aiotech.aios.common.to.Constants.Accounts.ProductPricingDetail;
import com.aiotech.aios.system.domain.entity.Implementation;

public class ProductDefinitionBL {

	private ProductDefinitionService productDefinitionService;
	private ProductBL productBL;
	private ProductCategoryBL productCategoryBL;

	public List<ProductDefinitionVO> getProductDefinitionSimpleTree(
			long productId) throws Exception {
		List<ProductDefinition> parentDefinitions = productDefinitionService
				.getProductDefinitionByProduct(productId);
		Map<String, ProductDefinitionVO> productDefinitionMap = null;
		if (null != parentDefinitions && parentDefinitions.size() > 0) {
			productDefinitionMap = new HashMap<String, ProductDefinitionVO>();
			ProductDefinitionVO productDefinitionVO = null;
			for (ProductDefinition parentDefinition : parentDefinitions) {
				if (null != productDefinitionMap
						&& productDefinitionMap.size() > 0
						&& productDefinitionMap.containsKey(parentDefinition
								.getDefinitionLabel())) {
					productDefinitionVO = productDefinitionMap
							.get(parentDefinition.getDefinitionLabel());
					List<ProductDefinitionVO> mappedProducts = productDefinitionVO
							.getProductDefinitionVOs();
					mappedProducts
							.add(addProductDefinitionVO(parentDefinition));
					productDefinitionVO.setProductDefinitionVOs(mappedProducts);
				} else {
					productDefinitionVO = addProductDefinitionVO(parentDefinition);
					List<ProductDefinitionVO> mappedProducts = new ArrayList<ProductDefinitionVO>();
					mappedProducts.add(productDefinitionVO);
					productDefinitionVO.setProductDefinitionVOs(mappedProducts);
				}
				productDefinitionMap.put(parentDefinition.getDefinitionLabel(),
						productDefinitionVO);
			}
		}
		if (null != productDefinitionMap && productDefinitionMap.size() > 0)
			return new ArrayList<ProductDefinitionVO>(
					productDefinitionMap.values());
		return null;
	}

	public List<ProductDefinitionVO> getProductDefinitionTree(long productId)
			throws Exception {
		List<ProductDefinition> parentDefinitionItemsList = productDefinitionService
				.getParentProductDefinitions(getImplementation(), productId);
		List<ProductDefinitionVO> parentProductDefinitionVOList = new ArrayList<ProductDefinitionVO>();
		ProductDefinitionVO productDefinitionVO;

		for (ProductDefinition productDefinition : parentDefinitionItemsList) {
			productDefinitionVO = ProductDefinitionVO
					.convertTOProductDefinitionVO(productDefinition);
			fetchSubLevelItemsRecursively(productDefinitionVO);
			parentProductDefinitionVOList.add(productDefinitionVO);
		}
		return parentProductDefinitionVOList;
	}

	private void fetchSubLevelItemsRecursively(ProductDefinitionVO parentItem)
			throws Exception {

		List<ProductDefinition> childProductDefinitions = productDefinitionService
				.getChildProductDefinitions(parentItem.getProductDefinitionId());
		List<ProductDefinitionVO> childProductDefinitionVOList = new ArrayList<ProductDefinitionVO>();
		ProductDefinitionVO productDefinitionVO;

		if (childProductDefinitions != null
				&& childProductDefinitions.size() > 0) {

			for (ProductDefinition subDefinitionItem : childProductDefinitions) {

				productDefinitionVO = ProductDefinitionVO
						.convertTOProductDefinitionVO(subDefinitionItem);
				fetchSubLevelItemsRecursively(productDefinitionVO);
				childProductDefinitionVOList.add(productDefinitionVO);
			}
		}

		parentItem.setProductDefinitionVOs(new ArrayList<ProductDefinitionVO>(
				childProductDefinitionVOList));
	}

	public List<Object> showAllProductDefinitionsList() throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionService
				.getAllProductDefinitions(getImplementation());
		List<Object> productDefinitionVOs = new ArrayList<Object>();
		if (null != productDefinitions && productDefinitions.size() > 0) {
			for (ProductDefinition productDefinition : productDefinitions)
				productDefinitionVOs
						.add(addProductDefinitionVO(productDefinition));
		}
		return productDefinitionVOs;
	}

	public List<Object> showProductDefinitionParents(long productId)
			throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionService
				.getAllProductDefinitionParents(productId);
		List<Object> productDefinitionVOs = new ArrayList<Object>();
		if (null != productDefinitions && productDefinitions.size() > 0) {
			for (ProductDefinition productDefinition : productDefinitions)
				productDefinitionVOs
						.add(addProductDefinitionVO(productDefinition));
		}
		return productDefinitionVOs;
	}

	public List<Object> showProductDefinitionSubCategory(long productId)
			throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionService
				.getProductDefinitionSubCategory(productId);
		List<Object> productDefinitionVOs = new ArrayList<Object>();
		if (null != productDefinitions && productDefinitions.size() > 0) {
			for (ProductDefinition productDefinition : productDefinitions)
				productDefinitionVOs
						.add(addProductDefinitionVO(productDefinition));
		}
		return productDefinitionVOs;
	}

	public List<Object> showAllDefinitionLabels(long productId)
			throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionService
				.getAllDefinitionLabels(productId);
		List<Object> productDefinitionVOs = new ArrayList<Object>();
		if (null != productDefinitions && productDefinitions.size() > 0) {
			for (ProductDefinition productDefinition : productDefinitions)
				productDefinitionVOs.add(addDefinitionLabel(productDefinition));
		}
		return productDefinitionVOs;
	}

	private String addDefinitionLabel(ProductDefinition productDefinition) {
		return productDefinition.getDefinitionLabel();
	}

	private ProductDefinitionVO addProductDefinitionVO(
			ProductDefinition productDefinition) {
		ProductDefinitionVO productDefinitionVO = new ProductDefinitionVO();
		productDefinitionVO.setProductDefinitionId(productDefinition
				.getProductDefinitionId());
		productDefinitionVO.setProductName(productDefinition
				.getProductByProductId().getProductName());
		productDefinitionVO.setDefinitionLabel(productDefinition
				.getDefinitionLabel());
		productDefinitionVO.setSpecialProductName(productDefinition
				.getProductBySpecialProductId().getProductName());
		productDefinitionVO.setPricingType(ProductPricingDetail
				.get(productDefinition.getPricingDetail()).name().trim());
		return productDefinitionVO;
	}

	public void saveProductDefinition(List<ProductDefinition> productDefinitions)
			throws Exception {
		productDefinitionService.saveProductDefinition(productDefinitions);
	}

	public void deleteProductDefinition(ProductDefinition productDefinition)
			throws Exception {
		productDefinitionService.deleteProductDefinition(productDefinition);
	}

	public void saveProductDefinition(ProductDefinition productDefinition)
			throws Exception {
		productDefinitionService.saveProductDefinition(productDefinition);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProductDefinitionService getProductDefinitionService() {
		return productDefinitionService;
	}

	public void setProductDefinitionService(
			ProductDefinitionService productDefinitionService) {
		this.productDefinitionService = productDefinitionService;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public ProductCategoryBL getProductCategoryBL() {
		return productCategoryBL;
	}

	public void setProductCategoryBL(ProductCategoryBL productCategoryBL) {
		this.productCategoryBL = productCategoryBL;
	}
}
