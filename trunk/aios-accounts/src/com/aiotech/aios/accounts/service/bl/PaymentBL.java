package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.DirectPaymentVO;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.PaymentService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.opensymphony.xwork2.ActionContext;

public class PaymentBL {

	private PaymentService paymentService;
	private CommentBL commentBL;
	private TransactionBL transactionBL;
	private CurrencyService currencyService;
	private BankBL bankBL;
	private PurchaseBL purchaseBL;
	private AlertBL alertBL;
	private double discountAmount;
	private InvoiceBL invoiceBL;
	private ChequeBookBL chequeBookBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private DirectPaymentBL directPaymentBL;

	public JSONObject getPaymentList(Implementation implementation)
			throws Exception {
		List<Payment> paymentList = this.getPaymentService().getAllPayments(
				implementation);
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (Payment list : paymentList) {
			array = new JSONArray();
			String voidCheck = "";
			if (null != list.getVoidPayments()
					&& list.getVoidPayments().size() > 0)
				voidCheck = "<span title='Void Payment' class='void_payment_view'></span>";
			array.add(list.getPaymentId());
			array.add(list.getPaymentNumber().concat(voidCheck));
			array.add(DateFormat.convertDateToString(list.getDate().toString()));
			array.add(null != list.getBankAccount() ? list.getBankAccount()
					.getAccountNumber() : "");
			array.add(null != list.getChequeDate() ? DateFormat
					.convertDateToString(list.getChequeDate().toString()) : "");
			array.add(null != list.getChequeNumber()
					&& list.getChequeNumber() > 0 ? list.getChequeNumber() : "");
			if (null != list.getSupplier().getPerson())
				array.add(list.getSupplier().getPerson().getFirstName()
						.concat(" ")
						.concat(list.getSupplier().getPerson().getLastName()));
			else
				array.add(null != list.getSupplier().getCmpDeptLocation() ? list
						.getSupplier().getCmpDeptLocation().getCompany()
						.getCompanyName()
						: list.getSupplier().getCompany().getCompanyName());
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// update cheque book & save payment & show alert
	public void postInvoicePaymentTransaction(Payment payment,
			List<PaymentDetail> paymentDetailList, List<Invoice> invoiceList,
			ChequeBook chequeBook) throws Exception {
		if (null == payment.getPaymentId())
			SystemBL.saveReferenceStamp(Payment.class.getName(),
					getImplementationId());
		if (null != chequeBook && !("").equals(chequeBook))
			chequeBookBL.saveChequeBook(chequeBook);
		paymentService.savePayment(payment, paymentDetailList);
		invoiceBL.getInvoiceService().saveInvoice(invoiceList);
		Date currentDate = DateFormat.convertStringToDate(DateFormat
				.convertSystemDateToString(new Date()));
		if (null != payment.getChequeDate()
				&& (payment.getChequeDate().before(currentDate) || payment
						.getChequeDate().equals(currentDate))) {
			payment.setPaymentDetails(new HashSet<PaymentDetail>(
					paymentDetailList));
			List<Payment> purchasePayment = new ArrayList<Payment>();
			purchasePayment.add(payment);
			alertBL.alertPurchasePaymentProcess(purchasePayment);
		} else if ((payment.getDate().before(currentDate) || payment.getDate()
				.equals(currentDate))) {
			payment.setPaymentDetails(new HashSet<PaymentDetail>(
					paymentDetailList));
			List<Payment> purchasePayment = new ArrayList<Payment>();
			purchasePayment.add(payment);
			alertBL.alertPurchasePaymentProcess(purchasePayment);
		}
	}

	public void payableDirectPayment(DirectPaymentVO vo) {

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;

			List<Currency> currencyList = null;
			if (vo.getObject() != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplementationId());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplementationId());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(vo.getRecordId());
				directPayment.setUseCase(vo.getUseCase());
				// Direct Payment details creation
				DirectPaymentVO dVO = null;
				if (vo.getUseCase().equalsIgnoreCase(
						Payment.class.getSimpleName())) {
					dVO = getDirectPaymentTransaction(vo);
				}

				if (dVO != null && dVO.getDirectPayments() != null
						&& dVO.getDirectPayments().size() > 0) {
					directPayment.setPaymentDate(dVO.getPaymentDate());
					directPayment.setCombination(dVO.getCombination());
					directPayment.setCurrency(dVO.getCurrency());
					directPayment.setPaymentMode(dVO.getPaymentMode());
					directPayment.setChequeBook(dVO.getChequeBook());
					directPayment.setChequeDate(dVO.getChequeDate());
					directPayment.setChequeDate(dVO.getChequeDate());
					directPayment.setChequeNumber(dVO.getChequeNumber());
					directPayment.setBankAccount(dVO.getBankAccount());
					directPayment.setSupplier(dVO.getSupplier());
					directPayment.setDescription(dVO.getDescription());
					directPayment
							.setPersonByPersonId(dVO.getPersonByPersonId());
					directPayment
							.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
									dVO.getDirectPayments()));
				}

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public DirectPaymentVO getDirectPaymentTransaction(DirectPaymentVO paymentVO)
			throws Exception {
		List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
		DirectPaymentVO directPaymentVO = new DirectPaymentVO();
		Payment payment = paymentService.getPurchasePaymentDetails(paymentVO
				.getRecordId());
		DirectPaymentDetail directPaymentDetail = null;
		for (PaymentDetail paymentDetail : payment.getPaymentDetails()) {
			directPaymentDetail = new DirectPaymentDetail();
			directPaymentDetail.setAmount(paymentDetail.getInvoiceDetail()
					.getInvoiceQty()
					* paymentDetail.getInvoiceDetail().getReceiveDetail()
							.getUnitRate());
			Combination depositcombination = null;
			if (payment.getSupplier().getCombination() != null) {
				depositcombination = new Combination();
				depositcombination.setCombinationId(payment.getSupplier()
						.getCombination().getCombinationId());
				depositcombination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAllAccountDetail(
								depositcombination.getCombinationId());
				directPaymentDetail.setCombination(depositcombination);
			}
			if (paymentDetail.getInvoiceDetail() != null
					&& paymentDetail.getInvoiceDetail().getInvoice() != null)
				directPaymentDetail.setInvoiceNumber(paymentDetail
						.getInvoiceDetail().getInvoice().getInvoiceNumber());
			directPaymentDetail.setDescription(paymentDetail.getDescription());
			directPayDetails.add(directPaymentDetail);
		}

		directPaymentVO.setPaymentDate(payment.getDate());
		directPaymentVO.setBankAccount(payment.getBankAccount());
		if (payment.getBankAccount() == null
				|| payment.getBankAccount().getBankAccountId() == null)
			directPaymentVO.setCombination(payment.getCombination());

		directPaymentVO.setCurrency(payment.getCurrency());
		directPaymentVO.setPaymentMode(payment.getPaymentMode());
		directPaymentVO.setChequeBook(payment.getChequeBook());
		directPaymentVO.setChequeDate(payment.getChequeDate());
		directPaymentVO.setChequeDate(payment.getChequeDate());
		directPaymentVO.setChequeNumber(payment.getChequeNumber());
		directPaymentVO.setDirectPayments(directPayDetails);
		directPaymentVO.setSupplier(payment.getSupplier());
		directPaymentVO.setDescription(payment.getDescription());
		return directPaymentVO;
	}

	// Purchase Payment Transaction
	public void createPurchasePaymentTransaction(Payment payment,
			List<PaymentDetail> paymentDetailList) throws Exception {
		Map<Long, List<InvoiceDetail>> invoiceDetailMap = new HashMap<Long, List<InvoiceDetail>>();
		Map<Long, List<ReceiveDetail>> receiveDetailMap = new HashMap<Long, List<ReceiveDetail>>();
		Map<Long, Purchase> purchaseMap = new HashMap<Long, Purchase>();
		List<ReceiveDetail> receiveDetailList = null;
		List<InvoiceDetail> invoiceDetailList = null;
		for (PaymentDetail paymentDetail : paymentDetailList) {
			if (invoiceDetailMap.containsKey(paymentDetail.getInvoiceDetail()
					.getReceiveDetail().getPurchase().getPurchaseId())) {
				invoiceDetailList = invoiceDetailMap.get(paymentDetail
						.getInvoiceDetail().getReceiveDetail().getPurchase()
						.getPurchaseId());
				invoiceDetailList.add(paymentDetail.getInvoiceDetail());
			} else {
				invoiceDetailList = new ArrayList<InvoiceDetail>();
				invoiceDetailList.add(paymentDetail.getInvoiceDetail());
			}
			invoiceDetailMap.put(paymentDetail.getInvoiceDetail()
					.getReceiveDetail().getPurchase().getPurchaseId(),
					invoiceDetailList);
			if (receiveDetailMap.containsKey(paymentDetail.getInvoiceDetail()
					.getInvoiceDetailId())) {
				receiveDetailList = receiveDetailMap.get(paymentDetail
						.getInvoiceDetail().getInvoiceDetailId());
				receiveDetailList.add(paymentDetail.getInvoiceDetail()
						.getReceiveDetail());
			} else {
				receiveDetailList = new ArrayList<ReceiveDetail>();
				receiveDetailList.add(paymentDetail.getInvoiceDetail()
						.getReceiveDetail());
			}
			receiveDetailMap.put(paymentDetail.getInvoiceDetail()
					.getInvoiceDetailId(), receiveDetailList);
			purchaseMap.put(paymentDetail.getInvoiceDetail().getReceiveDetail()
					.getPurchase().getPurchaseId(), paymentDetail
					.getInvoiceDetail().getReceiveDetail().getPurchase());
		}
		for (Entry<Long, List<InvoiceDetail>> invoiceDetail : invoiceDetailMap
				.entrySet()) {
			double amount = 0;
			double netAmount = 0;
			List<InvoiceDetail> invoiceDetails = invoiceDetailMap
					.get(invoiceDetail.getKey());
			if (null != invoiceDetails && invoiceDetails.size() > 0) {
				for (InvoiceDetail invDetail : invoiceDetails) {
					List<ReceiveDetail> receiveDetails = receiveDetailMap
							.get(invDetail.getInvoiceDetailId());
					for (ReceiveDetail rcvDetail : receiveDetails) {
						amount += invDetail.getInvoiceQty()
								* rcvDetail.getUnitRate();
					}
				}
			}
			List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
			Purchase purchase = purchaseMap.get(invoiceDetail.getKey());
			Transaction transaction = transactionBL.createTransaction(purchase
					.getCurrency().getCurrencyId(),
					"Inventory Payment by" + payment.getChequeNumber()
							+ " & Payment No." + payment.getPaymentNumber(),
					payment.getDate(), transactionBL
							.getCategory("Inventory Payment"), payment
							.getPaymentId(), Payment.class.getSimpleName());
			TransactionDetail debitTransactionDetail = this
					.getTransactionBL()
					.createTransactionDetail(
							amount,
							payment.getSupplier().getCombination()
									.getCombinationId(),
							true,
							(null != payment.getSupplier().getPerson() ? payment
									.getSupplier().getPerson().getFirstName()
									+ " "
									+ payment.getSupplier().getPerson()
											.getLastName()
									: (null != payment.getSupplier()
											.getCmpDeptLocation() ? payment
											.getSupplier().getCmpDeptLocation()
											.getCompany().getCompanyName()
											: payment.getSupplier()
													.getCompany()
													.getCompanyName())
											.concat(" Debited for Inventory Payment(Payment No : "
													+ payment
															.getPaymentNumber()
													+ ")")), null, null);
			transactionDetailList.add(debitTransactionDetail);
			if (null != payment.getBankAccount()
					&& !("").equals(payment.getBankAccount())) {
				if (null != purchase.getCreditTerm()
						&& !("").equals(purchase.getCreditTerm())) {
					if (purchase.getCreditTerm().getDiscount() > 0) {
						TransactionDetail discountTransactionDetail = createDiscountTransaction(
								payment, amount, purchase.getCreditTerm());
						if (null != discountTransactionDetail
								&& !("").equals(discountTransactionDetail)) {
							transactionDetailList
									.add(discountTransactionDetail);
							netAmount = amount
									- discountTransactionDetail.getAmount();
						} else {
							netAmount = amount;
						}
					} else {
						netAmount = amount;
					}
				} else {
					netAmount = amount;
				}
				TransactionDetail credittransactionDetail = this
						.getTransactionBL()
						.createTransactionDetail(
								netAmount,
								payment.getCombination().getCombinationId(),
								false,
								(null != payment.getCombination()
										.getAccountByAnalysisAccountId() ? payment
										.getCombination()
										.getAccountByAnalysisAccountId()
										.getAccount()
										: payment.getCombination()
												.getAccountByNaturalAccountId()
												.getAccount())
										.concat(" Credited for Inventory Payment(Payment No :"
												+ payment.getPaymentNumber()
												+ ")"), null, null);
				transactionDetailList.add(credittransactionDetail);
			} else {
				if (null != purchase.getCreditTerm()
						&& !("").equals(purchase.getCreditTerm())) {
					if (purchase.getCreditTerm().getDiscount() > 0) {
						TransactionDetail discountTransactionDetail = createDiscountTransaction(
								payment, amount, purchase.getCreditTerm());
						if (null != discountTransactionDetail
								&& !("").equals(discountTransactionDetail)) {
							transactionDetailList
									.add(discountTransactionDetail);
							netAmount = amount
									- discountTransactionDetail.getAmount();
						} else {
							netAmount = amount;
						}
					} else {
						netAmount = amount;
					}
				} else {
					netAmount = amount;
				}
				TransactionDetail credittransactionDetail = this
						.getTransactionBL()
						.createTransactionDetail(
								netAmount,
								payment.getCombination().getCombinationId(),
								false,
								(null != payment.getCombination()
										.getAccountByAnalysisAccountId() ? payment
										.getCombination()
										.getAccountByAnalysisAccountId()
										.getAccount()
										: payment.getCombination()
												.getAccountByNaturalAccountId()
												.getAccount())
										.concat(" Credited for Inventory Payment(Payment No :"
												+ payment.getPaymentNumber()
												+ ")"), null, null);
				transactionDetailList.add(credittransactionDetail);
			}
			transactionBL.saveTransaction(transaction, transactionDetailList);
		}
	}

	// Revert Purchase Payment Transaction
	public void revertPurchaseTransaction(Payment payment,
			List<PaymentDetail> paymentDetailList) throws Exception {
		Map<Long, List<InvoiceDetail>> invoiceDetailMap = new HashMap<Long, List<InvoiceDetail>>();
		Map<Long, List<ReceiveDetail>> receiveDetailMap = new HashMap<Long, List<ReceiveDetail>>();
		Map<Long, Purchase> purchaseMap = new HashMap<Long, Purchase>();
		List<ReceiveDetail> receiveDetailList = null;
		List<InvoiceDetail> invoiceDetailList = null;
		for (PaymentDetail paymentDetail : paymentDetailList) {
			if (invoiceDetailMap.containsKey(paymentDetail.getInvoiceDetail()
					.getReceiveDetail().getPurchase().getPurchaseId())) {
				invoiceDetailList = invoiceDetailMap.get(paymentDetail
						.getInvoiceDetail().getReceiveDetail().getPurchase()
						.getPurchaseId());
				invoiceDetailList.add(paymentDetail.getInvoiceDetail());
			} else {
				invoiceDetailList = new ArrayList<InvoiceDetail>();
				invoiceDetailList.add(paymentDetail.getInvoiceDetail());
			}
			invoiceDetailMap.put(paymentDetail.getInvoiceDetail()
					.getReceiveDetail().getPurchase().getPurchaseId(),
					invoiceDetailList);
			if (receiveDetailMap.containsKey(paymentDetail.getInvoiceDetail()
					.getInvoiceDetailId())) {
				receiveDetailList = receiveDetailMap.get(paymentDetail
						.getInvoiceDetail().getInvoiceDetailId());
				receiveDetailList.add(paymentDetail.getInvoiceDetail()
						.getReceiveDetail());
			} else {
				receiveDetailList = new ArrayList<ReceiveDetail>();
				receiveDetailList.add(paymentDetail.getInvoiceDetail()
						.getReceiveDetail());
			}
			receiveDetailMap.put(paymentDetail.getInvoiceDetail()
					.getInvoiceDetailId(), receiveDetailList);
			purchaseMap.put(paymentDetail.getInvoiceDetail().getReceiveDetail()
					.getPurchase().getPurchaseId(), paymentDetail
					.getInvoiceDetail().getReceiveDetail().getPurchase());
		}
		for (Entry<Long, List<InvoiceDetail>> invoiceDetail : invoiceDetailMap
				.entrySet()) {
			double amount = 0;
			double netAmount = 0;
			List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
			List<InvoiceDetail> invoiceDetails = invoiceDetailMap
					.get(invoiceDetail.getKey());
			if (null != invoiceDetails && invoiceDetails.size() > 0) {
				for (InvoiceDetail invDetail : invoiceDetails) {
					List<ReceiveDetail> receiveDetails = receiveDetailMap
							.get(invDetail.getInvoiceDetailId());
					for (ReceiveDetail rcvDetail : receiveDetails) {
						amount += invDetail.getInvoiceQty()
								* rcvDetail.getUnitRate();
					}
				}
			}
			Purchase purchase = purchaseMap.get(invoiceDetail.getKey());
			Transaction transaction = transactionBL.createTransaction(
					purchase.getCurrency().getCurrencyId(),
					"Inventory Reverted Payment Ref: Cheq.No"
							+ payment.getChequeNumber() + " & Payment No."
							+ payment.getPaymentNumber(), payment.getDate(),
					transactionBL.getCategory("Inventory Payment"), payment
							.getPaymentId(), Payment.class.getSimpleName());
			TransactionDetail creditTransactionDetail = this
					.getTransactionBL()
					.createTransactionDetail(
							amount,
							payment.getSupplier().getCombination()
									.getCombinationId(),
							false,
							(null != payment.getSupplier().getPerson() ? payment
									.getSupplier().getPerson().getFirstName()
									+ " "
									+ payment.getSupplier().getPerson()
											.getLastName()
									: (null != payment.getSupplier()
											.getCmpDeptLocation() ? payment
											.getSupplier().getCmpDeptLocation()
											.getCompany().getCompanyName()
											: payment.getSupplier()
													.getCompany()
													.getCompanyName())
											.concat(" Credited for Inventory Reverted Payment(Payment No : "
													+ payment
															.getPaymentNumber()
													+ ")")), null, null);
			transactionDetailList.add(creditTransactionDetail);
			if (null != payment.getBankAccount()
					&& !("").equals(payment.getBankAccount())) {
				if (null != purchase.getCreditTerm()
						&& !("").equals(purchase.getCreditTerm())) {
					if (purchase.getCreditTerm().getDiscount() > 0) {
						TransactionDetail discountTransactionDetail = createDiscountTransaction(
								payment, amount, purchase.getCreditTerm());
						if (null != discountTransactionDetail
								&& !("").equals(discountTransactionDetail)) {
							transactionDetailList
									.add(discountTransactionDetail);
							netAmount = amount
									- discountTransactionDetail.getAmount();
						} else {
							netAmount = amount;
						}
					} else {
						netAmount = amount;
					}
				} else {
					netAmount = amount;
				}
				TransactionDetail debittransactionDetail = this
						.getTransactionBL()
						.createTransactionDetail(
								netAmount,
								payment.getCombination().getCombinationId(),
								true,
								(null != payment.getCombination()
										.getAccountByAnalysisAccountId() ? payment
										.getCombination()
										.getAccountByAnalysisAccountId()
										.getAccount()
										: payment.getCombination()
												.getAccountByNaturalAccountId()
												.getAccount())
										.concat(" Debited for Inventory Reverted Payment(Payment No :"
												+ payment.getPaymentNumber()
												+ ")"), null, null);
				transactionDetailList.add(debittransactionDetail);
			} else {
				if (null != purchase.getCreditTerm()
						&& !("").equals(purchase.getCreditTerm())) {
					if (purchase.getCreditTerm().getDiscount() > 0) {
						TransactionDetail discountTransactionDetail = createDiscountTransaction(
								payment, amount, purchase.getCreditTerm());
						if (null != discountTransactionDetail
								&& !("").equals(discountTransactionDetail)) {
							transactionDetailList
									.add(discountTransactionDetail);
							netAmount = amount
									- discountTransactionDetail.getAmount();
						} else {
							netAmount = amount;
						}
					} else {
						netAmount = amount;
					}
				} else {
					netAmount = amount;
				}
				TransactionDetail debittransactionDetail = this
						.getTransactionBL()
						.createTransactionDetail(
								amount,
								payment.getCombination().getCombinationId(),
								false,
								(null != payment.getCombination()
										.getAccountByAnalysisAccountId() ? payment
										.getCombination()
										.getAccountByAnalysisAccountId()
										.getAccount()
										: payment.getCombination()
												.getAccountByNaturalAccountId()
												.getAccount())
										.concat(" Debited for Inventory Reverted Payment(Payment No :"
												+ payment.getPaymentNumber()
												+ ")"), null, null);
				transactionDetailList.add(debittransactionDetail);
			}
			transactionBL.saveTransaction(transaction, transactionDetailList);
		}
	}

	// Create or Update Purchase Payment Transaction
	public void updatePaymentTrascation(Payment payment, long alertId)
			throws Exception {
		createPurchasePaymentTransaction(payment, new ArrayList<PaymentDetail>(
				payment.getPaymentDetails()));
		Alert alert = new Alert();
		alert = alertBL.getAlertService().getAlertInfo(alertId);
		alert.setIsActive(false);
		alertBL.commonSaveAlert(alert);
		this.getPaymentService().updatePayment(payment);
	}

	// Revert Purchase Payment Transaction & Delete Purchase Payment
	public void deletePaymementTransaction(Payment payment) throws Exception {
		List<Invoice> invoices = new ArrayList<Invoice>();
		Invoice invoice = null;
		for (PaymentDetail paymentDetail : payment.getPaymentDetails()) {
			invoice = new Invoice();
			invoice = paymentDetail.getInvoiceDetail().getInvoice();
			invoice.setStatus(Constants.Accounts.InvoiceStatus.Active.getCode());
			invoices.add(invoice);
		}
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				payment.getPaymentId(), "Payment");
		if (null != alert)
			alertBL.getAlertService().deleteAlert(alert);
		invoiceBL.getInvoiceService().saveInvoice(invoices);
		/*
		 * if (null != payment.getPrintTaken() && payment.getPrintTaken())
		 * transactionBL.deleteTransaction(payment.getPaymentId(),
		 * Payment.class.getSimpleName());
		 */
		directPaymentBL.deleteDirectPayment(payment.getPaymentId(),
				Payment.class.getSimpleName());
		paymentService.deletePayment(payment, new ArrayList<PaymentDetail>(
				payment.getPaymentDetails()));
	}

	// Discount Transaction
	private TransactionDetail createDiscountTransaction(Payment payment,
			double amount, CreditTerm terms) throws Exception {
		if (null != terms.getDiscount() && !("0").equals(terms.getDiscount())) {
			long discountDays = DateFormat.dayVariance(payment.getDate(),
					new Date());
			discountAmount = 0;
			if (discountDays >= terms.getDiscountDay()) {
				discountAmount = (amount / 100) * terms.getDiscount();
				return transactionBL.createTransactionDetail(discountAmount,
						getImplementationId().getRevenueAccount(), false,
						"Discount Received", null, null);
			}
		}
		return null;
	}

	public Map<String, String> getModeOfPayment() throws Exception {
		Map<String, String> modeOfPayments = new HashMap<String, String>();
		String key = "";
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class)) {
			if ((byte) ModeOfPayment.Cash.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cash.getCode().toString().concat("@@")
						.concat("CSH");
			else if ((byte) ModeOfPayment.Cheque.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cheque.getCode().toString().concat("@@")
						.concat("CHQ");
			else if ((byte) ModeOfPayment.Credit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Credit_Card.getCode().toString()
						.concat("@@").concat("CCD");
			else if ((byte) ModeOfPayment.Debit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Debit_Card.getCode().toString()
						.concat("@@").concat("DCD");
			else if ((byte) ModeOfPayment.Wire_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Wire_Transfer.getCode().toString()
						.concat("@@").concat("WRT");
			else if ((byte) ModeOfPayment.Inter_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Inter_Transfer.getCode().toString()
						.concat("@@").concat("IAT");
			modeOfPayments.put(key, modeOfPayment.name().replaceAll("_", " "));
		}
		return modeOfPayments;
	}

	/*
	 * public void postTransaction(Payment payment, List<PaymentDetail>
	 * paymentDetailList) throws Exception {
	 * this.getPaymentService().savePayment(payment, paymentDetailList);
	 * List<TransactionDetail> detailList = new ArrayList<TransactionDetail>();
	 * for (PaymentDetail plist : paymentDetailList) { LoanCharge charges =
	 * this.getLoanService().getLoanCharges(
	 * plist.getLoanCharge().getOtherChargeId()); List<Category> categoryList =
	 * this .getTransactionBL() .getCategoryBL() .getCategoryService()
	 * .getAllActiveCategories(
	 * plist.getLoanCharge().getLoan().getImplementation()); Category category =
	 * new Category(); if (null != categoryList) { for (Category list :
	 * categoryList) { if (list.getName().equalsIgnoreCase("Payment Detail")) {
	 * category.setCategoryId(list.getCategoryId()); break; } } } if (null ==
	 * category || category.equals("")) { category = this .getTransactionBL()
	 * .getCategoryBL() .createCategory( plist.getLoanCharge().getLoan()
	 * .getImplementation(), "Payment Detail"); } else if (null ==
	 * category.getCategoryId()) { category = this .getTransactionBL()
	 * .getCategoryBL() .createCategory( plist.getLoanCharge().getLoan()
	 * .getImplementation(), "Payment Detail"); } Transaction transaction =
	 * getTransactionBL().createTransaction(
	 * charges.getLoan().getCurrency().getCurrencyId(), charges.getName(),
	 * plist.getPayment().getDate(), category); TransactionDetail
	 * transctionDetail = getTransactionBL() .createTransactionDetail(
	 * charges.getValue(), plist.getCombination().getCombinationId(), false,
	 * "Loan(" + charges.getLoan().getLoanNumber() + ") Credited for charges",
	 * null); detailList.add(transctionDetail); transctionDetail =
	 * getTransactionBL().createTransactionDetail( charges.getValue(),
	 * charges.getCombination().getCombinationId(), true, "Loan(" +
	 * charges.getLoan().getLoanNumber() + ") Debited for charges", null);
	 * detailList.add(transctionDetail);
	 * getTransactionBL().saveTransaction(transaction, detailList); } }
	 */

	public Implementation getImplementationId() {
		Implementation implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public PaymentService getPaymentService() {
		return paymentService;
	}

	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public BankBL getBankBL() {
		return bankBL;
	}

	public void setBankBL(BankBL bankBL) {
		this.bankBL = bankBL;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public InvoiceBL getInvoiceBL() {
		return invoiceBL;
	}

	public void setInvoiceBL(InvoiceBL invoiceBL) {
		this.invoiceBL = invoiceBL;
	}

	public ChequeBookBL getChequeBookBL() {
		return chequeBookBL;
	}

	public void setChequeBookBL(ChequeBookBL chequeBookBL) {
		this.chequeBookBL = chequeBookBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

}
