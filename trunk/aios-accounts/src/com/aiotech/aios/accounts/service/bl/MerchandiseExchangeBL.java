package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.accounts.domain.entity.vo.MerchandiseExchangeVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.accounts.service.MerchandiseExchangeService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class MerchandiseExchangeBL {

	// Dependencies
	private MerchandiseExchangeService merchandiseExchangeService;

	public List<Object> showAllMerchandiseExchange() throws Exception {
		List<MerchandiseExchange> merchandiseExchanges = merchandiseExchangeService
				.getAllMerchandiseExchange(getImplementation());
		List<Object> merchandiseExchangeVOs = new ArrayList<Object>();
		if (null != merchandiseExchanges && merchandiseExchanges.size() > 0) {
			for (MerchandiseExchange merchandiseExchange : merchandiseExchanges)
				merchandiseExchangeVOs
						.add(addMerchandiseExchange(merchandiseExchange));
		}
		return merchandiseExchangeVOs;
	}

	private MerchandiseExchangeVO addMerchandiseExchange(
			MerchandiseExchange merchandiseExchange) {
		MerchandiseExchangeVO merchandiseExchangeVO = new MerchandiseExchangeVO();
		merchandiseExchangeVO.setMerchandiseExchangeId(merchandiseExchange
				.getMerchandiseExchangeId());
		merchandiseExchangeVO.setReferenceNumber(merchandiseExchange
				.getReferenceNumber());
		merchandiseExchangeVO.setExchangeDate(DateFormat
				.convertDateToString(merchandiseExchange.getDate().toString()));
		return merchandiseExchangeVO;
	}

	// Get Merchandise Exchange By Reference
	public MerchandiseExchange getMerchandiseExchangeByReference(
			String exchangeReference) throws Exception {
		return merchandiseExchangeService.getMerchandiseExchangeByReference(
				exchangeReference, getImplementation());
	}

	// Get Return Product Details
	public List<PointOfSaleVO> getReturnProductDetails(
			Map<String, Object> object) throws Exception {
		if (null != object && object.size() > 0) {
			Map<Long, PointOfSaleVO> pointOfSaleVOs = new HashMap<Long, PointOfSaleVO>();
			List<MerchandiseExchangeDetail> exchangeDetails = null;
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
			PointOfSaleDetailVO pointOfSaleDetailVO = null;
			PointOfSaleVO pointOfSaleVO = null;
			for (Object exchangeId : object.values()) {
				exchangeDetails = merchandiseExchangeService
						.getExchangeDetailByExchangeId((Long) exchangeId);
				for (MerchandiseExchangeDetail exchangeDetail : exchangeDetails) {
					pointOfSaleDetailVO = new PointOfSaleDetailVO();
					BeanUtils.copyProperties(pointOfSaleDetailVO,
							exchangeDetail.getPointOfSaleDetail());
					pointOfSaleDetailVO.setReturnQuantity(exchangeDetail
							.getReturnQuantity());
					if (pointOfSaleVOs.containsKey(exchangeDetail
							.getPointOfSaleDetail().getPointOfSale()
							.getPointOfSaleId())) {
						pointOfSaleVO = pointOfSaleVOs.get(exchangeDetail
								.getPointOfSaleDetail().getPointOfSale()
								.getPointOfSaleId());
						pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>(
								pointOfSaleVO.getPointOfSaleDetailVOs());
					} else {
						pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
						pointOfSaleVO = new PointOfSaleVO();
						BeanUtils.copyProperties(pointOfSaleVO, exchangeDetail
								.getPointOfSaleDetail().getPointOfSale());
					}
					pointOfSaleDetailVOs.add(pointOfSaleDetailVO);
					pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
					pointOfSaleVOs.put(pointOfSaleVO.getPointOfSaleId(),
							pointOfSaleVO);
				}
			}
			return (new ArrayList<PointOfSaleVO>(pointOfSaleVOs.values()));
		}
		return null;
	}

	// Save Merchandise Exchange
	public void saveMerchandiseExchange(
			MerchandiseExchange merchandiseExchange,
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails)
			throws Exception {
		merchandiseExchange.setReferenceNumber(SystemBL.saveReferenceStamp(
				MerchandiseExchange.class.getName(), getImplementation()));
		merchandiseExchange.setImplementation(getImplementation());
		merchandiseExchangeService.saveMerchandiseExchange(merchandiseExchange,
				merchandiseExchangeDetails);
	}

	public void deleteMerchandiseExchange(
			List<MerchandiseExchange> merchandiseExchanges,
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails)
			throws Exception {
		merchandiseExchangeService.deleteMerchandiseExchanges(
				merchandiseExchanges, merchandiseExchangeDetails);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public MerchandiseExchangeService getMerchandiseExchangeService() {
		return merchandiseExchangeService;
	}

	public void setMerchandiseExchangeService(
			MerchandiseExchangeService merchandiseExchangeService) {
		this.merchandiseExchangeService = merchandiseExchangeService;
	}
}
