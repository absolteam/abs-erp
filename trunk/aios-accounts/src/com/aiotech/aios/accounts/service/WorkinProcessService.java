package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.WorkinProcess;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessDetail;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessProduction;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class WorkinProcessService {

	private AIOTechGenericDAO<WorkinProcess> workinProcessDAO;
	private AIOTechGenericDAO<WorkinProcessProduction> workinProcessProductionDAO;
	private AIOTechGenericDAO<WorkinProcessDetail> workinProcessDetailDAO;

	public List<WorkinProcess> getAllWorkInProcess(Implementation implementation)
			throws Exception {
		return workinProcessDAO.findByNamedQuery("getAllWorkInProcess",
				implementation);
	}

	public WorkinProcess getWorkinProcessById(long workinProcessId)
			throws Exception {
		return workinProcessDAO.findByNamedQuery("getWorkinProcessById",
				workinProcessId).get(0);
	}

	public List<WorkinProcessProduction> getWorkinProductionByRequsitionDetailId(
			long productionRequsitionDetailId) throws Exception {
		return workinProcessProductionDAO.findByNamedQuery(
				"getWorkinProductionByRequsitionDetailId",
				productionRequsitionDetailId);
	}

	public List<WorkinProcessProduction> getProductionByRequsitionDetailId(
			long productionRequsitionDetailId, long workinProcessProductionId)
			throws Exception {
		return workinProcessProductionDAO.findByNamedQuery(
				"getProductionByRequsitionDetailId",
				productionRequsitionDetailId, workinProcessProductionId);
	}

	public List<WorkinProcessDetail> getWorkinProcessDetailById(
			long workinProcessId) throws Exception {
		return workinProcessDetailDAO.findByNamedQuery(
				"getWorkinProcessDetailById", workinProcessId);
	}

	public void deleteWorkinProcess(WorkinProcess workinProcess,
			List<WorkinProcessProduction> workinProcessProductions,
			List<WorkinProcessDetail> workinProcessDetails) throws Exception {
		workinProcessDetailDAO.deleteAll(workinProcessDetails);
		workinProcessProductionDAO.deleteAll(workinProcessProductions);
		workinProcessDAO.delete(workinProcess);
	}

	public void deleteWorkinProcessDetails(
			List<WorkinProcessDetail> deletedWorkinProcessDetails)
			throws Exception {
		workinProcessDetailDAO.deleteAll(deletedWorkinProcessDetails);
	}

	public void deleteWorkinProcessProduction(
			List<WorkinProcessProduction> deletedWorkinProcessProductions,
			List<WorkinProcessDetail> deletedWorkinProcessDetails)
			throws Exception {
		workinProcessDetailDAO.deleteAll(deletedWorkinProcessDetails);
		workinProcessProductionDAO.deleteAll(deletedWorkinProcessProductions);
	}

	public void saveWorkInProcess(WorkinProcess workinProcess,
			List<WorkinProcessProduction> workinProcessProductions,
			List<WorkinProcessDetail> workinProcessDetails) throws Exception {
		workinProcessDAO.saveOrUpdate(workinProcess);
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions)
			workinProcessProduction.setWorkinProcess(workinProcess);
		workinProcessProductionDAO.saveOrUpdateAll(workinProcessProductions);
		workinProcessDetailDAO.saveOrUpdateAll(workinProcessDetails);
	}

	@SuppressWarnings("unchecked")
	public List<WorkinProcess> getFilteredWorkingProcess(
			Implementation implementation, Long personId, Long productId,
			Date fromDate, Date toDate) throws Exception {

		Criteria criteria = workinProcessDAO.createCriteria();

		criteria.createAlias("person", "prs", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("workinProcessProductions", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.shelf", "fromslf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("fromslf.shelf", "frack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("frack.aisle", "fasl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("fasl.store", "fstr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("productionRequisition", "req",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("req.productionRequisitionDetails", "reqDetail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("req.person", "person",
				CriteriaSpecification.LEFT_JOIN);
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("person.personId", personId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("processDate", fromDate, toDate));
		}

		return criteria.list();
	}

	// Getters & Setters
	public AIOTechGenericDAO<WorkinProcess> getWorkinProcessDAO() {
		return workinProcessDAO;
	}

	public void setWorkinProcessDAO(
			AIOTechGenericDAO<WorkinProcess> workinProcessDAO) {
		this.workinProcessDAO = workinProcessDAO;
	}

	public AIOTechGenericDAO<WorkinProcessDetail> getWorkinProcessDetailDAO() {
		return workinProcessDetailDAO;
	}

	public void setWorkinProcessDetailDAO(
			AIOTechGenericDAO<WorkinProcessDetail> workinProcessDetailDAO) {
		this.workinProcessDetailDAO = workinProcessDetailDAO;
	}

	public AIOTechGenericDAO<WorkinProcessProduction> getWorkinProcessProductionDAO() {
		return workinProcessProductionDAO;
	}

	public void setWorkinProcessProductionDAO(
			AIOTechGenericDAO<WorkinProcessProduction> workinProcessProductionDAO) {
		this.workinProcessProductionDAO = workinProcessProductionDAO;
	}
}
