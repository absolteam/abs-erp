package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.VoidPayment;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class VoidPaymentService {

	private AIOTechGenericDAO<VoidPayment> voidPaymentDAO;

	// fetch all void payments
	public List<VoidPayment> getAllVoidPayment(Implementation implementation)
			throws Exception {
		return this.getVoidPaymentDAO().findByNamedQuery("findAllVoidPayment",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<VoidPayment> getVoidPaymentsByDirectPayments(
			Implementation implementation) throws Exception {
		return this.getVoidPaymentDAO().findByNamedQuery(
				"getVoidPaymentsByDirectPayments", implementation);
	}

	// fetch void payment by id
	public VoidPayment getVoidPaymentById(long voidPaymentId) throws Exception {
		return this.getVoidPaymentDAO()
				.findByNamedQuery("getVoidPaymentById", voidPaymentId).get(0);
	}

	public VoidPayment findVoidPaymentById(long voidPaymentId) throws Exception {
		return voidPaymentDAO.findById(voidPaymentId);
	}

	// save void payment
	public void saveVoidPayment(VoidPayment voidPayment) throws Exception {
		voidPaymentDAO.saveOrUpdate(voidPayment);
	}

	// delete void payment
	public void deleteVoidPayment(VoidPayment voidPayment) throws Exception {
		this.getVoidPaymentDAO().delete(voidPayment);
	}

	public AIOTechGenericDAO<VoidPayment> getVoidPaymentDAO() {
		return voidPaymentDAO;
	}

	public void setVoidPaymentDAO(AIOTechGenericDAO<VoidPayment> voidPaymentDAO) {
		this.voidPaymentDAO = voidPaymentDAO;
	}
}
