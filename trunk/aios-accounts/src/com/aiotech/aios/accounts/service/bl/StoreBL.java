package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.AisleVO;
import com.aiotech.aios.accounts.domain.entity.vo.ShelfVO;
import com.aiotech.aios.accounts.domain.entity.vo.StoreVO;
import com.aiotech.aios.accounts.service.StoreService;
import com.aiotech.aios.common.to.Constants.Accounts.StoreBinType;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;
import com.opensymphony.xwork2.ActionContext;

public class StoreBL {
	private LookupMasterBL lookupMasterBL;
	private StoreService storeService;
	private AIOTechGenericDAO<Person> personDAO;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public JSONObject getStoreList(Implementation implementation)
			throws Exception {
		List<Store> stores = this.getStoreService()
				.getAllStores(implementation);
		if (null != stores && !stores.equals("")) {
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Store list : stores) {
				array = new JSONArray();
				array.add(list.getStoreId());
				array.add(list.getStoreNumber());
				array.add(list.getStoreName());
				array.add(list.getPerson().getFirstName()
						.concat(" " + list.getPerson().getLastName()));
				array.add(list.getAddress());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			return jsonResponse;
		}
		return null;
	}

	// Get store details
	public List<Object> getStoreDetails(Implementation implementation,
			long storeId, long personId) throws Exception {
		List<Object> storeObject = new ArrayList<Object>();
		List<Store> stores = this.getStoreService().getStoreByCriteria(
				implementation, storeId, personId);
		StoreVO storeVO = null;
		if (null != stores && stores.size() > 0) {
			for (Store store : stores) {
				storeVO = new StoreVO();
				storeVO.setStoreId(store.getStoreId());
				storeVO.setStoreName(store.getStoreName());
				storeVO.setStoreNumber(store.getStoreNumber());
				storeVO.setAddress(store.getAddress());
				storeVO.setPersonId(store.getPerson().getPersonId());
				storeVO.setPersonNumber(store.getPerson().getPersonNumber());
				storeVO.setPersonName(store.getPerson().getFirstName()
						.concat(" ").concat(store.getPerson().getLastName()));
				storeVO.setStorageType(null != store.getLookupDetail() ? store
						.getLookupDetail().getDisplayName() : "");
				storeObject.add(storeVO);
			}
		}
		return storeObject;
	}

	// Get store details
	public List<Object> showStoreDetails(Implementation implementation)
			throws Exception {
		List<Object> storeObject = new ArrayList<Object>();
		List<Store> stores = this.getStoreService()
				.getAllStores(implementation);
		StoreVO storeVO = null;
		if (null != stores && stores.size() > 0) {
			for (Store store : stores) {
				storeVO = new StoreVO();
				storeVO.setStoreId(store.getStoreId());
				storeVO.setStoreName(store.getStoreName());
				storeVO.setStoreNumber(store.getStoreNumber());
				storeVO.setAddress(store.getAddress());
				storeVO.setPersonId(store.getPerson().getPersonId());
				storeVO.setPersonNumber(store.getPerson().getPersonNumber());
				storeVO.setPersonName(store.getPerson().getFirstName()
						.concat(" ").concat(store.getPerson().getLastName()));
				storeVO.setStorageType(null != store.getLookupDetail() ? store
						.getLookupDetail().getDisplayName() : "");
				storeObject.add(storeVO);
			}
		}
		return storeObject;
	}

	// Save Store Details
	public void saveStore(Store store, List<Aisle> aisles,
			Map<Integer, Map<Integer, ShelfVO>> shelfVOs,
			List<Shelf> deletedRacks, List<Shelf> deletedShelfs)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		store.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (store != null && store.getStoreId() != null
				&& store.getStoreId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		storeService.saveStoreDetais(store, aisles);
		List<Shelf> shelfDetails = new ArrayList<Shelf>();
		Map<Integer, ShelfVO> shelfVOMap = null;
		for (Entry<Integer, Map<Integer, ShelfVO>> shelfVOsMap : shelfVOs
				.entrySet()) {
			shelfVOMap = shelfVOsMap.getValue();
			for (Entry<Integer, ShelfVO> shelfVO : shelfVOMap.entrySet()) {
				shelfDetails.add(addShelfDetails(shelfVO.getValue()));
			}
		}
		if (null != shelfDetails && shelfDetails.size() > 0) {
			storeService.saveShelfDetails(shelfDetails);
			List<Shelf> shelfRacks = addRackDetails(shelfDetails);
			if (null != shelfRacks && shelfRacks.size() > 0)
				storeService.saveShelfDetails(shelfRacks);
		}
		if (null != deletedShelfs && deletedShelfs.size() > 0)
			storeService.deleteShelfs(deletedShelfs);
		if (null != deletedRacks && deletedRacks.size() > 0) {
			List<Shelf> shelfs = new ArrayList<Shelf>();
			for (Shelf shelf : shelfDetails) {
				if (null != shelf.getShelf())
					shelfs.add(shelf);
			}
			storeService.deleteRacks(shelfs, deletedRacks);
		}
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Store.class.getSimpleName(), store.getStoreId(), user,
				workflowDetailVo);
	}
	
	public void doStoreBusinessUpdate(Long storeId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				Store store = storeService.getStoreDetailByStoreId(
						storeId);
				List<Aisle> aisles = new ArrayList<Aisle>(store.getAisles());
				List<Shelf> shelfs = new ArrayList<Shelf>();
				List<Shelf> racks = new ArrayList<Shelf>();
				if (null != aisles && aisles.size() > 0) {
					for (Aisle aisle : aisles) {
						if (null != aisle.getShelfs())
							shelfs.addAll(aisle.getShelfs());
					}
				}
				if (null != shelfs && shelfs.size() > 0) {
					for (Shelf shelf : shelfs) {
						if (null != shelf.getShelfs())
							racks.addAll(shelf.getShelfs());
					}
				}
				storeService.deleteStore(store, aisles, shelfs, racks);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<Byte, StoreBinType> getStoreBinTypes() {
		Map<Byte, StoreBinType> storeBinTypes = new HashMap<Byte, StoreBinType>();
		for (StoreBinType storeBinType : EnumSet.allOf(StoreBinType.class))
			storeBinTypes.put(storeBinType.getCode(), storeBinType);
		return storeBinTypes;
	}

	// Add Rack Details
	public List<Shelf> addRackDetails(List<Shelf> shelfDetails) {
		List<Shelf> racks = new ArrayList<Shelf>();
		for (Shelf shelf : shelfDetails) {
			if (null != shelf.getShelfs()) {
				for (Shelf rack : shelf.getShelfs()) {
					rack.setShelf(shelf);
					rack.setAisle(shelf.getAisle());
					racks.add(rack);
				}
			}
		}
		return racks;
	}

	// Add Shelf Details
	public List<Shelf> addShelfDetails(List<Shelf> shelfDetails) {
		List<Shelf> racks = new ArrayList<Shelf>();
		if (null != shelfDetails && shelfDetails.size() > 0) {
			for (Shelf shelf : shelfDetails) {
				shelf.setShelf(shelf);
				shelf.setAisle(shelf.getAisle());
				racks.add(shelf);
			}
		}
		return racks;
	}

	// Add Shelf Details
	public Shelf addShelfDetails(ShelfVO shelfVO) {
		Shelf shelf = new Shelf();
		shelf.setDimension(shelfVO.getDimension());
		shelf.setShelfType(shelfVO.getShelfType());
		shelf.setShelfId(null != shelfVO.getShelfId()
				&& shelfVO.getShelfId() > 0 ? shelfVO.getShelfId() : null);
		shelf.setName(shelfVO.getName());
		shelf.setAisle(shelfVO.getAisle());
		if (null != shelfVO.getRackDetails()
				&& shelfVO.getRackDetails().size() > 0) {
			Shelf rackDetail = null;
			List<Shelf> rackDetailList = new ArrayList<Shelf>();
			for (ShelfVO rackDetails : shelfVO.getRackDetails()) {
				rackDetail = new Shelf();
				rackDetail.setName(rackDetails.getName());
				rackDetail.setShelfType(rackDetails.getShelfType());
				rackDetail.setDimension(rackDetails.getDimension());
				rackDetail.setShelfId(null != rackDetails.getShelfId()
						&& rackDetails.getShelfId() > 0 ? rackDetails
						.getShelfId() : null);
				rackDetailList.add(rackDetail);
			}
			shelf.setShelfs(new HashSet<Shelf>(rackDetailList));
		}
		return shelf;
	}

	// Delete Store Information
	public void deleteStore(Store store) throws Exception {
		List<Aisle> aisles = new ArrayList<Aisle>(store.getAisles());
		List<Shelf> shelfs = new ArrayList<Shelf>();
		List<Shelf> racks = new ArrayList<Shelf>();
		if (null != aisles && aisles.size() > 0) {
			for (Aisle aisle : aisles) {
				if (null != aisle.getShelfs())
					shelfs.addAll(aisle.getShelfs());
			}
		}
		if (null != shelfs && shelfs.size() > 0) {
			for (Shelf shelf : shelfs) {
				if (null != shelf.getShelfs())
					racks.addAll(shelf.getShelfs());
			}
		}
		storeService.deleteStore(store, aisles, shelfs, racks);
	}

	// Add to Store vo's
	public List<StoreVO> addStoreVos(List<Store> stores) throws Exception {
		List<StoreVO> storeVOs = new ArrayList<StoreVO>();
		for (Store store : stores) {
			storeVOs.add(addStores(store));
		}
		return storeVOs;
	}

	// Add to store vo
	private StoreVO addStores(Store store) throws Exception {
		StoreVO storeVO = new StoreVO();
		BeanUtils.copyProperties(storeVO, store);
		AisleVO aisleVO = null;
		List<AisleVO> aisleVOs = new ArrayList<AisleVO>();
		for (Aisle aisle : store.getAisles()) {
			aisleVO = addAisle(aisle);
			aisleVOs.add(aisleVO);
		}

		if (null != aisleVOs && aisleVOs.size() > 0) {
			Map<Integer, ShelfVO> shelfHashMap = null;
			for (AisleVO aisle : aisleVOs) {
				if (null != aisle.getShelfVOs()
						&& aisle.getShelfVOs().size() > 0) {
					shelfHashMap = new HashMap<Integer, ShelfVO>();
					for (ShelfVO shelfVO : aisle.getShelfVOs()) {
						shelfVO.setShelfTypeName(StoreBinType.get(
								shelfVO.getShelfType()).name());
						if (null != shelfVO.getRackDetails()
								&& shelfVO.getRackDetails().size() > 0) {
							for (ShelfVO rackVO : shelfVO.getRackDetails()) {
								rackVO.setShelfTypeName(StoreBinType.get(
										rackVO.getShelfType()).name());
								if (!shelfHashMap.containsKey(rackVO
										.getShelfId())) {
									shelfHashMap.put(rackVO.getShelfId(),
											rackVO);
								}
							}
						}
					}
					List<ShelfVO> finalShelfVOs = new ArrayList<ShelfVO>();
					for (ShelfVO shelfVO : aisle.getShelfVOs()) {
						if (!shelfHashMap.containsKey(shelfVO.getShelfId())) {
							finalShelfVOs.add(shelfVO);
						}
					}
					aisle.setShelfVOs(finalShelfVOs);
				}
			}
			Collections.sort(aisleVOs, new Comparator<Aisle>() {
				public int compare(Aisle o1, Aisle o2) {
					return o1.getAisleId().compareTo(o2.getAisleId());
				}
			});
		}
		storeVO.setAisleVOs(aisleVOs);
		return storeVO;
	}

	// Add to Aisle vo
	private AisleVO addAisle(Aisle aisle) throws Exception {
		AisleVO aisleVO = new AisleVO();
		BeanUtils.copyProperties(aisleVO, aisle);
		if (null != aisle.getShelfs() && aisle.getShelfs().size() > 0) {
			List<ShelfVO> shelfVos = new ArrayList<ShelfVO>();
			for (Shelf shelf : aisle.getShelfs()) {
				shelfVos.add(addShelf(shelf));
			}
			Collections.sort(shelfVos, new Comparator<Shelf>() {
				public int compare(Shelf s1, Shelf s2) {
					return s1.getShelfId().compareTo(s2.getShelfId());
				}
			});
			aisleVO.setShelfVOs(shelfVos);
		}
		return aisleVO;
	}

	// Add to Shelf vo
	private ShelfVO addShelf(Shelf shelf) throws Exception {
		ShelfVO shelfVO = new ShelfVO();
		BeanUtils.copyProperties(shelfVO, shelf);
		List<ShelfVO> rackVos = new ArrayList<ShelfVO>();
		if (null != shelf.getShelfs() && shelf.getShelfs().size() > 0) {
			for (Shelf rack : shelf.getShelfs()) {
				rackVos.add(addRack(rack));
			}
			Collections.sort(rackVos, new Comparator<Shelf>() {
				public int compare(Shelf s1, Shelf s2) {
					return s1.getShelfId().compareTo(s2.getShelfId());
				}
			});
			shelfVO.setRackDetails(rackVos);
		}
		return shelfVO;
	}

	private ShelfVO addRack(Shelf rack) throws Exception {
		ShelfVO shelfVO = new ShelfVO();
		BeanUtils.copyProperties(shelfVO, rack);
		return shelfVO;
	}

	public StoreService getStoreService() {
		return storeService;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
