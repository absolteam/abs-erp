/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceCharge;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * Sales Invoice Service
 * 
 * @author Saleem
 */
public class SalesInvoiceService {

	private AIOTechGenericDAO<SalesInvoice> salesInvoiceDAO;
	private AIOTechGenericDAO<SalesInvoiceDetail> salesInvoiceDetailDAO;
	private AIOTechGenericDAO<SalesInvoiceCharge> salesInvoiceChargeDAO;

	// Get all Sales Invoice
	public List<SalesInvoice> getAllSalesInvoice(Implementation implementation)
			throws Exception {
		return salesInvoiceDAO.findByNamedQuery("getAllSalesInvoice",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get all Sales Invoice
	public List<SalesInvoice> getAllContiniosSalesInvoice(
			Implementation implementation) throws Exception {
		return salesInvoiceDAO.findByNamedQuery("getAllContiniosSalesInvoice",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get all Sales Invoice
	public List<SalesInvoice> getSalesInvoiceByInvoice(long invoiceId)
			throws Exception {
		return salesInvoiceDAO.findByNamedQuery("getSalesInvoiceByInvoice",
				invoiceId);
	}

	// Get Sales Invoice Detail by Invoice Id
	public List<SalesInvoiceDetail> getSalesInvoiceDetailByInvoiceId(
			long salesInvoiceId) throws Exception {
		return salesInvoiceDetailDAO.findByNamedQuery(
				"getSalesInvoiceDetailByInvoiceId", salesInvoiceId);
	}

	// Get Sales Invoice Detail by Invoice Detail Id
	public SalesInvoiceDetail getSalesInvoiceDetail(long salesInvoiceDetailId)
			throws Exception {
		return salesInvoiceDetailDAO.findById(salesInvoiceDetailId);
	}

	// Get Sales Invoice Detail by Invoice Detail Id
	public SalesInvoiceDetail getSalesInvoiceDetailByInvoiceDetailId(
			long salesInvoiceDetailId) throws Exception {
		List<SalesInvoiceDetail> salesInvoiceDetail = salesInvoiceDetailDAO
				.findByNamedQuery("getSalesInvoiceDetailByInvoiceDetailId",
						salesInvoiceDetailId);
		return null != salesInvoiceDetail && salesInvoiceDetail.size() > 0 ? salesInvoiceDetail
				.get(0) : null;
	}

	// Save Sales Invoice
	public void saveSalesInvoice(SalesInvoice salesInvoice,
			List<SalesInvoiceDetail> salesInvoiceDetails) throws Exception {
		salesInvoiceDAO.saveOrUpdate(salesInvoice);
		for (SalesInvoiceDetail salesInvoiceDetail : salesInvoiceDetails) {
			salesInvoiceDetail.setSalesInvoice(salesInvoice);
		}
		salesInvoiceDetailDAO.saveOrUpdateAll(salesInvoiceDetails);
	}

	// Save Sales Invoice
	public void saveSalesInvoice(SalesInvoice salesInvoice) throws Exception {
		salesInvoiceDAO.saveOrUpdate(salesInvoice);
	}

	// Save Sales Invoice
	public void saveSalesInvoiceDetails(
			List<SalesInvoiceDetail> salesInvoiceDetails) throws Exception {
		salesInvoiceDetailDAO.saveOrUpdateAll(salesInvoiceDetails);
	}

	// Delete Sales Invoice
	public void deleteSalesInvoice(SalesInvoice salesInvoice,
			List<SalesInvoiceDetail> salesInvoiceDetails) throws Exception {
		salesInvoiceDetailDAO.deleteAll(salesInvoiceDetails);
		salesInvoiceDAO.delete(salesInvoice);
	}

	public void deleteSalesInvoice(List<SalesInvoice> salesInvoices) {
		salesInvoiceDAO.deleteAll(salesInvoices);
	}

	// Delete Sales Invoice Details
	public void deleteSalesInvoiceDetails(
			List<SalesInvoiceDetail> salesInvoiceDetails) throws Exception {
		salesInvoiceDetailDAO.deleteAll(salesInvoiceDetails);
	}

	// Get Sales Invoice and Invoice Details
	public SalesInvoice getSalesInvoiceById(long salesInvoiceId)
			throws Exception {
		return salesInvoiceDAO.findByNamedQuery("getSalesInvoiceById",
				salesInvoiceId).get(0);
	}

	// Get Sales Invoice and Invoice Details
	public SalesInvoice getSalesInvoiceDetailById(long salesInvoiceId)
			throws Exception {
		return salesInvoiceDAO.findByNamedQuery("getSalesInvoiceDetailById",
				salesInvoiceId).get(0);
	}

	// Get Sales Invoice and Invoice Details
	public SalesInvoice getInvoiceById(long salesInvoiceId) throws Exception {
		return salesInvoiceDAO.findById(salesInvoiceId);
	}

	@SuppressWarnings("unchecked")
	public List<SalesInvoice> getFilteredSalesInvoice(
			Implementation implementation, Long salesInvoiceId,
			Long customerId, Byte status, Date fromDate, Date toDate)
			throws Exception {
		Criteria criteria = salesInvoiceDAO.createCriteria();
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.shippingDetails", "sd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesInvoiceDetails", "sid",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sid.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("currency", "cur", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("creditTerm", "ct",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sid.salesDeliveryDetail", "salesDelivery",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesDelivery.salesDeliveryNote", "deliverNote",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesInvoice", "sv",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sv.salesInvoiceDetails", "svsid",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("svsid.product", "prd1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("svsid.salesDeliveryDetail", "svsalesDelivery",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("svsalesDelivery.salesDeliveryNote",
				"svdeliverNote", CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (salesInvoiceId != null && salesInvoiceId > 0) {
			criteria.add(Restrictions.eq("salesInvoiceId", salesInvoiceId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("c.customerId", customerId));
		}

		if (status != null && status > 0) {
			criteria.add(Restrictions.eq("status", status));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("invoiceDate", fromDate, toDate));
		} else if (fromDate != null) {
			criteria.add(Restrictions.ge("invoiceDate", fromDate));
		}

		return criteria.list();
	}

	public List<SalesInvoice> getCustomerActiveInvoices(long customerId,
			long currencyId, byte invoiceStatus) throws Exception {
		return salesInvoiceDAO.findByNamedQuery("getCustomerActiveInvoices",
				customerId, currencyId, invoiceStatus,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Getters and Setters
	public AIOTechGenericDAO<SalesInvoice> getSalesInvoiceDAO() {
		return salesInvoiceDAO;
	}

	public void setSalesInvoiceDAO(
			AIOTechGenericDAO<SalesInvoice> salesInvoiceDAO) {
		this.salesInvoiceDAO = salesInvoiceDAO;
	}

	public AIOTechGenericDAO<SalesInvoiceDetail> getSalesInvoiceDetailDAO() {
		return salesInvoiceDetailDAO;
	}

	public void setSalesInvoiceDetailDAO(
			AIOTechGenericDAO<SalesInvoiceDetail> salesInvoiceDetailDAO) {
		this.salesInvoiceDetailDAO = salesInvoiceDetailDAO;
	}

	public AIOTechGenericDAO<SalesInvoiceCharge> getSalesInvoiceChargeDAO() {
		return salesInvoiceChargeDAO;
	}

	public void setSalesInvoiceChargeDAO(
			AIOTechGenericDAO<SalesInvoiceCharge> salesInvoiceChargeDAO) {
		this.salesInvoiceChargeDAO = salesInvoiceChargeDAO;
	}
}
