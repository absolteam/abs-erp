package com.aiotech.aios.accounts.service.bl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CreditDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditNoteVO;
import com.aiotech.aios.accounts.service.CreditService;
import com.aiotech.aios.accounts.service.SalesInvoiceService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.to.Constants.ReturnProcessType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CreditBL {

	private ProductBL productBL;
	private TransactionBL transactionBL;
	private CreditService creditService;
	private CustomerBL customerBL;
	private SalesDeliveryNoteBL salesDeliveryNoteBL;
	private SalesInvoiceService salesInvoiceService;
	private ProductPricingBL productPricingBL;
	private DirectPaymentBL directPaymentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get Credit Notes list
	public List<Object> getAllCredits(Implementation implementation)
			throws Exception {
		List<Object> creditNotes = new ArrayList<Object>();
		List<Credit> creditNoteList = creditService
				.getAllCreditNotes(implementation);
		SalesDeliveryNote salesDeliveryNote = null;
		if (null != creditNoteList && creditNoteList.size() > 0) {
			CreditNoteVO creditNoteVO = null;
			for (Credit credit : creditNoteList) {
				creditNoteVO = new CreditNoteVO();
				creditNoteVO.setCreditDate(DateFormat
						.convertDateToString(credit.getDate().toString()));
				creditNoteVO.setCreditId(credit.getCreditId());
				creditNoteVO.setCustomerReference(credit.getCustomer()
						.getCustomerNumber());
				creditNoteVO.setCustomerName(null != credit.getCustomer()
						.getPersonByPersonId() ? credit
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat("")
						.concat(credit.getCustomer().getPersonByPersonId()
								.getLastName()) : (null != credit.getCustomer()
						.getCompany() ? credit.getCustomer().getCompany()
						.getCompanyName() : credit.getCustomer()
						.getCmpDeptLocation().getCompany().getCompanyName()));
				creditNoteVO.setCreditNumber(credit.getCreditNumber());
				creditNoteVO.setDescription(credit.getDescription());
				if (null != credit.getUseCase()
						&& credit.getUseCase().equalsIgnoreCase(
								SalesDeliveryNote.class.getSimpleName())) {
					salesDeliveryNote = salesDeliveryNoteBL
							.getSalesDeliveryNoteService()
							.getSalesDeliveryNoteById(credit.getRecordId());
					creditNoteVO.setSalesReference(salesDeliveryNote
							.getReferenceNumber());
				}
				creditNotes.add(creditNoteVO);
			}
		}
		return creditNotes;
	}

	public CreditNoteVO convertCreditToVO(Credit credit) {
		CreditNoteVO creditNoteVO = new CreditNoteVO();
		BeanUtils.copyProperties(credit, creditNoteVO);
		if (credit.getCustomer() != null) {
			creditNoteVO.setCustomerReference(credit.getCustomer()
					.getCustomerNumber());
			creditNoteVO.setCustomerName(null != credit.getCustomer()
					.getCompany() ? credit.getCustomer().getCompany()
					.getCompanyName() : credit
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat(" ")
					.concat(credit.getCustomer().getPersonByPersonId()
							.getLastName()));
		}
		creditNoteVO
				.setCreditDetailVOs(convertCreditDetailToVOs(new ArrayList<CreditDetail>(
						credit.getCreditDetails())));
		if (credit.getCreatedDate() != null)
			creditNoteVO.setCreditDate(DateFormat.convertDateToString(credit
					.getCreatedDate() + ""));
		if (credit.getIsDirectPayment() != null && credit.getIsDirectPayment())
			creditNoteVO.setProcessType(ReturnProcessType.DirectPayment.name());
		else if (credit.getIsDirectPayment() == null)
			creditNoteVO.setProcessType(ReturnProcessType.FreeReturn.name());
		else
			creditNoteVO.setProcessType(ReturnProcessType.Exchange.name());

		Double totalDeduction = 0.0;
		for (CreditDetailVO creditDetailVos : creditNoteVO.getCreditDetailVOs()) {
			if (creditDetailVos.getDeduction() != null)
				totalDeduction += creditDetailVos.getDeduction();
		}
		creditNoteVO.setTotalDeduction(totalDeduction);

		return creditNoteVO;
	}

	public List<CreditDetailVO> convertCreditDetailToVOs(
			List<CreditDetail> creditDetails) {
		List<CreditDetailVO> creditDetailVOs = new ArrayList<CreditDetailVO>();
		CreditDetailVO creditDetailVO = null;
		for (CreditDetail creditDetail : creditDetails) {
			creditDetailVO = new CreditDetailVO();

			creditDetailVO.setCreditDetailId(creditDetail.getCreditDetailId());
			creditDetailVO.setReturnQuantity(creditDetail.getReturnQuantity());

			creditDetailVO.setProductId(creditDetail.getProduct()
					.getProductId());
			creditDetailVO.setProductCode(creditDetail.getProduct().getCode());
			creditDetailVO.setProductName(creditDetail.getProduct()
					.getProductName());
			creditDetailVO.setShelfId(creditDetail.getShelf().getShelfId());
			creditDetailVO.setShelfName(creditDetail.getShelf().getShelf()
					.getAisle().getStore().getStoreName()
					+ ">>"
					+ creditDetail.getShelf().getShelf().getAisle()
							.getSectionName()
					+ ">>"
					+ creditDetail.getShelf().getShelf().getName()
					+ ">>"
					+ creditDetail.getShelf().getName());
			creditDetailVO.setUnitRate(creditDetail.getUnitRate());
			creditDetailVO.setReturnAmount(creditDetail.getReturnAmount());
			creditDetailVO.setDescription(creditDetail.getDescription());

			creditDetailVOs.add(creditDetailVO);
		}
		return creditDetailVOs;
	}

	// Manipulate Credit Detail
	public List<CreditDetailVO> manipulateCreditDetail(
			List<CreditDetail> creditDetails) {
		List<CreditDetailVO> creditDetailVOs = new ArrayList<CreditDetailVO>();
		CreditDetailVO creditDetailVO = null;
		double invoiceQuantity = 0;
		for (CreditDetail creditDetail : creditDetails) {
			creditDetailVO = new CreditDetailVO();
			creditDetailVO.setSalesDeliveryDetailId(creditDetail
					.getSalesDeliveryDetail().getSalesDeliveryDetailId());
			creditDetailVO.setCreditDetailId(creditDetail.getCreditDetailId());
			creditDetailVO.setReturnQuantity(creditDetail.getReturnQuantity());
			creditDetailVO.setDeliveryQuantity(creditDetail
					.getSalesDeliveryDetail().getDeliveryQuantity());
			creditDetailVO.setProductId(creditDetail.getSalesDeliveryDetail()
					.getProduct().getProductId());
			creditDetailVO.setProductName(creditDetail.getSalesDeliveryDetail()
					.getProduct().getProductName());
			creditDetailVO.setShelfId(creditDetail.getSalesDeliveryDetail()
					.getShelf().getShelfId());
			creditDetailVO.setShelfName(creditDetail.getSalesDeliveryDetail()
					.getShelf().getShelf().getAisle().getStore().getStoreName()
					+ ">>"
					+ creditDetail.getSalesDeliveryDetail().getShelf()
							.getShelf().getAisle().getSectionName()
					+ ">>"
					+ creditDetail.getSalesDeliveryDetail().getShelf()
							.getShelf().getName()
					+ ">>"
					+ creditDetail.getSalesDeliveryDetail().getShelf()
							.getName());
			creditDetailVO.setUnitRate(creditDetail.getSalesDeliveryDetail()
					.getUnitRate());
			if (null != creditDetail.getSalesDeliveryDetail().getDiscount()
					&& creditDetail.getSalesDeliveryDetail().getDiscount() > 0) {
				double discountAmount = 0;
				if (creditDetail.getSalesDeliveryDetail().getIsPercentage()) {
					discountAmount = ((creditDetail.getSalesDeliveryDetail()
							.getUnitRate() * creditDetail
							.getSalesDeliveryDetail().getDeliveryQuantity()) * creditDetail
							.getSalesDeliveryDetail().getDiscount()) / 100;
				} else {
					discountAmount = creditDetail.getSalesDeliveryDetail()
							.getDiscount();
				}
				creditDetailVO.setUnitRate(creditDetailVO.getUnitRate()
						- discountAmount);
			}
			invoiceQuantity = 0;
			if (null != creditDetail.getSalesDeliveryDetail()
					.getSalesInvoiceDetails()) {
				for (SalesInvoiceDetail salesInvoiceDetail : creditDetail
						.getSalesDeliveryDetail().getSalesInvoiceDetails()) {
					invoiceQuantity += salesInvoiceDetail.getInvoiceQuantity();
				}
				creditDetailVO.setInvoiceQuantity(invoiceQuantity);
			}
			creditDetailVO
					.setInwardedQuantity(creditDetail.getReturnQuantity());
			creditDetailVO.setReturnQuantity(null != creditDetailVO
					.getInwardedQuantity() ? creditDetailVO
					.getInwardedQuantity() - creditDetailVO.getReturnQuantity()
					: creditDetailVO.getReturnQuantity());
			creditDetailVO.setDescription(creditDetail.getDescription());
			creditDetailVO.setCreditDetailId(creditDetail.getCreditDetailId());

			creditDetailVOs.add(creditDetailVO);
		}
		return creditDetailVOs;
	}

	// Save Credit Details
	public void saveCreditNote(Credit credit, List<CreditDetail> creditDetails,
			List<CreditDetail> existingCreditDetails) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		credit.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		boolean updateFlag = false;
		if (null != credit.getCreditId())
			updateFlag = true;
		if (updateFlag) {
			creditService.deleteCreditDetails(existingCreditDetails);
			transactionBL.deleteTransaction(credit.getCreditId(),
					Credit.class.getSimpleName());
		} else
			SystemBL.saveReferenceStamp(Credit.class.getName(),
					credit.getImplementation());

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (credit != null && credit.getCreditId() != null
				&& credit.getCreditId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		// Update stock details
		salesDeliveryNoteBL.getStockBL().updateStockDetails(null,
				updateStockDetails(existingCreditDetails));

		credit.setCreditDetails(new HashSet<CreditDetail>(creditDetails));

		creditService.saveCreditNote(credit, creditDetails);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Credit.class.getSimpleName(), credit.getCreditId(), user,
				workflowDetailVo);
	}

	public void saveSalesReturn(Credit credit, List<CreditDetail> creditDetails)
			throws Exception {

		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			credit.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());

			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (credit != null && credit.getCreditId() != null
					&& credit.getCreditId() > 0) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}

			if (credit.getCreditId() == null || credit.getCreditId() == 0)
				credit.setCreditNumber(SystemBL.saveReferenceStamp(
						Credit.class.getName(), credit.getImplementation()));

			this.getCreditService().saveCreditNote(credit, creditDetails);

			// Stock Updates
			// Increase the stock what ever reduced before on save
			List<Stock> editStocks = new ArrayList<Stock>();
			Stock stock = null;
			Shelf shelf = null;
			for (CreditDetail detail : creditDetails) {
				stock = new Stock();
				if (detail.getShelf() != null) {
					shelf = salesDeliveryNoteBL.getStockBL().getStoreBL()
							.getStoreService()
							.getStoreByShelfId(detail.getShelf().getShelfId());
					stock.setShelf(shelf);
				}
				stock.setStore(shelf.getShelf().getAisle().getStore());
				stock.setProduct(detail.getProduct());
				stock.setQuantity(detail.getReturnQuantity());
				stock.setImplementation(getImplementation());
				editStocks.add(stock);
			}
			salesDeliveryNoteBL.getStockBL().updateStockDetails(editStocks,
					null);

			// JV Transactions
			if (null != credit.getIsDirectPayment()) {
				performeSalesReturnJVTransaction(credit, creditDetails);
			} else {
				performeSalesReturnJVTransactionForFreeReturn(credit,
						creditDetails);
			}

			// Payment Alert
			if (null != credit.getIsDirectPayment()
					&& credit.getIsDirectPayment()) {
				createSalesReturnDirectPaymentAlert(credit);
			}

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Credit.class.getSimpleName(), credit.getCreditId(), user,
					workflowDetailVo);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createSalesReturnDirectPaymentAlert(Credit credit)
			throws Exception {
		Alert alert = transactionBL
				.getAlertBL()
				.getAlertService()
				.getAlertRecordInfo(credit.getCreditId(),
						Credit.class.getSimpleName());
		if (null != alert) {
			alert.setIsActive(true);
			transactionBL.getAlertBL().commonSaveAlert(alert);
		} else {
			List<Credit> credits = new ArrayList<Credit>();
			credits.add(credit);
			transactionBL.getAlertBL().salesReturnDirectPayment(credits);
		}
	}

	public void salesReturnDirectPayment(Credit credit,
			List<CreditDetail> creditDetails) {

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;
			Combination combination = null;
			if (credit.getCustomer() != null) {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								credit.getCustomer().getCombination()
										.getCombinationId());
			} else {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								getImplementation().getAccountsReceivable());
			}
			List<Currency> currencyList = null;
			if (credit != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplementation());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplementation());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(credit.getCreditId());
				directPayment.setUseCase(Credit.class.getSimpleName());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				DirectPaymentDetail dpDetail = null;
				dpDetail = new DirectPaymentDetail();
				dpDetail.setAmount(credit.getTotalReturn());
				dpDetail.setCombination(combination);
				directPayDetails.add(dpDetail);
				directPayment.setCustomer(credit.getCustomer());
				directPayment.setDescription("Sales Return Reference "
						+ credit.getCreditNumber());
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public void doCreditBusinessUpdate(Long creditId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			Credit credit = creditService.getCreditNoteById(creditId);
			if (!credit.getUseCase().equals(Project.class.getSimpleName())) {// Project
																				// Skip
																				// call
				List<CreditDetail> creditDetails = new ArrayList<CreditDetail>(
						credit.getCreditDetails());
				if (!workflowDetailVO.isDeleteFlag()) {
					// Create Transaction
					List<TransactionDetail> transactionDetails = createTransactionDetail(
							credit, creditDetails,
							TransactionType.Debit.getCode(),
							TransactionType.Credit.getCode());
					Transaction transaction = creatTransaction(credit, "",
							transactionDetails);

					// Sales Unearned Revenue Transaction
					List<TransactionDetail> unearnedRevenueCustomer = createUnearnedRevenueTransaction(
							credit, creditDetails,
							TransactionType.Debit.getCode(),
							TransactionType.Credit.getCode());
					Transaction customerTransaction = creatTransaction(credit,
							"", transactionDetails);

					transactionBL.saveTransaction(transaction,
							transactionDetails);
					transactionBL.saveTransaction(customerTransaction,
							unearnedRevenueCustomer);

					// Update stock details
					salesDeliveryNoteBL.getStockBL().updateStockDetails(
							updateStockDetails(creditDetails), null);

				} else {
					transactionBL.deleteTransaction(credit.getCreditId(),
							Credit.class.getSimpleName());
					// Update stock details
					List<Stock> stocks = null;
					if (null != creditDetails && creditDetails.size() > 0) {
						Stock stock = null;
						stocks = new ArrayList<Stock>();
						for (CreditDetail creditDetail : creditDetails) {
							if (null != creditDetail.getShelf()) {
								stock = new Stock();
								stock.setProduct(creditDetail.getProduct());
								stock.setStore(creditDetail.getShelf()
										.getShelf().getAisle().getStore());
								stock.setShelf(creditDetail
										.getSalesDeliveryDetail().getShelf());
								stock.setQuantity(creditDetail
										.getReturnQuantity());
								stocks.add(stock);
							}
						}
					}
					salesDeliveryNoteBL.getStockBL().updateStockDetails(null,
							stocks);
					creditService.deleteCreditNote(credit, creditDetails);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void performeSalesReturnJVTransaction(Credit credit,
			List<CreditDetail> creditDetails) throws Exception {

		// Transaction
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalDeduction = 0.0;
		// Inventory Debit
		for (CreditDetail detail : creditDetails) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					detail.getReturnAmount(),
					productBL.getProductInventory(
							detail.getProduct().getProductId())
							.getCombinationId(), TransactionType.Debit
							.getCode(), null, null, credit.getCreditNumber()));
			totalDeduction += (detail.getDeduction() != null ? detail
					.getDeduction() : 0.0);
		}

		// Revenue credit
		if (totalDeduction > 0)
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalDeduction, getImplementation().getRevenueAccount(),
					TransactionType.Credit.getCode(), null, null, credit.getCreditNumber()));

		// Customer Credit
		if (credit.getCustomer() != null)
			transactionDetails.add(transactionBL.createTransactionDetail(
					credit.getTotalReturn(), credit.getCustomer()
							.getCombination().getCombinationId(),
					TransactionType.Credit.getCode(), null, null, credit.getCreditNumber()));
		else
			transactionDetails.add(transactionBL.createTransactionDetail(credit
					.getTotalReturn(), getImplementation()
					.getAccountsReceivable(), TransactionType.Credit.getCode(),
					null, null, credit.getCreditNumber()));

		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "", new Date(), transactionBL
				.getCategory("Sales Return"), credit.getCreditId(),
				Credit.class.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);

	}

	public void performeSalesReturnJVTransactionForFreeReturn(Credit credit,
			List<CreditDetail> creditDetails) throws Exception {

		// Transaction
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalRevenue = 0.0;
		// Inventory Debit
		for (CreditDetail detail : creditDetails) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					detail.getReturnAmount(),
					productBL.getProductInventory(
							detail.getProduct().getProductId())
							.getCombinationId(), TransactionType.Debit
							.getCode(), null, null, credit.getCreditNumber()));
			totalRevenue += (detail.getReturnAmount() != null ? detail
					.getReturnAmount() : 0.0);
		}

		// Revenue credit
		if (totalRevenue > 0)
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalRevenue, getImplementation().getRevenueAccount(),
					TransactionType.Credit.getCode(), null, null,credit.getCreditNumber()));

		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "", new Date(), transactionBL
				.getCategory("Sales Return(Free)"), credit.getCreditId(),
				Credit.class.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);

	}

	private List<Stock> updateStockDetails(List<CreditDetail> creditDetails)
			throws Exception {
		List<Stock> stocks = null;
		if (null != creditDetails && creditDetails.size() > 0) {
			Stock stock = null;
			stocks = new ArrayList<Stock>();
			for (CreditDetail creditDetail : creditDetails) {
				SalesDeliveryDetail salesDeliveryDetail = salesDeliveryNoteBL
						.getSalesDeliveryNoteService()
						.getSalesDeliveryDetailByDetailId(
								creditDetail.getSalesDeliveryDetail()
										.getSalesDeliveryDetailId());
				stock = new Stock();
				stock.setProduct(salesDeliveryDetail.getProduct());
				stock.setStore(salesDeliveryDetail.getShelf().getShelf()
						.getAisle().getStore());
				stock.setShelf(salesDeliveryDetail.getShelf());
				stock.setQuantity(creditDetail.getReturnQuantity());
				stocks.add(stock);
			}
		}
		return stocks;
	}

	// Create Transaction
	private Transaction creatTransaction(Credit credit, String description,
			List<TransactionDetail> transactionDetails) throws Exception {
		return transactionBL.createTransaction(credit.getCurrency()
				.getCurrencyId(), description, credit.getDate(), transactionBL
				.getCategory("Credit Note"), credit.getCreditId(), Credit.class
				.getSimpleName());
	}

	// Create Sales Delivery Note Transaction Details
	private List<TransactionDetail> createTransactionDetail(Credit credit,
			List<CreditDetail> creditDetails, Boolean debitFlag,
			Boolean creditFlag) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		TransactionDetail transactionDetail = null;
		ProductPricingDetail productPricingDetail = null;
		double basePrice = 0;
		Shelf shelf = null;
		for (CreditDetail creditDetail : creditDetails) {
			SalesDeliveryDetail salesDeliveryDetail = salesDeliveryNoteBL
					.getSalesDeliveryNoteService()
					.getSalesDeliveryDetailByDetailId(
							creditDetail.getSalesDeliveryDetail()
									.getSalesDeliveryDetailId());
			shelf = salesDeliveryNoteBL
					.getStockBL()
					.getStoreBL()
					.getStoreService()
					.getStoreByShelfId(
							salesDeliveryDetail.getShelf().getShelfId());
			productPricingDetail = productPricingBL.getProductPricingDetail(
					salesDeliveryDetail.getProduct().getProductId(), shelf
							.getShelf().getAisle().getStore().getStoreId());
			if (null != productPricingDetail)
				basePrice = productPricingDetail.getBasicCostPrice()
						* salesDeliveryDetail.getDeliveryQuantity();
			else
				basePrice = salesDeliveryDetail.getUnitRate()
						* salesDeliveryDetail.getDeliveryQuantity();

			// Inventory Account
			Combination inventoryAccount = productBL
					.getProductInventory(salesDeliveryDetail.getProduct()
							.getProductId());
			transactionDetail = transactionBL.createTransactionDetail(
					basePrice, inventoryAccount.getCombinationId(), debitFlag,
					null, null,credit.getCreditNumber());
			transactionDetails.add(transactionDetail);
			// Expense Account
			Combination expenseAccount = new Combination();
			expenseAccount.setCombinationId(getImplementation()
					.getExpenseAccount());
			transactionDetail = transactionBL.createTransactionDetail(
					basePrice, expenseAccount.getCombinationId(), creditFlag,
					null, null,credit.getCreditNumber());
			transactionDetails.add(transactionDetail); 
		}
		return transactionDetails;
	}

	// Create Unearned Revenue Sales Delivery Note Transaction Details
	private List<TransactionDetail> createUnearnedRevenueTransaction(
			Credit creditNote, List<CreditDetail> CreditDetails,
			Boolean debitFlag, Boolean creditFlag) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		TransactionDetail transactionDetail = null;
		double transactionAmount = 0;
		Combination unearnedRevenueAccount = new Combination();
		unearnedRevenueAccount.setCombinationId(getImplementation()
				.getUnearnedRevenue());
		for (CreditDetail creditDetail : CreditDetails) {
			SalesDeliveryDetail salesDeliveryDetail = salesDeliveryNoteBL
					.getSalesDeliveryNoteService()
					.getSalesDeliveryDetailByDetailId(
							creditDetail.getSalesDeliveryDetail()
									.getSalesDeliveryDetailId());
			// Unearned Revenue Account
			transactionDetail = transactionBL.createTransactionDetail(
					salesDeliveryDetail.getUnitRate()
							* salesDeliveryDetail.getDeliveryQuantity(),
					unearnedRevenueAccount.getCombinationId(), debitFlag, null,
					null,creditNote.getCreditNumber());
			transactionAmount += transactionDetail.getAmount();
			transactionDetails.add(transactionDetail);
		}

		// Customer Account
		Customer customer = productPricingBL.getCustomerBL()
				.getCustomerService()
				.getCustomerDetails(creditNote.getCustomer().getCustomerId());

		transactionDetail = transactionBL.createTransactionDetail(
				transactionAmount,
				customer.getCombination().getCombinationId(), creditFlag, null,
				null,creditNote.getCreditNumber());
		transactionDetails.add(transactionDetail);
		return transactionDetails;
	}

	// Delete Credit Details
	public void deleteCreditNote(Credit credit, List<CreditDetail> creditDetails)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Credit.class.getSimpleName(), credit.getCreditId(), user,
				workflowDetailVo);
	}

	public CreditNoteVO getExchangeInformation(String reference) {
		CreditNoteVO creditVO = null;
		try {
			Credit credit = creditService
					.getCreditNoteInformationByReference(reference);
			if (credit != null)
				creditVO = convertCreditToVO(credit);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return creditVO;
	}

	public CreditNoteVO getSalesReturnBySalesReference(String reference) {
		CreditNoteVO creditVO = null;
		try {
			Credit credit = creditService
					.getSalesReturnBySalesReference(reference);
			if (credit != null)
				creditVO = convertCreditToVO(credit);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return creditVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public CreditService getCreditService() {
		return creditService;
	}

	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public ProductPricingBL getProductPricingBL() {
		return productPricingBL;
	}

	public void setProductPricingBL(ProductPricingBL productPricingBL) {
		this.productPricingBL = productPricingBL;
	}

	public SalesDeliveryNoteBL getSalesDeliveryNoteBL() {
		return salesDeliveryNoteBL;
	}

	public void setSalesDeliveryNoteBL(SalesDeliveryNoteBL salesDeliveryNoteBL) {
		this.salesDeliveryNoteBL = salesDeliveryNoteBL;
	}

	public SalesInvoiceService getSalesInvoiceService() {
		return salesInvoiceService;
	}

	public void setSalesInvoiceService(SalesInvoiceService salesInvoiceService) {
		this.salesInvoiceService = salesInvoiceService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}
}
