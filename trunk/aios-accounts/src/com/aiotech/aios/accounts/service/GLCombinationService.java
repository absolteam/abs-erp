package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.COATemplate;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CombinationTemplate;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.common.to.Constants.Accounts.Segments;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class GLCombinationService {

	private AIOTechGenericDAO<Combination> combinationDAO;
	private AIOTechGenericDAO<CombinationTemplate> combinationTemplateDAO;
	private AIOTechGenericDAO<Account> accountDAO;
	private AIOTechGenericDAO<COATemplate> coaTemplateDAO;
	private AIOTechGenericDAO<Segment> segmentDAO;
	private AIOTechGenericDAO<Implementation> implementationDAO;
	private AIOTechGenericDAO<Ledger> ledgerDAO;
	private AIOTechGenericDAO<LedgerFiscal> ledgerFiscalDAO;

	public List<Combination> getAllCombination(Implementation implementation)
			throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getAllCodeCombination", implementation, implementation,
				implementation, implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
		return combinations;
	}

	public List<Combination> getAllCombinations(Implementation implementation)
			throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getAllCodeCombinations", implementation, implementation,
				implementation, implementation);
		return combinations;
	}

	public List<Combination> getCostCenterCombination(
			Implementation implementation) throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getCostCenterCombination", implementation);
		return combinations;
	}

	// Get all transaction combination
	public List<Combination> getTransactionCombination(
			Implementation implementation) throws Exception {
		return combinationDAO.findByNamedQuery("getTransactionCombination",
				implementation);
	}

	public List<Combination> getAllCombinationForView(
			Implementation implementation) throws Exception {
		return combinationDAO.findByNamedQuery("getAllCombinationForView",
				implementation);
	}

	public List<CombinationTemplate> getAllCombinationTemplate()
			throws Exception {
		return combinationTemplateDAO
				.findByNamedQuery("getAllCombinationTemplate");
	}

	public Account getAllAccount(long accountId) throws Exception {
		return accountDAO.findById(accountId);
	}

	public List<Account> getAllAccountCode(Implementation implementation,
			Long segmentId) throws Exception {
		return accountDAO.findByNamedQuery("getAllAccountCodes",
				implementation, segmentId);
	}

	public Account getDefaultAccountCodes(Implementation implementation,
			Long segmentId) throws Exception {
		return accountDAO.findByNamedQuery("getDefaultCodes", implementation,
				segmentId).get(0);
	}

	/**
	 * 
	 * @param ledgerFiscals
	 * @throws Exception
	 */
	public LedgerFiscal getLedgerFiscals(long combinationId, long periodId)
			throws Exception {
		List<LedgerFiscal> ledgerFiscals = getLedgerFiscalDAO()
				.findByNamedQuery("getLedgerFiscals", combinationId, periodId);
		if (null != ledgerFiscals && ledgerFiscals.size() > 0)
			return ledgerFiscals.get(0);
		else
			return null;
	}

	public List<LedgerFiscal> getLedgerFiscals(long periodId) throws Exception {
		List<LedgerFiscal> ledgerFiscals = getLedgerFiscalDAO()
				.findByNamedQuery("getLedgerFiscalsAgainstPeriod", periodId);

		return ledgerFiscals;
	}

	/**
	 * save ledger fiscal
	 * 
	 * @param ledgerFiscal
	 * @throws Exception
	 */
	public void saveLedgerFiscal(LedgerFiscal ledgerFiscal) throws Exception {
		this.getLedgerFiscalDAO().saveOrUpdate(ledgerFiscal);
	}

	public void saveAccountCode(List<Combination> combinationList)
			throws Exception {
		combinationDAO.saveOrUpdateAll(combinationList);
	}

	public void saveCombination(Combination combination) throws Exception {
		combinationDAO.saveOrUpdate(combination);
	}

	public void saveCombination(List<Combination> combinations)
			throws Exception {
		combinationDAO.saveOrUpdateAll(combinations);
	}

	public void saveCombination(Combination combination, Session session)
			throws Exception {
		session.saveOrUpdate(combination);
	}

	public void saveLedger(List<Ledger> ledger) throws Exception {
		ledgerDAO.saveOrUpdateAll(ledger);
	}

	public void saveLedger(Ledger ledger) throws Exception {
		ledgerDAO.saveOrUpdate(ledger);
	}

	public void saveLedger(Ledger ledger, Session session) throws Exception {
		session.saveOrUpdate(ledger);
	}

	public void deleteLedger(Ledger ledger) throws Exception {
		ledgerDAO.delete(ledger);
	}

	public void deleteLedger(List<Ledger> ledgers) throws Exception {
		ledgerDAO.deleteAll(ledgers);
	}

	public void saveAccounts(List<Account> accountList) throws Exception {
		this.getAccountDAO().saveOrUpdateAll(accountList);
	}

	public List<Ledger> getLedgerList(Implementation implementation)
			throws Exception {
		return ledgerDAO
				.findByNamedQuery("getLedgerDetailList", implementation);
	}

	public List<Ledger> getAllLedgers(Implementation implementation)
			throws Exception {
		return ledgerDAO.findByNamedQuery("getAllLedgers", implementation);
	}

	// Get all ledger fiscal
	public List<LedgerFiscal> getLedgerFiscalList(Implementation implementation)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getLedgerFiscalList",
				implementation);
	}

	public List<Ledger> getIncomeRevenueLedgerList(Implementation implementation)
			throws Exception {
		return ledgerDAO.findByNamedQuery("getIncomeRevenueLedgerList",
				implementation);
	}

	public List<LedgerFiscal> getIncomeRevenueLedgerFiscals(
			Implementation implementation) throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getIncomeRevenueLedgerFiscals", implementation);
	}

	/**
	 * 
	 * @param periodId
	 * @return
	 * @throws Exception
	 */
	public List<LedgerFiscal> getIncomeRevenueLedgerFiscals(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getIncomeRevenueLedgerFiscalsByPeriod", periodId);
	}

	// Added by rafiq for dashboard charts
	public List<LedgerFiscal> getRevenueStatementByPeriod(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getRevenueStatementByPeriod",
				periodId);
	}

	public List<LedgerFiscal> getExpenseStatementByPeriod(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getExpenseStatementByPeriod",
				periodId);
	}

	public List<LedgerFiscal> getAssetStatementByPeriod(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getAssetStatementByPeriod",
				periodId);
	}

	public List<LedgerFiscal> getOwnersEquityStatementByPeriod(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getOwnersEquityStatementByPeriod", periodId);
	}

	public List<LedgerFiscal> getLiablityStatementByPeriod(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getLiablityStatementByPeriod",
				periodId);
	}

	public List<LedgerFiscal> getRevenueStatementByCalendar(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getRevenueStatementByCalendar", periodId);
	}

	public List<LedgerFiscal> getExpenseStatementByCalendar(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getExpenseStatementByCalendar", periodId);
	}

	public List<LedgerFiscal> getAssetStatementByCalendar(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getAssetStatementByCalendar",
				periodId);
	}

	public List<LedgerFiscal> getOwnersEquityStatementByCalendar(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getOwnersEquityStatementByCalendar", periodId);
	}

	public List<LedgerFiscal> getLiablityStatementByCalendar(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery(
				"getLiablityStatementByCalendar", periodId);
	}

	public List<Ledger> getBalanceSheetLedgerList(Implementation implementation)
			throws Exception {
		return ledgerDAO.findByNamedQuery("getBalanceSheetLedgerList",
				implementation);
	}

	public Ledger getLederCombinationById(long combinationId) throws Exception {
		List<Ledger> ledgers = ledgerDAO.findByNamedQuery(
				"getLederCombinationById", combinationId);
		return null != ledgers && ledgers.size() > 0 ? ledgers.get(0) : null;
	}

	public Combination getCombinationWithLedger(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getCombinationWithLedger",
				combinationId).get(0);
	}

	public Combination getCombinationAccount(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationAccountByCombinationId", combinationId).get(0);
	}

	public Combination getCombinationWithAccountsLedger(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationWithAccountsLedger", combinationId).get(0);
	}

	public void deleteCombinationLedger(Combination combination, Ledger ledger) {
		if (null != ledger)
			ledgerDAO.delete(ledger);
		combinationDAO.delete(combination);
	}

	public void deleteCombination(Combination combination) throws Exception {
		combinationDAO.delete(combination);
	}

	public void deleteCombinations(List<Combination> combinations)
			throws Exception {
		combinationDAO.deleteAll(combinations);
	}

	public void deleteCombinations(List<Ledger> legers,
			List<Combination> combinations) throws Exception {
		ledgerDAO.deleteAll(legers);
		combinationDAO.deleteAll(combinations);
	}

	public Combination getCombination(long combinationId) throws Exception {
		return combinationDAO.findById(combinationId);
	}

	@SuppressWarnings("unchecked")
	public Combination getCombination(long combinationId, Session session)
			throws Exception {
		String getCombination = "SELECT c FROM Combination c WHERE c.combinationId = ?";
		List<Combination> combinations = session.createQuery(getCombination)
				.setLong(0, combinationId).list();
		return combinations.get(0);
	}

	public Combination getCombinationAccounts(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getCombinationAccounts",
				combinationId).get(0);
	}

	public Combination getCombinationByCombinationId(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getCombinationByCombinationId",
				combinationId).get(0);
	}

	public Combination getCombinationByAccount(long accountId) throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getCombinationByAnalysis", accountId);
		if (combinations != null && combinations.size() > 0)
			return combinations.get(0);
		else
			return null;
	}

	public Combination getCombinationByNaturalAccount(long accountId)
			throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getCombinationByNaturalAccount", accountId);
		return combinations != null && combinations.size() > 0 ? combinations
				.get(0) : null;
	}

	public List<Combination> getCombinationByNaturalAccountId(long accountId)
			throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationByNaturalAccountId", accountId);
	}

	public List<Combination> getCombinationByCostCenterNaturalAccountId(
			long accountId1, long accountId2) throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationByCostCenterNaturalAccountId", accountId1,
				accountId2);
	}

	public Combination getCombinationAccountDetail(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getCombinationAccountDetail",
				combinationId).get(0);
	}

	public Combination getCombinationAccountsDetail(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getCombinationAccountsDetail",
				combinationId,
				(byte) WorkflowConstants.Status.Published.getCode()).get(0);
	}

	public Combination getCombinationWithChildAccountsDetail(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationWithChildAccountsDetail", combinationId,
				(byte) WorkflowConstants.Status.Published.getCode()).get(0);
	}

	public Combination getCombinationForProduct(long costCentreAccount,
			long naturalAccountId, long analysisAccountId) {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getCombinationForProduct", costCentreAccount,
				naturalAccountId, analysisAccountId);
		return null != combinations && combinations.size() > 0 ? combinations
				.get(0) : null;
	}

	public Combination getCombinationAllAccountDetail(long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationAllAccountDetail", combinationId).get(0);
	}

	@SuppressWarnings("unchecked")
	public Combination getCombinationAllAccountDetail(long combinationId,
			Session session) throws Exception {
		String getCombinationAllAccountDetail = "SELECT c FROM Combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE c.combinationId=?";
		List<Combination> combinations = session
				.createQuery(getCombinationAllAccountDetail)
				.setLong(0, combinationId).list();
		return null != combinations && combinations.size() > 0 ? combinations
				.get(0) : null;
	}

	public Combination getCombinationAllAccountByBankAccount(long bankAccountId)
			throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationAllAccountByBankAccount", bankAccountId).get(0);
	}

	public List<Combination> validateByCompanyAccount(Account companyAccount)
			throws Exception {
		return combinationDAO.findByNamedQuery("validateByCompanyAccount",
				companyAccount);
	}

	public List<Combination> validateByCostAccount(Account companyAccount,
			Account costAccount) throws Exception {
		return combinationDAO.findByNamedQuery("validateByCostAccount",
				companyAccount, costAccount);
	}

	public List<Combination> validateByNaturalAccount(Account companyAccount,
			Account costAccount, Account naturalAccount) throws Exception {
		return combinationDAO.findByNamedQuery("validateByNaturalAccount",
				companyAccount, costAccount, naturalAccount);
	}

	public List<Combination> validateByAnalysisAccount(Account companyAccount,
			Account costAccount, Account naturalAccount, Account analyisAccount)
			throws Exception {
		return combinationDAO.findByNamedQuery("validateByAnalysisAccount",
				companyAccount, costAccount, naturalAccount, analyisAccount);
	}

	public List<Combination> validateByBuffer1Account(Account companyAccount,
			Account costAccount, Account naturalAccount,
			Account analyisAccount, Account buffer1Account) throws Exception {
		return combinationDAO.findByNamedQuery("validateByBuffer1Account",
				companyAccount, costAccount, naturalAccount, analyisAccount,
				buffer1Account);
	}

	public List<Combination> validateByBuffer2Account(Account companyAccount,
			Account costAccount, Account naturalAccount,
			Account analyisAccount, Account buffer1Account,
			Account buffer2Account) throws Exception {
		return combinationDAO.findByNamedQuery("validateByBuffer2Account",
				companyAccount, costAccount, naturalAccount, analyisAccount,
				buffer1Account, buffer2Account);
	}

	public Combination getNaturalAccountCombination(long companyAccountId,
			long costCenterAccountId, long naturalAccountId) throws Exception {
		return combinationDAO.findByNamedQuery("getNaturalAccountCombination",
				companyAccountId, costCenterAccountId, naturalAccountId).get(0);
	}

	@SuppressWarnings("unchecked")
	public Combination getNaturalAccountCombination(long companyAccountId,
			long costCenterAccountId, long naturalAccountId, Session session)
			throws Exception {
		String getNaturalAccountCombination = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId.accountId = ?"
				+ " AND c.accountByCostcenterAccountId.accountId = ?"
				+ " AND c.accountByNaturalAccountId.accountId = ?"
				+ " AND c.accountByAnalysisAccountId IS NULL";
		List<Combination> combinations = session
				.createQuery(getNaturalAccountCombination)
				.setLong(0, companyAccountId).setLong(1, costCenterAccountId)
				.setLong(2, naturalAccountId).list();
		return combinations.get(0);
	}

	public Combination getNaturalAccountCombination(long costCenterAccountId,
			long naturalAccountId) throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getNaturalAccountByCostCombination", costCenterAccountId,
				naturalAccountId);
		return null != combinations && combinations.size() > 0 ? combinations
				.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public Combination getNaturalAccountCombination(long costCenterAccountId,
			long naturalAccountId, Session session) throws Exception {
		String getNaturalAccountCombination = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCostcenterAccountId.accountId = ?"
				+ " AND c.accountByNaturalAccountId.accountId = ?";
		List<Combination> combinations = session
				.createQuery(getNaturalAccountCombination)
				.setLong(0, costCenterAccountId).setLong(1, naturalAccountId)
				.list();
		return null != combinations && combinations.size() > 0 ? combinations
				.get(0) : null;
	}

	public void updateCombination(Combination combination) throws Exception {
		combinationDAO.saveOrUpdate(combination);
	}

	public void updateImplementationAccounts(Implementation implementation)
			throws Exception {
		implementationDAO.saveOrUpdate(implementation);
	}

	public List<Combination> getCombiantionByAccount(long accountId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getAccountById", accountId,
				accountId, accountId, accountId, accountId, accountId);
	}

	public Account getAccountDetail(String account,
			Implementation implementation) {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getAccountsByAccount", account, implementation);
		if (accounts != null && accounts.size() > 0)
			return accounts.get(0);
		else
			return null;

	}

	@SuppressWarnings("unchecked")
	public Account getAccountDetail(String account,
			Implementation implementation, Session session) {
		String getAccountDetail = "SELECT a FROM Account a  "
				+ " WHERE a.account = ? AND a.implementation = ?";
		List<Account> accounts = session.createQuery(getAccountDetail)
				.setString(0, account).setEntity(1, implementation).list();
		if (accounts != null && accounts.size() > 0)
			return accounts.get(0);
		else
			return null;

	}

	public Combination getCombinationByCompanyAccount(
			Implementation implementation) throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationByCompanyAccount", implementation).get(0);
	}

	public Combination getCombinationByCostCenter(long accountId) {
		return combinationDAO.findByNamedQuery("getCombinationByCostAccount",
				accountId).get(0);
	}

	public List<LedgerFiscal> getLedgerFiscalByPeriodIdWithLedger(long periodId)
			throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getLedgerFiscalsWithLedger",
				periodId);
	}

	public Ledger getLederByCombinationIdWithChilds(long combinationId)
			throws Exception {
		return ledgerDAO.findByNamedQuery("getLederByCombinationIdWithChilds",
				combinationId).get(0);
	}

	public List<Combination> getAllParentCombination(
			Implementation implementation) throws Exception {
		List<Combination> combinations = combinationDAO.findByNamedQuery(
				"getAllParentCombination", implementation, implementation,
				implementation, implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
		return combinations;
	}

	public List<Combination> getCombinationChildDetails(Long combinationId)
			throws Exception {
		return combinationDAO.findByNamedQuery("getCombinationChildDetails",
				combinationId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Combination> getCombinationChildDetailWithAccount(
			Long combinationId) throws Exception {
		return combinationDAO.findByNamedQuery(
				"getCombinationChildDetailWithAccount", combinationId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	@SuppressWarnings("unchecked")
	public List<Combination> getCombinationDetail(Combination combination,
			int accessId) throws Exception {
		Criteria criteria = combinationDAO.createCriteria();
		criteria.createAlias("accountByCompanyAccountId", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByCostcenterAccountId", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByNaturalAccountId", "ntl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combinations", "ch",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.isNull("combination"));
		if (null != combination.getAccountByCompanyAccountId())
			criteria.add(Restrictions.eq("cy.accountId", combination
					.getAccountByCompanyAccountId().getAccountId()));
		if (null != combination.getAccountByCostcenterAccountId())
			criteria.add(Restrictions.eq("cst.accountId", combination
					.getAccountByCostcenterAccountId().getAccountId()));
		if (null != combination.getAccountByNaturalAccountId())
			criteria.add(Restrictions.eq("ntl.accountId", combination
					.getAccountByNaturalAccountId().getAccountId()));
		if (null != combination.getAccountByAnalysisAccountId())
			criteria.add(Restrictions.eq("ay.accountId", combination
					.getAccountByAnalysisAccountId().getAccountId()));
		if (null != combination.getAccountByBuffer1AccountId())
			criteria.add(Restrictions.eq("b1.accountId", combination
					.getAccountByBuffer1AccountId().getAccountId()));
		if (null != combination.getAccountByBuffer2AccountId())
			criteria.add(Restrictions.eq("b2.accountId", combination
					.getAccountByBuffer2AccountId().getAccountId()));

		if ((int) Segments.Company.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByCostcenterAccountId"));
		else if ((int) Segments.CostCenter.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByNaturalAccountId"));
		else if ((int) Segments.Natural.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByAnalysisAccountId"));
		else if ((int) Segments.Analysis.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByBuffer1AccountId"));
		else if ((int) Segments.Buffer1.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByBuffer2AccountId"));
		Set<Combination> uniqueRecord = new HashSet<Combination>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Combination>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public Combination getSourceCombination(Combination combination,
			Integer accessId) throws Exception {
		Criteria criteria = combinationDAO.createCriteria();
		criteria.createAlias("accountByCompanyAccountId", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByCostcenterAccountId", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByNaturalAccountId", "ntl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combinations", "ch",
				CriteriaSpecification.LEFT_JOIN);
		if (null != combination.getAccountByCompanyAccountId())
			criteria.add(Restrictions.eq("cy.accountId", combination
					.getAccountByCompanyAccountId().getAccountId()));
		if (null != combination.getAccountByCostcenterAccountId())
			criteria.add(Restrictions.eq("cst.accountId", combination
					.getAccountByCostcenterAccountId().getAccountId()));
		if (null != combination.getAccountByNaturalAccountId())
			criteria.add(Restrictions.eq("ntl.accountId", combination
					.getAccountByNaturalAccountId().getAccountId()));
		if (null != combination.getAccountByAnalysisAccountId())
			criteria.add(Restrictions.eq("ay.accountId", combination
					.getAccountByAnalysisAccountId().getAccountId()));
		if (null != combination.getAccountByBuffer1AccountId())
			criteria.add(Restrictions.eq("b1.accountId", combination
					.getAccountByBuffer1AccountId().getAccountId()));
		if (null != combination.getAccountByBuffer2AccountId())
			criteria.add(Restrictions.eq("b2.accountId", combination
					.getAccountByBuffer2AccountId().getAccountId()));

		if ((int) Segments.Company.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByCostcenterAccountId"));
		else if ((int) Segments.CostCenter.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByNaturalAccountId"));
		else if ((int) Segments.Natural.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByAnalysisAccountId"));
		else if ((int) Segments.Analysis.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByBuffer1AccountId"));
		else if ((int) Segments.Buffer1.getCode() == accessId)
			criteria.add(Restrictions.isNotNull("accountByBuffer2AccountId"));
		Set<Combination> uniqueRecord = new HashSet<Combination>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Combination>(
				uniqueRecord).get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Combination> getCombinationInDirectChild(Combination combination)
			throws Exception {
		Criteria criteria = combinationDAO.createCriteria();
		criteria.createAlias("accountByCompanyAccountId", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByCostcenterAccountId", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByNaturalAccountId", "ntl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "pr",
				CriteriaSpecification.LEFT_JOIN);

		if (null != combination.getAccountByCompanyAccountId())
			criteria.add(Restrictions.eq("cy.accountId", combination
					.getAccountByCompanyAccountId().getAccountId()));
		if (null != combination.getAccountByCostcenterAccountId())
			criteria.add(Restrictions.eq("cst.accountId", combination
					.getAccountByCostcenterAccountId().getAccountId()));
		if (null != combination.getAccountByNaturalAccountId())
			criteria.add(Restrictions.eq("ntl.accountId", combination
					.getAccountByNaturalAccountId().getAccountId()));
		if (null != combination.getAccountByAnalysisAccountId())
			criteria.add(Restrictions.eq("ay.accountId", combination
					.getAccountByAnalysisAccountId().getAccountId()));
		if (null != combination.getAccountByBuffer1AccountId())
			criteria.add(Restrictions.eq("b1.accountId", combination
					.getAccountByBuffer1AccountId().getAccountId()));
		if (null != combination.getAccountByBuffer2AccountId())
			criteria.add(Restrictions.eq("b2.accountId", combination
					.getAccountByBuffer2AccountId().getAccountId()));
		criteria.add(Restrictions.or(
				Restrictions.isNull("combination"),
				Restrictions.eq("pr.combinationId",
						combination.getCombinationId())));
		Set<Combination> uniqueRecord = new HashSet<Combination>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Combination>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Combination> getSelfCombinationChildDetailWithAccount(
			long combinationId, Combination sourceCobination) throws Exception {
		Criteria criteria = combinationDAO.createCriteria();
		criteria.createAlias("accountByCompanyAccountId", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByCostcenterAccountId", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByNaturalAccountId", "ntl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combinations", "ch",
				CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("pr.combinationId", combinationId));
		if (null != sourceCobination)
			criteria.add(Restrictions.ne("pr.combinationId",
					sourceCobination.getCombinationId()));

		Set<Combination> uniqueRecord = new HashSet<Combination>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Combination>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Combination> validateCombination(Combination combination,
			long combinationId) throws Exception {
		Criteria criteria = combinationDAO.createCriteria();
		criteria.createAlias("accountByCompanyAccountId", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByCostcenterAccountId", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByNaturalAccountId", "ntl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "pr",
				CriteriaSpecification.LEFT_JOIN);
		if (null != combination.getAccountByCompanyAccountId())
			criteria.add(Restrictions.eq("cy.accountId", combination
					.getAccountByCompanyAccountId().getAccountId()));
		if (null != combination.getAccountByCostcenterAccountId())
			criteria.add(Restrictions.eq("cst.accountId", combination
					.getAccountByCostcenterAccountId().getAccountId()));
		if (null != combination.getAccountByNaturalAccountId())
			criteria.add(Restrictions.eq("ntl.accountId", combination
					.getAccountByNaturalAccountId().getAccountId()));
		if (null != combination.getAccountByAnalysisAccountId())
			criteria.add(Restrictions.eq("ay.accountId", combination
					.getAccountByAnalysisAccountId().getAccountId()));
		if (null != combination.getAccountByBuffer1AccountId())
			criteria.add(Restrictions.eq("b1.accountId", combination
					.getAccountByBuffer1AccountId().getAccountId()));
		if (null != combination.getAccountByBuffer2AccountId())
			criteria.add(Restrictions.eq("b2.accountId", combination
					.getAccountByBuffer2AccountId().getAccountId()));
		if (null != combination.getCombination()
				&& null != combination.getCombination().getCombinationId())
			criteria.add(Restrictions.eq("pr.combinationId", combinationId));
		Set<Combination> uniqueRecord = new HashSet<Combination>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Combination>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Combination> getUnusedAnalysisAccountCombination(
			Implementation implementation) throws Exception {
		Criteria criteria = combinationDAO.createCriteria();
		criteria.createAlias("accountByCompanyAccountId", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByCostcenterAccountId", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByNaturalAccountId", "ntl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ledgers", "ld", CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("cy.implementation", implementation));
		criteria.add(Restrictions.isNotNull("accountByAnalysisAccountId"));
		criteria.add(Restrictions.isNull("accountByBuffer1AccountId"));
		criteria.add(Restrictions.isNull("ld.balance"));

		Set<Combination> uniqueRecord = new HashSet<Combination>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Combination>(
				uniqueRecord) : null;
	}

	// Getters & setters
	public AIOTechGenericDAO<Combination> getCombinationDAO() {
		return combinationDAO;
	}

	public void setCombinationDAO(AIOTechGenericDAO<Combination> combinationDAO) {
		this.combinationDAO = combinationDAO;
	}

	public AIOTechGenericDAO<Segment> getSegmentDAO() {
		return segmentDAO;
	}

	public void setSegmentDAO(AIOTechGenericDAO<Segment> segmentDAO) {
		this.segmentDAO = segmentDAO;
	}

	public AIOTechGenericDAO<Account> getAccountDAO() {
		return accountDAO;
	}

	public void setAccountDAO(AIOTechGenericDAO<Account> accountDAO) {
		this.accountDAO = accountDAO;
	}

	public AIOTechGenericDAO<Implementation> getImplementationDAO() {
		return implementationDAO;
	}

	public void setImplementationDAO(
			AIOTechGenericDAO<Implementation> implementationDAO) {
		this.implementationDAO = implementationDAO;
	}

	public AIOTechGenericDAO<Ledger> getLedgerDAO() {
		return ledgerDAO;
	}

	public void setLedgerDAO(AIOTechGenericDAO<Ledger> ledgerDAO) {
		this.ledgerDAO = ledgerDAO;
	}

	public AIOTechGenericDAO<CombinationTemplate> getCombinationTemplateDAO() {
		return combinationTemplateDAO;
	}

	public void setCombinationTemplateDAO(
			AIOTechGenericDAO<CombinationTemplate> combinationTemplateDAO) {
		this.combinationTemplateDAO = combinationTemplateDAO;
	}

	public AIOTechGenericDAO<COATemplate> getCoaTemplateDAO() {
		return coaTemplateDAO;
	}

	public void setCoaTemplateDAO(AIOTechGenericDAO<COATemplate> coaTemplateDAO) {
		this.coaTemplateDAO = coaTemplateDAO;
	}

	public AIOTechGenericDAO<LedgerFiscal> getLedgerFiscalDAO() {
		return ledgerFiscalDAO;
	}

	public void setLedgerFiscalDAO(
			AIOTechGenericDAO<LedgerFiscal> ledgerFiscalDAO) {
		this.ledgerFiscalDAO = ledgerFiscalDAO;
	}
}
