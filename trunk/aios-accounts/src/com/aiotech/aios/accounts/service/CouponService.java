package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CouponService {

	private AIOTechGenericDAO<Coupon> couponDAO;

	// Get All Coupons
	public List<Coupon> getAllCoupons(Implementation implementation)
			throws Exception {
		return couponDAO.findByNamedQuery("getAllCoupons", implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get All Active Coupons
	public List<Coupon> getAllActiveCoupons(Implementation implementation,
			Date currentDate) throws Exception {
		return couponDAO.findByNamedQuery("getAllActiveCoupons",
				implementation, currentDate);
	}

	// Save Coupon
	public void saveCoupon(Coupon coupon) throws Exception {
		couponDAO.saveOrUpdate(coupon);
	}

	// Get Coupon By Coupon Id
	public Coupon getCouponByCouponId(long couponId) throws Exception {
		return couponDAO.findById(couponId);
	}

	// Get Coupon by coupon number
	public Coupon getCouponByCouponNumber(int couponNumber, Date fromDate,
			Date toDate, Implementation implementation) throws Exception {
		List<Coupon> coupons = couponDAO.findByNamedQuery(
				"getCouponByCouponNumber", implementation, couponNumber, couponNumber,
				fromDate, toDate);
		return null != coupons && coupons.size() > 0 ? coupons.get(0) : null;
	}

	// Delete Coupon
	public void deleteCoupon(Coupon coupon) throws Exception {
		couponDAO.delete(coupon);
	}

	// Getters & Setters
	public AIOTechGenericDAO<Coupon> getCouponDAO() {
		return couponDAO;
	}

	public void setCouponDAO(AIOTechGenericDAO<Coupon> couponDAO) {
		this.couponDAO = couponDAO;
	}
}
