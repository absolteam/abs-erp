package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.joda.time.DateTime;

import com.aiotech.aios.accounts.domain.entity.AssetInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetInsurancePremium;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.AssetInsurancePremiumService;
import com.aiotech.aios.common.util.DateFormat;

public class AssetInsurancePremiumBL {

	// Dependency
	private TransactionBL transactionBL;
	private AssetInsurancePremiumService assetInsurancePremiumService;

	// Auto Premium Configuration
	public void premiumDueAutoPayment() throws Exception {
		List<AssetInsurancePremium> assetInsurancePremiums = assetInsurancePremiumService
				.getAllPremiumDuePaymentByDate(DateFormat.getFromDate(Calendar
						.getInstance().getTime()));
		List<Transaction> transactions = new ArrayList<Transaction>();
		List<AssetInsurancePremium> insurancePremiums = null;
		if (null != assetInsurancePremiums && assetInsurancePremiums.size() > 0) {
			insurancePremiums = new ArrayList<AssetInsurancePremium>();
			for (AssetInsurancePremium assetInsurancePremium : assetInsurancePremiums) {
				transactions.add(createPremiumInsurance(assetInsurancePremium));
				insurancePremiums.add(new AssetInsurancePremium(
						assetInsurancePremium.getAssetInsurance(),
						getNextPremiumDate(
								assetInsurancePremium.getAssetInsurance()
										.getInsurancePeriod(),
								new DateTime(assetInsurancePremium
										.getNextPremiumDate())),
						assetInsurancePremium.getAmount(), false));
				assetInsurancePremium.setStatus(true);
			}
			if (null != insurancePremiums && insurancePremiums.size() > 0) {
				for (AssetInsurancePremium insurancePremium : insurancePremiums)
					saveInsurancePremium(insurancePremium);
			}
			if (null != transactions && transactions.size() > 0)
				transactionBL.saveTransactions(transactions);
		}
	}

	// Create Premium Transaction Entry
	private Transaction createPremiumInsurance(
			AssetInsurancePremium assetInsurancePremium) throws Exception {
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCurrency(
						assetInsurancePremium.getAssetInsurance()
								.getAssetCreation().getImplementation())
						.getCurrencyId(), "Premium Insurance Expense  "
						.concat(assetInsurancePremium.getAssetInsurance()
								.getAssetCreation().getAssetName()), Calendar
						.getInstance().getTime(), transactionBL.getCategory(
						"Asset Insurance", assetInsurancePremium
								.getAssetInsurance().getAssetCreation()
								.getImplementation()), assetInsurancePremium
						.getAssetInsurance().getAssetCreation()
						.getImplementation(), assetInsurancePremium
						.getAssetInsurancePremiumId(),
				AssetInsurancePremium.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(createInsuranceExepense(
				assetInsurancePremium.getAssetInsurance(),
				assetInsurancePremium.getAmount(), true));
		transactionDetails.add(createPrepaidInsuranceDetail(
				assetInsurancePremium.getAssetInsurance(),
				assetInsurancePremium.getAmount(), false));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Save Premium
	public void saveInsurancePremium(AssetInsurancePremium assetInsurancePremium)
			throws Exception {
		assetInsurancePremiumService
				.saveInsurancePremium(assetInsurancePremium);
	}

	// Create Premium Insurance Transaction
	public Transaction createPremiumInsuranceEntry(
			AssetInsurance assetInsurance, double premiumExpense,
			boolean debitFlag, boolean creditFlag) throws Exception {
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "Premium Insurance Expense  "
				.concat(assetInsurance.getAssetCreation().getAssetName()),
				assetInsurance.getStartDate(), transactionBL
						.getCategory("Asset Insurance"), assetInsurance
						.getAssetInsuranceId(), AssetInsurance.class
						.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(createInsuranceExepense(assetInsurance,
				premiumExpense, debitFlag));
		transactionDetails.add(createPrepaidInsuranceDetail(assetInsurance,
				premiumExpense, creditFlag));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Prepaid Entry
	private TransactionDetail createPrepaidInsuranceDetail(
			AssetInsurance assetInsurance, double totalDueAmount,
			boolean debitFlag) throws Exception {
		return transactionBL.createTransactionDetail(totalDueAmount,
				assetInsurance.getAssetCreation().getImplementation()
						.getClearingAccount(), debitFlag, null, null, null);
	}

	// Premium Expense Entry
	private TransactionDetail createInsuranceExepense(
			AssetInsurance assetInsurance, double premiumExpense,
			boolean debitFlag) throws Exception {
		return transactionBL.createTransactionDetail(premiumExpense,
				assetInsurance.getAssetCreation().getImplementation()
						.getExpenseAccount(), debitFlag, null, null, null);
	}

	// Get next premium date
	public Date getNextPremiumDate(Byte insurancePeriod, DateTime premiumDate)
			throws Exception {
		DateTime nextPremiumDate = new DateTime(Calendar.getInstance()
				.getTime());
		switch (insurancePeriod) {
		case 1:
			nextPremiumDate = premiumDate.plusDays(1);
			break;
		case 2:
			nextPremiumDate = premiumDate.plusWeeks(1);
			break;
		case 3:
			nextPremiumDate = premiumDate.plusMonths(1);
			break;
		case 4:
			nextPremiumDate = premiumDate.plusMonths(4);
			break;
		case 5:
			nextPremiumDate = premiumDate.plusMonths(6);
			break;
		case 6:
			nextPremiumDate = premiumDate.plusYears(1);
			break;
		}
		return nextPremiumDate.toDate();
	}

	// Getters & Setters
	public AssetInsurancePremiumService getAssetInsurancePremiumService() {
		return assetInsurancePremiumService;
	}

	public void setAssetInsurancePremiumService(
			AssetInsurancePremiumService assetInsurancePremiumService) {
		this.assetInsurancePremiumService = assetInsurancePremiumService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}
}
