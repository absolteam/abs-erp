/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;

import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReconciliation;
import com.aiotech.aios.accounts.domain.entity.BankReconciliationDetail;
import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustment;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.BankReconciliationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankReconciliationVO;
import com.aiotech.aios.accounts.domain.entity.vo.LedgerVO;
import com.aiotech.aios.accounts.domain.entity.vo.TransactionDetailVO;
import com.aiotech.aios.accounts.service.BankReconciliationService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Saleem
 */
public class BankReconciliationBL {

	// Dependencies
	private TransactionBL transactionBL;
	private BankBL bankBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private BankReconciliationService bankReconciliationService;
	private DirectPaymentBL directPaymentBL;
	private BankDepositBL bankDepositBL;
	private BankTransferBL bankTransferBL;
	private ReconciliationAdjustmentBL reconciliationAdjustmentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get all Bank Reconciliation and convert into JSON object
	public JSONObject getAllBankReconciliations() throws Exception {
		List<BankReconciliation> bankReconciliations = bankReconciliationService
				.getAllBankReconciliations(getImplementation());

		JSONArray jsonData = new JSONArray();
		JSONArray jsonArray = null;
		JSONObject jsonObject = new JSONObject();

		for (BankReconciliation bankReconciliation : bankReconciliations) {
			jsonArray = new JSONArray();
			jsonArray.add(bankReconciliation.getBankReconciliationId());
			jsonArray.add(bankReconciliation.getReferenceNumber());
			jsonArray.add(bankReconciliation.getBankAccount()
					.getAccountNumber());
			jsonArray.add(DateFormat.convertDateToString(bankReconciliation
					.getReconciliationDate().toString()));
			jsonArray.add(DateFormat.convertDateToString(bankReconciliation
					.getStatementEndDate().toString()));
			jsonArray.add(bankReconciliation.getStatementReference());
			if (bankReconciliation.getAccountBalance() > 0)
				jsonArray.add(AIOSCommons.formatAmount(bankReconciliation
						.getAccountBalance()));
			else
				jsonArray.add("-"
						+ AIOSCommons.formatAmount(Math.abs(bankReconciliation
								.getAccountBalance())));
			if (bankReconciliation.getStatementBalance() > 0)
				jsonArray.add(AIOSCommons.formatAmount(bankReconciliation
						.getStatementBalance()));
			else
				jsonArray.add("-"
						+ AIOSCommons.formatAmount(Math.abs(bankReconciliation
								.getStatementBalance())));
			jsonData.add(jsonArray);
		}

		jsonObject.put("aaData", jsonData);
		return jsonObject;

	}

	public TransactionDetailVO getAccountClosingBalance(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		LedgerVO ledgerVO = new LedgerVO();
		ledgerVO.setSide(true);
		ledgerVO.setBalance(0d);
		ledgerVO = processUpdateLedgerGroup(ledgerVO, transactionDetails);
		transactionDetailVO.setCombination(ledgerVO.getCombination());
		transactionDetailVO.setIsDebit(ledgerVO.getSide());
		transactionDetailVO.setAmount(ledgerVO.getBalance());
		if (ledgerVO.getSide())
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		else
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		return transactionDetailVO;
	}

	public LedgerVO processUpdateLedgerGroup(LedgerVO ledger,
			List<TransactionDetail> transactionDetailVOs) throws Exception {
		for (TransactionDetail transactionDetailVO : transactionDetailVOs) {
			double ledgerBalance = ledger.getSide() ? (-1 * ledger.getBalance())
					: ledger.getBalance();
			double transactionAmount = transactionDetailVO.getIsDebit() ? (-1 * transactionDetailVO
					.getAmount()) : transactionDetailVO.getAmount();
			double calculationAmount = 0.0;
			ledger.setCombination(transactionDetailVO.getCombination());
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs(transactionDetailVO.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs(transactionDetailVO.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs(transactionDetailVO.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs(transactionDetailVO.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		}
		return ledger;
	}

	// Create bank reconciliation detail system statement
	public BankReconciliationVO getBankReconciliationDetails(
			List<TransactionDetail> transactionDetails) throws Exception {
		BankReconciliationVO bankReconciliationVO = null;
		List<BankReconciliationDetailVO> bankReconciliationDetailVOs = convertBankReconciliationDetail(
				transactionDetails, 1l, true);
		if (null != bankReconciliationDetailVOs
				&& bankReconciliationDetailVOs.size() > 0) {
			bankReconciliationVO = new BankReconciliationVO();
			double receiptTotal = 0;
			double paymentTotal = 0;
			for (BankReconciliationDetailVO bankReconciliationDetailVO : bankReconciliationDetailVOs) {
				if (null != bankReconciliationDetailVO.getReceiptAmount())
					receiptTotal += AIOSCommons
							.formatAmountToDouble(bankReconciliationDetailVO
									.getReceiptAmount());
				else
					paymentTotal += AIOSCommons
							.formatAmountToDouble(bankReconciliationDetailVO
									.getPaymentAmount());
			}
			TransactionDetailVO transactionDetailVO = getAccountClosingBalance(transactionDetails);
			bankReconciliationVO
					.setAccountBalanceStr(null != transactionDetailVO ? transactionDetailVO
							.getDebitAmount() : "");
			bankReconciliationVO.setReceiptTotal(AIOSCommons
					.formatAmount(receiptTotal));
			bankReconciliationVO.setPaymentTotal(AIOSCommons
					.formatAmount(paymentTotal));
			bankReconciliationVO
					.setAccountSide(null != transactionDetailVO ? transactionDetailVO
							.getIsDebit() : false);
			bankReconciliationVO
					.setBankReconciliationDetailVOs(bankReconciliationDetailVOs);
		}
		return bankReconciliationVO;
	}

	// Convert into bank reconciliation detail vo
	public List<BankReconciliationDetailVO> convertBankReconciliationDetail(
			List<TransactionDetail> transactionDetails, long count,
			boolean reconcileStatus) throws Exception {
		List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>();
		BankReconciliationDetailVO bankReconciliationDetailVO = null;
		DirectPayment directPayment = null;
		BankReceipts bankReceipt = null;
		BankDeposit bankDeposit = null;
		BankTransfer bankTransfer = null;
		ReconciliationAdjustment reconciliationAdj = null;
		for (TransactionDetail transactionDetail : transactionDetails) {
			bankReconciliationDetailVO = new BankReconciliationDetailVO();
			bankReconciliationDetailVO.setChequeEntry(false);
			if (null != transactionDetail.getTransaction().getRecordId()
					&& null != transactionDetail.getTransaction().getUseCase()
					&& !("").equals(transactionDetail.getTransaction()
							.getUseCase())) {
				if (transactionDetail.getTransaction().getUseCase()
						.equals(DirectPayment.class.getSimpleName())) {
					directPayment = directPaymentBL.getDirectPaymentService()
							.findDirectPaymentById(
									transactionDetail.getTransaction()
											.getRecordId());
					bankReconciliationDetailVO.setUsecaseRecordId(directPayment
							.getDirectPaymentId());
					bankReconciliationDetailVO.setReferenceNumber(directPayment
							.getPaymentNumber());
					bankReconciliationDetailVO.setTransactionDateStr(DateFormat
							.convertDateToString(directPayment.getPaymentDate()
									.toString()));
					bankReconciliationDetailVO.setDescription(directPayment
							.getDescription());
					bankReconciliationDetailVO.setUseCase(DirectPayment.class
							.getSimpleName());
					if (null != directPayment.getChequeDate()) {
						bankReconciliationDetailVO.setChequeDateStr(DateFormat
								.convertDateToString(directPayment
										.getChequeDate().toString()));
						bankReconciliationDetailVO.setChequeNumber(String
								.valueOf(directPayment.getChequeNumber()));
						bankReconciliationDetailVO
								.setUseCaseName(DirectPayment.class
										.getSimpleName());
						bankReconciliationDetailVO.setChequeEntry(true);
					}
				} else if (transactionDetail.getTransaction().getUseCase()
						.equals(BankReceipts.class.getSimpleName())) {
					bankReceipt = bankDepositBL
							.getBankReceiptsBL()
							.getBankReceiptsService()
							.findByBankReceiptsId(
									transactionDetail.getTransaction()
											.getRecordId());
					bankReconciliationDetailVO.setUsecaseRecordId(bankReceipt
							.getBankReceiptsId());
					bankReconciliationDetailVO.setReferenceNumber(bankReceipt
							.getReceiptsNo());
					bankReconciliationDetailVO.setTransactionDateStr(DateFormat
							.convertDateToString(bankReceipt.getReceiptsDate()
									.toString()));
					bankReconciliationDetailVO.setDescription(bankReceipt
							.getDescription());
					bankReconciliationDetailVO.setUseCase(BankReceipts.class
							.getSimpleName());
				} else if (transactionDetail.getTransaction().getUseCase()
						.equals(BankDeposit.class.getSimpleName())) {
					bankDeposit = bankDepositBL.getBankDepositService()
							.getBankDepositByDepositId(
									transactionDetail.getTransaction()
											.getRecordId());
					for (BankDepositDetail bankDepositDetail : bankDeposit
							.getBankDepositDetails()) { 
						if (null != bankDepositDetail.getBankReceiptsDetail()
								&& null != bankDepositDetail
										.getBankReceiptsDetail()
										.getChequeDate()) {
							bankReconciliationDetailVO
									.setChequeDateStr(DateFormat
											.convertDateToString(bankDepositDetail
													.getBankReceiptsDetail()
													.getChequeDate().toString()));
							bankReconciliationDetailVO
									.setChequeNumber(bankDepositDetail
											.getBankReceiptsDetail()
											.getBankRefNo());
							bankReconciliationDetailVO.setChequeEntry(true);
							bankReconciliationDetailVO
									.setUsecaseRecordId(bankDepositDetail
											.getBankDepositDetailId());
							bankReconciliationDetailVO
									.setUseCaseName(BankDepositDetail.class
											.getSimpleName());
						} 
					}
					bankReconciliationDetailVO.setReferenceNumber(bankDeposit
							.getBankDepositNumber());
					bankReconciliationDetailVO.setTransactionDateStr(DateFormat
							.convertDateToString(bankDeposit.getDate()
									.toString()));
					bankReconciliationDetailVO.setDescription(bankDeposit
							.getDescription());
					bankReconciliationDetailVO.setUseCase(BankDeposit.class
							.getSimpleName());
				} else if (transactionDetail.getTransaction().getUseCase()
						.equals(ReconciliationAdjustment.class.getSimpleName())) {
					reconciliationAdj = reconciliationAdjustmentBL
							.getReconciliationAdjustmentService()
							.findReconciliationAdjustmentById(
									transactionDetail.getTransaction()
											.getRecordId());
					bankReconciliationDetailVO
							.setUsecaseRecordId(reconciliationAdj
									.getReconciliationAdjustmentId());
					bankReconciliationDetailVO
							.setReferenceNumber(reconciliationAdj
									.getReferenceNumber());
					bankReconciliationDetailVO.setTransactionDateStr(DateFormat
							.convertDateToString(reconciliationAdj.getDate()
									.toString()));
					bankReconciliationDetailVO.setDescription(reconciliationAdj
							.getDescription());
					bankReconciliationDetailVO
							.setUseCase(ReconciliationAdjustment.class
									.getSimpleName());
				} else if (transactionDetail.getTransaction().getUseCase()
						.equals(BankTransfer.class.getSimpleName())) {
					bankTransfer = bankTransferBL.getBankTransferService()
							.getBankTransferById(
									transactionDetail.getTransaction()
											.getRecordId());
					bankReconciliationDetailVO.setUsecaseRecordId(bankTransfer
							.getBankTransferId());
					bankReconciliationDetailVO.setReferenceNumber(bankTransfer
							.getTransferNo());
					bankReconciliationDetailVO.setTransactionDateStr(DateFormat
							.convertDateToString(bankTransfer.getDate()
									.toString()));
					bankReconciliationDetailVO.setDescription(bankTransfer
							.getDescription());
					bankReconciliationDetailVO.setUseCase(BankTransfer.class
							.getSimpleName());
				}
			} else {
				bankReconciliationDetailVO.setReferenceNumber(transactionDetail
						.getTransaction().getJournalNumber());
				bankReconciliationDetailVO.setTransactionDateStr(DateFormat
						.convertDateToString(transactionDetail.getTransaction()
								.getTransactionTime().toString()));
				bankReconciliationDetailVO.setDescription(transactionDetail
						.getDescription());
				bankReconciliationDetailVO.setUsecaseRecordId(transactionDetail
						.getTransactionDetailId());
				bankReconciliationDetailVO.setUseCase(Transaction.class
						.getSimpleName());
			}
			bankReconciliationDetailVO.setReceiptSide(transactionDetail
					.getIsDebit());
			bankReconciliationDetailVO.setRecordId(transactionDetail
					.getTransactionDetailId());
			bankReconciliationDetailVO.setReconsoleStatus(reconcileStatus);
			if (transactionDetail.getIsDebit())
				bankReconciliationDetailVO.setReceiptAmount(AIOSCommons
						.formatAmount(transactionDetail.getAmount()));
			else
				bankReconciliationDetailVO.setPaymentAmount(AIOSCommons
						.formatAmount(transactionDetail.getAmount()));
			bankReconciliationDetailVO.setFlowId(count++);
			bankReconciliationDetailVOs.add(bankReconciliationDetailVO);
		}
		return bankReconciliationDetailVOs;
	}

	// Update Bank Reconciliation
	public BankReconciliation updateBankReconciliationDetails(
			BankReconciliation bankReconciliation, int index,
			boolean reconciliationStatus, String transactionDate)
			throws Exception {
		List<BankReconciliationDetail> reconciliationDetails = new LinkedList<BankReconciliationDetail>(
				bankReconciliation.getBankReconciliationDetails());
		BankReconciliationDetail bankReconciliationDetail = reconciliationDetails
				.get(index);

		reconciliationDetails.remove(index);
		reconciliationDetails.add(bankReconciliationDetail);
		return bankReconciliation;
	}

	// Save Bank Reconciliation
	@SuppressWarnings("rawtypes")
	public void saveBankReconciliation(BankReconciliation bankReconciliation,
			List<BankReconciliationDetail> bankReconciliationDetails,
			List<BankReconciliationDetail> bankReconciliationDetailsDel,
			List<BankReconciliationDetailVO> reconciliationDetailVOs)
			throws Exception {

		bankReconciliation
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (bankReconciliation != null
				&& bankReconciliation.getBankReconciliationId() != null
				&& bankReconciliation.getBankReconciliationId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		if (bankReconciliation.getBankReconciliationId() == null)
			SystemBL.saveReferenceStamp(BankReconciliation.class.getName(),
					getImplementation());
		if (null != bankReconciliationDetailsDel
				&& bankReconciliationDetailsDel.size() > 0)
			bankReconciliationService
					.deleteBankReconciliationDetail(bankReconciliationDetailsDel);
		bankReconciliation.setImplementation(getImplementation());
		bankReconciliation
				.setBankReconciliationDetails(new HashSet<BankReconciliationDetail>(
						bankReconciliationDetails));
		if (null != reconciliationDetailVOs
				&& reconciliationDetailVOs.size() > 0) {
			for (BankReconciliationDetailVO reconciliationDetailVO : reconciliationDetailVOs) {
				Class[] argTypes = null;
				AIOTechGenericDAO<Object> dao = WorkflowUtils
						.getEntityDAO(reconciliationDetailVO.getUseCase());
				Object recordObject = dao.findById(reconciliationDetailVO
						.getUsecaseRecordId());
				String nameTemp = recordObject.getClass().getName()
						.replace(".domain.entity.", ".DOMAIN.ENTITY.");
				String[] name = nameTemp.split(".DOMAIN.ENTITY.");
				if (name[name.length - 1].contains("_$$_javassist")) {
					name[name.length - 1] = name[name.length - 1].substring(0,
							name[name.length - 1].indexOf("_$$_javassist"));
				}
				String approvalflagmethodId = "setChequePresented";
				Object status = true;
				argTypes = new Class[] { Boolean.class };
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);
				dao.saveOrUpdate(recordObject);
			}
		}

		for (BankReconciliationDetail reconciliationDetail : bankReconciliationDetails) {
			if (reconciliationDetail.getUseCase().equals(
					DirectPayment.class.getSimpleName())
					|| reconciliationDetail.getUseCase().equals(
							BankDeposit.class.getSimpleName())) {
				Class[] argTypes = null;
				AIOTechGenericDAO<Object> dao = WorkflowUtils
						.getEntityDAO(reconciliationDetail.getUseCase().equals(
								BankDeposit.class.getSimpleName()) ? BankDepositDetail.class
								.getSimpleName() : reconciliationDetail
								.getUseCase());
				Object recordObject = dao.findById(reconciliationDetail
						.getUsecaseRecordId());
				String nameTemp = recordObject.getClass().getName()
						.replace(".domain.entity.", ".DOMAIN.ENTITY.");
				String[] name = nameTemp.split(".DOMAIN.ENTITY.");
				if (name[name.length - 1].contains("_$$_javassist")) {
					name[name.length - 1] = name[name.length - 1].substring(0,
							name[name.length - 1].indexOf("_$$_javassist"));
				}

				String approvalflagmethodId = "setChequePresented";
				Object status = false;
				argTypes = new Class[] { Boolean.class };
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);
				dao.saveOrUpdate(recordObject);
			}
		}

		bankReconciliationService.saveBankReconciliation(bankReconciliation,
				bankReconciliationDetails);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankReconciliation.class.getSimpleName(),
				bankReconciliation.getBankReconciliationId(), user,
				workflowDetailVo);

	}

	@SuppressWarnings("rawtypes")
	public void saveBankReconciliation(BankReconciliation bankReconciliation,
			List<BankReconciliationDetail> bankReconciliationDetails,
			List<BankReconciliationDetailVO> reconciliationDetailVOs)
			throws Exception {

		bankReconciliation
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (bankReconciliation != null
				&& bankReconciliation.getBankReconciliationId() != null
				&& bankReconciliation.getBankReconciliationId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		if (bankReconciliation.getBankReconciliationId() == null)
			SystemBL.saveReferenceStamp(BankReconciliation.class.getName(),
					getImplementation());
		bankReconciliation.setImplementation(getImplementation());
		bankReconciliation
				.setBankReconciliationDetails(new HashSet<BankReconciliationDetail>(
						bankReconciliationDetails));

		bankReconciliationService.saveBankReconciliation(bankReconciliation,
				bankReconciliationDetails);

		for (BankReconciliationDetailVO reconciliationDetail : reconciliationDetailVOs) {
			if (reconciliationDetail.getUseCase().equals(
					DirectPayment.class.getSimpleName())
					|| reconciliationDetail.getUseCase().equals(
							BankDepositDetail.class.getSimpleName())) {
				Class[] argTypes = null;
				AIOTechGenericDAO<Object> dao = WorkflowUtils
						.getEntityDAO(reconciliationDetail.getUseCase());
				Object recordObject = dao.findById(reconciliationDetail
						.getUsecaseRecordId());
				String nameTemp = recordObject.getClass().getName()
						.replace(".domain.entity.", ".DOMAIN.ENTITY.");
				String[] name = nameTemp.split(".DOMAIN.ENTITY.");
				if (name[name.length - 1].contains("_$$_javassist")) {
					name[name.length - 1] = name[name.length - 1].substring(0,
							name[name.length - 1].indexOf("_$$_javassist"));
				}

				String approvalflagmethodId = "setChequePresented";
				Object status = false;
				argTypes = new Class[] { Boolean.class };
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);
				dao.saveOrUpdate(recordObject);
			}
		}

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankReconciliation.class.getSimpleName(),
				bankReconciliation.getBankReconciliationId(), user,
				workflowDetailVo);

	}

	// Delete Bank Reconciliation
	public void deleteBankReconciliation(BankReconciliation bankReconciliation)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankReconciliation.class.getSimpleName(),
				bankReconciliation.getBankReconciliationId(), user,
				workflowDetailVo);

	}

	public void doBankReconciliationBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		BankReconciliation bankReconciliation = bankReconciliationService
				.getBankReconciliation(recordId);
		if (workflowDetailVO.isDeleteFlag())
			bankReconciliationService.deleteBankReconciliation(
					bankReconciliation,
					new ArrayList<BankReconciliationDetail>(bankReconciliation
							.getBankReconciliationDetails()));
		else
			saveBankReconciliationDocuments(bankReconciliation, false);
	}

	// Delete Bank Reconciliation
	public void deleteBankReconciliation(
			List<BankReconciliation> bankReconciliations) throws Exception {
		for (BankReconciliation bankReconciliation : bankReconciliations) {
			transactionBL.deleteTransaction(
					bankReconciliation.getBankReconciliationId(),
					BankReconciliation.class.getSimpleName());
			bankReconciliationService.deleteBankReconciliation(
					bankReconciliation,
					new ArrayList<BankReconciliationDetail>(bankReconciliation
							.getBankReconciliationDetails()));
		}
	}

	public void deleteAddBankReconciliation() throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(6l);
		List<BankReconciliationDetail> bankReconciliationDts = bankReconciliationService
				.getBankReconDetailsWithoutTrans(impl);
		Set<String> paymentNumbers = new HashSet<String>();
		for (BankReconciliationDetail dt : bankReconciliationDts)
			paymentNumbers.add(dt.getReferenceNumber());
		List<BankReconciliationDetail> bankReconciliationDetails = new ArrayList<BankReconciliationDetail>();
		BankReconciliationDetail bankReconciliationDetail = null;
		List<DirectPayment> directPayments = directPaymentBL
				.getDirectPaymentService().getDirectPaymentsByPaymentNumber(
						impl, paymentNumbers);
		for (DirectPayment directPayment : directPayments) {
			double paymentAmount = 0;
			bankReconciliationDetail = new BankReconciliationDetail();
			bankReconciliationDetail.setReferenceNumber(directPayment
					.getPaymentNumber());
			bankReconciliationDetail.setTransactionDate(directPayment
					.getPaymentDate());
			bankReconciliationDetail.setDescription(directPayment
					.getDescription());
			bankReconciliationDetail.setUseCase(DirectPayment.class
					.getSimpleName());
			bankReconciliationDetail
					.setBankReconciliation(bankReconciliationDts.get(0)
							.getBankReconciliation());
			for (DirectPaymentDetail dt : directPayment
					.getDirectPaymentDetails()) {
				paymentAmount += dt.getAmount();
			}
			List<Transaction> transactions = transactionBL
					.getTransactionService()
					.getTransactionByRecordIdAndUseCaseDetail(
							directPayment.getDirectPaymentId(),
							DirectPayment.class.getSimpleName());
			for (Transaction transaction : transactions) {
				for (TransactionDetail dt : transaction.getTransactionDetails()) {
					if ((long) dt.getCombination().getCombinationId() == (long) directPayment
							.getCombination().getCombinationId()) {
						bankReconciliationDetail.setRecordId(dt
								.getTransactionDetailId());
						break;
					}
				}
			}
			bankReconciliationDetail.setPayment(paymentAmount);
			bankReconciliationDetails.add(bankReconciliationDetail);
		}
		bankReconciliationService
				.saveBankReconciliationDetail(bankReconciliationDetails);
	}

	// Get implementation from session
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Save Documents
	private void saveBankReconciliationDocuments(
			BankReconciliation bankReconciliation, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(bankReconciliation,
				"bankReconciliationDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, bankReconciliation.getBankReconciliationId(),
					"BankReconciliation", "bankReconciliationDocs");
			docVO.setDirStruct(dirStucture);
		}
		documentBL.saveUploadDocuments(docVO);
	}

	public boolean validateReconciliationDates(
			List<BankReconciliation> bankReconciliations, Date fromDate,
			Date toDate) throws Exception {
		LocalDate startDate = new LocalDate(fromDate);
		LocalDate endDate = new LocalDate(toDate);
		Map<LocalDate, Object> dateMap = new HashMap<LocalDate, Object>();
		for (LocalDate date = startDate; date.isBefore(endDate); date = date
				.plusDays(1))
			dateMap.put(date, date);
		for (BankReconciliation bankReconciliation : bankReconciliations) {
			if (dateMap.containsKey(new LocalDate(bankReconciliation
					.getReconciliationDate()))
					|| dateMap.containsKey(new LocalDate(bankReconciliation
							.getStatementEndDate())))
				return false;
		}
		return true;
	}

	public void bankReconciliationDataFix() {
		try {
			Implementation impl = new Implementation();
			impl.setImplementationId(6l);
			List<BankReconciliationDetail> bankReconciliationDetails = bankReconciliationService
					.getAllBankReconciliationDetails();
			DirectPayment directPayment = null;
			BankTransfer bankTransfer = null;
			for (BankReconciliationDetail bankReconciliationDetail : bankReconciliationDetails) {
				if (bankReconciliationDetail.getUseCase().trim()
						.equals(DirectPayment.class.getSimpleName())) {
					directPayment = directPaymentBL
							.getDirectPaymentService()
							.getDirectPaymentByReference(
									bankReconciliationDetail
											.getReferenceNumber(),
									impl);
					if (null != directPayment)
						bankReconciliationDetail
								.setUsecaseRecordId(directPayment
										.getDirectPaymentId());
				} else if (bankReconciliationDetail.getUseCase().trim()
						.equals(BankTransfer.class.getSimpleName())) {
					bankTransfer = bankTransferBL
							.getBankTransferService()
							.getBankTransferByReference(
									bankReconciliationDetail
											.getReferenceNumber(),
									impl);
					if (null != bankTransfer)
						bankReconciliationDetail
								.setUsecaseRecordId(bankTransfer
										.getBankTransferId());
				}
			}
			bankReconciliationService
					.saveBankReconciliationDetail(bankReconciliationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Getters and Setters
	public BankReconciliationService getBankReconciliationService() {
		return bankReconciliationService;
	}

	public void setBankReconciliationService(
			BankReconciliationService bankReconciliationService) {
		this.bankReconciliationService = bankReconciliationService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public BankBL getBankBL() {
		return bankBL;
	}

	public void setBankBL(BankBL bankBL) {
		this.bankBL = bankBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public Implementation getImplementationId() {
		Implementation implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public BankDepositBL getBankDepositBL() {
		return bankDepositBL;
	}

	public void setBankDepositBL(BankDepositBL bankDepositBL) {
		this.bankDepositBL = bankDepositBL;
	}

	public ReconciliationAdjustmentBL getReconciliationAdjustmentBL() {
		return reconciliationAdjustmentBL;
	}

	public void setReconciliationAdjustmentBL(
			ReconciliationAdjustmentBL reconciliationAdjustmentBL) {
		this.reconciliationAdjustmentBL = reconciliationAdjustmentBL;
	}

	public BankTransferBL getBankTransferBL() {
		return bankTransferBL;
	}

	public void setBankTransferBL(BankTransferBL bankTransferBL) {
		this.bankTransferBL = bankTransferBL;
	}
}
