package com.aiotech.aios.accounts.service.bl;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AccountsJobBL implements Job {

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		Map<?, ?> contextSchedule = context.getJobDetail().getJobDataMap();
		{
			AssetDepreciationBL assetDepreciationBL = (AssetDepreciationBL) contextSchedule
					.get("depreciationSchedule");

			try {
				if (assetDepreciationBL != null) {
					assetDepreciationBL.createDepreciation();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		{
			AssetInsurancePremiumBL assetInsurancePremiumBL = (AssetInsurancePremiumBL) contextSchedule
					.get("assetInsurancePremium");

			try {
				if (assetInsurancePremiumBL != null) {
					assetInsurancePremiumBL.premiumDueAutoPayment();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

}
