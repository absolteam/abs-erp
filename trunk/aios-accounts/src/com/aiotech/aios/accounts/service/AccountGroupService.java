package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AccountGroupDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AccountGroupService {

	private AIOTechGenericDAO<AccountGroup> accountGroupDAO;
	private AIOTechGenericDAO<AccountGroupDetail> accountGroupDetailDAO;

	public List<AccountGroup> getAllAccountGroups(
			Implementation implementation) throws Exception {
		return accountGroupDAO.findByNamedQuery("getAllAccountGroups",
				implementation);
	}
	
	public List<AccountGroup> getAccountGroupByTypes(
			Implementation implementation, int type1, int type2) throws Exception {
		return accountGroupDAO.findByNamedQuery("getAccountGroupByTypes",
				implementation, type1, type2);
	}
	
	public List<AccountGroup> getAccountGroupByType(
			Implementation implementation, int assetType) throws Exception {
		return accountGroupDAO.findByNamedQuery("getAccountGroupByType",
				implementation, assetType);
	}

	public AccountGroup getAccountGroupbyId(long groupId)
			throws Exception {
		return accountGroupDAO.findByNamedQuery("getAccountGroupbyId",
				groupId).get(0);
	}

	public List<AccountGroupDetail> getAccountGroupDetails(
			long groupId) throws Exception {
		return accountGroupDetailDAO.findByNamedQuery("getAccountGroupDetails",
				groupId);
	} 

	public void saveAccountGroup(AccountGroup accountGroup,
			List<AccountGroupDetail> accountGroupDetails)
			throws Exception {
		accountGroupDAO.saveOrUpdate(accountGroup);
		for (AccountGroupDetail accountGroupDetail : accountGroupDetails)
			accountGroupDetail.setAccountGroup(accountGroup);
		accountGroupDetailDAO.saveOrUpdateAll(accountGroupDetails);
	}
	
	
	public void deleteAccountGroupDetail(
			List<AccountGroupDetail> accountGroupDetails) throws Exception {
		accountGroupDetailDAO.deleteAll(accountGroupDetails);
	}
	
	public void deleteAccountGroup(AccountGroup accountGroup,
			List<AccountGroupDetail> accountGroupDetails) throws Exception {
		accountGroupDetailDAO.deleteAll(accountGroupDetails);
		accountGroupDAO.delete(accountGroup);
	} 
	
	// Getters & Setters
	
	public AIOTechGenericDAO<AccountGroupDetail> getAccountGroupDetailDAO() {
		return accountGroupDetailDAO;
	}

	public void setAccountGroupDetailDAO(
			AIOTechGenericDAO<AccountGroupDetail> accountGroupDetailDAO) {
		this.accountGroupDetailDAO = accountGroupDetailDAO;
	}

	public AIOTechGenericDAO<AccountGroup> getAccountGroupDAO() {
		return accountGroupDAO;
	}

	public void setAccountGroupDAO(AIOTechGenericDAO<AccountGroup> accountGroupDAO) {
		this.accountGroupDAO = accountGroupDAO;
	} 
}
