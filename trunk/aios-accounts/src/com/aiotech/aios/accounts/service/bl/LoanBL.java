package com.aiotech.aios.accounts.service.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.aiotech.aios.accounts.action.LoanInformationAction;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Eibor;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.LoanDetail;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.CalendarService;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.LoanService;
import com.aiotech.aios.accounts.to.EmiDetailTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class LoanBL {
	private int iTotalRecords;
	private int iTotalDisplayRecords;

	private LoanService loanService;
	private List<Loan> loanList;
	private Loan loan;
	private EiborBL eiborBL;
	private CurrencyService currencyService;
	private TransactionBL transactionBL;
	private GLChartOfAccountService accountService;
	private CombinationBL combinationBL;
	private CalendarService calendarService;
	private CommentBL commentBL;
	private EmiDetailBL emiDetailBL;
	private List<EmiDetailTO> emiTOList = new ArrayList<EmiDetailTO>();
	private EmiDetailTO emiTo = null;
	private double emiCumulative = 0;
	private WorkflowEnterpriseService workflowEnterpriseService;

	public JSONObject getLoanlist(Implementation implementation)
			throws Exception {
		loanList = this.getLoanService().getAllLoans(implementation);
		if (loanList != null && !loanList.equals("")) {
			iTotalRecords = loanList.size();
			iTotalDisplayRecords = loanList.size();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Loan list : loanList) {
				JSONArray array = new JSONArray();
				array.add(list.getLoanId());
				array.add(list.getLoanNumber());
				array.add(Constants.Accounts.LoanType.get(list.getLoanType()));
				array.add(DateFormat.convertDateToString(list.getSanctionDate()
						.toString()));
				if (null != list.getEibor() && !list.getEibor().equals(""))
					array.add(Constants.Accounts.EiborType.get(list.getEibor()
							.getEiborType()));
				else
					array.add("Nil");
				array.add(list.getAmount());
				array.add(list.getBankAccount().getBank().getBankName());
				array.add(list.getBankAccount().getAccountNumber());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			return jsonResponse;
		} else {
			return null;
		}
	}

	public List<LoanInformationAction> convertEibor(List<Eibor> eiborList) {
		List<LoanInformationAction> eibor = new ArrayList<LoanInformationAction>();
		for (Eibor list : eiborList) {
			eibor.add(convertType(list));
		}
		return eibor;
	}

	private static LoanInformationAction convertType(Eibor list) {
		LoanInformationAction info = new LoanInformationAction();
		info.setEiborId(list.getEiborId());
		Object object = Constants.Accounts.EiborType.get(list.getEiborType());
		info.setEiborTypes(object.toString());
		info.setEiborDate(DateFormat.convertDateToString(list.getEiborDate()
				.toString()));
		info.setEiborRate(list.getEiborRate());
		return info;
	}

	public void saveLoanEntry(Loan loan, List<LoanDetail> detail,
			List<LoanCharge> chargesDetail, String page) throws Exception {
		this.getLoanService().saveOrUpdate(loan, detail, chargesDetail);

		if (loan.getRelativeId() == null) {
			loan.setRelativeId(loan.getLoanId());
			this.getLoanService().saveOrUpdate(loan, detail, chargesDetail);
		}

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
						.getCode());
		workflowEnterpriseService.generateNotificationsAgainstDataEntry("Loan",
				loan.getLoanId(), user, workflowDetailVo);

		if (page.equalsIgnoreCase("add"))
			this.postTransaction(loan, detail);
	}

	public void postTransaction(Loan loan, List<LoanDetail> loanDetailList)
			throws Exception {
		List<TransactionDetail> detailList = new ArrayList<TransactionDetail>();
		double loanAmount = 0;
		java.util.Date loanDate;
		loanAmount = loanDetailList.get(0).getAmount();
		loanDate = loanDetailList.get(0).getDate();
		List<Category> categoryList = this.getTransactionBL().getCategoryBL()
				.getCategoryService()
				.getAllActiveCategories(loan.getImplementation());
		Category category = new Category();
		if (null != categoryList) {
			for (Category list : categoryList) {
				if (list.getName().equalsIgnoreCase("Loan Details")) {
					category.setCategoryId(list.getCategoryId());
					break;
				}
			}
		}
		if (null == category || category.equals("")) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(loan.getImplementation(), "Loan Details");
		} else if (null == category.getCategoryId()) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(loan.getImplementation(), "Loan Details");
		}
		Transaction transaction = getTransactionBL().createTransaction(
				loan.getCurrency().getCurrencyId(), loan.getDescription(),
				loanDate, category, loan.getLoanId(),
				Loan.class.getSimpleName());
		TransactionDetail transctionDetail = getTransactionBL()
				.createTransactionDetail(
						loanAmount,
						loan.getCombinationByLoanAccountId().getCombinationId(),
						new Boolean(false),
						"Loan(" + loan.getLoanNumber() + ") Credited.", null, null);
		detailList.add(transctionDetail);
		BankAccount bankAccount = getEiborBL().getBankService()
				.getBankAccountInfo(loan.getBankAccount().getBankAccountId());
		transctionDetail = getTransactionBL().createTransactionDetail(
				loanAmount, bankAccount.getCombination().getCombinationId(),
				new Boolean(true),
				"Loan(" + loan.getLoanNumber() + ") Debited.", null, null);
		detailList.add(transctionDetail);
		getTransactionBL().saveTransaction(transaction, detailList);
		postShortTermTransaction(loan);
	}

	private void postShortTermTransaction(Loan loan) throws Exception {
		Calendar cal = transactionBL.getCalendarBL().getCalendarService()
				.getAtiveCalendar(loan.getImplementation());
		if (DateFormat.compareDates(loan.getSanctionDate(), cal.getEndTime())) {
			getLoanDueAmount(loan, cal.getEndTime());
		}
	}

	private void getLoanDueAmount(Loan loan, Date endTime) throws Exception {
		getEmiFrequency(loan, endTime);
	}

	private void getEmiFrequency(Loan loan, Date endTime) throws Exception {
		if (loan.getEmiFrequency() == 'M') {
			processEMIMonthly(loan, endTime);
		} else if (loan.getEmiFrequency() == 'Y') {
			processEMIYearly(loan, endTime);
		} else if (loan.getEmiFrequency() == 'D') {
			processEMIDaily(loan, endTime);
		} else if (loan.getEmiFrequency() == 'W') {
			processEMIWeekly(loan, endTime);
		} else if (loan.getEmiFrequency() == 'H') {
			processEMISemiAunnaul(loan, endTime);
		} else if (loan.getEmiFrequency() == 'Q') {
			processEMIQuaterly(loan, endTime);
		}
	}

	private void createTransaction(Loan loan, List<EmiDetailTO> emiDetailList)
			throws Exception {
		List<TransactionDetail> detailList = new ArrayList<TransactionDetail>();
		for (EmiDetailTO list : emiDetailList) {
			emiCumulative += list.getEmiCumulative();
		}
		List<Category> categoryList = this.getTransactionBL().getCategoryBL()
				.getCategoryService()
				.getAllActiveCategories(loan.getImplementation());
		Category category = new Category();
		if (null != categoryList) {
			for (Category list : categoryList) {
				if (list.getName().equalsIgnoreCase("Loan Details")) {
					category.setCategoryId(list.getCategoryId());
				}
			}
		}
		if (null == category || category.equals("")) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(loan.getImplementation(), "Loan Details");
		} else if (null == category.getCategoryId()) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(loan.getImplementation(), "Loan Details");
		}
		Transaction transaction = getTransactionBL().createTransaction(
				loan.getCurrency().getCurrencyId(), loan.getDescription(),
				new Date(), category, loan.getLoanId(),
				Loan.class.getSimpleName());
		TransactionDetail transctionDetail = getTransactionBL()
				.createTransactionDetail(
						emiCumulative,
						loan.getCombinationByShortTermAccountId()
								.getCombinationId(),
						new Boolean(false),
						"Short term for Loan(" + loan.getLoanNumber()
								+ ") Credited.", null, null);
		detailList.add(transctionDetail);
		transctionDetail = getTransactionBL().createTransactionDetail(
				emiCumulative,
				loan.getCombinationByLoanAccountId().getCombinationId(),
				new Boolean(true),
				"Loan term for Loan(" + loan.getLoanNumber() + ") Debited.",
				null, null);
		detailList.add(transctionDetail);
		getTransactionBL().saveTransaction(transaction, detailList);
	}

	private void processEMIMonthly(Loan loan, Date endTime) throws Exception {
		int monthVariance = 0;
		Date nextMonth;
		if (DateFormat.compareDates(loan.getSanctionDate(), new Date())) {
			nextMonth = loan.getSanctionDate();
			monthVariance = DateFormat.monthsBetween(endTime, new Date());
		} else {
			nextMonth = new Date();
			monthVariance = DateFormat.monthsBetween(loan.getEmiEndDate(),
					endTime);
		}
		emiTOList = new ArrayList<EmiDetailTO>();
		for (int i = 0; i <= monthVariance; i++) {
			emiTo = new EmiDetailTO();
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(nextMonth);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextMonth);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			Date currentMonth = nextMonth;
			nextMonth = DateFormat.addMonth(nextMonth);
			emiDetailBL.emiSetValues(loan, currentMonth, nextMonth);
			emiTo.setEmiCumulative(emiDetailBL.getEmiTo().getEmiCumulative());
			emiTOList.add(emiTo);
		}
		createTransaction(loan, emiTOList);
	}

	private void processEMIYearly(Loan loan, Date endTime) throws Exception {
		int yearVariance = 0;
		Date nextYear;
		if (DateFormat.compareDates(loan.getSanctionDate(), new Date())) {
			yearVariance = DateFormat.yearDifference(loan.getEmiFirstDuedate(),
					endTime);
			nextYear = loan.getSanctionDate();
		} else {
			yearVariance = DateFormat.yearDifference(new Date(), endTime);
			nextYear = new Date();
		}
		emiTOList = new ArrayList<EmiDetailTO>();
		for (int i = 0; i <= yearVariance; i++) {
			emiTo = new EmiDetailTO();
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(nextYear);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextYear);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			Date currentMonth = nextYear;
			nextYear = DateFormat.addMonth(nextYear);
			emiDetailBL.emiSetValues(loan, currentMonth, nextYear);
			emiTOList.add(emiTo);
		}
		createTransaction(loan, emiTOList);
	}

	private void processEMIDaily(Loan loan, Date endTime) throws Exception {
		long dayVariance = 0;
		Date nextDay;
		if (DateFormat.compareDates(loan.getSanctionDate(), new Date())) {
			nextDay = loan.getEmiFirstDuedate();
			dayVariance = DateFormat.dayVariance(loan.getEmiFirstDuedate(),
					endTime);
		} else {
			nextDay = new Date();
			dayVariance = DateFormat.dayVariance(new Date(), endTime);
		}
		emiTOList = new ArrayList<EmiDetailTO>();
		for (int i = 0; i <= dayVariance; i++) {
			emiTo = new EmiDetailTO();
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(nextDay);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextDay);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			Date currentMonth = nextDay;
			nextDay = DateFormat.addMonth(nextDay);
			emiDetailBL.emiSetValues(loan, currentMonth, nextDay);
			emiTOList.add(emiTo);
		}
		createTransaction(loan, emiTOList);
	}

	private void processEMIWeekly(Loan loan, Date endTime) throws Exception {
		int weekVariance = 0;
		Date nextWeek = loan.getEmiFirstDuedate();
		if (DateFormat.compareDates(loan.getSanctionDate(), new Date())) {
			nextWeek = loan.getEmiFirstDuedate();
			weekVariance = DateFormat.weekVariance(loan.getEmiFirstDuedate(),
					endTime);
		} else {
			nextWeek = new Date();
			weekVariance = DateFormat.weekVariance(new Date(), endTime);
		}
		emiTOList = new ArrayList<EmiDetailTO>();
		for (int i = 0; i <= weekVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(nextWeek);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextWeek);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			Date currentMonth = nextWeek;
			nextWeek = DateFormat.addMonth(nextWeek);
			emiDetailBL.emiSetValues(loan, currentMonth, nextWeek);
			emiTOList.add(emiTo);
		}
		createTransaction(loan, emiTOList);
	}

	private void processEMIQuaterly(Loan loan, Date endTime) throws Exception {
		int quaterVariance = 0;
		Date nextQuater;
		if (DateFormat.compareDates(loan.getSanctionDate(), new Date())) {
			nextQuater = loan.getEmiFirstDuedate();
			quaterVariance = DateFormat.quaterVariance(
					loan.getEmiFirstDuedate(), endTime);
		} else {
			nextQuater = new Date();
			quaterVariance = DateFormat.quaterVariance(new Date(), endTime);
		}
		emiTOList = new ArrayList<EmiDetailTO>();
		for (int i = 0; i <= quaterVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(nextQuater);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextQuater);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			Date currentMonth = nextQuater;
			nextQuater = DateFormat.addMonth(nextQuater);
			emiDetailBL.emiSetValues(loan, currentMonth, nextQuater);
			emiTOList.add(emiTo);
		}
		createTransaction(loan, emiTOList);
	}

	private void processEMISemiAunnaul(Loan loan, Date endTime)
			throws Exception {
		int semiVariance = 0;
		Date nextSemi = loan.getEmiFirstDuedate();
		if (DateFormat.compareDates(loan.getSanctionDate(), new Date())) {
			nextSemi = loan.getEmiFirstDuedate();
			semiVariance = DateFormat.halfYearlyVariance(
					loan.getEmiFirstDuedate(), endTime);
		} else {
			nextSemi = new Date();
			semiVariance = DateFormat.halfYearlyVariance(new Date(), endTime);
		}
		emiTOList = new ArrayList<EmiDetailTO>();
		for (int i = 0; i <= semiVariance; i++) {
			emiTo = new EmiDetailTO();
			emiTo.setLineNumber(emiTOList.size() + 1);
			java.util.Calendar calendar = java.util.Calendar.getInstance();
			calendar.setTime(nextSemi);
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(nextSemi);
			emiTo.setEmiDueDate(DateFormat.convertDateToString(dateVal));
			if (emiTOList.size() == 0) {
				emiCumulative = 0;
			}
			Date currentMonth = nextSemi;
			nextSemi = DateFormat.addMonth(nextSemi);
			emiDetailBL.emiSetValues(loan, currentMonth, nextSemi);
			emiTOList.add(emiTo);
		}
		createTransaction(loan, emiTOList);
	}

	public Combination createAnalysisCombination(long loanCombinationId,
			Implementation implementation, String accountCode) throws Exception {
		Combination combination = combinationBL.getCombinationService()
				.getCombination(
						Integer.parseInt(String.valueOf(loanCombinationId)));
		Combination combinationAnalysis = combinationBL
				.createAnalysisCombination(combination, accountCode, false);
		combinationBL.getCombinationService().updateCombination(
				combinationAnalysis);
		return combinationAnalysis;
	}

	public LoanService getLoanService() {
		return loanService;
	}

	public void setLoanService(LoanService loanService) {
		this.loanService = loanService;
	}

	public List<Loan> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<Loan> loanList) {
		this.loanList = loanList;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public EiborBL getEiborBL() {
		return eiborBL;
	}

	public void setEiborBL(EiborBL eiborBL) {
		this.eiborBL = eiborBL;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CalendarService getCalendarService() {
		return calendarService;
	}

	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}

	public EmiDetailBL getEmiDetailBL() {
		return emiDetailBL;
	}

	public void setEmiDetailBL(EmiDetailBL emiDetailBL) {
		this.emiDetailBL = emiDetailBL;
	}

	public List<EmiDetailTO> getEmiTOList() {
		return emiTOList;
	}

	public void setEmiTOList(List<EmiDetailTO> emiTOList) {
		this.emiTOList = emiTOList;
	}

	public EmiDetailTO getEmiTo() {
		return emiTo;
	}

	public void setEmiTo(EmiDetailTO emiTo) {
		this.emiTo = emiTo;
	}

	public double getEmiCumulative() {
		return emiCumulative;
	}

	public void setEmiCumulative(double emiCumulative) {
		this.emiCumulative = emiCumulative;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
