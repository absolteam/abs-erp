package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsVO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class BankReceiptsService {

	private AIOTechGenericDAO<BankReceipts> bankReceiptsDAO;
	private AIOTechGenericDAO<BankReceiptsDetail> bankReceiptsDetailDAO;

	// get all bank receipts
	public List<BankReceipts> getAllBankReceipts(Implementation implementation)
			throws Exception {
		return this.getBankReceiptsDAO().findByNamedQuery("getAllBankReceipts",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// get all bank receipts
	public List<BankReceipts> getAllBankReceiptsOnlyReceipts(
			Implementation implementation) throws Exception {
		return this.getBankReceiptsDAO().findByNamedQuery(
				"getAllBankReceiptsOnlyReceipts", implementation);
	}

	// get all paid bank receipts
	public List<BankReceipts> getPaidBankReceipts(
			Implementation implementation, byte modeOfPayment) throws Exception {
		return this.getBankReceiptsDAO().findByNamedQuery(
				"getPaidBankReceipts", implementation, modeOfPayment);
	}

	// get all paid bank receipts by receipts type
	public List<BankReceipts> getBankReceiptsByType(
			Implementation implementation, long receiptTypeId,
			byte paymentStatus) throws Exception {
		return this.getBankReceiptsDAO().findByNamedQuery(
				"getBankReceiptsByType", implementation, receiptTypeId,
				paymentStatus);
	}

	// get all paid bank receipts by receipts type
	public List<BankReceiptsDetail> getBankReceiptsDetailByType(
			Implementation implementation, long receiptTypeId,
			byte paymentStatus) throws Exception {
		return this.getBankReceiptsDetailDAO().findByNamedQuery(
				"getBankReceiptsDetailByType", implementation, receiptTypeId,
				paymentStatus);
	}

	@SuppressWarnings("unchecked")
	public List<BankReceiptsDetail> getBankReceiptsByReceipt(
			Implementation implementation, byte paymentStatus, String dateFrom,
			String dateTo, String paymentMode) throws Exception {
		Criteria criteria = bankReceiptsDetailDAO.createCriteria();

		criteria.createAlias("lookupDetail", "ref",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankReceipts", "br",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("br.implementation", implementation));
		criteria.add(Restrictions.eq("paymentStatus", paymentStatus));
		criteria.add(Restrictions.or(Restrictions.eq("isDeposited", false),
				Restrictions.isNull("isDeposited")));
		if (null != dateFrom && !("").equals(dateFrom))
			criteria.add(Restrictions.ge("br.receiptsDate",
					DateFormat.convertStringToDate(dateFrom)));
		if (null != dateTo && !("").equals(dateTo))
			criteria.add(Restrictions.le("br.receiptsDate",
					DateFormat.convertStringToDate(dateTo)));
		if (null != paymentMode && !("").equals(paymentMode))
			criteria.add(Restrictions.eq("paymentMode",
					Byte.valueOf(paymentMode)));
		Set<BankReceiptsDetail> uniqueRecords = new HashSet<BankReceiptsDetail>(
				criteria.list());
		List<BankReceiptsDetail> bankReceipts = new ArrayList<BankReceiptsDetail>(
				uniqueRecords);
		return bankReceipts;
	}

	// save bank receipts
	public void saveBankReceipts(BankReceipts bankReceipts,
			List<BankReceiptsDetail> bankReceiptsDetails) throws Exception {
		this.getBankReceiptsDAO().saveOrUpdate(bankReceipts);
		for (BankReceiptsDetail detail : bankReceiptsDetails) {
			detail.setBankReceipts(bankReceipts);
		}
		bankReceiptsDetailDAO.saveOrUpdateAll(bankReceiptsDetails);
	}

	// save bank receipts detail
	public void updateBankReceiptsDetail(
			List<BankReceiptsDetail> bankReceiptsDetails) throws Exception {
		bankReceiptsDetailDAO.saveOrUpdateAll(bankReceiptsDetails);
	}

	public void updateBankReceiptsDetail(BankReceiptsDetail bankReceiptsDetail)
			throws Exception {
		bankReceiptsDetailDAO.saveOrUpdate(bankReceiptsDetail);
	}

	public void mergeBankReceiptsDetail(
			List<BankReceiptsDetail> bankReceiptsDetails) throws Exception {
		for (BankReceiptsDetail bankReceiptsDetail : bankReceiptsDetails) {
			bankReceiptsDetailDAO.merge(bankReceiptsDetail);
		}
	}

	// get all bank receipts
	public BankReceipts findByBankReceiptsId(long bankReceiptsId)
			throws Exception {
		return bankReceiptsDAO.findByNamedQuery("findByBankReceiptsId",
				bankReceiptsId).get(0);
	}

	// Get PDC Payment for current period by receipt id
	public BankReceipts findPDCBankReceiptsById(long bankReceiptsDetailId)
			throws Exception {
		return bankReceiptsDAO.findByNamedQuery("findPDCBankReceiptsById",
				bankReceiptsDetailId).get(0);
	}

	// get all bank receipts detail by receipt id
	public List<BankReceiptsDetail> getBankReceiptsDetailByReceipts(
			BankReceipts bankReceipts) throws Exception {
		return bankReceiptsDetailDAO.findByNamedQuery(
				"getBankReceiptsDetailByReceipts", bankReceipts);
	}

	// get all bank receipts detail by receipt id
	public BankReceipts getBankReceiptsByRecordIdUsecase(long recordId,
			String useCase) throws Exception {
		List<BankReceipts> bankReceipts = bankReceiptsDAO.findByNamedQuery(
				"getBankReceiptsByRecordIdUsecase", recordId, useCase);
		return null != bankReceipts && bankReceipts.size() > 0 ? bankReceipts
				.get(0) : null;
	}

	// get bank receipts detail by bank receipt detail id
	public BankReceiptsDetail getBankReceiptsDetailById(
			long bankReceiptsDetailId) throws Exception {
		return this
				.getBankReceiptsDetailDAO()
				.findByNamedQuery("getBankReceiptsDetailById",
						bankReceiptsDetailId).get(0);
	}

	// get all bank receipts detail by cash with implementation
	public List<BankReceiptsDetail> getAllBankReceiptDetails(
			Implementation implementation, byte modeOfPayment) throws Exception {
		return this.getBankReceiptsDetailDAO().findByNamedQuery(
				"getAllBankReceiptDetails", implementation, modeOfPayment);
	}

	// get all bank receipts detail by cash with implementation
	public List<BankReceiptsDetail> getUnDepositedCashReceiptDetails(
			Implementation implementation, byte modeOfPayment) throws Exception {
		return this.getBankReceiptsDetailDAO().findByNamedQuery(
				"getUnDepositedCashReceiptDetails", implementation,
				modeOfPayment);
	}

	// delete bank receipt detail
	public void deleteBankReceiptsDetail(
			List<BankReceiptsDetail> deleteBankReceiptsDetail) throws Exception {
		this.getBankReceiptsDetailDAO().deleteAll(deleteBankReceiptsDetail);
	}

	// delete bank receipts & details
	public void deleteBankReceipts(BankReceipts bankReceipts) throws Exception {
		this.getBankReceiptsDetailDAO().deleteAll(
				new ArrayList<BankReceiptsDetail>(bankReceipts
						.getBankReceiptsDetails()));
		this.getBankReceiptsDAO().delete(bankReceipts);
	}

	@SuppressWarnings("unchecked")
	public List<BankReceipts> getFilteredBankReceipts(
			Implementation implementation, Long bankReceiptId, Long source,
			Integer currencyId, Long receiptTypeId, Byte paymentModeId,
			Byte statusId, Date fromDate, Date toDate) throws Exception {
		Criteria criteria = bankReceiptsDAO.createCriteria();

		criteria.createAlias("lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("currency", "cur", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.person", "s_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customer", "cus", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.personByPersonId", "cus_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.cmpDeptLocation", "cus_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus_cdl.company", "cus_dpt_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.company", "cus_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByPersonId", "per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByCreatedBy", "cpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankReceiptsDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.lookupDetail", "ref",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (bankReceiptId != null && bankReceiptId > 0) {
			criteria.add(Restrictions.eq("bankReceiptsId", bankReceiptId));
		}

		/*
		 * if (source != null && source > 0) {
		 * criteria.add(Restrictions.eq("bankReceiptsId", source)); }
		 */

		if (currencyId != null && currencyId > 0) {
			criteria.add(Restrictions.eq("cur.currencyId", currencyId));
		}

		if (receiptTypeId != null && receiptTypeId > 0) {
			criteria.add(Restrictions.eq("ld.lookupDetailId", receiptTypeId));
		}

		if (paymentModeId != null && paymentModeId > 0) {
			criteria.add(Restrictions.eq("detail.paymentMode", paymentModeId));
		}

		if (statusId != null && statusId > 0) {
			criteria.add(Restrictions.eq("detail.paymentStatus", statusId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("receiptsDate", fromDate, toDate));
		}

		Set<BankReceipts> uniqueRecords = new HashSet<BankReceipts>(
				criteria.list());

		return null != uniqueRecords && uniqueRecords.size() > 0 ? new ArrayList<BankReceipts>(
				uniqueRecords) : null;

	}

	@SuppressWarnings("unchecked")
	public List<BankReceipts> getFilteredBankReceipts(BankReceiptsVO vo)
			throws Exception {
		Criteria criteria = bankReceiptsDAO.createCriteria();

		criteria.createAlias("lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("currency", "cur", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.person", "s_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customer", "cus", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.personByPersonId", "cus_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.cmpDeptLocation", "cus_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus_cdl.company", "cus_dpt_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.company", "cus_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByPersonId", "per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankReceiptsDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.lookupDetail", "ref",
				CriteriaSpecification.LEFT_JOIN);

		if (vo.getImplementation() != null) {
			criteria.add(Restrictions.eq("implementation",
					vo.getImplementation()));
		}

		if (vo.getRecordId() != null && vo.getRecordId() > 0) {
			criteria.add(Restrictions.eq("recordId", vo.getRecordId()));
		}

		if (vo.getUseCase() != null && !vo.getUseCase().equals("")) {
			criteria.add(Restrictions.eq("useCase", vo.getUseCase()));
		}

		Set<BankReceipts> uniqueRecords = new HashSet<BankReceipts>(
				criteria.list());

		return null != uniqueRecords && uniqueRecords.size() > 0 ? new ArrayList<BankReceipts>(
				uniqueRecords) : null;
	}

	@SuppressWarnings("unchecked")
	public List<BankReceiptsDetail> getChequeBankReceipts(
			Implementation implementation, String fromdate, String todate) {

		Criteria criteria = bankReceiptsDetailDAO.createCriteria();
		criteria.createAlias("bankReceipts", "br",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.personByPersonId", "ppid",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.customer", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "cstpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cstcp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.cmpDeptLocation", "cstcdpt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cstcdpt.company", "cstcdptcpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.supplier", "sp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "sppr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "spcp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "spcdpt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("spcdpt.company", "spcdptcpy",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("br.implementation", implementation));
		criteria.add(Restrictions.isNotNull("institutionName"));
		if (fromdate != null && !("").equals(fromdate))
			criteria.add(Restrictions.and(
					Restrictions.isNotNull("chequeDate"),
					Restrictions.gt("chequeDate",
							DateFormat.convertStringToDate(fromdate))));
		if (todate != null && !("").equals(todate))
			criteria.add(Restrictions.and(
					Restrictions.isNotNull("chequeDate"),
					Restrictions.le("chequeDate",
							DateFormat.convertStringToDate(todate))));
		Set<BankReceiptsDetail> uniqueRecords = new HashSet<BankReceiptsDetail>(
				criteria.list());
		return null != uniqueRecords && uniqueRecords.size() > 0 ? new ArrayList<BankReceiptsDetail>(
				uniqueRecords) : null;
	}

	// Get PDC Payment for current period by receipt id
	public BankReceipts getReceiptBySalesReference(String receiptNumber,
			Implementation implementation) throws Exception {
		List<BankReceipts> receipts = this.getBankReceiptsDAO()
				.findByNamedQuery("getReceiptBySalesReference", receiptNumber,
						implementation);
		if (receipts != null && receipts.size() > 0)
			return receipts.get(0);
		else
			return null;
	}

	public List<BankReceiptsDetail> getPDCBankReceipts(Date chequeDate,
			Implementation implementation, Byte paymentMode) throws Exception {
		return bankReceiptsDetailDAO.findByNamedQuery("getPDCBankReceipts",
				implementation, paymentMode, chequeDate,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Getter Setters
	public AIOTechGenericDAO<BankReceipts> getBankReceiptsDAO() {
		return bankReceiptsDAO;
	}

	public void setBankReceiptsDAO(
			AIOTechGenericDAO<BankReceipts> bankReceiptsDAO) {
		this.bankReceiptsDAO = bankReceiptsDAO;
	}

	public AIOTechGenericDAO<BankReceiptsDetail> getBankReceiptsDetailDAO() {
		return bankReceiptsDetailDAO;
	}

	public void setBankReceiptsDetailDAO(
			AIOTechGenericDAO<BankReceiptsDetail> bankReceiptsDetailDAO) {
		this.bankReceiptsDetailDAO = bankReceiptsDetailDAO;
	}
}
