package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PaymentService {
	
	private AIOTechGenericDAO<Payment> paymentDAO;
	private AIOTechGenericDAO<PaymentDetail> paymentDetailDAO;
	
	public List<Payment> getAllPayments(Implementation implementation)throws Exception{
		return this.getPaymentDAO().findByNamedQuery("getAllPayments", implementation);
	}
	
	// Get all payments without implementation
	public List<Payment> getAllPayments() throws Exception {
		return this.getPaymentDAO().findByNamedQuery(
				"getAllPaymentsWithoutImplementation");
	}
	
	public Payment getPayment(long paymentId) throws Exception{
		return this.getPaymentDAO().findById(paymentId);
	}
	
	public Payment getPurchasePaymentDetails(long paymentId) throws Exception {
		List<Payment> paymentDetails = this.getPaymentDAO().findByNamedQuery(
				"getPurchasePaymentDetails", paymentId);
		if (null != paymentDetails && paymentDetails.size() > 0)
			return paymentDetails.get(0);
		else
			return null;
	}
	
	public Payment getByPaymentId(long paymentId) throws Exception{
		return this.getPaymentDAO().findByNamedQuery("getByPaymentId", paymentId).get(0);
	}
	
	public List<PaymentDetail> getPaymentDetailById(long paymentId)throws Exception{
		return this.getPaymentDetailDAO().findByNamedQuery("getPaymentDetailById", paymentId);
	}
	
	public void savePayment(Payment payment, List<PaymentDetail> detailList)
			throws Exception {
		this.getPaymentDAO().saveOrUpdate(payment);
		for (PaymentDetail list : detailList) {
			list.setPayment(payment);
		}
		this.getPaymentDetailDAO().saveOrUpdateAll(detailList);
	}
	
	// Save Payment List & Payment Details List
	public void savePayment(List<Payment> payments,
			List<PaymentDetail> paymentDetails) throws Exception {
		this.getPaymentDAO().saveOrUpdateAll(payments);
		this.getPaymentDetailDAO().saveOrUpdateAll(paymentDetails);
	}
	
	public void updatePayment(Payment payment) throws Exception {
		this.getPaymentDAO().saveOrUpdate(payment);
	}
	
	public void deletePayment(Payment payment, List<PaymentDetail> detailList)
			throws Exception {
		this.getPaymentDetailDAO().deleteAll(detailList);
		this.getPaymentDAO().delete(payment);
	}
	
	public void deletePaymentDetail(List<PaymentDetail> detailList)
			throws Exception {
		this.getPaymentDetailDAO().deleteAll(detailList);
	}
	
	//show all cheque payments
	public List<Payment> getAllChequePayments(
			Implementation implementation) throws Exception {
		return this.getPaymentDAO().findByNamedQuery(
				"getAllChequePayments", implementation);
	}
	
	@SuppressWarnings("unchecked")
	public List<Payment> getFilteredPayments(Implementation implementation, Long paymentId, Long receiveId, 
			Long supplierId, Date fromDate, Date toDate)throws Exception{
		
		Criteria criteria = paymentDAO.createCriteria();

		criteria.createAlias("bankAccount", "ba", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("voidPayments", "vp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("receive", "r", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.person", "s_p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.company", "s_cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "s_cdl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s_cdl.company", "cdl_cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("paymentDetails", "pd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pd.invoiceDetail", "pd_id", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pd_id.product", "id_pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pd_id.invoice", "id_in", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pd_id.receiveDetail", "id_rd", CriteriaSpecification.LEFT_JOIN);
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		if (paymentId != null && paymentId > 0) {
			criteria.add(Restrictions.eq("paymentId", paymentId));
		}
		
		if (receiveId != null && receiveId > 0) {
			criteria.add(Restrictions.eq("r.receiveId", receiveId));
		}
		
		if (supplierId != null && supplierId > 0) {
			criteria.add(Restrictions.eq("s.supplierId", supplierId));
		}
		
		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date",fromDate, toDate));
		}
		
		Set<Payment> uniqueRecord = new HashSet<Payment>(criteria.list());
		List<Payment> paymentList = new ArrayList<Payment>(uniqueRecord);
		
		return paymentList;
	}
	
	public AIOTechGenericDAO<PaymentDetail> getPaymentDetailDAO() {
		return paymentDetailDAO;
	}
	public void setPaymentDetailDAO(
			AIOTechGenericDAO<PaymentDetail> paymentDetailDAO) {
		this.paymentDetailDAO = paymentDetailDAO;
	}
	public AIOTechGenericDAO<Payment> getPaymentDAO() {
		return paymentDAO;
	}
	public void setPaymentDAO(AIOTechGenericDAO<Payment> paymentDAO) {
		this.paymentDAO = paymentDAO;
	}
}
