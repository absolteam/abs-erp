package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CustomerService {

	private AIOTechGenericDAO<Customer> customerDAO;
	private AIOTechGenericDAO<ShippingDetail> shippingDetailDAO;

	public List<Customer> getAllApprovedCustomers(Implementation implementation)
			throws Exception {
		return customerDAO.findByNamedQuery("getAllApprovedCustomers",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Customer> getAllCustomers(Implementation implementation)
			throws Exception {
		return customerDAO.findByNamedQuery("getAllCustomers", implementation);
	}

	public List<Customer> getAllPOSCustomers(Implementation implementation)
			throws Exception {
		return customerDAO.findByNamedQuery("getAllPOSCustomers",
				implementation);
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getAllCustomers(Implementation implementation,
			Session session) throws Exception {
		String getAllCustomers = "SELECT c FROM Customer c WHERE c.implementation=?";
		List<Customer> customers = session.createQuery(getAllCustomers)
				.setEntity(0, implementation).list();
		return customers;
	}

	public List<Customer> getCustomersByReceipts(Implementation implementation)
			throws Exception {
		return customerDAO.findByNamedQuery("getCustomersByReceipts",
				implementation);
	}

	public Customer getCustomerById(long customerId) throws Exception {
		return customerDAO.findById(customerId);
	}

	public Customer getCustomerDetails(long customerId) throws Exception {
		List<Customer> customers = customerDAO.findByNamedQuery(
				"getCustomerDetails", customerId);
		if (customers != null && customers.size() > 0)
			return customers.get(0);
		else
			return null;
	}

	public Customer getCustomerAccount(long customerId) throws Exception {
		return customerDAO.findByNamedQuery("getCustomerAccount", customerId)
				.get(0);
	}

	public List<ShippingDetail> getCustomerShippingSite(long customerId)
			throws Exception {
		return shippingDetailDAO.findByNamedQuery("getCustomerShippingSite",
				customerId);
	}

	public void saveCustomer(Customer customer,
			List<ShippingDetail> shippingDetails) throws Exception {
		customerDAO.saveOrUpdate(customer);
		for (ShippingDetail list : shippingDetails) {
			list.setCustomer(customer);
		}
		shippingDetailDAO.saveOrUpdateAll(shippingDetails);
	}

	public void saveCustomer(List<Customer> customers,
			List<ShippingDetail> shippingDetails) throws Exception {
		customerDAO.saveOrUpdateAll(customers);
		shippingDetailDAO.saveOrUpdateAll(shippingDetails);
	}

	public void saveCustomer(List<Customer> customers) throws Exception {
		customerDAO.saveOrUpdateAll(customers);
	}

	public void saveCustomer(Customer customer, ShippingDetail shippingDetail)
			throws Exception {
		customerDAO.saveOrUpdate(customer);
		shippingDetail.setCustomer(customer);
		shippingDetailDAO.saveOrUpdate(shippingDetail);
	}

	public void saveCustomer(Customer customer, ShippingDetail shippingDetail,
			Session session) throws Exception {
		session.saveOrUpdate(customer);
		shippingDetail.setCustomer(customer);
		session.saveOrUpdate(shippingDetail);
	}

	public void saveCustomers(List<Customer> customers,
			List<ShippingDetail> shippingDetails, Session session)
			throws Exception {
		session.saveOrUpdate(customers);
		session.saveOrUpdate(shippingDetails);
	}

	public void deleteCustomer(Customer customer) throws Exception {
		shippingDetailDAO.deleteAll(new ArrayList<ShippingDetail>(customer
				.getShippingDetails()));
		customerDAO.delete(customer);
	}

	public void saveCustomers(List<Customer> customers) throws Exception {
		customerDAO.saveOrUpdateAll(customers);
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerByCombination(Set<Long> combinationIds)
			throws Exception {
		Criteria criteria = customerDAO.createCriteria();
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("cb.combinationId", combinationIds));
		Set<Customer> uniqueRecord = new HashSet<Customer>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Customer>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getFilteredCustomers(Implementation implementation,
			Long customerId, Integer customerTypeId, Long creditTermId,
			Long refferalSourceId, Byte memberTypeId) throws Exception {
		Criteria criteria = customerDAO.createCriteria();

		criteria.createAlias("personByPersonId", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("company", "cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personBySaleRepresentative", "psr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("creditTerm", "ct",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cmb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cmb.accountByAnalysisAccountId", "an",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shippingDetails", "ship",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByRefferalSource", "ldrs",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("customerId", customerId));
		}

		if (customerTypeId != null && customerTypeId > 0) {
			criteria.add(Restrictions.eq("customerType", customerTypeId));
		}

		if (creditTermId != null && creditTermId > 0) {
			criteria.add(Restrictions.eq("ct.creditTermId", creditTermId));
		}

		if (refferalSourceId != null && refferalSourceId > 0) {
			criteria.add(Restrictions.eq("ldrs.lookupDetailId",
					refferalSourceId));
		}

		if (memberTypeId != null && memberTypeId > 0) {
			criteria.add(Restrictions.eq("memberType", memberTypeId));
		}

		return criteria.list();
	}

	public AIOTechGenericDAO<Customer> getCustomerDAO() {
		return customerDAO;
	}

	public void setCustomerDAO(AIOTechGenericDAO<Customer> customerDAO) {
		this.customerDAO = customerDAO;
	}

	public AIOTechGenericDAO<ShippingDetail> getShippingDetailDAO() {
		return shippingDetailDAO;
	}

	public void setShippingDetailDAO(
			AIOTechGenericDAO<ShippingDetail> shippingDetailDAO) {
		this.shippingDetailDAO = shippingDetailDAO;
	}
}
