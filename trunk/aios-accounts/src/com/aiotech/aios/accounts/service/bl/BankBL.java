package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AccountsDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankAccountVO;
import com.aiotech.aios.accounts.service.BankService;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.to.Constants.Accounts.BankAccountType;
import com.aiotech.aios.common.to.Constants.Accounts.BankCardType;
import com.aiotech.aios.common.to.Constants.Accounts.InstitutionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CompanyVO;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;
import com.opensymphony.xwork2.ActionContext;

public class BankBL {
	private BankService bankService;
	private LookupMasterBL lookupMasterBL;
	private CurrencyService currencyService;
	private CombinationBL combinationBL;
	private AIOTechGenericDAO<Person> personDAO;
	private AIOTechGenericDAO<Company> companyDAO;
	private TransactionBL transactionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<Person> getAllPersons(Implementation implementation)
			throws Exception {
		return this.getPersonDAO().findByNamedQuery("getAllPerson",
				implementation);
	}

	public List<Company> getAllCompany(Implementation implementation)
			throws Exception {
		return this.getCompanyDAO().findByNamedQuery("getAllCompanys",
				implementation);
	}

	public Map<Byte, String> getInstitutionType() {
		Map<Byte, String> institutionTypes = new HashMap<Byte, String>();
		for (InstitutionType institutionType : EnumSet
				.allOf(InstitutionType.class))
			institutionTypes.put(institutionType.getCode(), institutionType
					.name().replaceAll("_", " "));
		return institutionTypes;
	}

	public Map<Byte, BankAccountType> getBankAccountType() {
		Map<Byte, BankAccountType> bankAccountTypes = new HashMap<Byte, BankAccountType>();
		for (BankAccountType bankAccountType : EnumSet
				.allOf(BankAccountType.class))
			bankAccountTypes.put(bankAccountType.getCode(), bankAccountType);
		return bankAccountTypes;
	}

	public Map<Byte, BankCardType> getBankCardType() {
		Map<Byte, BankCardType> bankCardTypes = new HashMap<Byte, BankCardType>();
		for (BankCardType bankCardType : EnumSet.allOf(BankCardType.class))
			bankCardTypes.put(bankCardType.getCode(), bankCardType);
		return bankCardTypes;
	}

	// merge company & person into companyvo
	public List<CompanyVO> listPersonCompany(List<Person> personList,
			List<Company> companyList) throws Exception {
		CompanyVO companyVO = null;
		List<CompanyVO> companyVOList = new ArrayList<CompanyVO>();
		for (Person person : personList) {
			companyVO = new CompanyVO();
			companyVO.setCompanyPersonId("P-".concat(person.getPersonId()
					.toString()));
			companyVO.setTradeNo(person.getPersonNumber());
			companyVO.setCompanyName(person.getFirstName().concat(" ")
					.concat(person.getLastName()));
			companyVO.setCompanyTypeName("Person");
			companyVOList.add(companyVO);
		}
		for (Company company : companyList) {
			companyVO = new CompanyVO();
			companyVO.setCompanyPersonId("C-".concat(company.getCompanyId()
					.toString()));
			companyVO.setCompanyName(company.getCompanyName());
			companyVO.setCompanyTypeName("Company");
			companyVOList.add(companyVO);
		}
		return companyVOList;
	}

	// save bank info
	public void saveBank(Bank bank, List<BankAccount> bankaccounts)
			throws Exception {

		bank.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (bank != null && bank.getBankId() != null && bank.getBankId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		// Business/Use Case Save Call
		for (BankAccount bankAccount : bankaccounts) {
			if (null == bankAccount.getBankAccountId()) {
				Combination analysisCombination = null;
				if ((byte) InstitutionType.get(bank.getInstitutionType())
						.getCode() != (byte) InstitutionType.Credit_Unions
						.getCode()) {
					analysisCombination = combinationBL
							.createAnalysisCombination(
									bank.getCombination(),
									bankAccount
											.getBranchName()
											.concat(" {")
											.concat(bankAccount
													.getAccountNumber())
											.concat("}"), false);

				} else {
					analysisCombination = combinationBL
							.createAnalysisCombination(
									bank.getCombination(),
									BankCardType
											.get(bankAccount.getCardType())
											.name()
											.concat(" {")
											.concat(bankAccount
													.getAccountNumber())
											.concat("}"), false);

				}
				bankAccount.setCombination(analysisCombination);

				if (bankAccount.getCombinationByClearingAccount() == null
						|| bankAccount.getCombinationByClearingAccount()
								.getCombinationId() == null) {
					Combination analysisClearingCombination = null;
					if ((byte) InstitutionType.get(bank.getInstitutionType())
							.getCode() != (byte) InstitutionType.Credit_Unions
							.getCode()) {

						// Clearing Account
						analysisClearingCombination = combinationBL
								.createAnalysisCombination(
										bank.getCombination(),
										bankAccount
												.getBranchName()
												.concat(" Clearing")
												.concat(" {")
												.concat(bankAccount
														.getAccountNumber())
												.concat("}"), true);
					} else {

						analysisClearingCombination = combinationBL
								.createAnalysisCombination(
										bank.getCombination(),
										BankCardType
												.get(bankAccount.getCardType())
												.name()
												.concat(" Clearing")
												.concat(" {")
												.concat(bankAccount
														.getAccountNumber())
												.concat("}"), true);
					}
					bankAccount
							.setCombinationByClearingAccount(analysisClearingCombination);
				}
			}
		}
		bankService.saveBank(bank, bankaccounts);

		// Workflow Call
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						Bank.class.getSimpleName(), bank.getBankId(), user,
						workflowDetailVo);
	
	}

	public void deleteBank(Bank bank) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						Bank.class.getSimpleName(), bank.getBankId(), user,
						workflowDetailVo);
		
	}

	public void doBankBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Bank bank = this.getBankService().getBankInfo(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			this.getBankService().deleteBank(bank,
					new ArrayList<BankAccount>(bank.getBankAccounts()));
		}
	}

	public BankAccountVO getBankAccountStatements(long bankAccountId,
			Date fromDate, Date toDate) throws Exception {
		BankAccount bankAccount = bankService.getBankAccountInfo(bankAccountId);
		BankAccountVO bankAccountVO = new BankAccountVO(bankAccount);

		bankAccountVO.setBankName(bankAccount.getBank().getBankName());
		bankAccountVO.setAccountTypeName(AccountType.get(
				bankAccount.getAccountType().intValue()).name());

		bankAccountVO.setAccountHolder(bankAccount.getBankAccountHolder());
		 
		Combination combination = combinationBL
				.getCombinationService()
				.getCombination(bankAccount.getCombination().getCombinationId());
		List<Transaction> transactions = transactionBL.getTransactionService()
				.getFilteredJournal(combination, fromDate, toDate);

		bankAccountVO.setTransactions(transactions);

		List<AccountsDetailVO> accountsDetailVOs = new ArrayList<AccountsDetailVO>();

		for (Transaction transaction : transactions) {
			for (TransactionDetail detail : transaction.getTransactionDetails()) {
				AccountsDetailVO accountsDetailVO = new AccountsDetailVO();
				accountsDetailVO.setReferenceNo("");
				accountsDetailVO.setDate(DateFormat
						.convertDateToString(transaction.getTransactionTime()
								.toString()));
				accountsDetailVO.setDescription(detail.getDescription());
				if (detail.getIsDebit()) {
					accountsDetailVO.setDebit(detail.getAmount());
				} else if (!detail.getIsDebit()) {
					accountsDetailVO.setCredit(detail.getAmount());
				}
				accountsDetailVOs.add(accountsDetailVO);
			}
		}
		bankAccountVO.setAccountsDetailVOs(accountsDetailVOs);
		return bankAccountVO;
	}

	public BankService getBankService() {
		return bankService;
	}

	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public AIOTechGenericDAO<Company> getCompanyDAO() {
		return companyDAO;
	}

	public void setCompanyDAO(AIOTechGenericDAO<Company> companyDAO) {
		this.companyDAO = companyDAO;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
