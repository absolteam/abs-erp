package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.VoidPayment;
import com.aiotech.aios.accounts.domain.entity.vo.VoidChequeVO;
import com.aiotech.aios.accounts.service.VoidPaymentService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class VoidPaymentBL {

	private VoidPaymentService voidPaymentService;
	private DirectPaymentBL directPaymentBL;
	private PaymentBL paymentBL;
	private Implementation implementation;
	private AlertBL alertBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// list all void payments
	public JSONObject getAllVoidPayment() throws Exception {
		List<VoidPayment> voidPaymentList = voidPaymentService
				.getAllVoidPayment(getImplementation());
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (VoidPayment list : voidPaymentList) {
			array = new JSONArray();
			array.add(list.getVoidPaymentId());
			array.add(list.getReferenceNumber());
			array.add(DateFormat.convertDateToString(list.getVoidDate()
					.toString()));
			if (null != list.getDirectPayment()) {
				array.add("Direct Payment");
				array.add(list.getDirectPayment().getPaymentNumber());
				array.add(list.getDirectPayment().getBankAccount().getBank()
						.getBankName());
				array.add(list.getDirectPayment().getBankAccount()
						.getAccountNumber());
				array.add(list.getDirectPayment().getChequeBook()
						.getChequeBookNo());
				array.add(list.getDirectPayment().getChequeNumber());
			}
			array.add(list.getPerson().getFirstName().concat(" ")
					.concat(list.getPerson().getLastName()));
			array.add(DateFormat.convertDateToString(list.getCreatedDate()
					.toString()));
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public VoidChequeVO convertVOObject(VoidPayment voidPayment) {
		VoidChequeVO voidChequeVO = new VoidChequeVO();
		voidChequeVO.setVoidPaymentId(voidPayment.getVoidPaymentId());
		voidChequeVO.setDescription(voidPayment.getDescription());
		voidChequeVO.setVoidDateStr(DateFormat.convertDateToString(voidPayment
				.getVoidDate().toString()));
		voidChequeVO.setReferenceNumber(voidPayment.getReferenceNumber());
		if (null != voidPayment.getDirectPayment()) {
			voidChequeVO.setDirectPaymentId(voidPayment.getDirectPayment()
					.getDirectPaymentId());
			voidChequeVO.setPaymentNumber(voidPayment.getDirectPayment()
					.getPaymentNumber());
			voidChequeVO.setChequeDate(DateFormat
					.convertDateToString(voidPayment.getDirectPayment()
							.getPaymentDate().toString()));
			voidChequeVO.setPaymentType(DirectPayment.class.getSimpleName());
			if (null != voidPayment.getDirectPayment().getSupplier()) {
				if (null != voidPayment.getDirectPayment().getSupplier()
						.getPerson())
					voidChequeVO.setPayeeName(voidPayment
							.getDirectPayment()
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(voidPayment.getDirectPayment()
									.getSupplier().getPerson().getLastName()));
				else if (null != voidPayment.getDirectPayment().getSupplier()
						.getCompany())
					voidChequeVO.setPayeeName(voidPayment.getDirectPayment()
							.getSupplier().getCompany().getCompanyName());
				else if (null != voidPayment.getDirectPayment().getSupplier()
						.getCmpDeptLocation())
					voidChequeVO.setPayeeName(voidPayment.getDirectPayment()
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName());
			} else if (null != voidPayment.getDirectPayment().getCustomer()) {
				if (null != voidPayment.getDirectPayment().getCustomer()
						.getPersonByPersonId())
					voidChequeVO.setPayeeName(voidPayment
							.getDirectPayment()
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat(" ")
							.concat(voidPayment.getDirectPayment()
									.getCustomer().getPersonByPersonId()
									.getLastName()));
				else if (null != voidPayment.getDirectPayment().getCustomer()
						.getCompany())
					voidChequeVO.setPayeeName(voidPayment.getDirectPayment()
							.getCustomer().getCompany().getCompanyName());
				else if (null != voidPayment.getDirectPayment().getCustomer()
						.getCmpDeptLocation())
					voidChequeVO.setPayeeName(voidPayment.getDirectPayment()
							.getCustomer().getCmpDeptLocation().getCompany()
							.getCompanyName());

			} else if (null != voidPayment.getDirectPayment().getCustomer())
				voidChequeVO.setPayeeName(voidPayment
						.getDirectPayment()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(voidPayment.getDirectPayment()
								.getPersonByPersonId().getLastName()));
			else
				voidChequeVO.setPayeeName(voidPayment.getDirectPayment()
						.getOthers());
			voidChequeVO.setChequeNo(voidPayment.getDirectPayment()
					.getChequeNumber());
			voidChequeVO.setChequeBookNo(voidPayment.getDirectPayment()
					.getChequeBook().getChequeBookNo());
			voidChequeVO.setBankAccount(voidPayment.getDirectPayment()
					.getBankAccount());
		}
		return voidChequeVO;
	}

	// show all cheque payments getChequePayments
	public List<Object> getChequePayments() throws Exception {
		List<DirectPayment> directPaymentList = directPaymentBL
				.getDirectPaymentService().getDirectChequePayments(
						this.getImplementation());
		List<Object> voidChequeVos = new ArrayList<Object>();
		VoidChequeVO voidChequeVO = null;
		for (DirectPayment direct : directPaymentList) {
			voidChequeVO = new VoidChequeVO();
			voidChequeVO.setDirectPaymentId(direct.getDirectPaymentId());
			voidChequeVO.setPaymentNumber(direct.getPaymentNumber());
			voidChequeVO.setChequeDate(DateFormat.convertDateToString(direct
					.getPaymentDate().toString()));
			voidChequeVO.setPaymentType(DirectPayment.class.getSimpleName());
			if (null != direct.getSupplier()) {
				if (null != direct.getSupplier().getPerson())
					voidChequeVO.setPayeeName(direct
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(direct.getSupplier().getPerson()
									.getLastName()));
				else if (null != direct.getSupplier().getCompany())
					voidChequeVO.setPayeeName(direct.getSupplier().getCompany()
							.getCompanyName());
				else if (null != direct.getSupplier().getCmpDeptLocation())
					voidChequeVO
							.setPayeeName(direct.getSupplier()
									.getCmpDeptLocation().getCompany()
									.getCompanyName());
			} else if (null != direct.getCustomer()) {
				if (null != direct.getCustomer().getPersonByPersonId())
					voidChequeVO.setPayeeName(direct
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat(" ")
							.concat(direct.getCustomer().getPersonByPersonId()
									.getLastName()));
				else if (null != direct.getCustomer().getCompany())
					voidChequeVO.setPayeeName(direct.getCustomer().getCompany()
							.getCompanyName());
				else if (null != direct.getCustomer().getCmpDeptLocation())
					voidChequeVO
							.setPayeeName(direct.getCustomer()
									.getCmpDeptLocation().getCompany()
									.getCompanyName());

			} else if (null != direct.getCustomer())
				voidChequeVO.setPayeeName(direct.getPersonByPersonId()
						.getFirstName().concat(" ")
						.concat(direct.getPersonByPersonId().getLastName()));
			else
				voidChequeVO.setPayeeName(direct.getOthers());

			// voidChequeVO.setBankAccount(direct.getBankAccount());
			voidChequeVO.setBankName(direct.getBankAccount().getBank()
					.getBankName());
			voidChequeVO.setAccountNumber(direct.getBankAccount()
					.getAccountNumber());
			voidChequeVO.setChequeNo(direct.getChequeNumber());
			voidChequeVO.setChequeBookNo(direct.getChequeBook()
					.getChequeBookNo());
			voidChequeVos.add(voidChequeVO);
		}
		return voidChequeVos;
	}

	// save void payment
	public void saveVoidPayment(VoidPayment voidPayment) throws Exception {
		voidPayment.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		boolean update = false;
		if (voidPayment != null && voidPayment.getVoidPaymentId() != null
				&& voidPayment.getVoidPaymentId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
			update = true;
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (!update)
			SystemBL.saveReferenceStamp(VoidPayment.class.getName(),
					getImplementation());
		voidPaymentService.saveVoidPayment(voidPayment);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				VoidPayment.class.getSimpleName(),
				voidPayment.getVoidPaymentId(), user, workflowDetailVo);
	}

	public void doVoidPaymentBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		VoidPayment voidPayment = voidPaymentService
				.getVoidPaymentById(recordId);
		directPaymentBL.getTransactionBL().deleteTransaction(
				voidPayment.getDirectPayment().getDirectPaymentId(),
				DirectPayment.class.getSimpleName());
		if (null != voidPayment.getDirectPayment()) {
			directPaymentBL.createJournalEntries(
					voidPayment.getDirectPayment(),
					new ArrayList<DirectPaymentDetail>(voidPayment
							.getDirectPayment().getDirectPaymentDetails()));
		}
		if (workflowDetailVO.isDeleteFlag()) {
			voidPayment.setImplementation(getImplementation());
			voidPaymentService.deleteVoidPayment(voidPayment);
		} else {
			if (null != voidPayment.getDirectPayment()) {
				directPaymentBL.createReverseJournalEntries(voidPayment
						.getDirectPayment(),
						new ArrayList<DirectPaymentDetail>(voidPayment
								.getDirectPayment().getDirectPaymentDetails()));
				Alert alert = alertBL.getAlertService().getAlertRecordInfo(
						voidPayment.getDirectPayment().getDirectPaymentId(),
						DirectPayment.class.getSimpleName());
				if (null != alert && !("").equals(alert)) {
					alert.setIsActive(false);
					alertBL.commonSaveAlert(alert);
				}
			}
		}
	}

	public void processAllVoidCheques() throws Exception {
		Implementation implementation = new Implementation();
		implementation.setImplementationId(6l);
		List<VoidPayment> voidPayments = voidPaymentService
				.getVoidPaymentsByDirectPayments(implementation);
		for (VoidPayment voidPayment : voidPayments) {
			doVoidChequeTransaction(voidPayment, implementation);
		}
	}

	private void doVoidChequeTransaction(VoidPayment voidPayment,
			Implementation implementation) throws Exception {
		directPaymentBL.getTransactionBL().deleteTransaction(
				voidPayment.getDirectPayment().getDirectPaymentId(),
				DirectPayment.class.getSimpleName());

		directPaymentBL.createJournalEntries(voidPayment.getDirectPayment(),
				new ArrayList<DirectPaymentDetail>(voidPayment
						.getDirectPayment().getDirectPaymentDetails()),
				implementation);

		directPaymentBL.createReverseJournalEntries(voidPayment
				.getDirectPayment(), new ArrayList<DirectPaymentDetail>(
				voidPayment.getDirectPayment().getDirectPaymentDetails()),
				implementation);
	}

	// delete void payment
	public void deleteVoidPayment(VoidPayment voidPayment) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				VoidPayment.class.getSimpleName(),
				voidPayment.getVoidPaymentId(), user, workflowDetailVo);

	}

	// session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public VoidPaymentService getVoidPaymentService() {
		return voidPaymentService;
	}

	public void setVoidPaymentService(VoidPaymentService voidPaymentService) {
		this.voidPaymentService = voidPaymentService;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public PaymentBL getPaymentBL() {
		return paymentBL;
	}

	public void setPaymentBL(PaymentBL paymentBL) {
		this.paymentBL = paymentBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
