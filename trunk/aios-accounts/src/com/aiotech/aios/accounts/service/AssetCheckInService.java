package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.AssetCheckIn;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetCheckInService {

	private AIOTechGenericDAO<AssetCheckIn> assetCheckInDAO;

	/**
	 * Get All Asset CheckIns 
	 * @param implementation
	 * @return
	 * @throws Exception
	 */
	public List<AssetCheckIn> getAllAssetCheckIns(Implementation implementation)
			throws Exception {
		return assetCheckInDAO.findByNamedQuery("getAllAssetCheckIns",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	/**
	 * Get Asset CheckIn Details 
	 * @param assetCheckInId
	 * @return
	 */
	public AssetCheckIn getAssetCheckInDetails(long assetCheckInId)
			throws Exception {
		return assetCheckInDAO.findByNamedQuery("getAssetCheckInDetails",
				assetCheckInId).get(0);
	}

	/**
	 * Save Asset Check In 
	 * @param assetCheckIn
	 */
	public void saveAssetCheckIn(AssetCheckIn assetCheckIn) throws Exception {
		assetCheckInDAO.saveOrUpdate(assetCheckIn);
	}
	
	/**
	 * Delete Asset Check In 
	 * @param assetCheckIn
	 */
	public void deleteAssetCheckIn(AssetCheckIn assetCheckIn) throws Exception {
		assetCheckInDAO.delete(assetCheckIn);
	}
	
	@SuppressWarnings("unchecked")
	public List<AssetCheckIn> getFilteredAssetCheckIns(
			Implementation implementation, Long assetCheckInId, Long assetCreationId, 
			Long checkInPersonId, Long checkOutPersonId, Date fromDate, Date toDate) throws Exception {
		Criteria criteria = assetCheckInDAO.createCriteria();
		
		criteria.createAlias("assetCheckOut", "co", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("co.assetCreation", "ac", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("co.personByCheckOutTo", "to", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "by", CriteriaSpecification.LEFT_JOIN);
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("ac.implementation", implementation));
		}
		
		if (assetCheckInId != null && assetCheckInId > 0) {
			criteria.add(Restrictions.eq("assetCheckInId", assetCheckInId));
		}
		
		if (assetCreationId != null && assetCreationId > 0) {
			criteria.add(Restrictions.eq("ac.assetCreationId", assetCreationId));
		}
		
		if (checkInPersonId != null && checkInPersonId > 0) {
			criteria.add(Restrictions.eq("by.personId", checkInPersonId));
		}
		
		if (checkOutPersonId != null && checkOutPersonId > 0) {
			criteria.add(Restrictions.eq("to.personId", checkOutPersonId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("checkInDate",fromDate, toDate));
		}
		return criteria.list();
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetCheckIn> getAssetCheckInDAO() {
		return assetCheckInDAO;
	}

	public void setAssetCheckInDAO(
			AIOTechGenericDAO<AssetCheckIn> assetCheckInDAO) {
		this.assetCheckInDAO = assetCheckInDAO;
	}

}
