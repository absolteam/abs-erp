package com.aiotech.aios.accounts.service.bl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.service.POSUserTillService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class POSUserTillBL {
	private POSUserTillService pOSUserTillService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void savePOSUserTill(POSUserTill posUserTill) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		posUserTill.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (posUserTill != null && posUserTill.getPosUserTillId() != null
				&& posUserTill.getPosUserTillId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		pOSUserTillService.savePOSUserTill(posUserTill);
		
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				POSUserTill.class.getSimpleName(), posUserTill.getPosUserTillId(), user,
				workflowDetailVo);
	}
	
	public void doPOSUserTillBusinessUpdate(Long posUserTillId,
			WorkflowDetailVO workflowDetailVO) {
		try { 
			if (workflowDetailVO.isDeleteFlag()) {
				POSUserTill posUserTill = pOSUserTillService
				.getPosUserTillInformation(posUserTillId);
				pOSUserTillService.deletePOSUserTill(posUserTill);
			}  
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deletePOSUserTill(POSUserTill posUserTill) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						POSUserTill.class.getSimpleName(), posUserTill.getPosUserTillId(),
						user, workflowDetailVo); 
	}

	public POSUserTillService getpOSUserTillService() {
		return pOSUserTillService;
	}

	public void setpOSUserTillService(POSUserTillService pOSUserTillService) {
		this.pOSUserTillService = pOSUserTillService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
