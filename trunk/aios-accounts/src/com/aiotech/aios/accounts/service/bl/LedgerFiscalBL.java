package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.LedgerFiscalVO;
import com.aiotech.aios.accounts.service.CalendarService;
import com.aiotech.aios.accounts.service.LedgerFiscalService;
import com.aiotech.aios.accounts.service.TransactionService;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.util.AIOSCommons;

public class LedgerFiscalBL {

	private LedgerFiscalService ledgerFiscalService;
	private TransactionService transactionService;
	private CalendarService calendarService;

	// Process Period Ledger Fiscal
	public void processPeriodLedgerFiscal(List<Period> periods, Long calendarId)
			throws Exception {
		List<Transaction> transactions = null;
		if (null != periods && periods.size() > 0) {
			transactions = new ArrayList<Transaction>();
			for (Period period : periods)
				transactions.addAll(transactionService
						.getTransactionsWithTransactionDetailsByPeriod(period
								.getPeriodId()));
		}
		if (null != transactions && transactions.size() > 0) {
			Map<Long, Period> periodMap = new HashMap<Long, Period>();
			for (Transaction transaction : transactions)
				periodMap.put(transaction.getPeriod().getPeriodId(),
						transaction.getPeriod());
			addLedgerFiscal(transactions,
					new ArrayList<Period>(periodMap.values()), calendarId);
		}
	}

	// Update Ledger Balance
	public void addLedgerFiscal(List<Transaction> transactions,
			List<Period> periods, Long calendarId) throws Exception {
		deleteLedgerFiscal(periods);
		LedgerFiscal ledgerFiscal = null;
		if (null != transactions && transactions.size() > 0) {
			for (Transaction transaction : transactions) {
				for (TransactionDetail transactionDetail : transaction
						.getTransactionDetails()) {
					ledgerFiscal = ledgerFiscalService.getLedgerFiscals(
							transactionDetail.getCombination()
									.getCombinationId(), transaction
									.getPeriod().getPeriodId());
					if (null != ledgerFiscal && !("").equals(ledgerFiscal)) {
						if (null == ledgerFiscal.getBalance()
								|| ("").equals(ledgerFiscal.getBalance()))
							ledgerFiscal.setBalance(new Double(0));
					} else {
						ledgerFiscal = new LedgerFiscal();
						ledgerFiscal.setCombination(transactionDetail
								.getCombination());
						ledgerFiscal.setSide(true);
						ledgerFiscal.setBalance(new Double(0));
						ledgerFiscal.setPeriod(transaction.getPeriod());
					}
					ledgerFiscalService
							.updateLedgerFiscalBalance(updateLedgerFiscal(
									ledgerFiscal, transactionDetail));
				}
			}
		}
	}

	// Business logic for update ledger balance
	private LedgerFiscal updateLedgerFiscal(LedgerFiscal ledgerFiscal,
			TransactionDetail transactionDetail) throws Exception {
		double ledgerBalance = ledgerFiscal.getSide() ? (-1 * ledgerFiscal
				.getBalance()) : ledgerFiscal.getBalance();
		double transactionAmount = transactionDetail.getIsDebit() ? (-1 * transactionDetail
				.getAmount()) : transactionDetail.getAmount();
		double calculationAmount = 0.0;
		if ((int) ledgerFiscal.getCombination().getAccountByNaturalAccountId()
				.getAccountType() == (int) AccountType.Assets.getCode()
				|| (int) ledgerFiscal.getCombination()
						.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
						.getCode()) {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				}
			}
		} else {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				}
			}
		}
		return ledgerFiscal;
	}

	private void deleteLedgerFiscal(List<Period> periods) throws Exception {
		List<LedgerFiscal> ledgerFiscals = new ArrayList<LedgerFiscal>();
		for (Period period : periods) {
			List<LedgerFiscal> ledgerFiscal = ledgerFiscalService
					.getLedgerFiscals(period.getPeriodId());
			if (null != ledgerFiscal)
				ledgerFiscals.addAll(ledgerFiscal);
		}
		if (null != ledgerFiscals && ledgerFiscals.size() > 0)
			ledgerFiscalService.deleteLedgerFiscal(ledgerFiscals);
	}

	public List<LedgerFiscalVO> getPeriodLedgerFiscalView(
			List<LedgerFiscal> ledgerFiscals) throws Exception {
		List<LedgerFiscalVO> ledgerFiscalVOs = null;
		if (null != ledgerFiscals && ledgerFiscals.size() > 0) {
			ledgerFiscalVOs = new ArrayList<LedgerFiscalVO>();
			for (LedgerFiscal ledgerFiscal : ledgerFiscals)
				ledgerFiscalVOs.add(processLedgerFiscalView(ledgerFiscal));
		}
		return ledgerFiscalVOs;
	}

	private LedgerFiscalVO processLedgerFiscalView(LedgerFiscal ledgerFiscal)
			throws Exception {
		LedgerFiscalVO ledgerFiscalVO = new LedgerFiscalVO();
		BeanUtils.copyProperties(ledgerFiscalVO, ledgerFiscal);
		ledgerFiscalVO.setPeriodName(ledgerFiscal.getPeriod().getName());
		StringBuffer accountName = new StringBuffer();
		accountName.append(ledgerFiscal
				.getCombination()
				.getAccountByCompanyAccountId()
				.getAccount()
				.concat("--")
				.concat(ledgerFiscal.getCombination()
						.getAccountByCostcenterAccountId().getAccount())
				.concat("--")
				.concat(ledgerFiscal.getCombination()
						.getAccountByNaturalAccountId().getAccount()));
		if (null != ledgerFiscal.getCombination()
				.getAccountByAnalysisAccountId()) {
			accountName.append("--"
					+ ledgerFiscal.getCombination()
							.getAccountByAnalysisAccountId().getAccount());
			if (null != ledgerFiscal.getCombination()
					.getAccountByBuffer1AccountId()) {
				accountName.append("--"
						+ ledgerFiscal.getCombination()
								.getAccountByBuffer1AccountId().getAccount());
				if (null != ledgerFiscal.getCombination()
						.getAccountByBuffer2AccountId()) {
					accountName.append("--"
							+ ledgerFiscal.getCombination()
									.getAccountByBuffer2AccountId()
									.getAccount());
				}
			}
		}
		ledgerFiscalVO.setAccountName(accountName.toString());
		ledgerFiscalVO.setAccountType(AccountType.get(
				ledgerFiscal.getCombination().getAccountByNaturalAccountId()
						.getAccountType()).name());
		if ((boolean) ledgerFiscal.getSide())
			ledgerFiscalVO.setDebitBalance(AIOSCommons
					.formatAmount(ledgerFiscal.getBalance()));
		else
			ledgerFiscalVO.setCreditBalance(AIOSCommons
					.formatAmount(ledgerFiscal.getBalance()));
		return ledgerFiscalVO;
	}

	// Getters & Setters
	public LedgerFiscalService getLedgerFiscalService() {
		return ledgerFiscalService;
	}

	public void setLedgerFiscalService(LedgerFiscalService ledgerFiscalService) {
		this.ledgerFiscalService = ledgerFiscalService;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public CalendarService getCalendarService() {
		return calendarService;
	}

	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
}
