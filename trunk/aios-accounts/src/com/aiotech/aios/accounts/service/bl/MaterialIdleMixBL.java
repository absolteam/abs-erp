package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialIdleMixVO;
import com.aiotech.aios.accounts.service.MaterialIdleMixService;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class MaterialIdleMixBL {

	private MaterialIdleMixService materialIdleMixService; 
	private StockBL stockBL;
	private PackageConversionBL packageConversionBL; 
	
	public List<Object> getAllMaterialIdleMix() throws Exception {
		List<MaterialIdleMix> materialIdleMixes = materialIdleMixService
				.getAllMaterialIdleMix(getImplementation());
		List<Object> materialIdleMixVO = new ArrayList<Object>();
		if (null != materialIdleMixes && materialIdleMixes.size() > 0) {
			for (MaterialIdleMix materialIdleMix : materialIdleMixes) {
				materialIdleMixVO.add(addMaterialIdleMixVO(materialIdleMix));
			}
		}
		return materialIdleMixVO;
	}

	public void saveMaterialIdleMix(MaterialIdleMix materialIdleMix,
			List<MaterialIdleMixDetail> materialIdleMixDetails,
			List<MaterialIdleMixDetail> deletedMaterialMixDetails)
			throws Exception {
		materialIdleMix.setImplementation(getImplementation());
		if (null == materialIdleMix.getMaterialIdleMixId())
			SystemBL.saveReferenceStamp(MaterialIdleMix.class.getName(),
					getImplementation());
		if (null != deletedMaterialMixDetails
				&& deletedMaterialMixDetails.size() > 0)
			materialIdleMixService
					.deleteMaterailIdleMixDetails(deletedMaterialMixDetails);
		materialIdleMixService.saveMaterialIdleMix(materialIdleMix,
				materialIdleMixDetails);
	}

	public void deleteMaterialIdleMix(MaterialIdleMix materialIdleMix,
			ArrayList<MaterialIdleMixDetail> materialIdleMixDetails)
			throws Exception {
		materialIdleMixService.deleteMaterailIdleMix(materialIdleMix,
				materialIdleMixDetails);
	}

	private MaterialIdleMixVO addMaterialIdleMixVO(
			MaterialIdleMix materialIdleMix) {
		MaterialIdleMixVO materialIdleMixVO = new MaterialIdleMixVO();
		materialIdleMixVO.setMaterialIdleMixId(materialIdleMix
				.getMaterialIdleMixId());
		materialIdleMixVO.setReferenceNumber(materialIdleMix
				.getReferenceNumber());
		materialIdleMixVO.setPersonName(materialIdleMix.getPerson()
				.getFirstName().concat(" ")
				.concat(materialIdleMix.getPerson().getLastName()));
		materialIdleMixVO
				.setProductCode(materialIdleMix.getProduct().getCode());
		materialIdleMixVO.setProductName(materialIdleMix.getProduct()
				.getProductName());
		return materialIdleMixVO;
	}
	
	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public MaterialIdleMixService getMaterialIdleMixService() {
		return materialIdleMixService;
	}

	public void setMaterialIdleMixService(
			MaterialIdleMixService materialIdleMixService) {
		this.materialIdleMixService = materialIdleMixService;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public PackageConversionBL getPackageConversionBL() {
		return packageConversionBL;
	}

	public void setPackageConversionBL(PackageConversionBL packageConversionBL) {
		this.packageConversionBL = packageConversionBL;
	} 
}
