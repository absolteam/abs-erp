package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MerchandiseExchangeService {

	private AIOTechGenericDAO<MerchandiseExchange> merchandiseExchangeDAO;
	private AIOTechGenericDAO<MerchandiseExchangeDetail> merchandiseExchangeDetailDAO;

	public List<MerchandiseExchange> getAllMerchandiseExchange(
			Implementation implementation) throws Exception {
		return merchandiseExchangeDAO.findByNamedQuery(
				"getAllMerchandiseExchange", implementation);
	}

	// Get Merchandise Exchange By Reference
	public MerchandiseExchange getMerchandiseExchangeByReference(
			String exchangeReference, Implementation implementation)
			throws Exception {
		List<MerchandiseExchange> merchandiseExchanges = merchandiseExchangeDAO
				.findByNamedQuery("getMerchandiseExchangeByReference",
						implementation, exchangeReference);
		return null != merchandiseExchanges && merchandiseExchanges.size() > 0 ? merchandiseExchanges
				.get(0) : null;
	}

	// Get Merchandise + POS
	public List<MerchandiseExchangeDetail> getExchangeDetailByExchangeId(
			Long exchangeId) throws Exception {
		return merchandiseExchangeDetailDAO.findByNamedQuery(
				"getExchangeDetailByExchangeId", exchangeId);
	}

	// Get Merchandise Detail
	public MerchandiseExchangeDetail getExchangeDetailByExchangeDetailId(
			long exchangeDetailId) throws Exception {
		return merchandiseExchangeDetailDAO.findById(exchangeDetailId);
	}

	// Get Merchandise Detail By POS Detail
	public MerchandiseExchangeDetail getExchangeDetailByPOSDetailId(
			long pointOfSaleDetailId) throws Exception {
		return merchandiseExchangeDetailDAO.findByNamedQuery(
				"getExchangeDetailByPOSDetailId", pointOfSaleDetailId).get(0);
	}

	// Save Merchandise Exchange
	public void saveMerchandiseExchange(
			MerchandiseExchange merchandiseExchange,
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails)
			throws Exception {
		merchandiseExchangeDAO.saveOrUpdate(merchandiseExchange);
		for (MerchandiseExchangeDetail merchandiseExchangeDetail : merchandiseExchangeDetails) {
			merchandiseExchangeDetail
					.setMerchandiseExchange(merchandiseExchange);
		}
		merchandiseExchangeDetailDAO
				.saveOrUpdateAll(merchandiseExchangeDetails);
	}

	public void deleteMerchandiseExchanges(
			List<MerchandiseExchange> merchandiseExchange,
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails)
			throws Exception {
		merchandiseExchangeDetailDAO.deleteAll(merchandiseExchangeDetails);
		merchandiseExchangeDAO.deleteAll(merchandiseExchange);
	}

	// Getters & Setters
	public AIOTechGenericDAO<MerchandiseExchange> getMerchandiseExchangeDAO() {
		return merchandiseExchangeDAO;
	}

	public void setMerchandiseExchangeDAO(
			AIOTechGenericDAO<MerchandiseExchange> merchandiseExchangeDAO) {
		this.merchandiseExchangeDAO = merchandiseExchangeDAO;
	}

	public AIOTechGenericDAO<MerchandiseExchangeDetail> getMerchandiseExchangeDetailDAO() {
		return merchandiseExchangeDetailDAO;
	}

	public void setMerchandiseExchangeDetailDAO(
			AIOTechGenericDAO<MerchandiseExchangeDetail> merchandiseExchangeDetailDAO) {
		this.merchandiseExchangeDetailDAO = merchandiseExchangeDetailDAO;
	}
}
