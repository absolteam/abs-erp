package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetInsurance;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetInsuranceService {

	private AIOTechGenericDAO<AssetInsurance> assetInsuranceDAO;

	// Get all Asset Insurance
	public List<AssetInsurance> getAllAssetInsurance(
			Implementation implementation) throws Exception {
		return assetInsuranceDAO.findByNamedQuery("getAllAssetInsurance",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Asset insurance by Insurance Id
	public AssetInsurance getAssetInsuranceByInsuranceId(long assetInsuranceId)
			throws Exception {
		return assetInsuranceDAO.findByNamedQuery(
				"getAssetInsuranceByInsuranceId", assetInsuranceId).get(0);
	}

	// Get Simple Asset insurance by Insurance Id
	public AssetInsurance getSimpleAssetInsuranceByInsuranceId(
			long assetInsuranceId) throws Exception {
		return assetInsuranceDAO.findById(assetInsuranceId);
	}

	// Save Asset Insurance
	public void saveAssetInsurance(AssetInsurance assetInsurance)
			throws Exception {
		assetInsuranceDAO.saveOrUpdate(assetInsurance);
	}

	// Delete Asset Insurance
	public void deleteAssetInsurance(AssetInsurance assetInsurance)
			throws Exception {
		assetInsuranceDAO.delete(assetInsurance);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetInsurance> getAssetInsuranceDAO() {
		return assetInsuranceDAO;
	}

	public void setAssetInsuranceDAO(
			AIOTechGenericDAO<AssetInsurance> assetInsuranceDAO) {
		this.assetInsuranceDAO = assetInsuranceDAO;
	}
}
