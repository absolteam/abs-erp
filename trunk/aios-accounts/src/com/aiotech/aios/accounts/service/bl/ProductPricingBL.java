/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingCalcVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingVO;
import com.aiotech.aios.accounts.service.BankService;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.ProductPricingService;
import com.aiotech.aios.accounts.service.SupplierService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * @author Saleem
 */
public class ProductPricingBL {

	// Dependencies
	private ProductBL productBL;
	private CustomerBL customerBL;
	private SupplierService supplierService;
	private BankService bankService;
	private CurrencyService currencyService;
	private ProductPricingService productPricingService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get all product pricing
	public List<Object> getAllProductPricing() throws Exception {
		List<Object> productPricingObject = new ArrayList<Object>();
		List<ProductPricing> productPricings = productPricingService
				.getAllProductPricing(getImplementation());
		ProductPricingVO productPricingVO = null;
		if (null != productPricings && productPricings.size() > 0) {
			for (ProductPricing productPricing : productPricings) {
				productPricingVO = new ProductPricingVO();
				productPricingVO.setPricingTitle(productPricing
						.getPricingTitle());
				productPricingVO.setCurrencyCode(productPricing.getCurrency()
						.getCurrencyPool().getCode());
				productPricingVO.setEffictiveStartDate(DateFormat
						.convertDateToString(productPricing.getStartDate()
								.toString()));
				productPricingVO.setEffictiveEndDate(null != productPricing
						.getEndDate() ? DateFormat
						.convertDateToString(productPricing.getEndDate()
								.toString()) : "");
				productPricingVO
						.setDescription(productPricing.getDescription());
				productPricingVO.setProductPricingId(productPricing
						.getProductPricingId());
				productPricingObject.add(productPricingVO);
			}
		}
		return productPricingObject;
	}

	// Get all product pricing
	public List<Object> getProductPricingDetail() throws Exception {
		List<Object> productPricingObject = new ArrayList<Object>();
		List<ProductPricingDetail> productPricingDetails = productPricingService
				.getAllProductPricingDetail(getImplementation());

		ProductPricingDetailVO productPricingDetailVO = null;
		if (null != productPricingDetails && productPricingDetails.size() > 0) {
			for (ProductPricingDetail productPricingDetail : productPricingDetails) {
				productPricingDetailVO = new ProductPricingDetailVO();
				productPricingDetailVO.setPricingTitle(productPricingDetail
						.getProductPricing().getPricingTitle());
				productPricingDetailVO.setCurrencyCode(productPricingDetail
						.getProductPricing().getCurrency().getCurrencyPool()
						.getCode());
				productPricingDetailVO.setProductPricingId(productPricingDetail
						.getProductPricing().getProductPricingId());
				productPricingDetailVO.setProductName(productPricingDetail
						.getProduct().getProductName());
				productPricingDetailVO.setProductCode(productPricingDetail
						.getProduct().getCode());
				productPricingDetailVO
						.setStoreName(null != productPricingDetail.getStore() ? productPricingDetail
								.getStore().getStoreName() : "");
				productPricingDetailVO
						.setCostPrice(AIOSCommons
								.formatAmount(productPricingDetail
										.getBasicCostPrice()));
				productPricingDetailVO
						.setProductPricingDetailId(productPricingDetail
								.getProductPricingDetailId());
				productPricingDetailVO.setProductId(productPricingDetail
						.getProduct().getProductId());
				if (null != productPricingDetail.getSellingPrice()
						&& (double) productPricingDetail.getSellingPrice() != 0) {
					productPricingDetailVO.setSalePrice(AIOSCommons
							.formatAmount(productPricingDetail
									.getSellingPrice()));
				} else
					productPricingDetailVO.setSalePrice("0.00");
				productPricingObject.add(productPricingDetailVO);
			}
		}
		return productPricingObject;
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingDetail getNonUnfiedProductPricingDetail(
			long productId, long storeId) throws Exception {
		List<ProductPricing> productPricings = productPricingService
				.getPricingDetailWithProductStore(productId, storeId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		ProductPricingDetail pricingDetail = null;
		if (null != productPricings && productPricings.size() > 0) {
			for (ProductPricingDetail pricingDetailtemp : productPricings
					.get(0).getProductPricingDetails()) {
				if (pricingDetailtemp.getStatus()) {
					pricingDetail = new ProductPricingDetail();
					BeanUtils.copyProperties(pricingDetail, pricingDetailtemp);
					break;
				}
			}
			return pricingDetail;
		}
		return null;
	}

	public ProductPricingDetail getDefalutProductPricingDetail(long productId)
			throws Exception {
		return productPricingService.getPricingDetailByProductId(productId);
	}

	public ProductPricingDetail getNonunfiedProductPricingDetail(
			long productId, long storeId) throws Exception {
		return productPricingService.getNonunfiedProductPricingDetail(
				productId, storeId);
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingDetail getPricingDetail(long productId, long storeId)
			throws Exception {
		List<ProductPricing> productPricings = productPricingService
				.getProductPricingWithoutCustomer(productId, storeId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		if (null == productPricings || productPricings.size() == 0) {
			productPricings = productPricingService
					.getProductPricingWithoutCustomerAndStore(productId,
							Calendar.getInstance().getTime(), Calendar
									.getInstance().getTime());
		}
		if (null != productPricings && productPricings.size() > 0) {
			List<ProductPricingDetail> pricingDetails = new ArrayList<ProductPricingDetail>(
					productPricings.get(0).getProductPricingDetails());
			ProductPricingDetail pricingDetail = pricingDetails.get(0);
			return pricingDetail;
		}
		return null;
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingDetail getPricingDetail(long productId)
			throws Exception {
		List<ProductPricing> productPricings = productPricingService
				.getProductPricingDetailByProduct(productId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		ProductPricingDetail pricingDetail = null;
		if (null != productPricings && productPricings.size() > 0) {
			List<ProductPricingDetail> pricingDetails = new ArrayList<ProductPricingDetail>(
					productPricings.get(0).getProductPricingDetails());
			for (ProductPricingDetail productPricingDetail : pricingDetails) {
				if (null != productPricingDetail.getStatus()
						&& (boolean) productPricingDetail.getStatus()) {
					pricingDetail = new ProductPricingDetail();
					BeanUtils.copyProperties(pricingDetail,
							productPricingDetail);
				}
			}
		}
		return pricingDetail;
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingDetail getPricingDetailWithProductStore(
			long productId, long storeId) throws Exception {
		List<ProductPricing> productPricings = productPricingService
				.getPricingDetailWithProductStore(productId, storeId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		ProductPricingDetail pricingDetail = null;
		if (null != productPricings && productPricings.size() > 0) {
			List<ProductPricingDetail> pricingDetails = new ArrayList<ProductPricingDetail>(
					productPricings.get(0).getProductPricingDetails());
			for (ProductPricingDetail productPricingDetail : pricingDetails) {
				if (null != productPricingDetail.getStatus()
						&& (boolean) productPricingDetail.getStatus()) {
					pricingDetail = new ProductPricingDetail();
					BeanUtils.copyProperties(pricingDetail,
							productPricingDetail);
				}
			}
		}
		return pricingDetail;
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingCalc getPricingDetails(long productId, long storeId)
			throws Exception {
		List<ProductPricing> productPricings = productPricingService
				.getProductPricingWithoutCustomer(productId, storeId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		if (null == productPricings || productPricings.size() == 0) {
			productPricings = productPricingService
					.getProductPricingWithoutCustomerAndStore(productId,
							Calendar.getInstance().getTime(), Calendar
									.getInstance().getTime());

		}
		if (null != productPricings && productPricings.size() > 0)
			return getProductPricingCalc(productPricings);
		return null;
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingCalc getComboPricingDetails(long productId)
			throws Exception {
		List<ProductPricing> productPricings = productPricingService
				.getComboProductPricing(productId, Calendar.getInstance()
						.getTime(), Calendar.getInstance().getTime());
		if (null != productPricings && productPricings.size() > 0)
			return getComboProductPricingCalc(productPricings);
		return null;
	}

	private ProductPricingCalc getProductPricingCalc(
			List<ProductPricing> productPricings) throws Exception {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
				productPricings.get(0).getProductPricingDetails());
		ProductPricingDetail productPricingDetail = productPricingDetails
				.get(0);
		if (null != productPricingDetail.getProductPricingCalcs()
				&& productPricingDetail.getProductPricingCalcs().size() > 0) {
			for (ProductPricingCalc pricingCalc : productPricingDetail
					.getProductPricingCalcs()) {
				if (null != pricingCalc.getIsDefault()
						&& pricingCalc.getIsDefault()) {
					return pricingCalc;
				}
			}
		}
		return null;
	}

	private ProductPricingCalc getComboProductPricingCalc(
			List<ProductPricing> productPricings) throws Exception {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
				productPricings.get(0).getProductPricingDetails());
		ProductPricingDetail productPricingDetail = productPricingDetails
				.get(0);
		if (null != productPricingDetail.getProductPricingCalcs()
				&& productPricingDetail.getProductPricingCalcs().size() > 0) {
			for (ProductPricingCalc pricingCalc : productPricingDetail
					.getProductPricingCalcs()) {
				if (null != pricingCalc.getComboType()
						&& pricingCalc.getComboType()) {
					return pricingCalc;
				}
			}
		}
		return null;
	}

	// Get Base Product Pricing Detail
	public ProductPricingDetail getProductPricingDetail(long productId)
			throws Exception {
		ProductPricingDetail productPricingDetail = productPricingService
				.getProductPricingDetailWithOutStore(productId, DateFormat
						.getFromDate(Calendar.getInstance().getTime()),
						DateFormat.getToDate(Calendar.getInstance().getTime()),
						getImplementation());
		return productPricingDetail;
	}

	// Get Base Product Pricing Detail
	public ProductPricingDetail getProductPricingDetail(long productId,
			long storeId) throws Exception {
		ProductPricingDetail productPricingDetail = productPricingService
				.getProductPricingDetail(productId, storeId, DateFormat
						.getFromDate(Calendar.getInstance().getTime()),
						DateFormat.getToDate(Calendar.getInstance().getTime()),
						getImplementation());
		if (null == productPricingDetail || productPricingDetail.equals("")) {
			productPricingDetail = productPricingService
					.getProductPricingDetailWithOutStore(productId, DateFormat
							.getFromDate(Calendar.getInstance().getTime()),
							DateFormat.getToDate(Calendar.getInstance()
									.getTime()), getImplementation());
		}
		return productPricingDetail;
	}

	// Get Base Product Pricing Detail
	public ProductPricingDetail getProductPricingStockDetail(long productId,
			long storeId) throws Exception {
		ProductPricingDetail productPricingDetail = productPricingService
				.getProductPricingDetail(productId, storeId, DateFormat
						.getFromDate(Calendar.getInstance().getTime()),
						DateFormat.getToDate(Calendar.getInstance().getTime()),
						getImplementation());
		return productPricingDetail;
	}

	// Get Base Product Pricing Detail
	public ProductPricingDetail getProductPricingDetail(long productId,
			long storeId, Session session) throws Exception {
		ProductPricingDetail productPricingDetail = productPricingService
				.getProductPricingDetail(productId, storeId, DateFormat
						.getFromDate(Calendar.getInstance().getTime()),
						DateFormat.getToDate(Calendar.getInstance().getTime()),
						getImplementation(), session);
		if (null == productPricingDetail || productPricingDetail.equals("")) {
			productPricingDetail = productPricingService
					.getProductPricingDetailWithOutStore(productId, DateFormat
							.getFromDate(Calendar.getInstance().getTime()),
							DateFormat.getToDate(Calendar.getInstance()
									.getTime()), getImplementation(), session);
		}
		return productPricingDetail;
	}

	// Get Product Pricing Details Without Customer Details
	public ProductPricingCalc getProductPricingCalc(long productId,
			long storeId, long pricingType) throws Exception {
		ProductPricingCalc productPricingCal = productPricingService
				.getProductPricingCalcWithoutCustomer(productId, storeId,
						Calendar.getInstance().getTime(), Calendar
								.getInstance().getTime(), pricingType);
		if (null == productPricingCal || productPricingCal.equals("")) {
			productPricingCal = productPricingService
					.getProductPricingCalcWithoutCustomerAndStore(productId,
							Calendar.getInstance().getTime(), Calendar
									.getInstance().getTime(), pricingType);
		}

		return productPricingCal;
	}

	// Get Product Pricing Details With Customer Details
	public ProductPricingCalc getProductPricingCalcWithCustomer(long productId,
			long customerId, long storeId, long pricingType) throws Exception {
		ProductPricingCalc productPricingCal = productPricingService
				.getProductPricingCalcWithCustomer(productId, customerId,
						storeId, Calendar.getInstance().getTime(), Calendar
								.getInstance().getTime(), pricingType);
		if (null == productPricingCal || productPricingCal.equals("")) {
			productPricingCal = productPricingService
					.getProductPricingCalcWithCustomerWithOutStore(productId,
							customerId, Calendar.getInstance().getTime(),
							Calendar.getInstance().getTime(), pricingType);
		}

		return productPricingCal;
	}

	// Get Product Pricing Details With Customer Details
	public ProductPricingCalc getCustomerSalesPrice(long productId,
			long customerId) throws Exception {
		ProductPricingCalc productPricingCal = productPricingService
				.getCustomerSalesPrice(productId, customerId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		return productPricingCal;
	}

	// Get Product Pricing Details With Customer Details
	public ProductPricingCalc getCustomerStoreSalesPrice(long productId,
			long customerId, long storeId) throws Exception {
		ProductPricingCalc productPricingCal = productPricingService
				.getCustomerSalesPrice(productId, customerId, Calendar
						.getInstance().getTime(), Calendar.getInstance()
						.getTime());
		if (null == productPricingCal && storeId > 0) {
			productPricingCal = productPricingService.getStoreSalesPrice(
					productId, storeId, Calendar.getInstance().getTime(),
					Calendar.getInstance().getTime());
		}
		return productPricingCal;
	}

	// Manipulate Product Pricing
	public List<?> manipulateProductPricing(long productId, long storeId,
			long customerId) throws Exception {
		List<ProductPricingVO> productPricingVOs = null;
		List<ProductPricing> productPricings = null;
		List<ProductPricingCalcVO> productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
		if (customerId > 0) {
			if (storeId > 0)
				productPricings = productPricingService
						.getProductPricingWithCustomer(productId, storeId,
								customerId, Calendar.getInstance().getTime(),
								Calendar.getInstance().getTime());
			else
				productPricings = productPricingService
						.getProductPricingCustomerWithoutStore(productId,
								customerId, Calendar.getInstance().getTime(),
								Calendar.getInstance().getTime());
			if (null == productPricings || productPricings.size() == 0) {
				if (storeId > 0) {
					productPricings = getProductPricingWithoutCustomer(
							productId, storeId,
							DateFormat.convertSystemDateToString(Calendar
									.getInstance().getTime()),
							DateFormat.convertSystemDateToString(Calendar
									.getInstance().getTime()));
					if (null == productPricings || productPricings.size() == 0)
						productPricings = productPricingService
								.getProductPricingWithoutStoreAndCustomer(
										productId, Calendar.getInstance()
												.getTime(), Calendar
												.getInstance().getTime());
				} else
					productPricings = productPricingService
							.getProductPricingWithoutStoreAndCustomer(
									productId,
									Calendar.getInstance().getTime(), Calendar
											.getInstance().getTime());
			}
		} else {
			if (storeId > 0) {
				productPricings = getProductPricingWithoutCustomer(productId,
						storeId, DateFormat.convertSystemDateToString(Calendar
								.getInstance().getTime()),
						DateFormat.convertSystemDateToString(Calendar
								.getInstance().getTime()));
				if (null == productPricings || productPricings.size() == 0)
					productPricings = productPricingService
							.getProductPricingWithoutStoreAndCustomer(
									productId,
									Calendar.getInstance().getTime(), Calendar
											.getInstance().getTime());
			} else
				productPricings = productPricingService
						.getProductPricingWithoutStoreAndCustomer(productId,
								Calendar.getInstance().getTime(), Calendar
										.getInstance().getTime());
		}
		if (null != productPricings && productPricings.size() > 0) {
			productPricingVOs = convertEntityVOs(productPricings);
			ProductPricingCalcVO productPricingCalcVO = null;
			for (ProductPricingVO productPricingVO : productPricingVOs) {
				for (ProductPricingDetailVO productPricingDetailVO : productPricingVO
						.getProductPricingDetailVOs()) {
					for (ProductPricingCalcVO pricingCalcVO : productPricingDetailVO
							.getProductPricingCalcVOs()) {
						productPricingCalcVO = new ProductPricingCalcVO();
						BeanUtils.copyProperties(productPricingCalcVO,
								pricingCalcVO);
						productPricingCalcVOs.add(productPricingCalcVO);
					}
				}
			}
		}
		return productPricingCalcVOs;
	}

	// Convert Entity into VOs
	private List<ProductPricingVO> convertEntityVOs(
			List<ProductPricing> productPricings) {
		List<ProductPricingVO> productPricingVOs = new ArrayList<ProductPricingVO>();
		List<ProductPricingDetailVO> productPricingDetailVOs = null;
		List<ProductPricingCalcVO> productPricingCalcVOs = null;
		ProductPricingVO productPricingVO = null;
		ProductPricingDetailVO productPricingDetailVO = null;
		ProductPricingCalcVO productPricingCalcVO = null;
		for (ProductPricing productPricing : productPricings) {
			productPricingVO = new ProductPricingVO();
			productPricingDetailVOs = new ArrayList<ProductPricingDetailVO>();
			productPricingVO.setProductPricingId(productPricing
					.getProductPricingId());
			for (ProductPricingDetail productPricingDetail : productPricing
					.getProductPricingDetails()) {
				productPricingDetailVO = new ProductPricingDetailVO();
				productPricingDetailVO
						.setProductPricingDetailId(productPricingDetail
								.getProductPricingDetailId());
				productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
				for (ProductPricingCalc productPricingCalc : productPricingDetail
						.getProductPricingCalcs()) {
					productPricingCalcVO = new ProductPricingCalcVO();
					productPricingCalcVO
							.setProductPricingCalcId(productPricingCalc
									.getProductPricingCalcId());
					productPricingCalcVO
							.setCalculationSubTypeCode(null != productPricingCalc
									.getCalculationSubType() ? Constants.Accounts.ProductCalculationSubType
									.get(productPricingCalc
											.getCalculationSubType()).name()
									: null);
					productPricingCalcVO
							.setCalculationTypeCode(Constants.Accounts.ProductCalculationType
									.get(productPricingCalc
											.getCalculationType()).name()
									.replaceAll("_", " "));
					productPricingCalcVO
							.setPricingType(null != productPricingCalc
									.getLookupDetail() ? productPricingCalc
									.getLookupDetail().getDisplayName() : null);
					productPricingCalcVO.setSalePrice(productPricingCalc
							.getSalePrice());
					productPricingCalcVO.setSalePriceAmount(productPricingCalc
							.getSalePriceAmount());
					productPricingCalcVO.setIsDefault(productPricingCalc
							.getIsDefault());
					if (ProductCalculationType.get(
							productPricingCalc.getCalculationType()).equals(
							Constants.Accounts.ProductCalculationType.Margin)
							|| ProductCalculationType
									.get(productPricingCalc
											.getCalculationType())
									.equals(Constants.Accounts.ProductCalculationType.Markup))
						productPricingCalcVO.setSalesAmount(productPricingCalc
								.getSalePrice());
					else
						productPricingCalcVO.setSalesAmount(productPricingCalc
								.getSalePriceAmount());
					productPricingCalcVOs.add(productPricingCalcVO);
				}
				productPricingDetailVO
						.setProductPricingCalcVOs(productPricingCalcVOs);
				productPricingDetailVOs.add(productPricingDetailVO);
			}
			productPricingVO
					.setProductPricingDetailVOs(productPricingDetailVOs);
			productPricingVOs.add(productPricingVO);
		}
		return productPricingVOs;
	}

	// Get Product Pricing Without Customer
	private List<ProductPricing> getProductPricingWithoutCustomer(
			long productId, long storeId, String startDate, String endDate)
			throws Exception {
		return productPricingService.getProductPricingWithoutCustomer(
				productId, storeId, Calendar.getInstance().getTime(), Calendar
						.getInstance().getTime());
	}

	// Save Product Pricing
	public void saveProductPricing(ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails,
			List<ProductPricingDetail> productPricingDetailsOld,
			List<ProductPricingCalc> productPricingCalcOldAll,
			List<ProductPricingCalc> productPricingCalcs) throws Exception {
		productPricing.setImplementation(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		productPricing.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		if (productPricing != null
				&& productPricing.getProductPricingId() != null) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		productPricingService.saveProductPricing(productPricing);

		if (null != productPricingDetails && productPricingDetails.size() > 0) {
			for (ProductPricingDetail pricingDetail : productPricingDetails)
				pricingDetail.setProductPricing(productPricing);
			productPricingService
					.saveProductPricingDetail(productPricingDetails);
		}
		if (null != productPricingCalcs && productPricingCalcs.size() > 0)
			productPricingService.saveProductPricingCalcs(productPricingCalcs);

		if (null != productPricingCalcOldAll
				&& productPricingCalcOldAll.size() > 0)
			productPricingService
					.deleteProductPricingCalc(productPricingCalcOldAll);
		if (null != productPricingDetailsOld
				&& productPricingDetailsOld.size() > 0)
			productPricingService
					.deleteProductPricingDetail(productPricingDetailsOld);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductPricing.class.getSimpleName(),
				productPricing.getProductPricingId(), user, workflowDetailVo);
	}

	public void doProductPricingBusinessUpdate(Long productPricingId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			ProductPricing productPricing = productPricingService
					.getProductPricingAll(productPricingId);
			if (workflowDetailVO.isDeleteFlag()) {
				List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
						productPricing.getProductPricingDetails());
				List<ProductPricingCalc> productPricingCalcs = new ArrayList<ProductPricingCalc>();
				for (ProductPricingDetail productPricingDetail : productPricingDetails) {
					productPricingCalcs
							.addAll(new ArrayList<ProductPricingCalc>(
									productPricingDetail
											.getProductPricingCalcs()));
				}
				productPricingService.deleteProductPricing(productPricing,
						productPricingDetails, productPricingCalcs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Save Product Pricing
	public void saveProductPricing(ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetailsOld,
			List<ProductPricingCalc> productPricingCalcOldAll) throws Exception {
		productPricing.setImplementation(getImplementation());
		productPricingService.saveProductPricing(productPricing);
		productPricingService.saveProductPricingDetailAndCalc(
				productPricingDetailsOld, productPricingCalcOldAll);
	}

	// Delete Product Pricing
	public void deleteProductPricing(ProductPricing productPricing)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		int workflowReturnStatus = workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						ProductPricing.class.getSimpleName(),
						productPricing.getProductPricingId(), user,
						workflowDetailVo);
		if (workflowReturnStatus == WorkflowConstants.ReturnStatus.DeleteApproved
				.getCode()) {
			// Common Status update blog
			workflowDetailVo.setDeleteFlag(true);
			doProductPricingBusinessUpdate(
					productPricing.getProductPricingId(), workflowDetailVo);
		}
	}

	// Delete Product Pricing
	public void deleteProductPricingDetail(
			ProductPricingDetail productPricingDetail) throws Exception {
		List<ProductPricingCalc> productPricingCalcs = new ArrayList<ProductPricingCalc>(
				productPricingDetail.getProductPricingCalcs());
		productPricingService.deleteProductPricingDetail(productPricingDetail,
				productPricingCalcs);
	}

	// Update Product Combo
	public void updateProductCombo() throws Exception {
		List<ProductPricingCalc> productPricingCalcs = productPricingService
				.getAllProductPricingCalc();
		if (null != productPricingCalcs && productPricingCalcs.size() > 0
				&& !(boolean) productPricingCalcs.get(0).getComboType()) {
			ProductPricingCalc productPricingCalc = null;
			List<ProductPricingCalc> updateProductPricingCalcs = new ArrayList<ProductPricingCalc>();
			for (ProductPricingCalc pricingCalc : productPricingCalcs) {
				productPricingCalc = new ProductPricingCalc();
				productPricingCalc.setSalePrice(0d);
				productPricingCalc.setSalePriceAmount(0d);
				productPricingCalc.setIsDefault(false);
				productPricingCalc.setStatus(false);
				productPricingCalc.setComboType(true);
				productPricingCalc.setCalculationType(pricingCalc
						.getCalculationType());
				productPricingCalc.setCalculationSubType(pricingCalc
						.getCalculationSubType());
				productPricingCalc.setLookupDetail(pricingCalc
						.getLookupDetail());
				productPricingCalc.setProductPricingDetail(pricingCalc
						.getProductPricingDetail());
				updateProductPricingCalcs.add(productPricingCalc);
			}
			productPricingService
					.saveProductPricingCalc(updateProductPricingCalcs);
		}
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public Double getSuggestedRetailPriceByProduct(Long productId) {
		ProductPricingDetail ppd = null;
		try {
			ppd = productPricingService.getPricingDetailByProductId(productId);

			if (ppd != null)
				return ppd.getSuggestedRetailPrice();
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// Update Selling Price
	public void updateProductSellingPrice() throws Exception {
		Implementation impl6 = new Implementation();
		impl6.setImplementationId(6l);
		Implementation impl14 = new Implementation();
		impl14.setImplementationId(14l);
		List<ProductPricingDetail> pricingDetails = productPricingService
				.getPricingDetailByImplementation(impl6, impl14);
		for (ProductPricingDetail productPricingDetail : pricingDetails) {
			if (null != productPricingDetail.getProductPricingCalcs()
					&& productPricingDetail.getProductPricingCalcs().size() > 0) {
				for (ProductPricingCalc pricingCalc : productPricingDetail
						.getProductPricingCalcs()) {
					if (null != pricingCalc.getStatus()
							&& (boolean) pricingCalc.getStatus() == true) {
						productPricingDetail.setSellingPrice(pricingCalc
								.getSalePrice());
						break;
					}
				}
			}
		}
	}

	public void deleteUnusedAnalysisAccountCombination() throws Exception {
		Implementation implementation = new Implementation();
		implementation.setImplementationId(null);
		List<Combination> combinations = productBL.getCombinationBL()
				.getCombinationService()
				.getUnusedAnalysisAccountCombination(implementation);
		if (null != combinations && combinations.size() > 0) {
			Map<Long, Combination> combinationMap = new HashMap<Long, Combination>();
			for (Combination combination : combinations)
				combinationMap.put(combination.getCombinationId(), combination);

			List<TransactionDetail> transactionDetails = customerBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailCombinationFilter(
							combinationMap.keySet());
			if (null != transactionDetails && transactionDetails.size() > 0) {
				for (TransactionDetail transactionDetail : transactionDetails) {
					if (combinationMap.containsKey(transactionDetail
							.getCombination().getCombinationId())) {
						combinationMap.remove(transactionDetail
								.getCombination().getCombinationId());
					}
				}
			}

			if (null != combinationMap && combinationMap.size() > 0) {
				List<Product> products = productBL.getProductService()
						.getProductsByCombination(combinationMap.keySet());
				if (null != products && products.size() > 0) {
					for (Product product : products) {
						if (null != product.getCombinationByInventoryAccount()) {
							if (combinationMap.containsKey(product
									.getCombinationByInventoryAccount()
									.getCombinationId()))
								combinationMap.remove(product
										.getCombinationByInventoryAccount()
										.getCombinationId());
						} else if (null != product
								.getCombinationByExpenseAccount()) {
							if (combinationMap.containsKey(product
									.getCombinationByExpenseAccount()
									.getCombinationId()))
								combinationMap.remove(product
										.getCombinationByExpenseAccount()
										.getCombinationId());
						}

					}
				}

			}

			if (null != combinationMap && combinationMap.size() > 0) {
				List<Customer> customers = customerBL.getCustomerService()
						.getCustomerByCombination(combinationMap.keySet());
				if (null != customers && customers.size() > 0) {
					for (Customer customer : customers) {
						if (combinationMap.containsKey(customer
								.getCombination().getCombinationId()))
							combinationMap.remove(customer.getCombination()
									.getCombinationId());
					}
				}
			}

			if (null != combinationMap && combinationMap.size() > 0) {
				List<Supplier> suppliers = supplierService
						.getSupplierByCombination(combinationMap.keySet());
				if (null != suppliers && suppliers.size() > 0) {
					for (Supplier supplier : suppliers) {
						if (combinationMap.containsKey(supplier
								.getCombination().getCombinationId()))
							combinationMap.remove(supplier.getCombination()
									.getCombinationId());
					}
				}
			}

			if (null != combinationMap && combinationMap.size() > 0) {
				List<BankAccount> bankAccounts = bankService
						.getBankAccountByCombination(combinationMap.keySet());
				if (null != bankAccounts && bankAccounts.size() > 0) {
					for (BankAccount bankAccount : bankAccounts) {
						if (combinationMap.containsKey(bankAccount
								.getCombination().getCombinationId()))
							combinationMap.remove(bankAccount.getCombination()
									.getCombinationId());
						if (null != bankAccount
								.getCombinationByClearingAccount()) {
							if (combinationMap.containsKey(bankAccount
									.getCombinationByClearingAccount()
									.getCombinationId()))
								combinationMap.remove(bankAccount
										.getCombinationByClearingAccount()
										.getCombinationId());
						}
					}
				}
			}

			if (null != combinationMap && combinationMap.size() > 0) {
				List<Ledger> ledgers = new ArrayList<Ledger>();
				List<Combination> combinationNw = new ArrayList<Combination>();
				for (Entry<Long, Combination> entry : combinationMap.entrySet()) {
					ledgers.addAll(entry.getValue().getLedgers());
					combinationNw.add(entry.getValue());
				}
				customerBL.getCombinationBL().getCombinationService()
						.deleteCombinations(ledgers, combinationNw);
			}
		}
	}

	// Getters and Setters
	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public ProductPricingService getProductPricingService() {
		return productPricingService;
	}

	public void setProductPricingService(
			ProductPricingService productPricingService) {
		this.productPricingService = productPricingService;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public SupplierService getSupplierService() {
		return supplierService;
	}

	@Autowired
	public void setSupplierService(SupplierService supplierService) {
		this.supplierService = supplierService;
	}

	public BankService getBankService() {
		return bankService;
	}

	@Autowired
	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	}
}
