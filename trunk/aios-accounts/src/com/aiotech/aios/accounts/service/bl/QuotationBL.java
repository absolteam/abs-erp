package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.service.ProductService;
import com.aiotech.aios.accounts.service.QuotationService;
import com.aiotech.aios.accounts.service.RequisitionService;
import com.aiotech.aios.accounts.service.SupplierService;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class QuotationBL {

	private QuotationService quotationService;
	private SupplierService supplierService;
	private RequisitionService requisitionService;
	private CurrencyBL currencyBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;
	private ProductService productService;
	private CreditTermBL creditTermBL;
	
	public void saveQuotation(Quotation quotation,
			List<QuotationDetail> deleteQuotationDetails,
			Requisition requisition) throws Exception {
		quotation.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		boolean editFlag = false;
		if (quotation != null && quotation.getQuotationId() != null
				&& quotation.getQuotationId() > 0) {
			editFlag = true;
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (null == quotation.getQuotationId())
			SystemBL.saveReferenceStamp(Quotation.class.getName(),
					quotation.getImplementation());
		quotationService.saveQuotation(quotation);
		saveDocuments(quotation, editFlag);
		if (null != deleteQuotationDetails && deleteQuotationDetails.size() > 0)
			this.getQuotationService().deleteQuotationDetail(
					deleteQuotationDetails);
		if (null != requisition)
			requisitionService.saveRequisition(requisition);
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Quotation.class.getSimpleName(), quotation.getQuotationId(),
				user, workflowDetailVo);
	}

	public void saveDocuments(Quotation obj, boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(obj, "quotationDocs",
				editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, obj.getQuotationId(),
					Quotation.class.getSimpleName(), "quotationDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	// Delete Quotation
	public void deleteQuotation(Quotation quotation,
			List<QuotationDetail> quotationDetails) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Quotation.class.getSimpleName(), quotation.getQuotationId(),
				user, workflowDetailVo);
	}

	public void doQuotationBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Quotation quotation = quotationService.getQuotationById(recordId)
				.get(0);

		if (workflowDetailVO.isDeleteFlag()) {
			quotationService.deleteQuotation(
					quotation,
					new ArrayList<QuotationDetail>(quotation
							.getQuotationDetails()));

		} else {
			if (null != quotation.getRequisition()) {
				Requisition requisition = requisitionService
						.getRequisitionById(quotation.getRequisition()
								.getRequisitionId());
				requisition.setStatus(RequisitionProcessStatus.InProgress
						.getCode());
				requisitionService.updateRequisition(requisition);
			}
		}
	}

	// Getters & Setters
	public QuotationService getQuotationService() {
		return quotationService;
	}

	public void setQuotationService(QuotationService quotationService) {
		this.quotationService = quotationService;
	}

	public SupplierService getSupplierService() {
		return supplierService;
	}

	public void setSupplierService(SupplierService supplierService) {
		this.supplierService = supplierService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CurrencyBL getCurrencyBL() {
		return currencyBL;
	}

	public void setCurrencyBL(CurrencyBL currencyBL) {
		this.currencyBL = currencyBL;
	}

	public RequisitionService getRequisitionService() {
		return requisitionService;
	}

	public void setRequisitionService(RequisitionService requisitionService) {
		this.requisitionService = requisitionService;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}
}
