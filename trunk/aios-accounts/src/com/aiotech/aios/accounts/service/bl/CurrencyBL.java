package com.aiotech.aios.accounts.service.bl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.vo.CurrencyVO;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CurrencyBL {

	private CurrencyService currencyService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void saveCurrency(List<Currency> currencies, User user)
			throws Exception { 
		WorkflowDetailVO workflowDetailVo = null;
		for (Currency currency : currencies) {
			workflowDetailVo = new WorkflowDetailVO();
			if (currency != null && currency.getCurrencyId() != null) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			currency.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			currencyService.saveCurrency(currency);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Currency.class.getSimpleName(), currency.getCurrencyId(),
					user, workflowDetailVo);
		} 
	}

	public void saveCurrency(Currency currency, User user) throws Exception {
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (currency != null && currency.getCurrencyId() != null) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			currency.setIsApprove(Byte.parseByte(String
					.valueOf(Constants.RealEstate.Status.NoDecession.getCode())));
			currencyService.saveCurrency(currency);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Currency.class.getSimpleName(), currency.getCurrencyId(),
					user, workflowDetailVo);
		} catch (Exception ex) {

		}
	}

	public void doCurrencyBusinessUpdate(Long currencyId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				Currency currency = currencyService.getCurrency(currencyId);
				currencyService.deleteCurrency(currency);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Currency getDefaultCurrency() {
		Currency currency = currencyService
				.getDefaultCurrency(getImplemenationId());
		return currency;
	}

	public void deleteCurrency(Currency currency) {
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
							.getCode());
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Currency.class.getSimpleName(), currency.getCurrencyId(),
					user, workflowDetailVo);
		} catch (Exception e) {

		}
	}

	public CurrencyVO addCurrencyVO(Currency currency) throws Exception {
		CurrencyVO currencyVO = new CurrencyVO();
		BeanUtils.copyProperties(currencyVO, currency);
		currencyVO.setCountryName(currency.getCurrencyPool().getCountry()
				.getCountryName());
		currencyVO.setCode(currency.getCurrencyPool().getCode());
		return currencyVO;
	}

	// Get Implementation
	public Implementation getImplemenationId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = new Implementation();
		if (null != session.getAttribute("THIS"))
			implementation = (Implementation) session.getAttribute("THIS");
		return implementation;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
