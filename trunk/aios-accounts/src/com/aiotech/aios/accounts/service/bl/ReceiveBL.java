package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.BeanUtils;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveVO;
import com.aiotech.aios.accounts.service.ReceiveService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ReceiveBL {

	// Dependencies
	private ReceiveService receiveService;
	private Stock stockDetails;
	private List<Stock> stockList;
	private PurchaseBL purchaseBL;
	private GoodsReturnBL goodsReturnBL;
	private TransactionBL transactionBL;
	private StoreBL storeBL;
	private StockBL stockBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;

	public List<Object> getClosedGRNList() throws Exception {
		List<Receive> receives = receiveService
				.getClosedReceiveNotes(purchaseBL.getImplemenationId());
		List<Object> receiveVOs = new ArrayList<Object>();
		if (null != receives && receives.size() > 0) {
			Map<Long, Receive> receiveMap = new HashMap<Long, Receive>();
			for (Receive receive : receives) {
				for (ReceiveDetail receiveDetail : receive.getReceiveDetails()) {
					double returnedQty = 0;
					if (null != receiveDetail.getGoodsReturnDetails()
							&& receiveDetail.getGoodsReturnDetails().size() > 0) {
						for (GoodsReturnDetail goodsReturnDetail : receiveDetail
								.getGoodsReturnDetails())
							returnedQty += goodsReturnDetail.getReturnQty();
					}
					if (receiveDetail.getReceiveQty() > returnedQty)
						receiveMap.put(receive.getReceiveId(), receive);
				}
			}
			if (null != receiveMap && receiveMap.size() > 0) {
				ReceiveVO receiveVO = null;
				for (Entry<Long, Receive> entry : receiveMap.entrySet()) {
					Receive receive = entry.getValue();
					receiveVO = new ReceiveVO();
					receiveVO.setGrnDate(DateFormat.convertDateToString(receive
							.getReceiveDate().toString()));
					receiveVO.setReceiveId(receive.getReceiveId());
					receiveVO.setReceiveNumber(receive.getReceiveNumber());
					receiveVO.setSupplierId(receive.getSupplier()
							.getSupplierId());
					if (null != receive.getSupplier().getPerson())
						receiveVO.setSupplierName(receive
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat(" ")
								.concat(receive.getSupplier().getPerson()
										.getLastName()));
					else
						receiveVO.setSupplierName(null != receive.getSupplier()
								.getCmpDeptLocation() ? receive.getSupplier()
								.getCmpDeptLocation().getCompany()
								.getCompanyName() : receive.getSupplier()
								.getCompany().getCompanyName());
					receiveVOs.add(receiveVO);
				}
			}
		}
		return receiveVOs;
	}

	// Save Receive
	public void saveReceiptDetails(Receive receive, GoodsReturn goodsReturn,
			List<Transaction> transactions, List<Purchase> purchaseList,
			String unifiedPrice, Receive receiveEdit) throws Exception {
		receive.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (receive != null && receive.getReceiveId() != null
				&& receive.getReceiveId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (receiveEdit != null && receiveEdit.getReceiveId() > 0) {
			receiveDirectDelete(receiveEdit, purchaseList);
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
			receive.setReceiveId(receiveEdit.getReceiveId());
			receive.setReceiveNumber(receiveEdit.getReceiveNumber());
		}
		boolean updateFlag = false;
		if (null != receive.getReceiveId())
			updateFlag = true;
		receiveService.saveReceipts(receive, new ArrayList<ReceiveDetail>(
				receive.getReceiveDetails()));
		if (updateFlag)
			purchaseBL.getPurchaseService().mergePurchaseOrder(purchaseList);
		else
			purchaseBL.getPurchaseService().savePurchaseOrder(purchaseList);

		if (null != goodsReturn) {
			if (null == goodsReturn.getGoodsReturnId())
				SystemBL.saveReferenceStamp(GoodsReturn.class.getName(),
						receive.getImplementation());
			List<GoodsReturnDetail> goodsReturnDetailList = new ArrayList<GoodsReturnDetail>(
					goodsReturn.getGoodsReturnDetails());
			goodsReturn.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			goodsReturnBL.getGoodsReturnService().saveReturns(goodsReturn,
					goodsReturnDetailList);
		}

		if (updateFlag) {
			transactionBL.deleteTransaction(receive.getReceiveId(),
					Receive.class.getSimpleName());

			Stock stock = null;
			List<Stock> deleteStocks = null;
			if (null != receiveEdit.getReceiveDetails()
					&& receiveEdit.getReceiveDetails().size() > 0) {
				deleteStocks = new ArrayList<Stock>();
				for (ReceiveDetail stockList : receiveEdit.getReceiveDetails()) {
					stock = new Stock();
					stock.setProduct(stockList.getProduct());
					stock.setQuantity(stockList.getReceiveQty());
					stock.setStore(stockList.getShelf().getShelf().getAisle()
							.getStore());
					stock.setShelf(stockList.getShelf());
					stock.setBatchNumber(stockList.getBatchNumber());
					stock.setProductExpiry(stockList.getProductExpiry());
					stock.setImplementation(purchaseBL.getImplemenationId());
					deleteStocks.add(stock);
				}
				stockBL.deleteBatchStockDetails(deleteStocks);
			}
		} else
			SystemBL.saveReferenceStamp(Receive.class.getName(),
					receive.getImplementation());

		saveDocuments(receive, updateFlag);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Receive.class.getSimpleName(), receive.getReceiveId(), user,
				workflowDetailVo);
	}

	public void saveDocuments(Receive obj, boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(obj, "receiveDocs",
				editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, obj.getReceiveId(), Receive.class.getSimpleName(),
					"receiveDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	private List<ProductPricingDetail> getUpdatedProductPricing(
			String unifiedPrice, double basePrice, ReceiveDetail receiveDetail)
			throws Exception {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>();
		ProductPricingDetail productPricingDetail = null;
		if (unifiedPrice.equals("true")) {
			productPricingDetail = stockBL
					.getProductPricingBL()
					.getPricingDetail(receiveDetail.getProduct().getProductId());
			if (null != productPricingDetail) {
				productPricingDetail.setBasicCostPrice(basePrice);
				productPricingDetails.add(productPricingDetail);
			}
		} else {
			Shelf shelf = stockBL
					.getStoreBL()
					.getStoreService()
					.getStoreByShelfId(
							receiveDetail.getProduct().getShelf().getShelfId());
			productPricingDetail = stockBL.getProductPricingBL()
					.getPricingDetailWithProductStore(
							receiveDetail.getProduct().getProductId(),
							shelf.getAisle().getStore().getStoreId());
			if (null != productPricingDetail) {
				productPricingDetail.setBasicCostPrice(basePrice);
				productPricingDetails.add(productPricingDetail);
			} else {
				ProductPricing productPricing = stockBL
						.getProductPricingBL()
						.getProductPricingService()
						.getProductPricingWithProductAndDate(
								receiveDetail.getProduct().getProductId(),
								Calendar.getInstance().getTime(),
								Calendar.getInstance().getTime());
				if (null != productPricing) {
					for (ProductPricingDetail productDetail : productPricing
							.getProductPricingDetails()) {
						if (null != productDetail.getStatus()
								&& (boolean) productDetail.getStatus()) {
							productPricingDetail = new ProductPricingDetail();
							productPricingDetail.setProduct(productDetail
									.getProduct());
							productPricingDetail.setStore(shelf.getAisle()
									.getStore());
							productPricingDetail.setBasicCostPrice(basePrice);
							productPricingDetail.setStandardPrice(productDetail
									.getStandardPrice());
							productPricingDetail
									.setSuggestedRetailPrice(productDetail
											.getSuggestedRetailPrice());
							productPricingDetail.setSellingPrice(productDetail
									.getSellingPrice());
							productPricingDetail
									.setProductPricing(productPricing);
							productPricingDetail.setStatus(true);
							productPricingDetails.add(productPricingDetail);
							break;
						}
					}
				}
			}
		}
		return productPricingDetails;
	}

	public void receiveDirectDelete(Receive receiveEdit,
			List<Purchase> purchaseList) throws Exception {
		// JV Transaction Delete
		transactionBL.deleteTransaction(receiveEdit.getReceiveId(),
				Receive.class.getSimpleName());

		Map<Long, Purchase> mapPurchase = new HashMap<Long, Purchase>();
		if (purchaseList != null && purchaseList.size() > 0) {
			for (Purchase purchase : purchaseList)
				mapPurchase.put(purchase.getPurchaseId(), purchase);
		}
		List<GoodsReturnDetail> returnDetails = new ArrayList<GoodsReturnDetail>();
		List<GoodsReturn> goodsReturns = new ArrayList<GoodsReturn>(
				receiveEdit.getGoodsReturns());
		if (goodsReturns != null && goodsReturns.size() > 0)
			returnDetails.addAll(goodsReturns.get(0).getGoodsReturnDetails());

		if (returnDetails != null && returnDetails.size() > 0)
			goodsReturnBL.directGoodsReturnDelete(returnDetails.get(0)
					.getGoodsReturn(), returnDetails);
		receiveService.deleteReceiptDetails(new ArrayList<ReceiveDetail>(
				receiveEdit.getReceiveDetails()));
	}

	public void deleteReceiveFullDetails(Receive receiveEdit,
			List<Purchase> purchaseList) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Receive.class.getSimpleName(), receiveEdit.getReceiveId(),
				user, workflowDetailVo);

	}

	public void doReceiveBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Receive receiveEdit = this.getReceiveService().getReceiveInfoById(
				recordId);
		List<Purchase> purchaseList = purchaseBL.getPurchaseService()
				.getSupplierActivePurchaseOrderWithReceive(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			// JV Transaction Delete
			transactionBL.deleteTransaction(receiveEdit.getReceiveId(),
					Receive.class.getSimpleName());

			Map<Long, Purchase> mapPurchase = new HashMap<Long, Purchase>();
			if (purchaseList != null && purchaseList.size() > 0)
				for (Purchase purchase : purchaseList) {
					mapPurchase.put(purchase.getPurchaseId(), purchase);
				}
			// Stock Details
			List<Stock> stockList = new ArrayList<Stock>();
			List<GoodsReturnDetail> returnDetails = new ArrayList<GoodsReturnDetail>();
			List<Purchase> purchases = new ArrayList<Purchase>();
			for (ReceiveDetail list : receiveEdit.getReceiveDetails()) {
				Shelf rack = storeBL.getStoreService().getShelfById(
						list.getShelf().getShelfId());
				Stock tempStock = new Stock();
				tempStock.setProduct(list.getProduct());
				tempStock.setStore(rack.getAisle().getStore());
				tempStock.setQuantity(list.getReceiveQty());
				tempStock.setImplementation(purchaseBL.getImplemenationId());
				tempStock.setUnitRate(list.getUnitRate());
				tempStock.setShelf(list.getShelf());
				tempStock.setBatchNumber(list.getBatchNumber());
				tempStock.setProductExpiry(list.getProductExpiry());
				stockList.add(tempStock);
				if (mapPurchase.containsKey(list.getPurchase().getPurchaseId())) {
					Purchase purchase = list.getPurchase();
					purchase.setStatus(1);
					purchases.add(purchase);
				}
			}
			stockBL.deleteBatchStockDetails(stockList);

			List<GoodsReturn> goodsReturns = new ArrayList<GoodsReturn>(
					receiveEdit.getGoodsReturns());
			if (goodsReturns != null && goodsReturns.size() > 0)
				returnDetails.addAll(goodsReturns.get(0)
						.getGoodsReturnDetails());

			// Goods Return
			if (returnDetails != null && returnDetails.size() > 0)
				goodsReturnBL.deleteGoodsReturn(returnDetails.get(0)
						.getGoodsReturn(), returnDetails, null);
			if (null != purchases && purchases.size() > 0)
				purchaseBL.updatePurchaseOrder(purchases);
			// Receive
			receiveService.deleteReceipts(
					receiveEdit,
					new ArrayList<ReceiveDetail>(receiveEdit
							.getReceiveDetails()));
		} else {
			List<Transaction> transactions = this.createGRNTransaction(
					receiveEdit,
					new ArrayList<ReceiveDetail>(receiveEdit
							.getReceiveDetails()), this.getTransactionBL()
							.getCategory("Good Receiving Note (GRN)"));
			List<ProductPricingDetail> pricingDetails = new ArrayList<ProductPricingDetail>();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			String unifiedPrice = "";
			if (session.getAttribute("unified_price") != null)
				unifiedPrice = session.getAttribute("unified_price").toString();
			for (ReceiveDetail receiveDetail : receiveEdit.getReceiveDetails()) {
				List<PurchaseDetail> updatePurchaseDetails = purchaseBL
						.getPurchaseService().getPurchaseProducts(
								receiveDetail.getProduct().getProductId());
				double totalRate = 0;
				double totalQty = 0;
				double unitRate = 0;
				List<ProductPricingDetail> productPricingDetails = null;
				for (PurchaseDetail detail : updatePurchaseDetails) {
					totalRate += (detail.getUnitRate() * detail.getQuantity());
					totalQty += detail.getQuantity();
				}
				unitRate = AIOSCommons.roundDecimals((totalRate / totalQty));

				// Update the cost price
				if (null != unifiedPrice && !("").equals(unifiedPrice)) {
					productPricingDetails = getUpdatedProductPricing(
							unifiedPrice, unitRate, receiveDetail);

					if (null != productPricingDetails
							&& productPricingDetails.size() > 0)
						pricingDetails.addAll(productPricingDetails);
				}
			}

			List<Stock> stockList = new ArrayList<Stock>();

			for (ReceiveDetail list : receiveEdit.getReceiveDetails()) {
				Shelf rack = storeBL.getStoreService().getShelfById(
						list.getShelf().getShelfId());
				Stock tempStock = new Stock();
				tempStock.setProduct(list.getProduct());
				tempStock.setStore(rack.getAisle().getStore());
				tempStock.setQuantity(list.getReceiveQty());
				tempStock.setImplementation(receiveEdit.getImplementation());
				tempStock.setUnitRate(list.getUnitRate());
				tempStock.setShelf(list.getShelf());
				tempStock.setBatchNumber(list.getBatchNumber());
				tempStock.setProductExpiry(list.getProductExpiry());
				stockList.add(tempStock);
			}
			stockBL.updateBatchStockDetails(stockList);

			for (Transaction transaction : transactions)
				transaction.setRecordId(receiveEdit.getReceiveId());
			transactionBL.saveTransactions(transactions);
		}
	}

	public List<Object> getAssetReceiveDetail(Implementation implementation)
			throws Exception {
		List<ReceiveDetail> receiveDetails = receiveService
				.getAssetReceiveDetail(implementation);
		List<Object> receiveDetailVOs = new ArrayList<Object>();
		if (null != receiveDetails && receiveDetails.size() > 0) {
			for (ReceiveDetail receiveDetail : receiveDetails)
				receiveDetailVOs.add(addReceiveDetail(receiveDetail));
		}
		return receiveDetailVOs;
	}

	private ReceiveDetailVO addReceiveDetail(ReceiveDetail receiveDetail)
			throws Exception {
		ReceiveDetailVO receiveDetailVO = new ReceiveDetailVO();
		receiveDetailVO.setReceiveDetailId(receiveDetail.getReceiveDetailId());
		receiveDetailVO.setReferenceNumber(receiveDetail.getReceive()
				.getReceiveNumber().toString());
		receiveDetailVO.setUnitRate(receiveDetail.getUnitRate());
		receiveDetailVO.setProductCode(receiveDetail.getProduct().getCode());
		receiveDetailVO.setProductName(receiveDetail.getProduct()
				.getProductName());
		if (null != receiveDetail.getReceive().getSupplier().getPerson())
			receiveDetailVO.setSupplierName(receiveDetail
					.getReceive()
					.getSupplier()
					.getPerson()
					.getFirstName()
					.concat(" ")
					.concat(receiveDetail.getReceive().getSupplier()
							.getPerson().getLastName()));
		else
			receiveDetailVO.setSupplierName(null != receiveDetail.getReceive()
					.getSupplier().getCmpDeptLocation() ? receiveDetail
					.getReceive().getSupplier().getCmpDeptLocation()
					.getCompany().getCompanyName() : receiveDetail.getReceive()
					.getSupplier().getCompany().getCompanyName());
		return receiveDetailVO;
	}

	public List<Transaction> createGRNTransaction(Receive receive,
			List<ReceiveDetail> receiveList, Category category)
			throws Exception {
		Map<Long, List<ReceiveDetail>> receiveDtMap = new HashMap<Long, List<ReceiveDetail>>();
		List<ReceiveDetail> receiveDetails = null;
		long key = 0;
		for (ReceiveDetail receiveDetail : receiveList) {
			receiveDetails = new ArrayList<ReceiveDetail>();
			key = receiveDetail.getPurchase().getCurrency().getCurrencyId();
			if (receiveDtMap.containsKey(key))
				receiveDetails.addAll(receiveDtMap.get(key));
			receiveDetails.add(receiveDetail);
			receiveDtMap.put(key, receiveDetails);
		}
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (Entry<Long, List<ReceiveDetail>> entry : receiveDtMap.entrySet()) {
			receiveDetails = new ArrayList<ReceiveDetail>(entry.getValue());
			double transactionAmount = 0;
			List<TransactionDetail> transactionDetail = new ArrayList<TransactionDetail>();
			for (ReceiveDetail inventoryList : receiveDetails) {
				transactionAmount += inventoryList.getUnitRate()
						* inventoryList.getReceiveQty();
				TransactionDetail debitTransaction = this
						.getTransactionBL()
						.createTransactionDetail(
								(inventoryList.getUnitRate() * inventoryList
										.getReceiveQty()),
								getProductCombination(inventoryList
										.getProduct().getProductId(),
										inventoryList.getShelf().getShelfId()),
								TransactionType.Debit.getCode(), null, null,
								receive.getReceiveNumber());
				transactionDetail.add(debitTransaction);
			}
			TransactionDetail creditTransaction = transactionBL
					.createTransactionDetail(transactionAmount, receive
							.getSupplier().getCombination().getCombinationId(),
							TransactionType.Credit.getCode(), null, null,
							receive.getReceiveNumber());
			transactionDetail.add(creditTransaction);
			Transaction transaction = transactionBL.createTransaction(
					transactionBL.getCalendarBL().transactionPostingPeriod(
							receive.getReceiveDate()), entry.getKey(),
					"Goods Receive Note Ref: " + receive.getReceiveNumber(),
					receive.getReceiveDate(), category, receive.getReceiveId(),
					Receive.class.getSimpleName(), receiveDetails.get(0)
							.getPurchase().getExchangeRate());
			transaction.setTransactionDetails(new HashSet<TransactionDetail>(
					transactionDetail));
			transactions.add(transaction);
		}
		return transactions;
	}

	private Long getProductCombination(long productId, int shelfId)
			throws Exception {
		Combination combination = purchaseBL.getProductBL()
				.getProductInventory(productId);
		return combination.getCombinationId();
	}

	// Validate Receive Details
	public List<ReceiveVO> validateReceiveDetailInvoice(
			List<ReceiveDetail> receiveDetailList) throws Exception {
		ReceiveVO receiveVO = null;
		ReceiveDetailVO receiveDetailVO = null;
		Map<Long, ReceiveVO> receiveVOHash = new HashMap<Long, ReceiveVO>();
		Map<Long, List<ReceiveDetailVO>> receiveDetailVOHash = new HashMap<Long, List<ReceiveDetailVO>>();
		List<ReceiveDetailVO> receiveDetails = new ArrayList<ReceiveDetailVO>();
		InvoiceDetailVO invoiceDetailVO = null;
		Map<Long, List<InvoiceDetailVO>> invoiceVOHash = new HashMap<Long, List<InvoiceDetailVO>>();
		List<InvoiceDetailVO> invoiceDetailVOList = null;
		for (ReceiveDetail receiveDetail : receiveDetailList) {
			receiveVO = new ReceiveVO();
			receiveDetailVO = new ReceiveDetailVO();
			receiveVO.setReceiveId(receiveDetail.getReceive().getReceiveId());
			receiveVO.setIsApprove(receiveDetail.getReceive().getIsApprove());
			receiveVO.setReceiveNumber(receiveDetail.getReceive()
					.getReceiveNumber());
			receiveVO.setReceiveDate(receiveDetail.getReceive()
					.getReceiveDate());
			receiveVO.setReferenceNo(receiveDetail.getReceive()
					.getReferenceNo());
			receiveVO.setSupplier(receiveDetail.getReceive().getSupplier());
			receiveVO.setDescription(receiveDetail.getReceive()
					.getDescription());
			receiveDetailVO.setReceiveDetailId(receiveDetail
					.getReceiveDetailId());
			receiveDetailVO.setProduct(receiveDetail.getProduct());
			receiveDetailVO.setReceiveQty(receiveDetail.getPackageUnit());
			receiveDetailVO.setUnitRate(receiveDetail.getUnitRate());
			receiveDetailVO.setDescription(receiveDetail.getDescription());
			receiveDetailVO.setPurchase(receiveDetail.getPurchase());
			receiveDetailVO.setPurchaseId(receiveDetail.getPurchase()
					.getPurchaseId());
			receiveDetailVO.setInvoiceQty(receiveDetail.getPackageUnit());
			receiveDetailVO
					.setInvoiceDetails(receiveDetail.getInvoiceDetails());
			receiveDetails.add(receiveDetailVO);
			List<ReceiveDetailVO> receiveDetailVOList = receiveDetailVOHash
					.get(receiveVO.getReceiveId());
			if (null != receiveDetailVOList && receiveDetailVOList.size() > 0) {
				receiveDetailVOList.add(receiveDetailVO);
				receiveDetailVOHash.put(receiveVO.getReceiveId(),
						receiveDetailVOList);
			} else {
				receiveDetailVOList = new ArrayList<ReceiveDetailVO>();
				receiveDetailVOList.add(receiveDetailVO);
				receiveDetailVOHash.put(receiveVO.getReceiveId(),
						receiveDetailVOList);
			}
			invoiceDetailVOList = new ArrayList<InvoiceDetailVO>();
			for (InvoiceDetail invoiceDetail : receiveDetail
					.getInvoiceDetails()) {
				invoiceDetailVO = new InvoiceDetailVO();
				invoiceDetailVO.setInvoiceDetailId(invoiceDetail
						.getInvoiceDetailId());
				invoiceDetailVO.setInvoiceQty(invoiceDetail.getInvoiceQty());
				invoiceDetailVO.setDescription(invoiceDetail.getDescription());
				invoiceDetailVOList.add(invoiceDetailVO);
			}
			if (invoiceVOHash.containsKey(receiveDetail.getReceiveDetailId())) {
				invoiceDetailVOList.addAll(invoiceVOHash.get(receiveDetail
						.getReceiveDetailId()));
			}
			invoiceVOHash.put(receiveDetail.getReceiveDetailId(),
					invoiceDetailVOList);
			receiveVOHash.put(receiveVO.getReceiveId(), receiveVO);
		}
		for (Entry<Long, List<ReceiveDetailVO>> receiveId : receiveDetailVOHash
				.entrySet()) {
			List<ReceiveDetailVO> receiveDetailVOList = receiveDetailVOHash
					.get(receiveId.getKey());
			invoiceDetailVOList = new ArrayList<InvoiceDetailVO>();
			for (ReceiveDetailVO detailVO : receiveDetailVOList) {
				if (invoiceVOHash.containsKey(detailVO.getReceiveDetailId())) {
					List<InvoiceDetailVO> invoiceDetails = invoiceVOHash
							.get(detailVO.getReceiveDetailId());
					for (InvoiceDetailVO invoiceDetail : invoiceDetails) {
						detailVO.setInvoiceDetailId(invoiceDetail
								.getInvoiceDetailId());
						detailVO.setInvoicedQty(invoiceDetail.getInvoiceQty()
								+ (null != detailVO.getInvoicedQty() ? detailVO
										.getInvoicedQty() : 0));
						detailVO.setInvoiceQty(detailVO.getReceiveQty()
								- detailVO.getInvoicedQty());
						detailVO.setReceivedQty(detailVO.getReceiveQty()
								+ (null != detailVO.getReceivedQty() ? detailVO
										.getReceivedQty() : 0));
						detailVO.setTotalRate(detailVO.getReceivedQty()
								* detailVO.getUnitRate());
						detailVO.setDescription(invoiceDetail.getDescription());
					}
					if (invoiceVOHash
							.containsKey(detailVO.getReceiveDetailId())) {
						invoiceDetailVOList.addAll(invoiceVOHash.get(detailVO
								.getReceiveDetailId()));
					}
					invoiceVOHash.put(detailVO.getReceiveDetailId(),
							invoiceDetails);
				}
			}
			if (receiveVOHash.containsKey(receiveId.getKey())) {
				receiveVO = receiveVOHash.get(receiveId.getKey());
				receiveVO.setReceiveDetailsVO(receiveDetailVOList);
				for (ReceiveDetailVO receiveDetail : receiveVO
						.getReceiveDetailsVO()) {
					receiveVO
							.setTotalReceiveQty(receiveDetail.getReceiveQty()
									+ (null != receiveVO.getTotalReceiveQty() ? receiveVO
											.getTotalReceiveQty() : 0));
					receiveVO
							.setTotalReceiveAmount(receiveDetail
									.getReceiveQty()
									* receiveDetail.getUnitRate()
									+ (null != receiveVO
											.getTotalReceiveAmount() ? receiveVO
											.getTotalReceiveAmount() : 0));
				}
				receiveVOHash.put(receiveId.getKey(), receiveVO);
			}
		}
		return new ArrayList<ReceiveVO>(receiveVOHash.values());
	}

	// Validate Receive Details
	public List<ReceiveVO> validateReceiveDetailInvoiceEdit(
			List<ReceiveDetail> receiveDetailList) throws Exception {
		ReceiveVO receiveVO = null;
		ReceiveDetailVO receiveDetailVO = null;
		Map<Long, ReceiveVO> receiveVOHash = new HashMap<Long, ReceiveVO>();
		Map<Long, List<ReceiveDetailVO>> receiveDetailVOHash = new HashMap<Long, List<ReceiveDetailVO>>();
		List<ReceiveDetailVO> receiveDetails = new ArrayList<ReceiveDetailVO>();
		// InvoiceDetailVO invoiceDetailVO = null;
		Map<Long, List<InvoiceDetailVO>> invoiceVOHash = new HashMap<Long, List<InvoiceDetailVO>>();
		List<InvoiceDetailVO> invoiceDetailVOList = null;
		for (ReceiveDetail receiveDetail : receiveDetailList) {
			receiveVO = new ReceiveVO();
			receiveDetailVO = new ReceiveDetailVO();
			receiveVO.setReceiveId(receiveDetail.getReceive().getReceiveId());
			receiveVO.setReceiveNumber(receiveDetail.getReceive()
					.getReceiveNumber());
			receiveVO.setReceiveDate(receiveDetail.getReceive()
					.getReceiveDate());
			receiveVO.setReferenceNo(receiveDetail.getReceive()
					.getReferenceNo());
			receiveVO.setSupplier(receiveDetail.getReceive().getSupplier());
			receiveVO.setDescription(receiveDetail.getReceive()
					.getDescription());
			receiveDetailVO.setReceiveDetailId(receiveDetail
					.getReceiveDetailId());
			receiveDetailVO.setProduct(receiveDetail.getProduct());
			receiveDetailVO.setReceiveQty(receiveDetail.getReceiveQty());
			receiveDetailVO.setPurchase(receiveDetail.getPurchase());
			receiveDetailVO.setPurchaseId(receiveDetail.getPurchase()
					.getPurchaseId());
			receiveDetailVO.setUnitRate(receiveDetail.getUnitRate());
			receiveDetailVO.setDescription(receiveDetail.getDescription());
			receiveDetailVO.setInvoiceQty(receiveDetail.getReceiveQty());
			receiveDetailVO
					.setInvoiceDetails(receiveDetail.getInvoiceDetails());
			receiveDetails.add(receiveDetailVO);
			List<ReceiveDetailVO> receiveDetailVOList = receiveDetailVOHash
					.get(receiveVO.getReceiveId());
			if (null != receiveDetailVOList && receiveDetailVOList.size() > 0) {
				receiveDetailVOList.add(receiveDetailVO);
				receiveDetailVOHash.put(receiveVO.getReceiveId(),
						receiveDetailVOList);
			} else {
				receiveDetailVOList = new ArrayList<ReceiveDetailVO>();
				receiveDetailVOList.add(receiveDetailVO);
				receiveDetailVOHash.put(receiveVO.getReceiveId(),
						receiveDetailVOList);
			}
			/*
			 * invoiceDetailVOList = new ArrayList<InvoiceDetailVO>(); for
			 * (InvoiceDetail invoiceDetail : receiveDetail
			 * .getInvoiceDetails()) { invoiceDetailVO = new InvoiceDetailVO();
			 * invoiceDetailVO.setInvoiceDetailId(invoiceDetail
			 * .getInvoiceDetailId());
			 * invoiceDetailVO.setInvoiceQty(invoiceDetail.getInvoiceQty());
			 * invoiceDetailVO.setDescription(invoiceDetail.getDescription());
			 * invoiceDetailVOList.add(invoiceDetailVO); } if
			 * (invoiceVOHash.containsKey(receiveDetail.getReceiveDetailId())) {
			 * invoiceDetailVOList.addAll(invoiceVOHash.get(receiveDetail
			 * .getReceiveDetailId())); }
			 * invoiceVOHash.put(receiveDetail.getReceiveDetailId(),
			 * invoiceDetailVOList);
			 */
			if (receiveDetail.getInvoiceDetails() != null
					&& receiveDetail.getInvoiceDetails().size() > 0)
				receiveVO.setEditFlag(true);
			else
				receiveVO.setEditFlag(false);

			receiveVOHash.put(receiveVO.getReceiveId(), receiveVO);
		}
		for (Entry<Long, List<ReceiveDetailVO>> receiveId : receiveDetailVOHash
				.entrySet()) {
			List<ReceiveDetailVO> receiveDetailVOList = receiveDetailVOHash
					.get(receiveId.getKey());
			invoiceDetailVOList = new ArrayList<InvoiceDetailVO>();
			for (ReceiveDetailVO detailVO : receiveDetailVOList) {
				if (invoiceVOHash.containsKey(detailVO.getReceiveDetailId())) {
					List<InvoiceDetailVO> invoiceDetails = invoiceVOHash
							.get(detailVO.getReceiveDetailId());
					for (InvoiceDetailVO invoiceDetail : invoiceDetails) {
						detailVO.setInvoiceDetailId(invoiceDetail
								.getInvoiceDetailId());
						detailVO.setInvoicedQty(invoiceDetail.getInvoiceQty()
								+ (null != detailVO.getInvoicedQty() ? detailVO
										.getInvoicedQty() : 0));
						detailVO.setInvoiceQty(detailVO.getReceiveQty()
								- detailVO.getInvoicedQty());
						detailVO.setReceivedQty(detailVO.getReceiveQty()
								+ (null != detailVO.getReceivedQty() ? detailVO
										.getReceivedQty() : 0));
						detailVO.setTotalRate(detailVO.getReceivedQty()
								* detailVO.getUnitRate());
						detailVO.setDescription(invoiceDetail.getDescription());
					}
					if (invoiceVOHash
							.containsKey(detailVO.getReceiveDetailId())) {
						invoiceDetailVOList.addAll(invoiceVOHash.get(detailVO
								.getReceiveDetailId()));
					}
					invoiceVOHash.put(detailVO.getReceiveDetailId(),
							invoiceDetails);
				}
			}
			if (receiveVOHash.containsKey(receiveId.getKey())) {
				receiveVO = receiveVOHash.get(receiveId.getKey());
				receiveVO.setReceiveDetailsVO(receiveDetailVOList);
				for (ReceiveDetailVO receiveDetail : receiveVO
						.getReceiveDetailsVO()) {
					receiveVO
							.setTotalReceiveQty(receiveDetail.getReceiveQty()
									+ (null != receiveVO.getTotalReceiveQty() ? receiveVO
											.getTotalReceiveQty() : 0));
					receiveVO
							.setTotalReceiveAmount(receiveDetail
									.getReceiveQty()
									* receiveDetail.getUnitRate()
									+ (null != receiveVO
											.getTotalReceiveAmount() ? receiveVO
											.getTotalReceiveAmount() : 0));
				}
				receiveVOHash.put(receiveId.getKey(), receiveVO);
			}
		}
		return new ArrayList<ReceiveVO>(receiveVOHash.values());
	}

	// Validate Receive Details
	public ReceiveVO convertReceiveToVO(Receive receive) throws Exception {
		ReceiveVO receiveVO = new ReceiveVO();
		BeanUtils.copyProperties(receive, receiveVO);
		receiveVO
				.setSupplierName(null != receive.getSupplier().getPerson() ? receive
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat("")
						.concat(receive.getSupplier().getPerson().getLastName())
						: (null != receive.getSupplier().getCompany() ? receive
								.getSupplier().getCompany().getCompanyName()
								: receive.getSupplier().getCmpDeptLocation()
										.getCompany().getCompanyName()));

		ReceiveDetailVO receiveDetailVO = null;
		List<ReceiveDetailVO> receiveDetails = new ArrayList<ReceiveDetailVO>();
		Set<String> purchaseNoSet = new HashSet<String>();
		Set<String> requisitionNoSet = new HashSet<String>();
		double netAmount = 0.0;
		for (ReceiveDetail receiveDetail : receive.getReceiveDetails()) {
			receiveDetailVO = new ReceiveDetailVO();
			BeanUtils.copyProperties(receiveDetail, receiveDetailVO);
			receiveDetailVO
					.setInvoiceDetails(receiveDetail.getInvoiceDetails());
			receiveDetailVO.setReceiveQty(null != receiveDetail
					.getPackageUnit() ? receiveDetail.getPackageUnit()
					: receiveDetail.getReceiveQty());
			receiveDetailVO.setTotalRate(receiveDetail.getUnitRate()
					* receiveDetailVO.getReceiveQty());
			receiveDetailVO.setDisplayTotalAmount(AIOSCommons
					.formatAmount(receiveDetailVO.getTotalRate())); 
			receiveDetailVO.setUnitCodeStr(null != receiveDetail
					.getProductPackageDetail() ? receiveDetail
					.getProductPackageDetail().getLookupDetail()
					.getDisplayName() : receiveDetail.getProduct()
					.getLookupDetailByProductUnit().getDisplayName());
			if ((double) receiveDetail.getUnitRate() > 1)
				receiveDetailVO.setDisplayUnitRate(AIOSCommons
						.formatAmount(receiveDetail.getUnitRate()));
			else
				receiveDetailVO.setDisplayUnitRate(String.valueOf(receiveDetail
						.getUnitRate()));
			receiveDetailVO.setDisplayTotalAmount(AIOSCommons
					.formatAmount(receiveDetailVO.getTotalRate()));
			netAmount += receiveDetailVO.getTotalRate();
			purchaseNoSet.add(receiveDetail.getPurchase().getPurchaseNumber());
			if (receiveDetail.getPurchase().getRequisition() != null)
				requisitionNoSet.add(receiveDetail.getPurchase()
						.getRequisition().getReferenceNumber());

			if (receiveDetail.getPurchase().getPurchaseDetails() != null
					&& receiveDetail.getPurchase().getPurchaseDetails().size() > 0) {
				for (PurchaseDetail det : receiveDetail.getPurchase()
						.getPurchaseDetails()) {
					if (det.getProduct() != null
							&& (long) det.getProduct().getProductId() == (long) receiveDetail
									.getProduct().getProductId())
						receiveDetailVO.setOrderedQty(det.getQuantity());
					else
						continue;

				}
			}

			receiveDetailVO.setPendingQuantity(receiveDetailVO.getOrderedQty()
					- receiveDetail.getReceiveQty());
			receiveDetails.add(receiveDetailVO);
		}
		receiveVO.setReceiveDetailsVO(receiveDetails);
		StringBuilder purchaseNumber = new StringBuilder();
		for (String purchaseNo : purchaseNoSet)
			purchaseNumber.append(purchaseNo).append(" ,");
		receiveVO.setPurchaseNumber(purchaseNumber.toString());

		StringBuilder requistionNumber = new StringBuilder();
		for (String reqNo : requisitionNoSet)
			requistionNumber.append(reqNo).append(" ,");
		receiveVO.setRequisitionNumber(requistionNumber.toString());

		String decimalAmount = String.valueOf(AIOSCommons
				.formatAmount(netAmount));
		decimalAmount = decimalAmount
				.substring(decimalAmount.lastIndexOf('.') + 1);
		String amountInWords = AIOSCommons.convertAmountToWords(netAmount);
		if (Double.parseDouble(decimalAmount) > 0) {
			amountInWords = amountInWords.concat(" and ")
					.concat(AIOSCommons.convertAmountToWords(decimalAmount))
					.concat(" Fils ");
			String replaceWord = "Only";
			amountInWords = amountInWords.replaceAll(replaceWord, "");
			amountInWords = amountInWords.concat(" " + replaceWord);
		}
		receiveVO.setTotalReceiveAmount(AIOSCommons
				.formatAmountToDouble(netAmount));
		receiveVO.setDisplayTotalAmount(AIOSCommons.formatAmount(netAmount));
		receiveVO.setDisplayTotalAmountInWords(amountInWords);
		return receiveVO;
	}

	public void updateReceiveDetailPO() throws Exception {
		List<ReceiveDetail> receiveDetails = receiveService
				.getAllReceiveDetailWithoutImpl();
		if (null != receiveDetails && receiveDetails.size() > 0) {
			for (ReceiveDetail receiveDetail : receiveDetails) {
				for (PurchaseDetail purchaseDetail : receiveDetail
						.getPurchase().getPurchaseDetails()) {
					if ((long) receiveDetail.getProduct().getProductId() == (long) purchaseDetail
							.getProduct().getProductId()) {
						receiveDetail.setPurchaseDetail(purchaseDetail);
						break;
					}
				}
			}
			receiveService.updateReceiveDetails(receiveDetails);
		}
	}

	public static String[] splitArrayValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public ReceiveService getReceiveService() {
		return receiveService;
	}

	public void setReceiveService(ReceiveService receiveService) {
		this.receiveService = receiveService;
	}

	public Stock getStockDetails() {
		return stockDetails;
	}

	public void setStockDetails(Stock stockDetails) {
		this.stockDetails = stockDetails;
	}

	public List<Stock> getStockList() {
		return stockList;
	}

	public void setStockList(List<Stock> stockList) {
		this.stockList = stockList;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}

	public GoodsReturnBL getGoodsReturnBL() {
		return goodsReturnBL;
	}

	public void setGoodsReturnBL(GoodsReturnBL goodsReturnBL) {
		this.goodsReturnBL = goodsReturnBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}
}
