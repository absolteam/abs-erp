/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.IssueReturn;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class IssueReturnService {

	private AIOTechGenericDAO<IssueReturn> issueReturnDAO;
	private AIOTechGenericDAO<IssueReturnDetail> issueReturnDetailDAO;

	// Get all Issue Returns
	public List<IssueReturn> getAllIssueReturn(Implementation implementation)
			throws Exception {
		return issueReturnDAO.findByNamedQuery("getAllIssueReturn",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Issue Return by Id
	public IssueReturn getIssueReturnById(long issueReturnId) throws Exception {
		return issueReturnDAO.findByNamedQuery("getIssueReturnById",
				issueReturnId).get(0);
	}

	// Get Issue Return by Id
	public IssueReturn getIssueReturns(long issueReturnId) throws Exception {
		return issueReturnDAO
				.findByNamedQuery("getIssueReturns", issueReturnId).get(0);
	}

	// Get Issue Return Detail by Requisition Detail Id
	public IssueReturnDetail getIssueReturnDetailByRequistionDetailId(
			long issueReturnDetailId) throws Exception {
		return issueReturnDetailDAO.findById(issueReturnDetailId); 
	}

	public void evict(IssueReturnDetail issueReturnDetail)
			throws Exception {
		issueReturnDetailDAO.evict(issueReturnDetail);
	}

	// Save Issue Returns
	public void saveIssueReturns(IssueReturn issueReturn,
			List<IssueReturnDetail> issueReturnDetails) throws Exception {
		issueReturnDAO.saveOrUpdate(issueReturn);
		for (IssueReturnDetail issueReturnDetail : issueReturnDetails) {
			issueReturnDetail.setIssueReturn(issueReturn);
		}
		issueReturnDetailDAO.saveOrUpdateAll(issueReturnDetails);
	}

	// Delete Issue Returns
	public void deleteIssueReturns(IssueReturn issueReturn,
			List<IssueReturnDetail> issueReturnDetails) throws Exception {
		issueReturnDetailDAO.deleteAll(issueReturnDetails);
		issueReturnDAO.delete(issueReturn);
	}
	
	@SuppressWarnings("unchecked")
	public List<IssueReturn> getFilteredIssueReturn(Implementation implementation, Long issueReturnId, Long locationId, Date fromDate, Date toDate)
			throws Exception {
		
		Criteria criteria = issueReturnDAO.createCriteria();
		
		criteria.createAlias("cmpDeptLocation", "cdl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.department", "dpt", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.location", "loc", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("issueReturnDetails", "ird", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ird.issueRequistionDetail", "detail", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ird.lookupDetail", "ld", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.issueRequistion", "req", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ird.product", "prd", CriteriaSpecification.LEFT_JOIN);
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		if (issueReturnId != null && issueReturnId > 0) {
			criteria.add(Restrictions.eq("issueReturnId", issueReturnId));
		}
		
		if (locationId != null && locationId > 0) {
			criteria.add(Restrictions.eq("loc.locationId", locationId));
		}
		
		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("returnDate",fromDate, toDate));
		}
		
		return criteria.list();
	}

	// Getters and setters
	public AIOTechGenericDAO<IssueReturn> getIssueReturnDAO() {
		return issueReturnDAO;
	}

	public void setIssueReturnDAO(AIOTechGenericDAO<IssueReturn> issueReturnDAO) {
		this.issueReturnDAO = issueReturnDAO;
	}

	public AIOTechGenericDAO<IssueReturnDetail> getIssueReturnDetailDAO() {
		return issueReturnDetailDAO;
	}

	public void setIssueReturnDetailDAO(
			AIOTechGenericDAO<IssueReturnDetail> issueReturnDetailDAO) {
		this.issueReturnDetailDAO = issueReturnDetailDAO;
	}
}
