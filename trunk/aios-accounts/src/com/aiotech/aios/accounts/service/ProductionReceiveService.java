package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.ProductionReceive;
import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductionReceiveService {

	private AIOTechGenericDAO<ProductionReceive> productionReceiveDAO;
	private AIOTechGenericDAO<ProductionReceiveDetail> productionReceiveDetailDAO;

	// Get all Production Receive
	public List<ProductionReceive> getAllProductionReceive(
			Implementation implementation) throws Exception {
		return productionReceiveDAO.findByNamedQuery("getAllProductionReceive",
				implementation);
	}
	
	// Get Production Receive by id
	public ProductionReceive getAllProductionReceiveById(
			long productionReceiveId) throws Exception {
		return productionReceiveDAO.findByNamedQuery("getAllProductionReceiveById",
				productionReceiveId).get(0);
	}
	
	// Get Production Receive by id
	public ProductionReceive getProductionReceiveById(
			long productionReceiveId) throws Exception {
		return productionReceiveDAO.findByNamedQuery("getProductionReceiveById",
				productionReceiveId).get(0);
	}
	
	// Save Production Receive
	public void saveProductionReceive(ProductionReceive productionReceive,
			List<ProductionReceiveDetail> productionReceiveDetails) throws Exception {
		productionReceiveDAO.saveOrUpdate(productionReceive);
		for (ProductionReceiveDetail productionReceiveDetail : productionReceiveDetails) 
			productionReceiveDetail.setProductionReceive(productionReceive);
		productionReceiveDetailDAO.saveOrUpdateAll(productionReceiveDetails);
	} 
	
	// Save Production Receive
	public void saveProductionReceive(ProductionReceive productionReceive,
			List<ProductionReceiveDetail> productionReceiveDetails, Session session) throws Exception {
		session.saveOrUpdate(productionReceive);
		for (ProductionReceiveDetail productionReceiveDetail : productionReceiveDetails) {
			productionReceiveDetail.setProductionReceive(productionReceive);
			session.saveOrUpdate(productionReceiveDetail);
		}
 	} 

	public void deleteProductionReceive(ProductionReceive productionReceive,
			List<ProductionReceiveDetail> productionReceiveDetails) throws Exception {
		productionReceiveDetailDAO.deleteAll(productionReceiveDetails);
		productionReceiveDAO.delete(productionReceive); 
	} 
	
	@SuppressWarnings("unchecked")
	public List<ProductionReceive> getFilteredProductionReceive(
			Implementation implementation) throws Exception {
		Criteria criteria = productionReceiveDAO.createCriteria();
		
		criteria.createAlias("personByReceivePerson", "prs", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetail", "ld", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("productionReceiveDetails", "detail", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productUnit", "unit", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.shelf", "rack", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.shelf", "shelf", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelf.aisle", "aisle", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("aisle.store", "store", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("aisle.lookupDetail", "a_type", CriteriaSpecification.LEFT_JOIN);
		
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		return criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ProductionReceiveDetail> getProductionReceiveDetailsByPeriod(
			Implementation implementation,Date fromDate,Date toDate,Long storeId) throws Exception {
		Criteria criteria = productionReceiveDetailDAO.createCriteria();
		
		criteria.createAlias("productionReceive", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pr.lookupDetail", "ld", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("product", "prd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productUnit", "unit", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelf", "rack", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.shelf", "shelf", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shelf.aisle", "aisle", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("aisle.store", "store", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("aisle.lookupDetail", "a_type", CriteriaSpecification.LEFT_JOIN);
		
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("pr.implementation", implementation));
		}
		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("pr.receiveDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("pr.receiveDate", DateFormat.getToDate(toDate))));
		}
		if (storeId!=null && storeId>0) {
			criteria.add(Restrictions.eq("store.storeId", storeId));
		}
		return criteria.list();
	}
	
	// Getters & Setters
	public AIOTechGenericDAO<ProductionReceive> getProductionReceiveDAO() {
		return productionReceiveDAO;
	}

	public void setProductionReceiveDAO(
			AIOTechGenericDAO<ProductionReceive> productionReceiveDAO) {
		this.productionReceiveDAO = productionReceiveDAO;
	}

	public AIOTechGenericDAO<ProductionReceiveDetail> getProductionReceiveDetailDAO() {
		return productionReceiveDetailDAO;
	}

	public void setProductionReceiveDetailDAO(
			AIOTechGenericDAO<ProductionReceiveDetail> productionReceiveDetailDAO) {
		this.productionReceiveDetailDAO = productionReceiveDetailDAO;
	} 
}
