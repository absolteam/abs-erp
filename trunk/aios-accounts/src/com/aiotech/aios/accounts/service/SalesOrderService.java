/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderCharge;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class SalesOrderService {

	private AIOTechGenericDAO<SalesOrder> salesOrderDAO;
	private AIOTechGenericDAO<SalesOrderCharge> salesOrderChargeDAO;
	private AIOTechGenericDAO<SalesOrderDetail> salesOrderDetailDAO;

	// Get all sales Order
	public List<SalesOrder> getAllSalesOrder(Implementation implementation)
			throws Exception {
		return salesOrderDAO.findByNamedQuery("getAllSalesOrder",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get all sales Order
	public List<SalesOrder> getUnDeliveredSalesOrder(
			Implementation implementation) throws Exception {
		return salesOrderDAO.findByNamedQuery("getUnDeliveredSalesOrder",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get sales order by sales order id
	public SalesOrder getSalesOrder(long salesOrderId) throws Exception {
		return salesOrderDAO.findByNamedQuery("getSalesOrder", salesOrderId)
				.get(0);
	}

	public SalesOrder getBasicSalesOrder(long salesOrderId) throws Exception {
		return salesOrderDAO.findByNamedQuery("getBasicSalesOrder", salesOrderId)
				.get(0);
	}
	
	// Get sales order info by sales order id
	public SalesOrder getSalesOrderInfo(long salesOrderId) throws Exception {
		return salesOrderDAO
				.findByNamedQuery("getSalesOrderInfo", salesOrderId).get(0);
	}

	// Get sales order info by sales order id
	public SalesOrder findSalesOrderById(long salesOrderId) throws Exception {
		return salesOrderDAO.findById(salesOrderId);
	}

	// Get Sales Order Details by sales order id
	public List<SalesOrderDetail> getSalesOrderDetailBySalesOrderId(
			long salesOrderId) throws Exception {
		return salesOrderDetailDAO.findByNamedQuery(
				"getSalesOrderDetailBySalesOrderId", salesOrderId);
	}

	// Get Sales Order Charges by sales order id
	public List<SalesOrderCharge> getSalesOrderChargesBySalesOrderId(
			long salesOrderId) throws Exception {
		return salesOrderChargeDAO.findByNamedQuery(
				"getSalesOrderChargesBySalesOrderId", salesOrderId);
	}

	// Delete Sales Order Details
	public void deleteSalesOrderDetails(
			List<SalesOrderDetail> deleteOrderDetails) throws Exception {
		salesOrderDetailDAO.deleteAll(deleteOrderDetails);
	}

	// Delete Sales Order Charges
	public void deleteSalesOrderCharges(
			List<SalesOrderCharge> deleteOrderCharges) throws Exception {
		salesOrderChargeDAO.deleteAll(deleteOrderCharges);
	}

	// Save Sales Order Details
	public void saveSalesOrder(SalesOrder salesOrder,
			List<SalesOrderDetail> salesOrderDetails,
			List<SalesOrderCharge> salesOrderCharges) throws Exception {
		salesOrderDAO.saveOrUpdate(salesOrder);
		for (SalesOrderDetail salesOrderDetail : salesOrderDetails) {
			salesOrderDetail.setSalesOrder(salesOrder);
		}
		salesOrderDetailDAO.saveOrUpdateAll(salesOrderDetails);
		if (null != salesOrderCharges && salesOrderCharges.size() > 0) {
			for (SalesOrderCharge salesOrderCharge : salesOrderCharges) {
				salesOrderCharge.setSalesOrder(salesOrder);
			}
			salesOrderChargeDAO.saveOrUpdateAll(salesOrderCharges);
		}
	}

	// Save Sales Order Details
	public void saveSalesOrder(SalesOrder salesOrder) throws Exception {
		salesOrderDAO.saveOrUpdate(salesOrder);
	}

	// Delete Sales Order
	public void deleteSalesOrder(SalesOrder salesOrder,
			List<SalesOrderDetail> salesOrderDetails,
			List<SalesOrderCharge> salesOrderCharges) throws Exception {
		salesOrderDetailDAO.deleteAll(salesOrderDetails);
		if (null != salesOrderCharges && salesOrderCharges.size() > 0)
			salesOrderChargeDAO.deleteAll(salesOrderCharges);
		salesOrderDAO.delete(salesOrder);
	}

	@SuppressWarnings("unchecked")
	public List<SalesOrder> getFilteredSalesOrder(
			Implementation implementation, Long salesOrderId, Long customerId,
			Byte statusId, Date fromDate, Date toDate) throws Exception {
		Criteria criteria = salesOrderDAO.createCriteria();

		criteria.createAlias("customer", "cs", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cs.personByPersonId", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cs.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cs.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByOrderType", "ot",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByShippingTerm", "st",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByShippingMethod", "sm",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("shippingDetail", "sd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesOrderDetails", "sod",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sod.product", "prod",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sod.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesOrderCharges", "soc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("soc.lookupDetail", "ct",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (salesOrderId != null && salesOrderId > 0) {
			criteria.add(Restrictions.eq("salesOrderId", salesOrderId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("cs.customerId", customerId));
		}

		if (statusId != null && statusId > 0) {
			criteria.add(Restrictions.eq("orderStatus", statusId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("orderDate", fromDate, toDate));
		}

		return criteria.list();
	}

	// Get sales Order by project
	public SalesOrder getSalesOrderByProject(Long projectId) throws Exception {
		List<SalesOrder> orders = salesOrderDAO.findByNamedQuery(
				"getSalesOrderByProject", projectId);
		if (orders != null && orders.size() > 0) {
			return orders.get(0);
		} else {
			return null;
		}
	}

	// Getters and Setters
	public AIOTechGenericDAO<SalesOrder> getSalesOrderDAO() {
		return salesOrderDAO;
	}

	public void setSalesOrderDAO(AIOTechGenericDAO<SalesOrder> salesOrderDAO) {
		this.salesOrderDAO = salesOrderDAO;
	}

	public AIOTechGenericDAO<SalesOrderCharge> getSalesOrderChargeDAO() {
		return salesOrderChargeDAO;
	}

	public void setSalesOrderChargeDAO(
			AIOTechGenericDAO<SalesOrderCharge> salesOrderChargeDAO) {
		this.salesOrderChargeDAO = salesOrderChargeDAO;
	}

	public AIOTechGenericDAO<SalesOrderDetail> getSalesOrderDetailDAO() {
		return salesOrderDetailDAO;
	}

	public void setSalesOrderDetailDAO(
			AIOTechGenericDAO<SalesOrderDetail> salesOrderDetailDAO) {
		this.salesOrderDetailDAO = salesOrderDetailDAO;
	}
}
