package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.LoanDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LoanService {
	private AIOTechGenericDAO<Loan> loanDAO;
	private AIOTechGenericDAO<LoanDetail> loanDetailDAO;
	private AIOTechGenericDAO<LoanCharge> loanChargeDAO;
	public List<Loan> getAllLoans(Implementation implementaion) throws Exception{
		return loanDAO.findByNamedQuery("getAllLoans", implementaion);
	}
	
	public Loan getLoanById(long loanId) throws Exception{
		return loanDAO.findByNamedQuery("getLoanDetails", loanId).get(0);
	}
	
	 
	public List<LoanDetail> getLoanDetailsById(Long loanId) throws Exception{ 
		return loanDetailDAO.findByNamedQuery("getLoanDetailsById", loanId);
	}
	
	public List<LoanCharge> getLoanChargesByValue(Long loanId) throws Exception{ 
		return loanChargeDAO.findByNamedQuery("getLoanChargesByValue", loanId);
	}
	
	public List<LoanCharge> getLoanChargesAllById(Long loanId) throws Exception{ 
		return loanChargeDAO.findByNamedQuery("getLoanChargesAllById", loanId);
	}
	
	public List<LoanCharge> getLoanChargesByAll(Long loanId) throws Exception{
		return loanChargeDAO.findByNamedQuery("getLoanChargesByAll", loanId);
	}
	
	public List<LoanCharge> getLoanChargesById(Long loanId) throws Exception{
		return loanChargeDAO.findByNamedQuery("getLoanChargesById", loanId);
	}
	
	public List<LoanCharge> getLoanChargePayments(Long loanId) throws Exception{
		return loanChargeDAO.findByNamedQuery("getLoanChargePayments", loanId);
	}
	
	public LoanCharge getLoanCharges(Long chargeId) throws Exception{
		return loanChargeDAO.findByNamedQuery("getLoanChargesWithLoan",chargeId).get(0);
	}
	
	public void deleteLoan(Loan loan, List<LoanDetail> loanDetail, List<LoanCharge> loanCharge) throws Exception{
		loanDetailDAO.deleteAll(loanDetail);
		loanChargeDAO.deleteAll(loanCharge);
		loanDAO.delete(loan);
	}
	
	public void saveOrUpdate(Loan loan, List<LoanDetail> loanDetail, List<LoanCharge> chargesDetail)throws Exception{
		loanDAO.saveOrUpdate(loan);
		for(LoanDetail detail : loanDetail){
			detail.setLoan(loan);
		}
		for(LoanCharge list : chargesDetail){
			list.setLoan(loan);
		}
		loanDetailDAO.saveOrUpdateAll(loanDetail);
		loanChargeDAO.saveOrUpdateAll(chargesDetail);
	}
	
	public void updateLoanCharges(List<LoanCharge> loanChargesList)throws Exception{
		loanChargeDAO.saveOrUpdateAll(loanChargesList);
	}
	
	public List<Loan>getLoanDetailsByAccountId(long accountId) throws Exception{
		return loanDAO.findByNamedQuery("getLoanByAccountId", accountId);
	}
	
	public List<Loan> getLoanDetails(Implementation implementation) throws Exception{
		return loanDAO.findByNamedQuery("getLoans", implementation);
	}
	
	public AIOTechGenericDAO<Loan> getLoanDAO() {
		return loanDAO;
	}
	public void setLoanDAO(AIOTechGenericDAO<Loan> loanDAO) {
		this.loanDAO = loanDAO;
	}

	public AIOTechGenericDAO<LoanDetail> getLoanDetailDAO() {
		return loanDetailDAO;
	}

	public void setLoanDetailDAO(AIOTechGenericDAO<LoanDetail> loanDetailDAO) {
		this.loanDetailDAO = loanDetailDAO;
	}

	public AIOTechGenericDAO<LoanCharge> getLoanChargeDAO() {
		return loanChargeDAO;
	}

	public void setLoanChargeDAO(AIOTechGenericDAO<LoanCharge> loanChargeDAO) {
		this.loanChargeDAO = loanChargeDAO;
	}
}
