package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.GoodsReturnService;
import com.aiotech.aios.accounts.service.PurchaseService;
import com.aiotech.aios.accounts.service.ReceiveService;
import com.aiotech.aios.accounts.service.SupplierService;
import com.aiotech.aios.accounts.to.SupplierTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.POStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PurchaseBL {

	private List<Purchase> purchaseList = null;
	private PurchaseService purchaseService = null;
	private CurrencyService currencyService = null;
	private QuotationBL quotationBL = null;
	private ProductBL productBL;
	private SupplierService supplierService;
	private ReceiveService receiveService;
	private GoodsReturnService goodsReturnService;
	private CreditTermBL creditTermBL;
	private LookupMasterBL lookupMasterBL;
	private TransactionBL transactionBL;
	private StockBL stockBL;
	private AlertBL alertBL;
	private DirectPaymentBL directPaymentBL;
	private PackageConversionBL packageConversionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;

	public JSONObject getPurchaselist(Implementation implementation)
			throws Exception {

		List<Purchase> purchaseList = getPurchaseService().getPurchaseOrders(
				implementation);

		if (null != purchaseList && !purchaseList.equals("")) {
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			double totalPurchase = 0;
			for (Purchase list : purchaseList) {
				totalPurchase = 0;
				array = new JSONArray();
				array.add(list.getPurchaseId());
				array.add(list.getPurchaseNumber());
				array.add(list.getCurrency().getCurrencyPool().getCode());
				List<PurchaseDetail> purchaseDetail = new ArrayList<PurchaseDetail>(
						list.getPurchaseDetails());
				for (PurchaseDetail list2 : purchaseDetail) {
					totalPurchase += list2.getUnitRate() * list2.getQuantity();
				}
				array.add(AIOSCommons.formatAmount(totalPurchase));
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(DateFormat.convertDateToString(list.getExpiryDate()
						.toString()));
				if (list.getSupplier() != null)
					array.add(list.getSupplier().getSupplierNumber());
				else
					array.add("");

				if (null != list.getSupplier()
						&& null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson())) {
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				} else if (null != list.getSupplier()
						&& (null != list.getSupplier().getCmpDeptLocation() || null != list
								.getSupplier().getCompany())) {
					array.add(null != list.getSupplier().getCmpDeptLocation()
							&& !("").equals(list.getSupplier()
									.getCmpDeptLocation()) ? list.getSupplier()
							.getCmpDeptLocation().getCompany().getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				} else {
					array.add("");
				}
				array.add(null != list.getCreditTerm() ? list.getCreditTerm()
						.getName() : "");
				if ((list.getStatus() != null && (int) list.getStatus() == (int) POStatus.Received
						.getCode())
						|| list.getReceiveDetails() != null
						&& list.getReceiveDetails().size() > 0) {
					array.add(true);
					array.add("Received");
				} else {
					array.add(false);
					array.add("Pending");
				}

				// receive details
				Set<String> receiveNoSet = new HashSet<String>();
				for (ReceiveDetail pdetail : list.getReceiveDetails()) {
					receiveNoSet.add(pdetail.getReceive().getReceiveNumber());
				}
				StringBuilder receiveNumber = new StringBuilder();
				for (String purchaseNo : receiveNoSet)
					receiveNumber.append(purchaseNo).append(" ,");
				array.add(receiveNumber.toString());

				// Requisition details
				if (list.getRequisition() != null)
					array.add(list.getRequisition().getReferenceNumber());
				else
					array.add("");

				array.add(null != list.getPersonByCreatedBy() ? list
						.getPersonByCreatedBy().getFirstName()
						+ " "
						+ list.getPersonByCreatedBy().getLastName() : "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			return jsonResponse;
		} else {
			return null;
		}
	}

	public List<String> validatePurchaseOrder(String purchaseDetails)
			throws Exception {
		JSONParser parser = new JSONParser();
		Object purchaseDetailObject = parser.parse(purchaseDetails);
		org.json.simple.JSONArray object = (org.json.simple.JSONArray) purchaseDetailObject;
		Map<String, Object> purchaseDetailMap = new HashMap<String, Object>();
		List<String> productDuplicates = new ArrayList<String>();
		String key = "";
		for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) iterator
					.next();
			org.json.simple.JSONArray detailArray = (org.json.simple.JSONArray) jsonObject
					.get("purchsaeDetailJS");
			org.json.simple.JSONObject jsonObjectDetail = (org.json.simple.JSONObject) detailArray
					.get(0);

			key = jsonObjectDetail
					.get("productId")
					.toString()
					.concat(null != jsonObjectDetail.get("batchNumber") ? jsonObjectDetail
							.get("batchNumber").toString() : ""
							.concat(null != jsonObjectDetail
									.get("productExpiry") ? jsonObjectDetail
									.get("productExpiry").toString() : ""));

			if (purchaseDetailMap.containsKey(key)) {
				productDuplicates.add(jsonObjectDetail.get("productId")
						.toString());
			}
			purchaseDetailMap.put(key, jsonObjectDetail);
		}
		return productDuplicates;
	}

	// Save Purchase order
	public void updatePurchaseOrder(List<Purchase> purchaseDetails)
			throws Exception {
		purchaseService.savePurchaseOrder(purchaseDetails);
	}

	// Save Purchase order
	public void savePurchaseOrder(Purchase purchase,
			List<PurchaseDetail> purchaseDetails,
			List<PurchaseDetail> deletePurchaseDetails, List<Stock> stocks,
			String unifiedPrice, Requisition requisition) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		boolean editFlag = false;
		purchase.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (purchase != null && purchase.getPurchaseId() != null
				&& purchase.getPurchaseId() > 0) {
			editFlag = true;
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		if (null != deletePurchaseDetails && deletePurchaseDetails.size() > 0)
			purchaseService.deletePurchaseDetail(deletePurchaseDetails);
		if (null == purchase.getPurchaseId()) {
			SystemBL.saveReferenceStamp(Purchase.class.getName(),
					purchase.getImplementation());
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			purchase.setPersonByCreatedBy(person);
			purchase.setCreatedDate(Calendar.getInstance().getTime());
		}
		if (null != stocks && stocks.size() > 0)
			stockBL.updateStockDetails(null, stocks);
		if (null != requisition)
			quotationBL.getRequisitionService().saveRequisition(requisition);
		purchaseService.savePurchaseOrder(purchase, purchaseDetails);
		saveDocuments(purchase, editFlag);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Purchase.class.getSimpleName(), purchase.getPurchaseId(), user,
				workflowDetailVo);

	}

	// Save Purchase order
	public void savePurchaseOrderWithOutSupplier(Purchase purchase,
			List<PurchaseDetail> purchaseDetails,
			List<PurchaseDetail> deletePurchaseDetails,
			List<PurchaseDetail> purchaseStockList, String unifiedPrice,
			Requisition requisition) throws Exception {
		boolean editFlag = false;
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		purchase.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (purchase != null && purchase.getPurchaseId() != null
				&& purchase.getPurchaseId() > 0) {
			editFlag = true;
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		if (null == purchase.getPurchaseId()) {
			SystemBL.saveReferenceStamp(Purchase.class.getName(),
					purchase.getImplementation());
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			purchase.setPersonByCreatedBy(person);
			purchase.setCreatedDate(Calendar.getInstance().getTime());
		} else {
			directPaymentBL.deleteDirectPayment(purchase.getPurchaseId(),
					Purchase.class.getName());
			Stock stock = null;
			List<Stock> deleteStocks = null;
			if (null != purchaseStockList && purchaseStockList.size() > 0) {
				deleteStocks = new ArrayList<Stock>();
				for (PurchaseDetail stockList : purchaseStockList) {
					stock = new Stock();
					stock.setProduct(stockList.getProduct());
					stock.setQuantity(stockList.getQuantity());
					stock.setStore(stockList.getShelf().getShelf().getAisle()
							.getStore());
					stock.setShelf(stockList.getShelf());
					stock.setBatchNumber(stockList.getBatchNumber());
					stock.setProductExpiry(stockList.getProductExpiry());
					deleteStocks.add(stock);
				}
				stockBL.deleteBatchStockDetails(deleteStocks);
			}
			if (null != deletePurchaseDetails
					&& deletePurchaseDetails.size() > 0)
				purchaseService.deletePurchaseDetail(deletePurchaseDetails);
		}
		if (null != requisition)
			quotationBL.getRequisitionService().saveRequisition(requisition);
		purchase.setPurchaseDetails(new HashSet<PurchaseDetail>(purchaseDetails));

		purchaseService.savePurchaseOrder(purchase, purchaseDetails);
		saveDocuments(purchase, editFlag);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Purchase.class.getSimpleName(), purchase.getPurchaseId(), user,
				workflowDetailVo);

	}

	public void saveDocuments(Purchase obj, boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(obj, "purchaseDocs",
				editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, obj.getPurchaseId(), Purchase.class.getSimpleName(),
					"purchaseDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	private List<ProductPricingDetail> getUpdatedProductPricing(
			String unifiedPrice, double basePrice, PurchaseDetail purchaseDetail)
			throws Exception {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>();
		ProductPricingDetail productPricingDetail = null;
		if (unifiedPrice.equals("true")) {
			productPricingDetail = stockBL.getProductPricingBL()
					.getPricingDetail(
							purchaseDetail.getProduct().getProductId());
			if (null != productPricingDetail) {
				productPricingDetail.setBasicCostPrice(basePrice);
				productPricingDetails.add(productPricingDetail);
			}
		} else {
			Shelf shelf = stockBL
					.getStoreBL()
					.getStoreService()
					.getStoreByShelfId(
							purchaseDetail.getProduct().getShelf().getShelfId());
			productPricingDetail = stockBL.getProductPricingBL()
					.getPricingDetailWithProductStore(
							purchaseDetail.getProduct().getProductId(),
							shelf.getAisle().getStore().getStoreId());
			if (null != productPricingDetail) {
				productPricingDetail.setBasicCostPrice(basePrice);
				productPricingDetails.add(productPricingDetail);
			} else {
				ProductPricing productPricing = stockBL
						.getProductPricingBL()
						.getProductPricingService()
						.getProductPricingWithProductAndDate(
								purchaseDetail.getProduct().getProductId(),
								Calendar.getInstance().getTime(),
								Calendar.getInstance().getTime());
				if (null != productPricing) {
					for (ProductPricingDetail productDetail : productPricing
							.getProductPricingDetails()) {
						if (null != productDetail.getStatus()
								&& (boolean) productDetail.getStatus()) {
							productPricingDetail = new ProductPricingDetail();
							productPricingDetail.setProduct(productDetail
									.getProduct());
							productPricingDetail.setStore(shelf.getAisle()
									.getStore());
							productPricingDetail.setBasicCostPrice(basePrice);
							productPricingDetail.setStandardPrice(productDetail
									.getStandardPrice());
							productPricingDetail
									.setSuggestedRetailPrice(productDetail
											.getSuggestedRetailPrice());
							productPricingDetail.setSellingPrice(productDetail
									.getSellingPrice());
							productPricingDetail
									.setProductPricing(productPricing);
							productPricingDetail.setStatus(true);
							productPricingDetails.add(productPricingDetail);
							break;
						}
					}
				}
			}
		}
		return productPricingDetails;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Set<Long> findDuplicates(List<Long> validateId) {
		final Set<Long> returnList = new HashSet();
		final Set<Long> checkList = new HashSet();
		for (Long yourInt : validateId) {
			if (!checkList.add(yourInt)) {
				returnList.add(yourInt);
			}
		}
		return returnList;
	}

	public void purchaseDirectPaymentEntry(PurchaseVO vo) {

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;

			List<Currency> currencyList = null;
			if (vo.getObject() != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplemenationId());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplemenationId());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(vo.getRecordId());
				directPayment.setUseCase(vo.getUseCase());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				Purchase purchase = (Purchase) vo.getObject();
				List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>(
						purchase.getPurchaseDetails());
				DirectPaymentDetail directPaymentDetail = null;
				Collections.sort(purchaseDetails,
						new Comparator<PurchaseDetail>() {
							public int compare(PurchaseDetail p1,
									PurchaseDetail p2) {
								return p1.getPurchaseDetailId().compareTo(
										p2.getPurchaseDetailId());
							}
						});
				for (PurchaseDetail purchaseDetail : purchaseDetails) {
					directPaymentDetail = new DirectPaymentDetail();
					directPaymentDetail.setAmount(purchaseDetail.getQuantity()
							* purchaseDetail.getUnitRate());
					Combination combination = productBL
							.getProductInventory(purchaseDetail.getProduct()
									.getProductId());
					combination = productBL
							.getCombinationBL()
							.getCombinationService()
							.getCombinationAccounts(
									combination.getCombinationId());
					directPaymentDetail.setCombination(combination);
					directPayDetails.add(directPaymentDetail);

				}
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}
			Combination combination = null;
			if (null != directPaymentBL.getImplementationId()
					.getClearingAccount()) {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								directPaymentBL.getImplementationId()
										.getClearingAccount());
			}
			ServletActionContext.getRequest().setAttribute("CASH_ACCOUNTS",
					combination);
			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Get Implementation
	public Implementation getImplemenationId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = new Implementation();
		if (null != session.getAttribute("THIS"))
			implementation = (Implementation) session.getAttribute("THIS");
		return implementation;
	}

	public void deletePurchaseOrder(Purchase purchase,
			List<PurchaseDetail> purchaseDetails) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Purchase.class.getSimpleName(), purchase.getPurchaseId(), user,
				workflowDetailVo);

	}

	public void doPurchaseBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Purchase purchase = purchaseService
				.getPurchaseOrderDetailByPurchaseId(recordId);
		List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>(
				purchase.getPurchaseDetails());
		if (workflowDetailVO.isDeleteFlag()) {
			if (purchase.getSupplier() == null
					|| purchase.getSupplier().getSupplierId() == null) {
				List<Stock> updateStocks = new ArrayList<Stock>();
				Stock tempStock = null;
				for (PurchaseDetail detail : purchaseDetails) {
					tempStock = new Stock();
					tempStock.setProduct(detail.getProduct());
					tempStock.setStore(detail.getShelf().getShelf().getAisle()
							.getStore());
					tempStock.setQuantity(detail.getQuantity());
					tempStock.setShelf(detail.getShelf());
					tempStock.setProductExpiry(detail.getProductExpiry());
					tempStock.setBatchNumber(detail.getBatchNumber());
					updateStocks.add(tempStock);
				}
				stockBL.deleteBatchStockDetails(updateStocks);

				directPaymentBL.deleteDirectPayment(purchase.getPurchaseId(),
						Purchase.class.getName());
			}

			purchaseService.deletePurchaseOrder(purchase, purchaseDetails);
		} else {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			List<ProductPricingDetail> pricingDetails = new ArrayList<ProductPricingDetail>();
			String unifiedPrice = "";
			if (session.getAttribute("unified_price") != null)
				unifiedPrice = session.getAttribute("unified_price").toString();
			for (PurchaseDetail purchaseDetail : purchaseDetails) {
				boolean updateCost = true;
				if (unifiedPrice.equals("false")) {
					if (null == purchaseDetail.getShelf()
							|| null == purchaseDetail.getShelf().getShelfId())
						updateCost = false;
				}
				if (updateCost) {
					List<PurchaseDetail> updatePurchaseDetails = purchaseService
							.getPurchaseProducts(purchaseDetail.getProduct()
									.getProductId());
					double totalRate = 0;
					double totalQty = 0;
					double unitRate = 0;
					List<ProductPricingDetail> productPricingDetails = null;
					for (PurchaseDetail detail : updatePurchaseDetails) {
						totalRate += (detail.getUnitRate() * detail
								.getQuantity());
						totalQty += detail.getQuantity();
					}
					unitRate = AIOSCommons
							.roundDecimals((totalRate / totalQty));

					// Update the cost price
					if (null != unifiedPrice && !("").equals(unifiedPrice)) {
						productPricingDetails = getUpdatedProductPricing(
								unifiedPrice, unitRate, purchaseDetail);

						if (null != productPricingDetails
								&& productPricingDetails.size() > 0)
							pricingDetails.addAll(productPricingDetails);
					}
				}
			}

			if (null != pricingDetails && pricingDetails.size() > 0) {
				List<ProductPricingDetail> updatePricingDetails = new ArrayList<ProductPricingDetail>();
				List<ProductPricingDetail> savePricingDetails = new ArrayList<ProductPricingDetail>();
				for (ProductPricingDetail productPricingDetail : pricingDetails) {
					if (null == productPricingDetail
							.getProductPricingDetailId())
						savePricingDetails.add(productPricingDetail);
					else
						updatePricingDetails.add(productPricingDetail);
				}
				if (null != updatePricingDetails
						&& updatePricingDetails.size() > 0)
					stockBL.getProductPricingBL().getProductPricingService()
							.mergeProductPricingDetail(updatePricingDetails);
				if (null != savePricingDetails && savePricingDetails.size() > 0)
					stockBL.getProductPricingBL().getProductPricingService()
							.saveProductPricingDetail(savePricingDetails);
			}

			if (purchase.getSupplier() == null) {
				// Stock Increase
				Stock stock = null;
				List<Stock> stocks = new ArrayList<Stock>();
				for (PurchaseDetail inventory : purchaseDetails) {
					Shelf shelf = productBL.getStoreBL().getStoreService()
							.getShelfById(inventory.getShelf().getShelfId());
					stock = new Stock();
					stock.setStore(shelf.getAisle().getStore());
					stock.setShelf(shelf);
					stock.setProduct(inventory.getProduct());
					stock.setUnitRate(inventory.getUnitRate());
					stock.setQuantity(inventory.getQuantity());
					stock.setImplementation(getImplemenationId());
					stock.setBatchNumber(inventory.getBatchNumber());
					stock.setProductExpiry(inventory.getProductExpiry());
					stocks.add(stock);
				}
				stockBL.updateBatchStockDetails(stocks);
				// Direct Payment Alert configuration
				alertBL.alertDirectPurchasePaymentProcess(purchase);
				purchase.setStatus((int) POStatus.Received.getCode());
			}
		}
	}

	public List<PurchaseVO> validatePurchaseReceive(
			List<PurchaseDetail> purchaseDetailList) throws Exception {
		PurchaseVO purchaseVO = null;
		PurchaseDetailVO purchaseDetailVO = null;
		Map<Long, PurchaseVO> purchaseVOHash = new HashMap<Long, PurchaseVO>();
		Map<Long, List<PurchaseDetailVO>> purchaseDetailVOHash = new HashMap<Long, List<PurchaseDetailVO>>();
		List<PurchaseDetailVO> purchaseDetailVOs = new ArrayList<PurchaseDetailVO>();
		ReceiveDetailVO receiveDetailVO = null;
		Map<Long, List<ReceiveDetailVO>> receiveVOHash = new HashMap<Long, List<ReceiveDetailVO>>();
		List<ReceiveDetailVO> receiveDetailVOList = null;
		for (PurchaseDetail purchaseDetail : purchaseDetailList) {
			purchaseVO = new PurchaseVO();
			BeanUtils.copyProperties(purchaseVO, purchaseDetail.getPurchase());
			purchaseDetailVO = new PurchaseDetailVO();

			purchaseDetailVO.setPurchaseDetailId(purchaseDetail
					.getPurchaseDetailId());
			purchaseDetailVO.setPurchaseQty(purchaseDetail.getPackageUnit());
			purchaseDetailVO.setPackageUnit(purchaseDetail.getPackageUnit());
			purchaseDetailVO.setReceivedQty(Double.valueOf("0"));
			purchaseDetailVO.setReturnedQty(Double.valueOf("0"));
			purchaseDetailVO.setQuantity(purchaseDetail.getPackageUnit());
			purchaseDetailVO
					.setBatchReceiveQty(purchaseDetail.getPackageUnit());
			purchaseDetailVO.setUnitRate(purchaseDetail.getUnitRate());
			purchaseDetailVO.setProduct(purchaseDetail.getProduct());
			purchaseDetailVO.setExpiryDate(null != purchaseDetail
					.getProductExpiry() ? DateFormat
					.convertDateToString(purchaseDetail.getProductExpiry()
							.toString()) : "");
			purchaseDetailVO.setShelfId(null != purchaseDetail.getProduct()
					.getShelf() ? purchaseDetail.getProduct().getShelf()
					.getShelfId() : null);
			purchaseDetailVO.setStoreName(null != purchaseDetail.getProduct()
					.getShelf() ? purchaseDetail.getProduct().getShelf()
					.getShelf().getAisle().getStore().getStoreName()
					+ ">>"
					+ purchaseDetail.getProduct().getShelf().getShelf()
							.getAisle().getSectionName()
					+ ">>"
					+ purchaseDetail.getProduct().getShelf().getShelf()
							.getName()
					+ ">>"
					+ purchaseDetail.getProduct().getShelf().getName() : null);
			purchaseDetailVO.setDescription(purchaseDetail.getDescription());
			purchaseDetailVO.setBatchNumber(purchaseDetail.getBatchNumber());
			purchaseDetailVO
					.setProductExpiry(purchaseDetail.getProductExpiry());
			purchaseDetailVO.setReceiveDetails(purchaseDetail
					.getReceiveDetails());
			purchaseDetailVO.setBaseUnitName(purchaseDetail.getProduct()
					.getLookupDetailByProductUnit().getDisplayName());
			purchaseDetailVO.setUnitCode(purchaseDetail.getProduct()
					.getLookupDetailByProductUnit().getAccessCode());
			purchaseDetailVO.setProductPackageVOs(packageConversionBL
					.getProductPackagingDetail(purchaseDetail.getProduct()
							.getProductId()));
			purchaseDetailVO.setPackageUnit(purchaseDetail.getPackageUnit());
			if (null != purchaseDetail.getProductPackageDetail()) {
				purchaseDetailVO.setPackageDetailId(purchaseDetail
						.getProductPackageDetail().getProductPackageDetailId());
				purchaseDetailVO.setProductPackageDetailVO(packageConversionBL
						.getBaseConversionUnit(purchaseDetail
								.getProductPackageDetail()
								.getProductPackageDetailId(), purchaseDetailVO
								.getQuantity(), 0));
				purchaseDetailVO
						.setConvertionUnitName(purchaseDetailVO
								.getProductPackageDetailVO()
								.getConversionUnitMeasure());
				purchaseDetailVO.setBaseQuantity(purchaseDetailVO
						.getProductPackageDetailVO().getConversionQuantity());
			}
			double returnedQty = 0;
			if (null != purchaseDetail.getGoodsReturnDetails()
					&& purchaseDetail.getGoodsReturnDetails().size() > 0) {
				purchaseDetailVO.setGoodsReturnDetails(purchaseDetail
						.getGoodsReturnDetails());
				for (GoodsReturnDetail returnDetail : purchaseDetail
						.getGoodsReturnDetails())
					returnedQty += returnDetail.getPackageUnit();
				purchaseDetailVO.setReturnedQty(returnedQty);
			}
			purchaseDetailVOs.add(purchaseDetailVO);

			List<PurchaseDetailVO> purchaseDetailVOList = purchaseDetailVOHash
					.get(purchaseVO.getPurchaseId());

			if (null != purchaseDetailVOList && purchaseDetailVOList.size() > 0) {
				purchaseDetailVOList.add(purchaseDetailVO);
				purchaseDetailVOHash.put(purchaseVO.getPurchaseId(),
						purchaseDetailVOList);
			} else {
				purchaseDetailVOList = new ArrayList<PurchaseDetailVO>();
				purchaseDetailVOList.add(purchaseDetailVO);
				purchaseDetailVOHash.put(purchaseVO.getPurchaseId(),
						purchaseDetailVOList);
			}
			receiveDetailVOList = new ArrayList<ReceiveDetailVO>();
			for (ReceiveDetail receiveDetail : purchaseDetail
					.getReceiveDetails()) {
				receiveDetailVO = new ReceiveDetailVO();
				receiveDetailVO.setReceiveDetailId(receiveDetail
						.getReceiveDetailId());
				receiveDetailVO.setReceiveQty(receiveDetail.getPackageUnit());
				receiveDetailVO.setDescription(receiveDetail.getDescription());
				receiveDetailVO.setBatchNumber(receiveDetail.getBatchNumber());
				receiveDetailVO.setProductExpiry(receiveDetail
						.getProductExpiry());
				returnedQty = 0;
				if (null != receiveDetail.getGoodsReturnDetails()
						&& receiveDetail.getGoodsReturnDetails().size() > 0) {
					for (GoodsReturnDetail returnDetail : receiveDetail
							.getGoodsReturnDetails())
						returnedQty += returnDetail.getPackageUnit();
					receiveDetailVO.setReturnedQty(returnedQty);
				}
				receiveDetailVOList.add(receiveDetailVO);
			}
			if (receiveVOHash.containsKey(purchaseDetail.getPurchaseDetailId())) {
				receiveDetailVOList.addAll(receiveVOHash.get(purchaseDetail
						.getPurchaseDetailId()));
			}
			receiveVOHash.put(purchaseDetail.getPurchaseDetailId(),
					receiveDetailVOList);

			purchaseVOHash.put(purchaseVO.getPurchaseId(), purchaseVO);
		}
		for (Entry<Long, List<PurchaseDetailVO>> purchaseId : purchaseDetailVOHash
				.entrySet()) {
			List<PurchaseDetailVO> purchaseDetailVOList = purchaseDetailVOHash
					.get(purchaseId.getKey());
			receiveDetailVOList = new ArrayList<ReceiveDetailVO>();
			for (PurchaseDetailVO detailVO : purchaseDetailVOList) {
				if (receiveVOHash.containsKey(detailVO.getPurchaseDetailId())) {
					List<ReceiveDetailVO> receiveDetails = receiveVOHash
							.get(detailVO.getPurchaseDetailId());
					for (ReceiveDetailVO receiveDetail : receiveDetails) {
						detailVO.setPurchaseQty(detailVO.getPurchaseQty());
						detailVO.setReceiveDetailId(receiveDetail
								.getReceiveDetailId());
						detailVO.setReceivedQty(receiveDetail.getReceiveQty()
								+ (null != detailVO.getReceivedQty() ? detailVO
										.getReceivedQty() : 0));
						detailVO.setReturnedQty(detailVO.getReturnedQty()
								+ (null != receiveDetail.getReturnedQty() ? receiveDetail
										.getReturnedQty() : 0));
						detailVO.setReceiveQty(detailVO.getPurchaseQty()
								- (detailVO.getReceivedQty() + detailVO
										.getReturnedQty()));
						detailVO.setBatchReceiveQty(detailVO.getReceiveQty());
						detailVO.setQuantity(detailVO.getReceiveQty());
						detailVO.setTotalRate(detailVO.getPurchaseQty()
								* detailVO.getUnitRate());
						detailVO.setDescription(receiveDetail.getDescription());
					}
					if (receiveVOHash.containsKey(detailVO
							.getPurchaseDetailId())) {
						receiveDetailVOList.addAll(receiveVOHash.get(detailVO
								.getPurchaseDetailId()));
					}
					receiveVOHash.put(detailVO.getPurchaseDetailId(),
							receiveDetails);
				}
			}
			if (purchaseVOHash.containsKey(purchaseId.getKey())) {
				purchaseVO = purchaseVOHash.get(purchaseId.getKey());
				purchaseVO.setPurchaseDetailVO(purchaseDetailVOList);
				for (PurchaseDetailVO purchaseDetail : purchaseVO
						.getPurchaseDetailVO()) {
					purchaseVO
							.setTotalPurchaseQty(purchaseDetail
									.getPurchaseQty()
									+ (null != purchaseDetail
											.getTotalPurchaseProduct() ? purchaseDetail
											.getTotalPurchaseProduct() : 0));
					purchaseVO
							.setTotalPurchaseAmount(purchaseDetail
									.getPurchaseQty()
									* purchaseDetail.getUnitRate()
									+ (null != purchaseVO
											.getTotalPurchaseAmount() ? purchaseVO
											.getTotalPurchaseAmount() : 0));
				}
				purchaseVOHash.put(purchaseId.getKey(), purchaseVO);
			}
		}
		return new ArrayList<PurchaseVO>(purchaseVOHash.values());
	}

	public List<PurchaseVO> validatePurchaseReceiveEdit(
			List<Purchase> purchaseList, List<PurchaseVO> vos) throws Exception {
		Map<Long, PurchaseVO> voMaps = new HashMap<Long, PurchaseVO>();
		if (vos != null)
			for (PurchaseVO purchaseVO : vos) {
				voMaps.put(purchaseVO.getPurchaseId(), purchaseVO);
			}
		PurchaseVO purchaseVO = null;
		PurchaseDetailVO purchaseDetailVO = null;
		Map<String, PurchaseDetailVO> productDetails = null;
		String key = "";
		for (Purchase purchase : purchaseList) {
			if (voMaps.containsKey(purchase.getPurchaseId())) {
				purchaseVO = voMaps.get(purchase.getPurchaseId());
				productDetails = new HashMap<String, PurchaseDetailVO>();
				for (PurchaseDetailVO detailVO : purchaseVO
						.getPurchaseDetailVO()) {
					key = (detailVO
							.getProduct()
							.getProductId()
							.toString()
							.concat((null != detailVO.getBatchNumber()
									&& !("").equals(detailVO.getBatchNumber()) ? "-"
									+ detailVO.getBatchNumber()
									: "")).concat(null != detailVO
							.getProductExpiry() ? "-"
							+ detailVO.getProductExpiry() : "")).trim();
					purchaseDetailVO.setExpiryDate(null != detailVO
							.getProductExpiry() ? DateFormat
							.convertDateToString(detailVO.getProductExpiry()
									.toString()) : "");
					productDetails.put(key, detailVO);
				}
				for (ReceiveDetail receives : purchase.getReceiveDetails()) {
					key = (receives
							.getProduct()
							.getProductId()
							.toString()
							.concat((null != receives.getBatchNumber()
									&& !("").equals(receives.getBatchNumber()) ? "-"
									+ receives.getBatchNumber()
									: "")).concat(null != receives
							.getProductExpiry() ? "-"
							+ receives.getProductExpiry() : "")).trim();
					if (productDetails.containsKey(key)) {
						PurchaseDetailVO product = productDetails.get(key);
						if (product.getReceivedQty() != null
								&& product.getReceivedQty() > 0)
							product.setReceivedQty(receives.getReceiveQty()
									+ product.getReceivedQty());
						else
							product.setReceivedQty(receives.getReceiveQty());

						productDetails.put(key, product);
					}
				}
				for (GoodsReturnDetail returns : purchase
						.getGoodsReturnDetails()) {
					key = (returns
							.getProduct()
							.getProductId()
							.toString()
							.concat((null != returns.getBatchNumber()
									&& !("").equals(returns.getBatchNumber()) ? "-"
									+ returns.getBatchNumber()
									: "")).concat(null != returns
							.getProductExpiry() ? "-"
							+ returns.getProductExpiry() : "")).trim();
					if (productDetails.containsKey(key)) {
						PurchaseDetailVO product = productDetails.get(key);
						if (product.getReturnedQty() != null
								&& product.getReturnedQty() > 0)
							product.setReturnedQty(product.getReturnedQty()
									+ returns.getReturnQty());
						else
							product.setReturnedQty(returns.getReturnQty());

						productDetails.put(key, product);
					}
				}
				purchaseVO.setEditFlag(true);
				voMaps.put(purchase.getPurchaseId(), purchaseVO);
			} else {
				purchaseVO = new PurchaseVO();
				purchaseVO.setPurchaseId(purchase.getPurchaseId());
				purchaseVO.setPurchaseNumber(purchase.getPurchaseNumber());
				purchaseVO.setCurrency(purchase.getCurrency());
				purchaseVO.setDate(purchase.getDate());
				purchaseVO.setExpiryDate(purchase.getExpiryDate());
				purchaseVO.setQuotation(purchase.getQuotation());
				purchaseVO.setDescription(purchase.getDescription());
				purchaseVO.setSupplier(purchase.getSupplier());
				purchaseVO.setIsApprove(purchase.getIsApprove());
				purchaseVO.setContinuousPurchase(purchase
						.getContinuousPurchase());
				purchaseVO.setRequisition(purchase.getRequisition());
				double purchaseQty = 0.0;
				double totalUnitRate = 0.0;
				productDetails = new HashMap<String, PurchaseDetailVO>();
				for (PurchaseDetail plist : purchase.getPurchaseDetails()) {
					key = (plist
							.getProduct()
							.getProductId()
							.toString()
							.concat((null != plist.getBatchNumber()
									&& !("").equals(plist.getBatchNumber()) ? "-"
									+ plist.getBatchNumber()
									: "")).concat(null != plist
							.getProductExpiry() ? "-"
							+ plist.getProductExpiry() : "")).trim();
					purchaseDetailVO = new PurchaseDetailVO();
					purchaseDetailVO.setPurchaseDetailId(plist
							.getPurchaseDetailId());
					purchaseDetailVO.setPurchaseQty(plist.getQuantity());
					purchaseDetailVO.setReceivedQty(Double.valueOf("0"));
					purchaseDetailVO.setBatchReceiveQty(plist.getQuantity());
					purchaseDetailVO.setUnitRate(plist.getUnitRate());
					purchaseDetailVO.setProduct(plist.getProduct());
					purchaseDetailVO.setBatchNumber(plist.getBatchNumber());
					purchaseDetailVO.setExpiryDate(null != plist
							.getProductExpiry() ? DateFormat
							.convertDateToString(plist.getProductExpiry()
									.toString()) : "");
					purchaseDetailVO.setRackId(null != plist.getProduct()
							.getShelf() ? plist.getProduct().getShelf()
							.getShelfId() : null);
					purchaseDetailVO.setRackType(null != plist.getProduct()
							.getShelf() ? plist.getProduct().getShelf()
							.getName() : null);
					purchaseDetailVO.setDescription(plist.getDescription());
					productDetails.put(key, purchaseDetailVO);
					purchaseQty += plist.getQuantity();
					totalUnitRate += (plist.getQuantity() * plist.getUnitRate());
				}
				for (ReceiveDetail receives : purchase.getReceiveDetails()) {
					key = (receives
							.getProduct()
							.getProductId()
							.toString()
							.concat((null != receives.getBatchNumber()
									&& !("").equals(receives.getBatchNumber()) ? "-"
									+ receives.getBatchNumber()
									: "")).concat(null != receives
							.getProductExpiry() ? "-"
							+ receives.getProductExpiry() : "")).trim();
					if (productDetails.containsKey(key)) {
						PurchaseDetailVO product = productDetails.get(key);
						product.setTotalPurchaseProduct(receives
								.getReceiveQty()
								+ (null != product
										&& null != product
												.getTotalPurchaseProduct() ? product
										.getTotalPurchaseProduct() : 0));
						product.setCurrentReceivedQty(receives.getReceiveQty());
						product.setQuantity(receives.getReceiveQty());
						productDetails.put(key, product);
					}
				}
				for (GoodsReturnDetail returns : purchase
						.getGoodsReturnDetails()) {
					key = (returns
							.getProduct()
							.getProductId()
							.toString()
							.concat((null != returns.getBatchNumber()
									&& !("").equals(returns.getBatchNumber()) ? "-"
									+ returns.getBatchNumber()
									: "")).concat(null != returns
							.getProductExpiry() ? "-"
							+ returns.getProductExpiry() : "")).trim();
					if (productDetails.containsKey(key)) {
						PurchaseDetailVO product = productDetails.get(key);
						product.setTotalPurchaseProduct(returns.getReturnQty()
								+ (null != product
										&& null != product
												.getTotalPurchaseProduct() ? product
										.getTotalPurchaseProduct() : 0));
						product.setCurrentReturnQty(returns.getReturnQty());
						product.setReturnQty(returns.getReturnQty());
						productDetails.put(key, product);
					}
				}

				purchaseVO.setPurchaseDetailVO(new ArrayList<PurchaseDetailVO>(
						productDetails.values()));
				purchaseVO.setPurchaseDetails(purchase.getPurchaseDetails());
				purchaseVO.setTotalPurchaseQty(purchaseQty);
				purchaseVO.setTotalPurchaseAmount(totalUnitRate);
				purchaseVO.setEditFlag(true);
				voMaps.put(purchase.getPurchaseId(), purchaseVO);
			}
		}
		return new ArrayList<PurchaseVO>(voMaps.values());
	}

	public List<PurchaseVO> getPurchaseAllOrderByFilter(PurchaseVO filterVO)
			throws Exception {
		filterVO.setImplementationId(getImplemenationId().getImplementationId());
		List<Purchase> purchaseList = getPurchaseService()
				.getPurchaseOrdersByFilters(filterVO);
		List<PurchaseVO> purchaseVOList = new ArrayList<PurchaseVO>();
		if (null != purchaseList && !purchaseList.equals("")) {
			for (Purchase purchase : purchaseList) {
				purchaseVOList.add(convertPurchaseToVO(purchase));
			}
			return purchaseVOList;
		} else {
			return null;
		}
	}

	public PurchaseVO convertPurchaseToVO(Purchase purchase) {
		PurchaseVO vo = new PurchaseVO();
		try {
			BeanUtils.copyProperties(vo, purchase);
			Double totalPurchase = 0.0;
			List<PurchaseDetail> purchaseDetail = new ArrayList<PurchaseDetail>(
					purchase.getPurchaseDetails());
			List<PurchaseDetailVO> detailVOs = new ArrayList<PurchaseDetailVO>();
			for (PurchaseDetail list2 : purchaseDetail) {
				totalPurchase += list2.getUnitRate() * list2.getQuantity();
				detailVOs.add(convertPurchaseDetailToVO(list2));
			}
			vo.setPurchaseDetailVO(detailVOs);
			vo.setTotalPurchaseAmount(AIOSCommons.roundDecimals(totalPurchase));
			vo.setFromDate(DateFormat.convertDateToString(purchase.getDate()
					.toString()));
			vo.setToDate(DateFormat.convertDateToString(purchase
					.getExpiryDate().toString()));

			if (null != purchase.getSupplier()
					&& null != purchase.getSupplier().getPerson()
					&& !("").equals(purchase.getSupplier().getPerson())) {
				vo.setSupplierName(purchase
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(purchase.getSupplier().getPerson()
								.getLastName()));
			} else if (null != purchase.getSupplier()
					&& (null != purchase.getSupplier().getCmpDeptLocation() || null != purchase
							.getSupplier().getCompany())) {
				vo.setSupplierName(null != purchase.getSupplier()
						.getCmpDeptLocation()
						&& !("").equals(purchase.getSupplier()
								.getCmpDeptLocation()) ? purchase.getSupplier()
						.getCmpDeptLocation().getCompany().getCompanyName()
						: purchase.getSupplier().getCompany().getCompanyName());
			} else {
				vo.setSupplierName("");
			}
			vo.setCreditTermValue(null != purchase.getCreditTerm() ? purchase
					.getCreditTerm().getName() : "");

			// receive details
			Set<String> receiveNoSet = new HashSet<String>();
			for (ReceiveDetail pdetail : purchase.getReceiveDetails()) {
				receiveNoSet.add(pdetail.getReceive().getReceiveNumber());
			}
			StringBuilder receiveNumber = new StringBuilder();
			for (String purchaseNo : receiveNoSet)
				receiveNumber.append(purchaseNo).append(" ,");
			vo.setReceiveNumber(receiveNumber.toString());

			// Requisition details
			if (purchase.getRequisition() != null) {
				vo.setRequisitionNumber(purchase.getRequisition()
						.getReferenceNumber());
				if (purchase.getRequisition().getCmpDeptLoc() != null
						&& purchase.getRequisition().getCmpDeptLoc()
								.getDepartment() != null)
					vo.setDepartmentName(purchase.getRequisition()
							.getCmpDeptLoc().getDepartment()
							.getDepartmentName());
			}
			vo.setCreatedBy(null != purchase.getPersonByCreatedBy() ? purchase
					.getPersonByCreatedBy().getFirstName()
					+ " "
					+ purchase.getPersonByCreatedBy().getLastName() : "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}

	public PurchaseDetailVO convertPurchaseDetailToVO(PurchaseDetail detail) {
		PurchaseDetailVO detailVO = new PurchaseDetailVO();
		try {
			BeanUtils.copyProperties(detailVO, detail);
			detailVO.setTotalRate(AIOSCommons.roundDecimals(detail
					.getQuantity() * detail.getUnitRate()));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return detailVO;
	}

	public List<Purchase> getSupplierActivePurchaseOrder(Long supplierId)
			throws Exception {
		return purchaseService.getSupplierActivePurchaseOrder(supplierId);
	}

	public List<Purchase> getSupplierActivePurchaseOrderAndCurrentReceive(
			Long receiveId) throws Exception {
		return purchaseService
				.getSupplierActivePurchaseOrderWithReceive(receiveId);
	}

	public List<Currency> getCurrencyList(Implementation implementation)
			throws Exception {
		return currencyService.getAllCurrency(implementation);
	}

	public List<SupplierTO> convertListTO(List<Supplier> supplierList)
			throws Exception {
		List<SupplierTO> supplierTOList = new ArrayList<SupplierTO>();
		for (Supplier list : supplierList) {
			supplierTOList.add(convertTO(list));
		}
		return supplierTOList;
	}

	public SupplierTO convertTO(Supplier supplier) {
		SupplierTO supplierTO = new SupplierTO();
		supplierTO.setSupplierId(supplier.getSupplierId());
		supplierTO.setSupplierNumber(supplier.getSupplierNumber());
		supplierTO.setCombinationId(supplier.getCombination()
				.getCombinationId());
		if (null != supplier.getPerson())
			supplierTO.setSupplierName(supplier.getPerson().getFirstName()
					+ " " + supplier.getPerson().getLastName());
		else
			supplierTO
					.setSupplierName(null != supplier.getCmpDeptLocation() ? supplier
							.getCmpDeptLocation().getCompany().getCompanyName()
							: supplier.getCompany().getCompanyName());
		if (supplier.getSupplierType() != null)
			supplierTO.setType(Constants.Accounts.SupplierType.get(
					supplier.getSupplierType()).toString());

		if (null != supplier.getCreditTerm())
			supplierTO.setPaymentTermId(supplier.getCreditTerm()
					.getCreditTermId());
		return supplierTO;

	}

	public List<Purchase> getPurchaseList() {
		return purchaseList;
	}

	public void setPurchaseList(List<Purchase> purchaseList) {
		this.purchaseList = purchaseList;
	}

	public PurchaseService getPurchaseService() {
		return purchaseService;
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public QuotationBL getQuotationBL() {
		return quotationBL;
	}

	public void setQuotationBL(QuotationBL quotationBL) {
		this.quotationBL = quotationBL;
	}

	public SupplierService getSupplierService() {
		return supplierService;
	}

	public void setSupplierService(SupplierService supplierService) {
		this.supplierService = supplierService;
	}

	public ReceiveService getReceiveService() {
		return receiveService;
	}

	public void setReceiveService(ReceiveService receiveService) {
		this.receiveService = receiveService;
	}

	public GoodsReturnService getGoodsReturnService() {
		return goodsReturnService;
	}

	public void setGoodsReturnService(GoodsReturnService goodsReturnService) {
		this.goodsReturnService = goodsReturnService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public PackageConversionBL getPackageConversionBL() {
		return packageConversionBL;
	}

	public void setPackageConversionBL(PackageConversionBL packageConversionBL) {
		this.packageConversionBL = packageConversionBL;
	}
}
