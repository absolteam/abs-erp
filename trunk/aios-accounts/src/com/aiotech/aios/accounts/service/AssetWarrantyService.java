package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetWarranty;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetWarrantyService {

	private AIOTechGenericDAO<AssetWarranty> assetWarrantyDAO;

	// Get all Asset Warranty
	public List<AssetWarranty> getAllAssetWarranty(Implementation implementation)
			throws Exception {
		return assetWarrantyDAO.findByNamedQuery("getAllAssetWarranty",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Simple Asset Warranty By WarrantyId
	public AssetWarranty getSimpleAssetWarrantyById(long assetWarrantyId)
			throws Exception {
		return assetWarrantyDAO.findById(assetWarrantyId);
	}

	// Get Asset Warranty By WarrantyId
	public AssetWarranty getAssetWarrantyById(long assetWarrantyId)
			throws Exception {
		return assetWarrantyDAO.findByNamedQuery("getAssetWarrantyById",
				assetWarrantyId).get(0);
	}

	// Save Asset Warranty
	public void saveAssetWarranty(AssetWarranty assetWarranty) throws Exception {
		assetWarrantyDAO.saveOrUpdate(assetWarranty);
	}

	// Delete Asset Warranty
	public void deleteAssetWarranty(AssetWarranty assetWarranty)
			throws Exception {
		assetWarrantyDAO.delete(assetWarranty);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetWarranty> getAssetWarrantyDAO() {
		return assetWarrantyDAO;
	}

	public void setAssetWarrantyDAO(
			AIOTechGenericDAO<AssetWarranty> assetWarrantyDAO) {
		this.assetWarrantyDAO = assetWarrantyDAO;
	}
}
