package com.aiotech.aios.accounts.service.bl;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Component;

import com.aiotech.aios.system.domain.entity.vo.Configuration;
import com.aiotech.aios.system.domain.entity.vo.ConfigurationVO;
import com.aiotech.aios.system.domain.entity.vo.Configurations;
import com.aiotech.aios.system.domain.entity.vo.MapEntryType;

@Component
public class ConfigurationHandler {

	public ConfigurationVO getConfigurationSetup(long implementationId) {
		try {

			Properties confProps = new Properties();
			File jarPath = new File(ConfigurationHandler.class
					.getProtectionDomain().getCodeSource().getLocation()
					.getPath());
			String propertiesPath = jarPath.getParentFile().getAbsolutePath();

			confProps.load(new FileInputStream(propertiesPath.substring(0,
					propertiesPath.lastIndexOf(File.separator) + 1)
					+ "aios.conf"));

			Configurations configurations = getConfiguration(confProps);
			List<Configuration> list = configurations.getConfigurations();
			Configuration configuration = null;
			if (list != null && list.size() > 0) {
				for (Configuration conf : list) {
					if ((long) conf.getImplementation() == (long) implementationId) {
						configuration = conf;
						break;
					}
				}
			}
			ConfigurationVO configurationVO = null;
			if (null != configuration && null != configuration.getMap()) {
				configurationVO = new ConfigurationVO(0, false, false,
						confProps);
				List<MapEntryType> entryType = configuration.getMap()
						.getEntry();
				for (MapEntryType entry : entryType) {
					if (entry.getKey().contains("pdc_alert_days"))
						configurationVO.setAlertDays(Integer.parseInt(entry
								.getValue()));
					if (entry.getKey().contains("pdc_alert_email"))
						configurationVO.setEmailEnabled(null != entry
								.getValue() ? Boolean.valueOf(entry.getValue())
								: false);
					if (entry.getKey().contains("pdc_alert_sms"))
						configurationVO
								.setSmsEnabled(null != entry.getValue() ? Boolean
										.valueOf(entry.getValue()) : false);
				}
			}
			return configurationVO;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private Configurations getConfiguration(Properties confProps)
			throws Exception {
		Configurations configurations = null;

		JAXBContext jaxbContext = JAXBContext.newInstance(Configurations.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		configurations = (Configurations) jaxbUnmarshaller
				.unmarshal((new File(confProps.getProperty("file.drive")
						+ "aios/configurations.xml")));
		return configurations;
	}
}
