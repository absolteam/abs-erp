package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductCategoryService {

	private AIOTechGenericDAO<ProductCategory> productCategoryDAO;

	// Get all product categories
	public List<ProductCategory> getProductCategory(
			Implementation implementation) throws Exception {
		return productCategoryDAO.findByNamedQuery("getProductCategory",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Save Product Category
	public void saveProductCategory(ProductCategory productCategory)
			throws Exception {
		productCategoryDAO.saveOrUpdate(productCategory);
	}

	// Save Product Category
	public void saveProductCategory(List<ProductCategory> productCategory)
			throws Exception {
		productCategoryDAO.saveOrUpdateAll(productCategory);
	}

	// Delete Product Category
	public void deleteProductCategory(ProductCategory productCategory)
			throws Exception {
		productCategoryDAO.delete(productCategory);
	}

	// Get all product sub categories
	public List<ProductCategory> getProductParentCategory(
			Implementation implementation) throws Exception {
		return productCategoryDAO.findByNamedQuery("getProductParentCategory",
				implementation);
	}

	// Get all product sub categories
	public List<ProductCategory> getAllPOSProductCategories(
			Implementation implementation) throws Exception {
		return productCategoryDAO.findByNamedQuery(
				"getAllPOSProductCategories", implementation);
	}

	// Get all product sub categories
	public List<ProductCategory> getProductSubCategory(
			Implementation implementation) throws Exception {
		return productCategoryDAO.findByNamedQuery("getProductSubCategory",
				implementation);
	}

	// Get all product sub categories
	public List<ProductCategory> getProductSubCategoryByCategory(Long categoryId)
			throws Exception {
		return productCategoryDAO.findByNamedQuery(
				"getProductSubCategoryByCategory", categoryId);
	}

	// Get all product sub categories
	public ProductCategory getProductSubCategoryByProduct(long productId)
			throws Exception {
		List<ProductCategory> productCategory = productCategoryDAO
				.findByNamedQuery("getProductSubCategoryByProduct", productId);
		return null != productCategory && productCategory.size() > 0 ? productCategory
				.get(0) : null;
	}

	// Get all product sub categories
	public ProductCategory getProductCategoryDetails(long productCategoryId)
			throws Exception {
		List<ProductCategory> productCategory = productCategoryDAO
				.findByNamedQuery("getProductCategoryDetails",
						productCategoryId);
		return null != productCategory && productCategory.size() > 0 ? productCategory
				.get(0) : null;
	}

	// Getters & Setters
	public AIOTechGenericDAO<ProductCategory> getProductCategoryDAO() {
		return productCategoryDAO;
	}

	public void setProductCategoryDAO(
			AIOTechGenericDAO<ProductCategory> productCategoryDAO) {
		this.productCategoryDAO = productCategoryDAO;
	}
}
