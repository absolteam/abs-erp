package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class StoreService {
	private AIOTechGenericDAO<Store> storeDAO;
	private AIOTechGenericDAO<Aisle> aisleDAO;
	private AIOTechGenericDAO<Shelf> shelfDAO;

	public List<Store> getAllStores(Implementation implementation)
			throws Exception {
		return storeDAO.findByNamedQuery("getAllStores", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Store> getStores(Implementation implementation)
			throws Exception {
		return storeDAO.findByNamedQuery("getStores", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get store and store details by store id
	public Store getStoreInfoDetails(long storeId) throws Exception {
		return storeDAO.findByNamedQuery("getStoreInfoDetails", storeId).get(0);
	}
	
	public Store getBasicStoreByStoreId(long storeId) throws Exception {
		return storeDAO.findByNamedQuery("getBasicStoreByStoreId", storeId).get(0);
	}

	// Get Store by rack id
	public Shelf getShelfById(int rackId) {
		return shelfDAO.findByNamedQuery("getShelfById", rackId).get(0);
	}
	
	public Shelf getShelfByShelfId(int shelfId) throws Exception {
		return shelfDAO.findById(shelfId);
	}

	// Get Racks by section id
	public List<Shelf> getRacksBySectionId(int sectionId) throws Exception {
		return shelfDAO.findByNamedQuery("getRacksBySectionId", sectionId);
	}

	// Get Racks by section id
	public List<Shelf> getShelfsByRackId(int rackId) throws Exception {
		return shelfDAO.findByNamedQuery("getShelfsByRackId", rackId);
	}

	public List<Shelf> getShelfsByStoreId(long storeId) throws Exception {
		return shelfDAO.findByNamedQuery("getShelfsByStoreId", storeId);
	}
	
	public Shelf getStoreByShelfId(Integer shelfId) {
		return shelfDAO.findByNamedQuery("getStoreByShelfId", shelfId).get(0);
	}

	public Shelf getStoreCompanyDetailsByShelfId(Integer shelfId) {
		return shelfDAO.findByNamedQuery("getStoreCompanyDetailsByShelfId",
				shelfId).get(0);
	}

	// Get Store by rack id
	@SuppressWarnings("unchecked")
	public Shelf getShelfById(int rackId, Session session) {
		String getShelfById = "SELECT r FROM Shelf r" + " JOIN FETCH r.aisle a"
				+ " JOIN FETCH a.store s" + " WHERE r.shelfId = ?";
		List<Shelf> shelf = session.createQuery(getShelfById)
				.setInteger(0, rackId).list();
		return shelf.get(0);
	}

	// Get Store by rack id
	public Shelf getShelfByStoreId(long storeId) {
		return shelfDAO.findByNamedQuery("getShelfByStoreId", storeId).get(0);
	}

	// Get Store by rack id
	@SuppressWarnings("unchecked")
	public Shelf getShelfByStoreId(long storeId, Session session) {
		String getShelfByStoreId = "SELECT DISTINCT(r) FROM Shelf r"
				+ " WHERE r.aisle.store.storeId = ? AND r IS NOT NULL";
		List<Shelf> shelf = session.createQuery(getShelfByStoreId)
				.setLong(0, storeId).list();
		return shelf.get(0);
	}

	// Get store by criteria
	@SuppressWarnings("unchecked")
	public List<Store> getStoreByCriteria(Implementation implementation,
			long storeId, long personId) throws Exception {
		Criteria storeCriteria = storeDAO.createCriteria();
		if (personId > 0) {
			storeCriteria.add(Restrictions.conjunction().add(
					Restrictions.eq("person.personId", personId)));
		}
		if (storeId > 0) {
			storeCriteria.add(Restrictions.conjunction().add(
					Restrictions.eq("storeId", storeId)));
		}
		storeCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("implementation", implementation)));

		storeCriteria.setFetchMode("lookupDetailByStorageType", FetchMode.JOIN);
		storeCriteria.setFetchMode("lookupDetailByStorageSection",
				FetchMode.JOIN);
		storeCriteria.setFetchMode("lookupDetailByBinType", FetchMode.JOIN);
		storeCriteria.setFetchMode("person", FetchMode.JOIN);
		return storeCriteria.list();
	}

	// Save Store Details
	public void saveStoreDetais(Store store, List<Aisle> aisles)
			throws Exception {
		storeDAO.saveOrUpdate(store);
		for (Aisle aisle : aisles) {
			aisle.setStore(store);
		}
		aisleDAO.saveOrUpdateAll(aisles);
	}

	// Save Shelf Detail
	public void saveShelfDetails(List<Shelf> shelfDetails) throws Exception {
		shelfDAO.saveOrUpdateAll(shelfDetails);
	}

	// Delete Store Details
	public void deleteStore(Store store, List<Aisle> aisles,
			List<Shelf> shelfs, List<Shelf> racks) throws Exception {
		if (null != racks && racks.size() > 0)
			shelfDAO.deleteAll(racks);
		if (null != shelfs && shelfs.size() > 0)
			shelfDAO.deleteAll(shelfs);
		if (null != aisles && aisles.size() > 0)
			aisleDAO.deleteAll(aisles);
		storeDAO.delete(store);
	}

	// Get Store Detail Information
	public Store getStoreDetailByStoreId(long storeId) throws Exception {
		return storeDAO.findByNamedQuery("getStoreDetailByStoreId", storeId)
				.get(0);
	}

	// Get Aisle By Aisle Id
	public Aisle getAisleById(int aisleId) throws Exception {
		return aisleDAO.findById(aisleId);
	}

	// Get Store By Store Id
	public Store getStoreById(long storeId) throws Exception {
		return storeDAO.findById(storeId);
	}

	// Get Store By Store Id
	@SuppressWarnings("unchecked")
	public Store getStoreById(long storeId, Session session) throws Exception {
		String getStoreById = "SELECT s FROM Store s WHERE s.storeId = ?";
		List<Store> stores = session.createQuery(getStoreById)
				.setLong(0, storeId).list();
		return stores.get(0);
	}

	// Get Aisle Details
	public List<Aisle> getAsileDetails(long storeId) throws Exception {
		return aisleDAO.findByNamedQuery("getAisleDetails", storeId);
	}

	// Get Store Details
	public Store getStoreDetails(long storeId) throws Exception {
		return storeDAO.findByNamedQuery("getStoreDetails", storeId).get(0);
	}

	// Get Store Location Cost centre
	public Store getStoreLocationCostCentre(long storeId) throws Exception {
		return storeDAO.findByNamedQuery("getStoreLocationCostCentre", storeId)
				.get(0);
	}

	// Get Store Location Cost centre
	@SuppressWarnings("unchecked")
	public Store getStoreLocationCostCentre(long storeId, Session session)
			throws Exception {
		String getStoreLocationCostCentre = "SELECT s FROM Store s"
				+ " JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " JOIN FETCH cmpLoc.combination c"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " WHERE s.storeId=?";
		List<Store> stores = session.createQuery(getStoreLocationCostCentre)
				.setLong(0, storeId).list();
		return stores.get(0);
	}

	public void deleteAsile(List<Aisle> aisleList) throws Exception {
		aisleDAO.deleteAll(aisleList);
	}

	public void deleteRacks(List<Shelf> shelfs, List<Shelf> deletedRacks)
			throws Exception {
		if (null != shelfs && shelfs.size() > 0)
			shelfDAO.deleteAll(shelfs);
		shelfDAO.deleteAll(deletedRacks);
	}

	public void deleteShelfs(List<Shelf> shelfs) throws Exception {
		shelfDAO.deleteAll(shelfs);
	}

	@SuppressWarnings("unchecked")
	public List<Store> getFilteredStoreDetails(Implementation implementation,
			Long storeId, Long productId) throws Exception {
		Criteria storeCriteria = storeDAO.createCriteria();

		storeCriteria.createAlias("lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		storeCriteria.createAlias("stocks", "stock",
				CriteriaSpecification.LEFT_JOIN);
		storeCriteria.createAlias("stock.product", "prd",
				CriteriaSpecification.LEFT_JOIN); 
		storeCriteria.createAlias("person", "pr",
				CriteriaSpecification.LEFT_JOIN);
		
		if (storeId > 0) {
			storeCriteria.add(Restrictions.eq("storeId", storeId));
		}
		if (productId > 0) {
			storeCriteria.add(Restrictions.eq("prd.productId", productId));
		}

		storeCriteria.add(Restrictions.eq("implementation", implementation));

		Set<Store> uniqueStores = new HashSet<Store>(storeCriteria.list());
		List<Store> stores = new ArrayList<Store>(uniqueStores);

		return stores;
	}

	public Store getStoreByDepartmentLocation(Long companyDepartementLocationId)
			throws Exception {
		List<Store> stores = storeDAO.findByNamedQuery(
				"getStoreByDepartmentLocation", companyDepartementLocationId);
		if (stores != null && stores.size() > 0)
			return stores.get(0);
		else
			return null;
	}

	public AIOTechGenericDAO<Store> getStoreDAO() {
		return storeDAO;
	}

	public void setStoreDAO(AIOTechGenericDAO<Store> storeDAO) {
		this.storeDAO = storeDAO;
	}

	public AIOTechGenericDAO<Aisle> getAisleDAO() {
		return aisleDAO;
	}

	public void setAisleDAO(AIOTechGenericDAO<Aisle> aisleDAO) {
		this.aisleDAO = aisleDAO;
	}

	public AIOTechGenericDAO<Shelf> getShelfDAO() {
		return shelfDAO;
	}

	public void setShelfDAO(AIOTechGenericDAO<Shelf> shelfDAO) {
		this.shelfDAO = shelfDAO;
	}
}
