package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.CommissionMethod;
import com.aiotech.aios.accounts.domain.entity.CommissionPersonCombination;
import com.aiotech.aios.accounts.domain.entity.CommissionRule;
import com.aiotech.aios.accounts.domain.entity.vo.CommissionRuleVO;
import com.aiotech.aios.accounts.service.CommissionRuleService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CommissionRuleBL {

	// Dependencies
	private CommissionRuleService commissionRuleService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	
	// Show all commission rule json data
	public List<Object> showCommissionRuleDetails() throws Exception {
		List<CommissionRule> commissionRules = commissionRuleService
				.getCommissionRuleDetails(getImplementation());
		List<Object> commissionRuleVOs = new ArrayList<Object>();
		for (CommissionRule commissionRule : commissionRules) {
			commissionRuleVOs.add(addCommissionRuleVO(commissionRule));
		}
		return commissionRuleVOs;
	}

	// Convert Entity
	public CommissionRuleVO addCommissionRuleVO(CommissionRule commissionRule)
			throws Exception {
		CommissionRuleVO commissionRuleVO = new CommissionRuleVO();
		commissionRuleVO.setCommissionRuleId(commissionRule
				.getCommissionRuleId());
		commissionRuleVO.setRuleName(commissionRule.getRuleName());
		commissionRuleVO.setBaseName(Constants.Accounts.CommissionBase.get(
				commissionRule.getCommissionBase()).name());
		commissionRuleVO.setFromDate(DateFormat
				.convertDateToString(commissionRule.getValidFrom().toString()));
		commissionRuleVO.setToDate(DateFormat
				.convertDateToString(commissionRule.getValidTo().toString()));
		commissionRuleVO.setMethodName(Constants.Accounts.CalculationMethod
				.get(commissionRule.getCalculationMethod()).name()
				.replaceAll("_", " "));
		commissionRuleVO.setCreditNote(null != commissionRule.getIsCreditNote()
				&& commissionRule.getIsCreditNote() ? "True" : "False");
		commissionRuleVO.setDebitNote(null != commissionRule.getIsDebitNote()
				&& commissionRule.getIsDebitNote() ? "True" : "False");
		return commissionRuleVO;
	}

	// Save Commission Rule
	public void saveCommissionRule(CommissionRule commissionRule,
			List<CommissionMethod> commissionDetails,
			List<CommissionMethod> deletedCommissionDetails,
			List<CommissionPersonCombination> commissionPersons,
			List<CommissionPersonCombination> deleteCommissionPersons)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");
		commissionRule.setImplementation(getImplementation());
		if (deletedCommissionDetails != null
				&& deletedCommissionDetails.size() > 0)
			commissionRuleService
					.deleteCommissionMethods(deletedCommissionDetails);
		if (deleteCommissionPersons != null
				&& deleteCommissionPersons.size() > 0)
			commissionRuleService
					.deleteCommissionPersons(deleteCommissionPersons); 
		commissionRule.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (commissionRule != null && commissionRule.getCommissionRuleId() != null
				&& commissionRule.getCommissionRuleId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		commissionRuleService.saveCommissionRule(commissionRule,
				commissionDetails, commissionPersons);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				CommissionRule.class.getSimpleName(), commissionRule.getCommissionRuleId(),
				user, workflowDetailVo);
	}

	public void doCommissionRuleBusinessUpdate(Long commissionRuleId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) { 
				CommissionRule commissionRule =
				 commissionRuleService.getCommissionRuleBase(
						commissionRuleId);
				commissionRuleService.deleteCommissionRule(
				commissionRule,
				new ArrayList<CommissionMethod>(commissionRule
						.getCommissionMethods()),
				new ArrayList<CommissionPersonCombination>(commissionRule
						.getCommissionPersonCombinations()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Delete Commission Rule
	public void deleteCommissionRule(CommissionRule commissionRule,
			List<CommissionMethod> commissionDetails,
			List<CommissionPersonCombination> commissionPersons)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						CommissionRule.class.getSimpleName(), commissionRule.getCommissionRuleId(),
						user, workflowDetailVo);
		workflowDetailVo.setDeleteFlag(true);  
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & setters
	public CommissionRuleService getCommissionRuleService() {
		return commissionRuleService;
	}

	public void setCommissionRuleService(
			CommissionRuleService commissionRuleService) {
		this.commissionRuleService = commissionRuleService;
	}
	
	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
