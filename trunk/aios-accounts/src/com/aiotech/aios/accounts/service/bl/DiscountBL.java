package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Discount;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.DiscountOption;
import com.aiotech.aios.accounts.domain.entity.vo.DiscountVO;
import com.aiotech.aios.accounts.service.DiscountService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class DiscountBL {

	// Dependencies
	private DiscountService discountService;
	private LookupMasterBL lookupMasterBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	
	// Show all commission rule json data
	public List<Object> showAllProductDiscounts() throws Exception {
		List<Discount> discounts = discountService
				.getAllProductDiscounts(getImplementation());
		List<Object> discountVOs = new ArrayList<Object>();
		for (Discount discount : discounts) {
			discountVOs.add(addDiscountVO(discount));
		}
		return discountVOs;
	}

	// Add Discount
	private DiscountVO addDiscountVO(Discount discount) {
		DiscountVO discountVO = new DiscountVO();
		discountVO.setDiscountName(discount.getDiscountName());
		discountVO.setDiscountId(discount.getDiscountId()); 
		discountVO.setValidFrom(DateFormat.convertDateToString(discount
				.getFromDate().toString()));
		discountVO.setValidTo(DateFormat.convertDateToString(discount
				.getToDate().toString()));
		discountVO.setStatusValue(discount.getStatus() ? "Active" : "Inactive");
		discountVO.setDiscountOptionName(Constants.Accounts.DiscountOptions
				.get(discount.getDiscountOption()).name().replaceAll("_", " "));
		discountVO.setPriceListName(null!=discount.getLookupDetail()? discount.getLookupDetail()
				.getDisplayName() : "");
		return discountVO;
	}

	// Save Discount
	public void saveDiscount(Discount discount,
			List<DiscountMethod> discountDetails,
			List<DiscountMethod> deletedDiscountDetails,
			List<DiscountOption> discountOptions,
			List<DiscountOption> deletedDiscountOptions) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");
		discount.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (discount != null && discount.getDiscountId() != null
				&& discount.getDiscountId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		discount.setImplementation(getImplementation());
		if (null != deletedDiscountDetails && deletedDiscountDetails.size() > 0)
			discountService.deleteDiscountMethods(deletedDiscountDetails);
		if (null != deletedDiscountOptions && deletedDiscountOptions.size() > 0)
			discountService.deleteDiscountOptions(deletedDiscountOptions);
		
		discountService
				.saveDiscount(discount, discountDetails, discountOptions);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Discount.class.getSimpleName(), discount.getDiscountId(),
				user, workflowDetailVo);
	}
	
	public void doDiscountBusinessUpdate(Long discountId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				Discount productDiscount = discountService
						.getProductDiscountByDiscountId(discountId);
				discountService.deleteProductDiscount(
						productDiscount,
						new ArrayList<DiscountMethod>(productDiscount
								.getDiscountMethods()),
						new ArrayList<DiscountOption>(productDiscount
								.getDiscountOptions()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Product Discount
	public void deleteProductDiscount(Discount productDiscount)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						Discount.class.getSimpleName(), productDiscount.getDiscountId(),
						user, workflowDetailVo);  
		workflowDetailVo.setDeleteFlag(true); 
	}

	// Get Product Discount By Product Id
	public Discount getDiscountByProductId(long productId) throws Exception {
		return discountService.getProductDiscountByProductId(productId,
				(DateFormat.getFromDate(Calendar.getInstance().getTime())));
	}

	// Get Product Discount By Customer Id
	public Discount getDiscountByCustomerId(long customerId) throws Exception {
		return discountService.getProductDiscountByCustomerId(customerId,
				(DateFormat.getFromDate(Calendar.getInstance().getTime())));
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public DiscountService getDiscountService() {
		return discountService;
	}

	public void setDiscountService(DiscountService discountService) {
		this.discountService = discountService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}
	
	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
