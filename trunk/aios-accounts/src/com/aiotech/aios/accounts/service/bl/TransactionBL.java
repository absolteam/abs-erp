package com.aiotech.aios.accounts.service.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.domain.entity.TransactionTempDetail;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.TransactionService;
import com.aiotech.aios.accounts.service.TransactionTempService;
import com.aiotech.aios.accounts.to.TransactionTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class TransactionBL {

	private TransactionService transactionService;
	private CalendarBL calendarBL;
	private CategoryBL categoryBL;
	private GLCombinationService combinationService;
	private CurrencyService currencyService;
	private TransactionTempService transactionTempService;
	private CurrencyConversionBL currencyConversionBL;
	private CommentBL commentBL;
	private AlertBL alertBL;
	private WorkflowEnterpriseService workflowEnterpriseService;

	// Save Transaction temp details
	public synchronized void saveTransaction(Transaction transaction,
			List<TransactionDetail> transactionDetails,
			List<TransactionDetail> transactionDetailsDelete, long alertId,
			List<TransactionDetail> reverseTransactionDetails) throws Exception {

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (transaction != null && transaction.getTransactionId() != null
				&& transaction.getTransactionId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		boolean updateFlag = false;
		if (null != transaction.getTransactionId())
			updateFlag = true;

		if (updateFlag) {
			if (null != transactionDetailsDelete
					&& transactionDetailsDelete.size() > 0)
				transactionService
						.deleteTransactionDetail(transactionDetailsDelete);
			// Reverse old entries
			deductBalance(transaction, reverseTransactionDetails);
		} else {
			SystemBL.saveReferenceStamp(Transaction.class.getName(),
					transaction.getImplementation());
		}
		transaction.setCreatedDate(new Date());
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		transactionService.saveJournalEntries(transaction, transactionDetails);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Transaction.class.getSimpleName(),
				transaction.getTransactionId(), user, workflowDetailVo);
	}

	public void doTransactionBusinessUpdate(Long transactionId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			Transaction transaction = transactionService
					.getTransactionById(transactionId);
			List<TransactionDetail> transcationDetails = new ArrayList<TransactionDetail>(
					transaction.getTransactionDetails());
			if (!workflowDetailVO.isDeleteFlag()) {
				addBalance(transcationDetails, transaction);
			} else {
				deductBalance(transaction, transcationDetails);
				transactionService.deleteJournalEntries(transaction,
						transcationDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveUpdateTransctionLedger(String recordId, String processFlag) {
		try {
			TransactionTemp transactionTemp = this.getTransactionTempService()
					.getJournalByTempId(Long.parseLong(recordId));
			List<TransactionTempDetail> transactionTempDetail = new ArrayList<TransactionTempDetail>(
					transactionTemp.getTransactionTempDetails());
			Transaction referenceTransaction = new Transaction();
			referenceTransaction = transactionTemp.getTransaction();
			if (null != transactionTemp.getIsDelete()
					&& !transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("1").equals(processFlag)) {
				Transaction transaction = new Transaction();
				TransactionDetail detailEntity = new TransactionDetail();
				List<TransactionDetail> transactionDetail = new ArrayList<TransactionDetail>();
				List<TransactionDetail> transactionProcessDetail = null;
				transaction.setPeriod(transactionTemp.getPeriod());
				transaction.setCategory(transactionTemp.getCategory());
				transaction.setCurrency(transactionTemp.getCurrency());
				transaction.setDescription(transactionTemp.getDescription());
				transaction.setTransactionTime(transactionTemp
						.getTransactionTime());
				transaction.setEntryTime(transactionTemp.getEntryTime());
				transaction.setPerson(transactionTemp.getPerson());
				transaction.setCreatedDate(transactionTemp.getCreatedDate());
				if (null != referenceTransaction
						&& !("").equals(referenceTransaction)) {
					transaction.setJournalNumber(referenceTransaction
							.getJournalNumber());
				} else {
					transaction.setJournalNumber(SystemBL.saveReferenceStamp(
							Transaction.class.getName(), getImplemenationId()));
				}
				transaction.setImplementation(transactionTemp
						.getImplementation());
				for (TransactionTempDetail list : transactionTempDetail) {
					detailEntity = new TransactionDetail();
					detailEntity.setCombination(list.getCombination());
					detailEntity.setDescription(list.getDescription());
					detailEntity.setAmount(list.getAmount());
					detailEntity.setIsDebit(list.getIsDebit());
					detailEntity.setIsreceipt(list.getIsreceipt());
					transactionDetail.add(detailEntity);
				}
				if (null != transactionTemp.getTransaction()
						&& !("").equals(transactionTemp.getTransaction())) {
					transaction.setTransactionId(transactionTemp
							.getTransaction().getTransactionId());
					transactionProcessDetail = this
							.getTransactionService()
							.getJournalLinesById(transaction.getTransactionId());

				}
				this.updateTransaction(transaction, transactionDetail,
						transactionProcessDetail, transactionTemp,
						transactionTempDetail);
			} else if (null != transactionTemp.getIsDelete()
					&& !transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("2").equals(processFlag)) {
				transactionTemp.setIsApprove(Byte.parseByte(String
						.valueOf(Constants.RealEstate.Status.Deny.getCode())));
				this.getTransactionTempService().saveTempTransaction(
						transactionTemp, transactionTempDetail);
			} else if (null != transactionTemp.getIsDelete()
					&& transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("1").equals(processFlag)) {
				String journalId = String.valueOf(transactionTemp
						.getTransaction().getTransactionId());
				Transaction transaction = this.getTransactionService()
						.getJournalById(Long.parseLong(journalId));
				List<TransactionDetail> transactionDetails = this
						.getTransactionService().getJournalLinesById(
								transaction.getTransactionId());
				this.deleteUpdateTransaction(transaction, transactionDetails,
						transactionTemp, transactionTempDetail);
			} else if (null != transactionTemp.getIsDelete()
					&& transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("2").equals(processFlag)) {
				transactionTemp.setIsApprove(Byte.parseByte(String
						.valueOf(Constants.RealEstate.Status.Deny.getCode())));
				this.getTransactionTempService().saveTempTransaction(
						transactionTemp, transactionTempDetail);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	// Save Transaction & Transaction Details & Update Ledger
	public void saveTransaction(Transaction transaction,
			List<TransactionDetail> transactionDetails) throws Exception {
		transaction.setCreatedDate(new Date());
		transactionService.saveJournalEntries(transaction, transactionDetails);
		addBalance(transactionDetails, transaction);
	}

	public void saveTransactionOnly(Transaction transaction,
			List<TransactionDetail> transactionDetails) throws Exception {
		transaction.setCreatedDate(new Date());
		transactionService.saveJournalEntries(transaction, transactionDetails);
	}

	// Save Transaction & Transaction Details & Update Ledger
	public void saveTransactions(List<Transaction> transactions)
			throws Exception {
		List<TransactionDetail> transactionDetails = null;
		for (Transaction transaction : transactions) {
			transaction.setCreatedDate(new Date());
			transactionDetails = new ArrayList<TransactionDetail>(
					transaction.getTransactionDetails());
			transactionService.saveJournalEntries(transaction,
					transactionDetails);
			addBalance(transactionDetails, transaction);
		}
	}

	// Reset Ledger
	public void resetLedger(List<Transaction> transactions) throws Exception {
		for (Transaction transaction : transactions) {
			addBalance(
					new ArrayList<TransactionDetail>(
							transaction.getTransactionDetails()), transaction);
		}
	}

	public void resetLedger2(List<Transaction> transactions,
			List<Combination> combinations) throws Exception {

		List<Ledger> ledgers = new ArrayList<Ledger>();
		Ledger finalLedger = null;
		for (Combination combination : combinations) {

			finalLedger = new Ledger();
			finalLedger.setCombination(combination);
			List<Ledger> ledgerStructure = new ArrayList<Ledger>();

			for (Transaction transaction : transactions) {
				for (TransactionDetail detail : transaction
						.getTransactionDetails()) {
					if ((double) combination.getCombinationId() == (double) detail
							.getCombination().getCombinationId()) {
						Ledger struct = new Ledger();
						struct.setBalance(detail.getAmount());
						struct.setSide(detail.getIsDebit());
						ledgerStructure.add(struct);
					}
				}
			}
			Double credit = 0.0;
			Double debit = 0.0;

			for (Ledger ledger : ledgerStructure) {
				System.out.println(ledger.getBalance() + "      "
						+ ledger.getSide());
			}
			System.out.println("test");
			for (Ledger ledger : ledgerStructure) {

				if (ledger.getSide()) {
					credit = credit + ledger.getBalance();
				} else {
					debit = debit + ledger.getBalance();
				}

			}
			finalLedger.setSide(true);
			if (debit > credit) {
				finalLedger.setSide(false);
				finalLedger.setBalance(debit - credit);

			} else {
				finalLedger.setSide(true);
				finalLedger.setBalance(credit - debit);
			}
			ledgers.add(finalLedger);
		}
		transactionService.saveAllAccountBalance(ledgers);
	}

	// Update Transaction, Transaction Details & Delete Exiting Transaction
	public void updateTransaction(Transaction transaction,
			List<TransactionDetail> transactionDetails,
			List<TransactionDetail> transactionDetailDeductList,
			TransactionTemp transactionTemp,
			List<TransactionTempDetail> transactionTempDetails)
			throws Exception {
		transactionService.saveJournalEntries(transaction, transactionDetails);
		if (null != transactionDetailDeductList
				&& transactionDetailDeductList.size() > 0) {
			deductBalance(transaction, transactionDetailDeductList);
			transactionService
					.deleteTransactionDetail(transactionDetailDeductList);
		}
		addBalance(transactionDetails, transaction);
		transactionTempService.deleteTransactionTemp(transactionTemp,
				transactionTempDetails);
	}

	public void deleteTransaction(long recordId, String useCase)
			throws Exception {
		List<Transaction> transactions = transactionService
				.getTransactionByRecordIdAndUseCase(recordId, useCase.trim());
		if (null != transactions && transactions.size() > 0) {
			for (Transaction transaction : transactions) {
				List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>(
						transaction.getTransactionDetails());
				deductBalance(transaction, transactionDetails);
				transactionService.deleteJournalEntries(transaction,
						transactionDetails);
			}
		}
	}

	public void deleteTransactionOnly(long recordId, String useCase)
			throws Exception {
		List<Transaction> transactions = transactionService
				.getTransactionByRecordIdAndUseCase(recordId, useCase.trim());
		if (null != transactions && transactions.size() > 0) {
			for (Transaction transaction : transactions) {
				List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>(
						transaction.getTransactionDetails());
				transactionService.deleteJournalEntries(transaction,
						transactionDetails);
			}
		}
	}

	// Delete Transaction & Transaction Temp and Update ledger
	public void deleteUpdateTransaction(Transaction transaction,
			List<TransactionDetail> transactionDetails,
			TransactionTemp transactionTemp,
			List<TransactionTempDetail> transactionTempDetails)
			throws Exception {
		deductBalance(transaction, transactionDetails);
		transactionTempService.deleteTransactionTemp(transactionTemp,
				transactionTempDetails);
		transactionService
				.deleteJournalEntries(transaction, transactionDetails);
	}

	// Delete Transaction & Transaction Temp and Update ledger
	public void deleteTransaction(Transaction transaction) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Transaction.class.getSimpleName(),
				transaction.getTransactionId(), user, workflowDetailVo);
	}

	// Deduct Balance form ledger
	private synchronized void deductBalance(Transaction transaction,
			List<TransactionDetail> transactionDetails) throws Exception {
		for (TransactionDetail detail : transactionDetails) {
			Ledger ledger = transactionService.getAccountBalance(detail
					.getCombination().getCombinationId());
			if (null == ledger.getBalance())
				ledger.setBalance(new Double(0));
			transactionService.saveAccountBalance(processDeductLedger(ledger,
					detail, transaction));
		}
	}

	// Business logic for deduct ledger balance
	private synchronized Ledger processDeductLedger(Ledger ledger,
			TransactionDetail transactionDetail, Transaction transaction)
			throws Exception {
		double ledgerBalance = ledger.getSide() ? (-1 * ledger.getBalance())
				: ledger.getBalance();
		double transactionAmount = transactionDetail.getIsDebit() ? (-1 * transactionDetail
				.getAmount()) : transactionDetail.getAmount();
		double calculationAmount = 0.0;
		transactionAmount = (transactionAmount * transaction.getExchangeRate());
		if ((int) transactionDetail.getCombination()
				.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
				.getCode()
				|| (int) transactionDetail.getCombination()
						.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
						.getCode()) {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		} else {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		}
		return ledger;
	}

	// Update Ledger Balance
	private synchronized void addBalance(
			List<TransactionDetail> transactionDetails, Transaction transaction)
			throws Exception {
		for (TransactionDetail transactionDetail : transactionDetails) {
			Ledger ledger = transactionService
					.getAccountBalance(transactionDetail.getCombination()
							.getCombinationId());
			if (null != ledger && !("").equals(ledger)) {
				if (null == ledger.getBalance()
						|| ("").equals(ledger.getBalance()))
					ledger.setBalance(new Double(0));
			} else {
				ledger = new Ledger();
				ledger.setSide(TransactionType.Debit.getCode());
				ledger.setBalance(new Double(0));
				ledger.setCombination(combinationService
						.getCombinationAccounts(transactionDetail
								.getCombination().getCombinationId()));
			}
			transactionService.saveAccountBalance(processUpdateLedger(ledger,
					transactionDetail, transaction));
		}
	}

	// Business logic for update ledger balance
	private synchronized Ledger processUpdateLedger(Ledger ledger,
			TransactionDetail transactionDetail, Transaction transaction)
			throws Exception {
		double ledgerBalance = ledger.getSide() ? (-1 * ledger.getBalance())
				: ledger.getBalance();
		double transactionAmount = transactionDetail.getIsDebit() ? (-1 * transactionDetail
				.getAmount()) : transactionDetail.getAmount();
		double calculationAmount = 0.0;

		if ((int) ledger.getCombination().getAccountByNaturalAccountId()
				.getAccountType() == (int) AccountType.Assets.getCode()
				|| (int) ledger.getCombination().getAccountByNaturalAccountId()
						.getAccountType() == (int) AccountType.Expenses
						.getCode()) {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		} else {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs((transactionDetail.getAmount() * transaction
									.getExchangeRate()));
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		}
		return ledger;
	}

	// Create Transaction
	public synchronized Transaction createTransaction(Long currencyId,
			String description, Date transactionDate, Category category,
			Long recordId, String useCase) throws Exception {
		Transaction transaction = new Transaction();
		transaction.setJournalNumber(SystemBL.saveReferenceStamp(
				Transaction.class.getName(), getImplemenationId()));
		Currency defaultCurrency = this.getCurrency();
		if (null != defaultCurrency) {
			if ((long) defaultCurrency.getCurrencyId() != currencyId) {
				Currency currency = this.getCurrencyService().getCurrency(
						currencyId);
				CurrencyConversion currencyConversion = this
						.getCurrencyConversionBL()
						.getCurrencyConversionService()
						.getCurrencyConversionByCurrency(currencyId);
				transaction.setCurrency(currency);
				transaction
						.setExchangeRate(null != currencyConversion ? currencyConversion
								.getConversionRate() : 1.0);
			} else {
				transaction.setCurrency(defaultCurrency);
				transaction.setExchangeRate(1.0);
			}
		}
		transaction.setDescription(description);
		transaction.setPeriod(getPeriodDetails());
		transaction.setCategory(category);
		transaction.setTransactionTime(transactionDate);
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		transaction.setEntryTime(new Date());

		transaction.setCreatedDate(new Date());
		transaction.setImplementation(getImplemenationId());
		transaction.setRecordId(recordId);
		transaction.setUseCase(useCase);
		return transaction;
	}

	// Create Transaction
	public synchronized Transaction createTransaction(Period period,
			Long currencyId, String description, Date transactionDate,
			Category category, Long recordId, String useCase) throws Exception {
		Transaction transaction = new Transaction();
		transaction.setJournalNumber(SystemBL.saveReferenceStamp(
				Transaction.class.getName(), getImplemenationId()));
		Currency defaultCurrency = this.getCurrency();
		if (null != defaultCurrency) {
			if ((long) defaultCurrency.getCurrencyId() != currencyId) {
				Currency currency = this.getCurrencyService().getCurrency(
						currencyId);
				CurrencyConversion currencyConversion = this
						.getCurrencyConversionBL()
						.getCurrencyConversionService()
						.getCurrencyConversionByCurrency(currencyId);
				transaction.setCurrency(currency);
				transaction
						.setExchangeRate(null != currencyConversion ? currencyConversion
								.getConversionRate() : 1.0);
			} else {
				transaction.setCurrency(defaultCurrency);
				transaction.setExchangeRate(1.0);
			}
		}
		transaction.setDescription(description);
		transaction.setPeriod(period);
		transaction.setCategory(category);
		transaction.setTransactionTime(transactionDate);
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		transaction.setEntryTime(new Date());
		transaction.setCreatedDate(new Date());
		transaction.setImplementation(getImplemenationId());
		transaction.setRecordId(recordId);
		transaction.setUseCase(useCase);
		return transaction;
	}

	public synchronized Transaction createTransaction(Period period,
			Long currencyId, String description, Date transactionDate,
			Category category, Long recordId, String useCase,
			Double exchangeRate) throws Exception {
		Transaction transaction = new Transaction();
		transaction.setJournalNumber(SystemBL.saveReferenceStamp(
				Transaction.class.getName(), getImplemenationId()));
		Currency currency = new Currency();
		currency.setCurrencyId(currencyId);
		transaction.setCurrency(currency);
		transaction.setExchangeRate(exchangeRate);
		transaction.setDescription(description);
		transaction.setPeriod(period);
		transaction.setCategory(category);
		transaction.setTransactionTime(transactionDate);
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		transaction.setEntryTime(new Date());
		transaction.setCreatedDate(new Date());
		transaction.setImplementation(getImplemenationId());
		transaction.setRecordId(recordId);
		transaction.setUseCase(useCase);
		return transaction;
	}

	public synchronized Transaction createTransaction(Period period,
			Long currencyId, String description, Date transactionDate,
			Category category, Long recordId, String useCase,
			Double exchangeRate, Implementation implementation)
			throws Exception {
		Transaction transaction = new Transaction();
		transaction.setJournalNumber(SystemBL.saveReferenceStamp(
				Transaction.class.getName(), implementation));
		Currency currency = new Currency();
		currency.setCurrencyId(currencyId);
		transaction.setCurrency(currency);
		transaction.setExchangeRate(exchangeRate);
		transaction.setDescription(description);
		transaction.setPeriod(period);
		transaction.setCategory(category);
		transaction.setTransactionTime(transactionDate);
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		transaction.setEntryTime(new Date());
		transaction.setCreatedDate(new Date());
		transaction.setImplementation(implementation);
		transaction.setRecordId(recordId);
		transaction.setUseCase(useCase);
		return transaction;
	}

	// Create Transaction
	public synchronized Transaction createTransaction(Period period,
			Long currencyId, String description, Date transactionDate,
			Category category, Long recordId, String useCase,
			Implementation implementation) throws Exception {
		Transaction transaction = new Transaction();
		transaction.setJournalNumber(SystemBL.saveReferenceStamp(
				Transaction.class.getName(), implementation));
		Currency defaultCurrency = this.getCurrency(implementation);
		if (null != defaultCurrency) {
			if ((long) defaultCurrency.getCurrencyId() != currencyId) {
				Currency currency = currencyService.getCurrency(currencyId);
				CurrencyConversion currencyConversion = this
						.getCurrencyConversionBL()
						.getCurrencyConversionService()
						.getCurrencyConversionByCurrency(currencyId);
				transaction.setCurrency(currency);
				transaction
						.setExchangeRate(null != currencyConversion ? currencyConversion
								.getConversionRate() : 1.0);
			} else {
				transaction.setCurrency(defaultCurrency);
				transaction.setExchangeRate(1.0);
			}
		}
		transaction.setDescription(description);
		transaction.setPeriod(period);
		transaction.setCategory(category);
		transaction.setTransactionTime(transactionDate);
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		transaction.setEntryTime(transactionDate);
		transaction.setCreatedDate(new Date());
		transaction.setImplementation(implementation);
		transaction.setRecordId(recordId);
		transaction.setUseCase(useCase);
		return transaction;
	}

	// Create Transaction
	public synchronized Transaction createTransaction(Long currencyId,
			String description, Date transactionDate, Category category,
			Implementation implementation, Long recordId, String useCase)
			throws Exception {
		Transaction transaction = new Transaction();
		transaction.setJournalNumber(SystemBL.saveReferenceStamp(
				Transaction.class.getName(), implementation));
		Currency defaultCurrency = this.getCurrency(implementation);
		if (null != defaultCurrency) {
			if ((long) defaultCurrency.getCurrencyId() != currencyId) {
				Currency currency = this.getCurrencyService().getCurrency(
						currencyId);
				CurrencyConversion currencyConversion = this
						.getCurrencyConversionBL()
						.getCurrencyConversionService()
						.getCurrencyConversionByCurrency(currencyId);
				transaction.setCurrency(currency);
				transaction
						.setExchangeRate(null != currencyConversion ? currencyConversion
								.getConversionRate() : 1.0);
			} else {
				transaction.setCurrency(defaultCurrency);
				transaction.setExchangeRate(1.0);
			}
		}
		transaction.setDescription(description);
		transaction.setPeriod(getPeriodDetails(implementation));
		transaction.setCategory(category);
		transaction.setTransactionTime(transactionDate);
		transaction.setEntryTime(new Date());
		transaction.setRecordId(recordId);
		transaction.setUseCase(useCase);
		transaction.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		transaction.setCreatedDate(new Date());
		transaction.setImplementation(implementation);
		return transaction;
	}

	// Get Current Active Period
	public Period getPeriodDetails() throws Exception {
		Calendar calendar = calendarBL.getCalendarService().getAtiveCalendar(
				getImplemenationId());
		if (null != calendar) {
			return calendarBL.getCalendarService().getActivePeriod(
					calendar.getCalendarId());
		}
		return null;
	}

	// Get Current Active Period
	public Period getPeriodDetails(Implementation implementation)
			throws Exception {
		Calendar calendar = calendarBL.getCalendarService().getAtiveCalendar(
				implementation);
		if (null != calendar) {
			return calendarBL.getCalendarService().getActivePeriod(
					calendar.getCalendarId());
		}
		return null;
	}

	// Create Transaction Detail
	public synchronized TransactionDetail createTransactionDetail(
			Double amount, Long combinationId, Boolean debitFlag,
			String description, Boolean isReceipt, String transactionReference)
			throws Exception {
		TransactionDetail transactionDetail = new TransactionDetail();
		transactionDetail.setAmount(AIOSCommons.roundThreeDecimals(amount));
		Combination combination = new Combination();
		combination.setCombinationId(combinationId);
		transactionDetail.setCombination(combination);
		transactionDetail.setDescription(description);
		transactionDetail.setIsDebit(debitFlag);
		transactionDetail.setIsreceipt(isReceipt);
		transactionDetail.setTransactionReference(transactionReference);
		return transactionDetail;
	}

	public synchronized TransactionDetail createTransactionDetail(
			Double amount, Long combinationId, Boolean debitFlag,
			String description, Boolean isReceipt, String transactionReference,
			Long recordId, String useCase) throws Exception {
		TransactionDetail transactionDetail = new TransactionDetail();
		transactionDetail.setAmount(AIOSCommons.roundThreeDecimals(amount));
		Combination combination = new Combination();
		combination.setCombinationId(combinationId);
		transactionDetail.setCombination(combination);
		transactionDetail.setDescription(description);
		transactionDetail.setIsDebit(debitFlag);
		transactionDetail.setIsreceipt(isReceipt);
		transactionDetail.setTransactionReference(transactionReference);
		transactionDetail.setRecordId(recordId);
		transactionDetail.setUseCase(useCase);
		return transactionDetail;
	}

	// Create Transaction Temp
	public TransactionTemp createTransactionTemp(Long currencyId,
			String description, Date transactionDate, Category category,
			String journalNumber, Byte isApprove) throws Exception {
		/*
		 * Map<String, Object> sessionObj = ActionContext.getContext()
		 * .getSession();
		 */
		TransactionTemp transactionTemp = new TransactionTemp();
		transactionTemp.setJournalNumber(journalNumber);
		Currency currency = new Currency();
		currency.setCurrencyId(currencyId);
		transactionTemp.setCurrency(currency);
		transactionTemp.setDescription(description);
		transactionTemp.setPeriod(getPeriodDetails());
		transactionTemp.setCategory(category);
		transactionTemp.setTransactionTime(transactionDate);
		transactionTemp.setIsApprove(isApprove);
		transactionTemp.setEntryTime(new Date());
		// User user = (User) sessionObj.get("USER");

		// transactionTemp.setPerson(user.getPerson());
		transactionTemp.setCreatedDate(new Date());
		transactionTemp.setImplementation(getImplemenationId());
		return transactionTemp;
	}

	// Create Transaction Temp Detail
	public TransactionTempDetail createTransactionTempDetail(Double amount,
			Long combinationId, Boolean debitFlag, String description,
			Boolean isReceipt) throws Exception {
		TransactionTempDetail transactionTempDetail = new TransactionTempDetail();
		transactionTempDetail.setAmount(AIOSCommons.roundThreeDecimals(amount));
		Combination combination = new Combination();
		combination.setCombinationId(combinationId);
		transactionTempDetail.setCombination(combination);
		transactionTempDetail.setDescription(description);
		transactionTempDetail.setIsDebit(debitFlag);
		transactionTempDetail.setIsreceipt(isReceipt);
		return transactionTempDetail;
	}

	// Inactive Alert
	public void closeAlert(long alertId) throws Exception {
		Alert alert = this.getAlertBL().getAlertService().getAlertInfo(alertId);
		alert.setIsActive(false);
		this.getAlertBL().commonSaveAlert(alert);
	}

	// Get Implementation
	public Implementation getImplemenationId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = new Implementation();
		if (null != session.getAttribute("THIS"))
			implementation = (Implementation) session.getAttribute("THIS");
		return implementation;
	}

	// Get Default Currency
	public Currency getCurrency() {
		Currency currency = currencyService
				.getDefaultCurrency(getImplemenationId());
		return currency;
	}

	// Get Default Currency With Implementation
	public Currency getCurrency(Implementation implementation) {
		Currency currency = currencyService.getDefaultCurrency(implementation);
		return currency;
	}

	public void deleteTranscationTempByJournalNO(String journalNumber,
			Implementation implementation) throws Exception {
		List<TransactionTemp> transactionTempList = this
				.getTransactionTempService().getTransactionTempJournalNo(
						journalNumber, implementation);
		if (null != transactionTempList && transactionTempList.size() > 0) {
			for (TransactionTemp list : transactionTempList) {
				this.getTransactionTempService().deleteTransactionTemp(
						list,
						new ArrayList<TransactionTempDetail>(list
								.getTransactionTempDetails()));
			}
		}
	}

	// get category
	public Category getCategory(String categoryName) throws Exception {
		Category category = new Category();
		List<Category> categories = categoryBL.getCategoryService()
				.getCategoryByName(getImplemenationId(), categoryName);
		if (null != categories && categories.size() > 0)
			return categories.get(0);
		else {
			category = categoryBL.createCategory(getImplemenationId(),
					categoryName);
			return category;
		}
	}

	// get category
	public Category getCategory(String categoryName,
			Implementation implementation) throws Exception {
		Category category = new Category();
		List<Category> categories = categoryBL.getCategoryService()
				.getAllActiveCategories(implementation);
		if (null != categories) {
			for (Category list : categories) {
				if ((list.getName().trim()).equalsIgnoreCase(categoryName
						.trim()))
					category.setCategoryId(list.getCategoryId());
			}
		}
		if (null != category && null != category.getCategoryId()) {
			return category;
		} else {
			category = categoryBL.createCategory(implementation, categoryName);
			return category;
		}
	}

	public boolean autoJVMakingSet(List<String> rowList) {
		try {
			List<Combination> combinations = combinationService
					.getAllCombination(getImplemenationId());
			Map<String, Combination> hashMapCombination = new HashMap<String, Combination>();
			String stringCode = null;
			for (Combination combination : combinations) {
				stringCode = new String();
				stringCode = stringCode.concat(combination
						.getAccountByCompanyAccountId().getCode());
				if (combination.getAccountByCostcenterAccountId() != null)
					stringCode = stringCode.concat("."
							+ combination.getAccountByCostcenterAccountId()
									.getCode());

				if (combination.getAccountByNaturalAccountId() != null)
					stringCode = stringCode.concat("."
							+ combination.getAccountByNaturalAccountId()
									.getCode());

				if (combination.getAccountByAnalysisAccountId() != null)
					stringCode = stringCode.concat("."
							+ combination.getAccountByAnalysisAccountId()
									.getCode());

				hashMapCombination.put(stringCode, combination);
			}
			TransactionTO transactionTO = null;
			List<TransactionTO> subListForTransaction = new ArrayList<TransactionTO>();
			double debitAmount = 0.0;
			double creditAmount = 0.0;
			int sizeOfJVs = 0;
			for (String string : rowList) {

				transactionTO = new TransactionTO();
				String[] detailString = splitValues(string, ",");
				SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy");
				Date dt = formatter.parse(detailString[0]);
				transactionTO.setDate(dt);
				transactionTO.setAccountCode(detailString[1].trim());
				transactionTO.setCombination(hashMapCombination
						.get(detailString[1].trim()));
				if (transactionTO.getCombination() == null)
					System.out
							.println("Combination is missing for this account Id--"
									+ detailString[0]
									+ "----"
									+ detailString[1].trim() + "------");

				if (detailString[2] != null
						&& !detailString[2].trim().equals(""))
					transactionTO.setRefFromXL(detailString[2]);

				transactionTO.setDescription(detailString[3]);
				if (detailString[4] != null
						&& !detailString[4].trim().equals("")) {
					transactionTO.setAmount(AIOSCommons
							.formatAmountToDouble(detailString[4]));
					transactionTO.setDebit(TransactionType.Debit.getCode());
					debitAmount += transactionTO.getAmount();
				} else if (detailString[5] != null
						&& !detailString[5].trim().equals("")) {
					transactionTO.setAmount(AIOSCommons
							.formatAmountToDouble(detailString[5]));
					transactionTO.setDebit(TransactionType.Credit.getCode());
					creditAmount += transactionTO.getAmount();
				}

				subListForTransaction.add(transactionTO);
				/*
				 * System.out.println("debitAmount"+AIOSCommons.roundDecimals(
				 * debitAmount)+"--creditAmount--"+AIOSCommons
				 * .roundDecimals(creditAmount));
				 */
				if (debitAmount > 0
						&& (double) AIOSCommons.roundDecimals(debitAmount) == (double) AIOSCommons
								.roundDecimals(creditAmount)) {
					// Call Making JV Entries
					autoJVMakingInsert(subListForTransaction);
					debitAmount = 0.0;
					creditAmount = 0.0;
					sizeOfJVs += subListForTransaction.size();
					// System.out.println("Size-------------"+sizeOfJVs);
					subListForTransaction = new ArrayList<TransactionTO>();

				}

			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	// Auto JV by XLS
	public boolean autoJVMakingInsert(
			List<TransactionTO> transactionDetailListString) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Transaction transaction = null;
			if (transactionDetailListString != null
					&& transactionDetailListString.size() > 0) {
				transaction = new Transaction();
				Currency currency = new Currency();
				currency.setCurrencyId((long) 9);
				Date transactionDate = DateFormat
						.getFromDate(transactionDetailListString.get(0)
								.getDate()); // the date in question
				Calendar calendar = calendarBL.getCalendarService()
						.getAtiveCalendar(getImplemenationId());
				List<Period> periodFromCalList = calendarBL
						.getCalendarService()
						.findPeriodByUsingDateFromCalendar(
								calendar.getCalendarId());

				// transaction.setPeriod(periodByDate);
				for (Period period : periodFromCalList) {
					Date min = DateFormat.getFromDate(period.getStartTime()), max = DateFormat
							.getFromDate(period.getEndTime());
					if (min.getTime() <= transactionDate.getTime()
							&& transactionDate.getTime() <= max.getTime()) {
						transaction.setPeriod(period);
						break;
					}

				}

				transaction.setImplementation(getImplemenationId());
				transaction.setTransactionTime(transactionDetailListString.get(
						0).getDate());
				transaction.setEntryTime(new Date());

				Category category = new Category();
				category.setCategoryId((long) 14);
				transaction.setCategory(category);

				transaction.setCurrency(currency);
				transaction.setPerson(person);
				transaction.setCreatedDate(new Date());

				transaction.setJournalNumber(SystemBL.saveReferenceStamp(
						Transaction.class.getName(), getImplemenationId()));
			}

			List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
			TransactionDetail transactionDetail = null;
			try {
				for (TransactionTO transactionTo : transactionDetailListString) {
					transactionDetail = new TransactionDetail();
					transactionDetail.setCombination(transactionTo
							.getCombination());
					if (transactionTo.isDebit())
						transactionDetail.setIsDebit(TransactionType.Debit
								.getCode());
					else
						transactionDetail.setIsDebit(TransactionType.Credit
								.getCode());
					transactionDetail.setAmount(transactionTo.getAmount());
					transactionDetail.setDescription((transactionTo
							.getRefFromXL() != null ? transactionTo
							.getRefFromXL() + "--" : "")
							+ transactionTo.getDescription());
					transactionDetailList.add(transactionDetail);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			saveTransaction(transaction, transactionDetailList);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

	public List<String> fileReader(String fileName) {
		final Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		System.out
				.println(confProps.getProperty("file.drive").concat(fileName));
		File file = new File(confProps.getProperty("file.drive").concat(
				fileName));
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			return null;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public synchronized void directDeleteJV(List<Transaction> transactions) {
		try {
			if (null != transactions && transactions.size() > 0) {
				for (Transaction transaction : transactions) {
					List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>(
							transaction.getTransactionDetails());
					deductBalance(transaction, transactionDetails);
					transactionService.deleteJournalEntries(transaction,
							transactionDetails);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateReceiptReference() throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(6l);
		long categoryId = 465l;
		List<Transaction> transactions = transactionService
				.getTransactionByReceive(impl, Receive.class.getSimpleName(),
						categoryId);
		String reference = null;
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		for (Transaction transaction : transactions) {
			if (null != transaction.getTransactionDetails()
					&& transaction.getTransactionDetails().size() > 0) {
				reference = transaction.getDescription().substring(
						(transaction.getDescription().indexOf(":") + 1),
						transaction.getDescription().length() - 1);
				for (TransactionDetail trnsDT : transaction
						.getTransactionDetails()) {
					trnsDT.setTransactionReference(reference.trim());
					transactionDetails.add(trnsDT);
				}
			}
		}
		if (null != transactionDetails && transactionDetails.size() > 0)
			transactionService.updateJournalEntries(transactionDetails);
	}

	public void deleteLedgers(List<Ledger> ledgers) throws Exception {
		transactionService.deleteLedgers(ledgers);
	}

	public CategoryBL getCategoryBL() {
		return categoryBL;
	}

	public void setCategoryBL(CategoryBL categoryBL) {
		this.categoryBL = categoryBL;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public TransactionTempService getTransactionTempService() {
		return transactionTempService;
	}

	public void setTransactionTempService(
			TransactionTempService transactionTempService) {
		this.transactionTempService = transactionTempService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;

	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CalendarBL getCalendarBL() {
		return calendarBL;
	}

	public void setCalendarBL(CalendarBL calendarBL) {
		this.calendarBL = calendarBL;
	}

	public CurrencyConversionBL getCurrencyConversionBL() {
		return currencyConversionBL;
	}

	public void setCurrencyConversionBL(
			CurrencyConversionBL currencyConversionBL) {
		this.currencyConversionBL = currencyConversionBL;
	}
}
