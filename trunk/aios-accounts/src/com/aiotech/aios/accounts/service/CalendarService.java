package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.common.to.Constants.Accounts.PeriodStatus;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CalendarService {

	private AIOTechGenericDAO<Calendar> calendarDAO;
	private AIOTechGenericDAO<Period> periodDAO;

	public List<Calendar> getAllCalendar(Implementation implementation)
			throws Exception {
		return calendarDAO.findByNamedQuery("getCalendarList", implementation);
	}

	public List<Calendar> getAllApprovedCalendar(Implementation implementation)
			throws Exception {
		return calendarDAO.findByNamedQuery("getAllApprovedCalendar",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Period> getAllActivePeriods(long calendarId) throws Exception {
		return periodDAO.findByNamedQuery("getAllActivePeriods", calendarId);
	}

	public List<Period> getAllActivePeriods(Implementation implementation)
			throws Exception {
		return periodDAO.findByNamedQuery("getActivePeriods", implementation);
	}

	public List<Period> getAllActivePeriods() throws Exception {
		return periodDAO.findByNamedQuery("periodsWithoutImplementation");
	}

	public Period getPeriodById(long periodId) {
		return periodDAO.findById(periodId);
	}

	public Period getCalendarPeriodById(long periodId) {
		return periodDAO.findByNamedQuery("getCalendarPeriodById", periodId)
				.get(0);
	}

	public Calendar getAtiveCalendar(Implementation implementation)
			throws Exception {
		List<Calendar> calendars = calendarDAO.findByNamedQuery(
				"getActiveCalendar", implementation);
		return null != calendars && calendars.size() > 0 ? calendars.get(0)
				: null;
	}

	public Period getActivePeriod(long calendarId) throws Exception {
		List<Period> periodList = periodDAO.findByNamedQuery("getActivePeriod",
				calendarId);
		Collections.sort(periodList, new Comparator<Period>() {
			public int compare(Period o1, Period o2) {
				return o2.getStartTime().compareTo(o1.getStartTime());
			}
		});
		return periodList.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Period> findPeriodByUsingDateFromCalendar(Long calendarId) {
		Criteria payPeriod = periodDAO.createCriteria();

		payPeriod.createAlias("calendar", "cal");

		payPeriod.add(Restrictions.eq("cal.calendarId", calendarId));
		return payPeriod.list();

	}

	public Period findPeriodByUsingDateFromCalendar(Long calendarId, Date date) {
		Criteria payPeriod = periodDAO.createCriteria();

		payPeriod.createAlias("calendar", "cal");

		if (date != null) {
			payPeriod.add(Restrictions.ge("startTime", date));
			payPeriod.add(Restrictions.le("endTime", date));
		}
		payPeriod.add(Restrictions.eq("cal.calendarId", calendarId));
		if (payPeriod.list() != null && payPeriod.list().size() > 0)
			return (Period) payPeriod.list().get(0);
		else
			return null;

	}

	public Calendar getCalendarDetails(Long calendarId) throws Exception {
		return calendarDAO.findById(calendarId);
	}

	public Calendar getFinancialYearById(Long calendarId) throws Exception {
		return calendarDAO.findByNamedQuery("getFinancialYearById", calendarId)
				.get(0);
	}

	public List<Period> getPeriodDetails(Long calendarId) throws Exception {
		return periodDAO.findByNamedQuery("getPeriodList", calendarId);
	}

	public Calendar getCalendarByPeriod(Long periodId) throws Exception {
		return calendarDAO.findByNamedQuery("getCalendarByPeriod", periodId)
				.get(0);
	}

	public List<Period> getAllPeriods(Implementation implementation)
			throws Exception {
		return periodDAO.findByNamedQuery("getAllPeriods", implementation);
	}

	public List<Period> getActiveCalendarClosedPeriods(
			Implementation implementation) throws Exception {
		return periodDAO.findByNamedQuery("getActiveCalendarClosedPeriods",
				implementation);
	}

	public List<Period> getAllClosedPeriods(Implementation implementation,
			Byte status1, Byte status2) throws Exception {
		return periodDAO.findByNamedQuery("getAllClosedPeriods",
				implementation, status1, status2);
	}

	public void addCalendar(Calendar calendar, List<Period> periods)
			throws Exception {
		calendarDAO.saveOrUpdate(calendar);
		if (periods != null && periods.size() > 0) {
			for (Period period : periods) {
				period.setCalendar(calendar);
			}
			periodDAO.saveOrUpdateAll(periods);
		}
	}

	public Period getPeriodFromDate(Implementation implementation,
			Date periodDate) throws Exception {
		List<Period> periodList = periodDAO.findByNamedQuery(
				"getPeriodFromDate", implementation, periodDate);
		if (null != periodList && periodList.size() > 0)
			return periodList.get(0);
		else
			return null;
	}

	public Period getPeriodByCalendarAndDate(Date periodDate, Long calendarId)
			throws Exception {
		List<Period> periodList = periodDAO.findByNamedQuery(
				"getPeriodByCalendarAndDate", calendarId, periodDate,
				periodDate);
		if (null != periodList && periodList.size() > 0)
			return periodList.get(0);
		else
			return null;
	}

	public void deletePeriods(List<Period> period) throws Exception {
		periodDAO.deleteAll(period);
	}

	public void deletePeriod(Calendar calendar) throws Exception {
		List<Period> period = new ArrayList<Period>();
		period = periodDAO.findByNamedQuery("getPeriodList",
				calendar.getCalendarId());
		periodDAO.deleteAll(period);
		calendarDAO.delete(calendar);
	}

	public void deleteCalendar(Calendar calendar) throws Exception {
		calendarDAO.delete(calendar);
	}

	public List<Period> getPeriodsByCalendarId(Long calendarId)
			throws Exception {
		return periodDAO.findByNamedQuery("getPeriodList", calendarId);
	}

	public List<Period> getFiscalPeriodDetails(Long calendarId, Date startTime,
			Date endTime) throws Exception {
		return periodDAO.findByNamedQuery("getFiscalPeriodDetails", calendarId,
				startTime, endTime);
	}

	public List<Period> getNonClosedPeriodDetails(Long calendarId,
			Date startTime, Date endTime) throws Exception {
		return periodDAO.findByNamedQuery("getNonClosedPeriodDetails",
				calendarId, startTime, endTime);
	}

	public List<Period> getFiscalAbovePeriods(Long calendarId, Date startTime,
			Date endTime, long periodId) throws Exception {
		return periodDAO.findByNamedQuery("getFiscalAbovePeriods", calendarId,
				startTime, endTime, periodId);
	}

	public Period getTransactionPostingPeriod(Date periodDate,
			Implementation implementation) {
		List<Period> periods = periodDAO.findByNamedQuery(
				"getTransactionPostingPeriod", implementation, periodDate,
				periodDate, PeriodStatus.Open.getCode());
		return null != periods && periods.size() > 0 ? periods.get(0) : null;
	}
	
	public Period getPeriodByPeriodDate(Date periodDate,
			Implementation implementation) {
		List<Period> periods = periodDAO.findByNamedQuery(
				"getPeriodByPeriodDate", implementation, periodDate,
				periodDate);
		return null != periods && periods.size() > 0 ? periods.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Calendar> getCalendarByDates(Implementation implementation,
			Date fromDate, Date toDate) throws Exception {
		Criteria criteria = calendarDAO.createCriteria();
		criteria.createAlias("periods", "pr", CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("implementation", implementation));
		
		if (fromDate != null)
			criteria.add(Restrictions.ge("startTime", fromDate));
		
		if (toDate != null) {
			criteria.add(Restrictions.or(Restrictions.le("startTime", toDate),
					Restrictions.le("endTime", toDate)));
		}
		Set<Calendar> uniqueRecord = new HashSet<Calendar>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Calendar>(
				uniqueRecord) : null; 
	}

	public AIOTechGenericDAO<Calendar> getCalendarDAO() {
		return calendarDAO;
	}

	public void setCalendarDAO(AIOTechGenericDAO<Calendar> calendarDAO) {
		this.calendarDAO = calendarDAO;
	}

	public AIOTechGenericDAO<Period> getPeriodDAO() {
		return periodDAO;
	}

	public void setPeriodDAO(AIOTechGenericDAO<Period> periodDAO) {
		this.periodDAO = periodDAO;
	}
}
