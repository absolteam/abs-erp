package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ChequeBookService {
	
	private AIOTechGenericDAO<ChequeBook> chequeBookDAO;

	//fetch all
	public List<ChequeBook> getAllChequeBooks(Implementation implementation)
			throws Exception {
		return this.getChequeBookDAO().findByNamedQuery("getAllChequeBooks",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	//find by id
	public ChequeBook findChequeById(long chequeBookId) throws Exception {
		return this.getChequeBookDAO().findById(chequeBookId);
	}
	
	//find by query findChequeBookById
	public ChequeBook findChequeBookById(long chequeBookId) throws Exception {
		return this.getChequeBookDAO()
				.findByNamedQuery("findChequeBookById", chequeBookId).get(0);
	}
	
	//save or update
	public void saveChequeBook(ChequeBook chequeBook) throws Exception {
		chequeBookDAO.saveOrUpdate(chequeBook);
	}
	
	//delete
	public void deleteChequeBook(ChequeBook chequeBook)  {
		chequeBookDAO.delete(chequeBook);
 	}
	
	public AIOTechGenericDAO<ChequeBook> getChequeBookDAO() {
		return chequeBookDAO;
	}

	public void setChequeBookDAO(AIOTechGenericDAO<ChequeBook> chequeBookDAO) {
		this.chequeBookDAO = chequeBookDAO;
	}
	
}
