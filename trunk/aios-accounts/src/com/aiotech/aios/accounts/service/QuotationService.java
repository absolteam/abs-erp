package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class QuotationService {

	private AIOTechGenericDAO<Quotation> quotationDAO;
	private AIOTechGenericDAO<QuotationDetail> quotationDetailDAO;

	public List<Quotation> getAllQuotation(Implementation implementation)
			throws Exception {
		return quotationDAO.findByNamedQuery("getAllQuotation", implementation);
	}

	public List<Quotation> getAllQuotationNumber(Implementation implementation)
			throws Exception {
		return quotationDAO.findByNamedQuery("getAllQuotationNumber",
				implementation);
	}

	public List<Quotation> getUnPurchasedQuotation(Implementation implementation)
			throws Exception {
		return quotationDAO.findByNamedQuery("getUnPurchasedQuotation",
				implementation, new Date(),
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<QuotationDetail> getQuotationDetail(long quotationId)
			throws Exception {
		return quotationDetailDAO.findByNamedQuery("getQuotationDetail",
				quotationId);
	}

	public void deleteQuotation(Quotation quotation,
			List<QuotationDetail> quotationDetails) throws Exception {
		quotationDetailDAO.deleteAll(quotationDetails);
		quotationDAO.delete(quotation);
	}

	public List<Quotation> getQuotationById(Long id) {
		return quotationDAO.findByNamedQuery("getQuotationById", id);
	}

	// Get Basic details of Quotation
	public Quotation getQuotationBasicById(long quotationId) {
		return quotationDAO.findByNamedQuery("getQuotationBasicById",
				quotationId).get(0);
	}
	
	public List<QuotationDetail> getQuotationDetailByProductId(long productId) {
		return quotationDetailDAO.findByNamedQuery("getQuotationDetailByProductId",
				productId);
	}

	public void deleteQuotationDetail(List<QuotationDetail> quotationDetailList)
			throws Exception {
		this.getQuotationDetailDAO().deleteAll(quotationDetailList);
	}

	public void saveQuotation(Quotation quotation) throws Exception {
		quotationDAO.saveOrUpdate(quotation);
		for (QuotationDetail detail : quotation.getQuotationDetails())
			detail.setQuotation(quotation);
		quotationDetailDAO.saveOrUpdateAll(quotation.getQuotationDetails());
	}

	@SuppressWarnings("unchecked")
	public List<Quotation> getQuotationsByDifferentValues(Date fromDate,
			Date toDate, Long supplierId, Long productId,
			Implementation implementation) throws Exception {

		Criteria quotationCriteria = quotationDAO.createCriteria();
		quotationCriteria.createAlias("quotationDetails", "qd",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("qd.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("currency", "cur",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("cur.currencyPool", "cpool",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("supplier", "sp",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("sp.person", "pr",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("cdl.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("sp.company", "spcpy",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("tender", "td",
				CriteriaSpecification.LEFT_JOIN);
		quotationCriteria.createAlias("requisition", "req",
				CriteriaSpecification.LEFT_JOIN);

		if (fromDate != null && !("").equals(fromDate))
			quotationCriteria.add(Restrictions.ge("quotationDate", fromDate));

		if (toDate != null && !("").equals(toDate))
			quotationCriteria.add(Restrictions.le("quotationDate", toDate));

		if (supplierId != null && supplierId > 0)
			quotationCriteria.add(Restrictions.eq("sp.supplierId", supplierId));

		if (productId != null && productId > 0)
			quotationCriteria.add(Restrictions.eq("prd.productId", productId));

		if (implementation != null)
			quotationCriteria.createCriteria("implementation").add(
					Restrictions.eq("implementationId",
							implementation.getImplementationId()));

		Set<Quotation> uniqueRecord = new HashSet<Quotation>(
				quotationCriteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Quotation>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("deprecation")
	public Quotation getQuotationByIdWithDifferentChilds(Long quotationId,
			Boolean isCurrency, Boolean isTender, Boolean isSupplier,
			Boolean isPurchases, Boolean isQuotationDetails) {

		Quotation quotation = null;

		Criteria quotationCriteria = quotationDAO.createCriteria();

		if (isCurrency) {
			quotationCriteria.createCriteria("currency").setFetchMode(
					"currencyPool", FetchMode.EAGER);
		}
		if (isTender) {
			quotationCriteria.setFetchMode("tender", FetchMode.EAGER);
		}

		if (isSupplier) {
			quotationCriteria.setFetchMode("supplier", FetchMode.EAGER)
					.setFetchMode("supplier.person", FetchMode.EAGER);
		}
		if (isPurchases) {
			quotationCriteria.setFetchMode("purchases", FetchMode.EAGER);
		}
		if (isQuotationDetails) {
			quotationCriteria
					.setFetchMode("quotationDetails", FetchMode.EAGER)
					.setFetchMode("quotationDetails.product", FetchMode.EAGER)
					.setFetchMode("quotationDetails.product.productUnit",
							FetchMode.EAGER);
		}

		quotationCriteria.add(Restrictions.eq("quotationId", quotationId));

		if (quotationCriteria.list().size() > 0) {
			quotation = (Quotation) quotationCriteria.list().get(0);
		}

		return quotation;

	}

	public AIOTechGenericDAO<Quotation> getQuotationDAO() {
		return quotationDAO;
	}

	public AIOTechGenericDAO<QuotationDetail> getQuotationDetailDAO() {
		return quotationDetailDAO;
	}

	public void setQuotationDAO(AIOTechGenericDAO<Quotation> quotationDAO) {
		this.quotationDAO = quotationDAO;
	}

	public void setQuotationDetailDAO(
			AIOTechGenericDAO<QuotationDetail> quotationDetailDAO) {
		this.quotationDetailDAO = quotationDetailDAO;
	}
}
