package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetServiceCharge;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetServiceChargeService {

	private AIOTechGenericDAO<AssetServiceCharge> assetServiceChargeDAO;
	
	/**
	 * Get Asset Service Charge By Id
	 * @param assetServiceChargeId
	 * @return
	 */
	public List<AssetServiceCharge> getAssetServiceChargeById(
			long assetServiceDetailId) throws Exception {
		return assetServiceChargeDAO.findByNamedQuery(
				"getAssetServiceChargeById", assetServiceDetailId);
	} 
	
	
	public AssetServiceCharge getAssetServiceChargeByChargeId(
			long assetServiceChargeId) throws Exception {
		return assetServiceChargeDAO.findByNamedQuery(
				"getAssetServiceChargeByChargeId", assetServiceChargeId).get(0);
	} 
	
	/**
	 * Get Asset Service Charges By Service Detail Id
	 * @param assetServiceDetailId
	 * @return
	 */
	public List<AssetServiceCharge> getAssetServiceChargeByDetailId(
			long assetServiceDetailId) throws Exception {
		return assetServiceChargeDAO.findByNamedQuery(
				"getAssetServiceChargeByDetailId", assetServiceDetailId);
	}
	
	/**
	 * Save Asset Service Charge
	 * @param assetServiceCharge
	 */
	public void saveAssetServiceCharge(
			AssetServiceCharge assetServiceCharge)
			throws Exception {
		assetServiceChargeDAO.saveOrUpdate(assetServiceCharge); 
	}
	
	/**
	 * Delete Asset Service Charges
	 * @param assetServiceCharges
	 */
	public void deleteAssetServiceCharge(
			List<AssetServiceCharge> assetServiceCharges)
			throws Exception {
		assetServiceChargeDAO.deleteAll(assetServiceCharges); 
	}
	
	//Getters & Setters
	public AIOTechGenericDAO<AssetServiceCharge> getAssetServiceChargeDAO() {
		return assetServiceChargeDAO;
	}

	public void setAssetServiceChargeDAO(
			AIOTechGenericDAO<AssetServiceCharge> assetServiceChargeDAO) {
		this.assetServiceChargeDAO = assetServiceChargeDAO;
	}
}
