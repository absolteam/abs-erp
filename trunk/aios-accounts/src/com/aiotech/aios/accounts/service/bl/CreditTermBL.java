package com.aiotech.aios.accounts.service.bl;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.service.CreditTermService;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermDiscountMode;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermDueDay;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermPenaltyType;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CreditTermBL {

	private CreditTermService creditTermService;
	private List<CreditTerm> creditTermList = null;
	private LookupMasterBL lookupMasterBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Form JSON Object List
	public JSONObject getCreditTermList(Implementation implementation)
			throws Exception {
		creditTermList = creditTermService.getAllCreditTerms(implementation);
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		if (null != creditTermList && creditTermList.size()>0) { 
			JSONArray array = null;
			for (CreditTerm list : creditTermList) {
				array = new JSONArray();
				array.add(list.getCreditTermId());
				array.add(list.getName());
				array.add(null != list.getDueDay() ? CreditTermDueDay.get(
						list.getDueDay()).name() : "");
				array.add(null != list.getDiscountDay() ? list.getDiscountDay()
						: "");
				array.add(null != list.getDiscountMode() ? CreditTermDiscountMode
						.get(list.getDiscountMode()).name() : "");
				array.add(list.getDiscount());
				array.add(null != list.getPenaltyType() ? CreditTermPenaltyType
						.get(list.getPenaltyType()).name() : "");
				array.add(null != list.getPenaltyMode() ? CreditTermDiscountMode
						.get(list.getPenaltyMode()).name() : "");
				array.add(list.getPenalty());
				data.add(array);
			} 
		} 
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// Save or Update Credit Term
	public void saveCreditTerm(CreditTerm creditTerm) throws Exception { 
		Map<String, Object> sessionObj = ActionContext.getContext()
		.getSession();
		User user = (User) sessionObj.get("USER");
		creditTerm.setIsApprove((byte)WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (creditTerm != null && creditTerm.getCreditTermId() != null
				&& creditTerm.getCreditTermId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		} 
		creditTermService.saveCreditTerm(creditTerm);
		workflowEnterpriseService
		.generateNotificationsAgainstDataEntry(
				CreditTerm.class.getSimpleName(),
				creditTerm.getCreditTermId(), user,
				workflowDetailVo); 
	}

	public void doCreditTermBusinessUpdate(Long creditTermId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				CreditTerm creditTerm = creditTermService
						.getCreditTermDetails(creditTermId);
				creditTermService.deleteCreditTerm(creditTerm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Credit Term & it's associate Accounts
	public void deleteCreditTerm(CreditTerm creditTerm) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						CreditTerm.class.getSimpleName(), creditTerm.getCreditTermId(),
						user, workflowDetailVo);
		workflowDetailVo.setDeleteFlag(true); 
	}

	public Map<Byte, CreditTermDueDay> getDueDate() {
		Map<Byte, CreditTermDueDay> dueDays = new HashMap<Byte, CreditTermDueDay>();
		for (CreditTermDueDay dueDay : EnumSet.allOf(CreditTermDueDay.class))
			dueDays.put(dueDay.getCode(), dueDay);
		return dueDays;
	}

	public Map<Byte, CreditTermDiscountMode> getPaymentMode() {
		Map<Byte, CreditTermDiscountMode> discountModes = new HashMap<Byte, CreditTermDiscountMode>();
		for (CreditTermDiscountMode discountMode : EnumSet
				.allOf(CreditTermDiscountMode.class))
			discountModes.put(discountMode.getCode(), discountMode);
		return discountModes;
	}

	public Map<Byte, CreditTermPenaltyType> getPenalityType() {
		Map<Byte, CreditTermPenaltyType> penaltyTypes = new HashMap<Byte, CreditTermPenaltyType>();
		for (CreditTermPenaltyType penaltyType : EnumSet
				.allOf(CreditTermPenaltyType.class))
			penaltyTypes.put(penaltyType.getCode(), penaltyType);
		return penaltyTypes;
	}

	// Getters & Setters
	public CreditTermService getCreditTermService() {
		return creditTermService;
	}

	public void setCreditTermService(CreditTermService creditTermService) {
		this.creditTermService = creditTermService;
	}

	public List<CreditTerm> getCreditTermList() {
		return creditTermList;
	}

	public void setCreditTermList(List<CreditTerm> creditTermList) {
		this.creditTermList = creditTermList;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
