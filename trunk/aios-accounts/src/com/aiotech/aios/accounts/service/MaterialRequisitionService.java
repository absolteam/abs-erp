package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MaterialRequisitionService {

	private AIOTechGenericDAO<MaterialRequisition> materialRequisitionDAO;
	private AIOTechGenericDAO<MaterialRequisitionDetail> materialRequisitionDetailDAO;

	public List<MaterialRequisition> getAllMaterialRequisition(
			Implementation implementation) throws Exception {
		return materialRequisitionDAO.findByNamedQuery(
				"getAllMaterialRequisition", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public MaterialRequisition getMaterialRequisitionById(
			long materialRequisitionId) throws Exception {
		return materialRequisitionDAO.findByNamedQuery(
				"getMaterialRequisitionById", materialRequisitionId).get(0);
	}

	public List<MaterialRequisitionDetail> getMaterialRequisitionDetailById(
			long materialRequisitionId) throws Exception {
		return materialRequisitionDetailDAO.findByNamedQuery(
				"getMaterialRequisitionDetailById", materialRequisitionId);
	}
	
	public MaterialRequisitionDetail getMaterialRequisitionDetailByDetailId(
			long materialRequisitionDetailId) throws Exception {
		return materialRequisitionDetailDAO.findById(materialRequisitionDetailId);
	}

	public List<MaterialRequisitionDetail> getActiveMaterialRequisitionDetail(
			Implementation implementation, long storeId, byte status)
			throws Exception {
		return materialRequisitionDetailDAO.findByNamedQuery(
				"getActiveMaterialRequisitionDetail", implementation, storeId,
				status);
	}

	public void saveMaterialRequisition(
			MaterialRequisition materialRequisition,
			List<MaterialRequisitionDetail> materialRequisitionDetails)
			throws Exception {
		materialRequisitionDAO.saveOrUpdate(materialRequisition);
		for (MaterialRequisitionDetail materialRequisitionDetail : materialRequisitionDetails)
			materialRequisitionDetail
					.setMaterialRequisition(materialRequisition);
		materialRequisitionDetailDAO
				.saveOrUpdateAll(materialRequisitionDetails);
	}

	public void saveMaterialRequisition(
			List<MaterialRequisition> materialRequisitions) throws Exception {
		materialRequisitionDAO.saveOrUpdateAll(materialRequisitions);
	}

	public void deleteMaterialRequisition(
			MaterialRequisition materialRequisition,
			List<MaterialRequisitionDetail> materialRequisitionDetails)
			throws Exception {
		materialRequisitionDetailDAO.deleteAll(materialRequisitionDetails);
		materialRequisitionDAO.delete(materialRequisition);

	}
	
	public void deleteMaterialRequisitionDetails( 
			List<MaterialRequisitionDetail> materialRequisitionDetails)
			throws Exception {
		materialRequisitionDetailDAO.deleteAll(materialRequisitionDetails);
 
	}

	@SuppressWarnings("unchecked")
	public List<MaterialRequisition> getMaterialRequisitionsByGroupIds(
			Long[] materialRequisitionIds) throws Exception {
		Criteria criteria = materialRequisitionDAO.createCriteria();
		criteria.createAlias("materialRequisitionDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("materialRequisitionId",
				materialRequisitionIds));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<MaterialRequisition> getFilteredMaterialRequisition(
			Implementation implementation, Long materialRequisitionId,
			Long productId, Long storeId, Byte statusId, Date fromDate,
			Date toDate) throws Exception {
		Criteria criteria = materialRequisitionDAO.createCriteria();

		criteria.createAlias("store", "str", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("materialRequisitionDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productUnit", "unit",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (materialRequisitionId != null && materialRequisitionId > 0) {
			criteria.add(Restrictions.eq("materialRequisitionId",
					materialRequisitionId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("str.storeId", storeId));
		}

		if (statusId != null && statusId > 0) {
			criteria.add(Restrictions.eq("requisitionStatus", statusId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("requisitionDate", fromDate,
					toDate));
		}

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<MaterialRequisition> getFilteredMaterialRequisition(
			MaterialRequisitionVO vo) throws Exception {
		Criteria criteria = materialRequisitionDAO.createCriteria();

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("store", "str", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("materialRequisitionDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productUnit", "unit",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.materialTransferDetails", "transfer",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("transfer.shelfByFromShelfId",
				"shelfByFromShelfId", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("transfer.shelfByShelfId", "shelfByShelfId",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.projectInventory", "projectInventory",
				CriteriaSpecification.LEFT_JOIN);

		if (vo.getImplementation() != null) {
			criteria.add(Restrictions.eq("implementation",
					vo.getImplementation()));
		}

		if (vo.getMaterialRequisitionId() != null
				&& vo.getMaterialRequisitionId() > 0) {
			criteria.add(Restrictions.eq("materialRequisitionId",
					vo.getMaterialRequisitionId()));
		}

		if (vo.getRecordId() != null && vo.getRecordId() > 0) {
			criteria.add(Restrictions.eq("recordId", vo.getRecordId()));
		}

		if (vo.getUseCase() != null && !vo.getUseCase().equals("")) {
			criteria.add(Restrictions.eq("useCase", vo.getUseCase()));
		}

		/*
		 * if (productId != null && productId > 0) {
		 * criteria.add(Restrictions.eq("prd.productId", productId)); }
		 * 
		 * if (storeId != null && storeId > 0) {
		 * criteria.add(Restrictions.eq("str.storeId", storeId)); }
		 * 
		 * if (statusId != null && statusId > 0) {
		 * criteria.add(Restrictions.eq("requisitionStatus", statusId)); }
		 * 
		 * if (fromDate != null && toDate != null) {
		 * criteria.add(Restrictions.between("requisitionDate",fromDate,
		 * toDate)); }
		 */

		return criteria.list();
	}

	// Getters & Setters
	public AIOTechGenericDAO<MaterialRequisition> getMaterialRequisitionDAO() {
		return materialRequisitionDAO;
	}

	public void setMaterialRequisitionDAO(
			AIOTechGenericDAO<MaterialRequisition> materialRequisitionDAO) {
		this.materialRequisitionDAO = materialRequisitionDAO;
	}

	public AIOTechGenericDAO<MaterialRequisitionDetail> getMaterialRequisitionDetailDAO() {
		return materialRequisitionDetailDAO;
	}

	public void setMaterialRequisitionDetailDAO(
			AIOTechGenericDAO<MaterialRequisitionDetail> materialRequisitionDetailDAO) {
		this.materialRequisitionDetailDAO = materialRequisitionDetailDAO;
	}
}
