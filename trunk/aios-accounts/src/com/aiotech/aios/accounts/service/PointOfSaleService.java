package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PointOfSaleService {

	private AIOTechGenericDAO<PointOfSale> pointOfSaleDAO;
	private AIOTechGenericDAO<PointOfSaleDetail> pointOfSaleDetailDAO;
	private AIOTechGenericDAO<PointOfSaleCharge> pointOfSaleChargeDAO;
	private AIOTechGenericDAO<PointOfSaleReceipt> pointOfSaleReceiptDAO;
	private AIOTechGenericDAO<PointOfSaleOffer> pointOfSaleOfferDAO;
	private AIOTechGenericDAO<MerchandiseExchange> merchandiseExchangeDAO;
	private AIOTechGenericDAO<MerchandiseExchangeDetail> merchandiseExchangeDetailDAO;
	private AIOTechGenericDAO<Person> personDAO;

	public List<PointOfSaleDetail> getAllPointOfSaleDetailsByProductId(
			Long productId) {

		return pointOfSaleDetailDAO.findByNamedQuery(
				"getAllPointOfSaleDetailsByProductId", productId, productId);
	}

	public List<PointOfSale> getDispatchedOrders(long posUserTillId,
			boolean dispatchFlag) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getDispatchedOrders",
				posUserTillId, dispatchFlag);
	}

	public List<PointOfSale> getAllPointOfSales(Implementation implementation)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getAllPointOfSales",
				implementation);
	}

	public List<PointOfSale> getAllPointOfSalesData(long pointOfSaleId,
			Implementation implementation) throws Exception {
		int size = 1000;
		return pointOfSaleDAO.findByNamedQuery("getAllPointOfSalesData", size,
				implementation, pointOfSaleId);
	}
	
	public List<PointOfSale> getAllPointOfSalesByDateRange(
			Implementation implementation, Date fromDate, Date toDate) throws Exception {
 		return pointOfSaleDAO.findByNamedQuery("getAllPointOfSalesByDateRange",
				implementation, fromDate, toDate);
	}

	public List<PointOfSale> getAllPointOfSalesAllData(
			Implementation implementation) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getAllPointOfSalesAllData",
				implementation);
	}

	public List<PointOfSale> getAllPointOfSalesWithImplementation()
			throws Exception {
		return pointOfSaleDAO
				.findByNamedQuery("getAllPointOfSalesWithImplementation");
	}

	public List<PointOfSale> getAllPointOfSalesByDate(
			Implementation implementation, Date saleDate1, Date saleDate2)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getAllPointOfSalesByDate",
				implementation, saleDate1, saleDate2);
	}

	// Get All PointOfSales
	@SuppressWarnings("unchecked")
	public List<PointOfSale> getAllPointOfSales(Implementation implementation,
			long pointOfSaleId, byte salesType, String salesDate,
			long customerId) throws Exception {
		if (pointOfSaleId == 0 && salesType == 0
				&& (null == salesDate || ("").equals(salesDate)))
			return pointOfSaleDAO.findByNamedQuery("getAllPointOfSales",
					implementation);
		Criteria pointOfSaleCriteria = pointOfSaleDAO.createCriteria();
		pointOfSaleCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("implementation", implementation)));
		if (salesType > 0) {
			if (customerId > 0) {
				pointOfSaleCriteria.add(Restrictions.conjunction().add(
						Restrictions.eq("customer.customerId", customerId)));
			} else {
				pointOfSaleCriteria.add(Restrictions.conjunction().add(
						Restrictions.eq("customer", null)));
			}

		}
		if (null != salesDate && !("").equals(salesDate))
			pointOfSaleCriteria.add(Restrictions.conjunction().add(
					Restrictions.eq("salesDate", salesDate)));
		if (pointOfSaleId > 0)
			pointOfSaleCriteria.add(Restrictions.conjunction().add(
					Restrictions.eq("pointOfSaleId", pointOfSaleId)));
		pointOfSaleCriteria.createAlias("customer", "c",
				CriteriaSpecification.LEFT_JOIN);
		return (List<PointOfSale>) pointOfSaleCriteria.list();
	}

	public PointOfSale getPointOfDetailByReference(
			Implementation implementation, String salesRefNumber)
			throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleDAO.findByNamedQuery(
				"getPointOfDetailByReference", implementation, salesRefNumber);
		return null != pointOfSales && pointOfSales.size() > 0 ? pointOfSales
				.get(0) : null;
	}

	public List<PointOfSale> getAllPointOfSaleBySession(
			Implementation implementation, long posUserTillId) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleDAO.findByNamedQuery(
				"getAllPointOfSaleBySession", implementation, posUserTillId);
		return pointOfSales;
	}

	public List<PointOfSale> getPointOfSaleBySession(
			Implementation implementation, long posUserTillId) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleDAO.findByNamedQuery(
				"getPointOfSaleBySession", implementation, posUserTillId);
		return pointOfSales;
	}

	public List<PointOfSale> getAllPointOfSaleSavedSession(
			Implementation implementation) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleDAO.findByNamedQuery(
				"getAllPointOfSaleSavedSession", implementation);
		return pointOfSales;
	}

	// Save Point of Sale Receipt
	public void savePointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<PointOfSaleOffer> pointOfSaleOffers) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			pointOfSaleDetail.setPointOfSale(pointOfSale);
		pointOfSaleDetailDAO.saveOrUpdateAll(pointOfSaleDetails);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				pointOfSaleCharge.setPointOfSale(pointOfSale);
			pointOfSaleChargeDAO.saveOrUpdateAll(pointOfSaleCharges);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				pointOfSaleReceipt.setPointOfSale(pointOfSale);
			pointOfSaleReceiptDAO.saveOrUpdateAll(pointOfSaleReceipts);
		}
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0) {
			for (PointOfSaleOffer pointOfSaleOffer : pointOfSaleOffers)
				pointOfSaleOffer.setPointOfSale(pointOfSale);
			pointOfSaleOfferDAO.saveOrUpdateAll(pointOfSaleOffers);
		}
	}

	// Save Point of Sale Receipt
	public void updateDispatchPointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				pointOfSaleReceipt.setPointOfSale(pointOfSale);
			pointOfSaleReceiptDAO.saveOrUpdateAll(pointOfSaleReceipts);
		}
	}

	public void savePOSOrderSession(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			pointOfSaleDetail.setPointOfSale(pointOfSale);
		pointOfSaleDetailDAO.saveOrUpdateAll(pointOfSaleDetails);
	}

	public void updatePointOfSale(List<PointOfSale> pointOfSales)
			throws Exception {
		pointOfSaleDAO.saveOrUpdateAll(pointOfSales);
	}

	// Update PointOfSale Details
	public void savePointOfSaleDetails(
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSaleDetailDAO.saveOrUpdateAll(pointOfSaleDetails);
	}

	// Update EOD PointOfSale
	public void updateEODPointOfSales(List<PointOfSale> pointOfSales)
			throws Exception {
		pointOfSaleDAO.saveOrUpdateAll(pointOfSales);
	}

	// Update EOD PointOfSale
	public void updateEODPointOfSales(List<PointOfSale> pointOfSales,
			Session session) throws Exception {
		for (PointOfSale pointOfSale : pointOfSales)
			session.saveOrUpdate(pointOfSale);
	}

	// Update EOD PointOfSale
	public void updateEODPointOfSales(PointOfSale pointOfSale, Session session)
			throws Exception {
		session.saveOrUpdate(pointOfSale);
	}

	// Get Point of Sales details
	public List<PointOfSaleDetail> getPointOfSaleDetailsByCurrencyMode(
			Implementation implementation, Date fromDate, Date toDate,
			Byte paymentMode) throws Exception {
		return pointOfSaleDetailDAO.findByNamedQuery(
				"getPointOfSaleDetailsByCurrencyMode", implementation,
				fromDate, toDate, paymentMode);
	}

	// Get Point of Sales receipts
	public List<PointOfSaleReceipt> getPointOfSaleReceiptsByCurrencyMode(
			Implementation implementation, Date fromDate, Date toDate,
			Byte paymentMode) throws Exception {
		return pointOfSaleReceiptDAO.findByNamedQuery(
				"getPointOfSaleReceiptsByCurrencyMode", implementation,
				fromDate, toDate, paymentMode);
	}

	// Get Point of Sales receipts by date
	public List<PointOfSaleReceipt> getPointOfSaleReceiptsBySalesDate(
			Implementation implementation, long storeId, Date fromDate,
			Date toDate, byte excludeCoupon) throws Exception {
		return pointOfSaleReceiptDAO.findByNamedQuery(
				"getPointOfSaleReceiptsBySalesDate", implementation, storeId,
				fromDate, toDate, excludeCoupon);
	}

	// Get Point of Sales receipts by date
	public List<PointOfSale> getPointOfSaleReceiptsByEmployeeService(
			Implementation implementation, long storeId, Date fromDate,
			Date toDate, byte salesType) throws Exception {
		return pointOfSaleDAO.findByNamedQuery(
				"getPointOfSaleReceiptsByEmployeeService", implementation,
				storeId, fromDate, toDate, salesType);
	}

	// Get Point of Sales receipts by date
	public List<PointOfSale> getPointOfSaleBySalesDate(
			Implementation implementation, long storeId, Date fromDate,
			Date toDate, byte excludeCoupon) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getPointOfSaleBySalesDate",
				implementation, storeId, fromDate, toDate);
	}

	// Get Point of Sales receipts by date
	public List<PointOfSale> getEODPointOfSaleBySalesDate(
			Implementation implementation, long storeId, Date fromDate,
			Date toDate, byte excludeCoupon) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getEODPointOfSaleBySalesDate",
				implementation, storeId, fromDate, toDate, excludeCoupon);
	}

	// Get UnBalanced PointOfSale by Current Date
	public List<PointOfSale> getUnBalancedPointOfSaleByCurrentDate(
			Implementation implementation, Date fromDate, Date toDate)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery(
				"getUnBalancedPointOfSaleByCurrentDate", implementation,
				fromDate, toDate);
	}

	// Get BalancedPointOfSale by Current Date
	public List<PointOfSale> getBalancedPointOfSaleByCurrentDate(
			Implementation implementation, Date fromDate, Date toDate)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery(
				"getBalancedPointOfSaleByCurrentDate", implementation,
				fromDate, toDate);
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getFilteredPointOfSales(
			Implementation implementation, List<Long> pointOfSaleIds,
			Long userTillId, Long storeId, Long personId, Date fromDate,
			Date toDate, String reportType) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "cust_prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "receipts",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productCategory", "prdCat",
				CriteriaSpecification.LEFT_JOIN);

		if (reportType.equals("pos")) {
			Byte employeeSaleType = 4;
			criteria.add(Restrictions.ne("saleType", employeeSaleType));
		} else if (reportType.equals("employee")) {
			criteria.createAlias("personByEmployeeId", "emp",
					CriteriaSpecification.LEFT_JOIN);
			Byte employeeSaleType = 4;
			criteria.add(Restrictions.eq("saleType", employeeSaleType));
		}

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (pointOfSaleIds != null && !pointOfSaleIds.isEmpty()) {
			criteria.add(Restrictions.in("pointOfSaleId", pointOfSaleIds));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("store.storeId", storeId));
		}

		if (userTillId != null && userTillId > 0) {
			criteria.add(Restrictions.eq("user.posUserTillId", userTillId));
		}

		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("prs.personId", personId));
		}

		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate", DateFormat.getToDate(toDate))));
		}

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getFilteredSalesReport(
			Implementation implementation, Long userTillId, Long personId,
			Long storeId, Date fromDate, Date toDate, Long pointOfSaleId) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "cust_prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "receipts",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productCategory", "prdCat",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("store.storeId",
					Long.valueOf(storeId + "")));
		}

		if (userTillId != null && userTillId > 0) {
			criteria.add(Restrictions.eq("user.posUserTillId", userTillId));
		}

		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("prs.personId", personId));
		}

		if (pointOfSaleId != null && pointOfSaleId > 0) {
			criteria.add(Restrictions.eq("pointOfSaleId", pointOfSaleId));
		}

		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate", DateFormat.getToDate(toDate))));
		}

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getFilteredCustomerPointOfSales(
			Implementation implementation, List<Long> pointOfSaleIds,
			Long customerId, Long storeId, Date fromDate, Date toDate) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.cmpDeptLocation", "cdept",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdept.company", "cdcmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "receipts",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productCategory", "prdCat",
				CriteriaSpecification.LEFT_JOIN);

		Byte employeeSaleType = 4;
		criteria.add(Restrictions.ne("saleType", employeeSaleType));

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (pointOfSaleIds != null && !pointOfSaleIds.isEmpty()) {
			criteria.add(Restrictions.in("pointOfSaleId", pointOfSaleIds));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("store.storeId", storeId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("c.customerId", customerId));
		}

		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate", DateFormat.getToDate(toDate))));
		}
		/*
		 * if (fromDate != null && toDate != null) {
		 * criteria.add(Restrictions.between("salesDate",fromDate, toDate)); }
		 */

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getFilteredDailySales(
			Implementation implementation, Integer orderType, Byte receiptType,
			Date fromDate, Date toDate, Long customerId, Long storeId) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "rec",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("store.storeId", storeId));
		}

		if (receiptType != null && receiptType > 0) {
			criteria.add(Restrictions.eq("rec.receiptType", receiptType));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("c.customerId", customerId));
		}

		if (orderType != null && orderType > 0) {
			if (orderType == 1) { // order type is dine in
				criteria.add(Restrictions.isNotNull("lookupDetail"));
			} else if (orderType == 2) { // order type is take away
				criteria.add(Restrictions.isNull("lookupDetail"));
			} else if (orderType == 3) { // order type is delivery
				criteria.add(Restrictions.isNotNull("shippingDetail"));
			}
		}

		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate", DateFormat.getToDate(toDate))));
		}

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getMonthlyPOSSales(Implementation implementation,
			Date fromDate, Date toDate, Long storeId) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "rec",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate", DateFormat.getToDate(toDate))));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("user.store.storeId", storeId));
		}

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getFilteredPeriodWiseSales(
			Implementation implementation, Date fromDate, Date toDate,
			Long storeId) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "rec",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate", DateFormat.getToDate(toDate))));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("user.store.storeId", storeId));
		}

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSaleDetail> getFilteredCategoryBySales(
			Implementation implementation, Date fromDate, Date toDate,
			Long storeId) {
		Criteria criteria = pointOfSaleDetailDAO.createCriteria();
		criteria.createAlias("pointOfSale", "pos",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pos.POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("pos.implementation", implementation));
		criteria.createAlias("productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.productCategory", "pc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pc.productCategory", "pcParent",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("pos.implementation", implementation));
		}
		if (fromDate != null && toDate != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("pos.salesDate",
							DateFormat.getFromDate(fromDate))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("pos.salesDate",
							DateFormat.getToDate(toDate))));
		}
		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("user.store.storeId", storeId));
		}
		Set<PointOfSaleDetail> uniqueRecords = new HashSet<PointOfSaleDetail>(
				criteria.list());
		List<PointOfSaleDetail> pointOfSales = new ArrayList<PointOfSaleDetail>(
				uniqueRecords);

		return pointOfSales;
	}

	public PointOfSale getPointOfDetailByOrderId(long pointOfSaleId)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getPointOfDetailByOrderId",
				pointOfSaleId).get(0);
	}

	public PointOfSale getPointOfDetailsProductByOrderId(long pointOfSaleId)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery(
				"getPointOfDetailsProductByOrderId", pointOfSaleId).get(0);
	}

	public PointOfSale findByPointOfSaleId(long pointOfSaleId) throws Exception {
		return pointOfSaleDAO.findById(pointOfSaleId);
	}

	public PointOfSale findByPointOfSaleBySessionValue(String posSessionValue)
			throws Exception {
		List<PointOfSale> posList = pointOfSaleDAO.findByNamedQuery(
				"findByPointOfSaleBySessionValue", posSessionValue);
		if (posList != null && posList.size() > 0)
			return posList.get(0);
		else
			return null;
	}

	public List<PointOfSale> getPointOfSalesByStore(
			Implementation implementation, long storeId, byte itemSubType)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getPointOfSalesByStore",
				implementation, storeId, itemSubType);
	}

	public List<PointOfSale> getPointOfSalesWithoutStore(
			Implementation implementation, byte itemSubType) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getPointOfSalesWithoutStore",
				implementation, itemSubType);
	}

	public PointOfSale getAllPointOfDetailByOrderId(long pointOfSaleId)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getAllPointOfDetailByOrderId",
				pointOfSaleId).get(0);
	}

	public PointOfSale getDispacthPointOfDetailByOrderId(long pointOfSaleId)
			throws Exception {
		return pointOfSaleDAO.findByNamedQuery(
				"getDispacthPointOfDetailByOrderId", pointOfSaleId).get(0);
	}

	public List<PointOfSale> getAllOfflinePOSTransactions(
			Implementation implementation) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getAllOfflinePOSTransactions",
				implementation);
	}

	public List<PointOfSale> getOfflinePOSTransactions(
			Implementation implementation) throws Exception {
		return pointOfSaleDAO.findByNamedQuery("getOfflinePOSTransactions",
				implementation);
	}

	public List<PointOfSaleDetail> getAllPointOfSaleDetailsByOrderId(
			long pointOfSaleId) throws Exception {
		return pointOfSaleDetailDAO.findByNamedQuery(
				"getAllPointOfSaleDetailsByOrderId", pointOfSaleId);
	}

	public PointOfSaleDetail getPointOfDetailByProductId(long productId,
			long pointOfSaleId) throws Exception {
		return pointOfSaleDetailDAO.findByNamedQuery(
				"getPointOfDetailByProductId", productId, pointOfSaleId).get(0);
	}

	public PointOfSaleDetail getPointOfDetailByDetailId(long pointOfSaleDetailId)
			throws Exception {
		return pointOfSaleDetailDAO.findByNamedQuery(
				"getPointOfDetailByDetailId", pointOfSaleDetailId).get(0);
	}

	public void deleteSessionPOSOrder(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<PointOfSaleOffer> pointOfSaleOffers) throws Exception {
		pointOfSaleDetailDAO.deleteAll(pointOfSaleDetails);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			pointOfSaleChargeDAO.deleteAll(pointOfSaleCharges);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			pointOfSaleReceiptDAO.deleteAll(pointOfSaleReceipts);
		}
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0) {
			pointOfSaleOfferDAO.deleteAll(pointOfSaleOffers);
		}
		pointOfSaleDAO.delete(pointOfSale);
	}

	// Save Point of Sale Receipt
	public void savePointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			pointOfSaleDetail.setPointOfSale(pointOfSale);
		pointOfSaleDetailDAO.saveOrUpdateAll(pointOfSaleDetails);

	}

	public void deleteSessionPOSOrder(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSaleDetailDAO.deleteAll(pointOfSaleDetails);
		pointOfSaleDAO.delete(pointOfSale);
	}

	public PointOfSale fetchSalesInformationByUseCase(Long recordId,
			String useCase, Implementation implementation) throws Exception {
		List<PointOfSale> pointOfSaless = pointOfSaleDAO.findByNamedQuery(
				"fetchSalesInformationByUseCase", recordId, useCase,
				implementation);
		if (pointOfSaless != null && pointOfSaless.size() > 0)
			return pointOfSaless.get(0);
		else
			return null;

	}

	public void updatePointOfSaleDetail(PointOfSaleDetail pointOfSaleDetail)
			throws Exception {
		pointOfSaleDetailDAO.saveOrUpdate(pointOfSaleDetail);
	}

	public void deletePointOfSaleDetail(PointOfSaleDetail pointOfSaleDetail)
			throws Exception {
		pointOfSaleDetailDAO.delete(pointOfSaleDetail);
	}

	public void deletePointOfSaleDetails(
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSaleDetailDAO.deleteAll(pointOfSaleDetails);
	}

	public void deletePointOfSaleCharges(
			List<PointOfSaleCharge> pointOfSaleCharges) throws Exception {
		pointOfSaleChargeDAO.deleteAll(pointOfSaleCharges);
	}

	public void deletePointOfSaleOffers(List<PointOfSaleOffer> pointOfSaleOffers)
			throws Exception {
		pointOfSaleOfferDAO.deleteAll(pointOfSaleOffers);
	}

	public void deletePointOfSaleReceipts(
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		pointOfSaleReceiptDAO.deleteAll(pointOfSaleReceipts);
	}

	public void deletePointOfSale(PointOfSale pointOfSale) throws Exception {
		pointOfSaleDAO.delete(pointOfSale);
	}

	public void deletePointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0)
			pointOfSaleDetailDAO.deleteAll(pointOfSaleDetails);
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0)
			pointOfSaleReceiptDAO.deleteAll(pointOfSaleReceipts);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0)
			pointOfSaleChargeDAO.deleteAll(pointOfSaleCharges);
		pointOfSaleDAO.delete(pointOfSale);
	}

	public void savePointOfSale(PointOfSale pointOfSale) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
	}

	public void savePointOfSale(List<PointOfSale> pointOfSales)
			throws Exception {
		pointOfSaleDAO.saveOrUpdateAll(pointOfSales);
	}

	public void updatePointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			pointOfSaleDetail.setPointOfSale(pointOfSale);
		pointOfSaleDetailDAO.saveOrUpdateAll(pointOfSaleDetails);
	}

	public void dispatchPointOfSale(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			pointOfSaleDetail.setPointOfSale(pointOfSale);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				pointOfSaleCharge.setPointOfSale(pointOfSale);
			pointOfSaleChargeDAO.saveOrUpdateAll(pointOfSaleCharges);
		}
		pointOfSaleDetailDAO.saveOrUpdateAll(pointOfSaleDetails);
	}

	public void deleteOfflinePointOfSales(List<PointOfSale> pointOfSales,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0)
			pointOfSaleChargeDAO.deleteAll(pointOfSaleCharges);
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0)
			pointOfSaleOfferDAO.deleteAll(pointOfSaleOffers);
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0)
			pointOfSaleReceiptDAO.deleteAll(pointOfSaleReceipts);
		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0)
			pointOfSaleDetailDAO.deleteAll(pointOfSaleDetails);
		if (null != pointOfSales && pointOfSales.size() > 0)
			pointOfSaleDAO.deleteAll(pointOfSales);
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getFilteredPaymentsByCustomer(
			SalesInvoiceVO salesInvoiceVO) {
		Criteria criteria = pointOfSaleDAO.createCriteria();

		criteria.add(Restrictions.eq("implementation",
				salesInvoiceVO.getImplementation()));
		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "user",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("user.store", "store",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "charge",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charge.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productByProductId", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleReceipts", "rec",
				CriteriaSpecification.LEFT_JOIN);

		if (salesInvoiceVO.getFormDate() != null
				&& salesInvoiceVO.getToDate() != null) {

			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("salesDate", DateFormat
							.getFromDate(salesInvoiceVO.getFormDate()))));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("salesDate",
							DateFormat.getToDate(salesInvoiceVO.getToDate()))));
		}

		if (salesInvoiceVO.getCustomerId() != null
				&& salesInvoiceVO.getCustomerId() > 0) {
			criteria.add(Restrictions.eq("c.customerId",
					salesInvoiceVO.getCustomerId()));
		}

		criteria.add(Restrictions.isNotNull("c.customerId"));

		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);

		return pointOfSales;
	}

	// Getters & Setters
	public AIOTechGenericDAO<PointOfSale> getPointOfSaleDAO() {
		return pointOfSaleDAO;
	}

	public void setPointOfSaleDAO(AIOTechGenericDAO<PointOfSale> pointOfSaleDAO) {
		this.pointOfSaleDAO = pointOfSaleDAO;
	}

	public AIOTechGenericDAO<PointOfSaleDetail> getPointOfSaleDetailDAO() {
		return pointOfSaleDetailDAO;
	}

	public void setPointOfSaleDetailDAO(
			AIOTechGenericDAO<PointOfSaleDetail> pointOfSaleDetailDAO) {
		this.pointOfSaleDetailDAO = pointOfSaleDetailDAO;
	}

	public AIOTechGenericDAO<PointOfSaleCharge> getPointOfSaleChargeDAO() {
		return pointOfSaleChargeDAO;
	}

	public void setPointOfSaleChargeDAO(
			AIOTechGenericDAO<PointOfSaleCharge> pointOfSaleChargeDAO) {
		this.pointOfSaleChargeDAO = pointOfSaleChargeDAO;
	}

	public AIOTechGenericDAO<PointOfSaleReceipt> getPointOfSaleReceiptDAO() {
		return pointOfSaleReceiptDAO;
	}

	public void setPointOfSaleReceiptDAO(
			AIOTechGenericDAO<PointOfSaleReceipt> pointOfSaleReceiptDAO) {
		this.pointOfSaleReceiptDAO = pointOfSaleReceiptDAO;
	}

	public AIOTechGenericDAO<PointOfSaleOffer> getPointOfSaleOfferDAO() {
		return pointOfSaleOfferDAO;
	}

	public void setPointOfSaleOfferDAO(
			AIOTechGenericDAO<PointOfSaleOffer> pointOfSaleOfferDAO) {
		this.pointOfSaleOfferDAO = pointOfSaleOfferDAO;
	}

	public void savePointOfSales(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			MerchandiseExchange merchandiseExchange,
			List<MerchandiseExchangeDetail> exchangeDetails, Session session)
			throws Exception {
		session.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			session.saveOrUpdate(pointOfSaleDetail);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				session.saveOrUpdate(pointOfSaleCharge);
		}
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0) {
			for (PointOfSaleOffer pointOfSaleOffer : pointOfSaleOffers)
				session.saveOrUpdate(pointOfSaleOffer);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				session.saveOrUpdate(pointOfSaleReceipt);
		}
		if (null != merchandiseExchange) {
			session.saveOrUpdate(merchandiseExchange);
			for (MerchandiseExchangeDetail merchandiseExchangeDetail : exchangeDetails) {
				merchandiseExchangeDetail
						.setMerchandiseExchange(merchandiseExchange);
				session.saveOrUpdate(merchandiseExchangeDetail);
			}
		}
	}

	public void savePointOfSales(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			MerchandiseExchange merchandiseExchange,
			List<MerchandiseExchangeDetail> exchangeDetails) throws Exception {
		pointOfSaleDAO.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			pointOfSaleDetailDAO.saveOrUpdate(pointOfSaleDetail);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				pointOfSaleChargeDAO.saveOrUpdate(pointOfSaleCharge);
		}
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0) {
			for (PointOfSaleOffer pointOfSaleOffer : pointOfSaleOffers)
				pointOfSaleOfferDAO.saveOrUpdate(pointOfSaleOffer);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				pointOfSaleReceiptDAO.saveOrUpdate(pointOfSaleReceipt);
		}
		if (null != merchandiseExchange) {
			merchandiseExchangeDAO.saveOrUpdate(merchandiseExchange);
			for (MerchandiseExchangeDetail merchandiseExchangeDetail : exchangeDetails) {
				merchandiseExchangeDetail
						.setMerchandiseExchange(merchandiseExchange);
				merchandiseExchangeDetailDAO
						.saveOrUpdate(merchandiseExchangeDetail);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSale> getPointOfSaleForEODBalance(
			Implementation implementation, String date, long storeId)
			throws Exception {
		Criteria criteria = pointOfSaleDAO.createCriteria();
		criteria.createAlias("pointOfSaleReceipts", "psr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleDetails", "psd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleOffers", "pso",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pointOfSaleCharges", "psc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "till",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("till.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("implementation", implementation));
		if (null != date && !("").equals(date))
			criteria.add(Restrictions.ge("pos.salesDate",
					DateFormat.convertStringToDate(date)));
		if (storeId > 0) {
			criteria.add(Restrictions.eq("str.storeId", storeId));
		}
		Set<PointOfSale> uniqueRecords = new HashSet<PointOfSale>(
				criteria.list());
		List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>(
				uniqueRecords);
		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public List<PointOfSaleReceipt> getNonDepositedReceipts(Byte cashMode,
			Byte chequeMode, Byte cardMode, Implementation implementation,
			String dateFrom, String dateTo, long storeId, byte paymentMode)
			throws Exception {
		Criteria criteria = pointOfSaleReceiptDAO.createCriteria();
		criteria.createAlias("pointOfSale", "pos",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pos.POSUserTill", "till",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("till.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("pos.implementation", implementation));
		criteria.add(Restrictions.or(Restrictions.eq("isDeposited", false),
				Restrictions.isNull("isDeposited")));
		if (paymentMode > 0)
			criteria.add(Restrictions.eq("receiptType", paymentMode));
		else
			criteria.add(Restrictions.or(Restrictions.or(
					Restrictions.eq("receiptType", cashMode),
					Restrictions.eq("receiptType", chequeMode)), Restrictions
					.eq("receiptType", cardMode)));
		if (null != dateFrom && !("").equals(dateFrom))
			criteria.add(Restrictions.ge("pos.salesDate",
					DateFormat.convertStringToDate(dateFrom)));
		if (null != dateTo && !("").equals(dateTo))
			criteria.add(Restrictions.le("pos.salesDate",
					DateFormat.convertStringToDate(dateTo)));
		if (storeId > 0) {
			criteria.add(Restrictions.eq("str.storeId", storeId));
		}
		Set<PointOfSaleReceipt> uniqueRecords = new HashSet<PointOfSaleReceipt>(
				criteria.list());
		List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>(
				uniqueRecords);

		return pointOfSaleReceipts;
	}

	public void savePointOfSaleCharges(
			List<PointOfSaleCharge> pointOfSaleCharges) throws Exception {
		pointOfSaleChargeDAO.saveOrUpdateAll(pointOfSaleCharges);
	}

	public PointOfSaleReceipt getPointOfSaleReceiptById(
			Long pointOfSaleReceiptId) throws Exception {
		return pointOfSaleReceiptDAO.findById(pointOfSaleReceiptId);
	}

	public void updatePointOfSaleReceipts(
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		pointOfSaleReceiptDAO.saveOrUpdateAll(pointOfSaleReceipts);
	}

	public void mergePointOfSaleReceipts(
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts) {
			pointOfSaleReceiptDAO.merge(pointOfSaleReceipt);
		}
	}

	public void savePerson(Person person, Session session) throws Exception {
		session.saveOrUpdate(person);
	}

	public void savePerson(Person person) throws Exception {
		personDAO.saveOrUpdate(person);
	}

	public AIOTechGenericDAO<MerchandiseExchange> getMerchandiseExchangeDAO() {
		return merchandiseExchangeDAO;
	}

	public void setMerchandiseExchangeDAO(
			AIOTechGenericDAO<MerchandiseExchange> merchandiseExchangeDAO) {
		this.merchandiseExchangeDAO = merchandiseExchangeDAO;
	}

	public AIOTechGenericDAO<MerchandiseExchangeDetail> getMerchandiseExchangeDetailDAO() {
		return merchandiseExchangeDetailDAO;
	}

	public void setMerchandiseExchangeDetailDAO(
			AIOTechGenericDAO<MerchandiseExchangeDetail> merchandiseExchangeDetailDAO) {
		this.merchandiseExchangeDetailDAO = merchandiseExchangeDetailDAO;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}
}
