package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.accounts.service.GoodsReturnService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class GoodsReturnBL {
	private GoodsReturnService goodsReturnService;
	private DebitBL debitBL;
	private PurchaseBL purchaseBL;
	private List<Long> purchaseInfo = new ArrayList<Long>();
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public JSONObject getGoodsReturn(Implementation implementation)
			throws Exception {
		List<GoodsReturn> goodsList = getGoodsReturnService()
				.getAllGoodsReturn(implementation);
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		if (null != goodsList && goodsList.size() > 0) {
			JSONArray array = null;
			for (GoodsReturn list : goodsList) {
				array = new JSONArray();
				array.add(list.getGoodsReturnId());
				array.add(list.getReturnNumber());
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(list.getSupplier().getSupplierNumber());
				if (null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson())) {
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				} else {
					array.add(null != list.getSupplier().getCmpDeptLocation() ? list
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				}

				if (list.getReceive() != null) {
					array.add(list.getReceive().getReceiveNumber());
					array.add(true);

				} else {
					array.add("");
					array.add(false);
				}

				if (list.getDebits() != null && list.getDebits().size() > 0) {
					array.add(true);

				} else {
					array.add(false);
				}
				data.add(array);
			}
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public List<Object> getActiveGoodsReturn(Implementation implementation)
			throws Exception {
		List<GoodsReturn> goodsReturns = goodsReturnService
				.getActiveGoodsReturn(implementation);
		List<Object> goodsReturnVOs = new ArrayList<Object>();
		if (null != goodsReturns && goodsReturns.size() > 0) {
			for (GoodsReturn goodsReturn : goodsReturns)
				goodsReturnVOs.add(addGoodsReturnVO(goodsReturn));
		}
		return goodsReturnVOs;
	}

	public GoodsReturnVO addGoodsReturnVO(GoodsReturn goodsReturns)
			throws Exception {
		GoodsReturnVO goodsReturnVO = new GoodsReturnVO();
		goodsReturnVO.setGoodsReturnId(goodsReturns.getGoodsReturnId());
		goodsReturnVO.setReturnNumber(goodsReturns.getReturnNumber());
		goodsReturnVO.setGoodsReturnDate(DateFormat
				.convertDateToString(goodsReturns.getDate().toString()));
		goodsReturnVO.setIsDebit(goodsReturns.getIsDebit());
		if (null != goodsReturns.getSupplier().getPerson())
			goodsReturnVO.setSupplierName(goodsReturns
					.getSupplier()
					.getPerson()
					.getFirstName()
					.concat(" ")
					.concat(goodsReturns.getSupplier().getPerson()
							.getLastName()));
		else
			goodsReturnVO.setSupplierName(null != goodsReturns.getSupplier()
					.getCmpDeptLocation() ? goodsReturns.getSupplier()
					.getCmpDeptLocation().getCompany().getCompanyName()
					: goodsReturns.getSupplier().getCompany().getCompanyName());

		return goodsReturnVO;
	}

	public GoodsReturnVO addGoodsReturnDetailVO(GoodsReturn goodsReturns)
			throws Exception {
		GoodsReturnVO goodsReturnVO = new GoodsReturnVO();
		goodsReturnVO.setGoodsReturnId(goodsReturns.getGoodsReturnId());
		goodsReturnVO.setReturnNumber(goodsReturns.getReturnNumber());
		goodsReturnVO.setGoodsReturnDate(DateFormat
				.convertDateToString(goodsReturns.getDate().toString()));
		goodsReturnVO.setIsDebit(goodsReturns.getIsDebit());
		if (null != goodsReturns.getSupplier().getPerson())
			goodsReturnVO.setSupplierName(goodsReturns
					.getSupplier()
					.getPerson()
					.getFirstName()
					.concat(" ")
					.concat(goodsReturns.getSupplier().getPerson()
							.getLastName()));
		else
			goodsReturnVO.setSupplierName(null != goodsReturns.getSupplier()
					.getCmpDeptLocation() ? goodsReturns.getSupplier()
					.getCmpDeptLocation().getCompany().getCompanyName()
					: goodsReturns.getSupplier().getCompany().getCompanyName());
		goodsReturnVO.setSupplierId(goodsReturns.getSupplier().getSupplierId());
		List<GoodsReturnDetailVO> returnDetails = new ArrayList<GoodsReturnDetailVO>();
		GoodsReturnDetailVO returnDetailVO = null;
		double unitRate = 0;
		double netAmount = 0;
		Set<String> purchaseNoSet = new HashSet<String>();
		Set<String> purchaseDateSet = new HashSet<String>();
		for (GoodsReturnDetail goodsReturnDetail : goodsReturns
				.getGoodsReturnDetails()) {
			returnDetailVO = new GoodsReturnDetailVO();
			BeanUtils.copyProperties(returnDetailVO, goodsReturnDetail);
			unitRate = 0;
			if (null != goodsReturnDetail.getPurchase()) {
				for (ReceiveDetail receiveDetail : goodsReturnDetail
						.getPurchase().getReceiveDetails()) {
					if ((long) receiveDetail.getProduct().getProductId() == (long) goodsReturnDetail
							.getProduct().getProductId()) {
						unitRate = receiveDetail.getUnitRate();
						returnDetailVO.setUnitRate(AIOSCommons
								.formatAmount(unitRate));

						returnDetailVO.setReceiveQty(receiveDetail
								.getPackageUnit());
						returnDetailVO.setReceiveDetail(receiveDetail);
						break;
					}
					purchaseNoSet.add(receiveDetail.getPurchase()
							.getPurchaseNumber());
					purchaseDateSet.add(DateFormat
							.convertDateToString(receiveDetail.getPurchase()
									.getDate() + ""));
				}
			} else {
				unitRate = goodsReturnDetail.getReceiveDetail().getUnitRate();
				returnDetailVO.setUnitRate(AIOSCommons.formatAmount(unitRate));
				returnDetailVO.setReceiveQty(goodsReturnDetail
						.getReceiveDetail().getPackageUnit());

				returnDetailVO.setReceiveDetail(goodsReturnDetail
						.getReceiveDetail());
			}
			returnDetailVO.setProductName(goodsReturnDetail.getProduct()
					.getProductName());
			returnDetailVO.setProductCode(goodsReturnDetail.getProduct()
					.getCode());
			returnDetailVO.setProductPackageVOs(purchaseBL
					.getPackageConversionBL().getProductPackagingDetail(
							goodsReturnDetail.getProduct().getProductId()));
			if (null != goodsReturnDetail.getProductPackageDetail()) {
				returnDetailVO.setPackageDetailId(goodsReturnDetail
						.getProductPackageDetail().getProductPackageDetailId());
				returnDetailVO.setBaseUnitName(goodsReturnDetail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());
				boolean iflag = true;
				for (ProductPackageVO packagevo : returnDetailVO
						.getProductPackageVOs()) {
					if (iflag) {
						for (ProductPackageDetailVO packageDtvo : packagevo
								.getProductPackageDetailVOs()) {
							if ((long) packageDtvo.getProductPackageDetailId() == (long) goodsReturnDetail
									.getProductPackageDetail()
									.getProductPackageDetailId()) {
								returnDetailVO
										.setConversionUnitName(packageDtvo
												.getConversionUnitName());
								iflag = true;
								break;
							}
						}
					} else
						break;
				}
				returnDetailVO.setBaseQuantity(String.valueOf(returnDetailVO
						.getReturnQty()));
			} else {
				returnDetailVO.setBaseQuantity("");
				returnDetailVO.setConversionUnitName("");
				returnDetailVO.setPackageUnit(goodsReturnDetail
						.getPackageUnit());
				returnDetailVO.setPackageDetailId(-1l);
			}
			returnDetailVO.setUnitNameStr(null != goodsReturnDetail
					.getProductPackageDetail() ? goodsReturnDetail
					.getProductPackageDetail().getLookupDetail()
					.getDisplayName() : goodsReturnDetail.getProduct()
					.getLookupDetailByProductUnit().getDisplayName());
			returnDetailVO.setPackageUnit(null != goodsReturnDetail
					.getPackageUnit() ? goodsReturnDetail.getPackageUnit()
					: goodsReturnDetail.getReturnQty());
			if (unitRate > 1)
				returnDetailVO.setUnitRateStr(AIOSCommons
						.formatAmount(unitRate));
			else
				returnDetailVO.setUnitRateStr(String.valueOf(unitRate));
			returnDetailVO.setTotalAmount(AIOSCommons.formatAmount(unitRate
					* returnDetailVO.getPackageUnit()));
			netAmount += unitRate * goodsReturnDetail.getPackageUnit();
			returnDetails.add(returnDetailVO);
		}
		goodsReturnVO.setGoodsReturnDetailVOs(returnDetails);
		if (goodsReturns.getReceive() != null
				&& goodsReturns.getReceive().getReceiveDetails() != null) {
			for (ReceiveDetail receiveDetail : goodsReturns.getReceive()
					.getReceiveDetails()) {
				purchaseNoSet.add(receiveDetail.getPurchaseDetail()
						.getPurchase().getPurchaseNumber());
			}
			goodsReturnVO.setReceive(goodsReturns.getReceive());
		}
		StringBuilder purchaseNumber = new StringBuilder();

		for (String purchaseNo : purchaseNoSet)
			purchaseNumber.append(purchaseNo).append(" ,");
		goodsReturnVO.setPurchaseNumber(purchaseNumber.toString());

		StringBuilder purchaseDate = new StringBuilder();
		for (String dt : purchaseDateSet)
			purchaseDate.append(dt).append(" ,");
		goodsReturnVO.setPurchaseDate(purchaseDate.toString());

		String decimalAmount = String.valueOf(AIOSCommons
				.formatAmount(netAmount));
		decimalAmount = decimalAmount
				.substring(decimalAmount.lastIndexOf('.') + 1);
		String amountInWords = AIOSCommons.convertAmountToWords(netAmount);
		if (Double.parseDouble(decimalAmount) > 0) {
			amountInWords = amountInWords.concat(" and ")
					.concat(AIOSCommons.convertAmountToWords(decimalAmount))
					.concat(" Fils ");
			String replaceWord = "Only";
			amountInWords = amountInWords.replaceAll(replaceWord, "");
			amountInWords = amountInWords.concat(" " + replaceWord);
		}

		goodsReturnVO
				.setDisplayTotalAmount(AIOSCommons.formatAmount(netAmount));
		goodsReturnVO.setDisplayTotalAmountInWords(amountInWords);
		return goodsReturnVO;
	}

	public Boolean validateReturnGoods(List<GoodsReturnDetail> returnDetails,
			Map<Long, PurchaseVO> recoredInv) throws Exception {
		Map<Long, PurchaseDetailVO> outstandinMap = new HashMap<Long, PurchaseDetailVO>();
		Map<Long, PurchaseDetailVO> purchaseDetailMap = new HashMap<Long, PurchaseDetailVO>();
		List<ReceiveDetail> receiveDetails = new ArrayList<ReceiveDetail>();
		PurchaseDetailVO purchaseDetailVO = null;
		for (GoodsReturnDetail list : returnDetails) {
			if (outstandinMap.containsKey(list.getProduct().getProductId())) {
				purchaseDetailVO = outstandinMap.get(list.getProduct()
						.getProductId());
				purchaseDetailVO
						.setOutStandingQty(list.getReturnQty()
								+ (null != purchaseDetailVO
										&& null != purchaseDetailVO
												.getOutStandingQty() ? purchaseDetailVO
										.getOutStandingQty() : 0));
				outstandinMap.put(list.getProduct().getProductId(),
						purchaseDetailVO);
			} else {
				purchaseDetailVO = new PurchaseDetailVO();
				purchaseDetailVO.setOutStandingQty(list.getReturnQty());
				outstandinMap.put(list.getProduct().getProductId(),
						purchaseDetailVO);
			}
		}
		for (PurchaseVO list : recoredInv.values()) {
			for (PurchaseDetailVO detail : list.getPurchaseDetailVO()) {
				if (outstandinMap.containsKey(detail.getProduct()
						.getProductId())) {
					PurchaseDetailVO detailVO = purchaseDetailMap.get(detail
							.getProduct().getProductId());
					if (null != detailVO) {
						detailVO.setPurchaseQty(detail.getQuantity()
								+ (null != detailVO
										&& null != detailVO.getPurchaseQty() ? detailVO
										.getPurchaseQty() : 0));
					} else {
						detailVO = new PurchaseDetailVO();
						detailVO.setPurchaseQty(detail.getQuantity());
					}
					detailVO.setPurchase(list);
					purchaseDetailMap.put(detail.getProduct().getProductId(),
							detailVO);
				}
			}
			receiveDetails.addAll(list.getReceiveDetails());
		}
		for (ReceiveDetail list : receiveDetails) {
			if (outstandinMap.containsKey(list.getProduct().getProductId())) {
				PurchaseDetailVO detailVO = outstandinMap.get(list.getProduct()
						.getProductId());
				detailVO.setOutStandingQty(list.getReceiveQty()
						+ (null != detailVO
								&& null != detailVO.getOutStandingQty() ? detailVO
								.getOutStandingQty() : 0));
				outstandinMap.put(list.getProduct().getProductId(), detailVO);
			}
		}
		for (Map.Entry<Long, PurchaseDetailVO> availInv : outstandinMap
				.entrySet()) {
			PurchaseDetailVO purchaseInv = purchaseDetailMap.get(availInv
					.getKey());
			PurchaseDetailVO outStandinInv = outstandinMap.get(availInv
					.getKey());
			if (outStandinInv.getOutStandingQty() > purchaseInv
					.getPurchaseQty())
				return false;
			else if (outStandinInv.getOutStandingQty() < purchaseInv
					.getPurchaseQty())
				purchaseInfo.add(purchaseInv.getPurchase().getPurchaseId());
		}
		return true;
	}

	public List<GoodsReturnDetail> getSimilarReturns(
			List<GoodsReturnDetail> returnDetailList,
			List<GoodsReturnDetail> modifiedList) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		List<GoodsReturnDetail> deleteDetailList = new ArrayList<GoodsReturnDetail>();
		Map<Long, GoodsReturnDetail> hashReturnDetail = new HashMap<Long, GoodsReturnDetail>();
		if (null != returnDetailList && returnDetailList.size() > 0) {
			for (GoodsReturnDetail list : returnDetailList) {
				listOne.add(list.getReturnDetailId());
				hashReturnDetail.put(list.getReturnDetailId(), list);
			}
			if (null != hashReturnDetail && hashReturnDetail.size() > 0) {
				for (GoodsReturnDetail list : modifiedList) {
					listTwo.add(list.getReturnDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		if (null != different && different.size() > 0) {
			GoodsReturnDetail returnDetail = null;
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					returnDetail = new GoodsReturnDetail();
					returnDetail = hashReturnDetail.get(list);
					deleteDetailList.add(returnDetail);
				}
			}
		}
		return deleteDetailList;
	}

	public void deleteGoodsReturn(GoodsReturn goodsReturn,
			List<GoodsReturnDetail> goodsReturnDetails, List<Purchase> purchases)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				GoodsReturn.class.getSimpleName(),
				goodsReturn.getGoodsReturnId(), user, workflowDetailVo);
	}

	public void doGoodsReturnBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		GoodsReturn goodsReturn = goodsReturnService.getGoodsReturns(recordId);
		List<GoodsReturnDetail> goodsReturnDetails = new ArrayList<GoodsReturnDetail>(
				goodsReturn.getGoodsReturnDetails());
		if (workflowDetailVO.isDeleteFlag()) {
			List<Stock> addStocks = null;
			if (goodsReturn.getIsDebit()) {
				// Add Stock
				addStocks = new ArrayList<Stock>();
				for (GoodsReturnDetail goodsReturnDetail : goodsReturnDetails)
					addStocks.add(addStockDetails(goodsReturnDetail));
				// Delete Transaction
				purchaseBL.getTransactionBL().deleteTransaction(
						goodsReturn.getGoodsReturnId(),
						GoodsReturn.class.getSimpleName());
			}
			goodsReturnService.deleteGoodsReturns(goodsReturn,
					goodsReturnDetails);
		}
	}

	public void directGoodsReturnDelete(GoodsReturn goodsReturn,
			List<GoodsReturnDetail> goodsReturnDetails) throws Exception {
		goodsReturnService.deleteGoodsReturns(goodsReturn, goodsReturnDetails);
	}

	public void saveReturns(GoodsReturn goodsReturn, List<Purchase> purchases)
			throws Exception {
		goodsReturn.setImplementation(getImplementation());
		if (null == goodsReturn.getGoodsReturnId())
			SystemBL.saveReferenceStamp(GoodsReturn.class.getName(),
					getImplementation());
		goodsReturn.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (goodsReturn != null && goodsReturn.getGoodsReturnId() != null
				&& goodsReturn.getGoodsReturnId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		goodsReturnService.saveReturns(
				goodsReturn,
				new ArrayList<GoodsReturnDetail>(goodsReturn
						.getGoodsReturnDetails()));
		if (null != purchases && purchases.size() > 0)
			purchaseBL.getPurchaseService().mergePurchaseOrder(purchases);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				GoodsReturn.class.getSimpleName(),
				goodsReturn.getGoodsReturnId(), user, workflowDetailVo);
	}

	public void saveReturns(GoodsReturn goodsReturn,
			List<GoodsReturnDetail> goodsReturnDetails,
			List<GoodsReturnDetail> goodsReturnDelete, List<Purchase> purchases)
			throws Exception {
		goodsReturn.setImplementation(getImplementation());
		goodsReturn.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		if (null == goodsReturn.getGoodsReturnId())
			SystemBL.saveReferenceStamp(GoodsReturn.class.getName(),
					getImplementation());
		if (null != goodsReturnDelete && goodsReturnDelete.size() > 0)
			goodsReturnService.deleteGoodsReturnDetail(goodsReturnDelete);
		goodsReturnService.saveReturns(goodsReturn, goodsReturnDetails);
		if (null != purchases && purchases.size() > 0)
			purchaseBL.getPurchaseService().mergePurchaseOrder(purchases);
	}

	public void saveGoodsReturns(GoodsReturn goodsReturn,
			List<GoodsReturnDetail> goodsReturnDetails,
			List<GoodsReturnDetail> goodsReturnDelete) throws Exception {
		goodsReturn.setImplementation(getImplementation());
		boolean updateFlag = false;
		goodsReturn.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		// Add Stocks
		List<Stock> addStocks = null;
		if (null != goodsReturn.getGoodsReturnId())
			updateFlag = true;
		if (updateFlag) {
			if (null != goodsReturnDelete && goodsReturnDelete.size() > 0) {
				addStocks = new ArrayList<Stock>();
				for (GoodsReturnDetail goodsReturnDetail : goodsReturnDelete)
					addStocks.add(addStockDetails(goodsReturnDetail));
				goodsReturnService.deleteGoodsReturnDetail(goodsReturnDelete);
			}
			// Reverse Transaction
			purchaseBL.getTransactionBL().deleteTransaction(
					goodsReturn.getGoodsReturnId(),
					GoodsReturn.class.getSimpleName());
		} else
			SystemBL.saveReferenceStamp(GoodsReturn.class.getName(),
					getImplementation());
		// Deduct Stocks
		List<Stock> deductStocks = new ArrayList<Stock>();
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();

		Supplier supplier = purchaseBL.getSupplierService()
				.getSupplierCombination(
						goodsReturn.getSupplier().getSupplierId());
		for (GoodsReturnDetail goodsReturnDetail : goodsReturnDetails) {
			deductStocks.add(addStockDetails(goodsReturnDetail));
			transactionDetails.addAll(createTransactionDetails(supplier,
					goodsReturnDetail));
		}
		purchaseBL.getStockBL().updateStockDetails(addStocks, deductStocks);
		goodsReturnService.saveReturns(goodsReturn, goodsReturnDetails);
		// Transaction
		Transaction transaction = purchaseBL.getTransactionBL()
				.createTransaction(
						purchaseBL.getTransactionBL().getCurrency()
								.getCurrencyId(),
						null,
						goodsReturn.getDate(),
						purchaseBL.getTransactionBL().getCategory(
								"Goods Returns"),
						goodsReturn.getGoodsReturnId(),
						GoodsReturn.class.getSimpleName());
		purchaseBL.getTransactionBL().saveTransaction(transaction,
				transactionDetails);
	}

	private List<TransactionDetail> createTransactionDetails(Supplier supplier,
			GoodsReturnDetail goodsReturnDetail) throws Exception {
		double totalAmount = goodsReturnDetail.getReceiveDetail().getUnitRate()
				* goodsReturnDetail.getReturnQty();
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		Combination combination = purchaseBL.getProductBL()
				.getProductInventory(
						goodsReturnDetail.getProduct().getProductId());
		transactionDetails.add(purchaseBL.getTransactionBL()
				.createTransactionDetail(totalAmount,
						supplier.getCombination().getCombinationId(),
						TransactionType.Debit.getCode(), null, null, null));
		transactionDetails.add(purchaseBL.getTransactionBL()
				.createTransactionDetail(totalAmount,
						combination.getCombinationId(),
						TransactionType.Credit.getCode(), null, null, null));
		return transactionDetails;
	}

	private Stock addStockDetails(GoodsReturnDetail goodsReturnDetail)
			throws Exception {
		Stock stock = new Stock();
		Shelf shelf = purchaseBL
				.getStockBL()
				.getStoreBL()
				.getStoreService()
				.getStoreByShelfId(
						goodsReturnDetail.getReceiveDetail().getShelf()
								.getShelfId());
		stock.setStore(shelf.getShelf().getAisle().getStore());
		stock.setShelf(shelf);
		stock.setProduct(goodsReturnDetail.getReceiveDetail().getProduct());
		stock.setQuantity(goodsReturnDetail.getReturnQty());
		stock.setImplementation(getImplementation());
		return stock;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public GoodsReturnService getGoodsReturnService() {
		return goodsReturnService;
	}

	public void setGoodsReturnService(GoodsReturnService goodsReturnService) {
		this.goodsReturnService = goodsReturnService;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}

	public DebitBL getDebitBL() {
		return debitBL;
	}

	public void setDebitBL(DebitBL debitBL) {
		this.debitBL = debitBL;
	}

	public List<Long> getPurchaseInfo() {
		return purchaseInfo;
	}

	public void setPurchaseInfo(List<Long> purchaseInfo) {
		this.purchaseInfo = purchaseInfo;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
