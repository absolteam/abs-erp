package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductPackage;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductPackageService {

	private AIOTechGenericDAO<ProductPackage> productPackageDAO;
	private AIOTechGenericDAO<ProductPackageDetail> productPackageDetailDAO;

	public List<ProductPackage> getAllProductPackagings(
			Implementation implementation) throws Exception {
		return productPackageDAO.findByNamedQuery("getAllProductPackagings",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public ProductPackage getProductPackagingbyId(long productPackageId)  throws Exception {
		return productPackageDAO.findByNamedQuery("getProductPackagingbyId",
				productPackageId).get(0);
	}
	
	public List<ProductPackage> getProductPackagingsByProduct(
			long productId) throws Exception {
		return productPackageDAO.findByNamedQuery("getProductPackagingsByProduct",
				productId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}
	
	public List<ProductPackageDetail> getProductPackageDetailsById(
			long productPackageId) throws Exception{ 
		return productPackageDetailDAO.findByNamedQuery("getProductPackageDetailsById",
				productPackageId);
	} 
	
	public ProductPackageDetail getProductPackageDetailsByDetailId(
			long productPackageDetailId) throws Exception{ 
		return productPackageDetailDAO.findByNamedQuery("getProductPackageDetailsByDetailId",
				productPackageDetailId).get(0);
	} 
	
	public void deleteProductPackageDetail(
			List<ProductPackageDetail> packageDetails) throws Exception {
		productPackageDetailDAO.deleteAll(packageDetails);
	} 
	
	public void saveProductPackage(ProductPackage productPackage,
			List<ProductPackageDetail> productPackageDetails) throws Exception{
		productPackageDAO.saveOrUpdate(productPackage);
		for (ProductPackageDetail productPackageDetail : productPackageDetails)  
			productPackageDetail.setProductPackage(productPackage);
		productPackageDetailDAO.saveOrUpdateAll(productPackageDetails);
	} 
	
	public void deleteProductProduction(ProductPackage productPackage,
			List<ProductPackageDetail> productPackageDetails) {
		productPackageDetailDAO.deleteAll(productPackageDetails);
		productPackageDAO.delete(productPackage);
	} 
	
	// Getters & Setters
	public AIOTechGenericDAO<ProductPackage> getProductPackageDAO() {
		return productPackageDAO;
	}

	public void setProductPackageDAO(
			AIOTechGenericDAO<ProductPackage> productPackageDAO) {
		this.productPackageDAO = productPackageDAO;
	}

	public AIOTechGenericDAO<ProductPackageDetail> getProductPackageDetailDAO() {
		return productPackageDetailDAO;
	}

	public void setProductPackageDetailDAO(
			AIOTechGenericDAO<ProductPackageDetail> productPackageDetailDAO) {
		this.productPackageDetailDAO = productPackageDetailDAO;
	} 
}
