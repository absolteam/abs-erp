package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.BeanUtils;

import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferDetailVO;
import com.aiotech.aios.accounts.service.MaterialRequisitionService;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class MaterialRequisitionBL {

	// Dependencies
	private LookupMasterBL lookupMasterBL;
	private MaterialRequisitionService materialRequisitionService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private StoreBL storeBL;
	private AlertBL alertBL;

	// Show All Material Requisition
	public List<Object> showAllMaterialRequisition() throws Exception {
		List<MaterialRequisition> materialRequisitions = materialRequisitionService
				.getAllMaterialRequisition(getImplementation());
		List<Object> materialRequisitionVOs = new ArrayList<Object>();
		if (null != materialRequisitions && materialRequisitions.size() > 0) {
			for (MaterialRequisition materialRequisition : materialRequisitions)
				materialRequisitionVOs
						.add(addMaterialRequisitionVO(materialRequisition));
		}
		return materialRequisitionVOs;
	}

	public List<MaterialRequisitionDetail> getActiveMaterialRequisitionDetail(
			long storeId) throws Exception {
		return materialRequisitionService.getActiveMaterialRequisitionDetail(
				getImplementation(), storeId,
				RequisitionStatus.Completed.getCode());
	}

	public List<MaterialRequisitionVO> validateRequisitionTransfer(
			List<MaterialRequisitionDetail> materialRequisitionDetails)
			throws Exception {
		MaterialRequisitionVO materialRequisitionVO = null;
		MaterialRequisitionDetailVO materialRequisitionDetailVO = null;
		Map<Long, MaterialRequisitionVO> materialRequisitionVOHash = new HashMap<Long, MaterialRequisitionVO>();
		Map<Long, List<MaterialRequisitionDetailVO>> materialRequisitionDetailVOHash = new HashMap<Long, List<MaterialRequisitionDetailVO>>();
		List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();
		MaterialTransferDetailVO materialTransferDetailVO = null;
		Map<Long, List<MaterialTransferDetailVO>> materialTransferDetailVOHash = new HashMap<Long, List<MaterialTransferDetailVO>>();
		List<MaterialTransferDetailVO> materialTransferDetailVOs = null;
		for (MaterialRequisitionDetail materialRequisitionDetail : materialRequisitionDetails) {
			materialRequisitionVO = new MaterialRequisitionVO();
			materialRequisitionDetailVO = new MaterialRequisitionDetailVO();
			materialRequisitionVO
					.setMaterialRequisitionId(materialRequisitionDetail
							.getMaterialRequisition()
							.getMaterialRequisitionId());
			materialRequisitionVO.setReferenceNumber(materialRequisitionDetail
					.getMaterialRequisition().getReferenceNumber());
			materialRequisitionVO.setDate(DateFormat
					.convertDateToString(materialRequisitionDetail
							.getMaterialRequisition().getRequisitionDate()
							.toString()));
			materialRequisitionVO.setStore(materialRequisitionDetail
					.getMaterialRequisition().getStore());
			materialRequisitionVO.setDescription(materialRequisitionDetail
					.getMaterialRequisition().getDescription());
			materialRequisitionDetailVO
					.setMaterialRequisitionDetailId(materialRequisitionDetail
							.getMaterialRequisitionDetailId());
			materialRequisitionDetailVO.setProduct(materialRequisitionDetail
					.getProduct());
			materialRequisitionDetailVO
					.setRequisitionQty(materialRequisitionDetail.getQuantity());
			materialRequisitionDetailVO.setQuantity(materialRequisitionDetail
					.getQuantity());
			materialRequisitionDetailVO
					.setDescription(materialRequisitionDetail.getDescription());
			materialRequisitionDetailVO
					.setTransferQty(materialRequisitionDetail.getQuantity());
			materialRequisitionDetailVO
					.setMaterialTransferDetails(materialRequisitionDetail
							.getMaterialTransferDetails());
			materialRequisitionDetailVOs.add(materialRequisitionDetailVO);
			List<MaterialRequisitionDetailVO> materialRequisitionDetailVOList = materialRequisitionDetailVOHash
					.get(materialRequisitionVO.getMaterialRequisitionId());
			if (null != materialRequisitionDetailVOList
					&& materialRequisitionDetailVOList.size() > 0) {
				materialRequisitionDetailVOList
						.add(materialRequisitionDetailVO);
				materialRequisitionDetailVOHash.put(
						materialRequisitionVO.getMaterialRequisitionId(),
						materialRequisitionDetailVOList);
			} else {
				materialRequisitionDetailVOList = new ArrayList<MaterialRequisitionDetailVO>();
				materialRequisitionDetailVOList
						.add(materialRequisitionDetailVO);
				materialRequisitionDetailVOHash.put(
						materialRequisitionVO.getMaterialRequisitionId(),
						materialRequisitionDetailVOList);
			}
			materialTransferDetailVOs = new ArrayList<MaterialTransferDetailVO>();
			for (MaterialTransferDetail materialTransferDetail : materialRequisitionDetail
					.getMaterialTransferDetails()) {
				materialTransferDetailVO = new MaterialTransferDetailVO();
				materialTransferDetailVO
						.setMaterialTransferDetailId(materialTransferDetail
								.getMaterialTransferDetailId());
				materialTransferDetailVO.setQuantity(materialTransferDetail
						.getQuantity());
				materialTransferDetailVO.setDescription(materialTransferDetail
						.getDescription());
				materialTransferDetailVOs.add(materialTransferDetailVO);
			}
			if (materialTransferDetailVOHash
					.containsKey(materialRequisitionDetail
							.getMaterialRequisitionDetailId()))
				materialTransferDetailVOs.addAll(materialTransferDetailVOHash
						.get(materialRequisitionDetail
								.getMaterialRequisitionDetailId()));
			materialTransferDetailVOHash.put(
					materialRequisitionDetail.getMaterialRequisitionDetailId(),
					materialTransferDetailVOs);
			materialRequisitionVOHash.put(
					materialRequisitionVO.getMaterialRequisitionId(),
					materialRequisitionVO);
		}
		for (Entry<Long, List<MaterialRequisitionDetailVO>> requisitionId : materialRequisitionDetailVOHash
				.entrySet()) {
			List<MaterialRequisitionDetailVO> requisitionDetailVOs = materialRequisitionDetailVOHash
					.get(requisitionId.getKey());
			materialTransferDetailVOs = new ArrayList<MaterialTransferDetailVO>();
			for (MaterialRequisitionDetailVO detailVO : requisitionDetailVOs) {
				if (materialTransferDetailVOHash.containsKey(detailVO
						.getMaterialRequisitionDetailId())) {
					List<MaterialTransferDetailVO> transferDetailVOs = materialTransferDetailVOHash
							.get(detailVO.getMaterialRequisitionDetailId());
					for (MaterialTransferDetailVO transferDetail : transferDetailVOs) {
						detailVO.setMaterialTransferDetailId(transferDetail
								.getMaterialTransferDetailId());
						detailVO.setTransferedQty(transferDetail.getQuantity()
								+ (null != detailVO.getTransferedQty() ? detailVO
										.getTransferedQty() : 0));
						detailVO.setRequisitionQty(detailVO.getQuantity());
						detailVO.setTransferQty(detailVO.getQuantity()
								- detailVO.getTransferedQty());
						detailVO.setDescription(transferDetail.getDescription());
					}
					if (materialTransferDetailVOHash.containsKey(detailVO
							.getMaterialRequisitionDetailId()))
						materialTransferDetailVOs
								.addAll(materialTransferDetailVOHash.get(detailVO
										.getMaterialRequisitionDetailId()));
					materialTransferDetailVOHash.put(
							detailVO.getMaterialRequisitionDetailId(),
							materialTransferDetailVOs);
				}
			}
			if (materialRequisitionVOHash.containsKey(requisitionId.getKey())) {
				materialRequisitionVO = materialRequisitionVOHash
						.get(requisitionId.getKey());
				materialRequisitionVO
						.setMaterialRequisitionDetailVOs(requisitionDetailVOs);
				for (MaterialRequisitionDetailVO requisitionDetail : materialRequisitionVO
						.getMaterialRequisitionDetailVOs()) {
					materialRequisitionVO
							.setTotalRequisitionQty(requisitionDetail
									.getQuantity()
									+ (null != materialRequisitionVO
											.getTotalRequisitionQty() ? materialRequisitionVO
											.getTotalRequisitionQty() : 0));
				}
				materialRequisitionVOHash.put(requisitionId.getKey(),
						materialRequisitionVO);
			}
		}
		return new ArrayList<MaterialRequisitionVO>(
				materialRequisitionVOHash.values());
	}

	public void saveMaterialRequisition(
			MaterialRequisition materialRequisition,
			List<MaterialRequisitionDetail> materialRequisitionDetails,
			List<MaterialRequisitionDetail> deletedMaterialRequisitionDetails)
			throws Exception {
		materialRequisition.setImplementation(getImplementation());
		if (null == materialRequisition.getMaterialRequisitionId())
			SystemBL.saveReferenceStamp(MaterialRequisition.class.getName(),
					getImplementation());
		materialRequisition
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		if (null != deletedMaterialRequisitionDetails
				&& deletedMaterialRequisitionDetails.size() > 0)
			materialRequisitionService
					.deleteMaterialRequisitionDetails(deletedMaterialRequisitionDetails);
		materialRequisitionService.saveMaterialRequisition(materialRequisition,
				materialRequisitionDetails);
	}

	public void saveMaterialRequisition(
			MaterialRequisition materialRequisition,
			List<MaterialRequisitionDetail> materialRequisitionDetails,
			Long alertId,
			List<MaterialRequisitionDetail> deletedMaterialRequisitionDetails)
			throws Exception {
		materialRequisition.setImplementation(getImplementation());
		if (null == materialRequisition.getMaterialRequisitionId())
			SystemBL.saveReferenceStamp(MaterialRequisition.class.getName(),
					getImplementation());
		materialRequisition
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (materialRequisition != null
				&& materialRequisition.getMaterialRequisitionId() != null
				&& materialRequisition.getMaterialRequisitionId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (null != deletedMaterialRequisitionDetails
				&& deletedMaterialRequisitionDetails.size() > 0)
			materialRequisitionService
					.deleteMaterialRequisitionDetails(deletedMaterialRequisitionDetails);
		materialRequisitionService.saveMaterialRequisition(materialRequisition,
				materialRequisitionDetails);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				MaterialRequisition.class.getSimpleName(),
				materialRequisition.getMaterialRequisitionId(), user,
				workflowDetailVo);
	}

	public void doMaterialRequisitionBusinessUpdate(Long materialRequisitionId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			MaterialRequisition materialRequisition = materialRequisitionService
					.getMaterialRequisitionById(materialRequisitionId);
			if (workflowDetailVO.isDeleteFlag()) {
				materialRequisitionService.deleteMaterialRequisition(
						materialRequisition,
						new ArrayList<MaterialRequisitionDetail>(
								materialRequisition
										.getMaterialRequisitionDetails()));
			} else {
				// Material Transfer Alert configuration
				Alert alert = alertBL.getAlertService().getAlertRecordInfo(
						materialRequisition.getMaterialRequisitionId(),
						MaterialRequisition.class.getSimpleName());
				if (null == alert)
					alertBL.alertMaterialRequisitionProcess(materialRequisition);
				else {
					alert.setIsActive(true);
					alertBL.getAlertService().updateAlert(alert);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteMaterialRequisition(
			MaterialRequisition materialRequisition,
			ArrayList<MaterialRequisitionDetail> materialRequisitionDetails,
			Long messageId) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				MaterialRequisition.class.getSimpleName(),
				materialRequisition.getMaterialRequisitionId(), user,
				workflowDetailVo);
	}

	private MaterialRequisitionVO addMaterialRequisitionVO(
			MaterialRequisition materialRequisition) throws Exception {
		MaterialRequisitionVO materialRequisitionVO = new MaterialRequisitionVO();
		materialRequisitionVO.setMaterialRequisitionId(materialRequisition
				.getMaterialRequisitionId());
		materialRequisitionVO.setDate(DateFormat
				.convertDateToString(materialRequisition.getRequisitionDate()
						.toString()));
		materialRequisitionVO.setStatus(RequisitionStatus
				.get(materialRequisition.getRequisitionStatus()).name()
				.replaceAll("_", " "));
		if ((byte) materialRequisition.getRequisitionStatus() != (byte) RequisitionStatus.Open
				.getCode())
			materialRequisitionVO.setEditStatus(false);
		else
			materialRequisitionVO.setEditStatus(true);
		materialRequisitionVO.setReferenceNumber(materialRequisition
				.getReferenceNumber());
		materialRequisitionVO.setStoreName(materialRequisition.getStore()
				.getStoreName());
		materialRequisitionVO.setPersonName(null != materialRequisition
				.getPerson() ? materialRequisition.getPerson().getFirstName()
				+ " " + materialRequisition.getPerson().getLastName() : "");
		Set<String> transferNumbers = new HashSet<String>();
		for (MaterialRequisitionDetail materialRequisitionDetail : materialRequisition
				.getMaterialRequisitionDetails()) {
			if (null != materialRequisitionDetail.getMaterialTransferDetails()
					&& materialRequisitionDetail.getMaterialTransferDetails()
							.size() > 0) {

				for (MaterialTransferDetail materialTransferDetail : materialRequisitionDetail
						.getMaterialTransferDetails())
					transferNumbers.add(materialTransferDetail
							.getMaterialTransfer().getReferenceNumber());
			}
		}
		String transferNumber = "";
		if (null != transferNumbers && transferNumbers.size() > 0) {
			transferNumber = Arrays.toString(transferNumbers.toArray());
			transferNumber = transferNumber.substring(1,
					transferNumber.length() - 1);
		}
		materialRequisitionVO.setTransferReference(transferNumber);
		return materialRequisitionVO;
	}

	public List<MaterialRequisitionVO> convertMaterialRequistionToVOs(
			List<MaterialRequisition> materialRequisitions) throws Exception {
		List<MaterialRequisitionVO> materialRequisitionVOs = new ArrayList<MaterialRequisitionVO>();
		MaterialRequisitionVO materialRequisitionVO = null;
		for (MaterialRequisition materialRequisition : materialRequisitions) {
			materialRequisitionVO = new MaterialRequisitionVO();
			materialRequisitionVO.setMaterialRequisitionId(materialRequisition
					.getMaterialRequisitionId());
			materialRequisitionVO.setDate(DateFormat
					.convertDateToString(materialRequisition
							.getRequisitionDate().toString()));
			materialRequisitionVO.setStatus(RequisitionStatus
					.get(materialRequisition.getRequisitionStatus()).name()
					.replaceAll("_", " "));
			if (materialRequisition.getIsApprove() != null
					&& (byte) materialRequisition.getIsApprove() == (byte) WorkflowConstants.Status.Published
							.getCode())
				materialRequisitionVO.setStatus(RequisitionStatus.Order_Ready
						.name().replaceAll("_", " "));

			materialRequisitionVO.setReferenceNumber(materialRequisition
					.getReferenceNumber());
			materialRequisitionVO.setStoreName(materialRequisition.getStore()
					.getStoreName());

			materialRequisitionVO
					.setMaterialRequisitionDetailVOs(convertMaterialRequistionDetailToVOs(new ArrayList<MaterialRequisitionDetail>(
							materialRequisition.getMaterialRequisitionDetails())));
			materialRequisitionVOs.add(materialRequisitionVO);
		}
		return materialRequisitionVOs;
	}

	public List<MaterialRequisitionDetailVO> convertMaterialRequistionDetailToVOs(
			List<MaterialRequisitionDetail> materialRequisitionDetails)
			throws Exception {
		List<MaterialRequisitionDetailVO> materialRequisitionVOs = null;
		if (materialRequisitionDetails != null
				&& materialRequisitionDetails.size() > 0) {
			materialRequisitionVOs = new ArrayList<MaterialRequisitionDetailVO>();
			MaterialRequisitionDetailVO materialRequisitionVO = null;
			for (MaterialRequisitionDetail detail : materialRequisitionDetails) {
				materialRequisitionVO = new MaterialRequisitionDetailVO();
				BeanUtils.copyProperties(detail, materialRequisitionVO);

				materialRequisitionVOs.add(materialRequisitionVO);
			}
		}
		return materialRequisitionVOs;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public MaterialRequisitionService getMaterialRequisitionService() {
		return materialRequisitionService;
	}

	public void setMaterialRequisitionService(
			MaterialRequisitionService materialRequisitionService) {
		this.materialRequisitionService = materialRequisitionService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}
}
