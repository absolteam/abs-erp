package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetInsurancePremium;
import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AssetInsuranceVO;
import com.aiotech.aios.accounts.service.AssetInsuranceService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetInsuranceBL {

	private AssetInsuranceService assetInsuranceService;
	private AssetCreationBL assetCreationBL;
	private LookupMasterBL lookupMasterBL;
	private ChequeBookBL chequeBookBL;
	private AssetInsurancePremiumBL assetInsurancePremiumBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Asset Check Outs
	public List<Object> showAllAssetInsurance() throws Exception {
		List<AssetInsurance> assetInsurances = assetInsuranceService
				.getAllAssetInsurance(getImplementation());
		List<Object> assetInsuranceVOs = new ArrayList<Object>();
		if (null != assetInsurances && assetInsurances.size() > 0) {
			for (AssetInsurance assetInsurance : assetInsurances)
				assetInsuranceVOs.add(addAssetInsuranceVO(assetInsurance));
		}
		return assetInsuranceVOs;
	}

	// Save Asset Insurance
	public void saveAssetInsurance(AssetInsurance assetInsurance,
			List<AssetInsurancePremium> assetInsurancePremiums)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetInsurance.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		boolean updateFlag = false;
		if (null != assetInsurance.getAssetInsuranceId())
			updateFlag = true;
		if (updateFlag) {
			assetInsurancePremiumBL.getTransactionBL().deleteTransaction(
					assetInsurance.getAssetInsuranceId(),
					AssetInsurance.class.getSimpleName());
			if (null != assetInsurancePremiums
					&& assetInsurancePremiums.size() > 0)
				assetInsurancePremiumBL.getAssetInsurancePremiumService()
						.deleteAssetInsurance(assetInsurancePremiums);
		}
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetInsurance != null
				&& assetInsurance.getAssetInsuranceId() != null
				&& assetInsurance.getAssetInsuranceId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetInsuranceService.saveAssetInsurance(assetInsurance);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetInsurance.class.getSimpleName(),
				assetInsurance.getAssetInsuranceId(), user, workflowDetailVo);
	}

	public void doAssetInsuranceBusinessUpdate(Long assetInsuranceId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			AssetInsurance assetInsurance = assetInsuranceService
					.getAssetInsuranceByInsuranceId(assetInsuranceId);
			if (workflowDetailVO.isDeleteFlag()) {
				List<AssetInsurancePremium> assetInsurancePremiums = this
						.getAssetInsurancePremiumBL()
						.getAssetInsurancePremiumService()
						.getPremiumDueInsuranceId(assetInsuranceId);
				assetInsurancePremiumBL.getTransactionBL().deleteTransaction(
						assetInsurance.getAssetInsuranceId(),
						AssetInsurance.class.getSimpleName());
				if (null != assetInsurancePremiums
						&& assetInsurancePremiums.size() > 0)
					assetInsurancePremiumBL.getAssetInsurancePremiumService()
							.deleteAssetInsurance(assetInsurancePremiums);
				assetInsuranceService.deleteAssetInsurance(assetInsurance);
			} else {
				AssetInsurancePremium assetInsurancePremium = null;
				if (null != assetInsurance.getChequeBook()
						&& (long) assetInsurance.getChequeBook()
								.getChequeBookId() > 0
						&& assetInsurance.getChequeNumber() == null) { 
					ChequeBook chequeBook = chequeBookBL.getChequeBookService()
							.findChequeBookById(
									assetInsurance.getChequeBook()
											.getChequeBookId());
					assetInsurance.setChequeNumber(
							null != chequeBook
									.getCurrentNo() ? chequeBook.getCurrentNo() + 1
									: chequeBook
											.getStartingNo()); 
					chequeBook.setCurrentNo(assetInsurance.getChequeNumber());
					chequeBookBL.directSaveChequeBook(chequeBook);
				}
				Transaction prepaidTransaction = createPrepaidInsuranceEntry(
						assetInsurance,
						(assetInsurance.getPremium() * getPreminumPeriodDifference(
								assetInsurance.getInsurancePeriod(),
								new DateTime(assetInsurance.getStartDate()),
								new DateTime(assetInsurance.getEndDate()))),
						true, false);
				assetInsurancePremiumBL.getTransactionBL().saveTransaction(
						prepaidTransaction,
						new ArrayList<TransactionDetail>(prepaidTransaction
								.getTransactionDetails()));
				int premiumDifference = getPreminumPeriodDifference(
						assetInsurance.getInsurancePeriod(), new DateTime(
								Calendar.getInstance().getTime()),
						new DateTime(assetInsurance.getStartDate()));
				assetInsurancePremium = new AssetInsurancePremium(
						assetInsurance,
						assetInsurancePremiumBL.getNextPremiumDate(
								assetInsurance.getInsurancePeriod(),
								new DateTime(assetInsurance.getStartDate())),
						assetInsurance.getPremium(), false);
				if (premiumDifference < 0) {
					assetInsurancePremium
							.setNextPremiumDate(assetInsurancePremiumBL.getNextPremiumDate(
									assetInsurance.getInsurancePeriod(),
									new DateTime(Calendar.getInstance()
											.getTime())));
					Transaction premiumTransaction = assetInsurancePremiumBL
							.createPremiumInsuranceEntry(assetInsurance,
									(assetInsurance.getPremium() * Math
											.abs(premiumDifference)),
									TransactionType.Debit.getCode(),
									TransactionType.Credit.getCode());
					assetInsurancePremiumBL.getTransactionBL().saveTransaction(
							premiumTransaction,
							new ArrayList<TransactionDetail>(premiumTransaction
									.getTransactionDetails()));
				}
				if (null != assetInsurancePremium)
					assetInsurancePremiumBL
							.saveInsurancePremium(assetInsurancePremium);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Create Prepaid Insurance Transaction
	private Transaction createPrepaidInsuranceEntry(
			AssetInsurance assetInsurance, double totalDueAmount,
			boolean debitFlag, boolean creditFlag) throws Exception {
		Transaction transaction = assetInsurancePremiumBL.getTransactionBL()
				.createTransaction(
						assetInsurancePremiumBL.getTransactionBL()
								.getCurrency().getCurrencyId(),
						"Prepaid Insurance ".concat(assetInsurance
								.getAssetCreation().getAssetName()),
						assetInsurance.getStartDate(),
						assetInsurancePremiumBL.getTransactionBL().getCategory(
								"Asset Insurance"),
						assetInsurance.getAssetInsuranceId(),
						AssetInsurance.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(createPrepaidInsuranceDetail(assetInsurance,
				totalDueAmount, debitFlag));
		transactionDetails.add(createCashBankDetail(assetInsurance,
				totalDueAmount, creditFlag));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Prepaid Entry
	private TransactionDetail createPrepaidInsuranceDetail(
			AssetInsurance assetInsurance, double totalDueAmount,
			boolean debitFlag) throws Exception {
		return assetInsurancePremiumBL.getTransactionBL()
				.createTransactionDetail(totalDueAmount,
						getImplementation().getClearingAccount(), debitFlag,
						"", null, null);
	}

	// Cash/ Bank Entry
	private TransactionDetail createCashBankDetail(
			AssetInsurance assetInsurance, double totalDueAmount,
			boolean creditFlag) throws Exception {
		long combinationId;
		if (null != assetInsurance.getChequeBook()
				&& null != assetInsurance.getChequeBook().getBankAccount())
			combinationId = assetInsurance.getChequeBook().getBankAccount()
					.getCombination().getCombinationId();
		else
			combinationId = assetInsurance.getCombination().getCombinationId();
		return assetInsurancePremiumBL.getTransactionBL()
				.createTransactionDetail(totalDueAmount, combinationId,
						creditFlag, "", null, null);
	}

	private int getPreminumPeriodDifference(Byte insurancePeriod,
			DateTime fromDate, DateTime endDate) {
		int diffrence = 0;
		switch (insurancePeriod) {
		case 1:
			diffrence = Days.daysBetween(fromDate, endDate).getDays();
			diffrence += 1;
			break;
		case 2:
			diffrence = Weeks.weeksBetween(fromDate, endDate).getWeeks();
			diffrence += 1;
			break;
		case 3:
			diffrence = Months.monthsBetween(fromDate, endDate).getMonths();
			diffrence += 1;
			break;
		case 4:
			diffrence = Months.monthsBetween(fromDate, endDate).getMonths();
			diffrence /= 4;
			diffrence += 1;
			break;
		case 5:
			diffrence = Months.monthsBetween(fromDate, endDate).getMonths();
			diffrence /= 2;
			diffrence += 1;
			break;
		case 6:
			diffrence = Years.yearsBetween(fromDate, endDate).getYears();
			break;
		}
		return diffrence;
	}

	// Delete Asset Insurance
	public void deleteAssetInsurance(AssetInsurance assetInsurance,
			List<AssetInsurancePremium> assetInsurancePremiums)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetInsurance.class.getSimpleName(),
				assetInsurance.getAssetInsuranceId(), user, workflowDetailVo);
	}

	// Add Asset Insurance
	private AssetInsuranceVO addAssetInsuranceVO(AssetInsurance assetInsurance) {
		AssetInsuranceVO assetInsuranceVO = new AssetInsuranceVO();
		assetInsuranceVO.setAssetInsuranceId(assetInsurance
				.getAssetInsuranceId());
		assetInsuranceVO.setPolicyNumber(assetInsurance.getPolicyNumber());
		assetInsuranceVO.setAssetName(assetInsurance.getAssetCreation()
				.getAssetName());
		assetInsuranceVO.setInsuranceType(assetInsurance
				.getLookupDetailByInsuranceType().getDisplayName());
		assetInsuranceVO.setPolicyType(assetInsurance
				.getLookupDetailByPolicyType().getDisplayName());
		assetInsuranceVO.setProvider(assetInsurance.getLookupDetailByProvider()
				.getDisplayName());
		assetInsuranceVO.setFromDate(DateFormat
				.convertDateToString(assetInsurance.getStartDate().toString()));
		assetInsuranceVO.setToDate(DateFormat
				.convertDateToString(assetInsurance.getEndDate().toString()));
		assetInsuranceVO
				.setInsurancePeriodName(Constants.Accounts.AssetInsurancePeriod
						.get(assetInsurance.getInsurancePeriod()).name());
		assetInsuranceVO.setBrokerName(assetInsurance.getBrokerName());
		return assetInsuranceVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetInsuranceService getAssetInsuranceService() {
		return assetInsuranceService;
	}

	public void setAssetInsuranceService(
			AssetInsuranceService assetInsuranceService) {
		this.assetInsuranceService = assetInsuranceService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public AssetCreationBL getAssetCreationBL() {
		return assetCreationBL;
	}

	public void setAssetCreationBL(AssetCreationBL assetCreationBL) {
		this.assetCreationBL = assetCreationBL;
	}

	public AssetInsurancePremiumBL getAssetInsurancePremiumBL() {
		return assetInsurancePremiumBL;
	}

	public void setAssetInsurancePremiumBL(
			AssetInsurancePremiumBL assetInsurancePremiumBL) {
		this.assetInsurancePremiumBL = assetInsurancePremiumBL;
	}

	public ChequeBookBL getChequeBookBL() {
		return chequeBookBL;
	}

	public void setChequeBookBL(ChequeBookBL chequeBookBL) {
		this.chequeBookBL = chequeBookBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
