package com.aiotech.aios.accounts.service.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CombinationTemplate;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationTemplateVO;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationTreeVO;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationVO;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CombinationBL {
	private GLCombinationService combinationService;
	private AccountBL accountBL;
	private LookupMasterBL lookupMasterBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<CombinationTemplateVO> generateCombinationTemplateTree()
			throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		List<CombinationTemplate> combinationTemplateList = combinationService
				.getAllCombinationTemplate();
		Map<Long, CombinationTemplate> sessionCombinationTemplate = new HashMap<Long, CombinationTemplate>();
		for (CombinationTemplate template : combinationTemplateList) {
			sessionCombinationTemplate.put(template.getCombinationId(),
					template);
		}
		if (null != sessionCombinationTemplate
				&& sessionCombinationTemplate.size() > 0) {
			if (null != session.getAttribute("SESSION_COMBINATION_TEMPLATE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TEMPLATE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_TEMPLATE_"
						+ sessionObj.get("jSessionId"));
			}
			session.setAttribute(
					"SESSION_COMBINATION_TEMPLATE_"
							+ sessionObj.get("jSessionId"),
					sessionCombinationTemplate);
		}
		List<CombinationTemplateVO> allVos = CombinationTemplateVO
				.convertTOCombinationTemplateVO(combinationTemplateList);
		List<CombinationTemplateVO> results = new ArrayList<CombinationTemplateVO>();
		for (CombinationTemplateVO company : allVos) {
			if (company.getCOATemplateByCompanyAccountId() != null
					&& company.getCOATemplateByCostcenterAccountId() == null) {
				List<CombinationTemplateVO> tempCost = new ArrayList<CombinationTemplateVO>();
				for (CombinationTemplateVO costCenter : allVos) {
					if (costCenter.getCOATemplateByCostcenterAccountId() != null
							&& costCenter.getCOATemplateByNaturalAccountId() == null
							&& company
									.getCOATemplateByCompanyAccountId()
									.equals(costCenter
											.getCOATemplateByCompanyAccountId())) {
						tempCost.add(costCenter);
						List<CombinationTemplateVO> tempNaturalList = new ArrayList<CombinationTemplateVO>();
						for (CombinationTemplateVO natural : allVos) {
							if (natural.getCOATemplateByNaturalAccountId() != null
									&& natural
											.getCOATemplateByAnalysisAccountId() == null
									&& company
											.getCOATemplateByCompanyAccountId()
											.equals(natural
													.getCOATemplateByCompanyAccountId())
									&& costCenter
											.getCOATemplateByCostcenterAccountId()
											.equals(natural
													.getCOATemplateByCostcenterAccountId())) {
								Constants.Accounts accountTypes = new Constants.Accounts();
								switch (natural
										.getCOATemplateByNaturalAccountId()
										.getAccountType()) {
								case 1:
									natural.setAccountType(accountTypes.asset);
									break;
								case 2:
									natural.setAccountType(accountTypes.liabilites);
									break;
								case 3:
									natural.setAccountType(accountTypes.revenue);
									break;
								case 4:
									natural.setAccountType(accountTypes.expenses);
									break;
								case 5:
									natural.setAccountType(accountTypes.ownersEquity);
									break;
								}
								tempNaturalList.add(natural);
								List<CombinationTemplateVO> tempAnalysisList = new ArrayList<CombinationTemplateVO>();
								for (CombinationTemplateVO analysis : allVos) {
									if (analysis
											.getCOATemplateByAnalysisAccountId() != null
											&& analysis
													.getCOATemplateByBuffer1AccountId() == null
											&& company
													.getCOATemplateByCompanyAccountId()
													.equals(analysis
															.getCOATemplateByCompanyAccountId())
											&& costCenter
													.getCOATemplateByCostcenterAccountId()
													.equals(analysis
															.getCOATemplateByCostcenterAccountId())
											&& natural
													.getCOATemplateByNaturalAccountId()
													.equals(analysis
															.getCOATemplateByNaturalAccountId())) {
										tempAnalysisList.add(analysis);
										List<CombinationTemplateVO> tempBuffer1List = new ArrayList<CombinationTemplateVO>();
										for (CombinationTemplateVO buffer1 : allVos) {
											if (buffer1
													.getCOATemplateByBuffer1AccountId() != null
													&& buffer1
															.getCOATemplateByBuffer2AccountId() == null
													&& company
															.getCOATemplateByCompanyAccountId()
															.equals(buffer1
																	.getCOATemplateByCompanyAccountId())
													&& natural
															.getCOATemplateByNaturalAccountId()
															.equals(buffer1
																	.getCOATemplateByNaturalAccountId())
													&& analysis
															.getCOATemplateByAnalysisAccountId()
															.equals(buffer1
																	.getCOATemplateByAnalysisAccountId())) {
												tempBuffer1List.add(buffer1);
												List<CombinationTemplateVO> tempBuffer2List = new ArrayList<CombinationTemplateVO>();

												for (CombinationTemplateVO buffer2 : allVos) {
													if (buffer2
															.getCOATemplateByBuffer2AccountId() != null
															&& company
																	.getCOATemplateByCompanyAccountId()
																	.equals(buffer2
																			.getCOATemplateByCompanyAccountId())
															&& costCenter
																	.getCOATemplateByCostcenterAccountId()
																	.equals(buffer2
																			.getCOATemplateByCostcenterAccountId())
															&& natural
																	.getCOATemplateByNaturalAccountId()
																	.equals(buffer2
																			.getCOATemplateByNaturalAccountId())
															&& analysis
																	.getCOATemplateByAnalysisAccountId()
																	.equals(buffer2
																			.getCOATemplateByAnalysisAccountId())
															&& buffer1
																	.getCOATemplateByBuffer1AccountId()
																	.equals(buffer2
																			.getCOATemplateByBuffer1AccountId())
															&& buffer2
																	.getCOATemplateByBuffer1AccountId()
																	.equals(buffer1
																			.getCOATemplateByBuffer1AccountId())) {
														tempBuffer2List
																.add(buffer2);
													}
												}
												buffer1.getBuffer2Vos().addAll(
														tempBuffer2List);
											}
										}
										analysis.getBuffer1Vos().addAll(
												tempBuffer1List);
									}
								}
								natural.getAnalysisVos().addAll(
										tempAnalysisList);
							}
						}
						costCenter.getNaturalVos().addAll(tempNaturalList);
					}
				}
				company.getCostVos().addAll(tempCost);
				results.add(company);

			}
		}
		return results;
	}

	public List<CombinationVO> generateCostCenterCombinationTree(
			Implementation implementation) throws Exception {
		List<Combination> combinations = combinationService
				.getCostCenterCombination(implementation);
		List<CombinationVO> allVos = CombinationVO
				.convertTOCombinationVO(combinations);
		List<CombinationVO> results = new ArrayList<CombinationVO>();
		for (CombinationVO company : allVos) {
			if (company.getAccountByCompanyAccountId() != null
					&& company.getAccountByCostcenterAccountId() == null) {
				List<CombinationVO> tempCost = new ArrayList<CombinationVO>();
				for (CombinationVO costCenter : allVos) {
					if (costCenter.getAccountByCostcenterAccountId() != null
							&& costCenter.getAccountByNaturalAccountId() == null
							&& company.getAccountByCompanyAccountId().equals(
									costCenter.getAccountByCompanyAccountId())) {
						tempCost.add(costCenter);
					}
				}
				company.getCostVos().addAll(tempCost);
				results.add(company);
			}
		}
		return results;
	}

	public List<Object> generateCombinationTree(List<Combination> combinations)
			throws Exception {
		List<CombinationVO> allVos = CombinationVO
				.convertTOCombinationVO(combinations);
		List<Object> results = new ArrayList<Object>();
		CombinationTreeVO treeVO = new CombinationTreeVO();
		treeVO.setKey(-1);
		treeVO.setTitle(Combination.class.getSimpleName());
		treeVO.setExtraClasses("fancytree-title-head");
		treeVO.setUnselectable(true);
		results.add(treeVO);
		CombinationTreeVO companyTreeVO = null;
		for (CombinationVO company : allVos) {
			if (company.getAccountByCompanyAccountId() != null
					&& company.getAccountByCostcenterAccountId() == null) {
				companyTreeVO = new CombinationTreeVO();
				companyTreeVO.setKey(company.getCombinationId());
				companyTreeVO.setTitle(company.getAccountByCompanyAccountId()
						.getAccount()
						+ " ["
						+ company.getAccountByCompanyAccountId().getCode()
						+ "]");
				companyTreeVO.setRefKey(company.getAccountByCompanyAccountId()
						.getSegment().getSegmentId()
						+ "_"
						+ company.getAccountByCompanyAccountId().getSegment()
								.getAccessId());
				companyTreeVO.setChildren(getCombinationChild(
						company.getCombinationId(), companyTreeVO)
						.getChildren());

				List<CombinationTreeVO> tempCost = new ArrayList<CombinationTreeVO>();
				CombinationTreeVO costTreeVO = null;
				for (CombinationVO costCenter : allVos) {
					if (costCenter.getAccountByCostcenterAccountId() != null
							&& costCenter.getAccountByNaturalAccountId() == null
							&& company.getAccountByCompanyAccountId().equals(
									costCenter.getAccountByCompanyAccountId())) {
						costTreeVO = new CombinationTreeVO();
						costTreeVO.setKey(costCenter.getCombinationId());
						costTreeVO.setTitle(costCenter
								.getAccountByCostcenterAccountId().getAccount()
								+ " ["
								+ company.getAccountByCompanyAccountId()
										.getCode()
								+ "."
								+ costCenter.getAccountByCostcenterAccountId()
										.getCode() + "]");
						costTreeVO.setRefKey(costCenter
								.getAccountByCostcenterAccountId().getSegment()
								.getSegmentId()
								+ "_"
								+ costCenter.getAccountByCostcenterAccountId()
										.getSegment().getAccessId());
						costTreeVO.setChildren(getCombinationChild(
								costCenter.getCombinationId(), costTreeVO)
								.getChildren());
						tempCost.add(costTreeVO);

						List<CombinationTreeVO> tempNaturalList = new ArrayList<CombinationTreeVO>();
						CombinationTreeVO naturalTreeVO = null;
						for (CombinationVO natural : allVos) {
							if (natural.getAccountByNaturalAccountId() != null
									&& natural.getAccountByAnalysisAccountId() == null
									&& company
											.getAccountByCompanyAccountId()
											.equals(natural
													.getAccountByCompanyAccountId())
									&& costCenter
											.getAccountByCostcenterAccountId()
											.equals(natural
													.getAccountByCostcenterAccountId())) {
								naturalTreeVO = new CombinationTreeVO();
								naturalTreeVO
										.setKey(natural.getCombinationId());
								naturalTreeVO
										.setTitle(natural
												.getAccountByNaturalAccountId()
												.getAccount()
												+ " ["
												+ company
														.getAccountByCompanyAccountId()
														.getCode()
												+ "."
												+ costCenter
														.getAccountByCostcenterAccountId()
														.getCode()
												+ "."
												+ natural
														.getAccountByNaturalAccountId()
														.getCode() + "]");
								naturalTreeVO.setRefKey(natural
										.getAccountByNaturalAccountId()
										.getSegment().getSegmentId()
										+ "_"
										+ natural
												.getAccountByNaturalAccountId()
												.getSegment().getAccessId());
								naturalTreeVO.setChildren(getCombinationChild(
										natural.getCombinationId(),
										naturalTreeVO).getChildren());

								Constants.Accounts accountTypes = new Constants.Accounts();
								switch (natural.getAccountByNaturalAccountId()
										.getAccountType()) {
								case 1:
									natural.setAccountType(accountTypes.asset);
									break;
								case 2:
									natural.setAccountType(accountTypes.liabilites);
									break;
								case 3:
									natural.setAccountType(accountTypes.revenue);
									break;
								case 4:
									natural.setAccountType(accountTypes.expenses);
									break;
								case 5:
									natural.setAccountType(accountTypes.ownersEquity);
									break;
								}
								tempNaturalList.add(naturalTreeVO);

								List<CombinationTreeVO> tempAnalysisList = new ArrayList<CombinationTreeVO>();
								CombinationTreeVO analysisTreeVO = null;
								for (CombinationVO analysis : allVos) {
									if (analysis
											.getAccountByAnalysisAccountId() != null
											&& analysis
													.getAccountByBuffer1AccountId() == null
											&& company
													.getAccountByCompanyAccountId()
													.equals(analysis
															.getAccountByCompanyAccountId())
											&& costCenter
													.getAccountByCostcenterAccountId()
													.equals(analysis
															.getAccountByCostcenterAccountId())
											&& natural
													.getAccountByNaturalAccountId()
													.equals(analysis
															.getAccountByNaturalAccountId())) {
										analysisTreeVO = new CombinationTreeVO();
										analysisTreeVO.setKey(analysis
												.getCombinationId());
										analysisTreeVO
												.setTitle(analysis
														.getAccountByAnalysisAccountId()
														.getAccount()
														+ " ["
														+ company
																.getAccountByCompanyAccountId()
																.getCode()
														+ "."
														+ costCenter
																.getAccountByCostcenterAccountId()
																.getCode()
														+ "."
														+ natural
																.getAccountByNaturalAccountId()
																.getCode()
														+ "."
														+ analysis
																.getAccountByAnalysisAccountId()
																.getCode()
														+ "]");
										analysisTreeVO
												.setRefKey(analysis
														.getAccountByAnalysisAccountId()
														.getSegment()
														.getSegmentId()
														+ "_"
														+ analysis
																.getAccountByAnalysisAccountId()
																.getSegment()
																.getAccessId());
										analysisTreeVO
												.setChildren(getCombinationChild(
														analysis.getCombinationId(),
														analysisTreeVO)
														.getChildren());
										tempAnalysisList.add(analysisTreeVO);

										List<CombinationTreeVO> tempBuffer1List = new ArrayList<CombinationTreeVO>();
										CombinationTreeVO buffer1TreeVO = null;
										for (CombinationVO buffer1 : allVos) {
											if (buffer1
													.getAccountByBuffer1AccountId() != null
													&& buffer1
															.getAccountByBuffer2AccountId() == null
													&& company
															.getAccountByCompanyAccountId()
															.equals(buffer1
																	.getAccountByCompanyAccountId())
													&& natural
															.getAccountByNaturalAccountId()
															.equals(buffer1
																	.getAccountByNaturalAccountId())
													&& analysis
															.getAccountByAnalysisAccountId()
															.equals(buffer1
																	.getAccountByAnalysisAccountId())) {
												buffer1TreeVO = new CombinationTreeVO();
												buffer1TreeVO.setKey(buffer1
														.getCombinationId());
												buffer1TreeVO
														.setTitle(buffer1
																.getAccountByBuffer1AccountId()
																.getAccount()
																+ " ["
																+ company
																		.getAccountByCompanyAccountId()
																		.getCode()
																+ "."
																+ costCenter
																		.getAccountByCostcenterAccountId()
																		.getCode()
																+ "."
																+ natural
																		.getAccountByNaturalAccountId()
																		.getCode()
																+ "."
																+ analysis
																		.getAccountByAnalysisAccountId()
																		.getCode()
																+ "."
																+ buffer1
																		.getAccountByBuffer1AccountId()
																		.getCode()
																+ "]");
												buffer1TreeVO
														.setRefKey(buffer1
																.getAccountByBuffer1AccountId()
																.getSegment()
																.getSegmentId()
																+ "_"
																+ buffer1
																		.getAccountByBuffer1AccountId()
																		.getSegment()
																		.getAccessId());
												buffer1TreeVO
														.setChildren(getCombinationChild(
																buffer1.getCombinationId(),
																buffer1TreeVO)
																.getChildren());
												tempBuffer1List
														.add(buffer1TreeVO);

												List<CombinationTreeVO> tempBuffer2List = new ArrayList<CombinationTreeVO>();
												CombinationTreeVO buffer2TreeVO = null;
												for (CombinationVO buffer2 : allVos) {
													if (buffer2
															.getAccountByBuffer2AccountId() != null
															&& company
																	.getAccountByCompanyAccountId()
																	.equals(buffer2
																			.getAccountByCompanyAccountId())
															&& costCenter
																	.getAccountByCostcenterAccountId()
																	.equals(buffer2
																			.getAccountByCostcenterAccountId())
															&& natural
																	.getAccountByNaturalAccountId()
																	.equals(buffer2
																			.getAccountByNaturalAccountId())
															&& analysis
																	.getAccountByAnalysisAccountId()
																	.equals(buffer2
																			.getAccountByAnalysisAccountId())
															&& buffer1
																	.getAccountByBuffer1AccountId()
																	.equals(buffer2
																			.getAccountByBuffer1AccountId())
															&& buffer2
																	.getAccountByBuffer1AccountId()
																	.equals(buffer1
																			.getAccountByBuffer1AccountId())) {
														buffer2TreeVO = new CombinationTreeVO();
														buffer2TreeVO
																.setKey(buffer2
																		.getCombinationId());
														buffer2TreeVO
																.setTitle(buffer2
																		.getAccountByBuffer2AccountId()
																		.getAccount()
																		+ " ["
																		+ company
																				.getAccountByCompanyAccountId()
																				.getCode()
																		+ "."
																		+ costCenter
																				.getAccountByCostcenterAccountId()
																				.getCode()
																		+ "."
																		+ natural
																				.getAccountByNaturalAccountId()
																				.getCode()
																		+ "."
																		+ analysis
																				.getAccountByAnalysisAccountId()
																				.getCode()
																		+ "."
																		+ buffer1
																				.getAccountByBuffer1AccountId()
																				.getCode()
																		+ "."
																		+ buffer2
																				.getAccountByBuffer2AccountId()
																				.getCode()
																		+ "]");
														buffer2TreeVO
																.setRefKey(buffer2
																		.getAccountByBuffer2AccountId()
																		.getSegment()
																		.getSegmentId()
																		+ "_"
																		+ buffer2
																				.getAccountByBuffer2AccountId()
																				.getSegment()
																				.getAccessId());
														buffer2TreeVO
																.setChildren(getCombinationChild(
																		buffer2.getCombinationId(),
																		buffer2TreeVO)
																		.getChildren());
														tempBuffer2List
																.add(buffer2TreeVO);
													}
												}
												buffer1TreeVO
														.getChildren()
														.addAll(tempBuffer2List);
											}
										}
										analysisTreeVO.getChildren().addAll(
												tempBuffer1List);
									}
								}
								naturalTreeVO.getChildren().addAll(
										tempAnalysisList);
							}
						}
						costTreeVO.getChildren().addAll(tempNaturalList);
					}
				}
				companyTreeVO.getChildren().addAll(tempCost);
				results.add(companyTreeVO);
			}
		}
		return results;
	}

	private CombinationTreeVO getCombinationChild(Long combinationId,
			CombinationTreeVO combinationTreeVO) throws Exception {
		List<Combination> combinations = combinationService
				.getCombinationChildDetails(combinationId);
		List<CombinationTreeVO> combinationTreeVOtmps = null;
		if (null != combinations && combinations.size() > 0) {
			combinationTreeVOtmps = new ArrayList<CombinationTreeVO>();
			CombinationTreeVO combinationTreeVOtmp = null;
			for (Combination combination : combinations) {
				combinationTreeVOtmp = setCombinationAccounts(combination);
				combinationTreeVOtmps.add(combinationTreeVOtmp);
				getCombinationChild(combination.getCombinationId(),
						combinationTreeVOtmp).getChildren();
			}
		}
		if (null != combinationTreeVOtmps && combinationTreeVOtmps.size() > 0)
			combinationTreeVO.setChildren(combinationTreeVOtmps);
		return combinationTreeVO;
	}

	private CombinationTreeVO setCombinationAccounts(Combination combination) {
		CombinationTreeVO combinationTreeVO = new CombinationTreeVO();
		combinationTreeVO.setKey(combination.getCombinationId());
		if (null != combination.getAccountByBuffer2AccountId()) {
			combinationTreeVO.setTitle(combination
					.getAccountByBuffer2AccountId().getAccount()
					+ " ["
					+ combination.getAccountByCompanyAccountId().getCode()
					+ "."
					+ combination.getAccountByCostcenterAccountId().getCode()
					+ "."
					+ combination.getAccountByNaturalAccountId().getCode()
					+ "."
					+ combination.getAccountByAnalysisAccountId().getCode()
					+ "."
					+ combination.getAccountByBuffer1AccountId().getCode()
					+ "."
					+ combination.getAccountByBuffer2AccountId().getCode()
					+ "]");
			combinationTreeVO.setRefKey(combination
					.getAccountByBuffer2AccountId().getSegment().getSegmentId()
					+ "_"
					+ combination.getAccountByBuffer2AccountId().getSegment()
							.getAccessId());
		} else if (null != combination.getAccountByBuffer1AccountId()) {
			combinationTreeVO.setTitle(combination
					.getAccountByBuffer1AccountId().getAccount()
					+ " ["
					+ combination.getAccountByCompanyAccountId().getCode()
					+ "."
					+ combination.getAccountByCostcenterAccountId().getCode()
					+ "."
					+ combination.getAccountByNaturalAccountId().getCode()
					+ "."
					+ combination.getAccountByAnalysisAccountId().getCode()
					+ "."
					+ combination.getAccountByBuffer1AccountId().getCode()
					+ "]");
			combinationTreeVO.setRefKey(combination
					.getAccountByBuffer1AccountId().getSegment().getSegmentId()
					+ "_"
					+ combination.getAccountByBuffer1AccountId().getSegment()
							.getAccessId());
		} else if (null != combination.getAccountByAnalysisAccountId()) {
			combinationTreeVO.setTitle(combination
					.getAccountByAnalysisAccountId().getAccount()
					+ " ["
					+ combination.getAccountByCompanyAccountId().getCode()
					+ "."
					+ combination.getAccountByCostcenterAccountId().getCode()
					+ "."
					+ combination.getAccountByNaturalAccountId().getCode()
					+ "."
					+ combination.getAccountByAnalysisAccountId().getCode()
					+ "]");
			combinationTreeVO.setRefKey(combination
					.getAccountByAnalysisAccountId().getSegment()
					.getSegmentId()
					+ "_"
					+ combination.getAccountByAnalysisAccountId().getSegment()
							.getAccessId());
		} else if (null != combination.getAccountByNaturalAccountId()) {
			combinationTreeVO.setTitle(combination
					.getAccountByNaturalAccountId().getAccount()
					+ " ["
					+ combination.getAccountByCompanyAccountId().getCode()
					+ "."
					+ combination.getAccountByCostcenterAccountId().getCode()
					+ "."
					+ combination.getAccountByNaturalAccountId().getCode()
					+ "]");
			combinationTreeVO.setRefKey(combination
					.getAccountByNaturalAccountId().getSegment().getSegmentId()
					+ "_"
					+ combination.getAccountByNaturalAccountId().getSegment()
							.getAccessId());
		} else if (null != combination.getAccountByCostcenterAccountId()) {
			combinationTreeVO.setTitle(combination
					.getAccountByCostcenterAccountId().getAccount()
					+ " ["
					+ combination.getAccountByCompanyAccountId().getCode()
					+ "."
					+ combination.getAccountByCostcenterAccountId().getCode()
					+ "]");
			combinationTreeVO.setRefKey(combination
					.getAccountByCostcenterAccountId().getSegment()
					.getSegmentId()
					+ "_"
					+ combination.getAccountByCostcenterAccountId()
							.getSegment().getAccessId());
		} else {
			combinationTreeVO.setTitle(combination
					.getAccountByCompanyAccountId().getAccount()
					+ " ["
					+ combination.getAccountByCompanyAccountId().getCode()
					+ "]");
			combinationTreeVO.setRefKey(combination
					.getAccountByCompanyAccountId().getSegment().getSegmentId()
					+ "_"
					+ combination.getAccountByCompanyAccountId().getSegment()
							.getAccessId());
		}
		return combinationTreeVO;
	}

	public List<CombinationVO> generateCombinationTree(
			Implementation implementation, Combination combination)
			throws Exception {
		List<Combination> combinations = new ArrayList<Combination>();
		combinations.add(combination);
		List<CombinationVO> allVos = CombinationVO
				.convertTOCombinationVO(combinations);
		List<CombinationVO> results = new ArrayList<CombinationVO>();
		for (CombinationVO company : allVos) {
			if (company.getAccountByCompanyAccountId() != null) {
				List<CombinationVO> tempCost = new ArrayList<CombinationVO>();
				for (CombinationVO costCenter : allVos) {
					if (costCenter.getAccountByCostcenterAccountId() != null
							&& company.getAccountByCompanyAccountId().equals(
									costCenter.getAccountByCompanyAccountId())) {
						tempCost.add(costCenter);
						List<CombinationVO> tempNaturalList = new ArrayList<CombinationVO>();
						for (CombinationVO natural : allVos) {
							if (natural.getAccountByNaturalAccountId() != null
									&& company
											.getAccountByCompanyAccountId()
											.equals(natural
													.getAccountByCompanyAccountId())
									&& costCenter
											.getAccountByCostcenterAccountId()
											.equals(natural
													.getAccountByCostcenterAccountId())) {
								Constants.Accounts accountTypes = new Constants.Accounts();
								switch (natural.getAccountByNaturalAccountId()
										.getAccountType()) {
								case 1:
									natural.setAccountType(accountTypes.asset);
									break;
								case 2:
									natural.setAccountType(accountTypes.liabilites);
									break;
								case 3:
									natural.setAccountType(accountTypes.revenue);
									break;
								case 4:
									natural.setAccountType(accountTypes.expenses);
									break;
								case 5:
									natural.setAccountType(accountTypes.ownersEquity);
									break;
								}
								tempNaturalList.add(natural);
								List<CombinationVO> tempAnalysisList = new ArrayList<CombinationVO>();
								for (CombinationVO analysis : allVos) {
									if (analysis
											.getAccountByAnalysisAccountId() != null
											&& company
													.getAccountByCompanyAccountId()
													.equals(analysis
															.getAccountByCompanyAccountId())
											&& costCenter
													.getAccountByCostcenterAccountId()
													.equals(analysis
															.getAccountByCostcenterAccountId())
											&& natural
													.getAccountByNaturalAccountId()
													.equals(analysis
															.getAccountByNaturalAccountId())) {
										tempAnalysisList.add(analysis);
										List<CombinationVO> tempBuffer1List = new ArrayList<CombinationVO>();
										for (CombinationVO buffer1 : allVos) {
											if (buffer1
													.getAccountByBuffer1AccountId() != null
													&& company
															.getAccountByCompanyAccountId()
															.equals(buffer1
																	.getAccountByCompanyAccountId())
													&& natural
															.getAccountByNaturalAccountId()
															.equals(buffer1
																	.getAccountByNaturalAccountId())
													&& analysis
															.getAccountByAnalysisAccountId()
															.equals(buffer1
																	.getAccountByAnalysisAccountId())) {
												tempBuffer1List.add(buffer1);
												List<CombinationVO> tempBuffer2List = new ArrayList<CombinationVO>();

												for (CombinationVO buffer2 : allVos) {
													if (buffer2
															.getAccountByBuffer2AccountId() != null
															&& company
																	.getAccountByCompanyAccountId()
																	.equals(buffer2
																			.getAccountByCompanyAccountId())
															&& costCenter
																	.getAccountByCostcenterAccountId()
																	.equals(buffer2
																			.getAccountByCostcenterAccountId())
															&& natural
																	.getAccountByNaturalAccountId()
																	.equals(buffer2
																			.getAccountByNaturalAccountId())
															&& analysis
																	.getAccountByAnalysisAccountId()
																	.equals(buffer2
																			.getAccountByAnalysisAccountId())
															&& buffer1
																	.getAccountByBuffer1AccountId()
																	.equals(buffer2
																			.getAccountByBuffer1AccountId())
															&& buffer2
																	.getAccountByBuffer1AccountId()
																	.equals(buffer1
																			.getAccountByBuffer1AccountId())) {
														tempBuffer2List
																.add(buffer2);
													}
												}
												buffer1.getBuffer2Vos().addAll(
														tempBuffer2List);
											}
										}
										analysis.getBuffer1Vos().addAll(
												tempBuffer1List);
									}
								}
								natural.getAnalysisVos().addAll(
										tempAnalysisList);
							}
						}
						costCenter.getNaturalVos().addAll(tempNaturalList);
					}
				}
				company.getCostVos().addAll(tempCost);
				results.add(company);

			}
		}
		return results;
	}

	public List<CombinationVO> generateCombinationTree(
			Implementation implementation, Long recordId) throws Exception {
		List<Combination> combinations = null;
		if (null != recordId && recordId > 0)
			combinations = combinationService
					.getAllCombinations(implementation);
		else
			combinations = combinationService.getAllCombination(implementation);
		List<CombinationVO> allVos = CombinationVO
				.convertTOCombinationVO(combinations);
		List<CombinationVO> results = new ArrayList<CombinationVO>();
		for (CombinationVO company : allVos) {
			if (company.getAccountByCompanyAccountId() != null
					&& company.getAccountByCostcenterAccountId() == null) {
				List<CombinationVO> tempCost = new ArrayList<CombinationVO>();
				for (CombinationVO costCenter : allVos) {
					if (costCenter.getAccountByCostcenterAccountId() != null
							&& costCenter.getAccountByNaturalAccountId() == null
							&& company.getAccountByCompanyAccountId().equals(
									costCenter.getAccountByCompanyAccountId())) {
						tempCost.add(costCenter);
						List<CombinationVO> tempNaturalList = new ArrayList<CombinationVO>();
						for (CombinationVO natural : allVos) {
							if (natural.getAccountByNaturalAccountId() != null
									&& natural.getAccountByAnalysisAccountId() == null
									&& company
											.getAccountByCompanyAccountId()
											.equals(natural
													.getAccountByCompanyAccountId())
									&& costCenter
											.getAccountByCostcenterAccountId()
											.equals(natural
													.getAccountByCostcenterAccountId())) {
								Constants.Accounts accountTypes = new Constants.Accounts();
								switch (natural.getAccountByNaturalAccountId()
										.getAccountType()) {
								case 1:
									natural.setAccountType(accountTypes.asset);
									break;
								case 2:
									natural.setAccountType(accountTypes.liabilites);
									break;
								case 3:
									natural.setAccountType(accountTypes.revenue);
									break;
								case 4:
									natural.setAccountType(accountTypes.expenses);
									break;
								case 5:
									natural.setAccountType(accountTypes.ownersEquity);
									break;
								}
								tempNaturalList.add(natural);
								List<CombinationVO> tempAnalysisList = new ArrayList<CombinationVO>();
								for (CombinationVO analysis : allVos) {
									if (analysis
											.getAccountByAnalysisAccountId() != null
											&& analysis
													.getAccountByBuffer1AccountId() == null
											&& company
													.getAccountByCompanyAccountId()
													.equals(analysis
															.getAccountByCompanyAccountId())
											&& costCenter
													.getAccountByCostcenterAccountId()
													.equals(analysis
															.getAccountByCostcenterAccountId())
											&& natural
													.getAccountByNaturalAccountId()
													.equals(analysis
															.getAccountByNaturalAccountId())) {
										tempAnalysisList.add(analysis);
										List<CombinationVO> tempBuffer1List = new ArrayList<CombinationVO>();
										for (CombinationVO buffer1 : allVos) {
											if (buffer1
													.getAccountByBuffer1AccountId() != null
													&& buffer1
															.getAccountByBuffer2AccountId() == null
													&& company
															.getAccountByCompanyAccountId()
															.equals(buffer1
																	.getAccountByCompanyAccountId())
													&& natural
															.getAccountByNaturalAccountId()
															.equals(buffer1
																	.getAccountByNaturalAccountId())
													&& analysis
															.getAccountByAnalysisAccountId()
															.equals(buffer1
																	.getAccountByAnalysisAccountId())) {
												tempBuffer1List.add(buffer1);
												List<CombinationVO> tempBuffer2List = new ArrayList<CombinationVO>();

												for (CombinationVO buffer2 : allVos) {
													if (buffer2
															.getAccountByBuffer2AccountId() != null
															&& company
																	.getAccountByCompanyAccountId()
																	.equals(buffer2
																			.getAccountByCompanyAccountId())
															&& costCenter
																	.getAccountByCostcenterAccountId()
																	.equals(buffer2
																			.getAccountByCostcenterAccountId())
															&& natural
																	.getAccountByNaturalAccountId()
																	.equals(buffer2
																			.getAccountByNaturalAccountId())
															&& analysis
																	.getAccountByAnalysisAccountId()
																	.equals(buffer2
																			.getAccountByAnalysisAccountId())
															&& buffer1
																	.getAccountByBuffer1AccountId()
																	.equals(buffer2
																			.getAccountByBuffer1AccountId())
															&& buffer2
																	.getAccountByBuffer1AccountId()
																	.equals(buffer1
																			.getAccountByBuffer1AccountId())) {
														tempBuffer2List
																.add(buffer2);
													}
												}
												buffer1.getBuffer2Vos().addAll(
														tempBuffer2List);
											}
										}
										analysis.getBuffer1Vos().addAll(
												tempBuffer1List);
									}
								}
								natural.getAnalysisVos().addAll(
										tempAnalysisList);
							}
						}
						costCenter.getNaturalVos().addAll(tempNaturalList);
					}
				}
				company.getCostVos().addAll(tempCost);
				results.add(company);

			}
		}
		return results;
	}

	// Delete Combination & Ledger
	public void deleteCombinationLedger(Combination combination, Ledger ledger) {

		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
							.getCode());
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Combination.class.getSimpleName(),
					combination.getCombinationId(), user, workflowDetailVo);
		} catch (Exception ex) {

		}
	}

	public void deleteCombination(Combination combination) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Combination.class.getSimpleName(),
				combination.getCombinationId(), user, workflowDetailVo);
	}

	public Combination createCostCenter(String accountDesc, Boolean isSystem)
			throws Exception {
		Account account = accountBL.createCostCenterAccount(accountDesc,
				isSystem);
		if (null != account && !account.equals("")) {
			Combination combination = combinationService
					.getCombinationByCompanyAccount(account.getImplementation());
			if (null != combination && !combination.equals("")) {
				Combination costCombination = new Combination();
				costCombination.setAccountByCompanyAccountId(combination
						.getAccountByCompanyAccountId());
				costCombination.setAccountByCostcenterAccountId(account);
				saveSystemCombination(costCombination);
				return costCombination;
			}
			return null;
		} else {
			return null;
		}
	}

	public Combination createNaturalAccount(Combination combination,
			String accountDesc, Integer accountType, Boolean isSystem)
			throws Exception {
		Account account = accountBL.createNaturalAccount(accountDesc,
				accountType, isSystem);
		if (null != account && !account.equals("")) {
			combination = combinationService.getCombination(combination
					.getCombinationId());
			if (null != combination && !combination.equals("")) {
				Combination natural = new Combination();
				natural.setAccountByCompanyAccountId(combination
						.getAccountByCompanyAccountId());
				natural.setAccountByCostcenterAccountId(combination
						.getAccountByCostcenterAccountId());
				natural.setAccountByNaturalAccountId(account);
				saveSystemCombination(natural);
				return natural;
			}
			return null;
		} else {
			return null;
		}
	}

	public Combination createNaturalCombiantionAccount(Combination combination,
			Account account) throws Exception {
		combination = combinationService.getCombination(combination
				.getCombinationId());
		Combination natural = new Combination();
		natural.setAccountByCompanyAccountId(combination
				.getAccountByCompanyAccountId());
		natural.setAccountByCostcenterAccountId(combination
				.getAccountByCostcenterAccountId());
		natural.setAccountByNaturalAccountId(account);
		saveSystemCombination(natural);
		return natural;
	}

	public Combination createNaturalCombiantionAccount(Combination combination,
			Account account, Session session) throws Exception {
		combination = combinationService.getCombination(
				combination.getCombinationId(), session);
		Combination natural = new Combination();
		natural.setAccountByCompanyAccountId(combination
				.getAccountByCompanyAccountId());
		natural.setAccountByCostcenterAccountId(combination
				.getAccountByCostcenterAccountId());
		natural.setAccountByNaturalAccountId(account);
		saveCombination(natural, session);
		return natural;
	}

	public Combination createAnalysisCombinationAccount(
			Combination combination, Account account) throws Exception {
		combination = combinationService.getCombination(combination
				.getCombinationId());
		Combination analysisAccount = new Combination();
		analysisAccount.setAccountByCompanyAccountId(combination
				.getAccountByCompanyAccountId());
		analysisAccount.setAccountByCostcenterAccountId(combination
				.getAccountByCostcenterAccountId());
		analysisAccount.setAccountByNaturalAccountId(combination
				.getAccountByNaturalAccountId());
		analysisAccount.setAccountByAnalysisAccountId(account);
		saveSystemCombination(analysisAccount);
		return analysisAccount;
	}

	public Combination createAnalysisCombinationAccount(
			Combination combination, Account account, Session session)
			throws Exception {
		combination = combinationService.getCombination(
				combination.getCombinationId(), session);
		Combination analysisAccount = new Combination();
		analysisAccount.setAccountByCompanyAccountId(combination
				.getAccountByCompanyAccountId());
		analysisAccount.setAccountByCostcenterAccountId(combination
				.getAccountByCostcenterAccountId());
		analysisAccount.setAccountByNaturalAccountId(combination
				.getAccountByNaturalAccountId());
		analysisAccount.setAccountByAnalysisAccountId(account);
		saveCombination(analysisAccount, session);
		return analysisAccount;
	}

	public Combination createAnalysisCombination(Combination combination,
			String accountDesc, Boolean isSystem) throws Exception {
		combination = combinationService
				.getCombinationAccountDetail(combination.getCombinationId());
		Account account = accountBL.createAnalyisAccount(accountDesc, isSystem,
				combination);
		if (null != account && !account.equals("")) {
			Combination newCombiantion = new Combination();
			newCombiantion.setAccountByCompanyAccountId(combination
					.getAccountByCompanyAccountId());
			newCombiantion.setAccountByCostcenterAccountId(combination
					.getAccountByCostcenterAccountId());
			newCombiantion.setAccountByNaturalAccountId(combination
					.getAccountByNaturalAccountId());
			newCombiantion.setAccountByAnalysisAccountId(account);
			saveSystemCombination(newCombiantion);
			return newCombiantion;
		} else {
			return null;
		}
	}

	public Combination createAnalysisCombination(Combination combination,
			String accountDesc, Boolean isSystem, Implementation implementation)
			throws Exception {
		combination = combinationService
				.getCombinationAccountDetail(combination.getCombinationId());
		Account account = accountBL.createAnalyisAccount(accountDesc, isSystem,
				combination, implementation);
		if (null != account && !account.equals("")) {
			Combination newCombiantion = new Combination();
			newCombiantion.setAccountByCompanyAccountId(combination
					.getAccountByCompanyAccountId());
			newCombiantion.setAccountByCostcenterAccountId(combination
					.getAccountByCostcenterAccountId());
			newCombiantion.setAccountByNaturalAccountId(combination
					.getAccountByNaturalAccountId());
			newCombiantion.setAccountByAnalysisAccountId(account);
			newCombiantion
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			saveSystemCombination(newCombiantion);
			return newCombiantion;
		} else
			return null;
	}

	public void saveCombination(Combination combination) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (combination != null && combination.getCombinationId() != null) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			combination.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			combinationService.saveCombination(combination);

			if (null != combination.getAccountByNaturalAccountId()) {
				Ledger ledger = new Ledger();
				ledger.setCombination(combination);
				ledger.setSide(true);
				combinationService.saveLedger(ledger);
			}
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Combination.class.getSimpleName(),
					combination.getCombinationId(), user, workflowDetailVo);
		} catch (Exception e) {

		}
	}

	public void saveCombinations(List<Combination> combinations)
			throws Exception {
		combinationService.saveCombination(combinations);
		List<Ledger> ledgers = new ArrayList<Ledger>();
		Ledger ledger = null;
		for (Combination combination : combinations) {
			if (null != combination.getAccountByNaturalAccountId()) {
				ledger = new Ledger();
				ledger.setCombination(combination);
				ledger.setSide(true);
				ledgers.add(ledger);
			}
		}
		if (null != ledgers && ledgers.size() > 0)
			combinationService.saveLedger(ledgers);
	}

	public void doCombinationBusinessUpdate(Long combinationId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		if (workflowDetailVO.isDeleteFlag()) {
			Combination combination = combinationService
					.getCombinationWithLedger(combinationId);
			Ledger ledger = null;
			if (null != combination.getLedgers()
					&& combination.getLedgers().size() > 0) {
				ledger = new ArrayList<Ledger>(combination.getLedgers()).get(0);
				if ((null == ledger.getBalance() || (double) ledger
						.getBalance() == 0)
						&& (null == combination.getTransactionDetails() || combination
								.getTransactionDetails().size() <= 0)) {
					combinationService.deleteCombinationLedger(combination,
							ledger);
				}
			} else
				combinationService.deleteCombination(combination);
		}
	}

	public void saveSystemCombination(Combination combination) {
		try {
			combination.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			combinationService.saveCombination(combination);
			if (null != combination.getAccountByNaturalAccountId()) {
				Ledger ledger = new Ledger();
				ledger.setCombination(combination);
				ledger.setSide(true);
				combinationService.saveLedger(ledger);
			}
		} catch (Exception e) {

		}
	}

	public void saveCombination(Combination combination, Session session)
			throws Exception {
		combinationService.saveCombination(combination, session);
		if (null != combination.getAccountByNaturalAccountId()
				&& !combination.getAccountByNaturalAccountId().equals("")) {
			Ledger ledger = new Ledger();
			ledger.setCombination(combination);
			ledger.setSide(true);
			combinationService.saveLedger(ledger, session);
		}
	}

	public void saveAccountCombinations(List<Account> accountList,
			List<Combination> combinationList) throws Exception {
		Set<Long> accountSet = new HashSet<Long>();
		Map<Long, Account> accountMap = new HashMap<Long, Account>();
		Long accountId = 0l;
		for (Account list : accountList) {
			accountId = list.getAccountId();
			accountSet.add(accountId);
			accountMap.put(accountId, list);
		}
		Account account = null;
		accountList = new ArrayList<Account>();
		for (Long list1 : accountSet) {
			account = new Account();
			account = accountMap.get(list1);
			accountList.add(account);
		}
		accountBL.getAccountService().saveAccounts(accountList);
		combinationService.saveAccountCode(combinationList);
		Ledger ledger = null;
		List<Ledger> ledgerList = new ArrayList<Ledger>();
		for (Combination list : combinationList) {
			if (null != list.getAccountByNaturalAccountId()
					&& !list.getAccountByNaturalAccountId().equals("")) {
				ledger = new Ledger();
				ledger.setCombination(list);
				ledger.setSide(true);
				ledgerList.add(ledger);
			}
		}
		if (null != ledgerList && ledgerList.size() > 0)
			combinationService.saveLedger(ledgerList);
	}

	public void saveLedgerList(List<Combination> combinationList)
			throws Exception {
		List<Ledger> ledgerList = new ArrayList<Ledger>();
		for (Combination list : combinationList) {
			Ledger ledger = new Ledger();
			ledger.setCombination(list);
			ledger.setSide(true);
			ledgerList.add(ledger);
		}
		combinationService.saveLedger(ledgerList);
	}

	// to save ledger details
	public void saveLedger(Combination combination) throws Exception {
		Ledger ledger = new Ledger();
		ledger.setCombination(combination);
		ledger.setSide(true);
		combinationService.saveLedger(ledger);
	}

	public void createAnalysisAccountFromFile() throws Exception {
		List<Combination> combinations = this.createCombinations(this
				.combinationFileReader("D:\\AIO\\Combination.csv"));
		if (null != combinations && combinations.size() > 0) {
			combinationService.saveCombination(combinations);
			List<Ledger> ledgers = new ArrayList<Ledger>();
			for (Combination combination : combinations) {
				Ledger ledger = new Ledger();
				ledger.setCombination(combination);
				ledger.setSide(true);
				ledgers.add(ledger);
			}
			combinationService.saveLedger(ledgers);
		}
	}

	public List<Combination> createCombinations(List<String> combinations) {
		String code = "";
		try {
			Combination combination = null;
			Account companyaccount = null;
			Account costaccount = null;
			Account naturalaccount = null;
			Account analysisaccount = null;
			Implementation impl = new Implementation();
			impl.setImplementationId(6l);
			List<Combination> combinationList = new ArrayList<Combination>();
			for (String cbstr : combinations) {
				String[] str = splitValues(cbstr, ",");
				code = str[0].trim() + "." + str[1].trim() + "."
						+ str[2].trim() + "." + str[3].trim();
				combination = new Combination();
				companyaccount = accountBL.getAccountService()
						.getAccountByImplementationAndCode(impl, str[0].trim());
				costaccount = accountBL.getAccountService()
						.getAccountByImplementationAndCode(impl, str[1].trim());
				naturalaccount = accountBL.getAccountService()
						.getAccountByImplementationAndCode(impl, str[2].trim());
				analysisaccount = accountBL.getAccountService()
						.getAccountByImplementationAndCode(impl, str[3].trim());
				combination.setAccountByCompanyAccountId(companyaccount);
				combination.setAccountByCostcenterAccountId(costaccount);
				combination.setAccountByNaturalAccountId(naturalaccount);
				combination.setAccountByAnalysisAccountId(analysisaccount);
				combinationList.add(combination);
			}
			return combinationList;
		} catch (Exception ex) {
			System.out.println(code);
			ex.printStackTrace();
			return null;
		}
	}

	public List<String> combinationFileReader(String fileName) {
		File file = new File(fileName);
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	} 

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public AccountBL getAccountBL() {
		return accountBL;
	}

	public void setAccountBL(AccountBL accountBL) {
		this.accountBL = accountBL;
	}
}
