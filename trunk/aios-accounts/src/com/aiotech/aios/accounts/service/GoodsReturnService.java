package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class GoodsReturnService {
	private AIOTechGenericDAO<GoodsReturn> goodsReturnDAO = null;
	private AIOTechGenericDAO<GoodsReturnDetail> goodsReturnDetailDAO = null;

	public List<GoodsReturn> getAllGoodsReturn(Implementation implementation)
			throws Exception {
		return goodsReturnDAO.findByNamedQuery("getAllGoodsReturn",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<GoodsReturn> getActiveGoodsReturn(Implementation implementation)
			throws Exception {
		return goodsReturnDAO.findByNamedQuery("getActiveGoodsReturn",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public GoodsReturn getGoodsReturnByReturnId(Long returnId) throws Exception {
		return goodsReturnDAO.findByNamedQuery("getGoodsReturnByReturnId",
				returnId).get(0);
	}
	
	public GoodsReturn getGoodsReturnInfoByReceive(Long returnId) throws Exception {
		return goodsReturnDAO.findByNamedQuery("getGoodsReturnInfoByReceive",
				returnId).get(0);
	}
	
	

	public List<GoodsReturn> getUnDebitGoodsReturn(Implementation implementation)
			throws Exception {
		return goodsReturnDAO.findByNamedQuery("getUnDebitGoodsReturn",
				implementation);
	}

	public GoodsReturn getGoodsReturns(Long goodsReturnId) throws Exception {
		return goodsReturnDAO
				.findByNamedQuery("getGoodsReturns", goodsReturnId).get(0);
	}

	public GoodsReturn getGoodsReturnById(Long goodsReturnId) throws Exception {
		return goodsReturnDAO.findById(goodsReturnId);
	}

	public GoodsReturn getGoodsReturnSupplier(Long goodsReturnId)
			throws Exception {
		return goodsReturnDAO.findByNamedQuery("getGoodsReturnSupplier",
				goodsReturnId).get(0);
	}

	public List<GoodsReturnDetail> getGoodsReturnDetailById(Long goodsReturnId)
			throws Exception {
		return goodsReturnDetailDAO.findByNamedQuery("getGoodsReturnLines",
				goodsReturnId);
	}

	public GoodsReturnDetail getGoodsReturnDetailWithReceive(
			Long goodsReturnDetailId) throws Exception {
		return goodsReturnDetailDAO.findByNamedQuery(
				"getGoodsReturnDetailWithReceive", goodsReturnDetailId).get(0);
	}

	public List<GoodsReturnDetail> getGoodsReturnDetailsWithGRNDetail(
			Long receiveDetailId) throws Exception {
		return goodsReturnDetailDAO.findByNamedQuery(
				"getGoodsReturnDetailsWithGRNDetail", receiveDetailId);
	}

	public List<GoodsReturnDetail> getGoodsReturnDetails(Long goodsReturnId)
			throws Exception {
		return goodsReturnDetailDAO.findByNamedQuery("getGoodsReturnDetails",
				goodsReturnId);
	}

	public void deleteGoodsReturns(GoodsReturn goodsReturn,
			List<GoodsReturnDetail> goodsReturnDetail) throws Exception {
		goodsReturnDetailDAO.deleteAll(goodsReturnDetail);
		goodsReturnDAO.delete(goodsReturn);
	}

	public void saveReturns(GoodsReturn goodsReturn,
			List<GoodsReturnDetail> goodsReturnDetail) {
		goodsReturnDAO.saveOrUpdate(goodsReturn);
		for (GoodsReturnDetail list : goodsReturnDetail) {
			list.setGoodsReturn(goodsReturn);
		}
		goodsReturnDetailDAO.saveOrUpdateAll(goodsReturnDetail);
	}

	public void deleteGoodsReturnDetail(
			List<GoodsReturnDetail> goodsReturnDetails) {
		goodsReturnDetailDAO.deleteAll(goodsReturnDetails);
	}

	@SuppressWarnings("unchecked")
	public List<GoodsReturn> getFilteredGoodsReturn(
			Implementation implementation, Long returnId, Long supplierId,
			Long purchaseId, Date toDate, Date fromDate) throws Exception {
		Criteria criteria = goodsReturnDAO.createCriteria();

		criteria.createAlias("supplier", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.person", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("s.company", "scmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdlcmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("purchase", "pur", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pur.purchaseDetails", "purd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("purd.product", "prod",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("goodsReturnDetails", "grd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("grd.purchase", "grd_p",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("grd_p.purchaseDetails", "grd_pd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("grd.product", "grd_prod",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("s.implementation", implementation));
		}

		if (returnId > 0) {
			criteria.add(Restrictions.eq("returnId", returnId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date", fromDate, toDate));
		}

		if (supplierId > 0) {
			criteria.add(Restrictions.eq("s.supplierId", supplierId));
		}

		if (purchaseId > 0) {
			criteria.add(Restrictions.eq("pur.purchaseId", purchaseId));
		}

		Set<GoodsReturn> uniqueRecords = new HashSet<GoodsReturn>(
				criteria.list());
		List<GoodsReturn> goodsReturnsList = new ArrayList<GoodsReturn>(
				uniqueRecords);
		return goodsReturnsList;
	}

	public AIOTechGenericDAO<GoodsReturn> getGoodsReturnDAO() {
		return goodsReturnDAO;
	}

	public void setGoodsReturnDAO(AIOTechGenericDAO<GoodsReturn> goodsReturnDAO) {
		this.goodsReturnDAO = goodsReturnDAO;
	}

	public AIOTechGenericDAO<GoodsReturnDetail> getGoodsReturnDetailDAO() {
		return goodsReturnDetailDAO;
	}

	public void setGoodsReturnDetailDAO(
			AIOTechGenericDAO<GoodsReturnDetail> goodsReturnDetailDAO) {
		this.goodsReturnDetailDAO = goodsReturnDetailDAO;
	}
}
