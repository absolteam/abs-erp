package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.accounts.domain.entity.vo.AssetCheckOutVO;
import com.aiotech.aios.accounts.service.AssetCheckOutService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetCheckOutBL {

	private AssetCheckOutService assetCheckOutService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Asset Check Outs
	public List<Object> showCheckOutJsonList() throws Exception {
		List<AssetCheckOut> assetCheckOuts = assetCheckOutService
				.getAllAssetCheckOuts(getImplementation());
		List<Object> assetCheckOutVOs = new ArrayList<Object>();
		AssetCheckOutVO assetCheckOutVO = null;
		if (null != assetCheckOuts && assetCheckOuts.size() > 0) {
			for (AssetCheckOut assetCheckOut : assetCheckOuts) {
				assetCheckOutVO = new AssetCheckOutVO();
				assetCheckOutVO.setAssetCheckOutId(assetCheckOut
						.getAssetCheckOutId());
				assetCheckOutVO.setCheckOutNumber(assetCheckOut
						.getCheckOutNumber());
				assetCheckOutVO.setAssetName(assetCheckOut.getAssetCreation()
						.getAssetName());
				/*assetCheckOutVO.setOutDue(Constants.Accounts.AssetCheckOutDue
						.get(assetCheckOut.getCheckOutDue()).name());*/
				assetCheckOutVO.setOutDate(DateFormat
						.convertDateToString(assetCheckOut.getCheckOutDate()
								.toString()));
				assetCheckOutVO.setCheckOutTo(assetCheckOut
						.getPersonByCheckOutTo()
						.getFirstName()
						.concat(" ")
						.concat(assetCheckOut.getPersonByCheckOutTo()
								.getLastName()));
				assetCheckOutVOs.add(assetCheckOutVO);
			}
		}
		return assetCheckOutVOs;
	}

	// Show All Asset Check Outs
	public List<Object> showActiveCheckOutJsonList() throws Exception {
		List<AssetCheckOut> assetCheckOuts = assetCheckOutService
				.getActiveAssetCheckOuts(getImplementation());
		List<Object> assetCheckOutVOs = new ArrayList<Object>();
		if (null != assetCheckOuts && assetCheckOuts.size() > 0) {
			for (AssetCheckOut assetCheckOut : assetCheckOuts)
				assetCheckOutVOs.add(addAssetCheckOuts(assetCheckOut));
		}
		return assetCheckOutVOs;
	}

	private AssetCheckOutVO addAssetCheckOuts(AssetCheckOut assetCheckOut) {
		AssetCheckOutVO assetCheckOutVO = null;
		assetCheckOutVO = new AssetCheckOutVO();
		assetCheckOutVO.setAssetNumber(assetCheckOut.getAssetCreation()
				.getAssetNumber());
		assetCheckOutVO.setAssetCheckOutId(assetCheckOut.getAssetCheckOutId());
		assetCheckOutVO.setCheckOutNumber(assetCheckOut.getCheckOutNumber());
		assetCheckOutVO.setAssetName(assetCheckOut.getAssetCreation()
				.getAssetName());
		/*assetCheckOutVO.setOutDue(Constants.Accounts.AssetCheckOutDue.get(
				assetCheckOut.getCheckOutDue()).name());*/
		assetCheckOutVO.setOutDate(DateFormat.convertDateToString(assetCheckOut
				.getCheckOutDate().toString()));
		assetCheckOutVO.setCheckOutTo(assetCheckOut.getPersonByCheckOutTo()
				.getFirstName().concat(" ")
				.concat(assetCheckOut.getPersonByCheckOutTo().getLastName()));
		assetCheckOutVO.setPersonId(assetCheckOut.getPersonByCheckOutTo()
				.getPersonId());
		return assetCheckOutVO;
	}

	// Save Asset Check Out
	public void saveAssetCheckOut(AssetCheckOut assetCheckOut) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetCheckOut.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		if (null == assetCheckOut.getAssetCheckOutId())
			SystemBL.saveReferenceStamp(AssetCheckOut.class.getName(),
					getImplementation());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetCheckOut != null && assetCheckOut.getAssetCheckOutId() != null
				&& assetCheckOut.getAssetCheckOutId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetCheckOutService.saveAssetCheckOut(assetCheckOut);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetCheckOut.class.getSimpleName(),
				assetCheckOut.getAssetCheckOutId(), user, workflowDetailVo);
	}

	public void doAssetCheckOutBusinessUpdate(Long assetCheckOutId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				AssetCheckOut assetCheckOut = this.getAssetCheckOutService()
						.getAssetCheckOutDetails(assetCheckOutId);
				assetCheckOutService.deleteAssetCheckOut(assetCheckOut);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Asset Check Out
	public void deleteAssetCheckOut(AssetCheckOut assetCheckOut)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetCheckOut.class.getSimpleName(),
				assetCheckOut.getAssetCheckOutId(), user, workflowDetailVo);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetCheckOutService getAssetCheckOutService() {
		return assetCheckOutService;
	}

	public void setAssetCheckOutService(
			AssetCheckOutService assetCheckOutService) {
		this.assetCheckOutService = assetCheckOutService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
