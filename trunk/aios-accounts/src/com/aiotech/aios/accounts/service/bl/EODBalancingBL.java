package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CurrencySetting;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.EODBalancing;
import com.aiotech.aios.accounts.domain.entity.EODBalancingDetail;
import com.aiotech.aios.accounts.domain.entity.EODProductionDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialWastageDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.domain.entity.vo.EODBalancingVO;
import com.aiotech.aios.accounts.service.EODBalancingService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class EODBalancingBL {

	private EODBalancingService eodBalancingService;
	private PointOfSaleBL pointOfSaleBL;
	private MaterialIdleMixBL materialIdleMixBL;
	private MaterialWastageBL materialWastageBL;

	// Show all EOD Balancing
	public List<Object> showAllEODBalancing() throws Exception {
		List<EODBalancing> eodBalancings = eodBalancingService
				.getAllEODBalancing(getImplementation());
		List<Object> eodBalancingVOs = new ArrayList<Object>();
		if (null != eodBalancings && eodBalancings.size() > 0) {
			for (EODBalancing eodBalancing : eodBalancings)
				eodBalancingVOs.add(addEODBalancingVO(eodBalancing));
		}
		return eodBalancingVOs;
	}

	// Add EODBalancingVO
	private EODBalancingVO addEODBalancingVO(EODBalancing eodBalancing) {
		EODBalancingVO eodBalancingVO = new EODBalancingVO();
		eodBalancingVO.setEodDate(DateFormat.convertDateToString(eodBalancing
				.getDate().toString()));
		eodBalancingVO.setReferenceNumber(eodBalancing.getReferenceNumber());
		eodBalancingVO.setEodBalancingId(eodBalancing.getEodBalancingId());
		eodBalancingVO.setPosUserName(eodBalancing.getPerson()
					.getFirstName()
					.concat(" " + eodBalancing.getPerson().getLastName()));
		eodBalancingVO.setStoreName(eodBalancing.getStore().getStoreName());
		return eodBalancingVO;
	}

	// Get All Currency Settings
	public List<EODBalancingVO> getAllCurrencySettings() throws Exception {
		List<CurrencySetting> currencySettings = eodBalancingService
				.getAllCurrencySettings();
		List<EODBalancingVO> eodBalancingVOs = new ArrayList<EODBalancingVO>();
		for (CurrencySetting currencySetting : currencySettings) {
			eodBalancingVOs.add(addCurrencySetting(currencySetting));
		}
		return eodBalancingVOs;
	}

	// Save EOD Production Balancing
	public void saveEODProduction(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails,
			List<EODProductionDetail> eodDProductionDetails,
			List<MaterialWastageDetail> wastageDetails,
			List<PointOfSale> pointOfSales, Store store) throws Exception {
		SystemBL.saveReferenceStamp(EODBalancing.class.getName(),
				getImplementation());
		if (null != wastageDetails && wastageDetails.size() > 0) {
			for (MaterialWastageDetail materialWastageDetail : wastageDetails)
				materialWastageDetail.setStatus(false);
			materialWastageBL.updateMaterialWastageDetails(wastageDetails);
		}
		pointOfSaleBL.updateEODProductionSales(pointOfSales);
		eodBalancingService.saveEODProduction(eodBalancing,
				eodBalancingDetails, eodDProductionDetails);
		// Update Stock & Transaction
		if (null != eodDProductionDetails && eodDProductionDetails.size() > 0) {
			List<Stock> stocks = getStockDetails(eodDProductionDetails, store);
			pointOfSaleBL.getStockBL().updateStockDetails(null, stocks);
			// Cost Price Transaction
			Transaction transaction = pointOfSaleBL.getTransactionBL()
					.createTransaction(
							pointOfSaleBL.getTransactionBL().getCurrency()
									.getCurrencyId(),
							"Production Cost Entry",
							Calendar.getInstance().getTime(),
							pointOfSaleBL.getTransactionBL().getCategory(
									"PointOfSale"),
							eodBalancing.getEodBalancingId(),
							EODBalancing.class.getSimpleName());
			List<TransactionDetail> transactionDetails = createCostTransactionDetails(eodDProductionDetails);
			pointOfSaleBL.getTransactionBL().saveTransaction(transaction,
					transactionDetails);
		}
	}

	private List<TransactionDetail> createCostTransactionDetails(
			List<EODProductionDetail> eodDProductionDetails) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		for (EODProductionDetail eodProductionDetail : eodDProductionDetails) {
			transactionDetails.add(createCOSEntry(eodProductionDetail,
					TransactionType.Debit.getCode()));
			transactionDetails.add(createCOSInventoryEntry(eodProductionDetail,
					TransactionType.Credit.getCode()));
		}
		return transactionDetails;
	}

	private TransactionDetail createCOSInventoryEntry(
			EODProductionDetail eodProductionDetail, boolean transactionType)
			throws Exception {
		double costPrice = 0;
		Combination combination = pointOfSaleBL
				.getProductPricingBL()
				.getProductBL()
				.getProductInventory(
						eodProductionDetail.getProduct().getProductId());
		ProductPricingDetail productPricingDetail = pointOfSaleBL
				.getProductPricingBL().getPricingDetail(
						eodProductionDetail.getProduct().getProductId());
		costPrice = productPricingDetail.getBasicCostPrice()
				* (eodProductionDetail.getProductQuantity() + eodProductionDetail
						.getProductionWastage());
		return pointOfSaleBL.getTransactionBL().createTransactionDetail(
				costPrice, combination.getCombinationId(), transactionType,
				null, null, null);
	}

	private TransactionDetail createCOSEntry(
			EODProductionDetail eodProductionDetail, boolean transactionType)
			throws Exception {
		double costPrice = 0;
		ProductPricingDetail productPricingDetail = pointOfSaleBL
				.getProductPricingBL().getPricingDetail(
						eodProductionDetail.getProduct().getProductId());
		costPrice = productPricingDetail.getBasicCostPrice()
				* (eodProductionDetail.getProductQuantity() + eodProductionDetail
						.getProductionWastage());
		return pointOfSaleBL.getTransactionBL().createTransactionDetail(
				costPrice, getImplementation().getExpenseAccount(),
				transactionType, null, null, null);
	}

	private List<Stock> getStockDetails(
			List<EODProductionDetail> eodDProductionDetails, Store store)
			throws Exception {
		Stock stock = null;
		double stockQuantity = 0;
		List<Stock> stocks = new ArrayList<Stock>();
		for (EODProductionDetail eodProductionDetail : eodDProductionDetails) {
			stock = new Stock();
			stockQuantity = eodProductionDetail.getProductQuantity()
					+ eodProductionDetail.getProductionWastage();
			stock.setStore(store);
			stock.setProduct(eodProductionDetail.getProduct());
			stock.setImplementation(getImplementation());
			stock.setQuantity(stockQuantity);
			stocks.add(stock);
		}
		return stocks;
	}

	// Save EOD Balancing
	public void saveEODBalancing(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails,
			List<PointOfSale> pointOfSales, CustomerVO customerVO)
			throws Exception {
		eodBalancingService.saveEODBalancing(eodBalancing, eodBalancingDetails);
		SystemBL.saveReferenceStamp(EODBalancing.class.getName(),
				eodBalancing.getImplementation());
		if (null != pointOfSales && pointOfSales.size() > 0)
			pointOfSaleBL.updateEODPointOfSales(pointOfSales);
		if (null != customerVO && null != customerVO.getCustomers()
				&& customerVO.getCustomers().size() > 0) {
			Customer customer = null;
			List<Customer> customers = new ArrayList<Customer>();
			for (Customer customervo : customerVO.getCustomers()) {
				customer = pointOfSaleBL.getCustomerBL().getCustomerService()
						.getCustomerDetails(customervo.getCustomerId());
				customer.setCustomerRecordId(customervo.getCustomerRecordId());
				customer.setPosCustomer(null);
				customers.add(customer);
			}
			pointOfSaleBL.getCustomerBL().updateCustomers(customers);
		}
	} 

	// Save EOD Balancing
	public void saveOfflineEODBalancing(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails) throws Exception {
		eodBalancingService.saveEODBalancing(eodBalancing, eodBalancingDetails);
		SystemBL.saveReferenceStamp(EODBalancing.class.getName(),
				eodBalancing.getImplementation());
	}

	// Add Currency Setting
	private EODBalancingVO addCurrencySetting(CurrencySetting currencySetting)
			throws Exception {
		EODBalancingVO eodBalancingVO = new EODBalancingVO();
		eodBalancingVO.setCurrencySettingId(currencySetting
				.getCurrencySettingId());
		eodBalancingVO.setCurrencyMode(Constants.Accounts.CurrencyMode.get(
				currencySetting.getCurrencyMode()).name());
		eodBalancingVO.setCurrencyType(Constants.Accounts.CurrencyType.get(
				currencySetting.getCurrencyType()).name());
		eodBalancingVO.setDenomination(currencySetting.getDenomination());
		eodBalancingVO.setEquiavalent(currencySetting.getEquiavalent());
		return eodBalancingVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public EODBalancingService getEodBalancingService() {
		return eodBalancingService;
	}

	public void setEodBalancingService(EODBalancingService eodBalancingService) {
		this.eodBalancingService = eodBalancingService;
	}

	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public MaterialIdleMixBL getMaterialIdleMixBL() {
		return materialIdleMixBL;
	}

	public void setMaterialIdleMixBL(MaterialIdleMixBL materialIdleMixBL) {
		this.materialIdleMixBL = materialIdleMixBL;
	}

	public MaterialWastageBL getMaterialWastageBL() {
		return materialWastageBL;
	}

	public void setMaterialWastageBL(MaterialWastageBL materialWastageBL) {
		this.materialWastageBL = materialWastageBL;
	}
}
