package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class POSUserTillService {
	private AIOTechGenericDAO<POSUserTill> pOSUserTillDAO;

	public List<POSUserTill> getAllPosUserTill(Implementation implementation) {
		return pOSUserTillDAO.findByNamedQuery("getAllPosUserTill",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	public POSUserTill getPosUserTillInformation(Long posUserTillId) {
		List<POSUserTill> posUserTills = pOSUserTillDAO.findByNamedQuery(
				"getPosUserTillInformation", posUserTillId);
		if (posUserTills != null && posUserTills.size() > 0)
			return posUserTills.get(0);
		else
			return null;
	}
	
	public POSUserTill getPosUserTillByPersonId(long personId) {
		List<POSUserTill> posUserTills = pOSUserTillDAO.findByNamedQuery(
				"getPosUserTillByPersonId", personId);
		return posUserTills != null && posUserTills.size() > 0 ? posUserTills.get(0) : null;
	}
	
	public POSUserTill getPosUserTillById(long posUserTillId) throws Exception { 
		return pOSUserTillDAO.findById(posUserTillId);
	}

	public void savePOSUserTill(POSUserTill posUserTill) {
		pOSUserTillDAO.saveOrUpdate(posUserTill);
	}

	public void deletePOSUserTill(POSUserTill posUserTill) {
		pOSUserTillDAO.delete(posUserTill);
	}

	public AIOTechGenericDAO<POSUserTill> getPOSUserTillDAO() {
		return pOSUserTillDAO;
	}

	public void setPOSUserTillDAO(AIOTechGenericDAO<POSUserTill> pOSUserTillDAO) {
		this.pOSUserTillDAO = pOSUserTillDAO;
	}
}
