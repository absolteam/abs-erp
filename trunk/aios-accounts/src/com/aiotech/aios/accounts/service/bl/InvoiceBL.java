package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceVO;
import com.aiotech.aios.accounts.service.InvoiceService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class InvoiceBL {

	// Dependencies
	private InvoiceService invoiceService;
	private Implementation implementation;
	private ReceiveBL receiveBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private DirectPaymentBL directPaymentBL;
	private AlertBL alertBL;
	private CommentBL commentBL;

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public List<Object> getAllContinuousPurchaseInvoice() throws Exception {
		List<Invoice> invoices = invoiceService
				.getAllContinuosPurchaseInvoice(getImplementationId());
		Map<Long, InvoiceVO> invoiceVOMap = new HashMap<Long, InvoiceVO>();
		if (null != invoices && invoices.size() > 0) {
			for (Invoice invoice : invoices) {
				InvoiceVO invoiceVO = new InvoiceVO();
				if (null != invoice.getInvoice()) {
					if (invoiceVOMap.containsKey(invoice.getInvoice()
							.getInvoiceId())) {
						invoiceVO = invoiceVOMap.get(invoice.getInvoice()
								.getInvoiceId());
						invoiceVO
								.setInvoiceAmount(invoice.getInvoiceAmount()
										+ (null != invoiceVO.getInvoiceAmount() ? invoiceVO
												.getInvoiceAmount() : 0));
						invoiceVO.setInvoiceAmountStr(AIOSCommons
								.formatAmount(invoiceVO.getInvoiceAmount()));
					}
				} else {
					invoiceVO.setInvoiceAmount(invoice.getInvoiceAmount()
							+ (null != invoiceVO.getInvoiceAmount() ? invoiceVO
									.getInvoiceAmount() : 0));
					invoiceVO.setInvoiceAmountStr(AIOSCommons
							.formatAmount(invoiceVO.getInvoiceAmount()));
					invoiceVO.setTotalInvoice(invoice.getTotalInvoice());
					invoiceVO.setTotalInvoiceStr(AIOSCommons
							.formatAmount(invoiceVO.getTotalInvoice()));
					invoiceVO.setInvoiceDate(DateFormat
							.convertDateToString(invoice.getDate().toString()));
					invoiceVO.setInvoiceNumber(invoice.getInvoiceNumber());
					invoiceVO.setInvoiceId(invoice.getInvoiceId());
					invoiceVO.setSupplierName(null != invoice.getSupplier()
							.getPerson() ? invoice
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat("")
							.concat(invoice.getSupplier().getPerson()
									.getLastName()) : (null != invoice
							.getSupplier().getCompany() ? invoice.getSupplier()
							.getCompany().getCompanyName() : invoice
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName()));
					invoiceVO.setPendingInvoice(invoiceVO.getTotalInvoice()
							- invoiceVO.getInvoiceAmount());
					invoiceVO.setPendingInvoiceStr(AIOSCommons
							.formatAmount(invoiceVO.getPendingInvoice()));
					invoiceVO.setProcessStatus(invoice.getProcessStatus());
					invoiceVO
							.setCreatedPerson(null != invoice.getPerson() ? invoice
									.getPerson().getFirstName()
									+ " "
									+ invoice.getPerson().getLastName() : "");
					invoiceVOMap.put(invoice.getInvoiceId(), invoiceVO);
				}
			}
		}
		List<InvoiceVO> invoiceVOs = new ArrayList<InvoiceVO>();
		if (null != invoiceVOMap && invoiceVOMap.size() > 0)
			invoiceVOs.addAll(new ArrayList<InvoiceVO>(invoiceVOMap.values()));
		List<Object> invoiceVOtemps = new ArrayList<Object>();
		for (InvoiceVO invoiceVO2 : invoiceVOs) {
			if (null != invoiceVO2.getProcessStatus()
					&& (boolean) invoiceVO2.getProcessStatus()) {
				if (invoiceVO2.getTotalInvoice()
						- invoiceVO2.getInvoiceAmount() > 0) {
					invoiceVO2.setPendingInvoice(AIOSCommons
							.roundDecimals(invoiceVO2.getTotalInvoice()
									- invoiceVO2.getInvoiceAmount()));
					invoiceVO2.setPendingInvoiceStr(AIOSCommons
							.formatAmount(invoiceVO2.getPendingInvoice()));
					invoiceVOtemps.add(invoiceVO2);
				}
			}
		}
		return invoiceVOtemps;
	}

	public InvoiceVO converttInvoiceEntityIntoVO(Invoice invoice)
			throws Exception {
		InvoiceVO invoiceVO = new InvoiceVO();
		BeanUtils.copyProperties(invoiceVO, invoice);
		invoiceVO.setTotalInvoice(invoice.getTotalInvoice());

		double invoiceAmount = 0;
		if (null != invoice.getInvoices() && invoice.getInvoices().size() > 0) {
			for (Invoice inv : invoice.getInvoices()) {
				invoiceAmount = null != inv.getInvoiceAmount() ? inv
						.getInvoiceAmount() + invoiceAmount : invoiceAmount;
			}
		}

		if (invoice.getInvoice() != null
				&& null != invoice.getInvoice().getInvoices()
				&& invoice.getInvoice().getInvoices().size() > 0) {
			for (Invoice inv : invoice.getInvoice().getInvoices()) {
				if (inv.getInvoiceId() != invoice.getInvoice().getInvoiceId())
					invoiceAmount = null != inv.getInvoiceAmount() ? inv
							.getInvoiceAmount() + invoiceAmount : invoiceAmount;
			}
			invoiceAmount = +invoice.getInvoice().getInvoiceAmount();
		}

		if (invoice.getInvoice() != null)
			invoiceVO.setTotalInvoice(invoice.getInvoice().getTotalInvoice());
		else
			invoiceVO.setTotalInvoice(invoice.getTotalInvoice());
		invoiceVO
				.setPendingInvoice(invoiceVO.getTotalInvoice() - invoiceAmount);

		return invoiceVO;
	}

	public JSONObject getInvoiceList(List<Invoice> invoices) throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
 		for (Invoice list : invoices) {
			array = new JSONArray();
			array.add(list.getInvoiceId());
			array.add(list.getSupplier().getSupplierId());
			array.add(list.getInvoiceNumber());
			array.add(DateFormat.convertDateToString(list.getDate().toString()));
			array.add(list.getSupplier().getSupplierNumber()); 
			if (null != list.getSupplier().getPerson())
				array.add(list.getSupplier().getPerson().getFirstName()
						.concat(" ")
						.concat(list.getSupplier().getPerson().getLastName()));
			else
				array.add(null != list.getSupplier().getCmpDeptLocation() ? list
						.getSupplier().getCmpDeptLocation().getCompany()
						.getCompanyName()
						: list.getSupplier().getCompany().getCompanyName());

			if (null != list.getInvoiceDetails()
					&& list.getInvoiceDetails().size() > 0) {
				Set<String> receiveNumbers = new HashSet<String>();
				Set<String> refNumbers = new HashSet<String>();
				for (InvoiceDetail invoiceDetail : list.getInvoiceDetails()) {
					receiveNumbers.add(
							invoiceDetail.getReceiveDetail().getReceive()
									.getReceiveNumber());
					if(null != invoiceDetail.getReceiveDetail()
									.getReceive().getReferenceNo() )
						refNumbers
							.add(invoiceDetail
									.getReceiveDetail().getReceive()
									.getReferenceNo());
				}
				StringBuilder receiveNumber = new StringBuilder();
				for (String receiveNum : receiveNumbers)
					receiveNumber.append(receiveNum).append(" ,");
				
				StringBuilder refNumber = new StringBuilder();
				for (String refNum : refNumbers)
					refNumber.append(refNum).append(" ,");
				
				array.add((null !=refNumber) ? refNumber.toString() : "");
			}else
				array.add("");
			array.add(AIOSCommons.formatAmount(list.getInvoiceAmount()));
			array.add(Constants.Accounts.InvoiceStatus.get(list.getStatus()));

			if (list.getStatus() != null
					&& (byte) Constants.Accounts.InvoiceStatus.Paid.getCode() == (byte) list
							.getStatus())
				array.add(true);
			else
				array.add(false);

			if (list.getInvoices() != null && list.getInvoices().size() > 0)
				array.add(true);
			else
				array.add(false);
			array.add(null != list.getPerson() ? list.getPerson()
					.getFirstName() + " " + list.getPerson().getLastName() : "");
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public void saveInvoice(Invoice invoice, List<Receive> receiveList,
			List<InvoiceDetail> detailsDelete, Invoice editInvoice)
			throws Exception {
		invoice.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (invoice != null && invoice.getInvoiceId() != null
				&& invoice.getInvoiceId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (editInvoice != null && editInvoice.getInvoiceId() != null) {

			deleteDirectInvoice(editInvoice, receiveList);
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		}
		/*
		 * if (null != detailsDelete && detailsDelete.size() > 0)
		 * invoiceService.deleteInvoiceDetail(detailsDelete);
		 */
		if (null == invoice.getInvoiceId())
			SystemBL.saveReferenceStamp(Invoice.class.getName(),
					getImplementationId());

		for (InvoiceDetail detail : invoice.getInvoiceDetails()) {

			detail.setReceiveDetail(receiveBL.getReceiveService()
					.getReceiveDetailById(
							detail.getReceiveDetail().getReceiveDetailId()));
		}

		invoiceService.saveInvoice(invoice, new ArrayList<InvoiceDetail>(
				invoice.getInvoiceDetails()));

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Invoice.class.getSimpleName(), invoice.getInvoiceId(), user,
				workflowDetailVo);
	}

	public void saveInvoice(Invoice invoice) throws Exception {
		invoice.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (invoice != null && invoice.getInvoiceId() != null
				&& invoice.getInvoiceId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (null == invoice.getInvoiceId())
			SystemBL.saveReferenceStamp(Invoice.class.getName(),
					getImplementationId());
		invoiceService.saveInvoice(invoice);
		// Alert Invoice
		List<Invoice> invoices = new ArrayList<Invoice>();
		invoices.add(invoice);
		alertBL.alertPurchaseInvoiceProcess(invoices);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Invoice.class.getSimpleName(), invoice.getInvoiceId(), user,
				workflowDetailVo);
	}

	public void deleteDirectInvoice(Invoice invoice, List<Receive> receiveList)
			throws Exception {

		// Skip the current delivery not from update conflicts
		Map<Long, Receive> receives = new HashMap<Long, Receive>();
		if (receiveList != null && receiveList.size() > 0)
			for (Receive receive : receiveList) {
				receives.put(receive.getReceiveId(), receive);
			}

		receiveList = new ArrayList<Receive>();
		for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
			if (!receives.containsKey(invoiceDetail.getReceiveDetail()
					.getReceive().getReceiveId())) {
				Receive receive = invoiceDetail.getReceiveDetail().getReceive();
				receive.setStatus((byte) 1);//
				receiveList.add(receive);
			}
		}
		invoiceService.deleteInvoice(invoice, new ArrayList<InvoiceDetail>(
				invoice.getInvoiceDetails()));
		alertBL.commonAlertDeleteByUsecaseAndRecord(
				Invoice.class.getSimpleName(), invoice.getInvoiceId());

		if (null != receiveList && receiveList.size() > 0)
			receiveBL.getReceiveService().saveReceipts(receiveList);

	}

	public void deleteInvoice(Invoice invoice, List<Receive> receiveList)
			throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Invoice.class.getSimpleName(), invoice.getInvoiceId(), user,
				workflowDetailVo);

	}

	public void doInvoiceBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Invoice invoice = invoiceService.getInvoiceInfoById(recordId);
		Map<Long, Receive> receiveDeails = new HashMap<Long, Receive>();
		for (InvoiceDetail detail : invoice.getInvoiceDetails()) {
			try {
				detail.getReceiveDetail().getReceive().setStatus((byte) 1);
				receiveDeails
						.put(detail.getReceiveDetail().getReceive()
								.getReceiveId(), detail.getReceiveDetail()
								.getReceive());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		List<Receive> receiveList = new ArrayList<Receive>(
				receiveDeails.values());
		if (workflowDetailVO.isDeleteFlag()) {
			// Skip the current delivery not from update conflicts
			Map<Long, Receive> receives = new HashMap<Long, Receive>();
			if (receiveList != null && receiveList.size() > 0)
				for (Receive receive : receiveList) {
					receives.put(receive.getReceiveId(), receive);
				}

			receiveList = new ArrayList<Receive>();
			for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
				if (!receives.containsKey(invoiceDetail.getReceiveDetail()
						.getReceive().getReceiveId())) {
					Receive receive = invoiceDetail.getReceiveDetail()
							.getReceive();
					receive.setStatus((byte) 1);//
					receiveList.add(receive);
				}
			}
			invoiceService.deleteInvoice(invoice, new ArrayList<InvoiceDetail>(
					invoice.getInvoiceDetails()));
			alertBL.commonAlertDeleteByUsecaseAndRecord(
					Invoice.class.getSimpleName(), invoice.getInvoiceId());

			if (null != receiveList && receiveList.size() > 0)
				receiveBL.getReceiveService().saveReceipts(receiveList);
		} else {
			receiveList = new ArrayList<Receive>();
			for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
				Receive receive = invoiceDetail.getReceiveDetail().getReceive();
				receive.setStatus((byte) 0);//
				receiveList.add(receive);
			}
			if (null != receiveList && receiveList.size() > 0)
				receiveBL.getReceiveService().saveReceipts(receiveList);
			// Alert Invoice
			List<Invoice> invoices = new ArrayList<Invoice>();
			invoices.add(invoice);
			alertBL.alertPurchaseInvoiceProcess(invoices);
		}
	}

	// Validate Invoice Details for payments
	public List<InvoiceVO> validateInvoiceDetail(
			List<InvoiceDetail> invoiceDetailList) throws Exception {
		InvoiceVO invoiceVO = null;
		InvoiceDetailVO invoiceDetailVO = null;
		Map<Long, InvoiceVO> invoiceVOHash = new HashMap<Long, InvoiceVO>();
		Map<Long, List<InvoiceDetailVO>> invoiceDetailVOHash = new HashMap<Long, List<InvoiceDetailVO>>();
		List<InvoiceDetailVO> invoiceDetails = new ArrayList<InvoiceDetailVO>();
		for (InvoiceDetail invoiceDetail : invoiceDetailList) {
			invoiceVO = new InvoiceVO();
			invoiceDetailVO = new InvoiceDetailVO();
			invoiceVO.setInvoiceId(invoiceDetail.getInvoice().getInvoiceId());
			invoiceVO.setInvoiceNumber(invoiceDetail.getInvoice()
					.getInvoiceNumber());
			invoiceVO.setDate(invoiceDetail.getInvoice().getDate());
			invoiceVO.setSupplier(invoiceDetail.getInvoice().getSupplier());
			invoiceVO.setDescription(invoiceDetail.getInvoice()
					.getDescription());
			invoiceDetailVO.setInvoiceDetailId(invoiceDetail
					.getInvoiceDetailId());
			invoiceDetailVO.setProduct(invoiceDetail.getProduct());
			invoiceDetailVO.setInvoiceQty(invoiceDetail.getInvoiceQty());
			invoiceDetailVO.setUnitRate(invoiceDetail.getReceiveDetail()
					.getUnitRate());
			invoiceDetailVO.setDescription(invoiceDetail.getDescription());
			invoiceDetails.add(invoiceDetailVO);
			List<InvoiceDetailVO> invoiceDetailVOList = invoiceDetailVOHash
					.get(invoiceVO.getInvoiceId());
			if (null != invoiceDetailVOList && invoiceDetailVOList.size() > 0) {
				invoiceDetailVOList.add(invoiceDetailVO);
				invoiceDetailVOHash.put(invoiceVO.getInvoiceId(),
						invoiceDetailVOList);
			} else {
				invoiceDetailVOList = new ArrayList<InvoiceDetailVO>();
				invoiceDetailVOList.add(invoiceDetailVO);
				invoiceDetailVOHash.put(invoiceVO.getInvoiceId(),
						invoiceDetailVOList);
			}
			invoiceVOHash.put(invoiceVO.getInvoiceId(), invoiceVO);
		}
		for (Entry<Long, List<InvoiceDetailVO>> invoiceId : invoiceDetailVOHash
				.entrySet()) {
			List<InvoiceDetailVO> invoiceDetailVOList = invoiceDetailVOHash
					.get(invoiceId.getKey());
			if (invoiceVOHash.containsKey(invoiceId.getKey())) {
				invoiceVO = invoiceVOHash.get(invoiceId.getKey());
				invoiceVO.setInvoiceDetailVOList(invoiceDetailVOList);
				for (InvoiceDetailVO invoiceDetail : invoiceVO
						.getInvoiceDetailVOList()) {
					invoiceVO
							.setTotalInvoiceQty(invoiceDetail.getInvoiceQty()
									+ (null != invoiceVO.getTotalInvoiceQty() ? invoiceVO
											.getTotalInvoiceQty() : 0));
					invoiceVO
							.setTotalInvoiceAmount(invoiceDetail
									.getInvoiceQty()
									* invoiceDetail.getUnitRate()
									+ (null != invoiceVO
											.getTotalInvoiceAmount() ? invoiceVO
											.getTotalInvoiceAmount() : 0));
				}
				invoiceVOHash.put(invoiceId.getKey(), invoiceVO);
			}
		}
		return new ArrayList<InvoiceVO>(invoiceVOHash.values());
	}

	// Get all purchase Invoice
	public List<InvoiceVO> getAllContiniousInvoiceForReportData(
			InvoiceVO salesInvoiceVOTemp) throws Exception {
		List<Invoice> salesInvoices = invoiceService.getFilteredInvoice(
				salesInvoiceVOTemp.getImplementation(),
				salesInvoiceVOTemp.getSupplierId(),
				salesInvoiceVOTemp.getFormDate(),
				salesInvoiceVOTemp.getToDate());
		List<InvoiceVO> salesInvoiceVOsFinal = null;
		List<InvoiceVO> salesInvoiceVOs = null;
		InvoiceVO salesInvoiceVO = null;
		Map<Long, InvoiceVO> completeMap = new HashMap<Long, InvoiceVO>();
		Map<Long, List<InvoiceVO>> salesInvoiceMap = new HashMap<Long, List<InvoiceVO>>();
		for (Invoice salesInvoice : salesInvoices) {
			if ((salesInvoice.getInvoice() != null && salesInvoiceMap
					.containsKey(salesInvoice.getInvoice().getInvoiceId()))) {
				salesInvoiceVOs = salesInvoiceMap.get(salesInvoice.getInvoice()
						.getInvoiceId());
				salesInvoiceVO = new InvoiceVO();
				salesInvoiceVO
						.setInvoiceAmount(salesInvoice.getInvoiceAmount()
								+ (null != salesInvoiceVO.getInvoiceAmount() ? salesInvoiceVO
										.getInvoiceAmount() : 0));

				Double totalInvoiceByNow = salesInvoiceVOs.get(
						salesInvoiceVOs.size() - 1).getPendingInvoice();
				salesInvoiceVO.setTotalInvoice(totalInvoiceByNow);
				salesInvoiceVO
						.setInvoiceDate(DateFormat
								.convertDateToString(salesInvoice.getDate()
										.toString()));
				salesInvoiceVO
						.setInvoiceNumber(salesInvoice.getInvoiceNumber());
				salesInvoiceVO.setInvoiceId(salesInvoice.getInvoiceId());
				salesInvoiceVO.setSupplierName(null != salesInvoice
						.getSupplier().getPerson() ? salesInvoice
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat("")
						.concat(salesInvoice.getSupplier().getPerson()
								.getLastName()) : (null != salesInvoice
						.getSupplier().getCompany() ? salesInvoice
						.getSupplier().getCompany().getCompanyName()
						: salesInvoice.getSupplier().getCmpDeptLocation()
								.getCompany().getCompanyName()));

				salesInvoiceVO.setPendingInvoice(AIOSCommons
						.roundDecimals(salesInvoiceVO.getTotalInvoice()
								- salesInvoiceVO.getInvoiceAmount()));
				salesInvoiceVO
						.setProcessStatus(salesInvoice.getProcessStatus());
				salesInvoiceVO.setSupplierNumber(salesInvoice.getSupplier()
						.getSupplierNumber());
				salesInvoiceVO.setParentInvoice(salesInvoice.getInvoice()
						.getInvoiceNumber());
				salesInvoiceVO.setSupplierId(salesInvoice.getSupplier()
						.getSupplierId());
				Map<String, String> salesNo = new HashMap<String, String>();
				Map<String, String> dt = new HashMap<String, String>();
				for (InvoiceDetail salesInvoiceDetail : salesInvoice
						.getInvoice().getInvoiceDetails()) {
					salesNo.put(salesInvoiceDetail.getReceiveDetail()
							.getReceive().getReceiveNumber(),
							salesInvoiceDetail.getReceiveDetail().getReceive()
									.getReceiveNumber());
					dt.put(DateFormat.convertDateToString(salesInvoiceDetail
							.getReceiveDetail().getReceive().getReceiveDate()
							+ ""),
							DateFormat.convertDateToString(salesInvoiceDetail
									.getReceiveDetail().getReceive()
									.getReceiveDate()
									+ ""));
				}
				salesInvoiceVO.setReceiveNumber("");
				for (Map.Entry<String, String> entry : salesNo.entrySet()) {
					salesInvoiceVO.setReceiveNumber(salesInvoiceVO
							.getReceiveNumber() + "," + entry.getValue());
				}
				salesInvoiceVO.setReceiveDate("");
				for (Map.Entry<String, String> entry : dt.entrySet()) {
					salesInvoiceVO.setReceiveDate(salesInvoiceVO
							.getReceiveDate() + "," + entry.getValue());
				}

				salesInvoiceVO.setInvoiceDate(DateFormat
						.convertDateToString(salesInvoice.getDate() + ""));
				salesInvoiceVOs.add(salesInvoiceVO);
				salesInvoiceMap.put(salesInvoice.getInvoice().getInvoiceId(),
						salesInvoiceVOs);
			} else {
				salesInvoiceVOs = new ArrayList<InvoiceVO>();
				salesInvoiceVO = new InvoiceVO();
				salesInvoiceVO
						.setInvoiceAmount(salesInvoice.getInvoiceAmount()
								+ (null != salesInvoiceVO.getInvoiceAmount() ? salesInvoiceVO
										.getInvoiceAmount() : 0));
				salesInvoiceVO.setTotalInvoice(salesInvoice.getTotalInvoice());
				salesInvoiceVO
						.setInvoiceDate(DateFormat
								.convertDateToString(salesInvoice.getDate()
										.toString()));
				salesInvoiceVO
						.setInvoiceNumber(salesInvoice.getInvoiceNumber());
				salesInvoiceVO.setInvoiceId(salesInvoice.getInvoiceId());
				salesInvoiceVO.setSupplierName(null != salesInvoice
						.getSupplier().getPerson() ? salesInvoice
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat("")
						.concat(salesInvoice.getSupplier().getPerson()
								.getLastName()) : (null != salesInvoice
						.getSupplier().getCompany() ? salesInvoice
						.getSupplier().getCompany().getCompanyName()
						: salesInvoice.getSupplier().getCmpDeptLocation()
								.getCompany().getCompanyName()));

				salesInvoiceVO
						.setPendingInvoice((salesInvoiceVO.getTotalInvoice() != null ? salesInvoiceVO
								.getTotalInvoice() : 0.0)
								- (salesInvoiceVO.getInvoiceAmount() != null ? salesInvoiceVO
										.getInvoiceAmount() : 0.0));
				salesInvoiceVO
						.setProcessStatus(salesInvoice.getProcessStatus());
				salesInvoiceVO.setSupplierNumber(salesInvoice.getSupplier()
						.getSupplierNumber());
				salesInvoiceVO.setSupplierId(salesInvoice.getSupplier()
						.getSupplierId());
				Map<String, String> salesNo = new HashMap<String, String>();
				Map<String, String> dt = new HashMap<String, String>();
				for (InvoiceDetail salesInvoiceDetail : salesInvoice
						.getInvoiceDetails()) {
					salesNo.put(salesInvoiceDetail.getReceiveDetail()
							.getReceive().getReceiveNumber(),
							salesInvoiceDetail.getReceiveDetail().getReceive()
									.getReceiveNumber());
					dt.put(DateFormat.convertDateToString(salesInvoiceDetail
							.getReceiveDetail().getReceive().getReceiveDate()
							+ ""),
							DateFormat.convertDateToString(salesInvoiceDetail
									.getReceiveDetail().getReceive()
									.getReceiveDate()
									+ ""));
				}
				salesInvoiceVO.setReceiveNumber("");
				for (Map.Entry<String, String> entry : salesNo.entrySet()) {
					salesInvoiceVO.setReceiveNumber(salesInvoiceVO
							.getReceiveNumber() + "," + entry.getValue());
				}
				salesInvoiceVO.setReceiveDate("");
				for (Map.Entry<String, String> entry : dt.entrySet()) {
					salesInvoiceVO.setReceiveDate(salesInvoiceVO
							.getReceiveDate() + "," + entry.getValue());
				}
				salesInvoiceVO.setInvoiceDate(DateFormat
						.convertDateToString(salesInvoice.getDate() + ""));

				salesInvoiceVO.setInvoiceAmount(AIOSCommons
						.roundDecimals(salesInvoiceVO.getInvoiceAmount()));
				salesInvoiceVO.setTotalInvoice(AIOSCommons
						.roundDecimals(salesInvoiceVO.getTotalInvoice()));
				salesInvoiceVO.setPendingInvoice(AIOSCommons
						.roundDecimals(salesInvoiceVO.getPendingInvoice()));

				if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Pending
						.getCode() && salesInvoiceVO.getPendingInvoice() > 0.0) {
					salesInvoiceVOs.add(salesInvoiceVO);
					salesInvoiceMap.put(salesInvoice.getInvoiceId(),
							salesInvoiceVOs);
				} else if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Paid
						.getCode() && salesInvoiceVO.getPendingInvoice() <= 0.0) {
					salesInvoiceVOs.add(salesInvoiceVO);
					salesInvoiceMap.put(salesInvoice.getInvoiceId(),
							salesInvoiceVOs);
				} else if ((byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Pending
						.getCode()
						&& (byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Paid
								.getCode()) {
					salesInvoiceVOs.add(salesInvoiceVO);
					salesInvoiceMap.put(salesInvoice.getInvoiceId(),
							salesInvoiceVOs);
				}
			}

			completeMap.put(salesInvoice.getInvoiceId(), salesInvoiceVO);

		}

		if (salesInvoiceMap != null && salesInvoiceMap.size() > 0) {
			salesInvoiceVOsFinal = new ArrayList<InvoiceVO>();
			salesInvoiceVOs = new ArrayList<InvoiceVO>();
			for (Invoice salesInvoice : salesInvoices) {
				if (salesInvoiceMap.containsKey(salesInvoice.getInvoiceId())) {
					salesInvoiceVO = new InvoiceVO();
					salesInvoiceVO = salesInvoiceMap.get(
							salesInvoice.getInvoiceId()).get(
							salesInvoiceMap.get(salesInvoice.getInvoiceId())
									.size() - 1);
					salesInvoiceVO.setInvoiceVOs(salesInvoiceMap
							.get(salesInvoice.getInvoiceId()));

					salesInvoiceVOsFinal.add(salesInvoiceVO);
				}
			}
		}

		return salesInvoiceVOsFinal;
	}

	public void invoiceDirectPaymentEntry(InvoiceVO vo) {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;
			List<Currency> currencyList = null;
			if (vo.getObject() != null) {
				currencyList = directPaymentBL.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(getImplementationId());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(), getImplementationId());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(vo.getRecordId());
				directPayment.setUseCase(vo.getUseCase());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				Invoice invoice = (Invoice) vo.getObject();
				directPayment.setSupplier(invoice.getSupplier());
				DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();

				directPaymentDetail.setAmount(invoice.getInvoiceAmount());
				directPaymentDetail
						.setInvoiceNumber(invoice.getInvoiceNumber());
				directPaymentDetail.setCombination(invoice.getSupplier()
						.getCombination());
				directPaymentDetail.setRecordId(vo.getRecordId());
				directPaymentDetail.setUseCase(vo.getUseCase());
				directPayDetails.add(directPaymentDetail);
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));
			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}
			Combination combination = null;
			if (null != directPaymentBL.getImplementationId()
					.getClearingAccount()) {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								directPaymentBL.getImplementationId()
										.getClearingAccount());
			}
			ServletActionContext.getRequest().setAttribute("CASH_ACCOUNTS",
					combination);
			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public InvoiceService getInvoiceService() {
		return invoiceService;
	}

	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public ReceiveBL getReceiveBL() {
		return receiveBL;
	}

	public void setReceiveBL(ReceiveBL receiveBL) {
		this.receiveBL = receiveBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
