package com.aiotech.aios.accounts.service.bl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.service.ChequeBookService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ChequeBookBL {

	private ChequeBookService chequeBookService;
	private Implementation implementation;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// list all bank transfers
	public JSONObject getAllChequeBooks() throws Exception {
		List<ChequeBook> chequeBookList = this.getChequeBookService()
				.getAllChequeBooks(getImplementation());
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("iTotalRecords", chequeBookList.size());
		jsonResponse.put("iTotalDisplayRecords", chequeBookList.size());
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (ChequeBook list : chequeBookList) {
			array = new JSONArray();
			array.add(list.getChequeBookId());
			array.add(list.getChequeBookNo());
			array.add(list.getBankAccount().getBank().getBankName());
			array.add(list.getBankAccount().getAccountNumber());
			array.add(list.getStartingNo());
			array.add(list.getEndingNo());
			array.add(list.getNotifyNo());
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// save or udpate cheque book
	public void saveChequeBook(ChequeBook chequeBook) throws Exception {

		chequeBook.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (chequeBook != null && chequeBook.getChequeBookId() != null
				&& chequeBook.getChequeBookId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		chequeBookService.saveChequeBook(chequeBook);
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ChequeBook.class.getSimpleName(), chequeBook.getChequeBookId(),
				user, workflowDetailVo);

	}
	
	
	public void directSaveChequeBook(ChequeBook chequeBook) throws Exception { 
		chequeBookService.saveChequeBook(chequeBook); 
	}

	// delete cheque book
	public void deleteChequeBook(ChequeBook chequeBook) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						ChequeBook.class.getSimpleName(),
						chequeBook.getChequeBookId(), user, workflowDetailVo);
		

	}

	public void doChequeBookBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		ChequeBook chequeBook = chequeBookService.findChequeBookById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			chequeBookService.deleteChequeBook(chequeBook);
		}
	}

	public ChequeBookService getChequeBookService() {
		return chequeBookService;
	}

	public void setChequeBookService(ChequeBookService chequeBookService) {
		this.chequeBookService = chequeBookService;
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
