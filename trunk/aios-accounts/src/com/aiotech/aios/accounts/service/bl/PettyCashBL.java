package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PettyCashDetail;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PettyCashVO;
import com.aiotech.aios.accounts.service.PettyCashService;
import com.aiotech.aios.common.to.Constants.Accounts.PettyCashTypes;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PettyCashBL {

	private Implementation implementation;
	private PettyCashService pettyCashService;
	private DirectPaymentBL directPaymentBL;
	private BankReceiptsBL bankReceiptsBL;
	private TransactionBL transactionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private SystemService systemService;

	// list all petty cash
	public JSONObject getAllPettyCash() throws Exception {
		List<PettyCash> pettyCashList = pettyCashService
				.getAllPettyCash(getImplementation());
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		List<TransactionDetail> transactionDetails = transactionBL
				.getTransactionService()
				.getTransactionByCombinationAndNonUsecase(
						getImplementation().getPettyCashAccount(),
						PettyCash.class.getSimpleName());
		if (null != transactionDetails && transactionDetails.size() > 0) {
			DirectPayment directPayment = null;
			BankReceipts bankReceipt = null;
			for (TransactionDetail transactionDetail : transactionDetails) {
				array = new JSONArray();
				array.add(transactionDetail.getTransactionDetailId());
				if (null != transactionDetail.getTransaction().getRecordId()
						&& null != transactionDetail.getTransaction()
								.getUseCase()
						&& !("").equals(transactionDetail.getTransaction()
								.getUseCase())) {
					if (transactionDetail.getTransaction().getUseCase()
							.equals(DirectPayment.class.getSimpleName())) {
						directPayment = directPaymentBL
								.getDirectPaymentService()
								.findDirectPaymentById(
										transactionDetail.getTransaction()
												.getRecordId());
						array.add(directPayment.getPaymentNumber());
						array.add(DateFormat.convertDateToString(directPayment
								.getPaymentDate().toString()));
						if (transactionDetail.getIsDebit()) {
							array.add("Cash In");
							array.add(AIOSCommons
									.formatAmount(transactionDetail.getAmount()));
						} else {
							array.add("Cash Out");
							array.add(AIOSCommons
									.formatAmount(transactionDetail.getAmount()));
						}
						array.add("");
						array.add(DirectPayment.class.getSimpleName());
					} else if (transactionDetail.getTransaction().getUseCase()
							.equals(BankReceipts.class.getSimpleName())) {
						bankReceipt = bankReceiptsBL.getBankReceiptsService()
								.findByBankReceiptsId(
										transactionDetail.getTransaction()
												.getRecordId());

						array.add(bankReceipt.getReceiptsNo());
						array.add(DateFormat.convertDateToString(bankReceipt
								.getReceiptsDate().toString()));
						if (transactionDetail.getIsDebit()) {
							array.add("Cash In");
							array.add(AIOSCommons
									.formatAmount(transactionDetail.getAmount()));
						} else {
							array.add("Cash Out");
							array.add(AIOSCommons
									.formatAmount(transactionDetail.getAmount()));
						}
						array.add("");
						array.add("Receipts");
					}
				} else {
					array.add(transactionDetail.getTransaction()
							.getJournalNumber());
					array.add(DateFormat.convertDateToString(transactionDetail
							.getTransaction().getTransactionTime().toString()));
				}
				array.add(null != transactionDetail.getCombination()
						.getAccountByAnalysisAccountId() ? transactionDetail
						.getCombination().getAccountByAnalysisAccountId()
						.getAccount() : transactionDetail.getCombination()
						.getAccountByNaturalAccountId().getAccount());
				array.add(transactionDetail.getTransaction().getDescription());
				array.add(null != transactionDetail.getTransaction()
						.getPerson() ? transactionDetail
						.getTransaction()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(transactionDetail.getTransaction().getPerson()
								.getLastName()) : "");
				array.add("false");
				array.add("false");
				data.add(array);
			}
		}
		for (PettyCash list : pettyCashList) {
			String combinationStr = "";
			array = new JSONArray();
			array.add(list.getPettyCashId());
			array.add(list.getPettyCashNo());
			array.add(DateFormat.convertDateToString(list.getVoucherDate()
					.toString()));
			array.add(PettyCashTypes.get(list.getPettyCashType()).name()
					.replaceAll("_", " "));
			if (list.getCashIn() != null)
				array.add(AIOSCommons.formatAmount(list.getCashIn()));
			else if (list.getCashOut() != null)
				array.add(AIOSCommons.formatAmount(list.getCashOut()));
			else
				array.add(AIOSCommons.formatAmount(list.getExpenseAmount()));
			if ((byte) PettyCashTypes.Spend.getCode() != (byte) list
					.getPettyCashType())
				array.add(null != list.getPersonByPersonId() ? list
						.getPersonByPersonId().getFirstName().concat(" ")
						.concat(list.getPersonByPersonId().getLastName())
						: list.getReceiverOther());
			else {
				String payeeName = "";

				for (PettyCashDetail detail : list.getPettyCashDetails()) {
					payeeName += (null != detail.getPerson() ? detail
							.getPerson().getFirstName().concat(" ")
							.concat(detail.getPerson().getLastName()) : detail
							.getPayeeName()) + ", ";
					combinationStr += (null != detail.getCombination()
							.getAccountByAnalysisAccountId() ? detail
							.getCombination().getAccountByAnalysisAccountId()
							.getAccount() : detail.getCombination()
							.getAccountByNaturalAccountId().getAccount())
							+ ", ";
				}
				array.add(payeeName);
			}
			if (null != list.getDirectPaymentDetail()) {
				array.add("Direct Payment");
			} else {
				array.add("Receipts");
			}
			if ((byte) PettyCashTypes.Spend.getCode() != (byte) list
					.getPettyCashType()) {
				if (null != list.getCombination())
					array.add(null != list.getCombination()
							.getAccountByAnalysisAccountId() ? list
							.getCombination().getAccountByAnalysisAccountId()
							.getAccount() : list.getCombination()
							.getAccountByNaturalAccountId().getAccount());
				else
					array.add("");
			} else
				array.add(combinationStr);
			array.add(list.getDescription());
			array.add(list.getPersonByCreatedBy().getFirstName().concat(" ")
					.concat(list.getPersonByCreatedBy().getLastName()));
			if (null != list.getPettyCashs() && list.getPettyCashs().size() > 0)
				array.add("false");
			else
				array.add("true");
			array.add("true");
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public JSONObject getConvertPettyCashList(List<PettyCashVO> pettyCashVOs)
			throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		if (pettyCashVOs != null)
			for (PettyCashVO list : pettyCashVOs) {
				array = new JSONArray();
				array.add(list.getPettyCashId());
				array.add(list.getPersonByCreatedBy().getPersonId());
				array.add(list.getExpenseAmount());
				array.add(list.getPettyCashNo());
				array.add(null != list.getPersonByCreatedBy() ? list
						.getPersonByCreatedBy().getFirstName().concat(" ")
						.concat(list.getPersonByCreatedBy().getLastName())
						: list.getReceiverOther());
				array.add(list.getInvoiceNumber());
				array.add(null != list.getCashOut() ? list.getCashOut() : list
						.getExpenseAmount());
				data.add(array);
			}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public JSONObject convertCashInPettyCashVO(List<PettyCashVO> pettyCashVOs)
			throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		if (pettyCashVOs != null) {
			for (PettyCashVO list : pettyCashVOs) {
				array = new JSONArray();
				array.add(list.getPettyCashId());
				array.add(list.getPettyCashNo());
				array.add(list.getInvoiceNumber());
				array.add(AIOSCommons.formatAmount(list.getCashIn()));
				array.add(null != list.getPersonByCreatedBy() ? list
						.getPersonByCreatedBy().getFirstName()
						+ " "
						+ list.getPersonByCreatedBy().getLastName() : "");
				data.add(array);
			}
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public JSONObject convertAdvanceIssuedPettyCashVO(
			List<PettyCashVO> pettyCashVOs) throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		if (pettyCashVOs != null)
			for (PettyCashVO list : pettyCashVOs) {
				array = new JSONArray();
				array.add(list.getPettyCashId());
				array.add(list.getPettyCashNo());
				array.add(DateFormat.convertDateToString(list.getVoucherDate()
						.toString()));
				array.add(AIOSCommons.roundDecimals(list.getCashOut()));
				array.add(null != list.getPersonByPersonId() ? list
						.getPersonByPersonId().getFirstName()
						+ " "
						+ list.getPersonByPersonId().getLastName() : "");
				array.add(null != list.getPersonByCreatedBy() ? list
						.getPersonByCreatedBy().getFirstName()
						+ " "
						+ list.getPersonByCreatedBy().getLastName() : "");
				data.add(array);
			}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public JSONObject convertExpenseVoucherPettyCashVO(
			List<PettyCashVO> pettyCashVOs) throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		if (pettyCashVOs != null)
			for (PettyCashVO list : pettyCashVOs) {
				array = new JSONArray();
				array.add(list.getPettyCashId());
				array.add(list.getPettyCashNo());
				array.add(DateFormat.convertDateToString(list.getVoucherDate()
						.toString()));
				array.add(list.getExpenseAmount());
				data.add(array);
			}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// list all parent petty cash
	public JSONObject getAllParentPettyCash() throws Exception {
		List<PettyCash> pettyCashList = pettyCashService
				.getAllParentPettyCash(getImplementation());
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (PettyCash list : pettyCashList) {
			array = new JSONArray();
			array.add(list.getPettyCashId());
			array.add(list.getPettyCashNo());
			array.add(DateFormat.convertDateToString(list.getVoucherDate()
					.toString()));
			array.add(PettyCashTypes.get(list.getPettyCashType()).name()
					.replaceAll("_", " "));
			array.add(null != list.getPersonByPersonId() ? list
					.getPersonByPersonId().getFirstName().concat(" ")
					.concat(list.getPersonByPersonId().getLastName()) : list
					.getReceiverOther());
			if (null != list.getDirectPaymentDetail()) {
				array.add("Direct Payment");
			} else {
				array.add("Receipt Voucher");
			}
			if (null != list.getCombination())
				array.add(null != list.getCombination()
						.getAccountByAnalysisAccountId() ? list
						.getCombination().getAccountByAnalysisAccountId()
						.getAccount() : list.getCombination()
						.getAccountByNaturalAccountId().getAccount());
			else
				array.add("");
			array.add(list.getDescription());
			array.add(list.getPersonByCreatedBy().getFirstName().concat(" ")
					.concat(list.getPersonByCreatedBy().getLastName()));
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// create petty cash receipts
	public JSONObject createReceipts(
			List<DirectPaymentDetail> directPaymentDetails,
			List<BankReceiptsDetail> bankReceiptDetails) throws Exception {
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		if (null != directPaymentDetails && directPaymentDetails.size() > 0) {
			for (DirectPaymentDetail list : directPaymentDetails) {
				if (list.getAmount() > 0) {
					array = new JSONArray();
					array.add(list.getDirectPaymentDetailId());
					array.add(DirectPayment.class.getSimpleName());
					array.add(list.getDirectPayment().getPaymentNumber());
					array.add(list.getInvoiceNumber());
					array.add(AIOSCommons.formatAmount(list.getAmount()));
					array.add(list.getDescription());
					array.add(null != list.getDirectPayment()
							.getPersonByCreatedBy() ? list.getDirectPayment()
							.getPersonByCreatedBy().getFirstName()
							+ " "
							+ list.getDirectPayment().getPersonByCreatedBy()
									.getLastName() : "");
					data.add(array);
				}
			}
		}
		if (null != bankReceiptDetails && bankReceiptDetails.size() > 0) {
			for (BankReceiptsDetail list : bankReceiptDetails) {
				if (list.getAmount() > 0) {
					array = new JSONArray();
					array.add(list.getBankReceiptsDetailId());
					array.add("Receipts");
					array.add(list.getBankReceipts().getReceiptsNo());
					array.add(null != list.getLookupDetail() ? list
							.getLookupDetail().getDisplayName() : "");
					array.add(AIOSCommons.formatAmount(list.getAmount()));
					array.add(list.getDescription());
					array.add(null != list.getBankReceipts()
							.getPersonByCreatedBy() ? list.getBankReceipts()
							.getPersonByCreatedBy().getFirstName()
							+ " "
							+ list.getBankReceipts().getPersonByCreatedBy()
									.getLastName() : "");
					data.add(array);
				}
			}
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public List<PettyCashVO> recursivePettyCashLog(PettyCash pettyCash)
			throws Exception {
		return getAllPettyCashDetails(pettyCash);
	}

	public PettyCash getParentPettyCashDetails(PettyCash pettyCash)
			throws Exception {
		return recursiveFindParentPettyCash(pettyCash);
	}

	private PettyCash recursiveFindParentPettyCash(PettyCash pettyCash)
			throws Exception {
		pettyCash = pettyCashService.getPettyCashParent(pettyCash
				.getPettyCashId());
		return null != pettyCash.getPettyCash() ? recursiveFindParentPettyCash(pettyCash
				.getPettyCash()) : pettyCash;
	}

	private List<PettyCashVO> getAllPettyCashDetails(PettyCash pettyCash)
			throws Exception {
		List<PettyCashVO> pettyCashVOs = new ArrayList<PettyCashVO>();
		PettyCashVO pettyCashVO = new PettyCashVO();
		BeanUtils.copyProperties(pettyCashVO, pettyCash);
		pettyCashVO = recursivePettyCash(pettyCash, pettyCashVO);
		pettyCashVOs.add(pettyCashVO);
		if (null != pettyCashVO.getPettyCashes()
				&& pettyCashVO.getPettyCashes().size() > 0)
			pettyCashVOs
					.addAll(addRecursivePettyCash(pettyCashVO.getPettyCashes(),
							new ArrayList<PettyCashVO>()));
		return pettyCashVOs;
	}

	private List<PettyCashVO> getAllPettyCashDetails(List<PettyCash> pettyCashes)
			throws Exception {
		PettyCashVO pettyCashVO = null;
		List<PettyCashVO> pettyCashVOs = new ArrayList<PettyCashVO>();
		for (PettyCash pettyCash : pettyCashes) {
			pettyCashVO = new PettyCashVO();
			BeanUtils.copyProperties(pettyCashVO, pettyCash);
			pettyCashVO = recursivePettyCash(pettyCash, pettyCashVO);
			if (null != pettyCashVO.getPettyCashes()
					&& pettyCashVO.getPettyCashes().size() > 0)
				pettyCashVO.setPettyCashes(addRecursivePettyCash(
						pettyCashVO.getPettyCashes(),
						new ArrayList<PettyCashVO>()));
			pettyCashVOs.add(pettyCashVO);
		}
		return pettyCashVOs;
	}

	private List<PettyCashVO> addRecursivePettyCash(
			List<PettyCashVO> pettyCashes, List<PettyCashVO> updatedPettyCashes)
			throws Exception {
		for (PettyCashVO pettyCashVO : pettyCashes) {
			updatedPettyCashes.add(pettyCashVO);
			if (null != pettyCashVO.getPettyCashes()
					&& pettyCashVO.getPettyCashes().size() > 0) {
				addRecursivePettyCash(pettyCashVO.getPettyCashes(),
						updatedPettyCashes);
			}
		}
		return updatedPettyCashes;
	}

	private PettyCashVO recursivePettyCash(PettyCash pettyCash,
			PettyCashVO pettyCashVO) throws Exception {
		List<PettyCashVO> pettyCashVOs = new ArrayList<PettyCashVO>();
		List<PettyCash> pettyCashLs = pettyCashService
				.getPettyCashChildById(pettyCash.getPettyCashId());
		PettyCashVO pettyCashVOTemp = null;
		if (null != pettyCashLs && pettyCashLs.size() > 0) {
			for (PettyCash cash : pettyCashLs) {
				pettyCashVOTemp = new PettyCashVO();
				BeanUtils.copyProperties(pettyCashVOTemp, cash);
				pettyCashVOs.add(pettyCashVOTemp);
				recursivePettyCash(cash, pettyCashVOTemp);
			}
			Collections.sort(pettyCashVOs, new Comparator<PettyCashVO>() {
				public int compare(PettyCashVO o1, PettyCashVO o2) {
					return o1.getPettyCashId().compareTo(o2.getPettyCashId());
				}
			});
			pettyCashVO.setPettyCashes(pettyCashVOs);
		}
		return pettyCashVO;
	}

	public List<PettyCashVO> validateAIPCPettyCashVoucher(
			List<PettyCash> pettyCashs) throws Exception {
		PettyCashVO fnPettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		List<PettyCashVO> pettyCashVOs = getAllPettyCashDetails(pettyCashs);
		for (PettyCashVO pettyCashVO : pettyCashVOs) {
			double cashOut = 0;
			double cashIn = (null != pettyCashVO.getCashIn() ? pettyCashVO
					.getCashIn() : 0);
			for (PettyCash pettyCash : pettyCashVO.getPettyCashes()) {
				if (((byte) PettyCashTypes.Cash_transfer.getCode() == (byte) pettyCash
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Carry_forward.getCode() == (byte) pettyCash
								.getPettyCashType())
						|| ((byte) PettyCashTypes.Cash_received_settlement
								.getCode() == (byte) pettyCash
								.getPettyCashType()))
					cashIn += pettyCash.getCashIn();
				else if (((byte) PettyCashTypes.Advance_issued.getCode() == (byte) pettyCash
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Cash_given_settlement
								.getCode() == (byte) pettyCash
								.getPettyCashType()))
					cashOut += pettyCash.getCashOut();
			}
			if ((cashIn - cashOut) > 0) {
				fnPettyCashVO = new PettyCashVO();
				fnPettyCashVO.setPettyCashId(pettyCashVO.getPettyCashId());
				fnPettyCashVO.setPettyCashNo(pettyCashVO.getPettyCashNo());
				fnPettyCashVO.setPersonByPersonId(pettyCashVO
						.getPersonByPersonId());
				fnPettyCashVO.setPersonByCreatedBy(pettyCashVO
						.getPersonByCreatedBy());
				if (null != pettyCashVO.getDirectPaymentDetail())
					fnPettyCashVO.setInvoiceNumber(pettyCashVO
							.getDirectPaymentDetail().getDirectPayment()
							.getPaymentNumber());
				else
					fnPettyCashVO.setInvoiceNumber(pettyCashVO
							.getBankReceiptsDetail().getBankReceipts()
							.getReceiptsNo());
				fnPettyCashVO.setCashIn(cashIn - cashOut);
				pettyCashVos.add(fnPettyCashVO);
			}
		}
		return pettyCashVos;
	}

	public List<PettyCashVO> validateAdvanceIssuedPettyCashVoucher(
			List<PettyCash> pettyCashs) throws Exception {
		PettyCashVO fnPettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		List<PettyCashVO> pettyCashVOs = getAllPettyCashDetails(pettyCashs);
		for (PettyCashVO pettyCashVO : pettyCashVOs) {
			double cashOut = (null != pettyCashVO.getCashOut() ? pettyCashVO
					.getCashOut() : 0);
			double expenseAmount = 0;
			for (PettyCash pettyCash : pettyCashVO.getPettyCashes()) {
				if (((byte) PettyCashTypes.Spend.getCode() == (byte) pettyCash
						.getPettyCashType())
						|| ((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Reimbursed
								.getCode()))
					expenseAmount += pettyCash.getExpenseAmount();
				if ((byte) PettyCashTypes.Cash_received_settlement.getCode() == (byte) pettyCash
						.getPettyCashType())
					cashOut -= pettyCash.getCashIn();
				if ((byte) PettyCashTypes.Cash_given_settlement.getCode() == (byte) pettyCash
						.getPettyCashType())
					cashOut += pettyCash.getCashOut();
			}
			if ((cashOut - expenseAmount) > 0) {
				fnPettyCashVO = new PettyCashVO();
				fnPettyCashVO.setPettyCashId(pettyCashVO.getPettyCashId());
				fnPettyCashVO.setPettyCashNo(pettyCashVO.getPettyCashNo());
				fnPettyCashVO.setPersonByPersonId(pettyCashVO
						.getPersonByPersonId());
				fnPettyCashVO.setPersonByCreatedBy(pettyCashVO
						.getPersonByCreatedBy());
				fnPettyCashVO.setVoucherDate(pettyCashVO.getVoucherDate());
				fnPettyCashVO.setCashOut(cashOut - expenseAmount);
				pettyCashVos.add(fnPettyCashVO);
			}
		}
		return pettyCashVos;
	}

	public List<PettyCashVO> validateSpendPettyCashVoucher(
			List<PettyCash> pettyCashs) throws Exception {
		PettyCashVO fnPettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		List<PettyCashVO> pettyCashVOs = getAllPettyCashDetails(pettyCashs);
		for (PettyCashVO pettyCashVO : pettyCashVOs) {
			double expenseAmount = (null != pettyCashVO.getExpenseAmount() ? pettyCashVO
					.getExpenseAmount() : 0);
			for (PettyCash pettyCash : pettyCashVO.getPettyCashes()) {
				if ((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Cash_received_settlement
						.getCode()) {
					expenseAmount -= pettyCash.getCashIn();
				} else if ((byte) PettyCashTypes.Cash_given_settlement
						.getCode() == (byte) pettyCash.getPettyCashType())
					expenseAmount += pettyCash.getCashOut();
			}
			if (expenseAmount > 0) {
				fnPettyCashVO = new PettyCashVO();
				fnPettyCashVO.setPettyCashId(pettyCashVO.getPettyCashId());
				fnPettyCashVO.setPettyCashNo(pettyCashVO.getPettyCashNo());
				fnPettyCashVO.setPersonByPersonId(pettyCashVO
						.getPersonByPersonId());
				fnPettyCashVO.setPersonByCreatedBy(pettyCashVO
						.getPersonByCreatedBy());
				fnPettyCashVO.setVoucherDate(pettyCashVO.getVoucherDate());
				fnPettyCashVO.setExpenseAmount(expenseAmount);
				pettyCashVos.add(fnPettyCashVO);
			}
		}
		return pettyCashVos;
	}

	public List<PettyCashVO> validateCashGivenPettyCashVoucher(
			List<PettyCash> pettyCashs, Byte pettyCashType) throws Exception {
		PettyCashVO fnPettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		List<PettyCashVO> pettyCashVOs = getAllPettyCashDetails(pettyCashs);
		for (PettyCashVO pettyCashVO : pettyCashVOs) {
			double expenseAmount = (null != pettyCashVO.getExpenseAmount() ? pettyCashVO
					.getExpenseAmount() : 0);
			for (PettyCash pettyCash : pettyCashVO.getPettyCashes()) {
				if ((byte) pettyCashType == (byte) PettyCashTypes.Cash_received_settlement
						.getCode()) {
					expenseAmount -= pettyCash.getCashIn();
				} else if (((byte) PettyCashTypes.Cash_given_settlement
						.getCode() == (byte) pettyCash.getPettyCashType()))
					expenseAmount += pettyCash.getCashOut();
			}
			if (expenseAmount > 0) {
				fnPettyCashVO = new PettyCashVO();
				fnPettyCashVO.setPettyCashId(pettyCashVO.getPettyCashId());
				fnPettyCashVO.setPettyCashNo(pettyCashVO.getPettyCashNo());
				fnPettyCashVO.setPersonByPersonId(pettyCashVO
						.getPersonByPersonId());
				fnPettyCashVO.setPersonByCreatedBy(pettyCashVO
						.getPersonByCreatedBy());
				fnPettyCashVO.setVoucherDate(pettyCashVO.getVoucherDate());
				fnPettyCashVO.setExpenseAmount(expenseAmount);
				pettyCashVos.add(fnPettyCashVO);
			}
		}
		return pettyCashVos;
	}

	// create & validate petty cash voucher SFAI & CRFS
	public List<PettyCashVO> validateSFAIPettyCashVoucher(
			List<PettyCash> pettyCashList,
			Map<Long, List<PettyCash>> hashSetlements, Byte pettyCashType)
			throws Exception {
		PettyCashVO pettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		for (PettyCash list : pettyCashList) {
			double cashOut = 0;
			double cashIn = 0;
			double expenseAmount = 0;
			if (hashSetlements.containsKey(list.getPettyCashId())) {
				List<PettyCash> settlements = hashSetlements.get(list
						.getPettyCashId());
				for (PettyCash hash : settlements) {
					cashOut += (null != hash.getCashOut() ? hash.getCashOut()
							: 0);
					cashIn += (null != hash.getCashIn() ? hash.getCashIn() : 0);
					expenseAmount += (null != hash.getExpenseAmount() ? hash
							.getExpenseAmount() : 0);
				}
			} else {
				if ((byte) pettyCashType == (byte) PettyCashTypes.Cash_given_settlement
						.getCode()
						&& null != list.getPettyCash()
						&& null != list.getPettyCash().getPettyCashs()) {
					for (PettyCash cash : list.getPettyCash().getPettyCashs()) {
						if ((byte) cash.getPettyCashType() == (byte) PettyCashTypes.Cash_given_settlement
								.getCode()) {
							expenseAmount += cash.getCashOut();
						}
					}
				}
			}
			cashOut += list.getCashOut();
			if (cashOut - (expenseAmount + cashIn) > 0) {
				pettyCashVO = new PettyCashVO();
				pettyCashVO.setPettyCashId(list.getPettyCashId());
				pettyCashVO.setPettyCashNo(list.getPettyCashNo());
				pettyCashVO.setPersonByCreatedBy(list.getPersonByCreatedBy());
				pettyCashVO.setPersonByPersonId(list.getPersonByPersonId());
				if (null != list.getDirectPaymentDetail()
						&& !("").equals(list.getDirectPaymentDetail()))
					pettyCashVO.setInvoiceNumber(list.getDirectPaymentDetail()
							.getDirectPayment().getPaymentNumber());
				pettyCashVO
						.setExpenseAmount(cashOut - (expenseAmount + cashIn));
				pettyCashVos.add(pettyCashVO);
			}
		}
		return pettyCashVos;
	}

	// create & validate petty cash voucher CGFS
	public List<PettyCashVO> validateCGFSPettyCashVoucher(
			List<PettyCash> pettyCashList,
			Map<Long, List<PettyCash>> hashSetlements) throws Exception {
		PettyCashVO pettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		for (PettyCash list : pettyCashList) {
			double cashOut = 0;
			double cashIn = 0;
			double expenseAmount = 0;
			if (hashSetlements.containsKey(list.getPettyCashId())) {
				List<PettyCash> settlements = hashSetlements.get(list
						.getPettyCashId());
				for (PettyCash hash : settlements) {
					cashOut += (null != hash.getCashOut() ? hash.getCashOut()
							: 0);
					cashIn += (null != hash.getCashIn() ? hash.getCashIn() : 0);
					expenseAmount += (null != hash.getExpenseAmount() ? hash
							.getExpenseAmount() : 0);
				}
			}
			cashOut += list.getCashOut();
			if (cashOut - (expenseAmount + cashIn) < 0) {
				pettyCashVO = new PettyCashVO();
				pettyCashVO.setPettyCashId(list.getPettyCashId());
				pettyCashVO.setPettyCashNo(list.getPettyCashNo());
				pettyCashVO.setPersonByPersonId(list.getPersonByPersonId());
				pettyCashVO.setPersonByCreatedBy(list.getPersonByCreatedBy());
				if (null != list.getDirectPaymentDetail()
						&& !("").equals(list.getDirectPaymentDetail()))
					pettyCashVO.setInvoiceNumber(list.getDirectPaymentDetail()
							.getDirectPayment().getPaymentNumber());
				pettyCashVO.setCashOut(Math.abs(cashOut
						- (expenseAmount + cashIn)));
				pettyCashVos.add(pettyCashVO);
			}
		}
		return pettyCashVos;
	}

	public List<PettyCashVO> validateCFSPettyCashVoucher(
			List<PettyCash> pettyCashList,
			Map<Long, List<PettyCash>> hashSetlements) throws Exception {
		PettyCashVO pettyCashVO = null;
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		for (PettyCash list : pettyCashList) {
			double cashIn = list.getCashIn();
			double expenseAmount = 0;
			if (hashSetlements.containsKey(list.getPettyCashId())) {
				List<PettyCash> settlements = hashSetlements.get(list
						.getPettyCashId());
				for (PettyCash hash : settlements) {
					if ((byte) hash.getPettyCashType() == (byte) PettyCashTypes.Cash_given_settlement
							.getCode()) {
						expenseAmount += hash.getCashOut();
					} else if (null != hash.getPettyCashs()
							&& hash.getPettyCashs().size() > 0) {
						for (PettyCash expense : hash.getPettyCashs()) {
							expenseAmount += (null != expense
									.getExpenseAmount() ? expense
									.getExpenseAmount() : 0);
						}
					}
				}
			}
			if (cashIn > expenseAmount) {
				pettyCashVO = new PettyCashVO();
				pettyCashVO.setPettyCashId(list.getPettyCashId());
				pettyCashVO.setPettyCashNo(list.getPettyCashNo());
				pettyCashVO.setPersonByPersonId(list.getPersonByPersonId());
				pettyCashVO.setPersonByCreatedBy(list.getPersonByCreatedBy());
				if (null != list.getDirectPaymentDetail()
						&& !("").equals(list.getDirectPaymentDetail()))
					pettyCashVO.setInvoiceNumber(list.getDirectPaymentDetail()
							.getDirectPayment().getPaymentNumber());
				pettyCashVO.setCashOut(cashIn - expenseAmount);
				pettyCashVos.add(pettyCashVO);
			}
		}
		return pettyCashVos;
	}

	// create petty cash log
	public List<PettyCashVO> createPettyCashLog(List<PettyCashVO> pettyCashVOs)
			throws Exception {
		List<PettyCashVO> pettyCashVos = new ArrayList<PettyCashVO>();
		if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
			Collections.sort(pettyCashVOs, new Comparator<PettyCashVO>() {
				public int compare(PettyCashVO o1, PettyCashVO o2) {
					return o1.getPettyCashId().compareTo(o2.getPettyCashId());
				}
			});

			PettyCashVO pettyCashVO = null;
			Collections.sort(pettyCashVOs, new Comparator<PettyCashVO>() {
				public int compare(PettyCashVO o1, PettyCashVO o2) {
					return o1.getPettyCashId().compareTo(o2.getPettyCashId());
				}
			});
			double remainingBalance = 0;
			for (PettyCashVO pettyCash : pettyCashVOs) {
				double cashOut = 0;
				double cashIn = 0;
				pettyCashVO = new PettyCashVO();
				pettyCashVO.setPettyCashId(pettyCash.getPettyCashId());
				pettyCashVO.setPettyCashNo(pettyCash.getPettyCashNo());
				pettyCashVO.setPettyCashDate(DateFormat
						.convertDateToString(pettyCash.getVoucherDate()
								.toString()));
				pettyCashVO.setPettyCashType(pettyCash.getPettyCashType());
				pettyCashVO.setPettyCashTypeName(PettyCashTypes
						.get(pettyCash.getPettyCashType()).name()
						.replaceAll("_", " "));
				pettyCashVO
						.setPersonByPersonId(pettyCash.getPersonByPersonId());
				pettyCashVO.setCombination(pettyCash.getCombination());
				pettyCashVO.setDescription(pettyCash.getDescription());

				if (((byte) PettyCashTypes.Cash_received_settlement.getCode() == (byte) pettyCash
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Cash_transfer.getCode() == (byte) pettyCash
								.getPettyCashType())
						|| ((byte) PettyCashTypes.Carry_forward.getCode() == (byte) pettyCash
								.getPettyCashType())) {
					cashIn += (null != pettyCash.getCashIn() ? pettyCash
							.getCashIn() : 0);
					remainingBalance += cashIn;
				}

				else if (((byte) PettyCashTypes.Cash_given_settlement.getCode() == (byte) pettyCash
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Advance_issued.getCode() == (byte) pettyCash
								.getPettyCashType())) {
					cashOut += (null != pettyCash.getCashOut() ? pettyCash
							.getCashOut() : 0);
					remainingBalance -= cashOut;
				}

				pettyCashVO.setCashIn(pettyCash.getCashIn());
				pettyCashVO.setCashOut(pettyCash.getCashOut());
				pettyCashVO.setExpenseAmount(pettyCash.getExpenseAmount());
				pettyCashVO.setRemainingBalance(remainingBalance);
				pettyCashVO.setInvoiceNumber(pettyCash.getInvoiceNumber());
				pettyCashVO.setPettyCash(pettyCash.getPettyCash());
				pettyCashVO.setSettelmentFlag(pettyCash.getSettelmentFlag());
				pettyCashVO.setPersonByCreatedBy(pettyCash
						.getPersonByCreatedBy());
				pettyCashVO.setClosedFlag(pettyCash.getClosedFlag());
				pettyCashVO.setEmployeeName(null != pettyCash
						.getPersonByPersonId() ? pettyCash
						.getPersonByPersonId().getFirstName()
						+ " "
						+ pettyCash.getPersonByPersonId().getLastName()
						: pettyCashVO.getReceiverOther());
				pettyCashVO.setCreatedBy(pettyCash.getPersonByCreatedBy()
						.getFirstName()
						+ " "
						+ pettyCash.getPersonByCreatedBy().getLastName());
				if (pettyCash.getPettyCash() != null) {
					pettyCashVO.setSettledWith(pettyCash.getPettyCash()
							.getPettyCashNo());
				}

				if (pettyCash.getCombination() != null
						&& pettyCash.getCombination()
								.getAccountByAnalysisAccountId() != null) {
					pettyCashVO.setExpenseAccount(pettyCash.getCombination()
							.getAccountByNaturalAccountId().getAccount()
							+ "-"
							+ pettyCash.getCombination()
									.getAccountByAnalysisAccountId()
									.getAccount());
				} else if (pettyCash.getCombination() != null
						&& pettyCash.getCombination()
								.getAccountByNaturalAccountId() != null) {
					pettyCashVO.setExpenseAccount(pettyCash.getCombination()
							.getAccountByNaturalAccountId().getAccount());
				}

				if (pettyCash.getCashIn() != null
						&& (!pettyCash.getCashIn().equals(""))
						&& pettyCash.getCashIn() != 0) {
					pettyCashVO.setCashInFormat(AIOSCommons
							.formatAmount(pettyCash.getCashIn()));
				} else {
					pettyCashVO.setCashInFormat("");
				}

				if (pettyCash.getCashOut() != null
						&& (!pettyCash.getCashOut().equals(""))
						&& pettyCash.getCashOut() != 0) {
					pettyCashVO.setCashOutFormat(AIOSCommons
							.formatAmount(pettyCash.getCashOut()));
				}

				if (pettyCashVO.getRemainingBalance() != null
						&& (!pettyCashVO.getRemainingBalance().equals(""))
						&& pettyCashVO.getRemainingBalance() != 0) {
					pettyCashVO.setRemainingBalanceFormat(AIOSCommons
							.formatAmount(pettyCashVO.getRemainingBalance()));
				}

				if (pettyCashVO.getExpenseAmount() != null
						&& (!pettyCashVO.getExpenseAmount().equals(""))
						&& pettyCashVO.getExpenseAmount() != 0) {
					pettyCashVO.setExpenseAmountFormat(AIOSCommons
							.formatAmount(pettyCashVO.getExpenseAmount()));
				}

				if ((byte) PettyCashTypes.Spend.getCode() == (byte) pettyCash
						.getPettyCashType()) {
					List<PettyCashDetail> pettyCashDetails = pettyCashService
							.getPettyCashDetailByPettyId(pettyCash
									.getPettyCashId());
					if (null != pettyCashDetails && pettyCashDetails.size() > 0) {
						Set<String> personstr = new HashSet<String>();
						Set<String> cbstr = new HashSet<String>();
						for (PettyCashDetail pettyCashDetail : pettyCashDetails) {
							personstr
									.add(null != pettyCashDetail.getPerson() ? pettyCashDetail
											.getPerson().getFirstName()
											+ " "
											+ pettyCashDetail.getPerson()
													.getLastName()
											: pettyCashDetail.getPayeeName());
							cbstr.add(null != pettyCashDetail.getCombination() ? pettyCashDetail
									.getCombination()
									.getAccountByNaturalAccountId()
									.getAccount()
									+ "-"
									+ (null != pettyCashDetail.getCombination()
											.getAccountByAnalysisAccountId() ? pettyCashDetail
											.getCombination()
											.getAccountByAnalysisAccountId()
											.getAccount()
											: "")
									: "");
							pettyCashVO.setInvoiceNumber(pettyCashDetail
									.getBillNo());
						}
						String[] array = personstr.toArray(new String[personstr
								.size()]);
						String person = Arrays.toString(array);
						pettyCashVO.setEmployeeName(person.substring(1,
								person.length() - 1));
						String[] array1 = cbstr
								.toArray(new String[cbstr.size()]);
						String cbi = Arrays.toString(array1);
						pettyCashVO.setExpenseAccount(cbi.substring(1,
								cbi.length() - 1));
					}
				}
				pettyCashVos.add(pettyCashVO);
			}
		}
		return pettyCashVos;
	}

	// create petty cash log
	public Double showMaximumPettyCash(List<PettyCash> pettyCashList)
			throws Exception {
		double cashIn = 0;
		double cashOut = 0;
		for (PettyCash pettyCash : pettyCashList) {
			cashIn += (null != pettyCash.getCashIn() ? pettyCash.getCashIn()
					: 0);
			cashOut += (null != pettyCash.getCashOut() ? pettyCash.getCashOut()
					: 0);
		}
		return (cashIn - cashOut);
	}

	// create petty cash log
	public Double showTotalCashOutPettyCash(List<PettyCash> pettyCashList,
			long recordId, String useCase) throws Exception {
		double cashOut = 0;
		if (null != pettyCashList && pettyCashList.size() > 0) {
			for (PettyCash pettyCash : pettyCashList) {
				if ((null == pettyCash.getRecordId() || null == pettyCash
						.getUseCase())
						|| (pettyCash.getRecordId() != recordId || !pettyCash
								.getUseCase().equals(useCase))) {
					cashOut += (null != pettyCash.getCashOut() ? pettyCash
							.getCashOut() : 0);
				}
			}
		}
		return cashOut;
	}

	// save petty cash
	public void savePettyCash(PettyCash pettyCash, PettyCash cashCarryForward,
			List<PettyCashDetail> pettyCashDetails,
			List<PettyCashDetail> pettyCashDetailDel, PettyCash takenVoucher,
			PettyCash parentVocher) throws Exception {
		boolean updateFlag = false;
		pettyCash.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (pettyCash != null && pettyCash.getPettyCashId() != null
				&& pettyCash.getPettyCashId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (null != pettyCash.getPettyCashId())
			updateFlag = true;
		if (!updateFlag)
			SystemBL.saveReferenceStamp(PettyCash.class.getName(),
					getImplementation());
		pettyCash.setImplementation(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		if (!updateFlag)
			pettyCash.setPersonByCreatedBy(person);
		else {
			transactionBL.deleteTransaction(pettyCash.getPettyCashId(),
					PettyCash.class.getSimpleName());
			if (null != pettyCashDetailDel && pettyCashDetailDel.size() > 0)
				pettyCashService.deletePettyCashDetail(pettyCashDetailDel);
		}
		if (null == pettyCash.getPersonByPersonId()
				&& (byte) pettyCash.getPettyCashType() != (byte) PettyCashTypes.Spend
						.getCode())
			pettyCash.setPersonByPersonId(person);

		List<PettyCash> pettyCashs = new ArrayList<PettyCash>();
		if (null != cashCarryForward) {
			cashCarryForward.setPersonByCreatedBy(person);
			cashCarryForward.setPersonByPersonId(person);
			pettyCashs.add(cashCarryForward);
			pettyCash.setPettyCashNo(SystemBL.getReferenceStamp(
					PettyCash.class.getName(), getImplementation()));
			SystemBL.saveReferenceStamp(PettyCash.class.getName(),
					getImplementation());
		}
		if (null != pettyCashDetails && pettyCashDetails.size() > 0) {
			for (PettyCashDetail pettyCashDetail : pettyCashDetails)
				pettyCashDetail.setPettyCash(pettyCash);
			pettyCash.setPettyCashDetails(new HashSet<PettyCashDetail>(
					pettyCashDetails));
		}
		pettyCashs.add(pettyCash);
		pettyCashService.savePettyCash(pettyCashs, pettyCashDetails,
				takenVoucher, parentVocher);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PettyCash.class.getSimpleName(), pettyCash.getPettyCashId(),
				user, workflowDetailVo);
	}

	public void savePettyCashDirect(PettyCash pettyCash,
			PettyCash cashCarryForward, List<PettyCashDetail> pettyCashDetails,
			List<PettyCashDetail> pettyCashDetailDel, PettyCash takenVoucher)
			throws Exception {
		boolean updateFlag = false;
		pettyCash.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());

		if (null != pettyCash.getPettyCashId())
			updateFlag = true;
		if (!updateFlag)
			SystemBL.saveReferenceStamp(PettyCash.class.getName(),
					getImplementation());
		pettyCash.setImplementation(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		if (!updateFlag)
			pettyCash.setPersonByCreatedBy(person);
		else {
			transactionBL.deleteTransaction(pettyCash.getPettyCashId(),
					PettyCash.class.getSimpleName());
			if (null != pettyCashDetailDel && pettyCashDetailDel.size() > 0)
				pettyCashService.deletePettyCashDetail(pettyCashDetailDel);
		}
		if (null == pettyCash.getPersonByPersonId()
				&& (byte) pettyCash.getPettyCashType() != (byte) PettyCashTypes.Spend
						.getCode())
			pettyCash.setPersonByPersonId(person);

		List<PettyCash> pettyCashs = new ArrayList<PettyCash>();
		if (null != cashCarryForward) {
			cashCarryForward.setPersonByCreatedBy(person);
			cashCarryForward.setPersonByPersonId(person);
			pettyCashs.add(cashCarryForward);
			pettyCash.setPettyCashNo(SystemBL.getReferenceStamp(
					PettyCash.class.getName(), getImplementation()));
			SystemBL.saveReferenceStamp(PettyCash.class.getName(),
					getImplementation());
		}
		if (null != pettyCashDetails && pettyCashDetails.size() > 0) {
			for (PettyCashDetail pettyCashDetail : pettyCashDetails)
				pettyCashDetail.setPettyCash(pettyCash);
			pettyCash.setPettyCashDetails(new HashSet<PettyCashDetail>(
					pettyCashDetails));
		}
		pettyCashs.add(pettyCash);
		pettyCashService.savePettyCash(pettyCashs, pettyCashDetails,
				takenVoucher, null);

		Transaction transaction = null;
		if (((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Spend
				.getCode())
				|| ((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Reimbursed
						.getCode()))
			transaction = createPettyCashExpenseTransaction(pettyCash);
		if (null != transaction)
			transactionBL.saveTransaction(
					transaction,
					new ArrayList<TransactionDetail>(transaction
							.getTransactionDetails()));

	}

	public void doPettyCashBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		PettyCash pettyCash = pettyCashService.getPettyCashById(recordId);

		if (workflowDetailVO.isDeleteFlag()) {
			if (null != pettyCash.getPettyCash()) {
				PettyCash pettyCashPr = pettyCash.getPettyCash();
				pettyCashPr.setClosedFlag(TransactionType.Credit.getCode());
				pettyCashService.updatePettyCash(pettyCashPr);
			}

			if (((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Spend
					.getCode())
					|| ((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Reimbursed
							.getCode())) {
				transactionBL.deleteTransaction(pettyCash.getPettyCashId(),
						PettyCash.class.getSimpleName());
			}
			List<PettyCashDetail> pettyCashDetails = null;
			if (null != pettyCash.getPettyCashDetails()
					&& pettyCash.getPettyCashDetails().size() > 0)
				pettyCashDetails = new ArrayList<PettyCashDetail>(
						pettyCash.getPettyCashDetails());
			pettyCashService.deletePettyCash(pettyCash, pettyCashDetails);
		} else {
			Transaction transaction = null;
			if (((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Spend
					.getCode())
					|| ((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Reimbursed
							.getCode()))
				transaction = createPettyCashExpenseTransaction(pettyCash);
			if (null != transaction)
				transactionBL.saveTransaction(
						transaction,
						new ArrayList<TransactionDetail>(transaction
								.getTransactionDetails()));
		}
	}

	// create expense petty cash transaction
	private Transaction createPettyCashExpenseTransaction(PettyCash pettyCash)
			throws Exception {
		PettyCash pettyCashTransaction = pettyCashService
				.getPettyCashById(pettyCash.getPettyCash().getPettyCashId());
		Transaction transaction = transactionBL
				.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pettyCash.getVoucherDate()),
						null != pettyCashTransaction.getDirectPaymentDetail() ? pettyCashTransaction
								.getDirectPaymentDetail().getDirectPayment()
								.getCurrency().getCurrencyId()
								: pettyCashTransaction.getBankReceiptsDetail()
										.getBankReceipts().getCurrency()
										.getCurrencyId(), "Petty Cash Ref "
								.concat(pettyCash.getPettyCashNo()), pettyCash
								.getVoucherDate(), transactionBL
								.getCategory("Petty Cash"), pettyCash
								.getPettyCashId(), PettyCash.class
								.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double expenseAmount = 0;
		if ((byte) PettyCashTypes.Spend.getCode() == (byte) pettyCash
				.getPettyCashType()) {
			for (PettyCashDetail pettyCashDetail : pettyCash
					.getPettyCashDetails()) {
				expenseAmount += pettyCashDetail.getExpenseAmount();
				transactionDetails.add(transactionBL.createTransactionDetail(
						pettyCashDetail.getExpenseAmount(), pettyCashDetail
								.getCombination().getCombinationId(),
						TransactionType.Debit.getCode(),
						"Expense petty cash ref: ".concat(pettyCash
								.getPettyCashNo()), null, pettyCash
								.getPettyCashNo()));
			}
		} else
			transactionDetails.add(transactionBL.createTransactionDetail(
					pettyCash.getExpenseAmount(), pettyCash.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(), "Expense petty cash ref: "
							.concat(pettyCash.getPettyCashNo()), null,
					pettyCash.getPettyCashNo()));
		transactionDetails.add(transactionBL.createTransactionDetail(
				expenseAmount, null != pettyCashTransaction
						.getDirectPaymentDetail() ? pettyCashTransaction
						.getDirectPaymentDetail().getCombination()
						.getCombinationId() : pettyCashTransaction
						.getBankReceiptsDetail().getCombination()
						.getCombinationId(), TransactionType.Credit.getCode(),
				"Petty cash ref ".concat(pettyCash.getPettyCashNo()), null,
				pettyCash.getPettyCashNo()));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	@SuppressWarnings("unused")
	private Transaction createPettyCashExpenseAddTransaction(PettyCash pettyCash)
			throws Exception {
		PettyCash pettyCashTransaction = pettyCashService
				.getPettyCashById(pettyCash.getPettyCash().getPettyCashId());
		Transaction transaction = transactionBL
				.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pettyCash.getVoucherDate()),
						null != pettyCashTransaction.getDirectPaymentDetail() ? pettyCashTransaction
								.getDirectPaymentDetail().getDirectPayment()
								.getCurrency().getCurrencyId()
								: pettyCashTransaction.getBankReceiptsDetail()
										.getBankReceipts().getCurrency()
										.getCurrencyId(), "Petty Cash Ref "
								.concat(pettyCash.getPettyCashNo()), pettyCash
								.getVoucherDate(), transactionBL
								.getCategory("Petty Cash"), pettyCash
								.getPettyCashId(), PettyCash.class
								.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(transactionBL.createTransactionDetail(pettyCash
				.getCashOut(), pettyCashTransaction.getCombination()
				.getCombinationId(), TransactionType.Debit.getCode(),
				"Expense petty cash ref: ".concat(pettyCash.getPettyCashNo()),
				null, pettyCash.getPettyCashNo()));
		transactionDetails.add(transactionBL.createTransactionDetail(pettyCash
				.getCashOut(), null != pettyCashTransaction
				.getDirectPaymentDetail() ? pettyCashTransaction
				.getDirectPaymentDetail().getCombination().getCombinationId()
				: pettyCashTransaction.getBankReceiptsDetail().getCombination()
						.getCombinationId(), TransactionType.Credit.getCode(),
				"Petty cash ref ".concat(pettyCash.getPettyCashNo()), null,
				pettyCash.getPettyCashNo()));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	@SuppressWarnings("unused")
	private Transaction createPettyCashExpenseDeductTransaction(
			PettyCash pettyCash) throws Exception {
		PettyCash pettyCashTransaction = pettyCashService
				.getPettyCashById(pettyCash.getPettyCash().getPettyCashId());
		Transaction transaction = transactionBL
				.createTransaction(
						transactionBL.getCalendarBL().transactionPostingPeriod(
								pettyCash.getVoucherDate()),
						null != pettyCashTransaction.getDirectPaymentDetail() ? pettyCashTransaction
								.getDirectPaymentDetail().getDirectPayment()
								.getCurrency().getCurrencyId()
								: pettyCashTransaction.getBankReceiptsDetail()
										.getBankReceipts().getCurrency()
										.getCurrencyId(), "Petty Cash Ref "
								.concat(pettyCash.getPettyCashNo()), pettyCash
								.getVoucherDate(), transactionBL
								.getCategory("Petty Cash"), pettyCash
								.getPettyCashId(), PettyCash.class
								.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(transactionBL.createTransactionDetail(pettyCash
				.getCashIn(), null != pettyCashTransaction
				.getDirectPaymentDetail() ? pettyCashTransaction
				.getDirectPaymentDetail().getCombination().getCombinationId()
				: pettyCashTransaction.getBankReceiptsDetail().getCombination()
						.getCombinationId(), TransactionType.Debit.getCode(),
				"Petty cash ref ".concat(pettyCash.getPettyCashNo()), null,
				pettyCash.getPettyCashNo()));
		transactionDetails.add(transactionBL.createTransactionDetail(pettyCash
				.getCashIn(), pettyCashTransaction.getCombination()
				.getCombinationId(), TransactionType.Credit.getCode(),
				"Expense petty cash ref: ".concat(pettyCash.getPettyCashNo()),
				null, pettyCash.getPettyCashNo()));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// delete petty cash
	public void deletePettyCash(PettyCash pettyCash) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PettyCash.class.getSimpleName(), pettyCash.getPettyCashId(),
				user, workflowDetailVo);
	}

	public void deletePettyCashDirect(PettyCash pettyCash) throws Exception {
		if (((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Spend
				.getCode())
				|| ((byte) pettyCash.getPettyCashType() == (byte) PettyCashTypes.Reimbursed
						.getCode())) {
			transactionBL.deleteTransaction(pettyCash.getPettyCashId(),
					PettyCash.class.getSimpleName());
		}
		List<PettyCashDetail> pettyCashDetails = null;
		if (null != pettyCash.getPettyCashDetails()
				&& pettyCash.getPettyCashDetails().size() > 0)
			pettyCashDetails = new ArrayList<PettyCashDetail>(
					pettyCash.getPettyCashDetails());
		pettyCashService.deletePettyCash(pettyCash, pettyCashDetails);
	}

	public Map<String, String> getPettyCashTypes() throws Exception {
		Map<String, String> pettyCashTypes = new HashMap<String, String>();
		String key = "";
		for (PettyCashTypes pettyCashType : EnumSet.allOf(PettyCashTypes.class)) {
			if ((byte) PettyCashTypes.Advance_issued.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Advance_issued.getCode().toString()
						.concat("@@").concat("AIPC");

			else if ((byte) PettyCashTypes.Carry_forward.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Carry_forward.getCode().toString()
						.concat("@@").concat("CFRD");

			else if ((byte) PettyCashTypes.Cash_transfer.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Cash_transfer.getCode().toString()
						.concat("@@").concat("CTPC");

			else if ((byte) PettyCashTypes.Cash_given_settlement.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Cash_given_settlement.getCode().toString()
						.concat("@@").concat("CGFS");

			else if ((byte) PettyCashTypes.Cash_received_settlement.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Cash_received_settlement.getCode()
						.toString().concat("@@").concat("CRFS");

			else if ((byte) PettyCashTypes.Reimbursed.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Reimbursed.getCode().toString()
						.concat("@@").concat("RFPC");

			else if ((byte) PettyCashTypes.Spend.getCode() == (byte) pettyCashType
					.getCode())
				key = PettyCashTypes.Spend.getCode().toString().concat("@@")
						.concat("SFAI");

			pettyCashTypes.put(key, pettyCashType.name().replaceAll("_", " "));
		}
		return pettyCashTypes;
	}

	public void updatePettyCashTransaction(List<PettyCash> pettyCashs)
			throws Exception {
		for (PettyCash pettyCash : pettyCashs)
			this.processPettyCashTransaction(pettyCash);
	}

	public void updatePettyCashTransaction() throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(6L);
		List<Transaction> transactions = transactionBL.getTransactionService()
				.getTransactionByImplementationAndUseCase(impl,
						PettyCash.class.getSimpleName());
		for (Transaction transaction : transactions) {
			PettyCash dp = pettyCashService.findPettyCashById(transaction
					.getRecordId());
			transaction.setTransactionTime(dp.getVoucherDate());
		}
		transactionBL.getTransactionService().saveJournalEntries(transactions);
	}

	public void updatePettyCashDetail(List<PettyCashDetail> pettyCashDetails)
			throws Exception {
		pettyCashService.savePettyCashDetails(pettyCashDetails);
	}

	private void processPettyCashTransaction(PettyCash pettyCash)
			throws Exception {
		transactionBL.deleteTransaction(pettyCash.getPettyCashId(),
				PettyCash.class.getSimpleName());
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						pettyCash.getVoucherDate()),
				null != pettyCash.getDirectPaymentDetail() ? pettyCash
						.getDirectPaymentDetail().getDirectPayment()
						.getCurrency().getCurrencyId() : pettyCash
						.getBankReceiptsDetail().getBankReceipts()
						.getCurrency().getCurrencyId(),
				"Petty Cash Reference ".concat(pettyCash.getPettyCashNo()),
				pettyCash.getVoucherDate(),
				transactionBL.getCategory("Petty Cash"),
				pettyCash.getPettyCashId(), PettyCash.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(transactionBL.createTransactionDetail(pettyCash
				.getExpenseAmount(), pettyCash.getCombination()
				.getCombinationId(), TransactionType.Debit.getCode(),
				"Expense petty cash ref ".concat(pettyCash.getPettyCashNo()),
				null, pettyCash.getPettyCashNo()));
		transactionDetails.add(transactionBL.createTransactionDetail(
				pettyCash.getExpenseAmount(),
				null != pettyCash.getDirectPaymentDetail() ? pettyCash
						.getDirectPaymentDetail().getCombination()
						.getCombinationId() : pettyCash.getBankReceiptsDetail()
						.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(),
				"Petty cash ref ".concat(pettyCash.getPettyCashNo()), null,
				pettyCash.getPettyCashNo()));
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PettyCashService getPettyCashService() {
		return pettyCashService;
	}

	public void setPettyCashService(PettyCashService pettyCashService) {
		this.pettyCashService = pettyCashService;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
}
