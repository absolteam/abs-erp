package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.vo.AssetClaimInsuranceVO;
import com.aiotech.aios.accounts.service.AssetClaimInsuranceService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetClaimInsuranceBL {

	private AssetClaimInsuranceService assetClaimInsuranceService;
	private LookupMasterBL lookupMasterBL;
	private AlertBL alertBL;
	private DirectPaymentBL directPaymentBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Asset Check Outs
	public List<Object> showAllAssetClaimInsurance() throws Exception {
		List<AssetClaimInsurance> assetClaimInsurances = assetClaimInsuranceService
				.getAllAssetClaimInsurance(getImplementation());
		List<Object> assetClaimInsuranceVOs = new ArrayList<Object>();
		if (null != assetClaimInsurances && assetClaimInsurances.size() > 0) {
			for (AssetClaimInsurance assetClaimInsurance : assetClaimInsurances)
				assetClaimInsuranceVOs
						.add(addAssetClaimInsuranceVO(assetClaimInsurance));
		}
		return assetClaimInsuranceVOs;
	}

	// Save Asset Claim Insurance
	public void saveAssetClaimInsurance(AssetClaimInsurance assetClaimInsurance)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetClaimInsurance
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		boolean updateFlag = false;
		assetClaimInsurance.setImplementation(getImplementation());
		if (null != assetClaimInsurance.getAssetClaimInsuranceId())
			updateFlag = true;
		if (!updateFlag) {
			SystemBL.saveReferenceStamp(AssetClaimInsurance.class.getName(),
					getImplementation());
		}
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetClaimInsurance != null
				&& assetClaimInsurance.getAssetClaimInsuranceId() != null
				&& assetClaimInsurance.getAssetClaimInsuranceId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetClaimInsuranceService.saveAssetClaimInsurance(assetClaimInsurance);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetClaimInsurance.class.getSimpleName(),
				assetClaimInsurance.getAssetClaimInsuranceId(), user,
				workflowDetailVo);
	}

	public void doAssetClaimInsuranceBusinessUpdate(Long assetClaimInsuranceId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			AssetClaimInsurance assetClaimInsurance = assetClaimInsuranceService
					.getAssetClaimInsuranceByClaimId(assetClaimInsuranceId);
			if (workflowDetailVO.isDeleteFlag()) {
				directPaymentBL.directDeleteDirectPayment(
						assetClaimInsurance.getAssetClaimInsuranceId(),
						AssetClaimInsurance.class.getSimpleName());
				assetClaimInsuranceService
						.deleteAssetClaimInsurance(assetClaimInsurance);
			} else if (workflowDetailVO.isEditFlag()) {
				// Alert
				LocalDate endDate = new DateTime(
						assetClaimInsurance.getClaimDate()).toLocalDate();
				LocalDate currentDate = DateTime.now().toLocalDate();
				if (endDate.compareTo(currentDate) <= 0)
					createClaimInsuranceAlert(assetClaimInsurance, true);
			} else {
				// Alert
				LocalDate endDate = new DateTime(
						assetClaimInsurance.getClaimDate()).toLocalDate();
				LocalDate currentDate = DateTime.now().toLocalDate();
				if (endDate.compareTo(currentDate) <= 0)
					createClaimInsuranceAlert(assetClaimInsurance, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createClaimInsuranceAlert(
			AssetClaimInsurance assetClaimInsurance, boolean updateFlag)
			throws Exception {
		if (updateFlag) {
			updateFlag = false;
			Alert alert = alertBL.getAlertService().getAlertRecordInfo(
					assetClaimInsurance.getAssetClaimInsuranceId(),
					AssetClaimInsurance.class.getSimpleName());
			if (null != alert) {
				alert.setIsActive(true);
				alertBL.commonSaveAlert(alert);
				updateFlag = true;
			}
			if (assetClaimInsurance.getClaimExpenses() > 0) {
				Alert alertExp = alertBL.getAlertService().getAlertRecordInfo(
						assetClaimInsurance.getAssetClaimInsuranceId(),
						AssetClaimInsurance.class.getSimpleName());
				if (null != alertExp) {
					alertExp.setIsActive(true);
					alertBL.commonSaveAlert(alertExp);
				}
			}
		}
		if (!updateFlag) {
			List<AssetClaimInsurance> assetClaimInsurances = new ArrayList<AssetClaimInsurance>();
			assetClaimInsurances.add(assetClaimInsurance);
			alertBL.alertClaimInsurance(assetClaimInsurances);
			if (assetClaimInsurance.getClaimExpenses() != null
					&& assetClaimInsurance.getClaimExpenses() > 0) {
				alertBL.alertClaimInsuranceExpense(assetClaimInsurances);
			}
		}
	}

	// Delete Asset Claim Insurance
	public void deleteAssetClaimInsurance(
			AssetClaimInsurance assetClaimInsurance) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetClaimInsurance.class.getSimpleName(),
				assetClaimInsurance.getAssetClaimInsuranceId(), user,
				workflowDetailVo);
	}

	// Add Asset Insurance
	private AssetClaimInsuranceVO addAssetClaimInsuranceVO(
			AssetClaimInsurance assetClaimInsurance) {
		AssetClaimInsuranceVO assetClaimInsuranceVO = new AssetClaimInsuranceVO();
		assetClaimInsuranceVO.setAssetClaimInsuranceId(assetClaimInsurance
				.getAssetClaimInsuranceId());
		assetClaimInsuranceVO.setReferenceNumber(assetClaimInsurance
				.getReferenceNumber());
		assetClaimInsuranceVO.setAssetName(assetClaimInsurance
				.getAssetInsurance().getAssetCreation().getAssetName());
		assetClaimInsuranceVO.setInsuranceType(assetClaimInsurance
				.getAssetInsurance().getLookupDetailByInsuranceType()
				.getDisplayName());
		assetClaimInsuranceVO.setDate(DateFormat
				.convertDateToString(assetClaimInsurance.getClaimDate()
						.toString()));
		assetClaimInsuranceVO.setStatusName(Constants.Accounts.POStatus.get(
				assetClaimInsurance.getStatus()).name());
		assetClaimInsuranceVO.setClaimAmount(assetClaimInsurance
				.getClaimAmount());
		assetClaimInsuranceVO.setPaymentMode(null != assetClaimInsurance
				.getModeOfPayment() ? Constants.Accounts.PaymentRequestMode
				.get(assetClaimInsurance.getModeOfPayment()).name() : "");
		assetClaimInsuranceVO.setPayOutNature(null != assetClaimInsurance
				.getLookupDetail() ? assetClaimInsurance.getLookupDetail()
				.getDisplayName() : "");
		return assetClaimInsuranceVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetClaimInsuranceService getAssetClaimInsuranceService() {
		return assetClaimInsuranceService;
	}

	public void setAssetClaimInsuranceService(
			AssetClaimInsuranceService assetClaimInsuranceService) {
		this.assetClaimInsuranceService = assetClaimInsuranceService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
