package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetDepreciation;
import com.aiotech.aios.accounts.domain.entity.AssetDetail;
import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AssetVO;
import com.aiotech.aios.accounts.service.AssetCreationService;
import com.aiotech.aios.accounts.service.AssetDepreciationService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.to.Constants.Accounts.ValuationType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;

public class AssetDepreciationBL {

	// Dependencies
	private AssetCreationService assetCreationService;
	private StockBL stockBL;
	private TransactionBL transactionBL;
	private AssetDepreciationService assetDepreciationService;

	// Create Depreciation
	public void createDepreciation() throws Exception {
		List<AssetCreation> assetCreations = assetCreationService
				.getAssetsWithReceiveDetails();
		if (null != assetCreations && assetCreations.size() > 0) {
			List<AssetVO> assetVOs = new ArrayList<AssetVO>();
			for (AssetCreation assetCreation : assetCreations) {
				if (validateDepreciationAsset(assetCreation))
					assetVOs.add(addDepreciationVO(assetCreation));
			}
			if (null != assetVOs && assetVOs.size() > 0) {
				assetDepreciationService
						.saveAssetDepreciation(calculateDepreciation(assetVOs));
				// Save Transaction
				transactionBL.saveTransactions(createTransactions(assetVOs));
			}
		}
	}

	private boolean validateDepreciationAsset(AssetCreation assetCreation)
			throws Exception {
		String currentDate = DateFormat.convertSystemDateToString(Calendar
				.getInstance().getTime());
		String depreciationDate = "";
		Date commenceDate = assetCreation.getCommenceDate();
		if (null != assetCreation.getAssetDepreciations()
				&& assetCreation.getAssetDepreciations().size() > 0) {
			commenceDate = getLastDepreciatedDate(new ArrayList<AssetDepreciation>(
					assetCreation.getAssetDepreciations()));
		}
		if (Constants.Accounts.DepreciationConvention.Full_Month.getCode()
				.equals(assetCreation.getConvention()))
			depreciationDate = DateFormat.convertSystemDateToString(DateFormat
					.addMonth(commenceDate));
		else if (Constants.Accounts.DepreciationConvention.Half_Year.getCode()
				.equals(assetCreation.getConvention()))
			depreciationDate = DateFormat.convertSystemDateToString(DateFormat
					.addToMonths(commenceDate, 6));
		else if (Constants.Accounts.DepreciationConvention.Full_Year.getCode()
				.equals(assetCreation.getConvention()))
			depreciationDate = DateFormat.convertSystemDateToString(DateFormat
					.addToMonths(commenceDate, 12));
		if ((!("").equals(depreciationDate))
				&& currentDate.equals(depreciationDate))
			return true;
		return false;
	}

	// Get Last Depreciated Date
	private Date getLastDepreciatedDate(
			List<AssetDepreciation> assetDepreciations) {
		Collections.sort(assetDepreciations,
				new Comparator<AssetDepreciation>() {
					public int compare(AssetDepreciation a1,
							AssetDepreciation a2) {
						return a2.getPeriod().compareTo(a1.getPeriod());
					}
				});
		return assetDepreciations.get(0).getPeriod();
	}

	// Add DepreciationVO
	public AssetVO addDepreciationVO(AssetCreation assetCreation)
			throws Exception {
		AssetVO assetVO = new AssetVO();
		assetVO.setAssetCreationId(assetCreation.getAssetCreationId());
		assetVO.setScrapValue(assetCreation.getScrapValue());
		assetVO.setCommenceDate(assetCreation.getCommenceDate());
		assetVO.setDepreciationMethod(assetCreation.getDepreciationMethod());
		if (null != assetCreation.getAssetRevaluations()
				&& assetCreation.getAssetRevaluations().size() > 0) {
			assetVO.setAssetCost(getAssetFairMarketValue(assetCreation
					.getAssetRevaluations()));
		} else {
			assetVO.setAssetCost(null); // Need to change due to stock code
										// update
			if (null != assetCreation.getAssetDetails()
					&& assetCreation.getAssetDetails().size() > 0) {
				for (AssetDetail assetDetail : assetCreation.getAssetDetails())
					assetVO.setAssetCost(assetVO.getAssetCost()
							+ assetDetail.getDirectPaymentDetail().getAmount());
			}
		}
		assetVO.setUsefulLife(assetCreation.getUsefulLife());
		assetVO.setConvention(assetCreation.getConvention());
		assetVO.setDecliningVariance(assetCreation.getDecliningVariance());
		assetVO.setImplementation(assetCreation.getImplementation());
		assetVO.setConventionValue((assetVO.getConvention()
				.equals(Constants.Accounts.DepreciationConvention.Full_Month
						.getCode())) ? 1 : (assetVO.getConvention()
				.equals(Constants.Accounts.DepreciationConvention.Half_Year
						.getCode())) ? 6 : 12);

		assetVO.setAssetDepreciations(assetCreation.getAssetDepreciations());
		return assetVO;
	}

	// Get Last Reevaluation Market Value
	private Double getAssetFairMarketValue(
			Set<AssetRevaluation> assetRevaluation) {
		List<AssetRevaluation> assetRevaluations = new ArrayList<AssetRevaluation>();
		for (AssetRevaluation revaluation : assetRevaluation) {
			if (revaluation.getValuationType().equals(
					ValuationType.Revaluation.getCode()))
				assetRevaluations.add(revaluation);
		}
		Collections.sort(assetRevaluations, new Comparator<AssetRevaluation>() {
			public int compare(AssetRevaluation a1, AssetRevaluation a2) {
				return a1.getRevaluationDate().compareTo(
						a2.getRevaluationDate());
			}
		});
		return assetRevaluations.get(assetRevaluations.size() - 1)
				.getFairMarketValue();
	}

	// Calculate Asset Depreciation
	private List<AssetDepreciation> calculateDepreciation(List<AssetVO> assetVOs)
			throws Exception {
		List<AssetDepreciation> assetDepreciations = new ArrayList<AssetDepreciation>();
		for (AssetVO assetVO : assetVOs) {
			AssetDepreciation assetDepreciation = addAssetDepreciation(assetVO);
			assetVO.setDepreciationExpenses(assetDepreciation.getDepreciation()
					* assetVO.getConventionValue());
			assetDepreciations.add(assetDepreciation);
		}
		return assetDepreciations;
	}

	// Calculate Asset Depreciation
	public AssetDepreciation addAssetDepreciation(AssetVO assetVO)
			throws Exception {
		AssetDepreciation assetDepreciation = new AssetDepreciation();
		switch (assetVO.getDepreciationMethod()) {
		case (byte) 1:
			assetDepreciation = straightLineDepreciation(assetVO);
			break;
		case (byte) 2:
			assetDepreciation = decliningBalanceDepreciation(assetVO);
			break;
		case (byte) 3:
			assetDepreciation = decliningVarianceDepreciation(assetVO);
			break;
		case (byte) 4:
			assetDepreciation = doubleDecliningDepreciation(assetVO);
			break;
		case (byte) 5:
			assetDepreciation = sumOfYearDigitDepreciation(assetVO);
			break;
		}
		return assetDepreciation;
	}

	// Straight Line Depreciation
	private AssetDepreciation straightLineDepreciation(AssetVO assetVO)
			throws Exception {
		AssetDepreciation assetDepreciation = setAssetDepreciationCommons(assetVO);
		assetDepreciation.setDepreciation(AIOSCommons.roundDecimals(((assetVO
				.getAssetCost() - (null != assetVO.getScrapValue() ? assetVO
				.getScrapValue() : 0)) / assetVO.getUsefulLife())));
		return assetDepreciation;
	}

	// Declining Balance Depreciation
	private AssetDepreciation decliningBalanceDepreciation(AssetVO assetVO)
			throws Exception {
		AssetDepreciation assetDepreciation = setAssetDepreciationCommons(assetVO);
		double depreciationRate = AIOSCommons.roundThreeDecimals(1 - (Math.pow(
				assetVO.getScrapValue() / assetVO.getAssetCost(), (double) 1
						/ (double) assetVO.getUsefulLife())));
		double accumulatedDepreciation = 0;
		if (null != assetVO.getAssetDepreciations()
				&& assetVO.getAssetDepreciations().size() > 0)
			accumulatedDepreciation = getAccumulatedDepreciation(assetVO
					.getAssetDepreciations());
		assetDepreciation.setDepreciation(AIOSCommons.roundDecimals((assetVO
				.getAssetCost() - accumulatedDepreciation) * depreciationRate));
		return assetDepreciation;
	}

	// Declining Variance Depreciation
	private AssetDepreciation decliningVarianceDepreciation(AssetVO assetVO)
			throws Exception {
		AssetDepreciation assetDepreciation = setAssetDepreciationCommons(assetVO);
		double accumulatedDepreciation = 0;
		if (null != assetVO.getAssetDepreciations()
				&& assetVO.getAssetDepreciations().size() > 0)
			accumulatedDepreciation = getAccumulatedDepreciation(assetVO
					.getAssetDepreciations());
		assetDepreciation.setDepreciation(AIOSCommons.roundDecimals(((assetVO
				.getAssetCost() - accumulatedDepreciation) * assetVO
				.getDecliningVariance())
				/ assetVO.getUsefulLife()));
		return assetDepreciation;
	}

	// Double Declining Balance Depreciation
	private AssetDepreciation doubleDecliningDepreciation(AssetVO assetVO)
			throws Exception {
		AssetDepreciation assetDepreciation = setAssetDepreciationCommons(assetVO);
		double accumulatedDepreciation = 0;
		if (null != assetVO.getAssetDepreciations()
				&& assetVO.getAssetDepreciations().size() > 0)
			accumulatedDepreciation = getAccumulatedDepreciation(assetVO
					.getAssetDepreciations());
		assetDepreciation.setDepreciation(AIOSCommons.roundDecimals(((assetVO
				.getAssetCost() - accumulatedDepreciation) / assetVO
				.getUsefulLife()) * 2));
		return assetDepreciation;
	}

	// SumOfYear Digit Depreciation
	private AssetDepreciation sumOfYearDigitDepreciation(AssetVO assetVO)
			throws Exception {
		AssetDepreciation assetDepreciation = setAssetDepreciationCommons(assetVO);
		assetDepreciation.setDepreciation(AIOSCommons.roundDecimals(((assetVO
				.getAssetCost() - (null != assetVO.getScrapValue() ? assetVO
				.getScrapValue() : 0)) * (assetVO.getUsefulLife() - assetVO
				.getAssetDepreciations().size()))
				/ getSumOfDigit(assetVO.getUsefulLife())));

		return assetDepreciation;
	}

	// Set Asset Depreciation Common Values
	private AssetDepreciation setAssetDepreciationCommons(AssetVO assetVO)
			throws Exception {
		AssetCreation assetCreation = new AssetCreation();
		AssetDepreciation assetDepreciation = new AssetDepreciation();
		assetCreation.setAssetCreationId(assetVO.getAssetCreationId());
		assetDepreciation.setAssetCreation(assetCreation);
		assetDepreciation.setPeriod(Calendar.getInstance().getTime());
		return assetDepreciation;
	}

	// Get Accumulated Depreciation
	private double getAccumulatedDepreciation(
			Set<AssetDepreciation> assetDepreciations) throws Exception {
		double accumulatedDepreciation = 0;
		for (AssetDepreciation depreciation : assetDepreciations)
			accumulatedDepreciation += depreciation.getDepreciation();
		return accumulatedDepreciation;
	}

	// Get SumOfDigit
	private double getSumOfDigit(Integer usefulLife) throws Exception {
		int sumOfDigit = 0;
		for (int i = 1; i <= usefulLife; i++)
			sumOfDigit += i;
		return sumOfDigit;
	}

	// Create Transactions
	private List<Transaction> createTransactions(List<AssetVO> assetVOs)
			throws Exception {
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (AssetVO assetVO : assetVOs)
			transactions.add(createTransaction(assetVO));
		return transactions;
	}

	// Create Transaction
	private Transaction createTransaction(AssetVO assetVO) throws Exception {
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency(assetVO.getImplementation()).getCurrencyId(), "",
				Calendar.getInstance().getTime(), transactionBL.getCategory(
						"ASSET DEPRECIATION", assetVO.getImplementation()),
				assetVO.getImplementation(), assetVO.getAssetCreationId(),
				AssetDepreciation.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(depreciationExpenseEntry(assetVO));
		transactionDetails.add(accumulatedDepreciationEntry(assetVO));
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	// Depreciation Expense Entry
	private TransactionDetail depreciationExpenseEntry(AssetVO assetVO)
			throws Exception {
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(assetVO.getDepreciationExpenses(),
						getImplementation().getExpenseAccount(),
						TransactionType.Debit.getCode(), null, null, null);
		return transactionDetail;
	}

	// Accumulated Depreciation Entry
	private TransactionDetail accumulatedDepreciationEntry(AssetVO assetVO)
			throws Exception {
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(assetVO.getDepreciationExpenses(),
						getImplementation().getAccumulatedDepreciation(),
						TransactionType.Credit.getCode(), null, null,null);
		return transactionDetail;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public AssetDepreciationService getAssetDepreciationService() {
		return assetDepreciationService;
	}

	public void setAssetDepreciationService(
			AssetDepreciationService assetDepreciationService) {
		this.assetDepreciationService = assetDepreciationService;
	}

	public AssetCreationService getAssetCreationService() {
		return assetCreationService;
	}

	public void setAssetCreationService(
			AssetCreationService assetCreationService) {
		this.assetCreationService = assetCreationService;
	}

}
