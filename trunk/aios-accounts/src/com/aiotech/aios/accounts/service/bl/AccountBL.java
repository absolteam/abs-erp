package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.common.to.Constants.Accounts.Segments;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AccountBL {

	private int segmentId;
	private SegmentBL segmentBL;
	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;

	private SystemService systemService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void updateSegments() throws Exception {
		try {
			List<Implementation> implementations = systemService
					.getAllImplementation();

			List<Segment> segmenttmp = segmentBL.getSegmentService()
					.getAllSegments(implementations.get(0));
			if (null != segmenttmp && segmenttmp.size() > 0)
				return;
			List<Segment> segments = new ArrayList<Segment>();
			for (Implementation implementation : implementations) {
				for (Segments e : EnumSet.allOf(Segments.class))
					segments.add(addSegment(e.getCode(), implementation));
			}
			if (null != segments && segments.size() > 0) {
				segmentBL.saveSegment(segments);
				List<Account> accounts = null;
				for (Segment segment : segments) {
					accounts = accountService.getAccountsBySegmentAccessId(
							segment.getImplementation(), segment.getAccessId());
					if (null != accounts && accounts.size() > 0) {
						for (Account account : accounts)
							account.setSegment(segment);
						accountService.updateAccountList(accounts);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Segment addSegment(Integer accessId, Implementation implementation) {
		Segment segment = new Segment();
		segment.setAccessId(accessId);
		segment.setSegmentName(Segments.get(accessId).name());
		segment.setImplementation(implementation);
		return segment;
	}

	/*
	 * public void updateCOAAccounts(List<Account> accounts) throws Exception {
	 * Map<Long, List<Account>> accountsMap = new HashMap<Long,
	 * List<Account>>(); List<Account> tempAccountList = null; for (Account
	 * account : accounts) { tempAccountList = new ArrayList<Account>(); if
	 * (null != accountsMap &&
	 * accountsMap.containsKey(account.getImplementation()
	 * .getImplementationId())) { tempAccountList.addAll(accountsMap.get(account
	 * .getImplementation().getImplementationId())); }
	 * tempAccountList.add(account);
	 * accountsMap.put(account.getImplementation().getImplementationId(),
	 * tempAccountList); }
	 * 
	 * Map<Long, List<Account>> sortedAccountsMap = new TreeMap<Long,
	 * List<Account>>( new Comparator<Long>() { public int compare(Long o1, Long
	 * o2) { return o1.compareTo(o2); } });
	 * sortedAccountsMap.putAll(accountsMap); List<Account> finalAccountList =
	 * new ArrayList<Account>(); List<Combination> companyList = null;
	 * List<Combination> costCenterList = null; List<Combination>
	 * naturalAccountList = null; List<Combination> analysisList = null;
	 * List<Combination> buffer1List = null; List<Combination> buffer2List =
	 * null; Map<String, List<Combination>> combinationMap = new HashMap<String,
	 * List<Combination>>(); List<Combination> finalCombinationList = new
	 * ArrayList<Combination>(); for (Entry<Long, List<Account>> entry :
	 * sortedAccountsMap.entrySet()) { tempAccountList = new
	 * ArrayList<Account>(entry.getValue()); companyList = new
	 * ArrayList<Combination>(); costCenterList = new ArrayList<Combination>();
	 * naturalAccountList = new ArrayList<Combination>(); analysisList = new
	 * ArrayList<Combination>(); buffer1List = new ArrayList<Combination>();
	 * buffer2List = new ArrayList<Combination>();
	 * Collections.sort(tempAccountList, new Comparator<Account>() { public int
	 * compare(Account o1, Account o2) { return o1.getSegment().getSegmentId()
	 * .compareTo(o2.getSegment().getSegmentId()); } }); long startingDigit =
	 * tempAccountList.get(0).getImplementation() .getAccountStartingId();
	 * String key = ""; for (Account account : tempAccountList) {
	 * account.setAccountTempId(startingDigit); startingDigit += 1; if ((int)
	 * account.getSegment().getSegmentId() == (int) Segments.Company .getCode())
	 * { key = ""; if (null != account.getCombinationsForCompanyAccountId() &&
	 * account.getCombinationsForCompanyAccountId() .size() > 0) { key =
	 * account.getImplementation()
	 * .getImplementationId().toString().concat("-"+Segments.Company
	 * .getCode().toString()); companyList = new ArrayList<Combination>(
	 * account.getCombinationsForCompanyAccountId()); for (Combination
	 * combination : companyList) { combination.setCompanyAccountTempId(account
	 * .getAccountTempId()); } combinationMap.put(key, companyList); } } if
	 * ((int) account.getSegment().getSegmentId() == (int) Segments.CostCenter
	 * .getCode()) { key = ""; if (null !=
	 * account.getCombinationsForCostcenterAccountId() &&
	 * account.getCombinationsForCostcenterAccountId() .size() > 0) { key =
	 * account.getImplementation()
	 * .getImplementationId().toString().concat("-"+Segments.CostCenter
	 * .getCode().toString()); costCenterList = new ArrayList<Combination>(
	 * account.getCombinationsForCostcenterAccountId()); for (Combination
	 * combination : costCenterList) {
	 * combination.setCostcenterAccountTempId(account .getAccountTempId()); }
	 * combinationMap.put(key, costCenterList); } } if ((int)
	 * account.getSegment().getSegmentId() == (int) Segments.Natural .getCode())
	 * { key = ""; if (null != account.getCombinationsForNaturalAccountId() &&
	 * account.getCombinationsForNaturalAccountId() .size() > 0) { key =
	 * account.getImplementation()
	 * .getImplementationId().toString().concat("-"+Segments.Natural
	 * .getCode().toString()); naturalAccountList = new ArrayList<Combination>(
	 * account.getCombinationsForNaturalAccountId()); for (Combination
	 * combination : naturalAccountList) {
	 * combination.setNaturalAccountTempId(account .getAccountTempId()); }
	 * combinationMap.put(key, naturalAccountList); } } if ((int)
	 * account.getSegment().getSegmentId() == (int) Segments.Analysis
	 * .getCode()) { key = ""; if (null !=
	 * account.getCombinationsForAnalysisAccountId() &&
	 * account.getCombinationsForAnalysisAccountId() .size() > 0) { key =
	 * account.getImplementation()
	 * .getImplementationId().toString().concat("-"+Segments.Analysis
	 * .getCode().toString()); analysisList = new ArrayList<Combination>(
	 * account.getCombinationsForAnalysisAccountId()); for (Combination
	 * combination : analysisList) {
	 * combination.setAnalysisAccountTempId(account .getAccountTempId()); }
	 * combinationMap.put(key, analysisList); } } if ((int)
	 * account.getSegment().getSegmentId() == (int) Segments.Buffer1 .getCode())
	 * { key = ""; if (null != account.getCombinationsForBuffer1AccountId() &&
	 * account.getCombinationsForBuffer1AccountId() .size() > 0) { key =
	 * account.getImplementation()
	 * .getImplementationId().toString().concat("-"+Segments.Buffer1
	 * .getCode().toString()); buffer1List = new ArrayList<Combination>(
	 * account.getCombinationsForBuffer1AccountId()); for (Combination
	 * combination : buffer1List) { combination.setBuffer1AccountTempId(account
	 * .getAccountTempId()); } combinationMap.put(key, buffer1List); } } if
	 * ((int) account.getSegment().getSegmentId() == (int) Segments.Buffer2
	 * .getCode()) { key = ""; if (null !=
	 * account.getCombinationsForBuffer2AccountId() &&
	 * account.getCombinationsForBuffer2AccountId() .size() > 0) { key =
	 * account.getImplementation()
	 * .getImplementationId().toString().concat("-"+Segments.Buffer2
	 * .getCode().toString()); buffer2List = new ArrayList<Combination>(
	 * account.getCombinationsForBuffer2AccountId()); for (Combination
	 * combination : buffer2List) { combination.setBuffer2AccountTempId(account
	 * .getAccountTempId()); } combinationMap.put(key, buffer2List); } } }
	 * entry.setValue(tempAccountList);
	 * finalAccountList.addAll(tempAccountList); }
	 * accountService.saveAccounts(finalAccountList); for (Entry<String,
	 * List<Combination>> entry : combinationMap.entrySet())
	 * finalCombinationList.addAll(entry.getValue());
	 * combinationService.saveCombination(finalCombinationList); }
	 */

	public void updateChartOfAccount(Account account, User user)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (account != null && account.getAccountId() != null) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		account.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		accountService.saveAccount(account);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Account.class.getSimpleName(), account.getAccountId(), user,
				workflowDetailVo);
	}

	public void doAccountBusinessUpdate(Long accountId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			Account account = accountService.getAccountDetails(accountId);
			if (workflowDetailVO.isDeleteFlag()) {
				accountService.deleteAccount(account);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteAccount(Account account) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Account.class.getSimpleName(), account.getAccountId(), user,
				workflowDetailVo);
	}

	public void saveChartOfAccounts(List<Account> accounts, User user)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = null;
		for (Account account : accounts) {
			account.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			workflowDetailVo = new WorkflowDetailVO();
			if (account != null && account.getAccountId() != null) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			Account tempAccount = accountService
					.getLastUpdatedAccount(getImplementation());
			long accountId = 0;
			if (null != tempAccount)
				accountId = tempAccount.getAccountId() + 1;
			else
				accountId = getImplementation().getAccountStartingId();
			account.setAccountId(accountId);
			accountService.saveAccount(account);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Account.class.getSimpleName(), account.getAccountId(),
					user, workflowDetailVo);
		}
	}

	public Account createCostCenterAccount(String accountDesc, Boolean isSystem)
			throws Exception {
		Segment segment = segmentBL.getParentSegment(
				segmentBL.getImplementation(), Segments.CostCenter.getCode());
		Account accounts = accountService.getLastUpdatedCostCenter(
				getImplementation(), segment.getSegmentId());
		if (validateAccounts(
				String.valueOf(new Integer(accounts.getCode().trim()) + 1),
				segment.getSegmentId())) {
			Account newAccount = new Account();
			newAccount.setCode(String.valueOf(new Integer(accounts.getCode()
					.trim()) + 1));
			newAccount.setAccount(accountDesc);
			newAccount.setSegment(accounts.getSegment());
			newAccount.setImplementation(accounts.getImplementation());
			newAccount.setAccountType(accounts.getAccountType());
			newAccount.setIsSystem(isSystem);
			newAccount.setAccountId(generateAccountId(
					accounts.getImplementation(), newAccount.getCode()));
			accountService.updateAccount(newAccount);
			return newAccount;
		} else {
			return null;
		}
	}

	private Long generateAccountId(Implementation implementation,
			String accountCode) throws Exception {
		Account tempAccount = accountService
				.getLastUpdatedAccount(implementation);
		long accountId = 0;
		if (null != tempAccount)
			accountId = tempAccount.getAccountId() + 1;
		else
			accountId = implementation.getAccountStartingId();
		return accountId;
	}

	private boolean validateAccounts(String code, Long segmentId)
			throws Exception {
		List<Account> accountList = accountService.validateAccountDetails(code,
				getImplementation(), segmentId);
		if (null != accountList && accountList.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public Account createNaturalAccount(String accountDesc,
			Integer accountType, Boolean isSystem) throws Exception {
		Segment segment = segmentBL.getParentSegment(
				segmentBL.getImplementation(), Segments.Natural.getCode());
		Account accounts = accountService.getLastUpdatedNatural(
				getImplementation(), segment.getSegmentId());
		if (validateAccounts(
				String.valueOf(new Integer(accounts.getCode().trim()) + 1),
				segment.getSegmentId())) {
			Account naturalAccount = new Account();
			naturalAccount.setCode(String.valueOf(new Integer(accounts
					.getCode().trim()) + 1));
			naturalAccount.setAccount(accountDesc);
			naturalAccount.setSegment(accounts.getSegment());
			naturalAccount.setImplementation(accounts.getImplementation());
			naturalAccount.setAccountType(accountType);
			naturalAccount.setIsSystem(isSystem);
			naturalAccount.setAccountId(generateAccountId(
					accounts.getImplementation(), naturalAccount.getCode()));
			accountService.updateAccount(naturalAccount);
			return naturalAccount;
		} else {
			return null;
		}
	}

	public Account createAnalyisAccount(String accountDesc, Boolean isSystem,
			Combination combination) throws Exception {
		Account newAccount = null;
		List<Combination> combinationAccounts = combinationService
				.getCombinationByNaturalAccountId(combination
						.getAccountByNaturalAccountId().getAccountId());
		boolean anyls = false;
		for (Combination combination2 : combinationAccounts) {
			if (null != combination2.getAccountByAnalysisAccountId()) {
				anyls = true;
				break;
			}
		}
		if (anyls) {
			List<Long> codeList = new ArrayList<Long>();
			for (Combination account : combinationAccounts) {
				if (null != account.getAccountByAnalysisAccountId())
					codeList.add(Long.valueOf(account
							.getAccountByAnalysisAccountId().getCode()));
			}
			Collections.sort(codeList, new Comparator<Long>() {
				public int compare(Long l1, Long l2) {
					return l2.compareTo(l1);
				}
			});
			String accountCode = String.valueOf(codeList.get(0) + 1);
			accountCode = getRecursiveAccountCode(accountCode, accountDesc,
					isSystem);
			newAccount = createAnalysisAccountDetails(accountCode, accountDesc,
					isSystem);
			accountService.updateAccount(newAccount);
			return newAccount;
		} else {
			String accountCode = combination.getAccountByNaturalAccountId()
					.getCode().concat("001");
			accountCode = getRecursiveAccountCode(accountCode, accountDesc,
					isSystem);
			newAccount = createAnalysisAccountDetails(accountCode, accountDesc,
					isSystem);
			accountService.updateAccount(newAccount);
			return newAccount;
		}
	}

	private String getRecursiveAccountCode(String accountCode,
			String accountDesc, Boolean isSystem) throws Exception {
		Segment segment = segmentBL.getParentSegment(
				segmentBL.getImplementation(), Segments.Analysis.getCode());
		List<Account> accounts = accountService.validateAccountDetails(
				accountCode, getImplementation(), segment.getSegmentId());
		if (null != accounts && accounts.size() > 0) {
			accountCode = String.valueOf(Long.parseLong(accountCode) + 1);
			getRecursiveAccountCode(accountCode, accountDesc, isSystem);
		}
		return accountCode;
	}

	public Account createAnalyisAccount(String accountDesc, Boolean isSystem,
			Combination combination, Implementation implementation)
			throws Exception {
		Account newAccount = null;
		List<Combination> combinationAccounts = combinationService
				.getCombinationByNaturalAccountId(combination
						.getAccountByNaturalAccountId().getAccountId());
		boolean anyls = false;
		for (Combination combination2 : combinationAccounts) {
			if (null != combination2.getAccountByAnalysisAccountId()) {
				anyls = true;
				break;
			}
		}
		if (anyls) {
			List<Long> codeList = new ArrayList<Long>();
			for (Combination account : combinationAccounts) {
				if (null != account.getAccountByAnalysisAccountId())
					codeList.add(Long.valueOf(account
							.getAccountByAnalysisAccountId().getCode()));
			}
			Collections.sort(codeList, new Comparator<Long>() {
				public int compare(Long l1, Long l2) {
					return l2.compareTo(l1);
				}
			});
			String accountCode = String.valueOf(codeList.get(0) + 1);
			Segment segement = segmentBL.getParentSegment(implementation,
					Segments.Analysis.getCode());
			List<Account> accounts = accountService.validateAccountDetails(
					accountCode, implementation, segement.getSegmentId());
			if (null == accounts || accounts.size() == 0) {
				newAccount = createAnalysisAccountDetails(accountCode,
						accountDesc, isSystem, implementation);
				accountService.updateAccount(newAccount);
			}
			return newAccount;
		} else {
			String accountCode = combination.getAccountByNaturalAccountId()
					.getCode().concat("001");
			accountCode = getRecursiveAccountCode(accountCode, accountDesc,
					isSystem);
			newAccount = createAnalysisAccountDetails(accountCode, accountDesc,
					isSystem, implementation);
			accountService.updateAccount(newAccount);
			return newAccount;
		}
	}

	private Account createAnalysisAccountDetails(String accountCode,
			String accountDesc, Boolean isSystem) throws Exception {
		Account newAccount = new Account();
		newAccount.setCode(accountCode);
		newAccount.setAccount(accountDesc);
		newAccount.setSegment(segmentBL.getParentSegment(
				segmentBL.getImplementation(), Segments.Analysis.getCode()));
		newAccount.setImplementation(getImplementation());
		newAccount.setAccountType(0);
		newAccount.setIsSystem(isSystem);
		newAccount.setAccountId(generateAccountId(getImplementation(),
				newAccount.getCode()));
		newAccount.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		return newAccount;
	}

	private Account createAnalysisAccountDetails(String accountCode,
			String accountDesc, Boolean isSystem, Implementation implementation)
			throws Exception {
		Account newAccount = new Account();
		newAccount.setCode(accountCode);
		newAccount.setAccount(accountDesc);
		newAccount.setSegment(segmentBL.getParentSegment(implementation,
				Segments.Analysis.getCode()));
		newAccount.setImplementation(implementation);
		newAccount.setAccountType(0);
		newAccount.setIsSystem(isSystem);
		newAccount.setAccountId(generateAccountId(implementation,
				newAccount.getCode()));
		return newAccount;
	}

	public String getSystemAccountCode(List<Account> accounts, Account account,
			long segmentId) throws Exception {
		String systemAccountCode = null;
		List<Long> codes = new ArrayList<Long>();
		List<Long> parentCodes = new ArrayList<Long>();
		long parentSegement = (segmentId - 1);
		if (null == accounts || accounts.size() == 0)
			accounts = new ArrayList<Account>();
		if (null != account)
			accounts.add(account);
		if (null != accounts && accounts.size() > 0) {
			codes = new ArrayList<Long>();
			for (Account coa : accounts) {
				if ((long) coa.getSegment().getSegmentId() == segmentId)
					codes.add(Long.parseLong(coa.getCode()));
				else if (parentSegement > 0
						&& coa.getSegment().getSegmentId() == parentSegement)
					parentCodes.add(Long.parseLong(coa.getCode()));
			}
			Collections.sort(codes, new Comparator<Long>() {
				public int compare(Long o1, Long o2) {
					return o2.compareTo(o1);
				}
			});
			Collections.sort(parentCodes, new Comparator<Long>() {
				public int compare(Long o1, Long o2) {
					return o2.compareTo(o1);
				}
			});
			if (null != codes && codes.size() > 0)
				systemAccountCode = String.valueOf(codes.get(0) + 1);
		} else {
			if (parentSegement > 0) {
				Account parentAccount = accountService.getLastUpdatedAccount(
						getImplementation(), parentSegement);
				if (null != parentAccount)
					systemAccountCode = parentAccount.getCode().concat("01");
			}
		}
		return systemAccountCode;
	}

	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public int getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(int segmentId) {
		this.segmentId = segmentId;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public SegmentBL getSegmentBL() {
		return segmentBL;
	}

	public void setSegmentBL(SegmentBL segmentBL) {
		this.segmentBL = segmentBL;
	}
}
