package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AssetRevaluationVO;
import com.aiotech.aios.accounts.service.AssetRevaluationService;
import com.aiotech.aios.common.to.Constants.Accounts.RevalautionSource;
import com.aiotech.aios.common.to.Constants.Accounts.RevaluationMethod;
import com.aiotech.aios.common.to.Constants.Accounts.RevaluationType;
import com.aiotech.aios.common.to.Constants.Accounts.SourceExternalDetail;
import com.aiotech.aios.common.to.Constants.Accounts.SourceInternalDetail;
import com.aiotech.aios.common.to.Constants.Accounts.ValuationType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class AssetRevaluationBL {

	// Dependencies
	private AssetCreationBL assetCreationBL;
	private TransactionBL transactionBL;
	private AssetRevaluationService assetRevaluationService;

	// Show all Asset Reevaluation
	public List<Object> showAllAssetRevaluation() throws Exception {
		List<AssetRevaluation> assetRevaluations = assetRevaluationService
				.getAllAssetRevaluation(getImplementation());
		List<Object> assetDisposalVOs = new ArrayList<Object>();
		if (null != assetRevaluations && assetRevaluations.size() > 0) {
			for (AssetRevaluation assetRevaluation : assetRevaluations)
				assetDisposalVOs.add(addAssetRevaluationVO(assetRevaluation));
		}
		return assetDisposalVOs;
	}

	// Save Asset Reevaluation
	public void saveAssetRevaluation(AssetRevaluation assetRevaluation,
			Double revaluationReserve) throws Exception {
		boolean updateFlag = false;
		if (null != assetRevaluation.getAssetRevaluationId())
			updateFlag = true;
		if (updateFlag) {
			transactionBL.deleteTransaction(
					assetRevaluation.getAssetRevaluationId(),
					AssetRevaluation.class.getSimpleName());
		} else
			SystemBL.saveReferenceStamp(AssetRevaluation.class.getName(),
					getImplementation());
		revaluationReserve = null != revaluationReserve ? revaluationReserve
				: 0;
		AssetCreation assetCreation = assetCreationBL.getAssetCreationService()
				.getSimpleAssetInformation(
						assetRevaluation.getAssetCreation()
								.getAssetCreationId());
		Transaction transaction = createTransaction(assetRevaluation,
				assetCreation);
		if ((assetRevaluation.getFairMarketValue() - assetRevaluation
				.getAssetBookValue()) > 0)
			transaction
					.setTransactionDetails(revaluationSurplusRevenueTransaction(
							assetRevaluation, assetCreation, revaluationReserve));
		else
			transaction.setTransactionDetails(surplusExpenseEquityTransaction(
					assetRevaluation, assetCreation, revaluationReserve));
		transactionBL.saveTransaction(
				transaction,
				new ArrayList<TransactionDetail>(transaction
						.getTransactionDetails()));
		assetRevaluationService.saveAssetRevaluation(assetRevaluation);
	}

	public void deleteRevaluation(AssetRevaluation assetRevaluation)
			throws Exception {
		transactionBL.deleteTransaction(
				assetRevaluation.getAssetRevaluationId(),
				AssetRevaluation.class.getSimpleName());
		assetRevaluationService.deleteRevaluation(assetRevaluation);
	}

	// Create Transaction
	private Transaction createTransaction(AssetRevaluation assetRevaluation,
			AssetCreation assetCreation) throws Exception {
		Calendar revaluationDate = Calendar.getInstance();
		revaluationDate.setTime(assetRevaluation.getRevaluationDate());
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCurrency().getCurrencyId(),
				"Reevaluation for Asset "
						.concat(assetCreation.getAssetName() + "["
								+ assetCreation.getAssetNumber() + "]")
						.concat(" On ")
						.concat(DateFormat
								.convertSystemDateToString(revaluationDate
										.getTime())), assetRevaluation
						.getRevaluationDate(), transactionBL
						.getCategory("ASSET REVALUATION"), assetRevaluation
						.getAssetRevaluationId(), AssetRevaluation.class
						.getSimpleName());
		return transaction;
	}

	// Create Surplus Expense Compound Transaction Details
	private Set<TransactionDetail> surplusExpenseEquityTransaction(
			AssetRevaluation assetRevaluation, AssetCreation assetCreation,
			Double revaluationReserve) throws Exception {
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		if (revaluationReserve <= 0) {
			transactionDetails.add(surplusExpenseTransaction(
					Math.abs(assetRevaluation.getAssetBookValue()
							- assetRevaluation.getFairMarketValue()),
					getImplementation().getExpenseAccount(), true));
			transactionDetails.add(surplusReserveEquityTransaction(
					Math.abs(assetRevaluation.getAssetBookValue()
							- assetRevaluation.getFairMarketValue()),
					getImplementation().getOwnersEquity(), false));
		} else {
			if ((assetRevaluation.getAssetBookValue() - assetRevaluation
					.getFairMarketValue()) >= revaluationReserve) {
				transactionDetails
						.add(surplusExpenseTransaction(
								Math.abs((assetRevaluation.getAssetBookValue() - assetRevaluation
										.getFairMarketValue())
										- revaluationReserve),
								getImplementation().getExpenseAccount(), true));
				transactionDetails.add(surplusRevenueTransaction(
						revaluationReserve, getImplementation()
								.getRevenueAccount(), true));
				transactionDetails
						.add(surplusReserveEquityTransaction(
								Math.abs((assetRevaluation.getAssetBookValue() - assetRevaluation
										.getFairMarketValue())
										+ revaluationReserve),
								getImplementation().getOwnersEquity(), false));
			} else {
				transactionDetails
						.add(surplusRevenueTransaction(
								Math.abs((assetRevaluation.getAssetBookValue() - assetRevaluation
										.getFairMarketValue())
										- revaluationReserve),
								getImplementation().getRevenueAccount(), true));
				transactionDetails
						.add(surplusReserveEquityTransaction(
								Math.abs((assetRevaluation.getAssetBookValue() - assetRevaluation
										.getFairMarketValue())
										- revaluationReserve),
								getImplementation().getOwnersEquity(), false));
			}
		}

		return transactionDetails;
	}

	// Create Surplus Revenue Compound Transaction Details
	private Set<TransactionDetail> revaluationSurplusRevenueTransaction(
			AssetRevaluation assetRevaluation, AssetCreation assetCreation,
			Double revaluationReserve) throws Exception {
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		if (revaluationReserve >= 0) {
			transactionDetails.add(surplusReserveEquityTransaction(
					Math.abs(assetRevaluation.getFairMarketValue()
							- assetRevaluation.getAssetBookValue()),
					getImplementation().getOwnersEquity(), true));
			transactionDetails.add(surplusRevenueTransaction(
					Math.abs(assetRevaluation.getFairMarketValue()
							- assetRevaluation.getAssetBookValue()),
					getImplementation().getRevenueAccount(), false));
		} else {
			if ((assetRevaluation.getFairMarketValue() - assetRevaluation
					.getAssetBookValue()) >= (Math.abs(revaluationReserve))) {
				transactionDetails.add(surplusReserveEquityTransaction(
						Math.abs(assetRevaluation.getFairMarketValue()
								- assetRevaluation.getAssetBookValue()
								+ Math.abs(revaluationReserve)),
						getImplementation().getOwnersEquity(), true));
				transactionDetails.add(surplusExpenseTransaction(Math
						.abs(revaluationReserve), getImplementation()
						.getExpenseAccount(), false));
				transactionDetails
						.add(surplusRevenueTransaction(
								Math.abs((assetRevaluation.getFairMarketValue() - assetRevaluation
										.getAssetBookValue())
										- Math.abs(revaluationReserve)),
								getImplementation().getOwnersEquity(), false));
			} else {
				transactionDetails
						.add(surplusReserveEquityTransaction(
								Math.abs((assetRevaluation.getFairMarketValue() - assetRevaluation
										.getAssetBookValue())),
								getImplementation().getOwnersEquity(), true));
				transactionDetails
						.add(surplusExpenseTransaction(
								Math.abs((assetRevaluation.getFairMarketValue() - assetRevaluation
										.getAssetBookValue())),
								getImplementation().getExpenseAccount(), false));
			}
		}
		return transactionDetails;
	}

	// Create Surplus Expense Transaction Detail
	private TransactionDetail surplusExpenseTransaction(double surplusExpense,
			long combinationId, boolean debitFlag) throws Exception {
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(surplusExpense, combinationId,
						debitFlag, null, null, null);
		return transactionDetail;
	}

	// Create Surplus Expense Transaction Detail
	private TransactionDetail surplusReserveEquityTransaction(
			double reserveEquity, long combinationId, boolean creditFlag)
			throws Exception {
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(reserveEquity, combinationId,
						creditFlag, null, null, null);
		return transactionDetail;
	}

	// Create Surplus Revenue Transaction Detail
	private TransactionDetail surplusRevenueTransaction(double surplusRevenue,
			long combinationId, boolean debitFlag) throws Exception {
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(surplusRevenue, combinationId,
						debitFlag, null, null, null);
		return transactionDetail;
	}

	// Add AssetRevaluationVO
	private AssetRevaluationVO addAssetRevaluationVO(
			AssetRevaluation assetRevaluation) {
		AssetRevaluationVO assetRevaluationVO = new AssetRevaluationVO();
		assetRevaluationVO.setAssetRevaluationId(assetRevaluation
				.getAssetRevaluationId());
		assetRevaluationVO.setReferenceNumber(assetRevaluation
				.getReferenceNumber());
		assetRevaluationVO.setDate(DateFormat
				.convertDateToString(assetRevaluation.getRevaluationDate()
						.toString()));
		assetRevaluationVO.setValuationTypeName(ValuationType.get(
				assetRevaluation.getValuationType()).name());
		assetRevaluationVO.setRevaluationTypeName(RevaluationType.get(
				assetRevaluation.getRevaluationType()).name());
		assetRevaluationVO
				.setMethodName(null != assetRevaluation.getRevaluationMethod()
						&& (byte) assetRevaluation.getRevaluationMethod() > (byte) 0 ? RevaluationMethod
						.get(assetRevaluation.getRevaluationMethod()).name()
						: "");
		assetRevaluationVO.setSourceName(RevalautionSource
				.get(assetRevaluation.getSource()).name().replaceAll("_", " "));
		assetRevaluationVO
				.setSourceDetailName((assetRevaluation.getSource()
						.equals(RevalautionSource.Internal.getCode())) ? (SourceInternalDetail
						.get(assetRevaluation.getSourceDetail()).name()
						.replaceAll("_", " ")) : (SourceExternalDetail.get(
						assetRevaluation.getSourceDetail()).name().replaceAll(
						"_", " ")));
		assetRevaluationVO.setAssetName(assetRevaluation.getAssetCreation()
				.getAssetName());
		return assetRevaluationVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetRevaluationService getAssetRevaluationService() {
		return assetRevaluationService;
	}

	public void setAssetRevaluationService(
			AssetRevaluationService assetRevaluationService) {
		this.assetRevaluationService = assetRevaluationService;
	}

	public AssetCreationBL getAssetCreationBL() {
		return assetCreationBL;
	}

	public void setAssetCreationBL(AssetCreationBL assetCreationBL) {
		this.assetCreationBL = assetCreationBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}
}
