package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PurchaseService {

	private AIOTechGenericDAO<Purchase> purchaseDAO = null;
	private AIOTechGenericDAO<PurchaseDetail> purchaseDetailDAO = null;

	public List<PurchaseDetail> getPurchaseDetailByProductId(Long productId) {

		return purchaseDetailDAO.findByNamedQuery(
				"getPurchaseDetailByProductId", productId);
	}

	public List<Purchase> getPurchaseOrders(Implementation implementation)
			throws Exception {
		return purchaseDAO.findByNamedQuery("getAllPurchaseOrder",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Purchase> getActivePurchaseOrder(Implementation implementation)
			throws Exception {
		return purchaseDAO.findByNamedQuery("getActivePurchaseOrder",
				implementation, new Date());
	}

	public List<Purchase> getUnReceivePurchaseOrder(
			Implementation implementation) throws Exception {
		return purchaseDAO.findByNamedQuery("getUnReceivePurchaseOrder",
				implementation, new Date());
	}

	public List<PurchaseDetail> getPurchaseProducts(long productId)
			throws Exception {
		return purchaseDetailDAO.findByNamedQuery("getPurchaseProducts",
				productId);
	}

	public List<Purchase> getSupplierActivePurchaseOrder(Long supplierId)
			throws Exception {
		List<Purchase> purchaseList = purchaseDAO.findByNamedQuery(
				"supplierActivePO", new Date(), supplierId,
				(byte) WorkflowConstants.Status.Published.getCode());
		return purchaseList;
	}

	public List<Purchase> getSupplierActivePurchaseOrderWithReceive(
			Long receiveId) throws Exception {
		List<Purchase> purchaseList = purchaseDAO.findByNamedQuery(
				"supplierActiveWithCurrentReceive", receiveId);
		return purchaseList;
	}

	public List<Purchase> supplierPOByOtherReceives(Long receiveId,
			List<Purchase> purchases) throws Exception {
		/*
		 * List<Purchase> purchaseList = purchaseDAO.findByNamedQuery(
		 * "supplierPOByOtherReceives",receiveId);
		 */
		Map<Long, Purchase> map = new HashMap<Long, Purchase>();
		for (Purchase purchase : purchases) {
			map.put(purchase.getPurchaseId(), purchase);
		}
		List<Purchase> purchaseList = purchaseDAO.findByNamedQuery(
				"supplierPOByOtherReceives", receiveId);

		purchases = new ArrayList<Purchase>();
		for (Purchase purchase : purchaseList) {
			if (map.containsKey(purchase.getPurchaseId()))
				purchases.add(purchase);
		}

		return purchases;
	}

	// Save PO
	public void savePurchaseOrder(Purchase purchase,
			List<PurchaseDetail> purchaseDetails) throws Exception {
		purchaseDAO.saveOrUpdate(purchase);
		for (PurchaseDetail list : purchaseDetails) {
			list.setPurchase(purchase);
		}
		purchaseDetailDAO.saveOrUpdateAll(purchaseDetails);
	}

	public void savePurchaseOrder(List<Purchase> purchaseList) throws Exception {
		purchaseDAO.saveOrUpdateAll(purchaseList);
	}

	public void mergePurchaseOrder(List<Purchase> purchaseList)
			throws Exception {
		for (Purchase purchase : purchaseList) {
			purchaseDAO.merge(purchase);
		}
	}

	public Purchase getPurchaseOrderById(Long purchaseId) throws Exception {
		return purchaseDAO.findById(purchaseId);
	}

	public List<Purchase> getPurchaseReceiveOrder(Implementation implementation)
			throws Exception {
		return purchaseDAO.findByNamedQuery("getPurchaseReceiveOrder",
				implementation, new Date());
	}

	@SuppressWarnings("unchecked")
	public List<Purchase> getReceivePurchaseOrders(Set<Long> purchaseIds)
			throws Exception {
		Criteria criteria = purchaseDAO.createCriteria();
		criteria.createAlias("purchaseDetails", "pd", Criteria.LEFT_JOIN);
		criteria.createAlias("pd.productPackageDetail", "prpackdetil",
				Criteria.LEFT_JOIN);
		criteria.createAlias("pd.goodsReturnDetails", "pdgrd",
				Criteria.LEFT_JOIN);
		criteria.createAlias("pd.receiveDetails", "pdrcv", Criteria.LEFT_JOIN);
		criteria.createAlias("pdrcv.productPackageDetail", "packdetil",
				Criteria.LEFT_JOIN);
		criteria.createAlias("pdrcv.receive", "rcv", Criteria.LEFT_JOIN);
		criteria.createAlias("requisition", "req", Criteria.LEFT_JOIN);
		criteria.createAlias("pdrcv.goodsReturnDetails", "grd",
				Criteria.LEFT_JOIN);
		criteria.createAlias("supplier", "s", Criteria.LEFT_JOIN);
		criteria.createAlias("s.person", "pr", Criteria.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "cpl", Criteria.LEFT_JOIN);
		criteria.createAlias("cpl.company", "cy", Criteria.LEFT_JOIN);
		criteria.createAlias("s.company", "cpy", Criteria.LEFT_JOIN);
		criteria.createAlias("currency", "cc", Criteria.LEFT_JOIN);
		criteria.createAlias("cc.currencyPool", "ccp", Criteria.LEFT_JOIN);
		criteria.createAlias("pd.product", "prd", Criteria.LEFT_JOIN);
		criteria.createAlias("prd.lookupDetailByProductUnit", "pu",
				Criteria.LEFT_JOIN);
		criteria.createAlias("prd.shelf", "slf", Criteria.LEFT_JOIN);
		criteria.createAlias("slf.shelf", "rack", Criteria.LEFT_JOIN);
		criteria.createAlias("slf.aisle", "asl", Criteria.LEFT_JOIN);
		criteria.createAlias("asl.store", "str", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.in("purchaseId", purchaseIds));
		Set<Purchase> uniqueRecords = new HashSet<Purchase>(criteria.list());
		List<Purchase> poList = new ArrayList<Purchase>(uniqueRecords);
		return poList;
	}

	public List<Purchase> gePurchaseSuppierComparision(Set<Long> productIds,
			Set<Long> supplierIds, String fromDate, String toDate)
			throws Exception {
		Criteria criteria = purchaseDAO.createCriteria();
		criteria.createAlias("purchaseDetails", "pd", Criteria.LEFT_JOIN);
		criteria.createAlias("supplier", "s", Criteria.LEFT_JOIN);
		criteria.createAlias("s.person", "pr", Criteria.LEFT_JOIN);
		criteria.createAlias("s.cmpDeptLocation", "cpl", Criteria.LEFT_JOIN);
		criteria.createAlias("cpl.company", "cy", Criteria.LEFT_JOIN);
		criteria.createAlias("s.company", "cpy", Criteria.LEFT_JOIN);
		criteria.createAlias("pd.product", "prd", Criteria.LEFT_JOIN);
		if (null != supplierIds && supplierIds.size() > 0)
			criteria.add(Restrictions.in("s.supplierId", supplierIds));
		if (null != productIds && productIds.size() > 0)
			criteria.add(Restrictions.in("prd.productId", productIds));
		if (null != fromDate && !("").equals(fromDate))
			criteria.add(Restrictions.ge("date",
					DateFormat.convertStringToDate(fromDate)));
		if (null != toDate && !("").equals(toDate))
			criteria.add(Restrictions.le("date",
					DateFormat.convertStringToDate(toDate)));
		@SuppressWarnings("unchecked")
		Set<Purchase> uniqueRecords = new HashSet<Purchase>(criteria.list());
		List<Purchase> rts = new ArrayList<Purchase>(uniqueRecords);
		return rts;
	}

	@SuppressWarnings("unchecked")
	public List<Purchase> getPurchaseOrdersByDifferentValues(Date purchaseDate,
			Date expiryDate, Integer status, Boolean isReceived,
			Long supplierId, Implementation implementation) throws Exception {

		Criteria purchaseCriteria = purchaseDAO.createCriteria();

		if (purchaseDate != null && expiryDate != null) {
			purchaseCriteria.add(Restrictions.conjunction()
					.add(Restrictions.ge("date", purchaseDate))
					.add(Restrictions.lt("expiryDate", expiryDate)));
		}

		if (status != null) {
			purchaseCriteria.add(Restrictions.eq("status", status));
		}

		if (isReceived != null) {
			/* purchaseCriteria.setFetchMode("receives", FetchMode.JOIN); */

			if (isReceived) {
				purchaseCriteria.createCriteria("receives");
			}

		}
		if (supplierId != null) {
			purchaseCriteria.createCriteria("supplier").add(
					Restrictions.eq("supplierId", supplierId));
		}
		if (implementation != null) {
			purchaseCriteria.createCriteria("implementation").add(
					Restrictions.eq("implementationId",
							implementation.getImplementationId()));
		}

		return purchaseCriteria.list();

	}

	@SuppressWarnings("deprecation")
	public Purchase getPurchaseByIdWithDifferentChilds(Long purchaseId,
			Boolean isCurrency, Boolean isPaymentTerm, Boolean isQuotation,
			Boolean isSupplier, Boolean isReceives,
			Boolean isGoodsReturnsDetails, Boolean isGoodsReturns,
			Boolean isPurchaseDetails, Boolean isReceiveDetails) {

		Purchase purchase = null;

		Criteria purchaseCriteria = purchaseDAO.createCriteria();

		if (isCurrency) {
			purchaseCriteria.createCriteria("currency").setFetchMode(
					"currencyPool", FetchMode.EAGER);
		}
		if (isPaymentTerm) {
			purchaseCriteria.setFetchMode("paymentTerm", FetchMode.EAGER);
		}
		if (isQuotation) {
			purchaseCriteria.setFetchMode("quotation", FetchMode.EAGER);
		}
		if (isSupplier) {
			purchaseCriteria.createCriteria("supplier").setFetchMode(
					"supplier.person", FetchMode.EAGER);
		}
		if (isReceives) {
			purchaseCriteria.setFetchMode("receives", FetchMode.EAGER);
		}
		if (isGoodsReturns) {
			purchaseCriteria.setFetchMode("goodsReturns", FetchMode.EAGER);
		}
		if (isGoodsReturnsDetails) {
			purchaseCriteria.setFetchMode("goodsReturnsDetails",
					FetchMode.EAGER);
		}
		if (isPurchaseDetails) {
			purchaseCriteria
					.setFetchMode("purchaseDetails", FetchMode.EAGER)
					.setFetchMode("purchaseDetails.product", FetchMode.EAGER)
					.setFetchMode(
							"purchaseDetails.product.lookupDetailByProductUnit",
							FetchMode.EAGER);
		}
		if (isReceiveDetails) {
			purchaseCriteria.setFetchMode("receiveDetails", FetchMode.EAGER);
		}

		purchaseCriteria.add(Restrictions.eq("purchaseId", purchaseId));

		if (purchaseCriteria.list().size() > 0) {
			purchase = (Purchase) purchaseCriteria.list().get(0);
		}

		return purchase;

	}

	public Purchase getPurchaseReceiveOrderById(Long purchaseId)
			throws Exception {
		return purchaseDAO.findByNamedQuery("getPurchaseReceiveOrderById",
				purchaseId).get(0);
	}

	public Purchase purchaseOrderActive(Long purchaseId) throws Exception {
		return purchaseDAO.findByNamedQuery("purchaseOrderActive", purchaseId)
				.get(0);
	}

	public Purchase getPurchaseOrder(Long purchaseId) throws Exception {
		return purchaseDAO.findByNamedQuery("getPurchaseOrder", purchaseId)
				.get(0);
	}

	public Purchase getPurchaseOrderDetailByPurchaseId(Long purchaseId)
			throws Exception {
		return purchaseDAO.findByNamedQuery(
				"getPurchaseOrderDetailByPurchaseId", purchaseId).get(0);
	}

	public Purchase getPurchaseOrderSupplier(long purchaseId) throws Exception {
		return purchaseDAO.findByNamedQuery("getPurchaseOrderSupplier",
				purchaseId).get(0);
	}

	public Purchase getPurchaseOrderPrint(Long purchaseId) throws Exception {
		return purchaseDAO
				.findByNamedQuery("getPurchaseOrderPrint", purchaseId).get(0);
	}

	public List<PurchaseDetail> getPurchaseDetailOrderById(Long purchaseId)
			throws Exception {
		return purchaseDetailDAO.findByNamedQuery("getPurchaseDetailOrderById",
				purchaseId);
	}

	public void deletePurchaseOrder(Purchase purchase,
			List<PurchaseDetail> detailList) throws Exception {
		purchaseDetailDAO.deleteAll(detailList);
		purchaseDAO.delete(purchase);
	}

	@SuppressWarnings("unchecked")
	public List<Purchase> getPurchaseOrdersByFilters(PurchaseVO vo)
			throws Exception {

		Criteria purchaseCriteria = purchaseDAO.createCriteria();
		purchaseCriteria.createAlias("supplier", "sup",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("requisition", "req",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("req.cmpDeptLoc", "cmdDept",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("cmdDept.department", "dept",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("personByCreatedBy", "crePer",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("purchaseDetails", "purDeta",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("purDeta.product", "prod",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("prod.lookupDetailByProductUnit", "unit",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("quotation", "quot",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("receiveDetails", "receDet",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("receDet.receive", "rece",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("sup.person", "supPer",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("sup.company", "supComp",
				CriteriaSpecification.LEFT_JOIN);
		purchaseCriteria.createAlias("implementation", "imp",
				CriteriaSpecification.LEFT_JOIN);

		if (vo.getDate() != null && vo.getExpiryDate() != null) {
			purchaseCriteria.add(Restrictions.conjunction()
					.add(Restrictions.ge("date", vo.getDate()))
					.add(Restrictions.lt("date", vo.getExpiryDate())));
		}

		if (vo.getStatus() != null) {
			if (vo.getStatus() == 1)
				purchaseCriteria.add(Restrictions.isNotNull("receiveDetails"));
			else
				purchaseCriteria.add(Restrictions.isNull("receiveDetails"));
		}

		if (vo.getSupplierId() != null) {
			purchaseCriteria.add(Restrictions.eq("sup.supplierId",
					vo.getSupplierId()));
		}

		if (vo.getProductId() != null) {
			purchaseCriteria.add(Restrictions.eq("prod.productId",
					vo.getProductId()));
		}

		if (vo.getDepartmentId() != null) {
			purchaseCriteria.add(Restrictions.eq("dept.departmentId",
					vo.getDepartmentId()));
		}

		if (vo.getImplementationId() != null) {

			purchaseCriteria.add(Restrictions.eq("imp.implementationId",
					vo.getImplementationId()));
		}
		Set<Purchase> uniqueRecords = new HashSet<Purchase>(
				purchaseCriteria.list());
		List<Purchase> list = new ArrayList<Purchase>(uniqueRecords);

		return list;

	}

	// Get Purchase order basic info
	public Purchase getPurchaseOrderBasicById(long purchaseId) {
		return purchaseDAO.findByNamedQuery("getPurchaseOrderBasicById",
				purchaseId).get(0);
	}

	public Purchase getPurchaseAndCurrencyById(long purchaseId) {
		return purchaseDAO.findByNamedQuery("getPurchaseAndCurrencyById",
				purchaseId).get(0);
	}

	public void deletePurchaseDetail(List<PurchaseDetail> detailList)
			throws Exception {
		this.getPurchaseDetailDAO().deleteAll(detailList);
	}

	public AIOTechGenericDAO<Purchase> getPurchaseDAO() {
		return purchaseDAO;
	}

	public void setPurchaseDAO(AIOTechGenericDAO<Purchase> purchaseDAO) {
		this.purchaseDAO = purchaseDAO;
	}

	public AIOTechGenericDAO<PurchaseDetail> getPurchaseDetailDAO() {
		return purchaseDetailDAO;
	}

	public void setPurchaseDetailDAO(
			AIOTechGenericDAO<PurchaseDetail> purchaseDetailDAO) {
		this.purchaseDetailDAO = purchaseDetailDAO;
	}
}
