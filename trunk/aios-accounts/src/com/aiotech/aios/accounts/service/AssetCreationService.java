package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetCreationService {

	private AIOTechGenericDAO<AssetCreation> assetCreationDAO;
	private AIOTechGenericDAO<AssetDetail> assetDetailDAO;

	/**
	 * Get All Assets
	 * 
	 * @param implementation
	 * @return
	 * @throws Exception
	 */
	public List<AssetCreation> getAllAssets(Implementation implementation)
			throws Exception {
		return assetCreationDAO.findByNamedQuery("getAllAssets",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<AssetCreation> getAssetCheckOuts(Implementation implementation)
			throws Exception {
		return assetCreationDAO.findByNamedQuery("getAssetCheckOuts",
				implementation);
	}

	/**
	 * Get All Assets
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<AssetCreation> getAssetsWithReceiveDetails() throws Exception {
		return assetCreationDAO.findByNamedQuery("getAssetsWithReceiveDetails");
	}

	/**
	 * Get basic asset info
	 * 
	 * @param implementation
	 * @return
	 * @throws Exception
	 */
	public List<AssetCreation> getSimpleAssetInfo(Implementation implementation)
			throws Exception {
		return assetCreationDAO.findByNamedQuery("getSimpleAssetInfo",
				implementation);
	}

	/**
	 * Get Simple Asset Creation info
	 * 
	 * @param assetCreationId
	 * @return
	 */
	public AssetCreation getSimpleAssetInformation(long assetCreationId)
			throws Exception {
		return assetCreationDAO.findById(assetCreationId);
	}

	/**
	 * Get Asset Creation info
	 * 
	 * @param assetCreationId
	 * @return
	 */
	public AssetCreation getAssetDetailInfo(long assetCreationId)
			throws Exception {
		return assetCreationDAO.findByNamedQuery("getAssetDetailInfo",
				assetCreationId).get(0);
	}

	/**
	 * Get Asset Creation info
	 * 
	 * @param assetCreationId
	 * @return
	 */
	public AssetCreation getAssetInformation(long assetCreationId)
			throws Exception {
		return assetCreationDAO.findByNamedQuery("getAssetInformation",
				assetCreationId).get(0);
	}

	/**
	 * Get Asset Creation Detailed info
	 * 
	 * @param assetCreationId
	 * @return
	 */
	public AssetCreation getAssetDetailedInformation(long assetCreationId)
			throws Exception {
		return assetCreationDAO.findByNamedQuery("getAssetDetailedInformation",
				assetCreationId).get(0);
	}

	/**
	 * Get Asset Detail by Asset Creation id
	 * 
	 * @param assetCreationId
	 * @return
	 * @throws Exception
	 */
	public List<AssetDetail> getAssetDetailByAssetCreationId(
			long assetCreationId) throws Exception {
		return assetDetailDAO.findByNamedQuery(
				"getAssetDetailByAssetCreationId", assetCreationId);
	}

	/**
	 * Save Asset Creation
	 * 
	 * @param assetCreation
	 * @param assetDetails
	 * @param deleteAssetDetails
	 * @throws Exception
	 */
	public void saveAssetCreation(AssetCreation assetCreation,
			List<AssetDetail> assetDetails, List<AssetDetail> deleteAssetDetails)
			throws Exception {
		assetCreationDAO.saveOrUpdate(assetCreation);
		if (null != deleteAssetDetails && deleteAssetDetails.size() > 0) {
			assetDetailDAO.deleteAll(deleteAssetDetails);
		}
		if (null != assetDetails && assetDetails.size() > 0) {
			for (AssetDetail assetDetail : assetDetails) {
				assetDetail.setAssetCreation(assetCreation);
			}
			assetDetailDAO.saveOrUpdateAll(assetDetails);
		}
	}

	/**
	 * Delete Asset Creation
	 * 
	 * @param assetCreation
	 * @param arrayDetails
	 */
	public void deleteAssetCreation(AssetCreation assetCreation,
			List<AssetDetail> assetDetails) {
		if (null != assetDetails && assetDetails.size() > 0) {
			assetDetailDAO.deleteAll(assetDetails);
		}
		assetCreationDAO.delete(assetCreation);
	}

	public AssetCreation getAssetInfoByProduct(Long productId) throws Exception {
		List<AssetCreation> assets = assetCreationDAO.findByNamedQuery(
				"getAssetInfoByProduct", productId);
		if (assets != null && assets.size() > 0)
			return assets.get(0);
		else
			return null;
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetDetail> getAssetDetailDAO() {
		return assetDetailDAO;
	}

	public void setAssetDetailDAO(AIOTechGenericDAO<AssetDetail> assetDetailDAO) {
		this.assetDetailDAO = assetDetailDAO;
	}

	public AIOTechGenericDAO<AssetCreation> getAssetCreationDAO() {
		return assetCreationDAO;
	}

	public void setAssetCreationDAO(
			AIOTechGenericDAO<AssetCreation> assetCreationDAO) {
		this.assetCreationDAO = assetCreationDAO;
	}

}
