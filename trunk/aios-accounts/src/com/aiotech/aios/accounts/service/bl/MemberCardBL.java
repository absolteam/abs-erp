package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.MemberCard;
import com.aiotech.aios.accounts.domain.entity.vo.MemberCardVO;
import com.aiotech.aios.accounts.service.MemberCardService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class MemberCardBL {

	// Dependencies
	private MemberCardService memberCardService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Member Cards
	public List<Object> showAllMemberCards() throws Exception {
		List<MemberCard> memberCards = memberCardService
				.getAllMemberCards(getImplementation());
		List<Object> memberCardVOs = new ArrayList<Object>();
		if (null != memberCards && memberCards.size() > 0)
			for (MemberCard memberCard : memberCards)
				memberCardVOs.add(addMemberCardVO(memberCard));
		return memberCardVOs;
	}

	// Get Member Card Number
	public Object getMemberCardNumber(byte cardType) throws Exception {
		MemberCard memberCard = memberCardService.getMemberCardByCardType(
				getImplementation(), cardType);
		if (null != memberCard) {
			MemberCardVO memberCardVO = addMemberCardVO(memberCard);
			if (null != memberCardVO.getCurrentDigit())
				memberCardVO
						.setUpdatedDigit(memberCardVO.getCurrentDigit() + 1);
			else
				memberCardVO.setUpdatedDigit(memberCardVO.getStartDigit());
			return memberCardVO;
		}
		return null;
	}

	// Add to Member Card VO
	public MemberCardVO addMemberCardVO(MemberCard memberCard) {
		MemberCardVO memberCardVO = new MemberCardVO();
		memberCardVO.setMemberCardId(memberCard.getMemberCardId());
		memberCardVO.setStartDigit(memberCard.getStartDigit());
		memberCardVO.setEndDigit(memberCard.getEndDigit());
		memberCardVO
				.setCurrentDigit(null != memberCard.getCurrentDigit() ? memberCard
						.getCurrentDigit() : memberCard.getStartDigit());
		memberCardVO.setStatusPoints(memberCard.getStatusPoints());
		memberCardVO.setCardType(memberCard.getCardType());
		memberCardVO.setCardTypeName(Constants.Accounts.MemberCardType.get(
				memberCard.getCardType()).name());
		memberCardVO.setDescription(memberCard.getDescription());
		return memberCardVO;
	}

	// Save Member Card
	public void saveMemberCard(MemberCard memberCard) throws Exception {
		memberCard.setImplementation(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		memberCard.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (memberCard != null && memberCard.getMemberCardId() != null
				&& memberCard.getMemberCardId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		memberCardService.saveMemberCard(memberCard);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				MemberCard.class.getSimpleName(), memberCard.getMemberCardId(),
				user, workflowDetailVo);
	}

	// Merge Member Card
	public void mergeMemberCard(MemberCard memberCard) throws Exception {
		memberCard.setImplementation(getImplementation());
		memberCardService.mergeMemberCard(memberCard);
	}

	public void doMemberCardBusinessUpdate(Long memberCardId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				MemberCard memberCard = this.getMemberCardService()
						.getMemberCardByCardId(memberCardId);
				memberCardService.deleteMemberCard(memberCard);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Member Card
	public void deleteMemberCard(MemberCard memberCard) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				MemberCard.class.getSimpleName(), memberCard.getMemberCardId(),
				user, workflowDetailVo); 
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public MemberCardService getMemberCardService() {
		return memberCardService;
	}

	public void setMemberCardService(MemberCardService memberCardService) {
		this.memberCardService = memberCardService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
