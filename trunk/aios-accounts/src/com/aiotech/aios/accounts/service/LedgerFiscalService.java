package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LedgerFiscalService {

	private AIOTechGenericDAO<LedgerFiscal> ledgerFiscalDAO;
	private TransactionBL transactionBL;
	
	public LedgerFiscal getLedgerFiscals(long combinationId, long periodId)
			throws Exception {
		List<LedgerFiscal> ledgerFiscals = ledgerFiscalDAO.findByNamedQuery(
				"getLedgerFiscals", combinationId, periodId);
		return null != ledgerFiscals && ledgerFiscals.size() > 0 ? ledgerFiscals
				.get(0) : null;
	}

	public List<LedgerFiscal> getLedgerFiscals(long periodId) throws Exception {
		return ledgerFiscalDAO.findByNamedQuery("getFiscalByPeriod", periodId);

	}
	
	@SuppressWarnings("unchecked")
	public List<LedgerFiscal> getLedgerFiscals(Long periodId[]) throws Exception {
		Criteria criteria = ledgerFiscalDAO.createCriteria(); 
		criteria.createAlias("period", "p", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByCompanyAccountId", "cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByCostcenterAccountId", "cost", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByNaturalAccountId", "ntl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByAnalysisAccountId", "ayl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByBuffer1AccountId", "b1", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByBuffer2AccountId", "b2", CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("p.periodId", periodId));
		
		Set<LedgerFiscal> uniqueRecord = new HashSet<LedgerFiscal>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<LedgerFiscal>(
				uniqueRecord) : null;
	} 

	public void updateLedgerFiscalBalance(LedgerFiscal ledgerFiscal)
			throws Exception {
		ledgerFiscalDAO.saveOrUpdate(ledgerFiscal);
	}

	public void updateLedgerFiscalBalance(List<LedgerFiscal> ledgerFiscals)
			throws Exception {
		ledgerFiscalDAO.saveOrUpdateAll(ledgerFiscals);
	}

	public void deleteLedgerFiscal(List<LedgerFiscal> ledgerFiscals)
			throws Exception {
		ledgerFiscalDAO.deleteAll(ledgerFiscals);
	}

	public AIOTechGenericDAO<LedgerFiscal> getLedgerFiscalDAO() {
		return ledgerFiscalDAO;
	}

	public void setLedgerFiscalDAO(
			AIOTechGenericDAO<LedgerFiscal> ledgerFiscalDAO) {
		this.ledgerFiscalDAO = ledgerFiscalDAO;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}
}
