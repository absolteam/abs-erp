package com.aiotech.aios.accounts.service.bl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.service.CategoryService;
import com.aiotech.aios.accounts.service.TransactionService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CategoryBL {

	private CategoryService categoryService;
	private TransactionService transactionService;
	private List<Category> categoryList = null;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public JSONObject getAllCategorylist(Implementation implementation)
			throws Exception {
		categoryList = categoryService.getAllCategories(implementation);
		if (null != categoryList && !categoryList.equals("")) {
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Category list : categoryList) {
				array = new JSONArray();
				array.add(list.getCategoryId());
				array.add(list.getName());
				array.add(DateFormat.convertDateToString(list.getFromDate()
						.toString()));
				if (null != list.getToDate() && !list.getToDate().equals(""))
					array.add(DateFormat.convertDateToString(list.getToDate()
							.toString()));
				else
					array.add("");
				if (list.getStatus() == 1)
					array.add("Active");
				else
					array.add("Inactive");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			return jsonResponse;
		} else {
			return null;
		}
	}

	public void saveCatgory(Category category) {

		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			category.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (category != null && category.getCategoryId() != null) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			categoryService.saveCategory(category);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Category.class.getSimpleName(), category.getCategoryId(),
					user, workflowDetailVo);
		} catch (Exception e) {

		}

	}

	public void doCategoryBusinessUpdate(Long categoryId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				Category category = categoryService.getCategoryById(categoryId);
				categoryService.deleteCategory(category);
			}
		} catch (Exception ex) {

		}
	}

	public void deleteCategory(Category categroy) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Category.class.getSimpleName(), categroy.getCategoryId(), user,
				workflowDetailVo);
	}

	public Category createCategory(Implementation implementation,
			String categoryName) throws Exception {
		Category category = new Category();
		category.setImplementation(implementation);
		category.setName(categoryName);
		category.setStatus(Byte.parseByte("1"));
		category.setFromDate(new Date());
		category.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		category.setToDate(DateFormat.addYear(category.getFromDate()));
		categoryService.saveCategory(category);
		return category;
	}

	public Category createCategory(Implementation implementation,
			String categoryName, Session session) throws Exception {
		Category category = new Category();
		category.setImplementation(implementation);
		category.setName(categoryName);
		category.setStatus(Byte.parseByte("1"));
		category.setFromDate(new Date());
		category.setToDate(DateFormat.addYear(category.getFromDate()));
		categoryService.saveCatgory(category, session);
		return category;
	}

	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
