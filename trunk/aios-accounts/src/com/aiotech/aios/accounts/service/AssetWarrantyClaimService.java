package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetWarrantyClaimService {

	private AIOTechGenericDAO<AssetWarrantyClaim> assetWarrantyClaimDAO;

	// Get all Asset Warranty Claim
	public List<AssetWarrantyClaim> getAllAssetWarrantyClaim(
			Implementation implementation) throws Exception {
		return assetWarrantyClaimDAO.findByNamedQuery(
				"getAllAssetWarrantyClaim", implementation);
	}

	// Get Asset Warranty Claim
	public AssetWarrantyClaim getAssetWarrantyClaimById(
			long assetWarrantyClaimId) throws Exception {
		return assetWarrantyClaimDAO.findByNamedQuery(
				"getAssetWarrantyClaimById", assetWarrantyClaimId).get(0);
	}

	// Get Simple Asset Warranty Claim
	public AssetWarrantyClaim getSimpleAssetWarrantyClaimById(
			long assetWarrantyClaimId) throws Exception {
		return assetWarrantyClaimDAO.findById(assetWarrantyClaimId);
	}

	// Save Asset Warranty Claim
	public void saveAssetWarrantyClaim(AssetWarrantyClaim assetWarrantyClaim)
			throws Exception {
		assetWarrantyClaimDAO.saveOrUpdate(assetWarrantyClaim);
	}

	// Delete Asset Warranty Claim
	public void deleteAssetWarrantyClaim(AssetWarrantyClaim assetWarrantyClaim)
			throws Exception {
		assetWarrantyClaimDAO.delete(assetWarrantyClaim);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetWarrantyClaim> getAssetWarrantyClaimDAO() {
		return assetWarrantyClaimDAO;
	}

	public void setAssetWarrantyClaimDAO(
			AIOTechGenericDAO<AssetWarrantyClaim> assetWarrantyClaimDAO) {
		this.assetWarrantyClaimDAO = assetWarrantyClaimDAO;
	}
}
