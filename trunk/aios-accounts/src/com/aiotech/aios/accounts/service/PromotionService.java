package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Promotion;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.PromotionOption;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PromotionService {

	private AIOTechGenericDAO<Promotion> promotionDAO;
	private AIOTechGenericDAO<PromotionOption> promotionOptionDAO;
	private AIOTechGenericDAO<PromotionMethod> promotionMethodDAO;

	// Get all promotions
	public List<Promotion> getAllPromotions(Implementation implementation)
			throws Exception {
		return promotionDAO.findByNamedQuery("getAllPromotions",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Promotion by Id
	public Promotion getPromotionbyId(long promotionId) throws Exception {
		return promotionDAO.findByNamedQuery("getPromotionbyId", promotionId)
				.get(0);
	}

	// Get Promotion by Product Id
	public Promotion getPromotionByProductId(long productId, Date toDate)
			throws Exception {
		List<Promotion> promotions = promotionDAO.findByNamedQuery(
				"getPromotionByProductId", toDate, productId);
		return null != promotions && promotions.size() > 0 ? promotions.get(0)
				: null;
	}

	// Get Promotion by Customer Id
	public Promotion getPromotionByCustomerId(long customerId, Date toDate)
			throws Exception {
		List<Promotion> promotions = promotionDAO.findByNamedQuery(
				"getPromotionByCustomerId", toDate, customerId);
		return null != promotions && promotions.size() > 0 ? promotions.get(0)
				: null;
	}

	// Get Promotion Method by Promotion Id
	public List<PromotionMethod> getPromotionMethodByPromotionId(
			long promotionId) throws Exception {
		return promotionMethodDAO.findByNamedQuery(
				"getPromotionMethodByPromotionId", promotionId);
	}

	// Get Promotion Option by Promotion Id
	public List<PromotionOption> getPromotionOptionByPromotionId(
			long promotionId) {
		return promotionOptionDAO.findByNamedQuery(
				"getPromotionOptionByPromotionId", promotionId);
	}

	// Save Promotion
	public void savePromotion(Promotion promotion,
			List<PromotionMethod> promotionMethods,
			List<PromotionOption> promotionOptions) throws Exception {
		promotionDAO.saveOrUpdate(promotion);
		if (null != promotionOptions && promotionOptions.size() > 0) {
			for (PromotionOption promotionOption : promotionOptions)
				promotionOption.setPromotion(promotion);
			promotionOptionDAO.saveOrUpdateAll(promotionOptions);
		}
		if (null != promotionMethods && promotionMethods.size() > 0) {
			for (PromotionMethod promotionMethod : promotionMethods)
				promotionMethod.setPromotion(promotion);
			promotionMethodDAO.saveOrUpdateAll(promotionMethods);
		}
	}

	// Delete Promotion Methods
	public void deletePromotionMethods(
			List<PromotionMethod> deletedPromotionDetails) throws Exception {
		promotionMethodDAO.deleteAll(deletedPromotionDetails);
	}

	// Delete Promotion Option
	public void deletePromotionOptions(
			List<PromotionOption> deletedPromotionOptions) throws Exception {
		promotionOptionDAO.deleteAll(deletedPromotionOptions);
	}

	// Delete Promotion
	public void deletePromotion(Promotion promotion) throws Exception {
		promotionDAO.delete(promotion);
	}

	@SuppressWarnings("unchecked")
	public List<Promotion> getFilteredPromotions(Implementation implementation,
			Long promotionId, Byte discountOptionId, Long productId,
			Long customerId, Long productCategoryId, Date fromDate, Date toDate)
			throws Exception {
		Criteria criteria = promotionDAO.createCriteria();

		criteria.createAlias("lookupDetail", "pt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("promotionMethods", "pm",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("promotionOptions", "po",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("po.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("po.customer", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("po.productCategory", "cat",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pm.coupon", "cpn",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (promotionId != null && promotionId > 0) {
			criteria.add(Restrictions.eq("promotionId", promotionId));
		}

		if (discountOptionId != null && discountOptionId > 0) {
			criteria.add(Restrictions.eq("promotionOption", discountOptionId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("cst.customerId", customerId));
		}

		if (productCategoryId != null && productCategoryId > 0) {
			criteria.add(Restrictions.eq("cat.productCategoryId",
					productCategoryId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("fromDate", fromDate, toDate));
		}

		Set<Promotion> uniquePromotion = new HashSet<Promotion>(criteria.list());
		List<Promotion> promotionList = new ArrayList<Promotion>(
				uniquePromotion);

		return promotionList;
	}

	// Getters & Setters
	public AIOTechGenericDAO<Promotion> getPromotionDAO() {
		return promotionDAO;
	}

	public void setPromotionDAO(AIOTechGenericDAO<Promotion> promotionDAO) {
		this.promotionDAO = promotionDAO;
	}

	public AIOTechGenericDAO<PromotionOption> getPromotionOptionDAO() {
		return promotionOptionDAO;
	}

	public void setPromotionOptionDAO(
			AIOTechGenericDAO<PromotionOption> promotionOptionDAO) {
		this.promotionOptionDAO = promotionOptionDAO;
	}

	public AIOTechGenericDAO<PromotionMethod> getPromotionMethodDAO() {
		return promotionMethodDAO;
	}

	public void setPromotionMethodDAO(
			AIOTechGenericDAO<PromotionMethod> promotionMethodDAO) {
		this.promotionMethodDAO = promotionMethodDAO;
	}
}
