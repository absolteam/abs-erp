package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductionReceive;
import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionReceiveVO;
import com.aiotech.aios.accounts.service.ProductionReceiveService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;

public class ProductionReceiveBL {

	// Dependencies
	private StoreBL storeBL;
	private StockBL stockBL;
	private ProductBL productBL;
	private TransactionBL transactionBL;
	private LookupMasterBL lookupMasterBL;
	private ProductionReceiveService productionReceiveService;

	public List<Object> showAllProductionReceive() throws Exception {
		List<ProductionReceive> productionReceives = productionReceiveService
				.getAllProductionReceive(getImplementation());
		List<Object> productionReceiveVOs = new ArrayList<Object>();
		if (null != productionReceives && productionReceives.size() > 0) {
			for (ProductionReceive productionReceive : productionReceives)
				productionReceiveVOs
						.add(addProductionReceiveVO(productionReceive));
		}
		return productionReceiveVOs;
	}

	private ProductionReceiveVO addProductionReceiveVO(
			ProductionReceive productionReceive) throws Exception {
		ProductionReceiveVO productionReceiveVO = new ProductionReceiveVO();
		productionReceiveVO.setProductionReceiveId(productionReceive
				.getProductionReceiveId());
		productionReceiveVO.setDate(DateFormat
				.convertDateToString(productionReceive.getReceiveDate()
						.toString()));
		productionReceiveVO.setReferenceNumber(productionReceive
				.getReferenceNumber());
		productionReceiveVO.setReceivePerson(productionReceive
				.getPersonByReceivePerson()
				.getFirstName()
				.concat(" ")
				.concat(productionReceive.getPersonByReceivePerson()
						.getLastName()));
		productionReceiveVO.setSource(productionReceive.getLookupDetail()
				.getDisplayName());
		return productionReceiveVO;
	}

	// Add Production Receive Detail
	public ProductionReceiveDetail addProductReceiveDetail(ProductVO productVO)
			throws Exception {
		Product product = new Product();
		Shelf shelf = new Shelf();
		ProductionReceiveDetail productReceiveDetail = new ProductionReceiveDetail();
		product.setProductId(productVO.getProductId());
		product.setCostingType(productVO.getCostingType());
		shelf.setShelfId(productVO.getShelfId());
		productReceiveDetail.setShelf(shelf);
		productReceiveDetail.setProduct(product);
		productReceiveDetail.setUnitRate(productVO.getFinalUnitPrice());
		productReceiveDetail.setQuantity(productVO.getQuantity()
				- productVO.getAvailableQuantity());
		productReceiveDetail.setShelf(shelf);
		return productReceiveDetail;
	}

	// Add Production Receive
	public void addProductReceive(Implementation implementation,
			List<ProductionReceiveDetail> productionReceiveDetails)
			throws Exception {
		ProductionReceive productionReceive = new ProductionReceive();
		productionReceive.setReceiveDate(new Date());
		List<LookupDetail> lookupDetails = lookupMasterBL
				.getActiveLookupDetails("PRODUCTION_SOURCE", true);
		productionReceive.setLookupDetail(lookupDetails.get(0));
		productionReceive.setReferenceNumber(SystemBL.getReferenceStamp(
				ProductionReceive.class.getName(), implementation));
		SystemBL.saveReferenceStamp(ProductionReceive.class.getName(),
				implementation);
		saveProductionReceive(productionReceive, productionReceiveDetails);
	} 

	// Save Production Receives
	public void saveProductionReceive(ProductionReceive productionReceive,
			List<ProductionReceiveDetail> productionReceiveDetails)
			throws Exception {
		boolean updateFlag = false;
		if (null != productionReceive.getProductionReceiveId())
			updateFlag = true;
		String transactionDescription = "Production received on "
				+ DateFormat.convertSystemDateToString(productionReceive
						.getReceiveDate()) + " for the reference of "
				+ productionReceive.getReferenceNumber();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		productionReceive.setPersonByCreatedBy(person);
		productionReceive.setCreatedDate(Calendar.getInstance().getTime());
		productionReceive.setPersonByReceivePerson(null == productionReceive
				.getPersonByReceivePerson() ? productionReceive
				.getPersonByCreatedBy() : productionReceive
				.getPersonByReceivePerson());
		productionReceive.setImplementation(getImplementation());
		productionReceiveService.saveProductionReceive(productionReceive,
				productionReceiveDetails);
		if (updateFlag)
			transactionBL.deleteTransaction(
					productionReceive.getProductionReceiveId(),
					ProductionReceive.class.getSimpleName());
		else
			SystemBL.saveReferenceStamp(ProductionReceive.class.getName(),
					getImplementation());
		// Update Stock
		List<Stock> stockDetails = new ArrayList<Stock>();
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		for (ProductionReceiveDetail receiveDetail : productionReceiveDetails) {
			Shelf rack = storeBL.getStoreService().getShelfById(
					receiveDetail.getShelf().getShelfId());
			Stock tempStock = new Stock();
			tempStock.setProduct(receiveDetail.getProduct());
			tempStock.setStore(rack.getAisle().getStore());
			tempStock.setQuantity(receiveDetail.getQuantity());
			tempStock.setImplementation(getImplementation());
			tempStock.setUnitRate(receiveDetail.getUnitRate());
			tempStock.setShelf(receiveDetail.getShelf());
 			stockDetails.add(tempStock);
			// Transaction Entries
			transactionDetails.addAll(addTransactionDetail(receiveDetail, true,
					false));
		}
		stockBL.updateStockDetails(stockDetails, null);
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), transactionDescription,
				productionReceive.getReceiveDate(), transactionBL
						.getCategory("Production Receive"), productionReceive
						.getProductionReceiveId(), ProductionReceive.class
						.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	// Delete Production Receives
	public boolean deleteProductionReceive(ProductionReceive productionReceive,
			List<ProductionReceiveDetail> productionReceiveDetails)
			throws Exception {
		transactionBL.deleteTransaction(
				productionReceive.getProductionReceiveId(),
				ProductionReceive.class.getSimpleName());
		// Delete Stock
		List<Stock> stockDetails = new ArrayList<Stock>();
		Stock stock = null;
		for (ProductionReceiveDetail receiveDetail : productionReceiveDetails) {
			stock = new Stock();
			stock.setProduct(receiveDetail.getProduct()); 
			stock.setStore(receiveDetail.getShelf().getShelf().getAisle().getStore());
			stock.setQuantity(receiveDetail.getQuantity());
			stockDetails.add(stock); 
		} 
		stockBL.addDeductStockDetails(null, stockDetails);
		productionReceiveService.deleteProductionReceive(productionReceive,
				productionReceiveDetails);
		return true;
	}

	private List<TransactionDetail> addTransactionDetail(
			ProductionReceiveDetail receiveDetail, boolean debitFlag,
			boolean creditFlag) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		transactionDetails.add(createInventoryTransaction(receiveDetail,
				debitFlag));
		transactionDetails.add(createExpenseTransaction(receiveDetail,
				creditFlag));
		return transactionDetails;
	}

	// Create Inventory Transaction Detail
	private TransactionDetail createInventoryTransaction(
			ProductionReceiveDetail receiveDetail, boolean debitFlag)
			throws Exception {
		Combination combination = productBL.getProductInventory(receiveDetail
				.getProduct().getProductId());
		return transactionBL.createTransactionDetail(
				receiveDetail.getUnitRate() * receiveDetail.getQuantity(),
				combination.getCombinationId(), debitFlag, null, null, null);
	}

	// Create Inventory Transaction Detail
	private TransactionDetail createExpenseTransaction(
			ProductionReceiveDetail receiveDetail, boolean creditFlag)
			throws Exception { 
		return transactionBL.createTransactionDetail(
				receiveDetail.getUnitRate() * receiveDetail.getQuantity(),
				getImplementation().getExpenseAccount(), creditFlag, null, null, null);
	} 

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProductionReceiveService getProductionReceiveService() {
		return productionReceiveService;
	}

	public void setProductionReceiveService(
			ProductionReceiveService productionReceiveService) {
		this.productionReceiveService = productionReceiveService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}
}
