package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.MaterialWastage;
import com.aiotech.aios.accounts.domain.entity.MaterialWastageDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialWastageVO;
import com.aiotech.aios.accounts.service.MaterialWastageService;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.WastageTypes;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class MaterialWastageBL {

	private MaterialWastageService materialWastageService;
	private LookupMasterBL lookupMasterBL;
	private StockBL stockBL;
	

	public List<Object> getAllMaterialWastages() throws Exception {
		List<MaterialWastage> materialWastages = materialWastageService
				.getAllMaterialWastages(getImplementation());
		List<Object> materialWastageVOs = new ArrayList<Object>();
		if (null != materialWastages && materialWastages.size() > 0) {
			for (MaterialWastage materialWastage : materialWastages)
				materialWastageVOs.add(addMaterialWastage(materialWastage));
		}
		return materialWastageVOs;
	}

	public void saveMaterialWastage(MaterialWastage materialWastage,
			List<MaterialWastageDetail> materialWastageDetails,
			List<MaterialWastageDetail> deletedWastageDetails,
			List<MaterialWastageDetail> existingWastageDetails)
			throws Exception {
		materialWastage.setImplementation(getImplementation());
		if (null == materialWastage.getMaterialWastageId())
			SystemBL.saveReferenceStamp(MaterialWastage.class.getName(),
					getImplementation());
		if (null != deletedWastageDetails && deletedWastageDetails.size() > 0) {
			materialWastageService
					.deleteMaterialWastageDetail(deletedWastageDetails);
		}
		materialWastageService.saveMaterialWastage(materialWastage,
				materialWastageDetails);
		if ((byte) materialWastage.getWastageType() == (byte) WastageTypes.General_Wastage
				.getCode()) {
			stockBL.updateBatchStockDetails(getStockDetails(existingWastageDetails));
			stockBL.deleteBatchStockDetails(getStockDetails(materialWastageDetails));
		}
	}

	public void updateMaterialWastageDetails(
			List<MaterialWastageDetail> wastageDetails) throws Exception {
		materialWastageService.updateMaterialWastageDetails(wastageDetails);
	}

	public void deleteMaterialWastage(MaterialWastage materialWastage,
			List<MaterialWastageDetail> materialWastageDetails)
			throws Exception {
		if ((byte) materialWastage.getWastageType() == (byte) WastageTypes.General_Wastage
				.getCode()) {
			stockBL.updateStockDetails(getStockDetails(materialWastageDetails),
					null);
		}
		materialWastageService.deleteMaterialWastage(materialWastage,
				materialWastageDetails);
	}

	private List<Stock> getStockDetails(
			List<MaterialWastageDetail> wastageDetails) throws Exception{
		List<Stock> stocks = null;
		if (null != wastageDetails && wastageDetails.size() > 0) {
			Stock stock = null;
			Shelf shelf = null;
			stocks = new ArrayList<Stock>();
			Product product = null;
			for (MaterialWastageDetail wastageDetail : wastageDetails) {
				product = stockBL.getProductPricingBL().getProductBL()
						.getProductService()
						.getBasicProductById(wastageDetail.getProduct().getProductId());
				if (null != product
						&& ((byte) InventorySubCategory.Craft0Manufacturer
								.getCode() != (byte) product.getItemSubType() && (byte) InventorySubCategory.Services
								.getCode() != (byte) product.getItemSubType())){
					stock = new Stock();
					shelf = stockBL
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									wastageDetail.getShelf().getShelfId());
					stock.setShelf(shelf);
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setProduct(wastageDetail.getProduct());
					stock.setQuantity(wastageDetail.getWastageQuantity());
					stock.setImplementation(getImplementation());
					stocks.add(stock);
				} 
			}
		}
		return stocks;
	}

	private MaterialWastage addMaterialWastage(MaterialWastage materialWastage) {
		MaterialWastageVO materialWastageVO = new MaterialWastageVO();
		materialWastageVO.setMaterialWastageId(materialWastage
				.getMaterialWastageId());
		materialWastageVO.setDate(DateFormat
				.convertDateToString(materialWastage.getWastageDate()
						.toString()));
		materialWastageVO.setPersonName(materialWastage.getPerson()
				.getFirstName().concat(" ")
				+ materialWastage.getPerson().getLastName());
		materialWastageVO.setReferenceNumber(materialWastage
				.getReferenceNumber());
		materialWastageVO.setWastageTypeStr(WastageTypes
				.get(materialWastage.getWastageType()).name()
				.replaceAll("_", " "));
		materialWastageVO.setStatusStr(materialWastage.getStatus() ? "Open"
				: "Closed");
		return materialWastageVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public MaterialWastageService getMaterialWastageService() {
		return materialWastageService;
	}

	public void setMaterialWastageService(
			MaterialWastageService materialWastageService) {
		this.materialWastageService = materialWastageService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	} 
}
