package com.aiotech.aios.accounts.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CategoryService {
	private AIOTechGenericDAO<Category> categoryDAO;

	public List<Category> getAllCategories(Implementation implementation)
			throws Exception {
		return categoryDAO.findByNamedQuery("getAllCategories", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Category> getAllActiveCategories(Implementation implementation)
			throws Exception {
		return categoryDAO.findByNamedQuery("getAllActiveCategories",
				implementation, new Date(),
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Category> getCategoryByName(Implementation implementation,
			String categoryName) throws Exception {
		return categoryDAO.findByNamedQuery("getActiveCategoryByName",
				implementation, Calendar.getInstance().getTime(), categoryName);
	}

	@SuppressWarnings("unchecked")
	public List<Category> getAllActiveCategories(Implementation implementation,
			Session session) throws Exception {
		String getAllActiveCategories = "SELECT c FROM Category c"
				+ " WHERE c.implementation=? AND c.toDate>=? AND c.status=1";
		List<Category> categories = session.createQuery(getAllActiveCategories)
				.setEntity(0, implementation)
				.setDate(1, Calendar.getInstance().getTime()).list();
		return categories;
	}

	public void saveCategory(Category category) throws Exception {
		categoryDAO.saveOrUpdate(category);
	}

	public void saveCatgory(Category category, Session session)
			throws Exception {
		session.saveOrUpdate(category);
	}

	public Category getCategoryByName(String categoryName,
			Implementation implementation) throws Exception {
		return categoryDAO.findByNamedQuery("getCategoryByName",
				implementation, categoryName).get(0);
	}

	public Category getCategoryById(Long categoryId) throws Exception {
		return categoryDAO.findById(categoryId);
	}

	public void deleteCategory(Category category) throws Exception {
		categoryDAO.delete(category);
	}

	public AIOTechGenericDAO<Category> getCategoryDAO() {
		return categoryDAO;
	}

	public void setCategoryDAO(AIOTechGenericDAO<Category> categoryDAO) {
		this.categoryDAO = categoryDAO;
	}
}
