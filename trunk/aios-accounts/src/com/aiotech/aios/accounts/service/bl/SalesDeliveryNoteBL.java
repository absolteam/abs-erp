/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryCharge;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryPack;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryChargeVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryPackVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.SalesDeliveryNoteService;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * @author Saleem
 */
public class SalesDeliveryNoteBL {

	private SalesDeliveryNoteService salesDeliveryNoteService;
	private SalesOrderBL salesOrderBL;
	private ProductBL productBL;
	private TransactionBL transactionBL;
	private StockBL stockBL;
	private ProductPricingBL productPricingBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private CreditTermBL creditTermBL;

	// Get all Delivery Notes
	public List<Object> getAllDeliveryNotes() throws Exception {
		List<SalesDeliveryNote> salesDeliveryNotes = salesDeliveryNoteService
				.getAllDeliveryNotes(getImplementation());
		List<Object> deliveryNoteObject = new ArrayList<Object>();
		SalesDeliveryNoteVO salesDeliveryNoteVO = null;
		if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0) {
			for (SalesDeliveryNote deliveryNote : salesDeliveryNotes) {
				salesDeliveryNoteVO = new SalesDeliveryNoteVO();
				salesDeliveryNoteVO.setSalesDeliveryNoteId(deliveryNote
						.getSalesDeliveryNoteId());
				salesDeliveryNoteVO.setReferenceNumber(deliveryNote
						.getReferenceNumber());
				salesDeliveryNoteVO
						.setCustomerName(null != deliveryNote.getCustomer()
								.getPersonByPersonId() ? deliveryNote
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat("")
								.concat(deliveryNote.getCustomer()
										.getPersonByPersonId().getLastName())
								: (null != deliveryNote.getCustomer()
										.getCompany() ? deliveryNote
										.getCustomer().getCompany()
										.getCompanyName() : deliveryNote
										.getCustomer().getCmpDeptLocation()
										.getCompany().getCompanyName()));
				salesDeliveryNoteVO.setSalesPerson(null != deliveryNote
						.getPersonBySalesPersonId() ? deliveryNote
						.getPersonBySalesPersonId()
						.getFirstName()
						.concat(" ")
						.concat(deliveryNote.getPersonBySalesPersonId()
								.getLastName()) : "");
				salesDeliveryNoteVO.setDeliveryStatus(O2CProcessStatus.get(
						deliveryNote.getStatus()).name());
				boolean flag = false;
				for (SalesDeliveryDetail detail : deliveryNote
						.getSalesDeliveryDetails()) {
					if (detail.getSalesInvoiceDetails() != null
							&& detail.getSalesInvoiceDetails().size() > 0) {
						flag = true;
						break;
					}
				}

				if (flag) {
					salesDeliveryNoteVO
							.setDeliveryStatus(O2CProcessStatus.Invoiced.name());
					salesDeliveryNoteVO.setDeliveryFlag(flag);
					deliveryNote.setStatus(O2CProcessStatus.Invoiced.getCode());
				} else {
					salesDeliveryNoteVO.setDeliveryStatus(O2CProcessStatus.get(
							deliveryNote.getStatus()).name());
					salesDeliveryNoteVO.setDeliveryFlag(flag);
				}

				salesDeliveryNoteVO.setDispatchDate(DateFormat
						.convertDateToString(deliveryNote.getDeliveryDate()
								.toString()));
				deliveryNoteObject.add(salesDeliveryNoteVO);
			}
		}
		return deliveryNoteObject;
	}

	// Validate Stock
	public boolean validateStockQuantity(
			List<SalesDeliveryDetail> salesDeliveryDetails) throws Exception {
		StockVO stockVO = null;
		Product product = null;
		for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveryDetails) {
			product = productBL.getProductService().getBasicProductById(
					salesDeliveryDetail.getProduct().getProductId());
			if (((byte) product.getItemSubType() != (byte) InventorySubCategory.Services
					.getCode())
					&& ((byte) product.getItemSubType() != (byte) InventorySubCategory.Craft0Manufacturer
							.getCode())) {
				if (null != salesDeliveryDetail.getShelf()
						&& !("").equals(salesDeliveryDetail.getShelf())) {
					List<Stock> stocks = stockBL
							.getStockService()
							.getStockFullDetails(
									salesDeliveryDetail.getProduct()
											.getProductId(),
									salesDeliveryDetail.getShelf().getShelfId());
					if (null != stocks && stocks.size() > 0) {
						List<Stock> tempStocks = new ArrayList<Stock>();
						for (Stock stock : stocks) {
							if (null != stock.getQuantity()
									&& stock.getQuantity() > 0) {
								tempStocks.add(stock);
							}
						}
						if (null != tempStocks && tempStocks.size() > 0) {
							stockVO = stockBL.validateStocks(tempStocks);
							if (null != stockVO.getAvailableQuantity()
									&& !("").equals(stockVO
											.getAvailableQuantity())
									&& stockVO.getAvailableQuantity() < salesDeliveryDetail
											.getDeliveryQuantity())
								return false;
						} else
							return false;

					} else
						return false;
				} else
					return false;
			}
		}
		return true;
	}

	// Manipulate Sales Delivery Note
	public SalesDeliveryNoteVO manipulateDeliverNote(
			SalesDeliveryNote salesDeliveryNote) {
		SalesDeliveryNoteVO salesDeliveryNoteVO = new SalesDeliveryNoteVO();

		try {

			salesDeliveryNoteVO.setCurrencyId(salesDeliveryNote.getCurrency()
					.getCurrencyId());
			salesDeliveryNoteVO.setDispatchDate(DateFormat
					.convertSystemDateToString(salesDeliveryNote
							.getDeliveryDate()));
			if (salesDeliveryNote.getCurrency() != null
					&& salesDeliveryNote.getCurrency().getCurrencyPool() != null)
				salesDeliveryNoteVO.setCurrencyCode(salesDeliveryNote
						.getCurrency().getCurrencyPool().getCode());
			salesDeliveryNoteVO.setCustomerId(salesDeliveryNote.getCustomer()
					.getCustomerId());
			salesDeliveryNoteVO
					.setCustomerName(null != salesDeliveryNote.getCustomer()
							.getPersonByPersonId() ? salesDeliveryNote
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat("")
							.concat(salesDeliveryNote.getCustomer()
									.getPersonByPersonId().getLastName())
							: (null != salesDeliveryNote.getCustomer()
									.getCompany() ? salesDeliveryNote
									.getCustomer().getCompany()
									.getCompanyName() : salesDeliveryNote
									.getCustomer().getCmpDeptLocation()
									.getCompany().getCompanyName()));
			if (salesDeliveryNote.getCustomer().getCompany() != null) {
				salesDeliveryNoteVO.setFax(salesDeliveryNote.getCustomer()
						.getCompany().getCompanyFax()
						+ "");
				salesDeliveryNoteVO.setTelephone(salesDeliveryNote
						.getCustomer().getCompany().getCompanyPhone()
						+ "");
				salesDeliveryNoteVO.setAddress(salesDeliveryNote.getCustomer()
						.getCompany().getCompanyAddress()
						+ "");
				salesDeliveryNoteVO.setPoBox(salesDeliveryNote.getCustomer()
						.getCompany().getCompanyPobox()
						+ "");
			}
			salesDeliveryNoteVO.setReferenceNumber(salesDeliveryNote
					.getReferenceNumber());
			salesDeliveryNoteVO.setSalesDeliveryNoteId(salesDeliveryNote
					.getSalesDeliveryNoteId());
			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			SalesDeliveryDetailVO salesDeliveryDetailVO = null;
			Double invoiceQty = 0d;
			Shelf shelf = null;
			salesDeliveryNoteVO.setTotalDeliveryAmount(0.0);
			for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveryNote
					.getSalesDeliveryDetails()) {
				salesDeliveryDetailVO = new SalesDeliveryDetailVO();
				invoiceQty = 0d;
				salesDeliveryDetailVO
						.setSalesDeliveryDetailId(salesDeliveryDetail
								.getSalesDeliveryDetailId());
				salesDeliveryDetailVO.setProductName(salesDeliveryDetail
						.getProduct().getProductName());
				salesDeliveryDetailVO.setProductId(salesDeliveryDetail
						.getProduct().getProductId());
				salesDeliveryDetailVO.setUnitCode(salesDeliveryDetail
						.getProduct().getLookupDetailByProductUnit()
						.getAccessCode());
				if (salesDeliveryDetail.getProduct()
						.getLookupDetailByProductUnit() != null) {
					salesDeliveryDetailVO.setUnitName(salesDeliveryDetail
							.getProduct().getLookupDetailByProductUnit()
							.getDisplayName());
				}
				if (salesDeliveryDetail.getShelf() != null) {
					shelf = stockBL
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									salesDeliveryDetail.getShelf().getShelfId());
					salesDeliveryDetailVO.setShelfId(shelf.getShelfId());
					salesDeliveryDetailVO.setStoreName(shelf.getShelf()
							.getAisle().getStore().getStoreName()
							+ " >> "
							+ shelf.getShelf().getAisle().getSectionName()
							+ " >> "
							+ shelf.getShelf().getName()
							+ " >> "
							+ shelf.getName());
				} else {
					salesDeliveryDetailVO.setStoreName("");
				}
				salesDeliveryDetailVO.setDeliveredQuantity(salesDeliveryDetail
						.getPackageUnit());
				salesDeliveryDetailVO.setUnitRate(salesDeliveryDetail
						.getUnitRate());
				if (null != salesDeliveryDetail.getDiscount()
						&& salesDeliveryDetail.getDiscount() > 0) {
					double discountAmount = 0;
					if (salesDeliveryDetail.getIsPercentage()) {
						discountAmount = ((salesDeliveryDetail.getUnitRate() * salesDeliveryDetail
								.getDeliveryQuantity()) * salesDeliveryDetail
								.getDiscount()) / 100;
					} else {
						discountAmount = salesDeliveryDetail.getDiscount();
					}
					salesDeliveryDetailVO.setUnitRate(salesDeliveryDetailVO
							.getUnitRate() - discountAmount);
				}

				salesDeliveryDetailVO.setTotalRate(salesDeliveryDetailVO
						.getUnitRate() * salesDeliveryDetail.getPackageUnit());
				salesDeliveryNoteVO.setTotalDeliveryAmount(salesDeliveryNoteVO
						.getTotalDeliveryAmount()
						+ salesDeliveryDetailVO.getTotalRate());

				if (null != salesDeliveryDetail.getSalesInvoiceDetails()
						&& salesDeliveryDetail.getSalesInvoiceDetails().size() > 0) {
					double invoiceQuantity = 0;
					for (SalesInvoiceDetail salesInvoiceDetail : salesDeliveryDetail
							.getSalesInvoiceDetails()) {
						invoiceQuantity += salesInvoiceDetail
								.getInvoiceQuantity();
					}
					salesDeliveryDetailVO.setInvoiceQuantity(invoiceQuantity);
				}
				if (null != salesDeliveryDetail.getCreditDetails()
						&& salesDeliveryDetail.getCreditDetails().size() > 0) {
					double inwardedQuantity = 0;
					double returnQuantity = 0;
					for (CreditDetail creditDetail : salesDeliveryDetail
							.getCreditDetails()) {
						inwardedQuantity += creditDetail.getPackageUnit();
						returnQuantity += creditDetail.getPackageUnit();
					}
					salesDeliveryDetailVO.setInwardedQuantity(inwardedQuantity);
				}
				if (null != salesDeliveryDetailVO.getInvoiceQuantity())
					invoiceQty += salesDeliveryDetailVO.getInvoiceQuantity();
				if (null != salesDeliveryDetailVO.getInwardedQuantity())
					invoiceQty += salesDeliveryDetailVO.getInwardedQuantity();
				invoiceQty = salesDeliveryDetailVO.getDeliveredQuantity()
						- invoiceQty;

				salesDeliveryDetailVO
						.setProductPackageVOs(salesOrderBL
								.getCustomerQuotationBL()
								.getPackageConversionBL()
								.getProductPackagingDetail(
										salesDeliveryDetail.getProduct()
												.getProductId()));
				if (null != salesDeliveryDetail.getProductPackageDetail()) {
					salesDeliveryDetailVO
							.setPackageDetailId(salesDeliveryDetail
									.getProductPackageDetail()
									.getProductPackageDetailId());
					boolean iflag = true;
					for (ProductPackageVO packagevo : salesDeliveryDetailVO
							.getProductPackageVOs()) {
						if (iflag) {
							for (ProductPackageDetailVO packageDtvo : packagevo
									.getProductPackageDetailVOs()) {
								if ((long) packageDtvo
										.getProductPackageDetailId() == (long) salesDeliveryDetail
										.getProductPackageDetail()
										.getProductPackageDetailId()) {
									salesDeliveryDetailVO
											.setConversionUnitName(packageDtvo
													.getConversionUnitName());
									iflag = true;
									break;
								}
							}
						} else
							break;
					}
					salesDeliveryDetailVO.setBaseUnitName(salesDeliveryDetail
							.getProduct().getLookupDetailByProductUnit()
							.getDisplayName());
				} else {
					salesDeliveryDetailVO
							.setConversionUnitName(salesDeliveryDetail
									.getProduct()
									.getLookupDetailByProductUnit()
									.getDisplayName());
					salesDeliveryDetailVO.setPackageDetailId(-1l);
				}

				if (invoiceQty > 0) {
					salesDeliveryDetailVO.setPackageUnit(invoiceQty);
					salesDeliveryDetailVO.setInwardQuantity(invoiceQty);
					if (salesDeliveryDetailVO.getPackageDetailId() > 0) {
						salesDeliveryDetailVO.setBaseQuantity(AIOSCommons
								.convertExponential(salesOrderBL
										.getCustomerQuotationBL()
										.getPackageConversionBL()
										.getPackageBaseQuantity(
												salesDeliveryDetailVO
														.getPackageDetailId(),
												invoiceQty)));
					} else
						salesDeliveryDetailVO.setBaseQuantity(String
								.valueOf(invoiceQty));
					salesDeliveryDetailVOs.add(salesDeliveryDetailVO);
				}
			}
			salesDeliveryNoteVO
					.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);

			boolean flag = false;
			for (SalesDeliveryDetail detail : salesDeliveryNote
					.getSalesDeliveryDetails()) {
				if (detail.getSalesInvoiceDetails() != null
						&& detail.getSalesInvoiceDetails().size() > 0) {
					flag = true;
					break;
				}
			}

			if (flag) {
				salesDeliveryNoteVO.setDeliveryStatus(O2CProcessStatus.Invoiced
						.name());
				salesDeliveryNoteVO.setDeliveryFlag(flag);
				salesDeliveryNote
						.setStatus(O2CProcessStatus.Invoiced.getCode());
			} else {
				salesDeliveryNoteVO.setDeliveryStatus(O2CProcessStatus.get(
						salesDeliveryNote.getStatus()).name());
				salesDeliveryNoteVO.setDeliveryFlag(flag);
			}

			String decimalAmount = String
					.valueOf(AIOSCommons.formatAmount(salesDeliveryNoteVO
							.getTotalDeliveryAmount()));
			decimalAmount = decimalAmount.substring(decimalAmount
					.lastIndexOf('.') + 1);
			String amountInWords = AIOSCommons
					.convertAmountToWords(salesDeliveryNoteVO
							.getTotalDeliveryAmount());
			if (Double.parseDouble(decimalAmount) > 0) {
				amountInWords = amountInWords
						.concat(" and ")
						.concat(AIOSCommons.convertAmountToWords(decimalAmount))
						.concat(" Fils ");
				String replaceWord = "Only";
				amountInWords = amountInWords.replaceAll(replaceWord, "");
				amountInWords = amountInWords.concat(" " + replaceWord);
			}

			salesDeliveryNoteVO
					.setTotalDeliveryAmount(AIOSCommons
							.roundDecimals(salesDeliveryNoteVO
									.getTotalDeliveryAmount()));
			salesDeliveryNoteVO.setAmountInWords(amountInWords);

			if (salesDeliveryNote.getCreditTerm() != null)
				salesDeliveryNoteVO.setCreditTermName(salesDeliveryNote
						.getCreditTerm().getName());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return salesDeliveryNoteVO;
	}

	// Manipulate Sales Delivery Note
	public SalesDeliveryNoteVO convertEntityTOVO(
			SalesDeliveryNote salesDeliveryNote) {
		SalesDeliveryNoteVO salesDeliveryNoteVO = new SalesDeliveryNoteVO();

		try {

			BeanUtils.copyProperties(salesDeliveryNoteVO, salesDeliveryNote);

			salesDeliveryNoteVO.setCurrencyId(salesDeliveryNote.getCurrency()
					.getCurrencyId());
			salesDeliveryNoteVO.setDispatchDate(DateFormat
					.convertSystemDateToString(salesDeliveryNote
							.getDeliveryDate()));
			if (salesDeliveryNote.getCurrency() != null
					&& salesDeliveryNote.getCurrency().getCurrencyPool() != null)
				salesDeliveryNoteVO.setCurrencyCode(salesDeliveryNote
						.getCurrency().getCurrencyPool().getCode());
			salesDeliveryNoteVO.setCustomerId(salesDeliveryNote.getCustomer()
					.getCustomerId());
			salesDeliveryNoteVO
					.setCustomerName(null != salesDeliveryNote.getCustomer()
							.getPersonByPersonId() ? salesDeliveryNote
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat("")
							.concat(salesDeliveryNote.getCustomer()
									.getPersonByPersonId().getLastName())
							: (null != salesDeliveryNote.getCustomer()
									.getCompany() ? salesDeliveryNote
									.getCustomer().getCompany()
									.getCompanyName() : salesDeliveryNote
									.getCustomer().getCmpDeptLocation()
									.getCompany().getCompanyName()));
			if (salesDeliveryNote.getCustomer().getCompany() != null) {
				salesDeliveryNoteVO.setFax(salesDeliveryNote.getCustomer()
						.getCompany().getCompanyFax()
						+ "");
				salesDeliveryNoteVO.setTelephone(salesDeliveryNote
						.getCustomer().getCompany().getCompanyPhone()
						+ "");
				salesDeliveryNoteVO.setAddress(salesDeliveryNote.getCustomer()
						.getCompany().getCompanyAddress()
						+ "");
				salesDeliveryNoteVO.setPoBox(salesDeliveryNote.getCustomer()
						.getCompany().getCompanyPobox()
						+ "");
			}
			salesDeliveryNoteVO.setReferenceNumber(salesDeliveryNote
					.getReferenceNumber());
			salesDeliveryNoteVO.setSalesDeliveryNoteId(salesDeliveryNote
					.getSalesDeliveryNoteId());
			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			SalesDeliveryDetailVO salesDeliveryDetailVO = null;
			Shelf shelf = null;
			salesDeliveryNoteVO.setTotalDeliveryAmount(0.0);
			for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveryNote
					.getSalesDeliveryDetails()) {
				salesDeliveryDetailVO = new SalesDeliveryDetailVO();
				BeanUtils.copyProperties(salesDeliveryDetailVO,
						salesDeliveryDetail);
				salesDeliveryDetailVO
						.setSalesDeliveryDetailId(salesDeliveryDetail
								.getSalesDeliveryDetailId());
				salesDeliveryDetailVO.setProductName(salesDeliveryDetail
						.getProduct().getProductName());
				salesDeliveryDetailVO.setProductId(salesDeliveryDetail
						.getProduct().getProductId());
				if (salesDeliveryDetailVO.getProduct()
						.getLookupDetailByProductUnit() != null) {
					salesDeliveryDetailVO.setUnitName(salesDeliveryDetailVO
							.getProduct().getLookupDetailByProductUnit()
							.getDisplayName());
				}
				if (salesDeliveryDetail.getShelf() != null) {
					shelf = stockBL
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									salesDeliveryDetail.getShelf().getShelfId());
					salesDeliveryDetailVO.setShelfId(shelf.getShelfId());
					salesDeliveryDetailVO.setStoreName(shelf.getShelf()
							.getAisle().getStore().getStoreName()
							+ " >> "
							+ shelf.getShelf().getAisle().getSectionName()
							+ " >> "
							+ shelf.getShelf().getName()
							+ " >> "
							+ shelf.getName());
				} else {
					salesDeliveryDetailVO.setStoreName("");
				}
				salesDeliveryDetailVO.setDeliveredQuantity(salesDeliveryDetail
						.getDeliveryQuantity());
				salesDeliveryDetailVO.setUnitRate(salesDeliveryDetail
						.getUnitRate());
				if (null != salesDeliveryDetail.getDiscount()
						&& salesDeliveryDetail.getDiscount() > 0) {
					double discountAmount = 0;
					if (salesDeliveryDetail.getIsPercentage()) {
						discountAmount = ((salesDeliveryDetail.getUnitRate() * salesDeliveryDetail
								.getDeliveryQuantity()) * salesDeliveryDetail
								.getDiscount()) / 100;
					} else {
						discountAmount = salesDeliveryDetail.getDiscount();
					}
					salesDeliveryDetailVO.setUnitRate(salesDeliveryDetailVO
							.getUnitRate() - discountAmount);
				}

				salesDeliveryDetailVO.setTotalRate(salesDeliveryDetailVO
						.getUnitRate()
						* salesDeliveryDetail.getDeliveryQuantity());
				salesDeliveryNoteVO.setTotalDeliveryAmount(salesDeliveryNoteVO
						.getTotalDeliveryAmount()
						+ salesDeliveryDetailVO.getTotalRate());

				if (null != salesDeliveryDetail.getSalesInvoiceDetails()
						&& salesDeliveryDetail.getSalesInvoiceDetails().size() > 0) {
					double invoiceQuantity = 0;
					for (SalesInvoiceDetail salesInvoiceDetail : salesDeliveryDetail
							.getSalesInvoiceDetails()) {
						invoiceQuantity += salesInvoiceDetail
								.getInvoiceQuantity();
					}
					salesDeliveryDetailVO.setInvoiceQuantity(invoiceQuantity);
				}
				if (null != salesDeliveryDetail.getCreditDetails()
						&& salesDeliveryDetail.getCreditDetails().size() > 0) {
					double inwardedQuantity = 0;
					double returnQuantity = 0;
					for (CreditDetail creditDetail : salesDeliveryDetail
							.getCreditDetails()) {
						inwardedQuantity += creditDetail.getReturnQuantity();
						returnQuantity += creditDetail.getReturnQuantity();
					}
					salesDeliveryDetailVO.setInwardedQuantity(inwardedQuantity);
				}

				salesDeliveryDetailVOs.add(salesDeliveryDetailVO);
			}
			salesDeliveryNoteVO
					.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);

			boolean flag = false;
			for (SalesDeliveryDetail detail : salesDeliveryNote
					.getSalesDeliveryDetails()) {
				if (detail.getSalesInvoiceDetails() != null
						&& detail.getSalesInvoiceDetails().size() > 0) {
					flag = true;
					break;
				}
			}

			if (flag) {
				salesDeliveryNoteVO.setDeliveryStatus(O2CProcessStatus.Invoiced
						.name());
				salesDeliveryNoteVO.setDeliveryFlag(flag);
				salesDeliveryNote
						.setStatus(O2CProcessStatus.Invoiced.getCode());
			} else {
				salesDeliveryNoteVO.setDeliveryStatus(O2CProcessStatus.get(
						salesDeliveryNote.getStatus()).name());
				salesDeliveryNoteVO.setDeliveryFlag(flag);
			}

			String decimalAmount = String
					.valueOf(AIOSCommons.formatAmount(salesDeliveryNoteVO
							.getTotalDeliveryAmount()));
			decimalAmount = decimalAmount.substring(decimalAmount
					.lastIndexOf('.') + 1);
			String amountInWords = AIOSCommons
					.convertAmountToWords(salesDeliveryNoteVO
							.getTotalDeliveryAmount());
			if (Double.parseDouble(decimalAmount) > 0) {
				amountInWords = amountInWords
						.concat(" and ")
						.concat(AIOSCommons.convertAmountToWords(decimalAmount))
						.concat(" Fils ");
				String replaceWord = "Only";
				amountInWords = amountInWords.replaceAll(replaceWord, "");
				amountInWords = amountInWords.concat(" " + replaceWord);
			}

			salesDeliveryNoteVO
					.setTotalDeliveryAmount(AIOSCommons
							.roundDecimals(salesDeliveryNoteVO
									.getTotalDeliveryAmount()));
			salesDeliveryNoteVO.setAmountInWords(amountInWords);

			if (salesDeliveryNote.getCreditTerm() != null)
				salesDeliveryNoteVO.setCreditTermName(salesDeliveryNote
						.getCreditTerm().getName());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return salesDeliveryNoteVO;
	}

	public SalesDeliveryDetailVO convertSalesDeliveryDetailToVO(
			SalesDeliveryDetail salesDeliveryDetail) {
		Double invoiceQty = 0d;
		Shelf shelf = null;
		SalesDeliveryDetailVO salesDeliveryDetailVO = new SalesDeliveryDetailVO();
		try {
			BeanUtils
					.copyProperties(salesDeliveryDetailVO, salesDeliveryDetail);
			invoiceQty = 0d;
			salesDeliveryDetailVO.setSalesDeliveryDetailId(salesDeliveryDetail
					.getSalesDeliveryDetailId());
			salesDeliveryDetailVO.setProductName(salesDeliveryDetail
					.getProduct().getProductName());
			salesDeliveryDetailVO.setProductId(salesDeliveryDetail.getProduct()
					.getProductId());
			if (salesDeliveryDetailVO.getProduct()
					.getLookupDetailByProductUnit() != null) {
				salesDeliveryDetailVO.setUnitName(salesDeliveryDetailVO
						.getProduct().getLookupDetailByProductUnit()
						.getDisplayName());
			}
			if (salesDeliveryDetail.getShelf() != null) {
				shelf = stockBL
						.getStoreBL()
						.getStoreService()
						.getStoreByShelfId(
								salesDeliveryDetail.getShelf().getShelfId());
				salesDeliveryDetailVO.setShelfId(shelf.getShelfId());
				salesDeliveryDetailVO.setStoreName(shelf.getShelf().getAisle()
						.getStore().getStoreName()
						+ " >> "
						+ shelf.getShelf().getAisle().getSectionName()
						+ " >> "
						+ shelf.getShelf().getName()
						+ " >> "
						+ shelf.getName());
			} else {
				salesDeliveryDetailVO.setStoreName("");
			}
			salesDeliveryDetailVO.setDeliveredQuantity(salesDeliveryDetail
					.getDeliveryQuantity());
			salesDeliveryDetailVO
					.setUnitRate(salesDeliveryDetail.getUnitRate());
			if (null != salesDeliveryDetail.getDiscount()
					&& salesDeliveryDetail.getDiscount() > 0) {
				double discountAmount = 0;
				if (salesDeliveryDetail.getIsPercentage()) {
					discountAmount = ((salesDeliveryDetail.getUnitRate() * salesDeliveryDetail
							.getDeliveryQuantity()) * salesDeliveryDetail
							.getDiscount()) / 100;
				} else {
					discountAmount = salesDeliveryDetail.getDiscount();
				}
				salesDeliveryDetailVO.setUnitRate(salesDeliveryDetailVO
						.getUnitRate() - discountAmount);
			}

			salesDeliveryDetailVO.setTotalRate(salesDeliveryDetailVO
					.getUnitRate() * salesDeliveryDetail.getDeliveryQuantity());

			if (null != salesDeliveryDetail.getSalesInvoiceDetails()
					&& salesDeliveryDetail.getSalesInvoiceDetails().size() > 0) {
				double invoiceQuantity = 0;
				for (SalesInvoiceDetail salesInvoiceDetail : salesDeliveryDetail
						.getSalesInvoiceDetails()) {
					invoiceQuantity += salesInvoiceDetail.getInvoiceQuantity();
				}
				salesDeliveryDetailVO.setInvoiceQuantity(invoiceQuantity);
			}
			if (null != salesDeliveryDetail.getCreditDetails()
					&& salesDeliveryDetail.getCreditDetails().size() > 0) {
				double inwardedQuantity = 0;
				double returnQuantity = 0;
				for (CreditDetail creditDetail : salesDeliveryDetail
						.getCreditDetails()) {
					inwardedQuantity += creditDetail.getReturnQuantity();
					returnQuantity += creditDetail.getReturnQuantity();
				}
				salesDeliveryDetailVO.setInwardedQuantity(inwardedQuantity);
			}
			if (null != salesDeliveryDetailVO.getInvoiceQuantity())
				invoiceQty += salesDeliveryDetailVO.getInvoiceQuantity();
			if (null != salesDeliveryDetailVO.getInwardedQuantity())
				invoiceQty += salesDeliveryDetailVO.getInwardedQuantity();
			invoiceQty = salesDeliveryDetailVO.getDeliveredQuantity()
					- invoiceQty;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return salesDeliveryDetailVO;
	}

	// Validate Sales Delivery Note
	public List<SalesDeliveryNoteVO> validateSalesDeliveryInvoice(
			List<SalesDeliveryDetail> salesDeliveryDetails) throws Exception {
		SalesDeliveryNoteVO deliveryNoteVO = null;
		SalesDeliveryDetailVO deliveryNoteDetailVO = null;
		Map<Long, SalesDeliveryNoteVO> deliveryNoteVOHash = new HashMap<Long, SalesDeliveryNoteVO>();
		Map<Long, List<SalesDeliveryDetailVO>> deliveryNoteDetailVOHash = new HashMap<Long, List<SalesDeliveryDetailVO>>();
		List<SalesDeliveryDetailVO> deliveryDetails = new ArrayList<SalesDeliveryDetailVO>();
		for (SalesDeliveryDetail deliveryDetail : salesDeliveryDetails) {
			deliveryNoteVO = new SalesDeliveryNoteVO();
			deliveryNoteDetailVO = new SalesDeliveryDetailVO();
			deliveryNoteVO.setSalesDeliveryNoteId(deliveryDetail
					.getSalesDeliveryNote().getSalesDeliveryNoteId());
			deliveryNoteVO.setReferenceNumber(deliveryDetail
					.getSalesDeliveryNote().getReferenceNumber());
			deliveryNoteVO.setDeliveryDate(deliveryDetail
					.getSalesDeliveryNote().getDeliveryDate());
			deliveryNoteVO.setCustomer(deliveryDetail.getSalesDeliveryNote()
					.getCustomer());
			deliveryNoteVO.setDescription(deliveryDetail.getSalesDeliveryNote()
					.getDescription());
			deliveryNoteDetailVO.setSalesDeliveryDetailId(deliveryDetail
					.getSalesDeliveryDetailId());
			deliveryNoteDetailVO.setProduct(deliveryDetail.getProduct());
			deliveryNoteDetailVO.setShelf(deliveryDetail.getShelf());
			deliveryNoteDetailVO.setDeliveryQuantity(deliveryDetail
					.getPackageUnit());
			deliveryNoteDetailVO
					.setPackageUnit(deliveryDetail.getPackageUnit());
			double inwardQty = 0;
			if (null != deliveryDetail.getCreditDetails()
					&& deliveryDetail.getCreditDetails().size() > 0) {
				for (CreditDetail creditDetail : deliveryDetail
						.getCreditDetails()) {
					inwardQty += creditDetail.getPackageUnit();
				}
			}
			deliveryNoteDetailVO.setDeliveryQuantity(deliveryNoteDetailVO
					.getDeliveryQuantity() - inwardQty);
			// Check for Discount
			if (null != deliveryDetail.getIsPercentage()
					&& null != deliveryDetail.getDiscount()
					&& deliveryDetail.getDiscount() > 0) {
				if (deliveryDetail.getIsPercentage()) {
					double totalRate = deliveryDetail.getUnitRate()
							* deliveryNoteDetailVO.getDeliveryQuantity();
					double discount = totalRate * deliveryDetail.getDiscount()
							/ 100;
					double finalAmount = ((totalRate - discount) / deliveryNoteDetailVO
							.getDeliveryQuantity());
					deliveryNoteDetailVO.setUnitRate(AIOSCommons
							.roundDecimals(finalAmount));
					deliveryNoteDetailVO
							.setInvoiceUnitRate(deliveryNoteDetailVO
									.getUnitRate());
				} else {
					double unitRate = ((deliveryDetail.getUnitRate() * deliveryNoteDetailVO
							.getDeliveryQuantity()) - deliveryDetail
							.getDiscount());
					double finalAmount = unitRate
							/ deliveryNoteDetailVO.getDeliveryQuantity();
					deliveryNoteDetailVO.setUnitRate(AIOSCommons
							.roundDecimals(finalAmount));
					deliveryNoteDetailVO
							.setInvoiceUnitRate(deliveryNoteDetailVO
									.getUnitRate());
				}
			} else {
				deliveryNoteDetailVO.setUnitRate(deliveryDetail.getUnitRate());
				deliveryNoteDetailVO.setInvoiceUnitRate(deliveryNoteDetailVO
						.getUnitRate());
			}
			deliveryNoteDetailVO.setTotalRate(AIOSCommons
					.roundDecimals(deliveryNoteDetailVO.getUnitRate()
							* deliveryNoteDetailVO.getDeliveryQuantity()));
			if (deliveryNoteDetailVO.getDeliveryQuantity() > 0)
				deliveryDetails.add(deliveryNoteDetailVO);

			List<SalesDeliveryDetailVO> salesDeliveryDetailVOList = deliveryNoteDetailVOHash
					.get(deliveryNoteVO.getSalesDeliveryNoteId());

			if (null != salesDeliveryDetailVOList
					&& salesDeliveryDetailVOList.size() > 0) {
				if (deliveryNoteDetailVO.getDeliveryQuantity() > 0) {
					salesDeliveryDetailVOList.add(deliveryNoteDetailVO);
					deliveryNoteDetailVOHash.put(
							deliveryNoteVO.getSalesDeliveryNoteId(),
							salesDeliveryDetailVOList);
				}
			} else {
				if (deliveryNoteDetailVO.getDeliveryQuantity() > 0) {
					salesDeliveryDetailVOList = new ArrayList<SalesDeliveryDetailVO>();
					salesDeliveryDetailVOList.add(deliveryNoteDetailVO);
					deliveryNoteDetailVOHash.put(
							deliveryNoteVO.getSalesDeliveryNoteId(),
							salesDeliveryDetailVOList);
				}
			}
			if (null != salesDeliveryDetailVOList
					&& salesDeliveryDetailVOList.size() > 0) {
				deliveryNoteVOHash.put(deliveryNoteVO.getSalesDeliveryNoteId(),
						deliveryNoteVO);
			}
		}
		for (Entry<Long, List<SalesDeliveryDetailVO>> noteReference : deliveryNoteDetailVOHash
				.entrySet()) {
			List<SalesDeliveryDetailVO> deliveryDetailVOList = deliveryNoteDetailVOHash
					.get(noteReference.getKey());
			if (deliveryNoteVOHash.containsKey(noteReference.getKey())) {
				deliveryNoteVO = deliveryNoteVOHash.get(noteReference.getKey());
				List<SalesDeliveryCharge> deliveryCharges = salesDeliveryNoteService
						.getSalesDeliveryChargesByDeliveryId(deliveryNoteVO
								.getSalesDeliveryNoteId());
				double addtionalCharges = 0;
				if (null != deliveryCharges && deliveryCharges.size() > 0) {
					for (SalesDeliveryCharge salesDeliveryCharge : deliveryCharges)
						addtionalCharges += salesDeliveryCharge.getCharges();
				}
				deliveryNoteVO.setSalesDeliveryDetailVOs(deliveryDetailVOList);
				deliveryNoteVO.setAddtionalCharges(addtionalCharges);
				for (SalesDeliveryDetailVO deliveryDetail : deliveryNoteVO
						.getSalesDeliveryDetailVOs()) {
					deliveryNoteVO
							.setTotalDeliveryQty(deliveryDetail
									.getDeliveryQuantity()
									+ (null != deliveryNoteVO
											.getTotalDeliveryQty() ? deliveryNoteVO
											.getTotalDeliveryQty() : 0));
					deliveryNoteVO
							.setTotalDeliveryAmount(deliveryDetail
									.getDeliveryQuantity()
									* deliveryDetail.getUnitRate()
									+ (null != deliveryNoteVO
											.getTotalDeliveryAmount() ? deliveryNoteVO
											.getTotalDeliveryAmount() : 0));
					if (null != deliveryNoteVO.getTotalDeliveryAmount()) {
						deliveryNoteVO.setTotalDeliveryAmount(AIOSCommons
								.roundDecimals(deliveryNoteVO
										.getTotalDeliveryAmount()));
					}
				}
				deliveryNoteVOHash.put(noteReference.getKey(), deliveryNoteVO);
			}
		}
		List<SalesDeliveryNoteVO> salesDeliveryNoteVOs = new ArrayList<SalesDeliveryNoteVO>(
				deliveryNoteVOHash.values());
		SalesDeliveryDetailVO salesDeliveryDetailVO = null;
		List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
		for (SalesDeliveryNoteVO salesDeliveryNoteVO : salesDeliveryNoteVOs) {
			if (null != salesDeliveryNoteVO.getSalesDeliveryDetailVOs()
					&& salesDeliveryNoteVO.getSalesDeliveryDetailVOs().size() > 0) {
				for (SalesDeliveryDetailVO deliveryDetailVO : salesDeliveryNoteVO
						.getSalesDeliveryDetailVOs()) {
					if (null != deliveryDetailVO.getSalesInvoiceDetails()
							&& deliveryDetailVO.getSalesInvoiceDetails().size() > 0) {
						for (SalesInvoiceDetail salesInvoice : deliveryDetailVO
								.getSalesInvoiceDetails()) {
							salesDeliveryDetailVO = new SalesDeliveryDetailVO();
							salesDeliveryDetailVO
									.setSalesInvoiceDetailId(salesInvoice
											.getSalesInvoiceDetailId());
							salesDeliveryDetailVO
									.setSalesDeliveryDetailId(deliveryDetailVO
											.getSalesDeliveryDetailId());
							salesDeliveryDetailVO.setShelf(deliveryDetailVO
									.getShelf());
							salesDeliveryDetailVO.setProduct(deliveryDetailVO
									.getProduct());
							salesDeliveryDetailVO
									.setDeliveryQuantity(deliveryDetailVO
											.getDeliveryQuantity());
							salesDeliveryDetailVO.setUnitRate(salesInvoice
									.getUnitRate());
							salesDeliveryDetailVO
									.setInvoiceUnitRate(salesInvoice
											.getUnitRate());
							salesDeliveryDetailVO
									.setInwardQuantity(deliveryDetailVO
											.getInwardQuantity());
							salesDeliveryDetailVO
									.setInvoiceQuantity(deliveryDetailVO
											.getDeliveryQuantity()
											- (salesInvoice
													.getInvoiceQuantity() != null ? salesInvoice
													.getInvoiceQuantity() : 0)
											- (salesDeliveryDetailVO
													.getInwardQuantity() != null ? salesDeliveryDetailVO
													.getInwardQuantity() : 0));
							salesDeliveryDetailVO.setInvoicedQty(salesInvoice
									.getInvoiceQuantity());
							salesDeliveryDetailVO.setTotalRate(salesInvoice
									.getInvoiceQuantity()
									* salesInvoice.getUnitRate());
							salesDeliveryDetailVOs.add(salesDeliveryDetailVO);
						}
					} else {
						salesDeliveryDetailVOs.add(deliveryDetailVO);
					}
				}
				salesDeliveryNoteVO
						.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);
				salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			}
		}
		return salesDeliveryNoteVOs;
	}

	// Save Delivery Note
	public void saveDeliveryNote(SalesDeliveryNote salesDeliveryNote,
			List<SalesDeliveryDetail> salesDeliveryDetail,
			List<SalesDeliveryCharge> salesDeliveryCharge,
			List<SalesDeliveryPack> salesDeliveryPack,
			List<SalesDeliveryDetail> deleteDeliveryDetails,
			List<SalesDeliveryCharge> deleteDeliveryCharges,
			List<SalesDeliveryPack> deleteDeliveryPack,
			List<SalesDeliveryDetail> exitingSalesDeliveryDetails,
			List<SalesDeliveryCharge> exitingSalesDeliveryCharges,
			SalesOrder salesOrder, SalesDeliveryNoteVO vo) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		salesDeliveryNote
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		boolean updateFlag = false;
		if (null != salesDeliveryNote.getSalesDeliveryNoteId())
			updateFlag = true;
		salesDeliveryNote.setImplementation(salesOrderBL.getImplementation());
		if (null != salesOrder)
			salesOrderBL.getSalesOrderService().saveSalesOrder(salesOrder);

		// Transaction Goes Here
		if (updateFlag) {
			transactionBL.deleteTransaction(
					salesDeliveryNote.getSalesDeliveryNoteId(),
					SalesDeliveryNote.class.getSimpleName());
			// Add Stock
			stockBL.updateBatchStockDetails(updateStockDetails(salesDeliveryDetail));
		} else
			SystemBL.saveReferenceStamp(SalesDeliveryNote.class.getName(),
					getImplementation());
		if (null != deleteDeliveryDetails && deleteDeliveryDetails.size() > 0)
			salesDeliveryNoteService
					.deleteDeliveryDetails(deleteDeliveryDetails);

		if (null != deleteDeliveryCharges && deleteDeliveryCharges.size() > 0)
			salesDeliveryNoteService
					.deleteDeliveryCharges(deleteDeliveryCharges);

		if (null != deleteDeliveryPack && deleteDeliveryPack.size() > 0)
			salesDeliveryNoteService.deleteDeliveryPack(deleteDeliveryPack);

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (salesDeliveryNote != null
				&& salesDeliveryNote.getSalesDeliveryNoteId() != null
				&& salesDeliveryNote.getSalesDeliveryNoteId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		salesDeliveryNote
				.setSalesDeliveryDetails(new HashSet<SalesDeliveryDetail>(
						salesDeliveryDetail));

		// Save Or Update Sales Delivery
		salesDeliveryNoteService.saveDeliveryNote(salesDeliveryNote,
				salesDeliveryDetail, salesDeliveryCharge, salesDeliveryPack);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesDeliveryNote.class.getSimpleName(),
				salesDeliveryNote.getSalesDeliveryNoteId(), user,
				workflowDetailVo);
	}

	public void doSalesDeliveryNoteBusinessUpdate(Long salesDeliveryNoteId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			SalesDeliveryNote salesDeliveryNote = salesDeliveryNoteService
					.getSalesDeliveryNoteInfo(salesDeliveryNoteId);
			if (!workflowDetailVO.isDeleteFlag()) {
				// Sales Delivery Transaction
				List<TransactionDetail> transactionDetails = createTransactionDetail(
						salesDeliveryNote, new ArrayList<SalesDeliveryDetail>(
								salesDeliveryNote.getSalesDeliveryDetails()),
						TransactionType.Debit.getCode(),
						TransactionType.Credit.getCode());
				Transaction transaction = creatTransaction(
						salesDeliveryNote,
						"SALES DELIVERY "
								+ salesDeliveryNote.getReferenceNumber(),
						transactionDetails);

				// Sales Unearned Revenue Transaction
				List<TransactionDetail> unearnedRevenueCustomer = createUnearnedRevenueTransaction(
						salesDeliveryNote,
						new ArrayList<SalesDeliveryDetail>(salesDeliveryNote
								.getSalesDeliveryDetails()),
						new ArrayList<SalesDeliveryCharge>(salesDeliveryNote
								.getSalesDeliveryCharges()),
						TransactionType.Debit.getCode(),
						TransactionType.Credit.getCode());
				Transaction customerTransaction = creatTransaction(
						salesDeliveryNote, "UNEARNED SALES REVENUE "
								+ salesDeliveryNote.getReferenceNumber(),
						transactionDetails);

				transactionBL.saveTransaction(transaction, transactionDetails);
				transactionBL.saveTransaction(customerTransaction,
						unearnedRevenueCustomer);

				// Update stock information
				stockBL.deleteBatchStockDetails(updateStockDetails(new ArrayList<SalesDeliveryDetail>(
						salesDeliveryNote.getSalesDeliveryDetails())));
			} else {
				transactionBL.deleteTransaction(
						salesDeliveryNote.getSalesDeliveryNoteId(),
						SalesDeliveryNote.class.getSimpleName());
				stockBL.updateBatchStockDetails(updateStockDetails(new ArrayList<SalesDeliveryDetail>(
						salesDeliveryNote.getSalesDeliveryDetails())));
				salesDeliveryNoteService.deleteSalesDeliveryNote(
						salesDeliveryNote,
						new ArrayList<SalesDeliveryDetail>(salesDeliveryNote
								.getSalesDeliveryDetails()),
						new ArrayList<SalesDeliveryCharge>(salesDeliveryNote
								.getSalesDeliveryCharges()),
						new ArrayList<SalesDeliveryPack>(salesDeliveryNote
								.getSalesDeliveryPacks()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Stock> updateStockDetails(
			List<SalesDeliveryDetail> salesDeliveryDetails) throws Exception {
		List<Stock> stocks = null;
		if (null != salesDeliveryDetails && salesDeliveryDetails.size() > 0) {
			Stock stock = null;
			Shelf shelf = null;
			stocks = new ArrayList<Stock>();
			for (SalesDeliveryDetail deliveryDetail : salesDeliveryDetails) {
				if (null != deliveryDetail.getShelf()) {
					stock = new Stock();
					if (null == deliveryDetail.getShelf().getShelf())
						shelf = stockBL
								.getStoreBL()
								.getStoreService()
								.getStoreByShelfId(
										deliveryDetail.getShelf().getShelfId());
					else {
						shelf = new Shelf();
						shelf = deliveryDetail.getShelf();
					}
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setShelf(shelf);
					stock.setProduct(deliveryDetail.getProduct());
					stock.setImplementation(getImplementation());
					stock.setQuantity(deliveryDetail.getDeliveryQuantity());
					stock.setBatchNumber(deliveryDetail.getBatchNumber());
					stock.setProductExpiry(deliveryDetail.getProductExpiry());
					stocks.add(stock);
				}
			}
		}
		return stocks;
	}

	// Delete Delivery Note
	public void deleteSalesDeliveryNote(SalesDeliveryNote salesDeliveryNote,
			List<SalesDeliveryDetail> salesDeliveryDetails,
			List<SalesDeliveryCharge> salesDeliveryCharges,
			List<SalesDeliveryPack> salesDeliveryPacks) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesDeliveryNote.class.getSimpleName(),
				salesDeliveryNote.getSalesDeliveryNoteId(), user,
				workflowDetailVo);
		workflowDetailVo.setDeleteFlag(true);
	}

	// Delete Delivery Note
	public void deleteSalesDeliveryNoteDirect(
			SalesDeliveryNote salesDeliveryNote) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		transactionBL.deleteTransaction(
				salesDeliveryNote.getSalesDeliveryNoteId(),
				SalesDeliveryNote.class.getSimpleName());
		stockBL.updateBatchStockDetails(updateStockDetails(new ArrayList<SalesDeliveryDetail>(
				salesDeliveryNote.getSalesDeliveryDetails())));
		salesDeliveryNoteService.deleteSalesDeliveryNote(
				salesDeliveryNote,
				new ArrayList<SalesDeliveryDetail>(salesDeliveryNote
						.getSalesDeliveryDetails()),
				new ArrayList<SalesDeliveryCharge>(salesDeliveryNote
						.getSalesDeliveryCharges()),
				new ArrayList<SalesDeliveryPack>(salesDeliveryNote
						.getSalesDeliveryPacks()));
	}

	// Create Transaction
	private Transaction creatTransaction(SalesDeliveryNote salesDeliveryNote,
			String description, List<TransactionDetail> transactionDetails)
			throws Exception {
		return transactionBL.createTransaction(transactionBL.getCalendarBL()
				.transactionPostingPeriod(salesDeliveryNote.getDeliveryDate()),
				salesDeliveryNote.getCurrency().getCurrencyId(), description,
				salesDeliveryNote.getDeliveryDate(), transactionBL
						.getCategory("Sales Delivery"), salesDeliveryNote
						.getSalesDeliveryNoteId(), SalesDeliveryNote.class
						.getSimpleName());
	}

	// Create Sales Delivery Note Transaction Details
	private List<TransactionDetail> createTransactionDetail(
			SalesDeliveryNote salesDeliveryNote,
			List<SalesDeliveryDetail> salesDeliveryDetails, Boolean debitFlag,
			Boolean creditFlag) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		TransactionDetail transactionDetail = null;
		ProductPricingDetail productPricingDetail = null;
		double basePrice = 0;
		for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveryDetails) {
			productPricingDetail = productPricingBL
					.getProductPricingDetail(salesDeliveryDetail.getProduct()
							.getProductId());
			if (null != productPricingDetail)
				basePrice = productPricingDetail.getBasicCostPrice()
						* salesDeliveryDetail.getDeliveryQuantity();
			else
				basePrice = salesDeliveryDetail.getUnitRate()
						* salesDeliveryDetail.getDeliveryQuantity();
			// Expense Account
			Combination expenseAccount = new Combination();
			expenseAccount.setCombinationId(getImplementation()
					.getExpenseAccount());
			transactionDetail = transactionBL.createTransactionDetail(
					basePrice, expenseAccount.getCombinationId(), debitFlag,
					null, null, salesDeliveryNote.getReferenceNumber());
			transactionDetails.add(transactionDetail);
			// Inventory Account
			Combination inventoryAccount = productBL
					.getProductInventory(salesDeliveryDetail.getProduct()
							.getProductId());
			transactionDetail = transactionBL.createTransactionDetail(
					basePrice, inventoryAccount.getCombinationId(), creditFlag,
					null, null, salesDeliveryNote.getReferenceNumber());
			transactionDetails.add(transactionDetail);
		}
		return transactionDetails;
	}

	// Create Unearned Revenue Sales Delivery Note Transaction Details
	private List<TransactionDetail> createUnearnedRevenueTransaction(
			SalesDeliveryNote salesDeliveryNote,
			List<SalesDeliveryDetail> salesDeliveryDetails,
			List<SalesDeliveryCharge> salesDeliveryCharge, Boolean debitFlag,
			Boolean creditFlag) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		TransactionDetail transactionDetail = null;
		double transactionAmount = 0;
		Combination unearnedRevenueAccount = new Combination();
		unearnedRevenueAccount.setCombinationId(getImplementation()
				.getUnearnedRevenue());
		for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveryDetails) {
			// Unearned Revenue Account
			transactionDetail = transactionBL.createTransactionDetail(
					salesDeliveryDetail.getUnitRate()
							* salesDeliveryDetail.getDeliveryQuantity(),
					unearnedRevenueAccount.getCombinationId(), creditFlag,
					null, null, salesDeliveryNote.getReferenceNumber());
			transactionAmount += transactionDetail.getAmount();
			transactionDetails.add(transactionDetail);
		}
		if (null != salesDeliveryCharge && salesDeliveryCharge.size() > 0) {
			for (SalesDeliveryCharge deliveryCharge : salesDeliveryCharge) {
				transactionDetail = transactionBL.createTransactionDetail(
						deliveryCharge.getCharges(),
						unearnedRevenueAccount.getCombinationId(), creditFlag,
						null, null, salesDeliveryNote.getReferenceNumber());
				transactionAmount += transactionDetail.getAmount();
				transactionDetails.add(transactionDetail);
			}
		}
		// Customer Account
		Customer customer = productPricingBL
				.getCustomerBL()
				.getCustomerService()
				.getCustomerDetails(
						salesDeliveryNote.getCustomer().getCustomerId());

		transactionDetail = transactionBL.createTransactionDetail(
				transactionAmount,
				customer.getCombination().getCombinationId(), debitFlag, null,
				null, salesDeliveryNote.getReferenceNumber());
		transactionDetails.add(transactionDetail);
		return transactionDetails;
	}

	public Map<Byte, String> getModeOfPayment() throws Exception {
		Map<Byte, String> modeOfPayments = new HashMap<Byte, String>();
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class))
			modeOfPayments.put(modeOfPayment.getCode(), modeOfPayment.name()
					.replaceAll("_", " "));
		return modeOfPayments;
	}

	public Map<Byte, String> getO2CProcessStatus() throws Exception {
		Map<Byte, String> o2CProcessStatuses = new HashMap<Byte, String>();
		for (O2CProcessStatus o2CProcessStatus : EnumSet
				.allOf(O2CProcessStatus.class))
			o2CProcessStatuses.put(o2CProcessStatus.getCode(), o2CProcessStatus
					.name().replaceAll("_", " "));
		return o2CProcessStatuses;
	}

	public List<SalesDeliveryNoteVO> fetchSalesDeliveryReportData()
			throws Exception {

		Long salesDeliveryId = 0L;
		Long customerId = 0L;
		Byte statusId = 0;
		Date fromDatee = null;
		Date toDatee = null;

		if (ServletActionContext.getRequest().getParameter("salesDeliveryId") != null) {
			salesDeliveryId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("salesDeliveryId"));
		}

		List<SalesDeliveryNote> salesDeliveryList = this
				.getSalesDeliveryNoteService().getFilteredDeliveryNotes(
						getImplementation(), salesDeliveryId, customerId,
						statusId, fromDatee, toDatee, null);

		List<SalesDeliveryNoteVO> salesDeliveryNoteVOs = new ArrayList<SalesDeliveryNoteVO>();

		for (SalesDeliveryNote salesDelivery : salesDeliveryList) {
			SalesDeliveryNoteVO deliveryNoteVO = new SalesDeliveryNoteVO(
					salesDelivery);
			deliveryNoteVO.setDispatchDate(DateFormat
					.convertDateToString(salesDelivery.getDeliveryDate()
							.toString()));
			deliveryNoteVO.setShippingDateFormat("");
			deliveryNoteVO.setExpiryDateFormat("");

			deliveryNoteVO.setCustomerName(null != salesDelivery.getCustomer()
					.getPersonByPersonId() ? salesDelivery
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesDelivery.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesDelivery
					.getCustomer().getCompany() ? salesDelivery.getCustomer()
					.getCompany().getCompanyName() : salesDelivery
					.getCustomer().getCmpDeptLocation().getCompany()
					.getCompanyName()));
			deliveryNoteVO.setSalesPerson(null != salesDelivery
					.getPersonBySalesPersonId() ? salesDelivery
					.getPersonBySalesPersonId()
					.getFirstName()
					.concat(" ")
					.concat(salesDelivery.getPersonBySalesPersonId()
							.getLastName()) : "");
			deliveryNoteVO.setStrStatus(O2CProcessStatus.get(
					salesDelivery.getStatus()).name());

			deliveryNoteVO.setCustomerNameAndNo(deliveryNoteVO
					.getCustomerName()
					+ " ["
					+ salesDelivery.getCustomer().getCustomerNumber() + "]");
			deliveryNoteVO.setShippingMethod(null != salesDelivery
					.getLookupDetailByShippingMethod() ? salesDelivery
					.getLookupDetailByShippingMethod().getDisplayName() : "");
			if (salesDelivery.getLookupDetailByShippingTerm() != null) {
				deliveryNoteVO.setShippingTerm(salesDelivery
						.getLookupDetailByShippingTerm().getDisplayName());
			}

			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			for (SalesDeliveryDetail detail : salesDelivery
					.getSalesDeliveryDetails()) {

				SalesDeliveryDetailVO salesDeliveryDetailVO = new SalesDeliveryDetailVO(
						detail);
				salesDeliveryDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesDeliveryDetailVO.setProductCode(detail.getProduct()
						.getCode());
				salesDeliveryDetailVO.setStoreName(detail.getShelf().getShelf()
						.getAisle().getStore().getStoreName()
						+ ">>"
						+ detail.getShelf().getShelf().getAisle()
								.getSectionName()
						+ ">>"
						+ detail.getShelf().getShelf().getName()
						+ ">>"
						+ detail.getShelf().getName());

				if (detail.getDeliveryQuantity() != null
						&& detail.getUnitRate() != null) {
					salesDeliveryDetailVO.setTotal((null != detail
							.getPackageUnit() ? detail.getPackageUnit()
							: detail.getDeliveryQuantity())
							* detail.getUnitRate());
				}
				salesDeliveryDetailVO.setDeliveryQuantity(null != detail
						.getPackageUnit() ? detail.getPackageUnit() : detail
						.getDeliveryQuantity());
				salesDeliveryDetailVO.setUnitName(null != detail
						.getProductPackageDetail() ? detail
						.getProductPackageDetail().getLookupDetail()
						.getDisplayName() : detail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());
				if (null != detail.getDiscount() && detail.getDiscount() > 0) {
					if (detail.getIsPercentage() != null
							&& detail.getIsPercentage()) {
						salesDeliveryDetailVO.setStrDiscountMode("Percentage");
						double discount = (salesDeliveryDetailVO.getTotal() * detail
								.getDiscount()) / 100;
						salesDeliveryDetailVO.setTotal(salesDeliveryDetailVO
								.getTotal() - discount);
					} else {
						salesDeliveryDetailVO.setStrDiscountMode("Amount");
						salesDeliveryDetailVO.setTotal(salesDeliveryDetailVO
								.getTotal() - detail.getDiscount());
					}
				}

				salesDeliveryDetailVOs.add(salesDeliveryDetailVO);
			}
			deliveryNoteVO.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);

			List<SalesDeliveryChargeVO> salesDeliveryChargeVOs = new ArrayList<SalesDeliveryChargeVO>();
			for (SalesDeliveryCharge charge : salesDelivery
					.getSalesDeliveryCharges()) {

				SalesDeliveryChargeVO salesDeliveryChargeVO = new SalesDeliveryChargeVO(
						charge);
				salesDeliveryChargeVO.setStrChargeType(charge.getLookupDetail()
						.getDisplayName());
				if (charge.getIsPercentage()) {
					salesDeliveryChargeVO.setStrMode("Percentage");
				} else {
					salesDeliveryChargeVO.setStrMode("Amount");
				}
				salesDeliveryChargeVOs.add(salesDeliveryChargeVO);
			}
			deliveryNoteVO.setSalesDeliveryChargeVOs(salesDeliveryChargeVOs);

			List<SalesDeliveryPackVO> salesDeliveryPackVOs = new ArrayList<SalesDeliveryPackVO>();

			for (SalesDeliveryPack packs : salesDelivery
					.getSalesDeliveryPacks()) {
				SalesDeliveryPackVO deliveryPackVO = new SalesDeliveryPackVO(
						packs);
				deliveryPackVO
						.setUnit(packs.getLookupDetail().getDisplayName());

				salesDeliveryPackVOs.add(deliveryPackVO);
			}
			deliveryNoteVO.setSalesDeliveryPackVOs(salesDeliveryPackVOs);

			salesDeliveryNoteVOs.add(deliveryNoteVO);
		}

		return salesDeliveryNoteVOs;
	}

	public String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(SalesDeliveryNote.class.getName(),
				getImplementation());
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public SalesDeliveryNoteService getSalesDeliveryNoteService() {
		return salesDeliveryNoteService;
	}

	public void setSalesDeliveryNoteService(
			SalesDeliveryNoteService salesDeliveryNoteService) {
		this.salesDeliveryNoteService = salesDeliveryNoteService;
	}

	public SalesOrderBL getSalesOrderBL() {
		return salesOrderBL;
	}

	public void setSalesOrderBL(SalesOrderBL salesOrderBL) {
		this.salesOrderBL = salesOrderBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public ProductPricingBL getProductPricingBL() {
		return productPricingBL;
	}

	public void setProductPricingBL(ProductPricingBL productPricingBL) {
		this.productPricingBL = productPricingBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}
}
