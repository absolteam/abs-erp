package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class RequisitionService {

	private AIOTechGenericDAO<Requisition> requisitionDAO;
	private AIOTechGenericDAO<RequisitionDetail> requisitionDetailDAO;

	public List<Requisition> getAllRequisitions(Implementation implementation)
			throws Exception {
		return requisitionDAO.findByNamedQuery("getAllRequisitions",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Requisition> getAllIssueRequisitions(
			Implementation implementation, byte status) throws Exception {
		return requisitionDAO.findByNamedQuery("getAllIssueRequisitions",
				implementation, status,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Requisition> getAllExpenseIssuedRequisitions(
			Implementation implementation, byte status, char itemType)
			throws Exception {
		return requisitionDAO.findByNamedQuery(
				"getAllExpenseIssuedRequisitions", implementation, status,
				(byte) WorkflowConstants.Status.Published.getCode(), itemType);
	}

	public List<Requisition> getAllQuotationPurchasedIssuedRequisitions(
			Implementation implementation, byte status) throws Exception {
		return requisitionDAO.findByNamedQuery(
				"getAllQuotationPurchasedIssuedRequisitions", implementation,
				status, (byte) WorkflowConstants.Status.Published.getCode());
	}
	
	public List<Requisition> getAllPurchasedIssuedRequisitions(
			Implementation implementation) throws Exception {
		return requisitionDAO.findByNamedQuery(
				"getAllPurchasedIssuedRequisitions", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public Requisition getRequisitionById(long requisitionId) throws Exception {
		return requisitionDAO.findByNamedQuery("getRequisitionById",
				requisitionId).get(0);
	}

	public List<RequisitionDetail> getRequisitionDetailsById(long requisitionId)
			throws Exception {
		return requisitionDetailDAO.findByNamedQuery(
				"getRequisitionDetailsById", requisitionId);
	}

	public RequisitionDetail requisitionDetailsFindById(long requisitionDetailId)
			throws Exception {
		return requisitionDetailDAO.findById(requisitionDetailId);
	}

	public List<RequisitionDetail> getRequisitionDetailIssuance(
			long requisitionId) throws Exception {
		return requisitionDetailDAO.findByNamedQuery(
				"getRequisitionDetailIssuance", requisitionId);
	}

	public List<RequisitionDetail> getRequisitionDetailByRequistionId(
			long requisitionId) throws Exception {
		return requisitionDetailDAO.findByNamedQuery(
				"getRequisitionDetailByRequistionId", requisitionId);
	}

	public List<RequisitionDetail> getRequisitionDetailQuotationPurchaseIssue(
			long requisitionId) throws Exception {
		return requisitionDetailDAO.findByNamedQuery(
				"getRequisitionDetailQuotationPurchaseIssue", requisitionId);
	}
	
	public Requisition getRequisitionCycle(Long requisitionId) throws Exception {
		return requisitionDAO.findByNamedQuery("getRequisitionCycle",
				requisitionId).get(0);
	}

	public void deleteRequisitionDetail(
			List<RequisitionDetail> deletedRequisitionDetails) throws Exception {
		requisitionDetailDAO.deleteAll(deletedRequisitionDetails);
	}

	public void saveRequisition(Requisition requisition,
			List<RequisitionDetail> requisitionDetails) throws Exception {
		requisitionDAO.saveOrUpdate(requisition);
		for (RequisitionDetail requisitionDetail : requisitionDetails)
			requisitionDetail.setRequisition(requisition);
		requisitionDetailDAO.saveOrUpdateAll(requisitionDetails);
	}

	public void saveRequisition(Requisition requisition) throws Exception {
		requisitionDAO.saveOrUpdate(requisition);
	}

	public void updateRequisition(Requisition requisition) throws Exception {
		requisitionDAO.merge(requisition);
	}

	public void deleteRequisition(Requisition requisition,
			List<RequisitionDetail> requisitionDetails) throws Exception {
		requisitionDetailDAO.deleteAll(requisitionDetails);
		requisitionDAO.delete(requisition);
	} 

	@SuppressWarnings("unchecked")
	public List<Requisition> getFilterRequisitionData(String fromdate,
			String todate, Long personId, Long locationId, Long productId, Implementation implementation)
			throws Exception {
		Criteria criteria = requisitionDAO.createCriteria();
		criteria.createAlias("cmpDeptLoc", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.location", "lc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.department", "dpt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("requisitionDetails", "reqdt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("reqdt.product", "prd",
				CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("implementation", implementation));
		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}
		if (locationId != null && locationId > 0) {
			criteria.add(Restrictions.eq("lc.locationId", locationId));
		}
		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("pr.personId", personId));
		}
		if (fromdate != null && !("").equals(fromdate)) {
			criteria.add(Restrictions.ge("requisitionDate",
					DateFormat.convertStringToDate(fromdate)));
		}
		if (todate != null && !("").equals(todate)) {
			criteria.add(Restrictions.le("requisitionDate",
					DateFormat.convertStringToDate(todate)));
		}
		Set<Requisition> uniqueRecord = new HashSet<Requisition>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Requisition>(
				uniqueRecord) : null;
	}

	// Getters & Setters
	public AIOTechGenericDAO<Requisition> getRequisitionDAO() {
		return requisitionDAO;
	}

	public void setRequisitionDAO(AIOTechGenericDAO<Requisition> requisitionDAO) {
		this.requisitionDAO = requisitionDAO;
	}

	public AIOTechGenericDAO<RequisitionDetail> getRequisitionDetailDAO() {
		return requisitionDetailDAO;
	}

	public void setRequisitionDetailDAO(
			AIOTechGenericDAO<RequisitionDetail> requisitionDetailDAO) {
		this.requisitionDetailDAO = requisitionDetailDAO;
	} 
}
