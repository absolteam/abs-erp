package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisitionDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductionRequisitionService {

	private AIOTechGenericDAO<ProductionRequisition> productionRequisitionDAO;
	private AIOTechGenericDAO<ProductionRequisitionDetail> productionRequisitionDetailDAO;

	public List<ProductionRequisition> getAllProductionRequisitions(
			Implementation implementation) throws Exception {
		return productionRequisitionDAO.findByNamedQuery(
				"getAllProductionRequisitions", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<ProductionRequisition> getProductionRequisitionsList(
			Implementation implementation, Byte requisitionStatus)
			throws Exception {
		return productionRequisitionDAO.findByNamedQuery(
				"getProductionRequisitionsList", implementation,
				(byte) WorkflowConstants.Status.Published.getCode(),
				requisitionStatus);
	}

	public ProductionRequisition getProductionRequisitionById(
			long productionRequisitionId) throws Exception {
		return productionRequisitionDAO.findByNamedQuery(
				"getProductionRequisitionById", productionRequisitionId).get(0);
	}
	
	public ProductionRequisition getProductionRequisitionByRequisionId(
			long productionRequisitionId) throws Exception {
		return productionRequisitionDAO.findByNamedQuery(
				"getProductionRequisitionByRequisionId",
				productionRequisitionId).get(0);
	}

	public List<ProductionRequisitionDetail> getProductionRequistionDetail(
			long productionRequisitionId) throws Exception {
		return productionRequisitionDetailDAO.findByNamedQuery(
				"getProductionRequistionDetail", productionRequisitionId);
	}

	public ProductionRequisitionDetail getProductionRequistionDetailById(
			long productionRequisitionDetailId) throws Exception {
		return productionRequisitionDetailDAO
				.findById(productionRequisitionDetailId);
	}

	public void deleteProductionRequisitionDetails(
			List<ProductionRequisitionDetail> deletedRequisitionDetails)
			throws Exception {
		productionRequisitionDetailDAO.deleteAll(deletedRequisitionDetails);
	}

	public void saveProductionRequisition(
			ProductionRequisition productionRequisition,
			List<ProductionRequisitionDetail> productionRequisitionDetails)
			throws Exception {
		productionRequisitionDAO.saveOrUpdate(productionRequisition);
		for (ProductionRequisitionDetail productionRequisitionDetail : productionRequisitionDetails)
			productionRequisitionDetail
					.setProductionRequisition(productionRequisition);
		productionRequisitionDetailDAO
				.saveOrUpdateAll(productionRequisitionDetails);
	}

	public void mergeProductionRequisition(
			ProductionRequisition productionRequisition,
			List<ProductionRequisitionDetail> productionRequisitionDetails)
			throws Exception {
		productionRequisitionDAO.merge(productionRequisition);
		for (ProductionRequisitionDetail productionRequisitionDetail : productionRequisitionDetails)
			productionRequisitionDetailDAO.merge(productionRequisitionDetail);
	}

	public void deleteProductionRequisition(
			ProductionRequisition productionRequisition,
			List<ProductionRequisitionDetail> productionRequisitionDetails)
			throws Exception {
		productionRequisitionDetailDAO.deleteAll(productionRequisitionDetails);
		productionRequisitionDAO.delete(productionRequisition);
	}

	// Getters & Setters
	public AIOTechGenericDAO<ProductionRequisition> getProductionRequisitionDAO() {
		return productionRequisitionDAO;
	}

	public void setProductionRequisitionDAO(
			AIOTechGenericDAO<ProductionRequisition> productionRequisitionDAO) {
		this.productionRequisitionDAO = productionRequisitionDAO;
	}

	public AIOTechGenericDAO<ProductionRequisitionDetail> getProductionRequisitionDetailDAO() {
		return productionRequisitionDetailDAO;
	}

	public void setProductionRequisitionDetailDAO(
			AIOTechGenericDAO<ProductionRequisitionDetail> productionRequisitionDetailDAO) {
		this.productionRequisitionDetailDAO = productionRequisitionDetailDAO;
	}
}
