/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderCharge;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.SalesDeliveryNoteService;
import com.aiotech.aios.accounts.service.SalesOrderService;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermDiscountMode;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * @author Saleem
 */
public class SalesOrderBL {

	// Dependencies
	private CustomerQuotationBL customerQuotationBL;
	private LookupMasterBL lookupMasterBL;
	private SalesOrderService salesOrderService;
	private CurrencyService currencyService;
	private StockBL stockBL;
	private SalesDeliveryNoteService salesDeliveryNoteService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private CreditTermBL creditTermBL;

	// show all sales order json list
	public List<Object> getAllSalesOrder() throws Exception {
		List<SalesOrder> salesOrders = salesOrderService
				.getAllSalesOrder(getImplementation());
		List<Object> salesOrderObject = new ArrayList<Object>();
		if (null != salesOrders && salesOrders.size() > 0) {
			SalesOrderVO salesOrderVO = null;
			for (SalesOrder salesOrder : salesOrders) {
				salesOrderVO = new SalesOrderVO();
				salesOrderVO
						.setReferenceNumber(salesOrder.getReferenceNumber());
				salesOrderVO.setSalesOrderId(salesOrder.getSalesOrderId());
				salesOrderVO.setSalesDate(DateFormat
						.convertDateToString(salesOrder.getOrderDate()
								.toString()));
				salesOrderVO
						.setShipDate(null != salesOrder.getShippingDate() ? DateFormat
								.convertDateToString(salesOrder
										.getShippingDate().toString()) : "");
				salesOrderVO.setExpirySales(DateFormat
						.convertDateToString(salesOrder.getExpiryDate()
								.toString()));
				salesOrderVO.setOrderTypeDetail(null != salesOrder
						.getLookupDetailByOrderType() ? salesOrder
						.getLookupDetailByOrderType().getDisplayName() : "");
				salesOrderVO.setCustomerName(null != salesOrder.getCustomer()
						.getPersonByPersonId() ? salesOrder
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat("")
						.concat(salesOrder.getCustomer().getPersonByPersonId()
								.getLastName()) : (null != salesOrder
						.getCustomer().getCompany() ? salesOrder.getCustomer()
						.getCompany().getCompanyName() : salesOrder
						.getCustomer().getCmpDeptLocation().getCompany()
						.getCompanyName()));
				salesOrderVO.setSalesReperesentive(null != salesOrder
						.getPerson() ? salesOrder.getPerson().getFirstName()
						.concat(" ")
						.concat(salesOrder.getPerson().getLastName()) : "");
				if (salesOrder.getSalesDeliveryNotes() != null
						&& salesOrder.getSalesDeliveryNotes().size() > 0) {
					salesOrderVO.setSalesStatus(O2CProcessStatus.Delivered
							.name());
					salesOrderVO.setDeliveryFlag(true);
					salesOrder.setOrderStatus(O2CProcessStatus.Delivered
							.getCode());
				} else {
					salesOrderVO.setSalesStatus(O2CProcessStatus
							.get(salesOrder.getOrderStatus()).name()
							.replaceAll("_", " "));
					salesOrderVO.setDeliveryFlag(false);
				}
				salesOrderVO.setDescription(salesOrder.getDescription());
				salesOrderObject.add(salesOrderVO);
			}
		}
		return salesOrderObject;
	}

	public List<Object> getUnDeliveredSalesOrder() throws Exception {
		List<SalesOrder> salesOrders = salesOrderService
				.getUnDeliveredSalesOrder(getImplementation());
		List<Object> salesOrderVOs = new ArrayList<Object>();
		if (null != salesOrders && salesOrders.size() > 0) {
			SalesOrderVO salesOrderVO = null;
			List<SalesDeliveryNote> salesDeliveryNotes = null;
			for (SalesOrder salesOrder : salesOrders) {
				double deliveryQty = 0;
				if (null != salesOrder.getContinuousSales()
						&& (boolean) salesOrder.getContinuousSales()) {
					salesDeliveryNotes = salesDeliveryNoteService
							.getSalesDeliveryBySalesOrder(salesOrder
									.getSalesOrderId());
					if (null != salesDeliveryNotes
							&& salesDeliveryNotes.size() > 0) {
						for (SalesDeliveryNote salesDeliveryNote : salesDeliveryNotes) {
							for (SalesDeliveryDetail salesDetail : salesDeliveryNote
									.getSalesDeliveryDetails()) {
								deliveryQty += salesDetail
										.getDeliveryQuantity();
							}
						}
					}
				}
				double orderQty = 0;
				for (SalesOrderDetail salesOrderDetail : salesOrder
						.getSalesOrderDetails()) {
					orderQty += salesOrderDetail.getOrderQuantity();
				}
				if (orderQty > deliveryQty) {
					salesOrderVO = new SalesOrderVO();
					salesOrderVO.setReferenceNumber(salesOrder
							.getReferenceNumber());
					salesOrderVO.setSalesOrderId(salesOrder.getSalesOrderId());
					salesOrderVO.setSalesDate(DateFormat
							.convertDateToString(salesOrder.getOrderDate()
									.toString()));
					salesOrderVO.setShipDate(null != salesOrder
							.getShippingDate() ? DateFormat
							.convertDateToString(salesOrder.getShippingDate()
									.toString()) : "");
					salesOrderVO.setExpirySales(DateFormat
							.convertDateToString(salesOrder.getExpiryDate()
									.toString()));
					salesOrderVO
							.setOrderTypeDetail(null != salesOrder
									.getLookupDetailByOrderType() ? salesOrder
									.getLookupDetailByOrderType()
									.getDisplayName() : "");
					salesOrderVO
							.setCustomerName(null != salesOrder.getCustomer()
									.getPersonByPersonId() ? salesOrder
									.getCustomer()
									.getPersonByPersonId()
									.getFirstName()
									.concat("")
									.concat(salesOrder.getCustomer()
											.getPersonByPersonId()
											.getLastName())
									: (null != salesOrder.getCustomer()
											.getCompany() ? salesOrder
											.getCustomer().getCompany()
											.getCompanyName() : salesOrder
											.getCustomer().getCmpDeptLocation()
											.getCompany().getCompanyName()));
					salesOrderVO.setSalesReperesentive(null != salesOrder
							.getPerson() ? salesOrder.getPerson()
							.getFirstName().concat(" ")
							.concat(salesOrder.getPerson().getLastName()) : "");
					salesOrderVO.setSalesStatus(O2CProcessStatus
							.get(salesOrder.getOrderStatus()).name()
							.replaceAll("_", " "));
					if (salesOrder.getCreditTerm() != null)
						salesOrderVO.setCreditTermId(salesOrder.getCreditTerm()
								.getCreditTermId());

					salesOrderVOs.add(salesOrderVO);
				}
			}
		}
		return salesOrderVOs;
	}

	// Get Sales order info
	public SalesOrderVO getSalesOrderInfo(SalesOrder salesOrder)
			throws Exception {
		double deliveryQty = 0;
		List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = null;
		if (null != salesOrder.getContinuousSales()
				&& (boolean) salesOrder.getContinuousSales()) {
			List<SalesDeliveryNote> salesDeliveryNotes = salesDeliveryNoteService
					.getSalesDeliveryBySalesOrder(salesOrder.getSalesOrderId());
			salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0) {
				for (SalesDeliveryNote salesDeliveryNote : salesDeliveryNotes) {
					for (SalesDeliveryDetail salesDetail : salesDeliveryNote
							.getSalesDeliveryDetails()) {
						deliveryQty += salesDetail.getDeliveryQuantity();
					}
				}
			}
		}
		SalesOrderVO salesOrderVO = new SalesOrderVO();
		salesOrderVO.setSalesOrderId(salesOrder.getSalesOrderId());
		salesOrderVO.setReferenceNumber(salesOrder.getReferenceNumber());
		salesOrderVO.setCustomerName(null != salesOrder.getCustomer()
				.getPersonByPersonId() ? salesOrder
				.getCustomer()
				.getPersonByPersonId()
				.getFirstName()
				.concat("")
				.concat(salesOrder.getCustomer().getPersonByPersonId()
						.getLastName()) : (null != salesOrder.getCustomer()
				.getCompany() ? salesOrder.getCustomer().getCompany()
				.getCompanyName() : salesOrder.getCustomer()
				.getCmpDeptLocation().getCompany().getCompanyName()));
		salesOrderVO.setCustomerId(salesOrder.getCustomer().getCustomerId());
		salesOrderVO
				.setShippingDetailId(null != salesOrder.getShippingDetail() ? salesOrder
						.getShippingDetail().getShippingId() : 0l);
		salesOrderVO.setCurrencyId(salesOrder.getCurrency().getCurrencyId());
		salesOrderVO.setDescription(salesOrder.getDescription());
		salesOrderVO
				.setSalesReperesentive(null != salesOrder.getPerson() ? salesOrder
						.getPerson().getFirstName().concat(" ")
						.concat(salesOrder.getPerson().getLastName())
						: "");
		salesOrderVO
				.setSalesReperesentiveId(null != salesOrder.getPerson() ? salesOrder
						.getPerson().getPersonId() : null);
		salesOrderVO.setShippingMethodId(null != salesOrder
				.getLookupDetailByShippingMethod() ? salesOrder
				.getLookupDetailByShippingMethod().getLookupDetailId() : null);
		salesOrderVO.setShippingTermsId(null != salesOrder
				.getLookupDetailByShippingTerm() ? salesOrder
				.getLookupDetailByShippingTerm().getLookupDetailId() : null);
		salesOrderVO.setModeOfPayment(salesOrder.getModeOfPayment());
		salesOrderVO.setContinuousSales(salesOrder.getContinuousSales());
		ShippingDetail shippingDetail = null;
		Set<ShippingDetail> shippingDetails = new HashSet<ShippingDetail>();
		Customer customer = new Customer();
		for (ShippingDetail shipDetail : salesOrder.getCustomer()
				.getShippingDetails()) {
			shippingDetail = new ShippingDetail();
			shippingDetail.setShippingId(shipDetail.getShippingId());
			shippingDetail.setName(shipDetail.getName());
			shippingDetails.add(shippingDetail);
		}
		customer.setShippingDetails(shippingDetails);
		salesOrderVO.setCustomer(customer);
		List<SalesOrderDetailVO> salesOrderDetails = new ArrayList<SalesOrderDetailVO>();
		List<SalesOrderCharge> salesOrderCharges = new ArrayList<SalesOrderCharge>();
		SalesOrderDetailVO salesOrderDetail = null;
		SalesOrderCharge salesOrderCharge = null;
		Product product = null;
		LookupDetail lookupDetail = null;
		List<StockVO> stocks = null;
		double remainingQty = 0;
		SalesDeliveryDetailVO salesDeliveryDetailVO = null;
		for (SalesOrderDetail orderDetail : salesOrder.getSalesOrderDetails()) {
			salesOrderDetail = new SalesOrderDetailVO();
			salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			remainingQty = 0;
			List<SalesDeliveryDetail> salesDeliveryDetails = salesDeliveryNoteService
					.getSalesDeliveryDetailByOrderDetail(orderDetail
							.getSalesOrderDetailId());
			if (null != salesDeliveryDetails && salesDeliveryDetails.size() > 0) {
				deliveryQty = 0;
				for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveryDetails) {
					salesDeliveryDetailVO = new SalesDeliveryDetailVO();
					salesDeliveryDetailVO
							.setSalesDeliveryDetailId(salesDeliveryDetail
									.getSalesDeliveryDetailId());
					salesDeliveryDetailVO.setProductName(salesDeliveryDetail
							.getProduct().getProductName());
					salesDeliveryDetailVO.setUnitCode(salesDeliveryDetail
							.getProduct().getLookupDetailByProductUnit()
							.getAccessCode());
					salesDeliveryDetailVO
							.setStoreName(null != salesDeliveryDetail
									.getShelf() ? salesDeliveryDetail
									.getShelf().getShelf().getAisle()
									.getStore().getStoreName()
									+ ">>"
									+ salesDeliveryDetail.getShelf().getShelf()
											.getAisle().getSectionName()
									+ ">>"
									+ salesDeliveryDetail.getShelf().getShelf()
											.getName()
									+ ">>"
									+ salesDeliveryDetail.getShelf().getName()
									: "");
					salesDeliveryDetailVO.setOrderQuantity(orderDetail
							.getPackageUnit());
					salesDeliveryDetailVO.setDeliveredQuantity(remainingQty);
					remainingQty += salesDeliveryDetail.getPackageUnit();
					deliveryQty += salesDeliveryDetail.getPackageUnit();
					salesDeliveryDetailVO
							.setRemainingQuantity(salesDeliveryDetailVO
									.getOrderQuantity()
									- salesDeliveryDetailVO
											.getDeliveredQuantity());
					salesDeliveryDetailVO
							.setDeliveryQuantity(salesDeliveryDetail
									.getDeliveryQuantity());
					salesDeliveryDetailVO.setPackageUnit(salesDeliveryDetail
							.getPackageUnit());
					salesDeliveryDetailVO.setUnitRate(salesDeliveryDetail
							.getUnitRate());
					salesDeliveryDetailVO.setDiscount(salesDeliveryDetail
							.getDiscount());
					if (null != salesDeliveryDetail.getIsPercentage()
							&& (boolean) salesDeliveryDetail.getIsPercentage())
						salesDeliveryDetailVO
								.setStrDiscountMode(CreditTermDiscountMode.Percentage
										.name());
					else if (null != salesDeliveryDetail.getIsPercentage()
							&& (boolean) !salesDeliveryDetail.getIsPercentage())
						salesDeliveryDetailVO
								.setStrDiscountMode(CreditTermDiscountMode.Amount
										.name());
					double totalAmount = salesDeliveryDetailVO.getPackageUnit()
							* salesDeliveryDetailVO.getUnitRate();
					if (null != salesDeliveryDetail.getDiscount()
							&& (double) salesDeliveryDetail.getDiscount() > 0) {
						if (null != salesDeliveryDetail.getIsPercentage()
								&& (boolean) salesDeliveryDetail
										.getIsPercentage()) {
							totalAmount = ((totalAmount * salesDeliveryDetail
									.getDiscount()) / 100);
						} else if (null != salesDeliveryDetail
								.getIsPercentage()
								&& (boolean) !salesDeliveryDetail
										.getIsPercentage()) {
							totalAmount -= salesDeliveryDetail.getDiscount();
						}
					}

					salesDeliveryDetailVO.setPackageUnit(salesDeliveryDetail
							.getPackageUnit());
					salesDeliveryDetailVO
							.setProductPackageVOs(customerQuotationBL
									.getPackageConversionBL()
									.getProductPackagingDetail(
											salesDeliveryDetail.getProduct()
													.getProductId()));
					if (null != salesDeliveryDetail.getProductPackageDetail()) {
						salesDeliveryDetailVO
								.setPackageDetailId(salesDeliveryDetail
										.getProductPackageDetail()
										.getProductPackageDetailId());
						boolean iflag = true;
						for (ProductPackageVO packagevo : salesDeliveryDetailVO
								.getProductPackageVOs()) {
							if (iflag) {
								for (ProductPackageDetailVO packageDtvo : packagevo
										.getProductPackageDetailVOs()) {
									if ((long) packageDtvo
											.getProductPackageDetailId() == (long) salesDeliveryDetail
											.getProductPackageDetail()
											.getProductPackageDetailId()) {
										salesDeliveryDetailVO
												.setConversionUnitName(packageDtvo
														.getConversionUnitName());
										iflag = true;
										break;
									}
								}
							} else
								break;
						}
						salesDeliveryDetailVO
								.setBaseUnitName(salesDeliveryDetail
										.getProduct()
										.getLookupDetailByProductUnit()
										.getDisplayName());
					} else {
						salesDeliveryDetailVO.setConversionUnitName("");
						salesDeliveryDetailVO.setPackageDetailId(-1l);
					}

					salesDeliveryDetailVO.setTotal(totalAmount);
					if (null != salesDeliveryDetail.getProductPackageDetail()) {
						salesDeliveryDetailVO
								.setPackageDetailId(salesDeliveryDetail
										.getProductPackageDetail()
										.getProductPackageDetailId());
						salesDeliveryDetailVO
								.setBaseUnitName(salesDeliveryDetail
										.getProduct()
										.getLookupDetailByProductUnit()
										.getDisplayName());
					} else
						salesDeliveryDetailVO.setPackageDetailId(-1l);
					salesDeliveryDetailVO
							.setProductPackageVOs(customerQuotationBL
									.getPackageConversionBL()
									.getProductPackagingDetail(
											salesDeliveryDetail.getProduct()
													.getProductId()));
					salesDeliveryDetailVOs.add(salesDeliveryDetailVO);
				}
				Collections.sort(salesDeliveryDetailVOs,
						new Comparator<SalesDeliveryDetailVO>() {
							public int compare(SalesDeliveryDetailVO o1,
									SalesDeliveryDetailVO o2) {
								return o1.getSalesDeliveryDetailId().compareTo(
										o2.getSalesDeliveryDetailId());
							}
						});
			}
			salesOrderDetail.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);
			product = new Product();
			lookupDetail = new LookupDetail();
			salesOrderDetail.setDiscount(orderDetail.getDiscount());
			salesOrderDetail.setIsPercentage(orderDetail.getIsPercentage());
			salesOrderDetail.setOrderQuantity(orderDetail.getPackageUnit());
			salesOrderDetail.setRemainingQtyQty(salesOrderDetail
					.getOrderQuantity() - deliveryQty);
			salesOrderDetail.setDeliveredQty(deliveryQty);
			salesOrderDetail.setUnitRate(orderDetail.getUnitRate());
			salesOrderDetail.setBasePrice(stockBL
					.getProductCostPrice(orderDetail.getProduct()
							.getProductPricingDetails()));
			salesOrderDetail.setStandardPrice(stockBL
					.getProductStandardPrice(orderDetail.getProduct()
							.getProductPricingDetails()));
			if (null != orderDetail.getProductPackageDetail()) {
				salesOrderDetail.setPackageDetailId(orderDetail
						.getProductPackageDetail().getProductPackageDetailId());
				salesOrderDetail.setBaseUnitName(orderDetail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());
			} else
				salesOrderDetail.setPackageDetailId(-1l);
			salesOrderDetail.setProductPackageVOs(customerQuotationBL
					.getPackageConversionBL().getProductPackagingDetail(
							orderDetail.getProduct().getProductId()));
			product.setProductId(orderDetail.getProduct().getProductId());
			product.setProductName(orderDetail.getProduct().getProductName());
			product.setCostingType(orderDetail.getProduct().getCostingType());
			salesOrderDetail.setUnitCode(orderDetail.getProduct()
					.getLookupDetailByProductUnit().getAccessCode());
			salesOrderDetail.setSalesOrderDetailId(orderDetail
					.getSalesOrderDetailId());
			salesOrderDetail.setPackageUnit(salesOrderDetail
					.getRemainingQtyQty());
			salesOrderDetail.setProductPackageVOs(customerQuotationBL
					.getPackageConversionBL().getProductPackagingDetail(
							orderDetail.getProduct().getProductId()));
			if (null != orderDetail.getProductPackageDetail()) {
				salesOrderDetail.setPackageDetailId(orderDetail
						.getProductPackageDetail().getProductPackageDetailId());
 				salesOrderDetail
						.setBaseQuantity(AIOSCommons
								.convertExponential(customerQuotationBL
										.getPackageConversionBL()
										.getPackageBaseQuantity(
												orderDetail
														.getProductPackageDetail()
														.getProductPackageDetailId(),
												salesOrderDetail
														.getRemainingQtyQty())));
				boolean iflag = true;
				for (ProductPackageVO packagevo : salesOrderDetail
						.getProductPackageVOs()) {
					if (iflag) {
						for (ProductPackageDetailVO packageDtvo : packagevo
								.getProductPackageDetailVOs()) {
							if ((long) packageDtvo.getProductPackageDetailId() == (long) orderDetail
									.getProductPackageDetail()
									.getProductPackageDetailId()) {
								salesOrderDetail
										.setConversionUnitName(packageDtvo
												.getConversionUnitName());
								iflag = true;
								break;
							}
						}
					} else
						break;
				}
				salesOrderDetail.setBaseUnitName(orderDetail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());
			} else {
				salesOrderDetail.setBaseQuantity(String.valueOf(salesOrderDetail
						.getRemainingQtyQty()));
				salesOrderDetail.setConversionUnitName("");
				salesOrderDetail.setPackageDetailId(-1l);
			}

			if (((byte) InventorySubCategory.Craft0Manufacturer.getCode() != (byte) orderDetail
					.getProduct().getItemSubType())
					&& ((byte) InventorySubCategory.Services.getCode() != (byte) orderDetail
							.getProduct().getItemSubType())) {
				stocks = stockBL.getProductStockFullDetails(product
						.getProductId());
				if (null != stocks && stocks.size() > 0) {
					double availableStock = 0;
					Map<Integer, StockVO> shelfs = new HashMap<Integer, StockVO>();
					for (StockVO stockVO : stocks) {
						availableStock += stockVO.getAvailableQuantity();
						shelfs.put(stockVO.getShelf().getShelfId(), stockVO);
					}
					if (null != shelfs && shelfs.size() == 1) {
						List<StockVO> tempStockVO = new ArrayList<StockVO>(
								shelfs.values());
						stockBL.getStoreBL().getStoreService().getShelfDAO()
								.evict(tempStockVO.get(0).getShelf());
						stockBL.getStoreBL()
								.getStoreService()
								.getShelfDAO()
								.evict(tempStockVO.get(0).getShelf().getShelf());
						stockBL.getStoreBL()
								.getStoreService()
								.getAisleDAO()
								.evict(tempStockVO.get(0).getShelf().getAisle());
						stockBL.getStoreBL().getStoreService().getStoreDAO()
								.evict(tempStockVO.get(0).getStore());
						Shelf shelf = stockBL
								.getStoreBL()
								.getStoreService()
								.getStoreByShelfId(
										tempStockVO.get(0).getShelf()
												.getShelfId());
						salesOrderDetail.setStoreName(shelf.getShelf()
								.getAisle().getStore().getStoreName()
								+ ">>"
								+ shelf.getShelf().getAisle().getSectionName()
								+ ">>"
								+ shelf.getShelf().getName()
								+ ">>"
								+ shelf.getName());
						salesOrderDetail.setStoreId(tempStockVO.get(0)
								.getStore().getStoreId());
						salesOrderDetail.setShelfId(tempStockVO.get(0)
								.getShelf().getShelfId());
					}
					salesOrderDetail.setAvailableQty(availableStock);
					salesOrderDetail.setProductType("F");
				}
			} else {
				salesOrderDetail.setProductType("C");
				salesOrderDetail.setAvailableQty(salesOrderDetail
						.getOrderQuantity());
			}
			salesOrderDetail.setProduct(product);
			salesOrderDetails.add(salesOrderDetail);
		}
		Collections.sort(salesOrderDetails,
				new Comparator<SalesOrderDetailVO>() {
					public int compare(SalesOrderDetailVO o1,
							SalesOrderDetailVO o2) {
						return o1.getSalesOrderDetailId().compareTo(
								o2.getSalesOrderDetailId());
					}

				});
		salesOrderVO.setSalesOrderDetailVOs(salesOrderDetails);
		if (null != salesOrder.getSalesOrderCharges()
				&& salesOrder.getSalesOrderCharges().size() > 0) {
			for (SalesOrderCharge orderCharge : salesOrder
					.getSalesOrderCharges()) {
				salesOrderCharge = new SalesOrderCharge();
				lookupDetail = new LookupDetail();
				salesOrderCharge.setCharges(orderCharge.getCharges());
				salesOrderCharge.setDescription(orderCharge.getDescription());
				salesOrderCharge.setIsPercentage(orderCharge.getIsPercentage());
				lookupDetail.setLookupDetailId(orderCharge.getLookupDetail()
						.getLookupDetailId());
				lookupDetail.setDisplayName(orderCharge.getLookupDetail()
						.getDisplayName());
				salesOrderCharge.setLookupDetail(lookupDetail);
				salesOrderCharges.add(salesOrderCharge);
			}
			salesOrderVO.setSalesOrderCharges(new HashSet<SalesOrderCharge>(
					salesOrderCharges));
		}

		if (salesOrder.getSalesDeliveryNotes() != null
				&& salesOrder.getSalesDeliveryNotes().size() > 0) {
			salesOrderVO.setSalesStatus(O2CProcessStatus.Delivered.name());
			salesOrderVO.setDeliveryFlag(true);
			salesOrder.setOrderStatus(O2CProcessStatus.Delivered.getCode());
		} else {
			salesOrderVO.setSalesStatus(O2CProcessStatus
					.get(salesOrder.getOrderStatus()).name()
					.replaceAll("_", " "));
			salesOrderVO.setDeliveryFlag(false);
		}
		return salesOrderVO;
	}

	// Save Sales Order
	public void saveSalesOrder(SalesOrder salesOrder,
			List<SalesOrderDetail> salesOrderDetails,
			List<SalesOrderCharge> salesOrderCharge,
			List<SalesOrderDetail> deleteOrderDetails,
			List<SalesOrderCharge> deleteOrderCharges) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		salesOrder.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (salesOrder != null && salesOrder.getSalesOrderId() != null
				&& salesOrder.getSalesOrderId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		boolean updateFlag = false;
		if (null != salesOrder.getSalesOrderId())
			updateFlag = true;
		salesOrder.setImplementation(getImplementation());
		if (null != deleteOrderDetails && deleteOrderDetails.size() > 0)
			salesOrderService.deleteSalesOrderDetails(deleteOrderDetails);
		if (null != deleteOrderCharges && deleteOrderCharges.size() > 0)
			salesOrderService.deleteSalesOrderCharges(deleteOrderCharges);
		salesOrderService.saveSalesOrder(salesOrder, salesOrderDetails,
				salesOrderCharge);
		if (!updateFlag)
			SystemBL.saveReferenceStamp(SalesOrder.class.getName(),
					getImplementation());
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesOrder.class.getSimpleName(), salesOrder.getSalesOrderId(),
				user, workflowDetailVo);
	}

	// Save Sales Order direct
	public void saveSalesOrderDirect(SalesOrder salesOrder,
			List<SalesOrderDetail> salesOrderDetails,
			List<SalesOrderCharge> salesOrderCharge,
			List<SalesOrderDetail> deleteOrderDetails,
			List<SalesOrderCharge> deleteOrderCharges) throws Exception {
		salesOrder.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());

		boolean updateFlag = false;
		if (null != salesOrder.getSalesOrderId())
			updateFlag = true;
		salesOrder.setImplementation(getImplementation());
		if (null != deleteOrderDetails && deleteOrderDetails.size() > 0)
			salesOrderService.deleteSalesOrderDetails(deleteOrderDetails);
		if (null != deleteOrderCharges && deleteOrderCharges.size() > 0)
			salesOrderService.deleteSalesOrderCharges(deleteOrderCharges);
		salesOrderService.saveSalesOrder(salesOrder, salesOrderDetails,
				salesOrderCharge);
		if (!updateFlag)
			SystemBL.saveReferenceStamp(SalesOrder.class.getName(),
					getImplementation());

	}

	public void doSalesOrderBusinessUpdate(Long salesOrderId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				SalesOrder salesOrder = salesOrderService
						.getSalesOrderInfo(salesOrderId);
				salesOrderService.deleteSalesOrder(
						salesOrder,
						new ArrayList<SalesOrderDetail>(salesOrder
								.getSalesOrderDetails()),
						new ArrayList<SalesOrderCharge>(salesOrder
								.getSalesOrderCharges()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Sales Order
	public void deleteSalesOrder(SalesOrder salesOrder) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesOrder.class.getSimpleName(), salesOrder.getSalesOrderId(),
				user, workflowDetailVo);
		workflowDetailVo.setDeleteFlag(true);
	}

	// Delete Sales Order
	public void deleteSalesOrderDirect(SalesOrder salesOrder) throws Exception {
		salesOrderService.deleteSalesOrder(
				salesOrder,
				new ArrayList<SalesOrderDetail>(salesOrder
						.getSalesOrderDetails()),
				new ArrayList<SalesOrderCharge>(salesOrder
						.getSalesOrderCharges()));
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public Map<Byte, String> getModeOfPayment() throws Exception {
		Map<Byte, String> modeOfPayments = new HashMap<Byte, String>();
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class))
			modeOfPayments.put(modeOfPayment.getCode(), modeOfPayment.name()
					.replaceAll("_", " "));
		return modeOfPayments;
	}

	public Map<Byte, String> getO2CProcessStatus() throws Exception {
		Map<Byte, String> o2CProcessStatuses = new HashMap<Byte, String>();
		for (O2CProcessStatus o2CProcessStatus : EnumSet
				.allOf(O2CProcessStatus.class))
			o2CProcessStatuses.put(o2CProcessStatus.getCode(), o2CProcessStatus
					.name().replaceAll("_", " "));
		return o2CProcessStatuses;
	}

	// Getters and Setters
	public SalesOrderService getSalesOrderService() {
		return salesOrderService;
	}

	public void setSalesOrderService(SalesOrderService salesOrderService) {
		this.salesOrderService = salesOrderService;
	}

	public CustomerQuotationBL getCustomerQuotationBL() {
		return customerQuotationBL;
	}

	public void setCustomerQuotationBL(CustomerQuotationBL customerQuotationBL) {
		this.customerQuotationBL = customerQuotationBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public SalesDeliveryNoteService getSalesDeliveryNoteService() {
		return salesDeliveryNoteService;
	}

	public void setSalesDeliveryNoteService(
			SalesDeliveryNoteService salesDeliveryNoteService) {
		this.salesDeliveryNoteService = salesDeliveryNoteService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}
}
