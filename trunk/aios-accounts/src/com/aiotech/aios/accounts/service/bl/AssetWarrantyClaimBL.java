package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.accounts.domain.entity.vo.AssetWarrantyClaimVO;
import com.aiotech.aios.accounts.service.AssetWarrantyClaimService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class AssetWarrantyClaimBL {

	// Dependencies
	private AssetWarrantyClaimService assetWarrantyClaimService;
	private LookupMasterBL lookupMasterBL;
	private AlertBL alertBL;
	private DirectPaymentBL directPaymentBL;

	// Show All Asset Check Outs
	public List<Object> showAllAssetWarrantyClaim() throws Exception {
		List<AssetWarrantyClaim> assetWarrantyClaims = assetWarrantyClaimService
				.getAllAssetWarrantyClaim(getImplementation());
		List<Object> assetWarrantyClaimVOs = new ArrayList<Object>();
		if (null != assetWarrantyClaims && assetWarrantyClaims.size() > 0) {
			for (AssetWarrantyClaim assetWarrantyClaim : assetWarrantyClaims)
				assetWarrantyClaimVOs
						.add(addAssetClaimWarrantyVO(assetWarrantyClaim));
		}
		return assetWarrantyClaimVOs;
	}

	// Save Asset Warranty Claim
	public void saveAssetWarrantyClaim(AssetWarrantyClaim assetWarrantyClaim)
			throws Exception {
		boolean updateFlag = false;
		assetWarrantyClaim.setImplementation(getImplementation());
		if (null != assetWarrantyClaim.getAssetWarrantyClaimId())
			updateFlag = true;
		if (!updateFlag) {
			SystemBL.saveReferenceStamp(AssetWarrantyClaim.class.getName(),
					getImplementation());
		}
		assetWarrantyClaimService.saveAssetWarrantyClaim(assetWarrantyClaim);
		// Alert
		LocalDate endDate = new DateTime(assetWarrantyClaim.getClaimDate())
				.toLocalDate();
		LocalDate currentDate = DateTime.now().toLocalDate();
		if (endDate.compareTo(currentDate) <= 0)
			createClaimWarrantyAlert(assetWarrantyClaim, updateFlag);
	}

	private void createClaimWarrantyAlert(
			AssetWarrantyClaim assetWarrantyClaim, boolean updateFlag)
			throws Exception {
		if (updateFlag) {
			updateFlag = false;
			Alert alert = alertBL.getAlertService().getAlertRecordInfo(
					assetWarrantyClaim.getAssetWarrantyClaimId(),
					AssetWarrantyClaim.class.getSimpleName());
			if (null != alert) {
				alert.setIsActive(true);
				alertBL.commonSaveAlert(alert);
				updateFlag = true;
			}
			if (assetWarrantyClaim.getClaimExpense() > 0) {
				Alert alertExp = alertBL.getAlertService().getAlertRecordInfo(
						assetWarrantyClaim.getAssetWarrantyClaimId(),
						AssetWarrantyClaim.class.getSimpleName());
				if (null != alertExp) {
					alertExp.setIsActive(true);
					alertBL.commonSaveAlert(alertExp);
				}
			}
		}
		if (!updateFlag) {
			List<AssetWarrantyClaim> assetWarrantyClaims = new ArrayList<AssetWarrantyClaim>();
			assetWarrantyClaims.add(assetWarrantyClaim);
			alertBL.alertClaimWarranty(assetWarrantyClaims);
			if (assetWarrantyClaim.getClaimExpense() != null
					&& assetWarrantyClaim.getClaimExpense() > 0) {
				alertBL.alertClaimWarrantyExpense(assetWarrantyClaims);
			}
		}
	}

	// Delete Asset Warranty Claim
	public void deleteAssetWarrantyClaim(AssetWarrantyClaim assetWarrantyClaim)
			throws Exception {
		directPaymentBL.deleteDirectPayment(
				assetWarrantyClaim.getAssetWarrantyClaimId(),
				AssetWarrantyClaim.class.getSimpleName());
		assetWarrantyClaimService.deleteAssetWarrantyClaim(assetWarrantyClaim);
	}

	// Add Asset Claim WarrantyVO
	private AssetWarrantyClaimVO addAssetClaimWarrantyVO(
			AssetWarrantyClaim assetWarrantyClaim) throws Exception {
		AssetWarrantyClaimVO assetWarrantyClaimVO = new AssetWarrantyClaimVO();
		assetWarrantyClaimVO.setAssetWarrantyClaimId(assetWarrantyClaim
				.getAssetWarrantyClaimId());
		assetWarrantyClaimVO.setAssetName(assetWarrantyClaim.getAssetWarranty()
				.getAssetCreation().getAssetName());
		assetWarrantyClaimVO.setDate(DateFormat
				.convertDateToString(assetWarrantyClaim.getClaimDate()
						.toString()));
		assetWarrantyClaimVO.setWarrantyNumber(assetWarrantyClaim
				.getAssetWarranty().getWarrantyNumber());
		assetWarrantyClaimVO.setClaimReference(assetWarrantyClaim
				.getClaimReference());
		assetWarrantyClaimVO
				.setClaimAmount(assetWarrantyClaim.getClaimAmount());
		assetWarrantyClaimVO.setStatusName(Constants.Accounts.POStatus.get(
				assetWarrantyClaim.getStatus()).name());
		assetWarrantyClaimVO.setPayOutNature(null != assetWarrantyClaim
				.getLookupDetail() ? assetWarrantyClaim.getLookupDetail()
				.getDisplayName() : "");
		assetWarrantyClaimVO.setPaymentMode(null != assetWarrantyClaim
				.getModeOfPayment() ? Constants.Accounts.PaymentRequestMode
				.get(assetWarrantyClaim.getModeOfPayment()).name() : "");
		return assetWarrantyClaimVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetWarrantyClaimService getAssetWarrantyClaimService() {
		return assetWarrantyClaimService;
	}

	public void setAssetWarrantyClaimService(
			AssetWarrantyClaimService assetWarrantyClaimService) {
		this.assetWarrantyClaimService = assetWarrantyClaimService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}
}
