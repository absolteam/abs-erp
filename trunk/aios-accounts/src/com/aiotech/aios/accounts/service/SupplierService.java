package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class SupplierService {
	private AIOTechGenericDAO<Supplier> supplierDAO;

	public List<Supplier> getAllSuppliers(Implementation implementation)
			throws Exception {
		return supplierDAO.findByNamedQuery("getAllSupplierDetails",
				implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<Supplier> getSuppliersByReceipts(Implementation implementation)
			throws Exception {
		return supplierDAO.findByNamedQuery("getSuppliersByReceipts",
				implementation);
	}

	public Supplier getSupplierById(long supplierId) throws Exception {
		return supplierDAO.findByNamedQuery("getSupplierById", supplierId).get(
				0);
	}
	
	public Supplier getSupplierCombination(long supplierId) throws Exception {
		return supplierDAO.findByNamedQuery("getSupplierCombination", supplierId).get(
				0);
	}

	public void deleteSupplier(Supplier supplier) throws Exception {
		supplierDAO.delete(supplier);
	}

	public void save(Supplier supplier) throws Exception {
		supplierDAO.saveOrUpdate(supplier);
	}

	@SuppressWarnings("unchecked")
	public List<Supplier> getSuppliersByDifferentValues(Long supplierId,
			Implementation implementation) throws Exception {

		Criteria supplierCriteria = supplierDAO.createCriteria();

		/* if (isPerson) { */
		//supplierCriteria.setFetchMode("person", FetchMode.EAGER);
		supplierCriteria.createAlias("person", "person", CriteriaSpecification.LEFT_JOIN);
		/* } else { */
		//supplierCriteria.setFetchMode("company", FetchMode.EAGER);
		supplierCriteria.createAlias("company", "company", CriteriaSpecification.LEFT_JOIN);
		/* } */

		if (supplierId != null) {
			supplierCriteria.add(Restrictions.eq("supplierId", supplierId));
		}

		if (implementation != null) {
			supplierCriteria.add(Restrictions.eq("implementation",
					implementation));
		}

		/*Criteria cmpDeptLocCriteria = supplierCriteria
				.createCriteria("cmpDeptLocation");

		cmpDeptLocCriteria.setFetchMode("location", FetchMode.EAGER);
		cmpDeptLocCriteria.setFetchMode("company", FetchMode.EAGER);

		supplierCriteria.setFetchMode("paymentTerm", FetchMode.EAGER);*/
		
		supplierCriteria.createAlias("cmpDeptLocation", "cdl", CriteriaSpecification.LEFT_JOIN);
		supplierCriteria.createAlias("cdl.location", "loc", CriteriaSpecification.LEFT_JOIN);
		supplierCriteria.createAlias("cdl.company", "cdl.cmp", CriteriaSpecification.LEFT_JOIN);
		supplierCriteria.createAlias("creditTerm", "ct", CriteriaSpecification.LEFT_JOIN);
		supplierCriteria.createAlias("payments", "pay", CriteriaSpecification.LEFT_JOIN);
		supplierCriteria.createAlias("pay.paymentDetails", "payd", CriteriaSpecification.LEFT_JOIN);
		supplierCriteria.createAlias("directPayments", "dpay", CriteriaSpecification.LEFT_JOIN);

		return supplierCriteria.list();

	}

	@SuppressWarnings("unchecked")
	public List<Supplier> getAllSupplierStatement(
			Implementation implementation, Long supplierId, Date fromDate,
			Date toDate) throws Exception {

		Criteria criteria = supplierDAO.createCriteria();

		criteria.createAlias("combination", "c",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("c.transactionDetails", "td",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByCompanyAccountId", "ac1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByNaturalAccountId", "ac2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByCostcenterAccountId", "ac3",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.accountByAnalysisAccountId", "ac4",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("td.transaction", "t",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("t.currency", "cur",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (supplierId != null) {
			criteria.add(Restrictions.eq("supplierId", supplierId));
		}

		DetachedCriteria dc = DetachedCriteria
				.forClass(TransactionDetail.class);

		dc.setProjection(Projections.distinct(Projections
				.property("combination.combinationId")));

		Disjunction disjunction = Restrictions.disjunction();

		if (fromDate != null && toDate != null) {
			disjunction.add(Restrictions.and(Property
					.forName("c.combinationId").in(dc), Restrictions.between(
					"t.transactionTime", fromDate, toDate)));
			criteria.add(disjunction);
		} else {
			disjunction.add(Property.forName("c.combinationId").in(dc));
			criteria.add(disjunction);
		}

		return criteria.list();

		// return productDAO.findByNamedQuery("getAllInventoryStatement",
		// implementation);
	}
	
	@SuppressWarnings("unchecked")
	public List<Supplier> getSupplierByCombination(Set<Long> combinationIds) {
		Criteria criteria = supplierDAO.createCriteria();
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("cb.combinationId", combinationIds));
		Set<Supplier> uniqueRecord = new HashSet<Supplier>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Supplier>(
				uniqueRecord) : null;
	}

	public List<Supplier> getSupplierPaymentsDetails(
			Implementation implementation) throws Exception {
		return supplierDAO.findByNamedQuery("getSupplierPaymentsDetails",
				implementation);
	}

	public List<Supplier> getSupplierPaymentsDetailsBySupplierId(
			Implementation implementation, Long supplierId) throws Exception {
		return supplierDAO.findByNamedQuery(
				"getSupplierPaymentsDetailsBySupplierId", implementation,
				supplierId);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getSupplierPaymentsTotals(
			Implementation implementation) throws Exception {

		Query query = supplierDAO.getNamedQuery("getSupplierPaymentsTotals");
		query.setParameter(0, implementation);

		return query.list();
	}

	@SuppressWarnings({ "deprecation", "static-access", "unchecked" })
	public List<Supplier> getSupplierHistory(Implementation implementation)
			throws Exception {
		Criteria criteria = supplierDAO.createCriteria();

		criteria.setFetchMode("receives", FetchMode.EAGER)
				.setFetchMode("receives.receiveDetails", FetchMode.EAGER)
				.setFetchMode("receives.receiveDetails.product",
						FetchMode.EAGER);

		criteria.setFetchMode("goodsReturns", FetchMode.EAGER)
				.setFetchMode("goodsReturns.goodsReturnDetails",
						FetchMode.EAGER)
				.setFetchMode("goodsReturns.goodsReturnDetails.product",
						FetchMode.EAGER);

		criteria.setFetchMode("debits", FetchMode.EAGER);

		criteria.setFetchMode("person", FetchMode.EAGER);

		Criteria cmpDeptLocCriteria = criteria
				.createCriteria("cmpDeptLocation");

		cmpDeptLocCriteria.setFetchMode("company", FetchMode.EAGER);

		criteria.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	public AIOTechGenericDAO<Supplier> getSupplierDAO() {
		return supplierDAO;
	}

	public void setSupplierDAO(AIOTechGenericDAO<Supplier> supplierDAO) {
		this.supplierDAO = supplierDAO;
	} 
}
