package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.CollateralDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CollateralService {
	private AIOTechGenericDAO<CollateralDetail> collateralDAO;

	public AIOTechGenericDAO<CollateralDetail> getCollateralDAO() {
		return collateralDAO;
	}
	
	public void saveCollateral(List<CollateralDetail> collateralDetails)throws Exception{
		collateralDAO.saveOrUpdateAll(collateralDetails);
	}
	
	public void deleteCollateral(CollateralDetail collateral)throws Exception{
		collateralDAO.delete(collateral);
	}
	
	public CollateralDetail getCollateralById(Long collateralId) throws Exception{
		return collateralDAO.findByNamedQuery("getCollateralById", collateralId).get(0);
	}
	
	public List<CollateralDetail> getCollateralListById(Long loanId) throws Exception{
		return collateralDAO.findByNamedQuery("getCollateralListById", loanId);
	}
	  
	public void setCollateralDAO(AIOTechGenericDAO<CollateralDetail> collateralDAO) {
		this.collateralDAO = collateralDAO;
	}
	
	public List<CollateralDetail> getAllCollateral(Implementation implementation) throws Exception{
		return collateralDAO.findByNamedQuery("getAllCollateral", implementation);
	}
}
