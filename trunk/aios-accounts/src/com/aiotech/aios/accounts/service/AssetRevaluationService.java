package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetRevaluationService {

	private AIOTechGenericDAO<AssetRevaluation> assetRevaluationDAO;

	// Get all Asset Reevaluation
	public List<AssetRevaluation> getAllAssetRevaluation(
			Implementation implementation) throws Exception {
		return assetRevaluationDAO.findByNamedQuery("getAllAssetRevaluation",
				implementation);
	}

	public AssetRevaluation getAssetRevaluationById(long assetRevaluationId) {
		return assetRevaluationDAO.findById(assetRevaluationId);
	}

	// Save Asset Reevaluation
	public void saveAssetRevaluation(AssetRevaluation assetRevaluation)
			throws Exception {
		assetRevaluationDAO.saveOrUpdate(assetRevaluation);
	}

	public void deleteRevaluation(AssetRevaluation assetRevaluation)
			throws Exception {
		assetRevaluationDAO.delete(assetRevaluation);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetRevaluation> getAssetRevaluationDAO() {
		return assetRevaluationDAO;
	}

	public void setAssetRevaluationDAO(
			AIOTechGenericDAO<AssetRevaluation> assetRevaluationDAO) {
		this.assetRevaluationDAO = assetRevaluationDAO;
	}
}