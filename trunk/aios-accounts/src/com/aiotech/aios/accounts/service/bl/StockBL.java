/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.StockPeriodic;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.StockService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.CostingType;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;

/**
 * 
 * @author Saleem
 */
public class StockBL {

	private StockService stockService;
	private ProductPricingBL productPricingBL;
	private StoreBL storeBL;
	private PackageConversionBL packageConversionBL;

	// Show Asset Stock Detail
	public List<Object> showProductStockDetail(long productId) throws Exception {
		List<Stock> stocks = stockService.getProductRequisitionStock(productId);
		List<Object> stockObjects = new ArrayList<Object>();
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			for (StockVO stockVO : stockVOs) {
				if (null != stockVO.getAvailableQuantity()
						&& (double) stockVO.getAvailableQuantity() > 0)
					stockObjects.add(addStockObject(stockVO));
			}

		}
		return stockObjects;
	}

	// Show Asset Stock Detail
	public List<Object> showProductStockDetail(char itemType) throws Exception {
		List<Stock> stocks = stockService.getProductStockDetail(
				getImplementation(), itemType);
		List<Object> stockObjects = new ArrayList<Object>();
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs) {
					if (null != stockVO.getAvailableQuantity()
							&& (double) stockVO.getAvailableQuantity() > 0)
						stockObjects.add(addStockObject(stockVO));
				}
			}
		}
		if (itemType == 'I') {
			List<Product> craftServiceProducts = productPricingBL
					.getProductBL()
					.getProductService()
					.getCraftProducts(getImplementation(),
							InventorySubCategory.Craft0Manufacturer.getCode(),
							InventorySubCategory.Services.getCode());
			if (null != craftServiceProducts && craftServiceProducts.size() > 0) {
				for (Product product : craftServiceProducts)
					stockObjects.add(addProductStockObject(product));
			}
		}
		return stockObjects;
	}

	// Show Asset Stock Detail
	public List<Object> showNonCraftProductStockDetails(char itemType)
			throws Exception {
		List<Stock> stocks = stockService.getProductStockDetail(
				getImplementation(), itemType);
		List<Object> stockObjects = new ArrayList<Object>();
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs) {
					if (null != stockVO.getAvailableQuantity()
							&& (double) stockVO.getAvailableQuantity() > 0)
						stockObjects.add(addStockObject(stockVO));
				}
			}
		}
		return stockObjects;
	}

	// Show Asset Stock Detail
	public List<Object> showProductStockDetailByProduct() throws Exception {
		List<Product> productList = productPricingBL.getProductBL()
				.getProductService()
				.getAllProductForStockCheckUp(getImplementation());
		List<StockVO> stockObjects = new ArrayList<StockVO>();
		List<Object> stockFinalVOs = new ArrayList<Object>();
		List<Stock> stocks = stockService
				.getProductStockDetail(getImplementation());
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs)
					stockObjects.add(addStockObject(stockVO));
			}
		}
		// Convert StockVo into map
		Map<Long, List<StockVO>> maps = new HashMap<Long, List<StockVO>>();
		List<StockVO> tempStockVOs = null;
		for (StockVO stockVO : stockObjects) {
			if (maps != null && maps.containsKey(stockVO.getProductId())) {
				tempStockVOs = maps.get(stockVO.getProductId());
				tempStockVOs.add(stockVO);
				maps.put(stockVO.getProductId(), tempStockVOs);
			} else {
				tempStockVOs = new ArrayList<StockVO>();
				tempStockVOs.add(stockVO);
				maps.put(stockVO.getProductId(), tempStockVOs);
			}
		}
		StockVO stockVO = null;
		double sellingPrice = 0;
		for (Product product : productList) {
			sellingPrice = 0;
			if (maps != null && maps.containsKey(product.getProductId())) {
				tempStockVOs = maps.get(product.getProductId());
				for (StockVO stockVO1 : tempStockVOs) {
					stockVO = stockVO1;
					stockVO.setCategoryName(null != product
							.getProductCategory() ? product
							.getProductCategory().getProductCategory()
							.getCategoryName() : "");
					stockVO.setSubCategoryName(null != product
							.getProductCategory() ? product
							.getProductCategory().getCategoryName() : "");
					stockVO.setProductId(product.getProductId());
					if (null != product.getProductPricingDetails()
							&& product.getProductPricingDetails().size() > 0) {
						for (ProductPricingDetail productPricingDetail : product
								.getProductPricingDetails()) {
							if (null != productPricingDetail.getStatus()
									&& productPricingDetail.getStatus()) {
								sellingPrice = productPricingDetail
										.getSellingPrice();
								break;
							}
						}
						stockVO.setSellingPrice(sellingPrice);
					}
					stockFinalVOs.add(stockVO);
				}
			} else {
				stockVO = new StockVO();
				stockVO.setProductName(product.getProductName());
				stockVO.setCategoryName(null != product.getProductCategory() ? product
						.getProductCategory().getProductCategory()
						.getCategoryName()
						: "");
				stockVO.setSubCategoryName(null != product.getProductCategory() ? product
						.getProductCategory().getCategoryName() : "");
				stockVO.setProductUnit(product.getLookupDetailByProductUnit()
						.getDisplayName());
				stockVO.setProductId(product.getProductId());
				if (product.getShelf() != null
						&& product.getShelf().getShelf() != null) {
					stockVO.setStoreName(product.getShelf().getShelf()
							.getAisle().getStore().getStoreName()
							+ " >> "
							+ product.getShelf().getShelf().getAisle()
									.getSectionName()
							+ " >> "
							+ product.getShelf().getShelf().getName()
							+ " >> "
							+ product.getShelf().getName());
				}
				stockVO.setProductCode(product.getCode());
				if (null != product.getProductPricingDetails()
						&& product.getProductPricingDetails().size() > 0) {
					for (ProductPricingDetail productPricingDetail : product
							.getProductPricingDetails()) {
						if (null != productPricingDetail.getStatus()
								&& productPricingDetail.getStatus()) {
							sellingPrice = productPricingDetail
									.getSellingPrice();
							break;
						}
					}
					stockVO.setSellingPrice(sellingPrice);
				}

				stockFinalVOs.add(stockVO);
			}
		}
		return stockFinalVOs;
		/*
		 * List<Object> stocksVOFinal = new ArrayList<Object>(); if (null !=
		 * stockFinalVOs && stockFinalVOs.size() > 0) {
		 * List<ProductPackageDetail> productPackageDetails = null; StockVO
		 * stockvo = null; for (StockVO stock : stockFinalVOs) {
		 * stocksVOFinal.add(stock); productPackageDetails = packageConversionBL
		 * .getProductPackageDetails(stock.getProductId()); if (null !=
		 * productPackageDetails && productPackageDetails.size() > 0) { for
		 * (ProductPackageDetail productPackageDetail : productPackageDetails) {
		 * stockvo = new StockVO(); BeanUtils.copyProperties(stockvo, stock);
		 * stockvo.setAvailableQuantity(AIOSCommons
		 * .roundThreeDecimals(productPackageDetail .getUnitQuantity()
		 * stock.getAvailableQuantity()));
		 * stockvo.setProductUnit(productPackageDetail
		 * .getLookupDetail().getDisplayName()); stocksVOFinal.add(stockvo); } }
		 * } }
		 */
	}

	// Show Asset Stock Detail
	public List<?> showStockDetailByProducts() throws Exception {
		List<Object> stockFinalVOs = new ArrayList<Object>();
		List<Stock> stocks = stockService
				.getProductStockDetail(getImplementation());
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs)
					stockFinalVOs.add(addStockObject(stockVO));
			}
		}
		List<Product> craftServiceProducts = productPricingBL
				.getProductBL()
				.getProductService()
				.getCraftProducts(getImplementation(),
						InventorySubCategory.Craft0Manufacturer.getCode(),
						InventorySubCategory.Services.getCode());
		if (null != craftServiceProducts && craftServiceProducts.size() > 0) {
			for (Product product : craftServiceProducts)
				stockFinalVOs.add(addProductStockObject(product));
		}
		return stockFinalVOs;
	}

	// Show Asset Stock Detail
	public List<Object> showProductStockDetail() throws Exception {
		List<Stock> stocks = stockService
				.getProductStockDetail(getImplementation());
		List<Object> stockObjects = new ArrayList<Object>();
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs) {
					if (stockVO.getAvailableQuantity() != null
							&& stockVO.getAvailableQuantity() > 0) {
						stockObjects.add(addStockObject(stockVO));
					}
				}
			}
		}
		List<Product> craftServiceProducts = productPricingBL
				.getProductBL()
				.getProductService()
				.getCraftProducts(getImplementation(),
						InventorySubCategory.Craft0Manufacturer.getCode(),
						InventorySubCategory.Services.getCode());
		if (null != craftServiceProducts && craftServiceProducts.size() > 0) {
			for (Product product : craftServiceProducts)
				stockObjects.add(addProductStockObject(product));
		}
		return stockObjects;
	}

	// Show Asset Stock Detail
	public List<Object> showNonCraftProductStockDetails() throws Exception {
		List<Stock> stocks = stockService
				.getProductStockDetail(getImplementation());
		List<Object> stockObjects = new ArrayList<Object>();
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs) {
					if (stockVO.getAvailableQuantity() != null
							&& stockVO.getAvailableQuantity() > 0) {
						stockObjects.add(addStockObject(stockVO));
					}
				}
			}
		}
		return stockObjects;
	}

	// Save Stock info
	public void saveStock(Stock stock) throws Exception {
		stockService.saveStock(stock);
	}

	// Save Stock info
	public void updateStockDetails(List<Stock> stocks, List<Stock> deleteStocks)
			throws Exception {

		// Delete Stock
		if (null != deleteStocks && deleteStocks.size() > 0) {
			List<Stock> updateStocks = null;
			double availableQuantity;
			double deleteQuantity;
			List<Stock> updatedStocks = new ArrayList<Stock>();
			Product product = null;
			for (Stock stock : deleteStocks) {
				product = productPricingBL.getProductBL().getProductService()
						.getBasicProductById(stock.getProduct().getProductId());
				if (((byte) product.getItemSubType() != (byte) InventorySubCategory.Services
						.getCode())
						|| ((byte) product.getItemSubType() != (byte) InventorySubCategory.Craft0Manufacturer
								.getCode())) {
					if (null != stock.getShelf()) {
						updateStocks = stockService.getStockFullDetails(stock
								.getProduct().getProductId(), stock.getShelf()
								.getShelfId());
					} else {
						updateStocks = stockService.getStockByStoreDetails(
								stock.getProduct().getProductId(), stock
										.getStore().getStoreId());
					}
					if (null != updateStocks && updateStocks.size() > 0) {
						updateStocks = orderStockByCostingType(updateStocks,
								stock.getProduct());
						availableQuantity = 0;
						deleteQuantity = stock.getQuantity();
						for (Stock orderStock : updateStocks) {
							availableQuantity = orderStock.getQuantity();
							if (availableQuantity > 0) {
								if (deleteQuantity <= availableQuantity) {
									orderStock.setQuantity(availableQuantity
											- deleteQuantity);
									updatedStocks.add(orderStock);
									break;
								} else {
									orderStock.setQuantity(0d);
									deleteQuantity = deleteQuantity
											- availableQuantity;
									updatedStocks.add(orderStock);
								}
							}
						}
					}
				}
			}
			if (null != updatedStocks && updatedStocks.size() > 0)
				stockService.updateStockDetails(updatedStocks);
		}

		if (null != stocks && stocks.size() > 0) {
			Set<Long> productIds = new HashSet<Long>();
			Map<String, Stock> stockMap = new HashMap<String, Stock>();
			Stock carryStock = null;
			String key = "";
			Product product = null;
			for (Stock stock : stocks) {
				product = productPricingBL.getProductBL().getProductService()
						.getBasicProductById(stock.getProduct().getProductId());
				if (((byte) product.getItemSubType() != (byte) InventorySubCategory.Services
						.getCode())
						|| ((byte) product.getItemSubType() != (byte) InventorySubCategory.Craft0Manufacturer
								.getCode())) {
					if (null != stock.getShelf())
						key = product
								.getProductId()
								.toString()
								.concat("-")
								.concat(stock.getShelf().getShelfId()
										.toString());
					else
						key = product.getProductId().toString();
					if (null != stockMap && stockMap.size() > 0) {
						if (stockMap.containsKey(key)) {
							carryStock = new Stock();
							carryStock = stockMap.get(key);
							carryStock.setQuantity(carryStock.getQuantity()
									+ stock.getQuantity());
							stockMap.put(key, carryStock);
						} else
							stockMap.put(key, stock);
					} else
						stockMap.put(key, stock);
					productIds.add(product.getProductId());
				}
			}
			if (null != productIds && productIds.size() > 0) {
				List<Stock> currentStocks = new ArrayList<Stock>(
						stockMap.values());
				Long[] productIdArray = productIds.toArray(new Long[productIds
						.size()]);

				List<Stock> availableStocks = stockService
						.getStockByProducts(productIdArray);

				List<Stock> updateStocks = new ArrayList<Stock>();
				if (null != availableStocks && availableStocks.size() > 0) {
					key = "";
					stockMap = new HashMap<String, Stock>();
					for (Stock stock : availableStocks) {
						if (null != stock.getShelf())
							key = stock
									.getProduct()
									.getProductId()
									.toString()
									.concat("-")
									.concat(stock.getShelf().getShelfId()
											.toString());
						else
							key = stock.getProduct().getProductId().toString();
						stockMap.put(key, stock);
					}
					Stock tempStock = null;
					for (Stock stock : currentStocks) {
						if (null != stock.getShelf())
							key = stock
									.getProduct()
									.getProductId()
									.toString()
									.concat("-")
									.concat(stock.getShelf().getShelfId()
											.toString());
						else
							key = stock.getProduct().getProductId().toString();
						if (stockMap.containsKey(key)) {
							tempStock = new Stock();
							tempStock = stockMap.get(key);
							tempStock.setQuantity(tempStock.getQuantity()
									+ stock.getQuantity());
							updateStocks.add(tempStock);
						} else {
							updateStocks.add(stock);
						}
					}
				} else
					updateStocks.addAll(currentStocks);
				stockService.updateStockDetails(updateStocks);
			}
		}
	}

	public void updateBatchStockDetails(List<Stock> stocks) throws Exception {
		if (null != stocks && stocks.size() > 0) {
			Set<Long> productIds = new HashSet<Long>();
			Map<String, Stock> stockMap = new HashMap<String, Stock>();
			Stock carryStock = null;
			String key = "";
			Product product = null;
			LocalDate productExpiry = null;
			for (Stock stock : stocks) {
				product = productPricingBL.getProductBL().getProductService()
						.getBasicProductById(stock.getProduct().getProductId());
				productExpiry = null != stock.getProductExpiry() ? new DateTime(
						stock.getProductExpiry()).toLocalDate() : null;
				if (((byte) product.getItemSubType() != (byte) InventorySubCategory.Services
						.getCode())
						&& ((byte) product.getItemSubType() != (byte) InventorySubCategory.Craft0Manufacturer
								.getCode())) {
					if (null != stock.getShelf())
						key = (product.getProductId().toString().concat("-")
								.concat(stock
										.getShelf()
										.getShelfId()
										.toString()
										.concat((null != stock.getBatchNumber()
												&& !("").equals(stock
														.getBatchNumber()) ? "-"
												+ stock.getBatchNumber()
												: ""))
										.concat(null != productExpiry ? "-"
												+ productExpiry : ""))).trim();
					else
						key = (product
								.getProductId()
								.toString()
								.concat((null != stock.getBatchNumber()
										&& !("").equals(stock.getBatchNumber()) ? "-"
										+ stock.getBatchNumber()
										: ""))
								.concat(null != productExpiry ? "-"
										+ productExpiry : "")).trim();
					if (null != stockMap && stockMap.size() > 0) {
						if (stockMap.containsKey(key)) {
							carryStock = new Stock();
							carryStock = stockMap.get(key);
							carryStock.setQuantity(carryStock.getQuantity()
									+ stock.getQuantity());
							stockMap.put(key, carryStock);
						} else
							stockMap.put(key, stock);
					} else
						stockMap.put(key, stock);
					productIds.add(product.getProductId());
				}
			}
			if (null != productIds && productIds.size() > 0) {
				List<Stock> currentStocks = new ArrayList<Stock>(
						stockMap.values());
				Long[] productIdArray = productIds.toArray(new Long[productIds
						.size()]);

				List<Stock> availableStocks = stockService
						.getStockByProducts(productIdArray);

				List<Stock> updateStocks = new ArrayList<Stock>();
				if (null != availableStocks && availableStocks.size() > 0) {
					key = "";
					stockMap = new HashMap<String, Stock>();
					for (Stock stock : availableStocks) {
						productExpiry = null != stock.getProductExpiry() ? new DateTime(
								stock.getProductExpiry()).toLocalDate() : null;
						if (null != stock.getShelf())
							key = (stock.getProduct().getProductId().toString()
									.concat("-").concat(stock
									.getShelf()
									.getShelfId()
									.toString()
									.concat((null != stock.getBatchNumber()
											&& !("").equals(stock
													.getBatchNumber()) ? "-"
											+ stock.getBatchNumber() : ""))
									.concat(null != productExpiry ? "-"
											+ productExpiry : ""))).trim();
						else
							key = (stock
									.getProduct()
									.getProductId()
									.toString()
									.concat((null != stock.getBatchNumber()
											&& !("").equals(stock
													.getBatchNumber()) ? "-"
											+ stock.getBatchNumber() : ""))
									.concat(null != productExpiry ? "-"
											+ productExpiry : "")).trim();
						stockMap.put(key, stock);
					}
					Stock tempStock = null;
					for (Stock stock : currentStocks) {
						productExpiry = null != stock.getProductExpiry() ? new DateTime(
								stock.getProductExpiry()).toLocalDate() : null;
						if (null != stock.getShelf())
							key = (stock.getProduct().getProductId().toString()
									.concat("-").concat(stock
									.getShelf()
									.getShelfId()
									.toString()
									.concat((null != stock.getBatchNumber()
											&& !("").equals(stock
													.getBatchNumber()) ? "-"
											+ stock.getBatchNumber() : ""))
									.concat(null != productExpiry ? "-"
											+ productExpiry : ""))).trim();
						else
							key = (stock
									.getProduct()
									.getProductId()
									.toString()
									.concat((null != stock.getBatchNumber()
											&& !("").equals(stock
													.getBatchNumber()) ? "-"
											+ stock.getBatchNumber() : ""))
									.concat(null != productExpiry ? "-"
											+ productExpiry : "")).trim();
						if (stockMap.containsKey(key)) {
							tempStock = new Stock();
							tempStock = stockMap.get(key);
							tempStock.setQuantity(tempStock.getQuantity()
									+ stock.getQuantity());
							updateStocks.add(tempStock);
						} else {
							updateStocks.add(stock);
						}
					}
				} else
					updateStocks.addAll(currentStocks);
				stockService.updateStockDetails(updateStocks);
			}
		}
	}

	public void deleteBatchStockDetails(List<Stock> deleteStocks)
			throws Exception {
		List<Stock> updateStocks = null;
		double availableQuantity;
		double deleteQuantity;
		List<Stock> updatedStocks = new ArrayList<Stock>();
		Product product = null;
		for (Stock stock : deleteStocks) {
			product = productPricingBL.getProductBL().getProductService()
					.getBasicProductById(stock.getProduct().getProductId());
			if (((byte) product.getItemSubType() != (byte) InventorySubCategory.Services
					.getCode())
					&& ((byte) product.getItemSubType() != (byte) InventorySubCategory.Craft0Manufacturer
							.getCode())) {
				if (null != stock.getShelf())
					updateStocks = stockService.getStockFullDetails(stock
							.getShelf().getShelfId(), stock.getProduct()
							.getProductId(), 0l, stock.getBatchNumber(), stock
							.getProductExpiry());
				else
					updateStocks = stockService.getStockFullDetails(0, stock
							.getProduct().getProductId(), stock.getStore()
							.getStoreId(), stock.getBatchNumber(), stock
							.getProductExpiry());

				if (null != updateStocks && updateStocks.size() > 0) {
					updateStocks = orderStockByCostingType(updateStocks,
							stock.getProduct());
					availableQuantity = 0;
					deleteQuantity = stock.getQuantity();
					for (Stock orderStock : updateStocks) {
						availableQuantity = orderStock.getQuantity();
						if (availableQuantity > 0) {
							if (deleteQuantity <= availableQuantity) {
								orderStock.setQuantity(availableQuantity
										- deleteQuantity);
								updatedStocks.add(orderStock);
								break;
							} else {
								orderStock.setQuantity(0d);
								deleteQuantity = deleteQuantity
										- availableQuantity;
								updatedStocks.add(orderStock);
							}
						}
					}
				}
			}
		}
		if (null != updatedStocks && updatedStocks.size() > 0)
			stockService.updateStockDetails(updatedStocks);
	}

	// Save Stock info
	public void addDeductStockDetails(List<Stock> stocks,
			List<Stock> deleteStocks) throws Exception {
		if (null != stocks && stocks.size() > 0)
			addStocks(stocks);
		if (null != deleteStocks && deleteStocks.size() > 0)
			deleteStocks(deleteStocks);
	}

	private void addStocks(List<Stock> stocks) throws Exception {
		List<Stock> updateStocks = new ArrayList<Stock>();
		Stock updateStock = null;
		for (Stock stock : stocks) {
			updateStock = new Stock();
			List<Stock> availableStocks = stockService.getStockByStoreDetails(
					stock.getProduct().getProductId(), stock.getStore()
							.getStoreId());
			if (null != availableStocks && availableStocks.size() > 0) {
				availableStocks = orderStockByCostingType(availableStocks,
						stock.getProduct());
				updateStock = availableStocks.get(0);
				updateStock.setQuantity(updateStock.getQuantity()
						+ stock.getQuantity());
				updateStocks.add(updateStock);
			}
		}
		stockService.updateStockDetails(updateStocks);
	}

	private List<Stock> orderStockByCostingType(List<Stock> updateStocks,
			Product product) throws Exception {
		if (product.getCostingType() == null)
			product = productPricingBL.getProductBL().getProductService()
					.getBasicProductById(product.getProductId());

		if (((byte) CostingType.FIFO.getCode() == (byte) product
				.getCostingType())) {
			Collections.sort(updateStocks, new Comparator<Stock>() {
				public int compare(Stock s1, Stock s2) {
					return s2.getStockId().compareTo(s1.getStockId());
				}
			});
		} else if (((byte) CostingType.LIFO.getCode() == (byte) product
				.getCostingType())) {
			Collections.sort(updateStocks, new Comparator<Stock>() {
				public int compare(Stock s1, Stock s2) {
					return s1.getStockId().compareTo(s2.getStockId());
				}
			});
		} else if (((byte) CostingType.Average.getCode() == (byte) product
				.getCostingType())) {
			Collections.sort(updateStocks, new Comparator<Stock>() {
				public int compare(Stock s1, Stock s2) {
					return s1.getStockId().compareTo(s2.getStockId());
				}
			});
		}
		return updateStocks;
	}

	public List<StockVO> getStockDetails(long productId, int shelfId)
			throws Exception {
		List<Stock> stockDetails = stockService.getStockFullDetails(productId,
				shelfId);
		if (null != stockDetails && stockDetails.size() > 0) {
			return manipulateStockVO(stockDetails);
		}
		return null;
	}

	public List<StockVO> getStockByStoreDetails(long productId, long storeId)
			throws Exception {
		List<Stock> stockDetails = stockService.getStockByStoreDetails(
				productId, storeId);
		if (null != stockDetails && stockDetails.size() > 0) {
			return manipulateStockVO(stockDetails);
		}
		return null;
	}

	public List<StockVO> getProductStockFullDetails(long productId)
			throws Exception {
		List<Stock> stockDetails = stockService
				.getProductStockFullDetails(productId);
		if (null != stockDetails && stockDetails.size() > 0) {
			return manipulateStockVO(stockDetails);
		}
		return null;
	}

	public List<StockVO> getStockDetails(long productId, long storeId,
			int sectionId, int rackId, int shelfId) throws Exception {
		List<Stock> stockDetails = null;
		if (shelfId > 0)
			stockDetails = stockService.getStockWithSelfs(productId, storeId,
					shelfId);
		else if (rackId > 0) {
			List<Integer> sectionIds = new ArrayList<Integer>();
			List<Shelf> shelfs = storeBL.getStoreService().getShelfsByRackId(
					rackId);
			for (Shelf shelf : shelfs) {
				sectionIds.add(shelf.getShelfId());
			}
			Integer[] sectionIdArray = sectionIds
					.toArray(new Integer[sectionIds.size()]);
			stockDetails = stockService.getStockByShelfs(productId, storeId,
					sectionIdArray);
		} else if (sectionId > 0) {
			List<Shelf> shelfs = storeBL.getStoreService().getRacksBySectionId(
					sectionId);
			List<Integer> sectionIds = new ArrayList<Integer>();
			for (Shelf rack : shelfs) {
				for (Shelf shelf : rack.getShelfs()) {
					sectionIds.add(shelf.getShelfId());
				}
			}
			Integer[] sectionIdArray = sectionIds
					.toArray(new Integer[sectionIds.size()]);
			stockDetails = stockService.getStockByShelfs(productId, storeId,
					sectionIdArray);
		}
		if (null != stockDetails && stockDetails.size() > 0) {
			return manipulateStockVO(stockDetails);
		}
		return null;
	}

	public List<StockVO> getProductStockDetails(long productId, int storeId,
			int shelfId) throws Exception {
		List<Stock> stockDetails = stockService.getStockWithSelfs(productId,
				storeId, shelfId);
		if (null != stockDetails && stockDetails.size() > 0) {
			return manipulateStockVO(stockDetails);
		}
		return null;
	}

	// Save Stock info
	public void updateStockDetail(Stock stock) throws Exception {

		// Get Receive Details based on costing type
		List<Stock> currentStocks = null;
		List<Stock> updatedStocks = new ArrayList<Stock>();
		Stock updateStock = null;
		currentStocks = stockService.getStockByStoreDetails(stock.getProduct()
				.getProductId(), stock.getStore().getStoreId());
		// Manipulate stocks
		List<StockVO> stockVOs = manipulateStockVO(currentStocks);
		double totalProduct = 0;
		double updateProduct = 0;
		for (StockVO stockVO : stockVOs) {
			updateStock = new Stock();
			updateStock.setProduct(stock.getProduct());
			updateStock.setStore(stock.getStore());
			totalProduct += stockVO.getQuantity();
			if (totalProduct <= stock.getQuantity()) {
				updateStock.setQuantity(stockVO.getQuantity());
			} else {
				updateStock.setQuantity(Math.abs(updateProduct
						- stock.getQuantity()));
			}
			updateProduct += updateStock.getQuantity();
			updateStock.setUnitRate(stockVO.getUnitRate());
			updateStock.setImplementation(stock.getImplementation());
			updatedStocks.add(updateStock);
			if (updateProduct == (double) stock.getQuantity())
				break;
		}
		stockService.updateStockDetails(updatedStocks);
	}

	// Validate Stock
	public StockVO validateStocks(List<Stock> stocks) throws Exception {
		StockVO stockVO = new StockVO();
		List<StockVO> stockVOs = manipulateStockVO(stocks);
		if (null != stockVOs && stockVOs.size() > 0) {
			for (StockVO stockVo : stockVOs) {
				stockVO.setAvailableQuantity((null != stockVO
						.getAvailableQuantity() ? stockVO
						.getAvailableQuantity() : 0)
						+ stockVo.getAvailableQuantity());
				ProductPricingDetail productPricingDetail = productPricingBL
						.getProductPricingStockDetail(stockVo.getProduct()
								.getProductId(), stockVo.getStore()
								.getStoreId());
				double unitRate = 0;
				if (null != productPricingDetail
						&& null != productPricingDetail
								.getProductPricingCalcs()
						&& productPricingDetail.getProductPricingCalcs().size() > 0) {
					for (ProductPricingCalc pricingCalc : productPricingDetail
							.getProductPricingCalcs()) {
						if (null != pricingCalc.getIsDefault()
								&& (boolean) pricingCalc.getIsDefault()) {
							if (ProductCalculationType
									.get(pricingCalc.getCalculationType())
									.equals(Constants.Accounts.ProductCalculationType.Margin)
									|| ProductCalculationType
											.get(pricingCalc
													.getCalculationType())
											.equals(Constants.Accounts.ProductCalculationType.Markup))
								unitRate = pricingCalc.getSalePrice();
							else
								unitRate = pricingCalc.getSalePriceAmount();
							break;
						}
					}
				} else
					unitRate = stockVo.getUnitRate();
				stockVO.setTotalAmount((null != stockVO.getTotalAmount() ? stockVO
						.getTotalAmount() : 0)
						+ (unitRate * stockVo.getAvailableQuantity()));
				stockVO.setProductName(stockVo.getProduct().getProductName());
				stockVO.setStoreName(stockVo.getStore().getStoreName());
				stockVO.setCostingType(Constants.Accounts.CostingType.get(
						stockVo.getProduct().getCostingType()).name());
				stockVO.setProductUnit(stockVo.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());
			}
			stockVO.setUnitRate(stockVO.getTotalAmount()
					/ stockVO.getAvailableQuantity());
			if (null != stockVO && !("").equals(stockVO)) {
				stockVO.setUnitRate(Double.parseDouble(AIOSCommons
						.roundTwoDecimals(stockVO.getUnitRate())));
				stockVO.setTotalAmount(Double.parseDouble(AIOSCommons
						.roundTwoDecimals(stockVO.getTotalAmount())));
			}
		}
		return stockVO;
	}

	// Manipulate stock
	private List<StockVO> manipulateStockVO(List<Stock> stocks)
			throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		String unifiedPrice = "";
		if (session.getAttribute("unified_price") != null)
			unifiedPrice = session.getAttribute("unified_price").toString();
		Map<String, StockVO> stockDetailMap = new HashMap<String, StockVO>();
		String key = "";
		StockVO stockVo = null;
		List<StockVO> availableStocks = null;
		for (Stock stock : stocks) {
			if (((byte) InventorySubCategory.Craft0Manufacturer.getCode() != (byte) stock
					.getProduct().getItemSubType())
					&& ((byte) InventorySubCategory.Services.getCode() != (byte) stock
							.getProduct().getItemSubType())) {
				availableStocks = new ArrayList<StockVO>();
				stockVo = new StockVO();
				key = (stock.getProduct().getProductId().toString().concat("-")
						.concat(stock
								.getShelf()
								.getShelfId()
								.toString()
								.concat((null != stock.getBatchNumber()
										&& !("").equals(stock.getBatchNumber()) ? "-"
										+ stock.getBatchNumber()
										: ""))
								.concat(null != stock.getProductExpiry() ? "-"
										+ stock.getProductExpiry() : "")))
						.trim();
				List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
						stock.getProduct().getProductPricingDetails());
				ProductPricingCalc pricingCalc = null;
				double basePrice = 0;
				double standardPrice = 0;
				double sellingPrice = 0;
				long calcId = 0;
				if (null != productPricingDetails
						&& productPricingDetails.size() > 0) {
					boolean storePrice = false;
					if (unifiedPrice.equals("false")) {
						for (ProductPricingDetail productPricingDetail : productPricingDetails) {
							if (productPricingDetail.getStatus()) {
								if (null != productPricingDetail.getStore()
										&& (long) productPricingDetail
												.getStore().getStoreId() == (long) stock
												.getStore().getStoreId()) {
									storePrice = true;
									basePrice = productPricingDetail
											.getBasicCostPrice();
									standardPrice = productPricingDetail
											.getStandardPrice();
									sellingPrice = null != productPricingDetail
											.getSellingPrice() ? productPricingDetail
											.getSellingPrice() : 0;
									if (null != productPricingDetail
											.getProductPricingCalcs()
											&& productPricingDetail
													.getProductPricingCalcs()
													.size() > 0) {
										for (ProductPricingCalc productPricingCalc : productPricingDetail
												.getProductPricingCalcs()) {
											if (null != productPricingCalc
													.getIsDefault()
													&& productPricingCalc
															.getIsDefault()) {
												pricingCalc = new ProductPricingCalc();
												calcId = productPricingCalc
														.getProductPricingCalcId();
												BeanUtils.copyProperties(
														pricingCalc,
														productPricingCalc);
												break;
											}
										}
									}
									break;
								}
							}
						}
					}
					if (!storePrice) {
						for (ProductPricingDetail productPricingDetail : productPricingDetails) {
							if (productPricingDetail.getStatus()
									&& null == productPricingDetail.getStore()) {
								basePrice = productPricingDetail
										.getBasicCostPrice();
								standardPrice = productPricingDetail
										.getStandardPrice();
								sellingPrice = null != productPricingDetail
										.getSellingPrice() ? productPricingDetail
										.getSellingPrice() : 0;
								if (null != productPricingDetail
										.getProductPricingCalcs()
										&& productPricingDetail
												.getProductPricingCalcs()
												.size() > 0) {
									for (ProductPricingCalc productPricingCalc : productPricingDetail
											.getProductPricingCalcs()) {
										if (null != productPricingCalc
												.getIsDefault()
												&& productPricingCalc
														.getIsDefault()) {
											pricingCalc = new ProductPricingCalc();
											calcId = productPricingCalc
													.getProductPricingCalcId();
											BeanUtils.copyProperties(
													pricingCalc,
													productPricingCalc);
											break;
										}
									}
								}
								break;
							}
						}
					}
				} else {
					basePrice = null != stock.getUnitRate() ? stock
							.getUnitRate() : 0;
					standardPrice = basePrice;
					sellingPrice = basePrice;
				}
				if (null != stockDetailMap && stockDetailMap.size() > 0) {
					if (stockDetailMap.containsKey(key)) {
						stockVo = stockDetailMap.get(key);
						stockVo.setAvailableQuantity(stockVo
								.getAvailableQuantity() + stock.getQuantity());
					} else {
						BeanUtils.copyProperties(stockVo, stock);
						stockVo.setAvailableQuantity(stock.getQuantity());
					}
				} else {
					BeanUtils.copyProperties(stockVo, stock);
					stockVo.setAvailableQuantity(stock.getQuantity());
				}
				if (null != pricingCalc) {
					if (Constants.Accounts.ProductCalculationType.get(
							pricingCalc.getCalculationType()).equals(
							Constants.Accounts.ProductCalculationType.Margin)
							|| Constants.Accounts.ProductCalculationType
									.get(pricingCalc.getCalculationType())
									.equals(Constants.Accounts.ProductCalculationType.Markup))
						sellingPrice = pricingCalc.getSalePrice();
					else
						sellingPrice = pricingCalc.getSalePriceAmount();
				}
				stockVo.setProductPricingCalcId(calcId);
				stockVo.setCostPrice(basePrice);
				stockVo.setStandardPrice(standardPrice);
				stockVo.setUnitRate(basePrice);
				stockVo.setSellingPrice(sellingPrice);
				stockVo.setBatchNumber(stock.getBatchNumber());
				stockVo.setExpiryDate(null != stock.getProductExpiry() ? DateFormat
						.convertDateToString(stock.getProductExpiry()
								.toString()) : "");
				String storeName = "";
				if (null != stock.getShelf()) {
					if (null != stock.getShelf().getShelf()) {
						storeName = stock.getShelf().getShelf().getAisle()
								.getStore().getStoreName()
								+ ">>"
								+ stock.getShelf().getShelf().getAisle()
										.getSectionName()
								+ ">>"
								+ stock.getShelf().getShelf().getName()
								+ ">>"
								+ stock.getShelf().getName();
						stockVo.setStoreName(storeName);
					}
				}
				stockDetailMap.put(key, stockVo);
			} else {
				stockVo = new StockVO();
				BeanUtils.copyProperties(stockVo, stock);

				stockVo.setAvailableQuantity(stock.getQuantity());
				stockDetailMap.put(
						String.valueOf(stock.getProduct().getProductId()),
						stockVo);
			}
			if (null != stockDetailMap && stockDetailMap.size() > 0)
				availableStocks = new ArrayList<StockVO>(
						stockDetailMap.values());
		}
		return availableStocks;
	}

	public Double getStockPrice(long productId) throws Exception {
		List<Stock> stocks = stockService.getStockByProduct(productId);
		for (Stock stock : stocks) {
			if (null != stock.getUnitRate() && stock.getUnitRate() > 0)
				return stock.getUnitRate();
		}
		return 0D;
	}

	private StockVO addStockObject(StockVO stock) {
		StockVO stockVO = new StockVO();
		stockVO.setStockId(stock.getStockId());
		stockVO.setStoreId(null != stock.getStore() ? stock.getStore()
				.getStoreId() : null);
		stockVO.setStoreName(null != stock.getStore() ? stock.getShelf()
				.getShelf().getAisle().getStore().getStoreName()
				+ " >> "
				+ stock.getShelf().getShelf().getAisle().getSectionName()
				+ " >> "
				+ stock.getShelf().getShelf().getName()
				+ " >> "
				+ stock.getShelf().getName() : "");
		stockVO.setProductId(stock.getProduct().getProductId());
		stockVO.setProductCode(stock.getProduct().getCode());
		stockVO.setProductName(stock.getProduct().getProductName());
		stockVO.setUnitRate(null != stock.getUnitRate() ? stock.getUnitRate()
				: 0);
		stockVO.setAvailableQuantity(stock.getAvailableQuantity());
		stockVO.setProductUnit(stock.getProduct()
				.getLookupDetailByProductUnit().getDisplayName());
		stockVO.setUnitCode(stock.getProduct().getLookupDetailByProductUnit()
				.getAccessCode());
		stockVO.setShelfName(null != stock.getShelf() ? stock.getShelf()
				.getName() : "");
		stockVO.setShelfId(null != stock.getShelf() ? stock.getShelf()
				.getShelfId() : null);
		stockVO.setStandardPrice(stock.getStandardPrice());
		stockVO.setCostPrice(stock.getCostPrice());
		stockVO.setProductPricingCalcId(stock.getProductPricingCalcId());
		stockVO.setSuggestedRetailPrice(stock.getStandardPrice());
		stockVO.setSellingPrice(stock.getSellingPrice());
		stockVO.setBatchNumber(stock.getBatchNumber());
		stockVO.setExpiryDate(stock.getExpiryDate());
		stockVO.setProductType("F");
		return stockVO;
	}

	public StockVO addStockObject(Stock stock) {
		StockVO stockVO = new StockVO();
		stockVO.setStockId(stock.getStockId());
		if (null != stock.getStore()) {
			stockVO.setStoreId(null != stock.getStore() ? stock.getStore()
					.getStoreId() : null);
			if (null != stock.getShelf()) {
				stockVO.setStoreName(null != stock.getStore() ? stock
						.getShelf().getShelf().getAisle().getStore()
						.getStoreName()
						+ " >> "
						+ stock.getShelf().getShelf().getAisle()
								.getSectionName()
						+ " >> "
						+ stock.getShelf().getShelf().getName()
						+ " >> "
						+ stock.getShelf().getName() : "");
			} else
				stockVO.setStoreName(stock.getStore().getStoreName());
		}

		stockVO.setProductId(stock.getProduct().getProductId());
		stockVO.setProductCode(stock.getProduct().getCode());
		stockVO.setProductName(stock.getProduct().getProductName());
		stockVO.setUnitRate(null != stock.getUnitRate() ? stock.getUnitRate()
				: 0);
		stockVO.setAvailableQuantity(stock.getQuantity());
		stockVO.setProductUnit(stock.getProduct()
				.getLookupDetailByProductUnit().getDisplayName());
		stockVO.setShelfName(null != stock.getShelf() ? stock.getShelf()
				.getName() : "");
		stockVO.setShelfId(null != stock.getShelf() ? stock.getShelf()
				.getShelfId() : null);
		double standardPrice = 0;
		double costPrice = 0;
		double sellingPrice = 0;
		if (null != stock.getProduct().getProductPricingDetails()
				&& stock.getProduct().getProductPricingDetails().size() > 0) {
			List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
					stock.getProduct().getProductPricingDetails());
			for (ProductPricingDetail productPricingDetail : productPricingDetails) {
				if (productPricingDetail.getStatus()) {
					standardPrice = productPricingDetail.getStandardPrice();
					costPrice = productPricingDetail.getBasicCostPrice();
					sellingPrice = null != productPricingDetail
							.getSellingPrice() ? productPricingDetail
							.getSellingPrice() : 0;
					break;
				}
			}
		}
		stockVO.setStandardPrice(standardPrice);
		stockVO.setCostPrice(costPrice);
		stockVO.setSuggestedRetailPrice(standardPrice);
		stockVO.setSellingPrice(sellingPrice);
		return stockVO;
	}

	public List<StockVO> validateStocks(Set<Long> productsIds, Long storeId)
			throws Exception {
		List<Stock> stocks = stockService.getStockByStoreProducts(productsIds,
				storeId);
		if (null != stocks && stocks.size() > 0) {
			Map<Long, StockVO> stockMap = new HashMap<Long, StockVO>();
			StockVO tempStockVO = null;
			for (Stock stock : stocks) {
				tempStockVO = new StockVO();
				BeanUtils.copyProperties(tempStockVO, stock);
				if (stockMap.containsKey(stock.getProduct().getProductId()))
					tempStockVO = stockMap.get(stock.getProduct()
							.getProductId());
				tempStockVO.setAvailableQuantity(null != tempStockVO
						.getAvailableQuantity() ? tempStockVO
						.getAvailableQuantity() : 0 + stock.getQuantity());
				stockMap.put(stock.getProduct().getProductId(), tempStockVO);
			}
			return new ArrayList<StockVO>(stockMap.values());
		}
		return null;
	}

	public StockVO getStoreAndStockByProduct(Long productId, Long storeId) {
		List<Stock> stocks = null;
		StockVO stockVO = null;
		try {
			stocks = stockService.getStockFullDetailsByProduct(productId);
			if (stocks != null && stocks.size() > 0) {
				for (Stock stock : stocks) {
					if ((long) stock.getStore().getStoreId() == (long) storeId) {
						stockVO = new StockVO();
						stockVO.setStoreId(stock.getStore().getStoreId());
						if (null != stock.getShelf()) {
							stockVO.setShelfId(stock.getShelf().getShelfId());
							stockVO.setStoreName(stock.getShelf().getShelf()
									.getAisle().getStore().getStoreName()
									+ " --"
									+ stock.getShelf().getShelf().getAisle()
											.getSectionName()
									+ " -- "
									+ stock.getShelf().getShelf().getName()
									+ " -- " + stock.getShelf().getName());
						} else {
							stockVO.setStoreName(stock.getStore()
									.getStoreName());
						}
					}
				}

			}
			if (stockVO == null) {
				Product product = productPricingBL.getProductBL()
						.getProductService().getProductById(productId);
				if (product != null && product.getShelf() != null) {
					stockVO = new StockVO();
					stockVO.setStoreId(product.getShelf().getShelf().getAisle()
							.getStore().getStoreId());
					stockVO.setShelfId(product.getShelf().getShelfId());
					stockVO.setStoreName(product.getShelf().getShelf()
							.getAisle().getStore().getStoreName()
							+ " -- "
							+ product.getShelf().getShelf().getAisle()
									.getSectionName()
							+ " -- "
							+ product.getShelf().getShelf().getName()
							+ " -- "
							+ product.getShelf().getName());
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return stockVO;
	}

	private StockVO addProductStockObject(Product product)
			throws IllegalAccessException, InvocationTargetException {
		StockVO stockVO = new StockVO();
		stockVO.setProductId(product.getProductId());
		stockVO.setProductCode(product.getCode());
		stockVO.setProductName(product.getProductName());
		stockVO.setProductUnit(product.getLookupDetailByProductUnit()
				.getDisplayName());
		stockVO.setUnitCode(product.getLookupDetailByProductUnit()
				.getAccessCode());
		stockVO.setUnitRate(getProductPrice(product.getProductPricingDetails()));
		stockVO.setAvailableQuantity(1.0);
		double suggestedPrice = 0;
		double basePrice = 0;
		double sellingPrice = 0;
		if (null != product.getProductPricingDetails()
				&& product.getProductPricingDetails().size() > 0) {
			Set<ProductPricingCalc> productPricingCalcs = null;
			for (ProductPricingDetail productPricingDetail2 : product
					.getProductPricingDetails()) {
				if (productPricingDetail2.getStatus()) {
					productPricingCalcs = new HashSet<ProductPricingCalc>(
							productPricingDetail2.getProductPricingCalcs());
					suggestedPrice = productPricingDetail2
							.getSuggestedRetailPrice();
					basePrice = productPricingDetail2.getBasicCostPrice();
					sellingPrice = productPricingDetail2.getSellingPrice();
					break;
				}
			}
			for (ProductPricingCalc pricingCalc : productPricingCalcs) {
				if (null != pricingCalc.getIsDefault()
						&& (boolean) pricingCalc.getIsDefault()) {
					stockVO.setProductPricingCalcId(pricingCalc
							.getProductPricingCalcId());
					break;
				}
			}
		}
		stockVO.setStandardPrice(suggestedPrice);
		stockVO.setCostPrice(basePrice);
		stockVO.setSellingPrice(sellingPrice);
		stockVO.setProductType("C");
		return stockVO;

	}

	private Double getProductPrice(Set<ProductPricingDetail> pricingDetail)
			throws IllegalAccessException, InvocationTargetException {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
				pricingDetail);
		ProductPricingCalc pricingCalc = null;
		if (null != productPricingDetails && productPricingDetails.size() > 0) {
			boolean priceFlag = true;
			for (ProductPricingDetail productPricingDetail : productPricingDetails) {
				if (priceFlag) {
					for (ProductPricingCalc productPricingCalc : productPricingDetail
							.getProductPricingCalcs()) {
						if (null != productPricingCalc.getIsDefault()
								&& productPricingCalc.getIsDefault()) {
							priceFlag = false;
							pricingCalc = new ProductPricingCalc();
							BeanUtils.copyProperties(pricingCalc,
									productPricingCalc);
							break;
						}
					}
				} else
					break;
			}
		}
		if (null != pricingCalc) {
			if (null != pricingCalc) {
				if (Constants.Accounts.ProductCalculationType.get(
						pricingCalc.getCalculationType()).equals(
						Constants.Accounts.ProductCalculationType.Margin)
						|| Constants.Accounts.ProductCalculationType
								.get(pricingCalc.getCalculationType())
								.equals(Constants.Accounts.ProductCalculationType.Markup))
					return pricingCalc.getSalePrice();
				else
					return pricingCalc.getSalePriceAmount();
			}
		}
		return 0d;
	}

	public Double getProductCostPrice(Set<ProductPricingDetail> pricingDetail)
			throws IllegalAccessException, InvocationTargetException {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
				pricingDetail);
		if (null != productPricingDetails && productPricingDetails.size() > 0) {
			for (ProductPricingDetail productPricingDetail : productPricingDetails) {
				if (null != productPricingDetail.getStatus()
						&& productPricingDetail.getStatus())
					return productPricingDetail.getBasicCostPrice();

			}
		}
		return 0d;
	}

	public Double getProductCostingPrice(Product product) throws Exception {
		double unitRate = 0;
		if (null != product) {
			List<Stock> stocks = stockService.getStockByProduct(product
					.getProductId());
			if (null != stocks && stocks.size() > 0) {
				double totalRate = 0;
				double totalQty = 0;
				if ((byte) product.getCostingType() == (byte) CostingType.LIFO
						.getCode()) {
					Collections.sort(stocks, new Comparator<Stock>() {
						public int compare(Stock o2, Stock o1) {
							return o2.getStockId().compareTo(o1.getStockId());
						}
					});
					unitRate = stocks.get(0).getUnitRate();
				} else if ((byte) product.getCostingType() == (byte) CostingType.FIFO
						.getCode()) {
					Collections.sort(stocks, new Comparator<Stock>() {
						public int compare(Stock o1, Stock o2) {
							return o1.getStockId().compareTo(o2.getStockId());
						}
					});
					unitRate = stocks.get(0).getUnitRate();
				} else {
					for (Stock stock : stocks) {
						totalRate += (stock.getUnitRate() * stock.getQuantity());
						totalQty += stock.getQuantity();
					}
					if (totalRate > 0 && totalQty > 0)
						unitRate = AIOSCommons
								.roundDecimals((totalRate / totalQty));
				}
			}
		}

		return unitRate;
	}

	public Double getProductStandardPrice(
			Set<ProductPricingDetail> pricingDetail)
			throws IllegalAccessException, InvocationTargetException {
		List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>(
				pricingDetail);
		if (null != productPricingDetails && productPricingDetails.size() > 0) {
			for (ProductPricingDetail productPricingDetail : productPricingDetails) {
				if (null != productPricingDetail.getStatus()
						&& productPricingDetail.getStatus())
					return productPricingDetail.getStandardPrice();
			}
		}
		return 0d;
	}

	private Double getProductSalePrice(Set<ProductPricingDetail> pricingDetail)
			throws IllegalAccessException, InvocationTargetException {

		Double selling = 0.0;
		if (null != pricingDetail && pricingDetail.size() > 0) {
			Set<ProductPricingCalc> productPricingCalcs = null;
			for (ProductPricingDetail productPricingDetail2 : pricingDetail) {
				if (null != productPricingDetail2.getStatus()
						&& productPricingDetail2.getStatus()) {

					selling = productPricingDetail2.getSellingPrice();
					productPricingCalcs = new HashSet<ProductPricingCalc>(
							productPricingDetail2.getProductPricingCalcs());
					break;
				}

			}
			for (ProductPricingCalc pricingCalc : productPricingCalcs) {
				if (null != pricingCalc.getIsDefault()
						&& (boolean) pricingCalc.getIsDefault()) {
					if (ProductCalculationType.get(
							pricingCalc.getCalculationType()).equals(
							Constants.Accounts.ProductCalculationType.Margin)
							|| ProductCalculationType
									.get(pricingCalc.getCalculationType())
									.equals(Constants.Accounts.ProductCalculationType.Markup))
						return pricingCalc.getSalePrice();
					else
						return pricingCalc.getSalePriceAmount();
				}
			}
		}
		return (null != selling) ? selling : 0.0;
	}

	// Delete stock by delivery note
	public void deleteStocks(List<Stock> deleteStocks) throws Exception {
		// Delete Stock
		if (null != deleteStocks && deleteStocks.size() > 0) {
			List<Stock> updateStocks = null;
			double availableQuantity;
			double deleteQuantity;
			List<Stock> updatedStocks = new ArrayList<Stock>();
			for (Stock stock : deleteStocks) {
				updateStocks = stockService.getStockByStoreDetails(stock
						.getProduct().getProductId(), stock.getStore()
						.getStoreId());
				if (null != updateStocks && updateStocks.size() > 0) {
					updateStocks = orderStockByCostingType(updateStocks,
							stock.getProduct());
					availableQuantity = 0;
					deleteQuantity = 0;
					for (Stock orderStock : updateStocks) {
						availableQuantity = orderStock.getQuantity();
						deleteQuantity = deleteQuantity + stock.getQuantity();
						if (deleteQuantity <= availableQuantity) {
							orderStock.setQuantity(availableQuantity
									- deleteQuantity);
							updatedStocks.add(orderStock);
							break;
						} else {
							orderStock.setQuantity(0d);
							deleteQuantity = deleteQuantity - availableQuantity;
							updatedStocks.add(orderStock);
						}
					}
				}
			}
			stockService.updateStockDetails(updatedStocks);
		}
	}

	public List<?> getStockReportVO(List<Stock> stocks) throws Exception {
		List<StockVO> availableStocks = null;
		if (null != stocks && stocks.size() > 0) {
			Map<String, StockVO> stockDetailMap = new HashMap<String, StockVO>();
			String key = "";
			StockVO stockVo = null;
			for (Stock stock : stocks) {
				availableStocks = new ArrayList<StockVO>();
				stockVo = new StockVO();
				key = stock.getProduct().getProductId().toString().concat("-")
						.concat(stock.getStore().getStoreId().toString());
				if (null != stockDetailMap && stockDetailMap.size() > 0) {
					if (stockDetailMap.containsKey(key)) {
						stockVo = stockDetailMap.get(key);
						stockVo.setAvailableQuantity(stockVo
								.getAvailableQuantity() + stock.getQuantity());
						stockVo.setUnitRate(stockVo.getUnitRate()
								+ stock.getUnitRate());
					} else {
						BeanUtils.copyProperties(stockVo, stock);
						stockVo.setAvailableQuantity(stock.getQuantity());
					}
				} else {
					BeanUtils.copyProperties(stockVo, stock);
					stockVo.setAvailableQuantity(stock.getQuantity());
				}
				stockDetailMap.put(key, stockVo);
			}
			if (null != stockDetailMap && stockDetailMap.size() > 0)
				availableStocks = new ArrayList<StockVO>(
						stockDetailMap.values());
		}
		return availableStocks;
	}

	public List<StockVO> getStoreStockReport(List<Stock> stocks)
			throws Exception {
		List<StockVO> availableStocks = null;
		if (null != stocks && stocks.size() > 0) {
			Map<Long, StockVO> stockDetailMap = new HashMap<Long, StockVO>();
			StockVO stockVo = null;
			for (Stock stock : stocks) {
				availableStocks = new ArrayList<StockVO>();
				stockVo = new StockVO();
				if (null != stockDetailMap && stockDetailMap.size() > 0) {
					if (stockDetailMap.containsKey(stock.getProduct()
							.getProductId())) {
						stockVo = stockDetailMap.get(stock.getProduct()
								.getProductId());
						stockVo.setAvailableQuantity(stockVo
								.getAvailableQuantity() + stock.getQuantity());
						stockVo.setUnitRate(stockVo.getUnitRate()
								+ stock.getUnitRate());
					} else {
						BeanUtils.copyProperties(stockVo, stock);
						stockVo.setAvailableQuantity(stock.getQuantity());
					}
				} else {
					BeanUtils.copyProperties(stockVo, stock);
					stockVo.setAvailableQuantity(stock.getQuantity());
				}
				stockDetailMap.put(stock.getProduct().getProductId(), stockVo);
			}
			if (null != stockDetailMap && stockDetailMap.size() > 0)
				availableStocks = new ArrayList<StockVO>(
						stockDetailMap.values());
		}
		return availableStocks;
	}

	public List<ProductVO> getStoreStockDetails(int storeId) throws Exception {
		List<ProductVO> productVOs = new ArrayList<ProductVO>();
		List<StockVO> stockVOs = getStoreStockReport(stockService
				.getStockByStoreId(storeId));
		if (null != stockVOs && stockVOs.size() > 0) {
			for (StockVO stockVO : stockVOs) {
				productVOs.add(addBasicProductDetail(stockVO.getProduct()));
			}
		}
		List<Product> serviceCraftProducts = productPricingBL
				.getProductBL()
				.getProductService()
				.getServiceAndCraftProducts(getImplementation(),
						InventorySubCategory.Services.getCode(),
						InventorySubCategory.Craft0Manufacturer.getCode());
		if (null != serviceCraftProducts && serviceCraftProducts.size() > 0) {
			for (Product product : serviceCraftProducts) {
				productVOs.add(addBasicProductDetail(product));
			}
		}
		return productVOs;
	}

	public ProductVO addBasicProductDetail(Product product) {
		ProductVO productVO = new ProductVO();
		productVO.setProductId(product.getProductId());
		productVO.setCode(product.getCode());
		productVO.setBarCode(product.getBarCode());
		productVO.setProductName(product.getProductName());
		return productVO;
	}

	// Show Product Stock Reorder
	public List<?> getProductReorder() throws Exception {
		List<Stock> stocks = stockService
				.getProductStockDetail(getImplementation());
		List<StockVO> stockObjects = new ArrayList<StockVO>();
		List<Object> stockFinalVOs = new ArrayList<Object>();
		if (null != stocks && stocks.size() > 0) {
			List<StockVO> stockVOs = manipulateStockVO(stocks);
			if (null != stockVOs && stockVOs.size() > 0) {
				for (StockVO stockVO : stockVOs)
					stockObjects.add(addStockObject(stockVO));
			}
		}

		// Convert StockVo into map
		Map<Long, StockVO> maps = new HashMap<Long, StockVO>();
		StockVO tempStockVO = null;
		for (StockVO stockVO : stockObjects) {
			if (maps != null && maps.containsKey(stockVO.getProductId())) {
				tempStockVO = maps.get(stockVO.getProductId());
				tempStockVO.setAvailableQuantity(tempStockVO
						.getAvailableQuantity()
						+ stockVO.getAvailableQuantity());
				maps.put(stockVO.getProductId(), tempStockVO);
			} else {
				maps.put(stockVO.getProductId(), stockVO);
			}
		}
		List<Product> productList = productPricingBL.getProductBL()
				.getProductService().getReorderProducts(getImplementation());
		StockVO stockVO = null;
		List<StockVO> tempstockObjects = new ArrayList<StockVO>();
		for (Product product : productList) {
			if (maps != null && maps.containsKey(product.getProductId())) {
				stockVO = maps.get(product.getProductId());
				stockVO.setReorderQuantity(null != product.getReOrderQuantity()
						&& product.getReOrderQuantity() > 0 ? product
						.getReOrderQuantity() : null);
				stockVO.setCostPrice(getProductCostPrice(product
						.getProductPricingDetails()));
				stockVO.setStandardPrice(getProductStandardPrice(product
						.getProductPricingDetails()));
				stockVO.setUnitRate(getProductSalePrice(product
						.getProductPricingDetails()));

				try {
					stockVO.setCategoryName(product.getProductCategory()
							.getPublicName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				stockVO = new StockVO();
				stockVO.setProductName(product.getProductName());
				stockVO.setProductUnit(product.getLookupDetailByProductUnit()
						.getDisplayName());
				stockVO.setProductId(product.getProductId());
				stockVO.setProductCode(product.getCode());
				stockVO.setReorderQuantity(null != product.getReOrderQuantity()
						&& product.getReOrderQuantity() > 0 ? product
						.getReOrderQuantity() : null);
				stockVO.setCostPrice(getProductCostPrice(product
						.getProductPricingDetails()));
				stockVO.setStandardPrice(getProductStandardPrice(product
						.getProductPricingDetails()));
				stockVO.setUnitRate(getProductSalePrice(product
						.getProductPricingDetails()));
				try {
					stockVO.setCategoryName(product.getProductCategory()
							.getPublicName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			tempstockObjects.add(stockVO);
		}
		for (StockVO stock : tempstockObjects) {
			if (null != stock.getAvailableQuantity()) {
				if (null != stock.getReorderQuantity()
						&& stock.getReorderQuantity() >= stock
								.getAvailableQuantity())
					stockFinalVOs.add(stock);
			}
		}
		return stockFinalVOs;
	}

	public void updatePeriodicStock(String month) throws Exception {
		List<ProductVO> productVOs = manipulatePeriodicStockDatas(month);
		List<StockPeriodic> stockPeriodics = new ArrayList<StockPeriodic>();
		StockPeriodic stockPeriodic = null;
		Product prd = null;
		List<ReceiveDetailVO> details = null;
		List<GoodsReturnDetailVO> details1 = null;
		List<IssueRequistionDetailVO> details2 = null;
		List<IssueReturnDetailVO> details3 = null;
		for (ProductVO productVO : productVOs) {
			stockPeriodic = new StockPeriodic();
			prd = new Product();
			prd.setProductId(productVO.getProductId());
			stockPeriodic.setProduct(prd);
			stockPeriodic.setImplementation(getImplementation());
			stockPeriodic.setUnitRate(productVO.getUnitRate());
			stockPeriodic.setQuantity(productVO.getQuantity());
			stockPeriodic.setShelf(productVO.getShelf());
			stockPeriodic.setMonth(month);
			for (Map.Entry<String, ArrayList<ReceiveDetailVO>> entry : productVO
					.getReceiveDetailVOMap().entrySet()) {
				details = entry.getValue();

				for (ReceiveDetailVO vo : details) {
					stockPeriodic.setQuantity(stockPeriodic.getQuantity()
							+ vo.getReceiveQty());
					stockPeriodic.setUnitRate(vo.getUnitRate());
					stockPeriodic.setShelf(vo.getShelf());
				}
			}

			for (Map.Entry<String, ArrayList<GoodsReturnDetailVO>> entry : productVO
					.getGoodsReturnDetailVOMap().entrySet()) {
				details1 = entry.getValue();
				for (GoodsReturnDetailVO vo : details1) {
					stockPeriodic.setQuantity(stockPeriodic.getQuantity()
							- vo.getReturnQty());
				}
			}

			for (Map.Entry<String, ArrayList<IssueRequistionDetailVO>> entry : productVO
					.getIssueRequistionDetailVOMap().entrySet()) {
				details2 = entry.getValue();
				for (IssueRequistionDetailVO vo : details2) {
					stockPeriodic.setQuantity(stockPeriodic.getQuantity()
							- vo.getQuantity());
				}
			}

			for (Map.Entry<String, ArrayList<IssueReturnDetailVO>> entry : productVO
					.getIssueReturnDetailVOMap().entrySet()) {
				details3 = entry.getValue();
				for (IssueReturnDetailVO vo : details3) {
					stockPeriodic.setQuantity(stockPeriodic.getQuantity()
							+ vo.getReturnQuantity());
				}
			}
			stockPeriodics.add(stockPeriodic);
		}

		stockService.updateStockPeriodic(stockPeriodics);

		List<Stock> stocks = stockService
				.getProductStockDetail(getImplementation());
		stockService.deleteStocks(stocks);

		List<Stock> stockToUpdate = new ArrayList<Stock>();
		Stock stk = null;
		for (StockPeriodic stkPeriod : stockPeriodics) {
			stk = new Stock();
			stk.setProduct(stkPeriod.getProduct());
			stk.setQuantity(stkPeriod.getQuantity());
			stk.setUnitRate(stkPeriod.getUnitRate());
			Store str = new Store();
			str.setStoreId((long) 20);
			stk.setStore(str);
			stk.setImplementation(stkPeriod.getImplementation());
			stk.setShelf(stkPeriod.getShelf());
			stockToUpdate.add(stk);
		}
		stockService.mergeStockDetails(stockToUpdate);
	}

	// Save Stock info
	public List<ProductVO> manipulatePeriodicStockDatas(String month)
			throws Exception {

		// Get Receive Details based on costing type
		List<Product> productList = productPricingBL.getProductBL()
				.getProductService()
				.getAllProductForStockPeriodic(getImplementation());

		List<StockPeriodic> periodicSotcksForSamePeriod = stockService
				.getStockPeriodicByMonth(getImplementation(), month);
		if (periodicSotcksForSamePeriod != null
				&& periodicSotcksForSamePeriod.size() > 0) {
			stockService.deleteStockPeriodicAll(periodicSotcksForSamePeriod);
		}

		// Month calculations
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MMMM,yyyy");
		LocalDate monthDt = new LocalDate(dtf.parseLocalDate(month));
		LocalDate date1 = new LocalDate(monthDt.dayOfMonth().withMinimumValue());
		LocalDate date2 = new LocalDate(monthDt.dayOfMonth().withMaximumValue());
		List<ProductVO> productVOs = new ArrayList<ProductVO>();

		LocalDate previousMonth = date1.minus(Period.months(1));
		String preMonth = previousMonth.toString("MMMM,yyyy");
		List<StockPeriodic> periodicSotcks = stockService
				.getStockPeriodicByMonth(getImplementation(), preMonth);
		Map<Long, ProductVO> productsMap = new HashMap<Long, ProductVO>();
		ProductVO tempProductVO = null;
		for (StockPeriodic stockPeriodic : periodicSotcks) {
			if (productsMap.containsKey(stockPeriodic.getProduct()
					.getProductId())) {
				tempProductVO = productsMap.get(stockPeriodic.getProduct()
						.getProductId());
				BeanUtils.copyProperties(tempProductVO,
						stockPeriodic.getProduct());
				tempProductVO.setQuantity(stockPeriodic.getQuantity());
			} else {
				tempProductVO = new ProductVO();
				BeanUtils.copyProperties(tempProductVO,
						stockPeriodic.getProduct());
				tempProductVO.setQuantity(stockPeriodic.getQuantity());
			}
			productsMap.put(stockPeriodic.getProduct().getProductId(),
					tempProductVO);
		}
		ProductVO vo = null;
		LocalDate dt = null;
		ReceiveDetailVO receiveDetailVO = null;
		GoodsReturnDetailVO goodsReturnDetailVO = null;
		IssueRequistionDetailVO issueRequisitionDetailVO = null;
		IssueReturnDetailVO issueReturnDetailVO = null;
		ArrayList<ReceiveDetailVO> details = null;
		ArrayList<GoodsReturnDetailVO> details1 = null;
		ArrayList<IssueRequistionDetailVO> details2 = null;
		ArrayList<IssueReturnDetailVO> details3 = null;
		boolean validFlag = false;
		for (Product product : productList) {
			validFlag = false;
			vo = productsMap.get(product.getProductId());
			if (vo == null) {
				vo = new ProductVO();
				BeanUtils.copyProperties(vo, product);
			} else {
				validFlag = true;
			}
			// Receive Details
			for (ReceiveDetail receiveDetil : product.getReceiveDetails()) {
				dt = new LocalDate(receiveDetil.getReceive().getReceiveDate());
				if (!dt.isBefore(date1) && !dt.isAfter(date2)) {
					validFlag = true;
					receiveDetailVO = new ReceiveDetailVO();
					BeanUtils.copyProperties(receiveDetailVO, receiveDetil);
					details = vo.getReceiveDetailVOMap().get(
							dt.toString("dd-MMM-yyyy"));
					if (details != null && details.size() > 0) {
						details.add(receiveDetailVO);
					} else {
						details = new ArrayList<ReceiveDetailVO>();
						details.add(receiveDetailVO);
					}
					vo.getReceiveDetailVOMap().put(dt.toString("dd-MMM-yyyy"),
							details);
				}
			}

			// Receive Return/ Goods Return Details
			for (GoodsReturnDetail returnDetail : product
					.getGoodsReturnDetails()) {
				dt = new LocalDate(returnDetail.getGoodsReturn().getDate());
				if (!dt.isBefore(date1) && !dt.isAfter(date2)) {
					validFlag = true;
					goodsReturnDetailVO = new GoodsReturnDetailVO();
					BeanUtils.copyProperties(goodsReturnDetailVO, returnDetail);
					details1 = vo.getGoodsReturnDetailVOMap().get(
							dt.toString("dd-MMM-yyyy"));
					if (details1 != null && details1.size() > 0) {
						details1.add(goodsReturnDetailVO);
					} else {
						details1 = new ArrayList<GoodsReturnDetailVO>();
						details1.add(goodsReturnDetailVO);
					}
					vo.getGoodsReturnDetailVOMap().put(
							dt.toString("dd-MMM-yyyy"), details1);
				}
			}

			// Issuance Details
			for (IssueRequistionDetail issueRequisitionDetail : product
					.getIssueRequistionDetails()) {
				dt = new LocalDate(issueRequisitionDetail.getIssueRequistion()
						.getIssueDate());
				if (!dt.isBefore(date1) && !dt.isAfter(date2)) {
					validFlag = true;
					issueRequisitionDetailVO = new IssueRequistionDetailVO();
					BeanUtils.copyProperties(issueRequisitionDetailVO,
							issueRequisitionDetail);
					details2 = vo.getIssueRequistionDetailVOMap().get(
							dt.toString("dd-MMM-yyyy"));
					if (details2 != null && details2.size() > 0) {
						details2.add(issueRequisitionDetailVO);
					} else {
						details2 = new ArrayList<IssueRequistionDetailVO>();
						details2.add(issueRequisitionDetailVO);
					}
					vo.getIssueRequistionDetailVOMap().put(
							dt.toString("dd-MMM-yyyy"), details2);
				}
			}

			// Issuance Details
			for (IssueReturnDetail issueReturnDetail : product
					.getIssueReturnDetails()) {
				dt = new LocalDate(issueReturnDetail.getIssueReturn()
						.getReturnDate());
				if (!dt.isBefore(date1) && !dt.isAfter(date2)) {
					validFlag = true;
					issueReturnDetailVO = new IssueReturnDetailVO();
					BeanUtils.copyProperties(issueReturnDetailVO,
							issueReturnDetail);
					details3 = vo.getIssueReturnDetailVOMap().get(
							dt.toString("dd-MMM-yyyy"));
					if (details3 != null && details3.size() > 0) {
						details3.add(issueReturnDetailVO);
					} else {
						details3 = new ArrayList<IssueReturnDetailVO>();
						details3.add(issueReturnDetailVO);
					}
					vo.getIssueReturnDetailVOMap().put(
							dt.toString("dd-MMM-yyyy"), details3);
				}
			}
			if (validFlag)
				productVOs.add(vo);
		}
		return productVOs;
	}

	// Save Stock info
	public void updatePeriodicStocks(String month, List<Stock> stocks)
			throws Exception {

		List<StockPeriodic> periodicSotcksForSamePeriod = stockService
				.getStockPeriodicByMonth(getImplementation(), month);
		if (periodicSotcksForSamePeriod != null
				&& periodicSotcksForSamePeriod.size() > 0) {
			stockService.deleteStockPeriodicAll(periodicSotcksForSamePeriod);
		}

		List<StockPeriodic> stockPeriodics = new ArrayList<StockPeriodic>();
		StockPeriodic periodStock = null;
		for (Stock stock : stocks) {
			periodStock = new StockPeriodic();
			periodStock.setImplementation(getImplementation());
			periodStock.setMonth(month);
			periodStock.setProduct(stock.getProduct());
			periodStock.setQuantity(stock.getQuantity());
			periodStock.setShelf(stock.getShelf());
			periodStock.setStore(stock.getStore());
			periodStock.setUnitRate(stock.getUnitRate());
			stockPeriodics.add(periodStock);
		}

		stockService.updateStockPeriodic(stockPeriodics);
	}

	// Delete stock by delivery note
	public void deleteStock(Stock stock) throws Exception {
		stockService.deleteStock(stock);
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters and Setters
	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public ProductPricingBL getProductPricingBL() {
		return productPricingBL;
	}

	public void setProductPricingBL(ProductPricingBL productPricingBL) {
		this.productPricingBL = productPricingBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public PackageConversionBL getPackageConversionBL() {
		return packageConversionBL;
	}

	public void setPackageConversionBL(PackageConversionBL packageConversionBL) {
		this.packageConversionBL = packageConversionBL;
	}
}
