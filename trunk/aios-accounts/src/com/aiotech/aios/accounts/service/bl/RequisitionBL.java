package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionVO;
import com.aiotech.aios.accounts.service.RequisitionService;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class RequisitionBL {

	private PurchaseBL purchaseBL;
	private RequisitionService requisitionService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;
	private LookupMasterBL lookupMasterBL;

	public List<Object> getAllRequisitions() throws Exception {
		List<Requisition> requisitions = requisitionService
				.getAllRequisitions(getImplementation());
		List<Object> requisitionVO = new ArrayList<Object>();
		if (null != requisitions && requisitions.size() > 0) {
			for (Requisition requisition : requisitions) {
				requisitionVO.add(addRequisitionVO(requisition));
			}
		}
		return requisitionVO;
	}

	public List<Object> getAllRequisitionsSelf() throws Exception {
		List<Requisition> requisitions = requisitionService
				.getAllRequisitions(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		List<Object> requisitionVO = new ArrayList<Object>();
		if (null != requisitions && requisitions.size() > 0) {
			for (Requisition requisition : requisitions) {
				if ((long) requisition.getPerson().getPersonId() == (long) user
						.getPersonId())
					requisitionVO.add(addRequisitionVO(requisition));
			}
		}
		return requisitionVO;
	}

	public List<Object> showAllIssueRequisitions() throws Exception {
		List<Requisition> requisitions = requisitionService
				.getAllQuotationPurchasedIssuedRequisitions(
						getImplementation(),
						RequisitionProcessStatus.Issued.getCode());
		List<Object> requisitionVO = new ArrayList<Object>();
		if (null != requisitions && requisitions.size() > 0) {
			double requistionQuantity = 0;
			double processedQuantity = 0;
			boolean addFlag = true;
			for (Requisition requisition : requisitions) {
				addFlag = true;
				requistionQuantity = 0;
				if ((null != requisition.getIssueRequistions() && requisition
						.getIssueRequistions().size() > 0)) {
					for (RequisitionDetail requisitionDetail : requisition
							.getRequisitionDetails()) {
						requistionQuantity = requisitionDetail.getQuantity();
						processedQuantity = 0;
						if (null != requisitionDetail
								.getIssueRequistionDetails()
								&& requisitionDetail
										.getIssueRequistionDetails().size() > 0) {
							for (IssueRequistionDetail issueRequistionDetail : requisitionDetail
									.getIssueRequistionDetails()) {
								processedQuantity += issueRequistionDetail
										.getQuantity();
							}
						}
						if (requistionQuantity > processedQuantity) {
							addFlag = true;
							break;
						} else
							addFlag = false;
					}
				}
				if (addFlag)
					requisitionVO.add(addRequisitionVO(requisition));
			}
		}
		return requisitionVO;
	}

	public List<Object> showAllQuotationRequisitions() throws Exception {
		/*
		 * List<Requisition> requisitions = requisitionService
		 * .getAllQuotationPurchasedIssuedRequisitions( getImplementation(),
		 * RequisitionProcessStatus.Issued.getCode());
		 */
		List<Requisition> requisitions = requisitionService
				.getAllPurchasedIssuedRequisitions(getImplementation());
		List<Object> requisitionVO = new ArrayList<Object>();
		if (null != requisitions && requisitions.size() > 0) {
			double requistionQuantity = 0;
			double processedQuantity = 0;
			boolean addFlag = true;
			for (Requisition requisition : requisitions) {
				addFlag = true;
				requistionQuantity = 0;
				if ((null != requisition.getPurchases() && requisition
						.getPurchases().size() > 0)
						|| (null != requisition.getIssueRequistions() && requisition
								.getIssueRequistions().size() > 0)) {
					for (RequisitionDetail requisitionDetail : requisition
							.getRequisitionDetails()) {
						requistionQuantity = requisitionDetail.getQuantity();
						processedQuantity = 0;
						if (null != requisitionDetail
								.getIssueRequistionDetails()
								&& requisitionDetail
										.getIssueRequistionDetails().size() > 0) {
							for (IssueRequistionDetail issueRequistionDetail : requisitionDetail
									.getIssueRequistionDetails()) {
								processedQuantity += issueRequistionDetail
										.getQuantity();
							}
						}

						if (null != requisitionDetail.getPurchaseDetails()
								&& requisitionDetail.getPurchaseDetails()
										.size() > 0) {
							for (PurchaseDetail purchaseDetail : requisitionDetail
									.getPurchaseDetails()) {
								processedQuantity += purchaseDetail
										.getQuantity();
							}
						}
						if (requistionQuantity > processedQuantity) {
							addFlag = true;
							break;
						} else
							addFlag = false;
					}
				}
				if (addFlag)
					requisitionVO.add(addRequisitionVO(requisition));
			}
		}
		return requisitionVO;
	}

	public List<Object> showAllPurchaseRequisitions() throws Exception {
		List<Requisition> requisitions = requisitionService
				.getAllQuotationPurchasedIssuedRequisitions(
						getImplementation(),
						RequisitionProcessStatus.Issued.getCode());
		List<Object> requisitionVO = new ArrayList<Object>();
		if (null != requisitions && requisitions.size() > 0) {
			double requistionQuantity = 0;
			double processedQuantity = 0;
			boolean addFlag = true;
			for (Requisition requisition : requisitions) {
				addFlag = true;
				requistionQuantity = 0;
				if ((null != requisition.getQuotations() && requisition
						.getQuotations().size() > 0)
						|| (null != requisition.getPurchases() && requisition
								.getPurchases().size() > 0)
						|| (null != requisition.getIssueRequistions() && requisition
								.getIssueRequistions().size() > 0)) {
					for (RequisitionDetail requisitionDetail : requisition
							.getRequisitionDetails()) {
						requistionQuantity = requisitionDetail.getQuantity();
						processedQuantity = 0;
						if (null != requisitionDetail
								.getIssueRequistionDetails()
								&& requisitionDetail
										.getIssueRequistionDetails().size() > 0) {
							for (IssueRequistionDetail issueRequistionDetail : requisitionDetail
									.getIssueRequistionDetails()) {
								processedQuantity += issueRequistionDetail
										.getQuantity();
							}
						}
						if (null != requisitionDetail.getQuotationDetails()
								&& requisitionDetail.getQuotationDetails()
										.size() > 0) {
							for (QuotationDetail quotationDetail : requisitionDetail
									.getQuotationDetails()) {
								if (null != quotationDetail.getQuotation()
										.getPurchases()) {
									for (Purchase purchase : quotationDetail
											.getQuotation().getPurchases()) {
										for (PurchaseDetail prDt : purchase
												.getPurchaseDetails()) {
											processedQuantity += prDt
													.getQuantity();
										}
									}
								}
							}
						}
						if (null != requisitionDetail.getPurchaseDetails()
								&& requisitionDetail.getPurchaseDetails()
										.size() > 0) {
							for (PurchaseDetail purchaseDetail : requisitionDetail
									.getPurchaseDetails()) {
								processedQuantity += purchaseDetail
										.getQuantity();
							}
						}
						if (requistionQuantity > processedQuantity) {
							addFlag = true;
							break;
						} else
							addFlag = false;
					}
				}
				if (addFlag)
					requisitionVO.add(addRequisitionVO(requisition));
			}
		}
		return requisitionVO;
	}

	private Object addRequisitionVO(Requisition requisition) {
		RequisitionVO requisitionVO = new RequisitionVO();
		requisitionVO.setRequisitionId(requisition.getRequisitionId());
		requisitionVO.setDate(DateFormat.convertDateToString(requisition
				.getRequisitionDate().toString()));
		requisitionVO.setReferenceNumber(requisition.getReferenceNumber());
		requisitionVO.setLocationName(requisition.getCmpDeptLoc()
				.getDepartment().getDepartmentName());
		requisitionVO.setPersonName(requisition.getPerson().getFirstName()
				.concat(" " + requisition.getPerson().getLastName()));
		requisitionVO.setStatusStr(RequisitionProcessStatus.get(
				requisition.getStatus()).name());
		requisitionVO.setLocationId(requisition.getCmpDeptLoc()
				.getCmpDeptLocId());

		// purchase details
		Set<String> purcahseNoSet = new HashSet<String>();
		for (Purchase purchase : requisition.getPurchases()) {
			purcahseNoSet.add(purchase.getPurchaseNumber());
		}
		StringBuilder purchaseNumber = new StringBuilder();
		for (String purchaseNo : purcahseNoSet)
			purchaseNumber.append(purchaseNo).append(" ,");

		requisitionVO.setPurchaseNumber(purchaseNumber.toString());

		// receive details
		Set<String> receiveNoSet = new HashSet<String>();
		for (Purchase purchase : requisition.getPurchases()) {
			for (ReceiveDetail receiveDetail : purchase.getReceiveDetails()) {
				receiveNoSet.add(receiveDetail.getReceive().getReceiveNumber());
			}
		}
		StringBuilder receiveNumber = new StringBuilder();
		for (String receiveNo : receiveNoSet)
			receiveNumber.append(receiveNo).append(" ,");

		requisitionVO.setReceiveNumber(receiveNumber.toString());

		return requisitionVO;
	}

	public void saveRequisition(Requisition requisition,
			List<RequisitionDetail> requisitionDetails,
			List<RequisitionDetail> deletedRequisitionDetails) throws Exception {
		requisition.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		boolean editFlag = false;
		if (requisition.getRequisitionId() != null
				&& requisition.getRequisitionId() > 0)
			editFlag = true;

		requisition.setImplementation(getImplementation());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (requisition != null && requisition.getRequisitionId() != null
				&& requisition.getRequisitionId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		requisition.setRequisitionDetails(new HashSet<RequisitionDetail>(
				requisitionDetails));
		if (null == requisition.getRequisitionId())
			SystemBL.saveReferenceStamp(Requisition.class.getName(),
					getImplementation());
		if (null != deletedRequisitionDetails
				&& deletedRequisitionDetails.size() > 0)
			requisitionService
					.deleteRequisitionDetail(deletedRequisitionDetails);
		requisitionService.saveRequisition(requisition, requisitionDetails);

		saveDocuments(requisition, editFlag);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Requisition.class.getSimpleName(),
				requisition.getRequisitionId(), user, workflowDetailVo);

	}

	public void saveDocuments(Requisition req, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(req,
				"requisitionDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, req.getRequisitionId(), "Requisition",
					"requisitionDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public void deleteRequisition(Requisition requisition,
			List<RequisitionDetail> requisitionDetails) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Requisition.class.getSimpleName(),
				requisition.getRequisitionId(), user, workflowDetailVo);
	}

	public void doRequisitionBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Requisition requisition = this.getRequisitionService()
				.getRequisitionById(recordId);
		List<RequisitionDetail> requisitionDetails = new ArrayList<RequisitionDetail>(
				requisition.getRequisitionDetails());

		if (workflowDetailVO.isDeleteFlag()) {
			requisitionService.deleteRequisition(requisition,
					requisitionDetails);
		} else {

		}
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public RequisitionService getRequisitionService() {
		return requisitionService;
	}

	public void setRequisitionService(RequisitionService requisitionService) {
		this.requisitionService = requisitionService;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}
}
