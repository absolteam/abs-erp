package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PaymentRequestDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PaymentRequestVO;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.PaymentRequestService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PaymentRequestBL {
	private PaymentRequestService paymentRequestService;
	private Implementation implementation;
	private LookupMasterBL lookupMasterBL;
	private CurrencyService currencyService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public JSONObject getAllPaymentRequest() throws Exception {
		List<PaymentRequest> paymentRequestList = this
				.getPaymentRequestService().getAllPaymentRequest(
						getImplementation());
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		double totatAmount = 0;
		String categoryName = "";
		for (PaymentRequest list : paymentRequestList) {
			totatAmount = 0;
			categoryName = "";
			array = new JSONArray();
			array.add(list.getPaymentRequestId());
			array.add(list.getPaymentReqNo());
			array.add(DateFormat.convertDateToString(list.getRequestDate()
					.toString()));
			for (PaymentRequestDetail payDt : list.getPaymentRequestDetails()) {
				totatAmount += payDt.getAmount();
				categoryName += payDt.getLookupDetail().getDisplayName() + ",";
			}
			array.add(AIOSCommons.formatAmount(totatAmount));
			array.add(categoryName);
			array.add(null != list.getPersonByPersonId() ? list
					.getPersonByPersonId().getFirstName().concat(" ")
					.concat(list.getPersonByPersonId().getLastName()) : list
					.getPersonName());
			array.add(Constants.Accounts.PaymentRequestStatus.get(list
					.getStatus()));
			array.add(list.getPersonByCreatedBy().getFirstName().concat(" ")
					.concat(list.getPersonByCreatedBy().getLastName()));
			array.add(DateFormat.convertDateToString(list.getCreatedDate()
					.toString()));
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public List<Object> convertPaymentRequestObject(
			List<PaymentRequest> paymentRequests) throws Exception {
		List<Object> paymentRequestObject = new ArrayList<Object>();
		if (null != paymentRequests && paymentRequests.size() > 0) {
			for (PaymentRequest paymentRequest : paymentRequests)
				paymentRequestObject.add(addPaymentRequest(paymentRequest));
		}
		return paymentRequestObject;
	}

	private PaymentRequestVO addPaymentRequest(PaymentRequest paymentRequest)
			throws Exception {
		PaymentRequestVO paymentRequestVO = new PaymentRequestVO();
		paymentRequestVO.setPaymentRequestId(paymentRequest
				.getPaymentRequestId());
		paymentRequestVO.setPaymentReqNo(paymentRequest.getPaymentReqNo());
		paymentRequestVO.setDate(DateFormat.convertDateToString(paymentRequest
				.getRequestDate().toString()));
		paymentRequestVO.setCreatedBy(null != paymentRequest
				.getPersonByCreatedBy() ? paymentRequest.getPersonByCreatedBy()
				.getFirstName()
				+ " "
				+ paymentRequest.getPersonByCreatedBy().getLastName() : "");
		paymentRequestVO.setRequestedBy(null != paymentRequest
				.getPersonByPersonId() ? paymentRequest.getPersonByPersonId()
				.getFirstName()
				+ " "
				+ paymentRequest.getPersonByPersonId().getLastName()
				: paymentRequest.getPersonName());
		return paymentRequestVO;
	}

	public void savePaymentRequest(PaymentRequest paymentRequest,
			List<PaymentRequestDetail> paymentRequestDetails,
			List<PaymentRequestDetail> deletePaymentRequestDetails, Long alertId) {
		try {
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (paymentRequest != null
					&& paymentRequest.getPaymentRequestId() != null
					&& paymentRequest.getPaymentRequestId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			workflowDetailVo.setRejectAlertId(alertId);
			paymentRequestService.savePaymentRequest(paymentRequest,
					paymentRequestDetails);
			if (null != deletePaymentRequestDetails
					&& deletePaymentRequestDetails.size() > 0)
				paymentRequestService
						.deletePaymentRequestDetails(deletePaymentRequestDetails);
			SystemBL.saveReferenceStamp(PaymentRequest.class.getName(),
					getImplementation());
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"PaymentRequest", paymentRequest.getPaymentRequestId(),
					user, workflowDetailVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deletePaymentRequest(PaymentRequest paymentRequest,
			Long messageId) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				PaymentRequest.class.getSimpleName(),
				paymentRequest.getPaymentRequestId(), user, workflowDetailVo);

	}

	public void doPaymentRequestBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		PaymentRequest paymentRequest = paymentRequestService
				.getPaymentRequestById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {

			paymentRequestService.deletePaymentRequest(
					paymentRequest,
					new ArrayList<PaymentRequestDetail>(paymentRequest
							.getPaymentRequestDetails()));
		}
	}

	public Map<Byte, String> getModeOfPayment() throws Exception {
		Map<Byte, String> modeOfPayments = new HashMap<Byte, String>();
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class)) {
			modeOfPayments.put(modeOfPayment.getCode(), modeOfPayment.name()
					.replaceAll("_", " "));
		}
		return modeOfPayments;
	}

	public PaymentRequestService getPaymentRequestService() {
		return paymentRequestService;
	}

	public void setPaymentRequestService(
			PaymentRequestService paymentRequestService) {
		this.paymentRequestService = paymentRequestService;
	}

	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
