package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.IssueReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferVO;
import com.aiotech.aios.accounts.service.MaterialTransferService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class MaterialTransferBL {

	// Dependencies
	private StockBL stockBL;
	private StoreBL storeBL;
	private ProductBL productBL;
	private TransactionBL transactionBL;
	private LookupMasterBL lookupMasterBL;
	private MaterialRequisitionBL materialRequisitionBL;
	private MaterialTransferService materialTransferService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<Object> showAllMaterialTransfer() throws Exception {
		List<MaterialTransfer> materialTransfers = materialTransferService
				.getAllMaterialTransfer(getImplementation());
		List<Object> materialTransferVOs = new ArrayList<Object>();
		if (null != materialTransfers && materialTransfers.size() > 0) {
			for (MaterialTransfer materialTransfer : materialTransfers)
				materialTransferVOs
						.add(addMaterialTransferVO(materialTransfer));
		}
		return materialTransferVOs;
	}

	public void saveMaterialTransfer(MaterialTransfer materialTransfer,
			List<MaterialTransferDetail> materialTransferDetails,
			List<MaterialRequisition> materialRequisitions) {
		try {
			boolean updateFlag = false;
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			materialTransfer.setImplementation(getImplementation());
			materialTransfer.setPersonByCreatedBy(person);
			materialTransfer.setCreatedDate(Calendar.getInstance().getTime());
			materialTransfer
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (materialTransfer != null
					&& materialTransfer.getMaterialTransferId() != null
					&& materialTransfer.getMaterialTransferId() > 0) {
				updateFlag = true;
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}

			if (!updateFlag)
				SystemBL.saveReferenceStamp(MaterialTransfer.class.getName(),
						getImplementation());
			materialTransfer
					.setMaterialTransferDetails(new HashSet<MaterialTransferDetail>(
							materialTransferDetails));

			materialTransferService.saveMaterialTransfer(materialTransfer,
					materialTransferDetails);
			if (null != materialRequisitions && materialRequisitions.size() > 0) {
				materialRequisitionBL.getMaterialRequisitionService()
						.saveMaterialRequisition(materialRequisitions);
				Set<Alert> alerts = new HashSet<Alert>();
				for (MaterialRequisition materialRequisition : materialRequisitions) {
					Alert alert = transactionBL
							.getAlertBL()
							.getAlertService()
							.getAlertRecordInfo(
									materialRequisition
											.getMaterialRequisitionId(),
									MaterialRequisition.class.getSimpleName());
					alert.setIsActive(false);
					alerts.add(alert);
				}
				transactionBL.getAlertBL().getAlertService().saveAlert(alerts);
			}

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					MaterialTransfer.class.getSimpleName(),
					materialTransfer.getMaterialTransferId(), user,
					workflowDetailVo);

		} catch (Exception e) {

		}
	}

	public void doMaterialTransferBusinessUpdate(Long materialTransferId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			MaterialTransfer materialTransfer = materialTransferService
					.getMaterialTransferById(materialTransferId);
			if (workflowDetailVO.isDeleteFlag()) {
				List<Stock> addStocks = new ArrayList<Stock>();
				Stock stock = null;
				List<Stock> deleteStocks = new ArrayList<Stock>();
				for (MaterialTransferDetail materialTransferDetail : materialTransfer
						.getMaterialTransferDetails()) {
					stock = new Stock();
					stock.setProduct(materialTransferDetail.getProduct());
					stock.setStore(materialTransferDetail.getShelfByShelfId()
							.getAisle().getStore());
					stock.setShelf(materialTransferDetail.getShelfByShelfId());
					stock.setQuantity(materialTransferDetail.getQuantity());
					deleteStocks.add(stock);

					stock = new Stock();
					stock.setProduct(materialTransferDetail.getProduct());
					stock.setStore(materialTransferDetail
							.getShelfByFromShelfId().getAisle().getStore());
					stock.setShelf(materialTransferDetail
							.getShelfByFromShelfId());
					stock.setQuantity(materialTransferDetail.getQuantity());
					addStocks.add(stock);
				}
				stockBL.updateStockDetails(addStocks, deleteStocks);
				materialTransferService.deleteMaterialTransfer(
						materialTransfer,
						new ArrayList<MaterialTransferDetail>(materialTransfer
								.getMaterialTransferDetails()));
			} else {
				List<Stock> addStocks = new ArrayList<Stock>();
				Stock stock = null;
				Shelf shelf = null;
				List<Stock> deleteStocks = new ArrayList<Stock>();
				for (MaterialTransferDetail materialTransferDetail : materialTransfer
						.getMaterialTransferDetails()) {
					stock = new Stock();
					// Increase
					shelf = storeBL.getStoreService().getStoreByShelfId(
							materialTransferDetail.getShelfByShelfId()
									.getShelfId());
					stock.setProduct(materialTransferDetail.getProduct());
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setQuantity(materialTransferDetail.getQuantity());
					stock.setUnitRate(materialTransferDetail.getUnitRate());
					stock.setImplementation(getImplementation());
					stock.setShelf(shelf);
					addStocks.add(stock);

					// Reduce
					stock = new Stock();
					shelf = storeBL.getStoreService().getStoreByShelfId(
							materialTransferDetail.getShelfByFromShelfId()
									.getShelfId());
					stock.setProduct(materialTransferDetail.getProduct());
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setShelf(shelf);
					stock.setQuantity(materialTransferDetail.getQuantity());
					deleteStocks.add(stock);
				}
				stockBL.updateStockDetails(addStocks, deleteStocks);
			}
		} catch (Exception ex) {

		}
	}

	@SuppressWarnings("unused")
	private Transaction materialTransferTransaction(
			MaterialTransfer materialTransfer,
			List<MaterialTransferDetail> materialTransferDetails)
			throws Exception {
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		for (MaterialTransferDetail materialTransferDetail : materialTransferDetails) {
			transactionDetails.add(createInventoryCreditEntry(materialTransfer,
					materialTransferDetail));
			transactionDetails.add(createInventoryDebitEntry(materialTransfer,
					materialTransferDetail));
		}
		Transaction transaction = createTransaction(
				materialTransfer,
				"Material Transfer Reference: "
						+ materialTransfer.getReferenceNumber());
		transaction.setTransactionDetails(transactionDetails);
		return transaction;
	}

	private Transaction createTransaction(MaterialTransfer materialTransfer,
			String description) throws Exception {
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), description, materialTransfer
				.getTransferDate(), transactionBL
				.getCategory("MATERIAL TRANSFER"), materialTransfer
				.getMaterialTransferId(), MaterialTransfer.class
				.getSimpleName());
		return transaction;
	}

	private TransactionDetail createInventoryDebitEntry(
			MaterialTransfer materialTransfer,
			MaterialTransferDetail materialTransferDetail) throws Exception {
		Combination combination = productBL
				.getProductInventory(materialTransferDetail.getProduct()
						.getProductId());
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(
						(materialTransferDetail.getUnitRate() * materialTransferDetail
								.getQuantity()),
						combination.getCombinationId(), TransactionType.Debit
								.getCode(), null, null, materialTransfer
								.getReferenceNumber());
		return transactionDetail;
	}

	private TransactionDetail createInventoryCreditEntry(
			MaterialTransfer materialTransfer,
			MaterialTransferDetail materialTransferDetail) throws Exception {
		Combination combination = productBL
				.getProductInventory(materialTransferDetail.getProduct()
						.getProductId());
		TransactionDetail transactionDetail = transactionBL
				.createTransactionDetail(
						(materialTransferDetail.getUnitRate() * materialTransferDetail
								.getQuantity()),
						combination.getCombinationId(), TransactionType.Credit
								.getCode(), null, null, materialTransfer
								.getReferenceNumber());
		return transactionDetail;
	}

	public void deleteMaterialTransfer(MaterialTransfer materialTransfer,
			List<MaterialTransferDetail> materialTransferDetails)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				MaterialTransfer.class.getSimpleName(),
				materialTransfer.getMaterialTransferId(), user,
				workflowDetailVo);
	}

	private MaterialTransferVO addMaterialTransferVO(
			MaterialTransfer materialTransfer) {
		MaterialTransferVO materialTransferVO = new MaterialTransferVO();
		materialTransferVO.setMaterialTransferId(materialTransfer
				.getMaterialTransferId());
		materialTransferVO.setReferenceNumber(materialTransfer
				.getReferenceNumber());
		materialTransferVO.setDate(DateFormat
				.convertDateToString(materialTransfer.getTransferDate()
						.toString()));
		materialTransferVO.setTransferPerson(materialTransfer
				.getPersonByTransferPerson()
				.getFirstName()
				.concat(" ")
				.concat(materialTransfer.getPersonByTransferPerson()
						.getLastName()));
		return materialTransferVO;
	}

	public List<MaterialTransferVO> validateMaterialTransfers(
			List<MaterialTransferDetail> materialTransferDetails)
			throws Exception {

		MaterialTransferVO materialTransferVO = null;
		MaterialTransferDetailVO materialTransferDetailVO = null;
		Map<Long, MaterialTransferVO> materialTransferVOHash = new HashMap<Long, MaterialTransferVO>();
		Map<Long, List<MaterialTransferDetailVO>> materialTransferDetailVOHash = new HashMap<Long, List<MaterialTransferDetailVO>>();
		List<MaterialTransferDetailVO> transferDetailsVOs = new ArrayList<MaterialTransferDetailVO>();
		IssueReturnDetailVO issueReturnDetailVO = null;
		Map<Long, List<IssueReturnDetailVO>> issueReturnVOHash = new HashMap<Long, List<IssueReturnDetailVO>>();
		List<IssueReturnDetailVO> issueReturnDetailVOList = null;

		for (MaterialTransferDetail materialTransferDetail : materialTransferDetails) {
			materialTransferVO = new MaterialTransferVO();
			materialTransferDetailVO = new MaterialTransferDetailVO();
			materialTransferVO.setMaterialTransferId(materialTransferDetail
					.getMaterialTransfer().getMaterialTransferId());
			materialTransferVO.setReferenceNumber(materialTransferDetail
					.getMaterialTransfer().getReferenceNumber());
			materialTransferVO.setTransferDate(materialTransferDetail
					.getMaterialTransfer().getTransferDate());
			materialTransferVO.setDescription(materialTransferDetail
					.getMaterialTransfer().getDescription());
			materialTransferVO.setPersonByTransferPerson(materialTransferDetail
					.getMaterialTransfer().getPersonByTransferPerson());
			materialTransferVO.setPersonByCreatedBy(materialTransferDetail
					.getMaterialTransfer().getPersonByCreatedBy());
			materialTransferVO.setCreatedDate(materialTransferDetail
					.getMaterialTransfer().getCreatedDate());
			materialTransferVO.setIsApprove(materialTransferDetail
					.getMaterialTransfer().getIsApprove());

			materialTransferDetailVO
					.setMaterialTransferDetailId(materialTransferDetail
							.getMaterialTransferDetailId());
			materialTransferDetailVO.setProduct(materialTransferDetail
					.getProduct());
			materialTransferDetailVO.setQuantity(materialTransferDetail
					.getQuantity());
			materialTransferDetailVO.setUnitRate(materialTransferDetail
					.getUnitRate());
			materialTransferDetailVO.setDescription(materialTransferDetail
					.getDescription());
			materialTransferDetailVO.setReturnQty(materialTransferDetail
					.getQuantity());
			materialTransferDetailVO
					.setIssueReturnDetails(materialTransferDetail
							.getIssueReturnDetails());
			transferDetailsVOs.add(materialTransferDetailVO);
			List<MaterialTransferDetailVO> transferDetailVOList = materialTransferDetailVOHash
					.get(materialTransferVO.getMaterialTransferId());

			if (null != transferDetailVOList && transferDetailVOList.size() > 0) {
				transferDetailVOList.add(materialTransferDetailVO);
				materialTransferDetailVOHash.put(
						materialTransferVO.getMaterialTransferId(),
						transferDetailVOList);
			} else {
				transferDetailVOList = new ArrayList<MaterialTransferDetailVO>();
				transferDetailVOList.add(materialTransferDetailVO);
				materialTransferDetailVOHash.put(
						materialTransferVO.getMaterialTransferId(),
						transferDetailVOList);
			}
			if (null != materialTransferDetail.getIssueReturnDetails()
					&& materialTransferDetail.getIssueReturnDetails().size() > 0) {
				issueReturnDetailVOList = new ArrayList<IssueReturnDetailVO>();
				for (IssueReturnDetail returnDetail : materialTransferDetail
						.getIssueReturnDetails()) {
					issueReturnDetailVO = new IssueReturnDetailVO();
					issueReturnDetailVO.setIssueReturnDetailId(returnDetail
							.getIssueReturnDetailId());
					issueReturnDetailVO.setReturnQuantity(returnDetail
							.getReturnQuantity());
					issueReturnDetailVO.setDescription(returnDetail
							.getDescription());
					issueReturnDetailVOList.add(issueReturnDetailVO);
				}
			}
			if (issueReturnVOHash.containsKey(materialTransferDetail
					.getMaterialTransferDetailId())) {
				issueReturnDetailVOList.addAll(issueReturnVOHash
						.get(materialTransferDetail
								.getMaterialTransferDetailId()));
			}
			if (null != issueReturnDetailVOList
					&& issueReturnDetailVOList.size() > 0)
				issueReturnVOHash.put(
						materialTransferDetail.getMaterialTransferDetailId(),
						issueReturnDetailVOList);
			materialTransferVOHash.put(
					materialTransferVO.getMaterialTransferId(),
					materialTransferVO);
		}
		Map<Long, MaterialTransferVO> issueVOReturns = new HashMap<Long, MaterialTransferVO>();
		for (Entry<Long, List<MaterialTransferDetailVO>> issueId : materialTransferDetailVOHash
				.entrySet()) {
			List<MaterialTransferDetailVO> issueDetailVOList = materialTransferDetailVOHash
					.get(issueId.getKey());
			issueReturnDetailVOList = new ArrayList<IssueReturnDetailVO>();
			for (MaterialTransferDetailVO detailVO : issueDetailVOList) {
				if (issueReturnVOHash.containsKey(detailVO
						.getMaterialTransferDetailId())) {
					List<IssueReturnDetailVO> returnDetails = issueReturnVOHash
							.get(detailVO.getMaterialTransferDetailId());
					detailVO.setTransferedQty(detailVO.getQuantity()
							+ (null != detailVO.getTransferedQty() ? detailVO
									.getTransferedQty() : 0));
					detailVO.setTotalRate(detailVO.getTransferedQty()
							* detailVO.getUnitRate());
					detailVO.setDescription(detailVO.getDescription());
					if (null != returnDetails && returnDetails.size() > 0) {
						for (IssueReturnDetailVO returnDetail : returnDetails) {
							detailVO.setReturnDetailId(returnDetail
									.getIssueReturnDetailId());
							detailVO.setReturnedQty(returnDetail
									.getReturnQuantity()
									+ (null != detailVO.getReturnedQty() ? detailVO
											.getReturnedQty() : 0));
							detailVO.setReturnQty(detailVO.getQuantity()
									- detailVO.getReturnedQty());
						}
					}
					if (issueReturnVOHash.containsKey(detailVO
							.getMaterialTransferDetailId())) {
						issueReturnDetailVOList.addAll(issueReturnVOHash
								.get(detailVO.getMaterialTransferDetailId()));
					}
					issueReturnVOHash.put(
							detailVO.getMaterialTransferDetailId(),
							returnDetails);
				}
			}
			if (materialTransferVOHash.containsKey(issueId.getKey())) {
				materialTransferVO = materialTransferVOHash.get(issueId
						.getKey());
				materialTransferVO
						.setMaterialTransferDetailVOs(issueDetailVOList);
				boolean addFlag = true;
				for (MaterialTransferDetailVO issueDetail : materialTransferVO
						.getMaterialTransferDetailVOs()) {
					if (null == issueDetail.getReturnedQty()
							|| (issueDetail.getReturnedQty() < issueDetail
									.getTransferedQty())) {
						addFlag = true;
						materialTransferVO
								.setTotalTransferedQty(issueDetail
										.getQuantity()
										+ (null != materialTransferVO
												.getTotalTransferedQty() ? materialTransferVO
												.getTotalTransferedQty() : 0));
						materialTransferVO
								.setTotalTransferAmount(issueDetail
										.getQuantity()
										* issueDetail.getUnitRate()
										+ (null != materialTransferVO
												.getTotalTransferAmount() ? materialTransferVO
												.getTotalTransferAmount() : 0));
					} else
						addFlag = false;
				}
				if (addFlag) {
					issueVOReturns.put(issueId.getKey(), materialTransferVO);
					materialTransferVOHash.put(issueId.getKey(),
							materialTransferVO);
				}
			}
		}
		return new ArrayList<MaterialTransferVO>(issueVOReturns.values());
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public MaterialTransferService getMaterialTransferService() {
		return materialTransferService;
	}

	public void setMaterialTransferService(
			MaterialTransferService materialTransferService) {
		this.materialTransferService = materialTransferService;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public MaterialRequisitionBL getMaterialRequisitionBL() {
		return materialRequisitionBL;
	}

	public void setMaterialRequisitionBL(
			MaterialRequisitionBL materialRequisitionBL) {
		this.materialRequisitionBL = materialRequisitionBL;
	}
}
