package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CreditService {

	private AIOTechGenericDAO<Credit> creditDAO;
	private AIOTechGenericDAO<CreditDetail> creditDetailDAO;

	public List<Credit> getAllCreditNotes(Implementation implementation)
			throws Exception {
		return creditDAO.findByNamedQuery("getAllCreditNotes", implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<Credit> getAllCreditNotesByReturn(Implementation implementation)
			throws Exception {
		return creditDAO.findByNamedQuery("getAllCreditNotesByReturn",
				implementation);
	}

	// Save Credit Note
	public void saveCreditNote(Credit credit, List<CreditDetail> creditDetails)
			throws Exception {
		creditDAO.saveOrUpdate(credit);
		for (CreditDetail list : creditDetails) {
			list.setCredit(credit);
		}
		creditDetailDAO.saveOrUpdateAll(creditDetails);
	}

	// Delete Credit Note Details
	public void deleteCreditDetails(List<CreditDetail> creditDetails)
			throws Exception {
		creditDetailDAO.deleteAll(creditDetails);
	}

	public Credit getCreditNote(Long creditId) throws Exception {
		return creditDAO.findById(creditId);
	}

	// Get simple credit detail by credit id
	public List<CreditDetail> getCreditDetailById(Long creditId)
			throws Exception {
		return creditDetailDAO
				.findByNamedQuery("getCreditDetailById", creditId);
	}

	// Get credit detail by sales delivery detail
	public List<CreditDetail> getCreditDetailByDeliveryDetailId(
			Long salesDeliveryDetailId) throws Exception {
		return creditDetailDAO.findByNamedQuery(
				"getCreditDetailByDeliveryDetailId", salesDeliveryDetailId);
	}

	@SuppressWarnings("unchecked")
	public List<Credit> getFilteredCreditNotes(Implementation implementation,
			Long creditId, Long customerId, Date fromDate, Date toDate)
			throws Exception {
		Criteria criteria = creditDAO.createCriteria();

		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "per",
				CriteriaSpecification.LEFT_JOIN); 
		criteria.createAlias("creditDetails", "cd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cd.salesDeliveryDetail", "sdd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sdd.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sdd.salesInvoiceDetails", "sid",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("currency", "cur", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur.currencyPool", "curp",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (creditId != null && creditId > 0) {
			criteria.add(Restrictions.eq("creditId", creditId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("c.customerId", customerId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date", fromDate, toDate));
		}
		
		Set<Credit> uniqueCreditNotes = new HashSet<Credit>(criteria.list());
		List<Credit> credits = new ArrayList<Credit>(uniqueCreditNotes);
		
		return credits;
	}

	// Get credit detail by credit id
	public List<CreditDetail> getCreditNoteDetails(Long creditId)
			throws Exception {
		return creditDetailDAO.findByNamedQuery("getCreditNoteDetails",
				creditId);
	}

	public Credit getCreditNoteById(Long creditId) throws Exception {
		return creditDAO.findByNamedQuery("getCreditNoteById", creditId).get(0);
	}

	public Credit getCreditNoteByRecordIdUseCase(long recordId, String useCase)
			throws Exception {
		List<Credit> credit = creditDAO.findByNamedQuery("getCreditNoteByRecordIdUseCase",
				recordId, useCase);
		return null != credit && credit.size() > 0 ? credit.get(0) : null;
	}

	public Credit getCreditNoteInformationById(Long creditId) throws Exception {
		return creditDAO.findByNamedQuery("getCreditNoteInformationById",
				creditId).get(0);
	}

	public Credit getCreditNoteInformationByReference(String reference)
			throws Exception {
		List<Credit> credits = creditDAO.findByNamedQuery(
				"getCreditNoteInformationByReference", reference);
		if (credits != null && credits.size() > 0)
			return credits.get(0);
		else
			return null;
	}

	public Credit getSalesReturnBySalesReference(String reference)
			throws Exception {
		List<Credit> credits = creditDAO.findByNamedQuery(
				"getSalesReturnBySalesReference", reference);
		if (credits != null && credits.size() > 0)
			return credits.get(0);
		else
			return null;
	}

	public void deleteCreditNote(Credit credit, List<CreditDetail> detailList)
			throws Exception {
		creditDetailDAO.deleteAll(detailList);
		creditDAO.delete(credit);
	}

	public AIOTechGenericDAO<Credit> getCreditDAO() {
		return creditDAO;
	}

	public void setCreditDAO(AIOTechGenericDAO<Credit> creditDAO) {
		this.creditDAO = creditDAO;
	}

	public AIOTechGenericDAO<CreditDetail> getCreditDetailDAO() {
		return creditDetailDAO;
	}

	public void setCreditDetailDAO(
			AIOTechGenericDAO<CreditDetail> creditDetailDAO) {
		this.creditDetailDAO = creditDetailDAO;
	}
}
