package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DebitDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.BankReceiptsService;
import com.aiotech.aios.accounts.service.DebitService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class DebitBL {
	private DebitService debitService;
	private PurchaseBL purchaseBL;
	private BankReceiptsService bankReceiptsService;
	private TransactionBL transactionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private static final Logger LOGGER = LogManager.getLogger(DebitBL.class);

	public JSONObject getAllDebits(Implementation implementation)
			throws Exception {
		LOGGER.info("Module: Accounts : Method: getAllDebits: Inside BL");
		List<Debit> debitList = this.getDebitService().getAllDebits(
				implementation);
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (Debit list : debitList) {
			array = new JSONArray();
			array.add(list.getDebitId());
			array.add(list.getDebitNumber());
			array.add(DateFormat.convertDateToString(list.getDate().toString()));
			array.add(list.getGoodsReturn().getReturnNumber());
			array.add(list.getGoodsReturn().getSupplier().getSupplierNumber());
			if (null != list.getGoodsReturn().getSupplier().getPerson()
					&& !("").equals(list.getGoodsReturn().getSupplier()
							.getPerson())) {
				array.add(list
						.getGoodsReturn()
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(list.getGoodsReturn().getSupplier().getPerson()
								.getLastName()));
			} else {
				array.add(null != list.getGoodsReturn().getSupplier()
						.getCmpDeptLocation() ? list.getGoodsReturn()
						.getSupplier().getCmpDeptLocation().getCompany()
						.getCompanyName() : list.getGoodsReturn().getSupplier()
						.getCompany().getCompanyName());
			}
			array.add(list.getItemExchange());
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	public void saveDebit(Debit debit, List<DebitDetail> detailDetails)
			throws Exception {
		debit.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (debit != null && debit.getDebitId() != null
				&& debit.getDebitId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		SystemBL.saveReferenceStamp(Debit.class.getName(),
				debit.getImplementation());
		 
		debit.setDebitDetails(new HashSet<DebitDetail>(detailDetails));
		debitService.saveDebit(debit, detailDetails); 

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Debit.class.getSimpleName(), debit.getDebitId(), user,
				workflowDetailVo);
	}

	private List<TransactionDetail> createTransactionDetails(Supplier supplier,
			DebitDetail detailDetail, Debit debit) throws Exception {
		double totalAmount = detailDetail.getGoodsReturnDetail()
				.getReceiveDetail().getUnitRate()
				* detailDetail.getReturnQty();
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		Combination combination = purchaseBL.getProductBL()
				.getProductInventory(detailDetail.getProduct().getProductId());
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, combination.getCombinationId(),
				TransactionType.Debit.getCode(), null, null, debit.getDebitNumber()));
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, supplier.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(), null, null, debit.getDebitNumber()));
		return transactionDetails;
	}

	public void deleteDebits(Debit debit, List<DebitDetail> debitDetails)
			throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Debit.class.getSimpleName(), debit.getDebitId(), user,
				workflowDetailVo);
	}

	public void doDebitBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Debit debit = this.getDebitService().getDebitNotesById(recordId);
		List<DebitDetail> debitDetails = new ArrayList<DebitDetail>(
				debit.getDebitDetails());
		if (workflowDetailVO.isDeleteFlag()) {
			if (debit.getItemExchange()) {
				transactionBL.deleteTransaction(debit.getDebitId(),
						Debit.class.getSimpleName());
				// Deduct Stock
				List<Stock> stocks = new ArrayList<Stock>();
				Stock stock = null;
				Shelf shelf = null;
				for (DebitDetail detailDetail : debitDetails) {
					stock = new Stock();
					stock.setProduct(detailDetail.getProduct());
					stock.setQuantity(detailDetail.getReturnQty());
					stock.setImplementation(debit.getImplementation());
					shelf = purchaseBL
							.getStockBL()
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									detailDetail.getGoodsReturnDetail()
											.getReceiveDetail().getShelf()
											.getShelfId());
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setShelf(shelf);
					stocks.add(stock);
				}
			} else {
				purchaseBL
						.getAlertBL()
						.getAlertService()
						.deleteAlert(
								purchaseBL
										.getAlertBL()
										.getAlertService()
										.getAlertAllRecordInfo(
												debit.getDebitId(),
												Debit.class.getSimpleName()));
			}
			debitService.deleteDebits(debit, debitDetails);
		} else {
			if (debit.getItemExchange()) {
				// Stock update
				List<Stock> stocks = new ArrayList<Stock>();
				Stock stock = null;
				Shelf shelf = null;
				List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();

				for (DebitDetail detailDetail : debitDetails) {
					transactionDetails.addAll(createTransactionDetails(debit
							.getGoodsReturn().getSupplier(), detailDetail, debit));
					stock = new Stock();
					stock.setProduct(detailDetail.getProduct());
					stock.setQuantity(detailDetail.getReturnQty());
					stock.setImplementation(debit.getImplementation());
					shelf = purchaseBL
							.getStockBL()
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									detailDetail.getGoodsReturnDetail()
											.getReceiveDetail().getShelf()
											.getShelfId());
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setShelf(shelf);
					stocks.add(stock);
				}
				purchaseBL.getStockBL().updateStockDetails(stocks, null);

				// Create Transaction
				Transaction transaction = transactionBL.createTransaction(
						transactionBL.getCurrency().getCurrencyId(), null,
						debit.getDate(), transactionBL.getCategory("Debit Note"),
						debit.getDebitId(), Debit.class.getSimpleName());
				transactionBL.saveTransaction(transaction, transactionDetails);
			} else {
				debit.setDebitDetails(new HashSet<DebitDetail>(debitDetails));
				List<Debit> debits = new ArrayList<Debit>();
				debits.add(debit);
				purchaseBL.getAlertBL().alertDebitNoteProcess(debits);
			}
		}
	}

	public DebitService getDebitService() {
		return debitService;
	}

	public void setDebitService(DebitService debitService) {
		this.debitService = debitService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}

	public BankReceiptsService getBankReceiptsService() {
		return bankReceiptsService;
	}

	public void setBankReceiptsService(BankReceiptsService bankReceiptsService) {
		this.bankReceiptsService = bankReceiptsService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
