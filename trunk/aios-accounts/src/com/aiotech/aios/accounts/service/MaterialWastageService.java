package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialWastage;
import com.aiotech.aios.accounts.domain.entity.MaterialWastageDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MaterialWastageService {

	private AIOTechGenericDAO<MaterialWastage> materialWastageDAO;
	private AIOTechGenericDAO<MaterialWastageDetail> materialWastageDetailDAO;

	public List<MaterialWastage> getAllMaterialWastages(
			Implementation implementation) throws Exception {
		return materialWastageDAO.findByNamedQuery("getAllMaterialWastages",
				implementation);
	}

	public MaterialWastage getMaterialWastagebyId(long materialWastageId)
			throws Exception {
		return materialWastageDAO.findByNamedQuery("getMaterialWastagebyId",
				materialWastageId).get(0);
	}

	public List<MaterialWastageDetail> getMaterialWastageDetails(
			long materialWastageId) throws Exception {
		return materialWastageDetailDAO.findByNamedQuery(
				"getMaterialWastageDetails", materialWastageId);
	}

	public List<MaterialWastageDetail> getMaterialWastageDetailsByStore(
			long storeId) throws Exception {
		return materialWastageDetailDAO.findByNamedQuery(
				"getMaterialWastageDetailsByStore", storeId);
	}

	public List<MaterialWastageDetail> getMaterialWastageDetailsWithoutStore(
			Implementation implementation) throws Exception {
		return materialWastageDetailDAO.findByNamedQuery(
				"getMaterialWastageDetailsWithoutStore", implementation);
	}

	public void saveMaterialWastage(MaterialWastage materialWastage,
			List<MaterialWastageDetail> materialWastageDetails)
			throws Exception {
		materialWastageDAO.saveOrUpdate(materialWastage);
		for (MaterialWastageDetail materialWastageDetail : materialWastageDetails)
			materialWastageDetail.setMaterialWastage(materialWastage);
		materialWastageDetailDAO.saveOrUpdateAll(materialWastageDetails);
	}

	public void updateMaterialWastageDetails(
			List<MaterialWastageDetail> materialWastageDetails)
			throws Exception {
		materialWastageDetailDAO.saveOrUpdateAll(materialWastageDetails);
	}

	public void deleteMaterialWastageDetail(
			List<MaterialWastageDetail> materialWastageDetails)
			throws Exception {
		materialWastageDetailDAO.deleteAll(materialWastageDetails);
	}

	public void deleteMaterialWastage(MaterialWastage materialWastage,
			List<MaterialWastageDetail> materialWastageDetails)
			throws Exception {
		materialWastageDetailDAO.deleteAll(materialWastageDetails);
		materialWastageDAO.delete(materialWastage);
	}

	// Getters & Setters
	public AIOTechGenericDAO<MaterialWastage> getMaterialWastageDAO() {
		return materialWastageDAO;
	}

	public void setMaterialWastageDAO(
			AIOTechGenericDAO<MaterialWastage> materialWastageDAO) {
		this.materialWastageDAO = materialWastageDAO;
	}

	public AIOTechGenericDAO<MaterialWastageDetail> getMaterialWastageDetailDAO() {
		return materialWastageDetailDAO;
	}

	public void setMaterialWastageDetailDAO(
			AIOTechGenericDAO<MaterialWastageDetail> materialWastageDetailDAO) {
		this.materialWastageDetailDAO = materialWastageDetailDAO;
	}
}
