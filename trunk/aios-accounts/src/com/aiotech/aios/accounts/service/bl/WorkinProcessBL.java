package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.WorkinProcess;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessDetail;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessProduction;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.WorkinProcessVO;
import com.aiotech.aios.accounts.service.WorkinProcessService;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionReqDetailStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionRequisitionStatus;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.to.Constants.Accounts.WorkinProcessStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;

public class WorkinProcessBL {

	private WorkinProcessService workinProcessService;
	private TransactionBL transactionBL;
	private StockBL stockBL;
	private ProductionRequisitionBL productionRequisitionBL;

	public List<Object> getAllWorkInProcess() throws Exception {
		List<WorkinProcess> workinProcess = workinProcessService
				.getAllWorkInProcess(getImplementation());
		List<Object> workinProcessVO = new ArrayList<Object>();
		if (null != workinProcess && workinProcess.size() > 0) {
			for (WorkinProcess workin : workinProcess) {
				workinProcessVO.add(addWorkinProcessVO(workin));
			}
		}
		return workinProcessVO;
	}

	public void saveWorkInProcess(WorkinProcess workinProcess,
			List<WorkinProcessProduction> workinProcessProduction,
			List<WorkinProcessProduction> existingWorkinProcessProductions,
			List<WorkinProcessProduction> deletedWorkinProductions,
			WorkinProcess existingWorkinProcess,
			ProductionRequisition productionRequisition,
			List<ProductionRequisitionDetail> requisitionDetails,
			Map<Long, ProductionRequisitionDetailVO> requisitionMap,
			Comment comment) throws Exception {
		boolean updateFlag = null != workinProcess.getWorkinProcessId() ? true
				: false;
		workinProcess.setImplementation(getImplementation());
		List<Stock> addWorkinProcessDetailStocks = null;
		List<Stock> deductWorkinProcessDetailStocks = null;
		if (updateFlag) {
			if (null != deletedWorkinProductions
					&& deletedWorkinProductions.size() > 0) {
				List<WorkinProcessDetail> deleteWorkinProcessDetails = new ArrayList<WorkinProcessDetail>();
				for (WorkinProcessProduction processProduction : deletedWorkinProductions) {
					for (WorkinProcessDetail deleteProcessDetail : processProduction
							.getWorkinProcessDetails()) {
						deleteWorkinProcessDetails.add(deleteProcessDetail);
					}
				}
				workinProcessService.deleteWorkinProcessProduction(
						deletedWorkinProductions, deleteWorkinProcessDetails);
			}
			transactionBL.deleteTransaction(workinProcess.getWorkinProcessId(),
					WorkinProcess.class.getSimpleName());
			addWorkinProcessDetailStocks = getStockDetailsRawMaterails(existingWorkinProcessProductions);
			stockBL.updateBatchStockDetails(addWorkinProcessDetailStocks);
		} else
			SystemBL.saveReferenceStamp(WorkinProcess.class.getName(),
					getImplementation());
		deductWorkinProcessDetailStocks = getStockDetailsRawMaterails(workinProcessProduction);
		stockBL.deleteBatchStockDetails(deductWorkinProcessDetailStocks);

		List<WorkinProcessDetail> workinProcessDetails = new ArrayList<WorkinProcessDetail>();
		for (WorkinProcessProduction processProduction : workinProcessProduction)
			workinProcessDetails.addAll(new ArrayList<WorkinProcessDetail>(
					processProduction.getWorkinProcessDetails()));
		if (null != productionRequisition) {
			boolean closed = true;
			for (Entry<Long, ProductionRequisitionDetailVO> entry : requisitionMap
					.entrySet()) {
				if (((byte) entry.getValue().getStatus() == (byte) ProductionReqDetailStatus.Open
						.getCode())
						|| ((byte) entry.getValue().getStatus() == (byte) ProductionReqDetailStatus.Processing
								.getCode())) {
					closed = false;
					break;
				}
			}
			if (closed)
				productionRequisition
						.setRequisitionStatus(ProductionRequisitionStatus.Closed
								.getCode());
			productionRequisitionBL.getProductionRequisitionService()
					.saveProductionRequisition(productionRequisition,
							requisitionDetails);
		}
		if (null != comment)
			transactionBL.getCommentBL().getCommentService()
					.saveComment(comment);
		workinProcessService.saveWorkInProcess(workinProcess,
				workinProcessProduction, workinProcessDetails);
		Transaction workinProcessDetailEntries = createWorkinProcessDetailEntries(
				workinProcess, workinProcessProduction);
		transactionBL.saveTransaction(
				workinProcessDetailEntries,
				new ArrayList<TransactionDetail>(workinProcessDetailEntries
						.getTransactionDetails()));
		if (updateFlag
				&& (byte) WorkinProcessStatus.Issue_Materials.getCode() != (byte) workinProcess
						.getStatus()) {
			List<Stock> addInventoryStock = getStockDetailsInventory(workinProcessProduction);
			if (null != addInventoryStock && addInventoryStock.size() > 0)
				stockBL.updateBatchStockDetails(addInventoryStock);
			List<Stock> deleteInventoryStock = null;
			if ((byte) WorkinProcessStatus.Finshed_Goods.getCode() == (byte) workinProcess
					.getStatus()) {
				deleteInventoryStock = getStockDetailsInventory(existingWorkinProcessProductions);
				if (null != deleteInventoryStock
						&& deleteInventoryStock.size() > 0)
					stockBL.deleteBatchStockDetails(deleteInventoryStock);
			}
			Transaction workinProcessEntries = createWorkinProcessEntries(
					workinProcess, workinProcessProduction);
			transactionBL.saveTransaction(
					workinProcessEntries,
					new ArrayList<TransactionDetail>(workinProcessEntries
							.getTransactionDetails()));
		}
	}

	public void deleteWorkInProcess(WorkinProcess workinProcess)
			throws Exception {
		transactionBL.deleteTransaction(workinProcess.getWorkinProcessId(),
				WorkinProcess.class.getSimpleName());
		List<WorkinProcessProduction> workinProcessProductions = new ArrayList<WorkinProcessProduction>(
				workinProcess.getWorkinProcessProductions());
		List<Stock> addWorkinProcessDetailStocks = getStockDetailsRawMaterails(workinProcessProductions);
		List<Stock> deductWorkinProcessDetailStocks = null;
		if ((byte) WorkinProcessStatus.Issue_Materials.getCode() != (byte) workinProcess
				.getStatus())
			deductWorkinProcessDetailStocks = getStockDetailsInventory(workinProcessProductions);
		List<WorkinProcessDetail> workinProcessDetails = new ArrayList<WorkinProcessDetail>();
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions)
			workinProcessDetails.addAll(workinProcessProduction
					.getWorkinProcessDetails());
		stockBL.updateStockDetails(addWorkinProcessDetailStocks,
				deductWorkinProcessDetailStocks);
		ProductionRequisition productionRequisition = null;
		if (null != workinProcess.getProductionRequisition()) {
			productionRequisition = workinProcess.getProductionRequisition();
			List<ProductionRequisitionDetail> productionRequisitionDetails = new ArrayList<ProductionRequisitionDetail>(
					productionRequisition.getProductionRequisitionDetails());
			Map<Long, ProductionRequisitionDetail> requisitionDetailMap = new HashMap<Long, ProductionRequisitionDetail>();
			for (ProductionRequisitionDetail productionRequisitionDetail : productionRequisitionDetails)
				requisitionDetailMap.put(productionRequisitionDetail
						.getProductionRequisitionDetailId(),
						productionRequisitionDetail);
			ProductionRequisitionDetail productionRequisitionDetail = null;
			List<ProductionRequisitionDetail> productionReqDetails = new ArrayList<ProductionRequisitionDetail>();
			for (WorkinProcessProduction productionDetail : workinProcessProductions) {
				List<WorkinProcessProduction> productionDetails = workinProcessService
						.getProductionByRequsitionDetailId(productionDetail
								.getProductionRequisitionDetail()
								.getProductionRequisitionDetailId(),
								productionDetail.getWorkinProcessProductionId());
				productionRequisitionDetail = new ProductionRequisitionDetail();
				if (requisitionDetailMap.containsKey(productionDetail
						.getProductionRequisitionDetail()
						.getProductionRequisitionDetailId())) {
					productionRequisitionDetail = requisitionDetailMap
							.get(productionDetail
									.getProductionRequisitionDetail()
									.getProductionRequisitionDetailId());
					if (null != productionDetails
							&& productionDetails.size() > 0)
						productionRequisitionDetail
								.setStatus(ProductionReqDetailStatus.Processing
										.getCode());
					else
						productionRequisitionDetail
								.setStatus(ProductionReqDetailStatus.Open
										.getCode());
					productionReqDetails.add(productionRequisitionDetail);
				}
			}
			boolean openFlag = true;
			if (null != productionReqDetails && productionReqDetails.size() > 0) {
				for (ProductionRequisitionDetail productionRequisitionDetail2 : productionReqDetails) {
					if (requisitionDetailMap
							.containsKey(productionRequisitionDetail2
									.getProductionRequisitionDetailId())) {
						if ((byte) productionRequisitionDetail2.getStatus() == (byte) ProductionReqDetailStatus.Processing
								.getCode()) {
							openFlag = false;
							break;
						}
					} else {
						openFlag = false;
						break;
					}
				}
			}
			if (openFlag)
				productionRequisition
						.setRequisitionStatus(ProductionRequisitionStatus.Open
								.getCode());
			else
				productionRequisition
						.setRequisitionStatus(ProductionRequisitionStatus.Processing
								.getCode());
			productionRequisitionBL.getProductionRequisitionService()
					.mergeProductionRequisition(productionRequisition,
							productionRequisitionDetails);
		}
		workinProcessService.deleteWorkinProcess(workinProcess,
				workinProcessProductions, workinProcessDetails);
	}

	private Transaction createWorkinProcessEntries(WorkinProcess workinProcess,
			List<WorkinProcessProduction> workinProcessProductions)
			throws Exception {
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), null, workinProcess
				.getProcessDate(), transactionBL.getCategory("WORKINPROCESS"),
				workinProcess.getWorkinProcessId(), WorkinProcess.class
						.getSimpleName());
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		TransactionDetail transactionDetail = null;
		Combination combination = null;
		ProductPricingDetail productPricingDetail = null;
		double costPrice = 0;
		double totalCostPrice = 0;
		double productPrice = 0;
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions) {
			costPrice = 0;
			productPrice = 0;
			for (WorkinProcessDetail workinProcessDetail : workinProcessProduction
					.getWorkinProcessDetails()) {
				productPricingDetail = stockBL
						.getProductPricingBL()
						.getProductPricingDetail(
								workinProcessDetail.getProduct().getProductId());
				if (null != productPricingDetail)
					costPrice = productPricingDetail.getBasicCostPrice()
							* workinProcessDetail.getIssueQuantity();
				else {
					double unitRate = stockBL.getStockPrice(workinProcessDetail
							.getProduct().getProductId());
					costPrice = (workinProcessDetail.getIssueQuantity() * unitRate);
				}
				totalCostPrice += costPrice;
				productPrice += costPrice;
			}
			combination = stockBL
					.getProductPricingBL()
					.getProductBL()
					.getProductInventory(
							workinProcessProduction.getProduct().getProductId());
			transactionDetail = transactionBL.createTransactionDetail(
					productPrice, combination.getCombinationId(),
					TransactionType.Debit.getCode(), null, null,
					workinProcess.getReferenceNumber());
			transactionDetails.add(transactionDetail);
		}
		transactionDetail = transactionBL.createTransactionDetail(
				totalCostPrice, getImplementation().getExpenseAccount(),
				TransactionType.Credit.getCode(), null, null,
				workinProcess.getReferenceNumber());
		transactionDetails.add(transactionDetail);
		transaction.setTransactionDetails(transactionDetails);
		return transaction;

	}

	private Transaction createWorkinProcessDetailEntries(
			WorkinProcess workinProcess,
			List<WorkinProcessProduction> workinProcessProductions)
			throws Exception {

		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), null, workinProcess
				.getProcessDate(), transactionBL.getCategory("WORKINPROCESS"),
				workinProcess.getWorkinProcessId(), WorkinProcess.class
						.getSimpleName());
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		TransactionDetail transactionDetail = null;
		Combination combination = null;
		ProductPricingDetail productPricingDetail = null;
		double costPrice = 0;
		double totalCostPrice = 0;
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions) {
			totalCostPrice = 0;
			costPrice = 0;
			for (WorkinProcessDetail workinProcessDetail : workinProcessProduction
					.getWorkinProcessDetails()) {
				productPricingDetail = stockBL
						.getProductPricingBL()
						.getProductPricingDetail(
								workinProcessDetail.getProduct().getProductId());
				if (null != productPricingDetail)
					costPrice = productPricingDetail.getBasicCostPrice()
							* workinProcessDetail.getIssueQuantity();
				else {
					double unitRate = stockBL.getStockPrice(workinProcessDetail
							.getProduct().getProductId());
					costPrice = (workinProcessDetail.getIssueQuantity() * unitRate);
				}
				combination = stockBL
						.getProductPricingBL()
						.getProductBL()
						.getProductInventory(
								workinProcessDetail.getProduct().getProductId());
				transactionDetail = transactionBL.createTransactionDetail(
						costPrice, combination.getCombinationId(),
						TransactionType.Credit.getCode(), null, null,
						workinProcess.getReferenceNumber());
				transactionDetails.add(transactionDetail);
				totalCostPrice += costPrice;
			}

		}
		transactionDetail = transactionBL.createTransactionDetail(
				totalCostPrice, getImplementation().getExpenseAccount(),
				TransactionType.Debit.getCode(), null, null,
				workinProcess.getReferenceNumber());
		transactionDetails.add(transactionDetail);
		transaction.setTransactionDetails(transactionDetails);
		return transaction;
	}

	private List<Stock> getStockDetailsRawMaterails(
			List<WorkinProcessProduction> workinProcessProductions)
			throws Exception {
		Stock stock = null;
		List<Stock> stocks = new ArrayList<Stock>();
		Shelf shelf = null;
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions) {
			for (WorkinProcessDetail workinProcessDetail : workinProcessProduction
					.getWorkinProcessDetails()) {

				stock = new Stock();
				shelf = stockBL
						.getStoreBL()
						.getStoreService()
						.getStoreByShelfId(
								workinProcessDetail.getShelf().getShelfId());
				stock.setProduct(workinProcessDetail.getProduct());
				stock.setStore(shelf.getShelf().getAisle().getStore());
				if (null != workinProcessDetail.getProductPackageDetail())
					stock.setQuantity(productionRequisitionBL
							.getMaterialIdleMixBL()
							.getPackageConversionBL()
							.getPackageBaseQuantity(
									workinProcessDetail
											.getProductPackageDetail()
											.getProductPackageDetailId(),
									(workinProcessDetail.getIssueQuantity() + (null != workinProcessDetail
											.getWastageQuantity() ? workinProcessDetail
											.getWastageQuantity() : 0))));
				else
					stock.setQuantity((workinProcessDetail.getIssueQuantity() + (null != workinProcessDetail
							.getWastageQuantity() ? workinProcessDetail
							.getWastageQuantity() : 0)));
				stock.setImplementation(getImplementation());
				stock.setShelf(shelf);
				stocks.add(stock);
			}
		}
		return stocks;
	}

	private List<Stock> getStockDetailsInventory(
			List<WorkinProcessProduction> workinProcessProductions)
			throws Exception {
		Stock stock = null;
		List<Stock> stocks = new ArrayList<Stock>();
		Shelf shelf = null;
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions) {
			if (null != workinProcessProduction.getProductionQuantity()
					&& workinProcessProduction.getProductionQuantity() > 0) {
				stock = new Stock();
				shelf = stockBL
						.getStoreBL()
						.getStoreService()
						.getStoreByShelfId(
								workinProcessProduction.getShelf().getShelfId());
				stock.setProduct(workinProcessProduction.getProduct());
				stock.setStore(shelf.getShelf().getAisle().getStore());
				stock.setQuantity(workinProcessProduction
						.getProductionQuantity());
				stock.setImplementation(getImplementation());
				stock.setShelf(shelf);
				stocks.add(stock);
			}
		}
		return stocks;
	}

	private WorkinProcessVO addWorkinProcessVO(WorkinProcess workinProcess)
			throws Exception {
		WorkinProcessVO workinProcessVO = new WorkinProcessVO();
		workinProcessVO.setWorkinProcessId(workinProcess.getWorkinProcessId());
		workinProcessVO.setDate(DateFormat.convertDateToString(workinProcess
				.getProcessDate().toString()));
		workinProcessVO.setStatusStr(WorkinProcessStatus
				.get(workinProcess.getStatus()).name().replaceAll("_", " "));
		workinProcessVO.setReferenceNumber(workinProcess.getReferenceNumber());
		workinProcessVO.setPersonName(workinProcess.getPerson().getFirstName()
				+ " " + workinProcess.getPerson().getLastName());
		if (null != workinProcess.getProductionRequisition()
				&& null != workinProcess.getProductionRequisition()
						.getReferenceNumber())
			workinProcessVO.setRequisitionReference(workinProcess
					.getProductionRequisition().getReferenceNumber());
		return workinProcessVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public WorkinProcessService getWorkinProcessService() {
		return workinProcessService;
	}

	public void setWorkinProcessService(
			WorkinProcessService workinProcessService) {
		this.workinProcessService = workinProcessService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public ProductionRequisitionBL getProductionRequisitionBL() {
		return productionRequisitionBL;
	}

	public void setProductionRequisitionBL(
			ProductionRequisitionBL productionRequisitionBL) {
		this.productionRequisitionBL = productionRequisitionBL;
	}
}
