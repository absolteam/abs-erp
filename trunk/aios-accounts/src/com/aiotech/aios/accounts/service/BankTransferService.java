package com.aiotech.aios.accounts.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class BankTransferService {

	private AIOTechGenericDAO<BankTransfer> bankTransferDAO;

	public List<BankTransfer> getAllBankTransfer(Implementation implementation)
			throws Exception {
		return this.getBankTransferDAO().findByNamedQuery("getAllBankTransfer",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public void saveBankTransfer(BankTransfer bankTransfer) throws Exception {
		this.getBankTransferDAO().saveOrUpdate(bankTransfer);
	}

	public BankTransfer findBankTransferById(long bankTransferId)
			throws Exception {
		return this.getBankTransferDAO()
				.findByNamedQuery("findBankTransferById", bankTransferId)
				.get(0);
	}

	public BankTransfer getBankTransferById(long bankTransferId)
			throws Exception {
		return bankTransferDAO.findById(bankTransferId);
	}

	public void deleteBankTransfer(BankTransfer bankTransfer) throws Exception {
		this.getBankTransferDAO().delete(bankTransfer);
	}

	public BankTransfer getBankTransferByReference(String referenceNumber,
			Implementation implementation) {
		List<BankTransfer> bankTransfers = bankTransferDAO.findByNamedQuery(
				"getBankTransferByReference", implementation, referenceNumber);
		return null != bankTransfers && bankTransfers.size() > 0 ? bankTransfers
				.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<BankTransfer> getFilteredBankTransfer(
			Implementation implementation) throws Exception {
		Criteria criteria = bankTransferDAO.createCriteria();

		criteria.createAlias("currency", "cr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cr.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankAccountByCreditBankAccount", "cred",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankAccountByDebitBankAccount", "dbt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cred.bank", "crd_bank",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("dbt.bank", "dbt_bank",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		return criteria.list();
	}

	public AIOTechGenericDAO<BankTransfer> getBankTransferDAO() {
		return bankTransferDAO;
	}

	public void setBankTransferDAO(
			AIOTechGenericDAO<BankTransfer> bankTransferDAO) {
		this.bankTransferDAO = bankTransferDAO;
	}
}
