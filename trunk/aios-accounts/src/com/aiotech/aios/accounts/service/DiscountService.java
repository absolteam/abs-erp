package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Discount;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.DiscountOption;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DiscountService {

	private AIOTechGenericDAO<Discount> discountDAO;
	private AIOTechGenericDAO<DiscountOption> discountOptionDAO;
	private AIOTechGenericDAO<DiscountMethod> discountMethodDAO;

	// Get all Discounts
	public List<Discount> getAllProductDiscounts(Implementation implementation)
			throws Exception {
		return discountDAO.findByNamedQuery("getAllProductDiscounts",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Discount by Discount Id
	public Discount getProductDiscountByDiscountId(long discountId)
			throws Exception {
		return discountDAO.findByNamedQuery("getProductDiscountByDiscountId",
				discountId).get(0);
	}

	// Get Discount Methods by Discount Id
	public List<DiscountMethod> getDiscountMethodByDiscountId(long discountId)
			throws Exception {
		return discountMethodDAO.findByNamedQuery(
				"getDiscountMethodByDiscountId", discountId);
	}

	public Discount getProductDiscountByProductId(long productId, Date currentDate) {
		List<Discount> discounts = discountDAO.findByNamedQuery(
				"getProductDiscountByProductId", productId, currentDate, currentDate);
		return null != discounts && discounts.size() > 0 ? discounts.get(0)
				: null;
	}

	public Discount getProductDiscountByCustomerId(long customerId,
			Date currentDate) {
		List<Discount> discounts = discountDAO.findByNamedQuery(
				"getProductDiscountByCustomerId", customerId, currentDate,
				currentDate);
		return null != discounts && discounts.size() > 0 ? discounts.get(0)
				: null;
	}

	// Get Discount Options by Discount Id
	public List<DiscountOption> getDiscountOptionByDiscountId(long discountId)
			throws Exception {
		return discountOptionDAO.findByNamedQuery(
				"getDiscountOptionByDiscountId", discountId);
	}

	// Delete Discount Details
	public void deleteDiscountMethods(
			List<DiscountMethod> deletedDiscountDetails) throws Exception {
		discountMethodDAO.deleteAll(deletedDiscountDetails);
	}

	// Delete Discount Options
	public void deleteDiscountOptions(
			List<DiscountOption> deletedDiscountOptions) throws Exception {
		discountOptionDAO.deleteAll(deletedDiscountOptions);
	}

	// Save Discount
	public void saveDiscount(Discount discount,
			List<DiscountMethod> discountDetails,
			List<DiscountOption> discountOptions) throws Exception {
		discountDAO.saveOrUpdate(discount);
		for (DiscountMethod discountMethod : discountDetails)
			discountMethod.setDiscount(discount);
		discountMethodDAO.saveOrUpdateAll(discountDetails);
		for (DiscountOption discountOption : discountOptions)
			discountOption.setDiscount(discount);
		discountOptionDAO.saveOrUpdateAll(discountOptions);
	}

	// Delete Discount
	public void deleteProductDiscount(Discount productDiscount,
			List<DiscountMethod> discountMethods,
			List<DiscountOption> discountOptions) throws Exception {
		discountOptionDAO.deleteAll(discountOptions);
		discountMethodDAO.deleteAll(discountMethods);
		discountDAO.delete(productDiscount);
	}

	
	@SuppressWarnings("unchecked")
	public List<Discount> getFilteredProductDiscounts(Implementation implementation, Long discountId, 
			Byte discountOption, Byte discountType, Long productId, Long customerId, Long productCategoryId, Date fromDate, Date toDate)
			throws Exception {
		Criteria criteria = discountDAO.createCriteria();
		
		criteria.createAlias("lookupDetailByDiscountTo", "dto", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByDiscountOnPrice", "onp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("discountMethods", "mthd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("discountOptions", "dopt", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("dopt.product", "prd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("dopt.customer", "cst", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "prs", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.cmpDeptLocation", "cdl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("dopt.productCategory", "cat", CriteriaSpecification.LEFT_JOIN);
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		if (discountId != null && discountId > 0) {
			criteria.add(Restrictions.eq("discountId", discountId));
		}
		
		if (discountOption != null && discountOption > 0) {
			criteria.add(Restrictions.eq("discountOption", discountOption));
		}
		
		if (discountType != null && discountType > 0) {
			criteria.add(Restrictions.eq("discountType", discountType));
		}
		
		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}
		
		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("cst.customerId", customerId));
		}
		
		if (productCategoryId != null && productCategoryId > 0) {
			criteria.add(Restrictions.eq("cat.productCategoryId", productCategoryId));
		}
		
		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.ge("fromDate",fromDate));
			criteria.add(Restrictions.le("toDate",toDate));
		}
		
		Set<Discount> uniqueDiscount = new HashSet<Discount>(criteria.list());
		List<Discount> discountList = new ArrayList<Discount>(uniqueDiscount);
		
		return discountList;
	}

	// Getters & Setters
	public AIOTechGenericDAO<Discount> getDiscountDAO() {
		return discountDAO;
	}

	public void setDiscountDAO(AIOTechGenericDAO<Discount> discountDAO) {
		this.discountDAO = discountDAO;
	}

	public AIOTechGenericDAO<DiscountOption> getDiscountOptionDAO() {
		return discountOptionDAO;
	}

	public void setDiscountOptionDAO(
			AIOTechGenericDAO<DiscountOption> discountOptionDAO) {
		this.discountOptionDAO = discountOptionDAO;
	}

	public AIOTechGenericDAO<DiscountMethod> getDiscountMethodDAO() {
		return discountMethodDAO;
	}

	public void setDiscountMethodDAO(
			AIOTechGenericDAO<DiscountMethod> discountMethodDAO) {
		this.discountMethodDAO = discountMethodDAO;
	}
}
