package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetService;
import com.aiotech.aios.accounts.domain.entity.AssetServiceDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AssetServiceVO;
import com.aiotech.aios.accounts.service.AssetServiceService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetServiceBL {

	private AssetServiceService assetServiceService;
	private LookupMasterBL lookupMasterBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Asset Check Outs
	public List<Object> showAssetServiceJsonList() throws Exception {
		List<AssetService> assetServices = assetServiceService
				.getAllAssetServices(getImplementation());
		List<Object> assetSerivceVOs = new ArrayList<Object>();
		AssetServiceVO assetServiceVO = null;
		for (AssetService assetService : assetServices) {
			assetServiceVO = new AssetServiceVO();
			assetServiceVO.setAssetServiceId(assetService.getAssetServiceId());
			assetServiceVO.setServiceNumber(assetService.getServiceNumber());
			assetServiceVO.setAssetName(assetService.getAssetCreation()
					.getAssetName());
			assetServiceVO.setAssetServiceType(assetService
					.getLookupDetailByServiceType().getDisplayName());
			assetServiceVO
					.setAssetServiceLevel(Constants.Accounts.AssetServiceLevel
							.get(assetService.getServiceLevel()).name());
			assetServiceVO.setServiceInCharge(assetService.getPerson()
					.getFirstName().concat(" ")
					.concat(assetService.getPerson().getLastName()));
			assetSerivceVOs.add(assetServiceVO);
		}
		return assetSerivceVOs;
	}

	// Save Asset Service
	public void saveAssetService(AssetService assetService,
			List<AssetServiceDetail> assetServiceDetails,
			List<AssetServiceDetail> deleteAssetServiceDetails)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetService.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		if (null == assetService.getAssetServiceId())
			SystemBL.saveReferenceStamp(AssetService.class.getName(),
					getImplementation());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetService != null && assetService.getAssetServiceId() != null
				&& assetService.getAssetServiceId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetServiceService.saveAssetService(assetService, assetServiceDetails,
				deleteAssetServiceDetails);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetService.class.getSimpleName(),
				assetService.getAssetServiceId(), user, workflowDetailVo);
	}

	public void doAssetServiceBusinessUpdate(Long assetServiceId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				AssetService assetService = this.getAssetServiceService()
						.getAssetServiceDetails(assetServiceId);
				assetServiceService.deleteAssetService(
						assetService,
						new ArrayList<AssetServiceDetail>(assetService
								.getAssetServiceDetails()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Asset Service
	public void deleteAssetService(AssetService assetService) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetService.class.getSimpleName(),
				assetService.getAssetServiceId(), user, workflowDetailVo);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetServiceService getAssetServiceService() {
		return assetServiceService;
	}

	public void setAssetServiceService(AssetServiceService assetServiceService) {
		this.assetServiceService = assetServiceService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
