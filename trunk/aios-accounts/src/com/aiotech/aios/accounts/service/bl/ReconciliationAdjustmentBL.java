/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustment;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustmentDetail;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.ReconciliationAdjustmentService;
import com.aiotech.aios.common.to.Constants.Accounts.ReconciliationAdjustmentType;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * @author Saleem
 */
public class ReconciliationAdjustmentBL {

	// Dependencies
	private TransactionBL transactionBL;
	private BankBL bankBL;
	private ReconciliationAdjustmentService reconciliationAdjustmentService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get Reconciliation Adjustments
	public JSONObject getAllReconciliationAdjustments() throws Exception {
		List<ReconciliationAdjustment> reconciliationAdjustments = reconciliationAdjustmentService
				.getAllReconciliationAdjustments(getImplementation());
		JSONArray jsonData = new JSONArray();
		JSONArray jsonArray = null;
		JSONObject jsonObject = new JSONObject();
		for (ReconciliationAdjustment reconciliationAdjustment : reconciliationAdjustments) {
			jsonArray = new JSONArray();
			jsonArray.add(reconciliationAdjustment
					.getReconciliationAdjustmentId());
			jsonArray.add(reconciliationAdjustment.getReferenceNumber());
			jsonArray.add(DateFormat
					.convertDateToString(reconciliationAdjustment.getDate()
							.toString()));
			jsonArray.add(reconciliationAdjustment.getBankAccount()
					.getAccountNumber());
			jsonArray.add(reconciliationAdjustment.getDescription());
			jsonData.add(jsonArray);
		}
		jsonObject.put("aaData", jsonData);
		return jsonObject;
	}

	// Save Adjustment Reconciliation
	public void saveReconciliationAdjustment(
			ReconciliationAdjustment reconciliationAdjustment,
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails,
			List<ReconciliationAdjustmentDetail> deletedAdjustmentDetails,
			List<ReconciliationAdjustmentDetail> reverseAdjustmentDetails)
			throws Exception {
		reconciliationAdjustment.setImplementation(getImplementation());
		reconciliationAdjustment
				.setReconciliationAdjustmentDetails(new HashSet<ReconciliationAdjustmentDetail>(
						reconciliationAdjustmentDetails));
		reconciliationAdjustment
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (reconciliationAdjustment != null
				&& reconciliationAdjustment.getReconciliationAdjustmentId() != null
				&& reconciliationAdjustment.getReconciliationAdjustmentId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		boolean updateFlag = false;
		if (null != reconciliationAdjustment.getReconciliationAdjustmentId())
			updateFlag = true;
		 
		if (null != deletedAdjustmentDetails
				&& deletedAdjustmentDetails.size() > 0) {
			reconciliationAdjustmentService
					.deleteReconciliationAdjustmentDetail(deletedAdjustmentDetails);
		}
		reconciliationAdjustmentService.saveReconciliationAdjustment(
				reconciliationAdjustment, reconciliationAdjustmentDetails);

		if (updateFlag)
			transactionBL.deleteTransaction(
					reconciliationAdjustment.getReconciliationAdjustmentId(),
					ReconciliationAdjustment.class.getSimpleName());
		else
			SystemBL.saveReferenceStamp(
					ReconciliationAdjustment.class.getName(),
					getImplementation());

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ReconciliationAdjustment.class.getSimpleName(),
				reconciliationAdjustment.getReconciliationAdjustmentId(), user,
				workflowDetailVo);
	}

	public void doReconciliationAdjustmentBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		ReconciliationAdjustment reconciliationAdjustment = reconciliationAdjustmentService
				.getReconciliationAdjustmentById(recordId);
		List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails = new ArrayList<ReconciliationAdjustmentDetail>(
				reconciliationAdjustment.getReconciliationAdjustmentDetails());

		if (workflowDetailVO.isDeleteFlag()) {
			// Reverse Transaction
			transactionBL.deleteTransaction(
					reconciliationAdjustment.getReconciliationAdjustmentId(),
					ReconciliationAdjustment.class.getSimpleName());
			reconciliationAdjustmentService.deleteReconciliationAdjustment(
					reconciliationAdjustment, reconciliationAdjustmentDetails);
		} else {
			List<ReconciliationAdjustmentDetail> reconciliationPayments = new ArrayList<ReconciliationAdjustmentDetail>();
			List<ReconciliationAdjustmentDetail> reconciliationRecipts = new ArrayList<ReconciliationAdjustmentDetail>();

			double transactionAmount = 0;
			for (ReconciliationAdjustmentDetail adjustmentDetail : reconciliationAdjustmentDetails) {
				if ((byte) ReconciliationAdjustmentType.Payment.getCode() == (byte) adjustmentDetail
						.getAdjustmentType())
					reconciliationPayments.add(adjustmentDetail);
				else
					reconciliationRecipts.add(adjustmentDetail);
				transactionAmount += adjustmentDetail.getAmount();
			}
			List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
			BankAccount bankAccount = bankBL.getBankService()
					.getBankAccountInfo(
							reconciliationAdjustment.getBankAccount()
									.getBankAccountId());
			if (null != reconciliationPayments
					&& reconciliationPayments.size() > 0) {
				transactionDetails.addAll(reconciliationAdjustmentTransaction(
						reconciliationPayments, bankAccount,
						reconciliationAdjustment.getReferenceNumber(),
						TransactionType.Debit.getCode(),
						TransactionType.Credit.getCode()));
			}
			if (null != reconciliationRecipts
					&& reconciliationRecipts.size() > 0) {
				transactionDetails.addAll(reconciliationAdjustmentTransaction(
						reconciliationRecipts, bankAccount,
						reconciliationAdjustment.getReferenceNumber(),
						TransactionType.Credit.getCode(),
						TransactionType.Debit.getCode()));
			}
			if (null != transactionDetails && transactionDetails.size() > 0) {
				Transaction transaction = transactionBL
						.createTransaction(
								transactionBL.getCalendarBL()
										.transactionPostingPeriod(
												reconciliationAdjustment
														.getDate()),
								transactionBL.getCurrency().getCurrencyId(),
								"Transaction created from Reconciliation Adjustment Ref: "
										+ reconciliationAdjustment
												.getReferenceNumber(),
								reconciliationAdjustment.getDate(),
								transactionBL
										.getCategory("Reconciliation Adjustments"),
								reconciliationAdjustment
										.getReconciliationAdjustmentId(),
								ReconciliationAdjustment.class.getSimpleName());
				transactionBL.saveTransaction(transaction, transactionDetails);
			}
		}
	}

	// Delete Reconciliation
	public void deleteReconciliationAdjustment(
			ReconciliationAdjustment reconciliationAdjustment,
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ReconciliationAdjustment.class.getSimpleName(),
				reconciliationAdjustment.getReconciliationAdjustmentId(), user,
				workflowDetailVo);
	}

	// Reverse Transaction
	@SuppressWarnings("unused")
	private void reverseTransaction(
			ReconciliationAdjustment reconciliationAdjustment,
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails)
			throws Exception {
		BankAccount bankAccount = bankBL.getBankService().getBankAccountInfo(
				reconciliationAdjustment.getBankAccount().getBankAccountId());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();

		List<ReconciliationAdjustmentDetail> reconciliationPayments = new ArrayList<ReconciliationAdjustmentDetail>();
		List<ReconciliationAdjustmentDetail> reconciliationRecipts = new ArrayList<ReconciliationAdjustmentDetail>();

		double transactionAmount = 0;
		for (ReconciliationAdjustmentDetail adjustmentDetail : reconciliationAdjustmentDetails) {
			if ((byte) ReconciliationAdjustmentType.Payment.getCode() == (byte) adjustmentDetail
					.getAdjustmentType())
				reconciliationPayments.add(adjustmentDetail);
			else
				reconciliationRecipts.add(adjustmentDetail);
			transactionAmount += adjustmentDetail.getAmount();
		}

		if (null != reconciliationPayments && reconciliationPayments.size() > 0) {
			transactionDetails
					.addAll(reconciliationAdjustmentTransaction(
							reconciliationPayments, bankAccount,
							reconciliationAdjustment.getReferenceNumber(),
							false, true));// true = bank, false = payment

		}
		if (null != reconciliationRecipts && reconciliationRecipts.size() > 0) {
			transactionDetails
					.addAll(reconciliationAdjustmentTransaction(
							reconciliationRecipts, bankAccount,
							reconciliationAdjustment.getReferenceNumber(),
							true, false));// false = bank, true = payment
		}

		if (null != transactionDetails && transactionDetails.size() > 0) {
			Transaction transaction = transactionBL.createTransaction(
					transactionBL.getCurrency().getCurrencyId(),
					"Reverse Transaction created from Reconciliation Adjustment Ref: "
							+ reconciliationAdjustment.getReferenceNumber(),
					reconciliationAdjustment.getDate(),
					transactionBL.getCategory("Reconciliation Adjustments"),
					reconciliationAdjustment.getReconciliationAdjustmentId(),
					ReconciliationAdjustment.class.getSimpleName());

			transactionBL.saveTransaction(transaction, transactionDetails);
		}
	}

	// Create Reconciliation Adjustment Transaction
	private List<TransactionDetail> reconciliationAdjustmentTransaction(
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails,
			BankAccount bankAccount, String referenceNumber, boolean debitFlag,
			boolean creditFlag) throws Exception {

		double transactionAmount = 0;
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		for (ReconciliationAdjustmentDetail adjustmentDetail : reconciliationAdjustmentDetails) {
			transactionAmount += adjustmentDetail.getAmount();
			transactionDetails.add(createDebitEntry(referenceNumber,
					adjustmentDetail, debitFlag));
		}
		transactionDetails.add(transactionBL.createTransactionDetail(
				transactionAmount, bankAccount
						.getCombination().getCombinationId(),
				creditFlag, "Reference Transaction " + referenceNumber, null,
				referenceNumber));
		return transactionDetails;
	}

	// Create Transaction Entry
	private TransactionDetail createDebitEntry(String referenceNumber,
			ReconciliationAdjustmentDetail adjustmentDetail, boolean debitFlag)
			throws Exception {
		return transactionBL.createTransactionDetail(adjustmentDetail
				.getAmount(), adjustmentDetail.getCombination()
				.getCombinationId(), debitFlag, null, null, referenceNumber);
	}

	// Get implementation from session
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters and Setters
	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public ReconciliationAdjustmentService getReconciliationAdjustmentService() {
		return reconciliationAdjustmentService;
	}

	public void setReconciliationAdjustmentService(
			ReconciliationAdjustmentService reconciliationAdjustmentService) {
		this.reconciliationAdjustmentService = reconciliationAdjustmentService;
	}

	public BankBL getBankBL() {
		return bankBL;
	}

	public void setBankBL(BankBL bankBL) {
		this.bankBL = bankBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
