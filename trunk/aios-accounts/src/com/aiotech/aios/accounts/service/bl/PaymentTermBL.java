package com.aiotech.aios.accounts.service.bl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.aiotech.aios.accounts.domain.entity.PaymentTerm;
import com.aiotech.aios.accounts.service.PaymentTermService;
import com.aiotech.aios.system.domain.entity.Implementation;

public class PaymentTermBL {
	private static final Logger LOGGER =LogManager.getLogger(PaymentTermBL.class); 
	private PaymentTermService paymentTermService;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	public JSONObject getAllPayment(Implementation implementation) throws Exception{
		List<PaymentTerm> termList=this.getPaymentTermService().getAllPaymentTerms(implementation);
		if(null!=termList){
			iTotalRecords=termList.size();
			iTotalDisplayRecords=termList.size();
			LOGGER.info("Payment term list size "+termList.size());
		}
		JSONObject jsonResponse= new JSONObject();
		jsonResponse.put("iTotalRecords", iTotalRecords);
		jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);  
		JSONArray data= new JSONArray();
		JSONArray array= null;
		for(PaymentTerm list: termList){
			array= new JSONArray();
			array.add(list.getPaymentTermId());
			array.add(list.getName());
			array.add(list.getNoOfDays());
			array.add(list.getDiscountDays());
			array.add(list.getDiscount());
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}
	
	public PaymentTermService getPaymentTermService() {
		return paymentTermService;
	}

	public void setPaymentTermService(PaymentTermService paymentTermService) {
		this.paymentTermService = paymentTermService;
	}
}
