/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.accounts.service.SalesInvoiceService;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.SalesInvoiceType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Saleem
 */
public class SalesInvoiceBL {

	// Dependencies
	private SalesInvoiceService salesInvoiceService;
	private SalesDeliveryNoteBL salesDeliveryNoteBL;
	private CreditTermBL creditTermBL;
	private CustomerBL customerBL;
	private CreditBL creditBL;
	private AlertBL alertBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get all Sales Invoice
	public List<Object> getAllSalesInvoice() throws Exception {
		List<SalesInvoice> salesInvoices = salesInvoiceService
				.getAllSalesInvoice(getImplementation());
		List<Object> salesInvoiceVOs = new ArrayList<Object>();
		if (null != salesInvoices && salesInvoices.size() > 0) {
			SalesInvoiceVO salesInvoiceVO = null;
			for (SalesInvoice salesInvoice : salesInvoices) {
				salesInvoiceVO = new SalesInvoiceVO();
				salesInvoiceVO.setSalesInvoiceDate(DateFormat
						.convertDateToString(salesInvoice.getInvoiceDate()
								.toString()));
				salesInvoiceVO.setReferenceNumber(salesInvoice
						.getReferenceNumber());
				salesInvoiceVO.setSalesInvoiceId(salesInvoice
						.getSalesInvoiceId());
				salesInvoiceVO
						.setCustomerName(null != salesInvoice.getCustomer()
								.getPersonByPersonId() ? salesInvoice
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat("")
								.concat(salesInvoice.getCustomer()
										.getPersonByPersonId().getLastName())
								: (null != salesInvoice.getCustomer()
										.getCompany() ? salesInvoice
										.getCustomer().getCompany()
										.getCompanyName() : salesInvoice
										.getCustomer().getCmpDeptLocation()
										.getCompany().getCompanyName()));
				salesInvoiceVO.setCreditTermName(null != salesInvoice
						.getCreditTerm() ? salesInvoice.getCreditTerm()
						.getName() : "");
				salesInvoiceVO.setInvoiceTypeName(SalesInvoiceType.get(
						salesInvoice.getInvoiceType()).name());

				salesInvoiceVO.setStatusName(O2CProcessStatus.get(
						salesInvoice.getStatus()).name());
				if (salesInvoice.getStatus() != null
						&& (byte) O2CProcessStatus.Paid.getCode() == (byte) salesInvoice
								.getStatus())
					salesInvoiceVO.setDeliveryFlag(true);
				else
					salesInvoiceVO.setDeliveryFlag(false);

				if (salesInvoice.getSalesInvoices() != null
						&& salesInvoice.getSalesInvoices().size() > 0)
					salesInvoiceVO.setEditFlag(true);
				else
					salesInvoiceVO.setEditFlag(false);

				salesInvoiceVOs.add(salesInvoiceVO);
			}
		}
		return salesInvoiceVOs;
	}

	// Get all Sales Invoice
	public List<Object> getAllContiniousSalesInvoice() throws Exception {
		List<SalesInvoice> salesInvoices = salesInvoiceService
				.getAllContiniosSalesInvoice(getImplementation());
		Map<Long, SalesInvoiceVO> salesInvoiceMap = new HashMap<Long, SalesInvoiceVO>();
		if (null != salesInvoices && salesInvoices.size() > 0) {
			for (SalesInvoice salesInvoice : salesInvoices) {
				SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO();
				if (null != salesInvoice.getSalesInvoice()) {
					if (salesInvoiceMap.containsKey(salesInvoice
							.getSalesInvoice().getSalesInvoiceId())) {
						salesInvoiceVO = salesInvoiceMap.get(salesInvoice
								.getSalesInvoice().getSalesInvoiceId());
						salesInvoiceVO
								.setInvoiceAmount(salesInvoice
										.getInvoiceAmount()
										+ (null != salesInvoiceVO
												.getInvoiceAmount() ? salesInvoiceVO
												.getInvoiceAmount() : 0));
						salesInvoiceVO
								.setInvoiceAmountStr(AIOSCommons
										.formatAmount(salesInvoiceVO
												.getInvoiceAmount()));
					}
				} else {
					salesInvoiceVO
							.setInvoiceAmount(salesInvoice.getInvoiceAmount()
									+ (null != salesInvoiceVO
											.getInvoiceAmount() ? salesInvoiceVO
											.getInvoiceAmount() : 0));
					salesInvoiceVO.setTotalInvoice(salesInvoice
							.getTotalInvoice());
					salesInvoiceVO.setInvoiceAmountStr(AIOSCommons
							.formatAmount(salesInvoiceVO.getInvoiceAmount()));
					salesInvoiceVO.setTotalInvoiceStr(AIOSCommons
							.formatAmount(salesInvoiceVO.getTotalInvoice()));
					salesInvoiceVO.setSalesInvoiceDate(DateFormat
							.convertDateToString(salesInvoice.getInvoiceDate()
									.toString()));
					salesInvoiceVO.setReferenceNumber(salesInvoice
							.getReferenceNumber());
					salesInvoiceVO.setSalesInvoiceId(salesInvoice
							.getSalesInvoiceId());
					salesInvoiceVO
							.setCustomerName(null != salesInvoice.getCustomer()
									.getPersonByPersonId() ? salesInvoice
									.getCustomer()
									.getPersonByPersonId()
									.getFirstName()
									.concat("")
									.concat(salesInvoice.getCustomer()
											.getPersonByPersonId()
											.getLastName())
									: (null != salesInvoice.getCustomer()
											.getCompany() ? salesInvoice
											.getCustomer().getCompany()
											.getCompanyName() : salesInvoice
											.getCustomer().getCmpDeptLocation()
											.getCompany().getCompanyName()));
					salesInvoiceVO.setCreditTermName(null != salesInvoice
							.getCreditTerm() ? salesInvoice.getCreditTerm()
							.getName() : "");
					salesInvoiceVO.setPendingInvoice(salesInvoiceVO
							.getTotalInvoice()
							- salesInvoiceVO.getInvoiceAmount());
					salesInvoiceVO.setPendingInvoiceStr(AIOSCommons
							.formatAmount(salesInvoiceVO.getPendingInvoice()));
					salesInvoiceVO.setProcessStatus(salesInvoice
							.getProcessStatus());
					salesInvoiceMap.put(salesInvoice.getSalesInvoiceId(),
							salesInvoiceVO);
				}
			}
		}
		List<SalesInvoiceVO> salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();
		if (null != salesInvoiceMap && salesInvoiceMap.size() > 0)
			salesInvoiceVOs.addAll(new ArrayList<SalesInvoiceVO>(
					salesInvoiceMap.values()));
		List<Object> salesInvoiceVOtemps = new ArrayList<Object>();
		for (SalesInvoiceVO salesInvoiceVO2 : salesInvoiceVOs) {
			if (null != salesInvoiceVO2.getProcessStatus()
					&& (boolean) salesInvoiceVO2.getProcessStatus()) {
				if (salesInvoiceVO2.getTotalInvoice()
						- salesInvoiceVO2.getInvoiceAmount() > 0) {
					salesInvoiceVO2.setPendingInvoice(salesInvoiceVO2
							.getTotalInvoice()
							- salesInvoiceVO2.getInvoiceAmount());
					salesInvoiceVO2.setPendingInvoiceStr(AIOSCommons
							.formatAmount(salesInvoiceVO2.getPendingInvoice()));
					salesInvoiceVOtemps.add(salesInvoiceVO2);
				}
			}
		}
		return salesInvoiceVOtemps;
	}

	public void saveSalesInvoice(SalesInvoice salesInvoice,
			List<SalesInvoiceDetail> salesInvoiceDetails) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		salesInvoice.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		SystemBL.saveReferenceStamp(SalesInvoice.class.getName(),
				getImplementation());
		salesInvoice.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		boolean updateFlag = false;
		if (null != salesInvoice.getSalesInvoiceId())
			updateFlag = true;
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (salesInvoice != null && salesInvoice.getSalesInvoiceId() != null
				&& salesInvoice.getSalesInvoiceId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		salesInvoiceService.saveSalesInvoice(salesInvoice);
		List<SalesInvoice> salesInvoices = new ArrayList<SalesInvoice>();
		salesInvoices.add(salesInvoice);
		salesInvoices.get(0).setSalesInvoiceDetails(
				new HashSet<SalesInvoiceDetail>(salesInvoiceDetails));

		if (updateFlag) {
			Alert alert = alertBL.getAlertService().getAlertRecordInfo(
					salesInvoice.getSalesInvoiceId(), "SalesInvoice");
			alertBL.getAlertService().deleteAlert(alert);
		}

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesInvoice.class.getSimpleName(),
				salesInvoice.getSalesInvoiceId(), user, workflowDetailVo);
	}

	// Save Sales Invoice
	public void saveSalesInvoice(SalesInvoice salesInvoice,
			List<SalesInvoiceDetail> salesInvoiceDetails,
			List<SalesDeliveryNote> salesDeliveryNotes,
			List<SalesInvoiceDetail> deleteSalesInvoiceDetails,
			SalesInvoice salesInvoiceEdit) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		salesInvoice.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (salesInvoice != null && salesInvoice.getSalesInvoiceId() != null
				&& salesInvoice.getSalesInvoiceId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		if (salesInvoiceEdit != null
				&& salesInvoiceEdit.getSalesInvoiceId() > 0) {
			deleteDirectSalesInvoice(salesInvoiceEdit, salesDeliveryNotes);
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		}

		salesInvoice.setImplementation(getImplementation());
		boolean updateFlag = false;
		if (null != salesInvoice.getSalesInvoiceId())
			updateFlag = true;
		if (null != deleteSalesInvoiceDetails
				&& deleteSalesInvoiceDetails.size() > 0)
			salesInvoiceService
					.deleteSalesInvoiceDetails(deleteSalesInvoiceDetails);

		salesInvoiceService.saveSalesInvoice(salesInvoice, salesInvoiceDetails);

		if (updateFlag) {
			List<SalesInvoiceDetail> salesInvoiceDetailList = new ArrayList<SalesInvoiceDetail>();
			for (SalesInvoiceDetail salesInvoiceDetail : salesInvoiceDetails) {
				if (null == salesInvoiceDetail.getSalesInvoiceDetailId())
					salesInvoiceDetailList.add(salesInvoiceDetail);
			}
		} else {
			SystemBL.saveReferenceStamp(SalesInvoice.class.getName(),
					getImplementation());
		}

		if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0)
			salesDeliveryNoteBL.getSalesDeliveryNoteService()
					.saveSalesDeliveryNotes(salesDeliveryNotes);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesInvoice.class.getSimpleName(),
				salesInvoice.getSalesInvoiceId(), user, workflowDetailVo);
		// Alert Bank Receipt
		if (updateFlag) {
			Alert alert = alertBL.getAlertService().getAlertRecordInfo(
					salesInvoice.getSalesInvoiceId(), "SalesInvoice");
			alertBL.getAlertService().deleteAlert(alert);
		}
	}

	public void doSalesInvoiceBusinessUpdate(Long salesInvoiceId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			SalesInvoice salesInvoice = salesInvoiceService
					.getSalesInvoiceById(salesInvoiceId);
			if (!workflowDetailVO.isDeleteFlag()) {
				List<SalesInvoice> salesInvoices = new ArrayList<SalesInvoice>();
				salesInvoices.add(salesInvoice);
				salesInvoices.get(0).setSalesInvoiceDetails(
						salesInvoice.getSalesInvoiceDetails());
				alertBL.alertSalesInvoiceProcess(salesInvoices);
			} else {
				Alert alert = alertBL.getAlertService().getAlertRecordInfo(
						salesInvoice.getSalesInvoiceId(), "SalesInvoice");
				if (null != alert)
					alertBL.getAlertService().deleteAlert(alert);
				if (null != salesInvoice.getSalesInvoiceDetails()
						&& salesInvoice.getSalesInvoiceDetails().size() > 0) {
					salesInvoiceService.deleteSalesInvoice(
							salesInvoice,
							new ArrayList<SalesInvoiceDetail>(salesInvoice
									.getSalesInvoiceDetails()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<Byte, String> getO2CProcessStatus() throws Exception {
		Map<Byte, String> o2CProcessStatuses = new HashMap<Byte, String>();
		for (O2CProcessStatus o2CProcessStatus : EnumSet
				.allOf(O2CProcessStatus.class))
			o2CProcessStatuses.put(o2CProcessStatus.getCode(), o2CProcessStatus
					.name().replaceAll("_", " "));
		return o2CProcessStatuses;
	}

	// Get all Sales Invoice
	public List<SalesInvoiceVO> getAllContiniousSalesInvoiceForReportData(
			SalesInvoiceVO salesInvoiceVOTemp) throws Exception {
		List<SalesInvoice> salesInvoices = salesInvoiceService
				.getFilteredSalesInvoice(
						salesInvoiceVOTemp.getImplementation(),
						salesInvoiceVOTemp.getSalesInvoiceId(),
						salesInvoiceVOTemp.getCustomerId(),
						salesInvoiceVOTemp.getStatus(),
						salesInvoiceVOTemp.getFormDate(),
						salesInvoiceVOTemp.getToDate());
		List<SalesInvoiceVO> salesInvoiceVOsFinal = null;
		List<SalesInvoiceVO> salesInvoiceVOs = null;
		SalesInvoiceVO salesInvoiceVO = null;
		Map<Long, SalesInvoiceVO> completeMap = new HashMap<Long, SalesInvoiceVO>();
		Map<Long, List<SalesInvoiceVO>> salesInvoiceMap = new HashMap<Long, List<SalesInvoiceVO>>();

		for (SalesInvoice salesInvoice : salesInvoices) {
			if ((salesInvoice.getSalesInvoice() != null && salesInvoiceMap
					.containsKey(salesInvoice.getSalesInvoice()
							.getSalesInvoiceId()))) {
				salesInvoiceVOs = salesInvoiceMap.get(salesInvoice
						.getSalesInvoice().getSalesInvoiceId());
				salesInvoiceVO = new SalesInvoiceVO();
				salesInvoiceVO
						.setInvoiceAmount(salesInvoice.getInvoiceAmount()
								+ (null != salesInvoiceVO.getInvoiceAmount() ? salesInvoiceVO
										.getInvoiceAmount() : 0));

				Double totalInvoiceByNow = salesInvoiceVOs.get(
						salesInvoiceVOs.size() - 1).getPendingInvoice();
				salesInvoiceVO.setTotalInvoice(totalInvoiceByNow);
				salesInvoiceVO.setSalesInvoiceDate(DateFormat
						.convertDateToString(salesInvoice.getInvoiceDate()
								.toString()));
				salesInvoiceVO.setReferenceNumber(salesInvoice
						.getReferenceNumber());
				salesInvoiceVO.setSalesInvoiceId(salesInvoice
						.getSalesInvoiceId());
				salesInvoiceVO
						.setCustomerName(null != salesInvoice.getCustomer()
								.getPersonByPersonId() ? salesInvoice
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat("")
								.concat(salesInvoice.getCustomer()
										.getPersonByPersonId().getLastName())
								: (null != salesInvoice.getCustomer()
										.getCompany() ? salesInvoice
										.getCustomer().getCompany()
										.getCompanyName() : salesInvoice
										.getCustomer().getCmpDeptLocation()
										.getCompany().getCompanyName()));
				salesInvoiceVO.setCreditTermName(null != salesInvoice
						.getCreditTerm() ? salesInvoice.getCreditTerm()
						.getName() : "");
				salesInvoiceVO.setPendingInvoice(salesInvoiceVO
						.getTotalInvoice() - salesInvoiceVO.getInvoiceAmount());
				salesInvoiceVO
						.setProcessStatus(salesInvoice.getProcessStatus());
				salesInvoiceVO.setCustomerNumber(salesInvoice.getCustomer()
						.getCustomerNumber());
				salesInvoiceVO.setParentInvoice(salesInvoice.getSalesInvoice()
						.getReferenceNumber());
				salesInvoiceVO.setCustomerId(salesInvoice.getCustomer()
						.getCustomerId());
				Map<String, String> salesNo = new HashMap<String, String>();
				Map<String, String> dt = new HashMap<String, String>();
				for (SalesInvoiceDetail salesInvoiceDetail : salesInvoice
						.getSalesInvoice().getSalesInvoiceDetails()) {
					salesNo.put(salesInvoiceDetail.getSalesDeliveryDetail()
							.getSalesDeliveryNote().getReferenceNumber(),
							salesInvoiceDetail.getSalesDeliveryDetail()
									.getSalesDeliveryNote()
									.getReferenceNumber());
					dt.put(DateFormat.convertDateToString(salesInvoiceDetail
							.getSalesDeliveryDetail().getSalesDeliveryNote()
							.getDeliveryDate()
							+ ""),
							DateFormat.convertDateToString(salesInvoiceDetail
									.getSalesDeliveryDetail()
									.getSalesDeliveryNote().getDeliveryDate()
									+ ""));
				}
				salesInvoiceVO.setSalesDeliveryNumber("");
				for (Map.Entry<String, String> entry : salesNo.entrySet()) {
					salesInvoiceVO.setSalesDeliveryNumber(salesInvoiceVO
							.getSalesDeliveryNumber() + "," + entry.getValue());
				}
				salesInvoiceVO.setSalesDate("");
				for (Map.Entry<String, String> entry : dt.entrySet()) {
					salesInvoiceVO.setSalesDate(salesInvoiceVO.getSalesDate()
							+ "," + entry.getValue());
				}

				salesInvoiceVO
						.setSalesInvoiceDate(DateFormat
								.convertDateToString(salesInvoice
										.getInvoiceDate() + ""));
				salesInvoiceVO.setSalesFlag("O2C");
				salesInvoiceVOs.add(salesInvoiceVO);
				salesInvoiceMap.put(salesInvoice.getSalesInvoice()
						.getSalesInvoiceId(), salesInvoiceVOs);
			} else {
				salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();
				salesInvoiceVO = new SalesInvoiceVO();
				salesInvoiceVO
						.setInvoiceAmount(salesInvoice.getInvoiceAmount()
								+ (null != salesInvoiceVO.getInvoiceAmount() ? salesInvoiceVO
										.getInvoiceAmount() : 0));
				salesInvoiceVO.setTotalInvoice(salesInvoice.getTotalInvoice());
				salesInvoiceVO.setSalesInvoiceDate(DateFormat
						.convertDateToString(salesInvoice.getInvoiceDate()
								.toString()));
				salesInvoiceVO.setReferenceNumber(salesInvoice
						.getReferenceNumber());
				salesInvoiceVO.setSalesInvoiceId(salesInvoice
						.getSalesInvoiceId());
				salesInvoiceVO
						.setCustomerName(null != salesInvoice.getCustomer()
								.getPersonByPersonId() ? salesInvoice
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat("")
								.concat(salesInvoice.getCustomer()
										.getPersonByPersonId().getLastName())
								: (null != salesInvoice.getCustomer()
										.getCompany() ? salesInvoice
										.getCustomer().getCompany()
										.getCompanyName() : salesInvoice
										.getCustomer().getCmpDeptLocation()
										.getCompany().getCompanyName()));
				salesInvoiceVO.setCreditTermName(null != salesInvoice
						.getCreditTerm() ? salesInvoice.getCreditTerm()
						.getName() : "");
				salesInvoiceVO.setPendingInvoice(salesInvoiceVO
						.getTotalInvoice() - salesInvoiceVO.getInvoiceAmount());
				salesInvoiceVO
						.setProcessStatus(salesInvoice.getProcessStatus());
				salesInvoiceVO.setCustomerNumber(salesInvoice.getCustomer()
						.getCustomerNumber());
				salesInvoiceVO.setCustomerId(salesInvoice.getCustomer()
						.getCustomerId());
				Map<String, String> salesNo = new HashMap<String, String>();
				Map<String, String> dt = new HashMap<String, String>();
				for (SalesInvoiceDetail salesInvoiceDetail : salesInvoice
						.getSalesInvoiceDetails()) {
					salesNo.put(salesInvoiceDetail.getSalesDeliveryDetail()
							.getSalesDeliveryNote().getReferenceNumber(),
							salesInvoiceDetail.getSalesDeliveryDetail()
									.getSalesDeliveryNote()
									.getReferenceNumber());
					dt.put(DateFormat.convertDateToString(salesInvoiceDetail
							.getSalesDeliveryDetail().getSalesDeliveryNote()
							.getDeliveryDate()
							+ ""),
							DateFormat.convertDateToString(salesInvoiceDetail
									.getSalesDeliveryDetail()
									.getSalesDeliveryNote().getDeliveryDate()
									+ ""));
				}
				salesInvoiceVO.setSalesDeliveryNumber("");
				for (Map.Entry<String, String> entry : salesNo.entrySet()) {
					salesInvoiceVO.setSalesDeliveryNumber(salesInvoiceVO
							.getSalesDeliveryNumber() + "," + entry.getValue());
				}
				salesInvoiceVO.setSalesDate("");
				for (Map.Entry<String, String> entry : dt.entrySet()) {
					salesInvoiceVO.setSalesDate(salesInvoiceVO.getSalesDate()
							+ "," + entry.getValue());
				}
				salesInvoiceVO
						.setSalesInvoiceDate(DateFormat
								.convertDateToString(salesInvoice
										.getInvoiceDate() + ""));
				salesInvoiceVOs.add(salesInvoiceVO);
				salesInvoiceMap.put(salesInvoice.getSalesInvoiceId(),
						salesInvoiceVOs);
			}
			salesInvoiceVO.setSalesFlag("O2C");
			completeMap.put(salesInvoice.getSalesInvoiceId(), salesInvoiceVO);

		}

		if (salesInvoiceMap != null && salesInvoiceMap.size() > 0) {
			salesInvoiceVOsFinal = new ArrayList<SalesInvoiceVO>();
			salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();
			for (SalesInvoice salesInvoice : salesInvoices) {
				if (salesInvoiceMap.containsKey(salesInvoice
						.getSalesInvoiceId())) {
					salesInvoiceVO = new SalesInvoiceVO();
					salesInvoiceVO = salesInvoiceMap
							.get(salesInvoice.getSalesInvoiceId())
							.get(salesInvoiceMap.get(
									salesInvoice.getSalesInvoiceId()).size() - 1);
					salesInvoiceVO.setSalesInvoiceVOs(salesInvoiceMap
							.get(salesInvoice.getSalesInvoiceId()));
					salesInvoiceVO.setInvoiceAmount(AIOSCommons
							.roundDecimals(salesInvoiceVO.getInvoiceAmount()));
					salesInvoiceVO.setTotalInvoice(AIOSCommons
							.roundDecimals(salesInvoiceVO.getTotalInvoice()));
					salesInvoiceVO.setPendingInvoice(AIOSCommons
							.roundDecimals(salesInvoiceVO.getPendingInvoice()));

					if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Pending
							.getCode()
							&& salesInvoiceVO.getPendingInvoice() > 0.0)
						salesInvoiceVOsFinal.add(salesInvoiceVO);
					else if ((byte) salesInvoiceVOTemp.getPaymentStatus() == (byte) PaymentStatus.Paid
							.getCode()
							&& salesInvoiceVO.getPendingInvoice() <= 0.0)
						salesInvoiceVOsFinal.add(salesInvoiceVO);
					else if ((byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Pending
							.getCode()
							&& (byte) salesInvoiceVOTemp.getPaymentStatus() != (byte) PaymentStatus.Paid
									.getCode())
						salesInvoiceVOsFinal.add(salesInvoiceVO);
				}
			}
		}

		if (salesInvoiceVOsFinal != null)
			Collections.sort(salesInvoiceVOsFinal,
					new Comparator<SalesInvoiceVO>() {
						public int compare(SalesInvoiceVO m1, SalesInvoiceVO m2) {
							return m2.getSalesInvoiceId().compareTo(
									m1.getSalesInvoiceId());
						}
					});
		return salesInvoiceVOsFinal;
	}

	public SalesInvoiceVO convertEntityToVO(SalesInvoice salesInvoice) {
		SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO();
		BeanUtils.copyProperties(salesInvoice, salesInvoiceVO);
		salesInvoiceVO.setReferenceNumber(salesInvoice.getReferenceNumber());
		salesInvoiceVO.setSalesDate(DateFormat.convertDateToString(salesInvoice
				.getInvoiceDate().toString()));
		salesInvoiceVO.setSalesInvoiceId(salesInvoice.getSalesInvoiceId());
		salesInvoiceVO.setCustomerName(null != salesInvoice.getCustomer()
				.getPersonByPersonId() ? salesInvoice
				.getCustomer()
				.getPersonByPersonId()
				.getFirstName()
				.concat("")
				.concat(salesInvoice.getCustomer().getPersonByPersonId()
						.getLastName()) : (null != salesInvoice.getCustomer()
				.getCompany() ? salesInvoice.getCustomer().getCompany()
				.getCompanyName() : salesInvoice.getCustomer()
				.getCmpDeptLocation().getCompany().getCompanyName()));
		if (salesInvoice.getCustomer().getCompany() != null) {
			salesInvoiceVO.setFax(salesInvoice.getCustomer().getCompany()
					.getCompanyFax()
					+ "");
			salesInvoiceVO.setTelephone(salesInvoice.getCustomer().getCompany()
					.getCompanyPhone()
					+ "");
			salesInvoiceVO.setAddress(salesInvoice.getCustomer().getCompany()
					.getCompanyAddress()
					+ "");
			salesInvoiceVO.setPoBox(salesInvoice.getCustomer().getCompany()
					.getCompanyPobox()
					+ "");
		}
		if (null != salesInvoiceVO.getCustomer().getPersonByPersonId()) {
			salesInvoiceVO.setCustomerMobile(salesInvoiceVO.getCustomer()
					.getPersonByPersonId().getMobile());
			salesInvoiceVO.setCustomerAddress(salesInvoiceVO.getCustomer()
					.getPersonByPersonId().getResidentialAddress());
		} else {
			salesInvoiceVO.setCustomerMobile(salesInvoiceVO.getCustomer()
					.getCompany().getCompanyPhone());
			salesInvoiceVO.setCustomerAddress(salesInvoiceVO.getCustomer()
					.getCompany().getCompanyAddress());
		}
		salesInvoiceVO
				.setCreditTermName(null != salesInvoice.getCreditTerm() ? salesInvoice
						.getCreditTerm().getName() : "");
		List<SalesInvoice> salesInvoiceList = null;
		if (salesInvoice.getSalesInvoice() != null) {
			salesInvoiceVO.setTotalInvoice(salesInvoice.getSalesInvoice()
					.getTotalInvoice());
			salesInvoiceList = new ArrayList<SalesInvoice>(salesInvoice
					.getSalesInvoice().getSalesInvoices());
			salesInvoiceList.add(salesInvoice.getSalesInvoice());
		} else {
			salesInvoiceVO.setTotalInvoice(salesInvoice.getTotalInvoice());
			salesInvoiceList = new ArrayList<SalesInvoice>(
					salesInvoice.getSalesInvoices());
		}
		double paidAmount = 0.0;
		for (SalesInvoice salesInvoice2 : salesInvoiceList) {
			if (salesInvoice.getSalesInvoiceId() != salesInvoice2
					.getSalesInvoiceId())
				paidAmount += salesInvoice2.getInvoiceAmount();
		}
		salesInvoiceVO.setPaidInvoice(paidAmount);

		if (salesInvoiceVO.getTotalInvoice() != null)
			salesInvoiceVO.setPendingInvoice(salesInvoiceVO.getTotalInvoice()
					- (salesInvoiceVO.getPaidInvoice() + salesInvoiceVO
							.getInvoiceAmount()));
		if (salesInvoice.getSalesInvoiceDetails() != null) {
			SalesDeliveryNoteVO masterVO = new SalesDeliveryNoteVO();
			masterVO.setTotalDeliveryAmount(0.0);
			SalesDeliveryDetailVO deldetailVO = null;
			for (SalesInvoiceDetail detail : salesInvoice
					.getSalesInvoiceDetails()) {
				try {
					deldetailVO = salesDeliveryNoteBL
							.convertSalesDeliveryDetailToVO(detail
									.getSalesDeliveryDetail());
					masterVO.setTotalDeliveryAmount(masterVO
							.getTotalDeliveryAmount()
							+ deldetailVO.getTotalRate());
					if (detail.getSalesDeliveryDetail().getSalesDeliveryNote() != null) {
						masterVO.setReference2(detail.getSalesDeliveryDetail()
								.getSalesDeliveryNote().getReference2());
						masterVO.setReference1(detail.getSalesDeliveryDetail()
								.getSalesDeliveryNote().getReference1());
					}
					masterVO.getSalesDeliveryDetailVOs().add(deldetailVO);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			String decimalAmount = String.valueOf(AIOSCommons
					.formatAmount(salesInvoice.getInvoiceAmount()));
			decimalAmount = decimalAmount.substring(decimalAmount
					.lastIndexOf('.') + 1);
			String amountInWords = AIOSCommons
					.convertAmountToWords(salesInvoice.getInvoiceAmount());
			if (Double.parseDouble(decimalAmount) > 0) {
				amountInWords = amountInWords
						.concat(" and ")
						.concat(AIOSCommons.convertAmountToWords(decimalAmount))
						.concat(" Fils ");
				String replaceWord = "Only";
				amountInWords = amountInWords.replaceAll(replaceWord, "");
				amountInWords = amountInWords.concat(" " + replaceWord);
			}
			salesInvoiceVO.setTotalInvoice(AIOSCommons
					.roundDecimals(salesInvoice.getInvoiceAmount()));
			salesInvoiceVO.setAmountInWords(amountInWords);

			salesInvoiceVO.setSalesDeliveryNoteVO(masterVO);
		}

		return salesInvoiceVO;
	}

	public void deleteSalesInvoice(SalesInvoice salesInvoice,
			List<SalesDeliveryNote> salesDeliveryNotesCurrent) throws Exception {

		alertBL.commonAlertDeleteByUsecaseAndRecord(
				SalesInvoice.class.getSimpleName(),
				salesInvoice.getSalesInvoiceId());

		// Skip the current delivery not from update conflicts
		Map<Long, SalesDeliveryNote> salesDeliveryNotesTemp = new HashMap<Long, SalesDeliveryNote>();
		if (salesDeliveryNotesCurrent != null
				&& salesDeliveryNotesCurrent.size() > 0)
			for (SalesDeliveryNote salesDeliveryNote : salesDeliveryNotesCurrent) {
				salesDeliveryNotesTemp.put(
						salesDeliveryNote.getSalesDeliveryNoteId(),
						salesDeliveryNote);
			}

		Map<Long, SalesDeliveryNote> salesDeliveryNotes = new HashMap<Long, SalesDeliveryNote>();
		for (SalesInvoiceDetail salesInvoiceDetail : salesInvoice
				.getSalesInvoiceDetails()) {
			if (!salesDeliveryNotesTemp.containsKey(salesInvoiceDetail
					.getSalesDeliveryDetail().getSalesDeliveryNote()
					.getSalesDeliveryNoteId())) {
				salesInvoiceDetail.getSalesDeliveryDetail()
						.getSalesDeliveryNote().setStatusNote((byte) 1);
				salesDeliveryNotes.put(salesInvoiceDetail
						.getSalesDeliveryDetail().getSalesDeliveryNote()
						.getSalesDeliveryNoteId(), salesInvoiceDetail
						.getSalesDeliveryDetail().getSalesDeliveryNote());
			}
		}

		if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0)
			salesDeliveryNoteBL.getSalesDeliveryNoteService()
					.saveSalesDeliveryNotes(
							new ArrayList<SalesDeliveryNote>(salesDeliveryNotes
									.values()));

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				SalesInvoice.class.getSimpleName(),
				salesInvoice.getSalesInvoiceId(), user, workflowDetailVo);
	}

	public void deleteDirectSalesInvoice(SalesInvoice salesInvoice,
			List<SalesDeliveryNote> salesDeliveryNotesCurrent) throws Exception {

		alertBL.commonAlertDeleteByUsecaseAndRecord(
				SalesInvoice.class.getSimpleName(),
				salesInvoice.getSalesInvoiceId());

		// Skip the current delivery not from update conflicts
		Map<Long, SalesDeliveryNote> salesDeliveryNotesTemp = new HashMap<Long, SalesDeliveryNote>();
		if (salesDeliveryNotesCurrent != null
				&& salesDeliveryNotesCurrent.size() > 0)
			for (SalesDeliveryNote salesDeliveryNote : salesDeliveryNotesCurrent) {
				salesDeliveryNotesTemp.put(
						salesDeliveryNote.getSalesDeliveryNoteId(),
						salesDeliveryNote);
			}

		Map<Long, SalesDeliveryNote> salesDeliveryNotes = new HashMap<Long, SalesDeliveryNote>();
		for (SalesInvoiceDetail salesInvoiceDetail : salesInvoice
				.getSalesInvoiceDetails()) {
			if (!salesDeliveryNotesTemp.containsKey(salesInvoiceDetail
					.getSalesDeliveryDetail().getSalesDeliveryNote()
					.getSalesDeliveryNoteId())) {
				salesInvoiceDetail.getSalesDeliveryDetail()
						.getSalesDeliveryNote().setStatusNote((byte) 1);
				salesDeliveryNotes.put(salesInvoiceDetail
						.getSalesDeliveryDetail().getSalesDeliveryNote()
						.getSalesDeliveryNoteId(), salesInvoiceDetail
						.getSalesDeliveryDetail().getSalesDeliveryNote());
			}
		}

		if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0)
			salesDeliveryNoteBL.getSalesDeliveryNoteService()
					.saveSalesDeliveryNotes(
							new ArrayList<SalesDeliveryNote>(salesDeliveryNotes
									.values()));

		salesInvoiceService.deleteSalesInvoice(
				salesInvoice,
				new ArrayList<SalesInvoiceDetail>(salesInvoice
						.getSalesInvoiceDetails()));
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters and Setters
	public SalesInvoiceService getSalesInvoiceService() {
		return salesInvoiceService;
	}

	public void setSalesInvoiceService(SalesInvoiceService salesInvoiceService) {
		this.salesInvoiceService = salesInvoiceService;
	}

	public SalesDeliveryNoteBL getSalesDeliveryNoteBL() {
		return salesDeliveryNoteBL;
	}

	public void setSalesDeliveryNoteBL(SalesDeliveryNoteBL salesDeliveryNoteBL) {
		this.salesDeliveryNoteBL = salesDeliveryNoteBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public CreditBL getCreditBL() {
		return creditBL;
	}

	public void setCreditBL(CreditBL creditBL) {
		this.creditBL = creditBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
