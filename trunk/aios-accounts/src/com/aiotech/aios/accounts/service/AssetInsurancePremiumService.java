package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetInsurancePremium;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetInsurancePremiumService {

	private AIOTechGenericDAO<AssetInsurancePremium> assetInsurancePremiumDAO;

	// Get All Asset Insurance Premium
	public List<AssetInsurancePremium> getAllPremiumDuePaymentByDate(
			Date currentDate) {
		return assetInsurancePremiumDAO.findByNamedQuery(
				"getAllPremiumDuePaymentByDate", currentDate);
	}
	
	// Get All Asset Insurance Premium
	public List<AssetInsurancePremium> getActivePremiumDueInsuranceId(
			long assetInsuranceId) {
		return assetInsurancePremiumDAO.findByNamedQuery(
				"getActivePremiumDueInsuranceId", assetInsuranceId);
	}
	
	// Get All Asset Insurance Premium
	public List<AssetInsurancePremium> getPremiumDueInsuranceId(
			long assetInsuranceId) {
		return assetInsurancePremiumDAO.findByNamedQuery(
				"getPremiumDueInsuranceId", assetInsuranceId);
	}

	public void deleteAssetInsurance(
			List<AssetInsurancePremium> assetInsurancePremiums) {
		assetInsurancePremiumDAO.deleteAll(assetInsurancePremiums);
	}
	
	// Save Insurance Premium
	public void saveInsurancePremium(AssetInsurancePremium assetInsurancePremium)
			throws Exception {
		assetInsurancePremiumDAO.saveOrUpdate(assetInsurancePremium);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetInsurancePremium> getAssetInsurancePremiumDAO() {
		return assetInsurancePremiumDAO;
	}

	public void setAssetInsurancePremiumDAO(
			AIOTechGenericDAO<AssetInsurancePremium> assetInsurancePremiumDAO) {
		this.assetInsurancePremiumDAO = assetInsurancePremiumDAO;
	} 
}
