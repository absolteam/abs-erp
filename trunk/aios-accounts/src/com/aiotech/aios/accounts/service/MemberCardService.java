package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MemberCard;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MemberCardService {

	private AIOTechGenericDAO<MemberCard> memberCardDAO;

	// Get all Member Cards
	public List<MemberCard> getAllMemberCards(Implementation implementation)
			throws Exception {
		return memberCardDAO.findByNamedQuery("getAllMemberCards",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Member Card by Card Type
	public MemberCard getMemberCardByCardType(Implementation implementation,
			byte cardType) throws Exception {
		List<MemberCard> memberCards = memberCardDAO.findByNamedQuery(
				"getMemberCardByCardType", implementation, cardType);
		if (null != memberCards && memberCards.size() > 0)
			return memberCardDAO.findByNamedQuery("getMemberCardByCardType",
					implementation, cardType).get(0);
		return null;
	}

	// Save Member Card
	public void saveMemberCard(MemberCard memberCard) throws Exception {
		
		memberCardDAO.saveOrUpdate(memberCard);
	}

	// Merge Member Card
	public void mergeMemberCard(MemberCard memberCard) throws Exception {
		memberCardDAO.merge(memberCard);
	}

	// Delete Member Card
	public void deleteMemberCard(MemberCard memberCard) throws Exception {
		memberCardDAO.delete(memberCard);
	}

	// Get Member Card by Member Card Id
	public MemberCard getMemberCardByCardId(long memberCardId) throws Exception {
		return memberCardDAO.findById(memberCardId);
	}

	// Getters & Setters
	public AIOTechGenericDAO<MemberCard> getMemberCardDAO() {
		return memberCardDAO;
	}

	public void setMemberCardDAO(AIOTechGenericDAO<MemberCard> memberCardDAO) {
		this.memberCardDAO = memberCardDAO;
	}
}
