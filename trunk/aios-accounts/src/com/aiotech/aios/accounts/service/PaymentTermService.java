package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.PaymentTerm;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PaymentTermService {
	
	private AIOTechGenericDAO<PaymentTerm> paymentTermDAO;

	public List<PaymentTerm> getAllPaymentTerms(Implementation implementation)throws Exception{
		return this.getPaymentTermDAO().findByNamedQuery("getAllPaymentTerms", implementation);
	}
	
	public PaymentTerm getPaymentTerm(Long paymentTermId) throws Exception{
		return this.getPaymentTermDAO().findById(paymentTermId);
	}
	
	public void deletePaymentTerm(PaymentTerm paymentTerm)throws Exception{
		this.getPaymentTermDAO().delete(paymentTerm);
	}
	
	public PaymentTerm getPaymentTermById(Long paymentTermId) throws Exception{
		return this.getPaymentTermDAO().findByNamedQuery("getPaymentTermById", paymentTermId).get(0);
	}
	
	public void savePaymentTerm(PaymentTerm paymentTerm) throws Exception{
		 this.getPaymentTermDAO().saveOrUpdate(paymentTerm);
	}
	
	public AIOTechGenericDAO<PaymentTerm> getPaymentTermDAO() {
		return paymentTermDAO;
	}

	public void setPaymentTermDAO(AIOTechGenericDAO<PaymentTerm> paymentTermDAO) {
		this.paymentTermDAO = paymentTermDAO;
	}
	
}
