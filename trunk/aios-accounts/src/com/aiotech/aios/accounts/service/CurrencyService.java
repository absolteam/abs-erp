package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyPool;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CurrencyService {

	private AIOTechGenericDAO<Currency> currencyDAO;
	private AIOTechGenericDAO<CurrencyPool> currencyPoolDAO;
	private AIOTechGenericDAO<Country> countryDAO;

	public List<Currency> getAllCurrency(Implementation implementation)
			throws Exception {
		return this.getCurrencyDAO().findByNamedQuery("getCurrencyList",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public String getCountryDetails(int countryId) throws Exception {
		Country country = new Country();
		country = countryDAO.findById(countryId);
		return country.getCountryName();
	}

	public void saveCurrency(List<Currency> currency) throws Exception {
		currencyDAO.saveOrUpdateAll(currency);

	}

	public void deleteCurrency(Currency currency) throws Exception {
		currencyDAO.delete(currency);

	}

	public Currency getCurrency(long currencyId) throws Exception {
		Currency currecy = currencyDAO.findByNamedQuery("getCurrency",
				currencyId).get(0);
		return currecy;
	}

	public Currency getDefaultCurrency(Implementation implementation) {
		List<Currency> currency = currencyDAO.findByNamedQuery(
				"getDefaultCurrency", implementation);
		return null != currency && currency.size() > 0 ? currency.get(0) : null;
	}

	public void saveCurrency(Currency currency) throws Exception {
		currencyDAO.saveOrUpdate(currency);
	}

	public Country getCountryList(Long countryId) throws Exception {
		return countryDAO.findById(countryId);
	}

	public List<CurrencyPool> getAllCurrencyPool() throws Exception {
		return currencyPoolDAO.findByNamedQuery("getAllCurrencyPool");
	}

	public List<CurrencyPool> getCurrencyPool(Implementation implementation)
			throws Exception {
		return currencyPoolDAO.findByNamedQuery("getCurrencyPool",
				implementation);
	}

	public List<Currency> getNonFnCurrencies(Implementation implementation)
			throws Exception {
		return currencyDAO.findByNamedQuery("getNonFnCurrencies",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public AIOTechGenericDAO<Currency> getCurrencyDAO() {
		return currencyDAO;
	}

	public void setCurrencyDAO(AIOTechGenericDAO<Currency> currencyDAO) {
		this.currencyDAO = currencyDAO;
	}

	public AIOTechGenericDAO<CurrencyPool> getCurrencyPoolDAO() {
		return currencyPoolDAO;
	}

	public void setCurrencyPoolDAO(
			AIOTechGenericDAO<CurrencyPool> currencyPoolDAO) {
		this.currencyPoolDAO = currencyPoolDAO;
	}

	public AIOTechGenericDAO<Country> getCountryDAO() {
		return countryDAO;
	}

	public void setCountryDAO(AIOTechGenericDAO<Country> countryDAO) {
		this.countryDAO = countryDAO;
	}
}
