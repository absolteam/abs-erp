package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.RewardPolicy;
import com.aiotech.aios.accounts.domain.entity.vo.RewardPolicyVO;
import com.aiotech.aios.accounts.service.RewardPolicyService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class RewardPolicyBL {

	// Dependencies
	private RewardPolicyService rewardPolicyService;
	private CouponBL couponBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show all Reward Policy
	public List<Object> showAllRewardPolicy() throws Exception {
		List<RewardPolicy> rewardPolicys = rewardPolicyService
				.getAllRewardPolicies(getImplementation());
		List<Object> rewardPolicyVOs = new ArrayList<Object>();
		if (null != rewardPolicys && rewardPolicys.size() > 0)
			for (RewardPolicy rewardPolicy : rewardPolicys)
				rewardPolicyVOs.add(addRewardPolicyVO(rewardPolicy));
		return rewardPolicyVOs;
	}

	// Add Reward Policy VO
	public Object addRewardPolicyVO(RewardPolicy rewardPolicy) {
		RewardPolicyVO rewardPolicyVO = new RewardPolicyVO();
		rewardPolicyVO.setRewardPolicyId(rewardPolicy.getRewardPolicyId());
		rewardPolicyVO.setRewardType(rewardPolicy.getRewardType());
		rewardPolicyVO.setDescription(rewardPolicy.getDescription());
		rewardPolicyVO.setPolicyName(rewardPolicy.getPolicyName());
		rewardPolicyVO.setIsPercentage(rewardPolicy.getIsPercentage());
		rewardPolicyVO.setReward(rewardPolicy.getReward());
		rewardPolicyVO.setRewardPercentageOrAmount(rewardPolicy.getReward()
				.toString()
				.concat(rewardPolicy.getIsPercentage() ? " %" : " AED"));
		rewardPolicyVO.setRewardTypeName(Constants.Accounts.RewardType.get(
				rewardPolicy.getRewardType()).name());
		rewardPolicyVO.setCouponVO(couponBL.addCoupontVO(rewardPolicy
				.getCoupon()));
		return rewardPolicyVO;
	}

	// Save Reward Policy
	public void saveRewardPolicy(RewardPolicy rewardPolicy) throws Exception {
		rewardPolicy.setImplementation(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		rewardPolicy.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (rewardPolicy != null && rewardPolicy.getRewardPolicyId() != null
				&& rewardPolicy.getRewardPolicyId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		} 
		rewardPolicyService.saveRewardPolicy(rewardPolicy);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				RewardPolicy.class.getSimpleName(), rewardPolicy.getRewardPolicyId(),
				user, workflowDetailVo);
	}

	public void doRewardPolicyBusinessUpdate(Long rewardPolicyId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				RewardPolicy rewardPolicy = rewardPolicyService
						.getRewardPolicyByPolicyId(rewardPolicyId);
				rewardPolicyService.deleteRewardPolicy(rewardPolicy);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Delete Reward Policy
	public void deleteRewardPolicy(RewardPolicy rewardPolicy) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						RewardPolicy.class.getSimpleName(), rewardPolicy.getRewardPolicyId(),
						user, workflowDetailVo);  
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public RewardPolicyService getRewardPolicyService() {
		return rewardPolicyService;
	}

	public void setRewardPolicyService(RewardPolicyService rewardPolicyService) {
		this.rewardPolicyService = rewardPolicyService;
	}

	public CouponBL getCouponBL() {
		return couponBL;
	}

	public void setCouponBL(CouponBL couponBL) {
		this.couponBL = couponBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
