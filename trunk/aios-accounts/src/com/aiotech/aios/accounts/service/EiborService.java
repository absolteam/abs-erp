package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Eibor; 
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class EiborService {
	private AIOTechGenericDAO<Eibor> eiborDAO;

	public List<Eibor> getEiborList(Implementation implementation) throws Exception{
		return eiborDAO.findByNamedQuery("getEiborList",implementation);
	}
	
	public void saveOrUpdateEibor(Eibor eibor)throws Exception{
		eiborDAO.saveOrUpdate(eibor);
	}
	
	public Eibor getEiborById(Long eiborId)throws Exception{
		return eiborDAO.findById(eiborId);
	}
	
	public List<Eibor> getEiborByBankId(Implementation implementation)throws Exception{
		return eiborDAO.findByNamedQuery("getEiborList",implementation);
	}
	
	
	public void deleteEibor(Eibor eibor) throws Exception{
		eiborDAO.delete(eibor);
	}
	
	public AIOTechGenericDAO<Eibor> getEiborDAO() {
		return eiborDAO;
	}

	public void setEiborDAO(AIOTechGenericDAO<Eibor> eiborDAO) {
		this.eiborDAO = eiborDAO;
	}
}
