package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.ProductPackage;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.service.ProductPackageService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ProductPackageBL {

	private LookupMasterBL lookupMasterBL;
	private ProductPackageService productPackageService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<?> getAllProductPackagings() throws Exception {
		List<ProductPackage> productPackages = productPackageService
				.getAllProductPackagings(getImplementation());
		List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
		if (null != productPackages && productPackages.size() > 0) {
			for (ProductPackage productPackage : productPackages) {
				productPackageVOs.add(addProductPackaging(productPackage));
			}
		}

		return productPackageVOs;
	}

	private ProductPackageVO addProductPackaging(ProductPackage productPackage)
			throws Exception {
		ProductPackageVO productPackageVO = new ProductPackageVO();
		productPackageVO.setProductPackageId(productPackage
				.getProductPackageId());
		productPackageVO.setPackageName(productPackage.getPackageName());
		productPackageVO.setProductName(productPackage.getProduct()
				.getProductName());
		productPackageVO.setNetWeight(productPackage.getNetWeight());
		productPackageVO.setGrossWeight(productPackage.getGrossWeight());
		productPackageVO
				.setDemension(null != productPackage.getLength() ? (productPackage
						.getLength() + "*" + productPackage.getWidth() + "*" + productPackage
						.getHeight()) : "");
		productPackageVO.setUnitName(productPackage.getProduct()
				.getLookupDetailByProductUnit().getDisplayName());
		productPackageVO
				.setReferenceNumber(productPackage.getReferenceNumber());
		return productPackageVO;
	}

	public void saveProductPackage(ProductPackage productPackage,
			List<ProductPackageDetail> productPackageDetails,
			List<ProductPackageDetail> deletedPackageDetails) throws Exception {
		productPackage.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		boolean editFlag = false;
		if (productPackage.getProductPackageId() != null
				&& productPackage.getProductPackageId() > 0)
			editFlag = true;

		productPackage.setImplementation(getImplementation());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (editFlag) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		productPackage
				.setProductPackageDetails(new HashSet<ProductPackageDetail>(
						productPackageDetails));
		if (!editFlag)
			SystemBL.saveReferenceStamp(ProductPackage.class.getName(),
					getImplementation());
		if (null != deletedPackageDetails && deletedPackageDetails.size() > 0)
			productPackageService
					.deleteProductPackageDetail(deletedPackageDetails);
		productPackageService.saveProductPackage(productPackage,
				productPackageDetails);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductPackage.class.getSimpleName(),
				productPackage.getProductPackageId(), user, workflowDetailVo);
	}

	public void deleteProductPackage(ProductPackage productPackage)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductPackage.class.getSimpleName(),
				productPackage.getProductPackageId(), user, workflowDetailVo);
	}

	public void doProductPackageBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		ProductPackage productPackage = productPackageService
				.getProductPackagingbyId(recordId);
		List<ProductPackageDetail> productPackageDetails = new ArrayList<ProductPackageDetail>(
				productPackage.getProductPackageDetails());
		if (workflowDetailVO.isDeleteFlag()) {
			productPackageService.deleteProductProduction(productPackage,
					productPackageDetails);
		}
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProductPackageService getProductPackageService() {
		return productPackageService;
	}

	public void setProductPackageService(
			ProductPackageService productPackageService) {
		this.productPackageService = productPackageService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
