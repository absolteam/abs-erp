/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.SalesDeliveryCharge;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryPack;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class SalesDeliveryNoteService {

	private AIOTechGenericDAO<SalesDeliveryNote> salesDeliveryNoteDAO;
	private AIOTechGenericDAO<SalesDeliveryDetail> salesDeliveryDetailDAO;
	private AIOTechGenericDAO<SalesDeliveryCharge> salesDeliveryChargeDAO;
	private AIOTechGenericDAO<SalesDeliveryPack> salesDeliveryPackDAO;

	public List<SalesDeliveryDetail> getSalesDeliveryByProductId(Long productId) {

		return salesDeliveryDetailDAO.findByNamedQuery(
				"getSalesDeliveryByProductId", productId);
	}

	// Get all Delivery Notes
	public List<SalesDeliveryNote> getAllDeliveryNotes(
			Implementation implementation) throws Exception {
		return salesDeliveryNoteDAO.findByNamedQuery("getAllDeliveryNotes",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Sales Delivery by Sales Delivery Id
	public SalesDeliveryNote getSalesDeliveryNote(long salesDeliveryNoteId)
			throws Exception {
		return salesDeliveryNoteDAO.findByNamedQuery("getSalesDeliveryNote",
				salesDeliveryNoteId).get(0);
	}

	// Get Sales Delivery by Sales Delivery Id
	public SalesDeliveryNote getSalesDeliveryWithInvoiceDetails(
			long salesDeliveryNoteId) throws Exception {
		return salesDeliveryNoteDAO.findByNamedQuery(
				"getSalesDeliveryWithInvoiceDetails", salesDeliveryNoteId).get(
				0);
	}

	// Get Sales Delivery Detail by Sales Delivery Id
	public List<SalesDeliveryDetail> getSalesDeliveryDetailByDeliveryId(
			long salesDeliveryNoteId) throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getSalesDeliveryDetailByDeliveryId", salesDeliveryNoteId);
	}

	// Get Sales Delivery Detail by Sales Delivery Id
	public List<SalesDeliveryDetail> getSalesDeliveryDetailBySalesInvoice(
			long salesInvoiceId) throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getSalesDeliveryDetailBySalesInvoice", salesInvoiceId);
	}

	// Get Sales Delivery Detail by Sales Delivery Id
	public SalesDeliveryDetail getSalesDeliveryDetailByDetailId(
			long salesDeliveryDetailId) throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getSalesDeliveryDetailByDetailId", salesDeliveryDetailId).get(
				0);
	}

	// Get Sales Delivery Charges by Sales Delivery Id
	public List<SalesDeliveryCharge> getSalesDeliveryChargesByDeliveryId(
			long salesDeliveryNoteId) throws Exception {

		return salesDeliveryChargeDAO.findByNamedQuery(
				"getSalesDeliveryChargesByDeliveryId", salesDeliveryNoteId);
	}

	// Get Sales Delivery Packs by Sales Delivery Id
	public List<SalesDeliveryPack> getSalesDeliveryPackByDeliveryId(
			long salesDeliveryNoteId) throws Exception {

		return salesDeliveryPackDAO.findByNamedQuery(
				"getSalesDeliveryPackByDeliveryId", salesDeliveryNoteId);
	}

	// Get Sales Delivery Note and all details info
	public SalesDeliveryNote getSalesDeliveryNoteInfo(long salesDeliveryNoteId)
			throws Exception {
		return salesDeliveryNoteDAO.findByNamedQuery(
				"getSalesDeliveryNoteInfo", salesDeliveryNoteId).get(0);
	}

	// Get Sales Delivery Note and all details info
	public SalesDeliveryNote getSalesDeliveryNoteById(long salesDeliveryNoteId)
			throws Exception {
		return salesDeliveryNoteDAO.findById(salesDeliveryNoteId);
	}

	public SalesDeliveryNote getBasicSalesDeliveryNote(long salesDeliveryNoteId)
			throws Exception {
		return salesDeliveryNoteDAO.findByNamedQuery(
				"getBasicSalesDeliveryNote", salesDeliveryNoteId).get(0);
	}

	// Get Sales Delivery Detail by customer id
	public List<SalesDeliveryDetail> getCustomerActiveNotes(long customerId)
			throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getCustomerActiveNotes", customerId);
	}

	// Get Sales Delivery Detail by customer id
	public List<SalesDeliveryDetail> getNonInvoicedDeliveryNotes(
			long customerId, long currencyId) throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getNonInvoicedDeliveryNotes", customerId, currencyId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Sales Delivery Detail by customer id
	public List<SalesDeliveryDetail> getNonInvoicedDeliveryNotesEdit(
			long customerId, Long salesInvoiceId, long currencyId)
			throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getNonInvoicedDeliveryNotesEdit", salesInvoiceId, customerId,
				currencyId);
	}

	// Get Sales Delivery Detail by customer id
	public List<SalesDeliveryDetail> getCustomerNotes(long customerId)
			throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery("getCustomerNotes",
				customerId);
	}

	// Delete Sales Delivery Details
	public void deleteDeliveryDetails(
			List<SalesDeliveryDetail> deleteDeliveryDetails) throws Exception {
		salesDeliveryDetailDAO.deleteAll(deleteDeliveryDetails);
	}

	// Delete Sales Delivery Charges
	public void deleteDeliveryCharges(
			List<SalesDeliveryCharge> deleteDeliveryCharges) throws Exception {
		salesDeliveryChargeDAO.deleteAll(deleteDeliveryCharges);
	}

	// Delete Sales Delivery Packs
	public void deleteDeliveryPack(List<SalesDeliveryPack> deleteDeliveryPack)
			throws Exception {
		salesDeliveryPackDAO.deleteAll(deleteDeliveryPack);
	}

	// Save or Update Sales Delivery Note
	public void saveDeliveryNote(SalesDeliveryNote salesDeliveryNote,
			List<SalesDeliveryDetail> salesDeliveryDetail,
			List<SalesDeliveryCharge> salesDeliveryCharge,
			List<SalesDeliveryPack> salesDeliveryPack) throws Exception {
		salesDeliveryNoteDAO.saveOrUpdate(salesDeliveryNote);
		if (null != salesDeliveryPack && salesDeliveryPack.size() > 0) {
			for (SalesDeliveryPack deliveryPack : salesDeliveryPack) {
				deliveryPack.setSalesDeliveryNote(salesDeliveryNote);
			}
			salesDeliveryPackDAO.saveOrUpdateAll(salesDeliveryPack);
		}
		if (null != salesDeliveryCharge && salesDeliveryCharge.size() > 0) {
			for (SalesDeliveryCharge deliveryCharge : salesDeliveryCharge) {
				deliveryCharge.setSalesDeliveryNote(salesDeliveryNote);
			}
			salesDeliveryChargeDAO.saveOrUpdateAll(salesDeliveryCharge);
		}
		for (SalesDeliveryDetail deliveryDetail : salesDeliveryDetail) {
			deliveryDetail.setSalesDeliveryNote(salesDeliveryNote);
		}
		salesDeliveryDetailDAO.saveOrUpdateAll(salesDeliveryDetail);
	}

	// Save Delivery Notes
	public void saveSalesDeliveryNotes(
			List<SalesDeliveryNote> salesDeliveryNotes) throws Exception {
		salesDeliveryNoteDAO.saveOrUpdateAll(salesDeliveryNotes);
	}

	// Delete Sales Delivery Note
	public void deleteSalesDeliveryNote(SalesDeliveryNote salesDeliveryNote,
			List<SalesDeliveryDetail> salesDeliveryDetail,
			List<SalesDeliveryCharge> salesDeliveryCharge,
			List<SalesDeliveryPack> salesDeliveryPack) throws Exception {
		salesDeliveryPackDAO.deleteAll(salesDeliveryPack);
		salesDeliveryChargeDAO.deleteAll(salesDeliveryCharge);
		salesDeliveryDetailDAO.deleteAll(salesDeliveryDetail);
		salesDeliveryNoteDAO.delete(salesDeliveryNote);
	}

	public List<SalesDeliveryNote> getSalesDeliveryBySalesOrder(
			Long salesOrderId) throws Exception {
		return salesDeliveryNoteDAO.findByNamedQuery(
				"getSalesDeliveryBySalesOrder", salesOrderId);
	}

	public List<SalesDeliveryDetail> getSalesDeliveryDetailByOrderDetail(
			Long salesOrderDetailId) throws Exception {
		return salesDeliveryDetailDAO.findByNamedQuery(
				"getSalesDeliveryDetailByOrderDetail", salesOrderDetailId);
	}

	@SuppressWarnings("unchecked")
	public List<SalesDeliveryNote> getFilteredDeliveryNotes(
			Implementation implementation, Long salesDeliveryNoteId,
			Long customerId, Byte status, Date fromDate, Date toDate,
			Long storeId) throws Exception {
		Criteria criteria = salesDeliveryNoteDAO.createCriteria();

		criteria.createAlias("customer", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personBySalesPersonId", "s_pr",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("lookupDetailByShippingTerm", "s_term",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByShippingMethod", "s_mthd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.shippingDetails", "ship_dtl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesDeliveryDetails", "sdd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sdd.productPackageDetail", "packDetail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("packDetail.lookupDetail", "unitld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sdd.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prd.lookupDetailByProductUnit", "prdUnit",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sdd.shelf", "slf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("slf.shelf", "rack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.aisle", "ae",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ae.store", "str", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("salesDeliveryCharges", "charges",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("charges.lookupDetail", "ch_types",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("salesDeliveryPacks", "packs",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("implementation", implementation));

		if (salesDeliveryNoteId != null && salesDeliveryNoteId > 0)
			criteria.add(Restrictions.eq("salesDeliveryNoteId",
					salesDeliveryNoteId));

		if (customerId != null && customerId > 0)
			criteria.add(Restrictions.eq("c.customerId", customerId));

		if (storeId != null && storeId > 0)
			criteria.add(Restrictions.eq("str.storeId", storeId));

		if (status != null && status > 0)
			criteria.add(Restrictions.eq("status", status));

		if (fromDate != null)
			criteria.add(Restrictions.ge("deliveryDate", fromDate));

		if (toDate != null)
			criteria.add(Restrictions.le("deliveryDate", toDate));

		Set<SalesDeliveryNote> uniqueRecord = new HashSet<SalesDeliveryNote>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<SalesDeliveryNote>(
				uniqueRecord) : null;
	}

	public List<SalesDeliveryNote> geCustomerSalesComparision(
			Set<Long> products, Set<Long> customers, String fromDate,
			String toDate) throws Exception {
		Criteria criteria = salesDeliveryNoteDAO.createCriteria();
		criteria.createAlias("salesDeliveryDetails", "sd", Criteria.LEFT_JOIN);
		criteria.createAlias("customer", "c", Criteria.LEFT_JOIN);
		criteria.createAlias("c.personByPersonId", "pr", Criteria.LEFT_JOIN);
		criteria.createAlias("c.cmpDeptLocation", "cpl", Criteria.LEFT_JOIN);
		criteria.createAlias("cpl.company", "cy", Criteria.LEFT_JOIN);
		criteria.createAlias("c.company", "cpy", Criteria.LEFT_JOIN);
		criteria.createAlias("sd.product", "prd", Criteria.LEFT_JOIN);
		if (null != customers && customers.size() > 0)
			criteria.add(Restrictions.in("c.customerId", customers));
		if (null != products && products.size() > 0)
			criteria.add(Restrictions.in("prd.productId", products));
		if (null != fromDate && !("").equals(fromDate))
			criteria.add(Restrictions.ge("date",
					DateFormat.convertStringToDate(fromDate)));
		if (null != toDate && !("").equals(toDate))
			criteria.add(Restrictions.le("date",
					DateFormat.convertStringToDate(toDate)));
		@SuppressWarnings("unchecked")
		Set<SalesDeliveryNote> uniqueRecords = new HashSet<SalesDeliveryNote>(
				criteria.list());
		List<SalesDeliveryNote> rts = new ArrayList<SalesDeliveryNote>(
				uniqueRecords);
		return rts;
	}

	public SalesDeliveryNote getSalesDeliveryByProject(Long projectId)
			throws Exception {
		List<SalesDeliveryNote> notes = salesDeliveryNoteDAO.findByNamedQuery(
				"getSalesDeliveryByProject", projectId);
		if (notes != null && notes.size() > 0)
			return notes.get(0);
		else
			return null;
	}

	// Getters and Setters
	public AIOTechGenericDAO<SalesDeliveryNote> getSalesDeliveryNoteDAO() {
		return salesDeliveryNoteDAO;
	}

	public void setSalesDeliveryNoteDAO(
			AIOTechGenericDAO<SalesDeliveryNote> salesDeliveryNoteDAO) {
		this.salesDeliveryNoteDAO = salesDeliveryNoteDAO;
	}

	public AIOTechGenericDAO<SalesDeliveryDetail> getSalesDeliveryDetailDAO() {
		return salesDeliveryDetailDAO;
	}

	public void setSalesDeliveryDetailDAO(
			AIOTechGenericDAO<SalesDeliveryDetail> salesDeliveryDetailDAO) {
		this.salesDeliveryDetailDAO = salesDeliveryDetailDAO;
	}

	public AIOTechGenericDAO<SalesDeliveryCharge> getSalesDeliveryChargeDAO() {
		return salesDeliveryChargeDAO;
	}

	public void setSalesDeliveryChargeDAO(
			AIOTechGenericDAO<SalesDeliveryCharge> salesDeliveryChargeDAO) {
		this.salesDeliveryChargeDAO = salesDeliveryChargeDAO;
	}

	public AIOTechGenericDAO<SalesDeliveryPack> getSalesDeliveryPackDAO() {
		return salesDeliveryPackDAO;
	}

	public void setSalesDeliveryPackDAO(
			AIOTechGenericDAO<SalesDeliveryPack> salesDeliveryPackDAO) {
		this.salesDeliveryPackDAO = salesDeliveryPackDAO;
	}
}
