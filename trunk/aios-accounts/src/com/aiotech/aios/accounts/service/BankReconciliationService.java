/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankReconciliation;
import com.aiotech.aios.accounts.domain.entity.BankReconciliationDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class BankReconciliationService {

	private AIOTechGenericDAO<BankReconciliation> bankReconciliationDAO;
	private AIOTechGenericDAO<BankReconciliationDetail> bankReconciliationDetailDAO;

	// Get all Bank Reconciliation
	public List<BankReconciliation> getAllBankReconciliations(
			Implementation implementation) throws Exception {
		return bankReconciliationDAO.findByNamedQuery(
				"getAllBankReconciliations", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<BankReconciliation> getBankReconciliationByBankAccount(
			long bankAccountId) throws Exception {
		return bankReconciliationDAO.findByNamedQuery(
				"getBankReconciliationByBankAccount", bankAccountId);
	}

	// Get Bank Reconciliation
	public BankReconciliation getBankReconciliation(long reconciliationId)
			throws Exception {
		return bankReconciliationDAO.findByNamedQuery("getBankReconciliation",
				reconciliationId).get(0);
	}

	public List<BankReconciliationDetail> getBankReconDetailsWithoutTrans(
			Implementation implementation) throws Exception {
		return bankReconciliationDetailDAO.findByNamedQuery(
				"getBankReconDetailsWithoutTrans", implementation,
				implementation);
	}

	// Get Bank Reconciliation by reconcilied dates
	public List<BankReconciliation> getReconciliedTransaction(
			Date reconciliationDate, Date statementEndDate,
			Implementation implementation) throws Exception {
		return bankReconciliationDAO.findByNamedQuery(
				"getReconciliedTransaction", implementation,
				reconciliationDate, statementEndDate);
	}

	// Save Bank Reconciliation
	public void saveBankReconciliation(BankReconciliation bankReconciliation,
			List<BankReconciliationDetail> bankReconciliationDetails) {
		bankReconciliationDAO.saveOrUpdate(bankReconciliation);
		for (BankReconciliationDetail bankReconciliationDetail : bankReconciliationDetails) {
			bankReconciliationDetail.setBankReconciliation(bankReconciliation);
		}
		bankReconciliationDetailDAO.saveOrUpdateAll(bankReconciliationDetails);
	}

	public List<BankReconciliationDetail> getAllBankReconciliationDetails()
			throws Exception {
		return bankReconciliationDetailDAO.getAll();
	}

	// Save Bank Reconciliation
	public void saveBankReconciliationDetail(
			List<BankReconciliationDetail> bankReconciliationDetails) {
		bankReconciliationDetailDAO.saveOrUpdateAll(bankReconciliationDetails);
	}

	// Delete Bank Reconciliation
	public void deleteBankReconciliation(BankReconciliation bankReconciliation,
			List<BankReconciliationDetail> bankReconciliationDetail)
			throws Exception {
		bankReconciliationDetailDAO.deleteAll(bankReconciliationDetail);
		bankReconciliationDAO.delete(bankReconciliation);
	}

	public void deleteBankReconciliationDetail(
			List<BankReconciliationDetail> bankReconciliationDetail)
			throws Exception {
		bankReconciliationDetailDAO.deleteAll(bankReconciliationDetail);
	}

	// Getters and Setters
	public AIOTechGenericDAO<BankReconciliation> getBankReconciliationDAO() {
		return bankReconciliationDAO;
	}

	public void setBankReconciliationDAO(
			AIOTechGenericDAO<BankReconciliation> bankReconciliationDAO) {
		this.bankReconciliationDAO = bankReconciliationDAO;
	}

	public AIOTechGenericDAO<BankReconciliationDetail> getBankReconciliationDetailDAO() {
		return bankReconciliationDetailDAO;
	}

	public void setBankReconciliationDetailDAO(
			AIOTechGenericDAO<BankReconciliationDetail> bankReconciliationDetailDAO) {
		this.bankReconciliationDetailDAO = bankReconciliationDetailDAO;
	}
}
