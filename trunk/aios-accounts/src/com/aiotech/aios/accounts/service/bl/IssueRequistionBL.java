/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueReturnDetailVO;
import com.aiotech.aios.accounts.service.IssueRequistionService;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * @author Saleem
 */
public class IssueRequistionBL {

	// Dependencies
	private IssueRequistionService issueRequistionService;
	private TransactionBL transactionBL;
	private ProductBL productBL;
	private LookupMasterBL lookupMasterBL;
	private StockBL stockBL;
	private RequisitionBL requisitionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get all Issue Requisitions
	public JSONObject getAllIssueRequistions() throws Exception {
		JSONObject jsonResponse = new JSONObject();
		List<IssueRequistion> issueRequistions = issueRequistionService
				.getAllIssueRequistions(getImplementation());
		JSONArray data = new JSONArray();
		JSONArray array = null;
		boolean editFlag = true;
		for (IssueRequistion issueRequistion : issueRequistions) {
			editFlag = true;
			for (IssueRequistionDetail reqDetail : issueRequistion
					.getIssueRequistionDetails()) {
				if (null != reqDetail.getIssueReturnDetails()
						&& reqDetail.getIssueReturnDetails().size() > 0) {
					editFlag = false;
					break;
				}
			}
			array = new JSONArray();
			array.add(issueRequistion.getIssueRequistionId());
			array.add(issueRequistion.getReferenceNumber());
			array.add(DateFormat.convertDateToString(issueRequistion
					.getIssueDate().toString()));
			if (null != issueRequistion.getRequisition())
				array.add(issueRequistion.getRequisition().getReferenceNumber());
			else
				array.add("");
			array.add(issueRequistion.getCmpDeptLocation().getDepartment()
					.getDepartmentName());
			array.add(issueRequistion.getPerson().getFirstName().concat(" ")
					.concat(issueRequistion.getPerson().getLastName()));
			array.add(editFlag);
			if (issueRequistion.getCustomer() != null) {
				array.add(null != issueRequistion.getCustomer()
						.getPersonByPersonId() ? issueRequistion
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat("")
						.concat(issueRequistion.getCustomer()
								.getPersonByPersonId().getLastName())
						: (null != issueRequistion.getCustomer().getCompany() ? issueRequistion
								.getCustomer().getCompany().getCompanyName()
								: issueRequistion.getCustomer()
										.getCmpDeptLocation().getCompany()
										.getCompanyName()));
			} else {
				array.add("");
			}

			array.add(issueRequistion.getOtherReference());
			array.add(issueRequistion.getDescription());
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// Save Issue Requisition
	public boolean saveIssueRequisition(IssueRequistion issueRequistion,
			List<IssueRequistionDetail> issueRequistionDetails,
			List<IssueRequistionDetail> reverseRequisitionDetails,
			List<IssueRequistionDetail> deleteRequisitionDetails,
			Requisition requisition) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		boolean updateFlag = false;
		if (null != issueRequistion.getIssueRequistionId())
			updateFlag = true;
		Stock stock = null;
		if (validateIssueRequisition(issueRequistionDetails)) {
			if (null != requisition) {
				requisitionBL.getRequisitionService().saveRequisition(
						requisition);
			}
			issueRequistion
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (issueRequistion != null
					&& issueRequistion.getIssueRequistionId() != null
					&& issueRequistion.getIssueRequistionId() > 0) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			issueRequistion.setImplementation(getImplementation());
			if (updateFlag) {
				List<Stock> addStocks = new ArrayList<Stock>();
				if (null != deleteRequisitionDetails
						&& deleteRequisitionDetails.size() > 0)
					issueRequistionService
							.deleteIssueRequisitionDetail(deleteRequisitionDetails);
				transactionBL.deleteTransaction(
						issueRequistion.getIssueRequistionId(),
						IssueRequistion.class.getSimpleName());
				for (IssueRequistionDetail reverseStock : reverseRequisitionDetails) {
					stock = new Stock();
					if (null != reverseStock.getShelf()) {
						Shelf shelf = stockBL
								.getStoreBL()
								.getStoreService()
								.getStoreByShelfId(
										reverseStock.getShelf().getShelfId());
						stock.setStore(shelf.getShelf().getAisle().getStore());
						stock.setShelf(reverseStock.getShelf());
					} 
					stock.setProduct(reverseStock.getProduct());
					stock.setQuantity(reverseStock.getQuantity()); 
					stock.setBatchNumber(reverseStock.getBatchNumber());
					stock.setProductExpiry(reverseStock.getProductExpiry());
					addStocks.add(stock);
				}
				stockBL.updateBatchStockDetails(addStocks);
			} else
				SystemBL.saveReferenceStamp(IssueRequistion.class.getName(),
						getImplementation());
			issueRequistion
					.setIssueRequistionDetails(new HashSet<IssueRequistionDetail>(
							issueRequistionDetails));
			issueRequistionService.saveIssueRequisition(issueRequistion,
					issueRequistionDetails);
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					IssueRequistion.class.getSimpleName(),
					issueRequistion.getIssueRequistionId(), user,
					workflowDetailVo);
			return true;
		}
		return false;
	}

	public void doIssueRequistionBusinessUpdate(Long issueRequistionId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		IssueRequistion issueRequistion = issueRequistionService
				.getIssueRequistionAndDetails(issueRequistionId);
		if (!workflowDetailVO.isDeleteFlag()) {
			Transaction transaction = issueRequisitionTransaction(
					issueRequistion,
					"Issue Material Ref: "
							+ issueRequistion.getReferenceNumber(),
					new ArrayList<IssueRequistionDetail>(issueRequistion
							.getIssueRequistionDetails()), true, false);
			transactionBL.saveTransaction(
					transaction,
					new ArrayList<TransactionDetail>(transaction
							.getTransactionDetails()));

			// Update stock information
			List<Stock> deleteStocks = new ArrayList<Stock>();
			Stock stock = null;
			for (IssueRequistionDetail issueRequistionDetail : issueRequistion
					.getIssueRequistionDetails()) {
				stock = new Stock();
				if (null != issueRequistionDetail.getShelf()) {
					Shelf shelf = stockBL
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									issueRequistionDetail.getShelf()
											.getShelfId());
					stock.setStore(shelf.getShelf().getAisle().getStore());
					stock.setShelf(issueRequistionDetail.getShelf());
				}
				stock.setProduct(issueRequistionDetail.getProduct());
				stock.setQuantity(issueRequistionDetail.getQuantity());
				stock.setImplementation(getImplementation());
				stock.setBatchNumber(issueRequistionDetail.getBatchNumber());
				stock.setProductExpiry(issueRequistionDetail.getProductExpiry());
				deleteStocks.add(stock);
			}
			stockBL.deleteBatchStockDetails(deleteStocks);
		} else {
			transactionBL.deleteTransaction(
					issueRequistion.getIssueRequistionId(),
					IssueRequistion.class.getSimpleName());
			List<Stock> stocks = new ArrayList<Stock>();
			Stock tempStock = null;
			for (IssueRequistionDetail issues : issueRequistion
					.getIssueRequistionDetails()) {
				tempStock = new Stock();
				tempStock.setProduct(issues.getProduct());
				if (null != issues.getShelf()) {
					tempStock.setStore(issues.getShelf().getShelf().getAisle()
							.getStore());
					tempStock.setShelf(issues.getShelf());
				}
				tempStock.setQuantity(issues.getQuantity());
				tempStock.setBatchNumber(issues.getBatchNumber());
				tempStock.setProductExpiry(issues.getProductExpiry());
				tempStock.setShelf(issues.getShelf());
				tempStock.setUnitRate(issues.getUnitRate());
				tempStock.setImplementation(getImplementation());
				stocks.add(tempStock);
			}
			stockBL.updateBatchStockDetails(stocks);
			if (null != issueRequistion.getRequisition()) {
				Requisition requisition = issueRequistion.getRequisition();
				requisition.setStatus(RequisitionProcessStatus.Open.getCode());
				requisitionBL.getRequisitionService().saveRequisition(
						requisition);
			}
			issueRequistionService.deleteIssueRequisition(
					issueRequistion,
					new ArrayList<IssueRequistionDetail>(issueRequistion
							.getIssueRequistionDetails()));
		}
	}

	// Validate Issue Requisition
	private boolean validateIssueRequisition(
			List<IssueRequistionDetail> issueRequistionDetails)
			throws Exception {
		Map<String, IssueRequistionDetail> hashRequistionDetail = new HashMap<String, IssueRequistionDetail>();
		for (IssueRequistionDetail issueRequistionDetail : issueRequistionDetails) {
			hashRequistionDetail
					.put(issueRequistionDetail
							.getProduct()
							.getProductId()
							.toString()
							.concat("-")
							.concat((null != issueRequistionDetail.getShelf() ? issueRequistionDetail
									.getShelf().getShelfId().toString()
									: ""))
							.concat((issueRequistionDetail.getLookupDetail() != null ? ""
									+ issueRequistionDetail.getLookupDetail()
											.getLookupDetailId()
									: "")), issueRequistionDetail);
		}
		if (issueRequistionDetails.size() > hashRequistionDetail.size()) {
			return false;
		}
		return true;
	}

	// Delete Issue Requisition
	public void deleteIssueRequisition(IssueRequistion issueRequistion,
			List<IssueRequistionDetail> issueRequistionDetails)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IssueRequistion.class.getSimpleName(),
				issueRequistion.getIssueRequistionId(), user, workflowDetailVo);
	}

	// Show Issue Requisition
	public List<IssueRequistionVO> validateIssueRequistionDetail(
			List<IssueRequistionDetail> issueRequistionDetails)
			throws Exception {
		IssueRequistionVO issueRequistionVO = null;
		IssueRequistionDetailVO issueRequistionDetailVO = null;
		Map<Long, IssueRequistionVO> issueRequistionVOHash = new HashMap<Long, IssueRequistionVO>();
		Map<Long, List<IssueRequistionDetailVO>> issueRequistionDetailVOHash = new HashMap<Long, List<IssueRequistionDetailVO>>();
		List<IssueRequistionDetailVO> requistionDetailsVOs = new ArrayList<IssueRequistionDetailVO>();
		IssueReturnDetailVO issueReturnDetailVO = null;
		Map<Long, List<IssueReturnDetailVO>> issueReturnVOHash = new HashMap<Long, List<IssueReturnDetailVO>>();
		List<IssueReturnDetailVO> issueReturnDetailVOList = null;

		for (IssueRequistionDetail issueRequistionDetail : issueRequistionDetails) {
			issueRequistionVO = new IssueRequistionVO();
			issueRequistionDetailVO = new IssueRequistionDetailVO();
			issueRequistionVO.setIssueRequistionId(issueRequistionDetail
					.getIssueRequistion().getIssueRequistionId());
			issueRequistionVO.setReferenceNumber(issueRequistionDetail
					.getIssueRequistion().getReferenceNumber());
			issueRequistionVO.setIssueDate(issueRequistionDetail
					.getIssueRequistion().getIssueDate());
			issueRequistionVO.setCmpDeptLocation(issueRequistionDetail
					.getIssueRequistion().getCmpDeptLocation());
			issueRequistionVO.setDescription(issueRequistionDetail
					.getIssueRequistion().getDescription());
			issueRequistionDetailVO
					.setIssueRequistionDetailId(issueRequistionDetail
							.getIssueRequistionDetailId());
			issueRequistionDetailVO.setProduct(issueRequistionDetail
					.getProduct());
			issueRequistionDetailVO.setQuantity(issueRequistionDetail
					.getQuantity());
			issueRequistionDetailVO.setUnitRate(issueRequistionDetail
					.getUnitRate());
			issueRequistionDetailVO.setDescription(issueRequistionDetail
					.getDescription());
			issueRequistionDetailVO.setReturnQty(issueRequistionDetail
					.getQuantity());
			issueRequistionDetailVO.setIssueReturnDetails(issueRequistionDetail
					.getIssueReturnDetails());
			requistionDetailsVOs.add(issueRequistionDetailVO);
			List<IssueRequistionDetailVO> requistionDetailVOList = issueRequistionDetailVOHash
					.get(issueRequistionVO.getIssueRequistionId());

			if (null != requistionDetailVOList
					&& requistionDetailVOList.size() > 0) {
				requistionDetailVOList.add(issueRequistionDetailVO);
				issueRequistionDetailVOHash.put(
						issueRequistionVO.getIssueRequistionId(),
						requistionDetailVOList);
			} else {
				requistionDetailVOList = new ArrayList<IssueRequistionDetailVO>();
				requistionDetailVOList.add(issueRequistionDetailVO);
				issueRequistionDetailVOHash.put(
						issueRequistionVO.getIssueRequistionId(),
						requistionDetailVOList);
			}
			issueReturnDetailVOList = new ArrayList<IssueReturnDetailVO>();
			for (IssueReturnDetail returnDetail : issueRequistionDetail
					.getIssueReturnDetails()) {
				issueReturnDetailVO = new IssueReturnDetailVO();
				issueReturnDetailVO.setIssueReturnDetailId(returnDetail
						.getIssueReturnDetailId());
				issueReturnDetailVO.setReturnQuantity(returnDetail
						.getReturnQuantity());
				issueReturnDetailVO.setDescription(returnDetail
						.getDescription());
				issueReturnDetailVOList.add(issueReturnDetailVO);
			}
			if (issueReturnVOHash.containsKey(issueRequistionDetail
					.getIssueRequistionDetailId())) {
				issueReturnDetailVOList
						.addAll(issueReturnVOHash.get(issueRequistionDetail
								.getIssueRequistionDetailId()));
			}
			issueReturnVOHash.put(
					issueRequistionDetail.getIssueRequistionDetailId(),
					issueReturnDetailVOList);
			issueRequistionVOHash.put(issueRequistionVO.getIssueRequistionId(),
					issueRequistionVO);
		}
		Map<Long, IssueRequistionVO> issueVOReturns = new HashMap<Long, IssueRequistionVO>();
		for (Entry<Long, List<IssueRequistionDetailVO>> issueId : issueRequistionDetailVOHash
				.entrySet()) {
			List<IssueRequistionDetailVO> issueDetailVOList = issueRequistionDetailVOHash
					.get(issueId.getKey());
			issueReturnDetailVOList = new ArrayList<IssueReturnDetailVO>();
			for (IssueRequistionDetailVO detailVO : issueDetailVOList) {
				if (issueReturnVOHash.containsKey(detailVO
						.getIssueRequistionDetailId())) {
					List<IssueReturnDetailVO> returnDetails = issueReturnVOHash
							.get(detailVO.getIssueRequistionDetailId());
					detailVO.setIssuedQty(detailVO.getQuantity()
							+ (null != detailVO.getIssuedQty() ? detailVO
									.getIssuedQty() : 0));
					detailVO.setTotalRate(detailVO.getIssuedQty()
							* detailVO.getUnitRate());
					detailVO.setDescription(detailVO.getDescription());
					for (IssueReturnDetailVO returnDetail : returnDetails) {
						detailVO.setReturnDetailId(returnDetail
								.getIssueReturnDetailId());
						detailVO.setReturnedQty(returnDetail
								.getReturnQuantity()
								+ (null != detailVO.getReturnedQty() ? detailVO
										.getReturnedQty() : 0));
						detailVO.setReturnQty(detailVO.getQuantity()
								- detailVO.getReturnedQty());
					}
					if (issueReturnVOHash.containsKey(detailVO
							.getIssueRequistionDetailId())) {
						issueReturnDetailVOList.addAll(issueReturnVOHash
								.get(detailVO.getIssueRequistionDetailId()));
					}
					issueReturnVOHash.put(
							detailVO.getIssueRequistionDetailId(),
							returnDetails);
				}
			}
			if (issueRequistionVOHash.containsKey(issueId.getKey())) {
				issueRequistionVO = issueRequistionVOHash.get(issueId.getKey());
				issueRequistionVO.setIssueRequistionDetailVO(issueDetailVOList);
				boolean addFlag = true;
				for (IssueRequistionDetailVO issueDetail : issueRequistionVO
						.getIssueRequistionDetailVO()) {
					if (null == issueDetail.getReturnedQty()
							|| (issueDetail.getReturnedQty() < issueDetail
									.getIssuedQty())) {
						addFlag = true;
						issueRequistionVO
								.setTotalIssueQty(issueDetail.getQuantity()
										+ (null != issueRequistionVO
												.getTotalIssueQty() ? issueRequistionVO
												.getTotalIssueQty() : 0));
						issueRequistionVO
								.setTotalReceiveAmount(issueDetail
										.getQuantity()
										* issueDetail.getUnitRate()
										+ (null != issueRequistionVO
												.getTotalReceiveAmount() ? issueRequistionVO
												.getTotalReceiveAmount() : 0));
					} else
						addFlag = false;
				}
				if (addFlag) {
					issueVOReturns.put(issueId.getKey(), issueRequistionVO);
					issueRequistionVOHash.put(issueId.getKey(),
							issueRequistionVO);
				}
			}
		}
		return new ArrayList<IssueRequistionVO>(issueVOReturns.values());
	}

	// Create Issue Requisition Transaction
	private Transaction issueRequisitionTransaction(
			IssueRequistion issueRequisition, String description,
			List<IssueRequistionDetail> issueRequistionDetails,
			boolean debitFlag, boolean creditFlag) throws Exception {

		double transactionAmount = 0;
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		for (IssueRequistionDetail issueRequistionDetail : issueRequistionDetails) {
			transactionAmount += (issueRequistionDetail.getUnitRate() * issueRequistionDetail
					.getQuantity());
			transactionDetails.add(createCreditEntry(issueRequisition,
					issueRequistionDetail, creditFlag));
			transactionDetails.add(createDebitEntry(issueRequisition,
					issueRequistionDetail, debitFlag));
		}
		Transaction transaction = createTransaction(issueRequisition,
				description, transactionAmount);
		transaction.setTransactionDetails(transactionDetails);
		return transaction;
	}

	private TransactionDetail createDebitEntry(IssueRequistion issueRequistion,
			IssueRequistionDetail issueRequistionDetail, boolean debitFlag)
			throws Exception {
		Combination combination = new Combination();
		combination.setCombinationId(getImplementation().getExpenseAccount());
		return transactionBL.createTransactionDetail((issueRequistionDetail
				.getUnitRate() * issueRequistionDetail.getQuantity()),
				combination.getCombinationId(), debitFlag, null, null,
				issueRequistion.getReferenceNumber());
	}

	// Create TransactionEntry
	private Transaction createTransaction(IssueRequistion issueRequisition,
			String description, double transactionAmount) throws Exception {
		return transactionBL.createTransaction(transactionBL.getCurrency()
				.getCurrencyId(), description, issueRequisition.getIssueDate(),
				transactionBL.getCategory("Issue Material"), issueRequisition
						.getIssueRequistionId(), IssueRequistion.class
						.getSimpleName());
	}

	// Create Transaction Detail Entry
	private TransactionDetail createCreditEntry(
			IssueRequistion issueRequistion,
			IssueRequistionDetail issueRequistionDetail, boolean creditFlag)
			throws Exception {
		Combination combination = productBL
				.getProductInventory(issueRequistionDetail.getProduct()
						.getProductId());
		return transactionBL.createTransactionDetail((issueRequistionDetail
				.getUnitRate() * issueRequistionDetail.getQuantity()),
				combination.getCombinationId(), creditFlag, null, null,
				issueRequistion.getReferenceNumber());
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters and Setters
	public IssueRequistionService getIssueRequistionService() {
		return issueRequistionService;
	}

	public void setIssueRequistionService(
			IssueRequistionService issueRequistionService) {
		this.issueRequistionService = issueRequistionService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public RequisitionBL getRequisitionBL() {
		return requisitionBL;
	}

	public void setRequisitionBL(RequisitionBL requisitionBL) {
		this.requisitionBL = requisitionBL;
	}
}
