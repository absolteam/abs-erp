package com.aiotech.aios.accounts.service.bl;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Eibor;
import com.aiotech.aios.accounts.service.BankService;
import com.aiotech.aios.accounts.service.EiborService; 
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.system.domain.entity.Implementation;

public class EiborBL {

	private EiborService eiborService;
	private BankService bankService;
	private GLCombinationService combinationService;
	
	List<Eibor> eiborList=null;
	
	public List<Eibor> getEiborList(Implementation implementation) throws Exception{
		return eiborList=getEiborService().getEiborList(implementation);
	}
	
	public EiborService getEiborService() {
		return eiborService;
	}

	public void setEiborService(EiborService eiborService) {
		this.eiborService = eiborService;
	}

	public void setEiborList(List<Eibor> eiborList) {
		this.eiborList = eiborList;
	}

	public BankService getBankService() {
		return bankService;
	}

	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}
}
