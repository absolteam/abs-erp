package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class GLChartOfAccountService {
	private AIOTechGenericDAO<Account> accountDAO;
	private AIOTechGenericDAO<Segment> segmentDAO;

	public int getAccountCount() throws Exception {
		Query query = accountDAO.getNamedQuery("getAccountsCount");
		if (query.list().iterator().next() != null) {
			return ((Long) query.list().iterator().next()).intValue();
		} else {
			return 0;
		}
	}

	public void saveAccounts(List<Account> accountList) throws Exception {
		accountDAO.saveOrUpdateAll(accountList);
	}

	public List<Account> getAllAccounts(Implementation implementation)
			throws Exception {
		return accountDAO.findByNamedQuery("getAllAccounts", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Account> getAllAccounts() throws Exception {
		return accountDAO.findByNamedQuery("getAccountsWithOutImplementation");
	}

	public Account getAccountDetails(String accountName,
			Implementation implementation) throws Exception {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getAccountsByAccount", accountName, implementation);
		return null != accounts && accounts.size() > 0 ? accounts.get(0) : null;
	}

	public List<Account> getAccountByAccountType(int accountType,
			Implementation implementation) throws Exception {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getAccountsByAccountType", implementation, accountType);
		return accounts;
	}

	public Account getLastUpdatedAccount(Implementation implementation)
			throws Exception {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getLastUpdatedAccount", implementation, implementation);
		return null != accounts && accounts.size() > 0 ? accounts.get(0) : null;
	}

	public List<Account> getCOAAccountByAccountType(int accountType,
			Implementation implementation) throws Exception {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getCOAAccountsByAccountType", implementation, accountType);
		return accounts;
	}

	public Account getAccountsByCode(String code, Implementation implemenatation) {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getAccountsByCode", code, implemenatation);
		return null != accounts && accounts.size() > 0 ? accounts.get(0) : null;
	}

	public List<Account> validateAccountDetails(String code,
			Implementation implementation, Long segmentId) throws Exception {
		return accountDAO.findByNamedQuery("validateAccountDetails",
				implementation, code, segmentId);
	}

	public List<Segment> getAllSegment() throws Exception {
		List<Segment> segment = segmentDAO.getAll();
		return segment;
	}

	public List<Account> getAllAccountCode(Implementation implementation,
			Long segmentId) throws Exception {
		return accountDAO.findByNamedQuery("getAllAccountCodes",
				implementation, segmentId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Account> getAccountsBySegmentId(Implementation implementation,
			long segmentId) throws Exception {
		return accountDAO.findByNamedQuery("getAccountsBySegmentId",
				implementation, segmentId);
	}

	public List<Account> getAllAccountCode(Implementation implementation,
			Integer accessId) throws Exception {
		return accountDAO.findByNamedQuery("getAllAccountCodes",
				implementation, accessId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Account> getFilterAccounts(Implementation implementation,
			int accountTypeId) throws Exception {
		return accountDAO.findByNamedQuery("getFilterAccounts", implementation,
				accountTypeId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public Account getAccountDetails(long accountId) throws Exception {
		return accountDAO.findById(accountId);
	}

	public Account getAccountByAccountId(long accountId) throws Exception {
		return accountDAO.findByNamedQuery("getAccountByAccountId", accountId)
				.get(0);
	}

	public Account getAccountByImplementationAndCode(
			Implementation implementation, String code) throws Exception {
		return accountDAO.findByNamedQuery("getAccountByImplementationAndCode",
				implementation, code).get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Account> getAccountBySegmentIds(Set<Long> segmentIds,
			int accountType) throws Exception {
		Criteria criteria = accountDAO.createCriteria();
		criteria.createAlias("segment", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.and(Restrictions.isNotNull("isSystem"),
				Restrictions.eq("isSystem", false)));
		criteria.add(Restrictions.eq("isApprove",
				(byte) WorkflowConstants.Status.Published.getCode()));
		criteria.add(Restrictions.in("s.segmentId", segmentIds));
		if (accountType > 0)
			criteria.add(Restrictions.eq("accountType", accountType));
		Set<Account> uniqueRecord = new HashSet<Account>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Account>(
				uniqueRecord) : null;
	}

	public void updateAccountList(List<Account> accounts) throws Exception {
		accountDAO.saveOrUpdateAll(accounts);
	}

	public void saveAccount(Account account) throws Exception {
		accountDAO.saveOrUpdate(account);
	}

	public void updateAccount(Account account) throws Exception {
		accountDAO.saveOrUpdate(account);
	}

	public void deleteAccount(Account account) throws Exception {
		accountDAO.delete(account);
	}

	public void deleteAccounts(List<Account> accounts) throws Exception {
		accountDAO.deleteAll(accounts);
	}

	public Account getLastUpdatedAnalysis(Implementation implementation,
			Long segmentId) {
		return accountDAO.findByNamedQuery("getLastUpdatedAnalysis",
				implementation, segmentId, implementation, segmentId).get(0);
	}

	public Account getLastUpdatedCostCenter(Implementation implementation,
			Long segmentId) {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getLastUpdatedCostCenter", implementation, segmentId,
				implementation, segmentId);
		return null != accounts && accounts.size() > 0 ? accounts.get(0) : null;
	}

	public Account getLastUpdatedAccount(Implementation implementation,
			Long segmentId) {
		List<Account> accounts = accountDAO.findByNamedQuery(
				"getSegmentLastUpdatedAccount", implementation, segmentId,
				implementation, segmentId);
		return null != accounts && accounts.size() > 0 ? accounts.get(0) : null;
	}

	public Account getLastUpdatedNatural(Implementation implementation,
			Long segmentId) throws Exception {
		return accountDAO.findByNamedQuery("getLastUpdatedNatural",
				implementation, segmentId, implementation, segmentId).get(0);
	}

	public List<Segment> getDefaultSegments(Implementation implementation)
			throws Exception {
		return segmentDAO
				.findByNamedQuery("getDefaultSegments", implementation);
	}

	public List<Account> getAccountsBySegmentAccessId(
			Implementation implementation, Integer accessId) {
		return accountDAO.findByNamedQuery("getAccountsBySegmentAccessId",
				implementation, accessId);
	}

	// Getters and Setters
	public AIOTechGenericDAO<Segment> getSegmentDAO() {
		return segmentDAO;
	}

	public void setSegmentDAO(AIOTechGenericDAO<Segment> segmentDAO) {
		this.segmentDAO = segmentDAO;
	}

	public AIOTechGenericDAO<Account> getAccountDAO() {
		return accountDAO;
	}

	public void setAccountDAO(AIOTechGenericDAO<Account> accountDAO) {
		this.accountDAO = accountDAO;
	}
}
