package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductService {
	private AIOTechGenericDAO<Product> productDAO;

	// To get all product
	public List<Product> getAllProduct(Implementation implementation)
			throws Exception {
		List<Product> products = productDAO.findByNamedQuery("getAllProducts",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
		return products;
	}

	public List<Product> getOnlyProducts(Implementation implementation)
			throws Exception {
		List<Product> products = productDAO.findByNamedQuery("getOnlyProducts",
				implementation);
		return products;
	}

	// To get all product
	public List<Product> getAllProduct() throws Exception {
		List<Product> productList = new ArrayList<Product>();
		productList = productDAO.findByNamedQuery("getProductsWithoutImpl");
		return productList;
	}

	// To get all active products
	public List<Product> getActiveProduct(Implementation implementation)
			throws Exception {
		List<Product> productList = new ArrayList<Product>();
		productList = productDAO.findByNamedQuery("getActiveProduct",
				implementation);
		return productList;
	}

	// To get all active products
	public List<Product> showRequisitionProductDetails(
			Implementation implementation, byte subType1, byte subType2)
			throws Exception {
		List<Product> productList = new ArrayList<Product>();
		productList = productDAO.findByNamedQuery(
				"showRequisitionProductDetails", implementation,
				(byte) WorkflowConstants.Status.Published.getCode(), subType1,
				subType2);
		return productList;
	}

	// To get all active products
	public List<Product> getShortActiveProduct(Implementation implementation,
			byte itemSubType1, byte itemSubType2) throws Exception {
		List<Product> productList = new ArrayList<Product>();
		productList = productDAO.findByNamedQuery("getShortActiveProduct",
				implementation, itemSubType1, itemSubType2);
		return productList;
	}

	// To get all active products
	public List<Product> getActiveSalesProduct(Implementation implementation)
			throws Exception {
		List<Product> productList = new ArrayList<Product>();
		productList = productDAO.findByNamedQuery("getActiveSalesProduct",
				implementation);
		return productList;
	}

	// To get all active products
	public List<Product> getActivePurchaseProduct(
			Implementation implementation, Byte itemSubType) throws Exception {
		List<Product> productList = new ArrayList<Product>();
		productList = productDAO.findByNamedQuery("getActivePurchaseProduct",
				implementation, itemSubType);
		return productList;
	}

	// To get all product where status=true and with item nature
	public List<Product> getPurchaseProductCodeByType(
			Implementation implementation, char itemType, Byte itemSubType)
			throws Exception {
		return productDAO.findByNamedQuery("getPurchaseProductByType",
				implementation, itemType, itemSubType);
	}

	// To get all active products
	@SuppressWarnings("unchecked")
	public List<Product> getActiveSpecialProduct(Set<Long> productIds)
			throws Exception {
		Criteria productCriteria = productDAO.createCriteria();
		productCriteria.add(Restrictions.conjunction().add(
				Restrictions.in("productId", productIds)));
		return (List<Product>) productCriteria.list();
	}

	// To get all active products
	public List<Product> getActivePosSpecialProduct(
			Implementation implementation, Date fromDate, Date toDate)
			throws Exception {
		return productDAO.findByNamedQuery("getActivePosSpecialProduct",
				implementation, fromDate, toDate);
	}

	// To get all product where status=true and with item nature
	public List<Product> getActiveProduct(Implementation implementation,
			char itemNature) throws Exception {
		return productDAO.findByNamedQuery("getActiveProducts", implementation,
				itemNature);
	}

	// To get all product where status=true and with item nature
	public List<Product> getActiveProduct(Implementation implementation,
			char itemNature, Date fromDate) throws Exception {
		return productDAO.findByNamedQuery("getActiveInventoryProducts",
				implementation, itemNature, fromDate, fromDate);
	}

	// To get all product where status=true and with item nature
	public List<Product> getActivePOSProduct(Implementation implementation,
			char itemNature, Date fromDate) throws Exception {
		return productDAO.findByNamedQuery("getActivePOSInventoryProducts",
				implementation, itemNature, fromDate, fromDate);
	}

	// To get all product where status=true and with item nature
	public List<Product> getAllPOSProducts(Implementation implementation)
			throws Exception {
		return productDAO.findByNamedQuery("getAllPOSProducts", implementation);
	}

	// To get all product where status=true and with item nature
	public List<Product> getAllProductForStockCheckUp(
			Implementation implementation) throws Exception {
		return productDAO.findByNamedQuery("getAllProductForStockCheckUp",
				implementation);
	}

	// To get all product where status=true and with item nature
	public List<Product> getReorderProducts(Implementation implementation)
			throws Exception {
		return productDAO
				.findByNamedQuery("getReorderProducts", implementation);
	}

	// to get product by id
	public List<Product> getProductByCategory(Implementation implementation,
			long categoryId, char itemNature, Date fromDate) throws Exception {
		return productDAO.findByNamedQuery("getActiveProductByCategory",
				implementation, categoryId, itemNature, fromDate, fromDate);
	}

	// to get product by id
	public List<Product> getPOSProductByCategory(Implementation implementation,
			long categoryId, char itemNature, Date fromDate, long storeId)
			throws Exception {
		return productDAO.findByNamedQuery("getActivePOSProductByCategory",
				implementation, categoryId, itemNature, fromDate, fromDate,
				storeId);
	}

	@SuppressWarnings("unchecked")
	public List<Product> getProductByCategories(Implementation implementation,
			Long[] categoryIds, char itemType, Date fromDate) throws Exception {
		Criteria criteria = productDAO.createCriteria();
		criteria.createAlias("lookupDetailByProductUnit", "pu",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetailByItemNature", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("productCategory", "pc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("productPricingDetails", "ppd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ppd.productPricing", "ppr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.add(Restrictions.eq("itemType", itemType));
		criteria.add(Restrictions.eq("status", true));
		criteria.add(Restrictions.le("ppr.startDate", fromDate));
		criteria.add(Restrictions.ge("ppr.endDate", fromDate));

		criteria.add(Restrictions.in("pc.productCategoryId", categoryIds));

		Set<Product> uniqueRecord = new HashSet<Product>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Product>(
				uniqueRecord) : null;
	}

	// to get product by id
	public Product getProductById(long productId) throws Exception {
		return productDAO.findByNamedQuery("getProductById", productId).get(0);
	}

	// to get basic product by id
	public Product getBasicProductById(long productId) throws Exception {
		return productDAO.findById(productId);
	}

	// to get basic product by id
	public Product getSimpleProductById(Long productId) throws Exception {
		return productDAO.findByNamedQuery("getSimpleProductById", productId)
				.get(0);
	}

	// To save product details
	public void saveProducts(List<Product> products) throws Exception {
		productDAO.saveOrUpdateAll(products);
	}

	// To save product details
	public void saveProductDetails(Product productDetails) throws Exception {
		productDAO.saveOrUpdate(productDetails);
	}

	// to delete product
	public void deleteProduct(Product productDetails) throws Exception {
		productDAO.delete(productDetails);
	}

	public Product getProductByProductName(Implementation implementation,
			String productName) throws Exception {
		List<Product> products = productDAO.findByNamedQuery(
				"getProductByProductName", implementation, productName);
		return null != products && products.size() > 0 ? products.get(0) : null;
	}

	public Product getProductByProductCode(Implementation implementation,
			String productCode) throws Exception {
		List<Product> products = productDAO.findByNamedQuery(
				"getProductByProductCode", implementation, productCode);
		return null != products && products.size() > 0 ? products.get(0) : null;
	}

	// To get all product where status=true and with item nature
	public List<Product> getProductCodeByType(Implementation implementation,
			char itemType) throws Exception {
		return productDAO.findByNamedQuery("getProductCodeByType",
				implementation, itemType);
	}

	// To get all product where status=true and with item nature
	public List<Product> getProductCodeByType(Implementation implementation,
			char itemType, byte itemSubType) throws Exception {
		return productDAO.findByNamedQuery("getProductCodeByItemSubType",
				implementation, itemType, itemSubType);
	}

	@SuppressWarnings("unchecked")
	public List<Product> getProductFinishedInventory(
			Implementation implementation, char itemType, Set<Byte> itemSubTypes)
			throws Exception {
		Criteria criteria = productDAO.createCriteria();

		criteria.createAlias("productCategory", "pc",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		criteria.add(Restrictions.eq("itemType", itemType));
		criteria.add(Restrictions.in("itemSubType", itemSubTypes));
		Set<Product> uniqueRecords = new HashSet<Product>(criteria.list());
		List<Product> products = new ArrayList<Product>(uniqueRecords);
		return products;
	}

	public List<Product> getProductsByItemType(Implementation implementation,
			Character type) throws Exception {

		return productDAO.findByNamedQuery("getProductsByItemType",
				implementation, type);
	}

	public List<Product> getCraftProducts(Implementation implementation,
			Byte itemSubType1, Byte itemSubType2) throws Exception {
		return productDAO.findByNamedQuery("getCraftProducts", implementation,
				itemSubType1, itemSubType2);
	}

	public List<Product> getFinsishedGoodsProducts(
			Implementation implementation, Byte itemSubType) throws Exception {
		return productDAO.findByNamedQuery("getFinsishedGoodsProducts",
				implementation, itemSubType);
	}

	public List<Product> getNonCraftServiceProductDetails(
			Implementation implementation, Character itemType,
			Byte itemSubType1, Byte itemSubType2) throws Exception {
		return productDAO.findByNamedQuery("getNonCraftServiceProductDetails",
				implementation, itemType, itemSubType1, itemSubType2);
	}

	public List<Product> getServiceProducts(Implementation implementation,
			Byte itemSubType1, Byte itemSubType2) throws Exception {
		return productDAO.findByNamedQuery("getServiceProducts",
				implementation, itemSubType1);
	}

	public List<Product> getServiceAndCraftProducts(
			Implementation implementation, Byte itemSubType1, Byte itemSubType2) {
		return productDAO.findByNamedQuery("getServiceAndCraftProducts",
				implementation, itemSubType1, itemSubType2);
	}

	public Product getProductCodeByBarCode(Implementation implementation,
			String barCode) {
		List<Product> products = productDAO.findByNamedQuery(
				"getProductCodeByBarCode", implementation, barCode);
		return null != products && products.size() > 0 ? products.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Product> getAllInventoryStatement(
			Implementation implementation, Long productId, Date fromDate,
			Date toDate) throws Exception {

		Criteria criteria = productDAO.createCriteria();

		/*
		 * if (fromDate != null && toDate != null) {
		 * criteria.add(Restrictions.between("expireDate", fromDate, toDate)); }
		 */

		// criteria.setFetchMode("company", FetchMode.JOIN);

		criteria.createAlias("accountByInventoryAccount", "inv",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("inv.combinationsForNaturalAccountId",
				"natural_acc", CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("inv.combinationsForAnalysisAccountId",
				"analysis_acc", CriteriaSpecification.LEFT_JOIN);

		/*
		 * criteria.createAlias("combinationByProductCombinationId", "c2",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("combinationByRevenueAccount", "c3",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("combinationByExpenseAccount", "c4",
		 * CriteriaSpecification.LEFT_JOIN);
		 */

		criteria.createAlias("natural_acc.transactionDetails", "td2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("natural_acc.accountByCompanyAccountId", "ac21",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("natural_acc.accountByNaturalAccountId", "ac22",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("natural_acc.accountByCostcenterAccountId",
				"ac23", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("natural_acc.accountByAnalysisAccountId", "ac24",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("td2.transaction", "t2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("t2.currency", "cur2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur2.currencyPool", "cp2",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("analysis_acc.transactionDetails", "td3",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("analysis_acc.accountByCompanyAccountId", "ac31",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("analysis_acc.accountByNaturalAccountId", "ac32",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("analysis_acc.accountByCostcenterAccountId",
				"ac33", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("analysis_acc.accountByAnalysisAccountId", "ac34",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("td3.transaction", "t3",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("t3.currency", "cur3",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur3.currencyPool", "cp3",
				CriteriaSpecification.LEFT_JOIN);

		/*
		 * criteria.createAlias("c4.transactionDetails", "td4",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("c4.accountByCompanyAccountId", "ac41",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("c4.accountByNaturalAccountId", "ac42",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("c4.accountByCostcenterAccountId", "ac43",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("c4.accountByAnalysisAccountId", "ac44",
		 * CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("td4.transaction", "t4",
		 * CriteriaSpecification.LEFT_JOIN); criteria.createAlias("t4.currency",
		 * "cur4", CriteriaSpecification.LEFT_JOIN);
		 * criteria.createAlias("cur4.currencyPool", "cp4",
		 * CriteriaSpecification.LEFT_JOIN);
		 */

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (productId != null) {
			criteria.add(Restrictions.eq("productId", productId));
		}

		DetachedCriteria dc = DetachedCriteria
				.forClass(TransactionDetail.class);

		dc.setProjection(Projections.distinct(Projections
				.property("combination.combinationId")));

		Disjunction disjunction = Restrictions.disjunction();

		if (fromDate != null && toDate != null) {
			disjunction.add(Restrictions.and(
					Property.forName("natural_acc.combinationId").in(dc),
					Restrictions
							.between("t2.transactionTime", fromDate, toDate)));
			disjunction.add(Restrictions.and(
					Property.forName("analysis_acc.combinationId").in(dc),
					Restrictions
							.between("t3.transactionTime", fromDate, toDate)));
			/*
			 * disjunction.add(Restrictions.and(
			 * Property.forName("c4.combinationId").in(dc), Restrictions
			 * .between("t4.transactionTime", fromDate, toDate)));
			 */
			criteria.add(disjunction);
		} else {
			disjunction.add(Property.forName("natural_acc.combinationId")
					.in(dc));
			disjunction.add(Property.forName("analysis_acc.combinationId").in(
					dc));
			/* disjunction.add(Property.forName("c4.combinationId").in(dc)); */
			criteria.add(disjunction);
		}

		return criteria.list();

		// return productDAO.findByNamedQuery("getAllInventoryStatement",
		// implementation);
	}

	public List<Product> getProductsByCode(Implementation implementation)
			throws Exception {
		return productDAO.findByNamedQuery("getProductsByCode", implementation);
	}

	public List<Product> getAllProductForStockPeriodic(
			Implementation implementation) throws Exception {
		List<Product> products = productDAO.findByNamedQuery(
				"getAllProductForStockPeriodic", implementation);
		return products;
	}

	// To get all active products
	@SuppressWarnings("unchecked")
	public List<Product> getActiveProductByProductList(Set<Long> productIds)
			throws Exception {
		Criteria productCriteria = productDAO.createCriteria();
		productCriteria.createAlias("lookupDetailByProductUnit", "pu",
				CriteriaSpecification.LEFT_JOIN);
		productCriteria.createAlias("lookupDetailByItemNature", "ld",
				CriteriaSpecification.LEFT_JOIN);
		productCriteria.createAlias("productCategory", "pc",
				CriteriaSpecification.LEFT_JOIN);
		productCriteria.add(Restrictions.conjunction().add(
				Restrictions.in("productId", productIds)));
		productCriteria.add(Restrictions.not(Restrictions.eq("specialPos", true)));
		Set<Product> uniqueRecord = new HashSet<Product>(productCriteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Product>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Product> getProductsByCombination(Set<Long> combinationIds) {
		Criteria criteria = productDAO.createCriteria();
		criteria.createAlias("combinationByInventoryAccount", "inv",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combinationByExpenseAccount", "exp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.or(
				Restrictions.in("inv.combinationId", combinationIds),
				Restrictions.in("exp.combinationId", combinationIds)));
		Set<Product> uniqueRecord = new HashSet<Product>(criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Product>(
				uniqueRecord) : null;
	}

	// Getter & Setters
	public AIOTechGenericDAO<Product> getProductDAO() {
		return productDAO;
	}

	public void setProductDAO(AIOTechGenericDAO<Product> productDAO) {
		this.productDAO = productDAO;
	}
}
