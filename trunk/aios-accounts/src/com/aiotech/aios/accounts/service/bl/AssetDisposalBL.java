package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetDisposal;
import com.aiotech.aios.accounts.domain.entity.vo.AssetDisposalVO;
import com.aiotech.aios.accounts.service.AssetDisposalService;
import com.aiotech.aios.common.to.Constants.Accounts.DisposalMethod;
import com.aiotech.aios.common.to.Constants.Accounts.DisposalType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class AssetDisposalBL {

	// Dependencies
	private AssetDisposalService assetDisposalService;

	// Show all Asset Disposals
	public List<Object> showAllAssetDisposal() throws Exception {
		List<AssetDisposal> assetDisposals = assetDisposalService
				.getAllAssetDisposal(getImplementation());
		List<Object> assetDisposalVOs = new ArrayList<Object>();
		if (null != assetDisposals && assetDisposals.size() > 0) {
			for (AssetDisposal assetDisposal : assetDisposals)
				assetDisposalVOs.add(addAssetDisposalVO(assetDisposal));
		}
		return assetDisposalVOs;
	}

	// Save Asset Disposal
	public void saveAssetDisposal(AssetDisposal assetDisposal) throws Exception {
		if (null == assetDisposal.getAssetDisposalId())
			SystemBL.saveReferenceStamp(AssetDisposal.class.getName(),
					getImplementation());
		assetDisposalService.saveAssetDisposal(assetDisposal);
	}

	// Create Asset Disposal
	private AssetDisposalVO addAssetDisposalVO(AssetDisposal assetDisposal) {
		AssetDisposalVO assetDisposalVO = new AssetDisposalVO();
		assetDisposalVO.setAssetDisposalId(assetDisposal.getAssetDisposalId());
 		assetDisposalVO.setDate(DateFormat.convertDateToString(assetDisposal
				.getDisposalDate().toString()));
		assetDisposalVO.setMethodName(DisposalMethod.get(
				assetDisposal.getDisposalMethod()).name());
		assetDisposalVO.setTypeName(DisposalType.get(
				assetDisposal.getDisposalType()).name());
		assetDisposalVO.setReferenceNumber(assetDisposal.getReferenceNumber());
		assetDisposalVO.setAssetName(assetDisposal.getAssetCreationByAssetId()
				.getAssetName());
		return assetDisposalVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetDisposalService getAssetDisposalService() {
		return assetDisposalService;
	}

	public void setAssetDisposalService(
			AssetDisposalService assetDisposalService) {
		this.assetDisposalService = assetDisposalService;
	}
}
