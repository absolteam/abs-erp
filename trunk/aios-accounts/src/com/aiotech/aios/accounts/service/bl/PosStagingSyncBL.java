package com.aiotech.aios.accounts.service.bl;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.pos.domain.entity.vo.PosSyncSchema;
import com.aiotech.aios.pos.domain.entity.vo.PosSyncTable;
import com.aiotech.aios.pos.domain.entity.vo.SyncTableColumnEntry;

public class PosStagingSyncBL {

	static final Logger LOGGER = LogManager.getLogger(PosStagingSyncBL.class);
	static Properties props = null;
	private final String SKIP = "skip";
	private final String FALSE = "false";
	Connection productionDatabase = null;
	Connection stagingDatabase = null;

	PosStagingSyncBL() {

	}

	Boolean bothDatabasesAvailable() throws Exception {

		try {

			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			props = new Properties();
			props.load(loader.getResourceAsStream("dbconfig.properties"));
			LOGGER.info("POS staging config file loaded");

			Properties syncConf = new Properties();
			syncConf.load(new FileInputStream(new File(props
					.getProperty("pos.sync.config"))));

			if (syncConf.getProperty("pos.sync.run").equalsIgnoreCase(FALSE)) {
				LOGGER.warn("POS staging sync is disabled!");
				return false;
			}

			Class.forName("com.mysql.jdbc.Driver");
			productionDatabase = DriverManager.getConnection(
					"jdbc:mysql://" + props.getProperty("prod.db.host") + ":"
							+ props.getProperty("prod.db.port") + "/"
							+ props.getProperty("prod.db.name"),
					props.getProperty("prod.db.user"),
					props.getProperty("prod.db.password"));
			productionDatabase.setAutoCommit(false);

			stagingDatabase = DriverManager.getConnection(
					"jdbc:mysql://" + props.getProperty("staging.db.host")
							+ ":" + props.getProperty("staging.db.port") + "/"
							+ props.getProperty("staging.db.name"),
					props.getProperty("staging.db.user"),
					props.getProperty("staging.db.password"));
			stagingDatabase.setAutoCommit(false);

			return (productionDatabase != null) && (stagingDatabase != null);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Failed to initialize POS Staging Sync (error: "
					+ e.getMessage() + ")");
			return false;
		}
	}

	void closeDatabaseConnection() throws Exception {

		if (productionDatabase != null)
			productionDatabase.close();

		if (stagingDatabase != null)
			stagingDatabase.close();
	}

	public Boolean syncPosTablesToStaging() throws Exception {

		if (!bothDatabasesAvailable())
			return false;

		try {
			PosSyncSchema posSyncSchema = getPosSyncSchema(props
					.getProperty("pos.sync.schema"));
			List<PosSyncTable> posSyncTables = posSyncSchema.getPosSyncTables();
			ResultSet selectResult = null;
			String SELECT = null;
			String INSERT = null;
			String TRUNCATE = null;
			String columns = null;
			String values = null;
			Integer recordsCount = 0;
			Integer tableCount = 0;
			String resolvedValue = null;

			stagingDatabase.createStatement().execute(
					"SET FOREIGN_KEY_CHECKS=0");

			for (PosSyncTable tableToSync : posSyncTables) {

				TRUNCATE = "DELETE FROM ".concat(tableToSync.getTableName());
				stagingDatabase.createStatement().execute(TRUNCATE);

				SELECT = "SELECT * FROM ".concat(tableToSync.getTableName());
				selectResult = productionDatabase.createStatement()
						.executeQuery(SELECT);

				recordsCount = 0;
				tableCount++;

				while (selectResult.next()) {

					columns = "";
					values = "";

					Integer colCount = selectResult.getMetaData()
							.getColumnCount();
					List<SyncTableColumnEntry> crossCheckColumnEntries = tableToSync
							.getSyncTableStructure()
							.getSyncTableColumnEntries();
					List<String> columnsToSkip = new ArrayList<String>();

					for (SyncTableColumnEntry tableColumn : crossCheckColumnEntries) {
						if (tableColumn.getOptions().equalsIgnoreCase(SKIP))
							columnsToSkip.add(tableColumn.getColumn());
					}

					for (Integer columnIndex = 1; columnIndex <= colCount; columnIndex++) {

						if (selectResult.getString(columnIndex) != null
								&& !columnsToSkip.contains(selectResult
										.getMetaData().getColumnName(
												columnIndex))) {

							columns += selectResult.getMetaData()
									.getColumnName(columnIndex).concat(",");

							resolvedValue = this.resolveToValue(
									selectResult.getMetaData()
											.getColumnTypeName(columnIndex),
									selectResult.getString(columnIndex));
							values += resolvedValue != null ? resolvedValue
									.concat(",") : null + ",";
						}
					}

					INSERT = "INSERT INTO ".concat(tableToSync.getTableName())
							.concat("("
									+ AIOSCommons
											.removeTrailingSymbols(columns)
									+ ") VALUES ("
									+ AIOSCommons.removeTrailingSymbols(values)
									+ ")");

					stagingDatabase.createStatement().execute(INSERT);
					recordsCount++;
				}

				LOGGER.info("Will add " + recordsCount + " records for table: "
						+ tableToSync.getTableName());
			}

			stagingDatabase.commit();
			LOGGER.info("Staging database commit success for '" + tableCount
					+ "' table(s)");

			stagingDatabase.createStatement().execute(
					"SET FOREIGN_KEY_CHECKS=1");
			stagingDatabase.commit();
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Staging database sync failed: " + e.getMessage());
			return false;

		} finally {
			closeDatabaseConnection();
		}
	}

	private String resolveToValue(String type, String value) {

		if (type.equalsIgnoreCase("CHAR") && value != null
				&& value.trim().isEmpty() && value.trim().length() == 0)
			return null;
		if (type.contains("CHAR") || type.contains("DATE")
				|| type.contains("TIME") || type.contains("TEXT"))
			return "'" + value.replace("'", "-") + "'";
		else
			return value;
	}

	private PosSyncSchema getPosSyncSchema(String configFilePath)
			throws Exception {

		PosSyncSchema posSyncSchema = null;
		JAXBContext jaxbContext = JAXBContext.newInstance(PosSyncSchema.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		posSyncSchema = (PosSyncSchema) jaxbUnmarshaller.unmarshal((new File(
				configFilePath)));
		return posSyncSchema;
	}

}
