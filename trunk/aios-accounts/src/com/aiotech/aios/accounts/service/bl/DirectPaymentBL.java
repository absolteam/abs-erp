package com.aiotech.aios.accounts.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.DirectPaymentVO;
import com.aiotech.aios.accounts.service.BankService;
import com.aiotech.aios.accounts.service.DirectPaymentService;
import com.aiotech.aios.common.to.Constants.Accounts.GeneralStatus;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.ReceiptSource;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.vo.ConfigurationVO;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;
import com.opensymphony.xwork2.ActionContext;

public class DirectPaymentBL {

	private DirectPaymentService directPaymentService;
	private BankService bankService;
	private List<DirectPayment> directPaymentList;
	private TransactionBL transactionBL;
	private ChequeBookBL chequeBookBL;
	private AlertBL alertBL;
	private ConfigurationHandler configurationHandler;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Retrieve direct payments and form json array
	public JSONObject getDirectPaymentList(Implementation implementation)
			throws Exception {
		directPaymentList = this.getDirectPaymentService()
				.getAllDirectPayments(implementation);
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		if (null != directPaymentList && directPaymentList.size() > 0) {
			JSONArray array = null;
			double totalAmount = 0;
			for (DirectPayment list : directPaymentList) {
				array = new JSONArray();
				String voidCheck = "";
				if (null != list.getVoidPayments()
						&& list.getVoidPayments().size() > 0)
					voidCheck = "<span title='Void Payment' class='void_payment_view'></span>";
				array.add(list.getDirectPaymentId());
				array.add(list.getPaymentNumber().concat(voidCheck));
				array.add(DateFormat.convertDateToString(list.getPaymentDate()
						.toString()));
				array.add(ModeOfPayment.get(list.getPaymentMode()).name()
						.replaceAll("_", " "));
				totalAmount = 0;
				List<DirectPaymentDetail> detailList = new ArrayList<DirectPaymentDetail>(
						list.getDirectPaymentDetails());
				for (DirectPaymentDetail list2 : detailList) {
					totalAmount += list2.getAmount();
				}
				array.add(AIOSCommons.formatAmount(totalAmount));
				if (null != list.getSupplier()) {
					if (null != list.getSupplier().getPerson())
						array.add(list
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat(" ")
								.concat(list.getSupplier().getPerson()
										.getLastName()));
					else
						array.add(null != list.getSupplier().getCompany() ? list
								.getSupplier().getCompany().getCompanyName()
								: list.getSupplier().getCmpDeptLocation()
										.getCompany().getCompanyName());
				} else if (null != list.getCustomer()) {
					if (null != list.getCustomer().getPersonByPersonId())
						array.add(list
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(list.getCustomer()
										.getPersonByPersonId().getLastName()));
					else
						array.add(null != list.getCustomer().getCompany() ? list
								.getCustomer().getCompany().getCompanyName()
								: list.getCustomer().getCmpDeptLocation()
										.getCompany().getCompanyName());
				} else if (null != list.getPersonByPersonId()) {
					array.add(list.getPersonByPersonId().getFirstName()
							.concat(" ")
							.concat(list.getPersonByPersonId().getLastName()));
				} else {
					array.add(list.getOthers());
				}
				if (null != list.getBankAccount()) {
					array.add(list.getBankAccount().getBank().getBankName());
					array.add(list.getBankAccount().getAccountNumber());
					if (null != list.getChequeNumber())
						array.add(list.getChequeNumber());
					else
						array.add("");
					if (null != list.getChequeDate())
						array.add(DateFormat.convertDateToString(list
								.getChequeDate().toString()));
					else
						array.add("");
				} else {
					array.add("");
					array.add("");
					array.add("");
					array.add("");
				}
				if (null != list.getVoidPayments()
						&& list.getVoidPayments().size() > 0)
					array.add("false");
				else
					array.add("true");
				data.add(array);
			}
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// Get all DirectPaymentDetails
	public List<Object> getDirectPaymentDetails(Implementation implementation)
			throws Exception {
		List<DirectPaymentDetail> directPaymentDetails = directPaymentService
				.getAllDirectPaymentDetails(implementation);
		List<Object> directPaymentDetailVOs = new ArrayList<Object>();
		if (null != directPaymentDetails && directPaymentDetails.size() > 0) {
			for (DirectPaymentDetail directPaymentDetail : directPaymentDetails)
				directPaymentDetailVOs
						.add(addDirectPaymentDetail(directPaymentDetail));
		}
		return directPaymentDetailVOs;
	}

	private DirectPaymentVO addDirectPaymentDetail(
			DirectPaymentDetail directPaymentDetail) throws Exception {
		DirectPaymentVO directPaymentVO = new DirectPaymentVO();
		directPaymentVO.setDirectPaymentDetailId(directPaymentDetail
				.getDirectPaymentDetailId());
		directPaymentVO.setPaymentNumber(directPaymentDetail.getDirectPayment()
				.getPaymentNumber());
		directPaymentVO
				.setInvoiceNumber(directPaymentDetail.getInvoiceNumber());
		directPaymentVO.setAmount(AIOSCommons.formatAmount(directPaymentDetail
				.getAmount()));
		directPaymentVO.setDirectPaymentMode(ModeOfPayment
				.get(directPaymentDetail.getDirectPayment().getPaymentMode())
				.name().replaceAll("_", " "));
		if (null != directPaymentDetail.getDirectPayment().getSupplier()) {
			if (null != directPaymentDetail.getDirectPayment().getSupplier()
					.getPerson())
				directPaymentVO.setPayable(directPaymentDetail
						.getDirectPayment()
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(directPaymentDetail.getDirectPayment()
								.getSupplier().getPerson().getLastName()));
			else if (null != directPaymentDetail.getDirectPayment()
					.getSupplier().getCompany())
				directPaymentVO.setPayable(directPaymentDetail
						.getDirectPayment().getSupplier().getCompany()
						.getCompanyName());
			else if (null != directPaymentDetail.getDirectPayment()
					.getSupplier().getCmpDeptLocation())
				directPaymentVO.setPayable(directPaymentDetail
						.getDirectPayment().getSupplier().getCmpDeptLocation()
						.getCompany().getCompanyName());
			directPaymentVO.setPayableType(ReceiptSource.Supplier.name());
		} else if (null != directPaymentDetail.getDirectPayment().getCustomer()) {
			if (null != directPaymentDetail.getDirectPayment().getCustomer()
					.getPersonByPersonId())
				directPaymentVO.setPayable(directPaymentDetail
						.getDirectPayment()
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(directPaymentDetail.getDirectPayment()
								.getCustomer().getPersonByPersonId()
								.getLastName()));
			else if (null != directPaymentDetail.getDirectPayment()
					.getCustomer().getCompany())
				directPaymentVO.setPayable(directPaymentDetail
						.getDirectPayment().getCustomer().getCompany()
						.getCompanyName());
			else if (null != directPaymentDetail.getDirectPayment()
					.getCustomer().getCmpDeptLocation())
				directPaymentVO.setPayable(directPaymentDetail
						.getDirectPayment().getCustomer().getCmpDeptLocation()
						.getCompany().getCompanyName());
			directPaymentVO.setPayableType(ReceiptSource.Customer.name());
		} else if (null != directPaymentDetail.getDirectPayment()
				.getPersonByPersonId()) {
			directPaymentVO.setPayable(directPaymentDetail
					.getDirectPayment()
					.getPersonByPersonId()
					.getFirstName()
					.concat(" ")
					.concat(directPaymentDetail.getDirectPayment()
							.getPersonByPersonId().getLastName()));
			directPaymentVO.setPayableType(ReceiptSource.Employee.name());
		} else {
			directPaymentVO.setPayable(directPaymentDetail.getDirectPayment()
					.getOthers());
			directPaymentVO.setPayableType(ReceiptSource.Others.name());
		}
		return directPaymentVO;
	}

	// Save direct payment & show alert
	@SuppressWarnings("rawtypes")
	public void saveDirectPayments(DirectPayment directPayment,
			DirectPayment directPaymentDB,
			List<DirectPaymentDetail> paymentDetailList,
			List<DirectPaymentDetail> paymentDetailDBList,
			ChequeBook chequeBook,
			List<DirectPaymentDetail> deletePaymentDetailList,
			DirectPaymentVO directPaymentVO) throws Exception {

		directPayment.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (directPayment != null && directPayment.getDirectPaymentId() != null
				&& directPayment.getDirectPaymentId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		boolean editFlag = false;
		directPayment.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
				paymentDetailList));
		if (directPayment.getDirectPaymentId() != null)
			editFlag = true;

		directPaymentService.saveDirectPayments(directPayment,
				paymentDetailList);

		for (DirectPaymentDetail directPaymentDetail : paymentDetailList) {
			if (directPaymentDetail.getUseCase() != null
					&& directPaymentDetail.getRecordId() != null) {
				Class[] argTypes = null;
				AIOTechGenericDAO<Object> dao = WorkflowUtils
						.getEntityDAO(directPaymentDetail.getUseCase());
				Object recordObject = dao.findById(directPaymentDetail
						.getRecordId());
				String nameTemp = recordObject.getClass().getName()
						.replace(".domain.entity.", ".DOMAIN.ENTITY.");
				String[] name = nameTemp.split(".DOMAIN.ENTITY.");
				if (name[name.length - 1].contains("_$$_javassist")) {
					name[name.length - 1] = name[name.length - 1].substring(0,
							name[name.length - 1].indexOf("_$$_javassist"));
				}
				String approvalflagmethodId = "setStatus";
				Object status = null;
				argTypes = new Class[] { Byte.class };
				if (directPaymentDetail.getUseCase().equalsIgnoreCase(
						Invoice.class.getSimpleName())) {
					status = InvoiceStatus.Paid.getCode();
				}
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);
				dao.saveOrUpdate(recordObject);
			}
		}

		// Reverse the status update
		if (editFlag) {
			// Reverse the status update
			if (directPayment.getUseCase() != null
					&& directPayment.getRecordId() != null)
				alertBL.commonReverseUpdateUseCaseStatus(
						directPayment.getUseCase(),
						directPayment.getRecordId(),
						GeneralStatus.Pending.getCode());

			// JV Reverse Transaction
			transactionBL.deleteTransaction(directPayment.getDirectPaymentId(),
					DirectPayment.class.getSimpleName());

			if (null != deletePaymentDetailList
					&& deletePaymentDetailList.size() > 0) {
				for (DirectPaymentDetail directPaymentDetail : deletePaymentDetailList) {
					if (null != directPaymentDetail.getUseCase()
							&& !("").equals(directPaymentDetail.getUseCase())) {
						Class[] argTypes = null;
						AIOTechGenericDAO<Object> dao = WorkflowUtils
								.getEntityDAO(directPaymentDetail.getUseCase());
						Object recordObject = dao.findById(directPaymentDetail
								.getRecordId());
						String nameTemp = recordObject.getClass().getName()
								.replace(".domain.entity.", ".DOMAIN.ENTITY.");
						String[] name = nameTemp.split(".DOMAIN.ENTITY.");
						if (name[name.length - 1].contains("_$$_javassist")) {
							name[name.length - 1] = name[name.length - 1]
									.substring(0, name[name.length - 1]
											.indexOf("_$$_javassist"));
						}
						String approvalflagmethodId = "setStatus";
						Object status = null;
						argTypes = new Class[] { Byte.class };
						if (directPaymentDetail.getUseCase().equalsIgnoreCase(
								Invoice.class.getSimpleName())) {
							status = InvoiceStatus.Active.getCode();
						}
						Method approvalmethod = recordObject.getClass()
								.getMethod(approvalflagmethodId, argTypes);
						approvalmethod.invoke(recordObject, status);

						dao.saveOrUpdate(recordObject);
					}
					directPaymentService
							.deletePaymentDetail(deletePaymentDetailList);
				}
			}

		} else
			SystemBL.saveReferenceStamp(DirectPayment.class.getName(),
					getImplementationId());

		if (null != chequeBook && null != directPayment.getChequeBook()
				&& (long) directPayment.getChequeBook().getChequeBookId() > 0) {
			chequeBook.setCurrentNo(directPayment.getChequeNumber());
			chequeBookBL.directSaveChequeBook(chequeBook);
		}

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				DirectPayment.class.getSimpleName(),
				directPayment.getDirectPaymentId(), user, workflowDetailVo);

	}

	public void directPaymentAfterWorkflowApproval(DirectPayment directPayment,
			DirectPayment directPaymentDB,
			List<DirectPaymentDetail> paymentDetailList,
			List<DirectPaymentDetail> paymentDetailDBList,
			ChequeBook chequeBook,
			List<DirectPaymentDetail> deletePaymentDetailList, long alertId,
			DirectPaymentVO directPaymentVO) {

	}

	public void createPDCJournalEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;
		for (DirectPaymentDetail list : paymentDetailList) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), list.getDescription(),
					null, list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, getImplementationId().getPdcIssued(),
				TransactionType.Credit.getCode(), "Direct Payment(PDC Issued)",
				null, directPayment.getPaymentNumber()));
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						java.util.Calendar.getInstance().getTime()),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment for PDC & Payment No : "
						+ directPayment.getPaymentNumber(),
				directPayment.getPaymentDate(),
				transactionBL.getCategory(DirectPayment.class.getSimpleName()),
				directPayment.getDirectPaymentId(),
				DirectPayment.class.getSimpleName(),
				directPayment.getExchangeRate());
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	// Create Release Transaction
	public void createReleaseJournalEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList, Boolean debitFlag,
			Boolean creditFlag) throws Exception {
		Set<TransactionDetail> transactionDetails = null;
		for (DirectPaymentDetail list : paymentDetailList) {
			transactionDetails = new HashSet<TransactionDetail>();
			if (list.getCombination().getCombinationId()
					.equals(this.getImplementationId().getPdcReceived())) {
				transactionDetails
						.add(this.getTransactionBL()
								.createTransactionDetail(
										list.getAmount(),
										this.getImplementationId()
												.getUnearnedRevenue(),
										debitFlag, "UnEarned Revenue", null,
										list.getInvoiceNumber()));
				transactionDetails.add(this.getTransactionBL()
						.createTransactionDetail(list.getAmount(),
								list.getCombination().getCombinationId(),
								creditFlag, "PDC Received", null,
								directPayment.getPaymentNumber()));
			} else {
				transactionDetails.add(this.getTransactionBL()
						.createTransactionDetail(list.getAmount(),
								list.getCombination().getCombinationId(),
								debitFlag, "Release Payments", null,
								list.getInvoiceNumber()));
				if (null != directPayment.getBankAccount()) {
					BankAccount bankAccount = this.getBankService()
							.getBankAccountInfo(
									directPayment.getBankAccount()
											.getBankAccountId());
					transactionDetails.add(transactionBL
							.createTransactionDetail(
									list.getAmount(),
									bankAccount.getCombination()
											.getCombinationId(),
									creditFlag,
									"Paid by Bank "
											+ bankAccount.getAccountNumber()
											+ " :Cheque No "
											+ directPayment.getChequeNumber(),
									null, directPayment.getPaymentNumber()));
				} else {
					transactionDetails.add(transactionBL
							.createTransactionDetail(list.getAmount(),
									directPayment.getCombination()
											.getCombinationId(), creditFlag,
									"Paid by cash", null, directPayment
											.getPaymentNumber()));
				}
			}
			Transaction transaction = this.getTransactionBL()
					.createTransaction(
							directPayment.getCurrency().getCurrencyId(),
							"Release Transaction",
							directPayment.getPaymentDate(),
							transactionBL.getCategory("Direct Payment"),
							directPayment.getDirectPaymentId(),
							DirectPayment.class.getSimpleName());
			transaction.setTransactionDetails(transactionDetails);
			transactionBL.saveTransaction(
					transaction,
					new ArrayList<TransactionDetail>(transaction
							.getTransactionDetails()));
		}
	}

	public void createJournalEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;

		for (DirectPaymentDetail list : paymentDetailList) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), list.getDescription(),
					null, list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}

		if (null != directPayment.getBankAccount()) {
			BankAccount bankAccount = bankService
					.getBankAccountInfo(directPayment.getBankAccount()
							.getBankAccountId());
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, bankAccount.getCombination()
							.getCombinationId(), TransactionType.Credit
							.getCode(),
					"Paid by Bank " + bankAccount.getAccountNumber()
							+ " :Cheque No " + directPayment.getChequeNumber(),
					null, directPayment.getPaymentNumber()));

		} else {
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, directPayment.getCombination()
							.getCombinationId(), TransactionType.Credit
							.getCode(), "Paid by cash", null, directPayment
							.getPaymentNumber()));
		}
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						directPayment.getPaymentDate()),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment for Payment No: "
						+ directPayment.getPaymentNumber(), directPayment
						.getPaymentDate(), transactionBL
						.getCategory(DirectPayment.class.getSimpleName()),
				directPayment.getDirectPaymentId(), DirectPayment.class
						.getSimpleName(), directPayment.getExchangeRate());
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	public void createReverseJournalEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;

		for (DirectPaymentDetail list : paymentDetailList) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Credit.getCode(), list.getDescription(),
					null, list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}

		if (null != directPayment.getBankAccount()) {
			BankAccount bankAccount = bankService
					.getBankAccountInfo(directPayment.getBankAccount()
							.getBankAccountId());
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, bankAccount.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(),
					"Paid by Bank " + bankAccount.getAccountNumber()
							+ " :Cheque No " + directPayment.getChequeNumber(),
					null, directPayment.getPaymentNumber()));

		} else {
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, directPayment.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(), "Paid by cash", null, directPayment
							.getPaymentNumber()));
		}
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						directPayment.getPaymentDate()),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment(Reverse) for Payment No: "
						+ directPayment.getPaymentNumber(), directPayment
						.getPaymentDate(), transactionBL
						.getCategory(DirectPayment.class.getSimpleName()),
				directPayment.getDirectPaymentId(), DirectPayment.class
						.getSimpleName(), directPayment.getExchangeRate());
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	public void createJournalEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList,
			Implementation implementation) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;

		for (DirectPaymentDetail list : paymentDetailList) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), list.getDescription(),
					null, list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}

		if (null != directPayment.getBankAccount()) {
			BankAccount bankAccount = bankService
					.getBankAccountInfo(directPayment.getBankAccount()
							.getBankAccountId());
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, bankAccount.getCombination()
							.getCombinationId(), TransactionType.Credit
							.getCode(),
					"Paid by Bank " + bankAccount.getAccountNumber()
							+ " :Cheque No " + directPayment.getChequeNumber(),
					null, directPayment.getPaymentNumber()));

		} else {
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, directPayment.getCombination()
							.getCombinationId(), TransactionType.Credit
							.getCode(), "Paid by cash", null, directPayment
							.getPaymentNumber()));
		}
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						directPayment.getPaymentDate(), implementation),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment for Payment No: "
						+ directPayment.getPaymentNumber(), directPayment
						.getPaymentDate(), transactionBL.getCategory(
						DirectPayment.class.getSimpleName(), implementation),
				directPayment.getDirectPaymentId(), DirectPayment.class
						.getSimpleName(), directPayment.getExchangeRate(),
				implementation);
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	public void createReverseJournalEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList,
			Implementation implementation) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;

		for (DirectPaymentDetail list : paymentDetailList) {
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Credit.getCode(), list.getDescription(),
					null, list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}

		if (null != directPayment.getBankAccount()) {
			BankAccount bankAccount = bankService
					.getBankAccountInfo(directPayment.getBankAccount()
							.getBankAccountId());
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, bankAccount.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(),
					"Paid by Bank " + bankAccount.getAccountNumber()
							+ " :Cheque No " + directPayment.getChequeNumber(),
					null, directPayment.getPaymentNumber()));

		} else {
			transactionDetails.add(transactionBL.createTransactionDetail(
					totalAmount, directPayment.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(), "Paid by cash", null, directPayment
							.getPaymentNumber()));
		}

		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						directPayment.getPaymentDate(), implementation),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment(Reverse) for Payment No: "
						+ directPayment.getPaymentNumber(), directPayment
						.getPaymentDate(), transactionBL.getCategory(
						DirectPayment.class.getSimpleName(), implementation),
				directPayment.getDirectPaymentId(), DirectPayment.class
						.getSimpleName(), directPayment.getExchangeRate(),
				implementation);
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	public void deleteDirectPayment(long recordId, String useCase)
			throws Exception {
		DirectPayment directPayment = directPaymentService
				.getDirectPaymentByRecordIdAndUseCase(recordId, useCase);
		if (directPayment != null)
			deleteDirectPayment(directPayment);
	}

	public void directDeleteDirectPayment(long recordId, String useCase)
			throws Exception {
		DirectPayment directPayment = directPaymentService
				.getDirectPaymentByRecordIdAndUseCase(recordId, useCase);
		if (directPayment != null) {
			if (directPayment.getUseCase() != null
					&& directPayment.getRecordId() != null)
				alertBL.commonReverseUpdateUseCaseStatus(
						directPayment.getUseCase(),
						directPayment.getRecordId(),
						GeneralStatus.Pending.getCode());

			// JV Reverse Transaction
			transactionBL.deleteTransaction(directPayment.getDirectPaymentId(),
					DirectPayment.class.getSimpleName());

			directPaymentService.deletePayment(
					directPayment,
					new ArrayList<DirectPaymentDetail>(directPayment
							.getDirectPaymentDetails()));
		}
	}

	// Reverse transaction & Delete Payments
	public void deleteDirectPayment(DirectPayment directPayment)
			throws Exception {
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				directPayment.getDirectPaymentId(),
				DirectPayment.class.getSimpleName());
		if (null != alert)
			alertBL.getAlertService().deleteAlert(alert);
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				DirectPayment.class.getSimpleName(),
				directPayment.getDirectPaymentId(), user, workflowDetailVo);

	}

	@SuppressWarnings("rawtypes")
	public void doDirectPaymentBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		DirectPayment directPayment = this.getDirectPaymentService()
				.getDirectPaymentDetail(recordId);

		if (workflowDetailVO.isDeleteFlag()) {
			// Reverse the status update
			if (directPayment.getUseCase() != null
					&& directPayment.getRecordId() != null)
				alertBL.commonReverseUpdateUseCaseStatus(
						directPayment.getUseCase(),
						directPayment.getRecordId(),
						GeneralStatus.Pending.getCode());

			// JV Reverse Transaction
			transactionBL.deleteTransaction(directPayment.getDirectPaymentId(),
					DirectPayment.class.getSimpleName());

			for (DirectPaymentDetail directPaymentDetail : directPayment
					.getDirectPaymentDetails()) {
				if (null != directPaymentDetail.getUseCase()
						&& null != directPaymentDetail.getRecordId()) {
					Class[] argTypes = null;
					AIOTechGenericDAO<Object> dao = WorkflowUtils
							.getEntityDAO(directPaymentDetail.getUseCase());
					Object recordObject = dao.findById(directPaymentDetail
							.getRecordId());
					String nameTemp = recordObject.getClass().getName()
							.replace(".domain.entity.", ".DOMAIN.ENTITY.");
					String[] name = nameTemp.split(".DOMAIN.ENTITY.");
					if (name[name.length - 1].contains("_$$_javassist")) {
						name[name.length - 1] = name[name.length - 1]
								.substring(0, name[name.length - 1]
										.indexOf("_$$_javassist"));
					}
					String approvalflagmethodId = "setStatus";
					String processStatusMethodId = "setProcessStatus";
					Object status = null;
					Object processStatus = null;
					argTypes = new Class[] { Byte.class };
					if (directPaymentDetail.getUseCase().equalsIgnoreCase(
							Invoice.class.getSimpleName())) {
						status = InvoiceStatus.Active.getCode();
						processStatus = false;
					}
					Method approvalmethod = recordObject.getClass().getMethod(
							approvalflagmethodId, argTypes);
					approvalmethod.invoke(recordObject, status);

					argTypes = new Class[] { Boolean.class };
					Method approvalmethod1 = recordObject.getClass().getMethod(
							processStatusMethodId, argTypes);
					approvalmethod1.invoke(recordObject, processStatus);

					dao.saveOrUpdate(recordObject);
				}
			}

			directPaymentService.deletePayment(
					directPayment,
					new ArrayList<DirectPaymentDetail>(directPayment
							.getDirectPaymentDetails()));
		} else {
			// Common Status update blog
			if (directPayment.getUseCase() != null
					&& directPayment.getRecordId() != null)
				alertBL.commonAlertDeleteByUsecaseAndRecord(
						directPayment.getUseCase(), directPayment.getRecordId());
			for (DirectPaymentDetail paymentDetail : directPayment
					.getDirectPaymentDetails()) {
				alertBL.commonAlertDeleteByUsecaseAndRecord(
						paymentDetail.getUseCase(), paymentDetail.getRecordId());
			}
			// PDC Transaction
			directPayment.setIsPdc(false);
			if (null != directPayment.getChequeDate()) {
				Period period = transactionBL.getCalendarBL()
						.transactionPostingPeriod(
								DateFormat.convertStringToDate(DateFormat
										.convertDateToString(DateTime.now()
												.toLocalDate().toString())));
				Period pdcPeriod = transactionBL
						.getCalendarBL()
						.getCalendarService()
						.getPeriodByPeriodDate(directPayment.getChequeDate(),
								getImplementationId());
				int days = 0;
				if (null != pdcPeriod) {
					days = Days.daysBetween(new DateTime(period.getEndTime()),
							new DateTime(pdcPeriod.getEndTime())).getDays();
				} else {
					days = Days.daysBetween(new DateTime(period.getEndTime()),
							new DateTime(directPayment.getChequeDate()))
							.getDays();
				}
				if (days > 0) {
					createPDCJournalEntries(
							directPayment,
							new ArrayList<DirectPaymentDetail>(directPayment
									.getDirectPaymentDetails()));
					Combination pdcCb = new Combination();
					pdcCb.setCombinationId(getImplementationId().getPdcIssued());
					directPayment.setCombination(pdcCb);
					directPayment.setIsPdc(true);
				}
			}

			if (directPayment.getIsPdc() == null || !directPayment.getIsPdc()) {
				// JV Transaction
				createJournalEntries(
						directPayment,
						new ArrayList<DirectPaymentDetail>(directPayment
								.getDirectPaymentDetails()));
			}
		}

	}

	public Map<String, String> getModeOfPayment() throws Exception {
		Map<String, String> modeOfPayments = new HashMap<String, String>();
		String key = "";
		for (ModeOfPayment modeOfPayment : EnumSet.allOf(ModeOfPayment.class)) {
			if ((byte) ModeOfPayment.Cash.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cash.getCode().toString().concat("@@")
						.concat("CSH");
			else if ((byte) ModeOfPayment.Cheque.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Cheque.getCode().toString().concat("@@")
						.concat("CHQ");
			else if ((byte) ModeOfPayment.Credit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Credit_Card.getCode().toString()
						.concat("@@").concat("CCD");
			else if ((byte) ModeOfPayment.Debit_Card.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Debit_Card.getCode().toString()
						.concat("@@").concat("DCD");
			else if ((byte) ModeOfPayment.Wire_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Wire_Transfer.getCode().toString()
						.concat("@@").concat("WRT");
			else if ((byte) ModeOfPayment.Inter_Transfer.getCode() == (byte) modeOfPayment
					.getCode())
				key = ModeOfPayment.Inter_Transfer.getCode().toString()
						.concat("@@").concat("IAT");
			modeOfPayments.put(key, modeOfPayment.name().replaceAll("_", " "));
		}
		return modeOfPayments;
	}

	public Map<String, ReceiptSource> getReceiptSource() throws Exception {
		Map<String, ReceiptSource> receiptSources = new HashMap<String, ReceiptSource>();
		String key = "";
		for (ReceiptSource receiptSource : EnumSet.allOf(ReceiptSource.class)) {
			if ((byte) ReceiptSource.Customer.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Customer.getCode().toString().concat("@@")
						.concat("CST");
			else if ((byte) ReceiptSource.Supplier.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Supplier.getCode().toString().concat("@@")
						.concat("SPL");
			else if ((byte) ReceiptSource.Employee.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Employee.getCode().toString().concat("@@")
						.concat("EMP");
			else if ((byte) ReceiptSource.Others.getCode() == (byte) receiptSource
					.getCode())
				key = ReceiptSource.Others.getCode().toString().concat("@@")
						.concat("OT");
			receiptSources.put(key, receiptSource);
		}
		return receiptSources;
	}

	public void paymentStatusUpdateCall() {
		try {
			List<Implementation> implementations = alertBL.getSystemService()
					.getAllImplementation();
			for (Implementation implementation : implementations) {
				List<DirectPayment> directPaymentList = this
						.getDirectPaymentService().getAllDirectPayments(
								implementation);
				for (DirectPayment directPayment : directPaymentList) {
					if (directPayment.getUseCase() != null
							&& directPayment.getRecordId() != null)
						alertBL.commonUpdateUseCaseStatus(
								directPayment.getUseCase(),
								directPayment.getRecordId(),
								GeneralStatus.Pending.getCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void convertPDCPayment() {
		try {
			Implementation impl = new Implementation();
			impl.setImplementationId(6l);
			List<DirectPayment> directPayments = directPaymentService
					.getDirectPaymentsByCurrentDate(impl,
							DateFormat.convertStringToDate("31-Jun-2015"));
			for (DirectPayment directPayment : directPayments) {
				processTransaction(
						directPayment,
						new ArrayList<DirectPaymentDetail>(directPayment
								.getDirectPaymentDetails()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertDirectPaymentPDCCheque() {
		try {
			List<Implementation> implementations = alertBL.getSystemService()
					.getAllImplementation();
			ConfigurationVO configurationVO = null;
			LocalDate localeDate = null;
			for (Implementation implementation : implementations) {
				localeDate = DateTime.now().toLocalDate();
				configurationVO = configurationHandler
						.getConfigurationSetup(implementation
								.getImplementationId());
				if (null != configurationVO)
					localeDate = DateTime.now().toLocalDate()
							.plusDays(configurationVO.getAlertDays());
				List<DirectPayment> directPayments = directPaymentService
						.getPDCDirectPayments(DateFormat
								.convertStringToDate(DateFormat
										.convertDateToString(localeDate
												.toString())), implementation);
				Set<Long> dpIds = null;
				if (null != directPayments && directPayments.size() > 0) {
					dpIds = new HashSet<Long>();
					for (DirectPayment directPayment : directPayments)
						dpIds.add(directPayment.getDirectPaymentId());
				} else
					directPayments = new ArrayList<DirectPayment>();
				// Check for any missing pdc in alerts
				List<DirectPayment> directPaymentCks = directPaymentService
						.getCtPDCDirectPayments(DateFormat
								.convertStringToDate(DateFormat
										.convertDateToString(localeDate
												.toString())), implementation,
								dpIds);
				if (null != directPaymentCks && directPaymentCks.size() > 0) {
					Alert alert = null;
					for (DirectPayment directPayment : directPaymentCks) {
						alert = alertBL.getAlertService().getAlertRecordInfo(
								directPayment.getDirectPaymentId(),
								DirectPayment.class.getSimpleName());
						if (null == alert)
							directPayments.add(directPayment);
					}
				}
				if (null != directPayments && directPayments.size() > 0)
					alertBL.alertDirectPaymentPDCCheque(directPayments,
							configurationVO);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void postTransactionAgainstPDCCheque() {
		try {
			LocalDate localeDate = DateTime.now().toLocalDate();
			List<DirectPayment> directPayments = directPaymentService
					.getPDCDirectPayments(DateFormat.convertStringToDate(DateFormat
							.convertDateToString(localeDate.toString())));
			if (null != directPayments && directPayments.size() > 0) {
				Map<Long, List<DirectPayment>> directPaymentMap = new HashMap<Long, List<DirectPayment>>();
				List<DirectPayment> processPayments = null;
				for (DirectPayment directPayment : directPayments) {
					processPayments = new ArrayList<DirectPayment>();
					if (directPaymentMap.containsKey(directPayment
							.getImplementation().getImplementationId())) {
						processPayments.addAll(directPaymentMap
								.get(directPayment.getImplementation()
										.getImplementationId()));
					}
					processPayments.add(directPayment);
					directPaymentMap.put(directPayment.getImplementation()
							.getImplementationId(), processPayments);
				}
				List<Transaction> transactions = null;
				for (Entry<Long, List<DirectPayment>> entry : directPaymentMap
						.entrySet()) {
					transactions = new ArrayList<Transaction>();
					for (DirectPayment directpayment : entry.getValue())
						transactions
								.add(createPDCReverseEntries(
										directpayment,
										new ArrayList<DirectPaymentDetail>(
												directpayment
														.getDirectPaymentDetails())));
					transactionBL.saveTransactions(transactions);
				}
				for (DirectPayment directPayment : directPayments) {
					directPayment.setIsPdc(false);
					directPayment.setCombination(directPayment.getBankAccount()
							.getCombination());
				}
				directPaymentService.saveDirectPayment(directPayments);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void postTransactionAgainstPDCCheque(DirectPayment directPayment)
			throws Exception {
		Transaction transaction = createPDCReverseEntries(
				directPayment,
				new ArrayList<DirectPaymentDetail>(directPayment
						.getDirectPaymentDetails()));
		transactionBL.saveTransaction(
				transaction,
				new ArrayList<TransactionDetail>(transaction
						.getTransactionDetails()));
		directPayment.setIsPdc(false);
		directPayment.setCombination(directPayment.getBankAccount()
				.getCombination());
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				directPayment.getDirectPaymentId(),
				DirectPayment.class.getSimpleName());
		alert.setIsActive(false);
		alertBL.getAlertService().updateAlert(alert);
		directPaymentService.saveDirectPayment(directPayment);
	}

	private Transaction createPDCReverseEntries(DirectPayment directPayment,
			List<DirectPaymentDetail> directPaymentDetails) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;
		for (DirectPaymentDetail dpDt : directPaymentDetails)
			totalAmount += dpDt.getAmount();
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, directPayment.getBankAccount().getCombination()
						.getCombinationId(), TransactionType.Credit.getCode(),
				"Direct Payment(PDC Clearance)", null,
				directPayment.getPaymentNumber()));
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, directPayment.getCombination().getCombinationId(),
				TransactionType.Debit.getCode(),
				"Direct Payment(PDC Clearance)", null,
				directPayment.getPaymentNumber()));
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						java.util.Calendar.getInstance().getTime(),
						directPayment.getImplementation()),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment for Payment No : "
						+ directPayment.getPaymentNumber(), DateFormat
						.convertStringToDate(DateFormat
								.convertDateToString(DateTime.now()
										.toLocalDate().toString())),
				transactionBL.getCategory("Direct Payment",
						directPayment.getImplementation()), directPayment
						.getDirectPaymentId(), DirectPayment.class
						.getSimpleName(), directPayment.getExchangeRate(),
				directPayment.getImplementation());
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	private void processTransaction(DirectPayment directPayment,
			List<DirectPaymentDetail> directPaymentDetails) throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(6l);
		transactionBL.deleteTransactionOnly(directPayment.getDirectPaymentId(),
				DirectPayment.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;
		String lineDescription = "";
		String description = null != directPayment.getDescription() ? directPayment
				.getDescription()
				+ " & Payment Ref: "
				+ directPayment.getPaymentNumber() : "Payment Ref: "
				+ directPayment.getPaymentNumber();
		for (DirectPaymentDetail list : directPaymentDetails) {
			if (null != list.getDescription()
					&& !("").equals(list.getDescription()))
				lineDescription = list.getDescription() + " & Payment Ref: "
						+ directPayment.getPaymentNumber();
			else
				lineDescription = "Payment Ref:"
						+ directPayment.getPaymentNumber();
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), lineDescription, null,
					list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, directPayment.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(), description, null,
				directPayment.getPaymentNumber()));
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						directPayment.getPaymentDate(), impl),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment for Payment No: "
						+ directPayment.getPaymentNumber(), directPayment
						.getPaymentDate(), transactionBL.getCategory(
						"Direct Payment", impl), directPayment
						.getDirectPaymentId(), DirectPayment.class
						.getSimpleName(), impl);
		transactionBL.saveTransactionOnly(transaction, transactionDetails);
	}

	public void updateDirectPaymentTransaction(
			List<DirectPayment> directPayments) throws Exception {
		for (DirectPayment directPayment : directPayments)
			this.processBusinessUpdates(
					directPayment,
					new ArrayList<DirectPaymentDetail>(directPayment
							.getDirectPaymentDetails()));
	}

	public void updateDirectPaymentTransaction() throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(6L);
		List<Transaction> transactions = transactionBL.getTransactionService()
				.getTransactionByImplementationAndUseCase(impl,
						DirectPayment.class.getSimpleName());
		for (Transaction transaction : transactions) {
			DirectPayment dp = directPaymentService
					.findDirectPaymentById(transaction.getRecordId());
			transaction.setTransactionTime(dp.getPaymentDate());
		}
		transactionBL.getTransactionService().saveJournalEntries(transactions);
	}

	private void processBusinessUpdates(DirectPayment directPayment,
			List<DirectPaymentDetail> directPaymentDetails) throws Exception {
		transactionBL.deleteTransaction(directPayment.getDirectPaymentId(),
				DirectPayment.class.getSimpleName());
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		double totalAmount = 0;
		String lineDescription = "";
		String description = null != directPayment.getDescription() ? directPayment
				.getDescription()
				+ " & Payment Ref: "
				+ directPayment.getPaymentNumber() : "Payment Ref: "
				+ directPayment.getPaymentNumber();
		for (DirectPaymentDetail list : directPaymentDetails) {
			if (null != list.getDescription()
					&& !("").equals(list.getDescription()))
				lineDescription = list.getDescription() + " & Payment Ref: "
						+ directPayment.getPaymentNumber();
			else
				lineDescription = "Payment Ref:"
						+ directPayment.getPaymentNumber();
			transactionDetails.add(transactionBL.createTransactionDetail(
					list.getAmount(), list.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), lineDescription, null,
					list.getInvoiceNumber()));
			totalAmount += list.getAmount();
		}
		transactionDetails.add(transactionBL.createTransactionDetail(
				totalAmount, directPayment.getCombination().getCombinationId(),
				TransactionType.Credit.getCode(), description, null,
				directPayment.getPaymentNumber()));
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						directPayment.getPaymentDate()),
				directPayment.getCurrency().getCurrencyId(),
				"Direct Payment for Payment No: "
						+ directPayment.getPaymentNumber(), directPayment
						.getPaymentDate(), transactionBL
						.getCategory("Direct Payment"), directPayment
						.getDirectPaymentId(), DirectPayment.class
						.getSimpleName());
		transactionBL.saveTransaction(transaction, transactionDetails);
	}

	public Implementation getImplementationId() {
		Implementation implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public DirectPaymentService getDirectPaymentService() {
		return directPaymentService;
	}

	public void setDirectPaymentService(
			DirectPaymentService directPaymentService) {
		this.directPaymentService = directPaymentService;
	}

	public List<DirectPayment> getDirectPaymentList() {
		return directPaymentList;
	}

	public void setDirectPaymentList(List<DirectPayment> directPaymentList) {
		this.directPaymentList = directPaymentList;
	}

	public BankService getBankService() {
		return bankService;
	}

	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public ChequeBookBL getChequeBookBL() {
		return chequeBookBL;
	}

	public void setChequeBookBL(ChequeBookBL chequeBookBL) {
		this.chequeBookBL = chequeBookBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public ConfigurationHandler getConfigurationHandler() {
		return configurationHandler;
	}

	@Autowired
	public void setConfigurationHandler(
			ConfigurationHandler configurationHandler) {
		this.configurationHandler = configurationHandler;
	}
}
