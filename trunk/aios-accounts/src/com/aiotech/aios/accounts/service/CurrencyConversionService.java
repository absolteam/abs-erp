package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CurrencyConversionService {

	private AIOTechGenericDAO<CurrencyConversion> currencyConversionDAO;

	public List<CurrencyConversion> getAllCurrencyConversion(
			Implementation implementation) throws Exception {
		return currencyConversionDAO.findByNamedQuery(
				"getAllCurrencyConversion", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public CurrencyConversion getCurrencyConversionById(
			Long currencyConversionId) throws Exception {
		return currencyConversionDAO.findByNamedQuery(
				"getCurrencyConversionById", currencyConversionId).get(0);
	}

	public CurrencyConversion getCurrencyConversionByCurrency(long currencyId) {
		List<CurrencyConversion> currencyConversions = currencyConversionDAO
				.findByNamedQuery("getCurrencyConversionByCurrency", currencyId);
		return null != currencyConversions && currencyConversions.size() > 0 ? currencyConversions
				.get(0) : null;
	}

	public void saveCurrencyConversion(CurrencyConversion currencyConversion)
			throws Exception {
		currencyConversionDAO.saveOrUpdate(currencyConversion);
	}

	public void deleteCurrencyConversion(CurrencyConversion currencyConversion)
			throws Exception {
		currencyConversionDAO.delete(currencyConversion);
	}

	// Getters & Setters
	public AIOTechGenericDAO<CurrencyConversion> getCurrencyConversionDAO() {
		return currencyConversionDAO;
	}

	public void setCurrencyConversionDAO(
			AIOTechGenericDAO<CurrencyConversion> currencyConversionDAO) {
		this.currencyConversionDAO = currencyConversionDAO;
	}
}
