package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.BankTransferService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class BankTransferBL {

	private Implementation implementation;
	private BankTransferService bankTransferService;
	private LookupMasterBL lookupMasterBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private TransactionBL transactionBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// list all bank transfers
	public JSONObject getAllBankTransfer() throws Exception {
		List<BankTransfer> bankTransferList = this.getBankTransferService()
				.getAllBankTransfer(getImplementation());
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (BankTransfer list : bankTransferList) {
			array = new JSONArray();
			array.add(list.getBankTransferId());
			array.add(list.getTransferNo());
			array.add(DateFormat.convertDateToString(list.getDate().toString()));
			array.add(null != list.getLookupDetail() ? list.getLookupDetail()
					.getDisplayName() : "");
			array.add(list.getCurrency().getCurrencyPool().getCode());
			array.add(list.getBankAccountByDebitBankAccount()
					.getAccountNumber());
			array.add(list.getBankAccountByCreditBankAccount()
					.getAccountNumber());
			array.add(AIOSCommons.formatAmount(list.getTransferAmount()));
			array.add(null != list.getTransferCharges()
					&& list.getTransferCharges() > 0 ? AIOSCommons
					.formatAmount(list.getTransferCharges()) : "");
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// save bank transfer
	public void saveBankTransfer(BankTransfer bankTransfer) throws Exception {
		bankTransfer.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (bankTransfer != null && bankTransfer.getBankTransferId() != null
				&& bankTransfer.getBankTransferId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		boolean updateFlag = false;
		if (null != bankTransfer.getBankTransferId())
			updateFlag = true;
		bankTransferService.saveBankTransfer(bankTransfer);
		if (updateFlag)
			transactionBL.deleteTransaction(bankTransfer.getBankTransferId(),
					BankTransfer.class.getSimpleName());
		else
			SystemBL.saveReferenceStamp(BankTransfer.class.getName(),
					getImplementation());
		saveBankTransferDocuments(bankTransfer, updateFlag);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankTransfer.class.getSimpleName(),
				bankTransfer.getBankTransferId(), user, workflowDetailVo);

	}

	public void doBankTransferBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		BankTransfer bankTransfer = bankTransferService
				.findBankTransferById(recordId);
		if (workflowDetailVO.isDeleteFlag()) {
			transactionBL.deleteTransaction(bankTransfer.getBankTransferId(),
					BankTransfer.class.getSimpleName());
			documentBL.getDocumentService().deleteDocuments(
					documentBL.getDocumentService()
							.getChildDocumentsByRecordId(
									bankTransfer.getBankTransferId(),
									"BankTransfer"));
			documentBL.getDocumentService().deleteDocuments(
					documentBL.getDocumentService()
							.getParentDocumentsByRecordId(
									bankTransfer.getBankTransferId(),
									"BankTransfer"));
			bankTransferService.deleteBankTransfer(bankTransfer);
		} else {
			Transaction transaction = bankTransferTransaction(bankTransfer);
			transactionBL.saveTransaction(
					transaction,
					new ArrayList<TransactionDetail>(transaction
							.getTransactionDetails()));
		}
	}

	// save bank transfer docs
	private void saveBankTransferDocuments(BankTransfer bankTransfer,
			boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(bankTransfer,
				"transferDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, bankTransfer.getBankTransferId(), "BankTransfer",
					"transferDocs");
			docVO.setDirStruct(dirStucture);
		}
		documentBL.saveUploadDocuments(docVO);
	}

	// delete bank transfer
	public void deleteBankTransfer(BankTransfer bankTransfer) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankTransfer.class.getSimpleName(),
				bankTransfer.getBankTransferId(), user, workflowDetailVo);
	}

	// save bank transfer transaction
	private Transaction bankTransferTransaction(BankTransfer bankTransfer)
			throws Exception {
		Transaction transaction = transactionBL.createTransaction(
				transactionBL.getCalendarBL().transactionPostingPeriod(
						bankTransfer.getDate()), bankTransfer.getCurrency()
						.getCurrencyId(), "Bank Transfer", bankTransfer.getDate(), transactionBL
						.getCategory("Internal Transfer"), bankTransfer
						.getBankTransferId(), BankTransfer.class
						.getSimpleName(), bankTransfer.getExchangeRate());
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		TransactionDetail debitTransaction = transactionBL
				.createTransactionDetail(bankTransfer.getTransferAmount(),
						bankTransfer.getBankAccountByDebitBankAccount()
								.getCombination().getCombinationId(),
						TransactionType.Debit.getCode(), "Bank Transfer :"
								+ bankTransfer
										.getBankAccountByDebitBankAccount()
										.getAccountNumber(), null, bankTransfer
								.getTransferNo());
		transactionDetails.add(debitTransaction);
		if (null != bankTransfer.getTransferCharges()
				&& bankTransfer.getTransferCharges() > 0) {
			TransactionDetail chargesTransaction = transactionBL
					.createTransactionDetail(bankTransfer.getTransferCharges(),
							bankTransfer.getCombination().getCombinationId(),
							TransactionType.Debit.getCode(),
							"Bank Transfer Charges", null,
							bankTransfer.getTransferNo());
			transactionDetails.add(chargesTransaction);
		}
		TransactionDetail creditTransaction = this
				.getTransactionBL()
				.createTransactionDetail(
						bankTransfer.getTransferAmount()
								+ (null != bankTransfer.getTransferCharges()
										&& !("").equals(bankTransfer
												.getTransferCharges()) ? bankTransfer.getTransferCharges()
										: 0),
						bankTransfer.getBankAccountByCreditBankAccount()
								.getCombination().getCombinationId(),
						TransactionType.Credit.getCode(),
						"Bank Transfer :"
								+ bankTransfer
										.getBankAccountByCreditBankAccount()
										.getAccountNumber(), null,
						bankTransfer.getTransferNo());
		transactionDetails.add(creditTransaction);
		transaction.setTransactionDetails(transactionDetails);
		return transaction;
	}

	// Revert bank transfer transaction
	@SuppressWarnings("unused")
	private Transaction revertBankTransferTransaction(
			BankTransfer revertBankTransfer) throws Exception {
		Transaction transaction = transactionBL
				.createTransaction(
						revertBankTransfer.getCurrency().getCurrencyId(),
						"Bank Transfer Reverted Ref:Account NO Debit: "
								+ revertBankTransfer
										.getBankAccountByDebitBankAccount()
										.getAccountNumber()
										.concat(" & Credit: "
												+ revertBankTransfer
														.getBankAccountByCreditBankAccount()
														.getAccountNumber()),
						new Date(), transactionBL
								.getCategory("Internal Transfers"),
						revertBankTransfer.getBankTransferId(),
						BankTransfer.class.getSimpleName());
		Set<TransactionDetail> transactionDetails = new HashSet<TransactionDetail>();
		TransactionDetail debitTransaction = this
				.getTransactionBL()
				.createTransactionDetail(
						revertBankTransfer.getTransferAmount()
								+ (null != revertBankTransfer.getTransferCharges()
										&& !("").equals(revertBankTransfer
												.getTransferCharges()) ? revertBankTransfer.getTransferCharges()
										: 0),
						revertBankTransfer.getBankAccountByCreditBankAccount()
								.getCombination().getCombinationId(),
						true,
						"Bank Transfer Reverted Ref:Account NO: "
								+ revertBankTransfer
										.getBankAccountByCreditBankAccount()
										.getAccountNumber(), null,
						revertBankTransfer.getTransferNo());
		transactionDetails.add(debitTransaction);
		if (null != revertBankTransfer.getTransferCharges()
				&& revertBankTransfer.getTransferCharges() > 0) {
			TransactionDetail chargesTransaction = transactionBL
					.createTransactionDetail(revertBankTransfer
							.getTransferCharges(), revertBankTransfer
							.getCombination().getCombinationId(), false,
							"Bank Transfer Charges Reverted", null,
							revertBankTransfer.getTransferNo());
			transactionDetails.add(chargesTransaction);
		}
		TransactionDetail creditTransaction = transactionBL
				.createTransactionDetail(
						revertBankTransfer.getTransferAmount(),
						revertBankTransfer.getBankAccountByDebitBankAccount()
								.getCombination().getCombinationId(), false,
						"Bank Transfer Reverted Ref:Account NO: "
								+ revertBankTransfer
										.getBankAccountByDebitBankAccount()
										.getAccountNumber(), null,
						revertBankTransfer.getTransferNo());
		transactionDetails.add(creditTransaction);
		transaction.setTransactionDetails(transactionDetails);
		return transaction;
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public BankTransferService getBankTransferService() {
		return bankTransferService;
	}

	public void setBankTransferService(BankTransferService bankTransferService) {
		this.bankTransferService = bankTransferService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

}
