package com.aiotech.aios.accounts.service;



public class AccountsEnterpriseService {

	private GLChartOfAccountService accountService;
	private GLCombinationService combinationService;
	private CategoryService categoryService;
	private LoanService loanService;
	private PaymentService paymentService;
	private CurrencyService currencyService; 
	private CalendarService calendarService;
	private BankDepositService bankDepositService;
	private TransactionService transactionService;
	private TransactionTempService transactionTempService;
	private DirectPaymentService directPaymentService;
	private PaymentRequestService paymentRequestService;
	private PettyCashService pettyCashService;
	private ReceiveService receiveService;
	private InvoiceService invoiceService;
	private ProductService productService;
	private StoreService storeService;
	private SupplierService supplierService;
	private GoodsReturnService goodsReturnService;
	private PurchaseService purchaseService;
	private DebitService debitService;
	private CustomerService customerService;
	private CreditTermService creditTermService;
	private CustomerQuotationService customerQuotationService;
	private SalesOrderService salesOrderService;
	private SalesDeliveryNoteService salesDeliveryNoteService;
	private SalesInvoiceService salesInvoiceService;
	private CreditService creditService;
	private BankReceiptsService bankReceiptsService;
	private BankTransferService bankTransferService;
	private BankService bankService;
	private IssueRequistionService issueRequistionService;
	private IssueReturnService issueReturnService;
	private ProductPricingService productPricingService;
	private StockService stockService;
	private AssetCheckOutService assetCheckOutService;
	private AssetCheckInService assetCheckInService;
	private AssetCreationService assetCreationService;
	private DiscountService discountService;
	private PromotionService promotionService;
	private ProductCategoryService productCategoryService;
	private ProductionReceiveService productionReceiveService;
	private MaterialTransferService materialTransferService;
	private MaterialRequisitionService materialRequisitionService;
	private POSUserTillService pOSUserTillService;
	private PointOfSaleService pointOfSaleService;
	private EODBalancingService eodBalancingService;
	private AssetDepreciationService assetDepreciationService;
	private WorkinProcessService workinProcessService;
	
	public GLChartOfAccountService getAccountService() {
		return accountService;
	}
	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}
	public GLCombinationService getCombinationService() {
		return combinationService;
	}
	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}
	public LoanService getLoanService() {
		return loanService;
	}
	public void setLoanService(LoanService loanService) {
		this.loanService = loanService;
	}
	public PaymentService getPaymentService() {
		return paymentService;
	}
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}
	public CategoryService getCategoryService() {
		return categoryService;
	}
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	public CurrencyService getCurrencyService() {
		return currencyService;
	}
	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}
	public CalendarService getCalendarService() {
		return calendarService;
	}
	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
	public BankDepositService getBankDepositService() {
		return bankDepositService;
	}
	public void setBankDepositService(BankDepositService bankDepositService) {
		this.bankDepositService = bankDepositService;
	}
	public TransactionService getTransactionService() {
		return transactionService;
	}
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
	public TransactionTempService getTransactionTempService() {
		return transactionTempService;
	}
	public void setTransactionTempService(
			TransactionTempService transactionTempService) {
		this.transactionTempService = transactionTempService;
	}
	public DirectPaymentService getDirectPaymentService() {
		return directPaymentService;
	}
	public void setDirectPaymentService(DirectPaymentService directPaymentService) {
		this.directPaymentService = directPaymentService;
	}
	public PaymentRequestService getPaymentRequestService() {
		return paymentRequestService;
	}
	public void setPaymentRequestService(PaymentRequestService paymentRequestService) {
		this.paymentRequestService = paymentRequestService;
	}
	public PettyCashService getPettyCashService() {
		return pettyCashService;
	}
	public void setPettyCashService(PettyCashService pettyCashService) {
		this.pettyCashService = pettyCashService;
	}
	public ReceiveService getReceiveService() {
		return receiveService;
	}
	public void setReceiveService(ReceiveService receiveService) {
		this.receiveService = receiveService;
	}
	public InvoiceService getInvoiceService() {
		return invoiceService;
	}
	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}
	public ProductService getProductService() {
		return productService;
	}
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	public StoreService getStoreService() {
		return storeService;
	}
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	public SupplierService getSupplierService() {
		return supplierService;
	}
	public void setSupplierService(SupplierService supplierService) {
		this.supplierService = supplierService;
	}
	public GoodsReturnService getGoodsReturnService() {
		return goodsReturnService;
	}
	public void setGoodsReturnService(GoodsReturnService goodsReturnService) {
		this.goodsReturnService = goodsReturnService;
	}
	public PurchaseService getPurchaseService() {
		return purchaseService;
	}
	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}
	public DebitService getDebitService() {
		return debitService;
	}
	public void setDebitService(DebitService debitService) {
		this.debitService = debitService;
	}
	public CustomerService getCustomerService() {
		return customerService;
	}
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	public CreditTermService getCreditTermService() {
		return creditTermService;
	}
	public void setCreditTermService(CreditTermService creditTermService) {
		this.creditTermService = creditTermService;
	}
	public CustomerQuotationService getCustomerQuotationService() {
		return customerQuotationService;
	}
	public void setCustomerQuotationService(
			CustomerQuotationService customerQuotationService) {
		this.customerQuotationService = customerQuotationService;
	}
	public SalesOrderService getSalesOrderService() {
		return salesOrderService;
	}
	public void setSalesOrderService(SalesOrderService salesOrderService) {
		this.salesOrderService = salesOrderService;
	}
	public SalesDeliveryNoteService getSalesDeliveryNoteService() {
		return salesDeliveryNoteService;
	}
	public void setSalesDeliveryNoteService(
			SalesDeliveryNoteService salesDeliveryNoteService) {
		this.salesDeliveryNoteService = salesDeliveryNoteService;
	}
	public SalesInvoiceService getSalesInvoiceService() {
		return salesInvoiceService;
	}
	public void setSalesInvoiceService(SalesInvoiceService salesInvoiceService) {
		this.salesInvoiceService = salesInvoiceService;
	}
	public CreditService getCreditService() {
		return creditService;
	}
	public void setCreditService(CreditService creditService) {
		this.creditService = creditService;
	}
	public BankReceiptsService getBankReceiptsService() {
		return bankReceiptsService;
	}
	public void setBankReceiptsService(BankReceiptsService bankReceiptsService) {
		this.bankReceiptsService = bankReceiptsService;
	}
	public BankTransferService getBankTransferService() {
		return bankTransferService;
	}
	public void setBankTransferService(BankTransferService bankTransferService) {
		this.bankTransferService = bankTransferService;
	}
	public BankService getBankService() {
		return bankService;
	}
	public void setBankService(BankService bankService) {
		this.bankService = bankService;
	}
	public IssueRequistionService getIssueRequistionService() {
		return issueRequistionService;
	}
	public void setIssueRequistionService(
			IssueRequistionService issueRequistionService) {
		this.issueRequistionService = issueRequistionService;
	}
	public IssueReturnService getIssueReturnService() {
		return issueReturnService;
	}
	public void setIssueReturnService(IssueReturnService issueReturnService) {
		this.issueReturnService = issueReturnService;
	}
	public ProductPricingService getProductPricingService() {
		return productPricingService;
	}
	public void setProductPricingService(ProductPricingService productPricingService) {
		this.productPricingService = productPricingService;
	}
	public StockService getStockService() {
		return stockService;
	}
	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}
	public AssetCheckOutService getAssetCheckOutService() {
		return assetCheckOutService;
	}
	public void setAssetCheckOutService(AssetCheckOutService assetCheckOutService) {
		this.assetCheckOutService = assetCheckOutService;
	}
	public AssetCheckInService getAssetCheckInService() {
		return assetCheckInService;
	}
	public void setAssetCheckInService(AssetCheckInService assetCheckInService) {
		this.assetCheckInService = assetCheckInService;
	}
	public AssetCreationService getAssetCreationService() {
		return assetCreationService;
	}
	public void setAssetCreationService(AssetCreationService assetCreationService) {
		this.assetCreationService = assetCreationService;
	}
	public DiscountService getDiscountService() {
		return discountService;
	}
	public void setDiscountService(DiscountService discountService) {
		this.discountService = discountService;
	}
	public PromotionService getPromotionService() {
		return promotionService;
	}
	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}
	public ProductCategoryService getProductCategoryService() {
		return productCategoryService;
	}
	public void setProductCategoryService(
			ProductCategoryService productCategoryService) {
		this.productCategoryService = productCategoryService;
	}
	public ProductionReceiveService getProductionReceiveService() {
		return productionReceiveService;
	}
	public void setProductionReceiveService(
			ProductionReceiveService productionReceiveService) {
		this.productionReceiveService = productionReceiveService;
	}
	public MaterialTransferService getMaterialTransferService() {
		return materialTransferService;
	}
	public void setMaterialTransferService(
			MaterialTransferService materialTransferService) {
		this.materialTransferService = materialTransferService;
	}
	public MaterialRequisitionService getMaterialRequisitionService() {
		return materialRequisitionService;
	}
	public void setMaterialRequisitionService(
			MaterialRequisitionService materialRequisitionService) {
		this.materialRequisitionService = materialRequisitionService;
	}
	public POSUserTillService getpOSUserTillService() {
		return pOSUserTillService;
	}
	public void setpOSUserTillService(POSUserTillService pOSUserTillService) {
		this.pOSUserTillService = pOSUserTillService;
	}
	public PointOfSaleService getPointOfSaleService() {
		return pointOfSaleService;
	}
	public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
		this.pointOfSaleService = pointOfSaleService;
	}
	public EODBalancingService getEodBalancingService() {
		return eodBalancingService;
	}
	public void setEodBalancingService(EODBalancingService eodBalancingService) {
		this.eodBalancingService = eodBalancingService;
	}
	public AssetDepreciationService getAssetDepreciationService() {
		return assetDepreciationService;
	}
	public void setAssetDepreciationService(
			AssetDepreciationService assetDepreciationService) {
		this.assetDepreciationService = assetDepreciationService;
	}
	public WorkinProcessService getWorkinProcessService() {
		return workinProcessService;
	}
	public void setWorkinProcessService(WorkinProcessService workinProcessService) {
		this.workinProcessService = workinProcessService;
	}
	
}
