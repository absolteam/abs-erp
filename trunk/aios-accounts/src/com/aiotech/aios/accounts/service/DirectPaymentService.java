package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DirectPaymentService {

	private AIOTechGenericDAO<DirectPayment> directPaymentDAO;
	private AIOTechGenericDAO<DirectPaymentDetail> directPaymentDetailDAO;

	public List<DirectPayment> getAllDirectPayments(
			Implementation implementation) throws Exception {
		return this.getDirectPaymentDAO().findByNamedQuery(
				"getAllDirectPayments", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<DirectPayment> getPDCDirectPayments(Date chequeDate)
			throws Exception {
		return directPaymentDAO
				.findByNamedQuery("getPDCDirectPayments", chequeDate,
						(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<DirectPayment> getPDCDirectPayments(Date chequeDate,
			Implementation implementation) throws Exception {
		return directPaymentDAO.findByNamedQuery(
				"getPDCDirectPaymentsWithImpl", implementation, chequeDate,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<DirectPayment> getAllDirectPaymentsOnly(
			Implementation implementation) throws Exception {
		return this.getDirectPaymentDAO().findByNamedQuery(
				"getAllDirectPaymentsOnly", implementation);
	}

	public List<DirectPayment> getDirectPaymentsByCurrentDate(
			Implementation implementation, Date currentDate) throws Exception {
		return this.getDirectPaymentDAO().findByNamedQuery(
				"getDirectPaymentsByCurrentDate", implementation, currentDate);
	}

	@SuppressWarnings("unchecked")
	public List<DirectPayment> getDirectPaymentsByPaymentNumber(
			Implementation implementation, Set<String> paymentNumbers)
			throws Exception {

		Criteria criteria = directPaymentDAO.createCriteria();

		criteria.createAlias("directPaymentDetails", "dpd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cm",
				CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("implementation", implementation));

		criteria.add(Restrictions.in("paymentNumber", paymentNumbers));

		Set<DirectPayment> uniqueRecord = new HashSet<DirectPayment>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<DirectPayment>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<DirectPayment> getFilteredDirectPayments(
			Implementation implementation, Date fromDate, Date toDate,
			Byte paymentModeId, long payeeTypeId, Boolean isBankAccount,
			Boolean isCustomer, Boolean isSupplier, Boolean isOthers,
			Boolean isPDC) throws Exception {

		Criteria criteria = directPaymentDAO.createCriteria();

		criteria.createAlias("directPaymentDetails", "dpd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("currency", "c", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("c.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankAccount", "ba",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ba.bank", "b", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cm",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cm.accountByNaturalAccountId", "na",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("voidPayments", "vp",
				CriteriaSpecification.LEFT_JOIN);
		// criteria.createAlias("release", "rl",
		// CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByPersonId", "p",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customer", "cus", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.personByPersonId", "cus_per",
				CriteriaSpecification.LEFT_JOIN);

		criteria.createAlias("cus.company", "cus_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.cmpDeptLocation", "cus_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus_cdl.company", "cus_cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("paymentDate", fromDate, toDate));
		}

		if (paymentModeId != null && paymentModeId > 0) {
			criteria.add(Restrictions.eq("paymentMode", paymentModeId));
		}

		if (isPDC) {
			criteria.add(Restrictions.eq("isPdc", true));
		}

		if (isBankAccount || isSupplier || isCustomer || isOthers) {
			Disjunction disjunction = Restrictions.disjunction();
			if (isBankAccount) {
				disjunction.add(Restrictions.isNotNull("bankAccount"));
			}

			if (isSupplier) {
				disjunction.add(Restrictions.isNotNull("supplier"));
			}

			if (isCustomer) {
				disjunction.add(Restrictions.isNotNull("customer"));
			}

			if (isOthers) {
				disjunction.add(Restrictions.isNotNull("others"));
			}
			criteria.add(disjunction);
		}

		Set<DirectPayment> uniqueRecord = new HashSet<DirectPayment>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<DirectPayment>(
				uniqueRecord) : null;
	}

	public DirectPayment getDirectPaymentById(long directPaymentById)
			throws Exception {
		return this.getDirectPaymentDAO()
				.findByNamedQuery("getDirectPaymentById", directPaymentById)
				.get(0);
	}

	public List<DirectPayment> getDirectPaymentByPresentedCheques(
			long bankAccountId, Date startDate, Date endDate) throws Exception {
		return directPaymentDAO.findByNamedQuery(
				"getDirectPaymentByPresentedCheques", bankAccountId, startDate,
				endDate);
	}

	public DirectPayment findDirectPaymentById(long directPaymentById)
			throws Exception {
		return this.directPaymentDAO.findById(directPaymentById);
	}

	public DirectPayment getDirectPaymentByRecordIdAndUseCase(
			long directPaymentById, String useCase) throws Exception {
		List<DirectPayment> directPayments = directPaymentDAO.findByNamedQuery(
				"getDirectPaymentByRecordIdAndUseCase", directPaymentById,
				useCase);
		return null != directPayments && directPayments.size() > 0 ? directPayments
				.get(0) : null;
	}

	public DirectPayment getDirectPaymentDetail(long directPaymentById)
			throws Exception {
		return this.getDirectPaymentDAO()
				.findByNamedQuery("getDirectPaymentDetail", directPaymentById)
				.get(0);
	}

	public List<DirectPayment> getAllDirectPaymentDetail(String queryStr)
			throws Exception {
		Query qry = this.getDirectPaymentDAO().getNamedQuery(
				"getAllDirectPaymentDetail");
		queryStr = qry.getQueryString() + queryStr;
		return this.getDirectPaymentDAO().find(queryStr);
	}

	public List<DirectPaymentDetail> getPaymentDetail(
			DirectPayment directPayment) throws Exception {
		return this.getDirectPaymentDetailDAO().findByNamedQuery(
				"getPaymentDetail", directPayment);
	}

	public List<DirectPaymentDetail> getAllDirectPaymentDetails(
			Implementation implementation) throws Exception {
		return this.getDirectPaymentDetailDAO().findByNamedQuery(
				"getAllDirectPaymentDetails", implementation);
	}

	public List<DirectPaymentDetail> getCashDirectPaymentDetails(
			Implementation implementation, byte paymentMode) throws Exception {
		return this.getDirectPaymentDetailDAO().findByNamedQuery(
				"getCashDirectPaymentDetails", implementation, paymentMode);
	}

	public void deletePayment(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetail) throws Exception {
		this.getDirectPaymentDetailDAO().deleteAll(paymentDetail);
		this.getDirectPaymentDAO().delete(directPayment);
	}

	public void deletePaymentDetail(List<DirectPaymentDetail> paymentDetail)
			throws Exception {
		directPaymentDetailDAO.deleteAll(paymentDetail);
	}

	public void saveDirectPayment(DirectPayment directPayment) throws Exception {
		directPaymentDAO.saveOrUpdate(directPayment);
	}

	public void saveDirectPayment(List<DirectPayment> directPayments)
			throws Exception {
		directPaymentDAO.saveOrUpdateAll(directPayments);
	}

	public void mergeDirectPayment(DirectPayment directPayment)
			throws Exception {
		directPaymentDAO.merge(directPayment);
	}

	public void saveDirectPayments(DirectPayment directPayment,
			List<DirectPaymentDetail> paymentDetailList) throws Exception {
		this.getDirectPaymentDAO().saveOrUpdate(directPayment);
		for (DirectPaymentDetail list : paymentDetailList) {
			list.setDirectPayment(directPayment);
		}
		this.getDirectPaymentDetailDAO().saveOrUpdateAll(paymentDetailList);
	}

	// show all cheque payments
	public List<DirectPayment> getDirectChequePayments(
			Implementation implementation) throws Exception {
		return this.getDirectPaymentDAO().findByNamedQuery(
				"getDirectChequePayments", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	@SuppressWarnings("unchecked")
	public List<DirectPayment> getChequeDirectPayments(
			Implementation implementation, String fromdate, String todate)
			throws Exception {
		Criteria criteria = directPaymentDAO.createCriteria();
		criteria.createAlias("directPaymentDetails", "dpd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankAccount", "ba",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ba.bank", "bk", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByPersonId", "ppid",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customer", "cst", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "cstpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cstcp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.cmpDeptLocation", "cstcdpt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cstcdpt.company", "cstcdptcpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "sppr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "spcp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "spcdpt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("spcdpt.company", "spcdptcpy",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null)
			criteria.add(Restrictions.eq("implementation", implementation));
		criteria.add(Restrictions.isNotNull("bankAccount"));
		criteria.add(Restrictions.and(
				Restrictions.isNotNull("chequePresented"),
				Restrictions.eq("chequePresented", true)));
		if (fromdate != null && !("").equals(fromdate))
			criteria.add(Restrictions.and(
					Restrictions.isNotNull("chequeDate"),
					Restrictions.gt("chequeDate",
							DateFormat.convertStringToDate(fromdate))));
		if (todate != null && !("").equals(todate))
			criteria.add(Restrictions.and(
					Restrictions.isNotNull("chequeDate"),
					Restrictions.le("chequeDate",
							DateFormat.convertStringToDate(todate))));
		Set<DirectPayment> uniqueRecord = new HashSet<DirectPayment>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<DirectPayment>(
				uniqueRecord) : null;
	}

	public DirectPayment getDirectPaymentByReference(String referenceNumber,
			Implementation implementation) {
		List<DirectPayment> directPayments = directPaymentDAO.findByNamedQuery(
				"getDirectPaymentByReference", implementation, referenceNumber);
		return null != directPayments && directPayments.size() > 0 ? directPayments
				.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<DirectPayment> getCtPDCDirectPayments(Date chequeDate,
			Implementation implementation, Set<Long> dpIds) throws Exception {
		Criteria criteria = directPaymentDAO.createCriteria();
		criteria.createAlias("directPaymentDetails", "dpd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankAccount", "ba",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.add(Restrictions.isNotNull("bankAccount"));
		criteria.add(Restrictions.eq("isPdc", true));
		criteria.add(Restrictions.and(Restrictions.isNotNull("chequeDate"),
				Restrictions.le("chequeDate", chequeDate)));
		if(null != dpIds)
			criteria.add(Restrictions.not(Restrictions.in("directPaymentId", dpIds)));
		Set<DirectPayment> uniqueRecord = new HashSet<DirectPayment>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<DirectPayment>(
				uniqueRecord) : null;
	}

	public AIOTechGenericDAO<DirectPayment> getDirectPaymentDAO() {
		return directPaymentDAO;
	}

	public void setDirectPaymentDAO(
			AIOTechGenericDAO<DirectPayment> directPaymentDAO) {
		this.directPaymentDAO = directPaymentDAO;
	}

	public AIOTechGenericDAO<DirectPaymentDetail> getDirectPaymentDetailDAO() {
		return directPaymentDetailDAO;
	}

	public void setDirectPaymentDetailDAO(
			AIOTechGenericDAO<DirectPaymentDetail> directPaymentDetailDAO) {
		this.directPaymentDetailDAO = directPaymentDetailDAO;
	}
}
