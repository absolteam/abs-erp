package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductPoint;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ProductPointService {

	private AIOTechGenericDAO<ProductPoint> productPointDAO;

	// Get all Product Points
	public List<ProductPoint> getAllProductPoints(Implementation implementation)
			throws Exception {
		return productPointDAO.findByNamedQuery("getAllProductPoints",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get Active Product Points
	public List<ProductPoint> getActiveProductPoints(
			Implementation implementation, Date time) {
		return productPointDAO.findByNamedQuery("getActiveProductPoints",
				implementation, time);
	}

	// Get Product Point by Id
	public ProductPoint getProductPointById(long productPointId)
			throws Exception {
		return productPointDAO.findByNamedQuery("getProductPointById",
				productPointId).get(0);
	}

	// Get product by Product Id
	public ProductPoint getProductPointsByProductId(long productId,  Date toDate) {
		List<ProductPoint> productPoints = productPointDAO.findByNamedQuery(
				"getProductPointsByProductId", productId, toDate);
		return null != productPoints && productPoints.size() > 0 ? productPoints
				.get(0) : null;
	}

	// Save Product Point
	public void saveProductPoint(ProductPoint productPoint) throws Exception {
		productPointDAO.saveOrUpdate(productPoint);
	}

	// Delete Product Point
	public void deleteProductPoint(ProductPoint productPoint) throws Exception {
		productPointDAO.delete(productPoint);
	}

	// Getters & Setters
	public AIOTechGenericDAO<ProductPoint> getProductPointDAO() {
		return productPointDAO;
	}

	public void setProductPointDAO(
			AIOTechGenericDAO<ProductPoint> productPointDAO) {
		this.productPointDAO = productPointDAO;
	}
}
