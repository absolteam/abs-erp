package com.aiotech.aios.accounts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

@Service
@Transactional
public class BankDepositDetailService {

	private AIOTechGenericDAO<BankDepositDetail> bankDepositDetailDAO;

	public AIOTechGenericDAO<BankDepositDetail> getBankDepositDetailDAO() {
		return bankDepositDetailDAO;
	}
	
	@Autowired
	public void setBankDepositDetailDAO(
			AIOTechGenericDAO<BankDepositDetail> bankDepositDetailDAO) {
		this.bankDepositDetailDAO = bankDepositDetailDAO;
	} 
}
