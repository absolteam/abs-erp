package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.TenderDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class TenderService {

	private AIOTechGenericDAO<Tender> tenderDAO;
	private AIOTechGenericDAO<TenderDetail> tenderDetailDAO;

	public List<Tender> getAllTenders(Implementation implementation) {
		return tenderDAO.findByNamedQuery("getAllTenders", implementation);
	}

	public List<Tender> getUnQuotedTenders(Implementation implementation)
			throws Exception {
		return tenderDAO
				.findByNamedQuery("getUnQuotedTenders", implementation,
						new Date(),
						(byte) WorkflowConstants.Status.Published.getCode());
	}

	public Tender getTenderById(long id) {
		return tenderDAO.findById(id);
	}

	// Get Tender Basic Details
	public Tender getTenderBasicById(long tenderId) throws Exception {
		return tenderDAO.findByNamedQuery("getTenderBasicById", tenderId)
				.get(0);
	}

	public List<TenderDetail> getTenderDetails(long tenderId) {
		return tenderDetailDAO.findByNamedQuery("getTenderDetails", tenderId);
	}

	public void deleteTender(Tender tender, List<TenderDetail> tenderDetails)
			throws Exception {
		tenderDetailDAO.deleteAll(tenderDetails);
		tenderDAO.delete(tender);
	}

	public void saveTender(Tender tender) throws Exception {
		tenderDAO.saveOrUpdate(tender);
		for (TenderDetail detail : tender.getTenderDetails())
			detail.setTender(tender);
		tenderDetailDAO.saveOrUpdateAll(tender.getTenderDetails());
	}

	@SuppressWarnings("unchecked")
	public List<Tender> getTendersByFilterValues(Date fromDate, Date toDate,
			Long productId, Boolean isQuoted, Implementation implementation)
			throws Exception {

		Criteria quotationCriteria = tenderDAO.createCriteria();

		if (fromDate != null && toDate != null) {
			quotationCriteria.add(Restrictions.between("tenderDate", fromDate,
					toDate));
		}

		if (productId != null) {
			Criteria tenderDetailCritria = quotationCriteria
					.createCriteria("tenderDetails");
			tenderDetailCritria.add(Restrictions.eq("product.productId",
					productId));

		}

		if (isQuoted != null) {
			if (isQuoted) {
				quotationCriteria.createCriteria("quotations");
			}

		}

		if (implementation != null) {
			quotationCriteria.createCriteria("implementation").add(
					Restrictions.eq("implementationId",
							implementation.getImplementationId()));
		}

		return quotationCriteria.list();

	}

	@SuppressWarnings("unchecked")
	public Tender getTenderByIdWithDifferentChilds(Long tenderId,
			Boolean isCurrency, Boolean isTenderDetails, Boolean isQuotations) {

 		Criteria tenderCriteria = tenderDAO.createCriteria();

		if (isCurrency){
			tenderCriteria.createAlias("currency", "cr",
					CriteriaSpecification.LEFT_JOIN);
			tenderCriteria.createAlias("cr.currencyPool", "cp",
					CriteriaSpecification.LEFT_JOIN);
		}
		if (isTenderDetails) {
			tenderCriteria.createAlias("tenderDetails", "td",
					CriteriaSpecification.LEFT_JOIN);
			tenderCriteria.createAlias("td.product", "prd",
					CriteriaSpecification.LEFT_JOIN);
			tenderCriteria.createAlias("prd.lookupDetailByProductUnit", "unit",
					CriteriaSpecification.LEFT_JOIN);
		}

		if (isQuotations)
			tenderCriteria.createAlias("quotations", "qd",
					CriteriaSpecification.LEFT_JOIN);

		tenderCriteria.add(Restrictions.eq("tenderId", tenderId));

		Set<Tender> uniqueRecords = new HashSet<Tender>(tenderCriteria.list());
		if (uniqueRecords.size() > 0) {
			List<Tender> tenders = new ArrayList<Tender>(uniqueRecords);
			return tenders.get(0);
		} 
		return null; 
	}

	public AIOTechGenericDAO<Tender> getTenderDAO() {
		return tenderDAO;
	}

	public AIOTechGenericDAO<TenderDetail> getTenderDetailDAO() {
		return tenderDetailDAO;
	}

	public void setTenderDAO(AIOTechGenericDAO<Tender> tenderDAO) {
		this.tenderDAO = tenderDAO;
	}

	public void setTenderDetailDAO(
			AIOTechGenericDAO<TenderDetail> tenderDetailDAO) {
		this.tenderDetailDAO = tenderDetailDAO;
	}
}
