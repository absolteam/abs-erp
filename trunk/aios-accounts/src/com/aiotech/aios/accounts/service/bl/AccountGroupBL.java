package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AccountGroupDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AccountGroupVO;
import com.aiotech.aios.accounts.service.AccountGroupService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;

public class AccountGroupBL {

	private AccountGroupService accountGroupService;
	private CombinationBL combinationBL; 

	public List<Object> getAllAccountGroups() throws Exception {
		List<AccountGroup> accountGroups = accountGroupService
				.getAllAccountGroups(getImplementation());
		List<Object> accountGroupVOs = new ArrayList<Object>();
		if (null != accountGroups && accountGroups.size() > 0) {
			for (AccountGroup accountGroup : accountGroups)
				accountGroupVOs.add(addAccountGroup(accountGroup));
		}
		return accountGroupVOs;
	}

	public void saveAccountGroup(AccountGroup accountGroup,
			List<AccountGroupDetail> accountGroupDetails,
			List<AccountGroupDetail> deletedAccountGroupDetails,
			List<AccountGroupDetail> existingAccountGroupDetails)
			throws Exception {
		accountGroup.setImplementation(getImplementation());
		if (null != deletedAccountGroupDetails
				&& deletedAccountGroupDetails.size() > 0) {
			accountGroupService
					.deleteAccountGroupDetail(deletedAccountGroupDetails);
		}
		
		if(accountGroup.getAccountGroupId()!=null){
			SystemBL.saveReferenceStamp(AccountGroup.class.getName(),
					getImplementation());
		}
		accountGroupService.saveAccountGroup(accountGroup, accountGroupDetails);

	}

	public void deleteAccountGroup(AccountGroup accountGroup,
			List<AccountGroupDetail> accountGroupDetails) throws Exception {

		accountGroupService.deleteAccountGroup(accountGroup,
				accountGroupDetails);
	}

	private AccountGroupVO addAccountGroup(AccountGroup accountGroup) {
		AccountGroupVO accountGroupVO = new AccountGroupVO();
		accountGroupVO.setAccountGroupId(accountGroup.getAccountGroupId());
		accountGroupVO.setAccountType(accountGroup.getAccountType());
		accountGroupVO.setTitle(accountGroup.getTitle());
		String accountType = "";
		switch (accountGroup.getAccountType()) {
		case 1:
			accountType = Constants.Accounts.AccountType.Assets.name();
			break;
		case 2:
			accountType = Constants.Accounts.AccountType.Liabilites.name();
			break;
		case 3:
			accountType = Constants.Accounts.AccountType.Revenue.name();
			break;
		case 4:
			accountType = Constants.Accounts.AccountType.Expenses.name();
			break;
		case 5:
			accountType = Constants.Accounts.AccountType.OwnersEquity.name();
			break;
		}
		accountGroupVO.setAccountTypeName(accountType);
		accountGroupVO.setCode(accountGroup.getCode());
		accountGroupVO.setDescription(accountGroup.getDescription());
		return accountGroupVO;
	}

	public List<AccountGroupVO> detailsConversion(
			List<AccountGroupDetail> details) throws Exception {
		List<AccountGroupVO> detailVOs = new ArrayList<AccountGroupVO>();
		for (AccountGroupDetail detail : details) {
			detailVOs.add(convertDetailToVO(detail));
		}

		return detailVOs;
	}

	private AccountGroupVO convertDetailToVO(
			AccountGroupDetail accountGroupDetail) throws Exception {
		AccountGroupVO accountGroupVO = new AccountGroupVO();
		accountGroupVO.setAccountGroupDetail(accountGroupDetail);
		String name = "";
		String code="[";
		name = name
				+ ""
				+ accountGroupDetail.getCombination()
						.getAccountByCompanyAccountId().getAccount();
		code = code
			+ ""+accountGroupDetail.getCombination()
			.getAccountByCompanyAccountId().getCode();
		
		name = name
				+ " >> "
				+ accountGroupDetail.getCombination()
						.getAccountByCostcenterAccountId().getAccount();
		
		code = code
		+ "."+accountGroupDetail.getCombination()
		.getAccountByCostcenterAccountId().getCode();
		
		if (accountGroupDetail.getCombination().getAccountByNaturalAccountId() != null){
			name = name
					+ " >> "
					+ accountGroupDetail.getCombination()
							.getAccountByNaturalAccountId().getAccount();
			code = code
			+ "."+accountGroupDetail.getCombination()
			.getAccountByNaturalAccountId().getCode();
		}

		if (accountGroupDetail.getCombination().getAccountByAnalysisAccountId() != null){
			name = name
					+ " >> "
					+ accountGroupDetail.getCombination()
							.getAccountByAnalysisAccountId().getAccount();
			
			code = code
			+ "."+accountGroupDetail.getCombination()
			.getAccountByAnalysisAccountId().getCode();
		}

		code = code+"]";
		accountGroupVO.setCombinationName(name +" "+code);

		return accountGroupVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public AccountGroupService getAccountGroupService() {
		return accountGroupService;
	}

	public void setAccountGroupService(AccountGroupService accountGroupService) {
		this.accountGroupService = accountGroupService;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	// Getters & Setters

}
