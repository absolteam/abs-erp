/**
 * 
 */
package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.IssueReturn;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.IssueReturnService;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * @author Saleem
 */
public class IssueReturnBL {

	// Dependencies
	private IssueRequistionBL issueRequistionBL;
	private IssueReturnService issueReturnService;
	private MaterialTransferBL materialTransferBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Get all Issue Returns
	public JSONObject getAllIssueReturn() throws Exception {
		JSONObject jsonResponse = new JSONObject();
		List<IssueReturn> issueReturns = issueReturnService
				.getAllIssueReturn(getImplementation());
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (IssueReturn issueReturn : issueReturns) {
			array = new JSONArray();
			array.add(issueReturn.getIssueReturnId());
			array.add(issueReturn.getReferenceNumber());
			array.add(DateFormat.convertDateToString(issueReturn
					.getReturnDate().toString()));
			array.add(null != issueReturn.getCmpDeptLocation() ? issueReturn
					.getCmpDeptLocation().getLocation().getLocationName() : "");
			array.add(null != issueReturn.getStore() ? issueReturn.getStore()
					.getStoreName() : "");
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// Save Issue Returns
	public void saveIssueReturns(IssueReturn issueReturn,
			List<IssueReturnDetail> issueReturnDetails) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		boolean updateFlag = false;
		if (null != issueReturn.getIssueReturnId())
			updateFlag = true;
		issueReturn.setImplementation(getImplementation());
		issueReturn.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (issueReturn != null && issueReturn.getIssueReturnId() != null
				&& issueReturn.getIssueReturnId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (updateFlag) {
			issueRequistionBL.getTransactionBL().deleteTransaction(
					issueReturn.getIssueReturnId(),
					IssueReturn.class.getSimpleName());
			// Update stock details
			List<Stock> stocks = new ArrayList<Stock>();
			Stock stock = null;
			for (IssueReturnDetail issueReturnDetail : issueReturnDetails) {
				stock = new Stock();
				if (null != issueReturn.getCmpDeptLocation()) {
					IssueRequistionDetail issueRequistionDetail = issueRequistionBL
							.getIssueRequistionService()
							.getIssueRequistionDetailById(
									issueReturnDetail
											.getIssueRequistionDetail()
											.getIssueRequistionDetailId());
					stock.setProduct(issueRequistionDetail.getProduct());
					if (null != issueRequistionDetail.getShelf()) {
						stock.setStore(issueRequistionDetail.getShelf()
								.getShelf().getAisle().getStore());
						stock.setShelf(issueRequistionDetail.getShelf());
					}
					stock.setQuantity(issueReturnDetail.getReturnQuantity());
				} else {
					MaterialTransferDetail materialTransferDetail = materialTransferBL
							.getMaterialTransferService()
							.getMaterialTransferDetailById(
									issueReturnDetail
											.getMaterialTransferDetail()
											.getMaterialTransferDetailId());
					stock.setStore(materialTransferDetail.getShelfByShelfId()
							.getShelf().getAisle().getStore());
					stock.setShelf(materialTransferDetail.getShelfByShelfId());
					stock.setQuantity(issueReturnDetail.getReturnQuantity());
				}
				stocks.add(stock);
			}
			if (null != issueReturn.getCmpDeptLocation()) {
				issueRequistionBL.getStockBL().deleteBatchStockDetails(stocks);
			}
		} else
			SystemBL.saveReferenceStamp(IssueReturn.class.getName(),
					getImplementation());

		issueReturn.setIssueReturnDetails(new HashSet<IssueReturnDetail>(
				issueReturnDetails));

		issueReturnService.saveIssueReturns(issueReturn, issueReturnDetails);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IssueReturn.class.getSimpleName(),
				issueReturn.getIssueReturnId(), user, workflowDetailVo);
	}

	public void doIssueReturnBusinessUpdate(Long issueReturnId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		IssueReturn issueReturn = issueReturnService
				.getIssueReturnById(issueReturnId);
		if (!workflowDetailVO.isDeleteFlag()) {
			if (null != issueReturn.getCmpDeptLocation()) {
				List<Transaction> transactions = issueReturnTransaction(
						issueReturn,
						"Issue Return Material "
								+ issueReturn.getReferenceNumber(),
						new ArrayList<IssueReturnDetail>(issueReturn
								.getIssueReturnDetails()),
						TransactionType.Debit.getCode(),
						TransactionType.Credit.getCode());
				for (Transaction transaction : transactions) {
					issueRequistionBL.getTransactionBL().saveTransaction(
							transaction,
							new ArrayList<TransactionDetail>(transaction
									.getTransactionDetails()));
				}
				// Update stock details
				List<Stock> stocks = new ArrayList<Stock>();
				Stock stock = null;
				for (IssueReturnDetail issueReturnDetail : issueReturn
						.getIssueReturnDetails()) {
					IssueRequistionDetail issueRequistionDetail = issueRequistionBL
							.getIssueRequistionService()
							.getIssueRequistionDetailById(
									issueReturnDetail
											.getIssueRequistionDetail()
											.getIssueRequistionDetailId());
					stock = new Stock();
					stock.setProduct(issueRequistionDetail.getProduct());
					if(null!=issueRequistionDetail.getShelf()){
						stock.setStore(issueRequistionDetail.getShelf().getShelf()
								.getAisle().getStore());
						stock.setShelf(issueRequistionDetail.getShelf());
					} 
					stock.setQuantity(issueReturnDetail.getReturnQuantity());
					stocks.add(stock);
				}
				issueRequistionBL.getStockBL().updateBatchStockDetails(stocks);
			}
		} else {
			issueRequistionBL.getTransactionBL().deleteTransaction(
					issueReturn.getIssueReturnId(),
					IssueReturn.class.getSimpleName());
			List<Stock> stocks = new ArrayList<Stock>();
			Stock stock = null;
			if (null != issueReturn.getCmpDeptLocation()) {
				for (IssueReturnDetail issueReturnDetail : issueReturn
						.getIssueReturnDetails()) {
					IssueRequistionDetail issueRequistionDetail = issueRequistionBL
							.getIssueRequistionService()
							.getIssueRequistionDetailById(
									issueReturnDetail
											.getIssueRequistionDetail()
											.getIssueRequistionDetailId());
					stock = new Stock();
					stock.setProduct(issueRequistionDetail.getProduct());
					stock.setStore(issueRequistionDetail.getShelf().getShelf()
							.getAisle().getStore());
					stock.setShelf(issueRequistionDetail.getShelf());
					stock.setQuantity(issueReturnDetail.getReturnQuantity());
					stocks.add(stock);
				}
				issueRequistionBL.getStockBL().updateStockDetails(null, stocks);
			}
			issueReturnService.deleteIssueReturns(
					issueReturn,
					new ArrayList<IssueReturnDetail>(issueReturn
							.getIssueReturnDetails()));
		}
	}

	// Delete Issue Returns
	public void deleteIssueReturns(IssueReturn issueReturn,
			List<IssueReturnDetail> issueReturnDetails) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				IssueReturn.class.getSimpleName(),
				issueReturn.getIssueReturnId(), user, workflowDetailVo);
	}

	// Create Issue Return Transaction
	private List<Transaction> issueReturnTransaction(IssueReturn issueReturn,
			String description, List<IssueReturnDetail> issueReturnDetails,
			boolean debitFlag, boolean creditFlag) throws Exception {
		double transactionAmount = 0;
		Set<TransactionDetail> transactionDetails = null;
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (IssueReturnDetail issueReturnDetail : issueReturnDetails) {
			IssueRequistion issueRequistion = issueRequistionBL
					.getIssueRequistionService().getIssueRequistionByDetailId(
							issueReturnDetail.getIssueRequistionDetail()
									.getIssueRequistionDetailId());
			for (IssueRequistionDetail issueRequistionDetail : issueRequistion
					.getIssueRequistionDetails()) {
				transactionDetails = new HashSet<TransactionDetail>();
				if (issueRequistionDetail.getIssueRequistionDetailId().equals(
						issueReturnDetail.getIssueRequistionDetail()
								.getIssueRequistionDetailId())) {
					// Create Transaction Detail
					transactionDetails
							.add(createDebitEntry(issueReturn,
									issueRequistionDetail, issueReturnDetail,
									debitFlag));
					transactionDetails.add(createCreditEntry(issueReturn,
							issueRequistionDetail, issueReturnDetail,
							creditFlag));
					break;
				}
			}
			// Create Transaction
			Transaction transaction = createTransaction(issueReturn,
					description, transactionAmount);
			transaction.setTransactionDetails(transactionDetails);
			transactions.add(transaction);
		}

		return transactions;
	}

	private TransactionDetail createCreditEntry(IssueReturn issueReturn,
			IssueRequistionDetail issueRequistionDetail,
			IssueReturnDetail issueReturnDetail, boolean debitFlag)
			throws Exception {
		Combination combination = new Combination();
		combination.setCombinationId(getImplementation().getExpenseAccount());
		TransactionDetail transactionDetail = issueRequistionBL
				.getTransactionBL()
				.createTransactionDetail(
						(issueRequistionDetail.getUnitRate() * issueReturnDetail
								.getReturnQuantity()),
						combination.getCombinationId(), debitFlag, null, null,
						issueReturn.getReferenceNumber());
		return transactionDetail;
	}

	// Create Transaction Detail Entry
	private TransactionDetail createDebitEntry(IssueReturn issueReturn,
			IssueRequistionDetail issueRequistionDetail,
			IssueReturnDetail issueReturnDetail, boolean debitFlag)
			throws Exception {
		Combination combination = issueRequistionBL.getProductBL()
				.getProductInventory(
						issueRequistionDetail.getProduct().getProductId());
		TransactionDetail transactionDetail = issueRequistionBL
				.getTransactionBL()
				.createTransactionDetail(
						(issueRequistionDetail.getUnitRate() * issueReturnDetail
								.getReturnQuantity()),
						combination.getCombinationId(), debitFlag, null, null,
						issueReturn.getReferenceNumber());
		return transactionDetail;
	}

	// Create TransactionEntry
	private Transaction createTransaction(IssueReturn issueReturn,
			String description, double transactionAmount) throws Exception {
		return issueRequistionBL.getTransactionBL().createTransaction(
				issueRequistionBL.getTransactionBL().getCurrency()
						.getCurrencyId(),
				description,
				issueReturn.getReturnDate(),
				issueRequistionBL.getTransactionBL().getCategory(
						"Issue Returns"), issueReturn.getIssueReturnId(),
				IssueReturn.class.getSimpleName());
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters and Setters
	public IssueReturnService getIssueReturnService() {
		return issueReturnService;
	}

	public void setIssueReturnService(IssueReturnService issueReturnService) {
		this.issueReturnService = issueReturnService;
	}

	public IssueRequistionBL getIssueRequistionBL() {
		return issueRequistionBL;
	}

	public void setIssueRequistionBL(IssueRequistionBL issueRequistionBL) {
		this.issueRequistionBL = issueRequistionBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public MaterialTransferBL getMaterialTransferBL() {
		return materialTransferBL;
	}

	public void setMaterialTransferBL(MaterialTransferBL materialTransferBL) {
		this.materialTransferBL = materialTransferBL;
	}

}
