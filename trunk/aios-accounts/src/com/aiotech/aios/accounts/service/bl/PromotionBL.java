package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Promotion;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.PromotionOption;
import com.aiotech.aios.accounts.domain.entity.vo.PromotionVO;
import com.aiotech.aios.accounts.service.PromotionService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PromotionBL {

	// Dependencies
	private PromotionService promotionService;
	private LookupMasterBL lookupMasterBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show all Product points
	public List<Object> showAllPromotions() throws Exception {
		List<Promotion> promotions = promotionService
				.getAllPromotions(getImplementation());
		List<Object> promotionVOs = new ArrayList<Object>();
		if (null != promotions && promotions.size() > 0)
			for (Promotion promotion : promotions)
				promotionVOs.add(addPromotionVO(promotion));
		return promotionVOs;
	}

	// Add Promotion VO
	private Object addPromotionVO(Promotion promotion) {
		PromotionVO promotionVO = new PromotionVO();
		promotionVO.setPromotionId(promotion.getPromotionId());
		promotionVO.setPromotionName(promotion.getPromotionName());
		promotionVO
				.setMininumSalesName(null != promotion.getMiniumSales() ? (promotion
						.getMiniumSales() == 'A' ? "Amount" : "Quantity") : "");
		promotionVO.setSalesValue(promotion.getSalesValue());
		promotionVO
				.setStatusName(promotion.getStatus() ? "Active" : "Inactive");
		promotionVO.setPromotionOptionName(null != promotion
				.getPromotionOption() ? Constants.Accounts.DiscountOptions.get(
				promotion.getPromotionOption()).name() : "");
		promotionVO.setRewardTypeName(Constants.Accounts.PromotionRewardName
				.get(promotion.getRewardType()).name().replaceAll("_", " "));
		promotionVO.setPriceListName(promotion.getLookupDetail()
				.getDisplayName());
		return promotionVO;
	}

	// Save Promotion
	public void savePromotion(Promotion promotion,
			List<PromotionMethod> promotionMethods,
			List<PromotionMethod> deletedPromotionDetails,
			List<PromotionOption> promotionOptions,
			List<PromotionOption> deletedPromotionOptions) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		promotion.setImplementation(getImplementation());
		if (null != deletedPromotionDetails
				&& deletedPromotionDetails.size() > 0)
			promotionService.deletePromotionMethods(deletedPromotionDetails);
		if (null != deletedPromotionOptions
				&& deletedPromotionOptions.size() > 0)
			promotionService.deletePromotionOptions(deletedPromotionOptions);

		promotion.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (promotion != null && promotion.getPromotionId() != null
				&& promotion.getPromotionId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		} 
		promotionService.savePromotion(promotion, promotionMethods,
				promotionOptions);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Promotion.class.getSimpleName(), promotion.getPromotionId(), user,
				workflowDetailVo);
	}
	
	public void doPromotionBusinessUpdate(Long promotionId,
			WorkflowDetailVO workflowDetailVO) {
		try { 
			if (workflowDetailVO.isDeleteFlag()) {
				Promotion promotion = promotionService.getPromotionbyId(promotionId);
				if (null != promotion.getPromotionOptions()
						&& promotion.getPromotionOptions().size() > 0)
					promotionService
							.deletePromotionOptions(new ArrayList<PromotionOption>(
									promotion.getPromotionOptions()));
				if (null != promotion.getPromotionMethods()
						&& promotion.getPromotionMethods().size() > 0)
					promotionService
							.deletePromotionMethods(new ArrayList<PromotionMethod>(
									promotion.getPromotionMethods()));
				promotionService.deletePromotion(promotion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Promotion
	public void deletePromotion(Promotion promotion) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						Promotion.class.getSimpleName(), promotion.getPromotionId(),
						user, workflowDetailVo); 
	}

	// Get Promotion by Product Id
	public Promotion getPromotionByProductId(long productId) throws Exception {
		return promotionService.getPromotionByProductId(productId, Calendar
				.getInstance().getTime());
	}

	// Get Promotion by Customer Id
	public Promotion getPromotionByCustomerId(long customerId) throws Exception {
		return promotionService.getPromotionByCustomerId(customerId, Calendar
				.getInstance().getTime());
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public PromotionService getPromotionService() {
		return promotionService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
