package com.aiotech.aios.accounts.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CreditTermService {

	private AIOTechGenericDAO<CreditTerm> creditTermDAO;

	public List<CreditTerm> getAllCreditTerms(Implementation implementation)
			throws Exception {
		return creditTermDAO.findByNamedQuery("getAllCreditTerms",
				implementation,(byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<CreditTerm> getAllSupplierCreditTerms(
			Implementation implementation) throws Exception {
		return creditTermDAO.findByNamedQuery("getAllSupplierCreditTerms",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<CreditTerm> getAllCustomerCreditTerms(
			Implementation implementation) throws Exception {
		return creditTermDAO.findByNamedQuery("getAllCustomerCreditTerms",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	public CreditTerm getCreditTermDetails(long creditTermId) throws Exception {
		return creditTermDAO.findByNamedQuery("getCreditTermDetails",
				creditTermId).get(0);
	}

	public void saveCreditTerm(CreditTerm creditTerm) throws Exception {
		creditTermDAO.saveOrUpdate(creditTerm);
	}

	public CreditTerm creditTermById(long creditTermId) throws Exception {
		return creditTermDAO.findById(creditTermId);
	}

	public void deleteCreditTerm(CreditTerm creditTerm) throws Exception {
		creditTermDAO.delete(creditTerm);
	}
	
	@SuppressWarnings("unchecked")
	public List<CreditTerm> getFilteredCreditTerms(Implementation implementation, Long creditTermId, Boolean isSupplier, Byte dueDay )
			throws Exception {
		Criteria criteria = creditTermDAO.createCriteria(); 
		 
		criteria.createAlias("suppliers", "s", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("customers", "c", CriteriaSpecification.LEFT_JOIN);
		
		
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		if (creditTermId != null && creditTermId > 0) {
			criteria.add(Restrictions.eq("creditTermId", creditTermId));
		}
		
		if (isSupplier != null) {
			criteria.add(Restrictions.eq("isSupplier", isSupplier));
		}
		
		if (dueDay != null && dueDay > 0) {
			criteria.add(Restrictions.eq("dueDay",dueDay));
		}
		
		return criteria.list();
	}

	public AIOTechGenericDAO<CreditTerm> getCreditTermDAO() {
		return creditTermDAO;
	}

	public void setCreditTermDAO(AIOTechGenericDAO<CreditTerm> creditTermDAO) {
		this.creditTermDAO = creditTermDAO;
	}
}
