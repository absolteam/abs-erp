package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetClaimInsuranceService {

	private AIOTechGenericDAO<AssetClaimInsurance> assetClaimInsuranceDAO;

	// Get all Asset Claim Insurance
	public List<AssetClaimInsurance> getAllAssetClaimInsurance(
			Implementation implementation) throws Exception {
		return assetClaimInsuranceDAO.findByNamedQuery(
				"getAllAssetClaimInsurance", implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	// Get AssetClaimInsurance By ClaimInsuranceId
	public AssetClaimInsurance getAssetClaimInsuranceByClaimId(
			long assetClaimInsuranceId) throws Exception {
		return assetClaimInsuranceDAO.findByNamedQuery(
				"getAssetClaimInsuranceByClaimId", assetClaimInsuranceId)
				.get(0);
	}

	// Get simple AssetClaimInsurance By ClaimInsuranceId
	public AssetClaimInsurance getSimpleAssetClaimInsuranceByClaimId(
			long assetClaimInsuranceId) throws Exception {
		return assetClaimInsuranceDAO.findById(assetClaimInsuranceId);
	}

	// Save Asset Claim Insurance
	public void saveAssetClaimInsurance(AssetClaimInsurance assetClaimInsurance)
			throws Exception {
		assetClaimInsuranceDAO.saveOrUpdate(assetClaimInsurance);
	}

	// Delete Asset Claim Insurance
	public void deleteAssetClaimInsurance(
			AssetClaimInsurance assetClaimInsurance) throws Exception {
		assetClaimInsuranceDAO.delete(assetClaimInsurance);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetClaimInsurance> getAssetClaimInsuranceDAO() {
		return assetClaimInsuranceDAO;
	}

	public void setAssetClaimInsuranceDAO(
			AIOTechGenericDAO<AssetClaimInsurance> assetClaimInsuranceDAO) {
		this.assetClaimInsuranceDAO = assetClaimInsuranceDAO;
	}
}
