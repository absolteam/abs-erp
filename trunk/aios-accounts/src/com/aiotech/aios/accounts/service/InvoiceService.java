package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceStatus;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class InvoiceService {

	private AIOTechGenericDAO<Invoice> invoiceDAO;
	private AIOTechGenericDAO<InvoiceDetail> invoiceDetailDAO;

	public List<Invoice> getAllInvoice(Implementation implementation)
			throws Exception {
		return this.getInvoiceDAO().findByNamedQuery("getAllInvoice",
				implementation);
	}

	public List<Invoice> getAllNonContinuosPurchaseInvoice(
			Implementation implementation) throws Exception {
		return this.getInvoiceDAO().findByNamedQuery(
				"getAllNonContinuosPurchaseInvoice", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get all Sales Invoice
	public List<Invoice> getAllContinuosPurchaseInvoice(
			Implementation implementation) throws Exception {
		return invoiceDAO.findByNamedQuery("getAllContinuosPurchaseInvoice",
				implementation);
	}

	// Get all invoice without implementation
	public List<Invoice> getAllInvoices() throws Exception {
		return this.getInvoiceDAO().findByNamedQuery(
				"getAllInvoiceWithoutImplementation");
	}

	public List<Invoice> getAllInvoiceNumber(Implementation implementation,
			Long supplierId) throws Exception {
		return this.getInvoiceDAO().findByNamedQuery("getAllInvoiceNumber",
				implementation, supplierId);
	}

	public List<InvoiceDetail> getSupplierActiveInvoice(Long supplierId)
			throws Exception {
		return invoiceDetailDAO.findByNamedQuery("getSupplierActiveInvoice",
				supplierId);
	}

	public void saveInvoice(Invoice invoice, List<InvoiceDetail> invoiceDetails)
			throws Exception {
		invoiceDAO.saveOrUpdate(invoice);
		for (InvoiceDetail list : invoiceDetails) {
			list.setInvoice(invoice);
		}
		invoiceDetailDAO.saveOrUpdateAll(invoiceDetails);
	}

	public void saveInvoice(Invoice invoice) throws Exception {
		invoiceDAO.saveOrUpdate(invoice);
	}

	public void saveInvoice(List<Invoice> invoices) throws Exception {
		invoiceDAO.saveOrUpdateAll(invoices);
	}

	// Save Invoice list & Invoice Details list
	public void saveInvoice(List<Invoice> invoices,
			List<InvoiceDetail> invoiceDetails) throws Exception {
		invoiceDAO.saveOrUpdateAll(invoices);
		invoiceDetailDAO.saveOrUpdateAll(invoiceDetails);
	}

	public Invoice getInvoiceById(Long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getInvoiceById", invoiceId).get(0);
	}

	public Invoice getInvoiceCurrencyById(Long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getInvoiceCurrencyById", invoiceId)
				.get(0);
	}

	public Invoice getContinuousInvoiceById(Long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getContinuousInvoiceById",
				invoiceId).get(0);
	}

	public Invoice getInvoiceDetailById(Long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getInvoiceDetailById", invoiceId)
				.get(0);
	}

	public List<Invoice> getChildInvoiceById(Long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getChildInvoiceById", invoiceId);
	}

	public Invoice getParentInvoiceById(long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getParentInvoiceById", invoiceId)
				.get(0);
	}
	
	public Invoice getBaseParentInvoiceById(long invoiceId) throws Exception {
		return invoiceDAO.findByNamedQuery("getBaseParentInvoiceById", invoiceId)
				.get(0);
	}

	public Invoice getInvoiceFindById(Long invoiceId) throws Exception {
		return invoiceDAO.findById(invoiceId);
	}

	public Invoice getInvoiceInfoById(Long invoiceId) throws Exception {
		return this.getInvoiceDAO()
				.findByNamedQuery("getInvoiceInfoById", invoiceId).get(0);
	}

	public List<InvoiceDetail> getInvoiceDetails(Long invoiceId)
			throws Exception {
		return this.getInvoiceDetailDAO().findByNamedQuery("getInvoiceDetails",
				invoiceId);
	}

	public void deleteInvoice(Invoice invoice,
			List<InvoiceDetail> invoiceDetails) throws Exception {
		this.getInvoiceDetailDAO().deleteAll(invoiceDetails);
		this.getInvoiceDAO().delete(invoice);
	}

	public void deleteInvoiceDetail(List<InvoiceDetail> invoiceDetails)
			throws Exception {
		this.getInvoiceDetailDAO().deleteAll(invoiceDetails);
	}

	@SuppressWarnings("unchecked")
	public List<Invoice> getSupplierNonPaidInvoices(Set<Long> suppliers)
			throws Exception {
		Criteria criteria = invoiceDAO.createCriteria();
		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.creditTerm", "trm", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "sp_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("invoiceDetails", "inv_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inv_det.receiveDetail", "rec_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rec_det.receive", "rec",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rec_det.purchaseDetail", "pur_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pur_det.purchase", "pur",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pur.creditTerm", "cr",
				CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.in("sp.supplierId", suppliers));
		criteria.add(Restrictions.ne("status", InvoiceStatus.Paid.getCode()));
		criteria.add(Restrictions.eq("isApprove",
				(byte) WorkflowConstants.Status.Published.getCode()));

		Set<Invoice> uniqueRecords = new HashSet<Invoice>(criteria.list());
		List<Invoice> invoices = new ArrayList<Invoice>(uniqueRecords);
		return invoices;
	}

	@SuppressWarnings("unchecked")
	public List<Invoice> getFilteredInvoice(Implementation implementation,
			Long invoiceId, Long supplierId, Long receiveId, Date fromDate,
			Date toDate) throws Exception {
		Criteria criteria = invoiceDAO.createCriteria();

		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "sp_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("invoiceDetails", "inv_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inv_det.product", "det_prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inv_det.receiveDetail", "rec_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rec_det.receive", "rec",
				CriteriaSpecification.LEFT_JOIN);
		// criteria.add(Restrictions.ne("status", 4));
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (invoiceId > 0) {
			criteria.add(Restrictions.eq("invoiceId", invoiceId));
		}

		if (supplierId > 0) {
			criteria.add(Restrictions.eq("sp.supplierId", supplierId));
		}

		if (receiveId > 0) {
			criteria.add(Restrictions.eq("rec.receiveId", receiveId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date", fromDate, toDate));
		}

		Set<Invoice> uniqueRecords = new HashSet<Invoice>(criteria.list());
		List<Invoice> invoiceList = new ArrayList<Invoice>(uniqueRecords);

		return invoiceList;
	}

	@SuppressWarnings("unchecked")
	public List<Invoice> getFilteredInvoice(Implementation implementation,
			Long supplierId, Date fromDate, Date toDate) throws Exception {
		Criteria criteria = invoiceDAO.createCriteria();

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "sp_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("invoiceDetails", "inv_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inv_det.product", "det_prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("inv_det.receiveDetail", "rec_det",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rec_det.receive", "rec",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("invoice", "parentInvoice",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("parentInvoice.invoiceDetails", "pInvDetails",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pInvDetails.receiveDetail", "pInvRecDetails",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("pInvRecDetails.receive", "pdInvoice",
				CriteriaSpecification.LEFT_JOIN);
		// criteria.add(Restrictions.ne("status", 4));
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (supplierId != null && supplierId > 0) {
			criteria.add(Restrictions.eq("sp.supplierId", supplierId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date", fromDate, toDate));
		}

		return criteria.list();
	}

	public List<Invoice> getSupplierActiveInvoices(long supplierId,
			long currencyId) throws Exception {
		return invoiceDAO.findByNamedQuery("getSupplierActiveInvoices",
				supplierId, currencyId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public AIOTechGenericDAO<Invoice> getInvoiceDAO() {
		return invoiceDAO;
	}

	public void setInvoiceDAO(AIOTechGenericDAO<Invoice> invoiceDAO) {
		this.invoiceDAO = invoiceDAO;
	}

	public AIOTechGenericDAO<InvoiceDetail> getInvoiceDetailDAO() {
		return invoiceDetailDAO;
	}

	public void setInvoiceDetailDAO(
			AIOTechGenericDAO<InvoiceDetail> invoiceDetailDAO) {
		this.invoiceDetailDAO = invoiceDetailDAO;
	}
}
