package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.TenderDetail;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.ProductService;
import com.aiotech.aios.accounts.service.TenderService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class TenderBL {

	private TenderService tenderService;
	private CurrencyService currencyService;
	private ProductService productService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Save Tender
	public void saveTender(Tender tender) throws Exception {
		tender.setIsApprove((byte) WorkflowConstants.Status.Published.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (tender != null && tender.getTenderId() != null
				&& tender.getTenderId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (null == tender.getTenderId())
			SystemBL.saveReferenceStamp(Tender.class.getName(),
					tender.getImplementation());
		tenderService.saveTender(tender);
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Tender.class.getSimpleName(), tender.getTenderId(), user,
				workflowDetailVo);
	}

	// Delete Tender
	public void deleteTender(Tender tender, List<TenderDetail> tenderDetails)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						Tender.class.getSimpleName(),
						tender.getTenderId(), user,
						workflowDetailVo);
	}

	public void doTenderBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		Tender tender = tenderService.getTenderBasicById(recordId);

		if (workflowDetailVO.isDeleteFlag()) {
			tenderService.deleteTender(tender, new ArrayList<TenderDetail>(
					tender.getTenderDetails()));

		}
	}

	public boolean validateTender(String lineDetails) throws Exception {
		String[] tenderList = splitValues(lineDetails, "#@");
		List<Long> productIdList = new ArrayList<Long>();
		for (String string : tenderList) {
			String[] detailString = splitValues(string, "__");
			if (null != detailString[5] && !("").equals(detailString[5])
					&& !("e").equalsIgnoreCase(detailString[5].trim()))
				productIdList.add(Long.parseLong(detailString[0]));
		}
		Set<Long> productIdSet = findDuplicates(productIdList);
		if (null != productIdSet && productIdSet.size() > 0)
			return false;
		else
			return true;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Set<Long> findDuplicates(List<Long> validateId) {
		final Set<Long> returnList = new HashSet();
		final Set<Long> checkList = new HashSet();
		for (Long yourInt : validateId) {
			if (!checkList.add(yourInt)) {
				returnList.add(yourInt);
			}
		}
		return returnList;
	}

	private String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public List<Tender> getTenderFilterInformation(
			Implementation implementation, Date fromDate, Date toDate,
			Long productId, boolean showAll) {

		List<Tender> tenders = new ArrayList<Tender>();
		try {
			tenders = tenderService.getTendersByFilterValues(fromDate, toDate,
					productId, showAll, implementation);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tenders;
	}

	public TenderService getTenderService() {
		return tenderService;
	}

	public void setTenderService(TenderService tenderService) {
		this.tenderService = tenderService;
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
