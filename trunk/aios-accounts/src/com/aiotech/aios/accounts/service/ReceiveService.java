package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ReceiveService {
	private AIOTechGenericDAO<Receive> receiveDAO;
	private AIOTechGenericDAO<ReceiveDetail> receiveDetailDAO;

	public List<ReceiveDetail> getReceiveDetailByProductId(Long productId) {

		return receiveDetailDAO.findByNamedQuery("getReceiveDetailByProductId",
				productId);
	}

	// To get receive details
	public List<Receive> getAllReceiveNotes(Implementation implementation)
			throws Exception {
		List<Receive> receivelist = receiveDAO.findByNamedQuery(
				"getReceiptList", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
		return receivelist;
	}

	// To get closed receive details
	public List<Receive> getClosedReceiveNotes(Implementation implementation)
			throws Exception {
		List<Receive> receivelist = receiveDAO.findByNamedQuery(
				"getClosedReceiveNotes", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
		return receivelist;
	}

	// To get closed receive details
	public List<ReceiveDetail> getClosedReceiveDetails(long receiveId)
			throws Exception {
		List<ReceiveDetail> receiveDetails = receiveDetailDAO.findByNamedQuery(
				"getClosedReceiveDetails", receiveId);
		return receiveDetails;
	}

	// To get receive details without implementation
	public List<Receive> getAllReceiveNotes() throws Exception {
		List<Receive> receivelist = receiveDAO
				.findByNamedQuery("getReceiptListWithoutImpl");
		return receivelist;
	}

	// to save receipt details
	public void saveReceipts(Receive receive, List<ReceiveDetail> detailList)
			throws Exception {
		receiveDAO.saveOrUpdate(receive);
		for (ReceiveDetail list : detailList) {
			list.setReceive(receive);
		}
		receiveDetailDAO.saveOrUpdateAll(detailList);
	}

	// Save Receipts Detail
	public void saveReceiptsDetail(List<Receive> receives,
			List<ReceiveDetail> detailList) throws Exception {
		receiveDAO.saveOrUpdateAll(receives);
		receiveDetailDAO.saveOrUpdateAll(detailList);
	}

	public void saveReceipts(List<Receive> receiveList) throws Exception {
		receiveDAO.saveOrUpdateAll(receiveList);
	}

	public void updateReceiveDetails(List<ReceiveDetail> receiveDetails)
			throws Exception {
		receiveDetailDAO.saveOrUpdateAll(receiveDetails);
	}

	// to delete receive notes
	public void deleteReceipts(Receive receive, List<ReceiveDetail> detailList)
			throws Exception {
		receiveDetailDAO.deleteAll(detailList);
		receiveDAO.delete(receive);
	}

	public void deleteReceiptDetails(List<ReceiveDetail> detailList)
			throws Exception {
		receiveDetailDAO.deleteAll(detailList);
	}

	// to get receipt line details
	public List<ReceiveDetail> getReceiveLineDetails(long receiveId)
			throws Exception {
		List<ReceiveDetail> detailList = receiveDetailDAO.findByNamedQuery(
				"getReceiveLines", receiveId);
		return detailList;
	}

	// to get by receive id
	public Receive getReceiveById(long receiveId) throws Exception {
		return receiveDAO.findById(receiveId);
	}

	public Receive getReceiveInfoById(long receiveId) throws Exception {
		List<Receive> receives = receiveDAO.findByNamedQuery(
				"getReceiveInfoById", receiveId);
		if (receives != null && receives.size() > 0)
			return receives.get(0);
		else
			return null;
	}

	// to get by receive detail by id
	public ReceiveDetail getReceiveDetailById(long receiveDetailId)
			throws Exception {
		return receiveDetailDAO.findByNamedQuery("getReceiveDetailById",
				receiveDetailId).get(0);
	}

	// Get Receive Detail by implementation
	public List<ReceiveDetail> getAssetReceiveDetail(
			Implementation implementation) throws Exception {
		return receiveDetailDAO.findByNamedQuery("getAssetReceiveDetail",
				implementation);
	}

	// Get Receive Detail by implementation
	public List<ReceiveDetail> getAllReceiveDetails(
			Implementation implementation) throws Exception {
		return receiveDetailDAO.findByNamedQuery("getAllReceiveDetails",
				implementation);
	}

	public List<ReceiveDetail> getAllReceiveDetailWithoutImpl()
			throws Exception {
		return receiveDetailDAO
				.findByNamedQuery("getAllReceiveDetailWithoutImpl");
	}

	public Receive getReceiptsById(long receiveId) throws Exception {
		return receiveDAO.findByNamedQuery("getReceiptNotes", receiveId).get(0);
	}

	public Receive getReceiveDetails(long receiveId) throws Exception {
		return receiveDAO.findByNamedQuery("getReceiveDetails", receiveId).get(
				0);
	}

	public Receive getReceiveDetailsBarcode(long receiveId) throws Exception {
		return receiveDAO.findByNamedQuery("getReceiveDetailsBarcode",
				receiveId).get(0);
	}

	public ReceiveDetail getReceiveDetailByPurchaseProduct(long purchaseId,
			long productId) throws Exception {
		return receiveDetailDAO.findByNamedQuery(
				"getReceiveDetailByPurchaseProduct", purchaseId, productId)
				.get(0);
	}

	// Get supplier active GRN
	/*
	 * public List<Receive> getSupplierActiveGRN(long supplierId) throws
	 * Exception { return receiveDAO.findByNamedQuery("getSupplierActiveGRN",
	 * supplierId); }
	 */

	public List<ReceiveDetail> getSupplierActiveGRN(long supplierId)
			throws Exception {
		return receiveDetailDAO
				.findByNamedQuery("getSupplierActiveGRN", supplierId,
						(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<ReceiveDetail> getSupplierActiveGRNEdit(long supplierId,
			long invoiceId) throws Exception {
		return receiveDetailDAO.findByNamedQuery(
				"getSupplierActiveGRNWithInvoice", invoiceId, supplierId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<ReceiveDetail> getSupplierGRN(long supplierId) throws Exception {
		return receiveDetailDAO.findByNamedQuery("getSupplierGRN", supplierId);
	}

	@SuppressWarnings("unchecked")
	public List<Receive> getFilteredReceiveNotes(Implementation implementation,
			Long receiveId, Date toDate, Date fromDate, Long supplierId,
			Long storeId, Long productId) throws Exception {
		Criteria criteria = receiveDAO.createCriteria();

		criteria.createAlias("receiveDetails", "rd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.purchase", "p",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.shelf", "sh", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sh.shelf", "rk", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rk.aisle", "asl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("asl.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "ps", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "spy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("p.requisition", "req",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("req.cmpDeptLoc", "cdl1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl1.department", "dept1",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("p.implementation", implementation));
		}

		if (receiveId > 0) {
			criteria.add(Restrictions.eq("receiveId", receiveId));
		}

		if (fromDate != null && !("").equals(fromDate)) {
			criteria.add(Restrictions.ge("receiveDate", fromDate));
		}

		if (toDate != null && !("").equals(toDate)) {
			criteria.add(Restrictions.le("receiveDate", toDate));
		}

		if (supplierId > 0) {
			criteria.add(Restrictions.eq("sp.supplierId", supplierId));
		}

		if (storeId > 0) {
			criteria.add(Restrictions.eq("str.storeId", storeId));
		}

		if (productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		Set<Receive> uniqueRecords = new HashSet<Receive>(criteria.list());
		List<Receive> receiceList = new ArrayList<Receive>(uniqueRecords);

		return receiceList;
	}

	@SuppressWarnings("unchecked")
	public List<Receive> getFilteredReceiveNotes(Implementation implementation,
			Date toDate, Date fromDate, Set<Long> products, Long personId,
			Long departmentId) throws Exception {
		Criteria criteria = receiveDAO.createCriteria();

		criteria.createAlias("receiveDetails", "rd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.goodsReturnDetails", "goodreturndet",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("goodreturndet.goodsReturn", "goodreturn",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.purchase", "p",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rd.shelf", "sh", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sh.shelf", "rk", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rk.aisle", "asl", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("asl.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("supplier", "sp", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "ps", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "spy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("p.requisition", "req",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("req.cmpDeptLoc", "cdl1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl1.department", "depart",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("req.person", "reqPerson",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("p.implementation", implementation));
		}

		if (personId != null) {
			criteria.add(Restrictions.eq("reqPerson.personId", personId));
		}

		if (departmentId != null) {
			criteria.add(Restrictions.eq("depart.departmentId", departmentId));
		}

		if (fromDate != null && !("").equals(fromDate)) {
			criteria.add(Restrictions.ge("receiveDate", fromDate));
		}

		if (toDate != null && !("").equals(toDate)) {
			criteria.add(Restrictions.le("receiveDate", toDate));
		}

		if (products != null && products.size() > 0) {
			criteria.add(Restrictions.in("prd.productId", products));
		}

		Set<Receive> uniqueRecords = new HashSet<Receive>(criteria.list());
		List<Receive> receiceList = new ArrayList<Receive>(uniqueRecords);

		return receiceList;
	}

	public List<ReceiveDetail> getReceiveDetailByReceiveId(Set<Long> receiveIds)
			throws Exception {
		Criteria criteria = receiveDetailDAO.createCriteria();

		criteria.createAlias("product", "prd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("receive", "rc", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rc.supplier", "sp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.person", "ps", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sp.company", "spy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("purchase", "pr", CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.in("rc.receiveId", receiveIds));

		@SuppressWarnings("unchecked")
		Set<ReceiveDetail> uniqueRecords = new HashSet<ReceiveDetail>(
				criteria.list());
		List<ReceiveDetail> receiveDetailList = new ArrayList<ReceiveDetail>(
				uniqueRecords);

		return receiveDetailList;
	}

	public List<ReceiveDetail> getPurchaseDetailWithNonReceive(
			Set<Long> purchaseIds, long receiveId) throws Exception {
		Criteria criteria = receiveDetailDAO.createCriteria();
		criteria.createAlias("receive", "rc", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("purchase", "pr", CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.ne("rc.receiveId", receiveId));
		criteria.add(Restrictions.in("pr.purchaseId", purchaseIds));
		@SuppressWarnings("unchecked")
		Set<ReceiveDetail> uniqueRecords = new HashSet<ReceiveDetail>(
				criteria.list());
		List<ReceiveDetail> receiveDetailList = new ArrayList<ReceiveDetail>(
				uniqueRecords);

		return receiveDetailList;
	}

	// Getter & setters
	public AIOTechGenericDAO<Receive> getReceiveDAO() {
		return receiveDAO;
	}

	public void setReceiveDAO(AIOTechGenericDAO<Receive> receiveDAO) {
		this.receiveDAO = receiveDAO;
	}

	public AIOTechGenericDAO<ReceiveDetail> getReceiveDetailDAO() {
		return receiveDetailDAO;
	}

	public void setReceiveDetailDAO(
			AIOTechGenericDAO<ReceiveDetail> receiveDetailDAO) {
		this.receiveDetailDAO = receiveDetailDAO;
	}
}
