package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class BankDepositService {

	private AIOTechGenericDAO<BankDeposit> bankDepositDAO;
	private AIOTechGenericDAO<BankDepositDetail> bankDepositDetailDAO;

	public List<BankDeposit> getAllBankDeposit(Implementation implementation) {
		return bankDepositDAO.findByNamedQuery("getAllBankDeposit",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Save Bank Deposits & Details
	public void saveBankDeposit(BankDeposit bankDeposit,
			List<BankDepositDetail> bankDepositDetails) throws Exception {
		bankDepositDAO.saveOrUpdate(bankDeposit);
		for (BankDepositDetail list : bankDepositDetails) {
			list.setBankDeposit(bankDeposit);
		}
		bankDepositDetailDAO.saveOrUpdateAll(bankDepositDetails);
	}

	// Delete Bank Deposit & Deposit Details
	public void deleteBankDeposit(BankDeposit bankDeposit,
			List<BankDepositDetail> bankDepositDetails) throws Exception {
		bankDepositDetailDAO.deleteAll(bankDepositDetails);
		bankDepositDAO.delete(bankDeposit);
	}

	public void saveBankDeposit(BankDeposit bankDeposit) {
		bankDepositDAO.saveOrUpdate(bankDeposit);
	}

	public void saveBankDepositDetail(List<BankDepositDetail> bankDepositDetails) {
		bankDepositDetailDAO.saveOrUpdateAll(bankDepositDetails);
	}

	public BankDeposit getBankDepositInfo(Long bankDepositId) throws Exception {
		List<BankDeposit> bankDeposit = bankDepositDAO.findByNamedQuery(
				"getBankDepositInfo", bankDepositId);
		return bankDeposit != null && bankDeposit.size() > 0 ? bankDeposit
				.get(0) : null;
	}

	public BankDepositDetail getBankDepositDetailInfo(Long bankDepositDetailId)
			throws Exception {
		List<BankDepositDetail> bankDepositDetail = bankDepositDetailDAO
				.findByNamedQuery("getBankDepositDetailInfo",
						bankDepositDetailId);
		return bankDepositDetail != null && bankDepositDetail.size() > 0 ? bankDepositDetail
				.get(0) : null;
	}

	public BankDeposit getBankDepositByDepositId(Long bankDepositId)
			throws Exception {
		List<BankDeposit> bankDeposit = bankDepositDAO.findByNamedQuery(
				"getBankDepositByDepositId", bankDepositId);
		return bankDeposit != null && bankDeposit.size() > 0 ? bankDeposit
				.get(0) : null;
	}

	public List<BankDeposit> getBankDepositByPresentedCheques(
			Long bankAccountId, Date startDate, Date endDate) throws Exception {
		return bankDepositDAO.findByNamedQuery(
				"getBankDepositByPresentedCheques", bankAccountId, startDate,
				endDate);
	}

	public BankDeposit findBankDepositById(Long bankDepositId) throws Exception {
		return bankDepositDAO.findById(bankDepositId);
	}

	@SuppressWarnings("unchecked")
	public List<BankDeposit> getFilteredBankDeposit(
			Implementation implementation, Long depositId, Integer currencyId,
			Long receiptTypeId, Date fromDate, Date toDate) {
		Criteria criteria = bankDepositDAO.createCriteria();

		criteria.createAlias("personByCreatedBy", "pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByDepositerId", "dpr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankDepositDetails", "bdd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bdd.bankReceiptsDetail", "brd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("brd.bankReceipts", "br",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.customer", "cus",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.personByPersonId", "cus_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.company", "cus_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.cmpDeptLocation", "cus_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus_cdl.company", "cus_cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.personByPersonId", "br_pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.supplier", "sup",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup.person", "sup_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup.company", "sup_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup.cmpDeptLocation", "sup_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup_cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.currency", "cur",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cur.currencyPool", "cp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankAccount", "ba",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ba.bank", "b", CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (depositId != null && depositId > 0) {
			criteria.add(Restrictions.eq("bankDepositId", depositId));
		}

		if (currencyId != null && currencyId > 0) {
			criteria.add(Restrictions.eq("cur.currencyId", currencyId));
		}

		if (receiptTypeId != null && receiptTypeId > 0) {
			criteria.add(Restrictions.eq("ld.lookupDetailId", receiptTypeId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("date", fromDate, toDate));
		}

		Set<BankDeposit> uniqueRecords = new HashSet<BankDeposit>(
				criteria.list());
		return null != uniqueRecords && uniqueRecords.size() > 0 ? new ArrayList<BankDeposit>(
				uniqueRecords) : null;
	}

	@SuppressWarnings("unchecked")
	public List<BankDepositDetail> getChequeBankDeposits(
			Implementation implementation, String fromdate, String todate)
			throws Exception {
		Criteria criteria = bankDepositDetailDAO.createCriteria();

		criteria.createAlias("bankDeposit", "bd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bankReceiptsDetail", "brd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("brd.bankReceipts", "br",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.customer", "cus",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.personByPersonId", "cus_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.company", "cus_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus.cmpDeptLocation", "cus_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cus_cdl.company", "cus_cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.personByPersonId", "br_pr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("br.supplier", "sup",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup.person", "sup_per",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup.company", "sup_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup.cmpDeptLocation", "sup_cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("sup_cdl.company", "cdl_cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bd.bankAccount", "ba",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("ba.bank", "b", CriteriaSpecification.LEFT_JOIN);

		if (implementation != null)
			criteria.add(Restrictions.eq("bd.implementation", implementation));
		criteria.add(Restrictions.isNotNull("brd.institutionName"));
		criteria.add(Restrictions.and(
				Restrictions.isNotNull("chequePresented"),
				Restrictions.eq("chequePresented", true)));
		if (fromdate != null && !("").equals(fromdate))
			criteria.add(Restrictions.and(
					Restrictions.isNotNull("brd.chequeDate"),
					Restrictions.gt("brd.chequeDate",
							DateFormat.convertStringToDate(fromdate))));

		if (todate != null && !("").equals(todate))
			criteria.add(Restrictions.and(
					Restrictions.isNotNull("brd.chequeDate"),
					Restrictions.le("brd.chequeDate",
							DateFormat.convertStringToDate(todate))));
		Set<BankDepositDetail> uniqueRecords = new HashSet<BankDepositDetail>(
				criteria.list());
		return null != uniqueRecords && uniqueRecords.size() > 0 ? new ArrayList<BankDepositDetail>(
				uniqueRecords) : null;
	}

	public AIOTechGenericDAO<BankDeposit> getBankDepositDAO() {
		return bankDepositDAO;
	}

	public void setBankDepositDAO(AIOTechGenericDAO<BankDeposit> bankDepositDAO) {
		this.bankDepositDAO = bankDepositDAO;
	}

	public AIOTechGenericDAO<BankDepositDetail> getBankDepositDetailDAO() {
		return bankDepositDetailDAO;
	}

	public void setBankDepositDetailDAO(
			AIOTechGenericDAO<BankDepositDetail> bankDepositDetailDAO) {
		this.bankDepositDetailDAO = bankDepositDetailDAO;
	}
}
