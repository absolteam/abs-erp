package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetCheckOutService {

	private AIOTechGenericDAO<AssetCheckOut> assetCheckOutDAO;

	/**
	 * Get all Check out assets
	 * 
	 * @param implementation
	 * @return
	 */
	public List<AssetCheckOut> getAllAssetCheckOuts(
			Implementation implementation) throws Exception {
		return assetCheckOutDAO.findByNamedQuery("getAllAssetCheckOuts",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	/**
	 * Get Active Check out assets
	 * 
	 * @param implementation
	 * @return
	 */
	public List<AssetCheckOut> getActiveAssetCheckOuts(
			Implementation implementation) throws Exception {
		return assetCheckOutDAO.findByNamedQuery("getActiveAssetCheckOuts",
				implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	/**
	 * Get Asset CheckOut Details
	 * 
	 * @param assetCheckOutId
	 * @return
	 */
	public AssetCheckOut getAssetCheckOutDetails(long assetCheckOutId)
			throws Exception {
		return assetCheckOutDAO.findByNamedQuery("getAssetCheckOutDetails",
				assetCheckOutId).get(0);
	}

	public AssetCheckOut getAssetCheckOutById(long assetCheckOutId)
			throws Exception {
		return assetCheckOutDAO.findById(assetCheckOutId);
	}

	/**
	 * Save Asset Check Out
	 * 
	 * @param assetCheckOut
	 */
	public void saveAssetCheckOut(AssetCheckOut assetCheckOut) throws Exception {
		assetCheckOutDAO.saveOrUpdate(assetCheckOut);
	}

	/**
	 * Delete Asset Check out
	 * 
	 * @param assetCheckOut
	 * @throws Exception
	 */
	public void deleteAssetCheckOut(AssetCheckOut assetCheckOut)
			throws Exception {
		assetCheckOutDAO.delete(assetCheckOut);
	}

	@SuppressWarnings("unchecked")
	public List<AssetCheckOut> getFilteredAssetCheckOuts(
			Implementation implementation, Long assetCheckOutId,
			Long assetCreationId, Long locationId, Long personId,
			Date fromDate, Date toDate) throws Exception {
		Criteria criteria = assetCheckOutDAO.createCriteria();

		criteria.createAlias("assetCreation", "ac",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.department", "dpt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.location", "loc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByCheckOutTo", "to",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("personByCheckOutBy", "by",
				CriteriaSpecification.LEFT_JOIN);

		if (implementation != null) {
			criteria.add(Restrictions.eq("ac.implementation", implementation));
		}

		if (assetCheckOutId != null && assetCheckOutId > 0) {
			criteria.add(Restrictions.eq("assetCheckOutId", assetCheckOutId));
		}

		if (assetCreationId != null && assetCreationId > 0) {
			criteria.add(Restrictions.eq("ac.assetCreationId", assetCreationId));
		}

		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("to.personId", personId));
		}

		if (locationId != null && locationId > 0) {
			criteria.add(Restrictions.eq("loc.locationId", locationId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("checkOutDate", fromDate, toDate));
		}
		return criteria.list();
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetCheckOut> getAssetCheckOutDAO() {
		return assetCheckOutDAO;
	}

	public void setAssetCheckOutDAO(
			AIOTechGenericDAO<AssetCheckOut> assetCheckOutDAO) {
		this.assetCheckOutDAO = assetCheckOutDAO;
	}
}
