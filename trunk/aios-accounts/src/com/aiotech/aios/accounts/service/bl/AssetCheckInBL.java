package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetCheckIn;
import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.accounts.domain.entity.vo.AssetCheckInVO;
import com.aiotech.aios.accounts.service.AssetCheckInService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetCheckInBL {

	private AssetCheckOutBL assetCheckOutBL;
	private AssetCheckInService assetCheckInService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Asset Check Outs
	public List<Object> showCheckInJsonList() throws Exception {
		List<AssetCheckIn> assetCheckIns = assetCheckInService
				.getAllAssetCheckIns(getImplementation());
		List<Object> assetCheckInVOs = new ArrayList<Object>();
		AssetCheckInVO assetCheckInVO = null;
		for (AssetCheckIn assetCheckIn : assetCheckIns) {
			assetCheckInVO = new AssetCheckInVO();
			assetCheckInVO.setAssetCheckInId(assetCheckIn.getAssetCheckInId());
			assetCheckInVO.setCheckInNumber(assetCheckIn.getCheckInNumber());
			assetCheckInVO.setCheckOutNumber(assetCheckIn.getAssetCheckOut()
					.getCheckOutNumber());
			assetCheckInVO.setAssetName(assetCheckIn.getAssetCheckOut()
					.getAssetCreation().getAssetName());
			assetCheckInVO.setOutDate(DateFormat
					.convertDateToString(assetCheckIn.getAssetCheckOut()
							.getCheckOutDate().toString()));
			assetCheckInVO.setInDate(DateFormat
					.convertDateToString(assetCheckIn.getCheckInDate()
							.toString()));
			assetCheckInVO.setCheckOutTo(assetCheckIn
					.getAssetCheckOut()
					.getPersonByCheckOutTo()
					.getFirstName()
					.concat(" ")
					.concat(assetCheckIn.getAssetCheckOut()
							.getPersonByCheckOutTo().getLastName()));
			assetCheckInVOs.add(assetCheckInVO);
		}
		return assetCheckInVOs;
	}

	// Save Asset Check In
	public void saveAssetCheckIn(AssetCheckIn assetCheckIn,
			AssetCheckOut assetCheckOut) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetCheckIn.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		if (null == assetCheckIn.getAssetCheckInId())
			SystemBL.saveReferenceStamp(AssetCheckIn.class.getName(),
					getImplementation());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetCheckIn != null && assetCheckIn.getAssetCheckInId() != null
				&& assetCheckIn.getAssetCheckInId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetCheckInService.saveAssetCheckIn(assetCheckIn);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetCheckIn.class.getSimpleName(),
				assetCheckIn.getAssetCheckInId(), user, workflowDetailVo);
	}

	public void doAssetCheckInBusinessUpdate(Long assetCheckInId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			AssetCheckIn assetCheckIn = assetCheckInService
					.getAssetCheckInDetails(assetCheckInId);
			AssetCheckOut assetCheckOut = assetCheckOutBL
					.getAssetCheckOutService().getAssetCheckOutById(
							assetCheckIn.getAssetCheckOut()
									.getAssetCheckOutId());
			if (workflowDetailVO.isDeleteFlag()) {
				assetCheckOut.setStatus(true);
				assetCheckInService.deleteAssetCheckIn(assetCheckIn);
				assetCheckOutBL.getAssetCheckOutService().saveAssetCheckOut(
						assetCheckOut);
			} else {
				assetCheckOut.setStatus(false);
				assetCheckOutBL.getAssetCheckOutService().saveAssetCheckOut(
						assetCheckOut);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Asset Check In
	public void deleteAssetCheckIn(AssetCheckIn assetCheckIn) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						AssetCheckIn.class.getSimpleName(), assetCheckIn.getAssetCheckInId(),
						user, workflowDetailVo); 
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetCheckInService getAssetCheckInService() {
		return assetCheckInService;
	}

	public void setAssetCheckInService(AssetCheckInService assetCheckInService) {
		this.assetCheckInService = assetCheckInService;
	}

	public AssetCheckOutBL getAssetCheckOutBL() {
		return assetCheckOutBL;
	}

	public void setAssetCheckOutBL(AssetCheckOutBL assetCheckOutBL) {
		this.assetCheckOutBL = assetCheckOutBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
