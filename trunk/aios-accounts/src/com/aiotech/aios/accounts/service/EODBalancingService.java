package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.CurrencySetting;
import com.aiotech.aios.accounts.domain.entity.EODBalancing;
import com.aiotech.aios.accounts.domain.entity.EODBalancingDetail;
import com.aiotech.aios.accounts.domain.entity.EODProductionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.EODBalancingVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class EODBalancingService {

	private AIOTechGenericDAO<EODBalancing> eodBalancingDAO;
	private AIOTechGenericDAO<EODBalancingDetail> eodBalancingDetailDAO;
	private AIOTechGenericDAO<EODProductionDetail> eodProductionDetailDAO;
	private AIOTechGenericDAO<CurrencySetting> currencySettingDAO;

	// Get all currency settings
	public List<CurrencySetting> getAllCurrencySettings() throws Exception {
		return currencySettingDAO.getAll();
	}

	// Get all EOD Balancing
	public List<EODBalancing> getAllEODBalancing(Implementation implementation)
			throws Exception {
		return eodBalancingDAO.findByNamedQuery("getAllEODBalancing",
				implementation);
	}

	// Save Balancing
	public void saveEODBalancing(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails) throws Exception {
		eodBalancingDAO.saveOrUpdate(eodBalancing);
		for (EODBalancingDetail eodBalancingDetail : eodBalancingDetails) {
			eodBalancingDetail.setEODBalancing(eodBalancing);
		}
		eodBalancingDetailDAO.saveOrUpdateAll(eodBalancingDetails);
	}

	// Save Balancing
	public void saveEODProduction(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails,
			List<EODProductionDetail> eodDProductionDetails) throws Exception {
		eodBalancingDAO.saveOrUpdate(eodBalancing);
		eodBalancingDetailDAO.saveOrUpdateAll(eodBalancingDetails);
		eodProductionDetailDAO.saveOrUpdateAll(eodDProductionDetails);
	}

	// Delete Balancing
	public void deleteEODBalancing(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails) throws Exception {
		eodBalancingDetailDAO.deleteAll(eodBalancingDetails);
		eodBalancingDAO.delete(eodBalancing);
	}

	// Get EOD Balancing By EODBalancingId
	public EODBalancing getEODBalancingById(long eodBalancingId) {
		return eodBalancingDAO.findByNamedQuery("getEODBalancingById",
				eodBalancingId).get(0);
	}

	@SuppressWarnings("unchecked")
	public List<EODBalancing> getFilteredEODBalancing(
			Implementation implementation, EODBalancingVO vo) throws Exception {

		List<EODBalancing> eodBalancings = null;
		Criteria criteria = eodBalancingDAO.createCriteria("eod");

		criteria.createAlias("implementation", "imp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("EODBalancingDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("POSUserTill", "till");
		criteria.createAlias("till.store", "store");
		criteria.createAlias("till.personByPersonId", "employee");
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (vo.getTillId() != null && vo.getTillId() > 0) {
			criteria.add(Restrictions.eq("till.posUserTillId", vo.getTillId()));
		}

		if (vo.getStoreId() != null && vo.getStoreId() > 0) {
			criteria.add(Restrictions.eq("store.storeId", vo.getStoreId()));
		}

		if (vo.getPersonId() != null && vo.getPersonId() > 0) {
			criteria.add(Restrictions.eq("employee.personId", vo.getPersonId()));
		}

		if (vo.getFromDateView() != null) {
			criteria.add(Restrictions.conjunction().add(
					Restrictions.le("date", vo.getFromDate())));
			criteria.add(Restrictions.conjunction().add(
					Restrictions.ge("date", vo.getToDate())));
		}

		criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("detail.receiptType"))
				.add(Projections.max("eodBalancingId"))
				.add(Projections.max("implementation"))
				.add(Projections.max("referenceNumber"))
				.add(Projections.sum("detail.actualAmount"))
				.add(Projections.sum("detail.tillAmount"))
				.add(Projections.max("date")));

		List<Object> result = criteria.list();
		eodBalancings = new ArrayList<EODBalancing>();

		EODBalancing eodBalancing = new EODBalancing();
		Set<EODBalancingDetail> eodBalancingDetails = new HashSet<EODBalancingDetail>();
		for (Object data : result) {
			Object[] record = (Object[]) data;

			eodBalancing.setEodBalancingId((Long) record[1]);
			eodBalancing.setImplementation((Implementation) record[2]);
			eodBalancing.setReferenceNumber((String) record[3]);
			eodBalancing.setDate((Date) record[6]); 

		}
		eodBalancing.setEODBalancingDetails(eodBalancingDetails);
		eodBalancings.add(eodBalancing);

		Set<EODBalancing> uniqueRecords = new HashSet<EODBalancing>(
				eodBalancings);
		eodBalancings = new ArrayList<EODBalancing>(uniqueRecords);

		return eodBalancings;
	}

	// Getters & Setters
	public AIOTechGenericDAO<EODBalancing> getEodBalancingDAO() {
		return eodBalancingDAO;
	}

	public void setEodBalancingDAO(
			AIOTechGenericDAO<EODBalancing> eodBalancingDAO) {
		this.eodBalancingDAO = eodBalancingDAO;
	}

	public AIOTechGenericDAO<CurrencySetting> getCurrencySettingDAO() {
		return currencySettingDAO;
	}

	public void setCurrencySettingDAO(
			AIOTechGenericDAO<CurrencySetting> currencySettingDAO) {
		this.currencySettingDAO = currencySettingDAO;
	}

	public AIOTechGenericDAO<EODBalancingDetail> getEodBalancingDetailDAO() {
		return eodBalancingDetailDAO;
	}

	public void setEodBalancingDetailDAO(
			AIOTechGenericDAO<EODBalancingDetail> eodBalancingDetailDAO) {
		this.eodBalancingDetailDAO = eodBalancingDetailDAO;
	}

	public void saveEODBalancing(EODBalancing eodBalancing,
			List<EODBalancingDetail> eodBalancingDetails, Session session)
			throws Exception {
		session.saveOrUpdate(eodBalancing);
		for (EODBalancingDetail eodBalancingDetail : eodBalancingDetails) {
			eodBalancingDetail.setEODBalancing(eodBalancing);
			session.saveOrUpdate(eodBalancingDetail);
		}
	}

	public AIOTechGenericDAO<EODProductionDetail> getEodProductionDetailDAO() {
		return eodProductionDetailDAO;
	}

	public void setEodProductionDetailDAO(
			AIOTechGenericDAO<EODProductionDetail> eodProductionDetailDAO) {
		this.eodProductionDetailDAO = eodProductionDetailDAO;
	}
}
