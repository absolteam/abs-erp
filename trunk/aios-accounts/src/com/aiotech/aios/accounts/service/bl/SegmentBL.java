package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentTreeVO;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentVO;
import com.aiotech.aios.accounts.service.SegmentService;
import com.aiotech.aios.system.domain.entity.Implementation;

public class SegmentBL {

	private SegmentService segmentService;
 
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	} 

	public SegmentTreeVO getSegmentChild(Segment segment,
			SegmentTreeVO segmentVO) throws Exception {
		List<SegmentTreeVO> segmentVOs = new ArrayList<SegmentTreeVO>();
		List<Segment> segments = segmentService.getSegmentChildById(segment
				.getSegmentId());
		SegmentTreeVO childVO = null;
		if (null != segments && segments.size() > 0) {
			for (Segment seg : segments) {
				childVO = new SegmentTreeVO();
				childVO.setKey(seg.getSegmentId());
				childVO.setTitle(seg.getSegmentName());
				segmentVOs.add(childVO);
				getSegmentChild(seg, childVO);
			}
			Collections.sort(segmentVOs, new Comparator<SegmentTreeVO>() {
				public int compare(SegmentTreeVO o1, SegmentTreeVO o2) {
					return o1.getKey().compareTo(o2.getKey());
				}
			});
			segmentVO.setChildren(segmentVOs);
		}
		return segmentVO;
	}

	public List<SegmentVO> getSegmentDirectChild(Segment segment,
			List<SegmentVO> segmentVOs) throws Exception {
		SegmentVO childVO = null;
		List<Segment> segments = segmentService.getSegmentChildById(segment
				.getSegmentId());
		if (null != segments && segments.size() > 0) {
			for (Segment seg : segments) {
				childVO = new SegmentVO();
				childVO.setSegmentId(seg.getSegmentId());
				childVO.setSegmentName(seg.getSegmentName());
				segmentVOs.add(childVO);
				getSegmentDirectChild(seg, segmentVOs);
			}
			Collections.sort(segmentVOs, new Comparator<SegmentVO>() {
				public int compare(SegmentVO o1, SegmentVO o2) {
					return o1.getSegmentId().compareTo(o2.getSegmentId());
				}
			});
		}
		return segmentVOs;
	}

	public Segment getParentSegmentBySegment(Segment segment) throws Exception {
		if (null == segment.getSegment())
			return segment;
		else
			return getParentSegmentBySegment(segmentService
					.getSegmentParentById(segment.getSegment().getSegmentId()));
	}

	public Segment getParentSegment(Implementation implementation,
			Integer accessId) throws Exception {
		return segmentService.getParentSegment(implementation, accessId);
	}

	public void saveSegment(Segment segment) throws Exception {
		segmentService.saveSegment(segment);
	}
	
	public void saveSegment(List<Segment> segments) throws Exception {
		segmentService.saveSegment(segments);
	}

	public void deleteSegment(Segment segment) throws Exception {
		segmentService.deleteSegment(segment);
	}

	// Getters & Setters
	public SegmentService getSegmentService() {
		return segmentService;
	}

	public void setSegmentService(SegmentService segmentService) {
		this.segmentService = segmentService;
	} 
}
