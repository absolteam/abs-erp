package com.aiotech.aios.accounts.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.AssetDepreciation;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetDepreciationService {

	private AIOTechGenericDAO<AssetDepreciation> assetDepreciationDAO;

	// Save Asset Depreciation
	public void saveAssetDepreciation(List<AssetDepreciation> assetDepreciations)
			throws Exception {
		assetDepreciationDAO.saveOrUpdateAll(assetDepreciations);
	}
	
	@SuppressWarnings("unchecked")
	public List<AssetDepreciation> getDepreciationCritriea(
			Implementation implementation, Long assetCreationId,
			Date fromDate, Date toDate) throws Exception {
		Criteria criteria = assetDepreciationDAO.createCriteria();

		criteria.createAlias("assetCreation", "ac",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("ac.implementation", implementation));
		}

		if (assetCreationId != null && assetCreationId > 0) {
			criteria.add(Restrictions.eq("ac.assetCreationId", assetCreationId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("period", fromDate, toDate));
		}
		return criteria.list();
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetDepreciation> getAssetDepreciationDAO() {
		return assetDepreciationDAO;
	}

	public void setAssetDepreciationDAO(
			AIOTechGenericDAO<AssetDepreciation> assetDepreciationDAO) {
		this.assetDepreciationDAO = assetDepreciationDAO;
	}

}
