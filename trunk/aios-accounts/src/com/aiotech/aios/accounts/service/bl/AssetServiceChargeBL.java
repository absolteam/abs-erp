package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetServiceCharge;
import com.aiotech.aios.accounts.domain.entity.AssetServiceDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AssetServiceChargeVO;
import com.aiotech.aios.accounts.service.AssetServiceChargeService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetServiceChargeBL {

	private AssetServiceChargeService assetServiceChargeService;
	private AssetServiceBL assetServiceBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show Asset Service Charges
	public List<Object> showServiceChargeList() throws Exception {
		List<AssetServiceDetail> assetServiceDetails = assetServiceBL
				.getAssetServiceService().getAllAssetServiceCharges(
						getImplementation());
		List<Object> assetServiceChargeVOs = new ArrayList<Object>();
		AssetServiceChargeVO assetServiceChargeVO = null;
		for (AssetServiceDetail assetServiceDetail : assetServiceDetails) {
			assetServiceChargeVO = new AssetServiceChargeVO();
			assetServiceChargeVO.setAssetServiceDetailId(assetServiceDetail
					.getAssetServiceDetailId());
			assetServiceChargeVO.setServiceNumber(assetServiceDetail
					.getAssetService().getServiceNumber());
			assetServiceChargeVO.setAssetName(assetServiceDetail
					.getAssetService().getAssetCreation().getAssetName());
			assetServiceChargeVO.setServiceDate(DateFormat
					.convertDateToString(assetServiceDetail.getServiceDate()
							.toString()));
			double amount = 0;
			for (AssetServiceCharge assetServiceCharge : assetServiceDetail
					.getAssetServiceCharges()) {
				amount += assetServiceCharge.getAmount();
			}
			assetServiceChargeVO.setAmount(amount);
			assetServiceChargeVOs.add(assetServiceChargeVO);
		}
		return assetServiceChargeVOs;
	}

	// Save Asset Service Charges
	public void saveAssetServiceCharge(
			List<AssetServiceCharge> assetServiceCharges,
			List<AssetServiceCharge> deleteAssetServiceCharges)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		if (null != deleteAssetServiceCharges
				&& deleteAssetServiceCharges.size() > 0)
			assetServiceChargeService
					.deleteAssetServiceCharge(deleteAssetServiceCharges);
		for (AssetServiceCharge assetServiceCharge : assetServiceCharges) {
			assetServiceCharge
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (assetServiceCharge != null
					&& assetServiceCharge.getAssetServiceChargeId() != null
					&& assetServiceCharge.getAssetServiceChargeId() > 0) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			assetServiceChargeService
					.saveAssetServiceCharge(assetServiceCharge);
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					AssetServiceCharge.class.getSimpleName(),
					assetServiceCharge.getAssetServiceChargeId(), user,
					workflowDetailVo);
		}
	}

	public void doAssetServiceChargeBusinessUpdate(Long assetServiceChargeId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				List<AssetServiceCharge> assetServiceCharges = this
						.getAssetServiceChargeService()
						.getAssetServiceChargeById(assetServiceChargeId);
				assetServiceChargeService
						.deleteAssetServiceCharge(assetServiceCharges);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Asset Service Charges
	public void deleteAssetServiceCharge(
			List<AssetServiceCharge> assetServiceCharges) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		for (AssetServiceCharge assetServiceCharge : assetServiceCharges) {
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					AssetServiceCharge.class.getSimpleName(),
					assetServiceCharge.getAssetServiceChargeId(), user,
					workflowDetailVo);
		}
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetServiceChargeService getAssetServiceChargeService() {
		return assetServiceChargeService;
	}

	public void setAssetServiceChargeService(
			AssetServiceChargeService assetServiceChargeService) {
		this.assetServiceChargeService = assetServiceChargeService;
	}

	public AssetServiceBL getAssetServiceBL() {
		return assetServiceBL;
	}

	public void setAssetServiceBL(AssetServiceBL assetServiceBL) {
		this.assetServiceBL = assetServiceBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
