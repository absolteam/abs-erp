package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetWarranty;
import com.aiotech.aios.accounts.domain.entity.vo.AssetWarrantyVO;
import com.aiotech.aios.accounts.service.AssetWarrantyService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetWarrantyBL {

	// Dependencies
	private AssetWarrantyService assetWarrantyService;
	private LookupMasterBL lookupMasterBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show All Asset Warranty
	public List<Object> showAllAssetWarranty() throws Exception {
		List<AssetWarranty> assetWarranties = assetWarrantyService
				.getAllAssetWarranty(getImplementation());
		List<Object> assetWarrantyVOs = new ArrayList<Object>();
		if (null != assetWarranties && assetWarranties.size() > 0) {
			for (AssetWarranty assetWarranty : assetWarranties)
				assetWarrantyVOs.add(addAssetWarrantyVO(assetWarranty));
		}
		return assetWarrantyVOs;
	}

	// Show Asset Warranty
	public Object getAssetWarrantyById(long assetWarrantyId) throws Exception {
		AssetWarranty assetWarranty = assetWarrantyService
				.getAssetWarrantyById(assetWarrantyId);
		return addAssetWarrantyVO(assetWarranty);
	}

	// Save Asset Warranty
	public void saveAssetWarranty(AssetWarranty assetWarranty) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetWarranty.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetWarranty != null && assetWarranty.getAssetWarrantyId() != null
				&& assetWarranty.getAssetWarrantyId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetWarrantyService.saveAssetWarranty(assetWarranty);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetWarranty.class.getSimpleName(),
				assetWarranty.getAssetWarrantyId(), user, workflowDetailVo);
	}

	public void doAssetWarrantyBusinessUpdate(Long assetWarrantyId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				AssetWarranty assetWarranty = assetWarrantyService
						.getSimpleAssetWarrantyById(assetWarrantyId);
				assetWarrantyService.deleteAssetWarranty(assetWarranty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Asset Warranty
	public void deleteAssetWarranty(AssetWarranty assetWarranty)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetWarranty.class.getSimpleName(),
				assetWarranty.getAssetWarrantyId(), user, workflowDetailVo);
	}

	// Add Asset Warranty
	private AssetWarrantyVO addAssetWarrantyVO(AssetWarranty assetWarranty) {
		AssetWarrantyVO assetWarrantyVO = new AssetWarrantyVO();
		assetWarrantyVO.setAssetWarrantyId(assetWarranty.getAssetWarrantyId());
		assetWarrantyVO.setWarrantyNumber(assetWarranty.getWarrantyNumber());
		assetWarrantyVO.setWarrantyType(assetWarranty.getLookupDetail()
				.getDisplayName());
		assetWarrantyVO.setFromDate(DateFormat
				.convertDateToString(assetWarranty.getStartDate().toString()));
		assetWarrantyVO.setToDate(DateFormat.convertDateToString(assetWarranty
				.getEndDate().toString()));
		assetWarrantyVO.setAssetName(assetWarranty.getAssetCreation()
				.getAssetName());
		assetWarrantyVO.setDescription(assetWarranty.getDescription());
		assetWarrantyVO.setAssetId(assetWarranty.getAssetCreation()
				.getAssetCreationId());
		assetWarrantyVO.setWarrantyTypeId(assetWarranty.getLookupDetail()
				.getLookupDetailId());
		return assetWarrantyVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetWarrantyService getAssetWarrantyService() {
		return assetWarrantyService;
	}

	public void setAssetWarrantyService(
			AssetWarrantyService assetWarrantyService) {
		this.assetWarrantyService = assetWarrantyService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
