package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.vo.PeriodVO;
import com.aiotech.aios.accounts.service.CalendarService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.to.Constants.Accounts.PeriodStatus;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;

public class CalendarBL {

	private AlertBL alertBL;
	private LedgerFiscalBL ledgerFiscalBL;
	private CalendarService calendarService;
	private Implementation implementation;
	private GLCombinationService combinationService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public void saveCalendar(Calendar calendar, List<Period> periods,
			List<Period> openPeriods, List<Period> closePeriods,
			List<Period> deletePeriods, User user, List<Period> tempClosePeriods) {
		try {
			calendar.setIsApprove((byte) WorkflowConstants.Status.Published
					.getCode());
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (calendar != null && calendar.getCalendarId() != null) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}

			boolean allPeriodClosed = true;
			for (Period period : periods) {
				if ((byte) period.getStatus() != (byte) PeriodStatus.Close
						.getCode()) {
					allPeriodClosed = false;
					break;
				}
			}

			if (allPeriodClosed)
				calendar.setStatus((byte) 2);

			if (null != deletePeriods && deletePeriods.size() > 0)
				calendarService.deletePeriods(deletePeriods);

			calendarService.addCalendar(calendar, periods);

			if (null != openPeriods && openPeriods.size() > 0)
				alertBL.bankReceiptPDCDeposit(openPeriods,
						calendar.getImplementation());

			if (null != tempClosePeriods && tempClosePeriods.size() > 0)
				ledgerFiscalBL.processPeriodLedgerFiscal(tempClosePeriods,
						calendar.getCalendarId());

			if (null != closePeriods && closePeriods.size() > 0)
				ledgerFiscalBL.processPeriodLedgerFiscal(closePeriods,
						calendar.getCalendarId());

			if (allPeriodClosed) {
				Combination combination = new Combination();
				combination.setCombinationId(getImplementation()
						.getOwnersEquity());
				List<Ledger> ledgers = combinationService
						.getIncomeRevenueLedgerList(getImplementation());
				Ledger retainsEarnings = getRetainEarnings(ledgers);
				Ledger pastEarnings = ledgerFiscalBL.getTransactionService()
						.getLedger(combination);
				ledgerFiscalBL.getTransactionService().getLedgerDAO()
						.refresh(pastEarnings);
				if (null == pastEarnings.getBalance())
					pastEarnings.setBalance(0d);

				double ledgerBalance = pastEarnings.getSide() ? (-1 * pastEarnings
						.getBalance()) : pastEarnings.getBalance();
				double transactionAmount = retainsEarnings.getSide() ? (-1 * retainsEarnings
						.getBalance()) : retainsEarnings.getBalance();

				double calculationAmount = 0.0;

				if (ledgerBalance > 0) {
					if (transactionAmount > 0) {
						calculationAmount = Math.abs(pastEarnings.getBalance())
								+ Math.abs(retainsEarnings.getBalance());
						pastEarnings.setBalance(Math.abs(calculationAmount));
						pastEarnings
								.setSide((calculationAmount <= 0 ? TransactionType.Debit
										.getCode() : TransactionType.Credit
										.getCode()));
					} else {
						calculationAmount = Math.abs(pastEarnings.getBalance())
								- Math.abs(retainsEarnings.getBalance());
						pastEarnings.setBalance(Math.abs(calculationAmount));
						pastEarnings
								.setSide((calculationAmount <= 0 ? TransactionType.Debit
										.getCode() : TransactionType.Credit
										.getCode()));
					}
				} else {
					if (transactionAmount > 0) {
						calculationAmount = Math.abs(pastEarnings.getBalance())
								- Math.abs(retainsEarnings.getBalance());
						pastEarnings.setBalance(Math.abs(calculationAmount));
						pastEarnings
								.setSide((calculationAmount >= 0 ? TransactionType.Debit
										.getCode() : TransactionType.Credit
										.getCode()));
					} else {
						calculationAmount = Math.abs(pastEarnings.getBalance())
								+ Math.abs(retainsEarnings.getBalance());
						pastEarnings.setBalance(Math.abs(calculationAmount));
						pastEarnings
								.setSide((calculationAmount >= 0 ? TransactionType.Debit
										.getCode() : TransactionType.Credit
										.getCode()));
					}
				}
				for (Ledger ledger : ledgers)
					ledger.setBalance(null);
				ledgers.add(pastEarnings);
				combinationService.saveLedger(ledgers);
			}

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					Calendar.class.getSimpleName(), calendar.getCalendarId(),
					user, workflowDetailVo);
		} catch (Exception e) {

		}
	}

	public void doCalendarBusinessUpdate(Long calendarId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				Calendar calendar = calendarService
						.getCalendarDetails(calendarId);
				calendarService.deletePeriod(calendar);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Save Calendar
	public void saveCalendar(Calendar calendar, List<Period> periods,
			List<Period> openPeriods, List<Period> closePeriods,
			List<Period> deletePeriods) {
		try {
			boolean allPeriodClosed = true;
			for (Period period : periods) {
				if ((byte) period.getStatus() != (byte) PeriodStatus.Close
						.getCode()) {
					allPeriodClosed = false;
					break;
				}
			}

			if (allPeriodClosed)
				calendar.setStatus((byte) 2);

			if (null != deletePeriods && deletePeriods.size() > 0)
				calendarService.deletePeriods(deletePeriods);

			calendarService.addCalendar(calendar, periods);
			if (null != openPeriods && openPeriods.size() > 0) {
				alertBL.bankReceiptPDCDeposit(openPeriods,
						calendar.getImplementation());
			}
			if (null != closePeriods && closePeriods.size() > 0)
				ledgerFiscalBL.processPeriodLedgerFiscal(closePeriods,
						calendar.getCalendarId());

			if (allPeriodClosed) {
				List<Ledger> ledgers = combinationService
						.getIncomeRevenueLedgerList(getImplementation());
				Ledger retainsEarnings = getRetainEarnings(ledgers);
				for (Ledger ledger : ledgers)
					ledger.setBalance(null);
				ledgers.add(retainsEarnings);
				combinationService.saveLedger(ledgers);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Ledger getRetainEarnings(List<Ledger> ledgers) throws Exception {
		double expenseTotal = 0;
		double revenueTotal = 0;
		Ledger retainsEarnings = new Ledger();
		for (Ledger ledger : ledgers) {
			if ((int) ledger.getCombination().getAccountByNaturalAccountId()
					.getAccountType() == (int) AccountType.Expenses.getCode()) {
				if (ledger.getSide())
					expenseTotal = expenseTotal + ledger.getBalance();
				else
					expenseTotal = expenseTotal - ledger.getBalance();
			} else {
				if (ledger.getSide())
					revenueTotal = revenueTotal - ledger.getBalance();
				else
					revenueTotal = revenueTotal + ledger.getBalance();
			}
		}
		double netAmount = 0;
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				netAmount = revenueTotal - expenseTotal;
			else
				netAmount = revenueTotal + expenseTotal;
			if (netAmount > 0) {
				retainsEarnings.setBalance(netAmount);
				retainsEarnings.setSide(TransactionType.Credit.getCode());
			} else {
				retainsEarnings.setBalance(Math.abs(netAmount));
				retainsEarnings.setSide(TransactionType.Debit.getCode());
			}
		} else {
			if (expenseTotal > 0)
				netAmount = expenseTotal - revenueTotal;
			else
				netAmount = expenseTotal + revenueTotal;
			if (netAmount > 0) {
				retainsEarnings.setBalance(netAmount);
				retainsEarnings.setSide(TransactionType.Debit.getCode());
			} else {
				retainsEarnings.setBalance(Math.abs(netAmount));
				retainsEarnings.setSide(TransactionType.Credit.getCode());
			}
		}
		Combination combination = new Combination();
		combination.setCombinationId(getImplementation().getOwnersEquity());
		retainsEarnings.setCombination(combination);
		if ((double) retainsEarnings.getBalance() == 0)
			retainsEarnings.setSide(TransactionType.Debit.getCode());
		return retainsEarnings;
	}

	// Show Selected Date period
	public List<?> showSelectedDatePeriod(boolean isImplementation,
			DateTime periodDate) throws Exception {
		List<Period> periods = null;
		if (isImplementation)
			periods = calendarService.getAllActivePeriods(getImplementation());
		else
			periods = calendarService.getAllActivePeriods();
		List<Object> periodVOs = null;
		if (null != periods && periods.size() > 0) {
			periodVOs = new ArrayList<Object>();
			boolean flag;
			for (Period period : periods) {
				flag = false;
				DateTime startDate = new DateTime(period.getStartTime());
				DateTime endDate = new DateTime(period.getEndTime());
				if (periodDate.isEqual(startDate)
						|| periodDate.isAfter(startDate)) {
					if (periodDate.isEqual(endDate)
							|| periodDate.isBefore(endDate))
						flag = true;
				}
				if (!flag) {
					if (null != period.getExtention()
							&& period.getExtention() > 0) {
						endDate = endDate.plusDays(period.getExtention());
						if (periodDate.isEqual(startDate)
								|| periodDate.isAfter(startDate)) {
							if (periodDate.isEqual(endDate)
									|| periodDate.isBefore(endDate))
								flag = true;
						}
					}
				}
				if (flag)
					periodVOs.add(addPeriodObject(period));
			}
		}
		return periodVOs;
	}

	public boolean transactionPostingPeriod(String periodDate) throws Exception {
		Period period = calendarService
				.getTransactionPostingPeriod(
						DateFormat.convertStringToDate(periodDate),
						getImplementation());
		return null != period ? true : false;
	}

	public Period transactionPostingPeriod(Date periodDate) throws Exception {
		Period period = calendarService.getTransactionPostingPeriod(periodDate,
				getImplementation());
		return period;
	}
	
	public Period transactionPostingPeriod(Date periodDate, Implementation implementation) throws Exception {
		Period period = calendarService.getTransactionPostingPeriod(periodDate,
				implementation);
		return period;
	}

	private PeriodVO addPeriodObject(Period period) throws Exception {
		PeriodVO periodVO = new PeriodVO();
		periodVO.setPeriodId(period.getPeriodId());
		periodVO.setName(period.getName());
		return periodVO;
	}

	// get session implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public CalendarService getCalendarService() {
		return calendarService;
	}

	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public LedgerFiscalBL getLedgerFiscalBL() {
		return ledgerFiscalBL;
	}

	public void setLedgerFiscalBL(LedgerFiscalBL ledgerFiscalBL) {
		this.ledgerFiscalBL = ledgerFiscalBL;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
