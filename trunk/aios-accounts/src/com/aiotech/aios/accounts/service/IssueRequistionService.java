/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class IssueRequistionService {

	private AIOTechGenericDAO<IssueRequistion> issueRequistionDAO;
	private AIOTechGenericDAO<IssueRequistionDetail> issueRequistionDetailDAO;

	// Get all Issue requisition
	public List<IssueRequistion> getAllIssueRequistions(
			Implementation implementation) throws Exception {
		return issueRequistionDAO.findByNamedQuery("getAllIssueRequistions",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Get Issue requisition by Id
	public IssueRequistion getIssueRequistionById(long issueRequistionId)
			throws Exception {
		return issueRequistionDAO.findByNamedQuery("getIssueRequistionById",
				issueRequistionId).get(0);
	}

	// Get Issue requisition by Id
	public IssueRequistion getIssueRequistionAndDetails(long issueRequistionId)
			throws Exception {
		return issueRequistionDAO.findByNamedQuery(
				"getIssueRequistionAndDetails", issueRequistionId).get(0);
	}

	// Get Issue requisition by Id
	public IssueRequistion getIssueRequistionByDetailId(
			long issueRequistionDetailId) throws Exception {
		return issueRequistionDAO.findByNamedQuery(
				"getIssueRequistionByDetailId", issueRequistionDetailId).get(0);
	}

	// Get Issue requisition by Id
	public IssueRequistionDetail getIssueRequistionDetailById(
			long issueRequistionDetailId) throws Exception {
		return issueRequistionDetailDAO.findByNamedQuery(
				"getIssueRequistionDetailById", issueRequistionDetailId).get(0);
	}

	public List<IssueRequistion> getIssueRequistionsByRequistionId(
			long requisitionId) {
		return issueRequistionDAO.findByNamedQuery(
				"getIssueRequistionsByRequistionId", requisitionId);
	}

	// Get Issue requisition by location id
	public List<IssueRequistionDetail> getLocationIssueRequistion(
			long locationId) throws Exception {
		return issueRequistionDetailDAO.findByNamedQuery(
				"getLocationIssueRequistion", locationId,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	// Save Issue Requisition
	public void saveIssueRequisition(IssueRequistion issueRequistion,
			List<IssueRequistionDetail> issueRequistionDetails)
			throws Exception {
		issueRequistionDAO.saveOrUpdate(issueRequistion);
		for (IssueRequistionDetail issueRequistionDetail : issueRequistionDetails) {
			issueRequistionDetail.setIssueRequistion(issueRequistion);
		}
		issueRequistionDetailDAO.saveOrUpdateAll(issueRequistionDetails);
	}

	// Delete Issue Requisition
	public void deleteIssueRequisition(IssueRequistion issueRequistion,
			List<IssueRequistionDetail> issueRequistionDetails)
			throws Exception {
		issueRequistionDetailDAO.deleteAll(issueRequistionDetails);
		issueRequistionDAO.delete(issueRequistion);
	}

	// Delete Issue Requisition Detail
	public void deleteIssueRequisitionDetail(
			List<IssueRequistionDetail> deleteRequisitionDetails) {
		issueRequistionDetailDAO.deleteAll(deleteRequisitionDetails);
	}

	// Get all requisition details
	public List<IssueRequistionDetail> getIssueRequistionDetail(
			long requistionById) throws Exception {
		return issueRequistionDetailDAO.findByNamedQuery(
				"getIssueRequistionDetail", requistionById);
	}

	@SuppressWarnings("unchecked")
	public List<IssueRequistion> getFilteredIssueRequistions(
			Implementation implementation, Long issueRequisitionId,
			Long locationId, Long personId, Date fromDate, Date toDate,
			Long productId) throws Exception {
		Criteria criteria = issueRequistionDAO.createCriteria();

		criteria.createAlias("cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.location", "loc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.department", "dept",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("implementation", "imp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetail", "src",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("issueRequistionDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.shelf", "slf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("slf.shelf", "rack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.aisle", "asl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("asl.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (issueRequisitionId != null && issueRequisitionId > 0) {
			criteria.add(Restrictions.eq("issueRequistionId",
					issueRequisitionId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		if (locationId != null && locationId > 0) {
			criteria.add(Restrictions.eq("loc.locationId", locationId));
		}

		if (personId != null && personId > 0) {
			criteria.add(Restrictions.eq("pr.personId", personId));
		}

		if (fromDate != null && !("").equals(fromDate)) {
			criteria.add(Restrictions.ge("issueDate", fromDate));
		}

		if (toDate != null && !("").equals(toDate)) {
			criteria.add(Restrictions.le("issueDate", toDate));
		}

		Set<IssueRequistion> uniqueRecords = new HashSet<IssueRequistion>(criteria.list());
		List<IssueRequistion> list = new ArrayList<IssueRequistion>(uniqueRecords);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<IssueRequistion> getFilteredIssueRequistions(
			Implementation implementation, Date fromDate, Date toDate,
			Set<Long> products,Long personId,Long departmentId,Long sectionId) throws Exception {
		Criteria criteria = issueRequistionDAO.createCriteria();

		criteria.createAlias("cmpDeptLocation", "cdl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.company", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.location", "loc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cdl.department", "dept",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("person", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("implementation", "imp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("lookupDetail", "src",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("issueRequistionDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.shelf", "slf",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("slf.shelf", "rack",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("rack.aisle", "asl",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("asl.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.lookupDetail", "sectionLook",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.issueReturnDetails", "issueReturn",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("issueReturn.issueReturn", "iReturn",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		
		if (personId != null) {
			criteria.add(Restrictions.eq("pr.personId", personId));
		}
		
		if (departmentId != null) {
			criteria.add(Restrictions.eq("dept.departmentId", departmentId));
		}
		
		if (sectionId != null) {
			criteria.add(Restrictions.eq("sectionLook.lookupDetailId", sectionId));
		}

		if (fromDate != null && !("").equals(fromDate)) {
			criteria.add(Restrictions.ge("issueDate", fromDate));
		}

		if (toDate != null && !("").equals(toDate)) {
			criteria.add(Restrictions.le("issueDate", toDate));
		}

		if (products != null && products.size() > 0) {
			criteria.add(Restrictions.in("prd.productId", products));
		}

		Set<IssueRequistion> uniqueRecords = new HashSet<IssueRequistion>(criteria.list());
		List<IssueRequistion> list = new ArrayList<IssueRequistion>(uniqueRecords);
		return list;
	}

	// Getters and Setters
	public AIOTechGenericDAO<IssueRequistion> getIssueRequistionDAO() {
		return issueRequistionDAO;
	}

	public void setIssueRequistionDAO(
			AIOTechGenericDAO<IssueRequistion> issueRequistionDAO) {
		this.issueRequistionDAO = issueRequistionDAO;
	}

	public AIOTechGenericDAO<IssueRequistionDetail> getIssueRequistionDetailDAO() {
		return issueRequistionDetailDAO;
	}

	public void setIssueRequistionDetailDAO(
			AIOTechGenericDAO<IssueRequistionDetail> issueRequistionDetailDAO) {
		this.issueRequistionDetailDAO = issueRequistionDetailDAO;
	}
}
