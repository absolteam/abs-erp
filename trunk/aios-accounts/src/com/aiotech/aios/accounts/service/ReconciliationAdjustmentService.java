/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustment;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustmentDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class ReconciliationAdjustmentService {

	private AIOTechGenericDAO<ReconciliationAdjustment> reconciliationAdjustmentDAO;
	private AIOTechGenericDAO<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetailDAO;
	
	//Get all reconcile adjustments by implementation
	public List<ReconciliationAdjustment> getAllReconciliationAdjustments(
			Implementation implementation) throws Exception {
		return reconciliationAdjustmentDAO.findByNamedQuery("getAllReconciliationAdjustments", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	//Get Reconciliation by id by detailed
	public ReconciliationAdjustment getReconciliationAdjustmentById(
			long reconciliationId) throws Exception {
		return reconciliationAdjustmentDAO.findByNamedQuery(
				"getReconciliationAdjustmentById", reconciliationId).get(0);
	}
	
	//Get Reconciliation by id
	public ReconciliationAdjustment getReconciliationAdjustment(
			long reconciliationId) throws Exception {
		return reconciliationAdjustmentDAO.findByNamedQuery(
				"getReconciliationAdjustment", reconciliationId).get(0);
	}
	
	public ReconciliationAdjustment findReconciliationAdjustmentById(
			long reconciliationId) throws Exception {
		return reconciliationAdjustmentDAO.findById(reconciliationId);
	}
	
	//Get Reconciliation Details
	public List<ReconciliationAdjustmentDetail> getReconciliationAdjustmentDetails(
			long reconciliationId) throws Exception {
		return reconciliationAdjustmentDetailDAO.findByNamedQuery(
				"getReconciliationAdjustmentDetails", reconciliationId);
	}
	
	//Save Adjustment Reconciliation
	public void saveReconciliationAdjustment(
			ReconciliationAdjustment reconciliationAdjustment,
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails)
			throws Exception {
		reconciliationAdjustmentDAO.saveOrUpdate(reconciliationAdjustment);
		for (ReconciliationAdjustmentDetail reconciliationAdjustmentDetail : reconciliationAdjustmentDetails) {
			reconciliationAdjustmentDetail
					.setReconciliationAdjustment(reconciliationAdjustment);
		}
		reconciliationAdjustmentDetailDAO
				.saveOrUpdateAll(reconciliationAdjustmentDetails);
	}
	
	//Delete Adjustment Reconciliation Details
	public void deleteReconciliationAdjustmentDetail(
			List<ReconciliationAdjustmentDetail> deletedAdjustmentDetails) throws Exception { 
		reconciliationAdjustmentDetailDAO.deleteAll(deletedAdjustmentDetails);
	} 
	
	//Delete Adjustment Reconciliation
	public void deleteReconciliationAdjustment(
			ReconciliationAdjustment reconciliationAdjustment,
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails)
			throws Exception {
		reconciliationAdjustmentDetailDAO
				.deleteAll(reconciliationAdjustmentDetails);
		reconciliationAdjustmentDAO.delete(reconciliationAdjustment);
	}
	
	//Getters and Setters
	public AIOTechGenericDAO<ReconciliationAdjustment> getReconciliationAdjustmentDAO() {
		return reconciliationAdjustmentDAO;
	}
	public void setReconciliationAdjustmentDAO(
			AIOTechGenericDAO<ReconciliationAdjustment> reconciliationAdjustmentDAO) {
		this.reconciliationAdjustmentDAO = reconciliationAdjustmentDAO;
	}
	public AIOTechGenericDAO<ReconciliationAdjustmentDetail> getReconciliationAdjustmentDetailDAO() {
		return reconciliationAdjustmentDetailDAO;
	}
	public void setReconciliationAdjustmentDetailDAO(
			AIOTechGenericDAO<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetailDAO) {
		this.reconciliationAdjustmentDetailDAO = reconciliationAdjustmentDetailDAO;
	} 
	
}
