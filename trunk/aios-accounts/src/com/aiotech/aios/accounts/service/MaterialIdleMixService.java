package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class MaterialIdleMixService {

	private AIOTechGenericDAO<MaterialIdleMix> materialIdleMixDAO;
	private AIOTechGenericDAO<MaterialIdleMixDetail> materialIdleMixDetailDAO;

	public List<MaterialIdleMix> getAllMaterialIdleMix(
			Implementation implementation) throws Exception {
		return materialIdleMixDAO.findByNamedQuery("getAllMaterialIdleMix",
				implementation);
	}

	public MaterialIdleMix getMaterialIdleMixbyId(long materialIdleMixId)
			throws Exception {
		return materialIdleMixDAO.findByNamedQuery("getMaterialIdleMixbyId",
				materialIdleMixId).get(0);
	}

	public MaterialIdleMix getMaterialIdleMixbyProduct(long productId) {
		List<MaterialIdleMix> materialIdleMix = materialIdleMixDAO
				.findByNamedQuery("getMaterialIdleMixbyProduct", productId);
		return null != materialIdleMix && materialIdleMix.size() > 0 ? materialIdleMix
				.get(0) : null;
	}

	public List<MaterialIdleMixDetail> getMaterialIdleMixDetails(
			long materialIdleMixId) throws Exception {
		return materialIdleMixDetailDAO.findByNamedQuery(
				"getMaterialIdleMixDetails", materialIdleMixId);
	}

	public void saveMaterialIdleMix(MaterialIdleMix materialIdleMix,
			List<MaterialIdleMixDetail> materialIdleMixDetails)
			throws Exception {
		materialIdleMixDAO.saveOrUpdate(materialIdleMix);
		for (MaterialIdleMixDetail materialIdleMixDetail : materialIdleMixDetails)
			materialIdleMixDetail.setMaterialIdleMix(materialIdleMix);
		materialIdleMixDetailDAO.saveOrUpdateAll(materialIdleMixDetails);
	}

	public void deleteMaterailIdleMixDetails(
			List<MaterialIdleMixDetail> materialIdleMixDetails)
			throws Exception {
		materialIdleMixDetailDAO.deleteAll(materialIdleMixDetails);
	}

	public void deleteMaterailIdleMix(MaterialIdleMix materialIdleMix,
			List<MaterialIdleMixDetail> materialIdleMixDetails)
			throws Exception {
		materialIdleMixDetailDAO.deleteAll(materialIdleMixDetails);
		materialIdleMixDAO.delete(materialIdleMix);
	}

	// Getters & Setters
	public AIOTechGenericDAO<MaterialIdleMix> getMaterialIdleMixDAO() {
		return materialIdleMixDAO;
	}

	public void setMaterialIdleMixDAO(
			AIOTechGenericDAO<MaterialIdleMix> materialIdleMixDAO) {
		this.materialIdleMixDAO = materialIdleMixDAO;
	}

	public AIOTechGenericDAO<MaterialIdleMixDetail> getMaterialIdleMixDetailDAO() {
		return materialIdleMixDetailDAO;
	}

	public void setMaterialIdleMixDetailDAO(
			AIOTechGenericDAO<MaterialIdleMixDetail> materialIdleMixDetailDAO) {
		this.materialIdleMixDetailDAO = materialIdleMixDetailDAO;
	}
}
