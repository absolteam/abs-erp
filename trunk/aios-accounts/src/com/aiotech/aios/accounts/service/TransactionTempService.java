package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.domain.entity.TransactionTempDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class TransactionTempService {

	private AIOTechGenericDAO<TransactionTemp> transactionTempDAO; 
	private AIOTechGenericDAO<TransactionTempDetail> transactionTempDetailDAO; 
	
	public List<TransactionTemp> getAllTransactions(
			Implementation implementation) throws Exception {
		return this.getTransactionTempDAO().findByNamedQuery(
				"getAllTempTransactions", implementation);
	}
	
	public TransactionTemp getJournalByTempId(Long transactionTempId) throws Exception{
		List<TransactionTemp> transactionTemps= this.getTransactionTempDAO().findByNamedQuery(
				"getTempTransactionById", transactionTempId);
		if(transactionTemps!=null && transactionTemps.size()>0)
			return transactionTemps.get(0);
		else
			return null;
	}
	
	public List<TransactionTemp> getTransactionTempJournalNo(
			String journalNumber, Implementation implementation) throws Exception {
		return this.getTransactionTempDAO().findByNamedQuery(
				"getTransactionTempJournalNo", journalNumber, implementation);
	}
	
	public TransactionTemp getTransactionTempJournalId(Long transactionId) throws Exception{
		return this.getTransactionTempDAO().findByNamedQuery(
				"getTransactionTempJournalId", transactionId).get(0);
	}
	
	public List<TransactionTempDetail> getTransactionTempDetails(
			Long transactionTempId) throws Exception {
		return this.getTransactionTempDetailDAO().findByNamedQuery(
				"getTransactionTempDetails", transactionTempId);
	}
	
	public void deleteTransactionTempDetail(
			List<TransactionTempDetail> transactionTempDetail) throws Exception {
		 this.getTransactionTempDetailDAO().deleteAll(
				transactionTempDetail);
	}
	
	public void saveTempTransaction(TransactionTemp transactionTemp,
			List<TransactionTempDetail> transactionTempDetailList)
			throws Exception {
		this.getTransactionTempDAO().saveOrUpdate(transactionTemp);
		for (TransactionTempDetail list : transactionTempDetailList) {
			list.setTransactionTemp(transactionTemp);
		}
		this.getTransactionTempDetailDAO().saveOrUpdateAll(
				transactionTempDetailList);
	}
	
	public void deleteTransactionTemp(TransactionTemp transactionTemp,
			List<TransactionTempDetail> transactionTempDetailList)
	throws Exception {
		this.getTransactionTempDetailDAO().deleteAll(transactionTempDetailList);
		this.getTransactionTempDAO().delete(transactionTemp);
	}

	
	public AIOTechGenericDAO<TransactionTemp> getTransactionTempDAO() {
		return transactionTempDAO;
	}

	public void setTransactionTempDAO(
			AIOTechGenericDAO<TransactionTemp> transactionTempDAO) {
		this.transactionTempDAO = transactionTempDAO;
	}

	public AIOTechGenericDAO<TransactionTempDetail> getTransactionTempDetailDAO() {
		return transactionTempDetailDAO;
	}

	public void setTransactionTempDetailDAO(
			AIOTechGenericDAO<TransactionTempDetail> transactionTempDetailDAO) {
		this.transactionTempDetailDAO = transactionTempDetailDAO;
	}
}
