package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.vo.CurrencyConversionVO;
import com.aiotech.aios.accounts.service.CurrencyConversionService;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CurrencyConversionBL {

	private CurrencyBL currencyBL;
	private CurrencyConversionService currencyConversionService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<Object> getAllCurrencyConversion() throws Exception {
		List<CurrencyConversion> currencyConversions = currencyConversionService
				.getAllCurrencyConversion(getImplementation());
		Currency currency = currencyBL.getDefaultCurrency();
		List<Object> currencyConversionVOs = new ArrayList<Object>();
		if (null != currencyConversions && currencyConversions.size() > 0) {
			for (CurrencyConversion currencyConversion : currencyConversions) {
				currencyConversionVOs.add(addCurrencyConversionVO(
						currencyConversion, currency));
			}
		}
		return currencyConversionVOs;
	}

	public void saveCurrencyConversions(
			List<CurrencyConversion> currencyConversions) throws Exception {
		WorkflowDetailVO workflowDetailVo = null;
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		for (CurrencyConversion currencyConversion : currencyConversions) {
			workflowDetailVo = new WorkflowDetailVO();
			if (currencyConversion != null
					&& currencyConversion.getCurrencyConversionId() != null) {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			currencyConversion
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			currencyConversionService
					.saveCurrencyConversion(currencyConversion);

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					CurrencyConversion.class.getSimpleName(),
					currencyConversion.getCurrencyConversionId(), user,
					workflowDetailVo);
		}
	}

	public void deleteCurrencyConversion(CurrencyConversion currencyConversion)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				CurrencyConversion.class.getSimpleName(),
				currencyConversion.getCurrencyConversionId(), user,
				workflowDetailVo);
	}

	public void doCurrencyConversionBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		if (workflowDetailVO.isDeleteFlag()) {
			CurrencyConversion currencyConversion = currencyConversionService
					.getCurrencyConversionById(recordId);
			currencyConversionService
					.deleteCurrencyConversion(currencyConversion);
		}
	}

	private CurrencyConversionVO addCurrencyConversionVO(
			CurrencyConversion currencyConversion, Currency functionalCurrency)
			throws Exception {
		CurrencyConversionVO currencyConversionVO = new CurrencyConversionVO();
		currencyConversionVO.setCurrencyConversionId(currencyConversion
				.getCurrencyConversionId());
		currencyConversionVO.setConversionCurrency(currencyConversion
				.getCurrency().getCurrencyPool().getCode()
				+ " - "
				+ currencyConversion.getCurrency().getCurrencyPool()
						.getCountry().getCountryName());
		currencyConversionVO.setConversionRate(currencyConversion
				.getConversionRate());
		currencyConversionVO.setFunctionalCurrency(functionalCurrency
				.getCurrencyPool().getCode()
				+ " - "
				+ functionalCurrency.getCurrencyPool().getCountry()
						.getCountryName());
		return currencyConversionVO;
	}

	public Double getTransactionValue(long poCurrencyId, long currencyId,
			double invQty, double invUnit, double exchangeRate)
			throws Exception {
		double totalInvoice = 0;
		if (currencyId > 0) {
			CurrencyConversion currencyConversion = null;
			Currency defaultCurrency = currencyBL.getDefaultCurrency();
			double exchangeValue = 0;
			if (poCurrencyId != currencyId) {
				if ((long) defaultCurrency.getCurrencyId() == currencyId) {
					exchangeValue = exchangeRate;
					totalInvoice = ((invQty * invUnit) * exchangeValue);
				} else if ((long) defaultCurrency.getCurrencyId() == (long) poCurrencyId) {
					currencyConversion = this.getCurrencyConversionService()
							.getCurrencyConversionByCurrency(currencyId);
					exchangeValue = ((1 / currencyConversion
							.getConversionRate()) * exchangeRate);
					totalInvoice = ((invQty * invUnit) * exchangeValue);
				} else {
					currencyConversion = this.getCurrencyConversionService()
							.getCurrencyConversionByCurrency(currencyId);
					exchangeValue = (1 / currencyConversion.getConversionRate());
					totalInvoice = ((invQty * invUnit) * (exchangeValue * exchangeRate));
				}
			} else
				totalInvoice = ((invQty * invUnit));
		} else
			totalInvoice = ((invQty * invUnit));
		return totalInvoice;
	}

	public Double getCurrencyConversionValue(long oldCurrency,
			long conversionCurrency, double transactionValue) {
		CurrencyConversion currencyConversion = null;
		CurrencyConversion oldCurrencyConversion = null;
		Currency defaultCurrency = currencyBL.getDefaultCurrency();
		double exchangeRate = 0;
		if ((long) defaultCurrency.getCurrencyId() == conversionCurrency) {
			currencyConversion = this.getCurrencyConversionService()
					.getCurrencyConversionByCurrency(oldCurrency);
			transactionValue = (transactionValue * currencyConversion
					.getConversionRate());
		} else if ((long) defaultCurrency.getCurrencyId() == oldCurrency) {
			currencyConversion = this.getCurrencyConversionService()
					.getCurrencyConversionByCurrency(conversionCurrency);
			exchangeRate = 1 / currencyConversion.getConversionRate();
			transactionValue = (transactionValue * exchangeRate);
		} else {
			currencyConversion = this.getCurrencyConversionService()
					.getCurrencyConversionByCurrency(conversionCurrency);
			oldCurrencyConversion = this.getCurrencyConversionService()
					.getCurrencyConversionByCurrency(oldCurrency);
			exchangeRate = (1 / currencyConversion.getConversionRate());
			transactionValue = ((transactionValue * oldCurrencyConversion
					.getConversionRate()) * exchangeRate);
		}
		return AIOSCommons.roundDecimals(transactionValue);
	}

	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public CurrencyBL getCurrencyBL() {
		return currencyBL;
	}

	public void setCurrencyBL(CurrencyBL currencyBL) {
		this.currencyBL = currencyBL;
	}

	public CurrencyConversionService getCurrencyConversionService() {
		return currencyConversionService;
	}

	public void setCurrencyConversionService(
			CurrencyConversionService currencyConversionService) {
		this.currencyConversionService = currencyConversionService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
