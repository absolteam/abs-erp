package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PaymentRequestDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PaymentRequestService {
	private AIOTechGenericDAO<PaymentRequest> paymentRequestDAO;
	private AIOTechGenericDAO<PaymentRequestDetail> paymentRequestDetailDAO;
	
	public List<PaymentRequest> getAllPaymentRequest(
			Implementation implementation) throws Exception {
		return this.getPaymentRequestDAO().findByNamedQuery(
				"getAllPaymentRequest", implementation,
				(byte)WorkflowConstants.Status.Published.getCode());
	}
	
	public PaymentRequest getPaymentRequestById(Long paymentReqId)
			throws Exception {
		return this.getPaymentRequestDAO()
				.findByNamedQuery("getPaymentRequestById", paymentReqId).get(0);
	}
	
	public List<PaymentRequestDetail> getRequestDetailById(
			Long paymentReqId) throws Exception {
		return this.getPaymentRequestDetailDAO().findByNamedQuery(
				"getRequestDetailById", paymentReqId);
	}
	
	public void savePaymentRequest(PaymentRequest paymentRequest,
			List<PaymentRequestDetail> paymentRequestDetails) throws Exception {
		this.getPaymentRequestDAO().saveOrUpdate(paymentRequest);
		for(PaymentRequestDetail requestDetail : paymentRequestDetails)
			requestDetail.setPaymentRequest(paymentRequest);
		this.getPaymentRequestDetailDAO()
				.saveOrUpdateAll(paymentRequestDetails);
	}

	public void deletePaymentRequestDetails(
			List<PaymentRequestDetail> paymentRequestDetails) throws Exception {
		this.getPaymentRequestDetailDAO().deleteAll(paymentRequestDetails);
	}
	
	public void deletePaymentRequest(PaymentRequest paymentRequest,
			List<PaymentRequestDetail> paymentRequestDetails) throws Exception {
		this.getPaymentRequestDetailDAO().deleteAll(paymentRequestDetails);
		this.getPaymentRequestDAO().delete(paymentRequest);
	}
	
	//get all approved payment request
	public List<PaymentRequest> findAllApprovedPaymentRequest(
			Implementation implementation, byte paymentMode, byte approveFlag) throws Exception {
		return this.getPaymentRequestDAO().findByNamedQuery(
				"findAllApprovedPaymentRequest", implementation, paymentMode, approveFlag);
	}
	
	public AIOTechGenericDAO<PaymentRequest> getPaymentRequestDAO() {
		return paymentRequestDAO;
	}
	public void setPaymentRequestDAO(
			AIOTechGenericDAO<PaymentRequest> paymentRequestDAO) {
		this.paymentRequestDAO = paymentRequestDAO;
	}

	public AIOTechGenericDAO<PaymentRequestDetail> getPaymentRequestDetailDAO() {
		return paymentRequestDetailDAO;
	}
	public void setPaymentRequestDetailDAO(
			AIOTechGenericDAO<PaymentRequestDetail> paymentRequestDetailDAO) {
		this.paymentRequestDetailDAO = paymentRequestDetailDAO;
	}
}
