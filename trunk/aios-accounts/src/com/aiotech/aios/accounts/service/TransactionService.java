package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyPool;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class TransactionService {

	private AIOTechGenericDAO<Transaction> transactionDAO;
	private AIOTechGenericDAO<TransactionDetail> transactionDetailDAO;
	private AIOTechGenericDAO<Currency> currencyDAO;
	private AIOTechGenericDAO<CurrencyPool> currencyPoolDAO;
	private AIOTechGenericDAO<Period> periodDAO;
	private AIOTechGenericDAO<Combination> combinationDAO;
	private AIOTechGenericDAO<Account> accountDAO;
	private AIOTechGenericDAO<Ledger> ledgerDAO;

	public List<Transaction> getJournalDetails(Implementation implementation)
			throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getAllJournals", implementation);
		return transactionList;
	}

	public List<Transaction> getAllTransactions(Implementation implementation)
			throws Exception {
		return transactionDAO.findByNamedQuery("getAllTransactions",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<Transaction> getTransactionsWithOutPos(
			Implementation implementation, String usecase) throws Exception {
		return transactionDAO.findByNamedQuery("getTransactionsWithOutPos",
				implementation,
				(byte) WorkflowConstants.Status.Published.getCode(), usecase);
	}

	public List<Transaction> getTransactionsByCategory(long categoryId)
			throws Exception {
		return transactionDAO.findByNamedQuery("getTransactionsByCategory",
				categoryId);
	}

	/**
	 * @param periodId
	 * @return transactions
	 * @throws Exception
	 */
	public List<Transaction> getTransactionsByPeriod(long periodId)
			throws Exception {
		return transactionDAO.findByNamedQuery("getTransactionsByPeriod",
				periodId);
	}

	/**
	 * @param periodId
	 * @return transaction list
	 * @throws Exception
	 */
	public List<Transaction> getTransactionsWithTransactionDetailsByPeriod(
			long periodId) throws Exception {
		return transactionDAO.findByNamedQuery(
				"getTransactionsWithTransactionDetailsByPeriod", periodId);
	}

	public Transaction journalById(long transactionId) throws Exception {
		return transactionDAO.findById(transactionId);
	}

	public TransactionDetail getTransationDetailById(long transactionDetilId)
			throws Exception {
		List<TransactionDetail> transactionDetails = transactionDetailDAO
				.findByNamedQuery("getTransationDetailById", transactionDetilId);
		return null != transactionDetails && transactionDetails.size() > 0 ? transactionDetails
				.get(0) : null;
	}

	public List<Transaction> getJournalEntries(Implementation implementation)
			throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalEntries", implementation);
		return transactionList;
	}

	public List<Transaction> getJournalEntriesByCalendar(
			Implementation implementation, long calendarId) throws Exception {
		return transactionDAO.findByNamedQuery("getJournalEntriesByCalendar",
				implementation, calendarId);
	}

	public List<Transaction> getBigJournalEntriesByCalendar(
			Implementation implementation, long calendarId, long transactionId)
			throws Exception {
		int size = 2000;
		return transactionDAO.findByNamedQuery(
				"getBigJournalEntriesByCalendar", size, implementation,
				transactionId, calendarId);
	}

	public List<Transaction> getJournalDateByEntries(
			Implementation implementation, Date fromDate, Date toDate)
			throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalDateByEntries", implementation, fromDate, toDate);
		return transactionList;
	}

	public List<Transaction> getJournalDateByEntries(Combination combination,
			Date fromDate, Date toDate) throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalDateByCombinationEntries", combination, fromDate,
				toDate);
		return transactionList;
	}

	@SuppressWarnings("unchecked")
	public List<Transaction> getFilteredJournal(Combination combination,
			Date fromDate, Date toDate) throws Exception {
		Criteria criteria = transactionDAO.createCriteria();

		criteria.createAlias("transactionDetails", "td",
				CriteriaSpecification.LEFT_JOIN);

		if (combination != null) {
			criteria.add(Restrictions.eq("td.combination", combination));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("transactionTime", fromDate,
					toDate));
		}

		return criteria.list();
	}

	public List<Transaction> getJournalEntriesByCombination(String queryStr)
			throws Exception {
		Query qry = transactionDAO
				.getNamedQuery("getJournalEntriesByCombination");
		queryStr = qry.getQueryString() + queryStr;
		return transactionDAO.find(queryStr);
	}

	public List<Transaction> getFilterJournalEntries(String queryStr)
			throws Exception {
		Query qry = transactionDAO.getNamedQuery("getFilterJournalEntries");
		queryStr = qry.getQueryString() + queryStr;
		return transactionDAO.find(queryStr);
	}

	public List<Transaction> getJournalByCombinationCategory(
			Implementation implementation, Long combinationId, Long categoryId)
			throws Exception {
		return transactionDAO.findByNamedQuery(
				"getJournalByCombinationCategory", implementation,
				combinationId, categoryId);
	}

	public List<Transaction> getJournalDetailsForJVNumber(
			Implementation implementation) throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalDetailsForJVNumber", implementation);

		return transactionList;
	}

	public List<Transaction> getJournalEntriesByFromDate(
			Implementation implementation, Date fromDate) throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalEntriesByFromDate", implementation, fromDate);
		return transactionList;
	}

	public List<Transaction> getJournalEntriesByToDate(
			Implementation implementation, Date toDate) throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalEntriesByToDate", implementation, toDate);
		return transactionList;
	}

	public List<Transaction> getJournalEntriesByCategory(
			Implementation implementation, long categoryId) throws Exception {
		List<Transaction> transactionList = transactionDAO.findByNamedQuery(
				"getJournalEntriesByCategory", implementation, categoryId);
		return transactionList;
	}

	public Transaction getTransactionById(long transactionId) throws Exception {
		return transactionDAO.findByNamedQuery("getTransactionById",
				transactionId).get(0);
	}

	public List<Transaction> getTransactionByRecordIdAndUseCase(long recordId,
			String useCase) throws Exception {
		return transactionDAO.findByNamedQuery(
				"getTransactionByRecordIdAndUseCase", recordId, useCase);
	}

	public List<Transaction> getTransactionByImplementationAndUseCase(
			Implementation implementation, String useCase) throws Exception {
		return transactionDAO.findByNamedQuery(
				"getTransactionByImplementationAndUseCase", implementation,
				useCase);
	}

	public List<Transaction> getTransactionByRecordIdAndUseCaseDetail(
			long recordId, String useCase) throws Exception {
		return transactionDAO.findByNamedQuery(
				"getTransactionByRecordIdAndUseCaseDetail", recordId, useCase);
	}

	public Currency getCurrencyInfo(int currencyId) throws Exception {
		return currencyDAO.findById(currencyId);
	}

	public CurrencyPool getCurrencyPoolInfo(int currencyPoolId)
			throws Exception {
		return currencyPoolDAO.findById(currencyPoolId);
	}

	public Period getPeriodInfo(long periodId) throws Exception {
		return periodDAO.findById(periodId);
	}

	public Ledger getAccountBalance(long combinationId) throws Exception {
		List<Ledger> ledgers = ledgerDAO.findByNamedQuery("getLegerDetails",
				combinationId);
		return null != ledgers && ledgers.size() > 0 ? ledgers.get(0) : null;
	}

	public Ledger getLedgerDetail(long combinationId) throws Exception {
		return ledgerDAO.findByNamedQuery("getLegerDetails", combinationId)
				.get(0);
	}

	public void saveAccountBalance(Ledger ledger) {
		ledgerDAO.saveOrUpdate(ledger);
	}

	public void saveAllAccountBalance(List<Ledger> ledgers) {
		ledgerDAO.saveOrUpdateAll(ledgers);
	}

	public void saveAccountBalance(Ledger ledger, Session session) {
		session.saveOrUpdate(ledger);
	}

	// to get currency details
	public List<Currency> getCurrencyDetails() {
		List<Currency> currencyList = currencyDAO.getAll();
		int i = 0;
		for (Currency list : currencyList) {
			CurrencyPool currencyPool = currencyPoolDAO.findById(currencyList
					.get(i).getCurrencyPool().getCurrencyPoolId());
			list.setCurrencyPool(currencyPool);
			i++;
		}
		return currencyList;
	}

	// to get calendar details
	public List<Period> getPeriodDetails() {
		List<Period> periodList = periodDAO.getAll();
		return periodList;
	}

	// to get combinations
	public List<Combination> getCombinations(Implementation implemenatation) {
		List<Combination> combinationList = combinationDAO.findByNamedQuery(
				"getAllCodeCombination", implemenatation);
		return combinationList;
	}

	// to get filter combinations
	public List<Combination> getFilterCombinations(Long accountTypeId,
			Implementation implemenatation) {
		List<Combination> combinationList = combinationDAO.findByNamedQuery(
				"getFilterCombinations", implemenatation,
				Integer.parseInt(accountTypeId.toString()));
		return combinationList;
	}

	// to get combinations
	public Combination getFilterCombination(Long accountTypeId,
			Implementation implemenatation) {
		return combinationDAO.findByNamedQuery("getFilterCombination",
				implemenatation, Integer.parseInt(accountTypeId.toString()))
				.get(0);
	}

	// to get account code details
	public Account getAllAccount(Long id) throws Exception {
		return accountDAO.findById(id);
	}

	// to get all company account
	public Account getAllAccountCodes(Implementation implementation,
			Long accountId) {
		return accountDAO.findByNamedQuery("getAllAccountCodes",
				implementation, Integer.parseInt(accountId.toString())).get(0);
	}

	public List<Transaction> getAllTransactionByCategory(Long categoryId)
			throws Exception {
		return transactionDAO.findByNamedQuery("getAllTransactionByCategory",
				categoryId);
	}

	// to save journal entries
	public void saveJournalEntries(Transaction transaction,
			List<TransactionDetail> transactionList) throws Exception {
		transactionDAO.saveOrUpdate(transaction);
		for (TransactionDetail list : transactionList) {
			list.setTransaction(transaction);
		}
		transactionDetailDAO.saveOrUpdateAll(transactionList);
	}

	public void saveJournalEntries(List<Transaction> transactions)
			throws Exception {
		transactionDAO.saveOrUpdateAll(transactions);
	}

	// to save journal entries
	public void saveJournalEntries(Transaction transaction,
			List<TransactionDetail> transactionList, Session session)
			throws Exception {
		session.saveOrUpdate(transaction);
		for (TransactionDetail transactionDetail : transactionList) {
			transactionDetail.setTransaction(transaction);
			session.saveOrUpdate(transactionDetail);
		}
	}

	public void updateJournalEntries(List<TransactionDetail> transactionDetails)
			throws Exception {
		transactionDetailDAO.saveOrUpdateAll(transactionDetails);
	}

	// to get journal by id
	public Transaction getJournalById(Long transactionId) throws Exception {
		return transactionDAO.findByNamedQuery("getJournalById", transactionId)
				.get(0);
	}

	// to get journal lines by id
	public List<TransactionDetail> getJournalLinesById(Long transactionId)
			throws Exception {
		List<TransactionDetail> transactionList = transactionDetailDAO
				.findByNamedQuery("getJournalLinesList", transactionId);
		return transactionList;
	}

	// to get combination id
	public Combination getCombinationById(Long transactionId) throws Exception {
		return combinationDAO.findById(transactionId);
	}

	public List<TransactionDetail> getJournalLinesdel(Transaction transaction)
			throws Exception {
		List<TransactionDetail> transactionList = transactionDetailDAO
				.findByNamedQuery("getJournalLinesdel",
						transaction.getTransactionId());
		return transactionList;
	}

	// to delete journal entries deleteJournalEntries
	public void deleteJournalEntries(Transaction transaction,
			List<TransactionDetail> transactionList) throws Exception {
		transactionDetailDAO.deleteAll(transactionList);
		transactionDAO.delete(transaction);
	}

	public void deleteTransactionDetail(List<TransactionDetail> transactionList)
			throws Exception {
		transactionDetailDAO.deleteAll(transactionList);
	}

	// to get transaction detail by implementation and flag
	public List<TransactionDetail> getJournalLines(Implementation implementation)
			throws Exception {
		return transactionDetailDAO.findByNamedQuery("getJournalLines",
				implementation);
	}

	// to get bank entries
	public List<TransactionDetail> getJournalBankEntries(
			Implementation implementation) throws Exception {
		return transactionDetailDAO.findByNamedQuery("getJournalBankLines",
				implementation);
	}

	public Ledger getLedger(Combination combination) throws Exception {
		return ledgerDAO.findByNamedQuery("getLedgerByTransactionDetailId",
				combination).get(0);
	}

	public void deleteLedgers(List<Ledger> ledgers) throws Exception {
		ledgerDAO.deleteAll(ledgers);
	}

	public List<TransactionDetail> getTransactionDetailsByCombinationId(
			Long combinationId) throws Exception {
		return transactionDetailDAO.findByNamedQuery(
				"getTransactionDetailsByCombinationId", combinationId);
	}

	public List<TransactionDetail> getTransactionByCombinationAndNonUsecase(
			Long combinationId, String useCase) throws Exception {
		return transactionDetailDAO.findByNamedQuery(
				"getTransactionByCombinationAndNonUsecase", combinationId,
				useCase);
	}

	public List<TransactionDetail> getTransactionDetailsByCombinationAndDate(
			Long combinationId, Date startDate, Date endDate) throws Exception {
		return transactionDetailDAO.findByNamedQuery(
				"getTransactionDetailsByCombinationAndDate", combinationId,
				startDate, endDate);
	}

	public List<TransactionDetail> getTransactionDetailsByCombinationAndDate(
			Long combinationId, Date startDate, Date endDate,
			Set<Long> transactionDetailIds) throws Exception {
		Criteria criteria = transactionDetailDAO.createCriteria();
		criteria.createAlias("transaction", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCompanyAccountId", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCostcenterAccountId", "cost",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByNaturalAccountId", "nt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("cb.combinationId", combinationId));
		criteria.add(Restrictions.ge("tr.transactionTime", startDate));
		criteria.add(Restrictions.le("tr.transactionTime", endDate));
		criteria.add(Restrictions.not(Restrictions.in("transactionDetailId",
				transactionDetailIds)));
		@SuppressWarnings("unchecked")
		Set<TransactionDetail> uniqueRecord = new HashSet<TransactionDetail>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<TransactionDetail>(
				uniqueRecord) : null;
	}

	public List<TransactionDetail> getTransactionDetailsByCombinationAndUseCase(
			Long combinationId, Set<Long> recordIds, String useCase)
			throws Exception {
		Criteria criteria = transactionDetailDAO.createCriteria();
		criteria.createAlias("transaction", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCompanyAccountId", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCostcenterAccountId", "cost",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByNaturalAccountId", "nt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("cb.combinationId", combinationId));
		criteria.add(Restrictions.and(
				Restrictions.in("tr.recordId", recordIds),
				Restrictions.eq("tr.useCase", useCase)));
		@SuppressWarnings("unchecked")
		Set<TransactionDetail> uniqueRecord = new HashSet<TransactionDetail>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<TransactionDetail>(
				uniqueRecord) : null;
	}

	public List<TransactionDetail> getTransactionDetailsByUseCaseDetail(
			Long combinationId, Set<Long> recordIds, String useCase)
			throws Exception {
		Criteria criteria = transactionDetailDAO.createCriteria();
		criteria.createAlias("transaction", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCompanyAccountId", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCostcenterAccountId", "cost",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByNaturalAccountId", "nt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("cb.combinationId", combinationId));
		criteria.add(Restrictions.and(Restrictions.in("recordId", recordIds),
				Restrictions.eq("useCase", useCase)));
		@SuppressWarnings("unchecked")
		Set<TransactionDetail> uniqueRecord = new HashSet<TransactionDetail>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<TransactionDetail>(
				uniqueRecord) : null;
	}

	public List<Transaction> getTransactionDetailByIds(Set<Long> recordIds,
			String useCase, long salesCombinationId,
			long salesDiscountCombinationId) throws Exception {
		Criteria criteria = transactionDAO.createCriteria();
		criteria.createAlias("transactionDetails", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tr.combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("recordId", recordIds));
		criteria.add(Restrictions.eq("useCase", useCase));
		criteria.add(Restrictions.or(
				Restrictions.eq("cb.combinationId", salesCombinationId),
				Restrictions.eq("cb.combinationId", salesDiscountCombinationId)));
		@SuppressWarnings("unchecked")
		Set<Transaction> uniqueRecord = new HashSet<Transaction>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Transaction>(
				uniqueRecord) : null;
	}

	public List<Transaction> getTransactionByReceive(
			Implementation implementation, String useCase, long categoryId)
			throws Exception {
		Criteria criteria = transactionDAO.createCriteria();
		criteria.createAlias("transactionDetails", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("category", "ct", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tr.combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("implementation", implementation));
		criteria.add(Restrictions.eq("ct.categoryId", categoryId));
		criteria.add(Restrictions.eq("useCase", useCase));
		criteria.add(Restrictions.isNull("tr.transactionReference"));
		@SuppressWarnings("unchecked")
		Set<Transaction> uniqueRecord = new HashSet<Transaction>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Transaction>(
				uniqueRecord) : null;
	}

	@SuppressWarnings("unchecked")
	public List<TransactionDetail> getTransactionDetailsOfProduct(Long productId)
			throws Exception {

		Session session = transactionDAO.getHibernateSession();
		Query query = session.getNamedQuery("getTransactionDetailsOfProduct")
				.setLong("productId", productId);

		return query.list();
	}

	public List<Ledger> getNaturalLedgerList(Implementation implemenation)
			throws Exception {
		return ledgerDAO
				.findByNamedQuery("getNaturalLedgerList", implemenation);
	}

	public List<Ledger> getAnalysisLedgerList(Implementation implemenation)
			throws Exception {
		return ledgerDAO.findByNamedQuery("getAnalysisLedgerList",
				implemenation);
	}

	public List<Transaction> getTransactionFilter(
			Implementation implementation, long categoryId, Long combinationId,
			Date fromDatee, Date toDatee) throws Exception {
		Criteria criteria = transactionDAO.createCriteria();
		criteria.createAlias("currency", "cr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("category", "cat", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("period", "prd", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("transactionDetails", "trd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("trd.combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCompanyAccountId", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCostcenterAccountId", "cost",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByNaturalAccountId", "nt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}
		if (categoryId > 0) {
			criteria.add(Restrictions.eq("cat.categoryId", categoryId));
		}
		if (null != combinationId && combinationId > 0) {
			criteria.add(Restrictions.eq("cb.combinationId", combinationId));
		}
		if (fromDatee != null) {
			criteria.add(Restrictions.ge("transactionTime", fromDatee));
		}
		if (toDatee != null) {
			criteria.add(Restrictions.le("transactionTime", toDatee));
		}
		@SuppressWarnings("unchecked")
		Set<Transaction> uniqueRecord = new HashSet<Transaction>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<Transaction>(
				uniqueRecord) : null;
	}

	public List<TransactionDetail> getTransactionDetailFilter(
			Implementation implementation, Set<Long> combinationIds,
			Date fromDatee, Date toDatee) throws Exception {
		Criteria criteria = transactionDetailDAO.createCriteria();
		criteria.createAlias("transaction", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCompanyAccountId", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCostcenterAccountId", "cost",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByNaturalAccountId", "nt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("nt.lookupDetail", "sbtp",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("tr.implementation", implementation));
		}

		if (null != combinationIds && combinationIds.size() > 0) {
			criteria.add(Restrictions.in("cb.combinationId", combinationIds));
		}
		if (fromDatee != null) {
			criteria.add(Restrictions.ge("tr.transactionTime", fromDatee));
		}
		if (toDatee != null) {
			criteria.add(Restrictions.le("tr.transactionTime", toDatee));
		}
		@SuppressWarnings("unchecked")
		Set<TransactionDetail> uniqueRecord = new HashSet<TransactionDetail>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<TransactionDetail>(
				uniqueRecord) : null;
	}

	public List<TransactionDetail> getTransactionDetailCombinationFilter(
			Set<Long> combinationIds) throws Exception {
		Criteria criteria = transactionDetailDAO.createCriteria();
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.in("cb.combinationId", combinationIds));
		@SuppressWarnings("unchecked")
		Set<TransactionDetail> uniqueRecord = new HashSet<TransactionDetail>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<TransactionDetail>(
				uniqueRecord) : null;
	}

	public List<TransactionDetail> getTransactionDetailStatementFilter(
			Implementation implementation, Set<Long> periods, Date fromDatee,
			Date toDatee, Integer accountType, Integer accountType1,
			Integer accountType2) throws Exception {
		Criteria criteria = transactionDetailDAO.createCriteria();
		criteria.createAlias("transaction", "tr",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("tr.period", "pr", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("combination", "cb",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCompanyAccountId", "cpy",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByCostcenterAccountId", "cost",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByNaturalAccountId", "nt",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByAnalysisAccountId", "ay",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByBuffer1AccountId", "b1",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cb.accountByBuffer2AccountId", "b2",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("nt.lookupDetail", "sbtp",
				CriteriaSpecification.LEFT_JOIN);
		if (implementation != null) {
			criteria.add(Restrictions.eq("tr.implementation", implementation));
		}

		if (null != accountType1 && accountType1 > 0) {
			if (null != accountType2 && accountType2 > 0)
				criteria.add(Restrictions.or(Restrictions.or(
						Restrictions.eq("nt.accountType", accountType),
						Restrictions.eq("nt.accountType", accountType1)),
						Restrictions.eq("nt.accountType", accountType2)));
			else
				criteria.add(Restrictions.or(
						Restrictions.eq("nt.accountType", accountType),
						Restrictions.eq("nt.accountType", accountType1)));
		} else
			criteria.add(Restrictions.eq("nt.accountType", accountType));

		if (fromDatee != null)
			criteria.add(Restrictions.ge("tr.transactionTime", fromDatee));

		if (toDatee != null)
			criteria.add(Restrictions.le("tr.transactionTime", toDatee));

		if (null != periods)
			criteria.add(Restrictions.in("pr.periodId", periods));
		@SuppressWarnings("unchecked")
		Set<TransactionDetail> uniqueRecord = new HashSet<TransactionDetail>(
				criteria.list());
		return null != uniqueRecord && uniqueRecord.size() > 0 ? new ArrayList<TransactionDetail>(
				uniqueRecord) : null;
	}

	// Getters&setters
	public AIOTechGenericDAO<Currency> getCurrencyDAO() {
		return currencyDAO;
	}

	public void setCurrencyDAO(AIOTechGenericDAO<Currency> currencyDAO) {
		this.currencyDAO = currencyDAO;
	}

	public AIOTechGenericDAO<CurrencyPool> getCurrencyPoolDAO() {
		return currencyPoolDAO;
	}

	public void setCurrencyPoolDAO(
			AIOTechGenericDAO<CurrencyPool> currencyPoolDAO) {
		this.currencyPoolDAO = currencyPoolDAO;
	}

	public AIOTechGenericDAO<Period> getPeriodDAO() {
		return periodDAO;
	}

	public void setPeriodDAO(AIOTechGenericDAO<Period> periodDAO) {
		this.periodDAO = periodDAO;
	}

	public AIOTechGenericDAO<Combination> getCombinationDAO() {
		return combinationDAO;
	}

	public void setCombinationDAO(AIOTechGenericDAO<Combination> combinationDAO) {
		this.combinationDAO = combinationDAO;
	}

	public AIOTechGenericDAO<Transaction> getTransactionDAO() {
		return transactionDAO;
	}

	public void setTransactionDAO(AIOTechGenericDAO<Transaction> transactionDAO) {
		this.transactionDAO = transactionDAO;
	}

	public AIOTechGenericDAO<TransactionDetail> getTransactionDetailDAO() {
		return transactionDetailDAO;
	}

	public void setTransactionDetailDAO(
			AIOTechGenericDAO<TransactionDetail> transactionDetailDAO) {
		this.transactionDetailDAO = transactionDetailDAO;
	}

	public AIOTechGenericDAO<Account> getAccountDAO() {
		return accountDAO;
	}

	public void setAccountDAO(AIOTechGenericDAO<Account> accountDAO) {
		this.accountDAO = accountDAO;
	}

	public AIOTechGenericDAO<Ledger> getLedgerDAO() {
		return ledgerDAO;
	}

	public void setLedgerDAO(AIOTechGenericDAO<Ledger> ledgerDAO) {
		this.ledgerDAO = ledgerDAO;
	}
}
