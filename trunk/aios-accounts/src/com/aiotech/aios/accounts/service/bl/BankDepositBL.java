package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.BankDepositService;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.POSPaymentType;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class BankDepositBL {
	private BankDepositService bankDepositService;
	private PointOfSaleBL pointOfSaleBL;
	private TransactionBL transactionBL;
	private DocumentBL documentBL;
	Implementation implementation;
	private DirectoryBL directoryBL;
	private BankReceiptsBL bankReceiptsBL;
	private BankBL bankBL;
	private AlertBL alertBL;
	private LookupMasterBL lookupMasterBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Save Bank Deposits and Create Deposits Transactions
	public void saveBankDepositTransaction(BankDeposit bankDeposit,
			List<BankDepositDetail> bankDepositDetails, long alertId,
			List<BankReceiptsDetail> bankReceiptsDetails,
			List<PointOfSaleReceipt> pointOfSaleReceipts) throws Exception {
		bankDeposit.setBankDepositDetails(new HashSet<BankDepositDetail>(
				bankDepositDetails));
		bankDeposit.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (bankDeposit != null && bankDeposit.getBankDepositId() != null
				&& bankDeposit.getBankDepositId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		boolean updateFlag = false;
		if (null != bankDeposit.getBankDepositId())
			updateFlag = true;
		if (updateFlag)
			transactionBL.deleteTransaction(bankDeposit.getBankDepositId(),
					BankDeposit.class.getSimpleName());
		else
			SystemBL.saveReferenceStamp(BankDeposit.class.getName(),
					getImplementation());

		// Save Bank Deposit & Deposit Details
		bankDepositService.saveBankDeposit(bankDeposit, bankDepositDetails);

		saveBankDepositDocuments(bankDeposit, false); // Save Deposit Documents

		if (null != bankReceiptsDetails && bankReceiptsDetails.size() > 0)
			bankReceiptsBL.getBankReceiptsService().updateBankReceiptsDetail(
					bankReceiptsDetails);

		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0)
			pointOfSaleBL.getPointOfSaleService().updatePointOfSaleReceipts(
					pointOfSaleReceipts);

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankDeposit.class.getSimpleName(),
				bankDeposit.getBankDepositId(), user, workflowDetailVo);
	}

	// Delete Bank Deposits and Reverse Deposit Transactions
	public void deleteBankDeposit(BankDeposit bankDeposit) throws Exception {

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				BankDeposit.class.getSimpleName(),
				bankDeposit.getBankDepositId(), user, workflowDetailVo);
	}

	public void doBankDepositBusinessUpdate(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		BankDeposit bankDeposit = bankDepositService
				.getBankDepositInfo(recordId);
		List<BankDepositDetail> bankDepositDetails = new ArrayList<BankDepositDetail>(
				bankDeposit.getBankDepositDetails());
		if (workflowDetailVO.isDeleteFlag()) {
			transactionBL.deleteTransaction(bankDeposit.getBankDepositId(),
					BankDeposit.class.getSimpleName());
			List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
			List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
			BankReceiptsDetail bankReceiptsDetail = null;
			PointOfSaleReceipt pointOfSaleReceipt = null;
			for (BankDepositDetail bankDepositDetail : bankDepositDetails) {
				if (null != bankDepositDetail.getBankReceiptsDetail()) {
					bankReceiptsDetail = bankDepositDetail
							.getBankReceiptsDetail();
					bankReceiptsDetail.setIsDeposited(false);
					bankReceiptsDetails.add(bankReceiptsDetail);
				}
				if (null != bankDepositDetail.getUseCase()
						&& ("PointOfSaleReceipt").equals(bankDepositDetail
								.getUseCase())) {
					pointOfSaleReceipt = pointOfSaleBL.getPointOfSaleService()
							.getPointOfSaleReceiptById(
									bankDepositDetail.getRecordId());
					pointOfSaleReceipt.setIsDeposited(false);
					pointOfSaleReceipts.add(pointOfSaleReceipt);
				}
			}
			if (null != bankReceiptsDetails && bankReceiptsDetails.size() > 0) {
				bankReceiptsBL.getBankReceiptsService()
						.mergeBankReceiptsDetail(bankReceiptsDetails);
			}
			if (null != bankReceiptsDetails && bankReceiptsDetails.size() > 0) {
				pointOfSaleBL.getPointOfSaleService().mergePointOfSaleReceipts(
						pointOfSaleReceipts);
			}
			// Delete Bank Deposit & Deposit Details
			bankDepositService.deleteBankDeposit(
					bankDeposit,
					new ArrayList<BankDepositDetail>(bankDeposit
							.getBankDepositDetails()));
		} else {
			// Entry transaction
			Transaction transaction = createBankDepositTransaction(bankDeposit,
					bankDepositDetails);
			transactionBL.saveTransaction(
					transaction,
					new ArrayList<TransactionDetail>(transaction
							.getTransactionDetails()));
		}
	}

	// Create bank receipt transaction
	private Transaction createBankDepositTransaction(BankDeposit bankDeposit,
			List<BankDepositDetail> bankDepositDetails) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		Long combinationId = null;
		double depositAmount = 0;
		for (BankDepositDetail detail : bankDepositDetails) {
			if (null != detail.getBankReceiptsDetail())
				combinationId = detail.getBankReceiptsDetail().getCombination()
						.getCombinationId();
			else {
				if ((byte) detail.getReceiptType() == (byte) POSPaymentType.Card
						.getCode())
					combinationId = getImplementation().getCreditCardAccount();
				else
					combinationId = getImplementation().getClearingAccount();
			}
			if (null != detail.getBankReceiptsDetail()
					&& (byte) detail.getBankReceiptsDetail().getPaymentMode() == (byte) ModeOfPayment.Cheque
							.getCode()) {
				transactionDetails.add(transactionBL.createTransactionDetail(
						detail.getAmount(), bankDeposit.getBankAccount()
								.getCombination().getCombinationId(),
						TransactionType.Debit.getCode(), "Bank Depoist Ref "
								+ bankDeposit.getBankDepositNumber(), null,
						bankDeposit.getBankDepositNumber(),
						detail.getBankDepositDetailId(),
						BankDepositDetail.class.getSimpleName()));
			} else
				depositAmount += detail.getAmount();
			transactionDetails.add(transactionBL.createTransactionDetail(
					detail.getAmount(), combinationId,
					TransactionType.Credit.getCode(), "Bank Depoist Ref "
							+ bankDeposit.getBankDepositNumber(), null,
					bankDeposit.getBankDepositNumber()));
		}
		if (depositAmount > 0)
			transactionDetails.add(transactionBL.createTransactionDetail(
					depositAmount, bankDeposit.getBankAccount()
							.getCombination().getCombinationId(),
					TransactionType.Debit.getCode(), "Bank Depoist Ref "
							+ bankDeposit.getBankDepositNumber(), null,
					bankDeposit.getBankDepositNumber()));
		Transaction transaction = transactionBL.createTransaction(transactionBL
				.getCurrency().getCurrencyId(), "Bank Depoist Ref "
				+ bankDeposit.getBankDepositNumber(), bankDeposit.getDate(),
				transactionBL.getCategory("Bank Deposit"), bankDeposit
						.getBankDepositId(), BankDeposit.class.getSimpleName());
		transaction.setTransactionDetails(new HashSet<TransactionDetail>(
				transactionDetails));
		return transaction;
	}

	public void saveBankDepositDocuments(BankDeposit bankDeposit,
			boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(bankDeposit,
				"depositDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();
		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, bankDeposit.getBankDepositId(), "BankDeposit",
					"depositDocs");
			docVO.setDirStruct(dirStucture);
		}
		documentBL.saveUploadDocuments(docVO);
	}

	public BankDepositService getBankDepositService() {
		return bankDepositService;
	}

	public void setBankDepositService(BankDepositService bankDepositService) {
		this.bankDepositService = bankDepositService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public BankBL getBankBL() {
		return bankBL;
	}

	public void setBankBL(BankBL bankBL) {
		this.bankBL = bankBL;
	}
}
