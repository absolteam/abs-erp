package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetDepreciation;
import com.aiotech.aios.accounts.domain.entity.AssetDetail;
import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;
import com.aiotech.aios.accounts.domain.entity.vo.AssetVO;
import com.aiotech.aios.accounts.service.AssetCreationService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class AssetCreationBL {

	private AssetCreationService assetCreationService;
	private CombinationBL combinationBL;
	private StockBL stockBL;
	private ProductBL productBL;
	private LookupMasterBL lookupMasterBL;
	private AssetDepreciationBL assetDepreciationBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show Asset JSON
	public List<Object> showAssetJsonList() throws Exception {
		List<AssetCreation> assets = assetCreationService
				.getAllAssets(getImplementation());
		List<Object> assetVOs = new ArrayList<Object>();
		if (null != assets && assets.size() > 0) {
			for (AssetCreation asset : assets)
				assetVOs.add(addAssetVO(asset));
		}
		return assetVOs;
	}

	// Show Asset JSON
	public List<Object> showAssetCheckOutList() throws Exception {
		List<AssetCreation> assets = assetCreationService
				.getAssetCheckOuts(getImplementation());
		List<Object> assetVOs = new ArrayList<Object>();
		if (null != assets && assets.size() > 0) {
			for (AssetCreation asset : assets)
				assetVOs.add(addAssetVO(asset));
		}
		return assetVOs;
	}

	private AssetVO addAssetVO(AssetCreation asset) throws Exception {
		AssetVO assetVO = new AssetVO();
		assetVO.setAssetCreationId(asset.getAssetCreationId());
		assetVO.setAssetNumber(asset.getAssetNumber());
		assetVO.setAssetName(asset.getAssetName());
		assetVO.setAssetClassCategory(Constants.Accounts.AssetCategory.get(
				asset.getAssetClass()).name());
		assetVO.setAssetSubClass(asset.getLookupDetailByAssetSubclass()
				.getDisplayName());
		assetVO.setAssetType(asset.getLookupDetailByAssetType()
				.getDisplayName());
		assetVO.setAssetMake(asset.getAssetMake());
		assetVO.setCondition(null != asset.getAssetCondition() ? Constants.Accounts.AssetCondition
				.get(asset.getAssetCondition()).name() : "");
		if (null != asset.getPerson() && !("").equals(asset.getPerson())) {
			assetVO.setOwnerName(asset.getPerson().getFirstName().concat(" ")
					.concat(asset.getPerson().getLastName()));
		} else if (null != asset.getCompany()
				&& !("").equals(asset.getCompany())) {
			assetVO.setOwnerName(asset.getCompany().getCompanyName());
		}
		return assetVO;
	}
	
	public AssetVO addAssetToVO(AssetCreation asset) throws Exception {
		AssetVO assetVO = new AssetVO();
		BeanUtils.copyProperties(assetVO, asset);
		assetVO.setAssetCreationId(asset.getAssetCreationId());
		assetVO.setAssetNumber(asset.getAssetNumber());
		assetVO.setAssetName(asset.getAssetName());
		assetVO.setAssetClassCategory(Constants.Accounts.AssetCategory.get(
				asset.getAssetClass()).name());
		assetVO.setAssetSubClass(asset.getLookupDetailByAssetSubclass()
				.getDisplayName());
		assetVO.setAssetType(asset.getLookupDetailByAssetType()
				.getDisplayName());
		assetVO.setAssetMake(asset.getAssetMake());
		assetVO.setCondition(null != asset.getAssetCondition() ? Constants.Accounts.AssetCondition
				.get(asset.getAssetCondition()).name() : "");
		if (null != asset.getPerson() && !("").equals(asset.getPerson())) {
			assetVO.setOwnerName(asset.getPerson().getFirstName().concat(" ")
					.concat(asset.getPerson().getLastName()));
		} else if (null != asset.getCompany()
				&& !("").equals(asset.getCompany())) {
			assetVO.setOwnerName(asset.getCompany().getCompanyName());
		}
		return assetVO;
	}

	// Get Asset Value Detail
	public AssetVO getAssetValueDetail(AssetCreation assetCreation,
			String disposalDate) throws Exception {
		AssetVO assetVO = assetDepreciationBL.addDepreciationVO(assetCreation);
		double monthDifference = 0;
		if (null != assetCreation.getAssetDepreciations()
				&& assetCreation.getAssetDepreciations().size() > 0) {
			List<AssetDepreciation> assetDepreciations = new ArrayList<AssetDepreciation>(
					assetCreation.getAssetDepreciations());
			for (AssetDepreciation assetDepreciation : assetDepreciations)
				assetVO.setDepreciationExpenses(null != assetVO
						.getDepreciationExpenses() ? assetVO
						.getDepreciationExpenses() : 0 + assetDepreciation
						.getDepreciation());
			Collections.sort(assetDepreciations,
					new Comparator<AssetDepreciation>() {
						public int compare(AssetDepreciation d1,
								AssetDepreciation d2) {
							return d2.getPeriod().compareTo(d1.getPeriod());
						}
					});
			monthDifference = DateFormat.monthVariance(assetDepreciations
					.get(0).getPeriod(), DateFormat
					.convertStringToDate(disposalDate));
		} else
			monthDifference = DateFormat.monthVariance(
					assetCreation.getCommenceDate(),
					DateFormat.convertStringToDate(disposalDate));
		AssetDepreciation depreciation = assetDepreciationBL
				.addAssetDepreciation(assetVO);
		assetVO.setDepreciationExpenses(((null != assetVO
				.getDepreciationExpenses() ? assetVO.getDepreciationExpenses()
				: 0) + depreciation.getDepreciation())
				* monthDifference);
		if (null != assetCreation.getAssetRevaluations()
				&& assetCreation.getAssetRevaluations().size() > 0) {
			for (AssetRevaluation assetRevaluation : assetCreation
					.getAssetRevaluations())
				assetVO.setAccumulatedSurplus(null != assetVO
						.getAccumulatedSurplus() ? (assetRevaluation
						.getFairMarketValue() - assetRevaluation
						.getAssetBookValue())
						+ (assetVO.getAccumulatedSurplus()) : (assetRevaluation
						.getFairMarketValue() - assetRevaluation
						.getAssetBookValue()));
		}
		AssetVO objectVO = new AssetVO();
		objectVO.setAssetCost(assetVO.getAssetCost());
		objectVO.setDepreciationExpenses(assetVO.getDepreciationExpenses());
		objectVO.setBookValue(assetVO.getAssetCost()
				- assetVO.getDepreciationExpenses());
		objectVO.setAccumulatedSurplus(assetVO.getAccumulatedSurplus());
		objectVO.setShowAccumulatedDepreciation(AIOSCommons
				.formatAmount(assetVO.getDepreciationExpenses()));
		objectVO.setShowAssetCost(AIOSCommons.formatAmount(assetVO
				.getAssetCost()));
		objectVO.setShowBookValue(AIOSCommons.formatAmount(objectVO
				.getBookValue()));
		objectVO.setShowAccumulatedSurplus(null != objectVO
				.getAccumulatedSurplus() ? (objectVO.getAccumulatedSurplus() >= 0 ? (AIOSCommons
				.formatAmount(objectVO.getAccumulatedSurplus())) : "-"
				.concat((AIOSCommons.formatAmount(Math.abs(objectVO
						.getAccumulatedSurplus())))))
				: null);
		return objectVO;
	}

	// Save Asset Details
	public void saveAssetCreation(AssetCreation assetCreation,
			List<AssetDetail> assetDetailList,
			List<AssetDetail> deleteAssetDetails, boolean updateFlag)
			throws Exception {
		assetCreation.setImplementation(getImplementation());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		assetCreation.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());

		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (assetCreation != null && assetCreation.getAssetCreationId() != null
				&& assetCreation.getAssetCreationId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		assetCreationService.saveAssetCreation(assetCreation, assetDetailList,
				deleteAssetDetails);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				AssetCreation.class.getSimpleName(),
				assetCreation.getAssetCreationId(), user, workflowDetailVo);
	}

	public void doAssetCreationBusinessUpdate(Long assetCreationId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				AssetCreation assetCreation = this.getAssetCreationService()
						.getAssetDetailInfo(assetCreationId);
				assetCreationService.deleteAssetCreation(
						assetCreation,
						new ArrayList<AssetDetail>(assetCreation
								.getAssetDetails()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Asset Creation
	public void deleteAssetCreation(AssetCreation assetCreation)
			throws Exception { 
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService
				.generateNotificationsAgainstDataEntry(
						AssetCreation.class.getSimpleName(), assetCreation.getAssetCreationId(),
						user, workflowDetailVo); 
	}

	// Generate Asset Number
	public String generateAssetNumber() throws Exception {
		List<AssetCreation> assets = assetCreationService
				.getSimpleAssetInfo(getImplementation());
		if (null != assets && assets.size() > 0) {
			List<Long> assetNumbers = new ArrayList<Long>();
			for (AssetCreation asset : assets) {
				assetNumbers.add(Long.valueOf(asset.getAssetNumber()));
			}
			long tempAssetNumber = Collections.max(assetNumbers);
			return String.valueOf(tempAssetNumber += 1);
		}
		return "10000";
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public AssetCreationService getAssetCreationService() {
		return assetCreationService;
	}

	public void setAssetCreationService(
			AssetCreationService assetCreationService) {
		this.assetCreationService = assetCreationService;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public AssetDepreciationBL getAssetDepreciationBL() {
		return assetDepreciationBL;
	}

	public void setAssetDepreciationBL(AssetDepreciationBL assetDepreciationBL) {
		this.assetDepreciationBL = assetDepreciationBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
