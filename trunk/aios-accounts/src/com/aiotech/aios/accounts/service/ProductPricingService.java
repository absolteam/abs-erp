/**
 * 
 */
package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * 
 * @author Saleem
 */
public class ProductPricingService {

	private AIOTechGenericDAO<ProductPricing> productPricingDAO;
	private AIOTechGenericDAO<ProductPricingDetail> productPricingDetailDAO;
	private AIOTechGenericDAO<ProductPricingCalc> productPricingCalcDAO;
	
	// Get all product pricing
	public List<ProductPricing> getAllProductPricing(
			Implementation implementation) throws Exception {
		return productPricingDAO.findByNamedQuery("getAllProductPricing",
				implementation);
	}

	// Get all product pricing
	public List<ProductPricingDetail> getAllProductPricingDetail(
			Implementation implementation) throws Exception {
		return productPricingDetailDAO.findByNamedQuery(
				"getAllProductPricingDetail", implementation, (byte)WorkflowConstants.Status.Published.getCode());
	}

	public List<ProductPricingCalc> getAllProductPricingCalc() throws Exception {
		return productPricingCalcDAO
				.findByNamedQuery("getAllProductPricingCalc");
	}

	// Get Product Pricing by Product Pricing Id
	public ProductPricing getProductPricing(long productPricingId)
			throws Exception {
		return productPricingDAO.findByNamedQuery("getProductPricing",
				productPricingId).get(0);
	}

	// Get Product Pricing by Product Pricing Id
	public ProductPricing getProductPricingAll(long productPricingId)
			throws Exception {
		return productPricingDAO.findByNamedQuery("getProductPricingAll",
				productPricingId).get(0);
	}

	// Save Product Pricing
	public void saveProductPricing(ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails) throws Exception {
		productPricingDAO.saveOrUpdate(productPricing);
		for (ProductPricingDetail productPricingDetail : productPricingDetails) {
			productPricingDetail.setProductPricing(productPricing);
		}
		productPricingDetailDAO.saveOrUpdateAll(productPricingDetails);

	}

	// Save Product Pricing
	public void saveProductPricing(ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails,
			List<ProductPricingCalc> productPricingCalc) throws Exception {
		productPricingDAO.saveOrUpdate(productPricing);
		productPricingDetailDAO.saveOrUpdateAll(productPricingDetails);
		productPricingCalcDAO.saveOrUpdateAll(productPricingCalc);
	}
	
	// Save Product Pricing
	public void saveProductPricing(ProductPricing productPricing)
			throws Exception {
		productPricingDAO.saveOrUpdate(productPricing);
	}
	
	// Save Product Pricing Detail
	public void saveProductPricingDetail(List<ProductPricingDetail> productPricingDetails)
			throws Exception {
		productPricingDetailDAO.saveOrUpdateAll(productPricingDetails);
	}
	
	// Save Product Pricing Detail
	public void mergeProductPricingDetail(List<ProductPricingDetail> productPricingDetails)
			throws Exception {
		for (ProductPricingDetail productPricingDetail : productPricingDetails)  
			productPricingDetailDAO.merge(productPricingDetail); 
	}
	
	// Save Product Pricing Calc
	public void saveProductPricingCalcs(List<ProductPricingCalc> productPricingCalcs)
			throws Exception {
		productPricingCalcDAO.saveOrUpdateAll(productPricingCalcs);
	}
	// Delete Product Pricing Detail
	public void deleteProductPricingDetail(List<ProductPricingDetail> productPricingDetails)
			throws Exception {
		productPricingDetailDAO.deleteAll(productPricingDetails);
	}
	
	// Save Product Pricing
	public void deleteProductPricingCalc(List<ProductPricingCalc> productPricingCalcs)
			throws Exception {
		productPricingCalcDAO.deleteAll(productPricingCalcs);
	}

	// Save Product Pricing
	public void saveProductPricingDetailAndCalc(
			List<ProductPricingDetail> productPricingDetails,
			List<ProductPricingCalc> productPricingCalcs) throws Exception {
		productPricingDetailDAO.saveOrUpdateAll(productPricingDetails);
		productPricingCalcDAO.saveOrUpdateAll(productPricingCalcs);
	}

	// Save Product Pricing Calculation
	public void saveProductPricingCalc(
			List<ProductPricingCalc> productPricingCalcs) throws Exception {
		productPricingCalcDAO.saveOrUpdateAll(productPricingCalcs);
	}

	// Delete Product Pricing
	public void deleteProductPricing(ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails,
			List<ProductPricingCalc> productPricingCalcs) throws Exception {
		productPricingCalcDAO.deleteAll(productPricingCalcs);
		productPricingDetailDAO.deleteAll(productPricingDetails);
		productPricingDAO.delete(productPricing);
	}

	// Delete Product Pricing
	public void deleteProductPricingDetail(
			ProductPricingDetail productPricingDetail,
			List<ProductPricingCalc> productPricingCalcs) throws Exception {
		productPricingCalcDAO.deleteAll(productPricingCalcs);
		productPricingDetailDAO.delete(productPricingDetail);
	}

	// Get Product Pricing With Customer
	public List<ProductPricing> getProductPricingWithCustomer(long productId,
			long storeId, long customerId, Date startDate, Date endDate)
			throws Exception {
		return productPricingDAO.findByNamedQuery(
				"getProductPricingWithCustomer", startDate, endDate, productId,
				storeId, customerId);
	}
	
	public List<ProductPricingDetail> getProductPricingDetailByDate(
			Date startDate, Date endDate, Implementation implementation) throws Exception {
		return productPricingDetailDAO.findByNamedQuery(
				"getProductPricingDetailByDate", implementation, startDate, endDate);
	}

	public List<ProductPricing> getProductPricingWithoutStoreAndCustomer(
			long productId, Date startDate, Date endDate) throws Exception {
		return productPricingDAO.findByNamedQuery(
				"getProductPricingWithoutCustomerAndStore", startDate, endDate,
				productId);
	}
	
	public ProductPricing getProductPricingWithProductAndDate(long productId,
			Date startDate, Date endDate) throws Exception {
		List<ProductPricing> productPricings = productPricingDAO
				.findByNamedQuery("getProductPricingWithProductAndDate",
						startDate, endDate, productId);
		return null != productPricings && productPricings.size() > 0 ? productPricings
				.get(0) : null;
	}

	// Get Product Pricing With Customer
	@SuppressWarnings("unchecked")
	public List<ProductPricing> getProductPricingCustomerWithoutStore(
			long productId, long customerId, Date startDate, Date endDate)
			throws Exception {
		boolean flag = true;
		Criteria priceCriteria = productPricingDAO.createCriteria();
		priceCriteria.setFetchMode("currency", FetchMode.JOIN);
		priceCriteria.createAlias("productPricingDetails", "prd");
		priceCriteria.createAlias("prd.product", "prod");
		priceCriteria.createAlias("prd.productPricingCalcs", "ppc",
				CriteriaSpecification.LEFT_JOIN);
		priceCriteria.setFetchMode("ppc.lookupDetail", FetchMode.JOIN);
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("ppc.customer.customerId", customerId)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("prod.productId", productId)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.le("startDate", startDate)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.ge("endDate", endDate)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.ne("ppc.comboType", flag)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("ppc.status", flag)));
		return priceCriteria.list();
	}

	// Get Product Pricing With Customer
	@SuppressWarnings("unchecked")
	public List<ProductPricing> getProductPricingCriteriaWithoutStoreAndCustomer(
			long productId, Date startDate, Date endDate) throws Exception {
		boolean flag = true;
		Criteria priceCriteria = productPricingDAO.createCriteria();
		priceCriteria.setFetchMode("currency", FetchMode.JOIN);
		priceCriteria.createAlias("productPricingDetails", "prd");
		priceCriteria.createAlias("prd.product", "prod");
		priceCriteria.createAlias("prd.productPricingCalcs", "ppc",
				CriteriaSpecification.LEFT_JOIN);
		priceCriteria.setFetchMode("ppc.lookupDetail", FetchMode.JOIN);
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("prod.productId", productId)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.le("startDate", startDate)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.ge("endDate", endDate)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.ne("ppc.comboType", flag)));
		priceCriteria.add(Restrictions.conjunction().add(
				Restrictions.eq("ppc.status", flag)));
		Set<ProductPricing> uniqueRecords = new HashSet<ProductPricing>(
				priceCriteria.list());
		List<ProductPricing> productPricings = new ArrayList<ProductPricing>(
				uniqueRecords);
		return productPricings;
	}

	// Get Product Pricing Without Customer
	public List<ProductPricing> getProductPricingWithoutCustomer(
			long productId, long storeId, Date startDate, Date endDate)
			throws Exception {
		List<ProductPricing> productPricings = productPricingDAO
				.findByNamedQuery("getProductPricingWithoutCustomer",
						startDate, endDate, productId, storeId);
		return productPricings;
	}

	// Get Product Pricing Without Customer & Store
	public List<ProductPricing> getProductPricingWithoutCustomerAndStore(
			long productId, Date startDate, Date endDate) throws Exception {
		List<ProductPricing> productPricings = productPricingDAO
				.findByNamedQuery("getProductPricingWithoutCustomerAndStore",
						startDate, endDate, productId);
		return productPricings;
	}
	
	// Get Product Pricing Without Customer & Store
	public List<ProductPricing> getProductPricingDetailByProduct(
			long productId, Date startDate, Date endDate) throws Exception {
		List<ProductPricing> productPricings = productPricingDAO
				.findByNamedQuery("getProductPricingDetailByProduct",
						startDate, endDate, productId);
		return productPricings;
	}
	
	// Get Product Pricing Without Customer & Store
	public List<ProductPricing> getPricingDetailWithProductStore(
			long productId, long storeId, Date startDate, Date endDate) throws Exception {
		List<ProductPricing> productPricings = productPricingDAO
				.findByNamedQuery("getPricingDetailWithProductStore",
						startDate, endDate, productId, storeId);
		return productPricings;
	}

	// Get Product Pricing Without Customer & Store
	public List<ProductPricing> getComboProductPricing(long productId,
			Date startDate, Date endDate) throws Exception {
		List<ProductPricing> productPricings = productPricingDAO
				.findByNamedQuery("getComboProductPricing", startDate, endDate,
						productId);
		return productPricings;
	}

	// Get Product Pricing Calc without customer
	public ProductPricingCalc getProductPricingCalcWithoutCustomer(
			long productId, long storeId, Date startDate, Date endDate,
			long pricingType) {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery("getProductPricingCalcWithoutCustomer",
						startDate, endDate, productId, storeId, pricingType);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Product Pricing Calc without customer
	public ProductPricingCalc getProductPricingCalcWithoutCustomerAndStore(
			long productId, Date startDate, Date endDate, long pricingType) {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery(
						"getProductPricingCalcWithoutCustomerAndStore",
						startDate, endDate, productId, pricingType);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Product Pricing Calc with customer
	public ProductPricingCalc getProductPricingCalcWithCustomer(long productId,
			long customerId, long storeId, Date startDate, Date endDate,
			long pricingType) {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery("getProductPricingCalcWithCustomer",
						startDate, endDate, productId, customerId, storeId,
						pricingType);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Product Pricing Calc with customer without store
	public ProductPricingCalc getProductPricingCalcWithCustomerWithOutStore(
			long productId, long customerId, Date startDate, Date endDate,
			long pricingType) {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery(
						"getProductPricingCalcWithCustomerWithOutStore",
						startDate, endDate, productId, customerId, pricingType);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Product Pricing Calc with customer without store
	public ProductPricingCalc getCustomerSalesPrice(long productId,
			long customerId, Date startDate, Date endDate) {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery("getCustomerSalesPrice", startDate, endDate,
						productId, customerId);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Product Pricing Calc with customer without store
	public ProductPricingCalc getStoreSalesPrice(long productId, long storeId,
			Date startDate, Date endDate) {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery("getStoreSalesPrice", startDate, endDate,
						productId, storeId);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Basic Product Pricing Calc
	public ProductPricingCalc getProductPricingCalc(long productPricingCalcId)
			throws Exception {
		return productPricingCalcDAO.findById(productPricingCalcId);
	}

	// Get Basic Product Pricing Calc
	public ProductPricingCalc getProductPricingCalcComboType(
			long productPricingDetailId) throws Exception {
		List<ProductPricingCalc> productPricingCalc = productPricingCalcDAO
				.findByNamedQuery("getProductPricingCalcComboType",
						productPricingDetailId);
		return null != productPricingCalc && productPricingCalc.size() > 0 ? productPricingCalc
				.get(0) : null;
	}

	// Get Basic Product Pricing Calc
	public List<ProductPricingCalc> getProductPricingCalcByDetailId(
			long productPricingDetailId) throws Exception {
		return productPricingCalcDAO.findByNamedQuery(
				"getProductPricingCalcByDetailId", productPricingDetailId);
	}

	// Get Basic Product Pricing Calc
	public List<ProductPricingCalc> getActiveProductPricingCalcByDetailId(
			long productPricingDetailId) throws Exception {
		return productPricingCalcDAO
				.findByNamedQuery("getActiveProductPricingCalcByDetailId",
						productPricingDetailId);
	}

	// Get Basic Product Pricing Detail
	public ProductPricingDetail getProductPricingDetailByDetailId(
			long productPricingDetailId) throws Exception {
		return productPricingDetailDAO.findById(productPricingDetailId);
	}

	// Get Basic Product Pricing Detail
	public ProductPricingDetail getProductPricingDetailByPricingDetailId(
			long productPricingDetailId) throws Exception {
		return productPricingDetailDAO.findByNamedQuery(
				"getProductPricingDetailByPricingDetailId",
				productPricingDetailId).get(0);
	}

	// Get Basic Product Pricing Detail
	public List<ProductPricingDetail> getProductPricingDetailByPricingId(
			long productPricingId) throws Exception {
		return productPricingDetailDAO.findByNamedQuery(
				"getProductPricingDetailByPricingId", productPricingId);
	}

	// Get Base Product Pricing Detail
	public ProductPricingDetail getProductPricingDetail(long productId,
			long storeId, Date fromDate, Date toDate,
			Implementation implementation) throws Exception {
		List<ProductPricingDetail> productPricingDetails = productPricingDetailDAO
				.findByNamedQuery("getProductPricingDetail", implementation,
						fromDate, toDate, productId, storeId);
		return null != productPricingDetails
				&& productPricingDetails.size() > 0 ? productPricingDetails
				.get(0) : null;
	}

	// Get Base Product Pricing Detail
	@SuppressWarnings("unchecked")
	public ProductPricingDetail getProductPricingDetail(long productId,
			long storeId, Date fromDate, Date toDate,
			Implementation implementation, Session session) throws Exception {
		String getProductPricingDetail = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " JOIN FETCH p.productPricingCalcs pc"
				+ " WHERE p.productPricing.implementation = ?"
				+ " AND p.productPricing.startDate <= ?"
				+ " AND p.productPricing.endDate >= ?"
				+ " AND p.product.productId = ?" + " AND pc.store.storeId = ?";
		List<ProductPricingDetail> productPricingDetails = session
				.createQuery(getProductPricingDetail)
				.setEntity(0, implementation).setDate(1, fromDate)
				.setDate(2, toDate).setLong(3, productId)
				.setLong(4, storeId).list();
		return null != productPricingDetails
				&& productPricingDetails.size() > 0 ? productPricingDetails
				.get(0) : null;
	}

	// Get Base Product Pricing Detail
	@SuppressWarnings("unchecked")
	public ProductPricingDetail getProductPricingDetailWithOutStore(
			long productId, Date fromDate, Date toDate,
			Implementation implementation, Session session) throws Exception {
		String getProductPricingDetailWithOutStore = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " WHERE p.productPricing.implementation = ?"
				+ " AND p.productPricing.startDate <= ?"
				+ " AND p.productPricing.endDate >= ?"
				+ " AND p.product.productId = ?";
		List<ProductPricingDetail> productPricingDetails = session
				.createQuery(getProductPricingDetailWithOutStore)
				.setEntity(0, implementation).setDate(1, fromDate)
				.setDate(2, toDate).setLong(3, productId).list();
		return null != productPricingDetails
				&& productPricingDetails.size() > 0 ? productPricingDetails
				.get(0) : null;
	}

	// Get Base Product Pricing Detail
	public ProductPricingDetail getProductPricingDetailWithOutStore(
			long productId, Date fromDate, Date toDate,
			Implementation implementation) throws Exception {
		List<ProductPricingDetail> productPricingDetails = productPricingDetailDAO
				.findByNamedQuery("getProductPricingDetailWithOutStore",
						implementation, fromDate, toDate, productId);
		return null != productPricingDetails
				&& productPricingDetails.size() > 0 ? productPricingDetails
				.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<ProductPricing> getFilteredProductPricing(
			Implementation implementation, Long productPricingId,
			Integer storeId, Long productId, Long customerId, Date fromDate,
			Date toDate) throws Exception {
		boolean flag = true;
		Criteria criteria = productPricingDAO.createCriteria();

		criteria.createAlias("currency", "cry", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cry.currencyPool", "crp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("productPricingDetails", "detail",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.product", "prd",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("detail.productPricingCalcs", "prc_calc",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prc_calc.store", "str",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prc_calc.lookupDetail", "ld",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("prc_calc.customer", "cst",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.company", "cmp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("cst.personByPersonId", "prs",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.conjunction().add(
				Restrictions.ne("prc_calc.comboType", flag)));
		criteria.add(Restrictions.conjunction().add(
				Restrictions.eq("prc_calc.status", flag)));
		if (implementation != null) {
			criteria.add(Restrictions.eq("implementation", implementation));
		}

		if (productPricingId != null && productPricingId > 0) {
			criteria.add(Restrictions.eq("productPricingId", productPricingId));
		}

		if (storeId != null && storeId > 0) {
			criteria.add(Restrictions.eq("str.storeId", storeId));
		}

		if (productId != null && productId > 0) {
			criteria.add(Restrictions.eq("prd.productId", productId));
		}

		if (customerId != null && customerId > 0) {
			criteria.add(Restrictions.eq("cst.customerId", customerId));
		}

		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.between("startDate", fromDate, toDate));
		}

		Set<ProductPricing> uniqueProductPricing = new HashSet<ProductPricing>(
				criteria.list());
		List<ProductPricing> productPricings = new ArrayList<ProductPricing>(
				uniqueProductPricing);

		return productPricings;
	}

	public ProductPricingDetail getPricingDetailByProductId(Long productId)
			throws Exception {
		List<ProductPricingDetail> productPricingDetails = productPricingDetailDAO
				.findByNamedQuery("getPricingDetailByProductId", productId);
		return null != productPricingDetails
				&& productPricingDetails.size() > 0 ? productPricingDetails
				.get(0) : null;
	}
	
	public ProductPricingDetail getNonunfiedProductPricingDetail(Long productId, long storeId)
	throws Exception {
		List<ProductPricingDetail> productPricingDetails = productPricingDetailDAO
				.findByNamedQuery("getNonunfiedProductPricingDetail", productId, storeId);
		return null != productPricingDetails
				&& productPricingDetails.size() > 0 ? productPricingDetails
				.get(0) : null;
	}
	
	public List<ProductPricingDetail> getPricingDetailByImplementation(
			Implementation impl6, Implementation impl14) throws Exception{ 
		return productPricingDetailDAO
			.findByNamedQuery("getPricingDetailByImplementation", impl6, impl14);
	} 

	// Getters and Setters
	public AIOTechGenericDAO<ProductPricing> getProductPricingDAO() {
		return productPricingDAO;
	}

	public void setProductPricingDAO(
			AIOTechGenericDAO<ProductPricing> productPricingDAO) {
		this.productPricingDAO = productPricingDAO;
	}

	public AIOTechGenericDAO<ProductPricingDetail> getProductPricingDetailDAO() {
		return productPricingDetailDAO;
	}

	public void setProductPricingDetailDAO(
			AIOTechGenericDAO<ProductPricingDetail> productPricingDetailDAO) {
		this.productPricingDetailDAO = productPricingDetailDAO;
	}

	public AIOTechGenericDAO<ProductPricingCalc> getProductPricingCalcDAO() {
		return productPricingCalcDAO;
	}

	public void setProductPricingCalcDAO(
			AIOTechGenericDAO<ProductPricingCalc> productPricingCalcDAO) {
		this.productPricingCalcDAO = productPricingCalcDAO;
	} 
}
