package com.aiotech.aios.accounts.service.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.ProductDefinition;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.service.PointOfSaleService;
import com.aiotech.aios.accounts.service.ProductDefinitionService;
import com.aiotech.aios.accounts.service.ProductPricingService;
import com.aiotech.aios.accounts.service.ProductService;
import com.aiotech.aios.accounts.service.PurchaseService;
import com.aiotech.aios.accounts.service.SalesDeliveryNoteService;
import com.aiotech.aios.accounts.service.StockService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.CostingType;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.CompressionUtil;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ProductBL {

	private ProductService productService;
	private CombinationBL combinationBL;
	private LookupMasterBL lookupMasterBL;
	private ProductCategoryBL productCategoryBL;
	private ProductDefinitionService productDefinitionService;
	private StoreBL storeBL;
	private ProductPricingService productPricingService;
	private CurrencyService currencyService;
	private StockService stockService;
	private SalesDeliveryNoteService salesDeliveryNoteService;
	private PurchaseService purchaseService;
	private PointOfSaleService pointOfSaleService;
	private ImageBL imageBL;
	private SystemBL systemBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public List<Object> getProductSales(Long productId) {

		List<Object> sales = new ArrayList<Object>();
		SalesDeliveryDetailVO deliveryDetailVO = null;

		List<SalesDeliveryDetail> salesDeliveriesForProduct = salesDeliveryNoteService
				.getSalesDeliveryByProductId(productId);

		for (SalesDeliveryDetail salesDeliveryDetail : salesDeliveriesForProduct) {

			deliveryDetailVO = new SalesDeliveryDetailVO();

			deliveryDetailVO
					.setCustomerNameAndNumber(resolveCustomerName(salesDeliveryDetail
							.getSalesDeliveryNote().getCustomer()));

			deliveryDetailVO.setDescription(salesDeliveryDetail
					.getSalesDeliveryNote().getDescription());

			deliveryDetailVO.setDeliveredQuantity(salesDeliveryDetail
					.getDeliveryQuantity());

			deliveryDetailVO.setSalesPersonName(salesDeliveryDetail
					.getSalesOrderDetail().getSalesOrder().getPerson()
					.getFirstName());

			deliveryDetailVO.setUnitRate(salesDeliveryDetail.getUnitRate());
			deliveryDetailVO
					.setTotal((salesDeliveryDetail.getDeliveryQuantity() * salesDeliveryDetail
							.getUnitRate())
							- (salesDeliveryDetail.getDiscount() != null ? salesDeliveryDetail
									.getDiscount() : 0.0));

			try {
				deliveryDetailVO.setStatus(Constants.Accounts.O2CProcessStatus
						.get(salesDeliveryDetail.getSalesDeliveryNote()
								.getStatus()).name());
			} catch (Exception e) {
				e.printStackTrace();
			}

			deliveryDetailVO.setIsInvoiced(salesDeliveryDetail
					.getSalesInvoiceDetails() == null ? "No" : "Yes");

			if (salesDeliveryDetail.getSalesDeliveryNote().getProject() != null
					|| salesDeliveryDetail.getSalesDeliveryNote()
							.getProjectTask() != null) {

				if (salesDeliveryDetail.getSalesDeliveryNote().getProject() != null)
					deliveryDetailVO
							.setSalesReferenceNumber(salesDeliveryDetail
									.getSalesDeliveryNote().getProject()
									.getReferenceNumber());
				else if (salesDeliveryDetail.getSalesDeliveryNote()
						.getProjectTask() != null)
					deliveryDetailVO
							.setSalesReferenceNumber(salesDeliveryDetail
									.getSalesDeliveryNote().getProjectTask()
									.getReferenceNumber());

			} else {

				deliveryDetailVO.setSalesReferenceNumber(salesDeliveryDetail
						.getSalesOrderDetail().getSalesOrder()
						.getReferenceNumber());
			}

			sales.add(deliveryDetailVO);
		}

		// get POS sales and add to same object list as well
		List<PointOfSaleDetail> pointOfSaleDetails = pointOfSaleService
				.getAllPointOfSaleDetailsByProductId(productId);

		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails) {

			deliveryDetailVO = new SalesDeliveryDetailVO();

			deliveryDetailVO
					.setCustomerNameAndNumber(resolveCustomerName(pointOfSaleDetail
							.getPointOfSale().getCustomer()));

			deliveryDetailVO.setDescription("POS Sale");

			deliveryDetailVO.setDeliveredQuantity(pointOfSaleDetail
					.getQuantity());

			deliveryDetailVO
					.setSalesPersonName(resolvePersonName(pointOfSaleDetail
							.getPointOfSale().getPOSUserTill()
							.getPersonByPersonId()));

			deliveryDetailVO.setUnitRate(pointOfSaleDetail.getUnitRate());
			deliveryDetailVO
					.setTotal((pointOfSaleDetail.getQuantity() * pointOfSaleDetail
							.getUnitRate())
							- (pointOfSaleDetail.getDiscountValue() != null ? pointOfSaleDetail
									.getDiscountValue() : 0.0));

			try {
				Boolean printed = pointOfSaleDetail.getPointOfSale()
						.getOrderPrinted();

				deliveryDetailVO
						.setStatus((printed != null && printed != false) ? ""
								: "");

				deliveryDetailVO
						.setIsInvoiced((printed != null && printed != false) ? "Yes"
								: "No");
			} catch (Exception e) {
				e.printStackTrace();
			}

			deliveryDetailVO.setSalesReferenceNumber(pointOfSaleDetail
					.getPointOfSale().getReferenceNumber());

			sales.add(deliveryDetailVO);

		}

		return sales;

	}

	public void updateProductUnit() throws Exception {
		List<Product> products = productService.getAllProduct();
		List<LookupDetail> productUnits = lookupMasterBL
				.getActiveLookupDetails("PRODUCT_UNITNAME", false);

		Map<Long, List<Product>> implProducts = new HashMap<Long, List<Product>>();
		List<Product> tempProductList = null;
		for (Product product : products) {
			tempProductList = new ArrayList<Product>();
			if (null != implProducts
					&& implProducts.size() > 0
					&& implProducts.containsKey(product.getImplementation()
							.getImplementationId())) {
				tempProductList = new ArrayList<Product>(
						implProducts.get(product.getImplementation()
								.getImplementationId()));
			}
			tempProductList.add(product);
			implProducts.put(product.getImplementation().getImplementationId(),
					tempProductList);
		}

		LookupMaster lookupMaster = null;
		Implementation implementation = null;
		List<LookupDetail> lookupDetails = null;
		LookupDetail tempLookupDetail = null;
		for (Entry<Long, List<Product>> impl : implProducts.entrySet()) {
			lookupMaster = new LookupMaster();
			lookupMaster.setAccessCode("PRODUCT_UNITNAME");
			lookupMaster.setIsActive((byte) 1);
			lookupMaster.setIsSystem((byte) 0);
			lookupMaster.setDisplayName("PRODUCT_UNITNAME");
			implementation = new Implementation();
			implementation.setImplementationId(impl.getKey());
			lookupMaster.setImplementation(implementation);
			lookupDetails = new ArrayList<LookupDetail>();
			for (LookupDetail lookupDetail : productUnits) {
				tempLookupDetail = new LookupDetail();
				tempLookupDetail.setDataId(lookupDetail.getDataId());
				tempLookupDetail.setDisplayName(lookupDetail.getDisplayName());
				tempLookupDetail.setIsActive(true);
				tempLookupDetail.setIsSystem(false);
				tempLookupDetail.setLookupMaster(lookupMaster);
				lookupDetails.add(tempLookupDetail);
				lookupMaster.setLookupDetails(new HashSet<LookupDetail>(
						lookupDetails));
			}
			lookupMasterBL.getLookupMasterService().saveLookupInformation(
					lookupMaster, lookupDetails);
		}

		for (Entry<Long, List<Product>> prd : implProducts.entrySet()) {
			implementation = new Implementation();
			implementation.setImplementationId(prd.getKey());
			productUnits = lookupMasterBL.getActiveLookupDetails(
					"PRODUCT_UNITNAME", implementation);

			Map<Integer, LookupDetail> lookupMap = new HashMap<Integer, LookupDetail>();
			for (LookupDetail lookupDetail : productUnits) {
				lookupMap.put(lookupDetail.getDataId(), lookupDetail);
			}

			List<Product> listProduct = new ArrayList<Product>(prd.getValue());

			for (Product product2 : listProduct) {
				if (lookupMap
						.containsKey(product2.getProductUnit().getUnitId())) {
					product2.setLookupDetailByProductUnit(lookupMap
							.get(product2.getProductUnit().getUnitId()));
					productService.saveProductDetails(product2);
				}
			}
		}

	}

	private String resolvePersonName(Person person) {

		String name = "n/a";

		if (person != null) {

			name = "";

			if (person != null) {

				name += AIOSCommons.returnCompleteName(person.getFirstName(),
						person.getMiddleName(), person.getLastName());

				name += " - " + person.getPersonNumber();

			}
		}

		return name;
	}

	private String resolveCustomerName(Customer customer) {

		String cutomerName = "n/a";

		if (customer != null) {

			cutomerName = "";

			if (customer.getPersonByPersonId() != null) {

				cutomerName += AIOSCommons.returnCompleteName(customer
						.getPersonByPersonId().getFirstName(), customer
						.getPersonByPersonId().getMiddleName(), customer
						.getPersonByPersonId().getLastName());

				cutomerName += " - " + customer.getCustomerNumber();

			} else if (customer.getCompany() != null) {

				cutomerName += customer.getCompany().getCompanyName();
				cutomerName += " - " + customer.getCustomerNumber();
			}
		}

		return cutomerName;
	}

	private String resolveSupplierName(Supplier supplier) {

		String supplierName = "n/a";

		if (supplier != null) {

			supplierName = "";

			if (supplier.getPerson() != null) {

				supplierName += AIOSCommons.returnCompleteName(supplier
						.getPerson().getFirstName(), supplier.getPerson()
						.getMiddleName(), supplier.getPerson().getLastName());

				supplierName += " - " + supplier.getSupplierNumber();

			} else if (supplier.getCompany() != null) {

				supplierName += supplier.getCompany().getCompanyName();
				supplierName += " - " + supplier.getSupplierNumber();
			}
		}

		return supplierName;
	}

	public List<Object> getProductPurchases(Long productId) {

		List<Object> purchases = new ArrayList<Object>();
		PurchaseDetailVO purchaseDetailVO = null;

		List<PurchaseDetail> purchaseDetails = purchaseService
				.getPurchaseDetailByProductId(productId);
		Set<ReceiveDetail> receiveDetails = null;

		for (PurchaseDetail purchaseDetail : purchaseDetails) {

			if (purchaseDetail.getPurchase().getReceiveDetails() != null
					&& purchaseDetail.getPurchase().getReceiveDetails().size() != 0) {

				receiveDetails = purchaseDetail.getPurchase()
						.getReceiveDetails();

				for (ReceiveDetail receiveDetail : receiveDetails) {

					if (receiveDetail.getProduct().getProductId() != purchaseDetail
							.getProduct().getProductId())
						continue;

					purchaseDetailVO = new PurchaseDetailVO();

					purchaseDetailVO
							.setPurchaseOrderReferenceNumber(purchaseDetail
									.getPurchase().getPurchaseNumber());
					purchaseDetailVO
							.setSupplierNameAndNumber(resolveSupplierName(purchaseDetail
									.getPurchase().getSupplier()));
					purchaseDetailVO.setDescription(receiveDetail
							.getDescription());

					purchaseDetailVO.setReceivedQty(receiveDetail
							.getReceiveQty());
					purchaseDetailVO.setUnitRate(receiveDetail.getUnitRate());
					purchaseDetailVO.setTotalRate(receiveDetail.getUnitRate()
							* receiveDetail.getReceiveQty());

					try {
						if (purchaseDetail.getPurchase().getDeliveryDate() != null)
							purchaseDetailVO.setDeliveryDate(DateFormat
									.convertDateToString(purchaseDetail
											.getPurchase().getDeliveryDate()
											.toString()));
						else
							purchaseDetailVO.setDeliveryDate("");
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						purchaseDetailVO.setReceiveDate(DateFormat
								.convertDateToString(receiveDetail.getReceive()
										.getReceiveDate().toString()));
					} catch (Exception e) {
						e.printStackTrace();
					}

					purchases.add(purchaseDetailVO);
				}
			} else {

				purchaseDetailVO = new PurchaseDetailVO();

				purchaseDetailVO.setPurchaseOrderReferenceNumber(purchaseDetail
						.getPurchase().getPurchaseNumber());
				purchaseDetailVO
						.setSupplierNameAndNumber(resolveSupplierName(purchaseDetail
								.getPurchase().getSupplier()));
				purchaseDetailVO
						.setDescription(purchaseDetail.getDescription());

				purchaseDetailVO.setReceivedQty(purchaseDetail.getQuantity());
				purchaseDetailVO.setUnitRate(purchaseDetail.getUnitRate());
				purchaseDetailVO.setTotalRate(purchaseDetail.getUnitRate()
						* purchaseDetail.getQuantity());

				try {
					purchaseDetailVO.setDeliveryDate(DateFormat
							.convertDateToString(purchaseDetail.getPurchase()
									.getDeliveryDate().toString()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					purchaseDetailVO.setReceiveDate("");
				} catch (Exception e) {
					e.printStackTrace();
				}

				purchases.add(purchaseDetailVO);
			}
		}

		return purchases;
	}

	// Show all Product Details
	public List<Object> showProductDetails(String itemType) throws Exception {
		List<Product> products = null;
		if (null != itemType && !("").equals(itemType))
			products = productService.getProductCodeByType(getImplementation(),
					itemType.charAt(0));
		else
			products = productService.getActiveProduct(getImplementation());
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Show all Product Details
	public List<Object> showRequisitionProductDetails() throws Exception {
		List<Product> products = productService.showRequisitionProductDetails(
				getImplementation(),
				InventorySubCategory.Craft0Manufacturer.getCode(),
				InventorySubCategory.Services.getCode());
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Show all Product Details
	public List<Object> showSalesRequisitionProductDetails() throws Exception {
		Set<Byte> itemSubTypes = new HashSet<Byte>();
		itemSubTypes.add(InventorySubCategory.Finished_Goods.getCode());
		itemSubTypes.add(InventorySubCategory.Craft0Manufacturer.getCode());
		itemSubTypes.add(InventorySubCategory.Services.getCode());
		List<Product> products = productService
				.getProductFinishedInventory(getImplementation(),
						ItemType.Inventory.getCode(), itemSubTypes);
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Show all Product Details
	public List<ProductCategoryVO> getProductFinishedInventory()
			throws Exception {
		Set<Byte> itemSubTypes = new HashSet<Byte>();
		itemSubTypes.add(InventorySubCategory.Finished_Goods.getCode());
		itemSubTypes.add(InventorySubCategory.Craft0Manufacturer.getCode());
		List<Product> products = productService
				.getProductFinishedInventory(getImplementation(),
						ItemType.Inventory.getCode(), itemSubTypes);
		String key = "";
		List<ProductVO> productVOs = null;
		List<ProductCategoryVO> productCategoryVOs = null;
		ProductCategoryVO productCategoryVO = null;
		if (null != products && products.size() > 0) {
			Map<String, List<ProductVO>> productMap = new HashMap<String, List<ProductVO>>();
			for (Product product : products) {
				key = null != product.getProductCategory() ? product
						.getProductCategory().getCategoryName().trim()
						: "NO CATEGORY";
				productVOs = new ArrayList<ProductVO>();
				if (productMap.containsKey(key))
					productVOs.addAll(productMap.get(key));
				productVOs.add(addProductObject(product));
				productMap.put(key, productVOs);
			}

			productCategoryVOs = new ArrayList<ProductCategoryVO>();
			for (Entry<String, List<ProductVO>> entry : productMap.entrySet()) {
				productVOs = new ArrayList<ProductVO>(entry.getValue());
				Collections.sort(productVOs, new Comparator<ProductVO>() {
					public int compare(ProductVO o1, ProductVO o2) {
						return o1.getProductName().compareTo(
								o2.getProductName());
					}
				});
				productCategoryVO = new ProductCategoryVO();
				productCategoryVO.setCategoryName(entry.getKey());
				productCategoryVO.setProductVOs(productVOs);
				productCategoryVOs.add(productCategoryVO);
			}
			Collections.sort(productCategoryVOs,
					new Comparator<ProductCategoryVO>() {
						public int compare(ProductCategoryVO o1,
								ProductCategoryVO o2) {
							return o1.getCategoryName().compareTo(
									o2.getCategoryName());
						}
					});
		}
		return productCategoryVOs;
	}

	// Show all Product Details
	public List<ProductCategoryVO> getInventoryProduct(String itemType)
			throws Exception {
		List<Product> products = productService.getProductCodeByType(
				getImplementation(), itemType.charAt(0));
		String key = "";
		List<ProductVO> productVOs = null;
		List<ProductCategoryVO> productCategoryVOs = null;
		ProductCategoryVO productCategoryVO = null;
		if (null != products && products.size() > 0) {
			Map<String, List<ProductVO>> productMap = new HashMap<String, List<ProductVO>>();
			for (Product product : products) {
				key = null != product.getProductCategory() ? product
						.getProductCategory().getCategoryName().trim()
						: "NO CATEGORY";
				productVOs = new ArrayList<ProductVO>();
				if (productMap.containsKey(key))
					productVOs.addAll(productMap.get(key));
				productVOs.add(addProductObject(product));
				productMap.put(key, productVOs);
			}

			productCategoryVOs = new ArrayList<ProductCategoryVO>();
			for (Entry<String, List<ProductVO>> entry : productMap.entrySet()) {
				productVOs = new ArrayList<ProductVO>(entry.getValue());
				Collections.sort(productVOs, new Comparator<ProductVO>() {
					public int compare(ProductVO o1, ProductVO o2) {
						return o1.getProductName().compareTo(
								o2.getProductName());
					}
				});
				productCategoryVO = new ProductCategoryVO();
				productCategoryVO.setCategoryName(entry.getKey());
				productCategoryVO.setProductVOs(productVOs);
				productCategoryVOs.add(productCategoryVO);
			}
			Collections.sort(productCategoryVOs,
					new Comparator<ProductCategoryVO>() {
						public int compare(ProductCategoryVO o1,
								ProductCategoryVO o2) {
							return o1.getCategoryName().compareTo(
									o2.getCategoryName());
						}
					});
		}
		return productCategoryVOs;
	}

	// Show all Product Details
	public List<Object> showCraftProductDetails() throws Exception {
		List<Product> products = productService.getCraftProducts(
				getImplementation(),
				InventorySubCategory.Craft0Manufacturer.getCode(),
				InventorySubCategory.Finished_Goods.getCode());
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Show all Product Details
	public List<Object> showFinishedProductDetails() throws Exception {
		List<Product> products = productService.getFinsishedGoodsProducts(
				getImplementation(),
				InventorySubCategory.Finished_Goods.getCode());
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Show all Product Details
	public List<Object> showNonCraftServiceProductDetails() throws Exception {
		List<Product> products = productService
				.getNonCraftServiceProductDetails(getImplementation(),
						ItemType.Inventory.getCode(),
						InventorySubCategory.Craft0Manufacturer.getCode(),
						InventorySubCategory.Services.getCode());
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Show all Product Details
	public List<Object> showPurchaseProductDetails(String itemType)
			throws Exception {
		List<Product> products = productService.getShortActiveProduct(
				getImplementation(),
				InventorySubCategory.Craft0Manufacturer.getCode(),
				InventorySubCategory.Services.getCode());

		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			if (null != itemType && !("").equals(itemType)) {
				for (Product product : products) {
					if (product.getItemType().toString().equals(itemType))
						productVOs.add(addProductVO(product));
				}
			} else {
				for (Product product : products)
					productVOs.add(addProductVO(product));
			}
		}
		return productVOs;
	}

	// Show all Product Details
	public List<Object> showSalesProductDetails(String itemType)
			throws Exception {
		List<Product> products = productService
				.getActiveProduct(getImplementation());

		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			if (null != itemType && !("").equals(itemType)) {
				for (Product product : products) {
					if (product.getItemType().toString().equals(itemType))
						productVOs.add(addProductVO(product));
				}
			} else {
				for (Product product : products)
					productVOs.add(addProductVO(product));
			}
		}
		return productVOs;
	}

	// Get all Product Info
	public List<Object> getInventorySpecialProducts() throws Exception {
		List<Product> products = productService
				.getAllPOSProducts(getImplementation());
		List<Object> productVOs = new ArrayList<Object>();
		if (null != products && products.size() > 0) {
			for (Product product : products)
				productVOs.add(addProductVO(product));
		}
		return productVOs;
	}

	// Add ProductVO
	private ProductVO addProductVO(Product product) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		String unifiedPrice = "";
		if (session.getAttribute("unified_price") != null)
			unifiedPrice = session.getAttribute("unified_price").toString();
		ProductVO productVO = new ProductVO();
		productVO.setProductId(product.getProductId());
		productVO.setCode(product.getCode());
		productVO.setProductName(product.getProductName());
		if (null != product.getLookupDetailByProductUnit()) {
			productVO.setUnitName(product.getLookupDetailByProductUnit()
					.getDisplayName());
			productVO.setUnitCode(product.getLookupDetailByProductUnit()
					.getAccessCode());
		}
		productVO.setCostingMethod(CostingType.get(product.getCostingType())
				.name());
		productVO.setItemSubType(product.getItemSubType());
		double unitRate = 0;
		double standardPrice = 0;
		double basePrice = 0;
		double sellingPrice = 0;
		if (null != product.getProductPricingDetails()
				&& product.getProductPricingDetails().size() > 0) {
			boolean storePrice = false;
			if (unifiedPrice.equals("false") && null != product.getShelf()) {
				for (ProductPricingDetail productPricingDetail : product
						.getProductPricingDetails()) {
					if (productPricingDetail.getStatus()) {
						if (null != productPricingDetail.getStore()
								&& (long) productPricingDetail.getStore()
										.getStoreId() == (long) product
										.getShelf().getShelf().getAisle()
										.getStore().getStoreId()) {
							storePrice = true;
							basePrice = productPricingDetail
									.getBasicCostPrice();
							standardPrice = productPricingDetail
									.getStandardPrice();
							sellingPrice = null != productPricingDetail
									.getSellingPrice() ? productPricingDetail
									.getSellingPrice() : 0;
							unitRate = productPricingDetail.getBasicCostPrice();
							break;
						}
					}
				}
			}
			if (!storePrice) {
				for (ProductPricingDetail productPricingDetail : product
						.getProductPricingDetails()) {
					if (productPricingDetail.getStatus()
							&& null == productPricingDetail.getStore()) {
						basePrice = productPricingDetail.getBasicCostPrice();
						standardPrice = productPricingDetail.getStandardPrice();
						sellingPrice = null != productPricingDetail
								.getSellingPrice() ? productPricingDetail
								.getSellingPrice() : 0;
						unitRate = productPricingDetail.getBasicCostPrice();
						break;
					}
				}
			}
		}
		productVO.setFinalUnitPrice(unitRate);
		productVO.setSuggestedPrice(standardPrice);
		productVO.setStandardPrice(standardPrice);
		productVO.setBasePrice(basePrice);
		productVO.setSellingPrice(sellingPrice);
		productVO.setStoreName(null != product.getShelf() ? product.getShelf()
				.getShelf().getAisle().getStore().getStoreName()
				+ ">>"
				+ product.getShelf().getShelf().getAisle().getSectionName()
				+ ">>"
				+ product.getShelf().getShelf().getName()
				+ ">>"
				+ product.getShelf().getName() : "");
		productVO.setStoreId(null != product.getShelf() ? product.getShelf()
				.getShelf().getAisle().getStore().getStoreId() : 0);
		productVO.setShelfId(null != product.getShelf() ? product.getShelf()
				.getShelfId() : 0);
		return productVO;
	}

	// Add ProductVO
	public ProductVO addProductObject(Product product) {
		ProductVO productVO = new ProductVO();
		productVO.setProductId(product.getProductId());
		productVO.setCode(product.getCode());
		productVO.setProductName(product.getProductName());
		return productVO;
	}

	// Get Inventory Product combination by Product Id & Store Id
	public Combination getProductInventory(long productId) throws Exception {
		Product product = productService.getProductById(productId);
		if (!product.getItemType().equals(ItemType.Expense.getCode()))
			return product.getCombinationByInventoryAccount();
		else
			return product.getCombinationByExpenseAccount();
	}

	// Save Product Details
	public void saveProductDetails(Product product) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (product != null && product.getProductId() != null) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}

		product.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		boolean editFlag = false;
		if (product.getProductId() != null && product.getProductId() > 0)
			editFlag = true;

		productService.saveProductDetails(product);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Product.class.getSimpleName(), product.getProductId(), user,
				workflowDetailVo);
		saveProductImages(product, editFlag);

		try {
			this.extractImagesForOrderingMenu((Properties) ServletActionContext
					.getServletContext().getAttribute("confProps"),
					ServletActionContext.getServletContext(), true, product);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void doProductBusinessUpdate(Long productId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			Product product = productService.getProductById(productId);
			if (!workflowDetailVO.isDeleteFlag()) {

			} else if (workflowDetailVO.isDeleteFlag()) {

				productService.deleteProduct(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private Stock addStock(Product product) throws Exception {
		Stock stock = new Stock();
		stock.setProduct(product);
		stock.setImplementation(getImplementation());
		stock.setQuantity(1d);
		stock.setUnitRate(0d);
		return stock;
	}

	public void saveProductInventory(List<ProductCategory> productCategories,
			List<Product> products, List<Stock> stocks,
			ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails) throws Exception {
		productCategoryBL.saveProductCategory(productCategories);
		Combination combination = null;
		for (Product product : products) {
			if (product.getItemType().equals(ItemType.Expense.getCode())) {
				combination = combinationBL.createAnalysisCombination(
						product.getCombinationByExpenseAccount(),
						product.getCode(), true);
				product.setCombinationByExpenseAccount(combination);
			} else {
				combination = combinationBL.createAnalysisCombination(
						product.getCombinationByInventoryAccount(),
						product.getCode(), true);
				product.setCombinationByInventoryAccount(combination);
			}
		}
		productService.saveProducts(products);
		stockService.updateStockDetails(stocks);
		productPricingService.saveProductPricing(productPricing,
				productPricingDetails);
	}

	// Save Products + Categories Details
	public void saveProducts(List<ProductCategory> productCategories,
			List<Product> products, ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails,
			List<ProductPricingCalc> productPricingCalcs, List<Stock> stocks)
			throws Exception {
		productCategoryBL.saveProductCategory(productCategories);
		Combination combination = null;
		for (Product product : products) {
			if (product.getItemType().equals(ItemType.Expense.getCode())) {
				combination = combinationBL.createAnalysisCombination(
						product.getCombinationByExpenseAccount(),
						product.getCode(), true);
				product.setCombinationByExpenseAccount(combination);
			} else {
				combination = combinationBL.createAnalysisCombination(
						product.getCombinationByInventoryAccount(),
						product.getCode(), true);
				product.setCombinationByInventoryAccount(combination);
			}
		}
		productService.saveProducts(products);
		stockService.updateStockDetails(stocks);
		productPricingService.saveProductPricing(productPricing,
				productPricingDetails, productPricingCalcs);
	}

	public void saveProductImages(Product product, boolean editFlag)
			throws Exception {
		DocumentVO docVO2 = new DocumentVO();
		docVO2.setDisplayPane("productImages");
		docVO2.setEdit(editFlag);
		docVO2.setObject(product);
		imageBL.saveUploadedImages(product, docVO2);
	}

	// Get all Product Info
	public List<Product> getAllProducts() throws Exception {
		return productService.getActiveProduct(getImplementation(),
				ItemType.Inventory.getCode(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()));
	}

	// for product image extraction
	public List<Product> getActivePOSProduct(Implementation implementation)
			throws Exception {
		return productService.getActivePOSProduct(implementation,
				ItemType.Inventory.getCode(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()));
	}

	public List<Product> getActivePOSProduct() throws Exception {
		return productService.getActivePOSProduct(getImplementation(),
				ItemType.Inventory.getCode(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()));
	}

	// Get all Product Info
	public List<Product> getProductByCategory(long categoryId) throws Exception {
		return productService.getProductByCategory(getImplementation(),
				categoryId, ItemType.Inventory.getCode(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()));
	}

	// Get all Product Info
	public List<Product> getPOSProductByCategory(long categoryId, long storeId)
			throws Exception {
		return productService.getPOSProductByCategory(getImplementation(),
				categoryId, ItemType.Inventory.getCode(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()),
				storeId);
	}

	// Get all Product Info
	public List<Product> getProductByCategories(Long[] categoryIds)
			throws Exception {
		return productService.getProductByCategories(getImplementation(),
				categoryIds, ItemType.Inventory.getCode(),
				DateFormat.getFromDate(Calendar.getInstance().getTime()));
	}

	// Get all Product Info
	public List<Product> getAllActiveInventoryProducts() throws Exception {
		List<Product> products = productService
				.getActiveProduct(getImplementation());
		return products;
	}

	// Get all Product Info
	public List<Product> getAllActiveInventorySpecialProducts()
			throws Exception {
		List<ProductDefinition> productDefinitions = productDefinitionService
				.getAllProductDefinitions(getImplementation());
		Set<Long> productIds = null;
		Set<Long> allProductsIds = null;
		if (null != productDefinitions && productDefinitions.size() > 0) {
			productIds = new HashSet<Long>();
			allProductsIds = new HashSet<Long>();
			for (ProductDefinition productDefinition : productDefinitions) {
				productIds.add(productDefinition.getProductByProductId()
						.getProductId());
				allProductsIds.add(productDefinition.getProductByProductId()
						.getProductId());
				if (null != productDefinition.getProductBySpecialProductId())
					allProductsIds.add(productDefinition
							.getProductBySpecialProductId().getProductId());
			}
		}
		List<Product> finalProductList = null;
		if (null != productIds && productIds.size() > 0) {
			List<Product> products = productService
					.getActiveSpecialProduct(productIds);
			finalProductList = new ArrayList<Product>();
			finalProductList.addAll(products);
			List<Product> productSpecial = productService
					.getActivePosSpecialProduct(getImplementation(), Calendar
							.getInstance().getTime(), Calendar.getInstance()
							.getTime());

			for (Product product : productSpecial) {
				if (!allProductsIds.contains(product.getProductId()))
					finalProductList.add(product);
			}
		}

		return finalProductList;
	}

	// Get all Product Info
	public List<Product> getActiveInventorySpecialProducts() throws Exception {
		List<Product> products = productService.getActivePOSProduct(
				getImplementation(), ItemType.Inventory.getCode(), Calendar
						.getInstance().getTime());
		return products;
	}

	// Get all Product Info
	public List<Product> getAllInventorySpecialProducts() throws Exception {
		List<Product> products = productService
				.getAllPOSProducts(getImplementation());
		return products;
	}

	// Get Over Head expense for a store location
	public Combination getOverHeadExpenseAccount(int storeId) throws Exception {
		Combination combination = getOrCreateOverHeadExpenseCombination(storeId);
		return combination;
	}

	private Combination getOrCreateOverHeadExpenseCombination(int storeId)
			throws Exception {
		Store store = storeBL.getStoreService().getStoreLocationCostCentre(
				storeId);
		Combination combination = null;
		Account account = combinationBL
				.getAccountBL()
				.getAccountService()
				.getAccountDetails(
						"Factory Overhead ".concat(store.getStoreName()),
						getImplementation());
		if (null != account) {
			combination = combinationBL.getCombinationService()
					.getCombinationForProduct(
							store.getCmpDeptLocation().getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							getImplementation().getExpenseAccount(),
							account.getAccountId());
			if (null == combination) {
				Combination naturalCombination = combinationBL
						.getCombinationService().getNaturalAccountCombination(
								store.getCmpDeptLocation().getCombination()
										.getAccountByCostcenterAccountId()
										.getAccountId(),
								getImplementation().getExpenseAccount());
				if (null == naturalCombination) {
					Account tempAccount = combinationBL
							.getAccountBL()
							.getAccountService()
							.getAccountDetails(
									getImplementation().getExpenseAccount());
					Combination costCombination = combinationBL
							.createNaturalCombiantionAccount(store
									.getCmpDeptLocation().getCombination(),
									tempAccount);
					combination = combinationBL
							.createAnalysisCombinationAccount(costCombination,
									account);
					combinationBL.getAccountBL().getAccountService()
							.getAccountDAO().evict(tempAccount);
				} else
					combination = combinationBL
							.createAnalysisCombinationAccount(
									naturalCombination, account);
			}
		} else {
			Combination naturalCombination = combinationBL
					.getCombinationService().getNaturalAccountCombination(
							store.getCmpDeptLocation().getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							getImplementation().getExpenseAccount());
			if (null == naturalCombination) {
				Account tempAccount = combinationBL
						.getAccountBL()
						.getAccountService()
						.getAccountDetails(
								getImplementation().getExpenseAccount());
				Combination costCombination = combinationBL
						.createNaturalCombiantionAccount(store
								.getCmpDeptLocation().getCombination(),
								tempAccount);
				combination = combinationBL.createAnalysisCombination(
						costCombination,
						"Factory Overhead ".concat(store.getStoreName()), true);
				combinationBL.getAccountBL().getAccountService()
						.getAccountDAO().evict(tempAccount);
			} else
				combination = combinationBL.createAnalysisCombination(
						naturalCombination,
						"Factory Overhead ".concat(store.getStoreName()), true);
		}
		storeBL.getStoreService().getStoreDAO().evict(store);
		return combination;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public String getProductCodeByItemType(byte code) {
		String itemType = "";
		switch (code) {
		case 1:
			itemType = "RM";// Raw Material
			break;
		case 2:
			itemType = "UP";// Under Production
			break;
		case 3:
			itemType = "FG";// Finished Goods
			break;
		case 4:
			itemType = "UG";// UnFinished Goods
			break;
		case 5:
			itemType = "PM";// Packaging Material
			break;
		case 6:
			itemType = "SP";// Supplies
			break;
		case 7:
			itemType = "OP";// Office Product
			break;
		case 8:
			itemType = "OE";// Office Expense
			break;
		case 9:
			itemType = "TG";// InTangible
			break;
		case 10:
			itemType = "IT";// InTangible
			break;
		case 11:
			itemType = "SR";// Service
			break;
		case 12:
			itemType = "CM";// Craft/Manufacturer
			break;
		}
		return itemType;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public ProductCategoryBL getProductCategoryBL() {
		return productCategoryBL;
	}

	public void setProductCategoryBL(ProductCategoryBL productCategoryBL) {
		this.productCategoryBL = productCategoryBL;
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public ProductDefinitionService getProductDefinitionService() {
		return productDefinitionService;
	}

	public void setProductDefinitionService(
			ProductDefinitionService productDefinitionService) {
		this.productDefinitionService = productDefinitionService;
	}

	public void updateExpenseProductCombination() throws Exception {
		Combination combination = null;
		Implementation impl = new Implementation();
		impl.setImplementationId(14l);
		List<Product> products = productService
				.getProductsByItemType(impl, 'E');
		for (Product product : products) {
			if (null == product.getCombinationByExpenseAccount()) {
				combination = new Combination();
				combination.setCombinationId(14838L);
				combination = combinationBL.createAnalysisCombination(
						combination, product.getCode(), true, impl);
				product.setCombinationByExpenseAccount(combination);
			}
		}
		productService.saveProducts(products);
	}

	public void updateProductCombination() throws Exception {
		Combination combination = null;
		Implementation impl = new Implementation();
		impl.setImplementationId(14l);
		List<Product> products = productService
				.getProductsByItemType(impl, 'I');
		for (Product product : products) {
			if (null == product.getCombinationByInventoryAccount()) {
				if ((byte) product.getItemSubType() == (byte) InventorySubCategory.Raw_Material
						.getCode()) {
					combination = new Combination();
					combination.setCombinationId(14833L);
					combination = combinationBL.createAnalysisCombination(
							combination, product.getCode(), true, impl);
				} else if ((byte) product.getItemSubType() == (byte) InventorySubCategory.Finished_Goods
						.getCode()) {
					combination = new Combination();
					combination.setCombinationId(14836L);
					combination = combinationBL.createAnalysisCombination(
							combination, product.getCode(), true, impl);
				} else if ((byte) product.getItemSubType() == (byte) InventorySubCategory.Craft0Manufacturer
						.getCode()) {
					combination = new Combination();
					combination.setCombinationId(14836L);
					combination = combinationBL.createAnalysisCombination(
							combination, product.getCode(), true, impl);
				} else if ((byte) product.getItemSubType() == (byte) InventorySubCategory.Packaging_Materials
						.getCode()) {
					combination = new Combination();
					combination.setCombinationId(14834L);
					combination = combinationBL.createAnalysisCombination(
							combination, product.getCode(), true, impl);
				} else if ((byte) product.getItemSubType() == (byte) InventorySubCategory.Supplies
						.getCode()) {
					combination = new Combination();
					combination.setCombinationId(14837L);
					combination = combinationBL.createAnalysisCombination(
							combination, product.getCode(), true, impl);
				} else {
					System.out.println("Product Id " + product.getProductId()
							+ " combination not created.");
				}
				product.setCombinationByInventoryAccount(combination);
			}/*
			 * else{ combination = new Combination();
			 * combination.setCombinationId(14838L); combination =
			 * combinationBL.createAnalysisCombination( combination,
			 * product.getCode(), true);
			 * product.setCombinationByExpenseAccount(combination); }
			 */
		}
		productService.saveProducts(products);
	}

	public void deleteProduct(Product product) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Product.class.getSimpleName(), product.getProductId(), user,
				workflowDetailVo);
	}

	public void updateProductAccount() throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(6l);
		List<Product> products = productService.getProductsByCode(impl);
		List<Account> accounts = new ArrayList<Account>();
		for (Product product : products) {
			Account account = product.getCombinationByInventoryAccount()
					.getAccountByAnalysisAccountId();
			account.setAccount(product.getCode());
			accounts.add(account);
		}
		combinationBL.getAccountBL().getAccountService().saveAccounts(accounts);
	}

	public void updateProductCode() throws Exception {
		Implementation impl = new Implementation();
		impl.setImplementationId(12l);
		List<Product> products = productService.getProductsByCode(impl);
		Map<String, Product> productMap = new HashMap<String, Product>();
		String lastCode = "";
		for (Product product : products) {
			if (null != productMap && productMap.containsKey(product.getCode())) {
				product.setCode(updateProductCode(lastCode));
				lastCode = product.getCode();
				productMap.put(product.getCode(), product);
			} else {
				productMap.put(product.getCode(), product);
				lastCode = product.getCode();
			}
		}
		productService
				.saveProducts(new ArrayList<Product>(productMap.values()));
	}

	public List<String> productFileReader(String fileName) {
		File file = new File(fileName);
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Stock> addStockProducts(List<String> productList)
			throws Exception {
		Product product = null;
		List<Stock> stocks = new ArrayList<Stock>();
		Stock stock = null;
		for (String string : productList) {
			String[] prdstr = splitValues(string, ",");
			if (prdstr.length == 0 || prdstr[0] == null
					|| prdstr[0].trim().equalsIgnoreCase(""))
				continue;

			if (prdstr[0].equalsIgnoreCase("NO CODE"))
				product = productService.getProductByProductName(
						getImplementation(), prdstr[0].trim());
			else
				product = productService.getProductByProductCode(
						getImplementation(), prdstr[0].trim());

			if (null != product) {
				stock = new Stock();
				stock.setProduct(product);

				stock.setImplementation(getImplementation());
				if (prdstr[2] != null && !prdstr[2].trim().equalsIgnoreCase("")) {
					stock.setUnitRate(Double.parseDouble(prdstr[2].trim()));
				} else {
					stock.setUnitRate(1.0);
				}
				if (prdstr[3] != null && !prdstr[3].trim().equalsIgnoreCase("")) {
					stock.setQuantity(Double.parseDouble(prdstr[3].trim()));
				} else {
					stock.setQuantity(0.0);
				}
				if (prdstr[4] != null && !prdstr[4].equalsIgnoreCase(""))
					stock.setBatchNumber(prdstr[4].trim());
				if (prdstr[5] != null && !prdstr[5].trim().equalsIgnoreCase(""))
					stock.setProductExpiry(DateFormat
							.convertStringToDate(prdstr[5].trim()));
				if (prdstr[6] != null && !prdstr[6].trim().equalsIgnoreCase("")) {
					Shelf shelf = storeBL.getStoreService().getShelfById(
							Integer.valueOf(prdstr[6]));
					stock.setShelf(shelf);
					stock.setStore(shelf.getShelf().getAisle().getStore());
				} else {
					stock.setShelf(product.getShelf());
					stock.setStore(product.getShelf().getShelf().getAisle()
							.getStore());
				}

				stocks.add(stock);
			} else {
				System.out
						.println("Product Not in the system   ==" + prdstr[0]);
			}
		}
		return stocks;
	}

	public ProductCategoryVO addProductInventory(List<String> productList) {
		ProductCategoryVO productCategoryVO = null;
		try {
			productCategoryVO = new ProductCategoryVO();
			List<ProductVO> productVOs = new ArrayList<ProductVO>();
			ProductVO productVO = null;
			Combination combination = null;
			ProductCategoryVO category = null;
			LookupDetail lookupDetail = null;
			Map<String, Map<String, ProductCategoryVO>> categoryMap = new HashMap<String, Map<String, ProductCategoryVO>>();
			for (String product : productList) {
				productVO = new ProductVO();
				combination = new Combination();
				String[] prdstr = splitValues(product, ",");
				lookupDetail = new LookupDetail();
				productVO.setCategory(prdstr[0].trim());
				productVO.setSubCategory(prdstr[1].trim());
				productVO.setProductName(prdstr[2].trim());
				productVO.setCode(prdstr[3].trim());
				productVO.setItemType(ItemType.Inventory.getCode());
				productVO.setItemSubType(InventorySubCategory.Raw_Material
						.getCode());
				productVO.setCostingType(CostingType.LIFO.getCode());
				productVO.setBasePrice(Double.parseDouble(AIOSCommons
						.formatAmountToDouble(prdstr[4].trim()).toString()));
				productVO.setQuantity(Double.parseDouble(prdstr[5].trim()));
				combination.setCombinationId(Long.parseLong(prdstr[6].trim()));
				productVO.setCombinationByInventoryAccount(combination);
				lookupDetail.setLookupDetailId(3l);
				productVO.setLookupDetailByProductUnit(lookupDetail);
				productVO.setStatus(true);

				productVO.setBarCode(productVO.getCode());
				productVO.setImplementation(getImplementation());
				Map<String, ProductCategoryVO> subCategoryMap = null;
				if (null != categoryMap && categoryMap.size() > 0
						&& categoryMap.containsKey(productVO.getCategory())) {
					subCategoryMap = new HashMap<String, ProductCategoryVO>(
							categoryMap.get(productVO.getCategory()));
					if (null != subCategoryMap
							&& subCategoryMap.size() > 0
							&& subCategoryMap.containsKey(productVO
									.getSubCategory())) {
						productVO.setProductCategory(subCategoryMap
								.get(productVO.getSubCategory()));
					} else {
						category = productCategoryBL
								.addProductCategory(productVO.getSubCategory());
						subCategoryMap
								.put(productVO.getSubCategory(), category);
					}
				} else {
					subCategoryMap = new HashMap<String, ProductCategoryVO>();
					category = productCategoryBL.addProductCategory(productVO
							.getSubCategory());
					subCategoryMap.put(productVO.getSubCategory(), category);
				}
				categoryMap.put(productVO.getCategory(), subCategoryMap);
				productVOs.add(productVO);
			}
			List<ProductCategoryVO> categories = new ArrayList<ProductCategoryVO>();
			Set<ProductCategoryVO> subCategories = null;
			Map<String, ProductCategoryVO> subCategoryMap = null;
			for (Entry<String, Map<String, ProductCategoryVO>> prdCategory : categoryMap
					.entrySet()) {
				category = productCategoryBL.addProductCategory(prdCategory
						.getKey());
				subCategoryMap = new HashMap<String, ProductCategoryVO>(
						prdCategory.getValue());
				subCategories = new HashSet<ProductCategoryVO>();
				for (Entry<String, ProductCategoryVO> subPrdCategory : subCategoryMap
						.entrySet()) {
					subCategories.add(subPrdCategory.getValue());
				}
				category.setSubCategorySet(subCategories);
				categories.add(category);
			}
			productCategoryVO.setProductVOs(productVOs);
			productCategoryVO.setSubCategories(categories);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return productCategoryVO;
	}

	public ProductCategoryVO addProducts(List<String> productList) {
		ProductCategoryVO productCategoryVO = null;
		try {
			productCategoryVO = new ProductCategoryVO();
			List<ProductVO> productVOs = new ArrayList<ProductVO>();
			ProductVO productVO = null;
			Combination combination = null;
			List<String> productCodes = null;
			ProductCategoryVO category = null;
			Map<Byte, List<String>> codeMap = new HashMap<Byte, List<String>>();
			Map<String, Map<String, ProductCategoryVO>> categoryMap = new HashMap<String, Map<String, ProductCategoryVO>>();
			Map<String, LookupDetail> unitTypesMap = new HashMap<String, LookupDetail>();
			List<LookupDetail> lookupDetails = lookupMasterBL
					.getActiveLookupDetails("PRODUCT_UNITNAME", true);
			for (LookupDetail lookupDetail : lookupDetails) {
				unitTypesMap.put(lookupDetail.getDisplayName().trim()
						.toUpperCase(), lookupDetail);
			}
			Map<String, Shelf> shelfMap = new HashMap<String, Shelf>();
			List<Store> stores = storeBL.getStoreService().getAllStores(
					getImplementation());
			for (Store store : stores) {
				for (Aisle aisle : store.getAisles()) {
					for (Shelf rack : aisle.getShelfs()) {
						for (Shelf shelf : rack.getShelfs()) {
							shelfMap.put(shelf.getName().toUpperCase(), shelf);
						}
					}
				}
			}
			for (String product : productList) {
				productVO = new ProductVO();
				combination = new Combination();
				String[] prdstr = splitValues(product, ",");
				productVO.setCategory(prdstr[0].trim());
				productVO.setSubCategory(prdstr[1].trim());
				productVO.setProductName(prdstr[2].trim());
				productVO.setItemType(prdstr[3].trim().charAt(0));
				productVO.setItemSubType(InventorySubCategory.get(
						Byte.valueOf(prdstr[4].trim())).getCode());
				productVO.setCostingType(CostingType.get(
						Byte.valueOf(prdstr[5].trim())).getCode());
				if (productVO.getItemType().equals(ItemType.Expense.getCode())) {
					combination.setCombinationId(Long.parseLong(prdstr[6]
							.trim()));
					productVO.setCombinationByExpenseAccount(combination);
				} else {
					combination.setCombinationId(Long.parseLong(prdstr[6]
							.trim()));
					productVO.setCombinationByInventoryAccount(combination);
				}

				productVO.setLookupDetailByProductUnit(unitTypesMap
						.get(prdstr[7].trim().toUpperCase()));

				productVO.setBasePrice(Double.parseDouble(prdstr[8].trim()));
				if (prdstr[9] != null && !prdstr[9].equals("")
						&& Double.parseDouble(prdstr[9].trim()) > 0)
					productVO.setReOrderQuantity(Double.parseDouble(prdstr[9]
							.trim()));

				// Default store by product and stock
				productVO.setQuantity(Double.parseDouble(prdstr[10].trim()));
				productVO.setShelf(shelfMap
						.get(prdstr[11].trim().toUpperCase()));

				productCodes = new ArrayList<String>();
				if (prdstr.length == 12) {
					String productCode = "";
					if (null != codeMap && codeMap.size() > 0
							&& codeMap.containsKey(productVO.getItemSubType())) {
						productCodes.addAll(codeMap.get(productVO
								.getItemSubType()));
						productCode = generateProductCode(
								productVO.getItemSubType(), productCodes);
					} else {
						productCode = generateProductCode(
								productVO.getItemSubType(), productCodes);
					}
					productVO.setCode(productCode);
					productCodes.add(productCode);
					codeMap.put(productVO.getItemSubType(), productCodes);
				} else
					productVO.setCode(prdstr[12].trim());

				productVO
						.setStandardPrice(Double.parseDouble(prdstr[13].trim()));
				productVO
						.setSellingPrice(Double.parseDouble(prdstr[14].trim()));
				productVO.setSuggestedPrice(Double.parseDouble(prdstr[15]
						.trim()));

				productVO.setStatus(true);
				productVO.setBarCode(productVO.getCode());
				productVO.setImplementation(getImplementation());
				Map<String, ProductCategoryVO> subCategoryMap = null;
				if (null != categoryMap && categoryMap.size() > 0
						&& categoryMap.containsKey(productVO.getCategory())) {
					subCategoryMap = new HashMap<String, ProductCategoryVO>(
							categoryMap.get(productVO.getCategory()));
					if (null != subCategoryMap
							&& subCategoryMap.size() > 0
							&& subCategoryMap.containsKey(productVO
									.getSubCategory())) {
						productVO.setProductCategory(subCategoryMap
								.get(productVO.getSubCategory()));
					} else {
						category = productCategoryBL
								.addProductCategory(productVO.getSubCategory());
						subCategoryMap
								.put(productVO.getSubCategory(), category);
					}
				} else {
					subCategoryMap = new HashMap<String, ProductCategoryVO>();
					category = productCategoryBL.addProductCategory(productVO
							.getSubCategory());
					subCategoryMap.put(productVO.getSubCategory(), category);
				}
				categoryMap.put(productVO.getCategory(), subCategoryMap);
				productVOs.add(productVO);
			}
			List<ProductCategoryVO> categories = new ArrayList<ProductCategoryVO>();
			Set<ProductCategoryVO> subCategories = null;
			Map<String, ProductCategoryVO> subCategoryMap = null;
			for (Entry<String, Map<String, ProductCategoryVO>> prdCategory : categoryMap
					.entrySet()) {
				category = productCategoryBL.addProductCategory(prdCategory
						.getKey());
				subCategoryMap = new HashMap<String, ProductCategoryVO>(
						prdCategory.getValue());
				subCategories = new HashSet<ProductCategoryVO>();
				for (Entry<String, ProductCategoryVO> subPrdCategory : subCategoryMap
						.entrySet()) {
					subCategories.add(subPrdCategory.getValue());
				}
				category.setSubCategorySet(subCategories);
				categories.add(category);
			}
			productCategoryVO.setProductVOs(productVOs);
			productCategoryVO.setSubCategories(categories);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return productCategoryVO;
	}

	// to generate product code based on the sub category
	private String generateProductCode(byte subType, List<String> productCodes) {
		String productCode = "";
		try {
			if (productCodes != null && productCodes.size() > 0) {
				Collections.sort(productCodes, new Comparator<String>() {
					public int compare(String p1, String p2) {
						return p2.compareTo(p1);
					}
				});
				String subItemType = getProductCodeByItemType(subType);
				String s = productCodes.get(0);
				Long tempCode = Long.valueOf(s.substring(2, s.length()));
				tempCode += 1;
				productCode = subItemType + tempCode;
			} else {
				String categoryTyp = getProductCodeByItemType(subType);
				productCode = categoryTyp + 100000;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return productCode;
	}

	// to generate product code based on the sub category
	private String updateProductCode(String productCode) {
		try {
			Long tempCode = Long.valueOf(productCode.substring(2,
					productCode.length()));
			tempCode += 1;
			productCode = "RM" + tempCode;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return productCode;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public void extractImagesForOrderingMenu(Properties confProps,
			ServletContext context, Boolean overwrite, Product directProduct) {

		final String webappsImageFolder = "images/ordering-menu/company/";
		final String PRODUCTS = "products";
		final String CATEGORIES = "categories";
		String webContentProductsImageFolder = null;
		String webContentCategoriesImageFolder = null;
		List<Implementation> implementations = null;

		String hashedImageFilePath = null;
		byte[] fileBytes = null;
		byte[] extractedBytes = null;
		List<Product> products = null;
		HashMap<Long, String> productMap = null;
		HashMap<Long, Integer> productImageCountMap = null;
		List<Image> allImagesToExtract = null;
		String expectedImageName = null;
		Integer imgNumber = 0;

		System.out.println("Image Overwrite Status: " + overwrite);

		try {
			if (directProduct != null) {
				implementations = new ArrayList<Implementation>();
				implementations.add(directProduct.getImplementation());
			} else
				implementations = systemBL.getAllImplementations();

			for (Implementation implementation : implementations) {

				try {

					// currently only consider gcc, skip else
					if (!implementation.getCompanyKey().trim()
							.equalsIgnoreCase("gcc"))
						continue;

					System.out.println("Extracting images for "
							+ implementation.getCompanyKey());

					webContentProductsImageFolder = context
							.getRealPath(webappsImageFolder
									+ implementation.getCompanyKey()
									+ File.separator + PRODUCTS);
					System.out.println(webContentProductsImageFolder);
					webContentCategoriesImageFolder = context
							.getRealPath(webappsImageFolder
									+ implementation.getCompanyKey()
									+ File.separator + CATEGORIES);
					System.out.println(webContentCategoriesImageFolder);

					if (!(new File(webContentProductsImageFolder)).exists()) {
						new File(webContentProductsImageFolder).mkdir();
					}
					if (!(new File(webContentCategoriesImageFolder)).exists()) {
						new File(webContentCategoriesImageFolder).mkdir();
					}

					if (directProduct != null) {
						products = new ArrayList<Product>();
						products.add(directProduct);
					} else
						products = this.getActivePOSProduct(implementation);

					System.out.println("Products count : "
							+ ((products == null) ? "NULL" : products.size()));

					productMap = new HashMap<Long, String>();
					productImageCountMap = new HashMap<Long, Integer>();

					if (products == null || products.size() == 0)
						continue;

					for (Product product : products) {

						productMap.put(product.getProductId(),
								product.getCode());
					}

					if (directProduct != null)
						allImagesToExtract = imageBL
								.getRecordIdBasedImageListByImplementation(
										directProduct.getProductId(),
										implementation, "Product");
					else
						allImagesToExtract = imageBL
								.getEntityBasedImageListByImplementation(
										implementation, "Product");
					expectedImageName = null;

					// create a map to know number of images available per
					// product
					for (Image image : allImagesToExtract) {

						if (productImageCountMap.get(image.getRecordId()) == null)
							productImageCountMap.put(image.getRecordId(), 1);
						else
							productImageCountMap.put(image.getRecordId(),
									productImageCountMap.get(image
											.getRecordId()) + 1);
					}

					if (directProduct == null) {

						// get all category images to same image list
						allImagesToExtract.addAll(imageBL
								.getEntityBasedImageListByImplementation(
										implementation, "ProductCategory"));
					}

					for (Image image : allImagesToExtract) {

						try { // for category

							if (image.getTableName().equalsIgnoreCase(
									"ProductCategory")) {

								expectedImageName = "cat_"
										+ image.getRecordId() + ".png";

								System.out
										.println("Trying Product Catgegory Image: "
												+ expectedImageName);

								if (!(new File(webContentCategoriesImageFolder
										+ File.separator + expectedImageName)
										.exists()) || overwrite) {

									hashedImageFilePath = confProps
											.getProperty("file.upload.path")
											.concat(image.getHashedName());
									fileBytes = FileUtil.getBytes(new File(
											hashedImageFilePath));
									extractedBytes = CompressionUtil
											.extractBytes(fileBytes);

									FileUtil.writeExtracedBytesToDisk(
											extractedBytes,
											webContentCategoriesImageFolder
													+ File.separator
													+ expectedImageName);
								}
							} else { // for product

								if (productMap.get(image.getRecordId()) == null)
									continue;

								imgNumber = productImageCountMap.get(image
										.getRecordId());

								expectedImageName = productMap
										.get(image.getRecordId()).trim()
										.replace(" ", "")
										+ "_" + imgNumber + ".png";

								productImageCountMap.put(image.getRecordId(),
										imgNumber - 1);

								System.out.println("Trying Product Image: "
										+ expectedImageName);

								if (!(new File(webContentProductsImageFolder
										+ File.separator + expectedImageName)
										.exists()) || overwrite) {

									hashedImageFilePath = confProps
											.getProperty("file.upload.path")
											.concat(image.getHashedName());
									fileBytes = FileUtil.getBytes(new File(
											hashedImageFilePath));
									extractedBytes = CompressionUtil
											.extractBytes(fileBytes);

									FileUtil.writeExtracedBytesToDisk(
											extractedBytes,
											webContentProductsImageFolder
													+ File.separator
													+ expectedImageName);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CurrencyService getCurrencyService() {
		return currencyService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public ProductPricingService getProductPricingService() {
		return productPricingService;
	}

	public void setProductPricingService(
			ProductPricingService productPricingService) {
		this.productPricingService = productPricingService;
	}

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public SalesDeliveryNoteService getSalesDeliveryNoteService() {
		return salesDeliveryNoteService;
	}

	public void setSalesDeliveryNoteService(
			SalesDeliveryNoteService salesDeliveryNoteService) {
		this.salesDeliveryNoteService = salesDeliveryNoteService;
	}

	public PurchaseService getPurchaseService() {
		return purchaseService;
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

	public PointOfSaleService getPointOfSaleService() {
		return pointOfSaleService;
	}

	public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
		this.pointOfSaleService = pointOfSaleService;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public SystemBL getSystemBL() {
		return systemBL;
	}

	public void setSystemBL(SystemBL systemBL) {
		this.systemBL = systemBL;
	}
}
