package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryBO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryVO;
import com.aiotech.aios.accounts.service.ProductCategoryService;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ProductCategoryBL {

	private ProductCategoryService productCategoryService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;
	private ImageBL imageBL;

	public List<Object> showAllProductCategory() throws Exception {
		List<ProductCategory> productCategories = productCategoryService
				.getProductCategory(getImplementation());
		List<Object> productCategoryVO = new ArrayList<Object>();
		if (null != productCategories && productCategories.size() > 0) {
			for (ProductCategory productCategory : productCategories)
				productCategoryVO.add(addCategoryVO(productCategory));
		}
		return productCategoryVO;
	}

	// Save Product Category
	public void saveProductCategory(ProductCategory productCategory)
			throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		productCategory.setImplementation(getImplementation());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		boolean editFlag = false;
		if (productCategory != null
				&& productCategory.getProductCategoryId() != null) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
			editFlag = true;
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		productCategory.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		productCategoryService.saveProductCategory(productCategory);

		saveProductCategoryImages(productCategory, editFlag);
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductCategory.class.getSimpleName(),
				productCategory.getProductCategoryId(), user, workflowDetailVo);
	}

	public void saveProductCategoryImages(ProductCategory productCategory,
			boolean editFlag) throws Exception {
		DocumentVO docVO2 = new DocumentVO();
		docVO2.setDisplayPane("productCategoryImages");
		docVO2.setEdit(editFlag);
		docVO2.setObject(productCategory);
		imageBL.saveUploadedImages(productCategory, docVO2);
	}

	public void doProductCategoryBusinessUpdate(Long productCategoryId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			ProductCategory productCategory = productCategoryService
					.getProductCategoryDetails(productCategoryId);
			if (workflowDetailVO.isDeleteFlag()) {
				productCategoryService.deleteProductCategory(productCategory);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Save Product Category
	public void saveProductCategory(List<ProductCategory> productCategory)
			throws Exception {
		productCategoryService.saveProductCategory(productCategory);
	}

	// Delete Product Category
	public void deleteProductCategory(ProductCategory productCategory)
			throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductCategory.class.getSimpleName(),
				productCategory.getProductCategoryId(), user, workflowDetailVo);
	}

	public ProductCategoryVO showUpdateProductCategory(
			ProductCategory productCategory) throws Exception {
		ProductCategoryVO productCategoryVO = new ProductCategoryVO();
		productCategoryVO.setProductCategoryId(productCategory
				.getProductCategoryId());
		productCategoryVO.setCategoryName(productCategory.getCategoryName());
		productCategoryVO.setDescription(productCategory.getDescription());
		if (null != productCategory.getProductCategories()
				&& productCategory.getProductCategories().size() > 0) {
			productCategoryVO.setUpdateFlag(true);
		}
		productCategoryVO.setSpecialPos(productCategory.getSpecialPos());
		productCategoryVO.setStatus(productCategory.getStatus());
		productCategoryVO.setPublicName(productCategory.getPublicName());
		byte[] encoded = null;
		String pmg = new String();
		if (productCategory.getCategoryPicture() != null) {
			encoded = Base64.encodeBase64(productCategory.getCategoryPicture());
			pmg = new String(encoded);
			productCategoryVO.setCategoryPic(pmg);
			productCategoryVO.setCategoryPicture(productCategory
					.getCategoryPicture());
		}
		productCategoryVO.setBarPrint(productCategory.getBarPrint());
		productCategoryVO.setKitchenPrint(productCategory.getKitchenPrint());
		productCategoryVO.setParentCategoryId(null != productCategory
				.getProductCategory() ? productCategory.getProductCategory()
				.getProductCategoryId() : 0);
		return productCategoryVO;
	}

	private ProductCategoryBO addCategoryVO(ProductCategory productCategory) {
		ProductCategoryBO productCategoryBO = new ProductCategoryBO();
		productCategoryBO.setProductCategoryId(productCategory
				.getProductCategoryId());
		productCategoryBO.setCategoryName(productCategory.getCategoryName());
		productCategoryBO.setDescription(productCategory.getDescription());
		if (null != productCategory.getProductCategory()
				&& !("").equals(productCategory.getProductCategory()))
			productCategoryBO.setParentCatgoryName(productCategory
					.getProductCategory().getCategoryName());
		return productCategoryBO;
	}

	// Get all product categories
	public List<ProductCategoryVO> getProductCategory() throws Exception {
		List<ProductCategory> productCategories = productCategoryService
				.getProductCategory(getImplementation());
		List<ProductCategoryVO> productCategoryVOs = null;
		if (null != productCategories && productCategories.size() > 0) {
			productCategoryVOs = new ArrayList<ProductCategoryVO>();
			Map<Long, List<ProductCategoryVO>> subCategories = new HashMap<Long, List<ProductCategoryVO>>();
			List<ProductCategoryVO> tempCategory = null;
			for (ProductCategory productCategory : productCategories) {
				if (null != productCategory.getProductCategory()) {
					tempCategory = new ArrayList<ProductCategoryVO>();
					tempCategory.add(convertProductCategory(productCategory));
					if (null != subCategories
							&& subCategories.containsKey(productCategory
									.getProductCategory()
									.getProductCategoryId())) {
						tempCategory.addAll(subCategories.get(productCategory
								.getProductCategory().getProductCategoryId()));

					}
					subCategories.put(productCategory.getProductCategory()
							.getProductCategoryId(), tempCategory);
				} else {
					productCategoryVOs
							.add(convertProductCategory(productCategory));
				}
			}

			if (null != subCategories && subCategories.size() > 0) {
				for (ProductCategoryVO productCategory : productCategoryVOs) {
					productCategory.setSubCategories(subCategories
							.get(productCategory.getProductCategoryId()));
				}
			}
		}
		return productCategoryVOs;
	}

	// Get all product sub categories
	public List<ProductCategoryVO> getAllProductCategories() throws Exception {
		List<ProductCategory> productCategories = productCategoryService
				.getProductParentCategory(getImplementation());
		List<ProductCategoryVO> productCategoryVOs = new ArrayList<ProductCategoryVO>();
		for (ProductCategory productCategory : productCategories) {
			productCategoryVOs.add(convertProductCategory(productCategory));
		}
		return productCategoryVOs;
	}

	public List<ProductCategoryVO> getAllProductCategories(
			Implementation implementation) throws Exception {
		List<ProductCategory> productCategories = productCategoryService
				.getProductParentCategory(implementation);
		List<ProductCategoryVO> productCategoryVOs = new ArrayList<ProductCategoryVO>();
		for (ProductCategory productCategory : productCategories) {
			productCategoryVOs.add(convertProductCategory(productCategory));
		}
		return productCategoryVOs;
	}

	// Get all product sub categories
	public List<ProductCategoryVO> getAllPOSProductCategories()
			throws Exception {
		List<ProductCategory> productCategories = productCategoryService
				.getAllPOSProductCategories(getImplementation());
		List<ProductCategoryVO> productCategoryVOs = new ArrayList<ProductCategoryVO>();
		for (ProductCategory productCategory : productCategories) {
			productCategoryVOs.add(convertProductCategory(productCategory));
		}
		return productCategoryVOs;
	}

	// Get all product sub categories
	public List<ProductCategoryVO> getAllProductSubCategories()
			throws Exception {
		List<ProductCategory> productSubCategories = productCategoryService
				.getProductSubCategory(getImplementation());
		List<ProductCategoryVO> productCategoryVOs = new ArrayList<ProductCategoryVO>();
		for (ProductCategory productSubCategory : productSubCategories) {
			productCategoryVOs.add(convertProductCategory(productSubCategory));
		}
		return productCategoryVOs;
	}

	// Get all product sub categories by parent category
	public List<ProductCategoryVO> getProductSubCategoriesByCategory(
			Long categoryId) throws Exception {
		List<ProductCategory> productSubCategories = productCategoryService
				.getProductSubCategoryByCategory(categoryId);
		List<ProductCategoryVO> productCategoryVOs = new ArrayList<ProductCategoryVO>();
		for (ProductCategory productSubCategory : productSubCategories) {
			productCategoryVOs.add(convertProductCategory(productSubCategory));
		}
		return productCategoryVOs;
	}

	// Get all product sub categories by parent category
	public ProductCategory getProductSubCategoriesByProduct(long productId)
			throws Exception {
		ProductCategory productSubCategory = productCategoryService
				.getProductSubCategoryByProduct(productId);
		return productSubCategory;
	}

	// Convert Product Category
	private ProductCategoryVO convertProductCategory(ProductCategory subCategory)
			throws Exception {
		ProductCategoryVO productCategoryVO = new ProductCategoryVO();
		BeanUtils.copyProperties(productCategoryVO, subCategory);
		return productCategoryVO;
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public ProductCategoryVO addProductCategory(String categoryName)
			throws Exception {
		ProductCategoryVO category = new ProductCategoryVO();
		category.setCategoryName(categoryName);
		category.setStatus(true);
		category.setImplementation(getImplementation());
		category.setSpecialPos(false);
		category.setProductCategoryId(null);
		category.setImplementation(getImplementation());
		return category;
	}

	// Getters & Setters
	public ProductCategoryService getProductCategoryService() {
		return productCategoryService;
	}

	public void setProductCategoryService(
			ProductCategoryService productCategoryService) {
		this.productCategoryService = productCategoryService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}
}
