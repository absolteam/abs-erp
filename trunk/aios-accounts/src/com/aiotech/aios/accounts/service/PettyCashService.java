package com.aiotech.aios.accounts.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PettyCashDetail;
import com.aiotech.aios.common.to.Constants.Accounts.PettyCashTypes;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PettyCashService {

	private AIOTechGenericDAO<PettyCash> pettyCashDAO;
	private AIOTechGenericDAO<PettyCashDetail> pettyCashDetailDAO;

	// get all petty cash
	public List<PettyCash> getAllPettyCash(Implementation implementation)
			throws Exception {
		return pettyCashDAO.findByNamedQuery("getAllPettyCash", implementation,
				(byte) WorkflowConstants.Status.Published.getCode());
	}

	public List<PettyCash> getAllPettyCashOnlyExpense(
			Implementation implementation, byte pettyCashType) throws Exception {
		return pettyCashDAO.findByNamedQuery("getAllPettyCashOnlyExpense",
				implementation, pettyCashType);
	}

	// get all parent petty cash
	public List<PettyCash> getAllParentPettyCash(Implementation implementation)
			throws Exception {
		return pettyCashDAO.findByNamedQuery("getAllParentPettyCash",
				implementation);
	}

	public PettyCash getParentPettyCash(long pettyCashId) throws Exception {
		return pettyCashDAO.findByNamedQuery("getParentPettyCash", pettyCashId)
				.get(0);
	}

	// find by id
	public PettyCash findPettyCashById(long pettyCashId) throws Exception {
		return pettyCashDAO.findById(pettyCashId);
	}

	// find by petty cash by id
	public PettyCash getPettyCashById(long pettyCashId) throws Exception {
		return pettyCashDAO.findByNamedQuery("getPettyCashById", pettyCashId)
				.get(0);
	}

	public PettyCash getBasicPettyCashById(long pettyCashId) throws Exception {
		return pettyCashDAO.findByNamedQuery("getBasicPettyCashById",
				pettyCashId).get(0);
	}

	public List<PettyCash> getPettyCashChildById(long pettyCashId)
			throws Exception {
		return pettyCashDAO.findByNamedQuery("getPettyCashChildById",
				pettyCashId);
	}

	public PettyCash getPettyCashParent(long pettyCashId) throws Exception {
		return pettyCashDAO.findByNamedQuery("getPettyCashParent", pettyCashId)
				.get(0);
	}

	// find by petty cash by id
	public PettyCash getPettyCashForPrint(long pettyCashId) throws Exception {
		return pettyCashDAO.findByNamedQuery("getPettyCashForPrint",
				pettyCashId).get(0);
	}

	// find by petty cash by id
	public PettyCash getPettyCashByUseCase(long recordId, String useCase)
			throws Exception {
		List<PettyCash> pettyCash = pettyCashDAO.findByNamedQuery(
				"getPettyCashByUseCase", recordId, useCase);
		return null != pettyCash && pettyCash.size() > 0 ? pettyCash.get(0)
				: null;
	}

	// get all petty cash by direct payment detail
	public List<PettyCash> getAllPettyCashByPayments(
			DirectPaymentDetail directPaymentDetail) throws Exception {
		return pettyCashDAO.findByNamedQuery("getAllPettyCashByPayments",
				directPaymentDetail);
	}

	// get all petty cash by bank receipts detail
	public List<PettyCash> getAllPettyCashByReceipts(
			BankReceiptsDetail bankReceiptsDetail) throws Exception {
		return pettyCashDAO.findByNamedQuery("getAllPettyCashByReceipts",
				bankReceiptsDetail);
	}

	// get all receipts petty cash
	public List<PettyCash> getAllReceiptsPettyCash(byte pettyCashType,
			Implementation implementation, boolean pettyCashStatus)
			throws Exception {
		List<PettyCash> pettyCashs = pettyCashDAO.findByNamedQuery(
				"getAllReceiptsPettyCash", implementation, pettyCashType,
				pettyCashStatus);
		return pettyCashs;
	}

	// get all receipts petty cash
	public List<PettyCash> getAllNonClosedReceiptsPettyCash(byte pettyCashType,
			Implementation implementation) throws Exception {
		List<PettyCash> pettyCashs = pettyCashDAO.findByNamedQuery(
				"getAllNonClosedReceiptsPettyCash", implementation,
				pettyCashType);
		return pettyCashs;
	}

	// get all receipts petty cash
	public List<PettyCash> getPettyCashInCarryForward(byte pettyCashType,
			boolean closedFlag, Implementation implementation, long pettyCashId)
			throws Exception {
		List<PettyCash> pettyCashs = pettyCashDAO.findByNamedQuery(
				"getPettyCashInCarryForward", implementation, pettyCashType,
				closedFlag, pettyCashId);
		return pettyCashs;
	}

	// get all receipts petty cash
	public List<PettyCash> getReceiptsPettyCash(byte pettyCashType,
			byte pettyCashType1, Implementation implementation)
			throws Exception {
		List<PettyCash> pettyCashs = pettyCashDAO.findByNamedQuery(
				"getReceiptsPettyCash", implementation, pettyCashType,
				pettyCashType1);
		return null != pettyCashs && pettyCashs.size() > 0 ? pettyCashs : null;
	}

	// get all receipts petty cash
	public List<PettyCash> getReceiptsPettyCashWithOutSettlementId(
			byte pettyCashType, Implementation implementation, long pettyCashId)
			throws Exception {
		List<PettyCash> pettyCashs = pettyCashDAO.findByNamedQuery(
				"getReceiptsPettyCashWithOutSettlementId", implementation,
				pettyCashType, pettyCashId);
		return null != pettyCashs && pettyCashs.size() > 0 ? pettyCashs : null;
	}

	@SuppressWarnings("unchecked")
	public List<PettyCash> getReceiptsPettyCashOnlyCashTransfer(
			Set<Long> pettyCashIds) throws Exception {

		Criteria criteria = pettyCashDAO.createCriteria();

		criteria.createAlias("bankReceiptsDetail", "bk",
				CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("bk.bankReceipts", "b",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("pettyCashType",
				PettyCashTypes.Cash_transfer.getCode()));
		criteria.add(Restrictions.in("bk.bankReceiptsDetailId", pettyCashIds));
		Set<PettyCash> uniqueRecords = new HashSet<PettyCash>(criteria.list());
		List<PettyCash> pettyCashType = new ArrayList<PettyCash>(uniqueRecords);
		return pettyCashType;
	}

	@SuppressWarnings("unchecked")
	public List<PettyCash> getDirectPaymentPettyCashOnlyCashTransfer(
			Set<Long> pettyCashIds) throws Exception {

		Criteria criteria = pettyCashDAO.createCriteria();

		criteria.createAlias("directPaymentDetail", "dp",
				CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("pettyCashType",
				PettyCashTypes.Cash_transfer.getCode()));
		criteria.add(Restrictions.in("dp.directPaymentDetailId", pettyCashIds));
		Set<PettyCash> uniqueRecords = new HashSet<PettyCash>(criteria.list());
		List<PettyCash> pettyCashType = new ArrayList<PettyCash>(uniqueRecords);
		return pettyCashType;
	}

	@SuppressWarnings("unchecked")
	public List<PettyCash> getAllAdvanceIssedPettyCash(long pettyCashId,
			Byte spendType, Byte reimbursType, Byte receivedType,
			long pettyCashOldId) throws Exception {
		Criteria criteria = pettyCashDAO.createCriteria();
		criteria.createAlias("pettyCash", "py", CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("py.pettyCashId", pettyCashId));
		criteria.add(Restrictions.or(Restrictions.or(
				Restrictions.eq("pettyCashType", spendType),
				Restrictions.eq("pettyCashType", receivedType)), Restrictions
				.eq("pettyCashType", reimbursType)));
		if (pettyCashOldId > 0)
			criteria.add(Restrictions.ne("pettyCashId", pettyCashOldId));
		Set<PettyCash> uniqueRecords = new HashSet<PettyCash>(criteria.list());
		List<PettyCash> pettyCashs = new ArrayList<PettyCash>(uniqueRecords);
		return pettyCashs;
	}

	@SuppressWarnings("unchecked")
	public List<PettyCash> getAllAdvanceIssedPettyCashLevel(long pettyCashId,
			Byte pettyCashType, Byte pettyCashType1, Byte pettyCashType2,
			long pettyCashOldId) throws Exception {
		Criteria criteria = pettyCashDAO.createCriteria();
		criteria.createAlias("pettyCash", "py", CriteriaSpecification.LEFT_JOIN);

		criteria.add(Restrictions.eq("py.pettyCashId", pettyCashId));
		criteria.add(Restrictions.or(Restrictions.or(
				Restrictions.eq("pettyCashType", pettyCashType),
				Restrictions.eq("pettyCashType", pettyCashType1)), Restrictions
				.eq("pettyCashType", pettyCashType2)));
		if (pettyCashOldId > 0)
			criteria.add(Restrictions.ne("pettyCashId", pettyCashOldId));
		Set<PettyCash> uniqueRecords = new HashSet<PettyCash>(criteria.list());
		List<PettyCash> pettyCashs = new ArrayList<PettyCash>(uniqueRecords);
		return pettyCashs;
	}

	// get all advance petty cash
	public List<PettyCash> getAllAdvanceIssued(Implementation implementation)
			throws Exception {
		return pettyCashDAO.findByNamedQuery("getAllAdvanceIssued",
				implementation);
	}

	// get all advance petty cash
	public List<PettyCash> getAdvanceIssuedAndGiven(
			Implementation implementation, byte pettyCashType,
			byte pettyCashType1) throws Exception {
		return pettyCashDAO.findByNamedQuery("getAdvanceIssuedAndGiven",
				implementation, pettyCashType, pettyCashType1);
	}

	public List<PettyCashDetail> getPettyCashDetailByPettyId(long pettyCashId) {
		return pettyCashDetailDAO.findByNamedQuery(
				"getPettyCashDetailByPettyId", pettyCashId);
	}

	// delete petty cash
	public void deletePettyCash(PettyCash pettyCash,
			List<PettyCashDetail> pettyCashDetails) throws Exception {
		if (null != pettyCashDetails && pettyCashDetails.size() > 0)
			pettyCashDetailDAO.deleteAll(pettyCashDetails);
		pettyCashDAO.delete(pettyCash);
	}

	public void deletePettyCashDetail(List<PettyCashDetail> pettyCashDetails)
			throws Exception {
		pettyCashDetailDAO.deleteAll(pettyCashDetails);
	}

	// save or update petty cash
	public void savePettyCash(List<PettyCash> pettyCashs,
			List<PettyCashDetail> pettyCashDetails, PettyCash takenVoucher,
			PettyCash parentVocher) throws Exception {
		if (null != parentVocher)
			pettyCashDAO.saveOrUpdate(parentVocher);
		pettyCashDAO.saveOrUpdateAll(pettyCashs);
		if (null != takenVoucher)
			pettyCashDAO.saveOrUpdate(takenVoucher);
		if (null != pettyCashDetails && pettyCashDetails.size() > 0)
			pettyCashDetailDAO.saveOrUpdateAll(pettyCashDetails);
	}

	@SuppressWarnings("unchecked")
	public List<PettyCash> getParentPettyCashSettledVouchers(long pettyCashId,
			Byte pettyCashType) throws Exception {
		Criteria criteria = pettyCashDAO.createCriteria();
		criteria.createAlias("pettyCash", "py", CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("py.pettyCashId", pettyCashId));
		criteria.add(Restrictions.eq("pettyCashType", pettyCashType));
		Set<PettyCash> uniqueRecords = new HashSet<PettyCash>(criteria.list());
		List<PettyCash> pettyCashs = new ArrayList<PettyCash>(uniqueRecords);
		return pettyCashs;
	}

	public void savePettyCashDetails(List<PettyCashDetail> pettyCashDetails)
			throws Exception {
		pettyCashDetailDAO.saveOrUpdateAll(pettyCashDetails);
	}

	public void updatePettyCash(PettyCash pettyCash) throws Exception {
		pettyCashDAO.saveOrUpdate(pettyCash);
	}

	public AIOTechGenericDAO<PettyCash> getPettyCashDAO() {
		return pettyCashDAO;
	}

	public void setPettyCashDAO(AIOTechGenericDAO<PettyCash> pettyCashDAO) {
		this.pettyCashDAO = pettyCashDAO;
	}

	public AIOTechGenericDAO<PettyCashDetail> getPettyCashDetailDAO() {
		return pettyCashDetailDAO;
	}

	public void setPettyCashDetailDAO(
			AIOTechGenericDAO<PettyCashDetail> pettyCashDetailDAO) {
		this.pettyCashDetailDAO = pettyCashDetailDAO;
	}
}
