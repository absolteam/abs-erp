package com.aiotech.aios.accounts.service.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.MemberCard;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.domain.entity.vo.MemberCardVO;
import com.aiotech.aios.accounts.service.CustomerService;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.CustomerNature;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class CustomerBL {

	private CustomerService customerService;
	private CreditTermBL creditTermBL;
	private List<Customer> customerList = null;
	private CombinationBL combinationBL;
	private MemberCardBL memberCardBL;
	private TransactionBL transactionBL;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	public JSONObject getCustomerList(Implementation implementation)
			throws Exception {
		customerList = customerService.getAllApprovedCustomers(implementation);
		JSONObject jsonResponse = new JSONObject();
		JSONArray data = new JSONArray();
		JSONArray array = null;
		for (Customer list : customerList) {
			array = new JSONArray();
			array.add(list.getCustomerId());
			array.add(list.getCustomerNumber());
			array.add(Constants.Accounts.CustomerType.get(list
					.getCustomerType()));
			array.add(null != list.getCreditTerm() ? list.getCreditTerm()
					.getName() : "");
			if (null != list.getPersonByPersonId()
					&& !("").equals(list.getPersonByPersonId())) {
				array.add("Person");
				array.add(list
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(null != list.getPersonByPersonId()
								.getLastName() ? list.getPersonByPersonId()
								.getLastName() : ""));
			} else {
				array.add("Company");
				array.add(list.getCompany() != null ? list.getCompany()
						.getCompanyName() : list.getCmpDeptLocation()
						.getCompany().getCompanyName());
			}
			array.add(null != list.getPersonBySaleRepresentative() ? list
					.getPersonBySaleRepresentative().getFirstName().concat(" ")
					.concat(list.getPersonBySaleRepresentative().getLastName())
					: "");
			data.add(array);
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}

	// Show all Customer Details
	public List<Object> getAllCustomers(String pageInfo) throws Exception {
		List<Customer> customers = null;
		if (null != pageInfo && !("").equals(pageInfo)
				&& ("bankdeposit").equalsIgnoreCase(pageInfo))
			customers = customerService
					.getCustomersByReceipts(getImplementationId());
		else
			customers = customerService.getAllCustomers(getImplementationId());
		List<Object> customerVOs = new ArrayList<Object>();
		if (null != customers && customers.size() > 0) {
			for (Customer customer : customers)
				customerVOs.add(addCustomerVO(customer));
		}
		return customerVOs;
	}

	// Show all Customer Details
	public List<Object> getAllPOSCustomers(String pageInfo) throws Exception {
		List<Customer> customers = customerService
				.getAllPOSCustomers(getImplementationId());
		List<Object> customerVOs = new ArrayList<Object>();
		if (null != customers && customers.size() > 0) {
			for (Customer customer : customers)
				customerVOs.add(addPOSCustomerVO(customer));
		}
		return customerVOs;
	}

	// Add CustomerVO
	public CustomerVO addPOSCustomerVO(Customer customer) {
		CustomerVO customerVO = new CustomerVO();
		customerVO.setCustomerId(customer.getCustomerId());
		customerVO.setCustomerNumber(customer.getCustomerNumber());
		customerVO
				.setCustomerRecordId(null != customer.getCustomerRecordId() ? customer
						.getCustomerRecordId() : customer.getCustomerId());
		if (null != customer.getPersonByPersonId()) {
			customerVO.setCustomerName(customer.getPersonByPersonId()
					.getFirstName().concat(" ")
					.concat(customer.getPersonByPersonId().getLastName()));
			customerVO.setMobileNumber(customer.getPersonByPersonId()
					.getMobile());
			customerVO.setResidentialAddress(customer.getPersonByPersonId()
					.getResidentialAddress());
			customerVO.setType(customer.getPersonByPersonId().getGender()
					.toString());
			customerVO.setCustomerNature(CustomerNature.Person.getCode());
		} else if (null != customer.getCmpDeptLocation()) {
			customerVO.setCustomerName(customer.getCmpDeptLocation()
					.getCompany().getCompanyName());
			customerVO.setMobileNumber(customer.getCmpDeptLocation()
					.getCompany().getCompanyPhone());
			customerVO.setResidentialAddress(customer.getCmpDeptLocation()
					.getCompany().getCompanyAddress());
			customerVO.setCustomerNature(CustomerNature.CmpDeptLocation
					.getCode());
		} else {
			customerVO.setCustomerName(customer.getCompany().getCompanyName());
			customerVO.setMobileNumber(customer.getCompany().getCompanyPhone());
			customerVO.setResidentialAddress(customer.getCompany()
					.getCompanyAddress());
			customerVO.setCustomerNature(CustomerNature.Company.getCode());
		}
		return customerVO;
	}

	// Add CustomerVO
	public CustomerVO addCustomerVO(Customer customer) {
		CustomerVO customerVO = new CustomerVO();
		customerVO.setCustomerId(customer.getCustomerId());
		customerVO.setCustomerNumber(customer.getCustomerNumber());
		if ((null != customer.getCmpDeptLocation() && !("").equals(customer
				.getCmpDeptLocation()))
				|| (null != customer.getCompany() && !("").equals(customer
						.getCompany()))) {
			customerVO
					.setCustomerName(null != customer.getCmpDeptLocation() ? customer
							.getCmpDeptLocation().getCompany().getCompanyName()
							: customer.getCompany().getCompanyName());
			customerVO.setType("Company");
		} else {
			customerVO.setCustomerName(customer.getPersonByPersonId()
					.getFirstName().concat(" ")
					.concat(customer.getPersonByPersonId().getLastName()));
			customerVO.setType("Person");
		}
		if (customer.getCombination() != null) {
			customerVO.setCombinationId(customer.getCombination()
					.getCombinationId());
			customerVO.setAccountCode(customer
					.getCombination()
					.getAccountByAnalysisAccountId()
					.getAccount()
					.concat("["
							+ customer.getCombination()
									.getAccountByAnalysisAccountId().getCode()
							+ "]"));
		}
		customerVO.setCreditTermId(null != customer.getCreditTerm() ? customer
				.getCreditTerm().getCreditTermId() : 0);
		return customerVO;
	}

	// Get all customer details
	public List<CustomerVO> getAllCustomers(Implementation implementation)
			throws Exception {
		List<Customer> customers = customerService
				.getAllCustomers(implementation);
		List<CustomerVO> customerVOs = new ArrayList<CustomerVO>();
		CustomerVO customerVo = null;
		for (Customer customer : customers) {
			customerVo = new CustomerVO();
			BeanUtils.copyProperties(customerVo, customer);
			customerVOs.add(customerVo);
		}
		for (CustomerVO customerVO : customerVOs) {
			if ((null != customerVO.getCmpDeptLocation() && !("")
					.equals(customerVO.getCmpDeptLocation()))
					|| (null != customerVO.getCompany() && !("")
							.equals(customerVO.getCompany()))) {
				customerVO.setCustomerName(null != customerVO
						.getCmpDeptLocation() ? customerVO.getCmpDeptLocation()
						.getCompany().getCompanyName() : customerVO
						.getCompany().getCompanyName());
				customerVO.setType("Company");
			} else {
				customerVO
						.setCustomerName(customerVO
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(customerVO.getPersonByPersonId()
										.getLastName()));
				customerVO.setType("Person");
			}
		}
		return customerVOs;
	}

	public void saveCustomer(Customer customer,
			List<ShippingDetail> shippingDetails) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		customer.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (customer != null && customer.getCustomerId() != null
				&& customer.getCustomerId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		if (null != customer.getMemberType() && customer.getMemberType() > 0) {
			MemberCardVO memberCardVO = (MemberCardVO) memberCardBL
					.getMemberCardNumber(customer.getMemberType());
			if (null != customer.getCustomerId()
					&& customer.getMemberCardNumber() > memberCardVO
							.getCurrentDigit()
					&& null != memberCardVO.getUpdatedDigit()
					&& memberCardVO.getUpdatedDigit() > 0) {
				MemberCard memberCard = new MemberCard();
				BeanUtils.copyProperties(memberCard, memberCardVO);
				memberCard.setCurrentDigit(memberCardVO.getUpdatedDigit());
				memberCardBL.mergeMemberCard(memberCard);
			} else if (null == customer.getCustomerId()
					&& null != memberCardVO.getUpdatedDigit()
					&& memberCardVO.getUpdatedDigit() > 0) {
				customer.setMemberType(customer.getMemberType());
				customer.setMemberCardNumber(memberCardVO.getUpdatedDigit());
				MemberCard memberCard = new MemberCard();
				BeanUtils.copyProperties(memberCard, memberCardVO);
				memberCard.setCurrentDigit(memberCardVO.getUpdatedDigit());
				memberCardBL.mergeMemberCard(memberCard);
			}
		}
		if (null == shippingDetails || shippingDetails.size() == 0) {
			shippingDetails = new ArrayList<ShippingDetail>();
			ShippingDetail shippingDetail = new ShippingDetail();
			shippingDetail.setCustomer(customer);
			shippingDetail.setContactNo("");
			if (null != customer.getPersonByPersonId())
				shippingDetail.setName(customer.getPersonByPersonId()
						.getFirstName());
			else
				shippingDetail.setName(customer.getCompany().getCompanyName());
			shippingDetails.add(shippingDetail);
		}
		customerService.saveCustomer(customer, shippingDetails);
		if (null != customer.getOpeningBalance()
				&& customer.getOpeningBalance() > 0) {
			List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
			TransactionDetail transactionDetail = transactionBL
					.createTransactionDetail(customer.getOpeningBalance(),
							getImplementationId().getUnearnedRevenue(),
							TransactionType.Credit.getCode(), null, null, null);
			transactionDetails.add(transactionDetail);
			transactionDetail = new TransactionDetail();
			transactionDetail = transactionBL.createTransactionDetail(customer
					.getOpeningBalance(), customer.getCombination()
					.getCombinationId(), TransactionType.Debit.getCode(), null,
					null, null);
			transactionDetails.add(transactionDetail);
			Transaction transaction = transactionBL.createTransaction(
					transactionBL.getCurrency().getCurrencyId(), null, Calendar
							.getInstance().getTime(), transactionBL
							.getCategory("Accounts Receivables"), customer
							.getCustomerId(), Supplier.class.getSimpleName());
			transactionBL.saveTransaction(transaction, transactionDetails);

		}
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Customer.class.getSimpleName(), customer.getCustomerId(), user,
				workflowDetailVo);
	}

	public void saveCustomers(List<Customer> customers,
			List<ShippingDetail> shippingDetails) throws Exception {
		customerService.saveCustomer(customers, shippingDetails);
	}

	public void savePOSCustomer(Customer customer, ShippingDetail shippingDetail)
			throws Exception {
		customerService.saveCustomer(customer, shippingDetail);
	}

	public void savePOSCustomer(Customer customer,
			ShippingDetail shippingDetail, Session session) throws Exception {
		customerService.saveCustomer(customer, shippingDetail, session);
	}

	public void savePOSCustomers(List<Customer> customers,
			List<ShippingDetail> shippingDetails, Session session)
			throws Exception {
		customerService.saveCustomers(customers, shippingDetails, session);
	}

	public void doCustomerBusinessUpdate(Long customerId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				Customer customer = customerService
						.getCustomerDetails(customerId);
				Combination combination = customer.getCombination();
				Ledger ledger = combinationBL
						.getCombinationService()
						.getLederCombinationById(combination.getCombinationId());
				customerService.deleteCustomer(customer);
				combinationBL.getCombinationService().deleteLedger(ledger);
				combinationBL.getCombinationService().deleteCombination(
						combination);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteCustomer(Customer customer) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				Customer.class.getSimpleName(), customer.getCustomerId(), user,
				workflowDetailVo);
		workflowDetailVo.setDeleteFlag(true);
	}

	public void updateCustomers(List<Customer> customers) throws Exception {
		customerService.saveCustomers(customers);
	}

	public void updateCustomerFromFile(List<String> customerList) {
		Map<String, Customer> customersMap = null;
		try {
			customersMap = new HashMap<String, Customer>();
			List<Customer> customers = customerService
					.getAllCustomers(getImplementationId());
			for (Customer cust : customers) {
				customersMap.put(cust.getCustomerNumber(), cust);
			}
			String key = null;
			for (String string : customerList) {
				String[] customerstr = splitValues(string, ",");
				key = customerstr[0];
				Customer customer = null;
				if (null != customersMap && customersMap.containsKey(key)) {
					customer = customersMap.get(key);
					Double amount = Double.valueOf(customerstr[1])
							+ (customer.getOpeningBalance() != null ? customer
									.getOpeningBalance() : 0.0);
					customer.setOpeningBalance(amount);
				}
				customersMap.put(key, customer);

			}
			List<Customer> customersFinal = new ArrayList<Customer>(
					customersMap.values());
			for (Customer customer2 : customersFinal) {
				LocalDate fromDate = new LocalDate(
						DateFormat.convertStringToDate("01-Jan-2013"));
				LocalDate toDate = new LocalDate(
						DateFormat.convertStringToDate("30-Apr-2015"));
				List<Transaction> transactions = transactionBL
						.getTransactionService().getFilteredJournal(
								customer2.getCombination(), fromDate.toDate(),
								toDate.toDate());
				transactionBL.directDeleteJV(transactions);
				if (customer2 != null && customer2.getOpeningBalance() != null)
					custoemrBalanceTransactionCreation(customer2);
				customerService.saveCustomer(
						customer2,
						new ArrayList<ShippingDetail>(customer2
								.getShippingDetails()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void custoemrBalanceTransactionCreation(Customer customer) {
		Double amount = 0.0;
		List<TransactionDetail> transList = new ArrayList<TransactionDetail>();
		Transaction transaction = null;
		try {
			LocalDate currentDate = new LocalDate(
					DateFormat.convertStringToDate("30-Apr-2015"));
			amount = customer.getOpeningBalance();
			Category childCategory = transactionBL
					.getCategory("Opening Balance");

			TransactionDetail transactionDetail = transactionBL
					.createTransactionDetail(amount, customer.getCombination()
							.getCombinationId(), TransactionType.Debit
							.getCode(), "Opening Balance for customer "
							+ customer.getCustomerNumber(), null, null);

			transList.add(transactionDetail);

			transactionDetail = transactionBL.createTransactionDetail(
					amount,
					getImplementationId().getClearingAccount(),
					TransactionType.Credit.getCode(),
					"Opening Balance Cash Clearing for customer "
							+ customer.getCustomerNumber(), null, null);

			transList.add(transactionDetail);

			transaction = transactionBL.createTransaction(transactionBL
					.getCurrency().getCurrencyId(), "Opening Balance"
					+ customer.getCustomerNumber(), currentDate.toDate(),
					childCategory, customer.getCustomerId(), Customer.class
							.getSimpleName());
			transactionBL.saveTransaction(transaction, transList);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Get Session Implementation
	public Implementation getImplementationId() {
		Implementation implementation = null;
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public List<String> fileReader(String fileName) {
		File file = new File(fileName);
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public List<Customer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<Customer> customerList) {
		this.customerList = customerList;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public MemberCardBL getMemberCardBL() {
		return memberCardBL;
	}

	public void setMemberCardBL(MemberCardBL memberCardBL) {
		this.memberCardBL = memberCardBL;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}
}
