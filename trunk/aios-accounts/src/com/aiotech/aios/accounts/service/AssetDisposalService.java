package com.aiotech.aios.accounts.service;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetDisposal;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AssetDisposalService {

	private AIOTechGenericDAO<AssetDisposal> assetDisposalDAO;

	// Get all Asset Disposal
	public List<AssetDisposal> getAllAssetDisposal(Implementation implementation)
			throws Exception {
		return assetDisposalDAO.findByNamedQuery("getAllAssetDisposal",
				implementation);
	}

	// Save Asset Disposal
	public void saveAssetDisposal(AssetDisposal assetDisposal) throws Exception {
		assetDisposalDAO.saveOrUpdate(assetDisposal);
	}

	// Getters & Setters
	public AIOTechGenericDAO<AssetDisposal> getAssetDisposalDAO() {
		return assetDisposalDAO;
	}

	public void setAssetDisposalDAO(
			AIOTechGenericDAO<AssetDisposal> assetDisposalDAO) {
		this.assetDisposalDAO = assetDisposalDAO;
	}
}
