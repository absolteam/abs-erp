package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionRequisitionVO;
import com.aiotech.aios.accounts.service.ProductionRequisitionService;
import com.aiotech.aios.accounts.service.WorkinProcessService;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionRequisitionStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.util.WorkflowConstants;

public class ProductionRequisitionBL {

	private ProductionRequisitionService productionRequisitionService;
	private WorkinProcessService workinProcessService;
	private MaterialIdleMixBL materialIdleMixBL;

	public List<Object> getAllProductionRequisitions() throws Exception {
		List<ProductionRequisition> productionRequisitions = productionRequisitionService
				.getAllProductionRequisitions(getImplementation());
		List<Object> productionRequisitionVOs = new ArrayList<Object>();
		if (null != productionRequisitions && productionRequisitions.size() > 0) {
			for (ProductionRequisition requisition : productionRequisitions)
				productionRequisitionVOs
						.add(addProductionRequisition(requisition));
		}
		return productionRequisitionVOs;
	}

	public List<Object> getProductionRequisitionsList() throws Exception {
		List<ProductionRequisition> productionRequisitions = productionRequisitionService
				.getProductionRequisitionsList(getImplementation(),
						ProductionRequisitionStatus.Closed.getCode());
		List<Object> productionRequisitionVOs = new ArrayList<Object>();
		if (null != productionRequisitions && productionRequisitions.size() > 0) {
			for (ProductionRequisition requisition : productionRequisitions)
				productionRequisitionVOs
						.add(addProductionRequisition(requisition));
		}
		return productionRequisitionVOs;
	}

	private Object addProductionRequisition(ProductionRequisition requisition)
			throws Exception {
		ProductionRequisitionVO requisitionVO = new ProductionRequisitionVO();
		requisitionVO.setProductionRequisitionId(requisition
				.getProductionRequisitionId());
		requisitionVO.setReferenceNumber(requisition.getReferenceNumber());
		requisitionVO.setRequisitionDateStr(DateFormat
				.convertDateToString(requisition.getRequisitionDate()
						.toString()));
		requisitionVO.setPersonName(requisition.getPerson().getFirstName()
				+ " " + requisition.getPerson().getLastName());
		requisitionVO.setRequisitionStatusStr(ProductionRequisitionStatus.get(
				requisition.getRequisitionStatus()).name());
		requisitionVO.setRequisitionStatus(requisition.getRequisitionStatus());
		if ((byte) ProductionRequisitionStatus.Open.getCode() == (byte) requisition
				.getRequisitionStatus())
			requisitionVO.setEditStatus(true);
		else
			requisitionVO.setEditStatus(false);
		return requisitionVO;
	}

	public void saveProductionRequisition(
			ProductionRequisition productionRequisition,
			List<ProductionRequisitionDetail> productionRequisitionDetails,
			List<ProductionRequisitionDetail> deletedRequisitionDetails)
			throws Exception {
		productionRequisition
				.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
		productionRequisition.setImplementation(getImplementation());
		if (null == productionRequisition.getProductionRequisitionId())
			SystemBL.saveReferenceStamp(ProductionRequisition.class.getName(),
					getImplementation());
		if (null != deletedRequisitionDetails
				&& deletedRequisitionDetails.size() > 0)
			productionRequisitionService
					.deleteProductionRequisitionDetails(deletedRequisitionDetails);
		productionRequisitionService.saveProductionRequisition(
				productionRequisition, productionRequisitionDetails);
	}

	public void deleteProductionRequisition(
			ProductionRequisition productionRequisition,
			List<ProductionRequisitionDetail> productionRequisitionDetails)
			throws Exception {
		productionRequisitionService.deleteProductionRequisition(
				productionRequisition, productionRequisitionDetails);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProductionRequisitionService getProductionRequisitionService() {
		return productionRequisitionService;
	}

	public void setProductionRequisitionService(
			ProductionRequisitionService productionRequisitionService) {
		this.productionRequisitionService = productionRequisitionService;
	}

	public MaterialIdleMixBL getMaterialIdleMixBL() {
		return materialIdleMixBL;
	}

	public void setMaterialIdleMixBL(MaterialIdleMixBL materialIdleMixBL) {
		this.materialIdleMixBL = materialIdleMixBL;
	}

	public WorkinProcessService getWorkinProcessService() {
		return workinProcessService;
	}

	public void setWorkinProcessService(
			WorkinProcessService workinProcessService) {
		this.workinProcessService = workinProcessService;
	}
}
