package com.aiotech.aios.accounts.service.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPoint;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPointVO;
import com.aiotech.aios.accounts.service.ProductPointService;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ProductPointBL {

	// Dependencies
	private ProductBL productBL;
	private ProductPointService productPointService;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private CommentBL commentBL;

	// Show all Product points
	public List<Object> showAllProductPoints() throws Exception {
		List<ProductPoint> productPoints = productPointService
				.getAllProductPoints(getImplementation());
		List<Object> productPointVOs = new ArrayList<Object>();
		if (null != productPoints && productPoints.size() > 0)
			for (ProductPoint productPoint : productPoints)
				productPointVOs.add(addProductPointsVO(productPoint));
		return productPointVOs;
	}

	// Show Active Product Points
	public List<ProductPoint> showActiveProductPoints() throws Exception {
		List<ProductPoint> productPoints = productPointService
				.getActiveProductPoints(getImplementation(), Calendar
						.getInstance().getTime());
		return productPoints;
	}

	// Get Product by product Id
	public ProductPoint getProductPointsByProductId(long productId)
			throws Exception {
		return productPointService.getProductPointsByProductId(productId,
				Calendar.getInstance().getTime());
	}

	// Add Product Point VO
	public Object addProductPointsVO(ProductPoint productPoint) {
		ProductPointVO productPointVO = new ProductPointVO();
		productPointVO.setProductPointId(productPoint.getProductPointId());
		productPointVO.setStatusPoints(productPoint.getStatusPoints());
		productPointVO.setPurchaseAmount(productPoint.getPurchaseAmount());
		Product product = new Product();
		product.setProductId(productPoint.getProduct().getProductId());
		product.setProductName(productPoint.getProduct().getProductName());
		product.setCode(productPoint.getProduct().getCode());
		productPointVO.setProduct(product);
		productPointVO.setDescription(productPoint.getDescription());
		productPointVO.setValidFrom(DateFormat.convertDateToString(productPoint
				.getFromDate().toString()));
		productPointVO.setValidTo(null != productPoint.getToDate() ? DateFormat
				.convertDateToString(productPoint.getToDate().toString()) : "");
		return productPointVO;
	}

	// Save Product Point
	public void saveProductPoint(ProductPoint productPoint) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		productPoint.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (productPoint != null && productPoint.getProductPointId() != null
				&& productPoint.getProductPointId() > 0) {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		productPointService.saveProductPoint(productPoint);

		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductPoint.class.getSimpleName(),
				productPoint.getProductPointId(), user, workflowDetailVo);
	}

	public void doProductPointBusinessUpdate(Long productPointId,
			WorkflowDetailVO workflowDetailVO) {
		try {
			if (workflowDetailVO.isDeleteFlag()) {
				ProductPoint productPoint = this.getProductPointService()
						.getProductPointById(productPointId);
				productPointService.deleteProductPoint(productPoint);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Delete Product Point
	public void deleteProductPoint(ProductPoint productPoint) throws Exception {
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Delete
						.getCode());
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				ProductPoint.class.getSimpleName(),
				productPoint.getProductPointId(), user, workflowDetailVo);
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	// Getters & Setters
	public ProductPointService getProductPointService() {
		return productPointService;
	}

	public void setProductPointService(ProductPointService productPointService) {
		this.productPointService = productPointService;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	@Autowired
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	@Autowired
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
}
