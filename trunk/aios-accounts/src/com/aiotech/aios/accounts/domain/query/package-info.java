@org.hibernate.annotations.NamedQueries({

		@org.hibernate.annotations.NamedQuery(name = "getFinancialYearById", query = "SELECT DISTINCT(c) FROM Calendar c"
				+ " JOIN FETCH c.periods p WHERE c.calendarId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getPeriodList", query = "SELECT p FROM Period p"
				+ " JOIN p.calendar c WHERE c.calendarId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getFiscalPeriodDetails", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE c.calendarId=?"
				+ " AND p.startTime < ?" + " AND p.endTime < ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionPostingPeriod", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE c.implementation = ?"
				+ " AND p.startTime <= ?"
				+ " AND p.endTime >= ?"
				+ " AND p.status = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPeriodByPeriodDate", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE c.implementation = ?"
				+ " AND p.startTime <= ?" + " AND p.endTime >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getFiscalAbovePeriods", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN p.calendar c"
				+ " WHERE c.calendarId=?"
				+ " AND p.startTime < ?"
				+ " AND p.endTime < ?"
				+ " AND p.periodId != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getNonClosedPeriodDetails", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE c.calendarId=?"
				+ " AND p.status = 1"
				+ " AND p.startTime < ?"
				+ " AND p.endTime < ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllActivePeriods", query = "SELECT p FROM Period p"
				+ " WHERE p.calendar.calendarId = ? AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePeriods", query = "SELECT p FROM Period p"
				+ " WHERE p.calendar.implementation= ? AND p.status=1 AND p.calendar.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "periodsWithoutImplementation", query = "SELECT p FROM Period p"
				+ " WHERE p.status=1 AND p.calendar.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPeriods", query = "SELECT p FROM Period p"
				+ " JOIN p.calendar c" + " WHERE c.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveCalendarClosedPeriods", query = "SELECT DISTINCT(p) FROM Period p"
				+ " WHERE p.calendar.calendarId IN(SELECT c.calendarId FROM Calendar c WHERE c.implementation = ? AND c.status = 1)"
				+ " AND p.status = 2"),

		@org.hibernate.annotations.NamedQuery(name = "getAllClosedPeriods", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE c.implementation = ?"
				+ " AND (p.status = ? OR p.status = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePeriodsWithoutImplementation", query = "SELECT DISTINCT(p) FROM Period p"
				+ " JOIN p.calendar c WHERE c.implementation = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCalendarByPeriod", query = "SELECT DISTINCT(c) FROM Calendar c"
				+ " JOIN c.periods p" + " WHERE p.periodId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllAccountCodes", query = "SELECT DISTINCT(p) FROM Account p"
				+ " JOIN FETCH p.segment s"
				+ " WHERE p.implementation = ?"
				+ " AND s.segmentId = ?"
 				+ " AND p.isSystem = 0" 
 				+ " AND p.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountsBySegmentAccessId", query = "SELECT DISTINCT(a) FROM Account a"
				+ " JOIN FETCH a.segment s"
				+ " WHERE a.implementation = ?"
				+ " AND s.accessId = ?"),
 	 	
		@org.hibernate.annotations.NamedQuery(name = "getAccountsBySegmentId", query = "SELECT DISTINCT(a) FROM Account a"
				+ " JOIN FETCH a.segment s"
				+ " WHERE a.implementation = ?"
				+ " AND s.segmentId = ?"),
					
		@org.hibernate.annotations.NamedQuery(name = "getFilterAccounts", query = "SELECT DISTINCT a FROM Account a"
				+ " WHERE a.implementation=? AND a.accountType=? AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPeriodFromDate", query = "SELECT DISTINCT p FROM Period p"
				+ " JOIN p.calendar c"
				+ " WHERE c.implementation=?"
				+ " AND c.status=1"
				+ " AND ? BETWEEN p.startTime AND p.endTime"
				+ " AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getCalendarPeriodById", query = "SELECT DISTINCT p FROM Period p"
				+ " JOIN FETCH p.calendar c" + " WHERE p.periodId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPeriodByCalendarAndDate", query = "SELECT DISTINCT p FROM Period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE c.calendarId=?"
				+ " AND c.status=1"
				+ " AND p.startTime <= ?"
				+ " AND p.endTime >= ?" + " AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalLinesList", query = "SELECT DISTINCT detail FROM TransactionDetail detail"
				+ " LEFT JOIN FETCH detail.transaction trans"
				+ " LEFT JOIN FETCH detail.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE trans.transactionId=? "
				+ " ORDER BY natural.accountType, detail.isDebit DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalLinesdel", query = "SELECT DISTINCT detail FROM TransactionDetail detail"
				+ " LEFT JOIN FETCH detail.transaction trans"
				+ " WHERE trans.transactionId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllTransactionByCategory", query = "SELECT DISTINCT t FROM Transaction t"
				+ " JOIN t.category c" + " WHERE c.categoryId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalById", query = "SELECT DISTINCT t FROM Transaction t"
				+ " LEFT JOIN FETCH t.transactionTemps tt "
				+ " JOIN FETCH t.period p" + " WHERE t.transactionId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCalendarList", query = "SELECT DISTINCT c FROM Calendar c  "
				+ " WHERE c.implementation = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllApprovedCalendar", query = "SELECT DISTINCT c FROM Calendar c  "
				+ " WHERE c.implementation = ? " + " AND c.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveCalendar", query = "SELECT DISTINCT c FROM Calendar c  "
				+ " WHERE c.implementation = ? AND c.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePeriod", query = "SELECT DISTINCT p FROM Period p  "
				+ " WHERE p.calendar.calendarId = ? AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getCurrencyList", query = "SELECT DISTINCT(c) FROM Currency c  "
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE c.implementation = ? " + " AND c.isApprove = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getNonFnCurrencies", query = "SELECT DISTINCT(c) FROM Currency c  "
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE c.implementation = ? " 
				+ " AND c.isApprove = ?"
				+ " AND c.defaultCurrency = 0"),		
				
		@org.hibernate.annotations.NamedQuery(name = "getCurrency", query = "SELECT DISTINCT(c) FROM Currency c  "
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE c.currencyId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCurrencyPool", query = "SELECT DISTINCT(cp) FROM CurrencyPool cp  "
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE cp.currencyPoolId NOT IN (SELECT c.currencyPool.currencyPoolId FROM Currency c"
				+ " WHERE c.implementation = ?) "),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllCurrencyPool", query = "SELECT DISTINCT(cp) FROM CurrencyPool cp  "
				+ " LEFT JOIN FETCH cp.country cty"),
		
		@org.hibernate.annotations.NamedQuery(name = "getAllCurrencyConversion", query = "SELECT DISTINCT(c) FROM CurrencyConversion c "
				+ " JOIN FETCH c.currency cr"
				+ " JOIN FETCH cr.currencyPool cp"
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE c.implementation = ? " 
				+ " AND c.isApprove = ?"),	
				
		@org.hibernate.annotations.NamedQuery(name = "getCurrencyConversionById", query = "SELECT DISTINCT(c) FROM CurrencyConversion c "
				+ " JOIN FETCH c.currency cr"
				+ " JOIN FETCH cr.currencyPool cp"
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE c.currencyConversionId = ? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getCurrencyConversionByCurrency", query = "SELECT DISTINCT(c) FROM CurrencyConversion c "
				+ " JOIN FETCH c.currency cr"
 				+ " WHERE cr.currencyId = ? "),
					
		@org.hibernate.annotations.NamedQuery(name = "getAllAccounts", query = "SELECT a FROM Account a  "
				+ " JOIN FETCH a.segment seg WHERE a.implementation = ? "
				+ " AND a.isApprove = ?"
				+ " AND (a.isSystem IS NULL OR a.isSystem = 0)"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountsWithOutImplementation", query = "SELECT DISTINCT(a) FROM Account a"
				+ " JOIN FETCH a.implementation impl"
				+ " LEFT JOIN FETCH a.combinationsForCompanyAccountId cpy"
				+ " LEFT JOIN FETCH a.combinationsForCostcenterAccountId cst"
				+ " LEFT JOIN FETCH a.combinationsForNaturalAccountId nt"
				+ " LEFT JOIN FETCH a.combinationsForAnalysisAccountId ay"
				+ " LEFT JOIN FETCH a.combinationsForBuffer1AccountId b1"
				+ " LEFT JOIN FETCH a.combinationsForBuffer2AccountId b2"
				+ " JOIN FETCH a.segment s ORDER BY a.implementation DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountsByAccount", query = "SELECT a FROM Account a  "
				+ " WHERE a.account = ? AND a.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountsByCode", query = "SELECT a FROM Account a "
				+ " WHERE a.code = ? AND a.implementation = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAccountsByAccountType", query = "SELECT a FROM Account a "
				+ " JOIN FETCH a.segment segment"
				+ " WHERE a.implementation = ? AND a.accountType = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCOAAccountsByAccountType", query = "SELECT a FROM Account a "
				+ " JOIN FETCH a.segment segment"
				+ " WHERE a.implementation = ? AND a.accountType = ? AND a.isSystem = 0"),

		@org.hibernate.annotations.NamedQuery(name = "validateAccountDetails", query = "SELECT a FROM Account a "
				+ " WHERE a.implementation=? AND  a.code = ? AND a.segment.segmentId=? "),

		@org.hibernate.annotations.NamedQuery(name = "validateByCompanyAccount", query = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "validateByCostAccount", query = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId=? AND c.accountByCostcenterAccountId= ?"),

		@org.hibernate.annotations.NamedQuery(name = "validateByNaturalAccount", query = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId=? AND c.accountByCostcenterAccountId= ? "
				+ " AND c.accountByNaturalAccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "validateByAnalysisAccount", query = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId=? AND c.accountByCostcenterAccountId= ? "
				+ " AND c.accountByNaturalAccountId=? AND c.accountByAnalysisAccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "validateByBuffer1Account", query = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId=? AND c.accountByCostcenterAccountId= ? "
				+ " AND c.accountByNaturalAccountId=? AND c.accountByAnalysisAccountId=? "
				+ " AND c.accountByBuffer1AccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "validateByBuffer2Account", query = "SELECT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId=? AND c.accountByCostcenterAccountId= ? "
				+ " AND c.accountByNaturalAccountId=? AND c.accountByAnalysisAccountId=? "
				+ " AND c.accountByBuffer1AccountId=? AND c.accountByBuffer2AccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountById", query = "SELECT c FROM Combination c"
				+ " JOIN c.accountByCompanyAccountId company  "
				+ " JOIN c.accountByCostcenterAccountId cost  "
				+ " JOIN c.accountByNaturalAccountId natural "
				+ " JOIN c.accountByAnalysisAccountId analysis "
				+ " JOIN c.accountByBuffer1AccountId buffer1 "
				+ " JOIN c.accountByBuffer2AccountId buffer2 WHERE company.accountId = ? "
				+ " OR cost.accountId = ? OR natural.accountId = ? "
				+ " OR analysis.accountId= ? OR buffer1.accountId = ? OR buffer2.accountId= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByCompanyAccount", query = "SELECT c FROM Combination c  "
				+ " JOIN c.accountByCompanyAccountId ac"
				+ " WHERE ac.implementation=? "
				+ " AND c.accountByCostcenterAccountId IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationForProduct", query = "SELECT c FROM Combination c  "
				+ " WHERE c.accountByCostcenterAccountId.accountId=? "
				+ " AND c.accountByNaturalAccountId.accountId=? "
				+ " AND accountByAnalysisAccountId.accountId =?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByCostAccount", query = "SELECT ac FROM Combination ac  "
				+ " WHERE ac.accountByCostcenterAccountId.accountId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLastUpdatedAccount", query = "SELECT a FROM Account a  "
				+ " WHERE a.implementation=?"
				+ " AND a.accountId=(SELECT MAX(c.accountId)"
				+ " FROM Account c WHERE c.implementation=?) GROUP BY a.accountId"),

		@org.hibernate.annotations.NamedQuery(name = "getLastUpdatedNatural", query = "SELECT a FROM Account a  "
				+ " WHERE a.implementation=? AND a.segment.segmentId=?"
				+ " AND a.code=(SELECT MAX(CAST(c.code AS int))"
				+ " FROM Account c WHERE c.implementation=? AND c.segment.segmentId=? ) GROUP BY a.code"),

		@org.hibernate.annotations.NamedQuery(name = "getLastUpdatedAnalysis", query = "SELECT DISTINCT a FROM Account a  "
				+ " WHERE a.implementation=? AND a.segment.segmentId=?"
				+ " AND a.code=(SELECT MAX(CAST(c.code AS int))"
				+ " FROM Account c WHERE c.implementation=? AND c.segment.segmentId=?) GROUP BY a.code"),

		@org.hibernate.annotations.NamedQuery(name = "getLastUpdatedCostCenter", query = "SELECT a FROM Account a  "
				+ " WHERE a.implementation=? AND a.segment.segmentId= ?"
				+ " AND a.code=(SELECT MAX(CAST(c.code AS int))"
				+ " FROM Account c WHERE c.implementation=? AND c.segment.segmentId=?) GROUP BY a.code"),

		@org.hibernate.annotations.NamedQuery(name = "getSegmentLastUpdatedAccount", query = "SELECT a FROM Account a  "
				+ " LEFT JOIN FETCH a.segment s"
				+ " WHERE a.implementation=? AND s.segmentId= ?"
				+ " AND a.code=(SELECT MAX(CAST(c.code AS int))"
				+ " FROM Account c WHERE c.implementation=? AND  c.segment.segmentId=?) GROUP BY a.code"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSegments", query = "SELECT DISTINCT(s) FROM Segment s"
				+ " WHERE s.implementation = ?"),	
					
		@org.hibernate.annotations.NamedQuery(name = "getDefaultSegments", query = "SELECT DISTINCT(s) FROM Segment s"
				+ " WHERE s.implementation = ?"
				+ " AND s.segment IS NULL"), 
				
		@org.hibernate.annotations.NamedQuery(name = "getSegmentChildById", query = "SELECT DISTINCT(s) FROM Segment s"
				+ " LEFT JOIN FETCH s.segment ps"
 				+ " WHERE ps.segmentId = ?"),
 				
		@org.hibernate.annotations.NamedQuery(name = "getSegmentChilds", query = "SELECT DISTINCT(s) FROM Segment s"
				+ " LEFT JOIN FETCH s.segments cl"
				+ " LEFT JOIN FETCH s.segment ps"
				+ " WHERE s.segmentId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllSegmentChilds", query = "SELECT DISTINCT(s) FROM Segment s"
				+ " LEFT JOIN FETCH s.segments cl"
				+ " WHERE s.segmentId = ?"),
 				
		@org.hibernate.annotations.NamedQuery(name = "getParentSegment", query = "SELECT DISTINCT(s) FROM Segment s"
 				+ " WHERE s.implementation = ?"
 				+ " AND s.accessId = ?"
 				+ " AND s.segment IS NULL"),
 		
		@org.hibernate.annotations.NamedQuery(name = "getParentSegmentSp", query = "SELECT DISTINCT(s) FROM Segment s"
				+  " LEFT JOIN FETCH s.segments sg"
				+ " WHERE s.implementation = ?"
				+ " AND s.accessId = ?"
				+ " AND s.segment IS NULL"),
			
		@org.hibernate.annotations.NamedQuery(name = "getSegmentParentById", query = "SELECT DISTINCT(s) FROM Segment s"
			 	+ " LEFT JOIN FETCH s.segment sg"
 				+ " WHERE s.segmentId = ?"), 
					
		@org.hibernate.annotations.NamedQuery(name = "getDefaultCodes", query = "SELECT ac FROM Account ac"
				+ " WHERE ac.account LIKE '%Default%' AND ac.implementation = ? AND ac.segment.segmentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCombinationTemplate", query = "SELECT ct FROM CombinationTemplate ct"
				+ " LEFT JOIN FETCH ct.COATemplateByCompanyAccountId cpy"
				+ " LEFT JOIN FETCH ct.COATemplateByCostcenterAccountId cst"
				+ " LEFT JOIN FETCH ct.COATemplateByNaturalAccountId natl"
				+ " LEFT JOIN FETCH ct.COATemplateByAnalysisAccountId anly"
				+ " LEFT JOIN FETCH ct.COATemplateByBuffer1AccountId buf1"
				+ " LEFT JOIN FETCH ct.COATemplateByBuffer2AccountId buf2"
				+ " LEFT JOIN FETCH cpy.segment scy"
				+ " LEFT JOIN FETCH cst.segment scc"
				+ " LEFT JOIN FETCH natl.segment sna"
				+ " LEFT JOIN FETCH anly.segment say"
				+ " LEFT JOIN FETCH buf1.segment sb1"
				+ " LEFT JOIN FETCH buf2.segment sb2"
				+ " ORDER BY natl.code, anly.code,"
				+ " buf1.code, buf2.code,"
				+ " natl.accountType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllTransactions", query = "SELECT Distinct(t) FROM Transaction t"
				+ " LEFT JOIN FETCH t.currency c"
				+ " LEFT JOIN FETCH t.period tp"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH t.transactionDetails transDetail"
				+ " LEFT JOIN FETCH t.category tc"
				+ " LEFT JOIN FETCH t.transactionTemps tmp"
				+ " WHERE t.implementation = ? "
				+ " AND t.isApprove = ?"
				+ " ORDER BY tc.categoryId"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionsWithOutPos", query = "SELECT Distinct(t) FROM Transaction t"
				+ " LEFT JOIN FETCH t.currency c"
				+ " LEFT JOIN FETCH t.period tp"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH t.transactionDetails transDetail"
				+ " LEFT JOIN FETCH t.category tc"
				+ " LEFT JOIN FETCH t.transactionTemps tmp"
				+ " WHERE t.implementation = ? "
				+ " AND t.isApprove = ?"
				+ " AND (t.useCase IS NULL OR t.useCase != ?)"
				+ " ORDER BY tc.categoryId"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionsByCategory", query = "SELECT t FROM Transaction t"
				+ " LEFT JOIN FETCH t.currency c"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH t.period tp"
				+ " LEFT JOIN FETCH t.category tc"
				+ " WHERE tc.categoryId = ? " + " ORDER BY tp.periodId"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionsByPeriod", query = "SELECT t FROM Transaction t"
				+ " LEFT JOIN FETCH t.currency c"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH t.period tp"
				+ " LEFT JOIN FETCH t.category tc"
				+ " WHERE tp.periodId = ? "
				+ " ORDER BY tp.periodId"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionsWithTransactionDetailsByPeriod", query = "SELECT DISTINCT(t) FROM Transaction t"
				+ " LEFT JOIN FETCH t.transactionDetails td"
				+ " LEFT JOIN FETCH td.combination cb"
				+ " LEFT JOIN FETCH cb.accountByCompanyAccountId com"
				+ " LEFT JOIN FETCH cb.accountByCostcenterAccountId ca"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH cb.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH cb.accountByBuffer2AccountId b2"
				+ " LEFT JOIN FETCH t.period tp"
				+ " WHERE tp.periodId = ? "
				+ " ORDER BY tp.periodId"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionTempDetails", query = "SELECT t FROM TransactionTempDetail t"
				+ " WHERE t.transactionTemp.transactionTempId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionTempJournalId", query = "SELECT t FROM TransactionTemp t"
				+ " WHERE t.transaction.transactionId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getTempTransactionById", query = "SELECT DISTINCT t FROM TransactionTemp t"
				+ " JOIN FETCH t.period p"
				+ " JOIN FETCH t.currency cc"
				+ " JOIN FETCH t.category cy"
				+ " LEFT JOIN FETCH cc.currencyPool ccp"
				+ " JOIN FETCH t.transactionTempDetails td"
				+ " JOIN FETCH td.combination cb "
				+ " JOIN FETCH cb.accountByCompanyAccountId company"
				+ " JOIN FETCH cb.accountByCostcenterAccountId ca"
				+ " JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH cb.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH cb.accountByBuffer2AccountId b2"
				+ " LEFT JOIN FETCH t.transaction rt"
				+ " WHERE t.transactionTempId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionTempJournalNo", query = "SELECT DISTINCT t FROM TransactionTemp t"
				+ " LEFT JOIN FETCH t.transactionTempDetails td"
				+ " WHERE t.journalNumber = ?" + " AND t.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllTempTransactions", query = "SELECT t FROM TransactionTemp t"
				+ " LEFT JOIN FETCH t.currency c"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH t.period tp"
				+ " LEFT JOIN FETCH t.category tc"
				+ " WHERE t.implementation = ?"
				+ " AND t.transaction IS NULL"
				+ " ORDER BY tc.categoryId"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJournals", query = "SELECT t FROM Transaction t  "
				+ " LEFT JOIN FETCH t.category c WHERE t.implementation = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getJournalDetailsForJVNumber", query = "SELECT t FROM Transaction t  "
				+ " LEFT JOIN FETCH t.category c WHERE t.implementation = ? order by t.transactionId desc "),

		@org.hibernate.annotations.NamedQuery(name = "getJournalEntries", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td WHERE t.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalEntriesByCalendar", query = "SELECT DISTINCT(t) FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " JOIN FETCH t.period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE t.implementation = ?" + " AND c.calendarId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBigJournalEntriesByCalendar", query = "SELECT DISTINCT(t) FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " JOIN FETCH t.period p"
				+ " JOIN FETCH p.calendar c"
				+ " WHERE t.implementation = ?"
				+ " AND t.transactionId > ?"
				+ " AND c.calendarId = ? ORDER BY t.transactionId ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalDateByEntries", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " WHERE t.implementation = ?"
				+ " AND t.transactionTime >= ?"
				+ " AND t.transactionTime <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalDateByCombinationEntries", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " WHERE td.combination = ?"
				+ " AND t.transactionTime >= ?"
				+ " AND t.transactionTime <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getFilterJournalEntries", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " JOIN FETCH t.category ct"
				+ " JOIN FETCH t.currency cy"
				+ " JOIN FETCH cy.currencyPool cp" + " JOIN FETCH t.period pd"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalEntriesByCombination", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td "
				+ " JOIN FETCH td.combination cb"
				+ " JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId an"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalByCombinationCategory", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td WHERE t.implementation = ?"
				+ " AND td.combination.combinationId = ?"
				+ " AND t.category.categoryId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalEntriesByFromDate", query = "SELECT DISTINCT t FROM Transaction t  "
				+ "  JOIN FETCH t.transactionDetails td WHERE t.implementation = ? AND t.transactionTime >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalEntriesByToDate", query = "SELECT DISTINCT t FROM Transaction t  "
				+ "  JOIN FETCH t.transactionDetails td WHERE t.implementation = ? AND t.transactionTime <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalEntriesByCategory", query = "SELECT DISTINCT t FROM Transaction t  "
				+ "  JOIN FETCH t.transactionDetails td WHERE t.implementation = ? AND t.category.categoryId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransationDetailById", query = "SELECT DISTINCT td FROM TransactionDetail td  "
				+ "  JOIN FETCH td.transaction t WHERE td.transactionDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionById", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " JOIN FETCH t.period pd"
				+ " JOIN FETCH t.currency c"
				+ " JOIN FETCH c.currencyPool cp"
				+ " JOIN FETCH t.category ct"
				+ " JOIN FETCH td.combination cb"
				+ " JOIN FETCH cb.accountByCompanyAccountId company"
				+ " JOIN FETCH cb.accountByCostcenterAccountId cost"
				+ " JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId an"
				+ " LEFT JOIN FETCH cb.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH cb.accountByBuffer2AccountId b2"
				+ " WHERE t.transactionId = ?" + " ORDER BY td.isDebit DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionByRecordIdAndUseCase", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " WHERE t.recordId = ?" + " AND t.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionByImplementationAndUseCase", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " WHERE t.implementation = ?" + " AND t.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionByRecordIdAndUseCaseDetail", query = "SELECT DISTINCT t FROM Transaction t  "
				+ " JOIN FETCH t.transactionDetails td"
				+ " JOIN FETCH td.combination cb"
				+ " JOIN FETCH cb.accountByCompanyAccountId company"
				+ " JOIN FETCH cb.accountByCostcenterAccountId cost"
				+ " JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId an"
				+ " LEFT JOIN FETCH cb.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH cb.accountByBuffer2AccountId b2"
				+ " WHERE t.recordId = ?" + " AND t.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getFilterCombinations", query = "SELECT DISTINCT c FROM Combination c "
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company LEFT JOIN FETCH c.accountByCostcenterAccountId cost "
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1 LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " WHERE company.implementation=? AND natural.accountType = ?"
				+ " AND c.accountByAnalysisAccountId IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByAnalysis", query = " SELECT DISTINCT c FROM Combination c"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE analysis.accountId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionCombination", query = " SELECT DISTINCT c FROM Combination c"
				+ " WHERE c.accountByNaturalAccountId IS NOT NULL"
				+ " AND c.accountByCompanyAccountId.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByNaturalAccount", query = " SELECT DISTINCT c FROM Combination c"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " WHERE natural.accountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByNaturalAccountId", query = " SELECT DISTINCT(c) FROM Combination c"
				+ " JOIN FETCH c.accountByNaturalAccountId nrl"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId ays"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " WHERE nrl.accountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByCostCenterNaturalAccountId", query = " SELECT DISTINCT(c) FROM Combination c"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId nrl"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId ays"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " WHERE cost.accountId=?" + " AND nrl.accountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getFilterCombination", query = "SELECT Distinct(c) FROM Combination c"
				+ " JOIN FETCH c.accountByNaturalAccountId natural WHERE natural.implementation=?"
				+ " AND natural.accountType = ? AND c.accountByNaturalAccountId.accountId=natural.accountId"),

		@org.hibernate.annotations.NamedQuery(name = "getDefaultCurrency", query = "SELECT Distinct(c) FROM Currency c  "
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH cp.country cty"
				+ " WHERE c.implementation = ? AND c.status = true AND c.defaultCurrency = true"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationChildDetails", query = "SELECT DISTINCT(c) FROM Combination c  "
 				+ " LEFT JOIN FETCH c.combination cb" 
 				+ " LEFT JOIN FETCH c.combinations cbs"
 				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH company.segment scy"
				+ " LEFT JOIN FETCH cost.segment scc"
				+ " LEFT JOIN FETCH natural.segment sna"
				+ " LEFT JOIN FETCH analysis.segment say"
				+ " LEFT JOIN FETCH buffer1.segment sb1"
				+ " LEFT JOIN FETCH buffer2.segment sb2"
 				+ " WHERE cb.combinationId = ?"
 				+ " AND c.isApprove = ?"),
 				
		@org.hibernate.annotations.NamedQuery(name = "getCombinationChildDetailWithAccount", query = "SELECT DISTINCT(c) FROM Combination c  "
				+ " LEFT JOIN FETCH c.combination cb" 
				+ " LEFT JOIN FETCH c.combinations cbs"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH company.segment scy"
				+ " LEFT JOIN FETCH cost.segment scc"
				+ " LEFT JOIN FETCH natural.segment sna"
				+ " LEFT JOIN FETCH analysis.segment say"
				+ " LEFT JOIN FETCH buffer1.segment sb1"
				+ " LEFT JOIN FETCH buffer2.segment sb2"
				+ " WHERE cb.combinationId = ?"
				+ " AND c.isApprove = ?"), 
 				
		@org.hibernate.annotations.NamedQuery(name = "getCombinationAccountsDetail", query = "SELECT DISTINCT(c) FROM Combination c  "
 				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH c.combination cb"
  				+ " WHERE c.combinationId = ?"
				+ " AND c.isApprove = ?"),	
				
		@org.hibernate.annotations.NamedQuery(name = "getCombinationWithChildAccountsDetail", query = "SELECT DISTINCT(c) FROM Combination c  "
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH c.combination cb"
				+ " LEFT JOIN FETCH c.combinations cbs"
				+ " WHERE c.combinationId = ?"
				+ " AND c.isApprove = ?"),		
		
		@org.hibernate.annotations.NamedQuery(name = "getCombinationLedgerWithChild", query = "SELECT DISTINCT(c) FROM Combination c" 
				+ " LEFT JOIN FETCH c.combinations cb"
				+ " LEFT JOIN FETCH c.ledgers ld"
				+ " WHERE c.combinationId = ?"
				+ " AND c.isApprove = ?"),		
					
		@org.hibernate.annotations.NamedQuery(name = "getAllParentCombination", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH company.segment scy"
				+ " LEFT JOIN FETCH cost.segment scc"
				+ " LEFT JOIN FETCH natural.segment sna"
				+ " LEFT JOIN FETCH analysis.segment say"
				+ " LEFT JOIN FETCH buffer1.segment sb1"
				+ " LEFT JOIN FETCH buffer2.segment sb2"
				+ " WHERE (company.implementation=? OR cost.implementation=? OR natural.implementation=? OR analysis.implementation=?)"
				+ " AND c.isApprove = ?"
				+ " AND c.combination IS NULL"
				+ " AND (company.isSystem IS NULL OR company.isSystem = 0)"
				+ " AND (cost.isSystem IS NULL OR cost.isSystem = 0)"
				+ " AND (natural.isSystem IS NULL OR natural.isSystem = 0)"
				+ " AND (analysis.isSystem IS NULL OR analysis.isSystem = 0)"
				+ " ORDER BY cost.accountId, natural.code, analysis.code,"
				+ " buffer1.code, buffer2.code," 
				+ " natural.accountType ASC"),
 					
		@org.hibernate.annotations.NamedQuery(name = "getAllCodeCombination", query = "SELECT Distinct(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH company.segment scy"
				+ " LEFT JOIN FETCH cost.segment scc"
				+ " LEFT JOIN FETCH natural.segment sna"
				+ " LEFT JOIN FETCH analysis.segment say"
				+ " LEFT JOIN FETCH buffer1.segment sb1"
				+ " LEFT JOIN FETCH buffer2.segment sb2"
				+ " WHERE (company.implementation=? OR cost.implementation=? OR natural.implementation=? OR analysis.implementation=?)"
				+ " AND c.isApprove = ?"
				+ " AND (company.isSystem IS NULL OR company.isSystem = 0)"
				+ " AND (cost.isSystem IS NULL OR cost.isSystem = 0)"
				+ " AND (natural.isSystem IS NULL OR natural.isSystem = 0)"
				+ " AND (analysis.isSystem IS NULL OR analysis.isSystem = 0)"
				+ " ORDER BY cost.accountId, natural.code, analysis.code,"
				+ " buffer1.code, buffer2.code," + " natural.accountType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCodeCombinations", query = "SELECT Distinct(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH company.segment scy"
				+ " LEFT JOIN FETCH cost.segment scc"
				+ " LEFT JOIN FETCH natural.segment sna"
				+ " LEFT JOIN FETCH analysis.segment say"
				+ " LEFT JOIN FETCH buffer1.segment sb1"
				+ " LEFT JOIN FETCH buffer2.segment sb2"
				+ " WHERE (company.implementation=? OR cost.implementation=? OR natural.implementation=? OR analysis.implementation=?)"
				+ " AND (company.isSystem IS NULL OR company.isSystem = 0)"
				+ " AND (cost.isSystem IS NULL OR cost.isSystem = 0)"
				+ " AND (natural.isSystem IS NULL OR natural.isSystem = 0)"
				+ " AND (analysis.isSystem IS NULL OR analysis.isSystem = 0)"
				+ " ORDER BY cost.accountId, natural.code, analysis.code,"
				+ " buffer1.code, buffer2.code," + " natural.accountType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getCostCenterCombination", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH company.segment scy"
				+ " LEFT JOIN FETCH cost.segment scc"
				+ " WHERE company.implementation=?"
				+ " AND (company.isSystem IS NULL OR company.isSystem = 0)"
				+ " AND (cost.isSystem IS NULL OR cost.isSystem = 0)"
				+ " ORDER BY cost.accountId"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature l"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH p.combinationByInventoryAccount ivacc"
				+ " LEFT JOIN FETCH p.combinationByExpenseAccount expacc"
				+ " WHERE p.implementation=?"
				+ " AND p.isApprove = ?"
				+ " ORDER BY p DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getOnlyProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " WHERE p.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductsByCode", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH p.combinationByInventoryAccount ivacc"
				+ " LEFT JOIN FETCH p.combinationByExpenseAccount expacc"
				+ " LEFT JOIN FETCH ivacc.accountByAnalysisAccountId acc"
				+ " WHERE p.implementation=?" + " AND p.code LIKE '%RM%'"),

		@org.hibernate.annotations.NamedQuery(name = "getProductsWithoutImpl", query = "SELECT p FROM Product p "
				+ " LEFT JOIN FETCH p.productUnit pu"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH p.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " WHERE p.implementation=?"
				+ " AND p.itemType = ? "
				+ " AND p.status=1"
				+ " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveInventoryProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricing ppr"
				+ " WHERE p.implementation=?"
				+ " AND p.itemType = ? "
				+ " AND p.status=1"
				+ " AND ppr.startDate <= ?"
				+ " AND ppr.endDate >= ?" + " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePOSInventoryProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricing ppr"
				+ " WHERE p.implementation=?"
				+ " AND p.itemType = ? "
				+ " AND p.status=1"
				/*+ " AND p.specialPos = 1"*/
				+ " AND ppr.startDate <= ?"
				+ " AND ppr.endDate >= ?"
				+ " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPOSProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " WHERE p.implementation=?"
				+ " AND p.itemType = 'I' "
				+ " AND p.status=1"
				+ " AND p.specialPos = 1"
				+ " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePosSpecialProduct", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.productCategory pc"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricing ppr"
				+ " WHERE p.implementation=?"
				+ " AND p.itemType = 'I' "
				+ " AND p.status=1"
				+ " AND p.specialPos = 1"
				+ " AND ppr.startDate <= ?"
				+ " AND ppr.endDate >= ?"
				+ " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProductByCategory", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricing ppr"
				+ " WHERE p.implementation=?"
				+ " AND pc.productCategoryId = ?"
				+ " AND p.itemType = ? "
				+ " AND p.status=1"
				+ " AND ppr.startDate <= ?"
				+ " AND ppr.endDate >= ?"
				+ " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePOSProductByCategory", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricing ppr"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs calc"
				+ " WHERE p.implementation=?"
				+ " AND pc.productCategoryId = ?"
				+ " AND p.itemType = ? "
				+ " AND p.status=1"
				+ " AND ppr.startDate <= ?"
				+ " AND ppr.endDate >= ?"
				+ " AND (calc IS NULL OR calc.store.storeId IS NULL OR calc.store.storeId =?)"
				+ " ORDER BY p.itemType ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductForStockCheckUp", query = "SELECT DISTINCT(p) FROM Product p "
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH pc.productCategory parentpc"
				+ " LEFT JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH p.shelf shelf"
				+ " LEFT JOIN FETCH shelf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle aisle"
				+ " LEFT JOIN FETCH aisle.store store"
				+ " WHERE p.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getReorderProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH pc.productCategory parentpc"
				+ " LEFT JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH p.shelf shelf"
				+ " LEFT JOIN FETCH shelf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle aisle"
				+ " LEFT JOIN FETCH aisle.store store"
				+ " WHERE p.implementation=?"
				+ " AND p.reOrderQuantity IS NOT NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProduct", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " WHERE p.implementation=? AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "showRequisitionProductDetails", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " WHERE p.implementation=?"
				+ " AND p.isApprove = ?"
				+ " AND p.itemSubType != ? AND p.itemSubType != ?"
				+ " AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getShortActiveProduct", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u "
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " WHERE p.implementation=? AND p.status=1"
				+ " AND p.itemSubType != ? AND p.itemSubType != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveSalesProduct", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE p.implementation=? AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePurchaseProduct", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " WHERE p.implementation=?"
				+ " AND p.itemSubType <> ?"
				+ " AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseProductByType", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature ld"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " WHERE p.implementation=?"
				+ " AND p.itemType = ?"
				+ " AND p.itemSubType <> ?" + " AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getSimpleProductById", query = "SELECT DISTINCT(p) FROM Product p"
				+ " JOIN FETCH p.lookupDetailByProductUnit pu"
				+ " WHERE p.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductById", query = "SELECT DISTINCT(p) FROM Product p"
				+ " LEFT JOIN FETCH p.combinationByInventoryAccount inv"
				+ " LEFT JOIN FETCH p.combinationByExpenseAccount exp"
				+ " LEFT JOIN FETCH inv.accountByAnalysisAccountId invacc"
				+ " LEFT JOIN FETCH exp.accountByAnalysisAccountId expacc"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature inature"
				+ " LEFT JOIN FETCH p.productCategory category"
				+ " LEFT JOIN FETCH p.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle sec"
				+ " LEFT JOIN FETCH sec.store str"
				+ " JOIN FETCH p.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH p.stocks sks" + " WHERE p.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductCategory", query = "SELECT p FROM ProductCategory p"
				+ " LEFT JOIN FETCH p.productCategory parent"
				+ " WHERE p.implementation = ?" + " AND p.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductSubCategory", query = "SELECT p FROM ProductCategory p"
				+ " WHERE p.implementation = ?"
				+ " AND p.productCategory IS NOT NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getProductSubCategoryByCategory", query = "SELECT DISTINCT(p) FROM ProductCategory p"
				+ " JOIN FETCH p.productCategory pc"
				+ " JOIN FETCH p.products prd"
				+ " WHERE pc.productCategoryId=? AND prd.itemType='I' AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductSubCategoryByProduct", query = "SELECT Distinct(p) FROM ProductCategory p"
				+ " JOIN FETCH p.products prd"
				+ " WHERE prd.productId=? AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductParentCategory", query = "SELECT Distinct(p) FROM ProductCategory p"
				+ " JOIN FETCH p.productCategories pc"
				+ " JOIN FETCH pc.products prd"
				+ " WHERE p.implementation = ?"
				+ " AND p.productCategory IS NULL AND prd.itemType='I' AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPOSProductCategories", query = "SELECT Distinct(p) FROM ProductCategory p"
				+ " JOIN FETCH p.productCategories pc"
				+ " JOIN FETCH pc.products prd"
				+ " WHERE p.implementation = ?"
				+ " AND p.specialPos = 1"
				+ " AND p.productCategory IS NULL AND prd.itemType='I' AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductCategoryDetails", query = "SELECT p FROM ProductCategory p"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH p.productCategories pcs"
				+ " LEFT JOIN FETCH p.products pr"
				+ " WHERE p.productCategoryId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductPricing", query = "SELECT p FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr" + " WHERE p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductPricingDetail", query = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " JOIN FETCH p.product prd"
				+ " LEFT JOIN FETCH p.store str"
				+ " JOIN FETCH p.productPricing pr"
				+ " LEFT JOIN FETCH p.productPricingCalcs prc"
				+ " JOIN FETCH pr.currency cr"
				+ " WHERE pr.implementation = ?"
				+ " AND pr.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductPricingCalc", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.comboType != 1 AND p.isDefault = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricing", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.product prd"
				+ " LEFT JOIN FETCH ppd.store str"
				+ " WHERE p.productPricingId = ?" + " AND ppd.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingAll", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE p.productPricingId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingCalcByDetailId", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " LEFT JOIN FETCH p.store str"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH p.lookupDetail ppc"
				+ " WHERE p.productPricingDetail.productPricingDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProductPricingCalcByDetailId", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " LEFT JOIN FETCH p.store str"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH p.lookupDetail ppc"
				+ " WHERE p.productPricingDetail.productPricingDetailId = ?"
				+ " AND p.status = 1 AND p.comboType = 0"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingDetailByPricingId", query = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " LEFT JOIN FETCH p.productPricingCalcs calc"
				+ " WHERE p.productPricing.productPricingId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingDetailByPricingDetailId", query = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " LEFT JOIN FETCH p.productPricingCalcs calc"
				+ " WHERE p.productPricingDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingWithCustomer", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricingCalcs ppc"
				+ " LEFT JOIN FETCH ppc.lookupDetail priceType"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"
				+ " AND ppc.store.storeId = ?"
				+ " AND ppc.customer.customerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingDetailByDate", query = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " JOIN FETCH p.productPricing pr"
				+ " JOIN FETCH p.product prd"
				+ " WHERE pr.implementation = ?"
				+ " AND pr.startDate <= ?" + " OR pr.endDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingWithoutCustomer", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " LEFT JOIN FETCH ppc.lookupDetail priceType"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"
				+ " AND ppd.store IS NULL"
				+ " AND ppc.store.storeId = ?" + " AND ppc.customer IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingWithoutCustomerAndStore", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " LEFT JOIN FETCH ppc.lookupDetail priceType"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"
				+ " AND ppd.store IS NULL"
				+ " AND ppc.customer IS NULL"
				+ " AND ppc.store IS NULL"
				+ " AND (ppc.comboType IS NULL OR ppc.comboType = 0)"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingWithProductAndDate", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingDetailByProduct", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"
				+ " AND ppd.store.storeId IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getPricingDetailWithProductStore", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"
				+ " AND ppd.store.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getComboProductPricing", query = "SELECT DISTINCT(p) FROM ProductPricing p "
				+ " JOIN FETCH p.currency cr"
				+ " JOIN FETCH p.productPricingDetails ppd"
				+ " JOIN FETCH ppd.productPricingCalcs ppc"
				+ " LEFT JOIN FETCH ppc.lookupDetail priceType"
				+ " WHERE p.startDate <= ?"
				+ " AND p.endDate >= ?"
				+ " AND ppd.product.productId = ?"
				+ " AND ppc.customer IS NULL" + " AND ppc.comboType = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingCalcWithoutCustomer", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricing.startDate <= ?"
				+ " AND p.productPricingDetail.productPricing.endDate >= ?"
				+ " AND p.productPricingDetail.product.productId = ?"
				+ " AND p.store.storeId = ?"
				+ " AND p.lookupDetail.lookupDetailId =?"
				+ " AND p.customer IS NULL"
				+ " AND p.comboType != 1"
				+ " AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingCalcWithoutCustomerAndStore", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricing.startDate <= ?"
				+ " AND p.productPricingDetail.productPricing.endDate >= ?"
				+ " AND p.productPricingDetail.product.productId = ?"
				+ " AND p.lookupDetail.lookupDetailId =?"
				+ " AND p.customer IS NULL"
				+ " AND p.comboType != 1"
				+ " AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingCalcWithCustomer", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricing.startDate <= ?"
				+ " AND p.productPricingDetail.productPricing.endDate >= ?"
				+ " AND p.productPricingDetail.product.productId = ?"
				+ " AND p.customer.customerId = ?"
				+ " AND p.store.storeId = ?"
				+ " AND p.lookupDetail.lookupDetailId =?"
				+ " AND p.customer IS NULL"
				+ " AND p.comboType != 1"
				+ " AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingCalcWithCustomerWithOutStore", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricing.startDate <= ?"
				+ " AND p.productPricingDetail.productPricing.endDate >= ?"
				+ " AND p.productPricingDetail.product.productId = ?"
				+ " AND p.customer.customerId = ?"
				+ " AND p.lookupDetail.lookupDetailId =?"
				+ " AND p.customer IS NULL"
				+ " AND p.comboType != 1"
				+ " AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerSalesPrice", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricing.startDate <= ?"
				+ " AND p.productPricingDetail.productPricing.endDate >= ?"
				+ " AND p.productPricingDetail.product.productId = ?"
				+ " AND p.customer.customerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreSalesPrice", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricing.startDate <= ?"
				+ " AND p.productPricingDetail.productPricing.endDate >= ?"
				+ " AND p.productPricingDetail.product.productId = ?"
				+ " AND p.store.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingCalcComboType", query = "SELECT DISTINCT(p) FROM ProductPricingCalc p "
				+ " WHERE p.productPricingDetail.productPricingDetailId = ?"
				+ " AND p.comboType = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingDetail", query = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " JOIN FETCH p.productPricingCalcs pc"
				+ " WHERE p.productPricing.implementation = ?"
				+ " AND p.productPricing.startDate <= ?"
				+ " AND p.productPricing.endDate >= ?"
				+ " AND p.product.productId = ?"
				+ " AND pc.store.storeId = ?"
				+ " AND pc.comboType != 1" + " AND pc.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPricingDetailWithOutStore", query = "SELECT DISTINCT(p) FROM ProductPricingDetail p "
				+ " WHERE p.productPricing.implementation = ?"
				+ " AND p.productPricing.startDate <= ?"
				+ " AND p.productPricing.endDate >= ?"
				+ " AND p.product.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllRequisitions", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " JOIN FETCH r.cmpDeptLoc cdl"
				+ " JOIN FETCH cdl.company cpy"
				+ " JOIN FETCH cdl.department dpt"
				+ " JOIN FETCH cdl.location lc"
				+ " JOIN FETCH r.person pr"
				+ " LEFT JOIN FETCH r.purchases purs"
				+ " LEFT JOIN FETCH purs.receiveDetails recDet"
				+ " LEFT JOIN FETCH recDet.receive rece"
				+ " WHERE r.implementation = ? AND r.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllIssueRequisitions", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " JOIN FETCH r.cmpDeptLoc cdl"
				+ " JOIN FETCH cdl.company cpy"
				+ " JOIN FETCH cdl.department dpt"
				+ " JOIN FETCH cdl.location lc"
				+ " JOIN FETCH r.person pr"
				+ " WHERE r.implementation = ?"
				+ " AND r.status != ? AND r.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllExpenseIssuedRequisitions", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " JOIN FETCH r.cmpDeptLoc cdl"
				+ " JOIN FETCH cdl.company cpy"
				+ " JOIN FETCH cdl.department dpt"
				+ " JOIN FETCH cdl.location lc"
				+ " JOIN FETCH r.person pr"
				+ " JOIN FETCH r.requisitionDetails rd"
				+ " JOIN FETCH rd.product rprd"
				+ " LEFT JOIN FETCH r.quotations rfq"
				+ " LEFT JOIN FETCH rd.quotationDetails rfqd"
				+ " LEFT JOIN FETCH r.purchases lpo"
				+ " LEFT JOIN FETCH rd.purchaseDetails lpod"
				+ " LEFT JOIN FETCH r.issueRequistions ir"
				+ " LEFT JOIN FETCH rd.issueRequistionDetails ird"
				+ " WHERE r.implementation = ?"
				+ " AND r.status != ? AND r.isApprove=?"
				+ " AND rprd.itemType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllQuotationPurchasedIssuedRequisitions", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " JOIN FETCH r.cmpDeptLoc cdl"
				+ " JOIN FETCH cdl.company cpy"
				+ " JOIN FETCH cdl.department dpt"
				+ " JOIN FETCH cdl.location lc"
				+ " JOIN FETCH r.person pr"
				+ " JOIN FETCH r.requisitionDetails rd"
				+ " LEFT JOIN FETCH r.quotations rfq"
				+ " LEFT JOIN FETCH rd.quotationDetails rfqd"
				+ " LEFT JOIN FETCH r.purchases lpo"
				+ " LEFT JOIN FETCH rd.purchaseDetails lpod"
				+ " LEFT JOIN FETCH rfqd.quotation ckquot"
				+ " LEFT JOIN FETCH ckquot.purchases prrfq"
				+ " LEFT JOIN FETCH prrfq.purchaseDetails prdtrfq"
				+ " LEFT JOIN FETCH r.issueRequistions ir"
				+ " LEFT JOIN FETCH rd.issueRequistionDetails ird"
				+ " WHERE r.implementation = ?"
				+ " AND r.status != ? AND r.isApprove=?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllPurchasedIssuedRequisitions", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " JOIN FETCH r.cmpDeptLoc cdl"
				+ " JOIN FETCH cdl.company cpy"
				+ " JOIN FETCH cdl.department dpt"
				+ " JOIN FETCH cdl.location lc"
				+ " JOIN FETCH r.person pr"
				+ " JOIN FETCH r.requisitionDetails rd"
				+ " LEFT JOIN FETCH r.quotations rfq"
				+ " LEFT JOIN FETCH rd.quotationDetails rfqd"
				+ " LEFT JOIN FETCH r.purchases lpo"
				+ " LEFT JOIN FETCH rd.purchaseDetails lpod"
				+ " LEFT JOIN FETCH r.issueRequistions ir"
				+ " LEFT JOIN FETCH rd.issueRequistionDetails ird"
				+ " WHERE r.implementation = ?"
				+ " AND r.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequisitionById", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " LEFT JOIN FETCH r.cmpDeptLoc cdl"
				+ " LEFT JOIN FETCH cdl.company cpy"
				+ " LEFT JOIN FETCH cdl.department dpt"
				+ " LEFT JOIN FETCH cdl.location lc"
				+ " LEFT JOIN FETCH r.person pr"
				+ " LEFT JOIN FETCH r.requisitionDetails rd"
				+ " LEFT JOIN FETCH rd.product prd" 
				+ " WHERE r.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequisitionDetailsById", query = "SELECT DISTINCT(r) FROM RequisitionDetail r "
				+ " JOIN FETCH r.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit ldd"
				+ " WHERE r.requisition.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequisitionDetailIssuance", query = "SELECT DISTINCT(r) FROM RequisitionDetail r "
				+ " JOIN FETCH r.product prd"
				+ " LEFT JOIN FETCH prd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rk"
				+ " LEFT JOIN FETCH rk.aisle al"
				+ " LEFT JOIN FETCH al.store str"
				+ " JOIN FETCH prd.lookupDetailByProductUnit ldp"
				+ " LEFT JOIN FETCH r.quotationDetails rfq"
				+ " LEFT JOIN FETCH r.purchaseDetails lpo"
				+ " LEFT JOIN FETCH r.issueRequistionDetails ird"
				+ " WHERE r.requisition.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequisitionDetailByRequistionId", query = "SELECT DISTINCT(r) FROM RequisitionDetail r "
				+ " JOIN FETCH r.product prd"
				+ " LEFT JOIN FETCH prd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rk"
				+ " LEFT JOIN FETCH rk.aisle al"
				+ " LEFT JOIN FETCH al.store str"
				+ " LEFT JOIN FETCH prd.stocks stock"
				+ " JOIN FETCH prd.lookupDetailByProductUnit ldp"
				+ " LEFT JOIN FETCH r.quotationDetails rfq"
				+ " LEFT JOIN FETCH r.purchaseDetails lpo"
				+ " LEFT JOIN FETCH r.issueRequistionDetails ird"
				+ " LEFT JOIN FETCH r.requisition req"
				+ " LEFT JOIN FETCH req.purchases purchase"
				+ " LEFT JOIN FETCH purchase.purchaseDetails purcDetails"
				+ " LEFT JOIN FETCH prd.purchaseDetails pDet"
				+ " LEFT JOIN FETCH pDet.product reqProd"
				+ " WHERE req.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequisitionCycle", query = "SELECT DISTINCT(r) FROM Requisition r "
				+ " JOIN FETCH r.requisitionDetails rd"
				+ " JOIN FETCH rd.product prd"
				+ " LEFT JOIN FETCH r.quotations rfq"
				+ " LEFT JOIN FETCH rfq.quotationDetails rfqdts"
				+ " LEFT JOIN FETCH rfqdts.product rfqdtsprd"
				+ " LEFT JOIN FETCH rfq.purchases rfqlpo"
				+ " LEFT JOIN FETCH rfqlpo.supplier sp1"
				+ " LEFT JOIN FETCH sp1.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.company cpycdl1"
				+ " LEFT JOIN FETCH sp1.company cpy1"
				+ " LEFT JOIN FETCH sp1.person sppr1"
				+ " LEFT JOIN FETCH rfqlpo.purchaseDetails rfqlpodts"
				+ " LEFT JOIN FETCH rfqlpo.goodsReturnDetails grd1"
				+ " LEFT JOIN FETCH grd1.product prd1"
				+ " LEFT JOIN FETCH rfqlpodts.product rfqlpodtsprd"
				+ " LEFT JOIN FETCH rfqlpo.receiveDetails rfqrcvdts"
				+ " LEFT JOIN FETCH rfqrcvdts.product rfqrcvdtsprd"
				+ " LEFT JOIN FETCH rfqrcvdts.receive rfqrcv"
				+ " LEFT JOIN FETCH rfqrcv.goodsReturns rfqgrd"
				+ " LEFT JOIN FETCH rfqgrd.goodsReturnDetails rfqgrdDt"
				+ " LEFT JOIN FETCH rfqgrdDt.product rfqgrdDtprd"
				+ " LEFT JOIN FETCH r.purchases lpo"
				+ " LEFT JOIN FETCH lpo.supplier sp2"
				+ " LEFT JOIN FETCH sp2.cmpDeptLocation cdl2"
				+ " LEFT JOIN FETCH cdl2.company cpycdl2"
				+ " LEFT JOIN FETCH sp2.company cpy2"
				+ " LEFT JOIN FETCH sp2.person sppr2"
				+ " LEFT JOIN FETCH lpo.purchaseDetails lpodts"
				+ " LEFT JOIN FETCH lpo.goodsReturnDetails lpogrddts"
				+ " LEFT JOIN FETCH lpogrddts.product lpogrddtprd"
				+ " LEFT JOIN FETCH lpodts.product lpodtsprd"
				+ " LEFT JOIN FETCH lpo.receiveDetails rcvdts"
				+ " LEFT JOIN FETCH rcvdts.product rcvdtsprd"
				+ " LEFT JOIN FETCH rcvdts.receive rcv"
				+ " LEFT JOIN FETCH rcv.goodsReturns grd"
				+ " LEFT JOIN FETCH grd.goodsReturnDetails grdDt"
				+ " LEFT JOIN FETCH grdDt.product grdDtprd"
				+ " LEFT JOIN FETCH r.issueRequistions ird"
				+ " LEFT JOIN FETCH ird.person irdpr"
				+ " LEFT JOIN FETCH ird.issueRequistionDetails irddts"
				+ " LEFT JOIN FETCH irddts.product irddtsprd"
				+ " LEFT JOIN FETCH irddts.issueReturnDetails irtdts"
				+ " LEFT JOIN FETCH irtdts.issueReturn rt1"
				+ " LEFT JOIN FETCH irtdts.product irtdtsprd1"
				+ " WHERE r.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequisitionDetailQuotationPurchaseIssue", query = "SELECT DISTINCT(r) FROM RequisitionDetail r "
				+ " JOIN FETCH r.product prd"
				+ " LEFT JOIN FETCH prd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rk"
				+ " LEFT JOIN FETCH rk.aisle al"
				+ " LEFT JOIN FETCH al.store str"
				+ " JOIN FETCH prd.lookupDetailByProductUnit ldp"
				+ " LEFT JOIN FETCH r.quotationDetails rfq"
				+ " LEFT JOIN FETCH rfq.product rfqprd"
				+ " LEFT JOIN FETCH r.purchaseDetails lpo"
				+ " LEFT JOIN FETCH rfq.quotation quotes"
				+ " LEFT JOIN FETCH quotes.purchases prquote"
				+ " LEFT JOIN FETCH prquote.purchaseDetails prdtlpo"
				+ " LEFT JOIN FETCH prdtlpo.product prdtlpoprd"
				+ " LEFT JOIN FETCH r.issueRequistionDetails ird"
				+ " WHERE r.requisition.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllIssueRequistions", query = "SELECT DISTINCT(ir) FROM IssueRequistion ir "
				+ " JOIN FETCH ir.issueRequistionDetails ird"
				+ " LEFT JOIN FETCH ird.issueReturnDetails irnd"
				+ " JOIN FETCH ir.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.location lc"
				+ " JOIN FETCH ir.person pr"
				+ " LEFT JOIN FETCH ir.customer cus"
				+ " LEFT JOIN FETCH cus.company cusComp"
				+ " LEFT JOIN FETCH cus.personByPersonId cusPer"
				+ " WHERE ir.implementation = ?" + " AND ir.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueRequistionById", query = "SELECT DISTINCT(ir) FROM IssueRequistion ir "
				+ " LEFT JOIN FETCH ir.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location lc"
				+ " LEFT JOIN FETCH ir.person pr"
				+ " LEFT JOIN FETCH ir.lookupDetail ld"
				+ " JOIN FETCH ir.issueRequistionDetails ird"
				+ " LEFT JOIN FETCH ird.lookupDetail ldsec"
				+ " JOIN FETCH ird.product pr"
				+ " JOIN FETCH pr.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH ird.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH ir.requisition rqn"
				+ " LEFT JOIN FETCH ir.customer cus"
				+ " LEFT JOIN FETCH cus.company cmp"
				+ " LEFT JOIN FETCH cus.personByPersonId pr"
				+ " LEFT JOIN FETCH ird.productPackageDetail packdt"
				+ " LEFT JOIN FETCH packdt.lookupDetail unitlk"
				+ " WHERE ir.issueRequistionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueRequistionAndDetails", query = "SELECT DISTINCT(ir) FROM IssueRequistion ir "
				+ " JOIN FETCH ir.issueRequistionDetails ird"
				+ " LEFT JOIN FETCH ird.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH ird.issueReturnDetails returns"
				+ " WHERE ir.issueRequistionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueRequistionByDetailId", query = "SELECT DISTINCT(ir) FROM IssueRequistion ir "
				+ " JOIN FETCH ir.issueRequistionDetails ird"
				+ " JOIN FETCH ird.product prd"
				+ " WHERE ird.issueRequistionDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueRequistionDetailById", query = "SELECT DISTINCT(ird) FROM IssueRequistionDetail ird "
				+ " JOIN FETCH ird.product prd"
				+ " LEFT JOIN FETCH ird.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " WHERE ird.issueRequistionDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueRequistionDetail", query = "SELECT ird FROM IssueRequistionDetail ird "
				+ " WHERE ird.issueRequistion.issueRequistionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueRequistionsByRequistionId", query = "SELECT DISTINCT(ir) FROM IssueRequistion ir "
				+ " LEFT JOIN FETCH ir.issueRequistionDetails ird"
				+ " LEFT JOIN FETCH ird.requisitionDetail rqd"
				+ " WHERE ir.requisition.requisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getLocationIssueRequistion", query = "SELECT  DISTINCT(ird) FROM IssueRequistionDetail ird "
				+ " JOIN FETCH ird.issueRequistion irq"
				+ " JOIN FETCH irq.cmpDeptLocation lc"
				+ " JOIN FETCH ird.product prd"
				+ " LEFT JOIN FETCH ird.issueReturnDetails ir"
				+ " WHERE lc.cmpDeptLocId = ? AND irq.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllIssueReturn", query = "SELECT DISTINCT(ir) FROM IssueReturn ir "
				+ " LEFT JOIN FETCH ir.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location lc"
				+ " LEFT JOIN FETCH ir.store str"
				+ " WHERE ir.implementation = ? AND ir.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueReturns", query = "SELECT DISTINCT(ir) FROM IssueReturn ir "
				+ " JOIN FETCH ir.issueReturnDetails ird"
				+ " WHERE ir.issueReturnId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueReturnDetailByRequistionDetailId", query = "SELECT ird FROM IssueReturnDetail ird "
				+ " WHERE ird.issueRequistionDetail.issueRequistionDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIssueReturnById", query = "SELECT DISTINCT(ir) FROM IssueReturn ir "
				+ " LEFT JOIN FETCH ir.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location lc"
				+ " LEFT JOIN FETCH ir.store str"
				+ " JOIN FETCH ir.issueReturnDetails ird"
				+ " JOIN FETCH ird.product prd"
				+ " LEFT JOIN FETCH ird.issueRequistionDetail irqd"
				+ " LEFT JOIN FETCH irqd.issueRequistion irq"
				+ " LEFT JOIN FETCH ird.materialTransferDetail mtrdt"
				+ " LEFT JOIN FETCH mtrdt.materialTransfer mtr"
				+ " WHERE ir.issueReturnId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllReceiveNotes", query = "SELECT DISTINCT re FROM Receive re "
				+ " JOIN FETCH re.receiveDetails rd"
				+ " LEFT JOIN FETCH rd.product pd "
				+ " WHERE rd.product.productId=pd.productId"
				+ " AND re.implementation=?  "),

		@org.hibernate.annotations.NamedQuery(name = "getReceiptListWithoutImpl", query = "SELECT DISTINCT (r) FROM Receive r"
				+ " JOIN FETCH r.receiveDetails rd"
				+ " JOIN FETCH rd.purchase pr" + " JOIN FETCH pr.supplier sp"),

		@org.hibernate.annotations.NamedQuery(name = "getClosedReceiveNotes", query = "SELECT DISTINCT (r) FROM Receive r"
				+ " JOIN FETCH r.receiveDetails rd"
				+ " LEFT JOIN FETCH rd.goodsReturnDetails grd"
				+ " WHERE r.implementation = ? AND r.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getClosedReceiveDetails", query = "SELECT DISTINCT (rd) FROM ReceiveDetail rd"
				+ " JOIN FETCH rd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit unitname"
				+ " LEFT JOIN FETCH rd.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH rd.goodsReturnDetails grd"
				+ " WHERE rd.receive.receiveId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiveDetailByPurchaseProduct", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
				+ " JOIN FETCH rd.shelf slf"
				+ " JOIN FETCH slf.aisle ais"
				+ " JOIN FETCH ais.store str"
				+ " WHERE rd.purchase.purchaseId =?"
				+ " AND rd.product.productId =?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiveDetails", query = "SELECT DISTINCT (r) FROM Receive r"
				+ " JOIN FETCH r.receiveDetails rd"
				+ " JOIN FETCH rd.purchase pr"
				+ " JOIN FETCH r.supplier sp"
				+ " JOIN FETCH rd.shelf slf"
				+ " JOIN FETCH slf.aisle ais"
				+ " JOIN FETCH ais.store str"
				+ " LEFT JOIN FETCH sp.person ps"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " JOIN FETCH rd.product pd"
				+ " LEFT JOIN FETCH pr.purchaseDetails prpd"
				+ " LEFT JOIN FETCH prpd.product detailPrd"
				+ " JOIN FETCH pd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH pr.requisition req"
				+ " LEFT JOIN FETCH rd.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH packdetail.lookupDetail unitDt"
				+ " WHERE r.receiveId=? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getReceiveDetailsBarcode", query = "SELECT DISTINCT (r) FROM Receive r"
				+ " JOIN FETCH r.receiveDetails rd"
				+ " JOIN FETCH rd.product pd" + " WHERE r.receiveId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getStockDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " WHERE st.product.productId = ?"
				+ " AND st.store.storeId = ?" + " ORDER BY st.stockId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getIssuedStoreStockDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " WHERE st.product.productId = ?"
				+ " ORDER BY st.stockId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getStockByStockId", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product pr" + " WHERE st.stockId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockFullDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " JOIN FETCH st.store str"
				+ " LEFT JOIN FETCH st.shelf slf"
				+ " WHERE prd.productId = ?" + " AND slf.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBatchStockFullDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " JOIN FETCH st.store str"
				+ " LEFT JOIN FETCH st.shelf slf"
				+ " WHERE prd.productId = ?"
				+ " AND slf.shelfId = ?"
				+ " AND (st.batchNumber IS NULL OR st.batchNumber = ?)"
				+ " AND (st.productExpiry IS NULL OR st.productExpiry =?)"),

		@org.hibernate.annotations.NamedQuery(name = "getStockByStoreDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " JOIN FETCH st.store str"
				+ " LEFT JOIN FETCH st.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store slfstr"
				+ " WHERE prd.productId = ?" + " AND str.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductStockFullDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " JOIN FETCH st.store str"
				+ " LEFT JOIN FETCH st.shelf slf"
				+ " WHERE prd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockWithSelfs", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " JOIN FETCH st.store str"
				+ " LEFT JOIN FETCH st.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store slfstr"
				+ " WHERE prd.productId = ?"
				+ " AND str.storeId = ?"
				+ " AND slf.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockByProductId", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd" + " WHERE prd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockFullDetailsByProduct", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " JOIN FETCH st.store str" + " WHERE prd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockReportDetails", query = "SELECT DISTINCT(st) FROM Stock st"
				+ " JOIN FETCH st.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " WHERE st.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductStockDetail", query = "SELECT DISTINCT(sk) FROM Stock sk"
				+ " JOIN FETCH sk.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " JOIN FETCH sk.store st"
				+ " LEFT JOIN FETCH sk.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf parentslf"
				+ " LEFT JOIN FETCH parentslf.aisle ais"
				+ " LEFT JOIN FETCH ais.store selfStore"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd "
				+ " LEFT JOIN FETCH ppd.store ppdstr"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE sk.implementation = ?"
				+ " AND prd.itemType = ?"
				+ " AND sk.quantity > 0"),

		@org.hibernate.annotations.NamedQuery(name = "getCraftServiceProducts", query = "SELECT DISTINCT(sk) FROM Stock sk"
				+ " JOIN FETCH sk.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd "
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE sk.implementation = ?"
				+ " AND(prd.itemSubType = ? OR prd.itemSubType = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getProductStockDetailCommon", query = "SELECT DISTINCT(sk) FROM Stock sk"
				+ " JOIN FETCH sk.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " JOIN FETCH sk.store st"
				+ " LEFT JOIN FETCH sk.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf parentslf"
				+ " LEFT JOIN FETCH parentslf.aisle ais"
				+ " LEFT JOIN FETCH ais.store selfStore"
				+ " LEFT JOIN FETCH st.cmpDeptLocation cmpLoc"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd "
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE sk.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductRequisitionStock", query = "SELECT Distinct(sk) FROM Stock sk"
				+ " JOIN FETCH sk.product prd"
				+ " LEFT JOIN FETCH sk.store st"
				+ " LEFT JOIN FETCH sk.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH prd.productPricingDetails pd"
				+ " LEFT JOIN FETCH pd.productPricingCalcs pcd"
				+ " WHERE prd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockByProduct", query = "SELECT DISTINCT(sk) FROM Stock sk"
				+ " JOIN FETCH sk.product prd" + " WHERE prd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockByStoreId", query = "SELECT Distinct(sk) FROM Stock sk"
				+ " JOIN FETCH sk.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " LEFT JOIN FETCH sk.store st" + " WHERE st.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiveLines", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
				+ " JOIN FETCH rd.product p"
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH rd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rck"
				+ " LEFT JOIN FETCH rck.aisle ale"
				+ " LEFT JOIN FETCH ale.store str"
				+ " WHERE rd.receive.receiveId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiptNotes", query = "SELECT DISTINCT(r) FROM Receive r"
				+ " JOIN FETCH r.receiveDetails rd"
				+ " LEFT JOIN FETCH rd.productPackageDetail packDetail"
				+ " LEFT JOIN FETCH rd.goodsReturnDetails greturn"
				+ " LEFT JOIN FETCH rd.purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pdet"
				+ " LEFT JOIN FETCH pdet.goodsReturnDetails pdgrd"
				+ " LEFT JOIN FETCH p.currency pcc"
				+ " LEFT JOIN FETCH pcc.currencyPool pccp"
				+ " LEFT JOIN FETCH pdet.product prdpur"
				+ " LEFT JOIN FETCH r.supplier sp"
				+ " LEFT JOIN FETCH sp.person ps"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH rd.product prd"
				+ " LEFT JOIN FETCH rd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rck"
				+ " LEFT JOIN FETCH rck.aisle ale"
				+ " LEFT JOIN FETCH ale.store str"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE r.receiveId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiptList", query = "SELECT DISTINCT(r) FROM Receive r"
				+ " JOIN FETCH r.receiveDetails rd"
				+ " JOIN FETCH rd.purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pdet"
				+ " LEFT JOIN FETCH pdet.product prdpur"
				+ " JOIN FETCH r.supplier sp"
				+ " LEFT JOIN FETCH sp.person ps"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH rd.invoiceDetails invdetail"
				+ " LEFT JOIN FETCH r.person pr"
				+ " WHERE p.implementation=? AND r.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiveInfoById", query = "SELECT DISTINCT(r) FROM Receive r"
				+ " LEFT JOIN FETCH r.receiveDetails rd"
				+ " LEFT JOIN FETCH rd.purchase pur"
				+ " LEFT JOIN FETCH pur.purchaseDetails pdet"
				+ " LEFT JOIN FETCH pdet.product prdpur"
				+ " LEFT JOIN FETCH pur.currency cur"
				+ " LEFT JOIN FETCH cur.currencyPool ccp"
				+ " LEFT JOIN FETCH rd.product prd"
				+ " LEFT JOIN FETCH pur.supplier ss"
				+ " LEFT JOIN FETCH ss.person pr"
				+ " LEFT JOIN FETCH ss.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH ss.company cpy"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH r.goodsReturns goodreturn"
				+ " LEFT JOIN FETCH goodreturn.goodsReturnDetails returns"
				+ " LEFT JOIN FETCH r.person ctdPr" + " WHERE r.receiveId=?"),

		@org.hibernate.annotations.NamedQuery(name = "supplierActivePO", query = "SELECT DISTINCT(p) FROM Purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN pd.productPackageDetail packDetail"
				+ " LEFT JOIN FETCH pd.goodsReturnDetails pdgrd"
				+ " LEFT JOIN FETCH pd.receiveDetails pdrcv"
				+ " LEFT JOIN FETCH pdrcv.receive rcv"
				+ " LEFT JOIN FETCH p.quotation qt"
				+ " LEFT JOIN FETCH p.requisition req"
				+ " LEFT JOIN FETCH pdrcv.goodsReturnDetails grd"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.currency cc"
				+ " LEFT JOIN FETCH cc.currencyPool ccp"
				+ " LEFT JOIN FETCH pd.product prd"
				+ " LEFT JOIN FETCH prd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle al"
				+ " LEFT JOIN FETCH al.store str"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE p.status = 1"
				+ " AND p.expiryDate >= ?"
				+ " AND s.supplierId = ? AND p.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "supplierActiveWithCurrentReceive", query = "SELECT DISTINCT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH p.quotation qt"
				+ " LEFT JOIN FETCH p.requisition req"
				+ " LEFT JOIN FETCH p.receiveDetails rc"
				+ " LEFT JOIN FETCH p.goodsReturnDetails grd"
				+ " LEFT JOIN FETCH rc.receive recei"
				+ " LEFT JOIN FETCH recei.goodsReturns ret"
				+ " LEFT JOIN FETCH ret.goodsReturnDetails grd1"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.currency cc"
				+ " LEFT JOIN FETCH cc.currencyPool ccp"
				+ " LEFT JOIN FETCH pd.product prd"
				+ " LEFT JOIN FETCH prd.shelf slf"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE recei.receiveId=? AND (grd IS NULL OR grd=grd1) "),

		@org.hibernate.annotations.NamedQuery(name = "supplierPOByOtherReceives", query = "SELECT DISTINCT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH p.quotation qt"
				+ " LEFT JOIN FETCH p.requisition req"
				+ " LEFT JOIN FETCH p.receiveDetails rc"
				+ " LEFT JOIN FETCH p.goodsReturnDetails grd"
				+ " LEFT JOIN FETCH rc.receive recei"
				+ " LEFT JOIN FETCH recei.goodsReturns ret"
				+ " LEFT JOIN FETCH ret.goodsReturnDetails grd1"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.currency cc"
				+ " LEFT JOIN FETCH cc.currencyPool ccp"
				+ " LEFT JOIN FETCH pd.product prd"
				+ " LEFT JOIN FETCH prd.shelf slf"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE recei.receiveId !=? AND (grd IS NULL OR grd=grd1) AND recei IS NOT NULL "
				+ " AND s.supplierId = recei.supplier.supplierId "),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierActiveGRN", query = "SELECT DISTINCT(rcd) FROM ReceiveDetail rcd"
				+ " LEFT JOIN FETCH rcd.receive rc"
				+ " LEFT JOIN FETCH rcd.purchase prch"
				+ " LEFT JOIN FETCH prch.currency cncy"
				+ " LEFT JOIN FETCH rcd.invoiceDetails invd"
				+ " LEFT JOIN FETCH rc.supplier s"
				+ " LEFT JOIN FETCH rcd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE rc.status = 1"
				+ " AND s.supplierId = ? AND rc.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierActiveGRNWithInvoice", query = "SELECT DISTINCT rcd FROM ReceiveDetail rcd"
				+ " LEFT JOIN FETCH rcd.receive rc"
				+ " LEFT JOIN FETCH rcd.invoiceDetails invd"
				+ " LEFT JOIN FETCH invd.invoice inv"
				+ " LEFT JOIN FETCH rc.supplier s"
				+ " LEFT JOIN FETCH rcd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE (rc.status = 1 OR inv.invoiceId=? ) "
				+ " AND s.supplierId = ? AND rc.isApprove=? "),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierGRN", query = "SELECT DISTINCT rcd FROM ReceiveDetail rcd"
				+ " LEFT JOIN FETCH rcd.receive rc"
				+ " LEFT JOIN FETCH rcd.purchase purc"
				+ " LEFT JOIN FETCH purc.currency cy"
				+ " LEFT JOIN FETCH rcd.invoiceDetails invd"
				+ " LEFT JOIN FETCH rc.supplier s"
				+ " LEFT JOIN FETCH rcd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE s.supplierId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getLegerDetails", query = "SELECT DISTINCT ld FROM Ledger ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " WHERE ld.combination.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationWithLedger", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.ledgers l"
				+ " LEFT JOIN FETCH c.transactionDetails td"
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationWithAccountsLedger", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " JOIN FETCH c.ledgers l"
				+ " JOIN FETCH c.accountByNaturalAccountId td"
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationAccounts", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationAccountDetail", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationAllAccountDetail", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId b2"
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationAllAccountByBankAccount", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.bankAccountsForCombinationId ba"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ba.bankAccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLedgerDetailList", query = "SELECT DISTINCT(ld) FROM Ledger ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH natural.lookupDetail l"
				+ " WHERE company.implementation=?"
				+ " AND ld.balance IS NOT NULL"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLedgers", query = "SELECT DISTINCT(ld) FROM Ledger ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buffer2"
				+ " WHERE company.implementation=?"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getLedgerFiscalList", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE company.implementation=?"
				+ " AND ld.balance IS NOT NULL"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getLedgerFiscals", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination cb"
				+ " JOIN FETCH cb.accountByNaturalAccountId natural"
				+ " WHERE cb.combinationId = ?" + " AND ld.period.periodId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getLedgerFiscalsAgainstPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.period prd"
				+ " JOIN FETCH ld.combination cb"
				+ " JOIN FETCH cb.accountByCompanyAccountId company"
				+ " JOIN FETCH cb.accountByCostcenterAccountId cost"
				+ " JOIN FETCH cb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH cb.accountByBuffer1AccountId buf1"
				+ " LEFT JOIN FETCH cb.accountByBuffer2AccountId buf2"
				+ " WHERE ld.period.periodId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getFiscalByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " WHERE ld.period.periodId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getIncomeRevenueLedgerList", query = "SELECT DISTINCT(ld) FROM Ledger ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId buf1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId buf2"
				+ " LEFT JOIN FETCH natural.lookupDetail l"
				+ " WHERE company.implementation = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 3 OR natural.accountType = 4)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getIncomeRevenueLedgerFiscals", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE company.implementation = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 3 OR natural.accountType = 4)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getIncomeRevenueLedgerFiscalsByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId b2"
				+ " WHERE ld.period.periodId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 3 OR natural.accountType = 4)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getBalanceSheetLedgerList", query = "SELECT DISTINCT(ld) FROM Ledger ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH natural.lookupDetail lkd"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId b2"
				+ " WHERE company.implementation=?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 1 OR natural.accountType = 2 OR natural.accountType = 5)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getNaturalAccountCombination", query = "SELECT DISTINCT c FROM Combination c"
				+ " WHERE c.accountByCompanyAccountId.accountId = ?"
				+ " AND c.accountByCostcenterAccountId.accountId = ?"
				+ " AND c.accountByNaturalAccountId.accountId = ?"
				+ " AND c.accountByAnalysisAccountId IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getNaturalAccountByCostCombination", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " WHERE c.accountByCostcenterAccountId.accountId = ?"
				+ " AND c.accountByNaturalAccountId.accountId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalLines", query = "SELECT detail FROM TransactionDetail detail"
				+ " LEFT JOIN FETCH detail.transaction trans LEFT JOIN FETCH detail.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE isReceipt=1 AND trans.implementation=? "
				+ " ORDER BY natural.accountType, detail.isDebit DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getJournalBankLines", query = "SELECT DISTINCT detail FROM TransactionDetail detail"
				+ " LEFT JOIN FETCH detail.transaction trans"
				+ " LEFT JOIN FETCH detail.combination comb"
				+ " WHERE detail.isreceipt=1 AND trans.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBanks", query = "SELECT DISTINCT  b FROM Bank b"
				+ " LEFT JOIN FETCH b.person p "
				+ " WHERE b.implementation = ?" + " AND b.institutionType = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllInsitutions", query = "SELECT DISTINCT b FROM Bank b"
				+ " LEFT JOIN FETCH b.person p "
				+ " WHERE b.implementation = ? AND b.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDebitCreditCard", query = "SELECT DISTINCT(b) FROM Bank b"
				+ " LEFT JOIN FETCH b.person p "
				+ " LEFT JOIN FETCH b.bankAccounts ba"
				+ " WHERE b.implementation = ?" 
				+ " AND b.institutionType = ? "
				+ " AND ba.cardType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankAccountDetails", query = "SELECT DISTINCT bc FROM BankAccount bc"
				+ " LEFT JOIN FETCH bc.combinationByClearingAccount cacc"
				+ " LEFT JOIN FETCH bc.combination cb"
				+ " WHERE bc.bankAccountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBankAccounts", query = "SELECT DISTINCT bc FROM BankAccount bc"
				+ " JOIN FETCH bc.bank b "
				+ " LEFT JOIN FETCH bc.chequeBooks cb"
				+ " WHERE b.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllActiveCategories", query = "SELECT DISTINCT c FROM Category c"
				+ " WHERE c.implementation=? AND c.toDate>=? AND c.status=1 AND c.isApprove = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getActiveCategoryByName", query = "SELECT DISTINCT c FROM Category c"
				+ " WHERE c.implementation = ? AND c.toDate >= ? AND c.status=1 AND c.name = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllCategories", query = "SELECT DISTINCT c FROM Category c"
				+ " WHERE c.implementation=? AND c.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCategoryByName", query = "SELECT DISTINCT c FROM Category c"
				+ " WHERE c.implementation=? AND c.name = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankInfo", query = "SELECT DISTINCT b FROM Bank b "
				+ " JOIN FETCH b.bankAccounts ba"
				+ " LEFT JOIN FETCH ba.lookupDetailByCardIssuingEntity ice"
				+ " LEFT JOIN FETCH ba.lookupDetailByOperationMode om"
				+ " LEFT JOIN FETCH ba.lookupDetailBySignatory1LookupId s1"
				+ " LEFT JOIN FETCH ba.lookupDetailBySignatory2LookupId s2"
				+ " LEFT JOIN FETCH ba.combination bacb"
				+ " LEFT JOIN FETCH ba.currency cy"
				+ " LEFT JOIN FETCH cy.currencyPool cpl"
				+ " LEFT JOIN FETCH b.combination c"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId n"
				+ " WHERE b.bankId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllBankAccountsUsingBank", query = "SELECT DISTINCT ba FROM BankAccount ba "
				+ " WHERE ba.bank=? "),

		@org.hibernate.annotations.NamedQuery(name = "getEiborList", query = "SELECT e FROM Eibor e"
				+ " JOIN FETCH e.combination c JOIN FETCH"
				+ " c.accountByNaturalAccountId natural JOIN FETCH c.accountByCostcenterAccountId cost JOIN FETCH"
				+ " c.accountByCompanyAccountId company LEFT JOIN FETCH c.accountByAnalysisAccountId analysis LEFT JOIN FETCH"
				+ " c.accountByBuffer2AccountId buffer2 LEFT JOIN FETCH c.accountByBuffer1AccountId buffer1 WHERE e.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLoans", query = "SELECT l FROM Loan l "
				+ " LEFT JOIN FETCH l.eibor e JOIN FETCH l.bankAccount b JOIN FETCH b.bank"
				+ " WHERE l.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanDetailsById", query = "SELECT ld FROM LoanDetail ld"
				+ " JOIN ld.loan l WHERE l.loanId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanChargesById", query = "SELECT lc FROM LoanCharge lc"
				+ " JOIN lc.loan l WHERE l.loanId=? AND lc.type='I'"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanChargesByAll", query = "SELECT lc FROM LoanCharge lc"
				+ " LEFT JOIN FETCH lc.combination c "
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId a WHERE lc.loan.loanId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanChargesByValue", query = "SELECT lc FROM LoanCharge lc"
				+ " JOIN lc.loan l WHERE l.loanId=? AND lc.type='A' AND lc.paymentFlag=0"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanChargePayments", query = "SELECT lc FROM LoanCharge lc"
				+ " JOIN lc.paymentDetails pd WHERE lc.loan.loanId=? AND lc.type='A'"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanChargesWithLoan", query = "SELECT lc FROM LoanCharge lc"
				+ " JOIN FETCH lc.loan l WHERE lc.otherChargeId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanChargesAllById", query = "SELECT lc FROM LoanCharge lc"
				+ " JOIN lc.loan l WHERE l.loanId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLoanDetails", query = "SELECT l FROM Loan l"
				+ " LEFT JOIN FETCH l.eibor e"
				+ " LEFT JOIN FETCH l.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH l.combinationByEmiAccountId ce"
				+ " LEFT JOIN FETCH l.combinationByInterestExpenses cie"
				+ " LEFT JOIN FETCH l.combinationByInterestPayables cip"
				+ " LEFT JOIN FETCH l.combinationByLoanAccountId cl"
				+ " LEFT JOIN FETCH l.combinationByShortTermAccountId cs"
				+ " LEFT JOIN FETCH l.combinationByOtherChargesAccountId co"
				+ " LEFT JOIN FETCH ce.accountByNaturalAccountId cea"
				+ " LEFT JOIN FETCH cie.accountByNaturalAccountId ciea"
				+ " LEFT JOIN FETCH cip.accountByNaturalAccountId cipa"
				+ " LEFT JOIN FETCH cl.accountByNaturalAccountId cla"
				+ " LEFT JOIN FETCH cs.accountByNaturalAccountId csa"
				+ " LEFT JOIN FETCH co.accountByNaturalAccountId coa"
				+ " LEFT JOIN FETCH l.currency cur"
				+ " LEFT JOIN FETCH cur.currencyPool cpool"
				+ " WHERE l.loanId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCollateral", query = "SELECT c FROM CollateralDetail c"
				+ " LEFT JOIN FETCH c.loan l LEFT JOIN FETCH l.bankAccount acc LEFT JOIN FETCH acc.bank b"
				+ " WHERE l.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanByAccountId", query = "SELECT l FROM Loan l"
				+ " LEFT JOIN FETCH l.currency c LEFT JOIN FETCH c.currencyPool cp "
				+ " LEFT JOIN FETCH l.bankAccount b WHERE b.bankAccountId=?"
				+ " AND (l.isApprove=1 OR l.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getLoans", query = "SELECT l FROM Loan l"
				+ " LEFT JOIN FETCH l.currency c LEFT JOIN FETCH c.currencyPool cp LEFT JOIN FETCH l.bankAccount b"
				+ " WHERE l.implementation=? AND (l.isApprove=1 OR l.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getCollateralById", query = "SELECT c FROM CollateralDetail c"
				+ " LEFT JOIN FETCH c.loan l"
				+ " LEFT JOIN FETCH l.bankAccount b"
				+ " LEFT JOIN FETCH b.bank bk WHERE c.collateralId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCollateralListById", query = "SELECT c FROM CollateralDetail c"
				+ " WHERE c.loan.loanId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllEMI", query = "SELECT e FROM Emi e"
				+ " LEFT JOIN FETCH e.loan l WHERE l.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBanksWithAccounts", query = "SELECT b FROM Bank b"
				+ " LEFT JOIN FETCH b.bankAccounts ba WHERE b.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getEMIScheduleDetails", query = "SELECT e FROM Emi e"
				+ " LEFT JOIN FETCH e.loan l WHERE l.loanId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLoanAlertDetails", query = "SELECT l FROM Loan l"
				+ " WHERE l.maturityDate>=?"),

		@org.hibernate.annotations.NamedQuery(name = "checkEMIPaid", query = "SELECT e FROM Emi e"
				+ " WHERE e.loan.loanId=? AND e.date=?"),

		@org.hibernate.annotations.NamedQuery(name = "getEMIPayments", query = "SELECT e FROM EmiDetail e"
				+ " WHERE e.emi.emiId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllTenders", query = "SELECT DISTINCT t FROM Tender t"
				+ " JOIN FETCH t.currency c"
				+ " JOIN FETCH c.currencyPool"
				+ " WHERE t.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTenderBasicById", query = "SELECT DISTINCT t FROM Tender t"
				+ " JOIN FETCH t.tenderDetails td" + " WHERE t.tenderId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnQuotedTenders", query = "SELECT DISTINCT t FROM Tender t"
				+ " JOIN FETCH t.currency c"
				+ " JOIN FETCH c.currencyPool"
				+ " WHERE t.implementation = ?"
				+ " AND t.tenderExpiry >=? AND t.isApprove=? "
				+ " AND t.tenderId NOT IN"
				+ " (SELECT q.tender.tenderId FROM t.quotations q)"),

		@org.hibernate.annotations.NamedQuery(name = "getTenderDetails", query = "SELECT DISTINCT(td) FROM TenderDetail td"
				+ " JOIN FETCH td.tender t"
				+ " LEFT JOIN FETCH td.product p"
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit pu"
				+ " WHERE t.tenderId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllQuotation", query = "SELECT DISTINCT q FROM Quotation q"
				+ " LEFT JOIN FETCH q.tender t"
				+ " LEFT JOIN FETCH q.currency c "
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH q.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy "
				+ " LEFT JOIN FETCH sp.company cpy "
				+ " LEFT JOIN FETCH q.requisition rq"
				+ " LEFT JOIN FETCH q.purchases lpo"
				+ " WHERE q.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllQuotationNumber", query = "SELECT DISTINCT q FROM Quotation q"
				+ " WHERE q.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnPurchasedQuotation", query = "SELECT DISTINCT q FROM Quotation q"
				+ " LEFT JOIN FETCH q.tender t"
				+ " LEFT JOIN FETCH q.currency c "
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH q.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy "
				+ " LEFT JOIN FETCH sp.company cpy "
				+ " LEFT JOIN FETCH q.requisition rq"
				+ " WHERE q.implementation=?"
				+ " AND q.expiryDate>=? AND q.isApprove=? "
				+ " AND q.quotationId NOT IN ("
				+ " SELECT pur.quotation.quotationId FROM q.purchases pur)"),

		@org.hibernate.annotations.NamedQuery(name = "getQuotationDetail", query = "SELECT DISTINCT q FROM QuotationDetail q"
				+ " LEFT JOIN FETCH q.product p LEFT JOIN FETCH p.lookupDetailByProductUnit pu "
				+ " WHERE q.quotation.quotationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getQuotationById", query = "SELECT DISTINCT q FROM Quotation q"
				+ " LEFT JOIN FETCH q.tender t "
				+ " JOIN FETCH q.currency c "
				+ " JOIN FETCH q.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH q.requisition rq"
				+ " LEFT JOIN FETCH s.company cpy" + " WHERE q.quotationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getQuotationBasicById", query = "SELECT DISTINCT q FROM Quotation q"
				+ " JOIN FETCH q.quotationDetails qd "
				+ " WHERE q.quotationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getQuotationDetailByProductId", query = "SELECT DISTINCT(qd) FROM QuotationDetail qd"
				+ " LEFT JOIN FETCH qd.quotation q "
				+ " LEFT JOIN FETCH q.supplier s"
				+ " LEFT JOIN FETCH s.person sp "
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl "
				+ " LEFT JOIN FETCH cdl.company cy "
				+ " LEFT JOIN FETCH s.company cpy "
				+ " LEFT JOIN FETCH qd.product prd "
				+ " WHERE prd.productId = ?"),
					
		@org.hibernate.annotations.NamedQuery(name = "getAllPurchaseOrder", query = "SELECT DISTINCT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails ppd"
				+ " LEFT JOIN FETCH p.currency c "
				+ " LEFT JOIN FETCH p.supplier s "
				+ " LEFT JOIN FETCH s.person sp "
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl "
				+ " LEFT JOIN FETCH cdl.company cy "
				+ " LEFT JOIN FETCH s.company cpy "
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH p.creditTerm pyt"
				+ " LEFT JOIN FETCH p.receiveDetails receive"
				+ " LEFT JOIN FETCH receive.receive rce"
				+ " LEFT JOIN FETCH p.requisition req"
				+ " LEFT JOIN FETCH p.personByCreatedBy prcy"
				+ " WHERE p.implementation = ? AND p.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPaymentsWithoutImplementation", query = "SELECT DISTINCT py FROM Payment py"
				+ " JOIN FETCH py.paymentDetails pyd"
				+ " JOIN FETCH pyd.product prd"
				+ " JOIN FETCH py.receive rc"
				+ " JOIN FETCH rc.receiveDetails rcd"
				+ " JOIN FETCH rc.supplier sp"
				+ " LEFT JOIN FETCH rcd.invoiceDetails invd"
				+ " LEFT JOIN FETCH invd.product ivprd"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePurchaseOrder", query = "SELECT DISTINCT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.currency c "
				+ " LEFT JOIN FETCH p.supplier s "
				+ " LEFT JOIN FETCH s.person sp "
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl "
				+ " LEFT JOIN FETCH cdl.company cy "
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH p.creditTerm pyt"
				+ " WHERE p.implementation=? AND p.expiryDate>=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseAndCurrencyById", query = "SELECT DISTINCT(p) FROM Purchase p"
 				+ " LEFT JOIN FETCH p.currency cy" 
 				+ " WHERE p.purchaseId = ? "),
 				
		@org.hibernate.annotations.NamedQuery(name = "getPurchaseOrderBasicById", query = "SELECT DISTINCT p FROM Purchase p"
				+ " JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH pd.productPackageDetail packDetail"
				+ " JOIN FETCH pd.product prd"
				+ " LEFT JOIN FETCH pd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store ste" + " WHERE p.purchaseId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseOrderSupplier", query = "SELECT DISTINCT(p) FROM Purchase p"
				+ " LEFT JOIN FETCH p.supplier sp"
				+ " JOIN FETCH p.purchaseDetails pd"
				+ " JOIN FETCH pd.product prd"
				+ " LEFT JOIN FETCH pd.shelf slf"
				+ " LEFT JOIN FETCH p.personByCreatedBy prby"
				+ " WHERE p.purchaseId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getUnReceivePurchaseOrder", query = "SELECT DISTINCT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.currency c "
				+ " LEFT JOIN FETCH p.supplier s "
				+ " LEFT JOIN FETCH s.person sp "
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl "
				+ " LEFT JOIN FETCH cdl.company cy "
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH p.creditTerm pyt"
				+ " WHERE p.implementation=? AND p.expiryDate>=?"
				+ " AND p.purchaseId NOT IN ("
				+ " SELECT rc.purchase.purchaseId FROM p.receiveDetails rc)"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetReceiveDetail", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
				+ " JOIN FETCH rd.product pr"
				+ " JOIN FETCH pr.lookupDetailByProductUnit pu"
				+ " WHERE rd.receive.implementation = ?"
				+ " AND pr.itemType = 'A'"),

		@org.hibernate.annotations.NamedQuery(name = "getAllReceiveDetails", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
				+ " JOIN FETCH rd.product pr"
				+ " JOIN FETCH rd.shelf slf"
				+ " JOIN FETCH slf.shelf rk"
				+ " JOIN FETCH rk.aisle asl"
				+ " JOIN FETCH asl.store str"
				+ " JOIN FETCH rd.receive r"
				+ " WHERE r.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllReceiveDetailWithoutImpl", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
				+ " LEFT JOIN FETCH rd.product pr"
				+ " LEFT JOIN FETCH rd.purchaseDetail rdpo"
				+ " LEFT JOIN FETCH rd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rk"
				+ " LEFT JOIN FETCH rk.aisle asl"
				+ " LEFT JOIN FETCH asl.store str"
				+ " LEFT JOIN FETCH rd.receive r"
				+ " LEFT JOIN FETCH rd.purchase po"
				+ " LEFT JOIN FETCH po.purchaseDetails pos"
				+ " LEFT JOIN FETCH pos.product pr" + " WHERE rdpo IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiveDetailById", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
				+ " JOIN FETCH rd.product pr"
				+ " JOIN FETCH rd.shelf slf"
				+ " JOIN FETCH rd.receive r" + " WHERE rd.receiveDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseDetailOrderById", query = "SELECT DISTINCT(p) FROM PurchaseDetail p"
				+ " LEFT JOIN FETCH p.product pd"
				+ " LEFT JOIN FETCH pd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH p.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store ste"
				+ " LEFT JOIN FETCH p.productPackageDetail packdetail"
				+ " WHERE p.purchase.purchaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseProducts", query = "SELECT DISTINCT(p) FROM PurchaseDetail p"
				+ " JOIN FETCH p.product pd" + " WHERE pd.productId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseOrder", query = "SELECT DISTINCT(p) FROM Purchase p"
				+ " LEFT JOIN FETCH p.quotation q"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.currency cr"
				+ " LEFT JOIN FETCH s.combination cb"
				+ " LEFT JOIN FETCH p.creditTerm pyt"
				+ " LEFT JOIN FETCH p.personByPurchaseManager pm"
				+ " LEFT JOIN FETCH p.lookupDetail ld"
				+ " LEFT JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH pd.shelf shelf"
				+ " LEFT JOIN FETCH pd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH p.requisition rq"
				+ " LEFT JOIN FETCH pd.requisitionDetail rqd"
				+ " WHERE p.purchaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseOrderPrint", query = "SELECT p FROM Purchase p"
				+ " JOIN FETCH p.purchaseDetails pd"
				+ " JOIN FETCH pd.product pro"
				+ " JOIN FETCH pro.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH p.requisition rq"
				+ " LEFT JOIN FETCH pd.requisitionDetail rqd"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " JOIN FETCH p.currency cr"
				+ " LEFT JOIN FETCH p.creditTerm pyt"
				+ " LEFT JOIN FETCH p.personByPurchaseManager pm"
				+ " LEFT JOIN FETCH p.lookupDetail ld"
				+ " LEFT JOIN FETCH pd.productPackageDetail packUnit"
				+ " LEFT JOIN FETCH packUnit.lookupDetail packLpd"
				+ " WHERE p.purchaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseReceiveOrder", query = "SELECT DISTINCT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH p.receiveDetails rc"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.currency cc"
				+ " LEFT JOIN FETCH cc.currencyPool ccp"
				+ " LEFT JOIN FETCH p.creditTerm pyt"
				+ " WHERE p.implementation=?"
				+ " AND p.expiryDate>=?"
				+ " AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseReceiveOrderById", query = "SELECT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.receiveDetails rc"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.currency cc"
				+ " LEFT JOIN FETCH cc.currencyPool ccp"
				+ " LEFT JOIN FETCH p.creditTerm pyt" + " WHERE p.purchaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseOrderDetailByPurchaseId", query = "SELECT DISTINCT(p) FROM Purchase p"
				+ " JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH pd.requisitionDetail reqDeta"
				+ " LEFT JOIN FETCH reqDeta.requisition req"
				+ " JOIN FETCH pd.product prd" + " WHERE p.purchaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "purchaseOrderActive", query = "SELECT p FROM Purchase p"
				+ " LEFT JOIN FETCH p.purchaseDetails pd"
				+ " LEFT JOIN FETCH p.receiveDetails rc"
				+ " LEFT JOIN FETCH rc.receive rcd"
				+ " LEFT JOIN FETCH p.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cpl"
				+ " LEFT JOIN FETCH cpl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH p.creditTerm pyt" + " WHERE p.purchaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllGoodsReturn", query = "SELECT DISTINCT(gr) FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH gr.receive rece"
				+ " LEFT JOIN FETCH gr.debits debits"
				+ " WHERE s.implementation=? "
				+ " AND gr.isApprove=?"
				+ " AND gr.isDebit = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveGoodsReturn", query = "SELECT DISTINCT(gr) FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH gr.receive rece"
				+ " WHERE gr.implementation=? AND gr.isApprove=? "
				+ " AND gr.isDebit = 1"
				+ " AND gr.goodsReturnId NOT IN"
				+ "(SELECT dt.goodsReturn.goodsReturnId FROM gr.debits dt)"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnByReturnId", query = "SELECT DISTINCT(gr) FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.goodsReturnDetails grd"
				+ " LEFT JOIN FETCH grd.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH gr.receive receive"
				+ " LEFT JOIN FETCH receive.receiveDetails rceDet"
				+ " LEFT JOIN FETCH rceDet.purchaseDetail rdpr"
				+ " LEFT JOIN FETCH rdpr.purchase prrcv"
				+ " LEFT JOIN FETCH grd.receiveDetail grrcv"
				+ " LEFT JOIN FETCH grd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH grd.purchase pur"
				+ " LEFT JOIN FETCH pur.purchaseDetails prds"
				+ " LEFT JOIN FETCH prds.product pds"
				+ " LEFT JOIN FETCH pur.receiveDetails rcv"
				+ " LEFT JOIN FETCH rcv.product prcv"
				+ " LEFT JOIN FETCH gr.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " WHERE gr.goodsReturnId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnInfoByReceive", query = "SELECT DISTINCT(gr) FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.receive receive"
				+ " LEFT JOIN FETCH receive.receiveDetails rceDet"
				+ " LEFT JOIN FETCH rceDet.purchase purch"
				+ " LEFT JOIN FETCH rceDet.purchaseDetail recDtPodt"
				+ " LEFT JOIN FETCH recDtPodt.purchase podtPo"
				+ " LEFT JOIN FETCH gr.goodsReturnDetails grd"
				+ " LEFT JOIN FETCH grd.receiveDetail grrcv"
				+ " LEFT JOIN FETCH grd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH grd.purchase pur"
				+ " LEFT JOIN FETCH pur.purchaseDetails prds"
				+ " LEFT JOIN FETCH prds.product pds"
				+ " LEFT JOIN FETCH pur.receiveDetails rcv"
				+ " LEFT JOIN FETCH rcv.product prcv"
				+ " LEFT JOIN FETCH gr.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH grd.productPackageDetail packDetail"
				+ " LEFT JOIN FETCH packDetail.lookupDetail ldDtpck"
				+ " WHERE gr.goodsReturnId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturns", query = "SELECT DISTINCT(gr) FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.goodsReturnDetails grd"
				+ " LEFT JOIN FETCH grd.receiveDetail rcv"
				+ " LEFT JOIN FETCH rcv.shelf slf"
				+ " LEFT JOIN FETCH rcv.product prd"
				+ " LEFT JOIN FETCH grd.purchase pur"
				+ " LEFT JOIN FETCH pur.currency cur"
				+ " WHERE gr.goodsReturnId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnSupplier", query = "SELECT DISTINCT(gr) FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.supplier sp"
				+ " LEFT JOIN FETCH sp.combination cb"
				+ " WHERE gr.goodsReturnId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnDebitGoodsReturn", query = "SELECT gr FROM GoodsReturn gr"
				+ " LEFT JOIN FETCH gr.supplier s"
				+ " LEFT JOIN FETCH s.person pr"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " WHERE s.implementation=?"
				+ " AND gr.goodsReturnId NOT IN(SELECT d.goodsReturn.goodsReturnId FROM gr.debits d)"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnLines", query = "SELECT DISTINCT gd FROM GoodsReturnDetail gd"
				+ " LEFT JOIN FETCH gd.purchase pr"
				+ " LEFT JOIN FETCH pr.currency cry"
				+ " LEFT JOIN FETCH pr.supplier sp"
				+ " LEFT JOIN FETCH sp.combination scom"
				+ " LEFT JOIN FETCH pr.purchaseDetails prd"
				+ " LEFT JOIN FETCH prd.product prod"
				+ " LEFT JOIN FETCH gd.product p"
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit pu"
				+ " WHERE gd.goodsReturn.goodsReturnId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnDetails", query = "SELECT DISTINCT(gd) FROM GoodsReturnDetail gd"
				+ " JOIN FETCH gd.receiveDetail rd"
				+ " JOIN FETCH gd.goodsReturn gr"
				+ " JOIN FETCH gr.supplier sp"
				+ " JOIN FETCH gd.product prd"
				+ " LEFT JOIN FETCH gd.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " WHERE gr.goodsReturnId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnDetailWithReceive", query = "SELECT DISTINCT(gd) FROM GoodsReturnDetail gd"
				+ " JOIN FETCH gd.receiveDetail rd"
				+ " JOIN FETCH rd.shelf slf" + " WHERE gd.returnDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getGoodsReturnDetailsWithGRNDetail", query = "SELECT DISTINCT(gd) FROM GoodsReturnDetail gd"
				+ " WHERE gd.receiveDetail.receiveDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStores", query = "SELECT DISTINCT(s) FROM Store s"
				+ " WHERE s.implementation = ? AND s.isApprove = ? ORDER BY s.storeId ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getBasicStoreByStoreId", query = "SELECT DISTINCT(s) FROM Store s"
				+ " WHERE s.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllStores", query = "SELECT DISTINCT(s) FROM Store s"
				+ " LEFT JOIN FETCH s.lookupDetail st"
				+ " JOIN FETCH s.person p"
				+ " JOIN FETCH s.aisles asl"
				+ " LEFT JOIN FETCH asl.lookupDetail asld"
				+ " LEFT JOIN FETCH asl.shelfs slf"
				+ " LEFT JOIN FETCH slf.shelfs slfs"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " LEFT JOIN FETCH cmpLoc.department dept"
				+ " LEFT JOIN FETCH cmpLoc.location loc"
				+ " WHERE s.implementation = ? AND s.isApprove = ? ORDER BY s.storeId ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getRacksBySectionId", query = "SELECT DISTINCT(s) FROM Shelf s"
				+ " WHERE s.aisle.aisleId = ?" + " AND s.shelf IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getShelfsByRackId", query = "SELECT DISTINCT(s) FROM Shelf s"
				+ " WHERE s.shelf.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreByShelfId", query = "SELECT DISTINCT(s) FROM Shelf s"
				+ " JOIN FETCH s.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store ste" + " WHERE s.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getShelfsByStoreId", query = "SELECT DISTINCT(s) FROM Shelf s"
				+ " LEFT JOIN FETCH s.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store ste" + " WHERE ste.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreCompanyDetailsByShelfId", query = "SELECT DISTINCT(s) FROM Shelf s"
				+ " JOIN FETCH s.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store ste"
				+ " JOIN FETCH ste.cmpDeptLocation cmp"
				+ " JOIN FETCH cmp.company cpy"
				+ " LEFT JOIN FETCH cmp.location loc" + " WHERE s.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductsByShelfId", query = "SELECT DISTINCT(s) FROM Stock s"
				+ " LEFT JOIN FETCH s.product prd"
				+ " WHERE s.shelf.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreInfoDetails", query = "SELECT DISTINCT(s) FROM Store s"
				+ " JOIN FETCH s.lookupDetail st"
				+ " JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " LEFT JOIN FETCH cmpLoc.department dept"
				+ " LEFT JOIN FETCH cmpLoc.location loc"
				+ " JOIN FETCH s.aisles asl"
				+ " JOIN FETCH asl.lookupDetail secType "
				+ " JOIN FETCH asl.shelfs shlf "
				+ " JOIN FETCH shlf.shelf "
				+ " WHERE s.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getShelfById", query = "SELECT r FROM Shelf r"
				+ " JOIN FETCH r.aisle a"
				+ " JOIN FETCH a.store s"
				+ " WHERE r.shelfId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getShelfByStoreId", query = "SELECT DISTINCT(r) FROM Shelf r"
				+ " WHERE r.aisle.store.storeId = ? AND r IS NOT NULL "),

		@org.hibernate.annotations.NamedQuery(name = "getAisleDetails", query = "SELECT DISTINCT(a) FROM Aisle a"
				+ " LEFT JOIN FETCH a.shelfs shelf"
				+ " LEFT JOIN FETCH shelf.shelfs rack"
				+ " WHERE a.store.storeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreLocationCostCentre", query = "SELECT s FROM Store s"
				+ " JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " JOIN FETCH cmpLoc.combination c"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " WHERE s.storeId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreDetails", query = "SELECT s FROM Store s"
				+ " LEFT JOIN FETCH s.lookupDetail st"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " LEFT JOIN FETCH cmpLoc.department dept"
				+ " LEFT JOIN FETCH cmpLoc.location loc"
				+ " JOIN FETCH s.person p" + " WHERE s.storeId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreByDepartmentLocation", query = "SELECT Distinct(s) FROM Store s"
				+ " LEFT JOIN FETCH s.lookupDetail st"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " LEFT JOIN FETCH cmpLoc.department dept"
				+ " LEFT JOIN FETCH cmpLoc.location loc"
				+ " LEFT JOIN FETCH s.person p"
				+ " WHERE cmpLoc.cmpDeptLocId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getStoreDetailByStoreId", query = "SELECT s FROM Store s"
				+ " JOIN FETCH s.aisles ais"
				+ " LEFT JOIN FETCH ais.shelfs slf"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cmpLoc"
				+ " LEFT JOIN FETCH cmpLoc.department dept"
				+ " LEFT JOIN FETCH cmpLoc.location loc"
				+ " LEFT JOIN FETCH slf.shelfs slfs" + " WHERE s.storeId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDebitNotes", query = "SELECT d FROM Debit d"
				+ " LEFT JOIN FETCH d.goodsReturn r"
				+ " LEFT JOIN FETCH r.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " WHERE d.implementation=? AND d.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDebitNoteInfo", query = "SELECT d FROM Debit d"
				+ " LEFT JOIN FETCH d.debitDetails dd"
				+ " LEFT JOIN FETCH d.goodsReturn gr"
				+ " JOIN FETCH gr.supplier sp"
				+ " LEFT JOIN FETCH sp.combination scom"
				+ " JOIN FETCH dd.product pr" + " WHERE d.debitId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDebitNoteById", query = "SELECT d FROM Debit d"
				+ " LEFT JOIN FETCH d.goodsReturn r"
				+ " LEFT JOIN FETCH d.debitDetails dd"
				+ " LEFT JOIN FETCH dd.product prd"
				+ " LEFT JOIN FETCH dd.goodsReturnDetail rd"
				+ " LEFT JOIN FETCH rd.receiveDetail rcv"
				+ " LEFT JOIN FETCH rcv.shelf slf"
				+ " LEFT JOIN FETCH r.supplier sp"
				+ " LEFT JOIN FETCH sp.combination cb"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId ay"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH sp.company cpy" + " WHERE d.debitId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDebitByReturnId", query = "SELECT d FROM Debit d"
				+ " LEFT JOIN FETCH d.goodsReturn r"
				+ " LEFT JOIN FETCH r.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " WHERE d.goodsReturn.goodsReturnId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDebitDetailById", query = "SELECT d FROM DebitDetail d"
				+ " LEFT JOIN FETCH d.product"
				+ " p LEFT JOIN FETCH p.lookupDetailByProductUnit pu"
				+ " WHERE d.debit.debitId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCreditNotes", query = "SELECT c FROM Credit c"
				+ " JOIN FETCH c.customer cus"
				+ " LEFT JOIN FETCH cus.company"
				+ " LEFT JOIN FETCH cus.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cus.personByPersonId pr"
				+ " WHERE c.implementation = ?" + " AND c.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCreditNotesByReturn", query = "SELECT DISTINCT(c) FROM Credit c"
				+ " LEFT JOIN FETCH c.customer cus"
				+ " LEFT JOIN FETCH cus.company"
				+ " LEFT JOIN FETCH cus.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cus.personByPersonId pr"
				+ " WHERE c.implementation = ? and c.isCredit=0"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditDetailById", query = "SELECT c FROM CreditDetail c"
				+ " LEFT JOIN FETCH c.salesDeliveryDetail sd"
				+ " LEFT JOIN FETCH sd.product prd"
				+ " LEFT JOIN FETCH sd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " WHERE c.credit.creditId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditDetailByDeliveryDetailId", query = "SELECT c FROM CreditDetail c"
				+ " WHERE c.salesDeliveryDetail.salesDeliveryDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditNoteById", query = "SELECT DISTINCT(c) FROM Credit c"
				+ " LEFT JOIN FETCH c.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH c.creditDetails cds"
				+ " LEFT JOIN FETCH cds.product prd"
				+ " LEFT JOIN FETCH prd.combinationByInventoryAccount combinInv"
				+ " LEFT JOIN FETCH prd.combinationByExpenseAccount combinExp"
				+ " LEFT JOIN FETCH c.currency cry"
				+ " LEFT JOIN FETCH cry.currencyPool cpl"
				+ " WHERE c.creditId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditNoteByRecordIdUseCase", query = "SELECT DISTINCT(c) FROM Credit c"
				+ " LEFT JOIN FETCH c.creditDetails cd"
				+ " LEFT JOIN FETCH cd.product prd"
				+ " WHERE c.recordId = ?"
				+ " AND c.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditNoteInformationById", query = "SELECT DISTINCT(c) FROM Credit c"
				+ " LEFT JOIN FETCH c.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH c.creditDetails cds"
				+ " LEFT JOIN FETCH cds.product prd"
				+ " LEFT JOIN FETCH cds.shelf shelf"
				+ " LEFT JOIN FETCH shelf.shelf parentShelf"
				+ " LEFT JOIN FETCH parentShelf.aisle aisle"
				+ " LEFT JOIN FETCH aisle.store store"
				+ " LEFT JOIN FETCH prd.combinationByInventoryAccount combinInv"
				+ " LEFT JOIN FETCH prd.combinationByExpenseAccount combinExp"
				+ " LEFT JOIN FETCH c.currency cry"
				+ " LEFT JOIN FETCH cry.currencyPool cpl"
				+ " WHERE c.creditId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditNoteInformationByReference", query = "SELECT DISTINCT(c) FROM Credit c"
				+ " WHERE c.creditNumber = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesReturnBySalesReference", query = "SELECT DISTINCT(c) FROM Credit c"
				+ " WHERE c.salesReference = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditNoteDetails", query = "SELECT DISTINCT(c) FROM CreditDetail c"
				+ " JOIN FETCH c.salesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.product pr"
				+ " JOIN FETCH sdd.shelf slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH sdd.salesInvoiceDetails sid"
				+ " WHERE c.credit.creditId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayments", query = "SELECT p FROM Payment p"
				+ " LEFT JOIN FETCH p.bankAccount ba"
				+ " LEFT JOIN FETCH p.voidPayments vp"
				+ " JOIN FETCH p.person pr" + " WHERE p.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllChequePayments", query = "SELECT p FROM Payment p"
				+ " LEFT JOIN FETCH p.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank bk"
				+ " LEFT JOIN FETCH p.chequeBook cb"
				+ " LEFT JOIN FETCH p.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.company cy"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " WHERE p.chequeBook IS NOT NULL" + " AND p.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getByPaymentId", query = "SELECT p FROM Payment p"
				+ " LEFT JOIN FETCH p.loan l "
				+ " LEFT JOIN FETCH l.currency c "
				+ " LEFT JOIN FETCH c.currencyPool cp "
				+ " LEFT JOIN FETCH l.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b" + " WHERE p.paymentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPurchasePaymentDetails", query = "SELECT DISTINCT py FROM Payment py"
				+ " JOIN FETCH py.paymentDetails pyd"
				+ " LEFT JOIN FETCH py.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH sp.company scy"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH pyd.invoiceDetail invd"
				+ " LEFT JOIN FETCH invd.receiveDetail rcd"
				+ " LEFT JOIN FETCH rcd.purchase prc"
				+ " LEFT JOIN FETCH prc.creditTerm pyt"
				+ " LEFT JOIN FETCH invd.invoice inv"
				+ " LEFT JOIN FETCH inv.invoiceDetails invds"
				+ " LEFT JOIN FETCH invd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH py.combination pcb"
				+ " LEFT JOIN FETCH pcb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH pcb.accountByAnalysisAccountId ay"
				+ " LEFT JOIN FETCH py.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH py.voidPayments vp"
				+ " WHERE py.paymentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPaymentDetailById", query = "SELECT p FROM PaymentDetail p"
				+ " LEFT JOIN FETCH p.loanCharge lc"
				+ " LEFT JOIN FETCH lc.combination c"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId a"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId na"
				+ " WHERE p.payment.paymentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSupplierDetails", query = "SELECT s FROM Supplier s"
				+ " LEFT JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.company cmp"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH s.creditTerm pyt"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " JOIN FETCH s.combination cb"
				+ " JOIN FETCH cb.accountByAnalysisAccountId analysis"
				+ " WHERE s.implementation=? AND s.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getSuppliersByReceipts", query = "SELECT s FROM Supplier s"
				+ " LEFT JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.company cmp"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH s.creditTerm pyt"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH s.combination cb"
				+ " WHERE s.implementation=?"
				+ " AND s.supplierId IN(SELECT br.supplier.supplierId FROM s.bankReceiptses br)"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierById", query = "SELECT s FROM Supplier s"
				+ " LEFT JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.company cmp"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.department dp"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH s.creditTerm pyt"
				+ " LEFT JOIN FETCH s.combination cb"
				+ " LEFT JOIN FETCH cdl.company c WHERE s.supplierId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierCombination", query = "SELECT s FROM Supplier s"
				+ " JOIN FETCH s.combination c"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId aa"
				+ " WHERE s.supplierId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCombinationForView", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId cy"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cc"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId b2"
				+ " LEFT JOIN FETCH cy.segment scy"
				+ " LEFT JOIN FETCH cc.segment scc"
				+ " LEFT JOIN FETCH na.segment sna"
				+ " LEFT JOIN FETCH aa.segment saa"
				+ " LEFT JOIN FETCH b1.segment sb1"
				+ " LEFT JOIN FETCH b2.segment sb2"
				+ " WHERE cy.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationByCombinationId", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId cy"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cc"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId b2"
				+ " LEFT JOIN FETCH cy.segment scy"
				+ " LEFT JOIN FETCH cc.segment scc"
				+ " LEFT JOIN FETCH na.segment sna"
				+ " LEFT JOIN FETCH aa.segment saa"
				+ " LEFT JOIN FETCH b1.segment sb1"
				+ " LEFT JOIN FETCH b2.segment sb2"
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCombinationAccountByCombinationId", query = "SELECT DISTINCT(c) FROM Combination c"
				+ " LEFT JOIN FETCH c.accountByCompanyAccountId cy"
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cc"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH c.accountByBuffer1AccountId b1"
				+ " LEFT JOIN FETCH c.accountByBuffer2AccountId b2" 
				+ " LEFT JOIN FETCH c.combination cbpr"
				+ " WHERE c.combinationId = ?"),
					
		@org.hibernate.annotations.NamedQuery(name = "getLedgerByTransactionDetailId", query = "SELECT DISTINCT l FROM Ledger l"
				+ " WHERE l.combination=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDirectPayments", query = "SELECT DISTINCT dp FROM DirectPayment dp"
				+ " JOIN FETCH dp.directPaymentDetails dpd"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH dp.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cc"
				+ " LEFT JOIN FETCH sp.company cyn"
				+ " LEFT JOIN FETCH dp.voidPayments vp"
				+ " WHERE dp.implementation = ? AND dp.isApprove=?"),
		
		@org.hibernate.annotations.NamedQuery(name = "getDirectPaymentByReference", query = "SELECT DISTINCT d FROM DirectPayment d"
				+ " WHERE d.implementation = ? "
				+ " AND d.paymentNumber = ?"),
					
		@org.hibernate.annotations.NamedQuery(name = "getDirectPaymentByPresentedCheques", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.bankAccount ba"
				+ " WHERE ba.bankAccountId = ?  AND (dp.chequePresented IS NOT NULL AND dp.chequePresented = 1)"
				+ " AND dp.chequeDate >= ? AND dp.chequeDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDirectPaymentsByCurrentDate", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.directPaymentDetails dpd"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH dp.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cc"
				+ " LEFT JOIN FETCH sp.company cyn"
				+ " LEFT JOIN FETCH dp.voidPayments vp"
				+ " WHERE dp.implementation = ? AND dp.paymentDate <= ?"
				+ " AND dp.bankAccount IS NOT NULL" + " AND dp.isPdc = 1"),
				
		@org.hibernate.annotations.NamedQuery(name = "getPDCDirectPayments", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.directPaymentDetails dpd"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH dp.implementation impl"
 				+ " LEFT JOIN FETCH dp.bankAccount ba"
 				+ " LEFT JOIN FETCH ba.combination bacbi"
 				+ " WHERE dp.bankAccount IS NOT NULL" 
				+ " AND dp.chequeDate = ?" 
				+ " AND dp.isPdc = 1"
				+ " AND dp.isApprove=?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getPDCDirectPaymentsWithImpl", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.directPaymentDetails dpd"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH dp.implementation impl"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.combination bacbi"
				+ " WHERE dp.bankAccount IS NOT NULL" 
				+ " AND dp.implementation = ?"
				+ " AND dp.chequeDate = ?" 
				+ " AND dp.isPdc = 1"
				+ " AND dp.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDirectPaymentsOnly", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH dp.directPaymentDetails dpd"
				+ " JOIN FETCH dp.combination cb"
				+ " JOIN FETCH dpd.combination cbd"
				+ " WHERE dp.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPaymentDetail", query = "SELECT d FROM DirectPaymentDetail d"
				+ " JOIN FETCH d.combination c"
				+ " JOIN FETCH c.accountByNaturalAccountId na"
				+ " WHERE d.directPayment = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDirectPaymentDetails", query = "SELECT DISTINCT(d) FROM DirectPaymentDetail d"
				+ " JOIN FETCH d.directPayment dp"
				+ " LEFT JOIN FETCH dp.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH dp.personByPersonId person"
				+ " LEFT JOIN FETCH dp.customer cust"
				+ " LEFT JOIN FETCH cust.personByPersonId custpr"
				+ " LEFT JOIN FETCH cust.cmpDeptLocation custcdl"
				+ " LEFT JOIN FETCH cust.company custcpy"
				+ " LEFT JOIN FETCH custcdl.company custcy"
				+ " WHERE dp.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCashDirectPaymentDetails", query = "SELECT DISTINCT(d) FROM DirectPaymentDetail d"
				+ " JOIN FETCH d.directPayment dp"
				+ " LEFT JOIN FETCH dp.personByCreatedBy prcr"
				+ " LEFT JOIN FETCH dp.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH dp.personByPersonId person"
				+ " LEFT JOIN FETCH dp.customer cust"
				+ " LEFT JOIN FETCH cust.personByPersonId custpr"
				+ " LEFT JOIN FETCH cust.cmpDeptLocation custcdl"
				+ " LEFT JOIN FETCH cust.company custcpy"
				+ " LEFT JOIN FETCH custcdl.company custcy"
				+ " WHERE dp.implementation = ?" + " AND dp.paymentMode = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDirectPaymentById", query = "SELECT DISTINCT dp FROM DirectPayment dp"
				+ " JOIN FETCH dp.directPaymentDetails dpd "
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH dp.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH dp.combination cm"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH cm.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH dp.voidPayments vp"
				+ " LEFT JOIN FETCH dp.personByPersonId person"
				+ " LEFT JOIN FETCH dp.customer cust"
				+ " LEFT JOIN FETCH cust.personByPersonId custpr"
				+ " LEFT JOIN FETCH cust.cmpDeptLocation custcdl"
				+ " LEFT JOIN FETCH cust.company custcpy"
				+ " LEFT JOIN FETCH custcdl.company custcy"
				+ " WHERE dp.directPaymentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDirectPaymentByRecordIdAndUseCase", query = "SELECT dp FROM DirectPayment dp"
				+ " WHERE dp.recordId = ?" + " AND dp.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDirectPaymentDetail", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH dp.directPaymentDetails dpd"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.combination bacb"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH dp.personByPersonId dbpr"
				+ " LEFT JOIN FETCH dp.customer dbcst"
				+ " LEFT JOIN FETCH dbcst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH dbcst.cmpDeptLocation cstdl"
				+ " LEFT JOIN FETCH cstdl.company dlcy"
				+ " LEFT JOIN FETCH dbcst.company cstcy"
				+ " LEFT JOIN FETCH dp.supplier s"
				+ " LEFT JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company scy"
				+ " LEFT JOIN FETCH dp.combination cm"
				+ " LEFT JOIN FETCH cm.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cm.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH dpd.combination cmpd"
				+ " LEFT JOIN FETCH cmpd.accountByNaturalAccountId napd"
				+ " LEFT JOIN FETCH cmpd.accountByAnalysisAccountId aapd"
				+ " LEFT JOIN FETCH dp.paymentRequest preq"
				+ " LEFT JOIN FETCH dpd.paymentRequestDetail pdline"
				+ " LEFT JOIN FETCH dp.voidPayments vp"
				+ " WHERE dp.directPaymentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDirectChequePayments", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH dp.personByPersonId dbpr"
				+ " LEFT JOIN FETCH dp.customer dbcst"
				+ " LEFT JOIN FETCH dbcst.personByPersonId cstpr"
				+ " LEFT JOIN FETCH dbcst.cmpDeptLocation cstdl"
				+ " LEFT JOIN FETCH cstdl.company dlcy"
				+ " LEFT JOIN FETCH dbcst.company cstcy"
				+ " LEFT JOIN FETCH dp.supplier s"
				+ " LEFT JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company scy"
				+ " WHERE dp.chequeBook IS NOT NULL"
				+ " AND dp.implementation = ?"
				+ " AND dp.isApprove = ?"
				+ " AND dp.directPaymentId NOT IN(SELECT vc.directPayment.directPaymentId FROM VoidPayment vc)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDirectPaymentDetail", query = "SELECT DISTINCT(dp) FROM DirectPayment dp"
				+ " JOIN FETCH dp.currency c"
				+ " JOIN FETCH c.currencyPool cp"
				+ " LEFT JOIN FETCH dp.directPaymentDetails dpd"
				+ " LEFT JOIN FETCH dp.bankAccount ba"
				+ " LEFT JOIN FETCH ba.bank b"
				+ " LEFT JOIN FETCH dp.supplier s"
				+ " LEFT JOIN FETCH s.person p"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH s.company cpy"
				+ " LEFT JOIN FETCH dp.combination cm"
				+ " LEFT JOIN FETCH cm.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cm.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH dpd.combination cmpd"
				+ " LEFT JOIN FETCH cmpd.accountByNaturalAccountId napd"
				+ " LEFT JOIN FETCH cmpd.accountByAnalysisAccountId aapd"),

		@org.hibernate.annotations.NamedQuery(name = "getProductCodeByType", query = "SELECT p FROM Product p "
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " WHERE p.implementation=? " + " AND p.itemType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductByProductName", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.shelf slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " WHERE p.implementation=? AND p.productName = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getProductByProductCode", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.shelf slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " WHERE p.implementation=? AND p.code = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getProductCodeByItemSubType", query = "SELECT p FROM Product p "
				+ " WHERE p.implementation=? AND p.itemType = ? AND p.itemSubType = ? order by p desc "),

		@org.hibernate.annotations.NamedQuery(name = "getAllUpcomingDirectPayment", query = "SELECT DISTINCT dp FROM DirectPayment dp"
				+ " WHERE dp.paymentDate>=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllUpcomingPurchasePayment", query = "SELECT DISTINCT p FROM Payment p"
				+ " WHERE p.date>=? "),

		//Bank Deposit Queries	
		@org.hibernate.annotations.NamedQuery(name = "getAllBankDeposit", query = "SELECT DISTINCT bd FROM BankDeposit bd "
				+ " JOIN FETCH bd.personByCreatedBy pr"
				+ " JOIN FETCH bd.personByDepositerId dpr"
				+ " WHERE bd.implementation=? AND bd.isApprove=?"),
				 
		@org.hibernate.annotations.NamedQuery(name = "getBankDepositByPresentedCheques", query = "SELECT DISTINCT bd FROM BankDeposit bd " 
				+ " JOIN FETCH bd.bankDepositDetails bds"
				+ " JOIN FETCH bd.bankAccount ba"
				+ " JOIN FETCH bds.bankReceiptsDetail brsd"
				+ " WHERE ba.bankAccountId = ? AND (bds.chequePresented IS NOT NULL AND bds.chequePresented = 1)"
				+ " AND brsd.chequeDate >= ? AND brsd.chequeDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankDepositContractPayments", query = "SELECT DISTINCT cp FROM ContractPayment cp "
				+ " JOIN FETCH cp.contract c " + " WHERE c.implementation = ?"
				//+ " AND cp.feeType != ?"
				+ " AND cp.isDeposited=0"),

		@org.hibernate.annotations.NamedQuery(name = "getBankDepositContractPaymentInfo", query = "SELECT DISTINCT(cp) FROM ContractPayment cp "
				+ " JOIN FETCH cp.contract c "
				+ " JOIN FETCH c.offer o "
				+ " JOIN FETCH o.tenantOffers to "
				+ " LEFT JOIN FETCH to.company cmp "
				+ " LEFT JOIN FETCH to.person per "
				+ "WHERE cp.contractPaymentId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankDepositInfo", query = "SELECT DISTINCT(bd) FROM BankDeposit bd "
				+ " JOIN FETCH bd.personByCreatedBy pr"
				+ " JOIN FETCH bd.personByDepositerId dpr"
				+ " JOIN FETCH bd.bankDepositDetails bdd"
				+ " JOIN FETCH bd.bankAccount ba"
				+ " LEFT JOIN FETCH bdd.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts brs"
				+ " LEFT JOIN FETCH brs.lookupDetail ld"
				+ " LEFT JOIN FETCH brd.combination bcr"
				+ " WHERE bd.bankDepositId=? "),
				
				
		@org.hibernate.annotations.NamedQuery(name = "getBankDepositDetailInfo", query = "SELECT DISTINCT(bdd) FROM BankDepositDetail bdd "
 				+ " JOIN FETCH bdd.bankDeposit bd"
				+ " JOIN FETCH bd.bankAccount ba"
				+ " LEFT JOIN FETCH bdd.bankReceiptsDetail brd"
 				+ " WHERE bdd.bankDepositDetailId=? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getBankDepositByDepositId", query = "SELECT DISTINCT(bd) FROM BankDeposit bd "
 				+ " JOIN FETCH bd.bankDepositDetails bdd"
 				+ " LEFT JOIN FETCH bdd.bankReceiptsDetail brd"
 				+ " WHERE bd.bankDepositId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllInvoice", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " WHERE iv.implementation = ?" + " AND iv.status != 4"),

		@org.hibernate.annotations.NamedQuery(name = "getAllNonContinuosPurchaseInvoice", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH iv.invoiceDetails ivd"
				+ " LEFT JOIN FETCH ivd.receiveDetail rcvd"
				+ " LEFT JOIN FETCH rcvd.receive recv"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH iv.invoices invoices"
				+ " LEFT JOIN FETCH iv.person ctpr"
				+ " WHERE iv.implementation = ? AND iv.isApprove=?"
				+ " ORDER BY iv.invoiceId,iv.invoiceNumber DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllContinuosPurchaseInvoice", query = "SELECT iv FROM Invoice iv "
				+ " JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " LEFT JOIN FETCH iv.person crpr"
				+ " WHERE iv.implementation = ?"
				+ " AND (iv.processStatus IS NOT NULL)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllInvoiceWithoutImplementation", query = "SELECT iv FROM Invoice iv "
				+ " JOIN FETCH iv.invoiceDetails ivd"
				+ " JOIN FETCH ivd.product ivprd"
				+ " JOIN FETCH iv.receive re "
				+ " JOIN FETCH re.receiveDetails rcd"
				+ " JOIN FETCH rcd.product rprd"
				+ " JOIN FETCH rcd.purchase po"
				+ " JOIN FETCH po.supplier sp"
				+ " JOIN FETCH po.implementation impl"),

		@org.hibernate.annotations.NamedQuery(name = "getAllInvoiceNumber", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " WHERE iv.implementation = ? "
				+ " AND iv.supplier.supplierId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getInvoiceCurrencyById", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " LEFT JOIN FETCH iv.currency cy"
				+ " WHERE iv.invoiceId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getInvoiceById", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " LEFT JOIN FETCH iv.invoiceDetails ivd "
				+ " LEFT JOIN FETCH ivd.product pro"
				+ " LEFT JOIN FETCH pro.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH sp.combination scb"
				+ " LEFT JOIN FETCH scb.accountByNaturalAccountId nlAccount"
				+ " LEFT JOIN FETCH scb.accountByAnalysisAccountId ayAccount"
				+ " LEFT JOIN FETCH sp.creditTerm pt"
				+ " LEFT JOIN FETCH ivd.receiveDetail rcvd"
				+ " LEFT JOIN FETCH rcvd.receive rcv"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy" + " WHERE iv.invoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getChildInvoiceById", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " WHERE iv.invoice.invoiceId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getParentInvoiceById", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " LEFT JOIN FETCH iv.invoice prinv"
				+ " LEFT JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH iv.invoiceDetails ivd"
				+ " LEFT JOIN FETCH ivd.receiveDetail rDt"
				+ " LEFT JOIN FETCH rDt.receive rcv"
				+ " LEFT JOIN FETCH rDt.purchaseDetail prDt"
				+ " LEFT JOIN FETCH prDt.purchase pr"
				+ " LEFT JOIN FETCH pr.creditTerm ct"
				+ " LEFT JOIN FETCH sp.creditTerm cr"
				+ " WHERE iv.invoiceId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getBaseParentInvoiceById", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " LEFT JOIN FETCH iv.invoice prinv"
 				+ " LEFT JOIN FETCH iv.invoiceDetails ivd"
				+ " LEFT JOIN FETCH ivd.receiveDetail rDt"
				+ " LEFT JOIN FETCH rDt.receive rcv"
 				+ " WHERE iv.invoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getContinuousInvoiceById", query = "SELECT DISTINCT(div) FROM Invoice div "
				+ " JOIN FETCH div.invoice iv"
				+ " LEFT JOIN FETCH iv.invoiceDetails ivd "
				+ " LEFT JOIN FETCH ivd.product pro"
				+ " LEFT JOIN FETCH pro.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH div.supplier sp"
				+ " LEFT JOIN FETCH sp.combination scb"
				+ " LEFT JOIN FETCH scb.accountByNaturalAccountId nlAccount"
				+ " LEFT JOIN FETCH scb.accountByAnalysisAccountId ayAccount"
				+ " LEFT JOIN FETCH sp.creditTerm pt"
				+ " LEFT JOIN FETCH ivd.receiveDetail rcvd"
				+ " LEFT JOIN FETCH rcvd.receive rcv"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy"
				+ " WHERE div.invoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierActiveInvoices", query = "SELECT DISTINCT(iv) FROM Invoice iv "
				+ " JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH iv.currency cy"
				+ " LEFT JOIN FETCH iv.invoiceDetails ivd"
				+ " LEFT JOIN FETCH ivd.receiveDetail rcvd"
				+ " LEFT JOIN FETCH rcvd.receive rcv"
				+ " LEFT JOIN FETCH iv.invoice prinv"
				+ " WHERE sp.supplierId = ?"
				+ " AND cy.currencyId = ?"
				+ " AND iv.isApprove = ?"
				+ " AND iv.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getInvoiceDetailById", query = "SELECT DISTINCT iv FROM Invoice iv "
				+ " LEFT JOIN FETCH iv.invoiceDetails divd "
				+ " JOIN FETCH iv.invoice ivv"
				+ " JOIN FETCH ivv.invoiceDetails ivd "
				+ " JOIN FETCH ivd.product pro"
				+ " JOIN FETCH pro.lookupDetailByProductUnit pu"
				+ " JOIN FETCH iv.supplier sp"
				+ " JOIN FETCH sp.combination scb"
				+ " JOIN FETCH scb.accountByNaturalAccountId nlAccount"
				+ " JOIN FETCH scb.accountByAnalysisAccountId ayAccount"
				+ " LEFT JOIN FETCH sp.creditTerm pt"
				+ " LEFT JOIN FETCH ivd.receiveDetail rcvd"
				+ " LEFT JOIN FETCH rcvd.receive rcv"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH sp.company cpy" + " WHERE iv.invoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getInvoiceInfoById", query = "SELECT DISTINCT iv FROM Invoice iv "
				+ " LEFT JOIN FETCH iv.invoiceDetails ivd "
				+ " LEFT JOIN FETCH ivd.product pro"
				+ " LEFT JOIN FETCH pro.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH iv.supplier sp"
				+ " LEFT JOIN FETCH ivd.receiveDetail rcvd"
				+ " LEFT JOIN FETCH rcvd.receive rcv"
				+ " LEFT JOIN FETCH rcvd.purchase purch"
				+ " LEFT JOIN FETCH purch.currency cy"
				+ " LEFT JOIN FETCH rcvd.product prod"
				+ " LEFT JOIN FETCH sp.person pr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH iv.invoice pinv"
				+ " LEFT JOIN FETCH pinv.invoiceDetails pivd "
				+ " LEFT JOIN FETCH pivd.product ppro"
				+ " LEFT JOIN FETCH ppro.lookupDetailByProductUnit ppu"
				+ " LEFT JOIN FETCH pivd.receiveDetail prcvd"
				+ " LEFT JOIN FETCH prcvd.receive prcv"
				+ " LEFT JOIN FETCH prcvd.purchase ppurch"
				+ " LEFT JOIN FETCH ppurch.currency pcy"
				+ " LEFT JOIN FETCH prcvd.product pprod"
				+ " LEFT JOIN FETCH iv.invoices invs"
				+ " LEFT JOIN FETCH pinv.invoices invsparent"
				+ " LEFT JOIN FETCH iv.person ctpr"
				+ " LEFT JOIN FETCH sp.company cpy" + " WHERE iv.invoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getInvoiceDetails", query = "SELECT DISTINCT iv FROM InvoiceDetail iv"
				+ " JOIN FETCH iv.receiveDetail rcvd"
				+ " JOIN FETCH rcvd.receive rcv"
				+ " WHERE iv.invoice.invoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierActiveInvoice", query = "SELECT DISTINCT iv FROM InvoiceDetail iv"
				+ " JOIN FETCH iv.invoice inv"
				+ " JOIN FETCH iv.receiveDetail rcd"
				+ " JOIN FETCH iv.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " JOIN FETCH inv.supplier sp"
				+ " WHERE sp.supplierId = ?"
				+ " AND inv.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPaymentRequest", query = "SELECT DISTINCT pr FROM PaymentRequest pr"
				+ " LEFT JOIN FETCH pr.paymentRequestDetails prd"
				+ " LEFT JOIN FETCH pr.personByPersonId prp"
				+ " LEFT JOIN FETCH pr.personByCreatedBy prc"
				+ " WHERE pr.implementation = ? AND pr.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPaymentRequestById", query = "SELECT DISTINCT pr FROM PaymentRequest pr"
				+ " JOIN FETCH pr.paymentRequestDetails prd"
				+ " JOIN FETCH prd.lookupDetail category"
				+ " JOIN FETCH prd.lookupDetail category"
				+ " LEFT JOIN FETCH pr.personByPersonId prp"
				+ " LEFT JOIN FETCH pr.personByCreatedBy prc"
				+ " WHERE pr.paymentRequestId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRequestDetailById", query = "SELECT prd FROM PaymentRequestDetail prd"
				+ " WHERE prd.paymentRequest.paymentRequestId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "findAllApprovedPaymentRequest", query = "SELECT DISTINCT(pr) FROM PaymentRequest pr"
				+ " JOIN FETCH pr.paymentRequestDetails prd"
				+ " JOIN FETCH prd.lookupDetail category"
				+ " LEFT JOIN FETCH pr.personByPersonId ppr"
				+ " LEFT JOIN FETCH pr.personByCreatedBy pcb"
				+ " WHERE pr.implementation = ?"
				+ " AND prd.paymentMode = ?"
				+ " AND pr.isApprove = ?"
				+ " AND prd.paymentRequestDetailId NOT IN ("
				+ " SELECT dpd.paymentRequestDetail.paymentRequestDetailId"
				+ " FROM prd.directPaymentDetails dpd)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBankTransfer", query = "SELECT DISTINCT(bt) FROM BankTransfer bt"
				+ " JOIN FETCH bt.currency cy"
				+ " JOIN FETCH cy.currencyPool cyp"
				+ " LEFT JOIN FETCH bt.lookupDetail ld"
				+ " JOIN FETCH bt.bankAccountByDebitBankAccount dba"
				+ " JOIN FETCH bt.bankAccountByCreditBankAccount cba"
				+ " WHERE bt.implementation = ? AND bt.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "findBankTransferById", query = "SELECT DISTINCT(bt) FROM BankTransfer bt"
				+ " JOIN FETCH bt.currency cy"
				+ " JOIN FETCH cy.currencyPool cyp"
				+ " LEFT JOIN FETCH bt.lookupDetail ld"
				+ " LEFT JOIN FETCH bt.combination cb"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " JOIN FETCH bt.bankAccountByDebitBankAccount dba"
				+ " JOIN FETCH bt.bankAccountByCreditBankAccount cba"
				+ " JOIN FETCH dba.combination dcb"
				+ " JOIN FETCH cba.combination ccb"
				+ " WHERE bt.bankTransferId = ? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getBankTransferByReference", query = "SELECT DISTINCT(bt) FROM BankTransfer bt" 
				+ " WHERE bt.implementation = ?"
				+ " AND bt.transferNo = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllChequeBooks", query = "SELECT DISTINCT(cb) FROM ChequeBook cb"
				+ " JOIN FETCH cb.bankAccount ba"
				+ " JOIN FETCH ba.bank bk"
				+ " WHERE bk.implementation = ? AND cb.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "findChequeBookById", query = "SELECT DISTINCT(cb) FROM ChequeBook cb"
				+ " JOIN FETCH cb.bankAccount ba"
				+ " JOIN FETCH ba.bank bk"
				+ " JOIN FETCH ba.combination code"
				+ " WHERE cb.chequeBookId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "findAllVoidPayment", query = "SELECT DISTINCT(vp) FROM VoidPayment vp"
 				+ " LEFT JOIN FETCH vp.directPayment dpy"
 				+ " LEFT JOIN FETCH dpy.bankAccount dpyba"
				+ " LEFT JOIN FETCH dpyba.bank dpybk"
				+ " LEFT JOIN FETCH dpyba.chequeBooks dpybacb"
				+ " WHERE vp.implementation = ? AND vp.isApprove=?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getVoidPaymentsByDirectPayments", query = "SELECT DISTINCT(vp) FROM VoidPayment vp"
				+ " LEFT JOIN FETCH vp.directPayment dpy"
				+ " LEFT JOIN FETCH dpy.bankAccount dpyba"
				+ " LEFT JOIN FETCH dpyba.bank dpybk"
				+ " LEFT JOIN FETCH dpyba.chequeBooks dpybacb"
				+ " LEFT JOIN FETCH dpy.supplier spd"
				+ " LEFT JOIN FETCH spd.person prd"
				+ " LEFT JOIN FETCH spd.cmpDeptLocation cdld"
				+ " LEFT JOIN FETCH cdld.company ccd"
				+ " LEFT JOIN FETCH spd.company cynd"
				+ " WHERE vp.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getVoidPaymentById", query = "SELECT DISTINCT(vp) FROM VoidPayment vp"
 				+ " LEFT JOIN FETCH vp.directPayment dpy"
  				+ " LEFT JOIN FETCH dpy.bankAccount dpyba"
				+ " LEFT JOIN FETCH dpyba.bank dpybk"
				+ " LEFT JOIN FETCH dpyba.chequeBooks dpybacb"
				+ " LEFT JOIN FETCH dpy.supplier spd"
				+ " LEFT JOIN FETCH spd.person prd"
				+ " LEFT JOIN FETCH spd.cmpDeptLocation cdld"
				+ " LEFT JOIN FETCH cdld.company ccd"
				+ " LEFT JOIN FETCH spd.company cynd"
 				+ " WHERE vp.voidPaymentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPettyCash", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dp"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId ncb"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId acb"
				+ " LEFT JOIN FETCH pc.pettyCashs child"
				+ " LEFT JOIN FETCH pc.pettyCashDetails pcdetail"
				+ " LEFT JOIN FETCH pcdetail.person dtpr"
				+ " LEFT JOIN FETCH pcdetail.combination cbdt"
				+ " LEFT JOIN FETCH cbdt.accountByCostcenterAccountId ccbdt"
				+ " LEFT JOIN FETCH cbdt.accountByNaturalAccountId ncbdt"
				+ " LEFT JOIN FETCH cbdt.accountByAnalysisAccountId acbdt"
				+ " WHERE pc.implementation = ? AND pc.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPettyCashOnlyExpense", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dp"
				+ " LEFT JOIN FETCH dp.directPayment d"
				+ " LEFT JOIN FETCH d.currency dcr"
				+ " LEFT JOIN FETCH dp.combination dpcb"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH br.currency bcr"
				+ " LEFT JOIN FETCH brd.combination brdcb"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " WHERE pc.implementation = ?" + " AND pc.pettyCashType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllParentPettyCash", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dp"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId ncb"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId acb"
				+ " WHERE pc.pettyCash IS NULL" + " AND pc.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getParentPettyCash", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.pettyCash parent"
				+ " LEFT JOIN FETCH parent.directPaymentDetail dp"
				+ " LEFT JOIN FETCH dp.directPayment d"
				+ " LEFT JOIN FETCH parent.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH parent.personByPersonId pr"
				+ " LEFT JOIN FETCH parent.personByCreatedBy cpr"
				+ " WHERE pc.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBasicPettyCashById", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.personByCreatedBy prcr"
				+ " WHERE pc.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashById", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.pettyCashDetails pcdetail"
				+ " LEFT JOIN FETCH pcdetail.combination expcb"
				+ " LEFT JOIN FETCH pcdetail.person detailpr"
				+ " LEFT JOIN FETCH expcb.accountByCompanyAccountId dtcmpy"
				+ " LEFT JOIN FETCH expcb.accountByCostcenterAccountId dtcsta"
				+ " LEFT JOIN FETCH expcb.accountByNaturalAccountId dtntl"
				+ " LEFT JOIN FETCH expcb.accountByAnalysisAccountId dtaly"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dp"
				+ " LEFT JOIN FETCH dp.directPayment dt"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH br.currency bcy"
				+ " LEFT JOIN FETCH brd.combination bcb"
				+ " LEFT JOIN FETCH dt.currency cy"
				+ " LEFT JOIN FETCH dp.combination dcb"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByCompanyAccountId cmpy"
				+ " LEFT JOIN FETCH cb.accountByCostcenterAccountId csta"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId ntl"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aly"
				+ " LEFT JOIN FETCH pc.pettyCashs pcs"
				+ " LEFT JOIN FETCH pc.pettyCash parentcs"
				+ " LEFT JOIN FETCH pc.personByPersonId prs"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cprs"
				+ " WHERE pc.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashChildById", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dp"
				+ " LEFT JOIN FETCH dp.directPayment dt"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH br.currency bcy"
				+ " LEFT JOIN FETCH brd.combination bcb"
				+ " LEFT JOIN FETCH dt.currency cy"
				+ " LEFT JOIN FETCH dp.combination dcb"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByCompanyAccountId cmpy"
				+ " LEFT JOIN FETCH cb.accountByCostcenterAccountId csta"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId ntl"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aly"
				+ " LEFT JOIN FETCH pc.pettyCashs pcs"
				+ " LEFT JOIN FETCH pc.pettyCash parentcs"
				+ " LEFT JOIN FETCH pc.personByPersonId prs"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cprs"
				+ " WHERE parentcs.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashParent", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.pettyCash parentcs"
				+ " LEFT JOIN FETCH pc.personByPersonId prs"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cprs"
				+ " WHERE pc.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashForPrint", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByCompanyAccountId cmpy"
				+ " LEFT JOIN FETCH cb.accountByCostcenterAccountId csta"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId ntl"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aly"
				+ " LEFT JOIN FETCH pc.personByPersonId prs"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cprs"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " WHERE pc.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashByUseCase", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dp"
				+ " LEFT JOIN FETCH dp.directPayment dt"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH br.currency bcy"
				+ " LEFT JOIN FETCH brd.combination bcb"
				+ " LEFT JOIN FETCH dt.currency cy"
				+ " LEFT JOIN FETCH dp.combination cb"
				+ " LEFT JOIN FETCH pc.pettyCashs pcs"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.pettyCashDetails pcDetails"
				+ " WHERE pc.recordId = ?" + " AND pc.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiptsPettyCash", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH dpd.directPayment dp"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " WHERE pc.implementation = ?"
				+ " AND (pc.pettyCashType = ? OR pc.pettyCashType = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllReceiptsPettyCash", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH dpd.directPayment dp"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.pettyCash parent"
				+ " LEFT JOIN FETCH parent.pettyCashs child"
				+ " WHERE pc.implementation = ?" 
				+ " AND pc.pettyCashType = ?"
				+ " AND pc.closedFlag = ?"),
					
		@org.hibernate.annotations.NamedQuery(name = "getAllNonClosedReceiptsPettyCash", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH dpd.directPayment dp"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.pettyCash parent"
				+ " LEFT JOIN FETCH parent.pettyCashs child"
				+ " WHERE pc.implementation = ?"
				+ " AND pc.pettyCashType = ?"
				+ " AND parent.closedFlag != 1"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashInCarryForward", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH dpd.directPayment dp"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.pettyCash parent"
				+ " LEFT JOIN FETCH parent.pettyCashs child"
				+ " WHERE pc.implementation = ?"
				+ " AND pc.pettyCashType = ?"
				+ " AND pc.closedFlag = ?"
				+ " AND pc.pettyCashId != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiptsPettyCashWithOutSettlementId", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH dpd.directPayment dp"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.pettyCashs pcs"
				+ " WHERE pc.implementation = ?"
				+ " AND pc.pettyCashType = ?"
				+ " AND pc.pettyCashId != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPettyCashByPayments", query = "SELECT pc FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.pettyCash pcs"
				+ " WHERE pc.directPaymentDetail = ?"
				+ " ORDER BY pc.pettyCashId ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPettyCashByReceipts", query = "SELECT pc FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.combination cb"
				+ " LEFT JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " LEFT JOIN FETCH pc.personByPersonId pr"
				+ " LEFT JOIN FETCH pc.personByCreatedBy cpr"
				+ " LEFT JOIN FETCH pc.directPaymentDetail dpd"
				+ " LEFT JOIN FETCH pc.bankReceiptsDetail brd"
				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH pc.pettyCash pcs"
				+ " WHERE pc.bankReceiptsDetail = ?"
				+ " ORDER BY pc.pettyCashId ASC"),

		@org.hibernate.annotations.NamedQuery(name = "getPDCBankReceipts", query = "SELECT DISTINCT(brd) FROM BankReceiptsDetail brd"
 				+ " LEFT JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH br.currency c"
				+ " LEFT JOIN FETCH br.implementation impl"
 				+ " WHERE br.implementation = ?"
 				+ " AND brd.paymentMode = ?"
				+ " AND brd.chequeDate = ?" 
				+ " AND brd.isPdc = 1"
				+ " AND br.isApprove=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllBankReceipts", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " JOIN FETCH br.bankReceiptsDetails brd"
				+ " LEFT JOIN FETCH br.lookupDetail ld"
				+ " LEFT JOIN FETCH br.currency cy"
				+ " LEFT JOIN FETCH cy.currencyPool pl"
				+ " LEFT JOIN FETCH br.supplier sp"
				+ " LEFT JOIN FETCH sp.person spr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation scdl"
				+ " LEFT JOIN FETCH scdl.company scy"
				+ " LEFT JOIN FETCH sp.company scpy"
				+ " LEFT JOIN FETCH br.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cpr"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation ccdl"
				+ " LEFT JOIN FETCH ccdl.company ccy"
				+ " LEFT JOIN FETCH cst.company cpy"
				+ " LEFT JOIN FETCH br.personByPersonId pr"
				+ " LEFT JOIN FETCH br.personByCreatedBy ctpr"
				+ " WHERE br.implementation = ? AND br.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBankReceiptsOnlyReceipts", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " LEFT JOIN FETCH br.currency cy"
				+ " JOIN FETCH br.combination cb"
				+ " JOIN FETCH br.bankReceiptsDetails brd"
				+ " JOIN FETCH brd.combination cbd"
				+ " WHERE br.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPaidBankReceipts", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " JOIN FETCH br.lookupDetail ld"
				+ " LEFT JOIN FETCH br.supplier sp"
				+ " LEFT JOIN FETCH sp.person spr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation scdl"
				+ " LEFT JOIN FETCH scdl.company scy"
				+ " LEFT JOIN FETCH sp.company scpy"
				+ " LEFT JOIN FETCH br.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cpr"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation ccdl"
				+ " LEFT JOIN FETCH ccdl.company ccy"
				+ " LEFT JOIN FETCH cst.company cpy"
				+ " LEFT JOIN FETCH br.personByPersonId pr"
				+ " LEFT JOIN FETCH br.bankReceiptsDetails brd"
				+ " LEFT JOIN FETCH br.currency cy"
				+ " LEFT JOIN FETCH cy.currencyPool pl"
				+ " WHERE br.implementation = ?"
				+ " AND brd.paymentStatus = ?"
				+ " AND brd.bankReceiptsDetailId NOT IN "
				+ "(SELECT dpd.bankReceiptsDetail.bankReceiptsDetailId"
				+ " FROM brd.bankDepositDetails dpd)"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReceiptsByType", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " JOIN FETCH br.lookupDetail ld"
				+ " LEFT JOIN FETCH br.supplier sp"
				+ " LEFT JOIN FETCH sp.person spr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation scdl"
				+ " LEFT JOIN FETCH scdl.company scy"
				+ " LEFT JOIN FETCH sp.company scpy"
				+ " LEFT JOIN FETCH br.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cpr"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation ccdl"
				+ " LEFT JOIN FETCH ccdl.company ccy"
				+ " LEFT JOIN FETCH cst.company cpy"
				+ " LEFT JOIN FETCH br.personByPersonId pr"
				+ " LEFT JOIN FETCH br.bankReceiptsDetails brd"
				+ " WHERE br.implementation = ?"
				+ " AND ld.lookupDetailId = ?"
				+ " AND brd.paymentStatus = ?"
				+ " AND brd.bankReceiptsDetailId NOT IN "
				+ "(SELECT dpd.bankReceiptsDetail.bankReceiptsDetailId"
				+ " FROM brd.bankDepositDetails dpd)"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReceiptsDetailByType", query = "SELECT DISTINCT(brd) FROM BankReceiptsDetail brd"
				+ " JOIN FETCH brd.bankReceipts br"
				+ " JOIN FETCH br.lookupDetail ld"
				+ " LEFT JOIN FETCH br.supplier sp"
				+ " LEFT JOIN FETCH sp.person spr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation scdl"
				+ " LEFT JOIN FETCH scdl.company scy"
				+ " LEFT JOIN FETCH sp.company scpy"
				+ " LEFT JOIN FETCH br.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cpr"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation ccdl"
				+ " LEFT JOIN FETCH ccdl.company ccy"
				+ " LEFT JOIN FETCH cst.company cpy"
				+ " LEFT JOIN FETCH br.personByPersonId pr"
				+ " WHERE br.implementation = ?"
				+ " AND ld.lookupDetailId = ?"
				+ " AND brd.paymentStatus = ?"
				+ " AND brd.bankReceiptsDetailId NOT IN "
				+ "(SELECT dpd.bankReceiptsDetail.bankReceiptsDetailId"
				+ " FROM brd.bankDepositDetails dpd)"),

		@org.hibernate.annotations.NamedQuery(name = "findByBankReceiptsId", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " JOIN FETCH br.bankReceiptsDetails brd"
				+ " JOIN FETCH br.currency cy"
				+ " JOIN FETCH cy.currencyPool cycpl"
				+ " JOIN FETCH br.lookupDetail brld"
				+ " JOIN FETCH br.combination cb"
				+ " JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " JOIN FETCH brd.combination bcb"
				+ " JOIN FETCH bcb.accountByNaturalAccountId bna"
				+ " LEFT JOIN FETCH bcb.accountByAnalysisAccountId baa"
				+ " LEFT JOIN FETCH br.supplier sp"
				+ " LEFT JOIN FETCH sp.person spr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation scdl"
				+ " LEFT JOIN FETCH scdl.company scy"
				+ " LEFT JOIN FETCH sp.company scpy"
				+ " LEFT JOIN FETCH br.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cpr"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation ccdl"
				+ " LEFT JOIN FETCH ccdl.company ccy"
				+ " LEFT JOIN FETCH cst.company cpy"
				+ " LEFT JOIN FETCH br.personByPersonId pr"
				+ " WHERE br.bankReceiptsId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReceiptsByRecordIdUsecase", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " WHERE br.recordId = ?" + " AND br.useCase = ?"),

		@org.hibernate.annotations.NamedQuery(name = "findPDCBankReceiptsById", query = "SELECT DISTINCT(br) FROM BankReceipts br"
				+ " JOIN FETCH br.bankReceiptsDetails brd"
				+ " JOIN FETCH br.currency cy"
				+ " JOIN FETCH cy.currencyPool cpy"
				+ " JOIN FETCH br.lookupDetail brld"
				+ " JOIN FETCH br.combination cb"
				+ " JOIN FETCH cb.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId aa"
				+ " JOIN FETCH brd.combination bcb"
				+ " JOIN FETCH bcb.accountByNaturalAccountId bna"
				+ " LEFT JOIN FETCH bcb.accountByAnalysisAccountId baa"
				+ " LEFT JOIN FETCH br.supplier sp"
				+ " LEFT JOIN FETCH sp.person spr"
				+ " LEFT JOIN FETCH sp.cmpDeptLocation scdl"
				+ " LEFT JOIN FETCH scdl.company scy"
				+ " LEFT JOIN FETCH sp.company scpy"
				+ " LEFT JOIN FETCH br.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cpr"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation ccdl"
				+ " LEFT JOIN FETCH ccdl.company ccy"
				+ " LEFT JOIN FETCH cst.company cpy"
				+ " LEFT JOIN FETCH br.personByPersonId pr"
				+ " WHERE br.bankReceiptsId = ?" + " AND brd.isPdc = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReceiptsDetailByReceipts", query = "SELECT brd FROM BankReceiptsDetail brd"
				+ " WHERE brd.bankReceipts = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReceiptsDetailById", query = "SELECT brd FROM BankReceiptsDetail brd"
				+ " JOIN FETCH brd.bankReceipts br"
				+ " JOIN FETCH brd.combination cb"
				+ " LEFT JOIN FETCH br.currency cy"
				+ " LEFT JOIN FETCH br.lookupDetail ld"
				+ " LEFT JOIN FETCH brd.lookupDetail ld"
				+ " WHERE brd.bankReceiptsDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBankReceiptDetails", query = "SELECT DISTINCT(brd) FROM BankReceiptsDetail brd"
				+ " JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH brd.lookupDetail ld"
				+ " WHERE br.implementation = ?" + " AND brd.paymentMode = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnDepositedCashReceiptDetails", query = "SELECT DISTINCT(brd) FROM BankReceiptsDetail brd"
				+ " JOIN FETCH brd.bankReceipts br"
				+ " LEFT JOIN FETCH brd.lookupDetail ld"
				+ " LEFT JOIN FETCH br.personByCreatedBy prcr"
				+ " WHERE br.implementation = ?"
				+ " AND (brd.isDeposited IS NULL OR brd.isDeposited = 0) "
				+ " AND brd.paymentMode = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPDCBankReceipts", query = "SELECT DISTINCT(brd) FROM BankReceiptsDetail brd"
				+ " JOIN FETCH brd.bankReceipts br"
				+ " JOIN FETCH brd.combination cb"
				+ " LEFT JOIN FETCH br.currency cy"
				+ " LEFT JOIN FETCH br.lookupDetail ld"
				+ " WHERE br.implementation = ?" + " AND brd.isPdc = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAdvanceIssued", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.pettyCashs pycs"
				+ " WHERE pc.implementation = ?"
				+ " AND pc.pettyCash IS NOT NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getAdvanceIssuedAndGiven", query = "SELECT DISTINCT(pc) FROM PettyCash pc"
				+ " LEFT JOIN FETCH pc.pettyCashs pycs"
				+ " WHERE pc.implementation = ?"
				+ " AND (pc.pettyCashType = ? OR pc.pettyCashType = ?)"
				+ " AND pc.pettyCash IS NOT NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getPettyCashDetailByPettyId", query = "SELECT DISTINCT(pc) FROM PettyCashDetail pc"
				+ " JOIN FETCH pc.pettyCash p" + " WHERE p.pettyCashId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllReconciliationAdjustments", query = "SELECT rd FROM ReconciliationAdjustment rd"
				+ " JOIN FETCH rd.bankAccount ba"
				+ " WHERE rd.implementation = ? AND rd.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getReconciliationAdjustmentDetails", query = "SELECT rd FROM ReconciliationAdjustmentDetail rd"
				+ " WHERE rd.reconciliationAdjustment.reconciliationAdjustmentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReconciliationAdjustment", query = "SELECT rd FROM ReconciliationAdjustment rd"
				+ " JOIN FETCH rd.reconciliationAdjustmentDetails rad"
				+ " WHERE rd.reconciliationAdjustmentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReconciliationAdjustmentById", query = "SELECT DISTINCT(rd) FROM ReconciliationAdjustment rd"
				+ " JOIN FETCH rd.bankAccount ba"
				+ " JOIN FETCH ba.bank b"
				+ " JOIN FETCH rd.reconciliationAdjustmentDetails rad"
				+ " JOIN FETCH rad.combination cb"
				+ " JOIN FETCH cb.accountByCompanyAccountId compy"
				+ " JOIN FETCH cb.accountByCostcenterAccountId cost"
				+ " JOIN FETCH cb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH cb.accountByBuffer1AccountId buffer1"
				+ " LEFT JOIN FETCH cb.accountByBuffer2AccountId buffer2"
				+ " WHERE rd.reconciliationAdjustmentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllBankReconciliations", query = "SELECT DISTINCT(br) FROM BankReconciliation br"
				+ " JOIN FETCH br.bankAccount ba"
				+ " WHERE br.implementation = ? AND br.isApprove=?"), 
				
		@org.hibernate.annotations.NamedQuery(name = "getBankReconciliationByBankAccount", query = "SELECT DISTINCT(br) FROM BankReconciliation br"
				+ " JOIN FETCH br.bankAccount ba"
 				+ " WHERE ba.bankAccountId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReconciliedTransaction", query = "SELECT DISTINCT(br) FROM BankReconciliation br"
				+ " JOIN FETCH br.bankReconciliationDetails brd"
				+ " WHERE br.implementation = ?"
				+ " AND br.reconciliationDate >=?"
				+ " AND br.statementEndDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReconciliation", query = "SELECT DISTINCT(br) FROM BankReconciliation br"
				+ " JOIN FETCH br.bankAccount ba"
				+ " JOIN FETCH ba.bank b"
				+ " JOIN FETCH ba.combination cb"
				+ " JOIN FETCH cb.accountByCompanyAccountId compy"
				+ " JOIN FETCH cb.accountByCostcenterAccountId cost"
				+ " JOIN FETCH cb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId analysis"
				+ " JOIN FETCH br.bankReconciliationDetails brd"
				+ " WHERE br.bankReconciliationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getBankReconDetailsWithoutTrans", query = "SELECT DISTINCT(br) FROM BankReconciliationDetail br"
				+ " JOIN FETCH br.bankReconciliation b"
				+ " WHERE b.implementation = ?"
				+ " AND br.recordId NOT IN(SELECT td.transactionDetailId FROM TransactionDetail td"
				+ " WHERE td.transaction.transactionId IN (SELECT t.transactionId FROM Transaction t WHERE t.implementation = ? AND t.useCase = 'DirectPayment'))"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCreditTerms", query = "SELECT DISTINCT(ct) FROM CreditTerm ct"
				+ " WHERE ct.implementation = ? " + " AND ct.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSupplierCreditTerms", query = "SELECT DISTINCT(ct) FROM CreditTerm ct"
				+ " WHERE ct.implementation = ? "
				+ " AND ct.isSupplier = true"
				+ " AND ct.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCustomerCreditTerms", query = "SELECT DISTINCT(ct) FROM CreditTerm ct"
				+ " WHERE ct.implementation = ? "
				+ " AND ct.isSupplier = false" + " AND ct.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCreditTermDetails", query = "SELECT DISTINCT(c) FROM CreditTerm c"
				+ " WHERE c.creditTermId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getLederCombinationById", query = "SELECT DISTINCT(l) FROM Ledger l"
				+ " WHERE l.combination.combinationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllApprovedCustomers", query = "SELECT DISTINCT(c) FROM Customer c"
				+ " LEFT JOIN FETCH c.personByPersonId pr"
				+ " LEFT JOIN FETCH c.company cmp"
				+ " LEFT JOIN FETCH c.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH c.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH c.creditTerm ct"
				+ " LEFT JOIN FETCH c.combination comb"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " WHERE c.implementation = ? " + " AND c.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCustomers", query = "SELECT DISTINCT(c) FROM Customer c"
				+ " LEFT JOIN FETCH c.personByPersonId pr"
				+ " LEFT JOIN FETCH c.company cmp"
				+ " LEFT JOIN FETCH c.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH c.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH c.creditTerm ct"
				+ " LEFT JOIN FETCH c.combination comb"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " WHERE c.implementation = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllPOSCustomers", query = "SELECT DISTINCT(c) FROM Customer c"
				+ " LEFT JOIN FETCH c.personByPersonId pr"
				+ " LEFT JOIN FETCH c.company cmp"
				+ " LEFT JOIN FETCH c.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " WHERE c.implementation = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCustomersByReceipts", query = "SELECT DISTINCT(c) FROM Customer c"
				+ " LEFT JOIN FETCH c.personByPersonId pr"
				+ " LEFT JOIN FETCH c.company cmp"
				+ " LEFT JOIN FETCH c.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH c.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH c.creditTerm ct"
				+ " WHERE c.implementation = ? "
				+ " AND c.customerId IN(SELECT br.customer.customerId FROM c.bankReceiptses br)"),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerDetails", query = "SELECT DISTINCT (c) FROM Customer c"
				+ " LEFT JOIN FETCH c.shippingDetails sd"
				+ " LEFT JOIN FETCH c.personByPersonId pr"
				+ " LEFT JOIN FETCH c.company cmp"
				+ " LEFT JOIN FETCH c.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cy"
				+ " LEFT JOIN FETCH c.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH c.creditTerm ct"
				+ " LEFT JOIN FETCH c.combination combination"
				+ " WHERE c.customerId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerAccount", query = "SELECT DISTINCT (c) FROM Customer c"
				+ " LEFT JOIN FETCH c.combination cb"
				+ " WHERE c.customerId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerShippingSite", query = "SELECT DISTINCT(sd) FROM ShippingDetail sd"
				+ " WHERE sd.customer.customerId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllCustomerQuotation", query = "SELECT DISTINCT(cq) FROM CustomerQuotation cq"
				+ " LEFT JOIN FETCH cq.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cmp"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cs.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH cs.personByPersonId ppr"
				+ " LEFT JOIN FETCH cs.creditTerm creditT"
				+ " WHERE cq.implementation = ? " + " AND cq.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getNonSalesOrderCustomerQuotation", query = "SELECT DISTINCT(cq) FROM CustomerQuotation cq"
				+ " LEFT JOIN FETCH cq.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cmp"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cs.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH cs.personByPersonId ppr"
				+ " WHERE cq.implementation = ? " + " AND cq.expiryDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerQuotationById", query = "SELECT DISTINCT (cq) FROM CustomerQuotation cq"
				+ " LEFT JOIN FETCH cq.lookupDetailByShippingTerm ship"
				+ " LEFT JOIN FETCH cq.lookupDetailByShippingMethod met"
				+ " LEFT JOIN FETCH cq.person prs"
				+ " LEFT JOIN FETCH cq.customer cs"
				+ " LEFT JOIN FETCH cs.shippingDetails sd"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cs.personBySaleRepresentative psr"
				+ " LEFT JOIN FETCH cs.personByPersonId ppr"
				+ " LEFT JOIN FETCH cq.customerQuotationCharges cqc"
				+ " LEFT JOIN FETCH cqc.lookupDetail ld"
				+ " JOIN FETCH cq.customerQuotationDetails cqd"
				+ " JOIN FETCH cqd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH cqd.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH cqd.store str"
				+ " LEFT JOIN FETCH cq.creditTerm crditT"
				+ " WHERE cq.customerQuotationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerQuotation", query = "SELECT DISTINCT (cq) FROM CustomerQuotation cq"
				+ " JOIN FETCH cq.customerQuotationDetails cqd"
				+ " LEFT JOIN FETCH cq.customerQuotationCharges cqc"
				+ " WHERE cq.customerQuotationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerQuotationDetailById", query = "SELECT DISTINCT(cqd) FROM CustomerQuotationDetail cqd"
				+ " WHERE cqd.customerQuotation.customerQuotationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerQuotationChargesById", query = "SELECT DISTINCT(cqc) FROM CustomerQuotationCharge cqc"
				+ " WHERE cqc.customerQuotation.customerQuotationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllSalesOrder", query = "SELECT DISTINCT(so) FROM SalesOrder so"
				+ " JOIN FETCH so.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " LEFT JOIN FETCH so.person rp"
				+ " LEFT JOIN FETCH so.lookupDetailByOrderType ot"
				+ " LEFT JOIN FETCH so.salesDeliveryNotes sdns"
				+ " WHERE so.implementation = ? " + " AND so.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnDeliveredSalesOrder", query = "SELECT DISTINCT(so) FROM SalesOrder so"
				+ " JOIN FETCH so.salesOrderDetails sod"
				+ " JOIN FETCH so.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " LEFT JOIN FETCH so.person rp"
				+ " LEFT JOIN FETCH so.lookupDetailByOrderType ot"
				+ " LEFT JOIN FETCH so.creditTerm creditT"
				+ " WHERE so.implementation = ? "
				+ " AND so.isApprove = ?"
				+ " AND so.salesOrderId NOT IN "
				+ " (SELECT sd.salesOrder.salesOrderId FROM SalesDeliveryNote sd WHERE sd.salesOrder IS NOT NULL)"
				+ " OR so.continuousSales =1"),

		@org.hibernate.annotations.NamedQuery(name = "getBasicSalesOrder", query = "SELECT DISTINCT(so) FROM SalesOrder so"
				+ " JOIN FETCH so.currency cy"
				+ " WHERE so.salesOrderId = ? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getSalesOrder", query = "SELECT DISTINCT(so) FROM SalesOrder so"
				+ " JOIN FETCH so.customer cs"
				+ " JOIN FETCH cs.shippingDetails sd"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cscmpy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH so.person rp"
				+ " LEFT JOIN FETCH so.lookupDetailByShippingTerm ship"
				+ " LEFT JOIN FETCH so.lookupDetailByOrderType otype"
				+ " LEFT JOIN FETCH so.lookupDetailByShippingMethod met"
				+ " LEFT JOIN FETCH so.salesOrderCharges soc"
				+ " JOIN FETCH so.salesOrderDetails sod"
				+ " LEFT JOIN FETCH so.customerQuotation cq"
				+ " JOIN FETCH sod.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH sod.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH soc.lookupDetail ld"
				+ " LEFT JOIN FETCH so.salesDeliveryNotes sdn"
				+ " LEFT JOIN FETCH so.creditTerm creditT"
				+ " WHERE so.salesOrderId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesOrderByProject", query = "SELECT DISTINCT(so) FROM SalesOrder so"
				+ " LEFT JOIN FETCH so.customer cs"
				+ " LEFT JOIN FETCH cs.shippingDetails sd"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cscmpy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH so.person rp"
				+ " LEFT JOIN FETCH so.lookupDetailByShippingTerm ship"
				+ " LEFT JOIN FETCH so.lookupDetailByOrderType otype"
				+ " LEFT JOIN FETCH so.lookupDetailByShippingMethod met"
				+ " LEFT JOIN FETCH so.salesOrderCharges soc"
				+ " LEFT JOIN FETCH so.salesOrderDetails sod"
				+ " LEFT JOIN FETCH so.customerQuotation cq"
				+ " LEFT JOIN FETCH sod.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH sod.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH soc.lookupDetail ld"
				+ " LEFT JOIN FETCH so.salesDeliveryNotes sdn"
				+ " LEFT JOIN FETCH so.creditTerm creditT"
				+ " LEFT JOIN FETCH so.project prj"
				+ " WHERE prj.projectId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesOrderInfo", query = "SELECT DISTINCT(so) FROM SalesOrder so"
				+ " JOIN FETCH so.salesOrderDetails sod"
				+ " LEFT JOIN FETCH so.salesOrderCharges soc"
				+ " WHERE so.salesOrderId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesOrderDetailBySalesOrderId", query = "SELECT sod FROM SalesOrderDetail sod"
				+ " WHERE sod.salesOrder.salesOrderId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesOrderChargesBySalesOrderId", query = "SELECT soc FROM SalesOrderCharge soc"
				+ " WHERE soc.salesOrder.salesOrderId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllDeliveryNotes", query = "SELECT DISTINCT(do) FROM SalesDeliveryNote do"
				+ " JOIN FETCH do.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " LEFT JOIN FETCH do.personBySalesPersonId rp"
				+ " LEFT JOIN FETCH do.salesDeliveryDetails sdds"
				+ " LEFT JOIN FETCH sdds.salesInvoiceDetails salesInvoice"
				+ " WHERE do.implementation = ? " + " AND do.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getLedgerFiscalsWithLedger", query = "SELECT ld FROM LedgerFiscal ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " LEFT JOIN FETCH ld.period p"
				+ " LEFT JOIN FETCH p.calendar cl"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.period.periodId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getBasicSalesDeliveryNote", query = "SELECT DISTINCT(do) FROM SalesDeliveryNote do"
				+ " LEFT JOIN FETCH do.currency cy"
				+ " WHERE do.salesDeliveryNoteId = ? "),
					
		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryNoteInfo", query = "SELECT DISTINCT(do) FROM SalesDeliveryNote do"
				+ " LEFT JOIN FETCH do.salesDeliveryCharges sdc"
				+ " JOIN FETCH do.salesDeliveryDetails dod"
				+ " JOIN FETCH dod.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdUnit"
				+ " LEFT JOIN FETCH do.currency curr"
				+ " LEFT JOIN FETCH curr.currencyPool currPool"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH dod.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH do.salesDeliveryPacks sdp"
				+ " LEFT JOIN FETCH do.creditTerm crditT"
				+ " LEFT JOIN FETCH do.customer cs"
				+ " LEFT JOIN FETCH dod.creditDetails crds"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cmpy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmpl"
				+ " LEFT JOIN FETCH dod.salesInvoiceDetails invoice"
				+ " LEFT JOIN FETCH dod.productPackageDetail packDetail"
				+ " LEFT JOIN FETCH packDetail.lookupDetail unitld"
				+ " WHERE do.salesDeliveryNoteId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryDetailByDeliveryId", query = "SELECT do FROM SalesDeliveryDetail do"
				+ " LEFT JOIN FETCH do.product prd"
				+ " LEFT JOIN FETCH do.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " WHERE do.salesDeliveryNote.salesDeliveryNoteId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryDetailBySalesInvoice", query = "SELECT Distinct(do) FROM SalesDeliveryDetail do"
				+ " LEFT JOIN FETCH do.salesInvoiceDetails invdeta"
				+ " LEFT JOIN FETCH invdeta.salesInvoice inv"
				+ " LEFT JOIN FETCH do.salesDeliveryNote note"
				+ " WHERE inv.salesInvoiceId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryDetailByDetailId", query = "SELECT do FROM SalesDeliveryDetail do"
				+ " LEFT JOIN FETCH do.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " JOIN FETCH do.product prd"
				+ " WHERE do.salesDeliveryDetailId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryChargesByDeliveryId", query = "SELECT do FROM SalesDeliveryCharge do"
				+ " WHERE do.salesDeliveryNote.salesDeliveryNoteId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryPackByDeliveryId", query = "SELECT do FROM SalesDeliveryPack do"
				+ " WHERE do.salesDeliveryNote.salesDeliveryNoteId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryNote", query = "SELECT DISTINCT(do) FROM SalesDeliveryNote do"
				+ " JOIN FETCH do.customer cs"
				+ " JOIN FETCH cs.shippingDetails sd"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cmpy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmpl"
				+ " LEFT JOIN FETCH do.personBySalesPersonId rp"
				+ " LEFT JOIN FETCH do.lookupDetailByShippingTerm ship"
				+ " LEFT JOIN FETCH do.lookupDetailByShippingMethod met"
				+ " LEFT JOIN FETCH do.salesDeliveryCharges sdc"
				+ " JOIN FETCH do.salesDeliveryDetails dod"
				+ " LEFT JOIN FETCH dod.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH do.salesDeliveryPacks sdp"
				+ " LEFT JOIN FETCH do.salesOrder so"
				+ " JOIN FETCH dod.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH dod.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH sdc.lookupDetail ld"
				+ " LEFT JOIN FETCH do.creditTerm creditT"
				+ " WHERE do.salesDeliveryNoteId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryWithInvoiceDetails", query = "SELECT DISTINCT(do) FROM SalesDeliveryNote do"
				+ " JOIN FETCH do.currency cr"
				+ " JOIN FETCH cr.currencyPool crp"
				+ " JOIN FETCH do.salesDeliveryDetails dod"
				+ " LEFT JOIN FETCH dod.productPackageDetail packdetail"
				+ " JOIN FETCH dod.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH dod.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " JOIN FETCH do.customer cs"
				+ " LEFT JOIN FETCH dod.creditDetails crds"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.company cmpy"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmpl"
				+ " LEFT JOIN FETCH dod.salesInvoiceDetails invoice"
				+ " WHERE do.salesDeliveryNoteId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionDetailsByCombinationId", query = "SELECT  DISTINCT(t) FROM TransactionDetail t"
				+ " LEFT JOIN FETCH t.combination c"
				+ " LEFT JOIN FETCH t.transaction tr "
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na "
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId ay "
				+ " WHERE c.combinationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionByCombinationAndNonUsecase", query = "SELECT  DISTINCT(t) FROM TransactionDetail t"
				+ " LEFT JOIN FETCH t.combination c"
				+ " LEFT JOIN FETCH t.transaction tr "
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na "
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId ay "
				+ " LEFT JOIN FETCH tr.person pr"
				+ " WHERE c.combinationId = ? " + " AND tr.useCase != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionDetailsByCombinationAndDate", query = "SELECT DISTINCT(t) FROM TransactionDetail t"
				+ " LEFT JOIN FETCH t.combination c"
				+ " LEFT JOIN FETCH t.transaction tr "
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na "
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId ay "
				+ " WHERE c.combinationId = ? "
				+ " AND tr.transactionTime >= ?  AND tr.transactionTime <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTransactionDetailsOfProduct", query = "SELECT  DISTINCT(t) FROM TransactionDetail t"
				+ " LEFT JOIN FETCH t.combination c"
				+ " LEFT JOIN FETCH t.transaction tr "
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na "
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId an "
				+ " LEFT JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " WHERE product_id = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getNaturalLedgerList", query = "SELECT DISTINCT(l) FROM Ledger l"
				+ " JOIN FETCH l.combination c"
				+ " WHERE c.combinationId IN(SELECT cc.combinationId FROM Combination cc"
				+ " WHERE cc.accountByNaturalAccountId IN (SELECT a.accountId FROM Account a"
				+ " WHERE a.implementation = ?))"),

		@org.hibernate.annotations.NamedQuery(name = "getAnalysisLedgerList", query = "SELECT DISTINCT(l) FROM Ledger l"
				+ " JOIN FETCH l.combination c"
				+ " WHERE c.combinationId IN(SELECT cc.combinationId FROM Combination cc"
				+ " WHERE cc.accountByAnalysisAccountId IN (SELECT a.accountId FROM Account a"
				+ " WHERE a.implementation = ?))"),

		@org.hibernate.annotations.NamedQuery(name = "getProductsByItemType", query = "SELECT DISTINCT(p) FROM Product p "
				+ "LEFT JOIN FETCH p.lookupDetailByProductUnit pu "
				+ "LEFT JOIN FETCH p.lookupDetailByItemNature lookup "
				+ "LEFT JOIN FETCH p.combinationByInventoryAccount ia "
				+ "LEFT JOIN FETCH p.combinationByExpenseAccount ea "
				+ "WHERE p.implementation=? AND p.itemType=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCraftProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ "LEFT JOIN FETCH p.lookupDetailByProductUnit pu "
				+ "LEFT JOIN FETCH p.lookupDetailByItemNature lookup "
				+ "WHERE p.implementation=? AND (p.itemSubType=? OR p.itemSubType= ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getFinsishedGoodsProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ "LEFT JOIN FETCH p.lookupDetailByProductUnit pu "
				+ "LEFT JOIN FETCH p.lookupDetailByItemNature lookup "
				+ "WHERE p.implementation=? AND p.itemSubType=?"),

		@org.hibernate.annotations.NamedQuery(name = "getServiceProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit pu "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature lookup "
				+ " LEFT JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE p.implementation=? " + " AND p.itemSubType=?"),

		@org.hibernate.annotations.NamedQuery(name = "getNonCraftServiceProductDetails", query = "SELECT DISTINCT(p) FROM Product p "
				+ " LEFT JOIN FETCH p.lookupDetailByProductUnit pu "
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature lookup "
				+ " LEFT JOIN FETCH p.productPricingDetails ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE p.implementation=? "
				+ " AND p.itemType = ?"
				+ " AND p.itemSubType != ?" + " AND p.itemSubType != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getServiceAndCraftProducts", query = "SELECT DISTINCT(p) FROM Product p "
				+ " WHERE p.implementation=? "
				+ " AND (p.itemSubType=? OR p.itemSubType=?)"),

		@org.hibernate.annotations.NamedQuery(name = "getProductCodeByBarCode", query = "SELECT p FROM Product p "
				+ "WHERE p.implementation=? AND p.barCode=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLederByCombinationIdWithChilds", query = "SELECT ld FROM Ledger ld"
				+ " LEFT JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.combination.combinationId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllSalesInvoice", query = "SELECT DISTINCT(si) FROM SalesInvoice si "
				+ " JOIN FETCH si.customer cs"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " LEFT JOIN FETCH si.salesInvoice salesInvoice"
				+ " WHERE si.implementation = ? " + " AND si.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesInvoiceByInvoice", query = "SELECT si FROM SalesInvoice si "
				+ " LEFT JOIN FETCH si.salesInvoice sinv"
				+ " WHERE sinv.salesInvoiceId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllContiniosSalesInvoice", query = "SELECT si FROM SalesInvoice si "
				+ " JOIN FETCH si.customer cs"
				+ " LEFT JOIN FETCH si.salesInvoice sii"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " WHERE si.implementation = ? "
				+ " AND si.isApprove = ?"
				+ " AND (si.processStatus IS NOT NULL)"),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerActiveNotes", query = "SELECT DISTINCT(sdd) FROM SalesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.salesDeliveryNote sd"
				+ " JOIN FETCH sd.customer cus"
				+ " LEFT JOIN FETCH sdd.salesInvoiceDetails sinv"
				+ " LEFT JOIN FETCH sdd.creditDetails crds"
				+ " JOIN FETCH sdd.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH sdd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " WHERE sd.statusNote = 1"
				+ " AND cus.customerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerActiveInvoices", query = "SELECT DISTINCT(iv) FROM SalesInvoice iv "
				+ " JOIN FETCH iv.customer cst"
				+ " LEFT JOIN FETCH iv.currency cy"
				+ " WHERE cst.customerId = ?"
				+ " AND cy.currencyId = ?"
				+ " AND iv.status = ?"
				+ " AND iv.isApprove = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getNonInvoicedDeliveryNotes", query = "SELECT DISTINCT(sdd) FROM SalesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.salesDeliveryNote sd"
				+ " JOIN FETCH sd.customer cus"
				+ " JOIN FETCH sdd.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH sdd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH sdd.creditDetails crd"
				+ " LEFT JOIN FETCH sd.currency cy"
				+ " WHERE sd.statusNote = 1"
				+ " AND cus.customerId = ?"
				+ " AND cy.currencyId = ?"
				+ " AND sd.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getNonInvoicedDeliveryNotesEdit", query = "SELECT DISTINCT(sdd) FROM SalesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.salesDeliveryNote sd"
				+ " JOIN FETCH sd.customer cus"
				+ " JOIN FETCH sdd.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH sdd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str"
				+ " LEFT JOIN FETCH sdd.creditDetails crd"
				+ " LEFT JOIN FETCH sd.currency cy"
				+ " LEFT JOIN FETCH sdd.salesInvoiceDetails  invDetail"
				+ " LEFT JOIN FETCH invDetail.salesInvoice  inv"
				+ " WHERE (sd.statusNote = 1 or inv.salesInvoiceId=? )" 
				+ " AND cus.customerId = ?"
				+ " AND cy.currencyId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCustomerNotes", query = "SELECT DISTINCT(sdd) FROM SalesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.salesDeliveryNote sd"
				+ " JOIN FETCH sd.customer cus"
				+ " LEFT JOIN FETCH sdd.salesInvoiceDetails sinv"
				+ " LEFT JOIN FETCH sdd.creditDetails crds"
				+ " JOIN FETCH sdd.product prd"
				+ " JOIN FETCH prd.lookupDetailByProductUnit pu"
				+ " LEFT JOIN FETCH sdd.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rack"
				+ " LEFT JOIN FETCH rack.aisle ae"
				+ " LEFT JOIN FETCH ae.store str" + " WHERE cus.customerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryBySalesOrder", query = "SELECT DISTINCT(sd) FROM SalesDeliveryNote sd"
				+ " JOIN FETCH sd.salesDeliveryDetails sdd"
				+ " JOIN FETCH sdd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH sdd.productPackageDetail packdetail"
				+ " WHERE sd.salesOrder.salesOrderId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryByProject", query = "SELECT DISTINCT(sd) FROM SalesDeliveryNote sd"
				+ " JOIN FETCH sd.salesDeliveryDetails sdd"
				+ " JOIN FETCH sdd.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit prdunit"
				+ " LEFT JOIN FETCH sdd.productPackageDetail packdetail"
				+ " LEFT JOIN FETCH sd.project project"
				+ " WHERE project.projectId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryDetailByOrderDetail", query = "SELECT DISTINCT(sd) FROM SalesDeliveryDetail sd"
				+ " WHERE sd.salesOrderDetail.salesOrderDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesInvoiceDetailByInvoiceId", query = "SELECT sid FROM SalesInvoiceDetail sid"
				+ " WHERE sid.salesInvoice.salesInvoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesInvoiceDetailByInvoiceDetailId", query = "SELECT sid FROM SalesInvoiceDetail sid"
				+ " JOIN FETCH sid.salesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.salesDeliveryNote sdn"
				+ " WHERE sid.salesInvoice.salesInvoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesInvoiceById", query = "SELECT DISTINCT(si) FROM SalesInvoice si"
				+ " LEFT JOIN FETCH si.salesInvoiceDetails sid"
				+ " LEFT JOIN FETCH sid.salesDeliveryDetail sdd"
				+ " LEFT JOIN FETCH sdd.salesInvoiceDetails sddinvoice"
				+ " LEFT JOIN FETCH sdd.product product"
				+ " LEFT JOIN FETCH product.lookupDetailByProductUnit unitLook"
				+ " LEFT JOIN FETCH sdd.shelf shelf"
				+ " LEFT JOIN FETCH sdd.salesDeliveryNote sd"
				+ " LEFT JOIN FETCH si.customer cus"
				+ " LEFT JOIN FETCH cus.company cpy"
				+ " LEFT JOIN FETCH cus.personByPersonId"
				+ " LEFT JOIN FETCH cus.cmpDeptLocation"
				+ " LEFT JOIN FETCH si.creditTerm ct"
				+ " LEFT JOIN FETCH si.currency curr"
				+ " LEFT JOIN FETCH curr.currencyPool cpool"
				+ " LEFT JOIN FETCH cus.combination cb"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH si.salesInvoices sinv"
				+ " LEFT JOIN FETCH si.salesInvoice parentInv"
				+ " LEFT JOIN FETCH parentInv.salesInvoices parentInvces"
				+ " LEFT JOIN FETCH parentInv.salesInvoiceDetails parentInvDetail"
				+ " LEFT JOIN FETCH parentInvDetail.salesDeliveryDetail parentsdd"
				+ " LEFT JOIN FETCH parentsdd.product productparent"
				+ " LEFT JOIN FETCH parentsdd.shelf productshelf"
				+ " LEFT JOIN FETCH si.person invcr"
				+ " WHERE si.salesInvoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesInvoiceDetailById", query = "SELECT DISTINCT(si) FROM SalesInvoice si"
				+ " JOIN FETCH si.salesInvoice psi"
				+ " LEFT JOIN FETCH psi.salesInvoiceDetails sid"
				+ " LEFT JOIN FETCH sid.salesDeliveryDetail sdd"
				+ " LEFT JOIN FETCH sdd.product product"
				+ " LEFT JOIN FETCH sdd.shelf shelf"
				+ " LEFT JOIN FETCH sdd.salesDeliveryNote sd"
				+ " LEFT JOIN FETCH si.customer cus"
				+ " LEFT JOIN FETCH cus.company cpy"
				+ " LEFT JOIN FETCH cus.personByPersonId"
				+ " LEFT JOIN FETCH cus.cmpDeptLocation"
				+ " LEFT JOIN FETCH si.creditTerm ct"
				+ " LEFT JOIN FETCH si.currency curr"
				+ " LEFT JOIN FETCH cus.combination cb"
				+ " LEFT JOIN FETCH cb.accountByAnalysisAccountId analysis"
				+ " WHERE si.salesInvoiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierPaymentsDetails", query = "SELECT DISTINCT(s) FROM Supplier s"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH s.creditTerm pt"
				+ " LEFT JOIN FETCH s.payments p "
				+ " LEFT JOIN FETCH p.paymentDetails pd "
				+ " LEFT JOIN FETCH pd.product pro "
				+ " LEFT JOIN FETCH s.directPayments dp "
				+ " LEFT JOIN FETCH dp.directPaymentDetails dpd "
				+ " WHERE s.implementation=? ORDER BY p.date, dp.paymentDate"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierPaymentsDetailsBySupplierId", query = "SELECT DISTINCT(s) FROM Supplier s"
				+ " LEFT JOIN FETCH s.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH s.creditTerm pt"
				+ " LEFT JOIN FETCH s.payments p "
				+ " LEFT JOIN FETCH p.paymentDetails pd "
				+ " LEFT JOIN FETCH pd.product pro "
				+ " LEFT JOIN FETCH s.directPayments dp "
				+ " LEFT JOIN FETCH dp.directPaymentDetails dpd "
				+ " WHERE s.implementation=? AND s.supplierId=? ORDER BY p.date, dp.paymentDate"),

		@org.hibernate.annotations.NamedQuery(name = "getSupplierPaymentsTotals", query = "SELECT s.supplierId,s.supplierNumber,cmp.companyName,loc.locationName,pt.name,SUM(id.amount),SUM(dpd.amount) FROM Supplier s"
				+ " LEFT JOIN  s.cmpDeptLocation cdl"
				+ " LEFT JOIN  cdl.company cmp"
				+ " LEFT JOIN  cdl.location loc"
				+ " LEFT JOIN  s.creditTerm pt"
				+ " LEFT JOIN  s.payments p "
				+ " LEFT JOIN  p.paymentDetails pd "
				+ " LEFT JOIN  pd.invoiceDetail id "
				+ " LEFT JOIN  pd.product pro "
				+ " LEFT JOIN  s.directPayments dp "
				+ " LEFT JOIN  dp.directPaymentDetails dpd "
				+ " WHERE s.implementation= ? GROUP BY s.supplierId ORDER BY p.date, dp.paymentDate"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssets", query = "SELECT a FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetType type"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetSubclass sub"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH a.company c"
				+ " WHERE a.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetCheckOuts", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetType type"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetSubclass sub"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH a.company c"
				+ " WHERE a.implementation = ?"
				+ " AND a.assetCreationId NOT IN("
				+ " SELECT ack.assetCreation.assetCreationId FROM a.assetCheckOuts ack WHERE ack.status = 1)"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetsWithReceiveDetails", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.assetDetails ad"
				+ " LEFT JOIN FETCH ad.directPaymentDetail payment"
				+ " LEFT JOIN FETCH a.assetDepreciations dpr"
				+ " LEFT JOIN FETCH a.assetRevaluations rvalue"),

		@org.hibernate.annotations.NamedQuery(name = "getSimpleAssetInfo", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " WHERE a.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetDetailInfo", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.assetDetails ad"
				+ " WHERE a.assetCreationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetInformation", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetType assettype"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetSubclass sub"
				+ " LEFT JOIN FETCH a.lookupDetailByCategory cat"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH a.company c"
				+ " LEFT JOIN FETCH a.assetDetails ad"
				+ " LEFT JOIN FETCH ad.lookupDetail costtype"
				+ " LEFT JOIN FETCH ad.directPaymentDetail payment"
				+ " WHERE a.assetCreationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetDetailedInformation", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetType assettype"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetSubclass sub"
				+ " LEFT JOIN FETCH a.lookupDetailByCategory cat"
				+ " LEFT JOIN FETCH a.assetDepreciations ad"
				+ " LEFT JOIN FETCH a.assetRevaluations ar"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH a.company c"
				+ " LEFT JOIN FETCH a.assetDetails ad"
				+ " LEFT JOIN FETCH ad.directPaymentDetail payment"
				+ " LEFT JOIN FETCH a.assetDepreciations dpr"
				+ " LEFT JOIN FETCH ad.lookupDetail costtype"
				+ " LEFT JOIN FETCH a.assetRevaluations rvalue"
				+ " WHERE a.assetCreationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetDetailByAssetCreationId", query = "SELECT ad FROM AssetDetail ad"
				+ " WHERE ad.assetCreation.assetCreationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetCheckOuts", query = "SELECT DISTINCT(a) FROM AssetCheckOut a"
				+ " JOIN FETCH a.assetCreation ar"
				+ " JOIN FETCH a.personByCheckOutBy cby"
				+ " JOIN FETCH a.personByCheckOutTo cto"
				+ " WHERE ar.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveAssetCheckOuts", query = "SELECT DISTINCT(a) FROM AssetCheckOut a"
				+ " JOIN FETCH a.assetCreation ar"
				+ " JOIN FETCH a.personByCheckOutBy cby"
				+ " JOIN FETCH a.personByCheckOutTo cto"
				+ " WHERE ar.implementation = ?"
				+ " AND a.status = 1"
				+ " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetCheckOutDetails", query = "SELECT DISTINCT(a) FROM AssetCheckOut a"
				+ " JOIN FETCH a.assetCreation ar"
				+ " JOIN FETCH a.personByCheckOutBy cby"
				+ " JOIN FETCH a.personByCheckOutTo cto"
				+ " JOIN FETCH a.cmpDeptLocation cmp"
				+ " JOIN FETCH cmp.company cpy"
				+ " JOIN FETCH cmp.department dpt"
				+ " JOIN FETCH cmp.location loc"
				+ " WHERE a.assetCheckOutId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetCheckIns", query = "SELECT DISTINCT(a) FROM AssetCheckIn a"
				+ " JOIN FETCH a.assetCheckOut ao"
				+ " JOIN FETCH ao.assetCreation ac"
				+ " WHERE ac.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetCheckInDetails", query = "SELECT DISTINCT(a) FROM AssetCheckIn a"
				+ " JOIN FETCH a.assetCheckOut ao"
				+ " JOIN FETCH ao.assetCreation ac"
				+ " JOIN FETCH a.person p"
				+ " JOIN FETCH ao.personByCheckOutTo cto"
				+ " WHERE a.assetCheckInId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetServices", query = "SELECT DISTINCT(a) FROM AssetService a"
				+ " JOIN FETCH a.assetCreation ac"
				+ " JOIN FETCH a.person p"
				+ " JOIN FETCH a.lookupDetailByServiceType serviceType"
				+ " WHERE ac.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetServiceDetails", query = "SELECT DISTINCT(a) FROM AssetService a"
				+ " JOIN FETCH a.assetCreation ac"
				+ " JOIN FETCH a.person p"
				+ " JOIN FETCH a.lookupDetailByServiceTerms st"
				+ " JOIN FETCH a.lookupDetailByContractType ct"
				+ " JOIN FETCH a.assetServiceDetails asd"
				+ " JOIN FETCH asd.person pr" + " WHERE a.assetServiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetServiceDetailByServiceId", query = "SELECT DISTINCT(a) FROM AssetServiceDetail a"
				+ " WHERE a.assetService.assetServiceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetServiceDetails", query = "SELECT DISTINCT(a) FROM AssetService a"
				+ " JOIN FETCH a.assetCreation ac"
				+ " JOIN FETCH a.assetServiceDetails asd"
				+ " WHERE ac.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetServiceCharges", query = "SELECT DISTINCT(a) FROM AssetServiceDetail a"
				+ " JOIN FETCH a.assetServiceCharges asg"
				+ " JOIN FETCH a.assetService asr"
				+ " JOIN FETCH asr.assetCreation ac"
				+ " JOIN FETCH asg.lookupDetail ct"
				+ " WHERE ac.implementation = ?"
				+ " AND asr.isApprove = ?"
				+ " AND asg IS NOT NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetServiceChargeById", query = "SELECT DISTINCT(a) FROM AssetServiceCharge a"
				+ " JOIN FETCH a.assetServiceDetail asd"
				+ " JOIN FETCH asd.assetService asr"
				+ " JOIN FETCH asr.assetCreation ac"
				+ " JOIN FETCH a.lookupDetail ct"
				+ " WHERE asd.assetServiceDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetServiceChargeByChargeId", query = "SELECT DISTINCT(a) FROM AssetServiceCharge a"
				+ " JOIN FETCH a.assetServiceDetail asd"
				+ " JOIN FETCH asd.assetService asr"
				+ " JOIN FETCH asr.assetCreation ac"
				+ " JOIN FETCH a.lookupDetail ct"
				+ " WHERE a.assetServiceChargeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetServiceChargeByDetailId", query = "SELECT DISTINCT(a) FROM AssetServiceCharge a"
				+ " WHERE a.assetServiceDetail.assetServiceDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetInsurance", query = "SELECT DISTINCT(a) FROM AssetInsurance a"
				+ " JOIN FETCH a.lookupDetailByInsuranceType itype"
				+ " JOIN FETCH a.lookupDetailByPolicyType ptype"
				+ " JOIN FETCH a.lookupDetailByProvider provider"
				+ " JOIN FETCH a.assetCreation ac"
				+ " WHERE ac.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetInsuranceByInsuranceId", query = "SELECT DISTINCT(a) FROM AssetInsurance a"
				+ " JOIN FETCH a.lookupDetailByInsuranceType itype"
				+ " JOIN FETCH a.lookupDetailByPolicyType ptype"
				+ " JOIN FETCH a.lookupDetailByProvider provider"
				+ " JOIN FETCH a.assetCreation ac"
				+ " LEFT JOIN FETCH a.combination c"
				+ " LEFT JOIN FETCH c.accountByNaturalAccountId na"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH a.chequeBook cbook"
				+ " LEFT JOIN FETCH cbook.bankAccount account"
				+ " LEFT JOIN FETCH account.bank bank"
				+ " WHERE a.assetInsuranceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPremiumDuePaymentByDate", query = "SELECT DISTINCT(a) FROM AssetInsurancePremium a"
				+ " JOIN FETCH a.assetInsurance ai"
				+ " JOIN FETCH ai.assetCreation ac"
				+ " JOIN FETCH ac.implementation impl"
				+ " WHERE a.nextPremiumDate = ?" + " AND a.status = 0"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePremiumDueInsuranceId", query = "SELECT DISTINCT(a) FROM AssetInsurancePremium a"
				+ " JOIN FETCH a.assetInsurance ai"
				+ " JOIN FETCH ai.assetCreation ac"
				+ " WHERE ai.assetInsuranceId = ?" + " AND a.status = 0"),

		@org.hibernate.annotations.NamedQuery(name = "getPremiumDueInsuranceId", query = "SELECT DISTINCT(a) FROM AssetInsurancePremium a"
				+ " JOIN FETCH a.assetInsurance ai"
				+ " JOIN FETCH ai.assetCreation ac"
				+ " WHERE ai.assetInsuranceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetClaimInsurance", query = "SELECT a FROM AssetClaimInsurance a"
				+ " JOIN FETCH a.assetInsurance ai"
				+ " JOIN FETCH ai.assetCreation ac"
				+ " LEFT JOIN FETCH a.lookupDetail ld"
				+ " WHERE ac.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetClaimInsuranceByClaimId", query = "SELECT a FROM AssetClaimInsurance a"
				+ " JOIN FETCH a.assetInsurance ai"
				+ " LEFT JOIN FETCH a.lookupDetail ld"
				+ " WHERE a.assetClaimInsuranceId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetWarranty", query = "SELECT a FROM AssetWarranty a"
				+ " JOIN FETCH a.assetCreation ac"
				+ " LEFT JOIN FETCH a.lookupDetail ld"
				+ " WHERE ac.implementation = ?" + " AND a.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetWarrantyById", query = "SELECT a FROM AssetWarranty a"
				+ " JOIN FETCH a.assetCreation ac"
				+ " LEFT JOIN FETCH a.lookupDetail ld"
				+ " WHERE a.assetWarrantyId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetWarrantyClaim", query = "SELECT a FROM AssetWarrantyClaim a"
				+ " JOIN FETCH a.assetWarranty aw"
				+ " JOIN FETCH aw.assetCreation ac"
				+ " LEFT JOIN FETCH a.lookupDetail ld"
				+ " WHERE ac.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetWarrantyClaimById", query = "SELECT a FROM AssetWarrantyClaim a"
				+ " JOIN FETCH a.assetWarranty aw"
				+ " JOIN FETCH aw.assetCreation ac"
				+ " LEFT JOIN FETCH a.lookupDetail ld"
				+ " WHERE a.assetWarrantyClaimId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetRevaluation", query = "SELECT a FROM AssetRevaluation a"
				+ " JOIN FETCH a.assetCreation ac"
				+ " WHERE ac.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAssetDisposal", query = "SELECT a FROM AssetDisposal a"
				+ " JOIN FETCH a.assetCreationByAssetId ac"
				+ " WHERE ac.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCommissionRuleDetails", query = "SELECT cr FROM CommissionRule cr"
				+ " WHERE cr.implementation = ?" + " AND cr.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCommissionRuleBase", query = "SELECT cr FROM CommissionRule cr"
				+ " JOIN FETCH cr.commissionMethods cm"
				+ " JOIN FETCH cr.commissionPersonCombinations cpc"
				+ " JOIN FETCH cpc.person pr"
				+ " WHERE cr.commissionRuleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCommissionMethodByCommissionRuleId", query = "SELECT cm FROM CommissionMethod cm"
				+ " WHERE cm.commissionRule.commissionRuleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCommissionPersonByCommissionRuleId", query = "SELECT cm FROM CommissionPersonCombination cm"
				+ " WHERE cm.commissionRule.commissionRuleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductDiscounts", query = "SELECT d FROM Discount d"
				+ " LEFT JOIN FETCH d.lookupDetail dop"
				+ " WHERE d.implementation = ?" + " AND d.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDiscountMethodByDiscountId", query = "SELECT dm FROM DiscountMethod dm"
				+ " WHERE dm.discount.discountId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDiscountByProductId", query = "SELECT DISTINCT(d) FROM Discount d"
				+ " LEFT JOIN FETCH d.lookupDetail dop"
				+ " LEFT JOIN FETCH d.discountOptions dos"
				+ " WHERE d.status = 1"
				+ " AND dos.product.productId = ?"
				+ " AND d.fromDate <= ?" + " AND d.toDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDiscountByCustomerId", query = "SELECT DISTINCT(d) FROM Discount d"
				+ " LEFT JOIN FETCH d.lookupDetail dop"
				+ " LEFT JOIN FETCH d.discountOptions dos"
				+ " WHERE d.status = 1"
				+ " AND dos.customer.customerId = ?"
				+ " AND d.fromDate <= ?" + " AND d.toDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDiscountByDiscountId", query = "SELECT DISTINCT(d) FROM Discount d"
				+ " LEFT JOIN FETCH d.lookupDetail dop"
				+ " JOIN FETCH d.discountOptions dos"
				+ " JOIN FETCH d.discountMethods dms"
				+ " LEFT JOIN FETCH dos.product pr"
				+ " LEFT JOIN FETCH dos.customer cus"
				+ " LEFT JOIN FETCH cus.personByPersonId prs"
				+ " LEFT JOIN FETCH cus.company cmpy"
				+ " WHERE d.discountId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDiscountOptionByDiscountId", query = "SELECT DISTINCT do FROM DiscountOption do"
				+ " WHERE do.discount.discountId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCoupons", query = "SELECT DISTINCT c FROM Coupon c"
				+ " WHERE c.implementation = ?" + " AND c.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllActiveCoupons", query = "SELECT DISTINCT c FROM Coupon c"
				+ " WHERE c.implementation = ?"
				+ " AND c.toDate >= ?"
				+ " AND c.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getCouponByCouponNumber", query = "SELECT DISTINCT c FROM Coupon c"
				+ " WHERE c.implementation = ?"
				+ " AND c.startDigit <= ? AND c.endDigit >= ?"
				+ " AND c.fromDate <= ?"
				+ " AND c.toDate >= ?"
				+ " AND c.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMemberCards", query = "SELECT DISTINCT mc FROM MemberCard mc"
				+ " WHERE mc.implementation = ?" + " AND mc.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPosUserTill", query = "SELECT DISTINCT(ut) FROM POSUserTill ut"
				+ " LEFT JOIN FETCH ut.store str"
				+ " LEFT JOIN FETCH ut.personByPersonId per"
				+ " LEFT JOIN FETCH ut.personBySupervisor super"
				+ " WHERE ut.implementation = ?" + " AND ut.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPosUserTillInformation", query = "SELECT DISTINCT(ut) FROM POSUserTill ut"
				+ " LEFT JOIN FETCH ut.store str"
				+ " LEFT JOIN FETCH ut.personByPersonId per"
				+ " LEFT JOIN FETCH ut.personBySupervisor super"
				+ " WHERE ut.posUserTillId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPosUserTillByPersonId", query = "SELECT DISTINCT(ut) FROM POSUserTill ut"
				+ " JOIN FETCH ut.store str"
				+ " LEFT JOIN FETCH ut.personByPersonId per"
				+ " LEFT JOIN FETCH ut.personBySupervisor super"
				+ " JOIN FETCH str.cmpDeptLocation cmpDept"
				+ " LEFT JOIN FETCH cmpDept.company company"
				+ " LEFT JOIN FETCH cmpDept.department dept"
				+ " LEFT JOIN FETCH cmpDept.location loc"
				+ " WHERE ut.isActive = 1" 
				+ " AND per.personId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMemberCardByCardType", query = "SELECT DISTINCT mc FROM MemberCard mc"
				+ " WHERE mc.implementation = ?" + " AND mc.cardType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllRewardPolicies", query = "SELECT DISTINCT rp FROM RewardPolicy rp"
				+ " JOIN FETCH rp.coupon cp"
				+ " WHERE rp.implementation = ?"
				+ " AND rp.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRewardPolicyByPolicyId", query = "SELECT DISTINCT rp FROM RewardPolicy rp"
				+ " JOIN FETCH rp.coupon cp" + " WHERE rp.rewardPolicyId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRewardPolicyByCouponId", query = "SELECT DISTINCT rp FROM RewardPolicy rp"
				+ "  WHERE rp.coupon.couponId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductPoints", query = "SELECT DISTINCT pr FROM ProductPoint pr"
				+ " JOIN FETCH pr.product p"
				+ " WHERE p.implementation = ? AND pr.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveProductPoints", query = "SELECT DISTINCT pr FROM ProductPoint pr"
				+ " JOIN FETCH pr.product p "
				+ " WHERE p.implementation = ?"
				+ " AND pr.status = 1" + " AND pr.toDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPointById", query = "SELECT DISTINCT pr FROM ProductPoint pr"
				+ " JOIN FETCH pr.product p" + " WHERE pr.productPointId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPointsByProductId", query = "SELECT DISTINCT pr FROM ProductPoint pr"
				+ " WHERE pr.product.productId = ?"
				+ " AND pr.status = 1"
				+ " AND pr.toDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPromotions", query = "SELECT p FROM Promotion p"
				+ " JOIN FETCH p.lookupDetail ld"
				+ " WHERE p.implementation = ?" + " AND p.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPromotionbyId", query = "SELECT DISTINCT(p) FROM Promotion p"
				+ " JOIN FETCH p.promotionMethods pm"
				+ " JOIN FETCH p.lookupDetail ld"
				+ " LEFT JOIN FETCH pm.coupon cp"
				+ " LEFT JOIN FETCH p.promotionOptions po"
				+ " LEFT JOIN FETCH po.product pr"
				+ " LEFT JOIN FETCH po.customer cr"
				+ " LEFT JOIN FETCH cr.personByPersonId person"
				+ " LEFT JOIN FETCH cr.company cmpy"
				+ " WHERE p.promotionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPromotionByProductId", query = "SELECT DISTINCT(p) FROM Promotion p"
				+ " JOIN FETCH p.promotionMethods pm"
				+ " JOIN FETCH p.lookupDetail ld"
				+ " LEFT JOIN FETCH pm.coupon cp"
				+ " LEFT JOIN FETCH p.promotionOptions po"
				+ " WHERE p.toDate >=?"
				+ " AND p.status = 1"
				+ " AND po.product.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPromotionByCustomerId", query = "SELECT DISTINCT(p) FROM Promotion p"
				+ " JOIN FETCH p.promotionMethods pm"
				+ " JOIN FETCH p.lookupDetail ld"
				+ " LEFT JOIN FETCH pm.coupon cp"
				+ " LEFT JOIN FETCH p.promotionOptions po"
				+ " WHERE p.toDate >=?"
				+ " AND p.status = 1"
				+ " AND po.customer.customerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPromotionMethodByPromotionId", query = "SELECT pm FROM PromotionMethod pm"
				+ " WHERE pm.promotion.promotionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPromotionOptionByPromotionId", query = "SELECT po FROM PromotionOption po"
				+ " WHERE po.promotion.promotionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSales", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill ut"
				+ " WHERE p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSalesData", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " LEFT JOIN FETCH sale.productByProductId prd"
				+ " LEFT JOIN FETCH prd.combinationByInventoryAccount prdcb"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psc"
				+ " WHERE p.implementation = ?" + " AND p.pointOfSaleId > ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSalesByDateRange", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " LEFT JOIN FETCH sale.productByProductId prd"
				+ " LEFT JOIN FETCH prd.combinationByInventoryAccount prdcb"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psc"
				+ " WHERE p.implementation = ?"
				+ " AND p.salesDate >= ? AND p.salesDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSalesAllData", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " LEFT JOIN FETCH sale.productByProductId prd"
				+ " LEFT JOIN FETCH prd.combinationByInventoryAccount prdcb"
				+ " LEFT JOIN FETCH prd.productPricingDetails ppd"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts psr"
				+ " WHERE p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSalesByDate", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psc"
				+ " WHERE p.implementation = ?"
				+ " AND (p.salesDate = ? OR p.salesDate = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSalesWithImplementation", query = "SELECT p FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill ut"
				+ " JOIN FETCH p.implementation impl"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfDetailByOrderId", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleOffers pso"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psc"
				+ " LEFT JOIN FETCH p.POSUserTill ut"
				+ " LEFT JOIN FETCH ut.store str"
				+ " LEFT JOIN FETCH sale.productByProductId prd"
				+ " LEFT JOIN FETCH prd.productPricingDetails prdprice"
				+ " WHERE p.pointOfSaleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfDetailsProductByOrderId", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " JOIN FETCH sale.productByProductId prd"
				+ " WHERE p.pointOfSaleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSaleDetailsByOrderId", query = "SELECT DISTINCT(p) FROM PointOfSaleDetail p"
				+ " WHERE p.pointOfSale.pointOfSaleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSaleDetailsByProductId", query = "SELECT DISTINCT(p) FROM PointOfSaleDetail p"
				+ " JOIN FETCH p.pointOfSale sale"
				+ " LEFT JOIN FETCH sale.customer c"
				+ " WHERE p.productByProductId.productId = ? "
				+ " OR p.productByComboProductId.productId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfDetailByProductId", query = "SELECT DISTINCT(p) FROM PointOfSaleDetail p"
				+ " JOIN FETCH p.pointOfSale sale"
				+ " WHERE p.productByProductId.productId = ?"
				+ " AND sale.pointOfSaleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfDetailByDetailId", query = "SELECT DISTINCT(p) FROM PointOfSaleDetail p"
				+ " JOIN FETCH p.pointOfSale sale"
				+ " WHERE p.pointOfSaleDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfDetailByOrderId", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " JOIN FETCH sale.productByProductId prod"
				+ " LEFT JOIN FETCH sale.productByComboProductId cprd"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH cst.personByPersonId cstPerson"
				+ " LEFT JOIN FETCH cst.company cmpy"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation loc"
				+ " LEFT JOIN FETCH loc.company loccmpy"
				+ " LEFT JOIN FETCH p.lookupDetail dine"
				+ " WHERE p.pointOfSaleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "findByPointOfSaleBySessionValue", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " WHERE p.deliveryOptionName = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSalesByStore", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " JOIN FETCH sale.productByProductId prd"
				+ " WHERE p.implementation = ?"
				+ " AND p.POSUserTill.store.storeId = ?"
				+ " AND (p.isEodBalanced IS NULL OR p.isEodBalanced = 0)"
				+ " AND prd.itemSubType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSalesWithoutStore", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " JOIN FETCH sale.productByProductId prd"
				+ " WHERE p.implementation = ?"
				+ " AND (p.isEodBalanced IS NULL OR p.isEodBalanced = 0)"
				+ " AND prd.itemSubType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDispacthPointOfDetailByOrderId", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails sale"
				+ " JOIN FETCH sale.productByProductId prod"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psr"
				+ " LEFT JOIN FETCH cst.personByPersonId cstPerson"
				+ " LEFT JOIN FETCH cst.company cmpy"
				+ " LEFT JOIN FETCH cst.cmpDeptLocation loc"
				+ " LEFT JOIN FETCH loc.company loccmpy"
				+ " LEFT JOIN FETCH p.lookupDetail dine"
				+ " WHERE p.pointOfSaleId = ?"
				+ " AND (sale.comboType IS NULL OR sale.comboType = 0)"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSaleDetailsByCurrencyMode", query = "SELECT DISTINCT(po) FROM PointOfSaleDetail po"
				+ " JOIN FETCH po.pointOfSale p"
				+ " JOIN FETCH p.pointOfSaleReceipts psr"
				+ " WHERE p.implementation = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?" + " AND psr.receiptType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSaleReceiptsByCurrencyMode", query = "SELECT DISTINCT(po) FROM PointOfSaleReceipt po"
				+ " JOIN FETCH po.pointOfSale p"
				+ " WHERE p.implementation = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?" + " AND po.receiptType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSaleReceiptsBySalesDate", query = "SELECT DISTINCT(po) FROM PointOfSaleReceipt po"
				+ " JOIN FETCH po.pointOfSale p"
				+ " JOIN FETCH p.POSUserTill till"
				+ " WHERE p.implementation = ?"
				+ " AND till.store.storeId = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?"
				+ " AND (p.isEodBalanced != 1 OR p.isEodBalanced IS NULL)"
				+ " AND (p.deliveryStatus IS NULL OR p.deliveryStatus = 1)"
				+ " AND po.receiptType != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSaleReceiptsByEmployeeService", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill till"
				+ " JOIN FETCH till.store str"
				+ " JOIN FETCH p.pointOfSaleDetails psd"
				+ " JOIN FETCH psd.productByProductId prd"
				+ " WHERE p.implementation = ?"
				+ " AND till.store.storeId = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?"
				+ " AND p.saleType = ?"
				+ " AND (p.isEodBalanced != 1 OR p.isEodBalanced IS NULL)"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSaleBySalesDate", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill till"
				+ " JOIN FETCH till.store str"
				+ " JOIN FETCH p.pointOfSaleDetails psd"
				+ " JOIN FETCH psd.productByProductId prd"
				+ "  JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psr"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " WHERE p.implementation = ?"
				+ " AND till.store.storeId = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?"
				+ " AND (p.isEodBalanced != 1 OR p.isEodBalanced IS NULL)"),

		@org.hibernate.annotations.NamedQuery(name = "getEODPointOfSaleBySalesDate", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill till"
				+ " JOIN FETCH till.store str"
				+ " JOIN FETCH p.pointOfSaleDetails psd"
				+ " JOIN FETCH psd.productByProductId prd"
				+ "  JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psc"
				+ " LEFT JOIN FETCH p.customer cst"
				+ " WHERE p.implementation = ?"
				+ " AND till.store.storeId = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?"
				+ " AND psr.receiptType != ?"
				+ " AND (p.isEodBalanced != 1 OR p.isEodBalanced IS NULL)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOfflinePOSTransactions", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill till"
				+ " LEFT JOIN FETCH p.customer cust"
				+ " LEFT JOIN FETCH cust.personByPersonId prp"
				+ " LEFT JOIN FETCH cust.shippingDetails shpd"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts receipts"
				+ " LEFT JOIN FETCH p.pointOfSaleDetails saleDetails"
				+ " LEFT JOIN FETCH saleDetails.productByProductId prod"
				+ " LEFT JOIN FETCH saleDetails.productByComboProductId cprod"
				+ " LEFT JOIN FETCH saleDetails.discountMethod discount"
				+ " LEFT JOIN FETCH saleDetails.promotionMethod promotion"
				+ " LEFT JOIN FETCH saleDetails.productPricingCalc pricingCalc"
				+ " LEFT JOIN FETCH saleDetails.merchandiseExchangeDetails exchangeDetails"
				+ " LEFT JOIN FETCH exchangeDetails.merchandiseExchange exchange"
				+ " LEFT JOIN FETCH p.pointOfSaleOffers saleOffer"
				+ " LEFT JOIN FETCH saleOffer.rewardPolicy reward"
				+ " LEFT JOIN FETCH saleOffer.coupon coupon"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges saleCharges"
				+ " LEFT JOIN FETCH saleCharges.lookupDetail chargesType"
				+ " WHERE p.offlineEntry = 1"
				+ " AND (p.deliveryStatus IS NULL OR p.deliveryStatus = 1)"
				+ " AND (p.isEodBalanced != 1 OR p.isEodBalanced IS NULL)"
				+ " AND p.implementation =  ?"),

		@org.hibernate.annotations.NamedQuery(name = "getOfflinePOSTransactions", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.POSUserTill till"
				+ " LEFT JOIN FETCH p.customer cust"
				+ " LEFT JOIN FETCH p.implementation implementation"
				+ " LEFT JOIN FETCH cust.personByPersonId prp"
				+ " LEFT JOIN FETCH cust.shippingDetails shpd"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts receipts"
				+ " LEFT JOIN FETCH p.pointOfSaleDetails saleDetails"
				+ " LEFT JOIN FETCH saleDetails.productByProductId prod"
				+ " LEFT JOIN FETCH saleDetails.productByComboProductId cprod"
				+ " LEFT JOIN FETCH saleDetails.discountMethod discount"
				+ " LEFT JOIN FETCH saleDetails.promotionMethod promotion"
				+ " LEFT JOIN FETCH saleDetails.productPricingCalc pricingCalc"
				+ " LEFT JOIN FETCH saleDetails.merchandiseExchangeDetails exchangeDetails"
				+ " LEFT JOIN FETCH exchangeDetails.merchandiseExchange exchange"
				+ " LEFT JOIN FETCH p.pointOfSaleOffers saleOffer"
				+ " LEFT JOIN FETCH saleOffer.rewardPolicy reward"
				+ " LEFT JOIN FETCH saleOffer.coupon coupon"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges saleCharges"
				+ " LEFT JOIN FETCH saleCharges.lookupDetail chargesType"
				+ " WHERE p.offlineEntry = 1"
				+ " AND p.processedSession = 1"
				+ " AND (p.deliveryStatus IS NULL OR p.deliveryStatus = 1)"
				+ " AND p.implementation =  ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfDetailByReference", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails psd"
				+ " JOIN FETCH psd.productByProductId prd"
				+ " WHERE p.implementation = ?"
				+ " AND p.referenceNumber = ?"
				+ " AND (psd.comboType IS NULL OR psd.comboType = 0)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSaleBySession", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails psd"
				+ " JOIN FETCH psd.productByProductId prd"
				+ " WHERE p.implementation = ?"
				+ " AND p.POSUserTill.posUserTillId = ?"
				+ " AND p.offlineEntry = 0" + " AND p.processedSession = 0"),

		@org.hibernate.annotations.NamedQuery(name = "getDispatchedOrders", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.customer cst"
				+ " WHERE p.POSUserTill.posUserTillId = ?"
				+ " AND p.deliveryStatus = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPointOfSaleBySession", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " WHERE p.implementation = ?"
				+ " AND p.POSUserTill.posUserTillId = ?"
				+ " AND p.offlineEntry = 0"
				+ " AND (p.deliveryStatus IS NULL OR p.deliveryStatus = 1)"
				+ " AND p.processedSession = 0 ORDER BY p.salesDate DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPointOfSaleSavedSession", query = " SELECT DISTINCT(p) FROM PointOfSale p "
				+ " JOIN FETCH p.POSUserTill till"
				+ " LEFT JOIN FETCH p.customer cust"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts receipts"
				+ " LEFT JOIN FETCH p.pointOfSaleDetails saleDetails"
				+ " LEFT JOIN FETCH saleDetails.productByProductId prod"
				+ " LEFT JOIN FETCH saleDetails.discountMethod discount"
				+ " LEFT JOIN FETCH saleDetails.promotionMethod promotion"
				+ " LEFT JOIN FETCH saleDetails.productPricingCalc pricingCalc"
				+ " LEFT JOIN FETCH saleDetails.merchandiseExchangeDetails exchangeDetails"
				+ " LEFT JOIN FETCH exchangeDetails.merchandiseExchange exchange"
				+ " LEFT JOIN FETCH p.pointOfSaleOffers saleOffer"
				+ " LEFT JOIN FETCH saleOffer.rewardPolicy reward"
				+ " LEFT JOIN FETCH saleOffer.coupon coupon"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges saleCharges"
				+ " LEFT JOIN FETCH saleCharges.lookupDetail chargesType"
				+ " LEFT JOIN FETCH p.promotionMethod pm"
				+ " LEFT JOIN FETCH p.discountMethod pdm"
				+ " LEFT JOIN FETCH pdm.discount dis"
				+ " WHERE p.offlineEntry = 1"
				+ " AND p.processedSession = 1"
				+ " AND p.implementation =  ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnBalancedPointOfSaleByCurrentDate", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " JOIN FETCH p.pointOfSaleDetails psd"
				+ " JOIN FETCH psd.productByProductId prd"
				+ " JOIN FETCH p.POSUserTill till"
				+ " JOIN FETCH till.store str"
				+ " JOIN FETCH p.pointOfSaleReceipts psr"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges psr"
				+ " WHERE p.implementation = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?" + " AND p.isEodBalanced != 1"),

		@org.hibernate.annotations.NamedQuery(name = "getBalancedPointOfSaleByCurrentDate", query = "SELECT DISTINCT(p) FROM PointOfSale p"
				+ " WHERE p.implementation = ?"
				+ " AND p.salesDate >= ?"
				+ " AND p.salesDate <= ?" + " AND p.isEodBalanced = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getMerchandiseExchangeByReference", query = "SELECT DISTINCT(m) FROM MerchandiseExchange m"
				+ " JOIN FETCH m.merchandiseExchangeDetails md"
				+ " JOIN FETCH md.pointOfSaleDetail psd"
				+ " LEFT JOIN FETCH psd.promotionMethod pm"
				+ " LEFT JOIN FETCH psd.discountMethod dm"
				+ " LEFT JOIN FETCH dm.discount ds"
				+ " LEFT JOIN FETCH psd.pointOfSale p"
				+ " LEFT JOIN FETCH p.discountMethod pdm"
				+ " LEFT JOIN FETCH pdm.discount pds"
				+ " LEFT JOIN FETCH p.promotionMethod ppm"
				+ " WHERE m.implementation = ?" + " AND m.referenceNumber = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMerchandiseExchange", query = "SELECT m FROM MerchandiseExchange m"
				+ " WHERE m.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getExchangeDetailByExchangeId", query = "SELECT DISTINCT(md) FROM MerchandiseExchangeDetail md"
				+ " JOIN FETCH md.pointOfSaleDetail psd"
				+ " JOIN FETCH psd.pointOfSale ps"
				+ " JOIN FETCH ps.POSUserTill till"
				+ " JOIN FETCH till.store str"
				+ " JOIN FETCH psd.productByProductId prd"
				+ " WHERE md.merchandiseExchange.merchandiseExchangeId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getExchangeDetailByPOSDetailId", query = "SELECT DISTINCT(md) FROM MerchandiseExchangeDetail md"
				+ " WHERE md.pointOfSaleDetail.pointOfSaleDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllEODBalancing", query = "SELECT DISTINCT(e) FROM EODBalancing e"
				+ " LEFT JOIN FETCH e.person epr"
				+ " LEFT JOIN FETCH e.store estr"
				+ " WHERE e.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getEODBalancingById", query = "SELECT DISTINCT(e) FROM EODBalancing e"
				+ " JOIN FETCH e.EODBalancingDetails ed"
				+ " JOIN FETCH ed.product prd"
				+ " LEFT JOIN FETCH ed.EODProductionDetails edp"
				+ " LEFT JOIN FETCH edp.product prd"
				+ " LEFT JOIN FETCH e.person epsr"
				+ " LEFT JOIN FETCH e.store estr"
				+ " WHERE e.eodBalancingId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductionReceive", query = "SELECT DISTINCT(p) FROM ProductionReceive p"
				+ " JOIN FETCH p.personByReceivePerson person"
				+ " JOIN FETCH p.lookupDetail sr"
				+ " WHERE p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductionReceiveById", query = "SELECT DISTINCT(p) FROM ProductionReceive p"
				+ " JOIN FETCH p.productionReceiveDetails pd"
				+ " JOIN FETCH pd.product prd"
				+ " JOIN FETCH pd.shelf slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " WHERE p.productionReceiveId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductionReceiveById", query = "SELECT DISTINCT(p) FROM ProductionReceive p"
				+ " JOIN FETCH p.personByReceivePerson person"
				+ " JOIN FETCH p.lookupDetail sr"
				+ " JOIN FETCH p.productionReceiveDetails prd"
				+ " JOIN FETCH prd.product product"
				+ " JOIN FETCH prd.shelf slf"
				+ " JOIN FETCH slf.aisle aisle"
				+ " JOIN FETCH aisle.store str"
				+ " WHERE p.productionReceiveId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMaterialRequisition", query = "SELECT DISTINCT(m) FROM MaterialRequisition m"
				+ " JOIN FETCH m.store str"
				+ " LEFT JOIN FETCH m.person pr"
				+ " LEFT JOIN FETCH m.materialRequisitionDetails mrd"
				+ " LEFT JOIN FETCH mrd.materialTransferDetails mtd"
				+ " LEFT JOIN FETCH mtd.materialTransfer mt"
				+ " WHERE m.implementation = ?" + " AND m.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialRequisitionById", query = "SELECT DISTINCT(m) FROM MaterialRequisition m"
				+ " JOIN FETCH m.materialRequisitionDetails mt"
				+ " LEFT JOIN FETCH mt.materialTransferDetails mtd"
				+ " LEFT JOIN FETCH mtd.materialTransfer mtr"
				+ " LEFT JOIN FETCH mt.product prd"
				+ " LEFT JOIN FETCH mtd.product trprd"
				+ " LEFT JOIN FETCH m.store str"
				+ " LEFT JOIN FETCH m.person person"
				+ " WHERE m.materialRequisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveMaterialRequisitionDetail", query = "SELECT DISTINCT(mr) FROM MaterialRequisitionDetail mr"
				+ " JOIN FETCH mr.materialRequisition m"
				+ " JOIN FETCH m.store str"
				+ " JOIN FETCH mr.product prd"
				+ " LEFT JOIN FETCH mr.materialTransferDetails mt"
				+ " WHERE m.implementation = ?"
				+ " AND str.storeId = ?"
				+ " AND m.requisitionStatus != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialRequisitionDetailById", query = "SELECT DISTINCT(mr) FROM MaterialRequisitionDetail mr"
				+ " JOIN FETCH mr.materialRequisition m"
				+ " WHERE m.materialRequisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMaterialTransfer", query = "SELECT DISTINCT(m) FROM MaterialTransfer m"
				+ " JOIN FETCH m.personByTransferPerson person"
				+ " WHERE m.implementation = ?" + " AND m.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialTransferDetailByRequisitionId", query = "SELECT DISTINCT(m) FROM MaterialTransferDetail m"
				+ " JOIN FETCH m.materialRequisitionDetail mrd"
				+ " WHERE mrd.materialRequisitionDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialTransferById", query = "SELECT DISTINCT(m) FROM MaterialTransfer m"
				+ " JOIN FETCH m.materialTransferDetails mt"
				+ " JOIN FETCH mt.product prd"
				+ " JOIN FETCH mt.shelfByFromShelfId frmslf"
				+ " JOIN FETCH frmslf.shelf frmrack"
				+ " JOIN FETCH frmrack.aisle frmae"
				+ " JOIN FETCH frmae.store frmstr"
				+ " JOIN FETCH mt.shelfByShelfId slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " WHERE m.materialTransferId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getMaterialTransferDetailById", query = "SELECT DISTINCT(m) FROM MaterialTransferDetail m"
 				+ " JOIN FETCH m.product prd"
				+ " JOIN FETCH m.shelfByFromShelfId frmslf"
				+ " JOIN FETCH frmslf.shelf frmrack"
				+ " JOIN FETCH frmrack.aisle frmae"
				+ " JOIN FETCH frmae.store frmstr"
				+ " JOIN FETCH m.shelfByShelfId slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " WHERE m.materialTransferDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRevenueStatementByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.period.periodId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 3)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getExpenseStatementByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.period.periodId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 4)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetStatementByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.period.periodId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 1)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getOwnersEquityStatementByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.period.periodId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 5)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getLiablityStatementByPeriod", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE ld.period.periodId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 2)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getRevenueStatementByCalendar", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH ld.period prid"
				+ " LEFT JOIN FETCH prid.calendar calend"
				+ " WHERE calend.calendarId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 3)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getExpenseStatementByCalendar", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH ld.period prid"
				+ " LEFT JOIN FETCH prid.calendar calend"
				+ " WHERE calend.calendarId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 4)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetStatementByCalendar", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH ld.period prid"
				+ " LEFT JOIN FETCH prid.calendar calend"
				+ " WHERE calend.calendarId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 1)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getOwnersEquityStatementByCalendar", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH ld.period prid"
				+ " LEFT JOIN FETCH prid.calendar calend"
				+ " WHERE calend.calendarId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 5)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getLiablityStatementByCalendar", query = "SELECT DISTINCT(ld) FROM LedgerFiscal ld"
				+ " JOIN FETCH ld.combination c"
				+ " JOIN c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH ld.period prid"
				+ " LEFT JOIN FETCH prid.calendar calend"
				+ " WHERE calend.calendarId = ?"
				+ " AND ld.balance IS NOT NULL"
				+ " AND (natural.accountType = 2)"
				+ " ORDER BY c.accountByCostcenterAccountId, cost.code, natural.code, natural.accountType"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMaterialTransferById", query = "SELECT DISTINCT(m) FROM MaterialTransfer m"
				+ " JOIN FETCH m.personByTransferPerson person"
				+ " JOIN FETCH m.materialTransferDetails mt"
				+ " JOIN FETCH mt.lookupDetailByTransferType transferType"
				+ " LEFT JOIN FETCH mt.lookupDetailByTransferReason transferReason"
				+ " JOIN FETCH mt.shelfByFromShelfId frmslf"
				+ " JOIN FETCH frmslf.shelf frmrack"
				+ " JOIN FETCH frmrack.aisle frmae"
				+ " JOIN FETCH frmae.store frmstr"
				+ " JOIN FETCH mt.shelfByShelfId slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle ae"
				+ " JOIN FETCH ae.store str"
				+ " JOIN FETCH mt.product prd"
				+ " WHERE m.materialTransferId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductDefinitions", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " LEFT JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductDefinitionParents", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " WHERE prd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDefinitionLabels", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " WHERE p.productByProductId.productId = ?"
				+ " AND p.productBySpecialProductId IS NOT NULL"
				+ " GROUP BY definitionLabel"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDefinitionSubCategory", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " WHERE p.productByProductId.productId = ?"
				+ " AND p.productBySpecialProductId IS NULL"
				+ " AND p.productDefinition IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getParentProductDefinitions", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " LEFT JOIN p.productDefinition parent "
				+ " LEFT JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE parent.productDefinitionId IS NULL"
				+ " AND p.implementation = ?"
				+ " AND prd.productId = ?"
				+ " ORDER BY p.productDefinitionId"),

		@org.hibernate.annotations.NamedQuery(name = "getParentProductDefinitionByGroup", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " LEFT JOIN p.productDefinition parent "
				+ " WHERE parent.productDefinitionId IS NULL"
				+ " AND p.implementation = ?"
				+ " AND prd.productId = ?"
				+ " GROUP BY p.definitionLabel"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDefinitionByDefinitionId", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " LEFT JOIN FETCH p.productBySpecialProductId category "
				+ " LEFT JOIN FETCH p.productDefinition pd"
				+ " LEFT JOIN FETCH p.productDefinitions pds"
				+ " JOIN FETCH p.productByProductId prd"
				+ " WHERE p.productDefinitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDefinitionByGroup", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE prd.productId = ?"
				+ " AND p.definitionLabel = ?"
				+ " AND p.productDefinition IS NULL"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDefinitionByProductAndLabel", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE sprd.productId = ?" + " AND p.definitionLabel = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDefinitionBySpecialProductAndProduct", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productBySpecialProductId sprd"
				+ " JOIN FETCH p.productByProductId prd"
				+ " WHERE prd.productId = ?" + " AND sprd.productId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDefinitionByProduct", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE prd.productId = ?"
				+ " AND p.productDefinition IS NULL" + " AND p.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getProductDefinitionByGroupByDefinition", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE prd.productId = ?"
				+ " AND p.definitionLabel = ?"
				+ " AND p.productDefinition.productDefinitionId = ?"
				+ " GROUP BY p.definitionLabel"),

		@org.hibernate.annotations.NamedQuery(name = "getChildProductDefinitions", query = "SELECT DISTINCT(p) FROM ProductDefinition p"
				+ " JOIN FETCH p.productByProductId prd"
				+ " JOIN p.productDefinition parent "
				+ " LEFT JOIN FETCH p.productBySpecialProductId sprd"
				+ " WHERE parent.productDefinitionId = ? ORDER BY p.productDefinitionId"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductSplit", query = "SELECT DISTINCT(p) FROM ProductSplit p"
				+ " LEFT JOIN FETCH p.product prd"
				+ " LEFT JOIN FETCH p.shelf parentShelf "
				+ " LEFT JOIN FETCH parentShelf.shelf parentRack "
				+ " LEFT JOIN FETCH parentRack.aisle parentAsile "
				+ " LEFT JOIN FETCH parentAsile.store parentStore "
				+ " LEFT JOIN FETCH p.person person "
				+ " LEFT JOIN FETCH p.productSplitDetails pdetail"
				+ " LEFT JOIN FETCH pdetail.product product"
				+ " LEFT JOIN FETCH pdetail.store store"
				+ " LEFT JOIN FETCH pdetail.shelf detailShelf "
				+ " LEFT JOIN FETCH detailShelf.shelf detailRack "
				+ " LEFT JOIN FETCH detailRack.aisle detailAsiel "
				+ " LEFT JOIN FETCH detailAsiel.store detailStore "
				+ " WHERE p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductSplitById", query = "SELECT DISTINCT(p) FROM ProductSplit p"
				+ " LEFT JOIN FETCH p.product prd"
				+ " LEFT JOIN FETCH p.shelf parentShelf "
				+ " LEFT JOIN FETCH parentShelf.shelf parentRack "
				+ " LEFT JOIN FETCH parentRack.aisle parentAsile "
				+ " LEFT JOIN FETCH parentAsile.store parentStore "
				+ " LEFT JOIN FETCH p.person person "
				+ " LEFT JOIN FETCH p.productSplitDetails pdetail"
				+ " LEFT JOIN FETCH pdetail.product product"
				+ " LEFT JOIN FETCH pdetail.store store"
				+ " LEFT JOIN FETCH pdetail.shelf detailShelf "
				+ " LEFT JOIN FETCH detailShelf.shelf detailRack "
				+ " LEFT JOIN FETCH detailRack.aisle detailAsiel "
				+ " LEFT JOIN FETCH detailAsiel.store detailStore "
				+ " WHERE p.productSplitId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductSplitDetailBySplitId", query = "SELECT DISTINCT(pd) FROM ProductSplitDetail pd"
				+ " JOIN FETCH pd.productSplit ps"
				+ " JOIN FETCH ps.product prd"
				+ " LEFT JOIN FETCH ps.shelf shelf "
				+ " LEFT JOIN FETCH shelf.shelf rack "
				+ " LEFT JOIN FETCH rack.aisle ais "
				+ " LEFT JOIN FETCH ais.store str "
				+ " LEFT JOIN FETCH ps.person person "
				+ " LEFT JOIN FETCH pd.product detailPrd"
				+ " LEFT JOIN FETCH pd.shelf detailShelf "
				+ " LEFT JOIN FETCH detailShelf.shelf detailRack "
				+ " LEFT JOIN FETCH detailRack.aisle detailAsiel "
				+ " LEFT JOIN FETCH detailAsiel.store detailStore "
				+ " WHERE ps.productSplitId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "fetchSalesInformationByUseCase", query = "SELECT DISTINCT(pos) FROM PointOfSale pos"
				+ " LEFT JOIN FETCH pos.pointOfSaleDetails posDetail"
				+ " WHERE pos.recordId = ? AND pos.useCase= ? AND pos.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPricingDetailByProductId", query = "SELECT DISTINCT(ppd) FROM ProductPricingDetail ppd"
				+ " LEFT JOIN FETCH ppd.product p"
				+ " WHERE p.productId = ? AND ppd.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getNonunfiedProductPricingDetail", query = "SELECT DISTINCT(ppd) FROM ProductPricingDetail ppd"
				+ " LEFT JOIN FETCH ppd.product p"
				+ " WHERE p.productId = ?"
				+ " AND ppd.store.storeId = ?" + " AND ppd.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getPricingDetailByImplementation", query = "SELECT DISTINCT(ppd) FROM ProductPricingDetail ppd"
				+ " LEFT JOIN FETCH ppd.productPricingCalcs ppc"
				+ " WHERE (ppd.productPricing.implementation = ? OR ppd.productPricing.implementation = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getReceiptBySalesReference", query = "SELECT DISTINCT(r) FROM BankReceipts r"
				+ " WHERE r.receiptsNo = ? AND r.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMaterialIdleMix", query = "SELECT DISTINCT(m) FROM MaterialIdleMix m"
				+ " JOIN FETCH m.product prd"
				+ " JOIN FETCH m.person pr"
				+ " WHERE m.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialIdleMixbyId", query = "SELECT DISTINCT(m) FROM MaterialIdleMix m"
				+ " JOIN FETCH m.product prd"
				+ " JOIN FETCH m.person pr"
				+ " JOIN FETCH m.materialIdleMixDetails md"
				+ " LEFT JOIN FETCH md.productPackageDetail pack"
				+ " JOIN FETCH md.product mprd"
				+ " LEFT JOIN FETCH mprd.lookupDetailByProductUnit unit"
				+ " WHERE m.materialIdleMixId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialIdleMixbyProduct", query = "SELECT DISTINCT(m) FROM MaterialIdleMix m"
				+ " JOIN FETCH m.materialIdleMixDetails md"
				+ " JOIN FETCH md.product mprd"
				+ " LEFT JOIN FETCH mprd.lookupDetailByProductUnit"
				+ " LEFT JOIN FETCH md.productPackageDetail pack"
				+ " WHERE m.product.productId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialIdleMixDetails", query = "SELECT DISTINCT(m) FROM MaterialIdleMixDetail m"
				+ " WHERE m.materialIdleMix.materialIdleMixId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMaterialWastages", query = "SELECT DISTINCT(m) FROM MaterialWastage m"
				+ " JOIN FETCH m.person pr" + " WHERE m.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialWastagebyId", query = "SELECT DISTINCT(m) FROM MaterialWastage m"
				+ " JOIN FETCH m.person pr"
				+ " JOIN FETCH m.materialWastageDetails md"
				+ " JOIN FETCH md.product mprd"
				+ " LEFT JOIN FETCH mprd.lookupDetailByProductUnit punit"
				+ " LEFT JOIN FETCH md.lookupDetail typ"
				+ " LEFT JOIN FETCH md.shelf slf"
				+ " LEFT JOIN FETCH md.store str"
				+ " LEFT JOIN FETCH md.person mprd"
				+ " LEFT JOIN FETCH md.productPackageDetail packdt"
				+ " WHERE m.materialWastageId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialWastageDetails", query = "SELECT DISTINCT(m) FROM MaterialWastageDetail m"
				+ " LEFT JOIN FETCH m.shelf slf"
				+ " WHERE m.materialWastage.materialWastageId=?"
				+ " AND m.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialWastageDetailsByStore", query = "SELECT DISTINCT(m) FROM MaterialWastageDetail m"
				+ " JOIN FETCH m.product prd"
				+ " WHERE m.store.storeId=?"
				+ " AND m.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getMaterialWastageDetailsWithoutStore", query = "SELECT DISTINCT(m) FROM MaterialWastageDetail m"
				+ " JOIN FETCH m.materialWastage mw"
				+ " JOIN FETCH m.product prd"
				+ " WHERE mw.implementation = ?"
				+ " AND m.status = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllWorkInProcess", query = "SELECT DISTINCT(w) FROM WorkinProcess w"
				+ " JOIN FETCH w.person pr"
				+ " LEFT JOIN FETCH w.productionRequisition preq"
				+ " WHERE w.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkinProcessById", query = "SELECT DISTINCT(w) FROM WorkinProcess w"
				+ " JOIN FETCH w.person pr"
				+ " JOIN FETCH w.workinProcessProductions wp"
				+ " JOIN FETCH wp.product prd"
				+ " JOIN FETCH wp.shelf slf"
				+ " JOIN FETCH slf.shelf rack"
				+ " JOIN FETCH rack.aisle al"
				+ " JOIN FETCH al.store str"
				+ " JOIN FETCH wp.workinProcessDetails wpd"
				+ " JOIN FETCH wpd.product prwpd"
				+ " LEFT JOIN FETCH prwpd.lookupDetailByProductUnit prdut"
				+ " JOIN FETCH wpd.shelf wslf"
				+ " JOIN FETCH wslf.shelf wrack"
				+ " JOIN FETCH wrack.aisle wasl"
				+ " JOIN FETCH wasl.store wstr"
				+ " LEFT JOIN FETCH wpd.productPackageDetail pack"
				+ " LEFT JOIN FETCH pack.lookupDetail packunit"
				+ " LEFT JOIN FETCH w.productionRequisition prq"
				+ " LEFT JOIN FETCH prq.productionRequisitionDetails pprq"
				+ " LEFT JOIN FETCH wp.productionRequisitionDetail wprd"
				+ " WHERE w.workinProcessId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkinProductionByRequsitionDetailId", query = "SELECT DISTINCT(w) FROM WorkinProcessProduction w"
				+ " JOIN FETCH w.productionRequisitionDetail pr"
				+ " WHERE pr.productionRequisitionDetailId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductionByRequsitionDetailId", query = "SELECT DISTINCT(w) FROM WorkinProcessProduction w"
				+ " JOIN FETCH w.productionRequisitionDetail pr"
				+ " WHERE pr.productionRequisitionDetailId = ?"
				+ " AND w.workinProcessProductionId != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkinProcessDetailById", query = "SELECT DISTINCT(w) FROM WorkinProcessDetail w"
				+ " JOIN FETCH w.workinProcessProduction wpp"
				+ " JOIN FETCH wpp.workinProcess wp"
				+ " WHERE wp.workinProcessId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductionRequisitions", query = "SELECT DISTINCT(pr) FROM ProductionRequisition pr"
				+ " JOIN FETCH pr.person prs"
				+ " WHERE pr.implementation = ?"
				+ " AND pr.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductionRequisitionsList", query = "SELECT DISTINCT(pr) FROM ProductionRequisition pr"
				+ " JOIN FETCH pr.person prs"
				+ " WHERE pr.implementation = ?"
				+ " AND pr.isApprove = ?" + " AND pr.requisitionStatus != ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductionRequisitionById", query = "SELECT DISTINCT(pr) FROM ProductionRequisition pr"
				+ " JOIN FETCH pr.person prs"
				+ " JOIN FETCH pr.productionRequisitionDetails prds"
				+ " JOIN FETCH prds.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit unt"
				+ " LEFT JOIN FETCH prds.shelf slf"
				+ " LEFT JOIN FETCH slf.shelf rk"
				+ " LEFT JOIN FETCH rk.aisle al"
				+ " LEFT JOIN FETCH al.store str"
				+ " LEFT JOIN FETCH prds.productPackageDetail packdt"
				+ " WHERE pr.productionRequisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductionRequisitionByRequisionId", query = "SELECT DISTINCT(pr) FROM ProductionRequisition pr"
				+ " JOIN FETCH pr.productionRequisitionDetails prds"
				+ " WHERE pr.productionRequisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductionRequistionDetail", query = "SELECT DISTINCT(pr) FROM ProductionRequisitionDetail pr"
				+ " WHERE pr.productionRequisition.productionRequisitionId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAccountGroups", query = "SELECT DISTINCT(ag) FROM AccountGroup ag"
				+ " LEFT JOIN FETCH ag.accountGroupDetails agd"
				+ " LEFT JOIN FETCH agd.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE ag.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountGroupByTypes", query = "SELECT DISTINCT(ag) FROM AccountGroup ag"
				+ " LEFT JOIN FETCH ag.accountGroupDetails agd"
				+ " LEFT JOIN FETCH agd.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE ag.implementation=?"
				+ " AND (ag.accountType = ? OR ag.accountType = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountGroupByType", query = "SELECT DISTINCT(ag) FROM AccountGroup ag"
				+ " LEFT JOIN FETCH ag.accountGroupDetails agd"
				+ " LEFT JOIN FETCH agd.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE ag.implementation=?" + " AND ag.accountType = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountGroupbyId", query = "SELECT DISTINCT(ag) FROM AccountGroup ag"
				+ " LEFT JOIN FETCH ag.accountGroupDetails agd"
				+ " LEFT JOIN FETCH agd.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE ag.accountGroupId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductForStockPeriodic", query = "SELECT DISTINCT(p) FROM Product p "
				+ " JOIN FETCH p.lookupDetailByProductUnit u"
				+ " LEFT JOIN FETCH p.lookupDetailByItemNature l"
				+ " LEFT JOIN FETCH p.productCategory pc"
				+ " LEFT JOIN FETCH p.issueReturnDetails issRetDet"
				+ " LEFT JOIN FETCH issRetDet.issueReturn issRet"
				+ " LEFT JOIN FETCH p.receiveDetails reDet"
				+ " LEFT JOIN FETCH reDet.receive rece"
				+ " LEFT JOIN FETCH p.goodsReturnDetails goodRetDet"
				+ " LEFT JOIN FETCH goodRetDet.goodsReturn goodRet"
				+ " LEFT JOIN FETCH p.issueRequistionDetails issReqDet"
				+ " LEFT JOIN FETCH issReqDet.issueRequistion issReq"
				+ " LEFT JOIN FETCH p.stocks stocks"
				+ " WHERE p.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getStockPeriodicByMonth", query = "SELECT DISTINCT(sp) FROM StockPeriodic sp "
				+ " LEFT JOIN FETCH sp.product prd"
				+ " LEFT JOIN FETCH prd.lookupDetailByProductUnit punit"
				+ " LEFT JOIN FETCH sp.store str"
				+ " LEFT JOIN FETCH sp.shelf slf"
				+ " WHERE sp.implementation=? AND sp.month=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountGroupDetails", query = "SELECT DISTINCT(agd) FROM AccountGroupDetail agd"
				+ " LEFT JOIN FETCH agd.accountGroup ag"
				+ " LEFT JOIN FETCH agd.combination comb"
				+ " LEFT JOIN FETCH comb.accountByCompanyAccountId company"
				+ " LEFT JOIN FETCH comb.accountByCostcenterAccountId cost"
				+ " LEFT JOIN FETCH comb.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH comb.accountByAnalysisAccountId analysis"
				+ " LEFT JOIN FETCH comb.accountByBuffer2AccountId buffer2"
				+ " LEFT JOIN FETCH comb.accountByBuffer1AccountId buffer1"
				+ " WHERE ag.accountGroupId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountByAccountId", query = "SELECT DISTINCT(ac) FROM Account ac"
				+ " LEFT JOIN FETCH ac.segment sg"
				+ " LEFT JOIN FETCH ac.lookupDetail ld"
				+ " WHERE ac.accountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAccountByImplementationAndCode", query = "SELECT DISTINCT(ac) FROM Account ac"
				+ " JOIN FETCH ac.segment sg"
				+ " WHERE ac.implementation = ?"
				+ " AND ac.code = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getSalesDeliveryByProductId", query = "SELECT DISTINCT(sdd) FROM SalesDeliveryDetail sdd"
				+ " JOIN FETCH sdd.salesDeliveryNote sdn"
				+ " JOIN FETCH sdn.customer cs"
				+ " JOIN FETCH sdd.product p"
				+ " LEFT JOIN FETCH sdn.project proj"
				+ " LEFT JOIN FETCH sdn.projectTask projTask"
				+ " LEFT JOIN FETCH sdd.salesInvoiceDetails sid"
				+ " LEFT JOIN FETCH cs.personByPersonId pr"
				+ " LEFT JOIN FETCH cs.cmpDeptLocation cmp"
				+ " LEFT JOIN FETCH cs.company cpy"
				+ " JOIN FETCH sdd.salesOrderDetail sod"
				+ " JOIN FETCH sod.salesOrder so"
				+ " WHERE p.productId = ? ORDER BY sdd.salesDeliveryDetailId DESC "),

		@org.hibernate.annotations.NamedQuery(name = "getPurchaseDetailByProductId", query = "SELECT DISTINCT(p) FROM PurchaseDetail p"
				+ " JOIN FETCH p.purchase pr"
				+ " LEFT JOIN FETCH pr.receiveDetails rd"
				+ " LEFT JOIN FETCH pr.goodsReturnDetails gr"
				+ " WHERE p.product.productId = ? ORDER BY p.purchaseDetailId DESC "),

		@org.hibernate.annotations.NamedQuery(name = "getAllProductPackagings", query = "SELECT DISTINCT(p) FROM ProductPackage p"
				+ " LEFT JOIN FETCH p.product pr"
				+ " LEFT JOIN FETCH pr.lookupDetailByProductUnit ut"
				+ " WHERE p.implementation = ?" + " AND p.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPackagingbyId", query = "SELECT DISTINCT(p) FROM ProductPackage p"
				+ " LEFT JOIN FETCH p.product pr"
				+ " LEFT JOIN FETCH pr.lookupDetailByProductUnit ut"
				+ " LEFT JOIN FETCH p.productPackageDetails ppd"
				+ " LEFT JOIN FETCH ppd.lookupDetail ppdunit"
				+ " WHERE p.productPackageId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getAssetInfoByProduct", query = "SELECT DISTINCT(a) FROM AssetCreation a"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetType type"
				+ " LEFT JOIN FETCH a.lookupDetailByAssetSubclass sub"
				+ " LEFT JOIN FETCH a.product prd"
				+ " WHERE prd.productId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPackageDetailsById", query = "SELECT DISTINCT(p) FROM ProductPackageDetail p"
				+ " LEFT JOIN FETCH p.productPackage pp"
				+ " WHERE pp.productPackageId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPackagingsByProduct", query = "SELECT DISTINCT(p) FROM ProductPackage p"
				+ " LEFT JOIN FETCH p.product pr"
				+ " LEFT JOIN FETCH pr.lookupDetailByProductUnit ut"
				+ " LEFT JOIN FETCH p.productPackageDetails ppd"
				+ " LEFT JOIN FETCH ppd.lookupDetail ppdunit"
				+ " WHERE pr.productId = ?" + " AND p.isApprove = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getProductPackageDetailsByDetailId", query = "SELECT DISTINCT(p) FROM ProductPackageDetail p"
				+ " LEFT JOIN FETCH p.productPackage pp"
				+ " LEFT JOIN FETCH pp.product pr"
				+ " LEFT JOIN FETCH pr.lookupDetailByProductUnit ut"
				+ " WHERE p.productPackageDetailId = ?"),

/*@org.hibernate.annotations.NamedQuery(name = "getReceiveDetailByProductId", query = "SELECT DISTINCT(rd) FROM ReceiveDetail rd"
 + " JOIN FETCH rd.purchase pr"
 + " JOIN FETCH pr.purchaseDetails pd"
 + " JOIN FETCH rd.receive r"
 + " JOIN FETCH rd.product p"
 + " JOIN FETCH rd.goodsReturnDetails grd"			
 + " WHERE p.productId = ? "),*/

})
package com.aiotech.aios.accounts.domain.query;

