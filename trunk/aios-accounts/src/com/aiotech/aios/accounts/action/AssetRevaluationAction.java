package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;
import com.aiotech.aios.accounts.service.bl.AssetRevaluationBL;
import com.aiotech.aios.common.to.Constants.Accounts.RevalautionApproach;
import com.aiotech.aios.common.to.Constants.Accounts.RevalautionSource;
import com.aiotech.aios.common.to.Constants.Accounts.RevaluationMethod;
import com.aiotech.aios.common.to.Constants.Accounts.RevaluationType;
import com.aiotech.aios.common.to.Constants.Accounts.SourceExternalDetail;
import com.aiotech.aios.common.to.Constants.Accounts.SourceInternalDetail;
import com.aiotech.aios.common.to.Constants.Accounts.ValuationType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionSupport;

public class AssetRevaluationAction extends ActionSupport {

	private static final long serialVersionUID = -747993179483945291L;

	private static final Logger log = Logger
			.getLogger(AssetRevaluationAction.class);

	// Dependency
	private AssetRevaluationBL assetRevaluationBL;

	// Variables
	private long assetRevaluationId;
	private long assetId;
	private double fairMarketValue;
	private byte valuationType;
	private byte revaluationType;
	private byte revaluationMethod;
	private byte source;
	private byte sourceDetail;
	private byte apporach;
	private String referenceNumber;
	private String revaluationDate;
	private String valuationBy;
	private String description;
	private String returnMessage;
	private String assetDetails;
	private List<Object> aaData;
	private Map<Byte, String> sourceDetails;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		return SUCCESS;
	}

	// Show all Asset Reevaluation
	public String showAllAssetRevaluation() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAssetRevaluation");
			aaData = assetRevaluationBL.showAllAssetRevaluation();
			log.info("Module: Accounts : Method: showAllAssetRevaluation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllAssetRevaluation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Reevaluation Entry Screen
	public String assetRevaluationEntry() {
		try {
			log.info("Inside Module: Accounts : Method: assetRevaluationEntry");
			if (assetRevaluationId > 0) {

			} else
				referenceNumber = SystemBL.getReferenceStamp(
						AssetRevaluation.class.getName(),
						assetRevaluationBL.getImplementation());
			Map<Byte, ValuationType> valuationTypes = new HashMap<Byte, ValuationType>();
			for (ValuationType e : EnumSet.allOf(ValuationType.class))
				valuationTypes.put(e.getCode(), e);
			Map<Byte, RevaluationType> revaluationTypes = new HashMap<Byte, RevaluationType>();
			for (RevaluationType e : EnumSet.allOf(RevaluationType.class))
				revaluationTypes.put(e.getCode(), e);
			Map<Byte, RevaluationMethod> revaluationMethods = new HashMap<Byte, RevaluationMethod>();
			for (RevaluationMethod e : EnumSet.allOf(RevaluationMethod.class))
				revaluationMethods.put(e.getCode(), e);
			Map<Byte, RevalautionSource> reevalautionSources = new HashMap<Byte, RevalautionSource>();
			for (RevalautionSource e : EnumSet.allOf(RevalautionSource.class))
				reevalautionSources.put(e.getCode(), e);
			Map<Byte, RevalautionApproach> revalautionApproach = new HashMap<Byte, RevalautionApproach>();
			for (RevalautionApproach e : EnumSet
					.allOf(RevalautionApproach.class))
				revalautionApproach.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("VALUATION_TYPE",
					valuationTypes);
			ServletActionContext.getRequest().setAttribute("REVALUATION_TYPE",
					revaluationTypes);
			ServletActionContext.getRequest().setAttribute(
					"REVALUATION_METHOD", revaluationMethods);
			ServletActionContext.getRequest().setAttribute(
					"REVALUATION_SOURCE", reevalautionSources);
			ServletActionContext.getRequest().setAttribute(
					"REVALUATION_APPROACH", revalautionApproach);
			log.info("Module: Accounts : Method: assetRevaluationEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: assetRevaluationEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Reevaluation Source Detail
	public String showRevaluationSourceDetail() {
		try {
			log.info("Inside Module: Accounts : Method: showRevaluationSourceDetail");
			sourceDetails = new HashMap<Byte, String>();
			if (source == RevalautionSource.Internal.getCode())
				for (SourceInternalDetail e : EnumSet
						.allOf(SourceInternalDetail.class))
					sourceDetails.put(e.getCode(), e.name()
							.replaceAll("_", " "));
			else
				for (SourceExternalDetail e : EnumSet
						.allOf(SourceExternalDetail.class))
					sourceDetails.put(e.getCode(), e.name()
							.replaceAll("_", " "));

			log.info("Module: Accounts : Method: showRevaluationSourceDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showRevaluationSourceDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Asset Reevaluation
	public String saveAssetRevaluation() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetRevaluation");
			JSONParser parser = new JSONParser();
			Object assetObjet = parser.parse(assetDetails);
			JSONObject jsonObject = (JSONObject) assetObjet;
			assetRevaluationBL.saveAssetRevaluation(
					setAssetRevaluation(jsonObject), Double
							.parseDouble(jsonObject.get("revaluationReserve")
									.toString()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetRevaluation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetRevaluation: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteAssetRevaluation() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAssetRevaluation");
			AssetRevaluation assetRevaluation = assetRevaluationBL
					.getAssetRevaluationService().getAssetRevaluationById(
							assetRevaluationId);
			assetRevaluationBL.deleteRevaluation(assetRevaluation);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetRevaluation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: deleteAssetRevaluation: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Set Values to AssetRevaluation
	private AssetRevaluation setAssetRevaluation(JSONObject jsonObject)
			throws Exception {
		AssetRevaluation assetRevaluation = new AssetRevaluation();
		AssetCreation assetCreation = new AssetCreation();
		assetRevaluation
				.setAssetRevaluationId(assetRevaluationId > 0 ? assetRevaluationId
						: null);
		assetRevaluation.setRevaluationDate(DateFormat
				.convertStringToDate(revaluationDate));
		assetCreation.setAssetCreationId(assetId);
		assetRevaluation.setAssetCreation(assetCreation);
		assetRevaluation.setValuationType(valuationType);
		assetRevaluation.setRevaluationType(revaluationType);
		assetRevaluation.setFairMarketValue(AIOSCommons
				.roundThreeDecimals(fairMarketValue));
		assetRevaluation.setRevaluationMethod(revaluationMethod);
		assetRevaluation.setSource(source);
		assetRevaluation.setSourceDetail(sourceDetail);
		assetRevaluation.setApproach(apporach);
		assetRevaluation.setAssetBookValue(AIOSCommons
				.roundThreeDecimals(Double.parseDouble(jsonObject.get(
						"bookValue").toString())));
		assetRevaluation
				.setReferenceNumber(assetRevaluationId > 0 ? referenceNumber
						: SystemBL.getReferenceStamp(
								AssetRevaluation.class.getName(),
								assetRevaluationBL.getImplementation()));
		return assetRevaluation;
	}

	// Getters & Setters
	public AssetRevaluationBL getAssetRevaluationBL() {
		return assetRevaluationBL;
	}

	public void setAssetRevaluationBL(AssetRevaluationBL assetRevaluationBL) {
		this.assetRevaluationBL = assetRevaluationBL;
	}

	public long getAssetRevaluationId() {
		return assetRevaluationId;
	}

	public void setAssetRevaluationId(long assetRevaluationId) {
		this.assetRevaluationId = assetRevaluationId;
	}

	public long getAssetId() {
		return assetId;
	}

	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}

	public byte getValuationType() {
		return valuationType;
	}

	public void setValuationType(byte valuationType) {
		this.valuationType = valuationType;
	}

	public byte getRevaluationType() {
		return revaluationType;
	}

	public void setRevaluationType(byte revaluationType) {
		this.revaluationType = revaluationType;
	}

	public byte getRevaluationMethod() {
		return revaluationMethod;
	}

	public void setRevaluationMethod(byte revaluationMethod) {
		this.revaluationMethod = revaluationMethod;
	}

	public byte getSource() {
		return source;
	}

	public void setSource(byte source) {
		this.source = source;
	}

	public byte getSourceDetail() {
		return sourceDetail;
	}

	public void setSourceDetail(byte sourceDetail) {
		this.sourceDetail = sourceDetail;
	}

	public byte getApporach() {
		return apporach;
	}

	public void setApporach(byte apporach) {
		this.apporach = apporach;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRevaluationDate() {
		return revaluationDate;
	}

	public void setRevaluationDate(String revaluationDate) {
		this.revaluationDate = revaluationDate;
	}

	public String getValuationBy() {
		return valuationBy;
	}

	public void setValuationBy(String valuationBy) {
		this.valuationBy = valuationBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getAssetDetails() {
		return assetDetails;
	}

	public void setAssetDetails(String assetDetails) {
		this.assetDetails = assetDetails;
	}

	public double getFairMarketValue() {
		return fairMarketValue;
	}

	public void setFairMarketValue(double fairMarketValue) {
		this.fairMarketValue = fairMarketValue;
	}

	public Map<Byte, String> getSourceDetails() {
		return sourceDetails;
	}

	public void setSourceDetails(Map<Byte, String> sourceDetails) {
		this.sourceDetails = sourceDetails;
	}
}
