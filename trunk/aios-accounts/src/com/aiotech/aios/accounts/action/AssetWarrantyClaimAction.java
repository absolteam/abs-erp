package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.AssetWarranty;
import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.service.bl.AssetWarrantyClaimBL;
import com.aiotech.aios.common.to.Constants.Accounts.POStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentRequestMode;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AssetWarrantyClaimAction extends ActionSupport {

	private static final long serialVersionUID = 6424971198173470666L;

	private static final Logger log = Logger
			.getLogger(AssetWarrantyClaimAction.class);

	// Dependency
	private AssetWarrantyClaimBL assetWarrantyClaimBL;

	// Variables
	private long assetWarrantyClaimId;
	private long assetWarrantyId;
	private long payoutNature;
	private long recordId;
	private long alertId;
	private double claimAmount;
	private double claimExpense;
	private double payoutAmount;
	private byte paymentMode;
	private byte status;
	private String claimDate;
	private String description;
	private String referenceNumber;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	// Show all Asset Warranty Claim
	public String showAllAssetWarrantyClaim() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAssetWarrantyClaim");
			aaData = assetWarrantyClaimBL.showAllAssetWarrantyClaim();
			log.info("Module: Accounts : Method: showAllAssetWarrantyClaim: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllAssetWarrantyClaim Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Warranty Claim
	public String showAssetWarrantyClaimEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetWarrantyClaimEntry");
			if (assetWarrantyClaimId > 0) {
				Object assetWarrantyClaim = assetWarrantyClaimBL
						.getAssetWarrantyClaimService()
						.getAssetWarrantyClaimById(assetWarrantyClaimId);
				ServletActionContext.getRequest().setAttribute(
						"ASSET_WARRANTY_CLAIM", assetWarrantyClaim);
			}
			Map<Byte, POStatus> status = new HashMap<Byte, POStatus>();
			for (POStatus e : EnumSet.allOf(POStatus.class))
				status.put(e.getCode(), e);
			Map<Byte, PaymentRequestMode> modeOfPayment = new HashMap<Byte, PaymentRequestMode>();
			for (PaymentRequestMode e : EnumSet.allOf(PaymentRequestMode.class))
				modeOfPayment.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute(
					"PAYOUT_NATURE",
					assetWarrantyClaimBL.getLookupMasterBL()
							.getActiveLookupDetails(
									"ASSET_WARRANTY_PAYOUT_NATURE", true));
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					modeOfPayment);
			ServletActionContext.getRequest().setAttribute("CLAIM_STATUS",
					status);
			referenceNumber = SystemBL.getReferenceStamp(
					AssetWarrantyClaim.class.getName(),
					assetWarrantyClaimBL.getImplementation());
			log.info("Module: Accounts : Method: showAssetWarrantyClaimEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetWarrantyClaimEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String claimWarrantyDirectPayment() {
		try {
			log.info("Inside Module: Accounts : Method: claimWarrantyDirectPayment");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;
			assetWarrantyClaimId = recordId;
			List<Currency> currencyList = null;
			AssetWarrantyClaim assetWarrantyClaim = assetWarrantyClaimBL
					.getAssetWarrantyClaimService().getAssetWarrantyClaimById(
							assetWarrantyClaimId);
			if (assetWarrantyClaim != null) {
				currencyList = assetWarrantyClaimBL
						.getDirectPaymentBL()
						.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(
								assetWarrantyClaimBL.getImplementation());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(),
						assetWarrantyClaimBL.getImplementation());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(assetWarrantyClaim
						.getAssetWarrantyClaimId());
				directPayment.setUseCase(assetWarrantyClaim.getClass()
						.getSimpleName());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
				directPaymentDetail.setAmount(assetWarrantyClaim
						.getClaimExpense());
				directPaymentDetail.setInvoiceNumber(assetWarrantyClaim
						.getClaimReference());
				directPayDetails.add(directPaymentDetail);
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_MODE",
					assetWarrantyClaimBL.getDirectPaymentBL()
							.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute(
					"PAYEE_TYPES",
					assetWarrantyClaimBL.getDirectPaymentBL()
							.getReceiptSource());
			log.info("Module: Accounts : Method: claimWarrantyDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: claimWarrantyDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Warranty Claim
	public String saveAssetWarrantyClaim() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetWarrantyClaim");
			assetWarrantyClaimBL
					.saveAssetWarrantyClaim(setAssetWarrantyClaim());
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetWarrantyClaim: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetWarrantyClaim: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Asset Warranty Claim
	public String deleteAssetWarrantyClaim() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAssetWarrantyClaim");
			assetWarrantyClaimBL.deleteAssetWarrantyClaim(assetWarrantyClaimBL
					.getAssetWarrantyClaimService()
					.getSimpleAssetWarrantyClaimById(assetWarrantyClaimId));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetWarrantyClaim: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: deleteAssetWarrantyClaim: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Set Asset Warranty Claim
	private AssetWarrantyClaim setAssetWarrantyClaim() {
		AssetWarrantyClaim assetWarrantyClaim = new AssetWarrantyClaim();
		assetWarrantyClaim
				.setAssetWarrantyClaimId(assetWarrantyClaimId > 0 ? assetWarrantyClaimId
						: null);
		assetWarrantyClaim.setClaimDate(DateFormat
				.convertStringToDate(claimDate));
		assetWarrantyClaim.setClaimAmount(claimAmount > 0 ? claimAmount : null);
		assetWarrantyClaim.setStatus(status);
		assetWarrantyClaim.setClaimExpense(claimExpense > 0 ? claimExpense
				: null);
		assetWarrantyClaim.setPayoutAmount(payoutAmount > 0 ? payoutAmount
				: null);
		assetWarrantyClaim.setModeOfPayment(paymentMode > 0 ? paymentMode
				: null);
		assetWarrantyClaim.setDescription(description);
		if (payoutNature > 0) {
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(payoutNature);
			assetWarrantyClaim.setLookupDetail(lookupDetail);
		}
		AssetWarranty assetWarranty = new AssetWarranty();
		assetWarranty.setAssetWarrantyId(assetWarrantyId);
		assetWarrantyClaim.setAssetWarranty(assetWarranty);
		assetWarrantyClaim
				.setClaimReference(assetWarrantyClaimId == 0 ? SystemBL
						.getReferenceStamp(AssetWarrantyClaim.class.getName(),
								assetWarrantyClaimBL.getImplementation())
						: referenceNumber);
		return assetWarrantyClaim;
	}

	// Getters & Setters
	public AssetWarrantyClaimBL getAssetWarrantyClaimBL() {
		return assetWarrantyClaimBL;
	}

	public void setAssetWarrantyClaimBL(
			AssetWarrantyClaimBL assetWarrantyClaimBL) {
		this.assetWarrantyClaimBL = assetWarrantyClaimBL;
	}

	public long getAssetWarrantyClaimId() {
		return assetWarrantyClaimId;
	}

	public void setAssetWarrantyClaimId(long assetWarrantyClaimId) {
		this.assetWarrantyClaimId = assetWarrantyClaimId;
	}

	public long getAssetWarrantyId() {
		return assetWarrantyId;
	}

	public void setAssetWarrantyId(long assetWarrantyId) {
		this.assetWarrantyId = assetWarrantyId;
	}

	public long getPayoutNature() {
		return payoutNature;
	}

	public void setPayoutNature(long payoutNature) {
		this.payoutNature = payoutNature;
	}

	public double getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(double claimAmount) {
		this.claimAmount = claimAmount;
	}

	public double getClaimExpense() {
		return claimExpense;
	}

	public void setClaimExpense(double claimExpense) {
		this.claimExpense = claimExpense;
	}

	public double getPayoutAmount() {
		return payoutAmount;
	}

	public void setPayoutAmount(double payoutAmount) {
		this.payoutAmount = payoutAmount;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

}
