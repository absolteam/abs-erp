package com.aiotech.aios.accounts.action;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.vo.AssetCheckOutVO;
import com.aiotech.aios.accounts.service.bl.AssetCheckOutBL;
import com.aiotech.aios.common.to.Constants.Accounts.AssetCheckOutDue;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AssetCheckOutAction extends ActionSupport {

	private static final long serialVersionUID = 6873040406464002822L;

	private static final Logger log = Logger
			.getLogger(AssetCheckOutAction.class);

	private AssetCheckOutBL assetCheckOutBL;
	private List<Object> aaData;
	private long assetCheckOutId;
	private long assetCreationId;
	private long checkOutBy;
	private long checkOutTo;
	private long cmpDeptLocationId;
	private Long recordId;
	private byte checkOutDue;
	private boolean status;
	private String description;
	private String checkOutNumber;
	private String assetCheckOutDate;
	private String returnMessage;
	private String personName;

	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show All Asset Check Out JSON list
	public String showCheckOutJsonList() {
		try {
			log.info("Inside Module: Accounts : Method: showCheckOutJsonList");
			aaData = assetCheckOutBL.showCheckOutJsonList();
			log.info("Module: Accounts : Method: showCheckOutJsonList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCheckOutJsonList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showAssetCheckOutApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckOutApproval");
			assetCheckOutId = recordId;
			showAssetCheckoutEntry();
			log.info("Module: Accounts : Method: showAssetCheckOutApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckOutApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetCheckOutReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckOutReject");
			assetCheckOutId = recordId;
			showAssetCheckoutEntry();
			log.info("Module: Accounts : Method: showAssetCheckOutReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckOutReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Entry Screen
	public String showAssetCheckoutEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckoutEntry");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Map<Byte, AssetCheckOutDue> checkOutDue = new HashMap<Byte, AssetCheckOutDue>();
			for (AssetCheckOutDue e : EnumSet.allOf(AssetCheckOutDue.class)) {
				checkOutDue.put(e.getCode(), e);
			}
			checkOutBy = 0l;
			personName = null;
			if (assetCheckOutId > 0) {
				AssetCheckOut checkOut = assetCheckOutBL
						.getAssetCheckOutService().getAssetCheckOutDetails(
								assetCheckOutId);
				AssetCheckOutVO assetCheckOut = convertCheckOut(checkOut);
				ServletActionContext.getRequest().setAttribute("CHECK_OUT",
						assetCheckOut);
				CommentVO comment = new CommentVO();
				if (recordId != null && checkOut != null
						&& checkOut.getAssetCheckOutId() != 0 && recordId > 0) {
					comment.setRecordId(checkOut.getAssetCheckOutId());
					comment.setUseCase(AssetCheckOut.class.getName());
					comment = assetCheckOutBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			} else {
				checkOutNumber = getAssetCheckOutReference();
				checkOutBy = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute("CHECK_OUT_DUE",
					checkOutDue);
			log.info("Module: Accounts : Method: showAssetCheckoutEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckoutEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Check Out Details
	public String showAssetCheckOutList() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckOutList");
			aaData = assetCheckOutBL.showActiveCheckOutJsonList();
			log.info("Module: Accounts : Method: showAssetCheckOutList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckOutList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Check Out Details
	public String showAssetCheckOutCheckIn() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckOutCheckIn");
			List<AssetCheckOut> assetCheckOuts = assetCheckOutBL
					.getAssetCheckOutService().getActiveAssetCheckOuts(
							assetCheckOutBL.getImplementation());
			List<AssetCheckOutVO> assetCheckOutVOs = convertCheckOuts(assetCheckOuts);
			ServletActionContext.getRequest().setAttribute(
					"ASSET_CHECKOUT_INFO", assetCheckOutVOs);
			log.info("Module: Accounts : Method: showAssetCheckOutCheckIn: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckOutCheckIn Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Check Out
	public String saveAssetCheckOut() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetCheckOut");
			AssetCheckOut assetCheckOut = new AssetCheckOut();
			if (assetCheckOutId > 0) {
				assetCheckOut.setAssetCheckOutId(assetCheckOutId);
				assetCheckOut.setCheckOutNumber(checkOutNumber);
			} else
				assetCheckOut.setCheckOutNumber(getAssetCheckOutReference());
			if (null != assetCheckOutDate && !("").equals(assetCheckOutDate))
				assetCheckOut.setCheckOutDate(DateFormat
						.convertStringToDate(assetCheckOutDate));
			assetCheckOut.setCheckOutDue(checkOutDue);
			assetCheckOut.setDescription(description);
			Person person = new Person();
			person.setPersonId(checkOutBy);
			assetCheckOut.setPersonByCheckOutBy(person);
			person = new Person();
			person.setPersonId(checkOutTo);
			assetCheckOut.setPersonByCheckOutTo(person);
			AssetCreation assetCreation = new AssetCreation();
			assetCreation.setAssetCreationId(assetCreationId);
			assetCheckOut.setAssetCreation(assetCreation);
			CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
			cmpDeptLocation.setCmpDeptLocId(cmpDeptLocationId);
			assetCheckOut.setCmpDeptLocation(cmpDeptLocation);
			assetCheckOut.setStatus(true);
			assetCheckOutBL.saveAssetCheckOut(assetCheckOut);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetCheckOut: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Save failure.";
			log.info("Module: Accounts : Method: saveAssetCheckOut Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Asset Check out
	public String deleteAssetCheckOut() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAssetCheckOut");
			AssetCheckOut assetCheckOut = assetCheckOutBL
					.getAssetCheckOutService().getAssetCheckOutDetails(
							assetCheckOutId);
			assetCheckOutBL.deleteAssetCheckOut(assetCheckOut);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetCheckOut: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Save failure.";
			log.info("Module: Accounts : Method: deleteAssetCheckOut Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<AssetCheckOutVO> convertCheckOuts(
			List<AssetCheckOut> assetCheckOuts) throws Exception {
		List<AssetCheckOutVO> assetCheckOutVOs = new ArrayList<AssetCheckOutVO>();
		for (AssetCheckOut assetCheckOut : assetCheckOuts) {
			assetCheckOutVOs.add(convertCheckOut(assetCheckOut));
		}
		return assetCheckOutVOs;
	}

	private AssetCheckOutVO convertCheckOut(AssetCheckOut checkOut)
			throws IllegalAccessException, InvocationTargetException {
		AssetCheckOutVO assetCheckOutVO = new AssetCheckOutVO();
		BeanUtils.copyProperties(assetCheckOutVO, checkOut);
		assetCheckOutVO.setOutDate(DateFormat.convertDateToString(checkOut
				.getCheckOutDate().toString()));
		return assetCheckOutVO;
	}

	// Generate Reference number
	public String getAssetCheckOutReference() throws Exception {
		return SystemBL.getReferenceStamp(AssetCheckOut.class.getName(),
				assetCheckOutBL.getImplementation());
	}

	// Getters & Setters
	public AssetCheckOutBL getAssetCheckOutBL() {
		return assetCheckOutBL;
	}

	public void setAssetCheckOutBL(AssetCheckOutBL assetCheckOutBL) {
		this.assetCheckOutBL = assetCheckOutBL;
	}

	public long getAssetCheckOutId() {
		return assetCheckOutId;
	}

	public void setAssetCheckOutId(long assetCheckOutId) {
		this.assetCheckOutId = assetCheckOutId;
	}

	public long getAssetCreationId() {
		return assetCreationId;
	}

	public void setAssetCreationId(long assetCreationId) {
		this.assetCreationId = assetCreationId;
	}

	public String getAssetCheckOutDate() {
		return assetCheckOutDate;
	}

	public void setAssetCheckOutDate(String assetCheckOutDate) {
		this.assetCheckOutDate = assetCheckOutDate;
	}

	public long getCheckOutBy() {
		return checkOutBy;
	}

	public void setCheckOutBy(long checkOutBy) {
		this.checkOutBy = checkOutBy;
	}

	public long getCmpDeptLocationId() {
		return cmpDeptLocationId;
	}

	public void setCmpDeptLocationId(long cmpDeptLocationId) {
		this.cmpDeptLocationId = cmpDeptLocationId;
	}

	public byte getCheckOutDue() {
		return checkOutDue;
	}

	public void setCheckOutDue(byte checkOutDue) {
		this.checkOutDue = checkOutDue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getCheckOutNumber() {
		return checkOutNumber;
	}

	public void setCheckOutNumber(String checkOutNumber) {
		this.checkOutNumber = checkOutNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getCheckOutTo() {
		return checkOutTo;
	}

	public void setCheckOutTo(long checkOutTo) {
		this.checkOutTo = checkOutTo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
