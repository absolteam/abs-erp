package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.accounts.service.bl.GoodsReturnBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class GoodsReturnAction extends ActionSupport {

	private static final long serialVersionUID = -8893127826092161330L;
	private long returnId;
	private long supplierId;
	private long receiveId;
	private String returnDate;
	private String description;
	private String returnNumber;
	private GoodsReturnBL goodsReturnBL;
	private String returnMessage;
	private String returnDetails;
	private List<Object> aaData;
	private Long recordId;
	private String printableContent;

	private static final Logger LOGGER = LogManager
			.getLogger(GoodsReturnAction.class);

	public String returnSuccess() {
		return SUCCESS;
	}

	public String showGoodsReturnJson() {
		LOGGER.info("Inside Module: Accounts : Method: showGoodsReturnJson");
		try {
			JSONObject jsonList = goodsReturnBL
					.getGoodsReturn(getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showGoodsReturnJson: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showGoodsReturnJson: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show GoodsReturn to edit/delete purchase info
	public String showGoodsReturnEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showGoodsReturnEntry");
		try {
			returnNumber = null;
			if (recordId != null && recordId > 0) {
				returnId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (returnId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = goodsReturnBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (returnId > 0) {
				GoodsReturn goodsReturn = goodsReturnBL.getGoodsReturnService()
						.getGoodsReturnByReturnId(returnId);
				GoodsReturnVO goodsReturnVO = goodsReturnBL
						.addGoodsReturnDetailVO(goodsReturn);

				ServletActionContext.getRequest().setAttribute("GOODS_RETURN",
						goodsReturnVO);
			} else {
				returnNumber = SystemBL.getReferenceStamp(
						GoodsReturn.class.getName(),
						goodsReturnBL.getImplementation());
			}
			LOGGER.info("Module: Accounts : Method: showGoodsReturnEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showGoodsReturnEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveGoodsReturn() {
		LOGGER.info("Inside Module: Accounts : Method: saveGoodsReturn");
		try {
			if (returnId > 0) {
				GoodsReturn goodsReturn = goodsReturnBL.getGoodsReturnService()
						.getGoodsReturnByReturnId(returnId);
				if (receiveId > 0) {
					Receive receive = new Receive();
					receive.setReceiveId(receiveId);
					goodsReturn.setReceive(receive);
				}
				if (null != goodsReturn.getIsDebit()
						&& (boolean) goodsReturn.getIsDebit()) {
					returnMessage = saveReceiveGoodsReturn(goodsReturn);
				} else
					returnMessage = updateGoodsReturn(goodsReturn);
			} else
				returnMessage = saveReceiveGoodsReturn(null);
			LOGGER.info("Module: Accounts : Method: saveGoodsReturn: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "System error.";
			LOGGER.info("Module: Accounts : Method: saveGoodsReturn: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private String saveReceiveGoodsReturn(GoodsReturn goodsReturn)
			throws Exception {
		List<GoodsReturnDetail> returnDetailsOld = null;
		if (goodsReturn != null) {
			goodsReturn.setReturnNumber(goodsReturn.getReturnNumber());
			goodsReturn.setGoodsReturnId(goodsReturn.getGoodsReturnId());
			returnDetailsOld = new ArrayList<GoodsReturnDetail>(
					goodsReturn.getGoodsReturnDetails());
		} else {
			goodsReturn = new GoodsReturn();
			goodsReturn.setReturnNumber(SystemBL.getReferenceStamp(
					GoodsReturn.class.getName(),
					goodsReturnBL.getImplementation()));
		}
		Supplier supplier = new Supplier();
		supplier.setSupplierId(supplierId);
		goodsReturn.setSupplier(supplier);
		if (receiveId > 0) {
			Receive receive = new Receive();
			receive.setReceiveId(receiveId);
			goodsReturn.setReceive(receive);
		}
		goodsReturn.setDate(DateFormat.convertStringToDate(returnDate));
		goodsReturn.setIsDebit(true);
		goodsReturn.setDescription(description);
		GoodsReturnDetail goodsDetail = null;
		ReceiveDetail receiveDetail = null;
		List<GoodsReturnDetail> goodsReturnDetails = new ArrayList<GoodsReturnDetail>();
		Product product = null;
		ProductPackageDetail productPackageDetail = null;
		if (null != returnDetails && !("").equals(returnDetails)) {
			String[] returnDetail = splitValues(returnDetails, "#@");
			for (String string : returnDetail) {
				String[] returnLine = splitValues(string, "__");
				goodsDetail = new GoodsReturnDetail();
				product = new Product();
				product.setProductId(Long.parseLong(returnLine[0]));
				goodsDetail.setReturnQty(Double.parseDouble(returnLine[1]));
				if (Long.parseLong(returnLine[2]) > 0)
					goodsDetail
							.setReturnDetailId(Long.parseLong(returnLine[2]));
				if (Long.parseLong(returnLine[4]) > 0) {
					receiveDetail = goodsReturnBL
							.getPurchaseBL()
							.getReceiveService()
							.getReceiveDetailById(Long.parseLong(returnLine[4]));
					goodsDetail.setReceiveDetail(receiveDetail);
				}
				if (Long.parseLong(returnLine[5]) > 0) {
					productPackageDetail = new ProductPackageDetail();
					productPackageDetail.setProductPackageDetailId(Long
							.parseLong(returnLine[5]));
					goodsDetail.setProductPackageDetail(productPackageDetail);
				}
				goodsDetail.setPackageUnit(Double.parseDouble(returnLine[6]));
				goodsDetail.setProduct(product);
				if (goodsDetail.getReturnQty() > 0)
					goodsReturnDetails.add(goodsDetail);
			}
			List<GoodsReturnDetail> deleteReturns = null;
			if (null != returnDetailsOld && returnDetailsOld.size() > 0)
				deleteReturns = goodsReturnBL.getSimilarReturns(
						returnDetailsOld, goodsReturnDetails);
			boolean returnFlag = validateGoodsReturn(goodsReturnDetails);
			if (returnFlag)
				goodsReturnBL.saveGoodsReturns(goodsReturn, goodsReturnDetails,
						deleteReturns);
			else
				return "Goods returns should be eq or lt receive quantity.";
		}
		return "SUCCESS";
	}

	private boolean validateGoodsReturn(
			List<GoodsReturnDetail> goodsReturnDetails) throws Exception {
		boolean returnFlag = true;
		double returnQty = 0;
		for (GoodsReturnDetail goodsReturnDetail : goodsReturnDetails) {
			returnQty = goodsReturnDetail.getReturnQty();
			List<GoodsReturnDetail> returnDetails = goodsReturnBL
					.getPurchaseBL()
					.getGoodsReturnService()
					.getGoodsReturnDetailsWithGRNDetail(
							goodsReturnDetail.getReceiveDetail()
									.getReceiveDetailId());
			if (null != returnDetails && returnDetails.size() > 0) {
				for (GoodsReturnDetail goodsReturnDetail2 : returnDetails) {
					returnQty += goodsReturnDetail2.getReturnQty();
				}
			}
			if (returnQty > goodsReturnDetail.getReceiveDetail()
					.getReceiveQty()) {
				returnFlag = false;
				break;
			}
		}
		return returnFlag;
	}

	private String updateGoodsReturn(GoodsReturn goodsReturn) throws Exception {
		List<GoodsReturnDetail> returnDetailList = new ArrayList<GoodsReturnDetail>(
				goodsReturn.getGoodsReturnDetails());
		GoodsReturnDetail goodsDetail = null;
		Product product = null;
		Purchase purchase = null;
		List<GoodsReturnDetail> goodsReturnDetailList = new ArrayList<GoodsReturnDetail>();
		Map<Long, PurchaseVO> recordedInv = new HashMap<Long, PurchaseVO>();
		List<Purchase> purchaseList = new ArrayList<Purchase>();
		PurchaseVO purchaseVO = null;
		PurchaseDetailVO detailVO = null;
		List<PurchaseDetailVO> detailVOList = null;
		returnMessage = "SUCCESS";
		for (GoodsReturnDetail list : returnDetailList) {
			purchaseVO = new PurchaseVO();
			purchaseVO.setPurchaseId(list.getPurchase().getPurchaseId());
			purchaseVO.setCurrency(list.getPurchase().getCurrency());
			purchaseVO.setPurchaseId(list.getPurchase().getPurchaseId());
			purchaseVO
					.setPurchaseNumber(list.getPurchase().getPurchaseNumber());
			purchaseVO.setCurrency(list.getPurchase().getCurrency());
			purchaseVO.setDate(list.getPurchase().getDate());
			purchaseVO.setDescription(list.getPurchase().getDescription());
			purchaseVO.setExpiryDate(list.getPurchase().getExpiryDate());
			purchaseVO.setSupplier(list.getPurchase().getSupplier());
			purchaseVO.setQuotation(list.getPurchase().getQuotation());
			purchaseVO.setImplementation(getImplementationId());
			detailVOList = new ArrayList<PurchaseDetailVO>();
			for (PurchaseDetail detail : list.getPurchase()
					.getPurchaseDetails()) {
				detailVO = new PurchaseDetailVO();
				detailVO.setProduct(detail.getProduct());
				detailVO.setQuantity(detail.getQuantity());
				detailVO.setPurchaseDetailId(detail.getPurchaseDetailId());
				detailVOList.add(detailVO);
			}
			purchaseVO.setPurchaseDetailVO(detailVOList);
			purchaseVO
					.setReceiveDetails(list.getPurchase().getReceiveDetails());
			recordedInv.put(list.getPurchase().getPurchaseId(), purchaseVO);
		}
		if (null != returnDetails && !("").equals(returnDetails)) {
			String[] returnDetail = splitValues(returnDetails, "#@");
			for (String string : returnDetail) {
				String[] returnLine = splitValues(string, "__");
				goodsDetail = new GoodsReturnDetail();
				product = new Product();
				purchase = new Purchase();
				product.setProductId(Long.parseLong(returnLine[0]));
				goodsDetail.setReturnQty(Double.parseDouble(returnLine[1]));
				goodsDetail.setReturnDetailId(Long.parseLong(returnLine[2]));
				purchase.setPurchaseId(Long.parseLong(returnLine[3]));
				goodsDetail.setProduct(product);
				goodsDetail.setPurchase(purchase);
				if (goodsDetail.getReturnQty() > 0)
					goodsReturnDetailList.add(goodsDetail);
			}
			if (goodsReturnBL.validateReturnGoods(goodsReturnDetailList,
					recordedInv)) {
				List<GoodsReturnDetail> deleteReturnList = goodsReturnBL
						.getSimilarReturns(returnDetailList,
								goodsReturnDetailList);
				Map<Long, Purchase> purchaseMap = new HashMap<Long, Purchase>();
				for (Long purchaseId : goodsReturnBL.getPurchaseInfo()) {
					if (null != purchaseMap
							&& !purchaseMap.containsKey(purchaseId)) {
						PurchaseVO voMap = recordedInv.get(purchaseId);
						purchase = new Purchase();
						purchase.setPurchaseId(purchaseId);
						purchase.setCurrency(voMap.getCurrency());
						purchase.setPurchaseId(voMap.getPurchaseId());
						purchase.setPurchaseNumber(voMap.getPurchaseNumber());
						purchase.setCurrency(voMap.getCurrency());
						purchase.setDate(voMap.getDate());
						purchase.setDescription(voMap.getDescription());
						purchase.setExpiryDate(voMap.getExpiryDate());
						purchase.setSupplier(voMap.getSupplier());
						purchase.setQuotation(voMap.getQuotation());
						purchase.setImplementation(getImplementationId());
						purchase.setStatus(1);
						purchaseMap.put(purchaseId, purchase);
					}
				}
				goodsReturnBL.saveReturns(goodsReturn, goodsReturnDetailList,
						deleteReturnList,
						new ArrayList<Purchase>(purchaseMap.values()));
			} else {
				returnMessage = "Goods returns should be eq or lt purchased quantity.";
				return returnMessage;
			}
		} else {
			for (Long purchaseId : recordedInv.keySet()) {
				PurchaseVO voMap = recordedInv.get(purchaseId);
				purchase = new Purchase();
				purchase.setPurchaseId(purchaseId);
				purchase.setCurrency(voMap.getCurrency());
				purchase.setPurchaseId(voMap.getPurchaseId());
				purchase.setPurchaseNumber(voMap.getPurchaseNumber());
				purchase.setCurrency(voMap.getCurrency());
				purchase.setDate(voMap.getDate());
				purchase.setDescription(voMap.getDescription());
				purchase.setExpiryDate(voMap.getExpiryDate());
				purchase.setSupplier(voMap.getSupplier());
				purchase.setQuotation(voMap.getQuotation());
				purchase.setImplementation(getImplementationId());
				purchase.setStatus(1);
				purchaseList.add(purchase);
			}
			goodsReturnBL.deleteGoodsReturn(goodsReturn, returnDetailList,
					purchaseList);
		}
		return returnMessage;
	}

	public String showPurchaseOrders() {
		LOGGER.info("Module: Accounts : Method: showPurchaseOrders");
		try {
			List<Purchase> purchaseList = goodsReturnBL.getPurchaseBL()
					.getPurchaseService()
					.getPurchaseReceiveOrder(getImplementationId());

			List<PurchaseVO> purchaseReceiveList = null;
			if (null != purchaseList && purchaseList.size() > 0) {
				List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>();
				for (Purchase purchase : purchaseList)
					purchaseDetails.addAll(new ArrayList<PurchaseDetail>(
							purchase.getPurchaseDetails()));
				purchaseReceiveList = goodsReturnBL.getPurchaseBL()
						.validatePurchaseReceive(purchaseDetails);
			}

			ServletActionContext.getRequest().setAttribute("PURCHASE_INFO",
					purchaseReceiveList);
			LOGGER.info("Module: Accounts : Method: showPurchaseOrders: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseOrders: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get all active Return Notes for Debit Note
	public String showAllReturnNote() {
		LOGGER.info("Module: Accounts : Method: showAllReturnNote");
		try {
			aaData = goodsReturnBL.getActiveGoodsReturn(getImplementationId());
			LOGGER.info("Module: Accounts : Method: showAllReturnNote: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllReturnNote: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get all Return Notes Detail for Debit Note Detail with Goods return Id
	public String showReturnDetailList() {
		LOGGER.info("Module: Accounts : Method: showReturnDetailList");
		try {
			List<GoodsReturnDetail> goodsReturnDetails = goodsReturnBL
					.getGoodsReturnService().getGoodsReturnDetails(returnId);
			List<GoodsReturnDetailVO> goodsReturnDetailVOs = null;
			GoodsReturnDetailVO returnDetailVO = null;
			if (null != goodsReturnDetails && goodsReturnDetails.size() > 0) {
				goodsReturnDetailVOs = new ArrayList<GoodsReturnDetailVO>();
				for (GoodsReturnDetail dtVO : goodsReturnDetails) {
					returnDetailVO = new GoodsReturnDetailVO();
					BeanUtils.copyProperties(returnDetailVO, dtVO);
					returnDetailVO.setPackageUnit(dtVO.getPackageUnit());
					returnDetailVO.setProductPackageVOs(goodsReturnBL
							.getPurchaseBL()
							.getPackageConversionBL()
							.getProductPackagingDetail(
									dtVO.getProduct().getProductId()));

					if (null != dtVO.getProductPackageDetail()) {
						returnDetailVO.setPackageDetailId(dtVO
								.getProductPackageDetail()
								.getProductPackageDetailId());
						boolean iflag = true;
						for (ProductPackageVO packagevo : returnDetailVO
								.getProductPackageVOs()) {
							if (iflag) {
								for (ProductPackageDetailVO packageDtvo : packagevo
										.getProductPackageDetailVOs()) {
									if ((long) packageDtvo
											.getProductPackageDetailId() == (long) dtVO
											.getProductPackageDetail()
											.getProductPackageDetailId()) {
										returnDetailVO
												.setConversionUnitName(packageDtvo
														.getConversionUnitName());
										iflag = true;
										break;
									}
								}
							} else
								break;
						}
						returnDetailVO.setBaseUnitName(dtVO.getProduct()
								.getLookupDetailByProductUnit()
								.getDisplayName());
					} else {
						returnDetailVO.setConversionUnitName(dtVO.getProduct()
								.getLookupDetailByProductUnit()
								.getDisplayName());
						returnDetailVO.setPackageDetailId(-1l);
					}
					goodsReturnDetailVOs.add(returnDetailVO);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"RETURN_LINE_DETAILS", goodsReturnDetailVOs);
			LOGGER.info("Module: Accounts : Method: showReturnDetailList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showReturnDetailList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteGoodsReturn() {
		LOGGER.info("Module: Accounts : Method: deleteGoodsReturn");
		try {
			GoodsReturn goodsReturn = goodsReturnBL.getGoodsReturnService()
					.getGoodsReturns(returnId);
			List<GoodsReturnDetail> goodsReturnDetails = new ArrayList<GoodsReturnDetail>(
					goodsReturn.getGoodsReturnDetails());
			List<Purchase> purchases = new ArrayList<Purchase>();
			for (GoodsReturnDetail returnDetail : goodsReturnDetails) {
				if (null != returnDetail.getPurchase()) {
					returnDetail.getPurchase().setStatus(1);
					purchases.add(returnDetail.getPurchase());
				}
			}
			goodsReturnBL.deleteGoodsReturn(goodsReturn, goodsReturnDetails,
					purchases);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteGoodsReturn: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Delete failure.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteGoodsReturn: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String goodsReturnPrintout() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_goodsreturn_template") != null ? ((String) session
					.getAttribute("print_goodsreturn_template"))
					.equalsIgnoreCase("true") : false;

			GoodsReturn goodsReturn = goodsReturnBL.getGoodsReturnService()
					.getGoodsReturnInfoByReceive(returnId);
			GoodsReturnVO vO = goodsReturnBL
					.addGoodsReturnDetailVO(goodsReturn);

			Collections.sort(vO.getGoodsReturnDetailVOs(),
					new Comparator<GoodsReturnDetailVO>() {
						public int compare(GoodsReturnDetailVO t1,
								GoodsReturnDetailVO t2) {
							return t1.getReturnDetailId().compareTo(
									t2.getReturnDetailId());
						}
					});

			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute("RETURN_INFO",
						vO);
				return "default";
			} else {

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ getImplementationId().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-goodsreturn-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementationId().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementationId().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put(
						"datetime",
						vO.getDate() != null ? DateFormat
								.convertSystemDateToString(vO.getDate()) : vO
								.getGoodsReturnDate());

				rootMap.put("referenceNumber", vO.getReturnNumber());
				rootMap.put("purchaseNumber", vO.getPurchaseNumber());
				rootMap.put("purchaseDate", vO.getPurchaseDate());
				rootMap.put("reason", vO.getDescription());
				rootMap.put("receiveNumber", vO.getReceive().getReceiveNumber());
				rootMap.put(
						"receiveDate",
						DateFormat.convertDateToString(vO.getReceive()
								.getReceiveDate() + ""));

				rootMap.put("invoiceNumber", vO.getReceive().getReferenceNo());
				rootMap.put("deliveryNumber", vO.getReceive().getDeliveryNo());
				rootMap.put("supplier", vO.getSupplierName().toUpperCase());
				rootMap.put("totalAmount", vO.getDisplayTotalAmount());
				rootMap.put("totalAmountInWords",
						vO.getDisplayTotalAmountInWords());
				rootMap.put("returnDetails", vO.getGoodsReturnDetailVOs());
				rootMap.put("urlPath", urlPath);
				rootMap.put("extraLines",
						vO.getGoodsReturnDetailVOs().size() < 10 ? 10 - vO
								.getGoodsReturnDetailVOs().size() : null);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				LOGGER.info("Module: Accounts : Method: showReceiveEntry: Action Success");
				return "template";
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public Implementation getImplementationId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = new Implementation();
		if (null != session.getAttribute("THIS"))
			implementation = (Implementation) session.getAttribute("THIS");
		return implementation;
	}

	// Getters & Setters
	public GoodsReturnBL getGoodsReturnBL() {
		return goodsReturnBL;
	}

	public void setGoodsReturnBL(GoodsReturnBL goodsReturnBL) {
		this.goodsReturnBL = goodsReturnBL;
	}

	public long getReturnId() {
		return returnId;
	}

	public void setReturnId(long returnId) {
		this.returnId = returnId;
	}

	public String getReturnNumber() {
		return returnNumber;
	}

	public void setReturnNumber(String returnNumber) {
		this.returnNumber = returnNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getReturnDetails() {
		return returnDetails;
	}

	public void setReturnDetails(String returnDetails) {
		this.returnDetails = returnDetails;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getReceiveId() {
		return receiveId;
	}

	public void setReceiveId(long receiveId) {
		this.receiveId = receiveId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
