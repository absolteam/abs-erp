/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.accounts.domain.entity.BankReconciliation;
import com.aiotech.aios.accounts.domain.entity.BankReconciliationDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.BankReconciliationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankReconciliationVO;
import com.aiotech.aios.accounts.service.bl.BankReconciliationBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Saleem
 */
public class BankReconciliationAction extends ActionSupport {

	private static final long serialVersionUID = 7311902844564011659L;

	private static final Logger LOG = Logger
			.getLogger(BankReconciliationAction.class);

	// Dependency
	private BankReconciliationBL bankReconciliationBL;

	// Variables
	private long reconciliationId;
	private long bankAccountId;
	private long flowId;
	private double statementBalance;
	private String reconciliationDate;
	private String statementEndDate;
	private String referenceNumber;
	private boolean reconciliationStatus;
	private double accountBalance;
	private String description;
	private String statementReference;
	private int index;
	private String transactionDate;
	private String presentedChequeDetails;
	private String returnMessage;
	private Implementation implementation;
	private BankReconciliationVO reconciliationVO;
	private Long recordId;

	public String returnSuccess() {
		try {
			LOG.info("Inside Method:returnSuccess()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("RECONCILIATION_SESSION_OBJ_"
					+ sessionObj.get("jSessionId"));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : returnSuccess() throws Exception " + ex);
		}
		return SUCCESS;
	}

	// Get all Bank Reconciliations
	public String showAllBankReconciliations() {
		try {
			LOG.info("Inside Method: showAllBankReconciliations()");

			JSONObject jsonObject = bankReconciliationBL
					.getAllBankReconciliations();
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonObject);

			LOG.info("Method: showAllBankReconciliations: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showAllBankReconciliations() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBankReconciliationPresentedChequesEntry() {
		try {
			LOG.info("Inside Method: showBankReconciliationPresentedChequesEntry()");
			referenceNumber = generateReferenceNumber();
			LOG.info("Method: showBankReconciliationPresentedChequesEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showBankReconciliationPresentedChequesEntry() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPresentedChequeReconciliationDetails() {
		try {
			LOG.info("Inside Method: showPresentedChequeReconciliationDetails()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankAccount bankAccount = bankReconciliationBL.getBankBL()
					.getBankService().getBankAccountDetails(bankAccountId);
			List<DirectPayment> directPayments = bankReconciliationBL
					.getDirectPaymentBL()
					.getDirectPaymentService()
					.getDirectPaymentByPresentedCheques(bankAccountId,
							DateFormat.convertStringToDate(reconciliationDate),
							DateFormat.convertStringToDate(statementEndDate));
			List<BankDeposit> bankDeposits = bankReconciliationBL
					.getBankDepositBL()
					.getBankDepositService()
					.getBankDepositByPresentedCheques(bankAccountId,
							DateFormat.convertStringToDate(reconciliationDate),
							DateFormat.convertStringToDate(statementEndDate));
			Set<Long> directPaymentIds = null;
			Set<Long> bankDepositIds = null;
			List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
			if (null != directPayments && directPayments.size() > 0) {
				directPaymentIds = new HashSet<Long>();
				for (DirectPayment directPayment : directPayments)
					directPaymentIds.add(directPayment.getDirectPaymentId());
				transactionDetails
						.addAll(bankReconciliationBL
								.getTransactionBL()
								.getTransactionService()
								.getTransactionDetailsByCombinationAndUseCase(
										bankAccount.getCombination()
												.getCombinationId(),
										directPaymentIds,
										DirectPayment.class.getSimpleName()));
			}
			if (null != bankDeposits && bankDeposits.size() > 0) {
				bankDepositIds = new HashSet<Long>();
				for (BankDeposit bankDeposit : bankDeposits) {
					for (BankDepositDetail bankDepositDetail : bankDeposit
							.getBankDepositDetails()) {
						bankDepositIds.add(bankDepositDetail
								.getBankDepositDetailId());
					}
				}
				transactionDetails
						.addAll(bankReconciliationBL
								.getTransactionBL()
								.getTransactionService()
								.getTransactionDetailsByUseCaseDetail(
										bankAccount.getCombination()
												.getCombinationId(),
										bankDepositIds,
										BankDepositDetail.class.getSimpleName()));
			}
			if (null != transactionDetails && transactionDetails.size() > 0) {
				BankReconciliationVO bankReconciliationVO = bankReconciliationBL
						.getBankReconciliationDetails(transactionDetails);
				if (null != bankReconciliationVO) {
					bankReconciliationVO.setStartDate(reconciliationDate);
					bankReconciliationVO.setEndDate(statementEndDate);
					bankReconciliationVO.setReferenceNumber(referenceNumber);
					bankReconciliationVO
							.setStatementReference(statementReference);
					double difference = 0;
					double accountBalance = AIOSCommons
							.formatAmountToDouble(bankReconciliationVO
									.getAccountBalanceStr());
					if (!bankReconciliationVO.isAccountSide()) {
						accountBalance *= -1;
						bankReconciliationVO.setAccountBalanceStr("-"
								+ bankReconciliationVO.getAccountBalanceStr());
					}
					difference = (statementBalance - (accountBalance));
					if (difference < 0)
						bankReconciliationVO
								.setDifferenceTotal("-"
										+ AIOSCommons.formatAmount(Math
												.abs(difference)));
					else
						bankReconciliationVO.setDifferenceTotal(AIOSCommons
								.formatAmount(difference));
					if (statementBalance < 0)
						bankReconciliationVO.setStatementBalanceStr("-"
								+ AIOSCommons.formatAmount(Math
										.abs(statementBalance)));
					else
						bankReconciliationVO.setStatementBalanceStr(AIOSCommons
								.formatAmount(statementBalance));
					session.setAttribute("RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"),
							bankReconciliationVO);
				}
				ServletActionContext.getRequest().setAttribute(
						"BANK_RECONCILIATION_STATEMENT", bankReconciliationVO);
			}
			LOG.info("Method: showPresentedChequeReconciliationDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showPresentedChequeReconciliationDetails() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Bank Reconciliation Entry
	public String showBankReconciliationEntry() {
		try {
			LOG.info("Inside Method: showBankReconciliationEntry()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			referenceNumber = null;
			BankReconciliationVO bankReconciliationVO = null;
			if (reconciliationId > 0) {
				BankReconciliation bankReconciliation = bankReconciliationBL
						.getBankReconciliationService().getBankReconciliation(
								reconciliationId);
				Set<Long> transactionDetailIds = new HashSet<Long>();
				for (BankReconciliationDetail detail : bankReconciliation
						.getBankReconciliationDetails())
					transactionDetailIds.add(detail.getRecordId());
				bankReconciliationVO = convertTOBankReconcilationVO(bankReconciliation);
				bankReconciliationVO.setBankReconciliationId(reconciliationId);
				List<TransactionDetail> transactionDetails = bankReconciliationBL
						.getTransactionBL()
						.getTransactionService()
						.getTransactionDetailsByCombinationAndDate(
								bankReconciliation.getBankAccount()
										.getCombination().getCombinationId(),
								bankReconciliation.getReconciliationDate(),
								bankReconciliation.getStatementEndDate(),
								transactionDetailIds);
				List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>(
						bankReconciliationVO.getBankReconciliationDetailVOs());
				if (null != transactionDetails && transactionDetails.size() > 0) {
					Collections.sort(bankReconciliationDetailVOs,
							new Comparator<BankReconciliationDetailVO>() {
								public int compare(
										BankReconciliationDetailVO o1,
										BankReconciliationDetailVO o2) {
									return o2.getFlowId().compareTo(
											o1.getFlowId());
								}
							});
					long count = bankReconciliationDetailVOs.get(0).getFlowId();
					count += 1;
					List<BankReconciliationDetailVO> bankReconciliationDetailVOtmps = bankReconciliationBL
							.convertBankReconciliationDetail(
									transactionDetails, count, false);
					bankReconciliationDetailVOs
							.addAll(bankReconciliationDetailVOtmps);
				}
				Collections.sort(bankReconciliationDetailVOs,
						new Comparator<BankReconciliationDetailVO>() {
							public int compare(BankReconciliationDetailVO o1,
									BankReconciliationDetailVO o2) {
								return o1.getFlowId().compareTo(o2.getFlowId());
							}
						});
				bankReconciliationVO
						.setBankReconciliationDetailVOs(bankReconciliationDetailVOs);
				session.setAttribute(
						"RECONCILIATION_SESSION_OBJ_"
								+ sessionObj.get("jSessionId"),
						bankReconciliationVO);
			} else
				referenceNumber = generateReferenceNumber();
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECONCILIATION", bankReconciliationVO);
			LOG.info("Method: showBankReconciliationEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showBankReconciliationEntry() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBankReconciliationPrintOut() {
		try {
			LOG.info("Inside Method: showBankReconciliationPrintOut()");
			BankReconciliation bankReconciliation = bankReconciliationBL
					.getBankReconciliationService().getBankReconciliation(
							reconciliationId);
			BankReconciliationVO bankReconciliationVO = convertTOBankReconcilationVO(bankReconciliation);
			List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>(
					bankReconciliationVO.getBankReconciliationDetailVOs());
			Collections.sort(bankReconciliationDetailVOs,
					new Comparator<BankReconciliationDetailVO>() {
						public int compare(BankReconciliationDetailVO o1,
								BankReconciliationDetailVO o2) {
							return o1.getBankReconciliationDetailId()
									.compareTo(
											o2.getBankReconciliationDetailId());
						}
					});
			bankReconciliationVO
					.setBankReconciliationDetailVOs(bankReconciliationDetailVOs);
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECONCILIATION", bankReconciliationVO);
			LOG.info("Method: showBankReconciliationPrintOut(): Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showBankReconciliationPrintOut() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private BankReconciliationVO convertTOBankReconcilationVO(
			BankReconciliation bankReconciliation) throws Exception {
		List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>();
		BankReconciliationDetailVO bankReconciliationDetailVO = null;
		BankReconciliationVO bankReconciliationVO = new BankReconciliationVO();
		bankReconciliationVO.setReferenceNumber(bankReconciliation
				.getReferenceNumber());
		bankReconciliationVO.setTransactionDateStr(DateFormat
				.convertDateToString(bankReconciliation.getTransactionDate()
						.toString()));
		if ((double) bankReconciliation.getAccountBalance() > 0) {
			bankReconciliationVO.setAccountBalanceStr(AIOSCommons
					.formatAmount(bankReconciliation.getAccountBalance()));
			bankReconciliationVO.setAccountSide(true);
		} else {
			bankReconciliationVO.setAccountBalanceStr("-"
					+ AIOSCommons.formatAmount(Math.abs(bankReconciliation
							.getAccountBalance())));
			bankReconciliationVO.setAccountSide(false);
		}
		if ((double) bankReconciliation.getStatementBalance() > 0)
			bankReconciliationVO.setStatementBalanceStr(AIOSCommons
					.formatAmount(bankReconciliation.getStatementBalance()));
		else
			bankReconciliationVO.setStatementBalanceStr("-"
					+ AIOSCommons.formatAmount(Math.abs(bankReconciliation
							.getStatementBalance())));
		bankReconciliationVO.setStartDate(DateFormat
				.convertDateToString(bankReconciliation.getReconciliationDate()
						.toString()));
		bankReconciliationVO.setEndDate(DateFormat
				.convertDateToString(bankReconciliation.getStatementEndDate()
						.toString()));
		bankReconciliationVO.setStatementReference(bankReconciliation
				.getStatementReference());
		bankReconciliationVO.setBankName(bankReconciliation.getBankAccount()
				.getBank().getBankName());
		bankReconciliationVO.setBankAccountNumber(bankReconciliation
				.getBankAccount().getAccountNumber());
		bankReconciliationVO.setBankAccountId(bankReconciliation
				.getBankAccount().getBankAccountId());
		bankReconciliationVO
				.setDescription(bankReconciliation.getDescription());
		double receiptTotal = 0;
		double paymentTotal = 0;
		DirectPayment directPayment = null;
		for (BankReconciliationDetail reconciliationDetailVO : bankReconciliation
				.getBankReconciliationDetails()) {
			bankReconciliationDetailVO = new BankReconciliationDetailVO();
			bankReconciliationDetailVO.setUseCase(reconciliationDetailVO
					.getUseCase());
			bankReconciliationDetailVO.setRecordId(reconciliationDetailVO
					.getRecordId());
			bankReconciliationDetailVO.setChequeEntry(false);
			bankReconciliationDetailVO
					.setUsecaseRecordId(reconciliationDetailVO
							.getUsecaseRecordId());
			bankReconciliationDetailVO.setUseCaseName(reconciliationDetailVO
					.getUseCase());
			if (bankReconciliationDetailVO.getUseCase().equalsIgnoreCase(
					DirectPayment.class.getSimpleName())) {
				TransactionDetail transactionDetail = bankReconciliationBL
						.getTransactionBL()
						.getTransactionService()
						.getTransationDetailById(
								bankReconciliationDetailVO.getRecordId());
				if (null != transactionDetail) {
					directPayment = bankReconciliationBL
							.getDirectPaymentBL()
							.getDirectPaymentService()
							.findDirectPaymentById(
									transactionDetail.getTransaction()
											.getRecordId());
					if (null != directPayment.getChequeDate()) {
						bankReconciliationDetailVO.setChequeEntry(true);
						bankReconciliationDetailVO
								.setUsecaseRecordId(directPayment
										.getDirectPaymentId());
						bankReconciliationDetailVO.setChequeDateStr(DateFormat
								.convertDateToString(directPayment
										.getChequeDate().toString()));
						bankReconciliationDetailVO.setChequeNumber(String
								.valueOf(directPayment.getChequeNumber()));
					}
				}
			}
			if (bankReconciliationDetailVO.getUseCase().equalsIgnoreCase(
					BankDeposit.class.getSimpleName())) {
				TransactionDetail transactionDetail = bankReconciliationBL
						.getTransactionBL()
						.getTransactionService()
						.getTransationDetailById(
								bankReconciliationDetailVO.getRecordId());
				if (null != transactionDetail) {
					BankDepositDetail bankDepositDetail = bankReconciliationBL
							.getBankDepositBL()
							.getBankDepositService()
							.getBankDepositDetailInfo(
									bankReconciliationDetailVO
											.getUsecaseRecordId());
					if (null != bankDepositDetail
							&& null != bankDepositDetail
									.getBankReceiptsDetail()
							&& null != bankDepositDetail
									.getBankReceiptsDetail().getChequeDate()) {
						bankReconciliationDetailVO.setChequeEntry(true);
						bankReconciliationDetailVO
								.setUsecaseRecordId(bankReconciliationDetailVO
										.getUsecaseRecordId());
						bankReconciliationDetailVO.setChequeDateStr(DateFormat
								.convertDateToString(bankDepositDetail
										.getBankReceiptsDetail()
										.getChequeDate().toString()));
						bankReconciliationDetailVO
								.setChequeNumber(String
										.valueOf(bankDepositDetail
												.getBankReceiptsDetail()
												.getBankRefNo()));
					}
					bankReconciliationDetailVO
							.setUseCaseName(BankDepositDetail.class
									.getSimpleName());
				}
			}
			bankReconciliationDetailVO
					.setBankReconciliationDetailId(reconciliationDetailVO
							.getBankReconciliationDetailId());
			bankReconciliationDetailVO.setFlowId(reconciliationDetailVO
					.getBankReconciliationDetailId());
			bankReconciliationDetailVO.setReceiptSide(false);
			bankReconciliationDetailVO.setReconsoleStatus(true);
			bankReconciliationDetailVO.setDescription(reconciliationDetailVO
					.getDescription());
			bankReconciliationDetailVO
					.setReferenceNumber(reconciliationDetailVO
							.getReferenceNumber());
			bankReconciliationDetailVO.setTransactionDateStr(DateFormat
					.convertDateToString(reconciliationDetailVO
							.getTransactionDate().toString()));
			if (null != reconciliationDetailVO.getReceipt()) {
				bankReconciliationDetailVO.setReceiptAmount(AIOSCommons
						.formatAmount(reconciliationDetailVO.getReceipt()));
				receiptTotal += reconciliationDetailVO.getReceipt();
				bankReconciliationDetailVO.setReceiptSide(true);
			} else {
				bankReconciliationDetailVO.setPaymentAmount(AIOSCommons
						.formatAmount(reconciliationDetailVO.getPayment()));
				paymentTotal += reconciliationDetailVO.getPayment();
			}
			bankReconciliationDetailVOs.add(bankReconciliationDetailVO);
		}
		bankReconciliationVO.setPaymentTotal(AIOSCommons
				.formatAmount(paymentTotal));
		bankReconciliationVO.setReceiptTotal(AIOSCommons
				.formatAmount(receiptTotal));
		bankReconciliationVO
				.setBankReconciliationDetailVOs(bankReconciliationDetailVOs);
		return bankReconciliationVO;
	}

	// Find Bank Reconciliation Previous period against a bank
	public String findReconciliationPeriodByBank() {
		try {
			LOG.info("Inside Method: findReconciliationPeriodByBank()");
			if (reconciliationId > 0) {

				LocalDate localDate = new LocalDate(new Date());
				localDate = localDate.plusDays(1);
				LocalDate currentDateDate = new LocalDate();
				currentDateDate = new LocalDate(new Date());
				List<BankReconciliation> listBankRecons = bankReconciliationBL
						.getBankReconciliationService()
						.getAllBankReconciliations(getImplementationId());

				if (listBankRecons != null && listBankRecons.size() > 0) {
					// Sort the calculation order by reconciliationId descending
					Collections.sort(listBankRecons,
							new Comparator<BankReconciliation>() {
								public int compare(BankReconciliation m1,
										BankReconciliation m2) {
									return m2
											.getBankReconciliationId()
											.compareTo(
													m1.getBankReconciliationId());
								}
							});
					localDate = new LocalDate(listBankRecons.get(0)
							.getStatementEndDate());
					localDate = localDate.plusDays(1);

				}

				reconciliationDate = localDate.toString("dd-MMM-yyyy");
				statementEndDate = currentDateDate.toString("dd-MMM-yyyy");

			}

			LOG.info("Method: findReconciliationPeriodByBank: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : findReconciliationPeriodByBank() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Bank Reconciliation Entry
	public String showBankReconciliationView() {
		try {
			LOG.info("Inside Method: showBankReconciliationView()");
			referenceNumber = null;
			if (recordId != null && recordId > 0) {
				reconciliationId = recordId;
				CommentVO comment = new CommentVO();
				if (reconciliationId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(BankReconciliation.class.getName());
					comment = bankReconciliationBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			BankReconciliation bankReconciliation = null;
			if (reconciliationId > 0) {
				bankReconciliation = bankReconciliationBL
						.getBankReconciliationService().getBankReconciliation(
								reconciliationId);
			}
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECONCILIATION", bankReconciliation);
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECONCILIATION_STATEMENT", bankReconciliation);
			LOG.info("Method: showBankReconciliationView: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showBankReconciliationView() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Bank Reconciliation Details
	public String showBankReconciliationDetails() {
		try {
			LOG.info("Inside Method: showBankReconciliationDetails()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<BankReconciliation> bankReconciliations = bankReconciliationBL
					.getBankReconciliationService()
					.getBankReconciliationByBankAccount(bankAccountId);
			boolean validate = null != bankReconciliations
					&& bankReconciliations.size() > 0 ? bankReconciliationBL
					.validateReconciliationDates(bankReconciliations,
							DateFormat.convertStringToDate(reconciliationDate),
							DateFormat.convertStringToDate(statementEndDate))
					: true;
			if (!validate) {
				ServletActionContext
						.getRequest()
						.setAttribute("RETURN_MESSAGE",
								"Bank reconciliation has alreay entered for given statement dates.");
				LOG.info("Method: showBankReconciliationDetails: Action error :"
						+ "Bank reconciliation has alreay entered for given statement dates.");
				return SUCCESS;
			}
			BankAccount bankAccount = bankReconciliationBL.getBankBL()
					.getBankService().getBankAccountDetails(bankAccountId);
			List<TransactionDetail> transactionDetails = bankReconciliationBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailsByCombinationAndDate(
							bankAccount.getCombination().getCombinationId(),
							DateFormat.convertStringToDate(reconciliationDate),
							DateFormat.convertStringToDate(statementEndDate));
			BankReconciliationVO bankReconciliationVO = bankReconciliationBL
					.getBankReconciliationDetails(transactionDetails);

			if (null != bankReconciliationVO) {
				bankReconciliationVO.setStartDate(reconciliationDate);
				bankReconciliationVO.setEndDate(statementEndDate);
				bankReconciliationVO.setReferenceNumber(referenceNumber);
				bankReconciliationVO.setStatementReference(statementReference);
				double difference = 0;
				double accountBalance = AIOSCommons
						.formatAmountToDouble(bankReconciliationVO
								.getAccountBalanceStr());
				if (!bankReconciliationVO.isAccountSide()) {
					accountBalance *= -1;
					bankReconciliationVO.setAccountBalanceStr("-"
							+ bankReconciliationVO.getAccountBalanceStr());
				}
				difference = (statementBalance - (accountBalance));
				if (difference < 0)
					bankReconciliationVO.setDifferenceTotal("-"
							+ AIOSCommons.formatAmount(Math.abs(difference)));
				else
					bankReconciliationVO.setDifferenceTotal(AIOSCommons
							.formatAmount(difference));
				if (statementBalance < 0)
					bankReconciliationVO.setStatementBalanceStr("-"
							+ AIOSCommons.formatAmount(Math
									.abs(statementBalance)));
				else
					bankReconciliationVO.setStatementBalanceStr(AIOSCommons
							.formatAmount(statementBalance));
				session.setAttribute(
						"RECONCILIATION_SESSION_OBJ_"
								+ sessionObj.get("jSessionId"),
						bankReconciliationVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECONCILIATION_STATEMENT", bankReconciliationVO);

			LOG.info("Method: showBankReconciliationDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showBankReconciliationDetails() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Bank Reconciliation
	public String updateBankReconciliationDetails() {
		try {
			LOG.info("Inside Method: updateBankReconciliationDetails()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankReconciliationVO bankReconciliationVO = (BankReconciliationVO) session
					.getAttribute("RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"));
			bankReconciliationVO.setStatementBalance(statementBalance);
			bankReconciliationVO.setStatementBalanceStr(String
					.valueOf(statementBalance));
			double accountBalance = 0;
			double statementBalance = 0;
			double difference = 0;
			for (BankReconciliationDetailVO detailVO : bankReconciliationVO
					.getBankReconciliationDetailVOs()) {
				if ((long) detailVO.getFlowId() == flowId) {
					accountBalance = AIOSCommons
							.formatAmountToDouble(bankReconciliationVO
									.getAccountBalanceStr());
					if (reconciliationStatus) {
						if (detailVO.getReceiptSide())
							accountBalance += AIOSCommons
									.formatAmountToDouble(detailVO
											.getReceiptAmount());
						else
							accountBalance -= AIOSCommons
									.formatAmountToDouble(detailVO
											.getPaymentAmount());
					} else {
						if (detailVO.getReceiptSide())
							accountBalance -= AIOSCommons
									.formatAmountToDouble(detailVO
											.getReceiptAmount());
						else
							accountBalance += AIOSCommons
									.formatAmountToDouble(detailVO
											.getPaymentAmount());
					}
					statementBalance = AIOSCommons
							.formatAmountToDouble(bankReconciliationVO
									.getStatementBalanceStr());
					difference = (statementBalance - (accountBalance));
					if (difference < 0)
						bankReconciliationVO
								.setDifferenceTotal("-"
										+ AIOSCommons.formatAmount(Math
												.abs(difference)));
					else
						bankReconciliationVO.setDifferenceTotal(AIOSCommons
								.formatAmount(difference));
					if (statementBalance < 0)
						bankReconciliationVO.setStatementBalanceStr("-"
								+ AIOSCommons.formatAmount(Math
										.abs(statementBalance)));
					else
						bankReconciliationVO.setStatementBalanceStr(AIOSCommons
								.formatAmount(statementBalance));
					if (accountBalance >= 0) {
						bankReconciliationVO.setAccountSide(true);
						bankReconciliationVO.setAccountBalanceStr(AIOSCommons
								.formatAmount(accountBalance));
					} else {
						bankReconciliationVO.setAccountSide(false);
						bankReconciliationVO.setAccountBalanceStr("-"
								+ AIOSCommons.formatAmount(Math
										.abs(accountBalance)));
					}
					detailVO.setReconsoleStatus(reconciliationStatus);
					break;
				}
			}
			double receiptTotal = 0;
			double paymentTotal = 0;
			for (BankReconciliationDetailVO bankReconciliationDetailVO : bankReconciliationVO
					.getBankReconciliationDetailVOs()) {
				if (bankReconciliationDetailVO.isReconsoleStatus()) {
					if (null != bankReconciliationDetailVO.getReceiptAmount())
						receiptTotal += AIOSCommons
								.formatAmountToDouble(bankReconciliationDetailVO
										.getReceiptAmount());
					else
						paymentTotal += AIOSCommons
								.formatAmountToDouble(bankReconciliationDetailVO
										.getPaymentAmount());
				}
			}
			bankReconciliationVO.setReceiptTotal(AIOSCommons
					.formatAmount(receiptTotal));
			bankReconciliationVO.setPaymentTotal(AIOSCommons
					.formatAmount(paymentTotal));
			session.setAttribute(
					"RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"),
					bankReconciliationVO);
			reconciliationVO = new BankReconciliationVO();
			BeanUtils.copyProperties(reconciliationVO, bankReconciliationVO);
			LOG.info("Method: updateBankReconciliationDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : updateBankReconciliationDetails() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Bank Reconciliation
	public String updateBankReconciliationStatementBalance() {
		try {
			LOG.info("Inside Method: updateBankReconciliationStatementBalance()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankReconciliationVO bankReconciliationVO = (BankReconciliationVO) session
					.getAttribute("RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"));
			bankReconciliationVO.setStatementBalance(statementBalance);
			bankReconciliationVO.setStatementBalanceStr(String
					.valueOf(statementBalance));
			double accountBalance = 0;
			double statementBalance = 0;
			double difference = 0;
			accountBalance = AIOSCommons
					.formatAmountToDouble(bankReconciliationVO
							.getAccountBalanceStr());
			statementBalance = AIOSCommons
					.formatAmountToDouble(bankReconciliationVO
							.getStatementBalanceStr());
			difference = (statementBalance - (accountBalance));
			if (difference < 0)
				bankReconciliationVO.setDifferenceTotal("-"
						+ AIOSCommons.formatAmount(Math.abs(difference)));
			else
				bankReconciliationVO.setDifferenceTotal(AIOSCommons
						.formatAmount(difference));
			if (statementBalance < 0)
				bankReconciliationVO.setStatementBalanceStr("-"
						+ AIOSCommons.formatAmount(Math.abs(statementBalance)));
			else
				bankReconciliationVO.setStatementBalanceStr(AIOSCommons
						.formatAmount(statementBalance));
			if (accountBalance >= 0) {
				bankReconciliationVO.setAccountSide(true);
				bankReconciliationVO.setAccountBalanceStr(AIOSCommons
						.formatAmount(accountBalance));
			} else {
				bankReconciliationVO.setAccountSide(false);
				bankReconciliationVO.setAccountBalanceStr("-"
						+ AIOSCommons.formatAmount(Math.abs(accountBalance)));
			}
			double receiptTotal = 0;
			double paymentTotal = 0;
			for (BankReconciliationDetailVO bankReconciliationDetailVO : bankReconciliationVO
					.getBankReconciliationDetailVOs()) {
				if (bankReconciliationDetailVO.isReconsoleStatus()) {
					if (null != bankReconciliationDetailVO.getReceiptAmount())
						receiptTotal += AIOSCommons
								.formatAmountToDouble(bankReconciliationDetailVO
										.getReceiptAmount());
					else
						paymentTotal += AIOSCommons
								.formatAmountToDouble(bankReconciliationDetailVO
										.getPaymentAmount());
				}
			}
			bankReconciliationVO.setReceiptTotal(AIOSCommons
					.formatAmount(receiptTotal));
			bankReconciliationVO.setPaymentTotal(AIOSCommons
					.formatAmount(paymentTotal));
			session.setAttribute(
					"RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"),
					bankReconciliationVO);
			reconciliationVO = new BankReconciliationVO();
			BeanUtils.copyProperties(reconciliationVO, bankReconciliationVO);
			LOG.info("Method: updateBankReconciliationStatementBalance(): Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : updateBankReconciliationStatementBalance() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBankReconciliationDetailsPrint() {
		try {
			LOG.info("Inside Method: showBankReconciliationDetailsPrint()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankReconciliationVO bankReconciliationVO = (BankReconciliationVO) session
					.getAttribute("RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"));
			BankReconciliationVO bankReconciliationVOdp = new BankReconciliationVO();
			BeanUtils.copyProperties(bankReconciliationVOdp,
					bankReconciliationVO);
			List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>();
			BankReconciliationDetailVO reconciliationDetailVO = null;
			double paymentTotal = 0;
			double receiptTotal = 0;
			for (BankReconciliationDetailVO bankReconciliationDetailVO : bankReconciliationVO
					.getBankReconciliationDetailVOs()) {
				reconciliationDetailVO = new BankReconciliationDetailVO();
				BeanUtils.copyProperties(reconciliationDetailVO,
						bankReconciliationDetailVO);
				if (bankReconciliationDetailVO.getReceiptSide())
					receiptTotal += AIOSCommons
							.formatAmountToDouble(bankReconciliationDetailVO
									.getReceiptAmount());
				else
					paymentTotal += AIOSCommons
							.formatAmountToDouble(bankReconciliationDetailVO
									.getPaymentAmount());
				bankReconciliationDetailVOs.add(reconciliationDetailVO);

			}
			bankReconciliationVOdp.setPaymentTotal(AIOSCommons
					.formatAmount(paymentTotal));
			bankReconciliationVOdp.setReceiptTotal(AIOSCommons
					.formatAmount(receiptTotal));
			bankReconciliationVOdp
					.setBankReconciliationDetailVOs(bankReconciliationDetailVOs);

			ServletActionContext.getRequest().setAttribute(
					"BANK_RECONCILIATION", bankReconciliationVOdp);
			LOG.info("Method: showBankReconciliationDetailsPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showBankReconciliationDetailsPrint() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String savePresentedChequeBankReconciliation() {
		try {

			LOG.info("Inside Method: savePresentedChequeBankReconciliation()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankReconciliation bankReconciliation = new BankReconciliation();
			BankReconciliationVO bankReconciliationVO = (BankReconciliationVO) session
					.getAttribute("RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"));
			bankReconciliation.setAccountBalance(AIOSCommons
					.formatAmountToDouble(bankReconciliationVO
							.getAccountBalanceStr()));
			bankReconciliation.setStatementBalance(AIOSCommons
					.formatAmountToDouble(bankReconciliationVO
							.getStatementBalanceStr()));
			bankReconciliation.setDescription(description);
			bankReconciliation.setStatementReference(statementReference);
			bankReconciliation.setReconciliationDate(DateFormat
					.convertStringToDate(bankReconciliationVO.getStartDate()));
			bankReconciliation.setStatementEndDate(DateFormat
					.convertStringToDate(bankReconciliationVO.getEndDate()));
			bankReconciliation.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			BankAccount bankAccount = bankReconciliationBL.getBankBL()
					.getBankService().getBankAccountDetails(bankAccountId);
			bankReconciliation.setBankAccount(bankAccount);
			bankReconciliation.setReferenceNumber(SystemBL.getReferenceStamp(
					BankReconciliation.class.getName(),
					bankReconciliationBL.getImplementation()));
			List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>(
					bankReconciliationVO.getBankReconciliationDetailVOs());
			List<BankReconciliationDetail> bankReconciliationDetails = new ArrayList<BankReconciliationDetail>();
			BankReconciliationDetail bankReconciliationDetail = null;
			for (BankReconciliationDetailVO detailVO : bankReconciliationDetailVOs) {
				if (detailVO.isReconsoleStatus()) {
					bankReconciliationDetail = new BankReconciliationDetail();
					bankReconciliationDetail.setTransactionDate(DateFormat
							.convertStringToDate(detailVO
									.getTransactionDateStr()));
					bankReconciliationDetail.setReferenceNumber(detailVO
							.getReferenceNumber());
					bankReconciliationDetail.setUseCase(detailVO.getUseCase());
					bankReconciliationDetail
							.setRecordId(detailVO.getRecordId());
					bankReconciliationDetail.setUsecaseRecordId(detailVO
							.getUsecaseRecordId());
					if (detailVO.getReceiptSide())
						bankReconciliationDetail.setReceipt(AIOSCommons
								.formatAmountToDouble(detailVO
										.getReceiptAmount()));
					else
						bankReconciliationDetail.setPayment(AIOSCommons
								.formatAmountToDouble(detailVO
										.getPaymentAmount()));
					bankReconciliationDetail.setDescription(detailVO
							.getDescription());
					bankReconciliationDetails.add(bankReconciliationDetail);
				}
			}
			List<BankReconciliationDetailVO> reconciliationDetailVOs = null;
			if (null != presentedChequeDetails
					&& !("").equals(presentedChequeDetails)) {
				JSONParser parser = new JSONParser();
				Object receiveObject = parser.parse(presentedChequeDetails);
				JSONArray object = (JSONArray) receiveObject;
				BankReconciliationDetailVO reconciliationDetailVO = null;
				org.json.simple.JSONObject jsonObject = null;
				reconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>();
				for (Iterator<?> iterator = object.iterator(); iterator
						.hasNext();) {
					reconciliationDetailVO = new BankReconciliationDetailVO();
					jsonObject = (org.json.simple.JSONObject) iterator.next();
					reconciliationDetailVO.setUsecaseRecordId(Long
							.parseLong(jsonObject.get("useCaseId").toString()));
					reconciliationDetailVO.setUseCase(jsonObject.get("useCase")
							.toString());
					reconciliationDetailVOs.add(reconciliationDetailVO);
				}
			}
			bankReconciliationBL.saveBankReconciliation(bankReconciliation,
					bankReconciliationDetails, reconciliationDetailVOs);
			returnMessage = "SUCCESS";
			LOG.info("Method: savePresentedChequeBankReconciliation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Record save failure.";
			LOG.error("Method : savePresentedChequeBankReconciliation() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Bank Reconciliation
	public String saveBankReconciliation() {
		try {

			LOG.info("Inside Method: saveBankReconciliation()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankReconciliation bankReconciliation = new BankReconciliation();

			BankReconciliationVO bankReconciliationVO = (BankReconciliationVO) session
					.getAttribute("RECONCILIATION_SESSION_OBJ_"
							+ sessionObj.get("jSessionId"));

			bankReconciliation.setAccountBalance(AIOSCommons
					.formatAmountToDouble(bankReconciliationVO
							.getAccountBalanceStr()));
			bankReconciliation.setStatementBalance(AIOSCommons
					.formatAmountToDouble(bankReconciliationVO
							.getStatementBalanceStr()));
			bankReconciliation.setDescription(description);
			bankReconciliation.setStatementReference(statementReference);
			bankReconciliation.setReconciliationDate(DateFormat
					.convertStringToDate(bankReconciliationVO.getStartDate()));
			bankReconciliation.setStatementEndDate(DateFormat
					.convertStringToDate(bankReconciliationVO.getEndDate()));
			bankReconciliation.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			BankAccount bankAccount = bankReconciliationBL.getBankBL()
					.getBankService().getBankAccountDetails(bankAccountId);
			bankReconciliation.setBankAccount(bankAccount);
			List<BankReconciliationDetail> bankReconciliationDetailsDel = null;
			if (reconciliationId > 0) {
				BankReconciliation bankReconciliationtmp = bankReconciliationBL
						.getBankReconciliationService().getBankReconciliation(
								reconciliationId);
				bankReconciliation.setReferenceNumber(bankReconciliationtmp
						.getReferenceNumber());
				bankReconciliation.setBankReconciliationId(reconciliationId);
				bankReconciliationDetailsDel = new ArrayList<BankReconciliationDetail>(
						bankReconciliationtmp.getBankReconciliationDetails());
			} else
				bankReconciliation.setReferenceNumber(SystemBL
						.getReferenceStamp(BankReconciliation.class.getName(),
								bankReconciliationBL.getImplementation()));
			List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>(
					bankReconciliationVO.getBankReconciliationDetailVOs());
			List<BankReconciliationDetail> bankReconciliationDetails = new ArrayList<BankReconciliationDetail>();
			BankReconciliationDetail bankReconciliationDetail = null;
			for (BankReconciliationDetailVO detailVO : bankReconciliationDetailVOs) {
				if (detailVO.isReconsoleStatus()) {
					bankReconciliationDetail = new BankReconciliationDetail();
					bankReconciliationDetail.setTransactionDate(DateFormat
							.convertStringToDate(detailVO
									.getTransactionDateStr()));
					bankReconciliationDetail.setReferenceNumber(detailVO
							.getReferenceNumber());
					bankReconciliationDetail.setUseCase(detailVO.getUseCase());
					bankReconciliationDetail
							.setRecordId(detailVO.getRecordId());
					bankReconciliationDetail.setUsecaseRecordId(detailVO
							.getUsecaseRecordId());
					if (detailVO.getReceiptSide())
						bankReconciliationDetail.setReceipt(AIOSCommons
								.formatAmountToDouble(detailVO
										.getReceiptAmount()));
					else
						bankReconciliationDetail.setPayment(AIOSCommons
								.formatAmountToDouble(detailVO
										.getPaymentAmount()));
					bankReconciliationDetail.setDescription(detailVO
							.getDescription());
					bankReconciliationDetails.add(bankReconciliationDetail);
				}
			}
			List<BankReconciliationDetailVO> reconciliationDetailVOs = null;
			if (null != presentedChequeDetails
					&& !("").equals(presentedChequeDetails)) {
				JSONParser parser = new JSONParser();
				Object receiveObject = parser.parse(presentedChequeDetails);
				JSONArray object = (JSONArray) receiveObject;
				BankReconciliationDetailVO reconciliationDetailVO = null;
				org.json.simple.JSONObject jsonObject = null;
				reconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>();
				for (Iterator<?> iterator = object.iterator(); iterator
						.hasNext();) {
					reconciliationDetailVO = new BankReconciliationDetailVO();
					jsonObject = (org.json.simple.JSONObject) iterator.next();
					reconciliationDetailVO.setUsecaseRecordId(Long
							.parseLong(jsonObject.get("useCaseId").toString()));
					reconciliationDetailVO.setUseCase(jsonObject.get("useCase")
							.toString());
					reconciliationDetailVOs.add(reconciliationDetailVO);
				}
			}
			bankReconciliationBL.saveBankReconciliation(bankReconciliation,
					bankReconciliationDetails, bankReconciliationDetailsDel,
					reconciliationDetailVOs);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Method: saveBankReconciliation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.error("Method : saveBankReconciliation() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete bank reconciliation
	public String deleteBankReconciliation() {
		try {
			BankReconciliation bankReconciliation = bankReconciliationBL
					.getBankReconciliationService().getBankReconciliation(
							reconciliationId);
			bankReconciliationBL.deleteBankReconciliation(bankReconciliation);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Method: saveBankReconciliation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.error("Method : deleteBankReconciliation() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Generate reference number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(BankReconciliation.class.getName(),
				bankReconciliationBL.getImplementation());
	}

	// Getters and Setters
	public BankReconciliationBL getBankReconciliationBL() {
		return bankReconciliationBL;
	}

	public void setBankReconciliationBL(
			BankReconciliationBL bankReconciliationBL) {
		this.bankReconciliationBL = bankReconciliationBL;
	}

	public long getReconciliationId() {
		return reconciliationId;
	}

	public void setReconciliationId(long reconciliationId) {
		this.reconciliationId = reconciliationId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public String getReconciliationDate() {
		return reconciliationDate;
	}

	public void setReconciliationDate(String reconciliationDate) {
		this.reconciliationDate = reconciliationDate;
	}

	public String getStatementEndDate() {
		return statementEndDate;
	}

	public void setStatementEndDate(String statementEndDate) {
		this.statementEndDate = statementEndDate;
	}

	public double getStatementBalance() {
		return statementBalance;
	}

	public void setStatementBalance(double statementBalance) {
		this.statementBalance = statementBalance;
	}

	public boolean isReconciliationStatus() {
		return reconciliationStatus;
	}

	public void setReconciliationStatus(boolean reconciliationStatus) {
		this.reconciliationStatus = reconciliationStatus;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatementReference() {
		return statementReference;
	}

	public void setStatementReference(String statementReference) {
		this.statementReference = statementReference;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getFlowId() {
		return flowId;
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}

	public BankReconciliationVO getReconciliationVO() {
		return reconciliationVO;
	}

	public void setReconciliationVO(BankReconciliationVO reconciliationVO) {
		this.reconciliationVO = reconciliationVO;
	}

	public String getPresentedChequeDetails() {
		return presentedChequeDetails;
	}

	public void setPresentedChequeDetails(String presentedChequeDetails) {
		this.presentedChequeDetails = presentedChequeDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
