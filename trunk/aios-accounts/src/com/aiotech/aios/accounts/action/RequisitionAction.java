package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.RequisitionBL;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class RequisitionAction extends ActionSupport {

	private static final long serialVersionUID = 3884533852701098333L;

	private static final Logger log = Logger.getLogger(RequisitionAction.class);

	private long requisitionId;
	private long personId;
	private long locationId;
	private int rowId;
	private String personName;
	private String referenceNumber;
	private String requisitionDate;
	private String description;
	private String requisitionDetails;
	private String returnMessage;
	private List<Object> aaData;
	private RequisitionBL requisitionBL;
	private Long recordId;
	private boolean selfFlag;

	public String returnSuccess() {
		return SUCCESS;
	}

	public String showAllRequisitions() {
		try {
			log.info("Inside Module: Accounts : Method: showAllRequisitions");
			aaData = requisitionBL.getAllRequisitions();
			log.info("Module: Accounts : Method: showAllRequisitions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllRequisitions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAllRequisitionsSelf() {
		try {
			log.info("Inside Module: Accounts : Method: showAllRequisitions");
			aaData = requisitionBL.getAllRequisitionsSelf();
			log.info("Module: Accounts : Method: showAllRequisitions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllRequisitions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAllIssueRequisitions() {
		try {
			log.info("Inside Module: Accounts : Method: showAllIssueRequisitions");
			aaData = requisitionBL.showAllIssueRequisitions();
			log.info("Module: Accounts : Method: showAllIssueRequisitions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllIssueRequisitions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAllQuotationRequisitions() {
		try {
			log.info("Inside Module: Accounts : Method: showAllQuotationRequisitions");
			aaData = requisitionBL.showAllQuotationRequisitions();
			log.info("Module: Accounts : Method: showAllQuotationRequisitions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllQuotationRequisitions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAllPurchaseRequisitions() {
		try {
			log.info("Inside Module: Accounts : Method: showAllPurchaseRequisitions");
			aaData = requisitionBL.showAllPurchaseRequisitions();
			log.info("Module: Accounts : Method: showAllPurchaseRequisitions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllPurchaseRequisitions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showRequisitionEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showRequisitionEntry");
			RequisitionVO requisitionVO = null;
			if (recordId != null && recordId > 0) {
				requisitionId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (requisitionId > 0) {
					comment.setRecordId(requisitionId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = requisitionBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (requisitionId > 0) {
				Requisition requisition = requisitionBL.getRequisitionService()
						.getRequisitionById(requisitionId);
				List<RequisitionDetail> requisitionDetails = new ArrayList<RequisitionDetail>(
						requisition.getRequisitionDetails());
				requisitionVO = new RequisitionVO();
				BeanUtils.copyProperties(requisitionVO, requisition);
				requisitionVO.setDate(DateFormat
						.convertDateToString(requisition.getRequisitionDate()
								.toString()));
				requisitionVO.setPersonName(requisition.getPerson()
						.getFirstName()
						+ " "
						+ requisition.getPerson().getLastName());
				requisitionVO.setLocationName(requisition.getCmpDeptLoc()
						.getCompany().getCompanyName()
						+ ">>"
						+ requisition.getCmpDeptLoc().getDepartment()
								.getDepartmentName()
						+ ">>"
						+ requisition.getCmpDeptLoc().getLocation()
								.getLocationName());
				Collections.sort(requisitionDetails,
						new Comparator<RequisitionDetail>() {
							public int compare(RequisitionDetail o1,
									RequisitionDetail o2) {
								return o1.getRequisitionDetailId().compareTo(
										o2.getRequisitionDetailId());
							}
						});
				List<RequisitionDetailVO> requisitionDetailVOs = new ArrayList<RequisitionDetailVO>();
				RequisitionDetailVO requisitionDetailVO = null;
				for (RequisitionDetail requisitionDetail : requisitionDetails) {
					requisitionDetailVO = new RequisitionDetailVO();
					BeanUtils.copyProperties(requisitionDetailVO,
							requisitionDetail);
					requisitionDetailVOs.add(requisitionDetailVO);
				}
				requisitionVO.setRequisitionDetailVOs(requisitionDetailVOs);
			} else {
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				User user = (User) sessionObj.get("USER");
				referenceNumber = SystemBL.getReferenceStamp(
						Requisition.class.getName(),
						requisitionBL.getImplementation());
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute("REQUISITION",
					requisitionVO);
			log.info("Module: Accounts : Method: showRequisitionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showRequisitionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showRequisitionPrint() {
		try {
			log.info("Inside Module: Accounts : Method: showRequisitionPrint");
			List<RequisitionDetail> requisitionDetails = requisitionBL
					.getRequisitionService()
					.getRequisitionDetailByRequistionId(requisitionId);
			List<RequisitionDetailVO> requisitionDetailVOs = new ArrayList<RequisitionDetailVO>();
			RequisitionDetailVO requisitionDetailVO = null;
			for (RequisitionDetail requisitionDetail : requisitionDetails) {
				requisitionDetailVO = new RequisitionDetailVO();
				BeanUtils
						.copyProperties(requisitionDetailVO, requisitionDetail);
				List<PurchaseDetail> purchases = new ArrayList<PurchaseDetail>(
						requisitionDetail.getProduct().getPurchaseDetails());
				Double unitRate = null;
				if (purchases != null && purchases.size() > 0) {
					Collections.sort(purchases,
							new Comparator<PurchaseDetail>() {
								public int compare(PurchaseDetail m1,
										PurchaseDetail m2) {
									return m2.getPurchaseDetailId().compareTo(
											m1.getPurchaseDetailId());
								}
							});
					for (PurchaseDetail purchaseDetail : purchases) {
						if (purchaseDetail.getProduct().getProductId() == requisitionDetail
								.getProduct().getProductId()) {
							unitRate = purchaseDetail.getUnitRate();
							break;
						}
					}

				}
				requisitionDetailVO.setUnitRate(unitRate);
				requisitionDetailVOs.add(requisitionDetailVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_DETAIL", requisitionDetailVOs);
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION",
					requisitionBL.getRequisitionService().getRequisitionById(
							requisitionId));
			log.info("Module: Accounts : Method: showRequisitionPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showRequisitionPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showRequisitionDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showRequisitionDetails");
			List<RequisitionDetail> requisitionDetails = requisitionBL
					.getRequisitionService().getRequisitionDetailIssuance(
							requisitionId);
			List<RequisitionDetailVO> requisitionDetailVOs = new ArrayList<RequisitionDetailVO>();
			RequisitionDetailVO requisitionDetailVO = null;
			double processedQuantity = 0;
			double requisitionQuantity = 0;
			for (RequisitionDetail requisitionDetail : requisitionDetails) {
				requisitionQuantity = requisitionDetail.getQuantity();
				requisitionDetailVO = new RequisitionDetailVO();
				processedQuantity = 0;
				if (null != requisitionDetail.getIssueRequistionDetails()
						&& requisitionDetail.getIssueRequistionDetails().size() > 0) {
					for (IssueRequistionDetail issueRequistionDetail : requisitionDetail
							.getIssueRequistionDetails()) {
						processedQuantity += issueRequistionDetail
								.getQuantity();
					}
				}
				BeanUtils
						.copyProperties(requisitionDetailVO, requisitionDetail);

				requisitionDetailVO.setQuantity(requisitionQuantity
						- processedQuantity);
				requisitionDetailVO
						.setUnitRate(requisitionBL
								.getPurchaseBL()
								.getStockBL()
								.getProductCostingPrice(
										requisitionDetail.getProduct()));
				requisitionDetailVO.setTotalAmount(requisitionDetailVO
						.getQuantity() * requisitionDetailVO.getUnitRate());
				List<StockVO> stocks = requisitionBL
						.getPurchaseBL()
						.getStockBL()
						.getProductStockFullDetails(
								requisitionDetail.getProduct().getProductId());

				if (null != stocks && stocks.size() > 0) {
					double availableStock = 0;
					Map<Integer, StockVO> shelfs = new HashMap<Integer, StockVO>();
					for (StockVO stockVO : stocks) {
						availableStock += stockVO.getAvailableQuantity();
						shelfs.put(stockVO.getShelf().getShelfId(), stockVO);
					}
					if (null != shelfs && shelfs.size() == 1) {
						List<StockVO> tempStockVO = new ArrayList<StockVO>(
								shelfs.values());
						Shelf shelf = requisitionBL
								.getPurchaseBL()
								.getStockBL()
								.getStoreBL()
								.getStoreService()
								.getStoreByShelfId(
										tempStockVO.get(0).getShelf()
												.getShelfId());
						requisitionDetailVO.setStoreName(shelf.getShelf()
								.getAisle().getStore().getStoreName()
								+ ">>"
								+ shelf.getShelf().getAisle().getSectionName()
								+ ">>"
								+ shelf.getShelf().getName()
								+ ">>"
								+ shelf.getName());
						requisitionDetailVO.setStoreId(tempStockVO.get(0)
								.getStore().getStoreId());
						requisitionDetailVO.setShelfId(tempStockVO.get(0)
								.getShelf().getShelfId());
						requisitionDetailVO.setBatchNumber(tempStockVO.get(0)
								.getBatchNumber());
						requisitionDetailVO.setExpiryDate(null != tempStockVO
								.get(0).getProductExpiry() ? DateFormat
								.convertDateToString(tempStockVO.get(0)
										.getProductExpiry().toString()) : null);
					}
					requisitionDetailVO.setAvailableQty(availableStock);
				}

				requisitionDetailVO.setProductPackageVOs(requisitionBL
						.getPurchaseBL()
						.getPackageConversionBL()
						.getProductPackagingDetail(
								requisitionDetail.getProduct().getProductId()));
				requisitionDetailVO.setPackageUnit(requisitionDetail
						.getQuantity());
				requisitionDetailVO.setPackageDetailId(-1l);
				if ((double) requisitionDetailVO.getQuantity() > 0)
					requisitionDetailVOs.add(requisitionDetailVO);
			}

			ServletActionContext.getRequest().setAttribute(
					"SECTIONS",
					requisitionBL.getLookupMasterBL().getActiveLookupDetails(
							"SECTIONS", true));
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_DETAIL", requisitionDetailVOs);
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION",
					requisitionBL.getRequisitionService().getRequisitionById(
							requisitionId));
			log.info("Module: Accounts : Method: showRequisitionDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showRequisitionDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showQuotationPurchaseRequisitionDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showQuotationPurchaseRequisitionDetails");
			List<RequisitionDetail> requisitionDetails = requisitionBL
					.getRequisitionService()
					.getRequisitionDetailQuotationPurchaseIssue(requisitionId);
			List<RequisitionDetailVO> requisitionDetailVOs = new ArrayList<RequisitionDetailVO>();
			RequisitionDetailVO requisitionDetailVO = null;
			double requisitionQuantity = 0;
			double processedQuantity = 0;
			for (RequisitionDetail requisitionDetail : requisitionDetails) {
				requisitionQuantity = requisitionDetail.getQuantity();
				processedQuantity = 0;

				if (null != requisitionDetail.getQuotationDetails()
						&& requisitionDetail.getQuotationDetails().size() > 0) {
					for (QuotationDetail quotationDetail : requisitionDetail
							.getQuotationDetails()) {
						if (null != quotationDetail.getQuotation()
								.getPurchases()) {
							for (Purchase purchase : quotationDetail
									.getQuotation().getPurchases()) {
								for (PurchaseDetail prDt : purchase
										.getPurchaseDetails()) {
									if ((long) prDt.getProduct().getProductId() == (long) quotationDetail
											.getProduct().getProductId()) {
										processedQuantity += prDt.getQuantity();
									}
								}
							}
						}
					}
				}

				if (null != requisitionDetail.getPurchaseDetails()
						&& requisitionDetail.getPurchaseDetails().size() > 0) {
					for (PurchaseDetail purchaseDetail : requisitionDetail
							.getPurchaseDetails()) {
						processedQuantity += purchaseDetail.getQuantity();
					}
				}
				if (requisitionQuantity > processedQuantity) {
					requisitionDetailVO = new RequisitionDetailVO();
					BeanUtils.copyProperties(requisitionDetailVO,
							requisitionDetail);
					requisitionDetailVO.setUnitRate(requisitionBL
							.getPurchaseBL()
							.getStockBL()
							.getProductCostingPrice(
									requisitionDetail.getProduct()));
					requisitionDetailVO.setItemType(ItemType.get(
							requisitionDetail.getProduct().getItemType())
							.name());
					requisitionDetailVO.setQuantity(requisitionQuantity
							- processedQuantity);
					requisitionDetailVO.setTotalAmount(requisitionDetailVO
							.getQuantity() * requisitionDetailVO.getUnitRate());
					if (null != requisitionDetail.getProduct().getShelf()) {
						requisitionDetailVO.setShelfId(requisitionDetail
								.getProduct().getShelf().getShelfId());
						requisitionDetailVO.setStoreName(requisitionDetail
								.getProduct().getShelf().getShelf().getAisle()
								.getStore().getStoreName()
								+ "-->"
								+ requisitionDetail.getProduct().getShelf()
										.getShelf().getAisle().getSectionName()
								+ "-->"
								+ requisitionDetail.getProduct().getShelf()
										.getShelf().getName()
								+ "-->"
								+ requisitionDetail.getProduct().getShelf()
										.getName());
					}
					requisitionDetailVO.setProductPackageVOs(requisitionBL
							.getPurchaseBL()
							.getPackageConversionBL()
							.getProductPackagingDetail(
									requisitionDetail.getProduct()
											.getProductId()));
					requisitionDetailVO.setPackageUnit(requisitionDetailVO
							.getQuantity());
					requisitionDetailVOs.add(requisitionDetailVO);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_DETAIL", requisitionDetailVOs);
			log.info("Module: Accounts : Method: showQuotationPurchaseRequisitionDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showQuotationPurchaseRequisitionDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getProductAnalysis() {
		try {
			log.info("Inside Module: Accounts : Method: showQuotationPurchaseRequisitionDetails");
			List<RequisitionDetail> requisitionDetails = requisitionBL
					.getRequisitionService()
					.getRequisitionDetailByRequistionId(requisitionId);
			List<ProductVO> productVos = new ArrayList<ProductVO>();
			ProductVO productVO = null;
			List<Object> objectVos = null;
			List<PurchaseDetailVO> productPurchaseVos = null;
			String referenceNumber = "";
			Double qty = 0.0;
			for (RequisitionDetail detail : requisitionDetails) {
				productVO = new ProductVO();
				BeanUtils.copyProperties(productVO, detail.getProduct());

				for (Stock stk : detail.getProduct().getStocks()) {
					qty += stk.getQuantity();
				}
				productVO.setCurrentStock(qty);

				objectVos = requisitionBL.getPurchaseBL().getProductBL()
						.getProductPurchases(productVO.getProductId());
				productPurchaseVos = new ArrayList<PurchaseDetailVO>();
				for (Object object : objectVos) {
					productPurchaseVos.add((PurchaseDetailVO) object);
				}

				productVO
						.setProductPurchaseVos(new ArrayList<PurchaseDetailVO>(
								productPurchaseVos));
				referenceNumber = detail.getRequisition().getReferenceNumber();
				productVos.add(productVO);
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_LIST",
					productVos);
			ServletActionContext.getRequest().setAttribute("REFERENCE_NUMBER",
					referenceNumber);
			log.info("Module: Accounts : Method: showQuotationPurchaseRequisitionDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showQuotationPurchaseRequisitionDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: saveRequisition");
			Requisition requisition = new Requisition();
			requisition.setDescription(description);
			Person person = new Person();
			person.setPersonId(personId);
			requisition.setPerson(person);
			CmpDeptLoc cmpDeptLoc = new CmpDeptLoc();
			cmpDeptLoc.setCmpDeptLocId(locationId);
			requisition.setCmpDeptLoc(cmpDeptLoc);
			requisition.setRequisitionDate(DateFormat
					.convertStringToDate(requisitionDate));
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(requisitionDetails);
			JSONArray object = (JSONArray) receiveObject;
			RequisitionDetail requisitionDetail = null;
			Product product = null;
			List<RequisitionDetail> requisitionDetailList = new ArrayList<RequisitionDetail>();
			List<RequisitionDetail> deletedRequisitionDetails = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("requisitionDetails");
				for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					requisitionDetail = new RequisitionDetail();
					product = new Product();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"productId").toString()));
					requisitionDetail.setProduct(product);
					requisitionDetail.setQuantity(Double
							.parseDouble(jsonObjectDetail.get("quantity")
									.toString()));
					requisitionDetail.setDescription(jsonObjectDetail.get(
							"description").toString());
					requisitionDetail
							.setRequisitionDetailId((null != jsonObjectDetail
									.get("requisitionDetailId") && Long
									.parseLong(jsonObjectDetail.get(
											"requisitionDetailId").toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"requisitionDetailId").toString())
									: null);
				}
				requisitionDetailList.add(requisitionDetail);
			}
			if (requisitionId > 0) {
				requisition.setRequisitionId(requisitionId);
				requisition.setReferenceNumber(referenceNumber);
				deletedRequisitionDetails = getUserDeletedRequisitionDetails(requisitionDetailList);
			} else
				requisition.setReferenceNumber(SystemBL.getReferenceStamp(
						Requisition.class.getName(),
						requisitionBL.getImplementation()));

			requisition.setStatus(RequisitionProcessStatus.Open.getCode());
			requisitionBL.saveRequisition(requisition, requisitionDetailList,
					deletedRequisitionDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: deleteRequisition");
			Requisition requisition = requisitionBL.getRequisitionService()
					.getRequisitionById(requisitionId);
			requisitionBL.deleteRequisition(
					requisition,
					new ArrayList<RequisitionDetail>(requisition
							.getRequisitionDetails()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<RequisitionDetail> getUserDeletedRequisitionDetails(
			List<RequisitionDetail> requisitionDetailList) throws Exception {
		List<RequisitionDetail> existingRequisitionDetails = requisitionBL
				.getRequisitionService().getRequisitionDetailsById(
						requisitionId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, RequisitionDetail> hashRequisitionDetail = new HashMap<Long, RequisitionDetail>();
		if (null != existingRequisitionDetails
				&& existingRequisitionDetails.size() > 0) {
			for (RequisitionDetail list : existingRequisitionDetails) {
				listOne.add(list.getRequisitionDetailId());
				hashRequisitionDetail.put(list.getRequisitionDetailId(), list);
			}
			if (null != hashRequisitionDetail
					&& hashRequisitionDetail.size() > 0) {
				for (RequisitionDetail list : requisitionDetailList) {
					listTwo.add(list.getRequisitionDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<RequisitionDetail> deletedRequisitionDetails = null;
		if (null != different && different.size() > 0) {
			RequisitionDetail tempRequisitionDetail = null;
			deletedRequisitionDetails = new ArrayList<RequisitionDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempRequisitionDetail = new RequisitionDetail();
					tempRequisitionDetail = hashRequisitionDetail.get(list);
					deletedRequisitionDetails.add(tempRequisitionDetail);
				}
			}
		}
		return deletedRequisitionDetails;
	}

	// Getters & Setters
	public long getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(long requisitionId) {
		this.requisitionId = requisitionId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRequisitionDate() {
		return requisitionDate;
	}

	public void setRequisitionDate(String requisitionDate) {
		this.requisitionDate = requisitionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public RequisitionBL getRequisitionBL() {
		return requisitionBL;
	}

	public void setRequisitionBL(RequisitionBL requisitionBL) {
		this.requisitionBL = requisitionBL;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getRequisitionDetails() {
		return requisitionDetails;
	}

	public void setRequisitionDetails(String requisitionDetails) {
		this.requisitionDetails = requisitionDetails;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public boolean isSelfFlag() {
		return selfFlag;
	}

	public void setSelfFlag(boolean selfFlag) {
		this.selfFlag = selfFlag;
	}
}
