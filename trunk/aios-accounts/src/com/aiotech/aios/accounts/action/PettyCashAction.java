/**
 * Petty Cash Initial Development
 * Created On: 04-April-2013
 */
package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PettyCashDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PettyCashDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PettyCashVO;
import com.aiotech.aios.accounts.service.bl.PettyCashBL;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.PettyCashTypeCode;
import com.aiotech.aios.common.to.Constants.Accounts.PettyCashTypes;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PettyCashAction extends ActionSupport {

	private static final long serialVersionUID = -515938903981055260L;
	private long pettyCashId;
	private long receiptRefId;
	private long combinationId;
	private long settlementVoucherId;
	private long carryForwardVoucherId;
	private long personId;
	private int rowId;
	private boolean settlementRequired;
	private boolean againstInvoice;
	private String pettyCashNo;
	private String returnMessage;
	private String pettyCashType;
	private String description;
	private String receiptRefType;
	private String cashIn;
	private String cashOut;
	private String expenseAmount;
	private String invoiceNo;
	private Long recordId;
	private String voucherDate;
	private String receiverOther;
	private String printableContent;
	private String pettyCashDetailStr;

	private PettyCashBL pettyCashBL;
	private static final Logger LOGGER = LogManager
			.getLogger(PettyCashAction.class);

	// return success
	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess Success");
		return SUCCESS;
	}

	// list all petty cash
	public String showJsonPettyCash() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonPettyCash");
			JSONObject jsonList = pettyCashBL.getAllPettyCash();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonPettyCash: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonPettyCash Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// list all petty cash parent info
	public String showJsonParentPettyCash() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonParentPettyCash");
			JSONObject jsonList = pettyCashBL.getAllParentPettyCash();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonParentPettyCash: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonParentPettyCash Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showPettyCashPrint() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPettyCashPrint");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Boolean pettyCashTemplatePrintEnabled = session
					.getAttribute("print_pettycash_template") != null ? ((String) session
					.getAttribute("print_pettycash_template"))
					.equalsIgnoreCase("true") : false;
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.getPettyCashById(pettyCashId);
			if (!pettyCashTemplatePrintEnabled
					|| ((byte) pettyCash.getPettyCashType() != (byte) PettyCashTypes.Spend
							.getCode() && (byte) pettyCash.getPettyCashType() != (byte) PettyCashTypes.Reimbursed
							.getCode())) {
				PettyCash parentPettyCash = pettyCashBL
						.getParentPettyCashDetails(pettyCash);
				List<PettyCashVO> pettyCashVOs = pettyCashBL
						.recursivePettyCashLog(parentPettyCash);
				pettyCashVOs = pettyCashBL.createPettyCashLog(pettyCashVOs);
				ServletActionContext.getRequest().setAttribute(
						"PETTY_CASH_LOG", pettyCashVOs);

				ServletActionContext.getRequest().setAttribute(
						"PETTY_CASH_PRINT", pettyCashVOs);
				PettyCashVO pettyCashLog = totalCashOutAndExpense(pettyCashVOs,
						parentPettyCash.getPettyCashId());
				ServletActionContext.getRequest().setAttribute(
						"PETTYCASH_SUMMARAY", pettyCashLog);
				LOGGER.info("Module: Accounts : Method: showPettyCashPrint: Action Success");
				return SUCCESS;
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/templates/implementation_"
						+ pettyCashBL.getImplementation().getImplementationId()
						+ "/pettycash"));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				User user = (User) sessionObj.get("USER");
				List<UserRole> userRoles = pettyCashBL.getSystemService()
						.getUserRoles(user.getUsername());

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-pettycash-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();
				List<PettyCashDetailVO> pettyCashDetailVOs = new ArrayList<PettyCashDetailVO>();
				PettyCashDetailVO pettyCashDetailVO = null;
				if (null != pettyCash.getPettyCashDetails()
						&& pettyCash.getPettyCashDetails().size() > 0) {
					pettyCashDetailVOs = new ArrayList<PettyCashDetailVO>();
					for (PettyCashDetail pettyCashDetail : pettyCash
							.getPettyCashDetails()) {
						pettyCashDetailVO = new PettyCashDetailVO();
						BeanUtils.copyProperties(pettyCashDetailVO,
								pettyCashDetail);
						pettyCashDetailVO.setPersonName(null != pettyCashDetail
								.getPerson() ? pettyCashDetail.getPerson()
								.getFirstName()
								+ " "
								+ pettyCashDetail.getPerson().getLastName()
								: pettyCashDetail.getPayeeName());
						pettyCashDetailVO
								.setCostCenterStr(pettyCashDetail
										.getCombination()
										.getAccountByCostcenterAccountId()
										.getAccount());
						pettyCashDetailVO
								.setCombinationStr(pettyCashDetail
										.getCombination()
										.getAccountByNaturalAccountId()
										.getAccount()
										+ ""
										+ (null != pettyCashDetail
												.getCombination()
												.getAccountByAnalysisAccountId() ? "."
												+ pettyCashDetail
														.getCombination()
														.getAccountByAnalysisAccountId()
														.getAccount()
												: ""));
						pettyCashDetailVO.setExpenseAmountStr(AIOSCommons
								.formatAmount(pettyCashDetail
										.getExpenseAmount()));
						pettyCashDetailVOs.add(pettyCashDetailVO);
					}
					Collections.sort(pettyCashDetailVOs,
							new Comparator<PettyCashDetailVO>() {
								public int compare(PettyCashDetailVO o1,
										PettyCashDetailVO o2) {
									return o1.getPettyCashDetailId().compareTo(
											o2.getPettyCashDetailId());
								}
							});
				} else {
					pettyCashDetailVO = new PettyCashDetailVO();
					pettyCashDetailVO.setPersonName(null != pettyCash
							.getPersonByPersonId() ? pettyCash
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pettyCash.getPersonByPersonId().getLastName()
							: pettyCash.getReceiverOther());
					pettyCashDetailVO.setCostCenterStr(pettyCash
							.getCombination().getAccountByCostcenterAccountId()
							.getAccount());
					pettyCashDetailVO.setCombinationStr(pettyCash
							.getCombination().getAccountByNaturalAccountId()
							.getAccount()
							+ ""
							+ (null != pettyCash.getCombination()
									.getAccountByAnalysisAccountId() ? "."
									+ pettyCash.getCombination()
											.getAccountByAnalysisAccountId()
											.getAccount() : ""));
					pettyCashDetailVO.setExpenseAmountStr(AIOSCommons
							.formatAmount(pettyCash.getExpenseAmount()));
					pettyCashDetailVO.setBillNo(pettyCash.getInvoiceNumber());
					pettyCashDetailVOs.add(pettyCashDetailVO);
				}
				rootMap.put("datetime", DateFormat
						.convertDateToString(pettyCash.getVoucherDate()
								.toString()));
				rootMap.put("referenceNumber", pettyCash.getPettyCashNo());
				if (null != pettyCash.getReceiverOther()
						&& !("").equals(pettyCash.getReceiverOther()))
					rootMap.put("payeeName", pettyCash.getReceiverOther());
				else if (null != pettyCash.getPersonByPersonId())
					rootMap.put("payeeName", pettyCash.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ pettyCash.getPersonByPersonId().getLastName());
				rootMap.put("roleName", "");
				if (null != userRoles && userRoles.size() > 0) {
					Role role = userRoles.get(0).getRole();
					rootMap.put("roleName", role.getRoleName());
				}
				if (null != pettyCash.getCombination()) {
					if (null != pettyCash.getCombination()
							.getAccountByAnalysisAccountId())
						rootMap.put("accountDescription", pettyCash
								.getCombination()
								.getAccountByAnalysisAccountId().getAccount());
					else
						rootMap.put("accountDescription", pettyCash
								.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				}
				rootMap.put("expenseAmount",
						AIOSCommons.formatAmount(pettyCash.getExpenseAmount()));
				rootMap.put(
						"description",
						null != pettyCash.getDescription() ? pettyCash
								.getDescription() : "");
				rootMap.put("pettyCashDetailVOs", pettyCashDetailVOs);
				rootMap.put("preparedBy", pettyCash.getPersonByCreatedBy()
						.getFirstName()
						+ " "
						+ pettyCash.getPersonByCreatedBy().getLastName());
				String amountInWords = AIOSCommons
						.convertAmountToWords(pettyCash.getExpenseAmount());
				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(pettyCash.getExpenseAmount()));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("amountInWords", amountInWords);
				rootMap.put("urlPath", urlPath);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				LOGGER.info("Module: Accounts : Method: showPettyCashPrint: Action Success");
				return "template";
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPettyCashPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private PettyCashVO totalCashOutAndExpense(List<PettyCashVO> pettyCashVOs,
			long pettyCashId) throws Exception {
		double cashOut = 0;
		double expenseAmount = 0;
		PettyCashVO pettyCash = null;
		if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
			pettyCash = new PettyCashVO();
			for (PettyCashVO pettyCashVO : pettyCashVOs) {
				if (((byte) PettyCashTypes.Cash_transfer.getCode() == (byte) pettyCashVO
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Carry_forward.getCode() == (byte) pettyCashVO
								.getPettyCashType())) {
					cashOut += (null != pettyCashVO.getCashIn() ? pettyCashVO
							.getCashIn() : 0);
				} else if (((byte) PettyCashTypes.Spend.getCode() == (byte) pettyCashVO
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Reimbursed.getCode() == (byte) pettyCashVO
								.getPettyCashType())) {
					expenseAmount += (null != pettyCashVO.getExpenseAmount() ? pettyCashVO
							.getExpenseAmount() : 0);
				}
			}
			List<PettyCash> pettyCashSetteled = pettyCashBL
					.getPettyCashService().getParentPettyCashSettledVouchers(
							pettyCashId,
							PettyCashTypes.Cash_given_settlement.getCode());
			double forwaredAmount = 0;
			if (null != pettyCashSetteled && pettyCashSetteled.size() > 0) {
				for (PettyCash py : pettyCashSetteled)
					forwaredAmount += py.getCashOut();
			}
			pettyCash.setExpenseAmountFormat(AIOSCommons
					.formatAmount(expenseAmount));
			pettyCash.setCashOutFormat(AIOSCommons.formatAmount(cashOut));
			pettyCash.setCashForwaredStr(AIOSCommons
					.formatAmount(forwaredAmount));
			pettyCash.setRemainingBalanceFormat(AIOSCommons
					.formatAmount(cashOut - (expenseAmount + forwaredAmount)));
		}
		return pettyCash;
	}

	// show add or update petty cash
	public String showPettyCashEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPettyCashEntry");
			PettyCashVO editPettyCashVO = null;
			if (pettyCashId > 0) {
				PettyCash pettyCash = pettyCashBL.getPettyCashService()
						.getPettyCashById(pettyCashId);
				if (pettyCash.getPettyCash() != null
						&& pettyCash.getPettyCash().getPettyCashId() != null) {
					settlementVoucherId = pettyCashId;
				}

				editPettyCashVO = new PettyCashVO();
				BeanUtils.copyProperties(editPettyCashVO, pettyCash);
				if (pettyCash.getVoucherDate() != null) {
					editPettyCashVO.setVoucherDateView(DateFormat
							.convertDateToString(pettyCash.getVoucherDate()
									+ ""));
				}
				editPettyCashVO.setCreatedBy(null != pettyCash
						.getPersonByCreatedBy() ? pettyCash
						.getPersonByCreatedBy().getFirstName()
						+ " "
						+ pettyCash.getPersonByCreatedBy().getLastName() : "");
				editPettyCashVO.setEmployeeName(null != pettyCash
						.getPersonByPersonId() ? pettyCash
						.getPersonByPersonId().getFirstName()
						+ " "
						+ pettyCash.getPersonByPersonId().getLastName()
						: editPettyCashVO.getReceiverOther());
				editPettyCashVO.setPettyCashTypeName(pettyCash
						.getPettyCashType()
						+ "@@"
						+ PettyCashTypeCode.get(pettyCash.getPettyCashType())
								.name());
				if (null != pettyCash.getDirectPaymentDetail()) {
					editPettyCashVO.setReceiptVoucherId(pettyCash
							.getDirectPaymentDetail()
							.getDirectPaymentDetailId());
					editPettyCashVO.setReceiptVoucher(pettyCash
							.getDirectPaymentDetail().getDirectPayment()
							.getPaymentNumber());
					editPettyCashVO.setReceiptReference("Direct Payment");
				} else if (null != pettyCash.getBankReceiptsDetail()) {
					editPettyCashVO.setReceiptVoucherId(pettyCash
							.getBankReceiptsDetail().getBankReceiptsDetailId());
					editPettyCashVO.setReceiptVoucher(pettyCash
							.getBankReceiptsDetail().getBankReceipts()
							.getReferenceNo());
					editPettyCashVO.setReceiptReference("Bank Receipts");
				}
				if (null != pettyCash.getPettyCash()) {
					editPettyCashVO.setParentPettyCashId(pettyCash
							.getPettyCash().getPettyCashId());
					editPettyCashVO.setParentReference(pettyCash.getPettyCash()
							.getPettyCashNo());
				}
				if (null != pettyCash.getCombination()) {
					editPettyCashVO.setCombinationStr(pettyCash
							.getCombination().getAccountByCompanyAccountId()
							.getAccount()
							+ "."
							+ pettyCash.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccount()
							+ "."
							+ pettyCash.getCombination()
									.getAccountByNaturalAccountId()
									.getAccount());
					if (null != pettyCash.getCombination()
							.getAccountByAnalysisAccountId()) {
						editPettyCashVO.setCombinationStr(editPettyCashVO
								.getCombinationStr()
								+ "."
								+ pettyCash.getCombination()
										.getAccountByAnalysisAccountId()
										.getAccount());
					}
				}
				if (null != pettyCash.getPettyCashDetails()
						&& pettyCash.getPettyCashDetails().size() > 0) {
					List<PettyCashDetailVO> pettyCashDetailVOs = new ArrayList<PettyCashDetailVO>();
					PettyCashDetailVO pettyCashDetailVO = null;
					for (PettyCashDetail pettyCashDetail : pettyCash
							.getPettyCashDetails()) {
						pettyCashDetailVO = new PettyCashDetailVO();
						BeanUtils.copyProperties(pettyCashDetailVO,
								pettyCashDetail);
						pettyCashDetailVO.setPersonName(null != pettyCashDetail
								.getPerson() ? pettyCashDetail.getPerson()
								.getFirstName()
								+ " "
								+ pettyCashDetail.getPerson().getLastName()
								: pettyCashDetail.getPayeeName());
						pettyCashDetailVO
								.setCombinationStr(pettyCashDetail
										.getCombination()
										.getAccountByCostcenterAccountId()
										.getAccount()
										+ "."
										+ pettyCashDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccount()
										+ ""
										+ (null != pettyCashDetail
												.getCombination()
												.getAccountByAnalysisAccountId() ? "."
												+ pettyCashDetail
														.getCombination()
														.getAccountByAnalysisAccountId()
														.getAccount()
												: ""));
						pettyCashDetailVOs.add(pettyCashDetailVO);
					}
					List<PettyCash> pettyCashs = new ArrayList<PettyCash>();
					pettyCashs.add(pettyCash.getPettyCash());
					List<PettyCashVO> pettyCashVOs = pettyCashBL
							.validateAdvanceIssuedPettyCashVoucher(pettyCashs);
					if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
						editPettyCashVO.setCashOut(pettyCashVOs.get(0)
								.getCashOut());
						editPettyCashVO.setRemainingBalance(pettyCashVOs.get(0)
								.getCashOut()
								- editPettyCashVO.getExpenseAmount());
					}
					Collections.sort(pettyCashDetailVOs,
							new Comparator<PettyCashDetail>() {
								public int compare(PettyCashDetail o1,
										PettyCashDetail o2) {
									return o1.getPettyCashDetailId().compareTo(
											o2.getPettyCashDetailId());
								}
							});
					ServletActionContext.getRequest().setAttribute(
							"PETTYCASH_DETAIL", pettyCashDetailVOs);
				}
				if ((byte) editPettyCashVO.getPettyCashType() == (byte) PettyCashTypes.Reimbursed
						.getCode()) {
					List<PettyCash> pettyCashs = new ArrayList<PettyCash>();
					pettyCashs.add(pettyCash.getPettyCash());
					List<PettyCashVO> pettyCashVOs = pettyCashBL
							.validateAdvanceIssuedPettyCashVoucher(pettyCashs);
					if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
						editPettyCashVO.setCashOut(pettyCashVOs.get(0)
								.getCashOut()
								+ editPettyCashVO.getExpenseAmount());
					}
				}
			} else {
				ServletActionContext.getRequest().setAttribute(
						"PETTY_CASH_NUMBER",
						SystemBL.getReferenceStamp(PettyCash.class.getName(),
								pettyCashBL.getImplementation()));
			}
			Map<String, String> pettyCashTypes = pettyCashBL
					.getPettyCashTypes();
			List<PettyCashVO> pettyCashVOs = new ArrayList<PettyCashVO>();
			PettyCashVO pettyCashVO = null;
			for (Entry<String, String> types : pettyCashTypes.entrySet()) {
				String type[] = splitValues(types.getKey(), "@@");
				pettyCashVO = new PettyCashVO();
				pettyCashVO.setKey(type[0]);
				pettyCashVO.setCode(type[1]);
				pettyCashVO.setValue(types.getValue());
				pettyCashVOs.add(pettyCashVO);
			}
			Collections.sort(pettyCashVOs, new Comparator<PettyCashVO>() {
				public int compare(PettyCashVO p1, PettyCashVO p2) {
					return p1.getKey().compareTo(p2.getKey());
				}
			});
			for (PettyCashVO pettyCash : pettyCashVOs)
				pettyCash.setKey(pettyCash.getKey().concat("@@")
						.concat(pettyCash.getCode()));
			ServletActionContext.getRequest().setAttribute("PETTY_CASH_TYPES",
					pettyCashVOs);
			ServletActionContext.getRequest().setAttribute("PETTYCASH_INFO",
					editPettyCashVO);

			if (settlementVoucherId > 0) {
				List<PettyCashVO> pettyCashVOsLog = pettyCashBL
						.recursivePettyCashLog(pettyCashBL
								.getParentPettyCashDetails(editPettyCashVO));
				pettyCashVOsLog = pettyCashBL
						.createPettyCashLog(pettyCashVOsLog);
				ServletActionContext.getRequest().setAttribute(
						"PETTY_CASH_LOG", pettyCashVOsLog);
			}

			LOGGER.info("Module: Accounts : Method: showPettyCashEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPettyCashEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// show all petty cash receipt reference
	public String showReceiptReference() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showReceiptReference");
			List<BankReceiptsDetail> receiptDetails = pettyCashBL
					.getBankReceiptsBL()
					.getBankReceiptsService()
					.getUnDepositedCashReceiptDetails(
							pettyCashBL.getImplementation(),
							ModeOfPayment.Cash.getCode());
			Map<Long, BankReceiptsDetail> receiptMap = processReceiptPettyCash(receiptDetails);
			List<DirectPaymentDetail> directPaymentDetails = pettyCashBL
					.getDirectPaymentBL()
					.getDirectPaymentService()
					.getCashDirectPaymentDetails(
							pettyCashBL.getImplementation(),
							ModeOfPayment.Cash.getCode());
			Map<Long, DirectPaymentDetail> directPaymentMap = processDirectPaymentPettyCash(directPaymentDetails);
			JSONObject jsonList = pettyCashBL
					.createReceipts(
							(null != directPaymentMap
									&& directPaymentMap.size() > 0 ? new ArrayList<DirectPaymentDetail>(
									directPaymentMap.values()) : null),
							(null != receiptMap && receiptMap.size() > 0 ? new ArrayList<BankReceiptsDetail>(
									receiptMap.values()) : null));
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showReceiptReference: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showReceiptReference: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Map<Long, BankReceiptsDetail> processReceiptPettyCash(
			List<BankReceiptsDetail> receiptDetails) throws Exception {
		Map<Long, BankReceiptsDetail> receiptMap = null;
		if (null != receiptDetails && receiptDetails.size() > 0) {
			receiptMap = new HashMap<Long, BankReceiptsDetail>();
			for (BankReceiptsDetail bankReceiptsDetail : receiptDetails)
				receiptMap.put(bankReceiptsDetail.getBankReceiptsDetailId(),
						bankReceiptsDetail);
			List<PettyCash> pettyCashes = pettyCashBL.getPettyCashService()
					.getReceiptsPettyCashOnlyCashTransfer(receiptMap.keySet());
			BankReceiptsDetail receiptsDetail = null;
			if (null != pettyCashes && pettyCashes.size() > 0) {
				for (PettyCash pettyCash : pettyCashes) {
					if (receiptMap.containsKey(pettyCash
							.getBankReceiptsDetail().getBankReceiptsDetailId())) {
						receiptsDetail = receiptMap.get(pettyCash
								.getBankReceiptsDetail()
								.getBankReceiptsDetailId());
						receiptsDetail
								.setAmount((receiptsDetail.getAmount() - pettyCash
										.getCashIn()));
						if (receiptsDetail.getAmount() > 0)
							receiptMap.put(
									receiptsDetail.getBankReceiptsDetailId(),
									receiptsDetail);
					}
				}
			}
		}
		return receiptMap;
	}

	private Map<Long, DirectPaymentDetail> processDirectPaymentPettyCash(
			List<DirectPaymentDetail> directPaymentDetails) throws Exception {
		Map<Long, DirectPaymentDetail> entryMap = null;
		if (null != directPaymentDetails && directPaymentDetails.size() > 0) {
			entryMap = new HashMap<Long, DirectPaymentDetail>();
			for (DirectPaymentDetail paymentDetail : directPaymentDetails)
				entryMap.put(paymentDetail.getDirectPaymentDetailId(),
						paymentDetail);
			List<PettyCash> pettyCashes = pettyCashBL.getPettyCashService()
					.getDirectPaymentPettyCashOnlyCashTransfer(
							entryMap.keySet());
			DirectPaymentDetail directPaymentDetail = null;
			if (null != pettyCashes && pettyCashes.size() > 0) {
				for (PettyCash pettyCash : pettyCashes) {
					if (entryMap.containsKey(pettyCash.getDirectPaymentDetail()
							.getDirectPaymentDetailId())) {
						directPaymentDetail = entryMap.get(pettyCash
								.getDirectPaymentDetail()
								.getDirectPaymentDetailId());
						directPaymentDetail.setAmount((directPaymentDetail
								.getAmount() - pettyCash.getCashIn()));
						entryMap.put(
								directPaymentDetail.getDirectPaymentDetailId(),
								directPaymentDetail);
					}
				}
			}
		}
		return entryMap;
	}

	// show all settlement voucher
	public String showAllSettlementVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllSettlementVoucher");
			String[] pettyCashTypeArray = splitValues(pettyCashType, "@@");
			List<PettyCashVO> pettyCashVOs = null;
			if (null != pettyCashTypeArray[1]
					&& (("AIPC").equalsIgnoreCase(pettyCashTypeArray[1]) || ("RFPC")
							.equalsIgnoreCase(pettyCashTypeArray[1]))
					|| ("CFRD").equalsIgnoreCase(pettyCashTypeArray[1])) {
				List<PettyCash> pettyCashList = pettyCashBL
						.getPettyCashService().getAllReceiptsPettyCash(
								PettyCashTypes.Cash_transfer.getCode(),
								pettyCashBL.getImplementation(),
								TransactionType.Credit.getCode());
				if (null != pettyCashList && pettyCashList.size() > 0)
					pettyCashVOs = pettyCashBL
							.validateAIPCPettyCashVoucher(pettyCashList);
			} else {
				List<PettyCash> pettyCashList = pettyCashBL
						.getPettyCashService().getAllReceiptsPettyCash(
								PettyCashTypes.Advance_issued.getCode(),
								pettyCashBL.getImplementation(),
								TransactionType.Credit.getCode());
				if (null != pettyCashList && pettyCashList.size() > 0) {
					if (("SFAI").equalsIgnoreCase(pettyCashTypeArray[1])
							|| ("CRFS").equalsIgnoreCase(pettyCashTypeArray[1])) {
						pettyCashVOs = pettyCashBL
								.validateSFAIPettyCashVoucher(pettyCashList,
										getAdvanceIssuedPettyCash(),
										Byte.valueOf(pettyCashTypeArray[0]));
					} else if (("CGFS").equalsIgnoreCase(pettyCashTypeArray[1])) {
						pettyCashVOs = pettyCashBL
								.validateCGFSPettyCashVoucher(pettyCashList,
										getAdvanceIssuedPettyCash());
					}
				}
			}
			JSONObject jsonList = pettyCashBL
					.getConvertPettyCashList(pettyCashVOs);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showAllSettlementVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllSettlementVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAllCashInVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllCashInVoucher");
			List<PettyCashVO> pettyCashVOs = null;
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getAllReceiptsPettyCash(
							PettyCashTypes.Cash_transfer.getCode(),
							pettyCashBL.getImplementation(),
							TransactionType.Credit.getCode());
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashVOs = pettyCashBL
						.validateAIPCPettyCashVoucher(pettyCashs);
			JSONObject jsonList = pettyCashBL
					.convertCashInPettyCashVO(pettyCashVOs);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showAllCashInVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllCashInVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAllCashInForwardVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllCashInForwardVoucher");
			List<PettyCashVO> pettyCashVOs = null;
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getPettyCashInCarryForward(
							PettyCashTypes.Cash_transfer.getCode(),
							TransactionType.Credit.getCode(),
							pettyCashBL.getImplementation(), pettyCashId);
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashVOs = pettyCashBL
						.validateAIPCPettyCashVoucher(pettyCashs);
			JSONObject jsons = pettyCashBL
					.convertCashInPettyCashVO(pettyCashVOs);
			ServletActionContext.getRequest().setAttribute("jsonlist", jsons);
			LOGGER.info("Module: Accounts : Method: showAllCashInForwardVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllCashInForwardVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAllAdvanceIssuedVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllAdvanceIssuedVoucher");
			List<PettyCashVO> pettyCashVOs = null;
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getAllReceiptsPettyCash(
							PettyCashTypes.Advance_issued.getCode(),
							pettyCashBL.getImplementation(),
							TransactionType.Credit.getCode());
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashVOs = pettyCashBL
						.validateAdvanceIssuedPettyCashVoucher(pettyCashs);
			JSONObject jsonList = pettyCashBL
					.convertAdvanceIssuedPettyCashVO(pettyCashVOs);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showAllAdvanceIssuedVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllAdvanceIssuedVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAllNonClosedAdvanceIssuedVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllNonClosedAdvanceIssuedVoucher");
			List<PettyCashVO> pettyCashVOs = null;
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getAllNonClosedReceiptsPettyCash(
							PettyCashTypes.Advance_issued.getCode(),
							pettyCashBL.getImplementation());
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashVOs = pettyCashBL
						.validateAdvanceIssuedPettyCashVoucher(pettyCashs);
			JSONObject jsonList = pettyCashBL
					.convertAdvanceIssuedPettyCashVO(pettyCashVOs);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showAllNonClosedAdvanceIssuedVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllNonClosedAdvanceIssuedVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAllSpendVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllSpendVoucher");
			List<PettyCashVO> pettyCashVOs = null;
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getAllReceiptsPettyCash(PettyCashTypes.Spend.getCode(),
							pettyCashBL.getImplementation(),
							TransactionType.Credit.getCode());
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashVOs = pettyCashBL
						.validateSpendPettyCashVoucher(pettyCashs);
			JSONObject jsonList = pettyCashBL
					.convertExpenseVoucherPettyCashVO(pettyCashVOs);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showAllSpendVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllSpendVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAdvanceIssuedPettyCashAmount() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAdvanceIssuedPettyCashAmount");
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.getParentPettyCash(pettyCashId);
			List<PettyCash> pettyCashs = new ArrayList<PettyCash>();
			pettyCashs.add(pettyCash.getPettyCash());
			List<PettyCashVO> pettyCashVOs = pettyCashBL
					.validateAdvanceIssuedPettyCashVoucher(pettyCashs);
			cashOut = "0";
			if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
				for (PettyCashVO pettyCashVO : pettyCashVOs)
					cashOut = String.valueOf(Double.parseDouble(cashOut)
							+ pettyCashVO.getCashOut());
			}
			LOGGER.info("Module: Accounts : Method: showAdvanceIssuedPettyCashAmount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAdvanceIssuedPettyCashAmount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCashTransferPettyCashAmount() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCashTransferPettyCashAmount");
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.getParentPettyCash(pettyCashId);
			List<PettyCash> pettyCashs = new ArrayList<PettyCash>();
			pettyCashs.add(pettyCash.getPettyCash());
			List<PettyCashVO> pettyCashVOs = pettyCashBL
					.validateAIPCPettyCashVoucher(pettyCashs);
			cashIn = "0";
			if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
				for (PettyCashVO pettyCashVO : pettyCashVOs)
					cashIn = String.valueOf(Double.parseDouble(cashIn)
							+ pettyCashVO.getCashIn());
			}
			LOGGER.info("Module: Accounts : Method: showCashTransferPettyCashAmount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCashTransferPettyCashAmount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showNonSpendPettyCashVoucher() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showNonSpendPettyCashVoucher");
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getReceiptsPettyCashWithOutSettlementId(
							PettyCashTypes.Cash_transfer.getCode(),
							pettyCashBL.getImplementation(),
							carryForwardVoucherId);
			List<PettyCashVO> pettyCashVOs = null;
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashVOs = pettyCashBL.validateCFSPettyCashVoucher(
						pettyCashs, getOnlyAdvanceIssuedPettyCash());
			JSONObject jsonList = pettyCashBL
					.getConvertPettyCashList(pettyCashVOs);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showNonSpendPettyCashVoucher: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showNonSpendPettyCashVoucher: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// show maximum petty cash available balance
	public String showMaximumPettyCash() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showMaximumPettyCash");
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.findPettyCashById(settlementVoucherId);
			List<PettyCash> pettyCashList = null;
			if (null != pettyCash.getDirectPaymentDetail()) {
				pettyCashList = pettyCashBL.getPettyCashService()
						.getAllPettyCashByPayments(
								pettyCash.getDirectPaymentDetail());
			} else {
				pettyCashList = pettyCashBL.getPettyCashService()
						.getAllPettyCashByReceipts(
								pettyCash.getBankReceiptsDetail());
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					pettyCashBL.showMaximumPettyCash(pettyCashList));
			LOGGER.info("Module: Accounts : Method: showMaximumPettyCash: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showMaximumPettyCash: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// show petty cash log
	public String showPettyCashLog() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPettyCashLog");
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.getPettyCashById(settlementVoucherId);
			pettyCash = pettyCashBL.getParentPettyCashDetails(pettyCash);
			List<PettyCashVO> pettyCashVOs = pettyCashBL
					.recursivePettyCashLog(pettyCash);
			pettyCashVOs = pettyCashBL.createPettyCashLog(pettyCashVOs);
			ServletActionContext.getRequest().setAttribute("PETTY_CASH_LOG",
					pettyCashVOs);
			LOGGER.info("Module: Accounts : Method: showPettyCashLog: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPettyCashLog: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// save or update petty cash
	public String savePettyCash() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: savePettyCash");
			// Validate Period
			boolean openPeriod = pettyCashBL.getTransactionBL().getCalendarBL()
					.transactionPostingPeriod(voucherDate);

			if (!openPeriod) {
				returnMessage = "Period not open for the voucher date.";
				LOGGER.info("Module: Accounts : Method: savePettyCash: (Error : Period not opened)");
				return SUCCESS;
			}

			PettyCash pettyCash = new PettyCash();
			PettyCash pettyCashtmp = null;
			if (pettyCashId > 0) {
				pettyCashtmp = pettyCashBL.getPettyCashService()
						.getBasicPettyCashById(pettyCashId);
				pettyCash.setPettyCashId(pettyCashId);
				pettyCash.setPettyCashNo(pettyCashtmp.getPettyCashNo());
				pettyCash.setPersonByCreatedBy(pettyCashtmp
						.getPersonByCreatedBy());
			} else
				pettyCash.setPettyCashNo(SystemBL.getReferenceStamp(
						PettyCash.class.getName(),
						pettyCashBL.getImplementation()));
			String[] pettyCashTypeArray = splitValues(pettyCashType, "@@");
			PettyCash cashCarryForward = null;
			pettyCash.setPettyCashType(Byte.parseByte(pettyCashTypeArray[0]));
			pettyCash.setSettelmentFlag(settlementRequired);
			pettyCash.setAgainstInvoice(againstInvoice);
			pettyCash.setDescription(description);
			List<PettyCashDetail> pettyCashDetails = null;
			List<PettyCashDetail> pettyCashDetailDel = null;
			PettyCash voucher = null;
			PettyCash rootPettyCash = null;
			if (null != pettyCashTypeArray[1]
					&& ("CTPC").equalsIgnoreCase(pettyCashTypeArray[1])) {
				if (null != receiptRefType && !("").equals(receiptRefType)
						&& (DirectPayment.class.getSimpleName()).equalsIgnoreCase(receiptRefType)) {
					DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
					directPaymentDetail.setDirectPaymentDetailId(receiptRefId);
					pettyCash.setDirectPaymentDetail(directPaymentDetail);
				} else {
					BankReceiptsDetail bankReceiptsDetail = new BankReceiptsDetail();
					bankReceiptsDetail.setBankReceiptsDetailId(receiptRefId);
					pettyCash.setBankReceiptsDetail(bankReceiptsDetail);
				}
				pettyCash.setCashIn(AIOSCommons.formatAmountToDouble(cashIn));
			} else if (null != pettyCashTypeArray[1]
					&& ("AIPC").equalsIgnoreCase(pettyCashTypeArray[1])) {
				pettyCash.setCashOut(AIOSCommons.formatAmountToDouble(cashOut));
				voucher = pettyCashBL.getPettyCashService().findPettyCashById(
						settlementVoucherId);
				voucher.setSettelmentFlag(true);
				pettyCash.setPettyCash(voucher);
				if (null != voucher.getDirectPaymentDetail())
					pettyCash.setDirectPaymentDetail(voucher
							.getDirectPaymentDetail());
				else
					pettyCash.setBankReceiptsDetail(voucher
							.getBankReceiptsDetail());
				voucher.setClosedFlag(validateOpenCashTransferPettyCash(
						voucher, pettyCash, pettyCashId));
			} else if (null != pettyCashTypeArray[1]
					&& ("RFPC").equalsIgnoreCase(pettyCashTypeArray[1])) {
				pettyCash.setExpenseAmount(AIOSCommons
						.formatAmountToDouble(expenseAmount));
				pettyCash.setCashOut(("RFPC")
						.equalsIgnoreCase(pettyCashTypeArray[1]) ? AIOSCommons
						.formatAmountToDouble(expenseAmount) : null);
				voucher = pettyCashBL.getPettyCashService().findPettyCashById(
						settlementVoucherId);
				pettyCash.setPettyCash(voucher);
				if (null != voucher.getDirectPaymentDetail())
					pettyCash.setDirectPaymentDetail(voucher
							.getDirectPaymentDetail());
				else
					pettyCash.setBankReceiptsDetail(voucher
							.getBankReceiptsDetail());
				Combination combination = new Combination();
				combination.setCombinationId(combinationId);
				pettyCash.setCombination(combination);
				pettyCash.setInvoiceNumber(invoiceNo);

			} else if (null != pettyCashTypeArray[1]
					&& ("CGFS").equalsIgnoreCase(pettyCashTypeArray[1])) {
				pettyCash.setCashOut(AIOSCommons.formatAmountToDouble(cashOut));
				voucher = pettyCashBL.getPettyCashService().findPettyCashById(
						settlementVoucherId);
				pettyCash.setPettyCash(voucher);
				if (null != voucher.getDirectPaymentDetail())
					pettyCash.setDirectPaymentDetail(voucher
							.getDirectPaymentDetail());
				else
					pettyCash.setBankReceiptsDetail(voucher
							.getBankReceiptsDetail());
				pettyCash.setInvoiceNumber(invoiceNo);
				voucher.setClosedFlag(false);
				rootPettyCash = pettyCashBL.getParentPettyCashDetails(voucher);
				rootPettyCash.setClosedFlag(validateOpenCashTransferPettyCash(
						rootPettyCash, pettyCash, pettyCashId));
			} else if (null != pettyCashTypeArray[1]
					&& ("CRFS").equalsIgnoreCase(pettyCashTypeArray[1])) {
				pettyCash.setCashIn(AIOSCommons.formatAmountToDouble(cashIn));
				voucher = pettyCashBL.getPettyCashService().findPettyCashById(
						settlementVoucherId);
				pettyCash.setPettyCash(voucher);
				if (null != voucher.getDirectPaymentDetail())
					pettyCash.setDirectPaymentDetail(voucher
							.getDirectPaymentDetail());
				else
					pettyCash.setBankReceiptsDetail(voucher
							.getBankReceiptsDetail());
				pettyCash.setInvoiceNumber(invoiceNo);
				voucher.setClosedFlag(validateOpenAdvanceIssuedPettyCash(
						voucher, pettyCash, pettyCashId, pettyCash.getCashIn()));
				rootPettyCash = pettyCashBL.getParentPettyCashDetails(voucher);
				rootPettyCash.setClosedFlag(false);
			} else if (null != pettyCashTypeArray[1]
					&& ("CFRD").equalsIgnoreCase(pettyCashTypeArray[1])) {
				pettyCash.setCashIn(AIOSCommons.formatAmountToDouble(cashIn));
				cashCarryForward = pettyCashBL.getPettyCashService()
						.findPettyCashById(settlementVoucherId);
				voucher = pettyCashBL.getPettyCashService().findPettyCashById(
						settlementVoucherId);
				cashCarryForward.setPettyCash(voucher);
				cashCarryForward.setPettyCashId(null);
				PettyCash toVoucher = pettyCashBL.getPettyCashService()
						.findPettyCashById(carryForwardVoucherId);
				if (null != toVoucher.getDirectPaymentDetail())
					pettyCash.setDirectPaymentDetail(toVoucher
							.getDirectPaymentDetail());
				else
					pettyCash.setBankReceiptsDetail(toVoucher
							.getBankReceiptsDetail());
				pettyCash.setPettyCash(toVoucher);
				cashCarryForward.setCashIn(null);
				pettyCash.setInvoiceNumber(invoiceNo);
				cashCarryForward.setCashOut(AIOSCommons
						.formatAmountToDouble(cashIn));
				cashCarryForward
						.setPettyCashType(PettyCashTypes.Cash_given_settlement
								.getCode());
				cashCarryForward.setPettyCashNo(pettyCash.getPettyCashNo());
				cashCarryForward.setVoucherDate(Calendar.getInstance()
						.getTime());
				voucher.setClosedFlag(validateOpenCashTransferPettyCash(
						voucher, cashCarryForward, pettyCashId));
			} else if (null != pettyCashTypeArray[1]
					&& ("SFAI").equalsIgnoreCase(pettyCashTypeArray[1])) {
				voucher = pettyCashBL.getPettyCashService().findPettyCashById(
						settlementVoucherId);
				pettyCash.setPettyCash(voucher);
				if (null != voucher.getDirectPaymentDetail())
					pettyCash.setDirectPaymentDetail(voucher
							.getDirectPaymentDetail());
				else
					pettyCash.setBankReceiptsDetail(voucher
							.getBankReceiptsDetail());
				pettyCashDetails = getPettyCashDetail();
				double expenseAmount = 0;
				for (PettyCashDetail pettyCashDetail : pettyCashDetails)
					expenseAmount += pettyCashDetail.getExpenseAmount();
				pettyCash.setExpenseAmount(expenseAmount);
				if (pettyCashId > 0)
					pettyCashDetailDel = getDeletedPettyCashDetail(pettyCashDetails);

				voucher.setClosedFlag(validateOpenAdvanceIssuedPettyCash(
						voucher, pettyCash, pettyCashId, expenseAmount));
			}
			if (personId > 0) {
				Person person = new Person();
				person.setPersonId(personId);
				pettyCash.setPersonByPersonId(person);
			}
			if (voucherDate != null && !voucherDate.equals(""))
				pettyCash.setVoucherDate(DateFormat
						.convertStringToDate(voucherDate));
			else
				pettyCash.setVoucherDate(Calendar.getInstance().getTime());
			pettyCash.setClosedFlag(false);
			pettyCashBL.savePettyCash(pettyCash, cashCarryForward,
					pettyCashDetails, pettyCashDetailDel, voucher,
					rootPettyCash);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: savePettyCash: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Record save failure.";
			pettyCashId = 0;
			LOGGER.info("Module: Accounts : Method: savePettyCash: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Boolean validateOpenCashTransferPettyCash(PettyCash voucher,
			PettyCash pettyCash, long pettyCashOldId) throws Exception {
		List<PettyCash> pettyCashes = pettyCashBL.getPettyCashService()
				.getAllAdvanceIssedPettyCashLevel(voucher.getPettyCashId(),
						PettyCashTypeCode.AIPC.getCode(),
						PettyCashTypeCode.CGFS.getCode(), (byte) -1,
						pettyCashOldId);
		double totalIssued = pettyCash.getCashOut();
		double childCashIn = 0;
		if (null != pettyCashes && pettyCashes.size() > 0) {
			List<PettyCash> childPettyCash = null;
			for (PettyCash py : pettyCashes) {
				childPettyCash = pettyCashBL.getPettyCashService()
						.getAllAdvanceIssedPettyCashLevel(py.getPettyCashId(),
								PettyCashTypeCode.CRFS.getCode(),
								PettyCashTypeCode.CGFS.getCode(),
								PettyCashTypeCode.CFRD.getCode(), 0l);
				if (null != pettyCashes && pettyCashes.size() > 0) {
					for (PettyCash py1 : childPettyCash) {
						if (((byte) py1.getPettyCashType() == (byte) PettyCashTypeCode.CRFS
								.getCode())
								|| (byte) py1.getPettyCashType() == (byte) PettyCashTypeCode.CFRD
										.getCode())
							childCashIn += py1.getCashIn();
						else if ((byte) py1.getPettyCashType() == (byte) PettyCashTypeCode.CGFS
								.getCode())
							totalIssued += py1.getCashOut();
					}
				}
				totalIssued += null != py.getCashOut() ? py.getCashOut() : py
						.getCashIn();
			}
			totalIssued -= childCashIn;
		}
		return totalIssued < (double) voucher.getCashIn() ? false : true;
	}

	private Boolean validateOpenAdvanceIssuedPettyCash(PettyCash voucher,
			PettyCash pettyCash, long pettyCashOldId, double totalCashOut)
			throws Exception {
		List<PettyCash> pettyCashes = pettyCashBL.getPettyCashService()
				.getAllAdvanceIssedPettyCash(voucher.getPettyCashId(),
						PettyCashTypeCode.SFAI.getCode(),
						PettyCashTypeCode.RFPC.getCode(),
						PettyCashTypeCode.CRFS.getCode(), pettyCashOldId);
		if (null != pettyCashes && pettyCashes.size() > 0) {
			for (PettyCash py : pettyCashes) {
				if ((byte) py.getPettyCashType() == (byte) PettyCashTypeCode.SFAI
						.getCode()
						|| (byte) py.getPettyCashType() == (byte) PettyCashTypeCode.RFPC
								.getCode())
					totalCashOut += py.getExpenseAmount();
				else if ((byte) py.getPettyCashType() == (byte) PettyCashTypeCode.CRFS
						.getCode())
					totalCashOut += py.getCashIn();
			}
		}
		return totalCashOut < (double) voucher.getCashOut() ? false : true;
	}

	private List<PettyCashDetail> getDeletedPettyCashDetail(
			List<PettyCashDetail> pettyCashDetails) throws Exception {
		List<PettyCashDetail> existingPettyCashDetails = pettyCashBL
				.getPettyCashService().getPettyCashDetailByPettyId(pettyCashId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, PettyCashDetail> hashPettyCashDetail = new HashMap<Long, PettyCashDetail>();
		if (null != existingPettyCashDetails
				&& existingPettyCashDetails.size() > 0) {
			for (PettyCashDetail list : existingPettyCashDetails) {
				listOne.add(list.getPettyCashDetailId());
				hashPettyCashDetail.put(list.getPettyCashDetailId(), list);
			}
			if (null != hashPettyCashDetail && hashPettyCashDetail.size() > 0) {
				for (PettyCashDetail list : pettyCashDetails) {
					listTwo.add(list.getPettyCashDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<PettyCashDetail> deletedPettyCashDetails = null;
		if (null != different && different.size() > 0) {
			PettyCashDetail tempPettyCashDetail = null;
			deletedPettyCashDetails = new ArrayList<PettyCashDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempPettyCashDetail = new PettyCashDetail();
					tempPettyCashDetail = hashPettyCashDetail.get(list);
					deletedPettyCashDetails.add(tempPettyCashDetail);
				}
			}
		}
		return deletedPettyCashDetails;
	}

	private List<PettyCashDetail> getPettyCashDetail() throws Exception {
		List<PettyCashDetail> pettyCashDetails = new ArrayList<PettyCashDetail>();
		JSONParser parser = new JSONParser();
		Object receiveObject = parser.parse(pettyCashDetailStr);
		JSONArray object = (JSONArray) receiveObject;
		PettyCashDetail pettyCashDetail = null;
		Combination combination = null;
		Person person = null;
		for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
			org.json.simple.JSONObject jsonObjectDetail = (org.json.simple.JSONObject) iterator
					.next();
			pettyCashDetail = new PettyCashDetail();
			if (null != jsonObjectDetail.get("personId")
					&& Long.parseLong(jsonObjectDetail.get("personId")
							.toString()) > 0) {
				person = new Person();
				person.setPersonId(Long.parseLong(jsonObjectDetail.get(
						"personId").toString()));
				pettyCashDetail.setPerson(person);
			} else
				pettyCashDetail.setPayeeName(jsonObjectDetail.get("personName")
						.toString());
			combination = new Combination();
			combination.setCombinationId(Long.parseLong(jsonObjectDetail.get(
					"combinationId").toString()));
			pettyCashDetail.setCombination(combination);

			pettyCashDetail.setExpenseAmount(Double
					.parseDouble(jsonObjectDetail.get("expenseAmount")
							.toString()));
			pettyCashDetail.setDescription(jsonObjectDetail.get("description")
					.toString());
			pettyCashDetail.setBillNo(jsonObjectDetail.get("billNumber")
					.toString());
			pettyCashDetail.setPettyCashDetailId((null != jsonObjectDetail
					.get("pettyCashDetailId") && Long
					.parseLong(jsonObjectDetail.get("pettyCashDetailId")
							.toString()) > 0) ? Long.parseLong(jsonObjectDetail
					.get("pettyCashDetailId").toString()) : null);
			pettyCashDetails.add(pettyCashDetail);
		}
		return pettyCashDetails;
	}

	// delete petty cash
	public String deletePettyCash() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: deletePettyCash");
			PettyCash pettyCash = pettyCashBL.getPettyCashService()
					.getPettyCashById(pettyCashId);
			pettyCashBL.deletePettyCash(pettyCash);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: savePettyCash: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Record delete failure.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deletePettyCash: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// create petty cash map
	private Map<Long, List<PettyCash>> getAdvanceIssuedPettyCash()
			throws Exception {
		List<PettyCash> pettyCashSettlements = pettyCashBL
				.getPettyCashService().getAllAdvanceIssued(
						pettyCashBL.getImplementation());
		Map<Long, List<PettyCash>> hashSetlements = new HashMap<Long, List<PettyCash>>();
		for (PettyCash list : pettyCashSettlements) {
			List<PettyCash> pettyCashMapList = null;
			if (hashSetlements
					.containsKey(list.getPettyCash().getPettyCashId())) {
				pettyCashMapList = hashSetlements.get(list.getPettyCash()
						.getPettyCashId());
				pettyCashMapList.add(list);
			} else {
				pettyCashMapList = new ArrayList<PettyCash>();
				pettyCashMapList.add(list);
			}
			hashSetlements.put(list.getPettyCash().getPettyCashId(),
					pettyCashMapList);
		}
		return hashSetlements;
	}

	// create petty cash map
	private Map<Long, List<PettyCash>> getOnlyAdvanceIssuedPettyCash()
			throws Exception {
		List<PettyCash> pettyCashSettlements = pettyCashBL
				.getPettyCashService().getAdvanceIssuedAndGiven(
						pettyCashBL.getImplementation(),
						PettyCashTypes.Advance_issued.getCode(),
						PettyCashTypes.Cash_given_settlement.getCode());
		Map<Long, List<PettyCash>> hashSetlements = new HashMap<Long, List<PettyCash>>();
		List<PettyCash> pettyCashMapList = null;
		for (PettyCash list : pettyCashSettlements) {
			pettyCashMapList = new ArrayList<PettyCash>();
			if (hashSetlements
					.containsKey(list.getPettyCash().getPettyCashId())) {
				pettyCashMapList = hashSetlements.get(list.getPettyCash()
						.getPettyCashId());
			}
			pettyCashMapList.add(list);
			hashSetlements.put(list.getPettyCash().getPettyCashId(),
					pettyCashMapList);
		}
		return hashSetlements;
	}

	public String updatePettyCashTransaction() {
		try {
			LOGGER.info("Module: Accounts : Method: updatePettyCashTransaction Inside");
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getAllPettyCashOnlyExpense(
							pettyCashBL.getImplementation(),
							PettyCashTypes.Spend.getCode());
			if (null != pettyCashs && pettyCashs.size() > 0)
				pettyCashBL.updatePettyCashTransaction(pettyCashs);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: updatePettyCashTransaction Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: updatePettyCashTransaction Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updatePettyCashExpenseDetail() {
		try {
			LOGGER.info("Module: Accounts : Method: updatePettyCashExpenseDetail Inside");
			List<PettyCash> pettyCashs = pettyCashBL.getPettyCashService()
					.getAllPettyCashOnlyExpense(
							pettyCashBL.getImplementation(),
							PettyCashTypes.Spend.getCode());
			if (null != pettyCashs && pettyCashs.size() > 0) {
				List<PettyCashDetail> pettyCashDetails = new ArrayList<PettyCashDetail>();
				PettyCashDetail pettyCashDetail = null;
				for (PettyCash pettyCash : pettyCashs) {
					pettyCashDetail = new PettyCashDetail();
					pettyCashDetail.setPettyCash(pettyCash);
					pettyCashDetail.setPerson(pettyCash.getPersonByPersonId());
					pettyCashDetail.setPayeeName(pettyCash.getReceiverOther());
					pettyCashDetail.setExpenseAmount(pettyCash
							.getExpenseAmount());
					pettyCashDetail.setDescription(pettyCash.getDescription());
					pettyCashDetail.setCombination(pettyCash.getCombination());
					pettyCashDetail.setBillNo(pettyCash.getInvoiceNumber());
					pettyCashDetail.setSettelmentFlag(pettyCash
							.getSettelmentFlag());
					pettyCashDetail.setAgainstInvoice(pettyCash
							.getAgainstInvoice());
					pettyCashDetails.add(pettyCashDetail);
				}
				pettyCashBL.updatePettyCashDetail(pettyCashDetails);
			}
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: updatePettyCashExpenseDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: updatePettyCashExpenseDetail Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// split values
	private static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public long getPettyCashId() {
		return pettyCashId;
	}

	public void setPettyCashId(long pettyCashId) {
		this.pettyCashId = pettyCashId;
	}

	public String getPettyCashNo() {
		return pettyCashNo;
	}

	public void setPettyCashNo(String pettyCashNo) {
		this.pettyCashNo = pettyCashNo;
	}

	public PettyCashBL getPettyCashBL() {
		return pettyCashBL;
	}

	public void setPettyCashBL(PettyCashBL pettyCashBL) {
		this.pettyCashBL = pettyCashBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getPettyCashType() {
		return pettyCashType;
	}

	public void setPettyCashType(String pettyCashType) {
		this.pettyCashType = pettyCashType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getReceiptRefId() {
		return receiptRefId;
	}

	public void setReceiptRefId(long receiptRefId) {
		this.receiptRefId = receiptRefId;
	}

	public String getReceiptRefType() {
		return receiptRefType;
	}

	public void setReceiptRefType(String receiptRefType) {
		this.receiptRefType = receiptRefType;
	}

	public boolean isSettlementRequired() {
		return settlementRequired;
	}

	public void setSettlementRequired(boolean settlementRequired) {
		this.settlementRequired = settlementRequired;
	}

	public boolean isAgainstInvoice() {
		return againstInvoice;
	}

	public void setAgainstInvoice(boolean againstInvoice) {
		this.againstInvoice = againstInvoice;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getCashIn() {
		return cashIn;
	}

	public void setCashIn(String cashIn) {
		this.cashIn = cashIn;
	}

	public String getCashOut() {
		return cashOut;
	}

	public void setCashOut(String cashOut) {
		this.cashOut = cashOut;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public long getSettlementVoucherId() {
		return settlementVoucherId;
	}

	public void setSettlementVoucherId(long settlementVoucherId) {
		this.settlementVoucherId = settlementVoucherId;
	}

	public long getCarryForwardVoucherId() {
		return carryForwardVoucherId;
	}

	public void setCarryForwardVoucherId(long carryForwardVoucherId) {
		this.carryForwardVoucherId = carryForwardVoucherId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getReceiverOther() {
		return receiverOther;
	}

	public void setReceiverOther(String receiverOther) {
		this.receiverOther = receiverOther;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getPettyCashDetailStr() {
		return pettyCashDetailStr;
	}

	public void setPettyCashDetailStr(String pettyCashDetailStr) {
		this.pettyCashDetailStr = pettyCashDetailStr;
	}
}
