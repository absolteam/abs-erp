package com.aiotech.aios.accounts.action;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetWarranty;
import com.aiotech.aios.accounts.domain.entity.vo.AssetWarrantyVO;
import com.aiotech.aios.accounts.service.bl.AssetWarrantyBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class AssetWarrantyAction extends ActionSupport {

	private static final long serialVersionUID = 8632685722266676160L;

	private static final Logger log = Logger
			.getLogger(AssetWarrantyAction.class);

	// Dependency
	private AssetWarrantyBL assetWarrantyBL;

	// Variables
	private long assetWarrantyId;
	private long assetId;
	private long warrantyType;
	private Long recordId;
	private String startDate;
	private String endDate;
	private String warrantyNumber;
	private String description;
	private String returnMessage;
	private List<Object> aaData;
	private Object assetWarrantyVO;

	// return success
	public String returnSuccess() {
		log.info("Inside Module: Accounts : Method: returnSuccess");
		recordId = null;
		return SUCCESS;
	}

	// Show Asset Warranty
	public String showAssetWarranty() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetWarranty");
			ServletActionContext.getRequest().setAttribute(
					"WARRANTY_TYPE",
					assetWarrantyBL.getLookupMasterBL().getActiveLookupDetails(
							"ASSET_WARRANTY_TYPE", true));
			log.info("Module: Accounts : Method: showAssetWarranty: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showAssetWarranty: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show All Asset Warranty
	public String showAllAssetWarranty() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAssetWarranty");
			aaData = assetWarrantyBL.showAllAssetWarranty();
			log.info("Module: Accounts : Method: showAllAssetWarranty: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showAllAssetWarranty: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAssetWarrantyApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetWarrantyApproval");
			assetWarrantyId = recordId;
			showAssetWarrantyEntry();
			log.info("Module: Accounts : Method: showAssetWarrantyApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetWarrantyApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetWarrantyReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetWarrantyReject");
			assetWarrantyId = recordId;
			showAssetWarrantyEntry();
			log.info("Module: Accounts : Method: showAssetWarrantyReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetWarrantyReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Warranty Screen
	public String showAssetWarrantyEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetWarrantyEntry");
			if (assetWarrantyId > 0) {
				AssetWarrantyVO assetWarrantyVO = (AssetWarrantyVO) assetWarrantyBL
						.getAssetWarrantyById(assetWarrantyId);
				ServletActionContext.getRequest().setAttribute(
						"ASSET_WARRANTY", assetWarrantyVO);
				CommentVO comment = new CommentVO();
				if (recordId != null && assetWarrantyVO != null
						&& assetWarrantyVO.getAssetWarrantyId() != 0
						&& recordId > 0) {
					comment.setRecordId(assetWarrantyVO.getAssetWarrantyId());
					comment.setUseCase(AssetWarranty.class.getName());
					comment = assetWarrantyBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"WARRANTY_TYPE",
					assetWarrantyBL.getLookupMasterBL().getActiveLookupDetails(
							"ASSET_WARRANTY_TYPE", true));
			log.info("Module: Accounts : Method: showAssetWarrantyEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showAssetWarrantyEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Asset Warranty
	public String saveAssetWarranty() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetWarranty");
			assetWarrantyBL.saveAssetWarranty(setAssetWarranty());
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetWarranty: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetWarranty: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Asset Warranty
	public String deleteAssetWarranty() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAssetWarranty");
			assetWarrantyBL.deleteAssetWarranty(assetWarrantyBL
					.getAssetWarrantyService().getSimpleAssetWarrantyById(
							assetWarrantyId));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetWarranty: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: deleteAssetWarranty: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Set Asset Warranty
	private AssetWarranty setAssetWarranty() throws Exception {
		AssetWarranty assetWarranty = new AssetWarranty();
		AssetCreation assetCreation = new AssetCreation();
		assetWarranty.setAssetWarrantyId(assetWarrantyId > 0 ? assetWarrantyId
				: null);
		assetCreation.setAssetCreationId(assetId);
		assetWarranty.setAssetCreation(assetCreation);
		assetWarranty.setWarrantyNumber(warrantyNumber);
		assetWarranty.setStartDate(DateFormat.convertStringToDate(startDate));
		assetWarranty.setEndDate(DateFormat.convertStringToDate(endDate));
		LookupDetail lookupDetail = new LookupDetail();
		lookupDetail.setLookupDetailId(warrantyType);
		assetWarranty.setLookupDetail(lookupDetail);
		assetWarranty.setDescription(description);
		return assetWarranty;
	}

	// Getters & Setters
	public AssetWarrantyBL getAssetWarrantyBL() {
		return assetWarrantyBL;
	}

	public void setAssetWarrantyBL(AssetWarrantyBL assetWarrantyBL) {
		this.assetWarrantyBL = assetWarrantyBL;
	}

	public long getAssetWarrantyId() {
		return assetWarrantyId;
	}

	public void setAssetWarrantyId(long assetWarrantyId) {
		this.assetWarrantyId = assetWarrantyId;
	}

	public long getAssetId() {
		return assetId;
	}

	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}

	public long getWarrantyType() {
		return warrantyType;
	}

	public void setWarrantyType(long warrantyType) {
		this.warrantyType = warrantyType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getWarrantyNumber() {
		return warrantyNumber;
	}

	public void setWarrantyNumber(String warrantyNumber) {
		this.warrantyNumber = warrantyNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Object getAssetWarrantyVO() {
		return assetWarrantyVO;
	}

	public void setAssetWarrantyVO(Object assetWarrantyVO) {
		this.assetWarrantyVO = assetWarrantyVO;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
