package com.aiotech.aios.accounts.action;

import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.service.bl.CreditTermBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class CreditTermAction extends ActionSupport {

	private static final long serialVersionUID = -1989660891472672653L;
	private long creditTermId;
	private String name;
	private String tempName;
	private byte dueDay;
	private byte penaltyType;
	private byte discountMode;
	private byte penaltyMode;
	private String returnMessage;
	private String description;
	private double discount;
	private double discountDay;
	private double penalty;
	private long combinationId;
	private long penaltyCombinationId;
	private Boolean isSupplier;
	private Long recordId;
	private Implementation implementation;

	// Dependency
	private CreditTermBL creditTermBL;

	private static final Logger LOGGER = LogManager
			.getLogger(CreditTermAction.class);

	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		recordId = null;
		return SUCCESS;
	}

	public String showJsonCreditTerm() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonCreditTerm");
			JSONObject jsonList = creditTermBL.getCreditTermList(
					getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonCreditTerm: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonCreditTerm Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showCreditTermApproval(){
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCreditTermApproval");
			creditTermId = recordId;
			showCreditTermEntry();
			LOGGER.info("Module: Accounts : Method: showCreditTermApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCreditTermApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showCreditTermRejectEntry(){
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCreditTermRejectEntry");
			creditTermId = recordId;
			showCreditTermEntry();
			LOGGER.info("Module: Accounts : Method: showCreditTermRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCreditTermRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCreditTermEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCreditTermEntry");
			if (creditTermId > 0) {
				CreditTerm creditTerm = creditTermBL
						.getCreditTermService()
						.getCreditTermDetails(creditTermId);
				ServletActionContext.getRequest().setAttribute(
						"CREDIT_TERM_INFO", creditTerm);
				CommentVO comment = new CommentVO();
				if (recordId != null && creditTerm != null
						&& creditTerm.getCreditTermId() != 0 && recordId > 0) {
					comment.setRecordId(creditTerm.getCreditTermId());
					comment.setUseCase(CreditTerm.class.getName());
					comment = creditTermBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM_BASEDAY", creditTermBL.getDueDate());
			ServletActionContext.getRequest().setAttribute("PENALTY_TYPE",
					creditTermBL.getPenalityType());
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					creditTermBL.getPaymentMode());
			LOGGER.info("Module: Accounts : Method: showCreditTermEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCreditTermEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Credit Term
	public String saveCreditTerm() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveCreditTerm"); 
			CreditTerm creditTerm = new CreditTerm();
			creditTerm.setCreditTermId(creditTermId > 0 ? creditTermId : null);
			creditTerm.setName(name);
			creditTerm.setDiscountDay(discountDay);
			creditTerm.setDiscount(discount > 0 ? discount : null);
			creditTerm.setDueDay(dueDay > 0 ? dueDay : null);
			creditTerm.setDiscountMode(discountMode > 0 ? discountMode : null);
			creditTerm.setPenaltyType(penaltyType > 0 ? penaltyType : null);
			creditTerm.setPenaltyMode(penaltyMode > 0 ? penaltyMode : null);
			creditTerm.setPenalty(penalty > 0 ? penalty : null);
			creditTerm.setIsSupplier(isSupplier);
			creditTerm.setDescription(description);
			creditTerm.setImplementation(getImplementationId());
			creditTermBL.saveCreditTerm(creditTerm);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveCreditTerm: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"FAILURE");
			LOGGER.info("Module: Accounts : Method: saveCreditTerm Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteCreditTerm() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: deleteCreditTerm");
			CreditTerm creditTerm = creditTermBL.getCreditTermService()
					.creditTermById(creditTermId);
			creditTermBL.deleteCreditTerm(creditTerm);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteCreditTerm: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			LOGGER.info("Module: Accounts : Method: deleteCreditTerm Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Credit Term
	public String showCreditTerm() {
		LOGGER.info("Module: Accounts : Method: showCreditTerm");
		try {
			List<CreditTerm> creditTerms = creditTermBL.getCreditTermService()
					.getAllCreditTerms(getImplementationId());
			ServletActionContext.getRequest().setAttribute("CREDIT_TERM_INFO",
					creditTerms);
			LOGGER.info("Module: Accounts : Method: showCreditTerm: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCreditTerm: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	// Getters & Setters
	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public byte getDueDay() {
		return dueDay;
	}

	public void setDueDay(byte dueDay) {
		this.dueDay = dueDay;
	}

	public byte getPenaltyType() {
		return penaltyType;
	}

	public void setPenaltyType(byte penaltyType) {
		this.penaltyType = penaltyType;
	}

	public byte getDiscountMode() {
		return discountMode;
	}

	public void setDiscountMode(byte discountMode) {
		this.discountMode = discountMode;
	}

	public byte getPenaltyMode() {
		return penaltyMode;
	}

	public void setPenaltyMode(byte penaltyMode) {
		this.penaltyMode = penaltyMode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getDiscountDay() {
		return discountDay;
	}

	public void setDiscountDay(double discountDay) {
		this.discountDay = discountDay;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public long getPenaltyCombinationId() {
		return penaltyCombinationId;
	}

	public void setPenaltyCombinationId(long penaltyCombinationId) {
		this.penaltyCombinationId = penaltyCombinationId;
	}

	public Boolean getIsSupplier() {
		return isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public CreditTermBL getCreditTermBL() {
		return creditTermBL;
	}

	public void setCreditTermBL(CreditTermBL creditTermBL) {
		this.creditTermBL = creditTermBL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
