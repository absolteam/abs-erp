package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aiotech.aios.accounts.domain.entity.CommissionMethod;
import com.aiotech.aios.accounts.domain.entity.CommissionPersonCombination;
import com.aiotech.aios.accounts.domain.entity.CommissionRule;
import com.aiotech.aios.accounts.service.bl.CommissionRuleBL;
import com.aiotech.aios.common.to.Constants.Accounts.CalculationMethod;
import com.aiotech.aios.common.to.Constants.Accounts.CommissionBase;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class CommissionRuleAction extends ActionSupport {

	private static final long serialVersionUID = -6828738960608432480L;

	private static final Logger log = Logger
			.getLogger(CommissionRuleAction.class);

	// Dependency
	private CommissionRuleBL commissionRuleBL;

	// Variables
	private long commissionRuleId;
	private Long recordId;
	private int rowId;
	private byte commissionBase;
	private byte calculationMethod;
	private String ruleName;
	private String validFrom;
	private String validTo;
	private String description;
	private String commissionMethods;
	private String creditNote;
	private String debitNote;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess() success");
		recordId = null;
		return SUCCESS;
	}

	// Show all commission rule json data
	public String showCommissionRuleDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showCommissionRuleDetails");
			aaData = commissionRuleBL.showCommissionRuleDetails();
			log.info("Module: Accounts : Method: showCommissionRuleDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCommissionRuleDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showCommissionRuleApproval(){
		try {
			LOG.info("Inside Module: Accounts : Method: showCommissionRuleApproval");
			commissionRuleId = recordId;
			showCommissionRuleEntry();
			LOG.info("Module: Accounts : Method: showCommissionRuleApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCommissionRuleApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showCommissionRuleReject(){
		try {
			LOG.info("Inside Module: Accounts : Method: showCommissionRuleReject");
			commissionRuleId = recordId;
			showCommissionRuleEntry();
			LOG.info("Module: Accounts : Method: showCommissionRuleReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCommissionRuleReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Commission Rule Entry
	public String showCommissionRuleEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showCommissionRuleEntry");
			if (commissionRuleId > 0) {
				CommissionRule commissionRule = commissionRuleBL
						.getCommissionRuleService().getCommissionRuleBase(
								commissionRuleId);
				ServletActionContext.getRequest().setAttribute(
						"COMMISSION_RULE", commissionRule);
				CommentVO comment = new CommentVO();
				if (recordId != null && commissionRule != null
						&& commissionRule.getCommissionRuleId() != 0 && recordId > 0) {
					comment.setRecordId(commissionRule.getCommissionRuleId());
					comment.setUseCase(CommissionRule.class.getName());
					comment = commissionRuleBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			Map<Byte, CommissionBase> commissionBases = new HashMap<Byte, CommissionBase>();
			for (CommissionBase e : EnumSet.allOf(CommissionBase.class)) {
				commissionBases.put(e.getCode(), e);
			}
			Map<Byte, String> calculationMethod = new HashMap<Byte, String>();
			for (CalculationMethod e : EnumSet.allOf(CalculationMethod.class)) {
				calculationMethod.put(e.getCode(), e.name()
						.replaceAll("_", " "));
			}
			ServletActionContext.getRequest().setAttribute("COMMISSION_BASE",
					commissionBases);
			ServletActionContext.getRequest().setAttribute(
					"CALCULATION_METHOD", calculationMethod);
			log.info("Module: Accounts : Method: showCommissionRuleEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCommissionRuleEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Commission Rule
	public String saveCommissionRule() {
		try {
			log.info("Inside Module: Accounts : Method: saveCommissionRule");
			CommissionRule commissionRule = new CommissionRule();
			commissionRule.setRuleName(ruleName);
			commissionRule.setCommissionBase(commissionBase);
			commissionRule.setValidFrom(DateFormat
					.convertStringToDate(validFrom));
			commissionRule.setValidTo(DateFormat.convertStringToDate(validTo));
			commissionRule.setCalculationMethod(calculationMethod);
			commissionRule.setIsCreditNote(Boolean.parseBoolean(creditNote));
			commissionRule.setIsDebitNote(Boolean.parseBoolean(debitNote));
			commissionRule.setDescription(description);

			List<CommissionMethod> commissionDetails = new ArrayList<CommissionMethod>();
			List<CommissionMethod> deletedCommissionDetails = null;

			String[] commissionMethodArray = splitValues(commissionMethods,
					"#@");
			for (String commissionDetail : commissionMethodArray) {
				commissionDetails.add(addCommissionDetail(commissionDetail));
			}

			List<CommissionPersonCombination> commissionPersons = new ArrayList<CommissionPersonCombination>();
			List<CommissionPersonCombination> deleteCommissionPersons = null;

			JSONObject jsonObject = new JSONObject();
			for (Object object : aaData)
				jsonObject.put("personObjects", object);

			try {
				String personObjects = jsonObject.getString("personObjects");
				JSONObject productObject = new JSONObject(personObjects);
				JSONArray recordArray = productObject.getJSONArray("record");
				CommissionPersonCombination commissionPerson = null;
				Person person = null;
				for (int i = 0; i < recordArray.length(); i++) {
					person = new Person();
					commissionPerson = new CommissionPersonCombination();
					JSONObject jsonobj = recordArray.getJSONObject(i);
					person.setPersonId(jsonobj.getLong("personId"));
					commissionPerson.setPerson(person);
					commissionPersons.add(commissionPerson);
				}
			} catch (Exception e) {
				returnMessage = "Commission person combination failure.";
				e.printStackTrace();
				return ERROR;
			}
			if (commissionRuleId > 0) {
				commissionRule.setCommissionRuleId(commissionRuleId);
				deletedCommissionDetails = userDeletedCommissionDetails(
						commissionDetails,
						commissionRuleBL.getCommissionRuleService()
								.getCommissionMethodByCommissionRuleId(
										commissionRuleId));
				deleteCommissionPersons = userDeletedCommissionPersons(
						commissionPersons,
						commissionRuleBL.getCommissionRuleService()
								.getCommissionPersonByCommissionRuleId(
										commissionRuleId));
			}
			commissionRuleBL.saveCommissionRule(commissionRule,
					commissionDetails, deletedCommissionDetails,
					commissionPersons, deleteCommissionPersons);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveCommissionRule: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveCommissionRule: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Commission Rule
	public String deleteCommissionRule() {
		try {
			log.info("Inside Module: Accounts : Method: deleteCommissionRule");
			CommissionRule commissionRule = commissionRuleBL
					.getCommissionRuleService().getCommissionRuleBase(
							commissionRuleId);
			commissionRuleBL.deleteCommissionRule(
					commissionRule,
					new ArrayList<CommissionMethod>(commissionRule
							.getCommissionMethods()),
					new ArrayList<CommissionPersonCombination>(commissionRule
							.getCommissionPersonCombinations()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteCommissionRule: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteCommissionRule: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get User Deleted Commission Details
	private List<CommissionMethod> userDeletedCommissionDetails(
			List<CommissionMethod> commissionDetails,
			List<CommissionMethod> existingCommissionDetails) {

		Collection<Integer> listOne = new ArrayList<Integer>();
		Collection<Integer> listTwo = new ArrayList<Integer>();
		Map<Integer, CommissionMethod> hashCommissionDetails = new HashMap<Integer, CommissionMethod>();
		if (null != existingCommissionDetails
				&& existingCommissionDetails.size() > 0) {
			for (CommissionMethod list : existingCommissionDetails) {
				listOne.add(list.getCommissionMethodId());
				hashCommissionDetails.put(list.getCommissionMethodId(), list);
			}
			for (CommissionMethod list : commissionDetails) {
				listTwo.add(list.getCommissionMethodId());
			}
		}
		Collection<Integer> similar = new HashSet<Integer>(listOne);
		Collection<Integer> different = new HashSet<Integer>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<CommissionMethod> commissionDetailsFinal = new ArrayList<CommissionMethod>();
		if (null != different && different.size() > 0) {
			CommissionMethod tempCommissionDetail = null;

			for (Integer list : different) {
				if (null != list && !list.equals(0)) {
					tempCommissionDetail = new CommissionMethod();
					tempCommissionDetail = hashCommissionDetails.get(list);
					commissionDetailsFinal.add(tempCommissionDetail);
				}
			}
		}
		return commissionDetailsFinal;
	}

	// Get User Deleted Commission Person
	private List<CommissionPersonCombination> userDeletedCommissionPersons(
			List<CommissionPersonCombination> commissionPersons,
			List<CommissionPersonCombination> existingCommissionPersons) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, CommissionPersonCombination> hashCommissionPersons = new HashMap<Long, CommissionPersonCombination>();
		if (null != existingCommissionPersons
				&& existingCommissionPersons.size() > 0) {
			for (CommissionPersonCombination list : existingCommissionPersons) {
				listOne.add(list.getCommissionPersonCombinationId());
				hashCommissionPersons.put(
						list.getCommissionPersonCombinationId(), list);
			}
			for (CommissionPersonCombination list : commissionPersons) {
				listTwo.add(list.getCommissionPersonCombinationId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<CommissionPersonCombination> commissionPersonsFinal = new ArrayList<CommissionPersonCombination>();
		if (null != different && different.size() > 0) {
			CommissionPersonCombination tempCommissionPerson = null;

			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempCommissionPerson = new CommissionPersonCombination();
					tempCommissionPerson = hashCommissionPersons.get(list);
					commissionPersonsFinal.add(tempCommissionPerson);
				}
			}
		}
		return commissionPersonsFinal;
	}

	// Add Commission Methods
	private CommissionMethod addCommissionDetail(String commissionDetail) {
		CommissionMethod commissionMethod = new CommissionMethod();
		String commissionDetails[] = splitValues(commissionDetail, "__");
		commissionMethod.setFlatCommission(null != commissionDetails[0]
				&& !("##").equals(commissionDetails[0].trim()) ? Double
				.parseDouble(commissionDetails[0]) : null);
		commissionMethod.setTargetSale(null != commissionDetails[1]
				&& !("##").equals(commissionDetails[1].trim()) ? Double
				.parseDouble(commissionDetails[1]) : null);
		commissionMethod
				.setIsPercentage(null != commissionDetails[2]
						&& !("##").equals(commissionDetails[2].trim()) ? commissionDetails[2]
						.equals("1") ? true : false : null);
		commissionMethod
				.setDescription(null != commissionDetails[3]
						&& !("##").equals(commissionDetails[3].trim()) ? commissionDetails[3]
						: null);
		commissionMethod.setCommission(null != commissionDetails[4]
				&& !("##").equals(commissionDetails[4].trim()) ? Double
				.parseDouble(commissionDetails[4]) : null);
		commissionMethod.setCommissionMethodId(Integer
				.parseInt(commissionDetails[5]) > 0 ? Integer
				.parseInt(commissionDetails[5]) : null);
		return commissionMethod;
	}

	// Split Array Values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & setters
	public CommissionRuleBL getCommissionRuleBL() {
		return commissionRuleBL;
	}

	public void setCommissionRuleBL(CommissionRuleBL commissionRuleBL) {
		this.commissionRuleBL = commissionRuleBL;
	}

	public long getCommissionRuleId() {
		return commissionRuleId;
	}

	public void setCommissionRuleId(long commissionRuleId) {
		this.commissionRuleId = commissionRuleId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public byte getCommissionBase() {
		return commissionBase;
	}

	public void setCommissionBase(byte commissionBase) {
		this.commissionBase = commissionBase;
	}

	public byte getCalculationMethod() {
		return calculationMethod;
	}

	public void setCalculationMethod(byte calculationMethod) {
		this.calculationMethod = calculationMethod;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCommissionMethods() {
		return commissionMethods;
	}

	public void setCommissionMethods(String commissionMethods) {
		this.commissionMethods = commissionMethods;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getCreditNote() {
		return creditNote;
	}

	public void setCreditNote(String creditNote) {
		this.creditNote = creditNote;
	}

	public String getDebitNote() {
		return debitNote;
	}

	public void setDebitNote(String debitNote) {
		this.debitNote = debitNote;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
