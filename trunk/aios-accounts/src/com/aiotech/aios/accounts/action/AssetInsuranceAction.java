package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.AssetInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetInsurancePremium;
import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.service.bl.AssetInsuranceBL;
import com.aiotech.aios.common.to.Constants.Accounts.AssetInsurancePeriod;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class AssetInsuranceAction extends ActionSupport {

	private static final long serialVersionUID = -4734035676051171905L;

	private static final Logger log = Logger
			.getLogger(AssetInsuranceAction.class);

	// Dependency
	private AssetInsuranceBL assetInsuranceBL;

	// Variables
	private long assetInsuranceId;
	private long assetCreationId;
	private long insuranceType;
	private long policyType;
	private long provider;
	private long cashAccount;
	private long chequeId;
	private Long recordId;
	private int chequeNumber;
	private double premium;
	private byte insurancePeriod;
	private String startDate;
	private String endDate;
	private String policyNumber;
	private String contactName;
	private String contactNumber;
	private String brokerName;
	private String brokerNumber;
	private String description;
	private String returnMessage;
	private String chequeDate;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show all Asset Insurance
	public String showAllAssetInsurance() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAssetInsurance");
			aaData = assetInsuranceBL.showAllAssetInsurance();
			log.info("Module: Accounts : Method: showAllAssetInsurance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllAssetInsurance Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetInsuranceApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetInsuranceApproval");
			assetInsuranceId = recordId;
			showAssetInsuranceEntry();
			log.info("Module: Accounts : Method: showAssetInsuranceApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetInsuranceApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetInsuranceReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetInsuranceReject");
			assetInsuranceId = recordId;
			showAssetInsuranceEntry();
			log.info("Module: Accounts : Method: showAssetInsuranceReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetInsuranceReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Insurance Entry Screen
	public String showAssetInsuranceEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetInsuranceEntry");
			if (assetInsuranceId > 0) {
				AssetInsurance assetInsurance = assetInsuranceBL
						.getAssetInsuranceService()
						.getAssetInsuranceByInsuranceId(assetInsuranceId);
				ServletActionContext.getRequest().setAttribute(
						"ASSET_INSURANCE", assetInsurance);
				CommentVO comment = new CommentVO();
				if (recordId != null && assetInsurance != null
						&& assetInsurance.getAssetInsuranceId() != 0
						&& recordId > 0) {
					comment.setRecordId(assetInsurance.getAssetInsuranceId());
					comment.setUseCase(AssetInsurance.class.getName());
					comment = assetInsuranceBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			Map<Byte, AssetInsurancePeriod> period = new HashMap<Byte, AssetInsurancePeriod>();
			for (AssetInsurancePeriod e : EnumSet
					.allOf(AssetInsurancePeriod.class))
				period.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute(
					"INSURANCE_TYPE",
					assetInsuranceBL.getLookupMasterBL()
							.getActiveLookupDetails("ASSET_INSURANCE_TYPE",
									true));
			ServletActionContext.getRequest().setAttribute(
					"POLICY_TYPE",
					assetInsuranceBL.getLookupMasterBL()
							.getActiveLookupDetails(
									"ASSET_INSURANCE_POLICYTYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"PROVIDER",
					assetInsuranceBL.getLookupMasterBL()
							.getActiveLookupDetails("ASSET_INSURANCE_PROVIDER",
									true));
			ServletActionContext.getRequest().setAttribute("INSURANCE_PERIOD",
					period);
			log.info("Module: Accounts : Method: showAssetInsuranceEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showAssetInsuranceEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Asset Insurance
	public String saveAssetInsurance() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetInsurance");
			List<AssetInsurancePremium> assetInsurancePremiums = null;
			if (assetInsuranceId > 0)
				assetInsurancePremiums = assetInsuranceBL
						.getAssetInsurancePremiumBL()
						.getAssetInsurancePremiumService()
						.getPremiumDueInsuranceId(assetInsuranceId);
			assetInsuranceBL.saveAssetInsurance(setAssetInsurance(),
					assetInsurancePremiums);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetInsurance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetInsurance: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Asset Insurance
	public String deleteAssetInsurance() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetInsurance");
			List<AssetInsurancePremium> assetInsurancePremiums = assetInsuranceBL
					.getAssetInsurancePremiumBL()
					.getAssetInsurancePremiumService()
					.getPremiumDueInsuranceId(assetInsuranceId);
			assetInsuranceBL.deleteAssetInsurance(assetInsuranceBL
					.getAssetInsuranceService()
					.getSimpleAssetInsuranceByInsuranceId(assetInsuranceId),
					assetInsurancePremiums);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetInsurance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetInsurance: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Set Asset Insurance
	private final AssetInsurance setAssetInsurance() throws Exception {
		AssetInsurance assetInsurance = new AssetInsurance();
		assetInsurance
				.setAssetInsuranceId(assetInsuranceId > 0 ? assetInsuranceId
						: null);
		assetInsurance.setAssetCreation(assetInsuranceBL.getAssetCreationBL()
				.getAssetCreationService()
				.getSimpleAssetInformation(assetCreationId));
		LookupDetail lookupDetail = new LookupDetail();
		lookupDetail.setLookupDetailId(insuranceType);
		assetInsurance.setLookupDetailByInsuranceType(lookupDetail);
		lookupDetail = new LookupDetail();
		lookupDetail.setLookupDetailId(policyType);
		assetInsurance.setLookupDetailByPolicyType(lookupDetail);
		lookupDetail = new LookupDetail();
		lookupDetail.setLookupDetailId(provider);
		assetInsurance.setLookupDetailByProvider(lookupDetail);
		assetInsurance.setInsurancePeriod(insurancePeriod);
		assetInsurance.setStartDate(DateFormat.convertStringToDate(startDate));
		assetInsurance.setEndDate(DateFormat.convertStringToDate(endDate));
		assetInsurance.setPolicyNumber(policyNumber);
		assetInsurance.setPremium(premium);
		assetInsurance.setContactPerson(contactName);
		assetInsurance.setContactNumber(contactNumber);
		assetInsurance.setBrokerName(brokerName);
		assetInsurance.setBrokerNumber(brokerNumber);
		assetInsurance.setDescription(description);
		if (chequeId > 0) {
			ChequeBook chequeBook = assetInsuranceBL.getChequeBookBL()
					.getChequeBookService().findChequeBookById(chequeId); 
			assetInsurance.setChequeNumber(assetInsuranceId > 0 ? chequeBook
					.getCurrentNo() : null);
			assetInsurance.setChequeDate(DateFormat
					.convertStringToDate(chequeDate));
			assetInsurance.setChequeBook(chequeBook);
		} else {
			Combination combination = new Combination();
			combination.setCombinationId(cashAccount);
			assetInsurance.setCombination(combination);
		}
		return assetInsurance;
	}

	// Getters & Setters
	public AssetInsuranceBL getAssetInsuranceBL() {
		return assetInsuranceBL;
	}

	public void setAssetInsuranceBL(AssetInsuranceBL assetInsuranceBL) {
		this.assetInsuranceBL = assetInsuranceBL;
	}

	public long getAssetInsuranceId() {
		return assetInsuranceId;
	}

	public void setAssetInsuranceId(long assetInsuranceId) {
		this.assetInsuranceId = assetInsuranceId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public long getAssetCreationId() {
		return assetCreationId;
	}

	public void setAssetCreationId(long assetCreationId) {
		this.assetCreationId = assetCreationId;
	}

	public long getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(long insuranceType) {
		this.insuranceType = insuranceType;
	}

	public long getPolicyType() {
		return policyType;
	}

	public void setPolicyType(long policyType) {
		this.policyType = policyType;
	}

	public long getProvider() {
		return provider;
	}

	public void setProvider(long provider) {
		this.provider = provider;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public byte getInsurancePeriod() {
		return insurancePeriod;
	}

	public void setInsurancePeriod(byte insurancePeriod) {
		this.insurancePeriod = insurancePeriod;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getBrokerName() {
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}

	public String getBrokerNumber() {
		return brokerNumber;
	}

	public void setBrokerNumber(String brokerNumber) {
		this.brokerNumber = brokerNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCashAccount() {
		return cashAccount;
	}

	public void setCashAccount(long cashAccount) {
		this.cashAccount = cashAccount;
	}

	public long getChequeId() {
		return chequeId;
	}

	public void setChequeId(long chequeId) {
		this.chequeId = chequeId;
	}

	public int getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(int chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
