/**
 * Bank Reconciliation Adjustment
 * Initial : 20-08-2013
 */
package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustment;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustmentDetail;
import com.aiotech.aios.accounts.service.bl.ReconciliationAdjustmentBL;
import com.aiotech.aios.common.to.Constants.Accounts.ReconciliationAdjustmentType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Bank Reconciliation Adjustment
 * 
 * @author Saleem
 */
public class ReconciliationAdjustmentAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(ReconciliationAdjustmentAction.class);

	// Dependency
	private ReconciliationAdjustmentBL reconciliationAdjustmentBL;

	// Variables
	private long adjustmentId;
	private String referenceNumber;
	private String date;
	private String description;
	private String returnMessage;
	private long bankAccountId;
	private String adjustmentDetail;
	private int rowId;
	private Long recordId;

	public String returnSuccess() {
		LOG.info("Inside Method:returnSuccess()");
		return SUCCESS;
	}

	// Reconciliation Adjustment JSON List
	public String showReconciliationAdjustmentList() {

		try {
			LOG.info("Inside Method : showReconciliationAdjustmentList()");

			JSONObject jsonObject = reconciliationAdjustmentBL
					.getAllReconciliationAdjustments();

			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonObject);

			LOG.info("Module: Accounts : Method: showReconciliationAdjustmentList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Method : showReconciliationAdjustmentList() throws Exception "
					+ ex);
			return ERROR;
		}

	}

	// Show Reconciliation Adjustment ADD/EDIT entry
	public String showReconciliationAdjustmentEntry() {
		try {
			LOG.info("Inside Method : showReconciliationAdjustment()");
			if (recordId != null && recordId > 0) {
				adjustmentId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (adjustmentId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = reconciliationAdjustmentBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			ReconciliationAdjustment reconciliationAdjustment = null;
			referenceNumber = null;
			if (adjustmentId > 0)
				reconciliationAdjustment = reconciliationAdjustmentBL
						.getReconciliationAdjustmentService()
						.getReconciliationAdjustmentById(adjustmentId);
			else
				referenceNumber = generateReferenceNumber();
			Map<Byte, ReconciliationAdjustmentType> adjustmentTypes = new HashMap<Byte, ReconciliationAdjustmentType>();
			for (ReconciliationAdjustmentType adType : EnumSet
					.allOf(ReconciliationAdjustmentType.class))
				adjustmentTypes.put(adType.getCode(), adType);
			ServletActionContext.getRequest().setAttribute("ADJUSTMENT_TYPES",
					adjustmentTypes);
			ServletActionContext.getRequest().setAttribute("ADJUSTMENT_INFO",
					reconciliationAdjustment);
			return SUCCESS;
		} catch (Exception ex) {
			LOG.error("Method : showReconciliationAdjustment() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveReconciliationAdjustmentpopup() {
		try {
			LOG.info("Inside Method : saveReconciliationAdjustmentpopup()");
			this.saveReconciliationAdjustment(); 
			LOG.info("Module: Accounts : Method: saveReconciliationAdjustment(): Action Success");
			return SUCCESS;
		} catch (Exception ex) { 
			LOG.error("Method : saveReconciliationAdjustmentpopup() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Reconciliation Adjustment
	public String saveReconciliationAdjustment() {
		try {
			LOG.info("Inside Method : saveReconciliationAdjustment()");
			// Validate Period
			boolean openPeriod = reconciliationAdjustmentBL.getTransactionBL()
					.getCalendarBL().transactionPostingPeriod(date);
			if (!openPeriod) {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE",
						"Period not open for the Reconcilation Adj date.");
				LOG.info("Module: Accounts : Method: saveReconciliationAdjustment: (Error : Period not opened)");
				return SUCCESS;
			}
			ReconciliationAdjustment reconciliationAdjustment = new ReconciliationAdjustment();
			reconciliationAdjustment.setDate(DateFormat
					.convertStringToDate(date));
			BankAccount bankAccount = new BankAccount();
			bankAccount.setBankAccountId(bankAccountId);
			reconciliationAdjustment.setBankAccount(bankAccount);
			reconciliationAdjustment.setDescription(description);
			String[] adjustmentDetails = splitValues(adjustmentDetail, "@#");
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails = new ArrayList<ReconciliationAdjustmentDetail>();
			for (String details : adjustmentDetails) {
				reconciliationAdjustmentDetails
						.add(setReconciliationAdjustmentDetail(details));
			}
			List<ReconciliationAdjustmentDetail> deletedAdjustments = null;
			List<ReconciliationAdjustmentDetail> reverseAdjustmentDetails = null;
			if (adjustmentId > 0) {
				reconciliationAdjustment.setReferenceNumber(referenceNumber);
				reconciliationAdjustment
						.setReconciliationAdjustmentId(adjustmentId);
				reverseAdjustmentDetails = reconciliationAdjustmentBL
						.getReconciliationAdjustmentService()
						.getReconciliationAdjustmentDetails(adjustmentId);
				deletedAdjustments = userDeletedAdjustmentDetails(
						reconciliationAdjustmentDetails,
						reverseAdjustmentDetails);

			} else
				reconciliationAdjustment
						.setReferenceNumber(generateReferenceNumber());

			reconciliationAdjustmentBL.saveReconciliationAdjustment(
					reconciliationAdjustment, reconciliationAdjustmentDetails,
					deletedAdjustments, reverseAdjustmentDetails);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			returnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: saveReconciliationAdjustment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Record save failure.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.error("Method : saveReconciliationAdjustment() throws Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Reconciliation Adjustment
	public String deleteReconciliationAdjustment() {
		try {
			LOG.info("Inside Method : saveReconciliationAdjustment()");
			ReconciliationAdjustment reconciliationAdjustment = reconciliationAdjustmentBL
					.getReconciliationAdjustmentService()
					.getReconciliationAdjustment(adjustmentId);
			reconciliationAdjustmentBL.deleteReconciliationAdjustment(
					reconciliationAdjustment,
					new ArrayList<ReconciliationAdjustmentDetail>(
							reconciliationAdjustment
									.getReconciliationAdjustmentDetails()));
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: saveReconciliationAdjustment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.error("Method : deleteReconciliationAdjustment() throws Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Set Details
	private static final ReconciliationAdjustmentDetail setReconciliationAdjustmentDetail(
			String details) throws Exception {
		String[] adjustmentDetails = splitValues(details, "__");
		ReconciliationAdjustmentDetail reconciliationAdjustmentDetail = new ReconciliationAdjustmentDetail();
		Combination combination = new Combination();
		combination.setCombinationId(Long.valueOf(adjustmentDetails[1]));
		reconciliationAdjustmentDetail.setAdjustmentType(Byte
				.valueOf(adjustmentDetails[0]));
		reconciliationAdjustmentDetail.setCombination(combination);
		reconciliationAdjustmentDetail.setAmount(Double
				.parseDouble(adjustmentDetails[2]));
		if (!("##").equals(adjustmentDetails[3]))
			reconciliationAdjustmentDetail.setDescription(adjustmentDetails[3]);
		if (Long.parseLong(adjustmentDetails[4]) > 0)
			reconciliationAdjustmentDetail
					.setReconciliationAdjustmentDetailId(Long
							.parseLong(adjustmentDetails[4]));
		return reconciliationAdjustmentDetail;
	}

	// Get Deleted values
	private final List<ReconciliationAdjustmentDetail> userDeletedAdjustmentDetails(
			List<ReconciliationAdjustmentDetail> reconciliationAdjustmentDetails,
			List<ReconciliationAdjustmentDetail> adjustmentDetailDBList)
			throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ReconciliationAdjustmentDetail> hashAdjustmentDetail = new HashMap<Long, ReconciliationAdjustmentDetail>();
		if (null != adjustmentDetailDBList && adjustmentDetailDBList.size() > 0) {
			for (ReconciliationAdjustmentDetail list : adjustmentDetailDBList) {
				listOne.add(list.getReconciliationAdjustmentDetailId());
				hashAdjustmentDetail.put(
						list.getReconciliationAdjustmentDetailId(), list);
			}
			if (null != hashAdjustmentDetail && hashAdjustmentDetail.size() > 0) {
				for (ReconciliationAdjustmentDetail list : reconciliationAdjustmentDetails) {
					listTwo.add(list.getReconciliationAdjustmentDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ReconciliationAdjustmentDetail> adjustmentDetailList = new ArrayList<ReconciliationAdjustmentDetail>();
		if (null != different && different.size() > 0) {
			ReconciliationAdjustmentDetail tempAdjustmentDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempAdjustmentDetail = new ReconciliationAdjustmentDetail();
					tempAdjustmentDetail = hashAdjustmentDetail.get(list);
					adjustmentDetailList.add(tempAdjustmentDetail);
				}
			}
		}
		return adjustmentDetailList;
	}

	// Generate reference number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(
				ReconciliationAdjustment.class.getName(),
				reconciliationAdjustmentBL.getImplementation());
	}

	// Add Row
	public String showReconciliationAdjustmentAddRow() {
		try {
			LOG.info("Inside Method: showReconciliationAdjustmentAddRow");
			Map<Byte, ReconciliationAdjustmentType> adjustmentTypes = new HashMap<Byte, ReconciliationAdjustmentType>();
			for (ReconciliationAdjustmentType adType : EnumSet
					.allOf(ReconciliationAdjustmentType.class))
				adjustmentTypes.put(adType.getCode(), adType);
			ServletActionContext.getRequest().setAttribute("ADJUSTMENT_TYPES",
					adjustmentTypes);
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Method: showReconciliationAdjustmentAddRow Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Method: showReconciliationAdjustmentAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters and Setters
	public ReconciliationAdjustmentBL getReconciliationAdjustmentBL() {
		return reconciliationAdjustmentBL;
	}

	public void setReconciliationAdjustmentBL(
			ReconciliationAdjustmentBL reconciliationAdjustmentBL) {
		this.reconciliationAdjustmentBL = reconciliationAdjustmentBL;
	}

	public long getAdjustmentId() {
		return adjustmentId;
	}

	public void setAdjustmentId(long adjustmentId) {
		this.adjustmentId = adjustmentId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdjustmentDetail() {
		return adjustmentDetail;
	}

	public void setAdjustmentDetail(String adjustmentDetail) {
		this.adjustmentDetail = adjustmentDetail;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

}
