package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.PaymentTerm;
import com.aiotech.aios.accounts.service.bl.PaymentTermBL;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentTermBaseDay;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

public class PaymentTermAction extends ActionSupport{

	private static final long serialVersionUID = 1L;
	
	private PaymentTermBL paymentTermBL;
	private long paymentTermId;
	private String name;
	private int noOfDays;
	private int discountDays;
	private double discount;
	private long discountReceived;
	private String returnMessage;
	private byte baseDay;
	private static final Logger LOGGER =LogManager.getLogger(PaymentTermAction.class); 
	
	public String returnSuccess(){ 
		LOGGER.info("Module: Accounts : Method: returnSuccess"); 
		return SUCCESS; 
	}
	
	public String showPaymentAddEntry(){
		LOGGER.info("Module: Accounts : Method: showPaymentAddEntry"); 
		try {
			Map<Byte,PaymentTermBaseDay> baseDate=new HashMap<Byte,PaymentTermBaseDay>(); 
			for (PaymentTermBaseDay e : EnumSet.allOf(PaymentTermBaseDay.class)){
				baseDate.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("PAYMENT_TERM_BASE_DATE", baseDate);  
			LOGGER.info("Module: Accounts : Method: showPaymentAddEntry: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentAddEntry: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showJsonPaymentTermList(){
		LOGGER.info("Module: Accounts : Method: showJsonPaymentTermList"); 
		try {
			JSONObject jsonList=this.getPaymentTermBL().getAllPayment(getImplementationId()); 
			ServletActionContext.getRequest().setAttribute("jsonlist", jsonList);  
			LOGGER.info("Module: Accounts : Method: showJsonPaymentTermList: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showJsonPaymentTermList: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String paymentTermUpdate(){
		LOGGER.info("Module: Accounts : Method: paymentTermUpdate");
		try {
			PaymentTerm term=this.getPaymentTermBL().getPaymentTermService().getPaymentTermById(paymentTermId);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TERM_INFO", term); 
			Map<Byte,PaymentTermBaseDay> baseDate=new HashMap<Byte,PaymentTermBaseDay>(); 
			for (PaymentTermBaseDay e : EnumSet.allOf(PaymentTermBaseDay.class)){
				baseDate.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("PAYMENT_TERM_BASE_DATE", baseDate);  
			LOGGER.info("Module: Accounts : Method: paymentTermUpdate: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: paymentTermUpdate: throws Exception "+ex);  
			return ERROR;
		}
	} 
	
	public String showPaymentTerm(){
		LOGGER.info("Module: Accounts : Method: showPaymentTerm");
		try {
			List<PaymentTerm> termList=this.getPaymentTermBL().getPaymentTermService().getAllPaymentTerms(getImplementationId());
			ServletActionContext.getRequest().setAttribute("PAYMENT_TERM_INFO", termList); 
			LOGGER.info("Module: Accounts : Method: showPaymentTerm: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentTerm: throws Exception "+ex);  
			return ERROR;
		}
	} 
	
	public String savePaymentTerm(){
		LOGGER.info("Module: Accounts : Method: savePaymentTerm");
		try {
			PaymentTerm term=new PaymentTerm();
			if(paymentTermId>0)
				term.setPaymentTermId(paymentTermId);
			term.setName(name);
			term.setDiscount(discount);
			term.setNoOfDays(noOfDays);
			term.setDiscountDays(discountDays);
			term.setBaseDay(baseDay);
 			Combination combination=new Combination();
			combination.setCombinationId(discountReceived);
 			this.getPaymentTermBL().getPaymentTermService().savePaymentTerm(term);
			returnMessage="SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: savePaymentTerm: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage="FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: savePaymentTerm: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String deletePaymentTerm(){
		LOGGER.info("Module: Accounts : Method: deletePaymentTerm");
		try {
			PaymentTerm term=this.getPaymentTermBL().getPaymentTermService().getPaymentTerm(paymentTermId);
			this.getPaymentTermBL().getPaymentTermService().deletePaymentTerm(term);
			returnMessage="SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: savePaymentTerm: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage="FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: deletePaymentTerm: throws Exception "+ex);  
			return ERROR;
		}
		
	}
	
	public Implementation getImplementationId(){
		Implementation implementation=new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if(session.getAttribute("THIS")!=null){
			  implementation=(Implementation)session.getAttribute("THIS"); 
		}
		return implementation;
	}

	public PaymentTermBL getPaymentTermBL() {
		return paymentTermBL;
	}

	public void setPaymentTermBL(PaymentTermBL paymentTermBL) {
		this.paymentTermBL = paymentTermBL;
	}

	public long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public int getDiscountDays() {
		return discountDays;
	}

	public void setDiscountDays(int discountDays) {
		this.discountDays = discountDays;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public long getDiscountReceived() {
		return discountReceived;
	}

	public void setDiscountReceived(long discountReceived) {
		this.discountReceived = discountReceived;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public byte getBaseDay() {
		return baseDay;
	}

	public void setBaseDay(byte baseDay) {
		this.baseDay = baseDay;
	}

}
