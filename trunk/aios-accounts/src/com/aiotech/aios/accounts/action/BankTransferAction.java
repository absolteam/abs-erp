/**
 * Bank Transfer Initial Development 
 * Creation Date 27-March-2013
 */
package com.aiotech.aios.accounts.action;

import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.service.bl.BankTransferBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class BankTransferAction extends ActionSupport {

	private static final long serialVersionUID = 874027278179558109L;

	private long bankTransferId;
	private String transferNo;
	private String transferDate;
	private Long currency;
	private long transNature;
	private String description;
	private long debitBankAccount;
	private long creditBankAccount;
	private long debitCombinationId;
	private long creditCombinationId;
	private double transferAmount;
	private double transferCharges;
	private long combinationId;
	private long alertId;
	private String returnMessage;
	private String debitAccountNumber;
	private String creditAccountNumber;
	private BankTransferBL bankTransferBL;
	private Long recordId;

	private static final Logger LOGGER = LogManager
			.getLogger(BankTransferAction.class);

	// return success
	public String returnSuccess() {
		LOGGER.info("Inside Method returnSuccess");
		return SUCCESS;
	}

	// List all bank transfer in json format
	public String showJsonBankTransfer() {
		LOGGER.info("Inside Module: Accounts : Method: showJsonBankTransfer");
		try {
			JSONObject jsonList = bankTransferBL.getAllBankTransfer();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonBankTransfer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonBankTransfer Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show bank transfer add/edit
	public String showBankTransferEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showBankTransferEntry");
		try {

			if (recordId != null && recordId > 0) {
				bankTransferId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (bankTransferId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = bankTransferBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			transferNo = null;
			List<Currency> currencyList = bankTransferBL.getTransactionBL()
					.getCurrencyService()
					.getAllCurrency(bankTransferBL.getImplementation());
			List<LookupDetail> lookupDetails = bankTransferBL
					.getLookupMasterBL().getActiveLookupDetails(
							"INTER_BANK_TRANSFER_NATURE", true);
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("TRANSFER_NATURE",
					lookupDetails);
			if (bankTransferId > 0) {
				BankTransfer bankTransfer = bankTransferBL
						.getBankTransferService().findBankTransferById(
								bankTransferId);
				ServletActionContext.getRequest().setAttribute(
						"BANK_TRANSFER_INFO", bankTransfer);
			} else
				transferNo = SystemBL.getReferenceStamp(
						BankTransfer.class.getName(),
						bankTransferBL.getImplementation());
			for (Currency currency : currencyList) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency);
					break;
				}
			}
			LOGGER.info("Module: Accounts : Method: showBankTransferEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankTransferEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// to save or update bank transfer
	public String saveBankTransfer() {
		LOGGER.info("Inside Module: Accounts : Method: saveBankTransfer");
		long documentRecId = -1;
		HttpSession session = ServletActionContext.getRequest().getSession();
		try {
			// Validate Period
			boolean openPeriod = bankTransferBL.getTransactionBL()
					.getCalendarBL().transactionPostingPeriod(transferDate);

			if (!openPeriod) {
				returnMessage = "Period not open for the transfer date.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveBankTransfer: (Error : Period not opened)");
				return SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {

			if (creditBankAccount != debitBankAccount) {
				BankTransfer bankTransfer = new BankTransfer();
				BankTransfer revertBankTransfer = null;
				bankTransfer.setTransferNo(transferNo);
				bankTransfer.setDate(DateFormat
						.convertStringToDate(transferDate));
				bankTransfer.setDescription(description);
				bankTransfer.setTransferAmount(Double.parseDouble(AIOSCommons
						.formatAmount(transferAmount).toString()
						.replaceAll("\\,", "")));
				if (transferCharges > 0) {
					bankTransfer.setTransferCharges(Double
							.parseDouble(AIOSCommons
									.formatAmount(transferCharges).toString()
									.replaceAll("\\,", "")));
					Combination combination = new Combination();
					combination.setCombinationId(combinationId);
					bankTransfer.setCombination(combination);
				}
				BankAccount debitAccount = new BankAccount();
				debitAccount.setBankAccountId(debitBankAccount);
				debitAccount.setAccountNumber(debitAccountNumber);
				Combination debitCombination = new Combination();
				debitCombination.setCombinationId(debitCombinationId);
				debitAccount.setCombination(debitCombination);
				bankTransfer.setBankAccountByDebitBankAccount(debitAccount);
				BankAccount creditAccount = new BankAccount();
				creditAccount.setBankAccountId(creditBankAccount);
				creditAccount.setAccountNumber(creditAccountNumber);
				Combination creditCombination = new Combination();
				creditCombination.setCombinationId(creditCombinationId);
				creditAccount.setCombination(creditCombination);
				bankTransfer.setBankAccountByCreditBankAccount(creditAccount);

				if (transNature > 0) {
					LookupDetail lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(transNature);
					bankTransfer.setLookupDetail(lookupDetail);
				}
				bankTransfer.setImplementation(bankTransferBL
						.getImplementation());
				if (bankTransferId > 0) {
					bankTransfer.setBankTransferId(bankTransferId);
					revertBankTransfer = bankTransferBL
							.getBankTransferService().findBankTransferById(
									bankTransferId);
					bankTransfer.setTransferNo(revertBankTransfer
							.getTransferNo());
					bankTransfer.setCurrency(revertBankTransfer.getCurrency());
					bankTransfer.setExchangeRate(revertBankTransfer.getExchangeRate());
					documentRecId = bankTransferId;
				} else
					bankTransfer.setTransferNo(SystemBL.getReferenceStamp(
							BankTransfer.class.getName(),
							bankTransferBL.getImplementation()));
				if (bankTransferId == 0
						|| (null != revertBankTransfer && (long) revertBankTransfer
								.getCurrency().getCurrencyId() != this.currency)) {
					Currency defaultCurrency = bankTransferBL
							.getTransactionBL().getCurrency();
					if (null != defaultCurrency) {
						if ((long) defaultCurrency.getCurrencyId() != this.currency) {
							Currency currency = bankTransferBL
									.getTransactionBL().getCurrencyService()
									.getCurrency(this.currency);
							CurrencyConversion currencyConversion = bankTransferBL
									.getTransactionBL()
									.getCurrencyConversionBL()
									.getCurrencyConversionService()
									.getCurrencyConversionByCurrency(
											this.currency);
							bankTransfer.setCurrency(currency);
							bankTransfer
									.setExchangeRate(null != currencyConversion ? currencyConversion
											.getConversionRate() : 1.0);
						} else {
							bankTransfer.setCurrency(defaultCurrency);
							bankTransfer.setExchangeRate(1.0);
						}
					}
				}
				bankTransferBL.saveBankTransfer(bankTransfer);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveBankTransfer: Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Please check the Account Numbers, looks both Debit & Credit Accounts are same.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveBankTransfer: Action Success");
				return ERROR;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveBankTransfer: throws Exception "
					+ ex);
			return ERROR;
		} finally {
			AIOSCommons.removeUploaderSession(session, "transferDocs",
					documentRecId, "BankTransfer");
		}
	}

	// delete bank transfer
	public String deleteBankTransfer() {
		LOGGER.info("Inside Module: Accounts : Method: deleteBankTransfer");
		try {
			BankTransfer revertBankTransfer = bankTransferBL
					.getBankTransferService().findBankTransferById(
							bankTransferId);
			bankTransferBL.deleteBankTransfer(revertBankTransfer);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveBankTransfer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteBankTransfer: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// getters & setters
	public long getBankTransferId() {
		return bankTransferId;
	}

	public void setBankTransferId(long bankTransferId) {
		this.bankTransferId = bankTransferId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public BankTransferBL getBankTransferBL() {
		return bankTransferBL;
	}

	public void setBankTransferBL(BankTransferBL bankTransferBL) {
		this.bankTransferBL = bankTransferBL;
	}

	public String getTransferNo() {
		return transferNo;
	}

	public void setTransferNo(String transferNo) {
		this.transferNo = transferNo;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public Long getCurrency() {
		return currency;
	}

	public void setCurrency(Long currency) {
		this.currency = currency;
	}

	public long getTransNature() {
		return transNature;
	}

	public void setTransNature(long transNature) {
		this.transNature = transNature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getDebitBankAccount() {
		return debitBankAccount;
	}

	public void setDebitBankAccount(long debitBankAccount) {
		this.debitBankAccount = debitBankAccount;
	}

	public long getCreditBankAccount() {
		return creditBankAccount;
	}

	public void setCreditBankAccount(long creditBankAccount) {
		this.creditBankAccount = creditBankAccount;
	}

	public double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}

	public double getTransferCharges() {
		return transferCharges;
	}

	public void setTransferCharges(double transferCharges) {
		this.transferCharges = transferCharges;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getDebitAccountNumber() {
		return debitAccountNumber;
	}

	public void setDebitAccountNumber(String debitAccountNumber) {
		this.debitAccountNumber = debitAccountNumber;
	}

	public String getCreditAccountNumber() {
		return creditAccountNumber;
	}

	public void setCreditAccountNumber(String creditAccountNumber) {
		this.creditAccountNumber = creditAccountNumber;
	}

	public long getDebitCombinationId() {
		return debitCombinationId;
	}

	public void setDebitCombinationId(long debitCombinationId) {
		this.debitCombinationId = debitCombinationId;
	}

	public long getCreditCombinationId() {
		return creditCombinationId;
	}

	public void setCreditCombinationId(long creditCombinationId) {
		this.creditCombinationId = creditCombinationId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
