package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.accounts.domain.entity.vo.DirectPaymentVO;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceVO;
import com.aiotech.aios.accounts.domain.entity.vo.PaymentVO;
import com.aiotech.aios.accounts.service.bl.PaymentBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PaymentAction extends ActionSupport {

	private static final long serialVersionUID = -2115218128116425907L;
	private PaymentBL paymentBL;
	private String paymentNumber;
	private String paymentDate;
	private long paymentId;
	private String paymentDetails;
	private String returnMessage;
	private long loanId;
	private long recordId;
	private Integer currencyId;
	private long bankAccountId;
	private String description;
	private byte paymentMode;
	private long cashCombinationId;
	private double totalAmount;
	private double discountReceived;
	private double netAmount;
	private Integer chequeNumber;
	private String chequeDate;
	private String chequeFlag;
	private long alertId;
	private long transferAccountId;
	private long cardAccountId;
	private long chequeBookId;
	private static final Logger LOGGER = LogManager
			.getLogger(PaymentAction.class);

	public String returnSuccess() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		session.removeAttribute("INVOICE_SESSION_INFO_"
				+ sessionObj.get("jSessionId"));
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showJsonPaymentList() {
		LOGGER.info("Inside Module: Accounts : Method: showJsonPaymentList");
		try {
			JSONObject jsonList = paymentBL
					.getPaymentList(getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonPaymentList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showJsonPaymentList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPaymentApproved() {
		LOGGER.info("Inside Module: Accounts : Method: showPaymentApproved");
		try {
			JSONObject jsonList = paymentBL
					.getPaymentList(getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showPaymentApproved: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentApproved: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPaymentEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showPaymentEntry");
		try {
			paymentNumber = getPaymentNumber();

			Payment payment = null;
			if (paymentId > 0)
				payment = paymentBL.getPaymentService()
						.getPurchasePaymentDetails(paymentId);
			ServletActionContext.getRequest().setAttribute("PAYMENT_INFO",
					payment);
			ServletActionContext.getRequest().setAttribute("PAYMENT_NUMBER",
					paymentNumber);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					paymentBL.getModeOfPayment());
			LOGGER.info("Module: Accounts : Method: showPaymentEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPaymentPrint() {
		LOGGER.info("Inside Module: Accounts : Method: showPaymentPrint");
		try {
			if (paymentId > 0) {
				HttpSession session = ServletActionContext.getRequest()
						.getSession();
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				// Removing Sessions
				session.removeAttribute("PAYMENT_PRINT_"
						+ sessionObj.get("jSessionId"));
				Payment purchasePayment = paymentBL.getPaymentService()
						.getPurchasePaymentDetails(paymentId);
				Map<Long, Invoice> invoiceMap = new HashMap<Long, Invoice>();
				for (PaymentDetail paymentDetail : purchasePayment
						.getPaymentDetails()) {
					invoiceMap.put(paymentDetail.getInvoiceDetail()
							.getInvoice().getInvoiceId(), paymentDetail
							.getInvoiceDetail().getInvoice());
				}
				List<InvoiceVO> invoiceVOs = new ArrayList<InvoiceVO>();
				InvoiceVO invoiceVO = null;
				for (Invoice invoice : invoiceMap.values()) {
					double totalInvoiceQty = 0;
					double totalInvoiceAmount = 0;
					invoiceVO = new InvoiceVO();
					invoiceVO.setInvoiceNumber(invoice.getInvoiceNumber());
					invoiceVO.setDescription(invoice.getDescription());
					for (InvoiceDetail invoiceDetail : invoice
							.getInvoiceDetails()) {
						totalInvoiceQty += invoiceDetail.getInvoiceQty();
						totalInvoiceAmount += (invoiceDetail.getReceiveDetail()
								.getUnitRate() * invoiceDetail.getInvoiceQty());
					}
					invoiceVO.setTotalInvoiceQty(totalInvoiceQty);
					invoiceVO.setTotalInvoiceAmount(totalInvoiceAmount);
					invoiceVOs.add(invoiceVO);
				}
				ServletActionContext.getRequest().setAttribute(
						"PAYMENT_INFO_PRINT", purchasePayment);
				ServletActionContext.getRequest().setAttribute(
						"PAYMENT_INVOICE_INFO_PRINT", invoiceVOs);
				session.setAttribute(
						"PAYMENT_PRINT_" + sessionObj.get("jSessionId"),
						purchasePayment);
			}
			LOGGER.info("Module: Accounts : Method: showPaymentPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPurchasePaymentAlert() {
		LOGGER.info("Inside Module: Accounts : Method: showPurchasePaymentAlert");
		try {
			Payment purchasePayment = paymentBL.getPaymentService()
					.getPurchasePaymentDetails(recordId);
			PaymentVO paymentVO = new PaymentVO();
			BeanUtils.copyProperties(paymentVO, purchasePayment);
			paymentVO.setPaymentAccess(ModeOfPayment
					.get(paymentVO.getPaymentMode()).toString()
					.replaceAll("_", " "));
			if ((byte) paymentVO.getPaymentMode() == (byte) ModeOfPayment.Cash
					.getCode())
				paymentVO.setPaymentCode("CSH");
			else if ((byte) paymentVO.getPaymentMode() == (byte) ModeOfPayment.Wire_Transfer
					.getCode())
				paymentVO.setPaymentCode("WRT");
			else if ((byte) paymentVO.getPaymentMode() == (byte) ModeOfPayment.Cheque
					.getCode())
				paymentVO.setPaymentCode("CHQ");
			else if ((byte) paymentVO.getPaymentMode() == (byte) ModeOfPayment.Debit_Card
					.getCode())
				paymentVO.setPaymentCode("DCD");
			else if ((byte) paymentVO.getPaymentMode() == (byte) ModeOfPayment.Credit_Card
					.getCode())
				paymentVO.setPaymentCode("CCD");
			else if ((byte) paymentVO.getPaymentMode() == (byte) ModeOfPayment.Inter_Transfer
					.getCode())
				paymentVO.setPaymentCode("IAT");
			List<InvoiceDetail> invoiceDetails = new ArrayList<InvoiceDetail>();
			for (PaymentDetail detail : purchasePayment.getPaymentDetails()) {
				invoiceDetails.add(detail.getInvoiceDetail());
			}
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_INFO_ALERT", paymentVO);
			ServletActionContext.getRequest().setAttribute(
					"INVOICE_DETAIL_LIST", invoiceDetails);
			LOGGER.info("Module: Accounts : Method: showPurchasePaymentAlert: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchasePaymentAlert: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String processPurchasePayment() {
		LOGGER.info("Inside Module: Accounts : Method: processPurchasePayment");
		try {
			Payment purchasePayment = paymentBL.getPaymentService()
					.getPurchasePaymentDetails(paymentId);
			purchasePayment.setPrintTaken(true);
			paymentBL.updatePaymentTrascation(purchasePayment, alertId);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_INFO_PRINT", purchasePayment);
			Map<Long, Invoice> invoiceMap = new HashMap<Long, Invoice>();
			for (PaymentDetail paymentDetail : purchasePayment
					.getPaymentDetails()) {
				invoiceMap.put(paymentDetail.getInvoiceDetail().getInvoice()
						.getInvoiceId(), paymentDetail.getInvoiceDetail()
						.getInvoice());
			}
			List<InvoiceVO> invoiceVOs = new ArrayList<InvoiceVO>();
			InvoiceVO invoiceVO = null;
			for (Invoice invoice : invoiceMap.values()) {
				double totalInvoiceQty = 0;
				double totalInvoiceAmount = 0;
				invoiceVO = new InvoiceVO();
				invoiceVO.setInvoiceNumber(invoice.getInvoiceNumber());
				invoiceVO.setDescription(invoice.getDescription());
				for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
					totalInvoiceQty += invoiceDetail.getInvoiceQty();
					totalInvoiceAmount += (invoiceDetail.getReceiveDetail()
							.getUnitRate() * invoiceDetail.getInvoiceQty());
				}
				invoiceVO.setTotalInvoiceQty(totalInvoiceQty);
				invoiceVO.setTotalInvoiceAmount(totalInvoiceAmount);
				invoiceVOs.add(invoiceVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_INVOICE_INFO_PRINT", invoiceVOs);
			LOGGER.info("Module: Accounts : Method: showPurchasePaymentAlert: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: processPurchasePayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Inventory payment save
	public String saveInventoryPayment() {
		LOGGER.info("Inside Module: Accounts : Method: saveInventoryPayment");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Payment payment = new Payment();
			payment.setPaymentNumber(getPaymentNumber());
			payment.setDate(DateFormat.convertStringToDate(paymentDate));
			payment.setChequeDate(DateFormat.convertStringToDate(chequeDate));
			payment.setCreatedDate(new Date());
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			payment.setPerson(person);
			payment.setPaymentMode(paymentMode);
			BankAccount bankAccount = null;
			ChequeBook chequeBook = null;
			if (bankAccountId > 0 && transferAccountId == 0) {
				bankAccount = paymentBL.getBankBL().getBankService()
						.getBankAccountInfo(bankAccountId);
				chequeBook = paymentBL.getChequeBookBL().getChequeBookService()
						.findChequeBookById(chequeBookId);
				payment.setChequeBook(chequeBook);
				chequeBook
						.setCurrentNo(null != chequeBook.getCurrentNo() ? chequeBook
								.getCurrentNo() + 1 : chequeBook
								.getStartingNo());
				payment.setChequeNumber(chequeBook.getCurrentNo());
				payment.setCombination(bankAccount.getCombination());
				payment.setChequeBook(chequeBook);
				payment.setBankAccount(bankAccount);
				if (paymentId > 0) {
					payment.setChequeNumber(chequeNumber);
				}
			} else if (cardAccountId > 0) {
				bankAccount = paymentBL.getBankBL().getBankService()
						.getBankAccountInfo(cardAccountId);
				payment.setBankAccount(bankAccount);
				payment.setCombination(bankAccount.getCombination());
			} else if (transferAccountId > 0) {
				bankAccount = paymentBL.getBankBL().getBankService()
						.getBankAccountInfo(transferAccountId);
				payment.setBankAccount(bankAccount);
				payment.setCombination(bankAccount.getCombination());
			} else {
				Combination combination = new Combination();
				combination.setCombinationId(cashCombinationId);
				payment.setCombination(combination);
			}
			payment.setDescription(description);
			payment.setImplementation(getImplementationId());
			payment.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.NoDecession
							.getCode() + ""));
			@SuppressWarnings("unchecked")
			Map<Long, InvoiceVO> invoiceHash = (Map<Long, InvoiceVO>) (session
					.getAttribute("INVOICE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			String[] invoiceIdArray = splitValues(paymentDetails, "@#");
			List<InvoiceVO> invoiceVOList = new ArrayList<InvoiceVO>();
			for (String invoiceLine : invoiceIdArray) {
				if (invoiceHash.containsKey(Long.parseLong(invoiceLine))) {
					InvoiceVO invoiceVO = (InvoiceVO) invoiceHash.get(Long
							.parseLong(invoiceLine));
					invoiceVOList.add(invoiceVO);
				}
			}
			PaymentDetail paymentDetail = null;
			List<PaymentDetail> paymentDetailList = new ArrayList<PaymentDetail>();
			Invoice invoice = null;
			List<Invoice> invoiceList = new ArrayList<Invoice>();
			for (InvoiceVO list : invoiceVOList) {
				for (InvoiceDetailVO detailVO : list.getInvoiceDetailVOList()) {
					paymentDetail = new PaymentDetail();
					paymentDetail.setInvoiceDetail(detailVO);
					paymentDetail.setDescription(detailVO.getDescription());
					paymentDetailList.add(paymentDetail);
				}
				invoice = new Invoice();
				invoice.setInvoiceId(list.getInvoiceId());
				invoice.setInvoiceNumber(list.getInvoiceNumber());
				invoice.setDate(list.getDate());
				invoice.setIsApprove(list.getIsApprove());
				invoice.setStatus(InvoiceStatus.Paid.getCode());
				invoice.setDescription(list.getDescription());
				invoice.setSupplier(list.getSupplier());
				invoice.setImplementation(getImplementationId());
				invoice.setSupplier(list.getSupplier());
				invoiceList.add(invoice);
			}
			payment.setSupplier(invoiceVOList.get(0).getSupplier());
			paymentBL.postInvoicePaymentTransaction(payment, paymentDetailList,
					invoiceList, chequeBook);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: savePayment: Action Success");
			session.removeAttribute("INVOICE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveInventoryPayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/*
	 * public String savePayment() {
	 * LOGGER.info("Inside Module: Accounts : Method: savePayment"); try {
	 * Map<String, Object> sessionObj = ActionContext.getContext()
	 * .getSession(); User user = (User) sessionObj.get("USER"); Payment payment
	 * = new Payment(); if (paymentId > 0) { payment.setPaymentId(paymentId);
	 * Payment tempPayment = paymentBL.getPaymentService()
	 * .getPayment(paymentId);
	 * payment.setPrintTaken(tempPayment.getPrintTaken());
	 * payment.setPaymentNumber(tempPayment.getPaymentNumber()); } else {
	 * payment.setPaymentNumber(getPaymentNumber()); }
	 * payment.setDate(DateFormat.convertStringToDate(paymentDate));
	 * payment.setChequeNumber(chequeNumber);
	 * payment.setChequeDate(DateFormat.convertStringToDate(chequeDate));
	 * payment.setCreatedDate(new Date()); Person person = new Person();
	 * person.setPersonId(user.getPersonId()); payment.setPerson(person); if
	 * (paymentMode > 0) { LookupDetail lookupDetail = new LookupDetail();
	 * lookupDetail.setLookupDetailId(paymentMode);
	 * payment.setLookupDetail(lookupDetail); } if (loanId > 0) { Loan loan =
	 * new Loan(); loan.setLoanId(loanId); payment.setLoan(loan); } if
	 * (currencyId > 0) { Currency currency = new Currency();
	 * currency.setCurrencyId(currencyId); payment.setCurrency(currency); } if
	 * (bankAccountId > 0) { BankAccount bankAccount = paymentBL.getBankBL()
	 * .getBankService().getBankAccountInfo(bankAccountId);
	 * payment.setBankAccount(bankAccount);
	 * payment.setCombination(bankAccount.getCombination());
	 * 
	 * } else { Combination paymentCombination = new Combination();
	 * paymentCombination.setCombinationId(cashCombinationId);
	 * payment.setCombination(paymentCombination); }
	 * payment.setDescription(description);
	 * payment.setImplementation(getImplementationId());
	 * payment.setIsApprove(Byte
	 * .parseByte(Constants.RealEstate.Status.NoDecession .getCode() + ""));
	 * PaymentDetail detail = null;
	 * 
	 * String[] lineDetailArray = splitValues(paymentDetails, "#@");
	 * List<PaymentDetail> detailList = new ArrayList<PaymentDetail>(); for
	 * (String string : lineDetailArray) { String[] detailString =
	 * splitValues(string, "__"); detail = new PaymentDetail();
	 * 
	 * if (Long.parseLong(detailString[0]) > 0) { product = new Product();
	 * product.setProductId(Long.parseLong(detailString[0]));
	 * detail.setProduct(product);
	 * detail.setQuantity(Double.parseDouble(detailString[1]));
	 * detail.setUnitRate(Double.parseDouble(detailString[2] .replaceAll(",",
	 * ""))); } else { loanCharge = new LoanCharge(); loanCharge
	 * .setOtherChargeId(Long.parseLong(detailString[3])); combination = new
	 * Combination(); combination.setCombinationId(Long
	 * .parseLong(detailString[4])); detail.setLoanCharge(loanCharge);
	 * detail.setCombination(combination); }
	 * 
	 * if (!("###").equals(detailString[5]))
	 * detail.setDescription(detailString[5]); if (Long.valueOf(detailString[6])
	 * > 0) detail.setPaymentDetailId(Long.valueOf(detailString[6]));
	 * detailList.add(detail); } if (loanId > 0) { List<LoanCharge>
	 * loanChargesList = paymentBL
	 * .getLoanService().getLoanChargePayments(loanId); for (LoanCharge list :
	 * loanChargesList) { list.setPaymentFlag(true); paymentBL.getLoanService()
	 * .updateLoanCharges(loanChargesList); } paymentBL.postTransaction(payment,
	 * detailList); } else if (null != detailList.get(0).getInvoiceDetail() &&
	 * !("").equals(detailList.get(0).getInvoiceDetail())) {
	 * 
	 * paymentBL.postReceivePaymentTransaction(payment, detailList,
	 * revertPayment);
	 * 
	 * } returnMessage = "SUCCESS";
	 * ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
	 * returnMessage);
	 * LOGGER.info("Module: Accounts : Method: savePayment: Action Success");
	 * return SUCCESS; } catch (Exception ex) { ex.printStackTrace();
	 * returnMessage = "FAILURE";
	 * ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
	 * returnMessage);
	 * LOGGER.info("Module: Accounts : Method: savePayment: throws Exception " +
	 * ex); return ERROR; } }
	 */

	public String showPaymentDiscount() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPaymentDiscount");
			/*
			 * PaymentTerm terms = paymentBL.getPurchaseBL()
			 * .getPaymentTermService().getPaymentTerm(paymentTermId); if (null
			 * != terms && !("0").equals(terms.getDiscount()) &&
			 * !("0").equals(terms.getDiscountDays())) { long discountDays =
			 * DateFormat .dayVariance(
			 * DateFormat.convertStringToDate(paymentDate), new Date()); if
			 * (discountDays >= terms.getDiscountDays()) { discountReceived =
			 * (totalAmount / 100) * terms.getDiscount(); netAmount =
			 * totalAmount - discountReceived; } else { discountReceived = 0.0;
			 * netAmount = totalAmount; } } else { discountReceived = 0.0;
			 * netAmount = totalAmount; }
			 */
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentDiscount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deletePayment() {
		LOGGER.info("Inside Module: Accounts : Method: deletePayment");
		try {
			Payment payment = paymentBL.getPaymentService()
					.getPurchasePaymentDetails(paymentId);
			if (null != payment.getVoidPayments()
					&& payment.getVoidPayments().size() > 0) {
				returnMessage = "Payment marked as void, can't be delete.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: deletePayment: Action Error ");
				return ERROR;
			}
			paymentBL.deletePaymementTransaction(payment);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deletePayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deletePayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/*
	 * public String showLoanCharges() {
	 * LOGGER.info("Inside Module: Accounts : Method: showLoanCharges"); try {
	 * Loan loan = getPaymentBL().getLoanService().getLoanById(loanId);
	 * List<LoanCharge> loanCharges = getPaymentBL().getLoanService()
	 * .getLoanChargesByValue(loanId);
	 * ServletActionContext.getRequest().setAttribute("LOAN_INFO", loan);
	 * ServletActionContext.getRequest().setAttribute("LOAN_CHARGES",
	 * loanCharges); paymentNumber = getPaymentNumber();
	 * ServletActionContext.getRequest().setAttribute("PAYMENT_NUMBER",
	 * paymentNumber);
	 * LOGGER.info("Module: Accounts : Method: showLoanCharges: Action Success"
	 * ); return SUCCESS; } catch (Exception ex) { ex.printStackTrace();
	 * LOGGER.info
	 * ("Module: Accounts : Method: showLoanCharges: throws Exception " + ex);
	 * return ERROR; } }
	 */

	public String showLoanChargesUpdate() {
		try {
			if (paymentId == 0) {
				paymentId = recordId;
			}
			Payment payment = getPaymentBL().getPaymentService()
					.getByPaymentId(paymentId);
			List<PaymentDetail> paymentDetail = getPaymentBL()
					.getPaymentService().getPaymentDetailById(paymentId);
			ServletActionContext.getRequest().setAttribute("PAYMENT_INFO",
					payment);
			ServletActionContext.getRequest().setAttribute("PAYMENT_DETAILS",
					paymentDetail);
			Comment comment = new Comment();
			if (recordId != 0) {
				comment.setRecordId(recordId);
				comment.setUseCase("com.aiotech.aios.accounts.domain.entity.Payment");
				comment = paymentBL.getCommentBL().getCommentInfo(comment);
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_IFNO", comment);
			LOGGER.info("Module: Accounts : Method: showLoanChargesUpdate: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showLoanChargesUpdate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private String getPaymentNumber() throws Exception {
		return SystemBL.getReferenceStamp(Payment.class.getName(),
				paymentBL.getImplementationId());
	}

	private final static Implementation getImplementationId() {
		Implementation implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}
	
	public String payableDirectPayment() {

		LOGGER.info("Inside Module: Accounts : Method: employeeDirectPayment");
		try {
			Payment payment=null;
			if (recordId != 0) {
				DirectPaymentVO vo = new DirectPaymentVO();
				payment = paymentBL.getPaymentService()
				.getPurchasePaymentDetails(recordId);
				vo.setAlertId(alertId);
				vo.setMessageId(null);
				vo.setObject(payment);
				vo.setRecordId(recordId);
				vo.setUseCase(Payment.class.getSimpleName());
				paymentBL.payableDirectPayment(vo);
			}

			LOGGER.info("Module: Accounts : Method: employeeDirectPayment SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: employeeDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public PaymentBL getPaymentBL() {
		return paymentBL;
	}

	public void setPaymentBL(PaymentBL paymentBL) {
		this.paymentBL = paymentBL;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public long getLoanId() {
		return loanId;
	}

	public void setLoanId(long loanId) {
		this.loanId = loanId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCashCombinationId() {
		return cashCombinationId;
	}

	public void setCashCombinationId(long cashCombinationId) {
		this.cashCombinationId = cashCombinationId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

	public double getDiscountReceived() {
		return discountReceived;
	}

	public void setDiscountReceived(double discountReceived) {
		this.discountReceived = discountReceived;
	}

	public Integer getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(Integer chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeFlag() {
		return chequeFlag;
	}

	public void setChequeFlag(String chequeFlag) {
		this.chequeFlag = chequeFlag;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public long getTransferAccountId() {
		return transferAccountId;
	}

	public void setTransferAccountId(long transferAccountId) {
		this.transferAccountId = transferAccountId;
	}

	public long getCardAccountId() {
		return cardAccountId;
	}

	public void setCardAccountId(long cardAccountId) {
		this.cardAccountId = cardAccountId;
	}

	public long getChequeBookId() {
		return chequeBookId;
	}

	public void setChequeBookId(long chequeBookId) {
		this.chequeBookId = chequeBookId;
	}
}
