/**
 * Cheque Book Initial Development 
 * Creation Date 28-March-2013
 */
package com.aiotech.aios.accounts.action;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.service.bl.ChequeBookBL;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class ChequeBookAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private long chequeBookId;
	private Integer startingNo;
	private Integer endingNo;
	private Integer notifyNo;
	private String description;
	private long bankAccountId;
	private String returnMessage;
	private Integer chequeNo;
	private ChequeBookBL chequeBookBL;
	private Long recordId;

	private static final Logger LOGGER = LogManager
			.getLogger(ChequeBookAction.class);

	// return success
	public String returnSuccess() {
		LOGGER.info("Inside Method returnSuccess");
		return SUCCESS;
	}

	// list all cheque books in json format
	public String showJsonChequeBook() {
		LOGGER.info("Inside Module: Accounts : Method: showJsonChequeBook");
		try {
			JSONObject jsonList = chequeBookBL.getAllChequeBooks();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonChequeBook: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonChequeBook Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show update view
	public String showChequeBookEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showChequeBookEntry");
		try {
			ChequeBook chequeBook = null;
			if(recordId!=null && recordId>0){
				chequeBookId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (chequeBookId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(ChequeBook.class.getName());
					comment = chequeBookBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			
			if (chequeBookId > 0) 
				chequeBook = chequeBookBL
						.getChequeBookService()
						.findChequeBookById(chequeBookId); 
			ServletActionContext.getRequest().setAttribute("CHEQUE_BOOK",
					chequeBook);
			LOGGER.info("Module: Accounts : Method: showJsonChequeBook: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showChequeBookEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// save or update cheque book
	public String saveChequeBook() {
		LOGGER.info("Inside Module: Accounts : Method: saveChequeBook");
		try {
			ChequeBook chequeBook = new ChequeBook();
			if (chequeBookId > 0) {
				chequeBook.setChequeBookId(chequeBookId);
			}
			chequeBook.setStartingNo(startingNo);
			chequeBook.setEndingNo(endingNo);
			if (null != notifyNo && notifyNo > 0)
				chequeBook.setNotifyNo(notifyNo);
			BankAccount bankAccount = new BankAccount();
			bankAccount.setBankAccountId(bankAccountId);
			chequeBook.setBankAccount(bankAccount);
			chequeBook.setDescription(description);
			chequeBook.setChequeBookNo(chequeNo);
			chequeBookBL.saveChequeBook(chequeBook);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveChequeBook: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveChequeBook: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// delete cheque book
	public String deleteChequeBook() {
		LOGGER.info("Inside Module: Accounts : Method: deleteChequeBook");
		try {
			ChequeBook chequeBook = chequeBookBL.getChequeBookService()
					.findChequeById(chequeBookId);
			chequeBookBL.deleteChequeBook(chequeBook);
			returnMessage = ActionSupport.SUCCESS;
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveChequeBook: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Cheque book is in use. ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveChequeBook: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public long getChequeBookId() {
		return chequeBookId;
	}

	public void setChequeBookId(long chequeBookId) {
		this.chequeBookId = chequeBookId;
	}

	public ChequeBookBL getChequeBookBL() {
		return chequeBookBL;
	}

	public void setChequeBookBL(ChequeBookBL chequeBookBL) {
		this.chequeBookBL = chequeBookBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Integer getStartingNo() {
		return startingNo;
	}

	public void setStartingNo(Integer startingNo) {
		this.startingNo = startingNo;
	}

	public Integer getEndingNo() {
		return endingNo;
	}

	public void setEndingNo(Integer endingNo) {
		this.endingNo = endingNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public Integer getNotifyNo() {
		return notifyNo;
	}

	public void setNotifyNo(Integer notifyNo) {
		this.notifyNo = notifyNo;
	}

	public Integer getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(Integer chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
