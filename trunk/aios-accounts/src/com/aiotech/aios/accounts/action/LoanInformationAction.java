package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Eibor;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.LoanDetail;
import com.aiotech.aios.accounts.service.bl.LoanBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.InstitutionType;
import com.aiotech.aios.common.to.Constants.Accounts.LoanFrequency;
import com.aiotech.aios.common.to.Constants.Accounts.LoanType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoanInformationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	// Variable Declarations
	private long loanId;
	private long eiborId;
	private String returnMessage;
	private String showPage;
	private long bankId;
	private String loanNumber;
	private char loanType;
	private String sancationDate;
	private long accountId;
	private double loanAmount;
	private double eiborRate;
	private String interestType;
	private double interestRate;
	private double finalInterest;
	private int leadyDay;
	private char emiFrequency;
	private String emiStartDate;
	private String emiEndDate;
	private double emiPrincipal;
	private String loanLineDetail;
	private String loanChargesDetail;
	private String eiborTypes;
	private String description;
	private String maturityDate;
	private String providerName;
	private String landLine;
	private String mobileNumber;
	private String addressDetails;
	private char emiType;
	private long otherCombinationId;
	private long emiCombinationId;
	private long loanCombinationId;
	private int totalEMI;
	private String emailAddress;
	private long interestExpensesCombinationId;
	private long interestPayableCombinationId;
	private long shortTermCombinationId;
	private String emiDueDate;
	private String eiborDate;
	private Long currencyId;
	private Character rateVariance;
	private double insurance;
	private int totalEmi;
	private String fixedFrom;
	private String fixedTo;
	private String page;
	// Object Declarations
	private Loan loan;
	private List<Loan> loanList;
	private List<LoanDetail> loanDetails;
	private List<LoanCharge> loanCharge;
	private LoanBL loanBL;
	private long recordId;
	@SuppressWarnings("unused")
	private Implementation implementation;
	private Integer id;
	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(LoanInformationAction.class);

	public String returnSuccess() {
		if (null == id)
			id = 0;
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	public String showLoanList() {
		LOGGER.info("Module: Accounts : Method: showLoanList");
		try {
			JSONObject jsonList = getLoanBL().getLoanlist(getImplementaionId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showLoanList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showLoanList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showLoanEntry() {
		LOGGER.info("Module: Accounts : Method: showLoanEntry");
		try {
			Map<Character, LoanType> loanType = new HashMap<Character, LoanType>();
			for (LoanType e : EnumSet.allOf(LoanType.class)) {
				loanType.put(e.getCode(), e);
			}
			Map<Character, LoanFrequency> loanFrequency = new HashMap<Character, LoanFrequency>();
			for (LoanFrequency e : EnumSet.allOf(LoanFrequency.class)) {
				loanFrequency.put(e.getCode(), e);
			}
			List<Currency> currencyList = getLoanBL().getCurrencyService()
					.getAllCurrency(getImplementaionId());
			ServletActionContext.getRequest().setAttribute("PAGE_INFO",
					showPage);
			ServletActionContext.getRequest().setAttribute("LOAN_TYPE",
					loanType);
			ServletActionContext.getRequest().setAttribute("LOAN_FREQUENCY",
					loanFrequency);
			ServletActionContext.getRequest().setAttribute(
					"LOAN_CURRENCY_DETAILS", currencyList);
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
				}
			}
			// Financial Year
			Calendar cal = loanBL.getTransactionBL().getCalendarBL().getCalendarService()
					.getAtiveCalendar(getImplementation());
			if (cal != null && cal.getEndTime() != null)
				ServletActionContext.getRequest().setAttribute(
						"FINANCIAL_YEAR_ENDDATE",
						DateFormat.convertDateToString(cal.getEndTime()
								.toString()));

			LOGGER.info("Module: Accounts : Method: showLoanEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showLoanEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showLoanUpdateEntry() {
		LOGGER.info("Module: Accounts : Method: showLoanUpdateEntry");
		try {
			if (loanId == 0) {
				loanId = recordId;
				showPage = "edit";
			}
			Map<Character, LoanType> loanType = new HashMap<Character, LoanType>();
			for (LoanType e : EnumSet.allOf(LoanType.class)) {
				loanType.put(e.getCode(), e);
			}
			Map<Character, LoanFrequency> loanFrequency = new HashMap<Character, LoanFrequency>();
			for (LoanFrequency e : EnumSet.allOf(LoanFrequency.class)) {
				loanFrequency.put(e.getCode(), e);
			}
			List<Currency> currencyList = getLoanBL().getCurrencyService()
					.getAllCurrency(getImplementaionId());
			ServletActionContext.getRequest().setAttribute("PAGE_INFO",
					showPage);
			ServletActionContext.getRequest().setAttribute("LOAN_TYPE",
					loanType);
			ServletActionContext.getRequest().setAttribute("LOAN_FREQUENCY",
					loanFrequency);
			ServletActionContext.getRequest().setAttribute(
					"LOAN_CURRENCY_DETAILS", currencyList);
			Loan loan = getLoanBL().getLoanService().getLoanById(loanId);
			ServletActionContext.getRequest().setAttribute("LOAN_INFORMATION",
					loan);
			List<LoanDetail> detailList = getLoanBL().getLoanService()
					.getLoanDetailsById(loanId);
			ServletActionContext.getRequest().setAttribute(
					"LOAN_DETAIL_INFORMATION", detailList);
			List<LoanCharge> chargeList = getLoanBL().getLoanService()
					.getLoanChargesByAll(loanId);
			ServletActionContext.getRequest().setAttribute(
					"LOAN_CHARGES_INFORMATION", chargeList);
			Comment comment = new Comment();
			if (recordId != 0) {
				comment.setRecordId(recordId);
				comment.setUseCase("com.aiotech.aios.accounts.domain.entity.Payment");
				comment = this.getLoanBL().getCommentBL()
						.getCommentInfo(comment);
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_IFNO", comment);
			String provider = loan.getProviderDetails();
			String[] addressDetail = splitValues(provider, "##");
			if (!addressDetail[0].toString().equalsIgnoreCase("null"))
				providerName = addressDetail[0];
			else
				providerName = "";
			if (!addressDetail[1].toString().equalsIgnoreCase("null"))
				mobileNumber = addressDetail[1];
			else
				mobileNumber = "";
			if (!addressDetail[2].toString().equalsIgnoreCase("null"))
				landLine = addressDetail[2];
			else
				landLine = "";
			if (!addressDetail[3].toString().equalsIgnoreCase("null"))
				emailAddress = addressDetail[3];
			else
				emailAddress = "";
			if (!addressDetail[4].toString().equalsIgnoreCase("null"))
				addressDetails = addressDetail[4];
			else
				addressDetails = "";
			LOGGER.info("Module: Accounts : Method: showLoanUpdateEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showLoanUpdateEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteLoanEntry() {
		LOGGER.info("Module: Accounts : Method: deleteLoanEntry");
		try {
			loan = getLoanBL().getLoanService().getLoanById(loanId);
			loanDetails = getLoanBL().getLoanService().getLoanDetailsById(
					loanId);
			loanCharge = getLoanBL().getLoanService()
					.getLoanChargesById(loanId);
			getLoanBL().getLoanService().deleteLoan(loan, loanDetails,
					loanCharge);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteLoanEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: deleteLoanEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBanks() {
		LOGGER.info("Module: Accounts : Method: showBanks");
		try {
			List<Bank> bankList = getLoanBL()
					.getEiborBL()
					.getBankService()
					.getAllBanks(getImplementaionId(),
							InstitutionType.Bank.getCode());
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			LOGGER.info("Module: Accounts : Method: showBanks: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBanks: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBankAccount() {
		LOGGER.info("Module: Accounts : Method: showBankAccount");
		try {
			// Bank bank=new Bank();
			// bank.setBankId(bankId);
			// List<BankAccount>
			// banksList=getLoanBL().getEiborBL().getBankService().getAllBankAccount(bank);
			List<Bank> bankList = getLoanBL()
					.getEiborBL()
					.getBankService()
					.getAllBanks(getImplementaionId(),
							InstitutionType.Bank.getCode());
			List<BankAccount> bankAccountList = getLoanBL().getEiborBL()
					.getBankService().getAllBankAccounts(getImplementaionId());
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			ServletActionContext.getRequest().setAttribute(
					"BANK_ACCOUNT_DETAILS", bankAccountList);
			ServletActionContext.getRequest().setAttribute("PAGE_VALUE",
					showPage);
			LOGGER.info("Module: Accounts : Method: showBankAccount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBankAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Added by rafiq for Common Bank Listing system to Grid approach
	public String showBankAccountMultiple() {
		LOGGER.info("Module: Accounts : Method: showBankAccount");
		try {
			// Bank bank=new Bank();
			// bank.setBankId(bankId);
			// List<BankAccount>
			// banksList=getLoanBL().getEiborBL().getBankService().getAllBankAccount(bank);
			List<Bank> bankList = getLoanBL()
					.getEiborBL()
					.getBankService()
					.getAllBanks(getImplementaionId(),
							InstitutionType.Bank.getCode());
			List<BankAccount> bankAccountList = getLoanBL().getEiborBL()
					.getBankService().getAllBankAccounts(getImplementaionId());
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			ServletActionContext.getRequest().setAttribute(
					"BANK_ACCOUNT_DETAILS", bankAccountList);
			ServletActionContext.getRequest().setAttribute("rowId", id);
			LOGGER.info("Module: Accounts : Method: showBankAccount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBankAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showEiborDetails() {
		LOGGER.info("Module: Accounts : Method: showEiborDetails");
		try {
			List<Eibor> eiborList = getLoanBL().getEiborBL().getEiborService()
					.getEiborList(getImplementaionId());
			List<LoanInformationAction> eiborTypeList = getLoanBL()
					.convertEibor(eiborList);
			ServletActionContext.getRequest().setAttribute("EIBOR_DETAILS",
					eiborTypeList);
			LOGGER.info("Module: Accounts : Method: showEiborDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showEiborDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveLoanEntry() {
		LOGGER.info("Module: Accounts : Method: saveLoanEntry");
		try {
			Loan loan = new Loan();
			BankAccount account = new BankAccount();
			Eibor eibor = new Eibor();
			if (loanId > 0)
				loan.setLoanId(loanId);
			loan.setLoanNumber(loanNumber);
			loan.setSanctionDate(DateFormat.convertStringToDate(sancationDate));
			loan.setMaturityDate(DateFormat.convertStringToDate(maturityDate));
			loan.setLoanType(loanType);
			account.setBankAccountId(accountId);
			loan.setBankAccount(account);
			loan.setAmount(loanAmount);
			if (eiborId > 0) {
				eibor.setEiborId(eiborId);
				loan.setEibor(eibor);
			} else
				loan.setEibor(null);
			loan.setRateType(interestType);
			loan.setRate(interestRate);
			loan.setFinalRate(finalInterest);
			loan.setLeadDay(leadyDay);
			loan.setEmiFrequency(emiFrequency);
			loan.setEmiFirstDuedate(DateFormat.convertStringToDate(emiDueDate));
			loan.setPrincipal(emiPrincipal);
			loan.setImplementation(getImplementaionId());
			loan.setDescription(description);
			loan.setEmiType(emiType + "");
			loan.setEmiStartDate(DateFormat.convertStringToDate(emiStartDate));
			loan.setEmiEndDate(DateFormat.convertStringToDate(emiEndDate));
			loan.setRateVariance(rateVariance);
			StringBuilder provider = new StringBuilder();
			if (null != providerName && !providerName.equals(""))
				provider.append(providerName).append("##");
			else
				provider.append("null").append("##");
			if (null != mobileNumber && !mobileNumber.equals(""))
				provider.append(mobileNumber).append("##");
			else
				provider.append("null").append("##");
			if (null != landLine && !landLine.equals(""))
				provider.append(landLine).append("##");
			else
				provider.append("null").append("##");
			if (null != emailAddress && !emailAddress.equals(""))
				provider.append(emailAddress).append("##");
			else
				provider.append("null").append("##");
			if (null != addressDetails && !addressDetails.equals(""))
				provider.append(addressDetails).append("##");
			else
				provider.append("null");
			loan.setProviderDetails(provider.toString());
			Currency currency = new Currency();
			currency.setCurrencyId(currencyId);
			loan.setCurrency(currency);
			Combination combination = new Combination();
			combination.setCombinationId(interestExpensesCombinationId);
			loan.setCombinationByInterestExpenses(combination);
			Combination analysisTerm = getLoanBL().createAnalysisCombination(
					loanCombinationId, getImplementaionId(),
					"Long Term_" + loan.getLoanNumber());
			loan.setCombinationByLoanAccountId(analysisTerm);
			analysisTerm = getLoanBL().createAnalysisCombination(
					shortTermCombinationId, getImplementaionId(),
					"Short Term_" + loan.getLoanNumber());
			loan.setCombinationByShortTermAccountId(analysisTerm);

			if (loanId == 0) {
				
					loan.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.Published
									.getCode() + ""));
			
			}
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			loan.setPerson(person);
			loan.setCreatedDate(new Date());
			LoanDetail detail = null;
			List<LoanDetail> loanDetailList = new ArrayList<LoanDetail>();
			String[] lineDetailArray = splitValues(loanLineDetail, "@@");
			for (String string : lineDetailArray) {
				String[] detailList = splitValues(string, "__");
				detail = new LoanDetail();
				detail.setDate(DateFormat.convertStringToDate(detailList[0]));
				detail.setAmount(Double.parseDouble(detailList[1]));
				detail.setDescription(detailList[2]);
				if (Long.parseLong(detailList[3]) > 0)
					detail.setLoanDetailId(Long.parseLong(detailList[3]));
				loanDetailList.add(detail);
			}
			LoanCharge charges = null;
			List<LoanCharge> chargesList = new ArrayList<LoanCharge>();
			String[] chargesArray = splitValues(loanChargesDetail, "@@");
			for (String string : chargesArray) {
				String[] detailList = splitValues(string, "__");
				charges = new LoanCharge();
				charges.setType(detailList[0].charAt(0));
				charges.setName(detailList[1]);
				charges.setValue(Double.parseDouble(detailList[2]));
				if (Long.parseLong(detailList[3]) > 0)
					charges.setOtherChargeId(Long.parseLong(detailList[3]));
				combination = new Combination();
				combination.setCombinationId(Long.parseLong(detailList[4]));
				charges.setCombination(combination);
				if (charges.getType() == 'A') {
					charges.setPaymentFlag(new Boolean(detailList[5]));
				}
				chargesList.add(charges);

			}
			getLoanBL().saveLoanEntry(loan, loanDetailList, chargesList, page);
			boolean flag = false;
			if (null != chargesList && chargesList.size() > 0) {
				for (LoanCharge charge : chargesList) {
					if (charge.getType() == 'A'
							&& charge.getPaymentFlag().equals(false)) {
						returnMessage = "PAYMENTS";
						flag = true;
						break;
					}
				}
			}
			if (flag) {
				returnMessage = "PAYMENTS";
			} else {
				returnMessage = "SUCCESS";
			}
			ServletActionContext.getRequest().setAttribute("LOAN_INFOI", loan);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveLoanEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error " + ex;
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveLoanEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Implementation getImplementaionId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = new Implementation();
		if (null != session.getAttribute("THIS"))
			implementation = (Implementation) session.getAttribute("THIS");
		return implementation;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public List<Loan> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<Loan> loanList) {
		this.loanList = loanList;
	}

	public long getLoanId() {
		return loanId;
	}

	public void setLoanId(long loanId) {
		this.loanId = loanId;
	}

	public long getEiborId() {
		return eiborId;
	}

	public void setEiborId(long eiborId) {
		this.eiborId = eiborId;
	}

	public LoanBL getLoanBL() {
		return loanBL;
	}

	public void setLoanBL(LoanBL loanBL) {
		this.loanBL = loanBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<LoanDetail> getLoanDetails() {
		return loanDetails;
	}

	public void setLoanDetails(List<LoanDetail> loanDetails) {
		this.loanDetails = loanDetails;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}

	public char getLoanType() {
		return loanType;
	}

	public void setLoanType(char loanType) {
		this.loanType = loanType;
	}

	public String getSancationDate() {
		return sancationDate;
	}

	public void setSancationDate(String sancationDate) {
		this.sancationDate = sancationDate;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public double getEiborRate() {
		return eiborRate;
	}

	public void setEiborRate(double eiborRate) {
		this.eiborRate = eiborRate;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getFinalInterest() {
		return finalInterest;
	}

	public void setFinalInterest(double finalInterest) {
		this.finalInterest = finalInterest;
	}

	public int getLeadyDay() {
		return leadyDay;
	}

	public void setLeadyDay(int leadyDay) {
		this.leadyDay = leadyDay;
	}

	public char getEmiFrequency() {
		return emiFrequency;
	}

	public void setEmiFrequency(char emiFrequency) {
		this.emiFrequency = emiFrequency;
	}

	public String getEmiStartDate() {
		return emiStartDate;
	}

	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}

	public String getEmiEndDate() {
		return emiEndDate;
	}

	public void setEmiEndDate(String emiEndDate) {
		this.emiEndDate = emiEndDate;
	}

	public double getEmiPrincipal() {
		return emiPrincipal;
	}

	public void setEmiPrincipal(double emiPrincipal) {
		this.emiPrincipal = emiPrincipal;
	}

	public String getLoanLineDetail() {
		return loanLineDetail;
	}

	public void setLoanLineDetail(String loanLineDetail) {
		this.loanLineDetail = loanLineDetail;
	}

	public String getInterestType() {
		return interestType;
	}

	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}

	public String getEiborTypes() {
		return eiborTypes;
	}

	public void setEiborTypes(String eiborTypes) {
		this.eiborTypes = eiborTypes;
	}

	public static Logger getLogger() {
		return LOGGER;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getLandLine() {
		return landLine;
	}

	public void setLandLine(String landLine) {
		this.landLine = landLine;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddressDetails() {
		return addressDetails;
	}

	public void setAddressDetails(String addressDetails) {
		this.addressDetails = addressDetails;
	}

	public char getEmiType() {
		return emiType;
	}

	public void setEmiType(char emiType) {
		this.emiType = emiType;
	}

	public long getOtherCombinationId() {
		return otherCombinationId;
	}

	public void setOtherCombinationId(long otherCombinationId) {
		this.otherCombinationId = otherCombinationId;
	}

	public long getEmiCombinationId() {
		return emiCombinationId;
	}

	public void setEmiCombinationId(long emiCombinationId) {
		this.emiCombinationId = emiCombinationId;
	}

	public long getLoanCombinationId() {
		return loanCombinationId;
	}

	public void setLoanCombinationId(long loanCombinationId) {
		this.loanCombinationId = loanCombinationId;
	}

	public int getTotalEMI() {
		return totalEMI;
	}

	public void setTotalEMI(int totalEMI) {
		this.totalEMI = totalEMI;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public long getInterestExpensesCombinationId() {
		return interestExpensesCombinationId;
	}

	public void setInterestExpensesCombinationId(
			long interestExpensesCombinationId) {
		this.interestExpensesCombinationId = interestExpensesCombinationId;
	}

	public long getInterestPayableCombinationId() {
		return interestPayableCombinationId;
	}

	public void setInterestPayableCombinationId(
			long interestPayableCombinationId) {
		this.interestPayableCombinationId = interestPayableCombinationId;
	}

	public long getShortTermCombinationId() {
		return shortTermCombinationId;
	}

	public void setShortTermCombinationId(long shortTermCombinationId) {
		this.shortTermCombinationId = shortTermCombinationId;
	}

	public String getEmiDueDate() {
		return emiDueDate;
	}

	public void setEmiDueDate(String emiDueDate) {
		this.emiDueDate = emiDueDate;
	}

	public String getEiborDate() {
		return eiborDate;
	}

	public void setEiborDate(String eiborDate) {
		this.eiborDate = eiborDate;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Character getRateVariance() {
		return rateVariance;
	}

	public void setRateVariance(Character rateVariance) {
		this.rateVariance = rateVariance;
	}

	public double getInsurance() {
		return insurance;
	}

	public void setInsurance(double insurance) {
		this.insurance = insurance;
	}

	public int getTotalEmi() {
		return totalEmi;
	}

	public void setTotalEmi(int totalEmi) {
		this.totalEmi = totalEmi;
	}

	public String getFixedFrom() {
		return fixedFrom;
	}

	public void setFixedFrom(String fixedFrom) {
		this.fixedFrom = fixedFrom;
	}

	public String getFixedTo() {
		return fixedTo;
	}

	public void setFixedTo(String fixedTo) {
		this.fixedTo = fixedTo;
	}

	public String getLoanChargesDetail() {
		return loanChargesDetail;
	}

	public void setLoanChargesDetail(String loanChargesDetail) {
		this.loanChargesDetail = loanChargesDetail;
	}

	public List<LoanCharge> getLoanCharge() {
		return loanCharge;
	}

	public void setLoanCharge(List<LoanCharge> loanCharge) {
		this.loanCharge = loanCharge;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}
}
