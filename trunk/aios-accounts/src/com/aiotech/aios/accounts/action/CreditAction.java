package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.vo.CreditDetailVO;
import com.aiotech.aios.accounts.service.bl.CreditBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class CreditAction extends ActionSupport {

	private static final long serialVersionUID = -6809871786210276951L;
	private static final Logger LOGGER = LogManager
			.getLogger(CreditAction.class);
	private CreditBL creditBL;
	private String creditNumber;
	private String creditDetails;
	private Integer rowId;
	private long creditId;
	private long receiveId;
	private long customerId;
	private long salesDeliveryNoteId;
	private long currencyId;
	private String date;
	private String returnMessage;
	private String description;
	private List<Object> aaData;
	private Long recordId;

	public String returnSuccess() {
		LOGGER.info("Inside Account method: returnSuccess");
		recordId = null;
		return SUCCESS;
	}

	// Show Credit Notes list
	public String creditJsonList() {
		LOGGER.info("Module: Accounts : Method: creditJsonList");
		try {
			aaData = creditBL.getAllCredits(getImplementationId());
			LOGGER.info("Module: Accounts : Method: creditJsonList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: creditJsonList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCrediteNoteApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCrediteNoteApproval");
			creditId = recordId;
			showCreditEntry();
			LOG.info("Module: Accounts : Method: showCrediteNoteApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCrediteNoteApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCrediteNoteRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCrediteNoteRejectEntry");
			creditId = recordId;
			showCreditEntry();
			LOG.info("Module: Accounts : Method: showCrediteNoteRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCrediteNoteRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Credit Note Entry
	public String showCreditEntry() {
		LOGGER.info("Module: Accounts : Method: showCreditEntry");
		try {
			Credit credit = null;
			List<CreditDetailVO> creditDetailVOs = null;
			if (creditId > 0) {
				credit = creditBL.getCreditService()
						.getCreditNoteById(creditId);
				creditDetailVOs = creditBL.manipulateCreditDetail(creditBL
						.getCreditService().getCreditNoteDetails(creditId));
				CommentVO comment = new CommentVO();
				if (recordId != null && credit != null
						&& credit.getCreditId() != 0 && recordId > 0) {
					comment.setRecordId(credit.getCreditId());
					comment.setUseCase(Credit.class.getName());
					comment = creditBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else
				creditNumber = getCreditReferenceNumber();
			ServletActionContext.getRequest().setAttribute("CREDIT_INFO",
					credit);
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_DETAIL_INFO", creditDetailVOs);
			LOGGER.info("Module: Accounts : Method: showCreditEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCreditEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Credit Note
	public String saveCreditNote() {
		LOGGER.info("Module: Accounts : Method: saveCreditNote");
		try {
			Credit credit = new Credit();
			List<CreditDetail> existingCreditDetails = null;
			if (creditId > 0) {
				credit.setCreditId(creditId);
				credit.setCreditNumber(creditNumber);
				existingCreditDetails = creditBL.getCreditService()
						.getCreditDetailById(creditId);
			} else {
				credit.setCreditNumber(getCreditReferenceNumber());
			}
			credit.setDate(DateFormat.convertStringToDate(date));
			credit.setDescription(description);
			Currency currency = new Currency();
			currency.setCurrencyId(currencyId);
			credit.setCurrency(currency);
			credit.setIsCredit(false);
			credit.setIsDirectPayment(false);
			credit.setUseCase(SalesDeliveryNote.class.getSimpleName());
			credit.setRecordId(salesDeliveryNoteId);
			Customer customer = new Customer();
			customer.setCustomerId(customerId);
			credit.setCustomer(customer);
			credit.setImplementation(getImplementationId());
			CreditDetail creditDetail = null;
			List<CreditDetail> creditSalesDetails = new ArrayList<CreditDetail>();
			String[] creditValue = splitValues(creditDetails, "#@");
			SalesDeliveryDetail salesDeliveryDetail = null;
			ProductPackageDetail productPackageDetail = null;
			double inwardQty = 0;
			for (String string : creditValue) {
				inwardQty = 0;
				String[] credits = splitValues(string, "__");
				creditDetail = new CreditDetail();
				salesDeliveryDetail = new SalesDeliveryDetail();
				if (Double.parseDouble(credits[4]) > 0)
					inwardQty = Double.parseDouble(credits[4]);
				inwardQty += Double.parseDouble(credits[0]);
				creditDetail.setReturnQuantity(inwardQty);
				salesDeliveryDetail.setSalesDeliveryDetailId(Long
						.parseLong(credits[2]));
				creditDetail.setSalesDeliveryDetail(salesDeliveryDetail);
				if (Long.parseLong(credits[3]) > 0)
					creditDetail.setCreditDetailId(Long.parseLong(credits[3]));
				if (!("##").equals((credits[1])))
					creditDetail.setDescription(credits[1]);
				if (Long.parseLong(credits[5]) > 0) {
					productPackageDetail = new ProductPackageDetail();
					productPackageDetail.setProductPackageDetailId(Long
							.parseLong(credits[5]));
					creditDetail.setProductPackageDetail(productPackageDetail);
				}
				if (Double.parseDouble(credits[6]) > 0)
					creditDetail.setPackageUnit(Double.parseDouble(credits[6]));
				creditSalesDetails.add(creditDetail);
			}
			creditBL.saveCreditNote(credit, creditSalesDetails,
					existingCreditDetails);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: saveCreditNote: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: saveCreditNote: Action Success");
			return ERROR;
		}
	}

	// Delete Credit Note
	public String deleteCreditNote() {
		LOGGER.info("Module: Accounts : Method: deleteCreditNote");
		try {
			Credit credit = this.getCreditBL().getCreditService()
					.getCreditNote(creditId);
			List<CreditDetail> creditDetails = this.getCreditBL()
					.getCreditService().getCreditDetailById(creditId);
			creditBL.deleteCreditNote(credit, creditDetails);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: deleteCreditNote: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: deleteCreditNote: Action Success");
			return ERROR;
		}
	}

	public String showCreditUpdateEntry() {
		LOGGER.info("Module: Accounts : Method: showCreditUpdateEntry");
		try {
			Credit credit = this.getCreditBL().getCreditService()
					.getCreditNoteById(creditId);
			List<CreditDetail> creditList = this.getCreditBL()
					.getCreditService().getCreditDetailById(creditId);
			ServletActionContext.getRequest().setAttribute("CREDIT_INFO",
					credit);
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_DETAIL_INFO", creditList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: saveCreditNote: Action Success");
			return ERROR;
		}
	}

	private static final Implementation getImplementationId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = null;
		if (null != session.getAttribute("THIS")) {
			implementation = new Implementation();
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	private String getCreditReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(Credit.class.getName(),
				getImplementationId());
	}

	public CreditBL getCreditBL() {
		return creditBL;
	}

	public void setCreditBL(CreditBL creditBL) {
		this.creditBL = creditBL;
	}

	public String getCreditNumber() {
		return creditNumber;
	}

	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public long getCreditId() {
		return creditId;
	}

	public void setCreditId(long creditId) {
		this.creditId = creditId;
	}

	public long getReceiveId() {
		return receiveId;
	}

	public void setReceiveId(long receiveId) {
		this.receiveId = receiveId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCreditDetails() {
		return creditDetails;
	}

	public void setCreditDetails(String creditDetails) {
		this.creditDetails = creditDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getSalesDeliveryNoteId() {
		return salesDeliveryNoteId;
	}

	public void setSalesDeliveryNoteId(long salesDeliveryNoteId) {
		this.salesDeliveryNoteId = salesDeliveryNoteId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
