package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetInsurance;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.service.bl.AssetClaimInsuranceBL;
import com.aiotech.aios.common.to.Constants.Accounts.POStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentRequestMode;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AssetClaimInsuranceAction extends ActionSupport {

	private static final long serialVersionUID = -3036976967539062816L;

	private static final Logger log = Logger
			.getLogger(AssetClaimInsuranceAction.class);

	// Dependency
	private AssetClaimInsuranceBL assetClaimInsuranceBL;

	// Variables
	private long assetClaimInsuranceId;
	private long assetInsuranceId;
	private Long recordId;
	private long alertId;
	private long payOutNature;
	private double claimAmount;
	private double claimExpense;
	private double payOutAmount;
	private byte paymentMode;
	private byte status;
	private String claimDate;
	private String description;
	private String referenceNumber;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show all Asset Insurance
	public String showAllAssetClaimInsurance() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAssetClaimInsurance");
			aaData = assetClaimInsuranceBL.showAllAssetClaimInsurance();
			log.info("Module: Accounts : Method: showAllAssetClaimInsurance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllAssetClaimInsurance Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showAssetClaimInsuranceApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetClaimInsuranceApproval");
			assetClaimInsuranceId = recordId;
			showAssetClaimInsuranceEntry();
			log.info("Module: Accounts : Method: showAssetClaimInsuranceApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetClaimInsuranceApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetClaimInsuranceReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetClaimInsuranceReject");
			assetClaimInsuranceId = recordId;
			showAssetClaimInsuranceEntry();
			log.info("Module: Accounts : Method: showAssetClaimInsuranceReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetClaimInsuranceReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Claim Insurance
	public String showAssetClaimInsuranceEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetClaimInsuranceEntry");
			if (assetClaimInsuranceId > 0) {
				AssetClaimInsurance assetClaimInsurance = assetClaimInsuranceBL
						.getAssetClaimInsuranceService()
						.getAssetClaimInsuranceByClaimId(assetClaimInsuranceId);
				ServletActionContext.getRequest().setAttribute(
						"ASSET_CLAIM_INSURANCE", assetClaimInsurance);
				CommentVO comment = new CommentVO();
				if (recordId != null && assetClaimInsurance != null
						&& assetClaimInsurance.getAssetClaimInsuranceId() != 0
						&& recordId > 0) {
					comment.setRecordId(assetClaimInsurance.getAssetClaimInsuranceId());
					comment.setUseCase(AssetClaimInsurance.class.getName());
					comment = assetClaimInsuranceBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}else{
				referenceNumber = SystemBL.getReferenceStamp(
						AssetClaimInsurance.class.getName(),
						assetClaimInsuranceBL.getImplementation());
			}
			Map<Byte, POStatus> status = new HashMap<Byte, POStatus>();
			for (POStatus e : EnumSet.allOf(POStatus.class))
				status.put(e.getCode(), e);
			Map<Byte, PaymentRequestMode> modeOfPayment = new HashMap<Byte, PaymentRequestMode>();
			for (PaymentRequestMode e : EnumSet.allOf(PaymentRequestMode.class))
				modeOfPayment.put(e.getCode(), e);
			ServletActionContext.getRequest()
					.setAttribute(
							"PAYOUT_NATURE",
							assetClaimInsuranceBL.getLookupMasterBL()
									.getActiveLookupDetails(
											"CLAIM_PAYOUT_NATURE", true));
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					modeOfPayment);
			ServletActionContext.getRequest().setAttribute("CLAIM_STATUS",
					status); 
			log.info("Module: Accounts : Method: showAssetClaimInsuranceEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetClaimInsuranceEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String claimInsuranceDirectPayment() {
		try {
			log.info("Inside Module: Accounts : Method: claimInsuranceDirectPayment");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String paymentNumber = "";
			DirectPayment directPayment = null;
			assetClaimInsuranceId = recordId;
			List<Currency> currencyList = null;
			AssetClaimInsurance claimInsurance = assetClaimInsuranceBL
					.getAssetClaimInsuranceService()
					.getAssetClaimInsuranceByClaimId(assetClaimInsuranceId);
			if (claimInsurance != null) {
				currencyList = assetClaimInsuranceBL
						.getDirectPaymentBL()
						.getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(
								assetClaimInsuranceBL.getImplementation());
				directPayment = new DirectPayment();

				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(),
						assetClaimInsuranceBL.getImplementation());
				directPayment.setPersonByCreatedBy(person);
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(claimInsurance
						.getAssetClaimInsuranceId());
				directPayment.setUseCase(claimInsurance.getClass()
						.getSimpleName());
				// Direct Payment details creation
				List<DirectPaymentDetail> directPayDetails = new ArrayList<DirectPaymentDetail>();
				DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
				directPaymentDetail
						.setAmount(claimInsurance.getClaimExpenses());
				directPaymentDetail.setInvoiceNumber(claimInsurance
						.getReferenceNumber());
				directPayDetails.add(directPaymentDetail);
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								directPayDetails));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_MODE",
					assetClaimInsuranceBL.getDirectPaymentBL()
							.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute(
					"PAYEE_TYPES",
					assetClaimInsuranceBL.getDirectPaymentBL()
							.getReceiptSource());

			log.info("Module: Accounts : Method: claimInsuranceDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: claimInsuranceDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Claim Insurance
	public String saveAssetClaimInsurance() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetInsurance");
			assetClaimInsuranceBL
					.saveAssetClaimInsurance(setAssetClaimInsurance());
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetInsurance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetInsurance: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Asset Claim Insurance
	public String deleteAssetClaimInsurance() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAssetClaimInsurance");
			assetClaimInsuranceBL
					.deleteAssetClaimInsurance(assetClaimInsuranceBL
							.getAssetClaimInsuranceService()
							.getSimpleAssetClaimInsuranceByClaimId(
									assetClaimInsuranceId));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetClaimInsurance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: deleteAssetClaimInsurance: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Set Asset Claim Insurance
	private AssetClaimInsurance setAssetClaimInsurance() {
		AssetClaimInsurance assetClaimInsurance = new AssetClaimInsurance();
		assetClaimInsurance
				.setAssetClaimInsuranceId(assetClaimInsuranceId > 0 ? assetClaimInsuranceId
						: null);
		AssetInsurance assetInsurance = new AssetInsurance();
		assetInsurance.setAssetInsuranceId(assetInsuranceId);
		assetClaimInsurance.setAssetInsurance(assetInsurance);
		assetClaimInsurance.setClaimDate(DateFormat
				.convertStringToDate(claimDate));
		assetClaimInsurance.setClaimAmount(claimAmount);
		assetClaimInsurance.setClaimExpenses(claimExpense > 0 ? claimExpense
				: null);
		assetClaimInsurance.setDescription(description);
		assetClaimInsurance.setModeOfPayment(paymentMode > 0 ? paymentMode
				: null);
		assetClaimInsurance.setStatus(status);
		assetClaimInsurance.setPayoutAmount(payOutAmount > 0 ? payOutAmount
				: null);
		if (payOutNature > 0) {
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(payOutNature);
			assetClaimInsurance.setLookupDetail(lookupDetail);
		}
		assetClaimInsurance
				.setReferenceNumber(assetClaimInsuranceId == 0 ? SystemBL
						.getReferenceStamp(AssetClaimInsurance.class.getName(),
								assetClaimInsuranceBL.getImplementation())
						: referenceNumber);
		return assetClaimInsurance;
	}

	// Getters & Setters
	public AssetClaimInsuranceBL getAssetClaimInsuranceBL() {
		return assetClaimInsuranceBL;
	}

	public void setAssetClaimInsuranceBL(
			AssetClaimInsuranceBL assetClaimInsuranceBL) {
		this.assetClaimInsuranceBL = assetClaimInsuranceBL;
	}

	public long getAssetClaimInsuranceId() {
		return assetClaimInsuranceId;
	}

	public void setAssetClaimInsuranceId(long assetClaimInsuranceId) {
		this.assetClaimInsuranceId = assetClaimInsuranceId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getAssetInsuranceId() {
		return assetInsuranceId;
	}

	public void setAssetInsuranceId(long assetInsuranceId) {
		this.assetInsuranceId = assetInsuranceId;
	}

	public long getPayOutNature() {
		return payOutNature;
	}

	public void setPayOutNature(long payOutNature) {
		this.payOutNature = payOutNature;
	}

	public double getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(double claimAmount) {
		this.claimAmount = claimAmount;
	}

	public double getClaimExpense() {
		return claimExpense;
	}

	public void setClaimExpense(double claimExpense) {
		this.claimExpense = claimExpense;
	}

	public double getPayOutAmount() {
		return payOutAmount;
	}

	public void setPayOutAmount(double payOutAmount) {
		this.payOutAmount = payOutAmount;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}
}
