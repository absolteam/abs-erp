package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.MaterialWastage;
import com.aiotech.aios.accounts.domain.entity.MaterialWastageDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialWastageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialWastageVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.MaterialWastageBL;
import com.aiotech.aios.common.to.Constants.Accounts.WastageTypes;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MaterialWastageAction extends ActionSupport {

	private static final long serialVersionUID = 9130282613188403642L;

	private static final Logger log = Logger
			.getLogger(MaterialWastageAction.class);

	private long materialWastageId;
	private long personId;
	private int rowId;
	private boolean productionTypeWastage;
	private String referenceNumber;
	private String wastageDate;
	private String description;
	private String personName;
	private String returnMessage;
	private String wastageDetails;
	private List<Object> aaData;

	private MaterialWastageBL materialWastageBL;

	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showAllMaterialWastages() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMaterialWastages");
			aaData = materialWastageBL.getAllMaterialWastages();
			log.info("Module: Accounts : Method: showAllMaterialWastages: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllMaterialWastages Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// entry
	public String showMaterialWastageEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialWastageEntry");
			MaterialWastageVO materialWastageVO = null;
			if (materialWastageId > 0) {
				MaterialWastage materialWastage = materialWastageBL
						.getMaterialWastageService().getMaterialWastagebyId(
								materialWastageId);
				materialWastageVO = new MaterialWastageVO();
				BeanUtils.copyProperties(materialWastageVO, materialWastage);
				MaterialWastageDetailVO materialWastageDetailVO = null;
				List<MaterialWastageDetailVO> materialWastageDetailVOs = new ArrayList<MaterialWastageDetailVO>();
				for (MaterialWastageDetail wastageDetail : materialWastageVO
						.getMaterialWastageDetails()) {
					if ((boolean) wastageDetail.getStatus()) {
						materialWastageDetailVO = new MaterialWastageDetailVO();
						BeanUtils.copyProperties(materialWastageDetailVO,
								wastageDetail);
						if (null != wastageDetail.getShelf()) {
							List<StockVO> stockVOs = materialWastageBL
									.getStockBL().getStockDetails(
											wastageDetail.getProduct()
													.getProductId(),
											wastageDetail.getShelf()
													.getShelfId());
							for (StockVO stockVO : stockVOs) {
								materialWastageDetailVO
										.setAvailableQuantity(materialWastageDetailVO
												.getAvailableQuantity()
												+ stockVO
														.getAvailableQuantity());
							}
						}
						if (null != wastageDetail.getProductPackageDetail()) {
							materialWastageDetailVO
									.setPackageDetailId(wastageDetail
											.getProductPackageDetail()
											.getProductPackageDetailId());
							materialWastageDetailVO
									.setBaseUnitName(wastageDetail.getProduct()
											.getLookupDetailByProductUnit()
											.getDisplayName());
						} else {
							materialWastageDetailVO
									.setPackageUnit(wastageDetail
											.getWastageQuantity());
							materialWastageDetailVO.setPackageDetailId(-1l);
						}
						materialWastageDetailVO
								.setProductPackageVOs(materialWastageBL
										.getStockBL()
										.getPackageConversionBL()
										.getProductPackagingDetail(
												wastageDetail.getProduct()
														.getProductId()));
						materialWastageDetailVOs.add(materialWastageDetailVO);
					}
				}
				if (null != materialWastageDetailVOs
						&& materialWastageDetailVOs.size() > 0) {
					Collections.sort(materialWastageDetailVOs,
							new Comparator<MaterialWastageDetailVO>() {
								public int compare(MaterialWastageDetailVO o1,
										MaterialWastageDetailVO o2) {
									return o1
											.getMaterialWastageDetailId()
											.compareTo(
													o2.getMaterialWastageDetailId());
								}
							});
					materialWastageVO
							.setMaterialWastageDetailVOs(materialWastageDetailVOs);
				}

				materialWastageVO.setDate(DateFormat
						.convertDateToString(materialWastage.getWastageDate()
								.toString()));
			} else {
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				User user = (User) sessionObj.get("USER");
				referenceNumber = SystemBL.getReferenceStamp(
						MaterialWastage.class.getName(),
						materialWastageBL.getImplementation());
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute(
					"WASTAGE_TYPE",
					materialWastageBL.getLookupMasterBL()
							.getActiveLookupDetails("MATERIAL_WASTAGE_TYPE",
									true));
			ServletActionContext.getRequest().setAttribute("WASTAGE_DETAIL",
					materialWastageVO);
			log.info("Module: Accounts : Method: showMaterialWastageEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialWastageEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveMaterialWastage() {
		try {
			log.info("Inside Module: Accounts : Method: saveMaterialWastage");
			MaterialWastage materialWastage = new MaterialWastage();
			materialWastage.setDescription(description);
			Person person = new Person();
			person.setPersonId(personId);
			materialWastage.setPerson(person);
			materialWastage.setStatus(true);
			materialWastage.setWastageDate(DateFormat
					.convertStringToDate(wastageDate));
			if (productionTypeWastage)
				materialWastage.setWastageType(WastageTypes.Production_Wastage
						.getCode());
			else
				materialWastage.setWastageType(WastageTypes.General_Wastage
						.getCode());
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(wastageDetails);
			JSONArray object = (JSONArray) receiveObject;
			MaterialWastageDetail materialWastageDetail = null;
			Product product = null;
			LookupDetail lookupDetail = null;
			Shelf shelf = null;
			Store store = null;
			List<MaterialWastageDetail> materialWastageDetails = new ArrayList<MaterialWastageDetail>();
			List<MaterialWastageDetail> deletedMaterialWastageDetails = null;
			ProductPackageDetail productPackageDetail = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				materialWastageDetail = new MaterialWastageDetail();
				product = new Product();
				person = new Person();
				person.setPersonId((null != jsonObject.get("wastagePerson") && Long
						.parseLong(jsonObject.get("wastagePerson").toString()) > 0) ? Long
						.parseLong(jsonObject.get("wastagePerson").toString())
						: null);
				product.setProductId(Long.parseLong(jsonObject.get("productId")
						.toString()));
				if ((null != jsonObject.get("shelfId") && Long
						.parseLong(jsonObject.get("shelfId").toString()) > 0)) {
					shelf = new Shelf();
					shelf.setShelfId(Integer.parseInt(jsonObject.get("shelfId")
							.toString()));
					materialWastageDetail.setShelf(shelf);
				}
				if ((null != jsonObject.get("storeId") && Long
						.parseLong(jsonObject.get("storeId").toString()) > 0)) {
					store = new Store();
					store.setStoreId(Long.parseLong(jsonObject.get("storeId")
							.toString()));
					materialWastageDetail.setStore(store);
				}
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(Long.parseLong(jsonObject.get(
						"wastageType").toString()));
				materialWastageDetail.setWastageQuantity(Double
						.parseDouble(jsonObject.get("wastageQuantity")
								.toString()));
				materialWastageDetail
						.setMaterialWastageDetailId((null != jsonObject
								.get("materialWastageDetailId") && Long
								.parseLong(jsonObject.get(
										"materialWastageDetailId").toString()) > 0) ? Long
								.parseLong(jsonObject.get(
										"materialWastageDetailId").toString())
								: null);
				materialWastageDetail.setStatus(true);
				materialWastageDetail.setProduct(product);
				materialWastageDetail.setLookupDetail(lookupDetail);
				if ((null != jsonObject.get("packageType") && Long
						.parseLong(jsonObject.get("packageType").toString()) > 0)) {
					productPackageDetail = new ProductPackageDetail();
					productPackageDetail
							.setProductPackageDetailId(Long
									.parseLong(jsonObject.get("packageType")
											.toString()));
					materialWastageDetail
							.setProductPackageDetail(productPackageDetail);
				}
				materialWastageDetail.setPackageUnit(Double
						.parseDouble(jsonObject.get("packageUnit").toString()));
				if (null != person && null != person.getPersonId())
					materialWastageDetail.setPerson(person);
				materialWastageDetails.add(materialWastageDetail);
			}
			List<MaterialWastageDetail> existingWastageDetails = null;
			if (materialWastageId > 0) {
				materialWastage.setMaterialWastageId(materialWastageId);
				materialWastage.setReferenceNumber(referenceNumber);
				existingWastageDetails = materialWastageBL
						.getMaterialWastageService().getMaterialWastageDetails(
								materialWastageId);
				deletedMaterialWastageDetails = getUserDeletedWastageDetails(
						existingWastageDetails, materialWastageDetails);
			} else {
				materialWastage.setReferenceNumber(SystemBL.getReferenceStamp(
						MaterialWastage.class.getName(),
						materialWastageBL.getImplementation()));
			}
			materialWastageBL.saveMaterialWastage(materialWastage,
					materialWastageDetails, deletedMaterialWastageDetails,
					existingWastageDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMaterialWastage: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: saveMaterialWastage Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteMaterialWastage() {
		try {
			log.info("Inside Module: Accounts : Method: deleteMaterialWastage");
			MaterialWastage materialWastage = materialWastageBL
					.getMaterialWastageService().getMaterialWastagebyId(
							materialWastageId);
			materialWastageBL.deleteMaterialWastage(
					materialWastage,
					new ArrayList<MaterialWastageDetail>(materialWastage
							.getMaterialWastageDetails()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMaterialIdleMix: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: deleteMaterialWastage Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<MaterialWastageDetail> getUserDeletedWastageDetails(
			List<MaterialWastageDetail> existingWastageDetails,
			List<MaterialWastageDetail> materialWastageDetails)
			throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, MaterialWastageDetail> hashPurchaseDetail = new HashMap<Long, MaterialWastageDetail>();
		if (null != existingWastageDetails && existingWastageDetails.size() > 0) {
			for (MaterialWastageDetail list : existingWastageDetails) {
				listOne.add(list.getMaterialWastageDetailId());
				hashPurchaseDetail.put(list.getMaterialWastageDetailId(), list);
			}
			if (null != hashPurchaseDetail && hashPurchaseDetail.size() > 0) {
				for (MaterialWastageDetail list : materialWastageDetails) {
					listTwo.add(list.getMaterialWastageDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<MaterialWastageDetail> deletedWastageDetails = null;
		if (null != different && different.size() > 0) {
			MaterialWastageDetail tempWastageDetail = null;
			deletedWastageDetails = new ArrayList<MaterialWastageDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempWastageDetail = new MaterialWastageDetail();
					tempWastageDetail = hashPurchaseDetail.get(list);
					deletedWastageDetails.add(tempWastageDetail);
				}
			}
		}
		return deletedWastageDetails;
	}

	public String showMaterialWastageAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialWastageAddRow");
			ServletActionContext.getRequest().setAttribute(
					"WASTAGE_TYPE",
					materialWastageBL.getLookupMasterBL()
							.getActiveLookupDetails("MATERIAL_WASTAGE_TYPE",
									true));
			log.info("Module: Accounts : Method: showMaterialWastageAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialWastageAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public long getMaterialWastageId() {
		return materialWastageId;
	}

	public void setMaterialWastageId(long materialWastageId) {
		this.materialWastageId = materialWastageId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getWastageDate() {
		return wastageDate;
	}

	public void setWastageDate(String wastageDate) {
		this.wastageDate = wastageDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getWastageDetails() {
		return wastageDetails;
	}

	public void setWastageDetails(String wastageDetails) {
		this.wastageDetails = wastageDetails;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public MaterialWastageBL getMaterialWastageBL() {
		return materialWastageBL;
	}

	public void setMaterialWastageBL(MaterialWastageBL materialWastageBL) {
		this.materialWastageBL = materialWastageBL;
	}

	public boolean isProductionTypeWastage() {
		return productionTypeWastage;
	}

	public void setProductionTypeWastage(boolean productionTypeWastage) {
		this.productionTypeWastage = productionTypeWastage;
	}
}
