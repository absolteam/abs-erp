package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetService;
import com.aiotech.aios.accounts.domain.entity.AssetServiceDetail;
import com.aiotech.aios.accounts.service.bl.AssetServiceBL;
import com.aiotech.aios.common.to.Constants.Accounts.AssetPriorityType;
import com.aiotech.aios.common.to.Constants.Accounts.AssetServiceLevel;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class AssetServiceAction extends ActionSupport {

	private static final long serialVersionUID = 9066286836713473966L;

	private static final Logger log = Logger
			.getLogger(AssetServiceAction.class);

	private AssetServiceBL assetServiceBL;
	private List<Object> aaData;

	private long assetServiceId;
	private long assetCreationId;
	private long serviceTerm;
	private long contractType;
	private long serviceInChargeId;
	private long serviceType;
	private Long recordId;
	private byte serviceLevel;
	private String contactNumber;
	private String contactName;
	private String provider;
	private String contractNumber;
	private String serviceDetails;
	private String description;
	private String serviceNumber;
	private String returnMessage;
	private Integer rowId;
	private double contractAmount;

	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show All Asset Check In JSON list
	public String showAssetServiceJsonList() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceJsonList");
			aaData = assetServiceBL.showAssetServiceJsonList();
			log.info("Module: Accounts : Method: showAssetServiceJsonList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceJsonList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetServiceApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceApproval");
			assetServiceId = recordId;
			showAssetServiceEntry();
			log.info("Module: Accounts : Method: showAssetServiceApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetServiceReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceReject");
			assetServiceId = recordId;
			showAssetServiceEntry();
			log.info("Module: Accounts : Method: showAssetServiceReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	// Show Asset Check In Screen entry
	public String showAssetServiceEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceEntry");
			if (assetServiceId > 0) {
				AssetService assetService = assetServiceBL
						.getAssetServiceService().getAssetServiceDetails(
								assetServiceId);
				ServletActionContext.getRequest().setAttribute("ASSET_SERVICE",
						assetService);
				CommentVO comment = new CommentVO();
				if (recordId != null && assetService != null
						&& assetService.getAssetServiceId() != 0 && recordId > 0) {
					comment.setRecordId(assetService.getAssetServiceId());
					comment.setUseCase(AssetService.class.getName());
					comment = assetServiceBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			} else 
				serviceNumber = getAssetServiceReference();
			List<LookupDetail> assetServiceTerms = assetServiceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"ASSET_SERVICE_TERM", true);
			List<LookupDetail> assetContractTypes = assetServiceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"ASSET_CONTRACT_TYPE", true);
			List<LookupDetail> assetServiceType = assetServiceBL
					.getLookupMasterBL().getActiveLookupDetails(
							"ASSET_SERVICE_TYPE", true);
			Map<Byte, AssetServiceLevel> assetServiceLevel = new HashMap<Byte, AssetServiceLevel>();
			for (AssetServiceLevel e : EnumSet.allOf(AssetServiceLevel.class)) {
				assetServiceLevel.put(e.getCode(), e);
			}
			Map<Byte, AssetPriorityType> assetPriorityType = new HashMap<Byte, AssetPriorityType>();
			for (AssetPriorityType e : EnumSet.allOf(AssetPriorityType.class)) {
				assetPriorityType.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute(
					"ASSET_SERVICE_TERM", assetServiceTerms);
			ServletActionContext.getRequest().setAttribute(
					"ASSET_CONTRACT_TYPE", assetContractTypes);
			ServletActionContext.getRequest().setAttribute(
					"ASSET_SERVICE_TYPE", assetServiceType);
			ServletActionContext.getRequest().setAttribute(
					"ASSET_SERVICE_LEVEL", assetServiceLevel);
			ServletActionContext.getRequest().setAttribute(
					"ASSET_PRIORITY_TYPE", assetPriorityType);
			log.info("Module: Accounts : Method: showAssetServiceEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Creation
	public String saveAssetService() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetService");
			AssetService assetService = new AssetService();
			AssetCreation assetCreation = new AssetCreation();
			assetCreation.setAssetCreationId(assetCreationId);
			assetService.setAssetCreation(assetCreation);
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(serviceTerm);
			assetService.setLookupDetailByServiceTerms(lookupDetail);
			lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(contractType);
			assetService.setLookupDetailByContractType(lookupDetail);
			Person person = new Person();
			person.setPersonId(serviceInChargeId);
			assetService.setPerson(person);
			lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(serviceType);
			assetService.setLookupDetailByServiceType(lookupDetail);
			assetService.setServiceLevel(serviceLevel);
			assetService.setContactNumber(contactNumber);
			assetService.setContactPerson(contactName);
			assetService.setProvider(provider);
			assetService.setContractAmount(contractAmount);
			assetService.setContactNumber(contactNumber);
			assetService.setDescription(description);
			assetService.setContractNumber(contractNumber);
			List<AssetServiceDetail> assetServiceDetails = null;
			List<AssetServiceDetail> deleteAssetServiceDetails = null;
			if (null != serviceDetails && !("").equals(serviceDetails)) {
				assetServiceDetails = new ArrayList<AssetServiceDetail>();
				String[] serviceDetailArray = splitValues(serviceDetails, "#@");
				for (String serviceDetail : serviceDetailArray) {
					assetServiceDetails
							.add(addAssetServiceDetail(serviceDetail));
				}
			}
			if (assetServiceId > 0) {
				assetService.setAssetServiceId(assetServiceId);
				assetService.setServiceNumber(serviceNumber);
				deleteAssetServiceDetails = userDeletedAssetServiceDetails(
						assetServiceDetails,
						assetServiceBL.getAssetServiceService()
								.getAssetServiceDetailByServiceId(
										assetServiceId));
			} else
				assetService.setServiceNumber(getAssetServiceReference());
			assetServiceBL.saveAssetService(assetService, assetServiceDetails,
					deleteAssetServiceDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetService Success ");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Record creation failure.";
			log.info("Module: Accounts : Method: saveAssetService Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Asset Service
	public String deleteAssetService() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAssetService");
			AssetService assetService = assetServiceBL.getAssetServiceService()
					.getAssetServiceDetails(assetServiceId);
			assetServiceBL.deleteAssetService(assetService);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetService Success ");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Record deletion failure.";
			log.info("Module: Accounts : Method: deleteAssetService Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Add Asset Service Detail
	private AssetServiceDetail addAssetServiceDetail(String serviceDetail) {
		AssetServiceDetail assetServiceDetail = new AssetServiceDetail();
		String serviceDetails[] = splitValues(serviceDetail, "__");
		assetServiceDetail.setServiceDate(DateFormat
				.convertStringToDate(serviceDetails[0]));
		assetServiceDetail.setExtimatedDate(DateFormat
				.convertStringToDate(serviceDetails[1]));

		if (Long.parseLong(serviceDetails[2]) > 0) {
			Person person = new Person();
			person.setPersonId(Long.parseLong(serviceDetails[2]));
			assetServiceDetail.setPerson(person);
		}
		assetServiceDetail.setNextServiceDate(DateFormat
				.convertStringToDate(serviceDetails[3]));
		assetServiceDetail.setPriority(Byte.valueOf(serviceDetails[4]));
		if (null != serviceDetails[5] && !("##").equals(serviceDetails[5]))
			assetServiceDetail.setDescription(serviceDetails[5]);
		if (Long.parseLong(serviceDetails[6]) > 0l) {
			assetServiceDetail.setAssetServiceDetailId(Long
					.parseLong(serviceDetails[6]));
		}
		return assetServiceDetail;
	}

	// Get user deleted Asset Service Details
	private List<AssetServiceDetail> userDeletedAssetServiceDetails(
			List<AssetServiceDetail> assetServiceDetails,
			List<AssetServiceDetail> assetServiceDetailsOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, AssetServiceDetail> hashAssetDetails = new HashMap<Long, AssetServiceDetail>();
		if (null != assetServiceDetailsOld && assetServiceDetailsOld.size() > 0) {
			for (AssetServiceDetail list : assetServiceDetailsOld) {
				listOne.add(list.getAssetServiceDetailId());
				hashAssetDetails.put(list.getAssetServiceDetailId(), list);
			}
			for (AssetServiceDetail list : assetServiceDetails) {
				listTwo.add(list.getAssetServiceDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<AssetServiceDetail> assetServiceDetailsFinal = new ArrayList<AssetServiceDetail>();
		if (null != different && different.size() > 0) {
			AssetServiceDetail tempAssetDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempAssetDetail = new AssetServiceDetail();
					tempAssetDetail = hashAssetDetails.get(list);
					assetServiceDetailsFinal.add(tempAssetDetail);
				}
			}
		}
		return assetServiceDetailsFinal;
	}

	// Show Asset Service Details
	public String showAssetServiceDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceDetails");
			List<AssetService> assetServices = assetServiceBL
					.getAssetServiceService().getAllAssetServiceDetails(
							assetServiceBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("ASSET_SERVICES",
					assetServices);
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Add Row
	public String assetServiceAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: assetServiceAddRow");
			Map<Byte, AssetPriorityType> assetPriorityType = new HashMap<Byte, AssetPriorityType>();
			for (AssetPriorityType e : EnumSet.allOf(AssetPriorityType.class)) {
				assetPriorityType.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute(
					"ASSET_PRIORITY_TYPE", assetPriorityType);
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: assetServiceAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}

	}
	
	// Generate Asset Service Reference
	public String getAssetServiceReference() throws Exception { 
		return SystemBL.getReferenceStamp(AssetService.class.getName(), assetServiceBL.getImplementation());
	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public AssetServiceBL getAssetServiceBL() {
		return assetServiceBL;
	}

	public void setAssetServiceBL(AssetServiceBL assetServiceBL) {
		this.assetServiceBL = assetServiceBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getAssetServiceId() {
		return assetServiceId;
	}

	public void setAssetServiceId(long assetServiceId) {
		this.assetServiceId = assetServiceId;
	}

	public long getAssetCreationId() {
		return assetCreationId;
	}

	public void setAssetCreationId(long assetCreationId) {
		this.assetCreationId = assetCreationId;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getServiceTerm() {
		return serviceTerm;
	}

	public void setServiceTerm(long serviceTerm) {
		this.serviceTerm = serviceTerm;
	}

	public long getContractType() {
		return contractType;
	}

	public void setContractType(long contractType) {
		this.contractType = contractType;
	}

	public long getServiceInChargeId() {
		return serviceInChargeId;
	}

	public void setServiceInChargeId(long serviceInChargeId) {
		this.serviceInChargeId = serviceInChargeId;
	}

	public long getServiceType() {
		return serviceType;
	}

	public void setServiceType(long serviceType) {
		this.serviceType = serviceType;
	}

	public byte getServiceLevel() {
		return serviceLevel;
	}

	public void setServiceLevel(byte serviceLevel) {
		this.serviceLevel = serviceLevel;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getServiceDetails() {
		return serviceDetails;
	}

	public void setServiceDetails(String serviceDetails) {
		this.serviceDetails = serviceDetails;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
