package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionVO;
import com.aiotech.aios.accounts.service.bl.MaterialRequisitionBL;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MaterialRequisitionAction extends ActionSupport {

	private static final long serialVersionUID = 8464190878329894045L;

	private static Logger log = Logger
			.getLogger(MaterialRequisitionAction.class);

	// Dependency
	private MaterialRequisitionBL materialRequisitionBL;
	// Variables
	private long materialRequisitionId;
	private int rowId;
	private long storeId;
	private String referenceNumber;
	private String requisitionDate;
	private String description;
	private String returnMessage;
	private String printableContent;
	private String requisitionDetails;
	private List<Object> aaData;
	private Long alertId;
	private Long recordId;
	private Long messageId;
	private Long personId;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show All Material Requisition
	public String showAllMaterialRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMaterialRequisition");
			aaData = materialRequisitionBL.showAllMaterialRequisition();
			log.info("Module: Accounts : Method: showAllMaterialRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllMaterialRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	//
	public String showMaterialRequisitionApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showMaterialRequisitionApproval");
			materialRequisitionId = recordId;
			showMaterialRequisitionEntry();
			LOG.info("Module: Accounts : Method: showMaterialRequisitionApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showMaterialRequisitionApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMaterialRequisitionRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showMaterialRequisitionRejectEntry");
			materialRequisitionId = recordId;
			showMaterialRequisitionEntry();
			LOG.info("Module: Accounts : Method: showMaterialRequisitionRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showMaterialRequisitionRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMaterialRequisitionPrintout() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialRequisitionPrintout");
			MaterialRequisitionVO materialRequisitionVO = new MaterialRequisitionVO();
			if (materialRequisitionId > 0) {
				MaterialRequisition materialRequisition = materialRequisitionBL
						.getMaterialRequisitionService()
						.getMaterialRequisitionById(materialRequisitionId);
				MaterialRequisitionDetailVO materialRequisitionDetailVO = null;
				List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();
				Set<String> transferNumbers = new HashSet<String>();
				for (MaterialRequisitionDetail requisitionDetail : materialRequisition
						.getMaterialRequisitionDetails()) {
					materialRequisitionDetailVO = new MaterialRequisitionDetailVO();
					BeanUtils.copyProperties(materialRequisitionDetailVO,
							requisitionDetail);
					double issuedQty = 0;
					if (null != requisitionDetail.getMaterialTransferDetails()
							&& requisitionDetail.getMaterialTransferDetails()
									.size() > 0) {

						for (MaterialTransferDetail materialTransferDetail : requisitionDetail
								.getMaterialTransferDetails()) {
							issuedQty += materialTransferDetail.getQuantity();
							transferNumbers
									.add(materialTransferDetail
											.getMaterialTransfer()
											.getReferenceNumber());
						}
					}
					materialRequisitionDetailVO.setRequisitionQty(AIOSCommons
							.roundDecimals(requisitionDetail.getQuantity()));
					materialRequisitionDetailVO.setTransferedQty(AIOSCommons
							.roundDecimals(issuedQty));
					materialRequisitionDetailVO.setPendingQty(AIOSCommons
							.roundDecimals(requisitionDetail.getQuantity()
									- issuedQty));
					materialRequisitionDetailVOs
							.add(materialRequisitionDetailVO);
				}
				Collections.sort(materialRequisitionDetailVOs,
						new Comparator<MaterialRequisitionDetailVO>() {
							public int compare(MaterialRequisitionDetailVO o1,
									MaterialRequisitionDetailVO o2) {
								return o1
										.getMaterialRequisitionDetailId()
										.compareTo(
												o2.getMaterialRequisitionDetailId());
							}
						});
				BeanUtils.copyProperties(materialRequisitionVO,
						materialRequisition);
				String transferNumber = "";
				if (null != transferNumbers && transferNumbers.size() > 0) {
					transferNumber = Arrays.toString(transferNumbers.toArray());
					transferNumber = transferNumber.substring(1,
							transferNumber.length() - 1);
				}
				materialRequisitionVO.setTransferReference(transferNumber);
				materialRequisitionVO.setDate(DateFormat
						.convertDateToString(materialRequisition
								.getRequisitionDate().toString()));
				materialRequisitionVO.setStatus(RequisitionStatus.get(
						materialRequisition.getRequisitionStatus()).name());
				materialRequisitionVO.setStoreName(materialRequisition
						.getStore().getStoreName());
				materialRequisitionVO
						.setMaterialRequisitionDetailVOs(materialRequisitionDetailVOs);
				ServletActionContext.getRequest().setAttribute(
						"MATERIAL_REQUISITION", materialRequisitionVO);

			}
			log.info("Module: Accounts : Method: showMaterialRequisitionPrintout: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialRequisitionPrintout Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Material Requisition Entry
	public String showMaterialRequisitionEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialRequisitionEntry");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			MaterialRequisitionVO materialRequisitionVO = new MaterialRequisitionVO();
			if (materialRequisitionId > 0) {
				MaterialRequisition materialRequisition = materialRequisitionBL
						.getMaterialRequisitionService()
						.getMaterialRequisitionById(materialRequisitionId);
				CommentVO comment = new CommentVO();
				if (recordId != null && materialRequisition != null
						&& materialRequisition.getMaterialRequisitionId() != 0
						&& recordId > 0) {
					comment.setRecordId(materialRequisition
							.getMaterialRequisitionId());
					comment.setUseCase(MaterialRequisition.class.getName());
					comment = materialRequisitionBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
				MaterialRequisitionDetailVO materialRequisitionDetailVO = null;
				List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();
				for (MaterialRequisitionDetail requisitionDetail : materialRequisition
						.getMaterialRequisitionDetails()) {
					materialRequisitionDetailVO = new MaterialRequisitionDetailVO();
					BeanUtils.copyProperties(materialRequisitionDetailVO,
							requisitionDetail);
					materialRequisitionDetailVOs
							.add(materialRequisitionDetailVO);
				}
				Collections.sort(materialRequisitionDetailVOs,
						new Comparator<MaterialRequisitionDetailVO>() {
							public int compare(MaterialRequisitionDetailVO o1,
									MaterialRequisitionDetailVO o2) {
								return o1
										.getMaterialRequisitionDetailId()
										.compareTo(
												o2.getMaterialRequisitionDetailId());
							}
						});
				BeanUtils.copyProperties(materialRequisitionVO,
						materialRequisition);
				materialRequisitionVO
						.setMaterialRequisitionDetailVOs(materialRequisitionDetailVOs);
			} else {
				MaterialRequisition materialRequisition = new MaterialRequisition();
				referenceNumber = SystemBL.getReferenceStamp(
						MaterialRequisition.class.getName(),
						materialRequisitionBL.getImplementation());

				if (ServletActionContext.getRequest().getParameter("useCase") != null) {
					materialRequisition.setUseCase(ServletActionContext
							.getRequest().getParameter("useCase").toString());
					materialRequisition.setRecordId(Long
							.valueOf(ServletActionContext.getRequest()
									.getParameter("projectId").toString()));
				}
				BeanUtils.copyProperties(materialRequisitionVO,
						materialRequisition);
			}
			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_REQUISITION", materialRequisitionVO);

			List<Store> stores = materialRequisitionBL.getStoreBL()
					.getStoreService()
					.getAllStores(materialRequisitionBL.getImplementation());
			ServletActionContext.getRequest()
					.setAttribute("STORE_LIST", stores);

			// Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if (materialRequisitionVO != null
					&& materialRequisitionVO.getMaterialRequisitionId() != null
					&& materialRequisitionVO.getMaterialRequisitionId() != 0) {
				comment.setRecordId(materialRequisitionVO
						.getMaterialRequisitionId());
				comment.setUseCase(MaterialRequisition.class.getName());
				comment = materialRequisitionBL.getCommentBL().getCommentInfo(
						comment);

			}
			log.info("Module: Accounts : Method: showMaterialRequisitionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialRequisitionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Material Requisition
	public String saveMaterialRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: saveMaterialRequisition");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			MaterialRequisition materialRequisition = new MaterialRequisition();
			materialRequisition.setRequisitionDate(DateFormat
					.convertStringToDate(requisitionDate));
			materialRequisition.setDescription(description);
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			Store store = null;
			if (storeId == 0) {
				store = materialRequisitionBL
						.getStoreBL()
						.getStoreService()
						.getBasicStoreByStoreId(
								posUserSession.getStore().getStoreId());
				materialRequisition.setStore(store);
				materialRequisition.setPerson(posUserSession
						.getPersonByPersonId());
			} else {
				store = materialRequisitionBL.getStoreBL().getStoreService()
						.getBasicStoreByStoreId(storeId);
				materialRequisition.setStore(store);
			}
			materialRequisition.setRequisitionStatus(RequisitionStatus.Open
					.getCode());

			if (ServletActionContext.getRequest().getParameter("useCase") != null
					&& recordId != null) {
				materialRequisition.setUseCase(ServletActionContext
						.getRequest().getParameter("useCase").toString());
				materialRequisition.setRecordId(recordId);
			}
			JSONParser parser = new JSONParser();
			Object requisitionObject = parser.parse(requisitionDetails);
			JSONArray object = (JSONArray) requisitionObject;
			MaterialRequisitionDetail materialRequisitionDetail = null;
			Product product = null;
			List<MaterialRequisitionDetail> materialRequisitionDetails = new ArrayList<MaterialRequisitionDetail>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray detailArray = (JSONArray) jsonObject
						.get("requisitionDetails");
				for (Iterator<?> iteratorObject = detailArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					materialRequisitionDetail = new MaterialRequisitionDetail();
					product = new Product();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"productId").toString()));
					materialRequisitionDetail.setProduct(product);
					materialRequisitionDetail.setQuantity(Double
							.parseDouble(jsonObjectDetail.get("productQty")
									.toString()));
					materialRequisitionDetail
							.setDescription((null != jsonObjectDetail
									.get("linesDescription") && !("")
									.equals(jsonObjectDetail.get(
											"linesDescription").toString())) ? jsonObjectDetail
									.get("linesDescription").toString() : null);
					materialRequisitionDetail
							.setMaterialRequisitionDetailId((null != jsonObjectDetail
									.get("materialRequisitionDetailId") && Long
									.parseLong(jsonObjectDetail.get(
											"materialRequisitionDetailId")
											.toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"materialRequisitionDetailId")
											.toString()) : null);
				}
				materialRequisitionDetails.add(materialRequisitionDetail);
			}
			List<MaterialRequisitionDetail> deletedMaterialRequisitionDetails = null;
			if (materialRequisitionId > 0) {
				MaterialRequisition materialRequisitionEdit = materialRequisitionBL
						.getMaterialRequisitionService()
						.getMaterialRequisitionById(materialRequisitionId);
				materialRequisition
						.setMaterialRequisitionId(materialRequisitionId);
				materialRequisition.setReferenceNumber(materialRequisitionEdit
						.getReferenceNumber());
				materialRequisition.setPerson(materialRequisitionEdit
						.getPerson());
				materialRequisition.setCreatedDate(materialRequisitionEdit
						.getCreatedDate());
				deletedMaterialRequisitionDetails = getUserDeletedRequisitionDetails(materialRequisitionDetails);
			} else {
				materialRequisition.setReferenceNumber(SystemBL
						.getReferenceStamp(MaterialRequisition.class.getName(),
								materialRequisitionBL.getImplementation()));
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				materialRequisition.setPerson(person);
				materialRequisition.setCreatedDate(Calendar.getInstance()
						.getTime());
			}
			materialRequisitionBL.saveMaterialRequisition(materialRequisition,
					materialRequisitionDetails, alertId,
					deletedMaterialRequisitionDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMaterialRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveMaterialRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<MaterialRequisitionDetail> getUserDeletedRequisitionDetails(
			List<MaterialRequisitionDetail> requisitionDetailList)
			throws Exception {
		List<MaterialRequisitionDetail> existingRequisitionDetails = materialRequisitionBL
				.getMaterialRequisitionService()
				.getMaterialRequisitionDetailById(materialRequisitionId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, MaterialRequisitionDetail> hashRequisitionDetail = new HashMap<Long, MaterialRequisitionDetail>();
		if (null != existingRequisitionDetails
				&& existingRequisitionDetails.size() > 0) {
			for (MaterialRequisitionDetail list : existingRequisitionDetails) {
				listOne.add(list.getMaterialRequisitionDetailId());
				hashRequisitionDetail.put(
						list.getMaterialRequisitionDetailId(), list);
			}
			if (null != hashRequisitionDetail
					&& hashRequisitionDetail.size() > 0) {
				for (MaterialRequisitionDetail list : requisitionDetailList) {
					listTwo.add(list.getMaterialRequisitionDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<MaterialRequisitionDetail> deletedRequisitionDetails = null;
		if (null != different && different.size() > 0) {
			MaterialRequisitionDetail tempRequisitionDetail = null;
			deletedRequisitionDetails = new ArrayList<MaterialRequisitionDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempRequisitionDetail = new MaterialRequisitionDetail();
					tempRequisitionDetail = hashRequisitionDetail.get(list);
					deletedRequisitionDetails.add(tempRequisitionDetail);
				}
			}
		}
		return deletedRequisitionDetails;
	}

	// DeleteMaterial Requisition
	public String deleteMaterialRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: saveMaterialRequisition");
			MaterialRequisition materialRequisition = materialRequisitionBL
					.getMaterialRequisitionService()
					.getMaterialRequisitionById(materialRequisitionId);
			materialRequisitionBL
					.deleteMaterialRequisition(
							materialRequisition,
							new ArrayList<MaterialRequisitionDetail>(
									materialRequisition
											.getMaterialRequisitionDetails()),
							alertId);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMaterialRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveMaterialRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Active Material Requisition
	public String showActiveMaterialRequisition() {
		log.info("Inside Module: Accounts : Method: showActiveMaterialRequisition");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<MaterialRequisitionVO> materialRequisitionVOs = null;
			List<MaterialRequisitionDetail> materialRequisitionDetails = materialRequisitionBL
					.getActiveMaterialRequisitionDetail(storeId);
			if (null != materialRequisitionDetails
					&& materialRequisitionDetails.size() > 0) {
				materialRequisitionVOs = materialRequisitionBL
						.validateRequisitionTransfer(materialRequisitionDetails);
				Map<Long, MaterialRequisitionVO> requisitionSession = new HashMap<Long, MaterialRequisitionVO>();
				for (MaterialRequisitionVO materialRequisitionVO : materialRequisitionVOs) {
					requisitionSession.put(
							materialRequisitionVO.getMaterialRequisitionId(),
							materialRequisitionVO);
				}
				session.setAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
						+ sessionObj.get("jSessionId"), requisitionSession);
				Collections.sort(materialRequisitionVOs,
						new Comparator<MaterialRequisitionVO>() {
							public int compare(MaterialRequisitionVO o1,
									MaterialRequisitionVO o2) {
								return o2.getMaterialRequisitionId().compareTo(
										o1.getMaterialRequisitionId());
							}
						});
			}
			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_REQUISITION_INFO", materialRequisitionVOs);
			log.info("Module: Accounts : Method: showActiveMaterialRequisition Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showActiveMaterialRequisition: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Session Material Requisition
	@SuppressWarnings("unchecked")
	public String showSessionMaterialRequisition() {
		log.info("Inside Module: Accounts : Method: showSessionMaterialRequisition");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, MaterialRequisitionVO> requisitionHash = (Map<Long, MaterialRequisitionVO>) (session
					.getAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			MaterialRequisitionVO materialRequisitionVO = (MaterialRequisitionVO) requisitionHash
					.get(materialRequisitionId);
			Collections.sort(
					materialRequisitionVO.getMaterialRequisitionDetailVOs(),
					new Comparator<MaterialRequisitionDetailVO>() {
						public int compare(MaterialRequisitionDetailVO o1,
								MaterialRequisitionDetailVO o2) {
							return o1
									.getMaterialRequisitionDetailId()
									.compareTo(
											o2.getMaterialRequisitionDetailId());
						}
					});
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_SESSION", materialRequisitionVO);
			log.info("Module: Accounts : Method: showSessionMaterialRequisition Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showSessionMaterialRequisition: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String showSessionMaterialRequisitionDetail() {
		log.info("Inside Module: Accounts : Method: showSessionMaterialRequisitionDetail");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, MaterialRequisitionVO> requisitionHash = (Map<Long, MaterialRequisitionVO>) (session
					.getAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			MaterialRequisitionVO materialRequisitionVO = (MaterialRequisitionVO) requisitionHash
					.get(materialRequisitionId);
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_DETAIL_SESSION",
					materialRequisitionVO.getMaterialRequisitionDetailVOs());
			ServletActionContext.getRequest().setAttribute(
					"TRANSFER_TYPE",
					materialRequisitionBL.getLookupMasterBL()
							.getActiveLookupDetails("MATERIAL_TRANSFER_TYPE",
									true));
			ServletActionContext.getRequest().setAttribute(
					"TRANSFER_REASON",
					materialRequisitionBL.getLookupMasterBL()
							.getActiveLookupDetails("MATERIAL_TRANSFER_REASON",
									true));
			log.info("Module: Accounts : Method: showSessionMaterialRequisitionDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showSessionMaterialRequisitionDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update SessionRequisitionDetail
	@SuppressWarnings("unchecked")
	public String updateSessionRequisitionDetail() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, MaterialRequisitionVO> requisitionHash = (Map<Long, MaterialRequisitionVO>) (session
					.getAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			MaterialRequisitionVO materialRequisitionVO = (MaterialRequisitionVO) requisitionHash
					.get(materialRequisitionId);
			Map<Long, MaterialRequisitionDetailVO> detailVOMap = new HashMap<Long, MaterialRequisitionDetailVO>();
			for (MaterialRequisitionDetailVO detail : materialRequisitionVO
					.getMaterialRequisitionDetailVOs())
				detailVOMap
						.put(detail.getMaterialRequisitionDetailId(), detail);

			JSONParser parser = new JSONParser();
			Object requisitionObject = parser.parse(requisitionDetails);
			JSONArray object = (JSONArray) requisitionObject;
			MaterialRequisitionDetailVO materialRequisitionDetailVO = null;
			List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray detailArray = (JSONArray) jsonObject
						.get("requisitionDetails");
				for (Iterator<?> iteratorObject = detailArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					materialRequisitionDetailVO = new MaterialRequisitionDetailVO();
					if (detailVOMap.containsKey(Long.parseLong(jsonObjectDetail
							.get("materialRequisitionDetailId").toString()))) {
						materialRequisitionDetailVO = detailVOMap.get(Long
								.parseLong(jsonObjectDetail.get(
										"materialRequisitionDetailId")
										.toString()));
						materialRequisitionDetailVO.setTransferQty(Double
								.parseDouble(jsonObjectDetail.get("productQty")
										.toString()));
						materialRequisitionDetailVO
								.setDescription((null != jsonObjectDetail
										.get("linesDescription") && !("")
										.equals(jsonObjectDetail.get(
												"linesDescription").toString())) ? jsonObjectDetail
										.get("linesDescription").toString()
										: null);
						materialRequisitionDetailVO
								.setStoreName(jsonObjectDetail.get("storeName")
										.toString());
						materialRequisitionDetailVO.setShelfFromId(Integer
								.parseInt(jsonObjectDetail.get("storeFromId")
										.toString()));
						materialRequisitionDetailVO.setShelfToId(Integer
								.parseInt(jsonObjectDetail.get("shelfId")
										.toString()));
						materialRequisitionDetailVO
								.setTransferStoreName(jsonObjectDetail.get(
										"transferStoreName").toString());
						materialRequisitionDetailVO.setTransferType(Long
								.parseLong(jsonObjectDetail.get("transferType")
										.toString()));
						materialRequisitionDetailVO.setUnitRate(Double
								.parseDouble(jsonObjectDetail.get("amount")
										.toString()));
						if (null != jsonObjectDetail.get("transferReason")
								&& Long.parseLong(jsonObjectDetail.get(
										"transferReason").toString()) > 0)
							materialRequisitionDetailVO.setTransferReason(Long
									.parseLong(jsonObjectDetail.get(
											"transferReason").toString()));
						materialRequisitionDetailVOs
								.add(materialRequisitionDetailVO);
					}
				}
			}
			materialRequisitionVO
					.setMaterialRequisitionDetailVOs(materialRequisitionDetailVOs);
			requisitionHash.put(
					materialRequisitionVO.getMaterialRequisitionId(),
					materialRequisitionVO);
			session.setAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"), requisitionHash);
			log.info("Module: Accounts : Method: showSessionMaterialRequisitionDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showSessionMaterialRequisitionDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Getters & Setters
	public MaterialRequisitionBL getMaterialRequisitionBL() {
		return materialRequisitionBL;
	}

	public void setMaterialRequisitionBL(
			MaterialRequisitionBL materialRequisitionBL) {
		this.materialRequisitionBL = materialRequisitionBL;
	}

	public long getMaterialRequisitionId() {
		return materialRequisitionId;
	}

	public void setMaterialRequisitionId(long materialRequisitionId) {
		this.materialRequisitionId = materialRequisitionId;
	}

	public String getRequisitionDate() {
		return requisitionDate;
	}

	public void setRequisitionDate(String requisitionDate) {
		this.requisitionDate = requisitionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getRequisitionDetails() {
		return requisitionDetails;
	}

	public void setRequisitionDetails(String requisitionDetails) {
		this.requisitionDetails = requisitionDetails;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public static Logger getLog() {
		return log;
	}

	public static void setLog(Logger log) {
		MaterialRequisitionAction.log = log;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
