/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderCharge;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderVO;
import com.aiotech.aios.accounts.service.bl.SalesOrderBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Sales Order
 * 
 * @author Saleem
 */
public class SalesOrderAction extends ActionSupport {

	private static final long serialVersionUID = 6093662184466530756L;

	private static final Logger LOG = Logger.getLogger(SalesOrderAction.class);
	// Dependency
	private SalesOrderBL salesOrderBL;

	// Variables
	private long salesOrderId;
	private long customerId;
	private long shippingDetailId;
	private long salesPersonId;
	private byte paymentMode;
	private long shippingMethod;
	private long shippingTerm;
	private long customerQuotationId;
	private Long recordId;
	private byte status;
	private long orderType;
	private long currencyId;
	private int rowId;
	private String referenceNumber;
	private String orderDate;
	private String expiryDate;
	private String shippingDate;
	private String customerReference;
	private String description;
	private String salesOrderDetails;
	private String chargesDetails;
	private String personName;
	private boolean continuousSales;
	private List<Object> aaData;
	private SalesOrderVO salesOrderVO;
	private long creditTermId;

	public String returnSuccess() {
		LOG.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// show all sales order json list
	public String showAllSalesOrder() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllSalesOrder");
			aaData = salesOrderBL.getAllSalesOrder();
			LOG.info("Module: Accounts : Method: showAllSalesOrder: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllSalesOrder Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Sales Order Info
	public String showUnDeliveredSalesOrder() {
		try {
			LOG.info("Inside Module: Accounts : Method: showUnDeliveredSalesOrder");
			aaData = salesOrderBL.getUnDeliveredSalesOrder();
			LOG.info("Module: Accounts : Method: showUnDeliveredSalesOrder: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showUnDeliveredSalesOrder Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all sales order
	public String showAllCustomerSalesOrder() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllCustomerSalesOrder");
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_SALES_ORDER",
					salesOrderBL.getSalesOrderService().getAllSalesOrder(
							salesOrderBL.getImplementation()));
			LOG.info("Module: Accounts : Method: showAllSalesOrder: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllCustomerSalesOrder Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Sales Order Info
	public String showSalesOrderInfo() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesOrderInfo");
			salesOrderVO = salesOrderBL.getSalesOrderInfo(salesOrderBL
					.getSalesOrderService().getSalesOrder(salesOrderId));
			LOG.info("Module: Accounts : Method: showSalesOrderInfo: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesOrderInfo Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSalesOrderApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesOrderApproval");
			salesOrderId = recordId;
			showSalesOrderEntry();
			LOG.info("Module: Accounts : Method: showSalesOrderApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesOrderApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSalesOrderRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesOrderRejectEntry");
			salesOrderId = recordId;
			showSalesOrderEntry();
			LOG.info("Module: Accounts : Method: showSalesOrderRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesOrderRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Sales Order Entry
	public String showSalesOrderEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesOrderEntry");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			SalesOrderVO salesOrderVO = null;
			if (salesOrderId > 0) {
				salesOrderVO = new SalesOrderVO();
				SalesOrder salesOrder = salesOrderBL.getSalesOrderService()
						.getSalesOrder(salesOrderId);
				BeanUtils.copyProperties(salesOrderVO, salesOrder);
				List<SalesOrderDetailVO> salesOrderDetailVOs = new ArrayList<SalesOrderDetailVO>();
				SalesOrderDetailVO salesOrderDetailVO = null;
				for (SalesOrderDetail orderDetail : salesOrder
						.getSalesOrderDetails()) {
					salesOrderDetailVO = new SalesOrderDetailVO();
					BeanUtils.copyProperties(salesOrderDetailVO, orderDetail);
					if (null != orderDetail.getProductPackageDetail()) {
						salesOrderDetailVO.setPackageDetailId(orderDetail
								.getProductPackageDetail()
								.getProductPackageDetailId());
						salesOrderDetailVO.setBaseUnitName(orderDetail
								.getProduct().getLookupDetailByProductUnit()
								.getDisplayName()); 
					} else
						salesOrderDetailVO.setPackageDetailId(-1l);
					salesOrderDetailVO.setBaseQuantity(AIOSCommons
							.convertExponential(orderDetail
									.getOrderQuantity()));
					salesOrderDetailVO.setProductPackageVOs(salesOrderBL
							.getCustomerQuotationBL()
							.getPackageConversionBL()
							.getProductPackagingDetail(
									orderDetail.getProduct().getProductId()));
					salesOrderDetailVO.setBasePrice(salesOrderBL.getStockBL()
							.getProductCostPrice(
									orderDetail.getProduct()
											.getProductPricingDetails()));
					salesOrderDetailVO.setStandardPrice(salesOrderBL
							.getStockBL().getProductStandardPrice(
									orderDetail.getProduct()
											.getProductPricingDetails()));
					salesOrderDetailVOs.add(salesOrderDetailVO);
				}
				Collections.sort(salesOrderDetailVOs,
						new Comparator<SalesOrderDetailVO>() {
							public int compare(SalesOrderDetailVO s1,
									SalesOrderDetailVO s2) {
								return s1.getSalesOrderDetailId().compareTo(
										s2.getSalesOrderDetailId());
							}
						});
				salesOrderVO.setSalesOrderDetailVOs(salesOrderDetailVOs);
				CommentVO comment = new CommentVO();
				if (recordId != null && salesOrder != null
						&& salesOrder.getSalesOrderId() != 0 && recordId > 0) {
					comment.setRecordId(salesOrder.getSalesOrderId());
					comment.setUseCase(SalesOrder.class.getName());
					comment = salesOrderBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else {
				referenceNumber = generateReferenceNumber();
				salesPersonId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			List<Currency> currencies = salesOrderBL.getCurrencyService()
					.getAllCurrency(salesOrderBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_ALL",
					currencies);
			ServletActionContext.getRequest().setAttribute("SALES_ORDER",
					salesOrderVO);
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_METHOD",
					salesOrderBL.getLookupMasterBL().getActiveLookupDetails(
							"SHIPPING_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_TERMS",
					salesOrderBL.getLookupMasterBL().getActiveLookupDetails(
							"SHIPPING_TERMS", true));
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					salesOrderBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					salesOrderBL.getLookupMasterBL().getActiveLookupDetails(
							"CHARGES_TYPE", true));
			ServletActionContext.getRequest().setAttribute("ORDER_STATUS",
					salesOrderBL.getO2CProcessStatus());
			ServletActionContext.getRequest().setAttribute(
					"ORDER_TYPE",
					salesOrderBL.getLookupMasterBL().getActiveLookupDetails(
							"SALES_ORDER_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM",
					salesOrderBL
							.getCreditTermBL()
							.getCreditTermService()
							.getAllCustomerCreditTerms(
									salesOrderBL.getImplementation()));
			LOG.info("Module: Accounts : Method: showSalesOrderEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesOrderEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Sales Order
	public String saveSalesOrder() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveSalesOrder");
			SalesOrder salesOrder = new SalesOrder();
			salesOrder.setOrderDate(DateFormat.convertStringToDate(orderDate));
			salesOrder
					.setExpiryDate(DateFormat.convertStringToDate(expiryDate));
			salesOrder.setShippingDate(null != shippingDate ? DateFormat
					.convertStringToDate(shippingDate) : null);
			salesOrder.setDescription(description);
			if (customerId > 0) {
				Customer customer = new Customer();
				customer.setCustomerId(customerId);
				salesOrder.setCustomer(customer);
			}
			salesOrder.setModeOfPayment(paymentMode > 0 ? paymentMode : null);
			LookupDetail lookupDetail = null;
			if (shippingMethod > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(shippingMethod);
				salesOrder.setLookupDetailByShippingMethod(lookupDetail);
			}
			if (shippingTerm > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(shippingTerm);
				salesOrder.setLookupDetailByShippingTerm(lookupDetail);
			}
			if (salesPersonId > 0) {
				Person person = new Person();
				person.setPersonId(salesPersonId);
				salesOrder.setPerson(person);
			}
			salesOrder.setOrderStatus(status > 0 ? status : null);
			salesOrder.setContinuousSales(continuousSales);
			if (customerQuotationId > 0) {
				CustomerQuotation customerQuotation = new CustomerQuotation();
				customerQuotation.setCustomerQuotationId(customerQuotationId);
				salesOrder.setCustomerQuotation(customerQuotation);
			}
			if (shippingDetailId > 0) {
				ShippingDetail shippingDetail = new ShippingDetail();
				shippingDetail.setShippingId(shippingDetailId);
				salesOrder.setShippingDetail(shippingDetail);
			}
			if (orderType > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(orderType);
				salesOrder.setLookupDetailByOrderType(lookupDetail);
			}
			if (creditTermId > 0) {
				CreditTerm creditTerm = new CreditTerm();
				creditTerm.setCreditTermId(creditTermId);
				salesOrder.setCreditTerm(creditTerm);
			}

			List<SalesOrderDetail> salesOrderDetail = new ArrayList<SalesOrderDetail>();
			List<SalesOrderDetail> deleteOrderDetails = null;
			List<SalesOrderCharge> salesOrderCharge = null;
			List<SalesOrderCharge> deleteOrderCharges = null;
			String[] salesOrderDetails = splitValues(this.salesOrderDetails,
					"@#");
			for (String detail : salesOrderDetails) {
				salesOrderDetail.add(addSalesOrderDetail(detail));
			}
			if (null != chargesDetails && !("").equals(chargesDetails)) {
				String[] chargesDetails = splitValues(this.chargesDetails, "@#");
				salesOrderCharge = new ArrayList<SalesOrderCharge>();
				for (String detail : chargesDetails) {
					salesOrderCharge.add(addSalesOrderCharges(detail));
				}
			}
			SalesOrder salesOrderDB = null;
			if (salesOrderId > 0) {
				salesOrder.setSalesOrderId(salesOrderId);
				salesOrderDB = salesOrderBL.getSalesOrderService()
						.getBasicSalesOrder(salesOrderId);
				salesOrder
						.setReferenceNumber(salesOrderDB.getReferenceNumber());
				salesOrder.setCurrency(salesOrderDB.getCurrency());
				salesOrder.setExchangeRate(salesOrderDB.getExchangeRate());
				deleteOrderDetails = userDeletedSalesOrderDetails(
						salesOrderDetail,
						salesOrderBL
								.getSalesOrderService()
								.getSalesOrderDetailBySalesOrderId(salesOrderId));
				deleteOrderCharges = userDeletedSalesOrderCharges(
						salesOrderCharge,
						salesOrderBL.getSalesOrderService()
								.getSalesOrderChargesBySalesOrderId(
										salesOrderId));
			} else
				salesOrder.setReferenceNumber(generateReferenceNumber());

			if (salesOrderId == 0
					|| (null != salesOrderDB && (long) salesOrderDB
							.getCurrency().getCurrencyId() != currencyId)) {
				Currency defaultCurrency = salesOrderBL.getStockBL()
						.getProductPricingBL().getCustomerBL()
						.getTransactionBL().getCurrency();
				if (null != defaultCurrency) {
					if ((long) defaultCurrency.getCurrencyId() != currencyId) {
						Currency currency = salesOrderBL.getStockBL()
								.getProductPricingBL().getCustomerBL()
								.getTransactionBL().getCurrencyService()
								.getCurrency(currencyId);
						CurrencyConversion currencyConversion = salesOrderBL
								.getStockBL().getProductPricingBL()
								.getCustomerBL().getTransactionBL()
								.getCurrencyConversionBL()
								.getCurrencyConversionService()
								.getCurrencyConversionByCurrency(currencyId);
						salesOrder.setCurrency(currency);
						salesOrder
								.setExchangeRate(null != currencyConversion ? currencyConversion
										.getConversionRate() : 1.0);
					} else {
						salesOrder.setCurrency(defaultCurrency);
						salesOrder.setExchangeRate(1.0);
					}
				}
			}

			salesOrderBL.saveSalesOrder(salesOrder, salesOrderDetail,
					salesOrderCharge, deleteOrderDetails, deleteOrderCharges);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: saveSalesOrder: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: saveSalesOrder Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Sales Order
	public String deleteSalesOrder() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveSalesOrder");
			SalesOrder salesOrder = salesOrderBL.getSalesOrderService()
					.getSalesOrderInfo(salesOrderId);
			salesOrderBL.deleteSalesOrder(salesOrder);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: deleteSalesOrder: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: deleteSalesOrder Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Sales Order Add Row
	public String salesOrderDetailAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: salesOrderDetailAddRow");
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: salesOrderDetailAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: salesOrderDetailAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Sales Order Charges Add Row
	public String salesOrderChargesAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: salesOrderChargesAddRow");
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					salesOrderBL.getLookupMasterBL().getActiveLookupDetails(
							"CHARGES_TYPE", true));
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: salesOrderChargesAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: salesOrderChargesAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get user delete quotation charges
	private List<SalesOrderCharge> userDeletedSalesOrderCharges(
			List<SalesOrderCharge> salesOrderCharges,
			List<SalesOrderCharge> salesOrderChargesOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, SalesOrderCharge> hashSalesOrderCharges = new HashMap<Long, SalesOrderCharge>();
		if (null != salesOrderChargesOld && salesOrderChargesOld.size() > 0) {
			for (SalesOrderCharge list : salesOrderChargesOld) {
				listOne.add(list.getSalesOrderChargeId());
				hashSalesOrderCharges.put(list.getSalesOrderChargeId(), list);
			}
			if (null != salesOrderCharges && salesOrderCharges.size() > 0)
				for (SalesOrderCharge list : salesOrderCharges) {
					listTwo.add(list.getSalesOrderChargeId());
				}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<SalesOrderCharge> salesOrderChargeDetailList = null;
		if (null != different && different.size() > 0) {
			SalesOrderCharge tempSalesOrderCharge = null;
			salesOrderChargeDetailList = new ArrayList<SalesOrderCharge>();
			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempSalesOrderCharge = new SalesOrderCharge();
					tempSalesOrderCharge = hashSalesOrderCharges.get(list);
					salesOrderChargeDetailList.add(tempSalesOrderCharge);
				}
			}
		}
		return salesOrderChargeDetailList;
	}

	// Get user delete quotation details
	private List<SalesOrderDetail> userDeletedSalesOrderDetails(
			List<SalesOrderDetail> salesOrderQuotationDetails,
			List<SalesOrderDetail> salesOrderDetailOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, SalesOrderDetail> hashSalesCharges = new HashMap<Long, SalesOrderDetail>();
		if (null != salesOrderDetailOld && salesOrderDetailOld.size() > 0) {
			for (SalesOrderDetail list : salesOrderDetailOld) {
				listOne.add(list.getSalesOrderDetailId());
				hashSalesCharges.put(list.getSalesOrderDetailId(), list);
			}
			for (SalesOrderDetail list : salesOrderQuotationDetails) {
				listTwo.add(list.getSalesOrderDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<SalesOrderDetail> salesDetailList = new ArrayList<SalesOrderDetail>();
		if (null != different && different.size() > 0) {
			SalesOrderDetail tempSalesOrderDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempSalesOrderDetail = new SalesOrderDetail();
					tempSalesOrderDetail = hashSalesCharges.get(list);
					salesDetailList.add(tempSalesOrderDetail);
				}
			}
		}
		return salesDetailList;
	}

	// Add Sales Order Charges
	private SalesOrderCharge addSalesOrderCharges(String orderCharge) {
		SalesOrderCharge salesOrderCharge = new SalesOrderCharge();
		String orderCharges[] = splitValues(orderCharge, "__");
		LookupDetail charges = new LookupDetail();
		charges.setLookupDetailId(Long.parseLong(orderCharges[0]));
		salesOrderCharge.setLookupDetail(charges);
		salesOrderCharge.setCharges(Double.parseDouble(orderCharges[1]));
		if (null != orderCharges[2] && !("##").equals(orderCharges[2]))
			salesOrderCharge.setDescription(orderCharges[2]);
		if (null != orderCharges[3] && Long.parseLong(orderCharges[3]) > 0)
			salesOrderCharge.setSalesOrderChargeId(Long
					.parseLong(orderCharges[3]));
		return salesOrderCharge;
	}

	// Add Sales Order Detail
	private SalesOrderDetail addSalesOrderDetail(String salesOrderDetail) {
		SalesOrderDetail orderDetail = new SalesOrderDetail();
		String orderDetails[] = splitValues(salesOrderDetail, "__");
		Product product = new Product();
		product.setProductId(Long.parseLong(orderDetails[0]));
		orderDetail.setProduct(product);
		if (Long.parseLong(orderDetails[1]) > 0l) {
			Store store = new Store();
			store.setStoreId(Long.parseLong(orderDetails[1]));
			orderDetail.setStore(store);
		}
		orderDetail.setOrderQuantity(Double.parseDouble(orderDetails[2]));
		orderDetail.setUnitRate(Double.parseDouble(orderDetails[3]));
		if (null != orderDetails[4] && !("##").equals(orderDetails[4])) {
			if (("P").equals(orderDetails[4]))
				orderDetail.setIsPercentage(true);
			else
				orderDetail.setIsPercentage(false);
			orderDetail.setDiscount(Double.parseDouble(orderDetails[5]));
		}
		if (null != orderDetails[6] && Long.parseLong(orderDetails[6]) > 0) {
			ProductPackageDetail productPackageDetail = new ProductPackageDetail();
			productPackageDetail.setProductPackageDetailId(Long
					.parseLong(orderDetails[6]));
			orderDetail.setProductPackageDetail(productPackageDetail);
		}
		orderDetail.setPackageUnit(Double.parseDouble(orderDetails[7]));
		if (null != orderDetails[8] && Long.parseLong(orderDetails[8]) > 0)
			orderDetail.setSalesOrderDetailId(Long.parseLong(orderDetails[8]));
		return orderDetail;
	}

	// Split the value
	private static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Generate Reference Number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(SalesOrder.class.getName(),
				salesOrderBL.getImplementation());
	}

	// Getters and Setters
	public SalesOrderBL getSalesOrderBL() {
		return salesOrderBL;
	}

	public void setSalesOrderBL(SalesOrderBL salesOrderBL) {
		this.salesOrderBL = salesOrderBL;
	}

	public long getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(long salesOrderId) {
		this.salesOrderId = salesOrderId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getSalesPersonId() {
		return salesPersonId;
	}

	public void setSalesPersonId(long salesPersonId) {
		this.salesPersonId = salesPersonId;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public long getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(long shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public long getShippingTerm() {
		return shippingTerm;
	}

	public void setShippingTerm(long shippingTerm) {
		this.shippingTerm = shippingTerm;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSalesOrderDetails() {
		return salesOrderDetails;
	}

	public void setSalesOrderDetails(String salesOrderDetails) {
		this.salesOrderDetails = salesOrderDetails;
	}

	public String getChargesDetails() {
		return chargesDetails;
	}

	public void setChargesDetails(String chargesDetails) {
		this.chargesDetails = chargesDetails;
	}

	public long getCustomerQuotationId() {
		return customerQuotationId;
	}

	public void setCustomerQuotationId(long customerQuotationId) {
		this.customerQuotationId = customerQuotationId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public long getOrderType() {
		return orderType;
	}

	public void setOrderType(long orderType) {
		this.orderType = orderType;
	}

	public long getShippingDetailId() {
		return shippingDetailId;
	}

	public void setShippingDetailId(long shippingDetailId) {
		this.shippingDetailId = shippingDetailId;
	}

	public SalesOrderVO getSalesOrderVO() {
		return salesOrderVO;
	}

	public void setSalesOrderVO(SalesOrderVO salesOrderVO) {
		this.salesOrderVO = salesOrderVO;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public boolean isContinuousSales() {
		return continuousSales;
	}

	public void setContinuousSales(boolean continuousSales) {
		this.continuousSales = continuousSales;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}
}
