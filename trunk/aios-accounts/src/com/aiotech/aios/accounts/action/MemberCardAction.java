package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.MemberCard;
import com.aiotech.aios.accounts.service.bl.MemberCardBL;
import com.aiotech.aios.common.to.Constants.Accounts.MemberCardType;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class MemberCardAction extends ActionSupport {

	private static final long serialVersionUID = 5237399788435077968L;

	private static final Logger log = Logger.getLogger(MemberCardAction.class);

	// Dependency
	private MemberCardBL memberCardBL;

	// Variables
	private long memberCardId;
	private Long recordId;
	private byte cardType;
	private int startDigit;
	private int endDigit;
	private int currentDigit;
	private int statusPoints;
	private String description;
	private String returnMessage;
	private List<Object> aaData;
	private Object memberCardVO;

	// return success
	public String returnSuccess() {
		try {
			log.info("Inside Module: Accounts : Method: returnSuccess");
			Map<Byte, MemberCardType> cardTypes = new HashMap<Byte, MemberCardType>();
			for (MemberCardType e : EnumSet.allOf(MemberCardType.class)) {
				cardTypes.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("CARD_TYPES",
					cardTypes);
			CommentVO comment = new CommentVO();
			if (recordId != null && recordId > 0) {
				MemberCard memberCard = memberCardBL.getMemberCardService()
					.getMemberCardByCardId(recordId); 
				comment.setRecordId(memberCard.getMemberCardId());
				comment.setUseCase(MemberCard.class.getName());
				comment = memberCardBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}else{
				memberCardId = 0l;
			}
			recordId = null;
			log.info("Module: Accounts : Method: returnSuccess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: returnSuccess Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all commission rule json data
	public String showAllMemberCards() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMemberCards");
			aaData = memberCardBL.showAllMemberCards();
			log.info("Module: Accounts : Method: showAllMemberCards: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllMemberCards Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMemberCardApproval(){
		try {
			LOG.info("Inside Module: Accounts : Method: showMemberCardApproval");
			memberCardId = recordId; 
			returnSuccess();
			LOG.info("Module: Accounts : Method: showMemberCardApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showMemberCardApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showMemberCardRejectEntry(){
		try {
			LOG.info("Inside Module: Accounts : Method: showMemberCardRejectEntry");
			memberCardId = recordId; 
			returnSuccess();
			LOG.info("Module: Accounts : Method: showMemberCardRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showMemberCardRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	// Show Member Card entry screen
	public String showMemberCardEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showMemberCardEntry");
			MemberCard memberCard = memberCardBL.getMemberCardService()
					.getMemberCardByCardId(memberCardId); 
			memberCardVO = memberCardBL.addMemberCardVO(memberCard);
			log.info("Module: Accounts : Method: showMemberCardEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMemberCardEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Member Card
	public String saveMemberCard() {
		try {
			log.info("Inside Module: Accounts : Method: saveMemberCard");
			MemberCard memberCard = new MemberCard();
			memberCard.setMemberCardId(memberCardId > 0 ? memberCardId : null);
			memberCard.setCardType(cardType);
			memberCard.setStartDigit(startDigit);
			memberCard.setEndDigit(endDigit);
			memberCard.setCurrentDigit(startDigit == currentDigit ? null : startDigit);
			memberCard.setDescription(description);
			memberCard.setStatusPoints(statusPoints);
			memberCardBL.saveMemberCard(memberCard);
			memberCardId = 0l;
			recordId = null;
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMemberCard: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveMemberCard Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Member Card
	public String deleteMemberCard() {
		try {
			log.info("Inside Module: Accounts : Method: deleteMemberCard");
			MemberCard memberCard = memberCardBL.getMemberCardService()
					.getMemberCardByCardId(memberCardId);
			memberCardBL.deleteMemberCard(memberCard);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteMemberCard: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteMemberCard Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Member Card Number
	public String showMemberCardNumber() {
		try {
			log.info("Inside Module: Accounts : Method: showMemberCardNumber");
			memberCardVO = memberCardBL.getMemberCardNumber(cardType);
			log.info("Module: Accounts : Method: showMemberCardNumber: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			currentDigit = 0;
			log.info("Module: Accounts : Method: showMemberCardNumber Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public MemberCardBL getMemberCardBL() {
		return memberCardBL;
	}

	public void setMemberCardBL(MemberCardBL memberCardBL) {
		this.memberCardBL = memberCardBL;
	}

	public long getMemberCardId() {
		return memberCardId;
	}

	public void setMemberCardId(long memberCardId) {
		this.memberCardId = memberCardId;
	}

	public byte getCardType() {
		return cardType;
	}

	public void setCardType(byte cardType) {
		this.cardType = cardType;
	}

	public int getStartDigit() {
		return startDigit;
	}

	public void setStartDigit(int startDigit) {
		this.startDigit = startDigit;
	}

	public int getEndDigit() {
		return endDigit;
	}

	public void setEndDigit(int endDigit) {
		this.endDigit = endDigit;
	}

	public int getCurrentDigit() {
		return currentDigit;
	}

	public void setCurrentDigit(int currentDigit) {
		this.currentDigit = currentDigit;
	}

	public int getStatusPoints() {
		return statusPoints;
	}

	public void setStatusPoints(int statusPoints) {
		this.statusPoints = statusPoints;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Object getMemberCardVO() {
		return memberCardVO;
	}

	public void setMemberCardVO(Object memberCardVO) {
		this.memberCardVO = memberCardVO;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	} 
}
