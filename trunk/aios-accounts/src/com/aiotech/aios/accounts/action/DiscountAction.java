package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Discount;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.DiscountOption;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.service.bl.DiscountBL;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountCalcMethod;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountOptions;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class DiscountAction extends ActionSupport {

	private static final long serialVersionUID = 6966274088568059626L;

	// Logger
	private static final Logger log = Logger.getLogger(DiscountAction.class);

	// Dependency
	private DiscountBL discountBL;

	// Variables
	private long discountId; 
	private long discountOnPrice;
	private Long recordId;
	private int rowId; 
	private byte discountType;
	private byte status;
	private byte discountOption;
	private double minimumValue;
	private double maxLimit;
	private String discountName;
	private String fromDate;
	private String toDate;
	private String description;
	private String discountMethods;
	private String returnMessage;
	private char minimumSales;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess() success");
		recordId = null;
		return SUCCESS;
	}

	// Show all commission rule json data
	public String showAllProductDiscounts() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductDiscounts");
			aaData = discountBL.showAllProductDiscounts();
			log.info("Module: Accounts : Method: showAllProductDiscounts: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductDiscounts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductDiscountApproval(){
		try {
			LOG.info("Inside Module: Accounts : Method: showProductDiscountApproval");
			discountId = recordId;
			showProductDiscountEntry();
			LOG.info("Module: Accounts : Method: showProductDiscountApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductDiscountApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showProductDiscountReject(){
		try {
			LOG.info("Inside Module: Accounts : Method: showProductDiscountReject");
			discountId = recordId;
			showProductDiscountEntry();
			LOG.info("Module: Accounts : Method: showProductDiscountReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductDiscountReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	// Show Product Discount Entry
	public String showProductDiscountEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDiscountEntry");
			if (discountId > 0) {
				Discount discount = discountBL.getDiscountService()
						.getProductDiscountByDiscountId(discountId);
				ServletActionContext.getRequest().setAttribute(
						"PRODUCT_DISCOUNT", discount);
				CommentVO comment = new CommentVO();
				if (recordId != null && discount != null
						&& discount.getDiscountId() != 0 && recordId > 0) {
					comment.setRecordId(discount.getDiscountId());
					comment.setUseCase(Discount.class.getName());
					comment = discountBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			Map<Byte, String> discountMethods = new HashMap<Byte, String>();
			for (DiscountCalcMethod e : EnumSet.allOf(DiscountCalcMethod.class)) {
				discountMethods.put(e.getCode(), e.name().replaceAll("_", " "));
			}
			Map<Byte, String> discountOptions = new HashMap<Byte, String>();
			for (DiscountOptions e : EnumSet.allOf(DiscountOptions.class)) {
				discountOptions.put(e.getCode(), e.name().replaceAll("_", " "));
			}
			ServletActionContext.getRequest().setAttribute("DISCOUNT_METHODS",
					discountMethods);
			ServletActionContext.getRequest().setAttribute("DISCOUNT_OPTIONS",
					discountOptions);
			ServletActionContext.getRequest().setAttribute(
					"DISCOUNT_NAMES",
					discountBL.getLookupMasterBL().getActiveLookupDetails(
							"PRODUCT_DISCOUNT_NAME", true));
			ServletActionContext.getRequest().setAttribute(
					"DISCOUNT_ON_PRICE",
					discountBL.getLookupMasterBL().getActiveLookupDetails(
							"PRODUCT_PRICING_LEVEL", true));
			log.info("Module: Accounts : Method: showProductDiscountEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDiscountEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Product Discount
	public String saveProductDiscount() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductDiscount");
			Discount discount = new Discount();
			discount.setDiscountOption(discountOption);
			discount.setDiscountType(discountType);
			discount.setDiscountName(discountName);
			if(discountOnPrice > 0){
				LookupDetail lookupdetail = new LookupDetail();  
				lookupdetail.setLookupDetailId(discountOnPrice);
				discount.setLookupDetail(lookupdetail);
			} 
			discount.setStatus(status > 0 ? true : false);
			discount.setFromDate(DateFormat.convertStringToDate(fromDate));
			discount.setToDate(DateFormat.convertStringToDate(toDate));
			discount.setMinimumSales(minimumSales == '\u0000' ? minimumSales : null);
			discount.setMinimumValue(minimumValue);
			discount.setMaxDiscountLimit(maxLimit);
			discount.setDescription(description);
			List<DiscountOption> discountOptions = new ArrayList<DiscountOption>();
			List<DiscountOption> deletedDiscountOptions = null;
			JSONObject jsonObject = new JSONObject();
			for (Object object : aaData)
				jsonObject.put("discountNature", object);

			try {
				String discountNature = jsonObject.getString("discountNature");
				JSONObject productObject = new JSONObject(discountNature);
				JSONArray recordArray = productObject.getJSONArray("record");
				DiscountOption discountOption = null;
				Product product = null;
				Customer customer = null;
				for (int i = 0; i < recordArray.length(); i++) {
					JSONObject jsonobj = recordArray.getJSONObject(i);
					discountOption = new DiscountOption();
					if (jsonobj.getString("entityName").equalsIgnoreCase(
							"product")) {
						product = new Product();
						product.setProductId(jsonobj.getLong("recordId"));
						discountOption.setProduct(product);
						discountOptions.add(discountOption);
					} else if (jsonobj.getString("entityName")
							.equalsIgnoreCase("customer")) {
						customer = new Customer();
						customer.setCustomerId(jsonobj.getLong("recordId"));
						discountOption.setCustomer(customer);
						discountOptions.add(discountOption);
					}
				}
			} catch (Exception e) {
				returnMessage = "Discount option failure.";
				e.printStackTrace();
				return ERROR;
			}

			List<DiscountMethod> discountDetails = new ArrayList<DiscountMethod>();
			List<DiscountMethod> deletedDiscountDetails = null;

			String[] discountMethodArray = splitValues(discountMethods, "#@");
			for (String discountDetail : discountMethodArray) {
				discountDetails.add(addDiscountDetail(discountDetail));
			}
			if (null != discountDetails && discountDetails.size() > 0) {
				if (null != discountDetails.get(0).getFlatPercentage()
						&& (double) discountDetails.get(0)
								.getFlatPercentage() > 0)
					discount.setDiscountType(DiscountCalcMethod.Flat_Percentage
							.getCode());
				else
					discount.setDiscountType(DiscountCalcMethod.Flat_Amount
							.getCode());
			}

			if (discountId > 0) {
				discount.setDiscountId(discountId);
				deletedDiscountDetails = userDeletedDiscountDetails(
						discountDetails, discountBL.getDiscountService()
								.getDiscountMethodByDiscountId(discountId));
				deletedDiscountOptions = userDeletedDiscountMethods(
						discountOptions, discountBL.getDiscountService()
								.getDiscountOptionByDiscountId(discountId));
			}
			discountBL.saveDiscount(discount, discountDetails,
					deletedDiscountDetails, discountOptions,
					deletedDiscountOptions);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductDiscount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveProductDiscount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Product Discount
	public String deleteProductDiscount() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductDiscount");
			discountBL.deleteProductDiscount(discountBL.getDiscountService()
					.getProductDiscountByDiscountId(discountId));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductDiscount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveProductDiscount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get User Deleted Discount Options
	private List<DiscountOption> userDeletedDiscountMethods(
			List<DiscountOption> discountOptions,
			List<DiscountOption> existingDiscountOptions) {
		Collection<Integer> listOne = new ArrayList<Integer>();
		Collection<Integer> listTwo = new ArrayList<Integer>();
		Map<Integer, DiscountOption> hashDiscountDetails = new HashMap<Integer, DiscountOption>();
		if (null != existingDiscountOptions
				&& existingDiscountOptions.size() > 0) {
			for (DiscountOption list : existingDiscountOptions) {
				listOne.add(list.getDiscountOptionId());
				hashDiscountDetails.put(list.getDiscountOptionId(), list);
			}
			for (DiscountOption list : discountOptions) {
				listTwo.add(list.getDiscountOptionId());
			}
		}
		Collection<Integer> similar = new HashSet<Integer>(listOne);
		Collection<Integer> different = new HashSet<Integer>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<DiscountOption> discountOptionsFinal = new ArrayList<DiscountOption>();
		if (null != different && different.size() > 0) {
			DiscountOption tempDiscountOption = null;

			for (Integer list : different) {
				if (null != list && !list.equals(0)) {
					tempDiscountOption = new DiscountOption();
					tempDiscountOption = hashDiscountDetails.get(list);
					discountOptionsFinal.add(tempDiscountOption);
				}
			}
		}
		return discountOptionsFinal;
	}

	// Get User Deleted Discount Details
	private List<DiscountMethod> userDeletedDiscountDetails(
			List<DiscountMethod> discountDetails,
			List<DiscountMethod> existingDiscountDetails) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, DiscountMethod> hashDiscountDetails = new HashMap<Long, DiscountMethod>();
		if (null != existingDiscountDetails
				&& existingDiscountDetails.size() > 0) {
			for (DiscountMethod list : existingDiscountDetails) {
				listOne.add(list.getDiscountMethodId());
				hashDiscountDetails.put(list.getDiscountMethodId(), list);
			}
			for (DiscountMethod list : discountDetails) {
				listTwo.add(list.getDiscountMethodId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<DiscountMethod> discountDetailsFinal = new ArrayList<DiscountMethod>();
		if (null != different && different.size() > 0) {
			DiscountMethod tempDiscountDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempDiscountDetail = new DiscountMethod();
					tempDiscountDetail = hashDiscountDetails.get(list);
					discountDetailsFinal.add(tempDiscountDetail);
				}
			}
		}
		return discountDetailsFinal;
	}

	// Add Discount Method
	private DiscountMethod addDiscountDetail(String discountDetail)
			throws Exception {
		DiscountMethod discountMethod = new DiscountMethod();
		String discountDetails[] = splitValues(discountDetail, "__");
		discountMethod.setFlatPercentage(null != discountDetails[0]
				&& !("0").equals(discountDetails[0].trim()) ? Double
				.parseDouble(discountDetails[0]) : null);
		discountMethod.setFlatAmount(null != discountDetails[1]
				&& !("0").equals(discountDetails[1].trim()) ? Double
				.parseDouble(discountDetails[1]) : null);
		discountMethod.setDiscountMethodId(null != discountDetails[2]
				&& !("0").equals(discountDetails[2].trim()) ? Long
				.parseLong(discountDetails[2]) : null);
		return discountMethod;
	}

	// Split Array Values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public DiscountBL getDiscountBL() {
		return discountBL;
	}

	public void setDiscountBL(DiscountBL discountBL) {
		this.discountBL = discountBL;
	}

	public long getDiscountId() {
		return discountId;
	}

	public void setDiscountId(long discountId) {
		this.discountId = discountId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getDiscountName() {
		return discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}

	public byte getDiscountType() {
		return discountType;
	}

	public void setDiscountType(byte discountType) {
		this.discountType = discountType;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public byte getDiscountOption() {
		return discountOption;
	}

	public void setDiscountOption(byte discountOption) {
		this.discountOption = discountOption;
	}

	public double getMinimumValue() {
		return minimumValue;
	}

	public void setMinimumValue(double minimumValue) {
		this.minimumValue = minimumValue;
	}

	public double getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(double maxLimit) {
		this.maxLimit = maxLimit;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiscountMethods() {
		return discountMethods;
	}

	public void setDiscountMethods(String discountMethods) {
		this.discountMethods = discountMethods;
	}

	public char getMinimumSales() {
		return minimumSales;
	}

	public void setMinimumSales(char minimumSales) {
		this.minimumSales = minimumSales;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getDiscountOnPrice() {
		return discountOnPrice;
	}

	public void setDiscountOnPrice(long discountOnPrice) {
		this.discountOnPrice = discountOnPrice;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
