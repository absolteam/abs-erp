package com.aiotech.aios.accounts.action;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPoint;
import com.aiotech.aios.accounts.service.bl.ProductPointBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class ProductPointAction extends ActionSupport {

	private static final long serialVersionUID = 6839755806395463325L;

	private static final Logger log = Logger
			.getLogger(ProductPointAction.class);

	// Dependency
	private ProductPointBL productPointBL;

	// Variables
	private long productPointId;
	private long productId;
	private String fromDate;
	private String toDate;
	private String status;
	private String description;
	private double statusPoints;
	private double purchaseAmount;
	private String returnMessage;
	private List<Object> aaData;
	private Object productPointVO;
	private Long recordId;

	// return success
	public String returnSuccess() {
		try {
			log.info("Inside Module: Accounts : Method: returnSuccess");
			CommentVO comment = new CommentVO();
			if (recordId != null && recordId > 0) {
				ProductPoint productPoint = productPointBL
						.getProductPointService().getProductPointById(recordId);
				comment.setRecordId(productPoint.getProductPointId());
				comment.setUseCase(ProductPoint.class.getName());
				comment = productPointBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}else{
				productPointId = 0l;
			}
			recordId = null;
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Show all Product points
	public String showAllProductPoints() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductPoints");
			aaData = productPointBL.showAllProductPoints();
			log.info("Module: Accounts : Method: showAllProductPoints: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductPoints Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductPointApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductPointApproval");
			productPointId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showProductPointApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductPointApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductPointRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductPointRejectEntry");
			productPointId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showProductPointRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductPointRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product points entry screen
	public String showProductPointEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductPointEntry");

			if (productPointId > 0) {
				ProductPoint productPoint = productPointBL
						.getProductPointService().getProductPointById(
								productPointId);
				productPointVO = productPointBL
						.addProductPointsVO(productPoint);
			}
			log.info("Module: Accounts : Method: showProductPointEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductPointEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Product Point
	public String saveProductPoint() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductPoint");
			ProductPoint productPoint = new ProductPoint();
			productPoint.setProductPointId(productPointId > 0 ? productPointId
					: null);
			productPoint.setFromDate(DateFormat.convertStringToDate(fromDate));
			productPoint
					.setToDate(null != toDate && !("").equals(toDate) ? DateFormat
							.convertStringToDate(toDate) : null);
			productPoint.setStatusPoints(statusPoints);
			productPoint.setPurchaseAmount(purchaseAmount);
			productPoint.setStatus(status.equalsIgnoreCase("true") ? true
					: false);
			productPoint.setDescription(description);
			Product product = new Product();
			product.setProductId(productId);
			productPoint.setProduct(product);
			productPointBL.saveProductPoint(productPoint);
			productPointId = 0l;
			recordId = null;
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductPoint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveProductPoint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Product Point
	public String deleteProductPoint() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProductPoint");
			ProductPoint productPoint = productPointBL.getProductPointService()
					.getProductPointById(productPointId);
			productPointBL.deleteProductPoint(productPoint);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProductPoint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteProductPoint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Active purchase points
	public String showActiveProductPoints() {
		try {
			log.info("Inside Module: Accounts : Method: showActiveProductPoints");
			List<ProductPoint> productPoints = productPointBL
					.showActiveProductPoints();
			ServletActionContext.getRequest().setAttribute("PRODUCT_POINTS",
					productPoints);
			log.info("Module: Accounts : Method: showActiveProductPoints: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showActiveProductPoints Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public ProductPointBL getProductPointBL() {
		return productPointBL;
	}

	public void setProductPointBL(ProductPointBL productPointBL) {
		this.productPointBL = productPointBL;
	}

	public long getProductPointId() {
		return productPointId;
	}

	public void setProductPointId(long productPointId) {
		this.productPointId = productPointId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	@JSON(name = "productPointVO")
	public Object getProductPointVO() {
		return productPointVO;
	}

	public void setProductPointVO(Object productPointVO) {
		this.productPointVO = productPointVO;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getStatusPoints() {
		return statusPoints;
	}

	public void setStatusPoints(double statusPoints) {
		this.statusPoints = statusPoints;
	}

	public double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
