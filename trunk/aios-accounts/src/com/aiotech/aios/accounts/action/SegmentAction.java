package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentTreeVO;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentVO;
import com.aiotech.aios.accounts.service.bl.SegmentBL;
import com.aiotech.aios.common.to.Constants.Accounts.Segments;
import com.opensymphony.xwork2.ActionSupport;

public class SegmentAction extends ActionSupport {

	private static final long serialVersionUID = 885506999750236489L;
	private static final Logger LOG = Logger.getLogger(SegmentAction.class);

	private long segmentId;
	private long segmentDetailId;
	private String segmentName;
	private String returnMessage;
	private SegmentBL segmentBL;
	private List<Object> aaData;

	public String returnSuccess() {
		LOG.info("Inside Method returnSuccess()");
		return SUCCESS;
	}

	public String showSegmentTree() {
		try {
			LOG.info("Inside Method showSegmentTree()");
			List<Segment> segments = segmentBL.getSegmentService()
					.getDefaultSegments(segmentBL.getImplementation());
			aaData = new ArrayList<Object>();
			SegmentTreeVO segmentTreeVO = null;
			for (Segment segment : segments) {
				segmentTreeVO = new SegmentTreeVO();
				segmentTreeVO.setKey(segment.getSegmentId());
				segmentTreeVO.setTitle(segment.getSegmentName());
				aaData.add(segmentBL.getSegmentChild(segment, segmentTreeVO));
			}
			LOG.info("Method showSegmentTree() Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Method showSegmentTree() throws Exception " + ex);
			return ERROR;
		}
	}

	public String showSegmentAddEntry() {
		try {
			LOG.info("Inside Method showSegmentAddEntry()");
			Segment segment = segmentBL.getParentSegmentBySegment(segmentBL
					.getSegmentService().getSegmentParentById(segmentId));
			Segment editSegment = null;
			if (segmentDetailId > 0)
				editSegment = segmentBL.getSegmentService().getSegmentById(
						segmentDetailId);
			SegmentVO segmentVO = new SegmentVO();
			BeanUtils.copyProperties(segmentVO, segment);
			segmentVO.setAccessCode(Segments.get(segment.getAccessId()).name());
			ServletActionContext.getRequest().setAttribute("PARENT_SEGMENT",
					segmentVO);
			ServletActionContext.getRequest().setAttribute("SEGMENT_INFO",
					editSegment);
			LOG.info("Method showSegmentAddEntry() Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Method showSegmentAddEntry() throws Exception " + ex);
			return ERROR;
		}
	}

	public String saveSegment() {
		try {
			LOG.info("Inside Method saveSegment()");
			Segment segment = null;
			if (segmentDetailId > 0) {
				segment = segmentBL.getSegmentService().getSegmentParentById(
						segmentDetailId);
				segment.setSegmentId(segmentDetailId);
				segment.setSegment(segment.getSegment());
				segment.setAccessId(segment.getAccessId());
			} else {
				segment = new Segment();
				Segment parentSegment = segmentBL.getSegmentService()
						.getSegmentById(segmentId);
				segment.setAccessId(parentSegment.getAccessId());
				segment.setSegment(parentSegment);
			}
			segment.setSegmentName(segmentName);
			segment.setImplementation(segmentBL.getImplementation());
			segmentBL.saveSegment(segment);
			returnMessage = "SUCCESS";
			LOG.info("Method saveSegment() Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			LOG.info("Method saveSegment() throws Exception " + ex);
			return ERROR;
		}
	}

	public String deleteSegment() {
		try {
			LOG.info("Inside Method deleteSegment()");
			Segment segment = segmentBL.getSegmentService().getSegmentChilds(
					segmentId);
			if (null != segment.getSegment()
					&& (null == segment.getSegments() || segment.getSegments()
							.size() == 0)) {
				segmentBL.deleteSegment(segment);
				returnMessage = "SUCCESS";
			} else
				returnMessage = "Record can't be deleted.(Segment has child entries)";
			LOG.info("Method deleteSegment() Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			LOG.info("Method deleteSegment() throws Exception " + ex);
			return ERROR;
		}
	}

	// Getters & Setters
	public long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public SegmentBL getSegmentBL() {
		return segmentBL;
	}

	public void setSegmentBL(SegmentBL segmentBL) {
		this.segmentBL = segmentBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getSegmentDetailId() {
		return segmentDetailId;
	}

	public void setSegmentDetailId(long segmentDetailId) {
		this.segmentDetailId = segmentDetailId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
