/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.IssueRequistionBL;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 
 * @author Saleem
 */
public class IssueRequistionAction extends ActionSupport {

	private static final long serialVersionUID = 2528910734274685263L;
	private static final Logger LOG = Logger
			.getLogger(IssueRequistionAction.class);
	// Dependency
	private IssueRequistionBL issueRequistionBL;

	// Variables
	private long issueRequistionId;
	private long locationId;
	private long personId;
	private long requisitionId;
	private Long recordId;
	private String referenceNumber;
	private String personName;
	private String issueDate;
	private String description;
	private String requistionDetail;
	private int rowId;
	private Long shippingSourceId;
	private String printableContent;
	private Long customerId;
	private String otherReference;
	private String returnMessage;

	public String returnSuccess() {
		LOG.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// show all issue requisition json list
	public String showAllIssueRequistions() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllIssueRequistions");
			JSONObject jsonObject = issueRequistionBL.getAllIssueRequistions();
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonObject);
			LOG.info("Module: Accounts : Method: showAllIssueRequistions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllIssueRequistions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showIssueRequistionApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showIssueRequistionApproval");
			issueRequistionId = recordId;
			showIssueRequistionEntry();
			LOG.info("Module: Accounts : Method: showIssueRequistionApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showIssueRequistionApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showIssueRequistionRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showIssueRequistionRejectEntry");
			issueRequistionId = recordId;
			showIssueRequistionEntry();
			LOG.info("Module: Accounts : Method: showIssueRequistionRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showIssueRequistionRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Issue Requisition Entry
	public String showIssueRequistionEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showIssueRequistionEntry");
			IssueRequistion issueRequistion = null;
			List<IssueRequistionDetailVO> issueRequistionDetailVOs = null;
			int totalIssuedQuantity = 0;
			if (issueRequistionId > 0) {
				issueRequistion = issueRequistionBL.getIssueRequistionService()
						.getIssueRequistionById(issueRequistionId);
				CommentVO comment = new CommentVO();
				if (recordId != null && issueRequistion != null
						&& issueRequistion.getIssueRequistionId() != 0
						&& recordId > 0) {
					comment.setRecordId(issueRequistion.getIssueRequistionId());
					comment.setUseCase(IssueRequistion.class.getName());
					comment = issueRequistionBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
				List<IssueRequistionDetail> issueRequistionDetails = new ArrayList<IssueRequistionDetail>(
						issueRequistion.getIssueRequistionDetails());
				IssueRequistionDetailVO issueRequistionDetailVO = null;
				Collections.sort(issueRequistionDetails,
						new Comparator<IssueRequistionDetail>() {
							public int compare(IssueRequistionDetail o1,
									IssueRequistionDetail o2) {
								return o1
										.getIssueRequistionDetailId()
										.compareTo(
												o2.getIssueRequistionDetailId());
							}
						});
				double availableQuantity = 0;

				issueRequistionDetailVOs = new ArrayList<IssueRequistionDetailVO>();
				for (IssueRequistionDetail issueRequistionDetail : issueRequistionDetails) {
					availableQuantity = 0;
					totalIssuedQuantity += issueRequistionDetail.getQuantity()
							.intValue();
					issueRequistionDetailVO = new IssueRequistionDetailVO();
					BeanUtils.copyProperties(issueRequistionDetailVO,
							issueRequistionDetail);
					if (null != issueRequistionDetail.getShelf()) {
						List<StockVO> stockVOs = issueRequistionBL.getStockBL()
								.getStockDetails(
										issueRequistionDetail.getProduct()
												.getProductId(),
										issueRequistionDetail.getShelf()
												.getShelfId());
						if (null != stockVOs && stockVOs.size() > 0) {
							for (StockVO stockVO : stockVOs) {
								availableQuantity += stockVO
										.getAvailableQuantity();
							}
						}
						issueRequistionDetailVO
								.setAvailableQuantity(availableQuantity);
					}
					if (((byte) issueRequistionDetail.getProduct()
							.getItemSubType() == (byte) InventorySubCategory.Services
							.getCode())
							|| ((byte) issueRequistionDetail.getProduct()
									.getItemSubType() == (byte) InventorySubCategory.Craft0Manufacturer
									.getCode()))
						issueRequistionDetailVO.setProductType("C");
					else
						issueRequistionDetailVO.setProductType("F");
					if (null != issueRequistionDetail.getProductPackageDetail()) {
						issueRequistionDetailVO
								.setPackageDetailId(issueRequistionDetail
										.getProductPackageDetail()
										.getProductPackageDetailId());
						issueRequistionDetailVO
								.setBaseUnitName(issueRequistionDetail
										.getProduct()
										.getLookupDetailByProductUnit()
										.getDisplayName());
					} else {
						issueRequistionDetailVO
								.setPackageUnit(issueRequistionDetail
										.getQuantity());
						issueRequistionDetailVO.setPackageDetailId(-1l);
					}
					issueRequistionDetailVO.setBaseQuantity(AIOSCommons
							.convertExponential(issueRequistionDetail
									.getQuantity()));
					issueRequistionDetailVO
							.setProductPackageVOs(issueRequistionBL
									.getRequisitionBL()
									.getPurchaseBL()
									.getPackageConversionBL()
									.getProductPackagingDetail(
											issueRequistionDetail.getProduct()
													.getProductId()));
					issueRequistionDetailVO
							.setExpiryDate(null != issueRequistionDetail
									.getProductExpiry() ? DateFormat
									.convertDateToString(issueRequistionDetail
											.getProductExpiry().toString())
									: null);
					issueRequistionDetailVOs.add(issueRequistionDetailVO);
				}
			} else {
				referenceNumber = generateReferenceNumber();
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				User user = (User) sessionObj.get("USER");
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute("ISSUE_REQUISTION",
					issueRequistion);
			ServletActionContext.getRequest().setAttribute(
					"ISSUE_REQUISTION_DETAIL", issueRequistionDetailVOs);
			ServletActionContext.getRequest().setAttribute("TOTAL_QTY",
					totalIssuedQuantity);
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_SOURCE",
					issueRequistionBL.getLookupMasterBL()
							.getActiveLookupDetails("SHIPPING_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"SECTIONS",
					issueRequistionBL.getLookupMasterBL()
							.getActiveLookupDetails("SECTIONS", true));
			LOG.info("Module: Accounts : Method: showIssueRequistionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showIssueRequistionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String issueRequistionPrint() {
		try {
			LOG.info("Inside Module: Accounts : Method: issueRequistionPrint");
			IssueRequistion issueRequistion = null;
			List<IssueRequistionDetailVO> issueRequistionDetailVOs = null;
			double totalIssuedQuantity = 0;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_issuance_template") != null ? ((String) session
					.getAttribute("print_issuance_template"))
					.equalsIgnoreCase("true") : false;
			double totalAmount = 0.0;
			if (issueRequistionId > 0) {
				issueRequistion = issueRequistionBL.getIssueRequistionService()
						.getIssueRequistionById(issueRequistionId);
				CommentVO comment = new CommentVO();
				if (recordId != null && issueRequistion != null
						&& issueRequistion.getIssueRequistionId() != 0
						&& recordId > 0) {
					comment.setRecordId(issueRequistion.getIssueRequistionId());
					comment.setUseCase(IssueRequistion.class.getName());
					comment = issueRequistionBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
				List<IssueRequistionDetail> issueRequistionDetails = new ArrayList<IssueRequistionDetail>(
						issueRequistion.getIssueRequistionDetails());
				IssueRequistionDetailVO issueRequistionDetailVO = null;
				Collections.sort(issueRequistionDetails,
						new Comparator<IssueRequistionDetail>() {
							public int compare(IssueRequistionDetail o1,
									IssueRequistionDetail o2) {
								return o1
										.getIssueRequistionDetailId()
										.compareTo(
												o2.getIssueRequistionDetailId());
							}
						});

				issueRequistionDetailVOs = new ArrayList<IssueRequistionDetailVO>();

				for (IssueRequistionDetail issueRequistionDetail : issueRequistionDetails) {

					issueRequistionDetailVO = new IssueRequistionDetailVO();
					BeanUtils.copyProperties(issueRequistionDetailVO,
							issueRequistionDetail);
					if (issueRequistionDetail.getLookupDetail() != null)
						issueRequistionDetailVO
								.setSection(issueRequistionDetail
										.getLookupDetail().getDisplayName());
					else
						issueRequistionDetailVO.setSection("");
					issueRequistionDetailVO
							.setUnitNameStr(null != issueRequistionDetail
									.getProductPackageDetail() ? issueRequistionDetail
									.getProductPackageDetail()
									.getLookupDetail().getDisplayName()
									: issueRequistionDetail.getProduct()
											.getLookupDetailByProductUnit()
											.getDisplayName());
					issueRequistionDetailVO
							.setQuantity(null != issueRequistionDetail
									.getPackageUnit() ? issueRequistionDetail
									.getPackageUnit() : issueRequistionDetail
									.getQuantity());
					issueRequistionDetailVO.setDisplayTotalAmount(AIOSCommons
							.formatAmount(issueRequistionDetail.getQuantity()
									* issueRequistionDetail.getUnitRate()));
					totalAmount += issueRequistionDetail.getQuantity()
							* issueRequistionDetail.getUnitRate();
					totalIssuedQuantity += issueRequistionDetailVO.getQuantity();
					issueRequistionDetailVOs.add(issueRequistionDetailVO);
				}
			}
			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute(
						"ISSUE_REQUISTION", issueRequistion);
				ServletActionContext.getRequest().setAttribute(
						"ISSUE_REQUISTION_DETAIL", issueRequistionDetailVOs);
				ServletActionContext.getRequest().setAttribute("TOTAL_QTY",
						totalIssuedQuantity);
				ServletActionContext.getRequest()
						.setAttribute(
								"SHIPPING_SOURCE",
								issueRequistionBL.getLookupMasterBL()
										.getActiveLookupDetails(
												"SHIPPING_SOURCE", true));
				ServletActionContext.getRequest().setAttribute(
						"SECTIONS",
						issueRequistionBL.getLookupMasterBL()
								.getActiveLookupDetails("SECTIONS", true));
				LOG.info("Module: Accounts : Method: issueRequistionPrint: Action Success");
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ getImplementId().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-issuance-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementId().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(getImplementId()
							.getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put(
						"datetime",
						issueRequistion.getIssueDate() != null ? DateFormat
								.convertSystemDateToString(issueRequistion
										.getIssueDate()) : "");

				rootMap.put("referenceNumber",
						issueRequistion.getReferenceNumber());
				rootMap.put("description", issueRequistion.getDescription());
				rootMap.put("totalAmount",
						AIOSCommons.formatAmount(totalAmount));
				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(totalAmount));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				String amountInWords = AIOSCommons
						.convertAmountToWords(totalAmount);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("totalAmountInWords", amountInWords);
				rootMap.put("issuanceDetails", issueRequistionDetailVOs);
				rootMap.put("urlPath", urlPath);
				rootMap.put(
						"extraLines",
						issueRequistionDetailVOs.size() < 28 ? 28 - issueRequistionDetailVOs
								.size() : null);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				LOG.info("Module: Accounts : Method: issueRequistionPrint: Action Success");

				return "template";
			}
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: issueRequistionPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Issue Requisition
	public String saveIssueRequisition() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveIssueRequisition");
			IssueRequistion issueRequistion = new IssueRequistion();
			issueRequistion.setIssueDate(DateFormat
					.convertStringToDate(issueDate));
			issueRequistion.setDescription(description);
			Requisition requisition = null;
			if (shippingSourceId > 0) {
				LookupDetail shippingSource = new LookupDetail();
				shippingSource.setLookupDetailId(shippingSourceId);
				issueRequistion.setLookupDetail(shippingSource);
			}
			CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
			cmpDeptLocation.setCmpDeptLocId(locationId);
			issueRequistion.setCmpDeptLocation(cmpDeptLocation);
			Person person = new Person();
			person.setPersonId(personId);
			issueRequistion.setPerson(person);
			if (customerId != null && customerId > 0) {
				Customer customer = new Customer();
				customer.setCustomerId(customerId);
				issueRequistion.setCustomer(customer);
			}
			issueRequistion.setOtherReference(otherReference);
			String[] requistionDetails = splitValues(requistionDetail, "@#");
			List<IssueRequistionDetail> issueRequistionDetails = new ArrayList<IssueRequistionDetail>();
			for (String detail : requistionDetails)
				issueRequistionDetails.add(addissueRequistionDetail(detail));

			List<IssueRequistionDetail> reverseRequisitionDetails = null;
			List<IssueRequistionDetail> deleteRequisitionDetails = null;
			if (issueRequistionId > 0) {
				issueRequistion.setIssueRequistionId(issueRequistionId);
				issueRequistion.setReferenceNumber(referenceNumber);
				reverseRequisitionDetails = issueRequistionBL
						.getIssueRequistionService().getIssueRequistionDetail(
								issueRequistionId);
				deleteRequisitionDetails = userDeletedRequisitionDetails(
						issueRequistionDetails, reverseRequisitionDetails);
			} else
				issueRequistion.setReferenceNumber(generateReferenceNumber());

			if (requisitionId > 0) {
				requisition = issueRequistionBL.getRequisitionBL()
						.getRequisitionService()
						.getRequisitionById(requisitionId);
				issueRequistion.setRequisition(requisition);
				List<IssueRequistion> issueRequistions = issueRequistionBL
						.getIssueRequistionService()
						.getIssueRequistionsByRequistionId(requisitionId);
				List<RequisitionDetail> requisitionDetails = issueRequistionBL
						.getRequisitionBL().getRequisitionService()
						.getRequisitionDetailsById(requisitionId);
				Map<Long, RequisitionDetail> requisitionMap = new HashMap<Long, RequisitionDetail>();
				RequisitionDetail requisitionDetail = null;
				double requisitionQty = 0;
				if (issueRequistionId == 0) {
					for (IssueRequistionDetail requisitionDetail2 : issueRequistionDetails) {
						requisitionDetail = new RequisitionDetail();
						requisitionDetail = requisitionDetail2
								.getRequisitionDetail();
						requisitionDetail.setQuantity(requisitionDetail2
								.getQuantity());
						requisitionMap.put(requisitionDetail2
								.getRequisitionDetail()
								.getRequisitionDetailId(), requisitionDetail);
					}
				}
				requisitionDetail = null;
				if (null != issueRequistions && issueRequistions.size() > 0) {
					for (IssueRequistion issueRequisition : issueRequistions) {
						if (issueRequistionId > 0
								&& (long) issueRequisition
										.getIssueRequistionId() == issueRequistionId)
							issueRequisition
									.setIssueRequistionDetails(new HashSet<IssueRequistionDetail>(
											issueRequistionDetails));
						for (IssueRequistionDetail issueRequisitionDetail : issueRequisition
								.getIssueRequistionDetails()) {
							if (null != issueRequisitionDetail
									.getRequisitionDetail()) {
								requisitionDetail = new RequisitionDetail();
								requisitionQty = 0;
								if (null != requisitionMap
										&& requisitionMap.size() > 0
										&& null != issueRequisitionDetail
												.getRequisitionDetail()
										&& requisitionMap
												.containsKey(issueRequisitionDetail
														.getRequisitionDetail()
														.getRequisitionDetailId())) {
									requisitionDetail = requisitionMap
											.get(issueRequisitionDetail
													.getRequisitionDetail()
													.getRequisitionDetailId());
									requisitionQty = requisitionDetail
											.getQuantity();
									requisitionQty += issueRequisitionDetail
											.getRequisitionDetail()
											.getQuantity();
									requisitionDetail
											.setQuantity(requisitionQty);
								} else
									requisitionDetail = issueRequisitionDetail
											.getRequisitionDetail();
								requisitionMap.put(issueRequisitionDetail
										.getRequisitionDetail()
										.getRequisitionDetailId(),
										requisitionDetail);
							}
						}
					}
				}
				boolean issuedFlag = true;
				if (null != requisitionMap && requisitionMap.size() > 0) {
					for (RequisitionDetail reqDetail : requisitionDetails) {
						requisitionDetail = new RequisitionDetail();
						if (requisitionMap.containsKey(reqDetail
								.getRequisitionDetailId())) {
							requisitionDetail = requisitionMap.get(reqDetail
									.getRequisitionDetailId());
							if (requisitionDetail.getQuantity() < reqDetail
									.getQuantity()) {
								issuedFlag = false;
								break;
							}
						}
					}
				}
				if (issuedFlag)
					requisition.setStatus(RequisitionProcessStatus.Issued
							.getCode());
				else
					requisition.setStatus(RequisitionProcessStatus.InProgress
							.getCode());
			}
			boolean save = issueRequistionBL.saveIssueRequisition(
					issueRequistion, issueRequistionDetails,
					reverseRequisitionDetails, deleteRequisitionDetails,
					requisition);
			if (save) {
				IssueRequistion issueRequistion1 = issueRequistionBL
						.getIssueRequistionService()
						.getIssueRequistionAndDetails(
								issueRequistion.getIssueRequistionId());
				List<Stock> stocks = issueRequistionBL.getStockBL()
						.getStockService()
						.getStockReport(getImplementId(), 0, 0);
				Map<String, Stock> stockMaps = new HashMap<String, Stock>();
				for (Stock stock : stocks) {
					if (stock.getShelf() != null)
						stockMaps.put(stock.getProduct().getProductId() + "_"
								+ stock.getShelf().getShelfId(), stock);
				}
				Stock stock = null;
				for (IssueRequistionDetail issueRequistionDetail2 : issueRequistion1
						.getIssueRequistionDetails()) {
					if (null != issueRequistionDetail2.getShelf()) {
						stock = stockMaps.get(issueRequistionDetail2
								.getProduct().getProductId()
								+ "_"
								+ issueRequistionDetail2.getShelf()
										.getShelfId());
						if (stock != null)
							issueRequistionDetail2
									.setStock(stock.getQuantity());
					}
				}
				issueRequistionBL.getIssueRequistionService()
						.saveIssueRequisition(
								issueRequistion1,
								new ArrayList<IssueRequistionDetail>(
										issueRequistion1
												.getIssueRequistionDetails()));
				returnMessage = "SUCCESS";
				LOG.info("Module: Accounts : Method: saveIssueRequisition: Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Product repeats for the same store.";
				LOG.info("Module: Accounts : Method: saveIssueRequisition: Action Failure");
				return ERROR;
			}
		} catch (Exception ex) {
			returnMessage = "Failure.";
			LOG.info("Module: Accounts : Method: saveIssueRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Issue Requisition
	public String deleteIssueRequisition() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteIssueRequisition");
			IssueRequistion issueRequistion = issueRequistionBL
					.getIssueRequistionService().getIssueRequistionAndDetails(
							issueRequistionId);
			boolean deleteFlag = true;
			for (IssueRequistionDetail requistionDetail : issueRequistion
					.getIssueRequistionDetails()) {
				if (null != requistionDetail.getIssueReturnDetails()
						&& requistionDetail.getIssueReturnDetails().size() > 0) {
					deleteFlag = false;
					continue;
				}
			}
			if (deleteFlag) {
				issueRequistionBL.deleteIssueRequisition(
						issueRequistion,
						new ArrayList<IssueRequistionDetail>(issueRequistion
								.getIssueRequistionDetails()));
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "SUCCESS");
				LOG.info("Module: Accounts : Method: deleteIssueRequisition: Action Success");
				return SUCCESS;
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Issue Requisition has returns.");
				LOG.info("Module: Accounts : Method: deleteIssueRequisition: Action Error");
				return ERROR;
			}
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: deleteIssueRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Issue Requisition By Location
	public String showIssueRequistionLocation() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ISSUE_LOCATION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<IssueRequistionDetail> activeIssues = issueRequistionBL
					.getIssueRequistionService().getLocationIssueRequistion(
							locationId);
			if (null != activeIssues && activeIssues.size() > 0) {
				List<IssueRequistionVO> locationActiveIssues = issueRequistionBL
						.validateIssueRequistionDetail(activeIssues);
				Map<Long, IssueRequistionVO> issueSession = new HashMap<Long, IssueRequistionVO>();
				for (IssueRequistionVO list : locationActiveIssues) {
					issueSession.put(list.getIssueRequistionId(), list);
				}
				session.setAttribute("ISSUE_LOCATION_SESSION_INFO_"
						+ sessionObj.get("jSessionId"), issueSession);
				ServletActionContext.getRequest().setAttribute(
						"ISSUE_REQUISITION_INFO", locationActiveIssues);
				LOG.info("Module: Accounts : Method: showIssueRequistionLocation Success");
				return SUCCESS;
			} else {
				LOG.info("Module: Accounts : Method: showIssueRequistionLocation failure");
				return ERROR;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showIssueRequistionLocation: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Issue Requisition info from session by issue id
	public String showSessionIssueRequistionInfo() {
		LOG.info("Inside Module: Accounts : Method: showSessionIssueRequistionInfo");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, IssueRequistionVO> issueRequistionVOs = (Map<Long, IssueRequistionVO>) (session
					.getAttribute("ISSUE_LOCATION_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			IssueRequistionVO issueRequistionVO = (IssueRequistionVO) issueRequistionVOs
					.get(issueRequistionId);
			ServletActionContext.getRequest().setAttribute("rowId", rowId);
			ServletActionContext.getRequest().setAttribute(
					"ISSUE_REQUISTION_BYID", issueRequistionVO);
			LOG.info("Module: Accounts : Method: showSessionIssueRequistionInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showSessionIssueRequistionInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get issue requisition detail stored in session by issue id
	@SuppressWarnings("unchecked")
	public String showSessionIssueRequistionDetail() {
		LOG.info("Inside Module: Accounts : Method: showSessionIssueRequistionDetail");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, IssueRequistionVO> issueRequistionVOs = (Map<Long, IssueRequistionVO>) (session
					.getAttribute("ISSUE_LOCATION_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			ServletActionContext.getRequest()
					.setAttribute(
							"MATERIAL_CONDITION",
							issueRequistionBL.getLookupMasterBL()
									.getActiveLookupDetails(
											"MATERIAL_CONDITION", true));
			IssueRequistionVO issueRequistionVO = (IssueRequistionVO) issueRequistionVOs
					.get(issueRequistionId);
			ServletActionContext.getRequest().setAttribute("ISSUE_DETAIL_BYID",
					issueRequistionVO.getIssueRequistionDetailVO());
			LOG.info("Module: Accounts : Method: showSessionIssueRequistionDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showSessionIssueRequistionDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Issue Requisition Session
	public String updateRequisitionSession() {
		LOG.info("Inside Module: Accounts : Method: updateRequisitionSession");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, IssueRequistionVO> issueRequistionVOs = (Map<Long, IssueRequistionVO>) (session
					.getAttribute("ISSUE_LOCATION_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			IssueRequistionVO requistionVO = (IssueRequistionVO) issueRequistionVOs
					.get(issueRequistionId);
			String issueRequistionInfo[] = splitValues(requistionDetail, "#@");
			IssueRequistionDetailVO issueRequistionDetailVO = null;
			List<IssueRequistionDetailVO> detailList = new ArrayList<IssueRequistionDetailVO>();
			Map<Long, IssueRequistionDetailVO> detailVOMap = new HashMap<Long, IssueRequistionDetailVO>();
			for (IssueRequistionDetailVO list : requistionVO
					.getIssueRequistionDetailVO()) {
				detailVOMap.put(list.getIssueRequistionDetailId(), list);
			}
			for (String issueRequistion : issueRequistionInfo) {
				String requistionLine[] = splitValues(issueRequistion, "__");
				issueRequistionDetailVO = new IssueRequistionDetailVO();
				if (detailVOMap.containsKey(Long.parseLong(requistionLine[1]))) {
					issueRequistionDetailVO = detailVOMap.get(Long
							.parseLong(requistionLine[1]));
					issueRequistionDetailVO.setReturnQty(Double
							.parseDouble(requistionLine[0]));
					if (!("##").equals(requistionLine[2]))
						issueRequistionDetailVO
								.setDescription(requistionLine[2]);
					if (Long.parseLong(requistionLine[4]) > 0)
						issueRequistionDetailVO.setMaterialCondition(Long
								.parseLong(requistionLine[4]));
					detailList.add(issueRequistionDetailVO);
				}
			}
			requistionVO.setIssueRequistionDetailVO(detailList);
			issueRequistionVOs.put(requistionVO.getIssueRequistionId(),
					requistionVO);
			LOG.info("Module: Accounts : Method: updateRequisitionSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: updateRequisitionSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Generate reference number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(IssueRequistion.class.getName(),
				issueRequistionBL.getImplementation());
	}

	// Add Issue Requisition Detail
	private IssueRequistionDetail addissueRequistionDetail(
			String requistionDetail) throws Exception {
		IssueRequistionDetail issueRequistionDetail = new IssueRequistionDetail();
		Product product = new Product();
		String requistionDetails[] = splitValues(requistionDetail, "__");
		product.setProductId(Long.parseLong(requistionDetails[0]));
		issueRequistionDetail.setProduct(product);
		issueRequistionDetail.setQuantity(Double
				.parseDouble(requistionDetails[1]));
		issueRequistionDetail.setUnitRate(Double
				.parseDouble(requistionDetails[2]));
		if (null != requistionDetails[3]
				&& !("##").equals(requistionDetails[3]))
			issueRequistionDetail.setDescription(requistionDetails[3]);
		if (null != requistionDetails[4]
				&& Long.parseLong(requistionDetails[4]) > 0)
			issueRequistionDetail.setIssueRequistionDetailId(Long
					.parseLong(requistionDetails[4]));
		if (null != requistionDetails[5]
				&& Long.parseLong(requistionDetails[5]) > 0) {
			Shelf shelf = new Shelf();
			shelf.setShelfId(Integer.parseInt(requistionDetails[5]));
			issueRequistionDetail.setShelf(shelf);
		}
		if (null != requistionDetails[6]
				&& Long.parseLong(requistionDetails[6]) > 0) {
			RequisitionDetail requisitionDetail = issueRequistionBL
					.getRequisitionBL()
					.getRequisitionService()
					.requisitionDetailsFindById(
							Long.parseLong(requistionDetails[6]));
			issueRequistionDetail.setRequisitionDetail(requisitionDetail);
		}

		if (null != requistionDetails[7]
				&& Long.parseLong(requistionDetails[7]) > 0) {
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail
					.setLookupDetailId(Long.parseLong(requistionDetails[7]));
			issueRequistionDetail.setLookupDetail(lookupDetail);
		}

		if (null != requistionDetails[8]
				&& Double.parseDouble(requistionDetails[8]) > 0)
			issueRequistionDetail.setStock(Double
					.parseDouble(requistionDetails[8]));
		if (null != requistionDetails[9]
				&& Long.parseLong(requistionDetails[9]) > 0) {
			ProductPackageDetail packageDetail = new ProductPackageDetail();
			packageDetail.setProductPackageDetailId(Long
					.parseLong(requistionDetails[9]));
			issueRequistionDetail.setProductPackageDetail(packageDetail);
		}
		issueRequistionDetail.setPackageUnit(Double
				.parseDouble(requistionDetails[10]));
		if (null != requistionDetails[11]
				&& !("##").equals(requistionDetails[11]))
			issueRequistionDetail.setBatchNumber(requistionDetails[11]);
		if (null != requistionDetails[12]
				&& !("##").equals(requistionDetails[12]))
			issueRequistionDetail.setProductExpiry(DateFormat
					.convertStringToDate(requistionDetails[12]));
		return issueRequistionDetail;
	}

	// Get Deleted values
	private final List<IssueRequistionDetail> userDeletedRequisitionDetails(
			List<IssueRequistionDetail> issueRequistionDetails,
			List<IssueRequistionDetail> reverseRequisitionDetails)
			throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, IssueRequistionDetail> hashRequistionDetail = new HashMap<Long, IssueRequistionDetail>();
		if (null != reverseRequisitionDetails
				&& reverseRequisitionDetails.size() > 0) {
			for (IssueRequistionDetail list : reverseRequisitionDetails) {
				listOne.add(list.getIssueRequistionDetailId());
				hashRequistionDetail.put(list.getIssueRequistionDetailId(),
						list);
			}
			if (null != hashRequistionDetail && hashRequistionDetail.size() > 0) {
				for (IssueRequistionDetail list : issueRequistionDetails) {
					listTwo.add(list.getIssueRequistionDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<IssueRequistionDetail> requistionDetailList = new ArrayList<IssueRequistionDetail>();
		if (null != different && different.size() > 0) {
			IssueRequistionDetail tempRequistionDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempRequistionDetail = new IssueRequistionDetail();
					tempRequistionDetail = hashRequistionDetail.get(list);
					requistionDetailList.add(tempRequistionDetail);
				}
			}
		}
		return requistionDetailList;
	}

	// Add Row
	public String showIssueRequistionAddRow() {
		LOG.info("Inside Module: Accounts : Method: purchaseOrderAddRow");
		ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
		ServletActionContext.getRequest().setAttribute(
				"SECTIONS",
				issueRequistionBL.getLookupMasterBL().getActiveLookupDetails(
						"SECTIONS", true));
		return SUCCESS;
	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// get implementation
	public Implementation getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		} else {
			return null;
		}
	}

	// Getters and Setters
	public IssueRequistionBL getIssueRequistionBL() {
		return issueRequistionBL;
	}

	public void setIssueRequistionBL(IssueRequistionBL issueRequistionBL) {
		this.issueRequistionBL = issueRequistionBL;
	}

	public long getIssueRequistionId() {
		return issueRequistionId;
	}

	public void setIssueRequistionId(long issueRequistionId) {
		this.issueRequistionId = issueRequistionId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getShippingSourceId() {
		return shippingSourceId;
	}

	public void setShippingSourceId(Long shippingSourceId) {
		this.shippingSourceId = shippingSourceId;
	}

	public String getRequistionDetail() {
		return requistionDetail;
	}

	public void setRequistionDetail(String requistionDetail) {
		this.requistionDetail = requistionDetail;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public long getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(long requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getOtherReference() {
		return otherReference;
	}

	public void setOtherReference(String otherReference) {
		this.otherReference = otherReference;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
