package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.WorkinProcess;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessDetail;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessProduction;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.domain.entity.vo.WorkinProcessDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.WorkinProcessProductionVO;
import com.aiotech.aios.accounts.domain.entity.vo.WorkinProcessVO;
import com.aiotech.aios.accounts.service.bl.WorkinProcessBL;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionReqDetailStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionRequisitionStatus;
import com.aiotech.aios.common.to.Constants.Accounts.WorkinProcessStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class WorkinProcessAction extends ActionSupport {

	private static final long serialVersionUID = -2818944028304312726L;
	private static final Logger log = Logger
			.getLogger(WorkinProcessAction.class);
	private WorkinProcessBL workinProcessBL;

	private long workinProcessId;
	private long personId;
	private long productId;
	private Long recordId;
	private long productionRequisitionId;
	private double processQuantity;
	private int shelfId;
	private byte issueStatus;
	private byte addFinishedGoods;
	private byte alterFinishedGoods;
	private byte workinProcessStatus;
	private String referenceNumber;
	private String processDate;
	private String description;
	private String personName;
	private String alertComment;
	private String processDetails;
	private String workinProcessProductions;
	private String returnMessage;
	private List<Object> aaData;

	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showAllWorkInProcessList() {
		try {
			log.info("Inside Module: Accounts : Method: showAllWorkInProcessList");
			aaData = workinProcessBL.getAllWorkInProcess();
			log.info("Module: Accounts : Method: showAllWorkInProcessList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllWorkInProcessList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showWorkInProcessEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showWorkInProcessEntry");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			WorkinProcessVO workinProcessVO = null;
			personId = 0l;
			personName = null;
			referenceNumber = null;
			if (workinProcessId > 0) {
				WorkinProcess workinProcess = workinProcessBL
						.getWorkinProcessService().getWorkinProcessById(
								workinProcessId);
				workinProcessVO = new WorkinProcessVO();
				BeanUtils.copyProperties(workinProcessVO, workinProcess);
				workinProcessVO.setDate(DateFormat
						.convertDateToString(workinProcess.getProcessDate()
								.toString()));
				if (null != workinProcess.getProductionRequisition()) {
					WorkinProcessProductionVO workinProcessProductionVO = new WorkinProcessProductionVO();
					List<WorkinProcessProductionVO> workinProcessProductionVOs = new ArrayList<WorkinProcessProductionVO>();
					for (WorkinProcessProduction production : workinProcess
							.getWorkinProcessProductions()) {
						BeanUtils.copyProperties(workinProcessProductionVO,
								production);
						workinProcessProductionVO
								.setProductionQuantity(null != production
										.getProductionQuantity()
										&& production.getProductionQuantity() > 0 ? production
										.getProductionQuantity() : production
										.getExpectedQuantity());
						workinProcessProductionVO.setStoreName(production
								.getShelf().getShelf().getAisle().getStore()
								.getStoreName()
								+ " >> "
								+ production.getShelf().getShelf().getAisle()
										.getSectionName()
								+ " >> "
								+ production.getShelf().getShelf().getName()
								+ " >> " + production.getShelf().getName());
						workinProcessProductionVO
								.setWorkinProcessDetailVOs(addProductionDetails(
										production.getWorkinProcessDetails(),
										production));
						workinProcessProductionVOs
								.add(workinProcessProductionVO);
						workinProcessVO
								.setWorkinProcessProductionVOs(workinProcessProductionVOs);
					}
					Collections.sort(workinProcessProductionVOs,
							new Comparator<WorkinProcessProductionVO>() {
								public int compare(
										WorkinProcessProductionVO o1,
										WorkinProcessProductionVO o2) {
									return o1
											.getWorkinProcessProductionId()
											.compareTo(
													o2.getWorkinProcessProductionId());
								}
							});
				} else {
					List<WorkinProcessDetailVO> workinProcessDetailVOs = new ArrayList<WorkinProcessDetailVO>();
					WorkinProcessProductionVO workinProcessProductionVO = new WorkinProcessProductionVO();
					for (WorkinProcessProduction production : workinProcess
							.getWorkinProcessProductions()) {
						BeanUtils.copyProperties(workinProcessProductionVO,
								production);
						workinProcessProductionVO
								.setProductionQuantity(null != production
										.getProductionQuantity()
										&& production.getProductionQuantity() > 0 ? production
										.getProductionQuantity() : production
										.getExpectedQuantity());
						workinProcessDetailVOs.addAll(addProductionDetails(
								production.getWorkinProcessDetails(),
								production));
					}
					workinProcessProductionVO
							.setWorkinProcessDetailVOs(workinProcessDetailVOs);
					workinProcessProductionVO
							.setStoreName(workinProcessProductionVO.getShelf()
									.getShelf().getAisle().getStore()
									.getStoreName()
									+ " >> "
									+ workinProcessProductionVO.getShelf()
											.getShelf().getAisle()
											.getSectionName()
									+ " >> "
									+ workinProcessProductionVO.getShelf()
											.getShelf().getName()
									+ " >> "
									+ workinProcessProductionVO.getShelf()
											.getName());
					ServletActionContext.getRequest().setAttribute(
							"WORKIN_PROCESS_PRODUCTION",
							workinProcessProductionVO);
				}
			} else {
				referenceNumber = SystemBL.getReferenceStamp(
						WorkinProcess.class.getName(),
						workinProcessBL.getImplementation());
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			issueStatus = WorkinProcessStatus.Issue_Materials.getCode();
			addFinishedGoods = WorkinProcessStatus.Finshed_Goods.getCode();
			alterFinishedGoods = WorkinProcessStatus.Altered_Finshed_Goods
					.getCode();
			ServletActionContext.getRequest().setAttribute("WORKIN_PROCESS",
					workinProcessVO);
			log.info("Module: Accounts : Method: showWorkInProcessEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showWorkInProcessEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<WorkinProcessDetailVO> addProductionDetails(
			Set<WorkinProcessDetail> workinProcessDetails,
			WorkinProcessProduction production) throws Exception {
		WorkinProcessDetailVO workinProcessDetailVO = null;
		List<WorkinProcessDetailVO> workinProcessDetailVOs = new ArrayList<WorkinProcessDetailVO>();
		for (WorkinProcessDetail workinProcessDetail : workinProcessDetails) {
			workinProcessDetailVO = new WorkinProcessDetailVO();
			BeanUtils
					.copyProperties(workinProcessDetailVO, workinProcessDetail);
			workinProcessDetailVO.setStoreName(workinProcessDetail.getShelf()
					.getShelf().getAisle().getStore().getStoreName()
					+ " >> "
					+ workinProcessDetail.getShelf().getShelf().getAisle()
							.getSectionName()
					+ " >> "
					+ workinProcessDetail.getShelf().getShelf().getName()
					+ " >> " + workinProcessDetail.getShelf().getName());

			workinProcessDetailVO.setProductPackageVOs(workinProcessBL
					.getProductionRequisitionBL()
					.getMaterialIdleMixBL()
					.getPackageConversionBL()
					.getProductPackagingDetail(
							workinProcessDetail.getProduct().getProductId()));
			if (null != workinProcessDetail.getProductPackageDetail()) {
				workinProcessDetailVO.setPackageDetailId(workinProcessDetail
						.getProductPackageDetail().getProductPackageDetailId());
				workinProcessDetailVO.setBaseUnitName(workinProcessDetail
						.getProduct().getLookupDetailByProductUnit()
						.getDisplayName());
				workinProcessDetailVO.setConversionUnitName(workinProcessDetail
						.getProductPackageDetail().getLookupDetail()
						.getDisplayName());
			} else {
				workinProcessDetailVO.setPackageDetailId(-1l);
				if (null == workinProcessDetail.getPackageUnit()
						|| workinProcessDetail.getPackageUnit() <= 0)
					workinProcessDetailVO.setPackageUnit(workinProcessDetail
							.getIssueQuantity());
			}
			workinProcessDetailVO
					.setBaseQuantity(AIOSCommons
							.convertExponential(workinProcessDetail
									.getIssueQuantity()));
			if (null != workinProcessDetail.getWastageQuantity())
				workinProcessDetailVO.setWastageQuantityStr(String
						.valueOf(workinProcessDetail.getWastageQuantity()));
			else {
				MaterialIdleMix idleMix = workinProcessBL
						.getProductionRequisitionBL()
						.getMaterialIdleMixBL()
						.getMaterialIdleMixService()
						.getMaterialIdleMixbyProduct(
								production.getProduct().getProductId());
				if (null != idleMix) {
					for (MaterialIdleMixDetail mixDetail : idleMix
							.getMaterialIdleMixDetails()) {
						if ((long) mixDetail.getProduct().getProductId() == (long) workinProcessDetailVO
								.getProduct().getProductId()) {
							workinProcessDetailVO
									.setWastageQuantityStr(AIOSCommons.convertExponential(workinProcessDetailVO
											.getPackageUnit()
											* (null != mixDetail
													.getTypicalWastage() ? mixDetail
													.getTypicalWastage() : 0)));
							break;
						}
					}
				}
			}
			workinProcessDetailVOs.add(workinProcessDetailVO);
		}
		Collections.sort(workinProcessDetailVOs,
				new Comparator<WorkinProcessDetailVO>() {
					public int compare(WorkinProcessDetailVO o1,
							WorkinProcessDetailVO o2) {
						return o1.getWorkinProcessDetailId().compareTo(
								o2.getWorkinProcessDetailId());
					}
				});
		return workinProcessDetailVOs;
	}

	public String saveWorkInProcess() {
		try {
			log.info("Inside Module: Accounts : Method: saveWorkInProcess");
			WorkinProcess workinProcess = new WorkinProcess();
			workinProcess.setProcessDate(DateFormat
					.convertStringToDate(processDate));
			workinProcess.setDescription(description);
			Person person = new Person();
			person.setPersonId(personId);
			workinProcess.setPerson(person);
			workinProcess.setStatus(workinProcessStatus);
			ProductionRequisition productionRequisition = null;
			if (productionRequisitionId > 0) {
				productionRequisition = workinProcessBL
						.getProductionRequisitionBL()
						.getProductionRequisitionService()
						.getProductionRequisitionById(productionRequisitionId);
				productionRequisition
						.setRequisitionStatus(ProductionRequisitionStatus.Processing
								.getCode());
				workinProcess.setProductionRequisition(productionRequisition);
			}
			Comment comment = null;
			if ((byte) workinProcess.getStatus() == (byte) WorkinProcessStatus.Altered_Finshed_Goods
					.getCode()) {
				comment = new Comment();
				comment.setPersonId(personId);
				comment.setComment(alertComment);
				comment.setUseCase(WorkinProcess.class.getName());
				comment.setRecordId(workinProcessId);
				comment.setCreatedDate(Calendar.getInstance().getTime());
			}
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(workinProcessProductions);
			JSONArray object = (JSONArray) receiveObject;

			WorkinProcessProduction workinProcessProduction = null;
			List<WorkinProcessProduction> workinProcessProductions = new ArrayList<WorkinProcessProduction>();
			List<WorkinProcessProduction> deletedWorkinProcessProductions = null;
			List<WorkinProcessProduction> existingWorkinProcessProductions = null;
			WorkinProcessDetail workinProcessDetail = null;
			List<WorkinProcessDetail> workinProcessDetails = null;
			ProductionRequisitionDetail productionRequisitionDetail = null;
			ProductionRequisitionDetailVO requstDetailVO = null;
			Product product = null;
			Shelf shelf = null;
			Map<Long, ProductionRequisitionDetailVO> productionDetailMap = new HashMap<Long, ProductionRequisitionDetailVO>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("workinProcessProductions");
				for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					workinProcessProduction = new WorkinProcessProduction();
					product = new Product();
					shelf = new Shelf();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"finishedProductId").toString()));
					workinProcessProduction.setProduct(product);
					shelf.setShelfId(Integer.parseInt(jsonObjectDetail.get(
							"shelfId").toString()));
					workinProcessProduction.setShelf(shelf);
					workinProcessProduction.setProductionQuantity(Double
							.parseDouble(jsonObjectDetail
									.get("processQuantity").toString()));
					workinProcessProduction.setExpectedQuantity(Double
							.parseDouble(jsonObjectDetail.get(
									"expectedQuantity").toString()));
					workinProcessProduction
							.setExpectedQuantity(workinProcessProduction
									.getExpectedQuantity() > 0 ? workinProcessProduction
									.getExpectedQuantity() : 1);
					workinProcessProduction
							.setWorkinProcessProductionId((null != jsonObjectDetail
									.get("workinProcessProductionId") && Long
									.parseLong(jsonObjectDetail.get(
											"workinProcessProductionId")
											.toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"workinProcessProductionId")
											.toString()) : null);
					if (null != jsonObjectDetail
							.get("productionRequisitionDetailId")
							&& Long.parseLong(jsonObjectDetail.get(
									"productionRequisitionDetailId").toString()) > 0) {
						productionRequisitionDetail = new ProductionRequisitionDetail();
						productionRequisitionDetail
								.setProductionRequisitionDetailId(Long
										.parseLong(jsonObjectDetail
												.get("productionRequisitionDetailId")
												.toString()));
						workinProcessProduction
								.setProductionRequisitionDetail(productionRequisitionDetail);
						List<WorkinProcessProduction> workinProcessProductionsTemp = workinProcessBL
								.getWorkinProcessService()
								.getWorkinProductionByRequsitionDetailId(
										Long.parseLong(jsonObjectDetail
												.get("productionRequisitionDetailId")
												.toString()));
						if (null != workinProcessProductionsTemp
								&& workinProcessProductionsTemp.size() > 0) {
							for (WorkinProcessProduction processProduction : workinProcessProductionsTemp) {
								requstDetailVO = new ProductionRequisitionDetailVO();
								if (null != productionDetailMap
										&& productionDetailMap.size() > 0
										&& null != processProduction
												.getProductionRequisitionDetail()
										&& productionDetailMap
												.containsKey(processProduction
														.getProductionRequisitionDetail()
														.getProductionRequisitionDetailId())) {
									requstDetailVO = productionDetailMap
											.get(processProduction
													.getProductionRequisitionDetail()
													.getProductionRequisitionDetailId());
									requstDetailVO
											.setQuantity(processProduction
													.getProductionQuantity()
													+ requstDetailVO
															.getQuantity());
								} else {
									requstDetailVO
											.setQuantity(processProduction
													.getProductionQuantity());
								}
								productionDetailMap.put(processProduction
										.getProductionRequisitionDetail()
										.getProductionRequisitionDetailId(),
										requstDetailVO);
							}
						}
						requstDetailVO = new ProductionRequisitionDetailVO();
						if (workinProcessId > 0
								&& null != productionDetailMap
								&& productionDetailMap.size() > 0
								&& productionDetailMap
										.containsKey(productionRequisitionDetail
												.getProductionRequisitionDetailId())) {
							requstDetailVO = productionDetailMap
									.get(productionRequisitionDetail
											.getProductionRequisitionDetailId());
							requstDetailVO.setQuantity(workinProcessProduction
									.getProductionQuantity());
						} else if (null != productionDetailMap
								&& productionDetailMap.size() > 0
								&& productionDetailMap
										.containsKey(productionRequisitionDetail
												.getProductionRequisitionDetailId())) {
							requstDetailVO = productionDetailMap
									.get(productionRequisitionDetail
											.getProductionRequisitionDetailId());
							requstDetailVO.setQuantity(requstDetailVO
									.getQuantity()
									+ workinProcessProduction
											.getProductionQuantity());
						} else {
							requstDetailVO.setQuantity(workinProcessProduction
									.getProductionQuantity());
						}
						productionDetailMap.put(productionRequisitionDetail
								.getProductionRequisitionDetailId(),
								requstDetailVO);
					}
					JSONArray scheduleArray1 = (JSONArray) jsonObjectDetail
							.get("workinProcessDetails");

					workinProcessDetails = new ArrayList<WorkinProcessDetail>();
					for (Iterator<?> iteratorObject1 = scheduleArray1
							.iterator(); iteratorObject1.hasNext();) {
						JSONObject jsonObjectDetail1 = (JSONObject) iteratorObject1
								.next();
						workinProcessDetail = new WorkinProcessDetail();
						product = new Product();
						shelf = new Shelf();
						product.setProductId(Long.parseLong(jsonObjectDetail1
								.get("productId").toString()));
						shelf.setShelfId(Integer.parseInt(jsonObjectDetail1
								.get("shelfId").toString()));
						workinProcessDetail.setProduct(product);
						workinProcessDetail.setShelf(shelf);
						workinProcessDetail.setIssueQuantity(Double
								.parseDouble(jsonObjectDetail1.get(
										"issueQuantity").toString()));
						workinProcessDetail
								.setDescription(null != jsonObjectDetail1
										.get("description") ? jsonObjectDetail1
										.get("description").toString() : null);
						workinProcessDetail
								.setWorkinProcessDetailId((null != jsonObjectDetail1
										.get("workinProcessDetailId") && Long
										.parseLong(jsonObjectDetail1.get(
												"workinProcessDetailId")
												.toString()) > 0) ? Long
										.parseLong(jsonObjectDetail1.get(
												"workinProcessDetailId")
												.toString()) : null);
						if ((null != jsonObjectDetail1.get("packageType") && Long
								.parseLong(jsonObjectDetail1.get("packageType")
										.toString()) > 0)) {
							ProductPackageDetail packageDetail = new ProductPackageDetail();
							packageDetail.setProductPackageDetailId(Long
									.parseLong(jsonObjectDetail1.get(
											"packageType").toString()));
							workinProcessDetail
									.setProductPackageDetail(packageDetail);
						}
						workinProcessDetail.setPackageUnit(Double
								.parseDouble(jsonObjectDetail1.get(
										"packageUnit").toString()));
						if (null != workinProcessDetail
								.getWorkinProcessDetailId()
								&& workinProcessDetail
										.getWorkinProcessDetailId() > 0) {
							workinProcessDetail.setWastageQuantity(Double
									.parseDouble(jsonObjectDetail1.get(
											"wastageQuantity").toString()));
						}
						workinProcessDetail
								.setWorkinProcessProduction(workinProcessProduction);
						workinProcessDetails.add(workinProcessDetail);
					}
					workinProcessProduction
							.setWorkinProcessDetails(new HashSet<WorkinProcessDetail>(
									workinProcessDetails));
				}
				workinProcessProductions.add(workinProcessProduction);
			}
			WorkinProcess existingWorkinProcess = null;
			if (workinProcessId > 0) {
				workinProcess.setWorkinProcessId(workinProcessId);
				workinProcess.setReferenceNumber(referenceNumber);
				existingWorkinProcess = workinProcessBL
						.getWorkinProcessService().getWorkinProcessById(
								workinProcessId);
				existingWorkinProcessProductions = new ArrayList<WorkinProcessProduction>(
						existingWorkinProcess.getWorkinProcessProductions());
				deletedWorkinProcessProductions = getUserDeletedWorkinProcessProductions(
						existingWorkinProcessProductions,
						workinProcessProductions);
			} else {
				workinProcess.setReferenceNumber(SystemBL.getReferenceStamp(
						WorkinProcess.class.getName(),
						workinProcessBL.getImplementation()));
			}
			List<ProductionRequisitionDetail> productionRequisitionDetails = null;
			if (productionRequisitionId > 0 && null != productionDetailMap
					&& productionDetailMap.size() > 0) {
				List<ProductionRequisitionDetail> productionRequisitionDetailsTemp = workinProcessBL
						.getProductionRequisitionBL()
						.getProductionRequisitionService()
						.getProductionRequistionDetail(productionRequisitionId);
				ProductionRequisitionDetailVO requisitionDetailVO = null;
				for (ProductionRequisitionDetail reqDt : productionRequisitionDetailsTemp) {
					if (!productionDetailMap.containsKey(reqDt
							.getProductionRequisitionDetailId())) {
						requisitionDetailVO = new ProductionRequisitionDetailVO();
						BeanUtils.copyProperties(requisitionDetailVO, reqDt);
						productionDetailMap.put(
								reqDt.getProductionRequisitionDetailId(),
								requisitionDetailVO);
					}
				}
				productionRequisitionDetails = new ArrayList<ProductionRequisitionDetail>();
				ProductionRequisitionDetail requisitionDetail = null;
				for (WorkinProcessProduction production : workinProcessProductions) {
					if (productionDetailMap.containsKey(production
							.getProductionRequisitionDetail()
							.getProductionRequisitionDetailId())) {
						requisitionDetail = workinProcessBL
								.getProductionRequisitionBL()
								.getProductionRequisitionService()
								.getProductionRequistionDetailById(
										production
												.getProductionRequisitionDetail()
												.getProductionRequisitionDetailId());
						if ((double) requisitionDetail.getQuantity() <= (double) productionDetailMap
								.get(production
										.getProductionRequisitionDetail()
										.getProductionRequisitionDetailId())
								.getQuantity())
							requisitionDetail
									.setStatus(ProductionReqDetailStatus.Closed
											.getCode());
						else
							requisitionDetail
									.setStatus(ProductionReqDetailStatus.Processing
											.getCode());
						requstDetailVO = new ProductionRequisitionDetailVO();
						BeanUtils.copyProperties(requstDetailVO,
								requisitionDetail);
						productionDetailMap.put(production
								.getProductionRequisitionDetail()
								.getProductionRequisitionDetailId(),
								requstDetailVO);
						productionRequisitionDetails.add(requisitionDetail);
					}
				}
			}
			boolean saveWIp = this.validateIssueMaterialStocks(
					workinProcessProductions, existingWorkinProcessProductions);
			if (saveWIp) {
				workinProcessBL.saveWorkInProcess(workinProcess,
						workinProcessProductions,
						existingWorkinProcessProductions,
						deletedWorkinProcessProductions, existingWorkinProcess,
						productionRequisition, productionRequisitionDetails,
						productionDetailMap, comment);
				workinProcessId = workinProcess.getWorkinProcessId();
				returnMessage = "SUCCESS";
			} else
				returnMessage = "Please check, Issuing material is insufficient.";
			log.info("Module: Accounts : Method: saveWorkInProcess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveWorkInProcess Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public Boolean validateIssueMaterialStocks(
			List<WorkinProcessProduction> workinProcessProductions,
			List<WorkinProcessProduction> existingWorkinProcessProductions)
			throws Exception {
		Map<String, WorkinProcessDetail> productionMap = null;
		if (null != existingWorkinProcessProductions
				&& existingWorkinProcessProductions.size() > 0) {
			productionMap = new HashMap<String, WorkinProcessDetail>();
			for (WorkinProcessProduction workinProcessProduction : existingWorkinProcessProductions) {
				for (WorkinProcessDetail workinProcessDetail : workinProcessProduction
						.getWorkinProcessDetails()) {
					productionMap.put(
							workinProcessDetail.getWorkinProcessDetailId()
									+ "-"
									+ workinProcessDetail.getProduct()
											.getProductId()
									+ "-"
									+ workinProcessDetail.getShelf()
											.getShelfId(), workinProcessDetail);
				}
			}
		}
		double availableStock = 0;
		for (WorkinProcessProduction workinProcessProduction : workinProcessProductions) {
			for (WorkinProcessDetail workinProcessDetail : workinProcessProduction
					.getWorkinProcessDetails()) {
				availableStock = 0;
				String key = "";
				if (null != workinProcessDetail.getWorkinProcessDetailId()) {
					key = workinProcessDetail.getWorkinProcessDetailId() + "-"
							+ workinProcessDetail.getProduct().getProductId()
							+ "-" + workinProcessDetail.getShelf().getShelfId();
					if (productionMap.containsKey(key)) {
						WorkinProcessDetail workinProcessDetailtmp = productionMap
								.get(key);
						if (null != workinProcessDetailtmp
								.getProductPackageDetail())
							availableStock += workinProcessBL
									.getProductionRequisitionBL()
									.getMaterialIdleMixBL()
									.getPackageConversionBL()
									.getPackageBaseQuantity(
											workinProcessDetailtmp
													.getProductPackageDetail()
													.getProductPackageDetailId(),
											(workinProcessDetailtmp
													.getIssueQuantity() + (null != workinProcessDetailtmp
													.getWastageQuantity() ? workinProcessDetailtmp
													.getWastageQuantity() : 0)));
						else
							availableStock += (workinProcessDetailtmp
									.getIssueQuantity() + (null != workinProcessDetailtmp
									.getWastageQuantity() ? workinProcessDetailtmp
									.getWastageQuantity() : 0));
					}
				}
				List<StockVO> stockVOs = workinProcessBL
						.getStockBL()
						.getStockDetails(
								workinProcessDetail.getProduct().getProductId(),
								workinProcessDetail.getShelf().getShelfId());
				double issueQty = 0;
				if (null != stockVOs && stockVOs.size() > 0) {
					for (StockVO stockVO : stockVOs)
						availableStock += stockVO.getAvailableQuantity();
					if (null != workinProcessDetail.getProductPackageDetail())
						issueQty = workinProcessBL
								.getProductionRequisitionBL()
								.getMaterialIdleMixBL()
								.getPackageConversionBL()
								.getPackageBaseQuantity(
										workinProcessDetail
												.getProductPackageDetail()
												.getProductPackageDetailId(),
										(workinProcessDetail.getIssueQuantity() + (null != workinProcessDetail
												.getWastageQuantity() ? workinProcessDetail
												.getWastageQuantity() : 0)));
					else
						issueQty = (workinProcessDetail.getIssueQuantity() + (null != workinProcessDetail
								.getWastageQuantity() ? workinProcessDetail
								.getWastageQuantity() : 0));
					if (availableStock >= issueQty)
						continue;
					else
						return false;
				} else
					return false;
			}
		}
		return true;
	}

	public String deleteWorkInProcess() {
		try {
			log.info("Inside Module: Accounts : Method: deleteWorkInProcess");
			WorkinProcess workinProcess = workinProcessBL
					.getWorkinProcessService().getWorkinProcessById(
							workinProcessId);
			workinProcessBL.deleteWorkInProcess(workinProcess);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteWorkInProcess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteWorkInProcess Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<WorkinProcessProduction> getUserDeletedWorkinProcessProductions(
			List<WorkinProcessProduction> existingWorkinProcessProductions,
			List<WorkinProcessProduction> workinProcessProductions)
			throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, WorkinProcessProduction> hashWPDetail = new HashMap<Long, WorkinProcessProduction>();
		if (null != existingWorkinProcessProductions
				&& existingWorkinProcessProductions.size() > 0) {
			for (WorkinProcessProduction list : existingWorkinProcessProductions) {
				listOne.add(list.getWorkinProcessProductionId());
				hashWPDetail.put(list.getWorkinProcessProductionId(), list);
			}
			if (null != hashWPDetail && hashWPDetail.size() > 0) {
				for (WorkinProcessProduction list : workinProcessProductions) {
					listTwo.add(list.getWorkinProcessProductionId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<WorkinProcessProduction> deletedWPDetails = null;
		if (null != different && different.size() > 0) {
			WorkinProcessProduction tempWPDetail = null;
			deletedWPDetails = new ArrayList<WorkinProcessProduction>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempWPDetail = new WorkinProcessProduction();
					tempWPDetail = hashWPDetail.get(list);
					deletedWPDetails.add(tempWPDetail);
				}
			}
		}
		return deletedWPDetails;
	}

	@SuppressWarnings("unused")
	private List<WorkinProcessDetail> getUserDeletedWorkinProcessDetails(
			List<WorkinProcessDetail> existingWorkinProcessDetails,
			List<WorkinProcessDetail> workinProcessDetails) throws Exception {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, WorkinProcessDetail> hashWPDetail = new HashMap<Long, WorkinProcessDetail>();
		if (null != existingWorkinProcessDetails
				&& existingWorkinProcessDetails.size() > 0) {
			for (WorkinProcessDetail list : existingWorkinProcessDetails) {
				listOne.add(list.getWorkinProcessDetailId());
				hashWPDetail.put(list.getWorkinProcessDetailId(), list);
			}
			if (null != hashWPDetail && hashWPDetail.size() > 0) {
				for (WorkinProcessDetail list : workinProcessDetails) {
					listTwo.add(list.getWorkinProcessDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<WorkinProcessDetail> deletedWPDetails = null;
		if (null != different && different.size() > 0) {
			WorkinProcessDetail tempWPDetail = null;
			deletedWPDetails = new ArrayList<WorkinProcessDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempWPDetail = new WorkinProcessDetail();
					tempWPDetail = hashWPDetail.get(list);
					deletedWPDetails.add(tempWPDetail);
				}
			}
		}
		return deletedWPDetails;
	}

	// Getters & Setters
	public WorkinProcessBL getWorkinProcessBL() {
		return workinProcessBL;
	}

	public void setWorkinProcessBL(WorkinProcessBL workinProcessBL) {
		this.workinProcessBL = workinProcessBL;
	}

	public long getWorkinProcessId() {
		return workinProcessId;
	}

	public void setWorkinProcessId(long workinProcessId) {
		this.workinProcessId = workinProcessId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public double getProcessQuantity() {
		return processQuantity;
	}

	public void setProcessQuantity(double processQuantity) {
		this.processQuantity = processQuantity;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getProcessDate() {
		return processDate;
	}

	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getProcessDetails() {
		return processDetails;
	}

	public void setProcessDetails(String processDetails) {
		this.processDetails = processDetails;
	}

	public byte getIssueStatus() {
		return issueStatus;
	}

	public void setIssueStatus(byte issueStatus) {
		this.issueStatus = issueStatus;
	}

	public byte getAddFinishedGoods() {
		return addFinishedGoods;
	}

	public void setAddFinishedGoods(byte addFinishedGoods) {
		this.addFinishedGoods = addFinishedGoods;
	}

	public byte getAlterFinishedGoods() {
		return alterFinishedGoods;
	}

	public void setAlterFinishedGoods(byte alterFinishedGoods) {
		this.alterFinishedGoods = alterFinishedGoods;
	}

	public byte getWorkinProcessStatus() {
		return workinProcessStatus;
	}

	public void setWorkinProcessStatus(byte workinProcessStatus) {
		this.workinProcessStatus = workinProcessStatus;
	}

	public String getWorkinProcessProductions() {
		return workinProcessProductions;
	}

	public void setWorkinProcessProductions(String workinProcessProductions) {
		this.workinProcessProductions = workinProcessProductions;
	}

	public long getProductionRequisitionId() {
		return productionRequisitionId;
	}

	public void setProductionRequisitionId(long productionRequisitionId) {
		this.productionRequisitionId = productionRequisitionId;
	}

	public String getAlertComment() {
		return alertComment;
	}

	public void setAlertComment(String alertComment) {
		this.alertComment = alertComment;
	}
}
