package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Eibor;
import com.aiotech.aios.accounts.service.bl.EiborBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.EiborType;
import com.aiotech.aios.common.to.Constants.Accounts.InstitutionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

public class EiborAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	// Common variables
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String eiborDate;
	private char eiborType;
	private long bankId;
	private long eiborId;
	private long combinationId;
	private double eiborRate;
	private static String returnMessage;
	// Common Objects
	private EiborBL eiborBL;
	private Eibor eibor = null;
	private Implementation implementation;
	// Logger property
	private static final Logger LOGGER = LogManager
			.getLogger(EiborAction.class);

	public String returnSuccess() {
		return SUCCESS;
	}

	public String showEntryScreen() {
		LOGGER.info("Module: Accounts : Method: showEntryScreen");
		try {
			Map<Character, EiborType> eiborType = new HashMap<Character, EiborType>();
			for (EiborType e : EnumSet.allOf(EiborType.class)) {
				eiborType.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("EIBOR_TYPES",
					eiborType);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showEiborList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showEiborList() {
		LOGGER.info("Module: Accounts : Method: showEiborList");
		try {
			List<Eibor> eiborList = getEiborBL().getEiborList(getImplementId());
			iTotalRecords = eiborList.size();
			iTotalDisplayRecords = eiborList.size();
			if (eiborList != null && eiborList.size() > 0) {
				LOGGER.info("Module: Accounts : Method: showEiborList, "
						+ " Account List size() " + eiborList.size());
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Eibor list : eiborList) {
				JSONArray array = new JSONArray();
				array.add(list.getEiborId());
				array.add(list.getEiborType());
				array.add(list.getCombination().getCombinationId());
				array.add(Constants.Accounts.EiborType.get(list.getEiborType()));
				array.add(DateFormat.convertDateToString(list.getEiborDate()
						.toString()));
				array.add(list.getEiborRate());
				array.add(list.getCombination().getAccountByNaturalAccountId()
						.getCode()
						+ "["
						+ list.getCombination().getAccountByNaturalAccountId()
								.getAccount() + "]");
				array.add("<span class='delete' id='del_" + list.getEiborId()
						+ "'>Delete</span>");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showEiborList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showEiborList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBankList() {
		LOGGER.info("Module: Accounts : Method: showBankList");
		try {
			List<Bank> bankList = getEiborBL().getBankService().getAllBanks(
					getImplementId(), InstitutionType.Bank.getCode());
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			LOGGER.info("Module: Accounts : Method: showBankList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBankList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveEiborEntry() {
		LOGGER.info("Module: Accounts : Method: saveEiborEntry");
		try {
			eibor = new Eibor();
			eibor.setEiborType(eiborType);
			if (eiborId > 0)
				eibor.setEiborId(eiborId);

			Combination combination = new Combination();
			combination.setCombinationId(combinationId);
			eibor.setCombination(combination);
			eibor.setEiborRate(eiborRate);
			eibor.setImplementation(getImplementId());
			eibor.setEiborDate(DateFormat.convertStringToDate(eiborDate));
			getEiborBL().getEiborService().saveOrUpdateEibor(getEibor());
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_STATUS",
					returnMessage);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_STATUS",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveEiborEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteEiborEntry() {
		LOGGER.info("Module: Accounts : Method: deleteEiborEntry");
		try {
			eibor = getEiborBL().getEiborService().getEiborById(eiborId);
			getEiborBL().getEiborService().deleteEibor(eibor);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_STATUS",
					returnMessage);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_STATUS",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteEiborEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public Implementation getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
			return implementation;
		} else {
			return null;
		}
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public EiborBL getEiborBL() {
		return eiborBL;
	}

	public void setEiborBL(EiborBL eiborBL) {
		this.eiborBL = eiborBL;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Eibor getEibor() {
		return eibor;
	}

	public void setEibor(Eibor eibor) {
		this.eibor = eibor;
	}

	public char getEiborType() {
		return eiborType;
	}

	public void setEiborType(char eiborType) {
		this.eiborType = eiborType;
	}

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public long getEiborId() {
		return eiborId;
	}

	public void setEiborId(long eiborId) {
		this.eiborId = eiborId;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getEiborDate() {
		return eiborDate;
	}

	public void setEiborDate(String eiborDate) {
		this.eiborDate = eiborDate;
	}

	public static String getReturnMessage() {
		return returnMessage;
	}

	public static void setReturnMessage(String returnMessage) {
		EiborAction.returnMessage = returnMessage;
	}

	public double getEiborRate() {
		return eiborRate;
	}

	public void setEiborRate(double eiborRate) {
		this.eiborRate = eiborRate;
	}
}
