package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferVO;
import com.aiotech.aios.accounts.service.bl.MaterialTransferBL;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MaterialTransferAction extends ActionSupport {

	private static final long serialVersionUID = 465444498921232906L;

	private static final Logger log = Logger
			.getLogger(MaterialTransferAction.class);

	// Dependency
	private MaterialTransferBL materialTransferBL;

	// Variables
	private long materialTransferId;
	private long transferPerson;
	private int rowId;
	private int storeId;
	private int shelfId;
	private String referenceNumber;
	private String transferDate;
	private String description;
	private String transferDetails;
	private String requisitionDetails;
	private String returnMessage;
	private String personName;
	private Long recordId;
	private Long alertId;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show All Material Transfer
	public String showAllMaterialTransfer() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMaterialTransfer");
			aaData = materialTransferBL.showAllMaterialTransfer();
			log.info("Module: Accounts : Method: showAllMaterialTransfer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllMaterialTransfer Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMaterialTransferApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialTransferApproval");
			materialTransferId = recordId;
			showMaterialTransferEntry();
			log.info("Module: Accounts : Method: showMaterialTransferApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialTransferApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMaterialTransferRejectEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialTransferRejectEntry");
			materialTransferId = recordId;
			showMaterialTransferEntry();
			log.info("Module: Accounts : Method: showMaterialTransferRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialTransferRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMaterialTransferByStore() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialTransferByStore");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Shelf> shelfs = materialTransferBL.getStoreBL()
					.getStoreService()
					.getShelfsByStoreId(Long.valueOf(storeId));
			if (null != shelfs && shelfs.size() > 0) {
				Set<Integer> shelfIds = new HashSet<Integer>();
				for (Shelf shelf : shelfs)
					shelfIds.add(shelf.getShelfId());
				List<MaterialTransferDetail> materialTransferDetails = materialTransferBL
						.getMaterialTransferService()
						.getMaterialTransferByShelfs(
								materialTransferBL.getImplementation(),
								shelfIds);
				if (null != materialTransferDetails
						&& materialTransferDetails.size() > 0) {
					List<MaterialTransferVO> materialTransferVOs = materialTransferBL
							.validateMaterialTransfers(materialTransferDetails);
					if (null != materialTransferVOs
							&& materialTransferVOs.size() > 0) {
						Map<Long, MaterialTransferVO> transferSession = new HashMap<Long, MaterialTransferVO>();
						for (MaterialTransferVO list : materialTransferVOs) {
							transferSession.put(list.getMaterialTransferId(),
									list);
						}
						session.setAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
								+ sessionObj.get("jSessionId"), transferSession);
						ServletActionContext.getRequest().setAttribute(
								"MATERIAL_TRANSFER_INFO", materialTransferVOs);
					}
				}
			}
			log.info("Module: Accounts : Method: showMaterialTransferByStore: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialTransferByStore Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSessionMaterialTransferInfo() {
		try {
			log.info("Inside Module: Accounts : Method: showSessionMaterialTransferInfo");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, MaterialTransferVO> materialTransferVOs = (Map<Long, MaterialTransferVO>) (session
					.getAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			MaterialTransferVO materialTransferVO = (MaterialTransferVO) materialTransferVOs
					.get(materialTransferId);
			ServletActionContext.getRequest().setAttribute("rowId", rowId);
			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_TRANSFER_BYID", materialTransferVO);
			log.info("Module: Accounts : Method: showSessionMaterialTransferInfo: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showSessionMaterialTransferInfo Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String showSessionMaterialTransferDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showSessionMaterialTransferDetails");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, MaterialTransferVO> materialTransferVOs = (Map<Long, MaterialTransferVO>) (session
					.getAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			ServletActionContext.getRequest()
					.setAttribute(
							"MATERIAL_CONDITION",
							materialTransferBL.getLookupMasterBL()
									.getActiveLookupDetails(
											"MATERIAL_CONDITION", true));
			MaterialTransferVO materialTransferVO = (MaterialTransferVO) materialTransferVOs
					.get(materialTransferId);
			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_TRANSFER_DETAIL",
					materialTransferVO.getMaterialTransferDetailVOs());
			log.info("Module: Accounts : Method: showSessionMaterialTransferDetails Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showSessionMaterialTransferDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String updateSessionMaterialTransfer() {
		try {
			log.info("Inside Module: Accounts : Method: updateSessionMaterialTransfer");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, MaterialTransferVO> materialTransferVOs = (Map<Long, MaterialTransferVO>) (session
					.getAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			MaterialTransferVO materialTransferVO = (MaterialTransferVO) materialTransferVOs
					.get(materialTransferId);
			String materialTransferInfo[] = splitValues(transferDetails, "#@");
			MaterialTransferDetailVO materialTransferDetailVO = null;
			List<MaterialTransferDetailVO> detailList = new ArrayList<MaterialTransferDetailVO>();
			Map<Long, MaterialTransferDetailVO> detailVOMap = new HashMap<Long, MaterialTransferDetailVO>();
			for (MaterialTransferDetailVO list : materialTransferVO
					.getMaterialTransferDetailVOs()) {
				detailVOMap.put(list.getMaterialTransferDetailId(), list);
			}
			for (String issueRequistion : materialTransferInfo) {
				String requistionLine[] = splitValues(issueRequistion, "__");
				materialTransferDetailVO = new MaterialTransferDetailVO();
				if (detailVOMap.containsKey(Long.parseLong(requistionLine[1]))) {
					materialTransferDetailVO = detailVOMap.get(Long
							.parseLong(requistionLine[1]));
					materialTransferDetailVO.setReturnQty(Double
							.parseDouble(requistionLine[0]));
					if (!("##").equals(requistionLine[2]))
						materialTransferDetailVO
								.setDescription(requistionLine[2]);
					if (Long.parseLong(requistionLine[4]) > 0)
						materialTransferDetailVO.setMaterialCondition(Long
								.parseLong(requistionLine[4]));
					detailList.add(materialTransferDetailVO);
				}
			}
			materialTransferVO.setMaterialTransferDetailVOs(detailList);
			materialTransferVOs.put(materialTransferVO.getMaterialTransferId(),
					materialTransferVO);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updateSessionMaterialTransfer Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: updateSessionMaterialTransfer: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Show Material Transfer Entry
	public String showMaterialTransferEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialTransferEntry");
			MaterialTransfer materialTransfer = null;
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			if (materialTransferId > 0)
				materialTransfer = materialTransferBL
						.getMaterialTransferService()
						.getAllMaterialTransferById(materialTransferId);
			else {
				referenceNumber = SystemBL.getReferenceStamp(
						MaterialTransfer.class.getName(),
						materialTransferBL.getImplementation());
				transferPerson = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			loadLookupDetails();
			CommentVO comment = new CommentVO();
			if (recordId != null && materialTransfer != null
					&& materialTransfer.getMaterialTransferId() != 0
					&& recordId > 0) {
				comment.setRecordId(materialTransfer.getMaterialTransferId());
				comment.setUseCase(MaterialTransfer.class.getName());
				comment = materialTransferBL.getCommentBL().getCommentInfo(
						comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			ServletActionContext.getRequest().setAttribute("MATERIAL_TRANSFER",
					materialTransfer);
			log.info("Module: Accounts : Method: showMaterialTransferEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialTransferEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Material Transfer
	@SuppressWarnings("unchecked")
	public String saveMaterialTransfer() {
		try {
			log.info("Inside Module: Accounts : Method: saveMaterialTransfer");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			MaterialTransfer materialTransfer = new MaterialTransfer();
			materialTransfer.setTransferDate(DateFormat
					.convertStringToDate(transferDate));
			materialTransfer.setDescription(description);
			Person person = new Person();
			person.setPersonId(transferPerson);
			materialTransfer.setPersonByTransferPerson(person);
			List<MaterialTransferDetail> materialTransferDetails = new ArrayList<MaterialTransferDetail>();
			MaterialTransferDetail materialTransferDetail = null;
			List<MaterialRequisition> materialRequisitions = null;
			Shelf fromShelf = null;
			Shelf shelf = null;
			LookupDetail lookupDetail = null;
			MaterialRequisitionDetail materialRequisitionDetail = null;
			String[] materialRequisitionIds = null;
			Map<Long, MaterialRequisitionDetailVO> requisitionMap = new HashMap<Long, MaterialRequisitionDetailVO>();
			if (storeId == 0 && null != transferDetails
					&& !transferDetails.equals("")) {
				materialRequisitionIds = new String[1];
				JSONParser parser = new JSONParser();
				Object transferObject = parser.parse(transferDetails);
				JSONArray object = (JSONArray) transferObject;
				Product product = null;
				for (Iterator<?> iterator = object.iterator(); iterator
						.hasNext();) {
					JSONObject jsonObject = (JSONObject) iterator.next();
					JSONArray detailArray = (JSONArray) jsonObject
							.get("transferDetails");
					for (Iterator<?> iteratorObject = detailArray.iterator(); iteratorObject
							.hasNext();) {
						JSONObject jsonObjectDetail = (JSONObject) iteratorObject
								.next();
						materialTransferDetail = new MaterialTransferDetail();
						product = new Product();
						fromShelf = new Shelf();
						product.setProductId(Long.parseLong(jsonObjectDetail
								.get("productId").toString()));
						materialTransferDetail.setProduct(product);
						fromShelf.setShelfId(Integer.parseInt(jsonObjectDetail
								.get("storeFromId").toString()));
						materialTransferDetail.setShelfByFromShelfId(fromShelf);
						shelf = new Shelf();
						shelf.setShelfId(Integer.parseInt(jsonObjectDetail.get(
								"shelfId").toString()));
						materialTransferDetail.setShelfByShelfId(shelf);
						materialTransferDetail.setQuantity(Double
								.parseDouble(jsonObjectDetail.get("productQty")
										.toString()));
						lookupDetail = new LookupDetail();
						lookupDetail.setLookupDetailId(Long
								.parseLong(jsonObjectDetail.get("transferType")
										.toString()));
						materialTransferDetail
								.setLookupDetailByTransferType(lookupDetail);
						if (null != jsonObjectDetail.get("transferReason")
								&& Long.parseLong(jsonObjectDetail.get(
										"transferReason").toString()) > 0) {
							lookupDetail = new LookupDetail();
							lookupDetail.setLookupDetailId(Long
									.parseLong(jsonObjectDetail.get(
											"transferReason").toString()));
							materialTransferDetail
									.setLookupDetailByTransferReason(lookupDetail);
						}
						materialTransferDetail.setUnitRate(Double
								.parseDouble(jsonObjectDetail.get("amount")
										.toString()));
						materialTransferDetail
								.setDescription((null != jsonObjectDetail
										.get("linesDescription") && !("")
										.equals(jsonObjectDetail.get(
												"linesDescription").toString())) ? jsonObjectDetail
										.get("linesDescription").toString()
										: null);
						materialTransferDetail
								.setMaterialTransferDetailId((null != jsonObjectDetail
										.get("materialTransferDetailId") && Long
										.parseLong(jsonObjectDetail.get(
												"materialTransferDetailId")
												.toString()) > 0) ? Long
										.parseLong(jsonObjectDetail.get(
												"materialTransferDetailId")
												.toString()) : null);
						if (null != jsonObjectDetail
								.get("materialRequisitionDetailId")
								&& Long.parseLong(jsonObjectDetail.get(
										"materialRequisitionDetailId")
										.toString()) > 0) {
							materialRequisitionDetail = materialTransferBL
									.getMaterialRequisitionBL()
									.getMaterialRequisitionService()
									.getMaterialRequisitionDetailByDetailId(
											Long.parseLong(jsonObjectDetail
													.get("materialRequisitionDetailId")
													.toString()));
							materialTransferDetail
									.setMaterialRequisitionDetail(materialRequisitionDetail);
							MaterialRequisitionDetailVO tempMaterialRequisitionDetailVO = new MaterialRequisitionDetailVO();
							BeanUtils.copyProperties(
									tempMaterialRequisitionDetailVO,
									materialRequisitionDetail);
							tempMaterialRequisitionDetailVO
									.setQuantity(materialTransferDetail
											.getQuantity());
							requisitionMap.put(tempMaterialRequisitionDetailVO
									.getMaterialRequisitionDetailId(),
									tempMaterialRequisitionDetailVO);
							materialRequisitionIds[0] = String
									.valueOf(materialRequisitionDetail
											.getMaterialRequisition()
											.getMaterialRequisitionId());
						}
					}
					materialTransferDetails.add(materialTransferDetail);
				}
			} else {
				Map<Long, MaterialRequisitionVO> requisitionHash = (Map<Long, MaterialRequisitionVO>) (session
						.getAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
								+ sessionObj.get("jSessionId")));
				materialRequisitionIds = splitArrayValues(requisitionDetails,
						",");
				List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();
				for (String materialRequisitionId : materialRequisitionIds) {
					MaterialRequisitionVO materialRequisitionVO = (MaterialRequisitionVO) requisitionHash
							.get(Long.parseLong(materialRequisitionId));
					materialRequisitionDetailVOs.addAll(materialRequisitionVO
							.getMaterialRequisitionDetailVOs());
				}
				List<MaterialTransferDetail> tempMaterialTransferDetails = null;
				MaterialRequisitionDetailVO tempMaterialRequisitionDetailVO = null;
				for (MaterialRequisitionDetailVO materialRequisitionDetailVO : materialRequisitionDetailVOs) {
					if (null != materialRequisitionDetailVO.getTransferType()
							&& null != materialRequisitionDetailVO
									.getTransferType()
							&& null != materialRequisitionDetailVO
									.getShelfFromId()
							&& (int) materialRequisitionDetailVO
									.getShelfFromId() > 0) {
						materialTransferDetail = new MaterialTransferDetail();
						materialTransferDetail
								.setProduct(materialRequisitionDetailVO
										.getProduct());
						fromShelf = new Shelf();
						fromShelf.setShelfId(materialRequisitionDetailVO
								.getShelfFromId());
						materialTransferDetail.setShelfByFromShelfId(fromShelf);
						lookupDetail = new LookupDetail();
						lookupDetail
								.setLookupDetailId(materialRequisitionDetailVO
										.getTransferType());
						materialTransferDetail
								.setLookupDetailByTransferType(lookupDetail);
						if (null != materialRequisitionDetailVO
								.getTransferReason()
								&& materialRequisitionDetailVO
										.getTransferReason() > 0) {
							lookupDetail = new LookupDetail();
							lookupDetail
									.setLookupDetailId(materialRequisitionDetailVO
											.getTransferReason());
							materialTransferDetail
									.setLookupDetailByTransferReason(lookupDetail);
						}
						shelf = new Shelf();
						shelf.setShelfId(materialRequisitionDetailVO
								.getShelfToId());
						materialTransferDetail.setShelfByShelfId(shelf);
						materialRequisitionDetail = new MaterialRequisitionDetail();
						materialRequisitionDetail
								.setMaterialRequisitionDetailId(materialRequisitionDetailVO
										.getMaterialRequisitionDetailId());
						materialTransferDetail
								.setMaterialRequisitionDetail(materialRequisitionDetail);
						materialTransferDetail
								.setUnitRate(materialRequisitionDetailVO
										.getUnitRate());
						materialTransferDetail
								.setQuantity(materialRequisitionDetailVO
										.getTransferQty());
						materialTransferDetails.add(materialTransferDetail);
						tempMaterialTransferDetails = materialTransferBL
								.getMaterialTransferService()
								.getMaterialTransferDetailByRequisitionId(
										materialRequisitionDetailVO
												.getMaterialRequisitionDetailId());
						double transferedQuantity = materialTransferDetail
								.getQuantity();
						if (null != tempMaterialTransferDetails
								&& tempMaterialTransferDetails.size() > 0) {
							for (MaterialTransferDetail tempDetail : tempMaterialTransferDetails)
								transferedQuantity += tempDetail.getQuantity();
						}
						tempMaterialRequisitionDetailVO = new MaterialRequisitionDetailVO();
						BeanUtils.copyProperties(
								tempMaterialRequisitionDetailVO,
								materialRequisitionDetailVO);
						tempMaterialRequisitionDetailVO
								.setQuantity(transferedQuantity);
						requisitionMap.put(tempMaterialRequisitionDetailVO
								.getMaterialRequisitionDetailId(),
								tempMaterialRequisitionDetailVO);
					}
				}
			}
			if (null != requisitionMap && requisitionMap.size() > 0)
				materialRequisitions = processMaterialRequisition(
						materialRequisitionIds, requisitionMap);
			if (materialTransferId > 0) {
				materialTransfer.setMaterialTransferId(materialTransferId);
				materialTransfer.setReferenceNumber(referenceNumber);
			} else
				materialTransfer.setReferenceNumber(SystemBL.getReferenceStamp(
						MaterialTransfer.class.getName(),
						materialTransferBL.getImplementation()));
			materialTransferBL.saveMaterialTransfer(materialTransfer,
					materialTransferDetails, materialRequisitions);
			returnMessage = "SUCCESS";
			session.removeAttribute("MATERIAL_REQUISITION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			log.info("Module: Accounts : Method: saveMaterialTransfer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveMaterialTransfer Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<MaterialRequisition> processMaterialRequisition(
			String[] materialRequisitionIds,
			Map<Long, MaterialRequisitionDetailVO> requisitionMap)
			throws Exception {
		List<MaterialRequisition> materialRequisitions;
		MaterialRequisitionDetail materialRequisitionDetail;
		List<MaterialTransferDetail> tempMaterialTransferDetails;
		int idLength = materialRequisitionIds.length;
		Long[] materialRequisitionIdGroup = new Long[idLength];
		for (int i = 0; i < idLength; i++)
			materialRequisitionIdGroup[i] = Long
					.parseLong(materialRequisitionIds[i]);
		materialRequisitions = materialTransferBL.getMaterialRequisitionBL()
				.getMaterialRequisitionService()
				.getMaterialRequisitionsByGroupIds(materialRequisitionIdGroup);
		boolean closed = false;
		for (MaterialRequisition materialRequisition : materialRequisitions) {
			materialRequisition
					.setRequisitionStatus(RequisitionStatus.Processing
							.getCode());
			closed = false;
			for (MaterialRequisitionDetail requisitionDetail : materialRequisition
					.getMaterialRequisitionDetails()) {
				if (requisitionMap.containsKey(requisitionDetail
						.getMaterialRequisitionDetailId())) {
					materialRequisitionDetail = new MaterialRequisitionDetail();
					materialRequisitionDetail = requisitionMap
							.get(requisitionDetail
									.getMaterialRequisitionDetailId());
					if ((double) materialRequisitionDetail.getQuantity() == (double) requisitionDetail
							.getQuantity()) {
						closed = true;
					} else {
						closed = false;
						break;
					}
				} else {
					tempMaterialTransferDetails = materialTransferBL
							.getMaterialTransferService()
							.getMaterialTransferDetailByRequisitionId(
									requisitionDetail
											.getMaterialRequisitionDetailId());
					if (null != tempMaterialTransferDetails
							&& tempMaterialTransferDetails.size() > 0) {
						double transferedQuantity = 0;
						for (MaterialTransferDetail transferDetail : tempMaterialTransferDetails) {
							transferedQuantity += transferDetail.getQuantity();
						}
						if (transferedQuantity == (double) requisitionDetail
								.getQuantity()) {
							closed = true;
						}
					} else {
						closed = false;
						break;
					}
				}
			}
			if (closed)
				materialRequisition
						.setRequisitionStatus(RequisitionStatus.Completed
								.getCode());
		}
		return materialRequisitions;
	}

	public String deleteMaterialTransfer() {
		try {
			log.info("Inside Module: Accounts : Method: deleteMaterialTransfer");
			MaterialTransfer materialTransfer = materialTransferBL
					.getMaterialTransferService().getMaterialTransferById(
							materialTransferId);
			List<MaterialTransferDetail> materialTransferDetails = new ArrayList<MaterialTransferDetail>(
					materialTransfer.getMaterialTransferDetails());
			materialTransferBL.deleteMaterialTransfer(materialTransfer,
					materialTransferDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteMaterialTransfer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteMaterialTransfer Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Material Transfer Add Row
	public String showMaterialTransferAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialTransferAddRow");
			loadLookupDetails();
			log.info("Module: Accounts : Method: showMaterialTransferAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialTransferAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAlertMaterialRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: showAlertMaterialRequisition");
			MaterialRequisition materialRequisition = materialTransferBL
					.getMaterialRequisitionBL().getMaterialRequisitionService()
					.getMaterialRequisitionById(recordId);
			MaterialRequisitionVO materialRequisitionVO = new MaterialRequisitionVO();
			MaterialRequisitionDetailVO materialRequisitionDetailVO = null;
			List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();
			for (MaterialRequisitionDetail requisitionDetail : materialRequisition
					.getMaterialRequisitionDetails()) {
				materialRequisitionDetailVO = new MaterialRequisitionDetailVO();
				BeanUtils.copyProperties(materialRequisitionDetailVO,
						requisitionDetail);
				materialRequisitionDetailVOs.add(materialRequisitionDetailVO);
			}
			Collections.sort(materialRequisitionDetailVOs,
					new Comparator<MaterialRequisitionDetailVO>() {
						public int compare(MaterialRequisitionDetailVO o1,
								MaterialRequisitionDetailVO o2) {
							return o1
									.getMaterialRequisitionDetailId()
									.compareTo(
											o2.getMaterialRequisitionDetailId());
						}
					});
			BeanUtils
					.copyProperties(materialRequisitionVO, materialRequisition);
			materialRequisitionVO.setStoreName(materialRequisition.getStore()
					.getStoreName());
			materialRequisitionVO
					.setMaterialRequisitionDetailVOs(materialRequisitionDetailVOs);
			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_REQUISITION", materialRequisitionVO);
			loadLookupDetails();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			referenceNumber = SystemBL.getReferenceStamp(
					MaterialTransfer.class.getName(),
					materialTransferBL.getImplementation());
			transferPerson = user.getPersonId();
			personName = (String) sessionObj.get("PERSON_NAME");
			log.info("Module: Accounts : Method: showAlertMaterialRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAlertMaterialRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private void loadLookupDetails() {
		ServletActionContext.getRequest().setAttribute(
				"TRANSFER_TYPE",
				materialTransferBL.getLookupMasterBL().getActiveLookupDetails(
						"MATERIAL_TRANSFER_TYPE", true));
		ServletActionContext.getRequest().setAttribute(
				"TRANSFER_REASON",
				materialTransferBL.getLookupMasterBL().getActiveLookupDetails(
						"MATERIAL_TRANSFER_REASON", true));
	}

	private static String[] splitArrayValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public MaterialTransferBL getMaterialTransferBL() {
		return materialTransferBL;
	}

	public void setMaterialTransferBL(MaterialTransferBL materialTransferBL) {
		this.materialTransferBL = materialTransferBL;
	}

	public long getMaterialTransferId() {
		return materialTransferId;
	}

	public void setMaterialTransferId(long materialTransferId) {
		this.materialTransferId = materialTransferId;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getTransferDetails() {
		return transferDetails;
	}

	public void setTransferDetails(String transferDetails) {
		this.transferDetails = transferDetails;
	}

	public long getTransferPerson() {
		return transferPerson;
	}

	public void setTransferPerson(long transferPerson) {
		this.transferPerson = transferPerson;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public String getRequisitionDetails() {
		return requisitionDetails;
	}

	public void setRequisitionDetails(String requisitionDetails) {
		this.requisitionDetails = requisitionDetails;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}
}
