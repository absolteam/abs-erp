package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetDisposal;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.service.bl.AssetDisposalBL;
import com.aiotech.aios.common.to.Constants.Accounts.DisposalMethod;
import com.aiotech.aios.common.to.Constants.Accounts.DisposalType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionSupport;

public class AssetDisposalAction extends ActionSupport {

	private static final long serialVersionUID = 2583793102256887973L;

	private static final Logger log = Logger
			.getLogger(AssetDisposalAction.class);

	// Dependency
	private AssetDisposalBL assetDisposalBL;

	// Variables
	private long assetDisposalId;
	private long assetId;
	private long removalExpenseId;
	private long receiptsId;
	private long exchangeAssetId;
	private byte disposalMethod;
	private byte disposalType;
	private String disposalDate;
	private String referenceNumber;
	private String description;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Account: Method: returnSuccess()");
		return SUCCESS;
	}

	// Show all Asset Disposals
	public String showAllAssetDisposal() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAssetDisposal");
			aaData = assetDisposalBL.showAllAssetDisposal();
			log.info("Module: Accounts : Method: showAllAssetDisposal: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllAssetDisposal Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Disposal Entry
	public String showDisposalEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showDisposalEntry");
			if (assetDisposalId > 0) {

			}
			Map<Byte, String> disposalMethods = new HashMap<Byte, String>();
			for (DisposalMethod e : EnumSet.allOf(DisposalMethod.class))
				disposalMethods.put(e.getCode(), e.name().replaceAll("_", " "));
			Map<Byte, String> disposalTypes = new HashMap<Byte, String>();
			for (DisposalType e : EnumSet.allOf(DisposalType.class))
				disposalTypes.put(e.getCode(), e.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("DISPOSAL_METHOD",
					disposalMethods);
			ServletActionContext.getRequest().setAttribute("DISPOSAL_TYPE",
					disposalTypes);
			referenceNumber = SystemBL.getReferenceStamp(
					AssetDisposal.class.getName(),
					assetDisposalBL.getImplementation());
			log.info("Module: Accounts : Method: showDisposalEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showDisposalEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Disposal
	public String saveAssetDisposal() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetDisposal");
			assetDisposalBL.saveAssetDisposal(setAssetDisposal());
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetDisposal: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveAssetDisposal: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private AssetDisposal setAssetDisposal() throws Exception {
		AssetDisposal assetDisposal = new AssetDisposal();
		AssetCreation assetCreation = new AssetCreation();
		assetCreation.setAssetCreationId(assetId);
		assetDisposal.setAssetDisposalId(assetDisposalId > 0 ? assetDisposalId
				: null);
		assetDisposal.setReferenceNumber(assetDisposalId == 0 ? SystemBL
				.getReferenceStamp(AssetClaimInsurance.class.getName(),
						assetDisposalBL.getImplementation()) : referenceNumber);
		assetDisposal.setAssetCreationByAssetId(assetCreation);
		assetDisposal.setDisposalDate(DateFormat
				.convertStringToDate(disposalDate));
		assetDisposal.setDisposalMethod(disposalMethod);
		assetDisposal.setDisposalType(disposalType);
		assetDisposal.setDescription(description);
		if (receiptsId > 0) {
			BankReceipts bankReceipts = new BankReceipts();
			bankReceipts.setBankReceiptsId(receiptsId);
			assetDisposal.setBankReceipts(bankReceipts);
		}
		if (removalExpenseId > 0) {
			DirectPayment directPayment = new DirectPayment();
			directPayment.setDirectPaymentId(removalExpenseId);
			assetDisposal.setDirectPayment(directPayment);
		}
		if (exchangeAssetId > 0) {
			AssetCreation exchangeAsset = new AssetCreation();
			exchangeAsset.setAssetCreationId(exchangeAssetId);
			assetDisposal.setAssetCreationByExchangeAssetId(exchangeAsset);
		}
		return assetDisposal;
	}

	// Getters & Setters
	public AssetDisposalBL getAssetDisposalBL() {
		return assetDisposalBL;
	}

	public void setAssetDisposalBL(AssetDisposalBL assetDisposalBL) {
		this.assetDisposalBL = assetDisposalBL;
	}

	public long getAssetDisposalId() {
		return assetDisposalId;
	}

	public void setAssetDisposalId(long assetDisposalId) {
		this.assetDisposalId = assetDisposalId;
	}

	public long getAssetId() {
		return assetId;
	}

	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public byte getDisposalMethod() {
		return disposalMethod;
	}

	public void setDisposalMethod(byte disposalMethod) {
		this.disposalMethod = disposalMethod;
	}

	public byte getDisposalType() {
		return disposalType;
	}

	public void setDisposalType(byte disposalType) {
		this.disposalType = disposalType;
	}

	public String getDisposalDate() {
		return disposalDate;
	}

	public void setDisposalDate(String disposalDate) {
		this.disposalDate = disposalDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
