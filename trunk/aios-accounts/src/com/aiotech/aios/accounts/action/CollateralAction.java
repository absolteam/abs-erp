package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.CollateralDetail;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.service.bl.CollateralBL;
import com.aiotech.aios.common.to.Constants.Accounts.AssetDetail; 
import com.aiotech.aios.system.domain.entity.Implementation; 
import com.opensymphony.xwork2.ActionSupport;

public class CollateralAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	//Common Variables
	private long collateralId;
	private long loanId;
	private String showPage;
	private long accountId;
	private Integer id;
	private String returnMessage;
	private String collateralLineDetail;
	private List<CollateralDetail>collateralDetails=null;
	List<Loan> loanList=null;
	//Object Declarations
	private CollateralBL collateralBL;
	
	//Logger Property
	private static final Logger LOGGER=LogManager.getLogger(CollateralAction.class); 
	
	public String returnSuccess(){ 
		return SUCCESS;
	}
	
	public String showCollateralJsonList(){
		LOGGER.info("Inside showCollateralJsonList()");
		try {
			JSONObject jsonList=getCollateralBL().getCollateralList(getImplementationId()); 
			ServletActionContext.getRequest().setAttribute("jsonlist", jsonList);  
			LOGGER.info("Module: Accounts : Method: showCollateralJsonList: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCollateralJsonList: throws Exception "+ex);  
			return ERROR;
		}
	} 
	
	public String showCollateralEntry(){
		LOGGER.info("Inside showCollateralEntry()");
		try {
			Map<Long,AssetDetail> assetDetail=new HashMap<Long,AssetDetail>(); 
			for (AssetDetail e : EnumSet.allOf(AssetDetail.class)){
				assetDetail.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("ASSET_DETAIL", assetDetail);  
			ServletActionContext.getRequest().setAttribute("showPage", showPage);  
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCollateralEntry: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showAddRow(){
		LOGGER.info("Inside showAddRow()");
		try {  
			Map<Long,AssetDetail> assetDetail=new HashMap<Long,AssetDetail>(); 
			for (AssetDetail e : EnumSet.allOf(AssetDetail.class)){
				assetDetail.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("ASSET_DETAIL", assetDetail);  
			ServletActionContext.getRequest().setAttribute("rowId", id);  
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAddRow: throws Exception "+ex);  
			return ERROR;
		}
	}
	public String showLoanDetail(){
		LOGGER.info("Inside showLoanDetail()");
		try {  
			if(accountId!=0)
				loanList=getCollateralBL().getLoanService().getLoanDetailsByAccountId(accountId);
			else
				loanList=getCollateralBL().getLoanService().getLoanDetails(getImplementationId()); 
			ServletActionContext.getRequest().setAttribute("PAGE", showPage);  
			ServletActionContext.getRequest().setAttribute("LOAN_LIST", loanList);  
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showLoanDetail: throws Exception "+ex);  
			return ERROR;
		}
	} 
	
	public String saveCollateral(){
		LOGGER.info("Inside saveCollateral()");
		try {
			Loan loan=new Loan();
			loan.setLoanId(loanId);
			CollateralDetail detail=null; 
			collateralDetails=new ArrayList<CollateralDetail>();
			String [] collateralLines=splitValues(collateralLineDetail,"↕↕");
			for(String string: collateralLines){ 
				detail=new CollateralDetail(); 
				String[] detailList=splitValues(string,"↕");
				detail.setLoan(loan);
				detail.setAssetId(Long.parseLong(detailList[0]));
				detail.setAmount(Double.parseDouble(detailList[1]));
				if(Long.parseLong(detailList[2])>0)
					detail.setCollateralId(Long.parseLong(detailList[2]));
				collateralDetails.add(detail);
			}
			getCollateralBL().saveCollateral(collateralDetails);
			returnMessage="SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: saveCollateral: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) { 
			ex.printStackTrace();
			returnMessage="FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: saveCollateral: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showUpdateCollateral(){
		LOGGER.info("Inside showUpdateCollateral()");
		try {
			Map<Long,AssetDetail> assetDetail=new HashMap<Long,AssetDetail>(); 
			for (AssetDetail e : EnumSet.allOf(AssetDetail.class)){
				assetDetail.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("ASSET_DETAIL", assetDetail);  
			CollateralDetail collateral=getCollateralBL().getCollateral(collateralId);
			ServletActionContext.getRequest().setAttribute("COLLATERAL_INFO", collateral); 
			collateralDetails=getCollateralBL().getCollateralList(collateral.getLoan().getLoanId());
			ServletActionContext.getRequest().setAttribute("COLLATERAL_INFO_LIST", collateralDetails); 
			ServletActionContext.getRequest().setAttribute("showPage", showPage); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace(); 
			LOGGER.info("Module: Accounts : Method: showUpdateCollateral: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String deleteCollateral(){
		LOGGER.info("Inside showUpdateCollateral()");
		try {	
			CollateralDetail collateral=getCollateralBL().getCollateral(collateralId);
			getCollateralBL().getCollateralService().deleteCollateral(collateral);
			returnMessage="SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: saveCollateral: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace(); 
			LOGGER.info("Module: Accounts : Method: showUpdateCollateral: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public Implementation getImplementationId(){
		LOGGER.info("Inside getImplementationId()");
		HttpSession session=ServletActionContext.getRequest().getSession();
		Implementation implementation=new Implementation();
		if(null!=session.getAttribute("THIS"))
			implementation=(Implementation)session.getAttribute("THIS");
		return implementation;
	}
	
	public static String[] splitValues(String spiltValue, String delimeter){
		return spiltValue.split(""+delimeter+"(?=([^\"]*\"[^\"]*\")*[^\"]*$)"); 
	} 

	public long getCollateralId() {
		return collateralId;
	}

	public void setCollateralId(long collateralId) {
		this.collateralId = collateralId;
	}

	public CollateralBL getCollateralBL() {
		return collateralBL;
	}

	public void setCollateralBL(CollateralBL collateralBL) {
		this.collateralBL = collateralBL;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getLoanId() {
		return loanId;
	}

	public void setLoanId(long loanId) {
		this.loanId = loanId;
	}

	public String getCollateralLineDetail() {
		return collateralLineDetail;
	}

	public void setCollateralLineDetail(String collateralLineDetail) {
		this.collateralLineDetail = collateralLineDetail;
	}

	public List<CollateralDetail> getCollateralDetails() {
		return collateralDetails;
	}

	public void setCollateralDetails(List<CollateralDetail> collateralDetails) {
		this.collateralDetails = collateralDetails;
	}

	public List<Loan> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<Loan> loanList) {
		this.loanList = loanList;
	}
}

