package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.service.bl.MerchandiseExchangeBL;
import com.opensymphony.xwork2.ActionSupport;

public class MerchandiseExchangeAction extends ActionSupport {

	private static final long serialVersionUID = 8978259355698675454L;

	private static final Logger log = Logger
			.getLogger(MerchandiseExchangeAction.class);

	// Dependency
	private MerchandiseExchangeBL merchandiseExchangeBL;

	// Variables
	private long merchandiseReturnId;
 	private String returnMessage;
	private String exchangeDetails;
	private List<Object> aaData;
	
	// Return success
	public String returnSuccess() {
		log.info("Module: Accounts : Inside Method: returnSuccess() Action Success");
		return SUCCESS;
	}
	
	// Show All Merchandise Exchange
	public String showAllMerchandiseExchange() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMerchandiseExchange");
			aaData = merchandiseExchangeBL.showAllMerchandiseExchange();
			log.info("Module: Accounts : Method: showAllMerchandiseExchange: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllMerchandiseExchange Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Merchandise Return Search Screen
	public String showMerchandiseReturn() {
		try {
			log.info("Module: Accounts : Inside Method: showMerchandiseReturn()");
			 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showMerchandiseReturn: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Merchandise
	public String saveMerchandiseExchange() {
		try {
			log.info("Module: Accounts : Inside Method: saveMerchandiseExchange()");
			MerchandiseExchange merchandiseExchange = new MerchandiseExchange();
  			merchandiseExchange.setDate(Calendar.getInstance().getTime());
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails = new ArrayList<MerchandiseExchangeDetail>();
			if (null != exchangeDetails && !("").equals(exchangeDetails)
					&& !("[]").equals(exchangeDetails)) {
				JSONParser parser = new JSONParser();
				Object exchangeDetail = parser.parse(exchangeDetails);
				if (null != exchangeDetail && !("").equals(exchangeDetail)) {
					JSONArray jsonArray = (JSONArray) exchangeDetail;
					for (Object jsonObject : jsonArray) {
						JSONObject object = (JSONObject) jsonObject;
						merchandiseExchangeDetails
								.add(addExchangeDetail(object));
					}
				} else {
					returnMessage = "Please enter return details";
					log.info("Module: Accounts : Method: saveMerchandiseExchange: Action error");
					return ERROR;
				}
			}
			merchandiseExchangeBL.saveMerchandiseExchange(merchandiseExchange,
					merchandiseExchangeDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMerchandiseExchange: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveMerchandiseExchange: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Add Exchange Detail
	private MerchandiseExchangeDetail addExchangeDetail(JSONObject object) {
		MerchandiseExchangeDetail exchangeDetail = new MerchandiseExchangeDetail();
		PointOfSaleDetail pointOfSaleDetail = new PointOfSaleDetail();
		pointOfSaleDetail.setPointOfSaleDetailId(Long.parseLong(object.get(
				"pointOfSaleDetailId").toString()));
		exchangeDetail.setPointOfSaleDetail(pointOfSaleDetail);
		exchangeDetail.setReturnQuantity(Double.parseDouble(object.get(
				"returnQuantity").toString()));
		return exchangeDetail;
	}

	// Getters & Setters
	public long getMerchandiseReturnId() {
		return merchandiseReturnId;
	}

	public void setMerchandiseReturnId(long merchandiseReturnId) {
		this.merchandiseReturnId = merchandiseReturnId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public MerchandiseExchangeBL getMerchandiseExchangeBL() {
		return merchandiseExchangeBL;
	}

	public void setMerchandiseExchangeBL(
			MerchandiseExchangeBL merchandiseExchangeBL) {
		this.merchandiseExchangeBL = merchandiseExchangeBL;
	} 
	
	public String getExchangeDetails() {
		return exchangeDetails;
	}

	public void setExchangeDetails(String exchangeDetails) {
		this.exchangeDetails = exchangeDetails;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

}
