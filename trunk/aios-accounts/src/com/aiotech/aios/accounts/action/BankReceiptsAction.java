package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DebitDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsVO;
import com.aiotech.aios.accounts.service.bl.BankReceiptsBL;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class BankReceiptsAction extends ActionSupport {

	private static final long serialVersionUID = -8577185832354857556L;
	private long bankReceiptsId;
	private String receiptNo;
	private String receiptDate;
	private long receiptTypeId;
	private String description;
	private String receiptsNo;
	private Integer id;
	private String receiptsDate;
	private long currencyId;
	private String payeeType;
	private String referenceNo;
	private long supplierId;
	private String vendorName;
	private long combinationId;
	private String bankReceiptLines;
	private long receiptsTypeId;
	private String returnMessage;
	private String useCase;
	private String useCaseName;
	private String dateFrom;
	private String dateTo;
	private String institutionDate;
	private String paymentMode;
	private boolean templatePrint;
	private long alertId;
	private long recordId;
	private long useCaseId;
	private long bankReceiptsDetailId;
	private byte receiptType;
	private String printableContent;
	private BankReceiptsVO bankReceiptsObj;
	// Dependency
	private BankReceiptsBL bankReceiptsBL;

	private static final Logger LOGGER = LogManager
			.getLogger(BankReceiptsAction.class);

	// return success
	public String returnSuccess() {
		try {
			LOG.info("Inside Module: Accounts : Method: returnSuccess()");
			LOG.info("Module: Accounts : Method: returnSuccess(): Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: returnSuccess() Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show all bank receipts json list
	public String showAllBankReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllBankReceipts");
			List<BankReceipts> bankReceipts = bankReceiptsBL
					.getBankReceiptsService().getAllBankReceipts(
							bankReceiptsBL.getImplementation());
			JSONObject jsonList = bankReceiptsBL
					.convertBankReceiptsToJson(bankReceipts);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showAllBankReceipts: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllBankReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showBankReceiptPrint() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBankReceiptPrint");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			List<BankReceipts> bankReceipts = bankReceiptsBL
					.getBankReceiptsService().getFilteredBankReceipts(
							bankReceiptsBL.getImplementation(), bankReceiptsId,
							null, null, null, null, null, null, null);
			BankReceiptsVO bankReceiptsVO = getBankReceiptVO(bankReceipts
					.get(0));
			Boolean receiptTemplatePrintEnabled = session
					.getAttribute("print_bankreceipts_template") != null ? ((String) session
					.getAttribute("print_bankreceipts_template"))
					.equalsIgnoreCase("true") : false;
			if (receiptTemplatePrintEnabled) {
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				Configuration config = new Configuration();
				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/templates/implementation_"
						+ bankReceiptsBL.getImplementation()
								.getImplementationId() + "/cashbank"));
				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");
				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config.getTemplate("receipts.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();
				rootMap.put("customerName",
						bankReceiptsVO.getReceiptSourceName());
				rootMap.put("receiptDate", bankReceiptsVO.getStrReceiptsDate());
				rootMap.put("receiptAmount", bankReceiptsVO.getReceiptAmount());
				rootMap.put("receiptNumber", bankReceiptsVO.getReceiptsNo());
				rootMap.put("referenceNo", bankReceiptsVO.getReferenceNo());
				rootMap.put("currency", bankReceiptsVO.getCurrencyName());
				rootMap.put("companyName", bankReceiptsBL.getImplementation()
						.getCompanyName());
				rootMap.put("receipt", "RECEIPT");
				rootMap.put(
						"description",
						null != bankReceiptsVO.getDescription()
								&& !("").equals(bankReceiptsVO.getDescription()) ? bankReceiptsVO
								.getDescription() : null);
				rootMap.put(
						"accountant",
						null != bankReceiptsVO.getPersonByCreatedBy() ? bankReceiptsVO
								.getPersonByCreatedBy()
								.getFirstName()
								.concat(" "
										+ bankReceiptsVO.getPersonByCreatedBy()
												.getLastName())
								: "");
				rootMap.put("receiptList",
						bankReceiptsVO.getBankReceiptsDetailVOs());
				rootMap.put("urlPath", urlPath);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				return "template";
			} else {
				ServletActionContext.getRequest().setAttribute("BANK_RECEIPT",
						bankReceiptsVO);
			}
			LOGGER.info("Module: Accounts : Method: showBankReceiptPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public BankReceiptsVO getBankReceiptVO(BankReceipts receipt)
			throws Exception {
		double totalReceiptAmount = 0;
		totalReceiptAmount = 0;
		BankReceiptsVO bankReceiptsVO = new BankReceiptsVO(receipt);
		bankReceiptsVO.setStrReceiptsDate(DateFormat
				.convertDateToString(receipt.getReceiptsDate().toString()));
		bankReceiptsVO.setCurrencyName(receipt.getCurrency().getCurrencyPool()
				.getCode());
		bankReceiptsVO.setReceiptType(receipt.getLookupDetail()
				.getDisplayName());
		bankReceiptsVO.setDescription(receipt.getDescription());
		if (receipt.getSupplier() != null) {
			if (receipt.getSupplier().getPerson() != null)
				bankReceiptsVO
						.setReceiptSourceName(receipt
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat(" ")
								.concat(receipt.getSupplier().getPerson()
										.getLastName()));
			else
				bankReceiptsVO.setReceiptSourceName(receipt.getSupplier()
						.getCompany() != null ? receipt.getSupplier()
						.getCompany().getCompanyName() : receipt.getSupplier()
						.getCmpDeptLocation().getCompany().getCompanyName());

			bankReceiptsVO.setReceiptSourceType("Supplier");
		} else if (receipt.getCustomer() != null) {
			if (receipt.getCustomer().getPersonByPersonId() != null)
				bankReceiptsVO.setReceiptSourceName(receipt
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(receipt.getCustomer().getPersonByPersonId()
								.getLastName()));
			else
				bankReceiptsVO.setReceiptSourceName(receipt.getCustomer()
						.getCompany() != null ? receipt.getCustomer()
						.getCompany().getCompanyName() : receipt.getCustomer()
						.getCmpDeptLocation().getCompany().getCompanyName());

			bankReceiptsVO.setReceiptSourceType("Customer");
		} else if (receipt.getPersonByPersonId() != null) {
			bankReceiptsVO.setReceiptSourceName(receipt.getPersonByPersonId()
					.getFirstName().concat(" ")
					.concat(receipt.getPersonByPersonId().getLastName()));
			bankReceiptsVO.setReceiptSourceType("Employee");
		} else {
			bankReceiptsVO.setReceiptSourceName(receipt.getOthers());
			bankReceiptsVO.setReceiptSourceType("Others");
		}

		if (null != receipt.getBankReceiptsDetails()
				&& receipt.getBankReceiptsDetails().size() > 0) {
			List<BankReceiptsDetailVO> receiptsDetailVOs = new ArrayList<BankReceiptsDetailVO>();
			for (BankReceiptsDetail detail : receipt.getBankReceiptsDetails()) {
				totalReceiptAmount += detail.getAmount();
				BankReceiptsDetailVO receiptsDetailVO = new BankReceiptsDetailVO(
						detail);
				receiptsDetailVO.setPaymentModeName(ModeOfPayment
						.get(detail.getPaymentMode()).name()
						.replaceAll("_", " "));
				receiptsDetailVO.setPaymentStatusName(PaymentStatus
						.get(detail.getPaymentStatus()).name()
						.replaceAll("_", " "));
				if (detail.getChequeDate() != null) {
					receiptsDetailVO.setStrChequeDate(DateFormat
							.convertDateToString(detail.getChequeDate()
									.toString()));
				}

				if (detail.getLookupDetail() != null) {
					receiptsDetailVO.setReferenceNo(detail.getLookupDetail()
							.getDisplayName());
				} else {
					receiptsDetailVO.setReferenceNo("");
				}
				receiptsDetailVOs.add(receiptsDetailVO);
			}
			bankReceiptsVO.setReceiptAmount(AIOSCommons
					.formatAmount(totalReceiptAmount));
			bankReceiptsVO.setBankReceiptsDetailVOs(receiptsDetailVOs);
		}
		bankReceiptsVO.setUseCase(receipt.getUseCase());
		bankReceiptsVO.setRecordId(receipt.getRecordId());
		return bankReceiptsVO;
	}

	public String showBankReceiptClearPDC() {
		try {
			BankReceiptsDetail bankReceiptsDetail = bankReceiptsBL
					.getBankReceiptsService().getBankReceiptsDetailById(
							recordId);
			BankReceiptsVO bankReceiptsVO = getBankReceiptVO(bankReceiptsBL
					.getBankReceiptsService().findByBankReceiptsId(
							bankReceiptsDetail.getBankReceipts()
									.getBankReceiptsId()));
			BankReceiptsDetailVO bankReceiptsDetailVO = getBankReceiptDetailVO(bankReceiptsDetail);
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceiptsVO);
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_DETAIL", bankReceiptsDetailVO);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"RECEIPT");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private BankReceiptsDetailVO getBankReceiptDetailVO(
			BankReceiptsDetail bankReceiptsDetail) throws Exception {
		BankReceiptsDetailVO receiptsDetailVO = new BankReceiptsDetailVO();
		BeanUtils.copyProperties(receiptsDetailVO, bankReceiptsDetail);
		receiptsDetailVO.setPaymentModeName(ModeOfPayment
				.get(bankReceiptsDetail.getPaymentMode()).name()
				.replaceAll("_", " "));
		receiptsDetailVO.setPaymentStatusName(PaymentStatus
				.get(bankReceiptsDetail.getPaymentStatus()).name()
				.replaceAll("_", " "));
		if (bankReceiptsDetail.getChequeDate() != null)
			receiptsDetailVO.setStrChequeDate(DateFormat
					.convertDateToString(bankReceiptsDetail.getChequeDate()
							.toString()));
		if (bankReceiptsDetail.getLookupDetail() != null)
			receiptsDetailVO.setReferenceNo(bankReceiptsDetail
					.getLookupDetail().getDisplayName());
		else
			receiptsDetailVO.setReferenceNo("");
		return receiptsDetailVO;
	}

	// show bank receipts entry
	public String showBankReceiptsEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBankReceiptsEntry");

			if (recordId > 0) {
				bankReceiptsId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (bankReceiptsId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = bankReceiptsBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (bankReceiptsId > 0) {
				BankReceipts bankReceipts = bankReceiptsBL
						.getBankReceiptsService().findByBankReceiptsId(
								bankReceiptsId);
				ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
						bankReceipts);
			}
			bankReceiptScreenDetail();
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"RECEIPT");
			LOGGER.info("Module: Accounts : Method: showBankReceiptsEntry Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show bank receipts sales invoice entry
	public String showSalesInvoiceBankReceipt() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showSalesInvoiceBankReceipt");
			bankReceiptScreenDetail();
			SalesInvoice salesInvoice = bankReceiptsBL.getSalesInvoiceBL()
					.getSalesInvoiceService().getInvoiceById(recordId);
			if (null != salesInvoice && null != salesInvoice.getSalesInvoice()) {
				salesInvoice = bankReceiptsBL.getSalesInvoiceBL()
						.getSalesInvoiceService()
						.getSalesInvoiceDetailById(recordId);
			} else
				salesInvoice = bankReceiptsBL.getSalesInvoiceBL()
						.getSalesInvoiceService().getSalesInvoiceById(recordId);
			BankReceipts bankReceipts = convertSalesInvoice(salesInvoice);
			bankReceipts.setUseCase(SalesInvoice.class.getSimpleName());
			bankReceipts.setRecordId(salesInvoice.getSalesInvoiceId());
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"SALESINVOICE");
			LOGGER.info("Module: Accounts : Method: showSalesInvoiceBankReceipt Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showSalesInvoiceBankReceipt Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show bank receipts sales invoice entry
	public String showDebitNoteBankReceipt() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showDebitNoteBankReceipt");
			bankReceiptScreenDetail();
			Debit debitNote = bankReceiptsBL.getDebitBL().getDebitService()
					.getDebitNotesById(recordId);
			BankReceipts bankReceipts = convertDebitNote(debitNote);
			bankReceipts.setUseCase(Debit.class.getSimpleName());
			bankReceipts.setRecordId(debitNote.getDebitId());
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"SALESINVOICE");
			LOGGER.info("Module: Accounts : Method: showDebitNoteBankReceipt Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showDebitNoteBankReceipt Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show bank receipts sales invoice entry
	public String showClaimInsuranceBankReceipt() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showClaimInsuranceBankReceipt");
			bankReceiptScreenDetail();
			AssetClaimInsurance assetClaimInsurance = bankReceiptsBL
					.getAssetClaimInsuranceBL().getAssetClaimInsuranceService()
					.getAssetClaimInsuranceByClaimId(recordId);
			BankReceipts bankReceipts = convertClaimInsurance(assetClaimInsurance);
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"CLAIMINSURANCE");
			LOGGER.info("Module: Accounts : Method: showClaimInsuranceBankReceipt Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showClaimInsuranceBankReceipt Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show bank receipts sales invoice entry
	public String showClaimWarrantyBankReceipt() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showClaimWarrantyBankReceipt");
			bankReceiptScreenDetail();
			AssetWarrantyClaim assetWarrantyClaim = bankReceiptsBL
					.getAssetWarrantyClaimBL().getAssetWarrantyClaimService()
					.getAssetWarrantyClaimById(recordId);
			BankReceipts bankReceipts = convertClaimWarranty(assetWarrantyClaim);
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"CLAIMWARRANTY");
			LOGGER.info("Module: Accounts : Method: showClaimWarrantyBankReceipt Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showClaimWarrantyBankReceipt Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private BankReceipts convertSalesInvoice(SalesInvoice salesInvoice)
			throws Exception {
		BankReceipts bankReceipt = new BankReceipts();
		bankReceipt.setCurrency(salesInvoice.getCurrency());
		bankReceipt.setCustomer(salesInvoice.getCustomer());
		bankReceipt.setCombination(salesInvoice.getCustomer().getCombination());
		bankReceipt.setReferenceNo(salesInvoice.getReferenceNumber());
		bankReceipt.setDescription(salesInvoice.getDescription());
		BankReceiptsDetail bankReceiptDetail = null;
		List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
		bankReceiptDetail = new BankReceiptsDetail();
		bankReceiptDetail.setAmount(AIOSCommons.roundThreeDecimals(salesInvoice
				.getInvoiceAmount()));
		bankReceiptDetail.setRecordId(null);
		bankReceiptDetail.setUseCase(null);
		bankReceiptsDetails.add(bankReceiptDetail);
		bankReceipt.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
				bankReceiptsDetails));
		return bankReceipt;
	}

	private BankReceipts convertDebitNote(Debit debit) throws Exception {
		BankReceipts bankReceipt = new BankReceipts();
		bankReceipt
				.setCurrency(bankReceiptsBL.getTransactionBL().getCurrency());
		bankReceipt.setSupplier(debit.getGoodsReturn().getSupplier());
		bankReceipt.setCombination(debit.getGoodsReturn().getSupplier()
				.getCombination());
		bankReceipt.setReferenceNo(debit.getDebitNumber());
		BankReceiptsDetail bankReceiptDetail = null;
		List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
		bankReceiptDetail = new BankReceiptsDetail();
		double totalAmount = 0;
		for (DebitDetail debitDetail : debit.getDebitDetails()) {
			totalAmount += debitDetail.getReturnQty()
					* debitDetail.getGoodsReturnDetail().getReceiveDetail()
							.getUnitRate();
		}
		bankReceiptDetail
				.setAmount(AIOSCommons.roundThreeDecimals(totalAmount));
		bankReceiptDetail.setRecordId(null);
		bankReceiptDetail.setUseCase(null);
		bankReceiptsDetails.add(bankReceiptDetail);
		bankReceipt.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
				bankReceiptsDetails));
		return bankReceipt;
	}

	private BankReceipts convertClaimInsurance(
			AssetClaimInsurance assetClaimInsurance) throws Exception {
		BankReceipts bankReceipt = new BankReceipts();
		bankReceipt.setReferenceNo(assetClaimInsurance.getReferenceNumber());
		BankReceiptsDetail bankReceiptDetail = null;
		List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();

		bankReceiptDetail = new BankReceiptsDetail();
		bankReceiptDetail.setAmount(AIOSCommons
				.roundThreeDecimals(assetClaimInsurance.getClaimAmount()));
		bankReceiptDetail.setRecordId(assetClaimInsurance
				.getAssetClaimInsuranceId());
		bankReceiptDetail.setUseCase(AssetClaimInsurance.class.getSimpleName());
		bankReceiptsDetails.add(bankReceiptDetail);

		bankReceipt.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
				bankReceiptsDetails));
		return bankReceipt;
	}

	private BankReceipts convertClaimWarranty(
			AssetWarrantyClaim assetWarrantyClaim) throws Exception {
		BankReceipts bankReceipt = new BankReceipts();
		bankReceipt.setReferenceNo(assetWarrantyClaim.getClaimReference());
		BankReceiptsDetail bankReceiptDetail = null;
		List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();

		bankReceiptDetail = new BankReceiptsDetail();
		bankReceiptDetail.setAmount(AIOSCommons
				.roundThreeDecimals(assetWarrantyClaim.getClaimAmount()));
		bankReceiptDetail.setRecordId(assetWarrantyClaim
				.getAssetWarrantyClaimId());
		bankReceiptDetail.setUseCase(AssetWarrantyClaim.class.getSimpleName());
		bankReceiptsDetails.add(bankReceiptDetail);

		bankReceipt.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
				bankReceiptsDetails));
		return bankReceipt;
	}

	// Bank Receipt Screen Detail
	private void bankReceiptScreenDetail() throws Exception {
		List<Currency> currencyList = bankReceiptsBL.getTransactionBL()
				.getCurrencyService()
				.getAllCurrency(bankReceiptsBL.getImplementation());
		for (Currency list : currencyList) {
			if (list.getDefaultCurrency())
				ServletActionContext.getRequest().setAttribute(
						"DEFAULT_CURRENCY", list.getCurrencyId());
		}
		if (null == bankReceiptsBL.getImplementation().getClearingAccount())
			ServletActionContext.getRequest().setAttribute("CLEARING_ACCOUNT",
					false);
		else
			ServletActionContext.getRequest().setAttribute("CLEARING_ACCOUNT",
					true);
		if (null == bankReceiptsBL.getImplementation().getPdcReceived())
			ServletActionContext.getRequest().setAttribute("PDC_RECEIVED",
					false);
		else
			ServletActionContext.getRequest()
					.setAttribute("PDC_RECEIVED", true);
		if (null != bankReceiptsBL.getTransactionBL().getPeriodDetails()) {
			ServletActionContext.getRequest().setAttribute(
					"PERIOD_INFO",
					bankReceiptsBL.getTransactionBL().getPeriodDetails()
							.getStartTime());
		}
		if (null != bankReceiptsBL.getImplementation().getClearingAccount()
				&& (long) bankReceiptsBL.getImplementation()
						.getClearingAccount() > 0) {
			Combination combination = bankReceiptsBL
					.getTransactionBL()
					.getCombinationService()
					.getCombinationAllAccountDetail(
							bankReceiptsBL.getImplementation()
									.getClearingAccount());
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_LINECODE",
					combination.getAccountByNaturalAccountId().getAccount()
							+ ""
							+ (null != combination
									.getAccountByAnalysisAccountId() ? "."
									+ combination
											.getAccountByAnalysisAccountId()
											.getAccount() : ""));
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_LINEID", combination.getCombinationId());
		}

		ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
				bankReceiptsBL.getModeOfPayment());
		ServletActionContext.getRequest().setAttribute("PAYMENT_STATUS",
				bankReceiptsBL.getPaymentStatus());
		ServletActionContext.getRequest().setAttribute(
				"BANK_RECEIPTS_NO",
				SystemBL.getReferenceStamp(BankReceipts.class.getName(),
						bankReceiptsBL.getImplementation()));
		ServletActionContext.getRequest().setAttribute(
				"RECEIPT_TYPES",
				bankReceiptsBL.getLookupMasterBL().getActiveLookupDetails(
						"RECEIPT_TYPES", true));
		ServletActionContext.getRequest().setAttribute(
				"RECEIPT_REFERENCE",
				bankReceiptsBL.getLookupMasterBL().getActiveLookupDetails(
						"BANK_RECEIPT_REFERENCE", true));
		ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
				currencyList);
		ServletActionContext.getRequest().setAttribute("RECEIPT_SOURCE",
				bankReceiptsBL.getReceiptSource());
	}

	// Show PDC Bank Receipts
	public String showPDCBankReceipt() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPDCBankReceipt");
			BankReceipts bankReceipts = bankReceiptsBL.getBankReceiptsService()
					.findPDCBankReceiptsById(recordId);
			List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
			for (BankReceiptsDetail receipts : bankReceipts
					.getBankReceiptsDetails()) {
				if (DateFormat.monthDifference(bankReceiptsBL
						.getTransactionBL().getPeriodDetails().getStartTime(),
						receipts.getChequeDate()) == 0) {
					bankReceiptsDetails.add(receipts);
				}
			}
			bankReceipts
					.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
							bankReceiptsDetails));
			ServletActionContext.getRequest().setAttribute("PAYMENT_STATUS",
					bankReceiptsBL.getPaymentStatus());
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			LOGGER.info("Module: Accounts : Method: showPDCBankReceipt Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPDCBankReceipt Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show All Paid Bank Receipts for Deposit
	public String showAllPaidBankReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllPaidBankReceipts");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<BankReceipts> bankReceipts = bankReceiptsBL
					.getBankReceiptsService().getPaidBankReceipts(
							bankReceiptsBL.getImplementation(),
							PaymentStatus.Paid.getCode());
			JSONObject jsonList = bankReceiptsBL
					.convertBankReceiptsToJson(bankReceipts);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			Map<Long, BankReceipts> bankReceiptsMap = new HashMap<Long, BankReceipts>();
			for (BankReceipts list : bankReceipts)
				bankReceiptsMap.put(list.getBankReceiptsId(), list);
			session.setAttribute(
					"BANK_PAID_RECEIPTS_SESSION_"
							+ sessionObj.get("jSessionId"), bankReceiptsMap);
			LOGGER.info("Module: Accounts : Method: showAllPaidBankReceipts Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllPaidBankReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Bank Receipts by Receipts Id
	@SuppressWarnings("unchecked")
	public String showBankReceiptsByReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBankReceiptsByReceipts");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, BankReceipts> bankReceiptsMap = (Map<Long, BankReceipts>) session
					.getAttribute("BANK_PAID_RECEIPTS_SESSION_"
							+ sessionObj.get("jSessionId"));
			BankReceipts bankReceipts = bankReceiptsMap.get(bankReceiptsId);
			List<BankReceipts> bankReceiptList = new ArrayList<BankReceipts>();
			bankReceiptList.add(bankReceipts);
			BankReceiptsDetailVO receiptVO = null;
			List<BankReceiptsDetail> receiptVOs = new ArrayList<BankReceiptsDetail>();
			for (BankReceipts bankReceipt : bankReceiptList) {
				for (BankReceiptsDetail bankReceiptsDetail : bankReceipt
						.getBankReceiptsDetails()) {
					receiptVO = new BankReceiptsDetailVO();
					BeanUtils.copyProperties(receiptVO, bankReceiptsDetail);
					receiptVO.setPaymentModeName(ModeOfPayment
							.get(receiptVO.getPaymentMode()).name()
							.replaceAll("_", " "));
					receiptVOs.add(receiptVO);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_DETAIL", receiptVOs);
			LOGGER.info("Module: Accounts : Method: showBankReceiptsByReceipts Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsByReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Bank Receipts by Receipts Type Id
	public String showBankReceiptsByReceiptsType() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBankReceiptsByReceiptsType");
			List<BankReceiptsDetail> bankReceiptsDetails = bankReceiptsBL
					.getBankReceiptsService().getBankReceiptsDetailByType(
							bankReceiptsBL.getImplementation(), receiptTypeId,
							PaymentStatus.Paid.getCode());
			BankReceiptsDetailVO receiptVO = null;
			List<BankReceiptsDetail> receiptVOs = new ArrayList<BankReceiptsDetail>();
			for (BankReceiptsDetail bankReceiptsDetail : bankReceiptsDetails) {
				receiptVO = new BankReceiptsDetailVO();
				BeanUtils.copyProperties(receiptVO, bankReceiptsDetail);
				receiptVO.setPaymentModeName(ModeOfPayment
						.get(receiptVO.getPaymentMode()).name()
						.replaceAll("_", " "));
				receiptVOs.add(receiptVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_DETAIL", receiptVOs);
			LOGGER.info("Module: Accounts : Method: showBankReceiptsByReceiptsType Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsByReceiptsType Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showBankReceiptsByReceipt() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBankReceiptsByReceipt");
			List<BankReceiptsDetail> bankReceiptsDetails = bankReceiptsBL
					.getBankReceiptsService()
					.getBankReceiptsByReceipt(
							bankReceiptsBL.getImplementation(),
							PaymentStatus.Paid.getCode(),
							dateFrom,
							dateTo,
							null != paymentMode && !("").equals(paymentMode) ? paymentMode
									.split("@@")[0] : null);
			List<BankReceiptsDetailVO> receiptVOs = null;
			if (null != bankReceiptsDetails && bankReceiptsDetails.size() > 0) {
				Map<Long, BankReceiptsDetail> receiptMap = new HashMap<Long, BankReceiptsDetail>();
				for (BankReceiptsDetail bankReceiptsDt : bankReceiptsDetails) {
					receiptMap.put(bankReceiptsDt.getBankReceiptsDetailId(),
							bankReceiptsDt);
				}
				List<PettyCash> pettyCashes = bankReceiptsBL
						.getPettyCashService()
						.getReceiptsPettyCashOnlyCashTransfer(
								receiptMap.keySet());
				BankReceiptsDetail receiptsDetail = null;
				if (null != pettyCashes && pettyCashes.size() > 0) {
					for (PettyCash pettyCash : pettyCashes) {
						if (receiptMap.containsKey(pettyCash
								.getBankReceiptsDetail()
								.getBankReceiptsDetailId())) {
							receiptsDetail = receiptMap.get(pettyCash
									.getBankReceiptsDetail()
									.getBankReceiptsDetailId());
							receiptsDetail.setAmount((receiptsDetail
									.getAmount() - pettyCash.getCashIn()));
							receiptMap.put(
									receiptsDetail.getBankReceiptsDetailId(),
									receiptsDetail);
						}
					}
				}
				receiptVOs = new ArrayList<BankReceiptsDetailVO>();
				BankReceiptsDetailVO receiptVO = null;
				for (Entry<Long, BankReceiptsDetail> entry : receiptMap
						.entrySet()) {
					receiptVO = new BankReceiptsDetailVO();
					if (null != entry.getValue().getAmount()
							&& (double) entry.getValue().getAmount() > 0) {
						BeanUtils.copyProperties(receiptVO, entry.getValue());
						receiptVO.setReferenceNo(entry.getValue()
								.getBankReceipts().getReceiptsNo());
						receiptVO.setPaymentModeName(ModeOfPayment
								.get(receiptVO.getPaymentMode()).name()
								.replaceAll("_", " "));
						receiptVO.setUseCase(BankReceiptsDetail.class
								.getSimpleName());
						receiptVO.setRecordId(entry.getKey());
						receiptVOs.add(receiptVO);
					}
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_DETAIL", receiptVOs);
			LOGGER.info("Module: Accounts : Method: showBankReceiptsByReceipt Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsByReceipt Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Bank Receipts Add Row
	public String showBankReceiptsAddRow() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBankReceiptsAddRow");
			ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
					bankReceiptsBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYMENT_STATUS",
					bankReceiptsBL.getPaymentStatus());
			ServletActionContext.getRequest().setAttribute(
					"RECEIPT_REFERENCE",
					bankReceiptsBL.getLookupMasterBL().getActiveLookupDetails(
							"BANK_RECEIPT_REFERENCE", true));
			ServletActionContext.getRequest().setAttribute("ROW_ID", id);
			if (null != bankReceiptsBL.getImplementation().getClearingAccount()
					&& (long) bankReceiptsBL.getImplementation()
							.getClearingAccount() > 0) {
				Combination combination = bankReceiptsBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAllAccountDetail(
								bankReceiptsBL.getImplementation()
										.getClearingAccount());
				ServletActionContext
						.getRequest()
						.setAttribute(
								"COMBINATION_LINECODE",
								combination.getAccountByNaturalAccountId()
										.getAccount()
										+ ""
										+ (null != combination
												.getAccountByAnalysisAccountId() ? "."
												+ combination
														.getAccountByAnalysisAccountId()
														.getAccount()
												: ""));
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_LINEID", combination.getCombinationId());
			}
			LOGGER.info("Module: Accounts : Method: showDirectPaymentAddRow Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save or Update Bank Receipts
	public String saveBankReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveBankReceipts");

			// Validate Period
			boolean openPeriod = bankReceiptsBL.getTransactionBL()
					.getCalendarBL().transactionPostingPeriod(receiptsDate);

			if (!openPeriod) {
				returnMessage = "Period not open for the receipt date.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveBankReceipts: (Error : Period not opened)");
				return SUCCESS;
			}

			BankReceipts bankReceipts = new BankReceipts();
			BankReceipts bankReceiptsEdit = null;
			if (bankReceiptsId > 0) {
				bankReceiptsEdit = bankReceiptsBL.getBankReceiptsService()
						.findByBankReceiptsId(bankReceiptsId);
				bankReceipts.setPersonByCreatedBy(bankReceiptsEdit
						.getPersonByCreatedBy());
				bankReceipts.setBankReceiptsId(bankReceiptsId);
				bankReceipts
						.setExchangeRate(bankReceiptsEdit.getExchangeRate());
				bankReceipts.setCurrency(bankReceiptsEdit.getCurrency());
				bankReceipts.setReceiptsNo(bankReceiptsEdit.getReceiptsNo());
			} else {
				bankReceipts.setReceiptsNo(SystemBL.getReferenceStamp(
						BankReceipts.class.getName(),
						bankReceiptsBL.getImplementation()));
			}
			if (bankReceiptsId == 0
					|| (null != bankReceiptsEdit && (long) bankReceiptsEdit
							.getCurrency().getCurrencyId() != currencyId)) {
				Currency defaultCurrency = bankReceiptsBL.getTransactionBL()
						.getCurrency();
				if (null != defaultCurrency) {
					if ((long) defaultCurrency.getCurrencyId() != currencyId) {
						Currency currency = bankReceiptsBL.getTransactionBL()
								.getCurrencyService().getCurrency(currencyId);
						CurrencyConversion currencyConversion = bankReceiptsBL
								.getTransactionBL().getCurrencyConversionBL()
								.getCurrencyConversionService()
								.getCurrencyConversionByCurrency(currencyId);
						bankReceipts.setCurrency(currency);
						bankReceipts
								.setExchangeRate(null != currencyConversion ? currencyConversion
										.getConversionRate() : 1.0);
					} else {
						bankReceipts.setCurrency(defaultCurrency);
						bankReceipts.setExchangeRate(1.0);
					}
				}
			}
			bankReceipts.setReceiptsDate(DateFormat
					.convertStringToDate(receiptsDate));
			bankReceipts.setReferenceNo(referenceNo);
			if (useCaseId > 0) {
				bankReceipts.setUseCase(useCaseName);
				bankReceipts.setRecordId(useCaseId);
			}
			Combination combination = new Combination();
			combination.setCombinationId(combinationId);
			bankReceipts.setCombination(combination);
			bankReceipts.setDescription(description);
			if (null != payeeType && !("").equals(payeeType)
					&& !("OT").equalsIgnoreCase(payeeType)) {
				if (("SPL").equalsIgnoreCase(payeeType)) {
					Supplier supplier = new Supplier();
					supplier.setSupplierId(supplierId);
					bankReceipts.setSupplier(supplier);
				} else if (("EMP").equalsIgnoreCase(payeeType)) {
					Person person = new Person();
					person.setPersonId(supplierId);
					bankReceipts.setPersonByPersonId(person);
				} else if (("CST").equalsIgnoreCase(payeeType)) {
					Customer customer = new Customer();
					customer.setCustomerId(supplierId);
					bankReceipts.setCustomer(customer);
				}
			} else
				bankReceipts.setOthers(vendorName);
			String[] lineDetailArray = splitValues(bankReceiptLines, "@@");
			BankReceiptsDetail bankReceiptsDetail = null;
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(receiptsTypeId);
			bankReceipts.setLookupDetail(lookupDetail);
			bankReceipts.setImplementation(bankReceiptsBL.getImplementation());
			List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
			List<BankReceiptsDetailVO> bankReceiptsDetailsVo = new ArrayList<BankReceiptsDetailVO>();
			for (String string : lineDetailArray) {
				String[] detailList = splitValues(string, "__");
				bankReceiptsDetail = new BankReceiptsDetail();
				lookupDetail = new LookupDetail();
				bankReceiptsDetail.setPaymentMode(Byte
						.parseByte((detailList[0])));
				bankReceiptsDetail.setAmount(Double.parseDouble(detailList[1]));
				if (null != detailList[2] && !("##").equals(detailList[2]))
					bankReceiptsDetail.setDescription(detailList[2]);
				if (null != detailList[3] && !("##").equals(detailList[3]))
					bankReceiptsDetail.setInstitutionName(detailList[3]);
				if (null != detailList[4] && !("##").equals(detailList[4]))
					bankReceiptsDetail.setBankRefNo(detailList[4]);
				if (null != detailList[5] && !("##").equals(detailList[5]))
					bankReceiptsDetail.setChequeDate(DateFormat
							.convertStringToDate(detailList[5]));
				if (null != detailList[6] && Long.parseLong(detailList[6]) > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(Long
							.parseLong(detailList[6]));
					bankReceiptsDetail.setLookupDetail(lookupDetail);
				}
				bankReceiptsDetail.setPaymentStatus(PaymentStatus.Pending
						.getCode());
				if (null != detailList[7] && Long.parseLong(detailList[7]) > 0) {
					combination = new Combination();
					combination.setCombinationId(Long.parseLong(detailList[7]));
					bankReceiptsDetail.setCombination(combination);
				}
				if (null != detailList[8] && Long.parseLong(detailList[8]) > 0)
					bankReceiptsDetail.setBankReceiptsDetailId(Long
							.parseLong(detailList[8]));

				if (null != detailList[10]
						&& Integer.parseInt(detailList[10]) == 1) {
					bankReceiptsDetailsVo
							.add(convertReceiptsVO(bankReceiptsDetail));
				}
				if (null != detailList[11]
						&& Long.parseLong(detailList[11]) > 0) {
					bankReceiptsDetail.setRecordId(Long
							.parseLong(detailList[11]));
					bankReceiptsDetail.setUseCase(detailList[12]);
				}
				bankReceiptsDetails.add(bankReceiptsDetail);
			}

			if (bankReceiptsId > 0) {
				List<BankReceiptsDetail> bankReceiptsDetailDB = bankReceiptsBL
						.getBankReceiptsService()
						.getBankReceiptsDetailByReceipts(bankReceipts);
				List<BankReceiptsDetail> deleteBankReceiptsDetail = getDifferenceSimilarBankReceipts(
						bankReceiptsDetails, bankReceiptsDetailDB, "diffrence");
				List<BankReceiptsDetail> reverseBankReceiptsDetail = null;
				if (null != bankReceiptsDetailsVo
						&& bankReceiptsDetailsVo.size() > 0) {
					reverseBankReceiptsDetail = getDifferenceSimilarBankReceipts(
							convertReceiptList(bankReceiptsDetailsVo),
							bankReceiptsDetailDB, "similar");
				}
				bankReceiptsBL.saveBankReceipts(bankReceipts,
						bankReceiptsDetails, deleteBankReceiptsDetail,
						reverseBankReceiptsDetail,
						convertReceiptList(bankReceiptsDetailsVo), alertId);
			} else
				bankReceiptsBL.saveBankReceipts(bankReceipts,
						bankReceiptsDetails, null, null, null, alertId);

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Internal error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveBankReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Update PDC Receipt
	public String savePDCBankReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: savePDCBankReceipts");
			BankReceipts bankReceipts = bankReceiptsBL.getBankReceiptsService()
					.findByBankReceiptsId(bankReceiptsId);
			List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
			BankReceiptsDetail bankReceiptsDetail = null;
			for (BankReceiptsDetail receipts : bankReceipts
					.getBankReceiptsDetails()) {
				String[] lineDetailArray = splitValues(bankReceiptLines, "@@");
				for (String string : lineDetailArray) {
					String[] detailList = splitValues(string, "__");
					if (receipts.getBankReceiptsDetailId().equals(
							Long.parseLong(detailList[0]))
							|| receipts.getBankReceiptsDetailId() == Long
									.parseLong(detailList[0])) {
						bankReceiptsDetail = new BankReceiptsDetail();
						bankReceiptsDetail = receipts;
						bankReceiptsDetail.setIsPdc(false);
						bankReceiptsDetail.setPaymentStatus(Byte
								.parseByte(detailList[0]));
						bankReceiptsDetails.add(bankReceiptsDetail);
					}
				}
			}
			bankReceiptsBL.updateBankReceiptsPDC(bankReceipts,
					bankReceiptsDetails, alertId);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Internal error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: savePDCBankReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete bank receipts and bank receipts detail and reverse the transaction
	public String deleteBankReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveBankReceipts");
			bankReceiptsBL.deleteBankReceipts(bankReceiptsBL
					.getBankReceiptsService().findByBankReceiptsId(
							bankReceiptsId));
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Internal error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveBankReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String validateReceiptPDCCheque() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveBankReceipts");
			bankReceiptsObj = null;
			if (null != institutionDate && !("").equals(institutionDate)) {
				Period period = bankReceiptsBL
						.getTransactionBL()
						.getCalendarBL()
						.transactionPostingPeriod(
								DateFormat.convertStringToDate(DateFormat
										.convertDateToString(DateTime.now()
												.toLocalDate().toString())));
				Period pdcPeriod = bankReceiptsBL
						.getTransactionBL()
						.getCalendarBL()
						.getCalendarService()
						.getPeriodByPeriodDate(
								DateFormat.convertStringToDate(institutionDate),
								bankReceiptsBL.getImplementation());
				int days = 0;
				if (null != pdcPeriod)
					days = Days.daysBetween(new DateTime(period.getEndTime()),
							new DateTime(pdcPeriod.getEndTime())).getDays();
				else
					days = Days.daysBetween(
							new DateTime(period.getEndTime()),
							new DateTime(DateFormat
									.convertStringToDate(institutionDate)))
							.getDays();
				if (days > 0) {
					bankReceiptsObj = new BankReceiptsVO();
					Combination combination = bankReceiptsBL
							.getTransactionBL()
							.getCombinationService()
							.getCombinationAccountDetail(
									bankReceiptsBL.getImplementation()
											.getPdcReceived());
					bankReceiptsObj.setCombinationId(combination
							.getCombinationId());
					bankReceiptsObj.setCombinationAccount(combination
							.getAccountByNaturalAccountId().getAccount()
							+ ""
							+ (null != combination
									.getAccountByAnalysisAccountId() ? "."
									+ combination
											.getAccountByAnalysisAccountId()
											.getAccount() : ""));
				}
			}
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: validateReceiptPDCCheque Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// get Difference or Similar in two bank receipt list
	private List<BankReceiptsDetail> getDifferenceSimilarBankReceipts(
			List<BankReceiptsDetail> bankReceiptsDetails,
			List<BankReceiptsDetail> bankReceiptsDetailDB, String returnType)
			throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, BankReceiptsDetail> hashBankReceiptDetail = new HashMap<Long, BankReceiptsDetail>();
		if (null != bankReceiptsDetailDB && bankReceiptsDetailDB.size() > 0) {
			for (BankReceiptsDetail list : bankReceiptsDetailDB) {
				listOne.add(list.getBankReceiptsDetailId());
				hashBankReceiptDetail.put(list.getBankReceiptsDetailId(), list);
			}
			if (null != hashBankReceiptDetail
					&& hashBankReceiptDetail.size() > 0) {
				for (BankReceiptsDetail list : bankReceiptsDetails) {
					listTwo.add(list.getBankReceiptsDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		BankReceiptsDetail receiptsDetail = null;
		List<BankReceiptsDetail> receiptDetails = null;
		if (null != returnType && ("diffrence").equalsIgnoreCase(returnType)) {
			if (null != different && different.size() > 0) {
				receiptDetails = new ArrayList<BankReceiptsDetail>();
				for (Long list : different) {
					if (null != list && !list.equals(0)) {
						receiptsDetail = new BankReceiptsDetail();
						receiptsDetail = hashBankReceiptDetail.get(list);
						receiptDetails.add(receiptsDetail);
					}
				}
			}
			return receiptDetails;
		} else {
			if (null != similar && similar.size() > 0) {
				receiptDetails = new ArrayList<BankReceiptsDetail>();
				for (Long list : similar) {
					if (null != list && !list.equals(0)) {
						receiptsDetail = new BankReceiptsDetail();
						receiptsDetail = hashBankReceiptDetail.get(list);
						receiptDetails.add(receiptsDetail);
					}
				}
			}
			return receiptDetails;
		}
	}

	// convert vo's list to bank receipt detail list
	private static List<BankReceiptsDetail> convertReceiptList(
			List<BankReceiptsDetailVO> bankReceiptsDetailVOs) throws Exception {
		List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
		BankReceiptsDetail bankReceiptsDetail = null;
		for (BankReceiptsDetailVO list : bankReceiptsDetailVOs) {
			bankReceiptsDetail = new BankReceiptsDetail();
			bankReceiptsDetail.setBankReceiptsDetailId(list
					.getBankReceiptsDetailId());
			bankReceiptsDetail.setPaymentMode(list.getPaymentMode());
			bankReceiptsDetail.setPaymentStatus(list.getPaymentStatus());
			bankReceiptsDetail.setInstitutionName(list.getInstitutionName());
			bankReceiptsDetail.setBankRefNo(list.getBankRefNo());
			bankReceiptsDetail.setAmount(list.getAmount());
			bankReceiptsDetail.setChequeDate(list.getChequeDate());
			bankReceiptsDetail.setCombination(list.getCombination());
			bankReceiptsDetails.add(bankReceiptsDetail);
		}
		return bankReceiptsDetails;
	}

	// Convert to vo
	private static BankReceiptsDetailVO convertReceiptsVO(
			BankReceiptsDetail bankReceiptsDetail) throws Exception {
		BankReceiptsDetailVO bankReceiptsDetailVO = new BankReceiptsDetailVO();
		bankReceiptsDetailVO.setBankReceiptsDetailId(bankReceiptsDetail
				.getBankReceiptsDetailId());
		bankReceiptsDetailVO
				.setPaymentMode(bankReceiptsDetail.getPaymentMode());
		bankReceiptsDetailVO.setPaymentStatus(bankReceiptsDetail
				.getPaymentStatus());
		bankReceiptsDetailVO.setInstitutionName(bankReceiptsDetail
				.getInstitutionName());
		bankReceiptsDetailVO.setBankRefNo(bankReceiptsDetail.getBankRefNo());
		bankReceiptsDetailVO.setAmount(bankReceiptsDetail.getAmount());
		bankReceiptsDetailVO.setChequeDate(bankReceiptsDetail.getChequeDate());
		bankReceiptsDetailVO.setLookupDetail(bankReceiptsDetail
				.getLookupDetail());
		bankReceiptsDetailVO
				.setCombination(bankReceiptsDetail.getCombination());
		return bankReceiptsDetailVO;
	}

	public String updateBankReceiptsTransaction() {
		try {
			List<BankReceipts> bankReceipts = bankReceiptsBL
					.getBankReceiptsService().getAllBankReceiptsOnlyReceipts(
							bankReceiptsBL.getImplementation());
			if (null != bankReceipts && bankReceipts.size() > 0)
				bankReceiptsBL.updateBankReceiptsTransaction(bankReceipts);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: updateBankReceiptsTransaction Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: updateBankReceiptsTransaction Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updateBankReceiptsPDCCheque() {
		try {
			BankReceiptsDetail bankReceiptsDetail = bankReceiptsBL
					.getBankReceiptsService().getBankReceiptsDetailById(
							bankReceiptsDetailId);
			bankReceiptsBL.updateBankReceiptsPDC(bankReceiptsDetail, alertId);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: updateBankReceiptsPDCCheque Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: updateBankReceiptsPDCCheque Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Split the values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public long getBankReceiptsId() {
		return bankReceiptsId;
	}

	public void setBankReceiptsId(long bankReceiptsId) {
		this.bankReceiptsId = bankReceiptsId;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public long getReceiptTypeId() {
		return receiptTypeId;
	}

	public void setReceiptTypeId(long receiptTypeId) {
		this.receiptTypeId = receiptTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public String getReceiptsNo() {
		return receiptsNo;
	}

	public void setReceiptsNo(String receiptsNo) {
		this.receiptsNo = receiptsNo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReceiptsDate() {
		return receiptsDate;
	}

	public void setReceiptsDate(String receiptsDate) {
		this.receiptsDate = receiptsDate;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getPayeeType() {
		return payeeType;
	}

	public void setPayeeType(String payeeType) {
		this.payeeType = payeeType;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getBankReceiptLines() {
		return bankReceiptLines;
	}

	public void setBankReceiptLines(String bankReceiptLines) {
		this.bankReceiptLines = bankReceiptLines;
	}

	public long getReceiptsTypeId() {
		return receiptsTypeId;
	}

	public void setReceiptsTypeId(long receiptsTypeId) {
		this.receiptsTypeId = receiptsTypeId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public long getUseCaseId() {
		return useCaseId;
	}

	public void setUseCaseId(long useCaseId) {
		this.useCaseId = useCaseId;
	}

	public String getUseCaseName() {
		return useCaseName;
	}

	public void setUseCaseName(String useCaseName) {
		this.useCaseName = useCaseName;
	}

	public boolean isTemplatePrint() {
		return templatePrint;
	}

	public void setTemplatePrint(boolean templatePrint) {
		this.templatePrint = templatePrint;
	}

	public byte getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(byte receiptType) {
		this.receiptType = receiptType;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getInstitutionDate() {
		return institutionDate;
	}

	public void setInstitutionDate(String institutionDate) {
		this.institutionDate = institutionDate;
	}

	public BankReceiptsVO getBankReceiptsObj() {
		return bankReceiptsObj;
	}

	public void setBankReceiptsObj(BankReceiptsVO bankReceiptsObj) {
		this.bankReceiptsObj = bankReceiptsObj;
	}

	public long getBankReceiptsDetailId() {
		return bankReceiptsDetailId;
	}

	public void setBankReceiptsDetailId(long bankReceiptsDetailId) {
		this.bankReceiptsDetailId = bankReceiptsDetailId;
	}
}
