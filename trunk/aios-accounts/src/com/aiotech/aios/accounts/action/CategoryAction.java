package com.aiotech.aios.accounts.action;

import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.vo.CategoryVO;
import com.aiotech.aios.accounts.service.bl.CategoryBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class CategoryAction extends ActionSupport {

	private static final long serialVersionUID = -127721157513026491L;
	private CategoryBL categoryBL;
	private Implementation implementation = null;
	private String returnMessage;
	private long categoryId;
	private String name;
	private String fromDate;
	private String toDate;
	private byte status;

	private Long recordId;
	private Long messageId;
	private Integer processFlag;
	
	private static final Logger LOGGER = LogManager
			.getLogger(CategoryAction.class);

	public String returnSuccess() {
		LOGGER.info("Inside method returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	public String showJsonCategoryList() {
		LOGGER.info("Inside Module: Accounts : Method: showJsonCategoryList");
		try {
			JSONObject jsonList = categoryBL.getAllCategorylist(
					getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonCategoryList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showJsonCategoryList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCategoryUpdate() {
		LOGGER.info("Inside Module: Accounts : Method: showCategoryUpdate");
		try {
			if (null != recordId && recordId > 0)
				categoryId = recordId;
			Category category = categoryBL.getCategoryService()
					.getCategoryById(categoryId);
			CommentVO comment = new CommentVO();
			if (recordId != null && category != null
					&& category.getCategoryId() != 0 && recordId > 0) {
				comment.setRecordId(category.getCategoryId());
				comment.setUseCase(Category.class.getName());
				comment = categoryBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			ServletActionContext.getRequest().setAttribute("CATEGORY_INFO",
					category);
			LOGGER.info("Module: Accounts : Method: showCategoryUpdate: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCategoryUpdate: throws Exception "
					+ ex);
			return ERROR;
		}
	}
	
	public String categoryApproval() {
		LOGGER.info("Inside Module: Accounts : Method: categoryApproval");
		try {
			Category category = categoryBL.getCategoryService()
					.getCategoryById(recordId);
			CategoryVO categoryVO = new CategoryVO();
			BeanUtils.copyProperties(categoryVO, category);
			categoryVO.setStart(DateFormat.convertDateToString(category
					.getFromDate().toString()));
			categoryVO.setEnd(DateFormat.convertDateToString(category
					.getToDate().toString()));
			categoryVO.setStatusStr((byte) category.getStatus() == 1 ? "Active"
					: "Inactive");
			ServletActionContext.getRequest().setAttribute("CATEGORY_INFO",
					categoryVO);
			LOGGER.info("Module: Accounts : Method: categoryApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: categoryApproval: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveCategory() {
		LOGGER.info("Inside Module: Accounts : Method: saveCategory");
		try {
			Category category = new Category();
			if (categoryId > 0)
				category.setCategoryId(categoryId);
			boolean flag = true;
			List<Category> categoryList = getCategoryBL().getCategoryService()
					.getAllCategories(getImplementationId());
			if (null != categoryList && categoryList.size() > 0) {
				for (Category list : categoryList) {
					if (flag) {
						if (list.getName().equalsIgnoreCase(name)
								&& !list.getCategoryId().equals(categoryId)) {
							flag = false;
						}
					}
				}
			}
			if (flag) {
				category.setName(name);
				category.setFromDate(DateFormat.convertStringToDate(fromDate));
				category.setToDate(DateFormat.convertStringToDate(toDate));
				category.setStatus(status);
				category.setImplementation(getImplementation());
				categoryBL.saveCatgory(category);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveCategory: Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Category Name alreay exits.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveCategory: Action error "
						+ returnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry!Internal error. " + ex;
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveCategory: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCategoryDelete() {
		LOGGER.info("Inside Module: Accounts : Method: showCategoryDelete");
		try {
			List<Transaction> transaction = categoryBL
					.getTransactionService()
					.getAllTransactionByCategory(categoryId);
			if (null != transaction && transaction.size() > 0) {
				returnMessage = "Category has been used in transaction, Deletion Failed.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveCategory: Action error "
						+ returnMessage);
				return ERROR;
			} else {
				Category category = categoryBL.getCategoryService()
						.getCategoryById(categoryId);
				categoryBL
						.deleteCategory(category);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveCategory: Action Success");
				return SUCCESS;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry!Internal error. " + ex;
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveCategory: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public CategoryBL getCategoryBL() {
		return categoryBL;
	}

	public void setCategoryBL(CategoryBL categoryBL) {
		this.categoryBL = categoryBL;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Integer getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Integer processFlag) {
		this.processFlag = processFlag;
	}
}
