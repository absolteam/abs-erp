/**
 * @creation date: Sep 13, 2013
 * @usecase : Sales Invoice
 * @author Saleem
 */
package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryCharge;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.accounts.service.bl.SalesInvoiceBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.SalesInvoiceType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class SalesInvoiceAction extends ActionSupport {

	private static final long serialVersionUID = -6464580971486990234L;

	public static final Logger LOG = Logger.getLogger(SalesInvoiceAction.class);

	// Dependency
	private SalesInvoiceBL salesInvoiceBL;
	// Variables
	private long creditTermId;
	private long customerId;
	private long salesInvoiceId;
	private long invoiceId;
	private Long recordId;
	private byte status;
	private byte invoiceType;
	private long currencyId;
	private double totalInvoice;
	private double invoiceAmount;
	private boolean templatePrint;
	private String description;
	private String invoiceDate;
	private String returnMessage;
	private String referenceNumber;
	private String salesDeliveryDetails;
	private List<Object> aaData;
	private long parentInvoiceId;
	private String printableContent;
	private SalesInvoiceVO slInvoiceVO;

	public String returnSuccess() {
		try {
			LOG.info("Inside Module: Accounts : Method: returnSuccess()");
			recordId = null;
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			File file = new File(confProps.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ salesInvoiceBL.getImplementation().getImplementationId()
					+ "/o2c/sales-invoice.ftl");
			if (file.isFile())
				templatePrint = true;
			else
				templatePrint = false;
			LOG.info("Module: Accounts : Method: returnSuccess(): Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: returnSuccess() Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show All Sales Invoice JSON list
	public String showAllSalesInvoice() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllSalesInvoice");
			aaData = salesInvoiceBL.getAllSalesInvoice();
			LOG.info("Module: Accounts : Method: showAllSalesInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllSalesInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all Sales Invoice Continuous
	public String showContiniousInvoice() {
		try {
			LOG.info("Inside Module: Accounts : Method: showContiniousInvoice");
			aaData = salesInvoiceBL.getAllContiniousSalesInvoice();
			LOG.info("Module: Accounts : Method: showContiniousInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showContiniousInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public SalesInvoiceVO addSalesInvoice(SalesInvoice salesInvoice)
			throws Exception {
		double invoiceAmount = 0;
		SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO();
		invoiceAmount = salesInvoice.getInvoiceAmount();
		if (null != salesInvoice.getSalesInvoices()
				&& salesInvoice.getSalesInvoices().size() > 0) {
			for (SalesInvoice invoice : salesInvoice.getSalesInvoices()) {
				salesInvoiceVO = new SalesInvoiceVO();
				invoiceAmount += null != invoice.getInvoiceAmount() ? invoice
						.getInvoiceAmount() : 0;
			}
		}
		salesInvoiceVO.setTotalInvoice(salesInvoice.getTotalInvoice());
		salesInvoiceVO.setInvoiceAmount(invoiceAmount);
		salesInvoiceVO.setPendingInvoice(salesInvoiceVO.getTotalInvoice()
				- salesInvoiceVO.getInvoiceAmount());
		return salesInvoiceVO;
	}

	// Show Sales Continuous Entry
	public String showSalesInvoiceContinuosEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesInvoiceContinuosEntry");
			referenceNumber = generateReferenceNumber();
			SalesInvoice salesInvoice = salesInvoiceBL.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);
			List<Currency> currencies = salesInvoiceBL.getSalesDeliveryNoteBL()
					.getSalesOrderBL().getCurrencyService()
					.getAllCurrency(salesInvoiceBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			SalesDeliveryNoteVO salesDeliveryNoteVO = null;
			Map<Long, SalesDeliveryNoteVO> salesDeliveryNoteVOs = new HashMap<Long, SalesDeliveryNoteVO>();
			for (SalesInvoiceDetail invoiceDetail : salesInvoice
					.getSalesInvoiceDetails()) {
				salesDeliveryNoteVO = new SalesDeliveryNoteVO();
				if (salesDeliveryNoteVOs.containsKey(invoiceDetail
						.getSalesDeliveryDetail().getSalesDeliveryNote()
						.getSalesDeliveryNoteId())) {
					salesDeliveryNoteVO = salesDeliveryNoteVOs
							.get(invoiceDetail.getSalesDeliveryDetail()
									.getSalesDeliveryNote()
									.getSalesDeliveryNoteId());
					salesDeliveryNoteVO.setTotalDeliveryQty(salesDeliveryNoteVO
							.getTotalDeliveryQty()
							+ invoiceDetail.getSalesDeliveryDetail()
									.getDeliveryQuantity());
					salesDeliveryNoteVO
							.setTotalDeliveryAmount(salesDeliveryNoteVO
									.getTotalDeliveryAmount()
									+ (invoiceDetail.getSalesDeliveryDetail()
											.getUnitRate() * invoiceDetail
											.getSalesDeliveryDetail()
											.getDeliveryQuantity()));
				} else {
					List<SalesDeliveryCharge> deliveryCharges = salesInvoiceBL
							.getSalesDeliveryNoteBL()
							.getSalesDeliveryNoteService()
							.getSalesDeliveryChargesByDeliveryId(
									invoiceDetail.getSalesDeliveryDetail()
											.getSalesDeliveryNote()
											.getSalesDeliveryNoteId());
					double addtionalCharges = 0;
					if (null != deliveryCharges && deliveryCharges.size() > 0) {
						for (SalesDeliveryCharge salesDeliveryCharge : deliveryCharges)
							addtionalCharges += salesDeliveryCharge
									.getCharges();
					}
					salesDeliveryNoteVO.setAddtionalCharges(addtionalCharges);
					salesDeliveryNoteVO.setTotalDeliveryQty(invoiceDetail
							.getSalesDeliveryDetail().getDeliveryQuantity());
					salesDeliveryNoteVO
							.setTotalDeliveryAmount(invoiceDetail
									.getSalesDeliveryDetail().getUnitRate()
									* invoiceDetail.getSalesDeliveryDetail()
											.getDeliveryQuantity()
									+ (addtionalCharges));
					salesDeliveryNoteVO.setDispatchDate(DateFormat
							.convertDateToString(invoiceDetail
									.getSalesDeliveryDetail()
									.getSalesDeliveryNote().getDeliveryDate()
									.toString()));
					salesDeliveryNoteVO.setSalesDeliveryNoteId(invoiceDetail
							.getSalesDeliveryDetail().getSalesDeliveryNote()
							.getSalesDeliveryNoteId());
					salesDeliveryNoteVO.setReferenceNumber(invoiceDetail
							.getSalesDeliveryDetail().getSalesDeliveryNote()
							.getReferenceNumber());
					salesDeliveryNoteVO.setDescription(invoiceDetail
							.getSalesDeliveryDetail().getSalesDeliveryNote()
							.getDescription());
				}
				salesDeliveryNoteVOs.put(invoiceDetail.getSalesDeliveryDetail()
						.getSalesDeliveryNote().getSalesDeliveryNoteId(),
						salesDeliveryNoteVO);
			}
			ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
					salesInvoice);
			ServletActionContext.getRequest().setAttribute("SALES_DELIVERY",
					salesDeliveryNoteVOs.values());
			ServletActionContext.getRequest().setAttribute("CURRENCY_ALL",
					currencies);
			ServletActionContext.getRequest().setAttribute("INVOICE_STATUS",
					salesInvoiceBL.getO2CProcessStatus());
			ServletActionContext.getRequest().setAttribute(
					"SALES_INVOICE_INFO", addSalesInvoice(salesInvoice));
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM",
					salesInvoiceBL
							.getCreditTermBL()
							.getCreditTermService()
							.getAllCustomerCreditTerms(
									salesInvoiceBL.getImplementation()));
			recordId = null;
			LOG.info("Module: Accounts : Method: showSalesInvoiceContinuosEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesInvoiceContinuosEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSalesInvoiceApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesInvoiceApproval");
			salesInvoiceId = recordId;
			showSalesInvoiceEdit();
			LOG.info("Module: Accounts : Method: showSalesInvoiceApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesInvoiceApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSalesInvoiceRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesInvoiceRejectEntry");
			salesInvoiceId = recordId;
			showSalesInvoiceEdit();
			LOG.info("Module: Accounts : Method: showSalesInvoiceRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesInvoiceRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Sales Invoice Entry
	public String showSalesInvoiceEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesInvoiceEntry");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("DELIVERY_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));

			referenceNumber = generateReferenceNumber();

			List<Currency> currencies = salesInvoiceBL.getSalesDeliveryNoteBL()
					.getSalesOrderBL().getCurrencyService()
					.getAllCurrency(salesInvoiceBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_ALL",
					currencies);
			ServletActionContext.getRequest().setAttribute("INVOICE_STATUS",
					salesInvoiceBL.getO2CProcessStatus());
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM",
					salesInvoiceBL
							.getCreditTermBL()
							.getCreditTermService()
							.getAllCustomerCreditTerms(
									salesInvoiceBL.getImplementation()));
			LOG.info("Module: Accounts : Method: showSalesInvoiceEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesInvoiceEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Sales Invoice Edit
	public String showSalesInvoiceEdit() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesInvoiceEdit");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("DELIVERY_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));

			SalesInvoice invoice = salesInvoiceBL.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);
			SalesInvoiceVO salesInvoiceVO = salesInvoiceBL
					.convertEntityToVO(invoice);
			List<Currency> currencies = salesInvoiceBL.getSalesDeliveryNoteBL()
					.getSalesOrderBL().getCurrencyService()
					.getAllCurrency(salesInvoiceBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			CommentVO comment = new CommentVO();
			if (recordId != null && invoice != null
					&& invoice.getSalesInvoiceId() != 0 && recordId > 0) {
				comment.setRecordId(invoice.getSalesInvoiceId());
				comment.setUseCase(SalesInvoice.class.getName());
				comment = salesInvoiceBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			recordId = null;
			ServletActionContext.getRequest().setAttribute("CURRENCY_ALL",
					currencies);
			ServletActionContext.getRequest().setAttribute("INVOICE_STATUS",
					salesInvoiceBL.getO2CProcessStatus());
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM",
					salesInvoiceBL
							.getCreditTermBL()
							.getCreditTermService()
							.getAllCustomerCreditTerms(
									salesInvoiceBL.getImplementation()));

			ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
					salesInvoiceVO);
			LOG.info("Module: Accounts : Method: showSalesInvoiceEdit: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesInvoiceEdit Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveContinuousSalesInvoice() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveContinuousSalesInvoice");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			SalesInvoice salesInvoice = new SalesInvoice();
			salesInvoice.setInvoiceDate(DateFormat
					.convertStringToDate(invoiceDate));
			salesInvoice.setDescription(description);
			Currency currency = new Currency();
			currency.setCurrencyId(currencyId);
			salesInvoice.setCurrency(currency);
			if (creditTermId > 0) {
				CreditTerm creditTerm = new CreditTerm();
				creditTerm.setCreditTermId(creditTermId);
				salesInvoice.setCreditTerm(creditTerm);
			}
			Customer customer = new Customer();
			customer.setCustomerId(customerId);
			salesInvoice.setCustomer(customer);
			salesInvoice.setStatus(status);
			salesInvoice.setCreateDate(Calendar.getInstance().getTime());

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			salesInvoice.setPerson(person);
			salesInvoice.setReferenceNumber(SystemBL.getReferenceStamp(
					SalesInvoice.class.getName(),
					salesInvoiceBL.getImplementation()));
			salesInvoice.setInvoiceType(SalesInvoiceType.Credit.getCode());
			salesInvoice.setInvoiceAmount(invoiceAmount);
			SalesInvoice invoice = new SalesInvoice();
			invoice.setSalesInvoiceId(invoiceId);
			salesInvoice.setImplementation(salesInvoiceBL.getImplementation());
			salesInvoice.setSalesInvoice(invoice);
			salesInvoice.setProcessStatus(true);
			List<SalesInvoiceDetail> salesInvoiceDetails = salesInvoiceBL
					.getSalesInvoiceService().getSalesInvoiceDetailByInvoiceId(
							invoiceId);
			SalesInvoice salesInvoiceOld = salesInvoiceBL
					.getSalesInvoiceService().getInvoiceById(invoiceId);
			salesInvoiceOld.setProcessStatus(false);
			salesInvoiceBL.saveSalesInvoice(salesInvoice, salesInvoiceDetails);
			returnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: saveContinuousSalesInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "System Internal error";
			LOG.info("Module: Accounts : Method: saveContinuousSalesInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Sales Invoice
	public String saveSalesInvoice() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveSalesInvoice");
			SalesInvoice salesInvoice = new SalesInvoice();
			SalesInvoice salesInvoiceEdit = null;
			salesInvoice.setInvoiceDate(DateFormat
					.convertStringToDate(invoiceDate));
			salesInvoice.setDescription(description);
			Currency currency = new Currency();
			currency.setCurrencyId(currencyId);
			salesInvoice.setCurrency(currency);
			if (creditTermId > 0) {
				CreditTerm creditTerm = new CreditTerm();
				creditTerm.setCreditTermId(creditTermId);
				salesInvoice.setCreditTerm(creditTerm);
			}
			Customer customer = new Customer();
			customer.setCustomerId(customerId);
			salesInvoice.setCustomer(customer);
			salesInvoice.setStatus(status);
			salesInvoice.setCreateDate(Calendar.getInstance().getTime());
			salesInvoice.setInvoiceType(SalesInvoiceType.Credit.getCode());
			if (parentInvoiceId > 0) {
				SalesInvoice salesInvoiceParent = new SalesInvoice();
				salesInvoiceParent.setSalesInvoiceId(parentInvoiceId);
				salesInvoice.setSalesInvoice(salesInvoiceParent);
			} else {
				salesInvoice.setTotalInvoice(totalInvoice);
			}
			salesInvoice.setInvoiceAmount(invoiceAmount);
			if (totalInvoice > invoiceAmount)
				salesInvoice.setProcessStatus(true);
			else
				salesInvoice.setProcessStatus(false);
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, SalesDeliveryNoteVO> salesDeliveryNoteVOHash = (Map<Long, SalesDeliveryNoteVO>) (session
					.getAttribute("DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			String[] deliveryIdArray = splitValues(salesDeliveryDetails, "@#");
			List<SalesDeliveryNoteVO> salesDeliveryNoteVOs = new ArrayList<SalesDeliveryNoteVO>();
			for (String deliveryLine : deliveryIdArray) {
				if (salesDeliveryNoteVOHash.containsKey(Long
						.parseLong(deliveryLine))) {
					SalesDeliveryNoteVO salesDeliveryNoteVO = (SalesDeliveryNoteVO) salesDeliveryNoteVOHash
							.get(Long.parseLong(deliveryLine));
					salesDeliveryNoteVOs.add(salesDeliveryNoteVO);
				}
			}

			List<SalesInvoiceDetail> salesInvoiceDetails = null;
			List<SalesInvoiceDetail> salesInvoiceDetailList = new ArrayList<SalesInvoiceDetail>();
			List<SalesInvoiceDetail> deleteSalesInvoiceDetails = null;

			SalesInvoiceDetail salesInvoiceDetail = null;
			SalesDeliveryNote salesDeliveryNote = null;
			List<SalesDeliveryNote> salesDeliveryNotes = new ArrayList<SalesDeliveryNote>();
			List<CreditDetail> creditDetails = new ArrayList<CreditDetail>();
			for (SalesDeliveryNoteVO list : salesDeliveryNoteVOs) {
				salesInvoiceDetails = new ArrayList<SalesInvoiceDetail>();
				double totalAvailedQty = 0;
				for (SalesDeliveryDetailVO detail : list
						.getSalesDeliveryDetailVOs()) {
					salesInvoiceDetail = new SalesInvoiceDetail();
					salesInvoiceDetail.setProduct(detail.getProduct());
					salesInvoiceDetail.setInvoiceQuantity(null == detail
							.getInvoiceQuantity() ? detail
							.getDeliveryQuantity() : detail
							.getInvoiceQuantity());
					salesInvoiceDetail.setSalesDeliveryDetail(detail);
					salesInvoiceDetail.setUnitRate(detail.getInvoiceUnitRate());
					salesInvoiceDetails.add(salesInvoiceDetail);
					totalAvailedQty += (salesInvoiceDetail.getInvoiceQuantity() + (null != detail
							.getInvoicedQty() ? detail.getInvoicedQty() : 0));
					// Add Credit Note
					if ((double) salesInvoiceDetail.getInvoiceQuantity() < (double) detail
							.getDeliveryQuantity()) {
						CreditDetail creditDetail = new CreditDetail();
						creditDetail.setReturnQuantity((double) detail
								.getDeliveryQuantity()
								- (double) salesInvoiceDetail
										.getInvoiceQuantity());
						creditDetail.setSalesDeliveryDetail(detail);
						creditDetails.add(creditDetail);
					}
				}
				List<SalesInvoiceDetail> invoiceDetails = new ArrayList<SalesInvoiceDetail>(
						salesInvoiceDetails);
				for (SalesInvoiceDetail detail : salesInvoiceDetails) {
					if (detail.getInvoiceQuantity() <= 0)
						invoiceDetails.remove(detail);
				}
				salesInvoiceDetailList.addAll(invoiceDetails);
				if (null != invoiceDetails && invoiceDetails.size() > 0) {
					salesDeliveryNote = salesInvoiceBL
							.getSalesDeliveryNoteBL()
							.getSalesDeliveryNoteService()
							.getSalesDeliveryNoteInfo(
									list.getSalesDeliveryNoteId());
					salesDeliveryNote.setStatusNote((byte) 0);
					salesDeliveryNotes.add(salesDeliveryNote);
				}
			}
			List<SalesInvoiceDetail> existingSalesInvoiceDetails = null;
			if (salesInvoiceId > 0) {
				salesInvoice.setReferenceNumber(referenceNumber);
				salesInvoiceEdit = salesInvoiceBL.getSalesInvoiceService()
						.getSalesInvoiceById(salesInvoiceId);
				salesInvoice.setPerson(salesInvoiceEdit.getPerson());
				deleteSalesInvoiceDetails = getDeletedSalesInvoice(
						salesInvoiceDetailList, existingSalesInvoiceDetails);
			} else {
				salesInvoice.setReferenceNumber(generateReferenceNumber());
				User user = (User) sessionObj.get("USER");
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				salesInvoice.setPerson(person);
			}

			List<SalesDeliveryNote> salesDeliveryNoteList = new ArrayList<SalesDeliveryNote>(
					salesDeliveryNotes);
			if (null != deleteSalesInvoiceDetails
					&& deleteSalesInvoiceDetails.size() > 0) {
				for (SalesInvoiceDetail saleInvoiceDetail : deleteSalesInvoiceDetails) {
					if (null != salesDeliveryNoteList
							&& salesDeliveryNoteList.size() > 0) {
						for (SalesDeliveryNote deliveryNote : salesDeliveryNoteList) {
							SalesInvoiceDetail tempDetails = salesInvoiceBL
									.getSalesInvoiceService()
									.getSalesInvoiceDetailByInvoiceDetailId(
											saleInvoiceDetail
													.getSalesInvoiceDetailId());
							if (null != tempDetails
									&& !deliveryNote.getSalesDeliveryNoteId()
											.equals(tempDetails
													.getSalesDeliveryDetail()
													.getSalesDeliveryNote()
													.getSalesDeliveryNoteId())) {
								deliveryNote.setStatusNote((byte) 1);
								salesDeliveryNoteList.add(deliveryNote);
							}
						}
					} else {
						saleInvoiceDetail.getSalesDeliveryDetail()
								.getSalesDeliveryNote().setStatusNote((byte) 1);
						salesDeliveryNoteList.add(salesDeliveryNote);
					}
				}
			}
			if (null != salesInvoiceDetailList
					&& salesInvoiceDetailList.size() > 0) {
				salesInvoiceBL.saveSalesInvoice(salesInvoice,
						salesInvoiceDetailList, salesDeliveryNoteList,
						deleteSalesInvoiceDetails, salesInvoiceEdit);
				returnMessage = "SUCCESS";
			} else
				returnMessage = "Please enter Sales Invoice Detail";

			LOG.info("Module: Accounts : Method: saveSalesInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "System Internal error";
			LOG.info("Module: Accounts : Method: saveSalesInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Sales Invoice
	public String deleteSalesInvoice() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteSalesInvoice");
			SalesInvoice salesInvoice = salesInvoiceBL.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);
			salesInvoiceBL.deleteSalesInvoice(salesInvoice, null);
			returnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: deleteSalesInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "System Internal error";
			LOG.info("Module: Accounts : Method: deleteSalesInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get Sales Invoice Deleted List
	private List<SalesInvoiceDetail> getDeletedSalesInvoice(
			List<SalesInvoiceDetail> salesInvoiceDetails,
			List<SalesInvoiceDetail> salesInvoiceDetailsOld) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		List<SalesInvoiceDetail> salesInvoiceDetailList = new ArrayList<SalesInvoiceDetail>();
		Map<Long, SalesInvoiceDetail> hashInvoiceDetail = new HashMap<Long, SalesInvoiceDetail>();
		if (null != salesInvoiceDetailsOld && salesInvoiceDetailsOld.size() > 0) {
			for (SalesInvoiceDetail list : salesInvoiceDetailsOld) {
				listOne.add(list.getSalesInvoiceDetailId());
				hashInvoiceDetail.put(list.getSalesInvoiceDetailId(), list);
			}
			if (null != hashInvoiceDetail && hashInvoiceDetail.size() > 0) {
				for (SalesInvoiceDetail list : salesInvoiceDetails) {
					listTwo.add(list.getSalesInvoiceDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		if (null != different && different.size() > 0) {
			SalesInvoiceDetail tempInvoiceDetail = null;
			salesInvoiceDetailList = new ArrayList<SalesInvoiceDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempInvoiceDetail = new SalesInvoiceDetail();
					tempInvoiceDetail = hashInvoiceDetail.get(list);
					salesInvoiceDetailList.add(tempInvoiceDetail);
				}
			}
		}
		return salesInvoiceDetailList;
	}

	@SuppressWarnings("unused")
	public String printSalesInvoice() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_salesinvoice_template") != null ? ((String) session
					.getAttribute("print_salesinvoice_template"))
					.equalsIgnoreCase("true") : false;
			SalesInvoiceVO vo = null;
			List<SalesInvoiceVO> salesDeliveryList = null;
			SalesInvoice invoice = salesInvoiceBL.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);

			if (!isTemplatePrintEnabled) {
				List<SalesInvoiceVO> salesInvoiceList = this
						.fetchSalesInvoiceReportData();
				if (null != salesInvoiceList && salesInvoiceList.size() > 0) {
					for (SalesInvoiceVO salesInvoiceVO : salesInvoiceList) {
						Collections.sort(
								salesInvoiceVO.getSalesInvoiceDetailVOs(),
								new Comparator<SalesInvoiceDetailVO>() {
									public int compare(SalesInvoiceDetailVO s1,
											SalesInvoiceDetailVO s2) {
										return s1
												.getSalesInvoiceDetailId()
												.compareTo(
														s2.getSalesInvoiceDetailId());
									}
								});
					}
				}
				ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
						salesInvoiceList);
				return "default";

			} else {

				vo = salesInvoiceBL.convertEntityToVO(invoice);

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ salesInvoiceBL.getImplementation()
								.getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-sales-invoice-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (salesInvoiceBL.getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(salesInvoiceBL
							.getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("datetime",
						DateFormat.convertSystemDateToString(new Date()));

				rootMap.put("invoice", vo);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				return "template";
			}

		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	@SuppressWarnings("unused")
	public String printSalesDutyInvoice() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_salesinvoice_template") != null ? ((String) session
					.getAttribute("print_salesinvoice_template"))
					.equalsIgnoreCase("true") : false;
			SalesInvoiceVO vo = null;
			List<SalesInvoiceVO> salesDeliveryList = null;
			SalesInvoice invoice = salesInvoiceBL.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);

			if (!isTemplatePrintEnabled) {
				List<SalesInvoiceVO> salesInvoiceList = this
						.fetchSalesInvoiceReportData();
				if (null != salesInvoiceList && salesInvoiceList.size() > 0) {
					for (SalesInvoiceVO salesInvoiceVO : salesInvoiceList) {
						Collections.sort(
								salesInvoiceVO.getSalesInvoiceDetailVOs(),
								new Comparator<SalesInvoiceDetailVO>() {
									public int compare(SalesInvoiceDetailVO s1,
											SalesInvoiceDetailVO s2) {
										return s1
												.getSalesInvoiceDetailId()
												.compareTo(
														s2.getSalesInvoiceDetailId());
									}
								});
					}
				}
				ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
						salesInvoiceList);
				return SUCCESS;

			} else {

				vo = salesInvoiceBL.convertEntityToVO(invoice);

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ salesInvoiceBL.getImplementation()
								.getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-sales-duty-invoice-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (salesInvoiceBL.getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(salesInvoiceBL
							.getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("datetime",
						DateFormat.convertSystemDateToString(new Date()));

				rootMap.put("invoice", vo);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				return "template";
			}

		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public List<SalesInvoiceVO> fetchSalesInvoiceReportData() throws Exception {

		Long salesInvoiceId = 0L;

		if (ServletActionContext.getRequest().getParameter("salesInvoiceId") != null) {
			salesInvoiceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("salesInvoiceId"));
		}

		List<SalesInvoice> salesInvoiceList = salesInvoiceBL
				.getSalesInvoiceService().getFilteredSalesInvoice(
						salesInvoiceBL.getImplementation(), salesInvoiceId,
						null, null, null, null);

		List<SalesInvoiceVO> salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();

		for (SalesInvoice salesInvoice : salesInvoiceList) {
			SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO(salesInvoice);
			salesInvoiceVO.setSalesInvoiceDate(DateFormat
					.convertDateToString(salesInvoice.getInvoiceDate()
							.toString()));
			salesInvoiceVO.setStrCurrency(salesInvoice.getCurrency()
					.getCurrencyPool().getCode());

			if (salesInvoice.getCustomer() != null
					&& salesInvoice.getCustomer().getShippingDetails() != null
					&& salesInvoice.getCustomer().getShippingDetails().size() > 0) {
				salesInvoiceVO.setShippingAddress(salesInvoice.getCustomer()
						.getShippingDetails().iterator().next().getAddress());
			}

			if (salesInvoice.getCreditTerm() != null) {
				salesInvoiceVO.setCreditTermName(salesInvoice.getCreditTerm()
						.getName());
			} else {
				salesInvoiceVO.setCreditTermName("");
			}
			salesInvoiceVO.setStatusName(Constants.Accounts.O2CProcessStatus
					.get(salesInvoice.getStatus()).toString());
			salesInvoiceVO.setInvoiceTypeName(SalesInvoiceType.get(
					salesInvoice.getInvoiceType()).name());

			salesInvoiceVO.setCustomerName(null != salesInvoice.getCustomer()
					.getPersonByPersonId() ? salesInvoice
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesInvoice.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesInvoice
					.getCustomer().getCompany() ? salesInvoice.getCustomer()
					.getCompany().getCompanyName() : salesInvoice.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));
			salesInvoiceVO.setCustomerNameAndNo(salesInvoiceVO
					.getCustomerName()
					+ " ["
					+ salesInvoice.getCustomer().getCustomerNumber() + "]");

			List<SalesInvoiceDetailVO> salesInvoiceDetailVOs = new ArrayList<SalesInvoiceDetailVO>();
			if (null == salesInvoice.getSalesInvoiceDetails()
					|| salesInvoice.getSalesInvoiceDetails().size() == 0) {
				salesInvoice.setSalesInvoiceDetails(salesInvoice
						.getSalesInvoice().getSalesInvoiceDetails());
			}
			for (SalesInvoiceDetail detail : salesInvoice
					.getSalesInvoiceDetails()) {

				SalesInvoiceDetailVO salesInvoiceDetailVO = new SalesInvoiceDetailVO(
						detail);
				salesInvoiceDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesInvoiceDetailVO.setProductCode(detail.getProduct()
						.getCode());

				salesInvoiceDetailVO
						.setTotal((detail.getInvoiceQuantity() * detail
								.getUnitRate()));
				salesInvoiceDetailVOs.add(salesInvoiceDetailVO);
			}

			salesInvoiceVO.setSalesInvoiceDetailVOs(salesInvoiceDetailVOs);
			salesInvoiceVOs.add(salesInvoiceVO);

		}

		return salesInvoiceVOs;
	}

	public String showCustomerActiveInvoices() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCustomerActiveInvoices");
			List<SalesInvoice> salesInvoices = salesInvoiceBL
					.getSalesInvoiceService().getCustomerActiveInvoices(
							customerId, currencyId,
							O2CProcessStatus.Open.getCode());
			if (null != salesInvoices && salesInvoices.size() > 0) {
				java.util.Collections.sort(salesInvoices,
						new Comparator<SalesInvoice>() {
							public int compare(SalesInvoice o1, SalesInvoice o2) {
								return o2.getSalesInvoiceId().compareTo(
										o1.getSalesInvoiceId());
							}
						});
				ServletActionContext.getRequest().setAttribute(
						"CUSTOMER_INVOICE", salesInvoices);
			}
			LOG.info("Module: Accounts : Method: showCustomerActiveInvoices: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showCustomerActiveInvoices: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCustomerSalesInvoiceData() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCustomerSalesInvoiceData");
			SalesInvoice invoice = salesInvoiceBL.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);
			slInvoiceVO = new SalesInvoiceVO();
			slInvoiceVO.setInvoiceAmount(invoice.getInvoiceAmount());
			slInvoiceVO.setDescription(invoice.getDescription());
			slInvoiceVO.setReferenceNumber(invoice.getReferenceNumber());
			slInvoiceVO.setSalesInvoiceId(invoice.getSalesInvoiceId());
			slInvoiceVO.setUseCase(SalesInvoice.class.getSimpleName());
			LOG.info("Module: Accounts : Method: showCustomerSalesInvoiceData Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showCustomerSalesInvoiceData: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Generate Reference Number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(SalesInvoice.class.getName(),
				salesInvoiceBL.getImplementation());
	}

	// Split the value
	private static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters and Setters
	public SalesInvoiceBL getSalesInvoiceBL() {
		return salesInvoiceBL;
	}

	public void setSalesInvoiceBL(SalesInvoiceBL salesInvoiceBL) {
		this.salesInvoiceBL = salesInvoiceBL;
	}

	public long getSalesInvoiceId() {
		return salesInvoiceId;
	}

	public void setSalesInvoiceId(long salesInvoiceId) {
		this.salesInvoiceId = salesInvoiceId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getSalesDeliveryDetails() {
		return salesDeliveryDetails;
	}

	public void setSalesDeliveryDetails(String salesDeliveryDetails) {
		this.salesDeliveryDetails = salesDeliveryDetails;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public byte getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(byte invoiceType) {
		this.invoiceType = invoiceType;
	}

	public double getTotalInvoice() {
		return totalInvoice;
	}

	public void setTotalInvoice(double totalInvoice) {
		this.totalInvoice = totalInvoice;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public long getParentInvoiceId() {
		return parentInvoiceId;
	}

	public void setParentInvoiceId(long parentInvoiceId) {
		this.parentInvoiceId = parentInvoiceId;
	}

	public boolean isTemplatePrint() {
		return templatePrint;
	}

	public void setTemplatePrint(boolean templatePrint) {
		this.templatePrint = templatePrint;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public SalesInvoiceVO getSlInvoiceVO() {
		return slInvoiceVO;
	}

	public void setSlInvoiceVO(SalesInvoiceVO slInvoiceVO) {
		this.slInvoiceVO = slInvoiceVO;
	}
}
