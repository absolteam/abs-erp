/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationCharge;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationVO;
import com.aiotech.aios.accounts.service.bl.CustomerQuotationBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author Saleem
 */
public class CustomerQuotationAction extends ActionSupport {

	private static final long serialVersionUID = 609079491230674069L;

	private static final Logger LOG = Logger
			.getLogger(CustomerQuotationAction.class);

	// Dependency
	private CustomerQuotationBL customerQuotationBL;

	// Variables
	private long customerQuotationId;
	private long customerId;
	private byte paymentMode;
	private long personId;
	private long shippingMethod;
	private long shippingTerm;
	private Long recordId;
	private byte status;
	private String referenceNumber;
	private String quotationDate;
	private String expiryDate;
	private String shippingDate;
	private String customerReference;
	private String description;
	private String chargesDetails;
	private String quotationDetails;
	private String personName;
	private int rowId;
	private List<Object> aaData;
	private CustomerQuotationVO customerQuotationVO;
	private long creditTermId;

	public String returnSuccess() {
		LOG.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// show all customer quotation json list
	public String showAllCustomerQuotation() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllCustomerQuotation");
			aaData = customerQuotationBL.getAllCustomerQuotation();
			LOG.info("Module: Accounts : Method: showAllCustomerQuotation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllCustomerQuotation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Customer Quotation Entry
	public String customerQuotationEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: customerQuotationEntry");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			CustomerQuotationVO customerQuotationVO = null;
			if (customerQuotationId > 0) {
				CustomerQuotation customerQuotation = customerQuotationBL
						.getCustomerQuotationService()
						.getCustomerQuotationById(customerQuotationId);
				customerQuotationVO = new CustomerQuotationVO();
				BeanUtils
						.copyProperties(customerQuotationVO, customerQuotation);
				List<CustomerQuotationDetailVO> customerQuotationDetailVOs = new ArrayList<CustomerQuotationDetailVO>();
				CustomerQuotationDetailVO quotationDetailVO = null;
				for (CustomerQuotationDetail quotationDetail : customerQuotation
						.getCustomerQuotationDetails()) {
					quotationDetailVO = new CustomerQuotationDetailVO();
					BeanUtils
							.copyProperties(quotationDetailVO, quotationDetail);
					if (null != quotationDetail.getProductPackageDetail()) {
						quotationDetailVO.setPackageDetailId(quotationDetail
								.getProductPackageDetail()
								.getProductPackageDetailId());
						quotationDetailVO.setBaseUnitName(quotationDetail
								.getProduct().getLookupDetailByProductUnit()
								.getDisplayName());
					} else
						quotationDetailVO.setPackageDetailId(-1l);
					quotationDetailVO.setBaseQuantity(AIOSCommons
							.convertExponential(quotationDetail.getQuantity()));
					quotationDetailVO
							.setProductPackageVOs(customerQuotationBL
									.getPackageConversionBL()
									.getProductPackagingDetail(
											quotationDetail.getProduct()
													.getProductId()));
					customerQuotationDetailVOs.add(quotationDetailVO);
				}
				Collections.sort(customerQuotationDetailVOs,
						new Comparator<CustomerQuotationDetailVO>() {
							public int compare(CustomerQuotationDetailVO o1,
									CustomerQuotationDetailVO o2) {
								return o1
										.getCustomerQuotationDetailId()
										.compareTo(
												o2.getCustomerQuotationDetailId());
							}
						});
				customerQuotationVO
						.setCustomerQuotationDetailVOs(customerQuotationDetailVOs);
				CommentVO comment = new CommentVO();
				if (recordId != null && customerQuotation != null
						&& customerQuotation.getCustomerQuotationId() != 0
						&& recordId > 0) {
					comment.setRecordId(customerQuotation
							.getCustomerQuotationId());
					comment.setUseCase(CustomerQuotation.class.getName());
					comment = customerQuotationBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else {
				referenceNumber = generateReferenceNumber();
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_QUOTATION", customerQuotationVO);
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_METHOD",
					customerQuotationBL.getLookupMasterBL()
							.getActiveLookupDetails("SHIPPING_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_TERMS",
					customerQuotationBL.getLookupMasterBL()
							.getActiveLookupDetails("SHIPPING_TERMS", true));
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					customerQuotationBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					customerQuotationBL.getLookupMasterBL()
							.getActiveLookupDetails("CHARGES_TYPE", true));
			ServletActionContext.getRequest().setAttribute("QUOTATION_STATUS",
					customerQuotationBL.getO2CProcessStatus());
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM",
					customerQuotationBL
							.getCreditTermBL()
							.getCreditTermService()
							.getAllCustomerCreditTerms(
									customerQuotationBL.getImplementation()));
			LOG.info("Module: Accounts : Method: customerQuotationEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: customerQuotationEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Customer Quotation List
	public String showCustomerQuotation() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCustomerQuotation");
			List<CustomerQuotation> customerQuotations = customerQuotationBL
					.getCustomerQuotationService().getAllCustomerQuotation(
							customerQuotationBL.getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_QUOTATIONS", customerQuotations);
			LOG.info("Module: Accounts : Method: showCustomerQuotation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCustomerQuotation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Customer Quotation Info
	public String showCustomerQuotationInfo() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCustomerQuotationInfo");
			customerQuotationVO = customerQuotationBL
					.convertEntityQuotationVO(customerQuotationBL
							.getCustomerQuotationService()
							.getCustomerQuotationById(customerQuotationId));
			LOG.info("Module: Accounts : Method: showCustomerQuotationInfo: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCustomerQuotationInfo Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Customer Quotation
	public String saveCustomerQuotation() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveCustomerQuotation");

			CustomerQuotation customerQuotation = new CustomerQuotation();

			customerQuotation.setDate(DateFormat
					.convertStringToDate(quotationDate));
			customerQuotation.setExpiryDate(DateFormat
					.convertStringToDate(expiryDate));
			customerQuotation.setShippingDate(null != shippingDate ? DateFormat
					.convertStringToDate(shippingDate) : null);
			customerQuotation.setDescription(description);
			if (customerId > 0) {
				Customer customer = new Customer();
				customer.setCustomerId(customerId);
				customerQuotation.setCustomer(customer);
			}
			LookupDetail lookupDetail = null;
			if (paymentMode > 0) {
				customerQuotation.setModeOfPayment(paymentMode);
			}
			if (shippingMethod > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(shippingMethod);
				customerQuotation.setLookupDetailByShippingMethod(lookupDetail);
			}
			if (shippingTerm > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(shippingTerm);
				customerQuotation.setLookupDetailByShippingTerm(lookupDetail);
			}
			if (personId > 0) {
				Person person = new Person();
				person.setPersonId(personId);
				customerQuotation.setPerson(person);
			}

			if (creditTermId > 0) {
				CreditTerm creditTerm = new CreditTerm();
				creditTerm.setCreditTermId(creditTermId);
				customerQuotation.setCreditTerm(creditTerm);
			}
			customerQuotation.setStatus(status > 0 ? status : null);
			List<CustomerQuotationDetail> customerQuotationDetails = new ArrayList<CustomerQuotationDetail>();
			List<CustomerQuotationDetail> deleteQuotationDetails = null;
			List<CustomerQuotationCharge> customerQuotationCharges = null;

			List<CustomerQuotationCharge> deleteQuotationCharges = null;
			String[] quotationDetails = splitValues(this.quotationDetails, "@#");
			for (String detail : quotationDetails) {
				customerQuotationDetails
						.add(addCustomerQuotationDetail(detail));
			}

			if (null != chargesDetails && !("").equals(chargesDetails)) {
				String[] chargesDetails = splitValues(this.chargesDetails, "@#");
				customerQuotationCharges = new ArrayList<CustomerQuotationCharge>();
				for (String detail : chargesDetails) {
					customerQuotationCharges
							.add(addCustomerQuotationCharges(detail));
				}
			}

			if (customerQuotationId > 0) {
				customerQuotation.setCustomerQuotationId(customerQuotationId);
				customerQuotation.setReferenceNo(referenceNumber);
				deleteQuotationDetails = userDeletedQuotationDetails(
						customerQuotationDetails,
						customerQuotationBL.getCustomerQuotationService()
								.getCustomerQuotationDetailById(
										customerQuotationId));
				deleteQuotationCharges = userDeletedQuotationCharges(
						customerQuotationCharges,
						customerQuotationBL.getCustomerQuotationService()
								.getCustomerQuotationChargesById(
										customerQuotationId));
			} else {
				customerQuotation.setReferenceNo(generateReferenceNumber());
			}

			customerQuotationBL.saveCustomerQuotation(customerQuotation,
					customerQuotationDetails, customerQuotationCharges,
					deleteQuotationDetails, deleteQuotationCharges);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: customerQuotationEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: saveCustomerQuotation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCustomerQuotationApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCustomerQuotationApproval");
			customerQuotationId = recordId;
			customerQuotationEntry();
			LOG.info("Module: Accounts : Method: showCustomerQuotationApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCustomerQuotationApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCustomerQuotationRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCustomerQuotationRejectEntry");
			customerQuotationId = recordId;
			customerQuotationEntry();
			LOG.info("Module: Accounts : Method: showCustomerQuotationRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCustomerQuotationRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Quotation
	public String deleteCustomerQuotation() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteQuotation");
			CustomerQuotation customerQuotation = this.getCustomerQuotationBL()
					.getCustomerQuotationService()
					.getCustomerQuotation(customerQuotationId);
			customerQuotationBL.deleteCustomerQuotation(customerQuotation);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: customerQuotationEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: deleteQuotation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get user delete quotation charges
	private List<CustomerQuotationCharge> userDeletedQuotationCharges(
			List<CustomerQuotationCharge> customerQuotationCharges,
			List<CustomerQuotationCharge> customerQuotationChargesOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, CustomerQuotationCharge> hashQuotationCharges = new HashMap<Long, CustomerQuotationCharge>();
		if (null != customerQuotationChargesOld
				&& customerQuotationChargesOld.size() > 0) {
			for (CustomerQuotationCharge list : customerQuotationChargesOld) {
				listOne.add(list.getCustomerQuotationChargeId());
				hashQuotationCharges.put(list.getCustomerQuotationChargeId(),
						list);
			}
			if (null != customerQuotationCharges
					&& customerQuotationCharges.size() > 0) {
				for (CustomerQuotationCharge list : customerQuotationCharges) {
					listTwo.add(list.getCustomerQuotationChargeId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<CustomerQuotationCharge> quotationChargeDetailList = null;
		if (null != different && different.size() > 0) {
			CustomerQuotationCharge tempQuotationCharge = null;
			quotationChargeDetailList = new ArrayList<CustomerQuotationCharge>();
			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempQuotationCharge = new CustomerQuotationCharge();
					tempQuotationCharge = hashQuotationCharges.get(list);
					quotationChargeDetailList.add(tempQuotationCharge);
				}
			}
		}
		return quotationChargeDetailList;
	}

	// Get user delete quotation details
	private List<CustomerQuotationDetail> userDeletedQuotationDetails(
			List<CustomerQuotationDetail> customerQuotationDetails,
			List<CustomerQuotationDetail> customerQuotationDetailOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, CustomerQuotationDetail> hashQuotationCharges = new HashMap<Long, CustomerQuotationDetail>();
		if (null != customerQuotationDetailOld
				&& customerQuotationDetailOld.size() > 0) {
			for (CustomerQuotationDetail list : customerQuotationDetailOld) {
				listOne.add(list.getCustomerQuotationDetailId());
				hashQuotationCharges.put(list.getCustomerQuotationDetailId(),
						list);
			}
			for (CustomerQuotationDetail list : customerQuotationDetails) {
				listTwo.add(list.getCustomerQuotationDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<CustomerQuotationDetail> quotationDetailList = new ArrayList<CustomerQuotationDetail>();
		if (null != different && different.size() > 0) {
			CustomerQuotationDetail tempQuotationDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempQuotationDetail = new CustomerQuotationDetail();
					tempQuotationDetail = hashQuotationCharges.get(list);
					quotationDetailList.add(tempQuotationDetail);
				}
			}
		}
		return quotationDetailList;
	}

	// Add Customer Charges
	private CustomerQuotationCharge addCustomerQuotationCharges(
			String quotationCharge) {
		CustomerQuotationCharge customerQuotationCharge = new CustomerQuotationCharge();
		String quotationCharges[] = splitValues(quotationCharge, "__");
		LookupDetail charges = new LookupDetail();
		charges.setLookupDetailId(Long.parseLong(quotationCharges[0]));
		customerQuotationCharge.setLookupDetail(charges);
		customerQuotationCharge.setCharges(Double
				.parseDouble(quotationCharges[1]));
		if (null != quotationCharges[2] && !("##").equals(quotationCharges[2]))
			customerQuotationCharge.setDescription(quotationCharges[2]);
		if (null != quotationCharges[3]
				&& Long.parseLong(quotationCharges[3]) > 0)
			customerQuotationCharge.setCustomerQuotationChargeId(Long
					.parseLong(quotationCharges[3]));
		return customerQuotationCharge;
	}

	// Add Customer Detail
	private CustomerQuotationDetail addCustomerQuotationDetail(
			String quotationDetail) {
		CustomerQuotationDetail customerQuotationDetail = new CustomerQuotationDetail();
		String quotationDetails[] = splitValues(quotationDetail, "__");
		Product product = new Product();
		product.setProductId(Long.parseLong(quotationDetails[0]));
		customerQuotationDetail.setProduct(product);
		if (Long.parseLong(quotationDetails[1]) > 0l) {
			Store store = new Store();
			store.setStoreId(Long.parseLong(quotationDetails[1]));
			customerQuotationDetail.setStore(store);
		}
		customerQuotationDetail.setQuantity(Double
				.parseDouble(quotationDetails[2]));
		customerQuotationDetail.setUnitRate(Double
				.parseDouble(quotationDetails[3]));
		if (null != quotationDetails[4] && !("##").equals(quotationDetails[4])) {
			if (("P").equals(quotationDetails[4]))
				customerQuotationDetail.setIsPercentage(true);
			else
				customerQuotationDetail.setIsPercentage(false);
			customerQuotationDetail.setDiscount(Double
					.parseDouble(quotationDetails[5]));
		}

		if (null != quotationDetails[6]
				&& Long.parseLong(quotationDetails[6]) > 0) {
			ProductPackageDetail productPackageDetail = new ProductPackageDetail();
			productPackageDetail.setProductPackageDetailId(Long
					.parseLong(quotationDetails[6]));
			customerQuotationDetail
					.setProductPackageDetail(productPackageDetail);
		}
		if (null != quotationDetails[7]
				&& Double.parseDouble(quotationDetails[7]) > 0)
			customerQuotationDetail.setPackageUnit(Double
					.parseDouble(quotationDetails[7]));
		if (null != quotationDetails[8]
				&& Long.parseLong(quotationDetails[8]) > 0)
			customerQuotationDetail.setCustomerQuotationDetailId(Long
					.parseLong(quotationDetails[8]));

		return customerQuotationDetail;
	}

	// Quotation Detail Add Row
	public String customerQuotationDetailAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: customerQuotationDetailAddRow");
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: customerQuotationDetailAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: customerQuotationDetailAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Quotation Charges Add Row
	public String customerQuotationChargesAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: customerQuotationChargesAddRow");
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					customerQuotationBL.getLookupMasterBL()
							.getActiveLookupDetails("CHARGES_TYPE", true));
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: customerQuotationChargesAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: customerQuotationChargesAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Generate Reference Number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(CustomerQuotation.class.getName(),
				customerQuotationBL.getImplementation());
	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters and Setters
	public CustomerQuotationBL getCustomerQuotationBL() {
		return customerQuotationBL;
	}

	public void setCustomerQuotationBL(CustomerQuotationBL customerQuotationBL) {
		this.customerQuotationBL = customerQuotationBL;
	}

	public long getCustomerQuotationId() {
		return customerQuotationId;
	}

	public void setCustomerQuotationId(long customerQuotationId) {
		this.customerQuotationId = customerQuotationId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}

	public long getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(long shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public long getShippingTerm() {
		return shippingTerm;
	}

	public void setShippingTerm(long shippingTerm) {
		this.shippingTerm = shippingTerm;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getChargesDetails() {
		return chargesDetails;
	}

	public void setChargesDetails(String chargesDetails) {
		this.chargesDetails = chargesDetails;
	}

	public String getQuotationDetails() {
		return quotationDetails;
	}

	public void setQuotationDetails(String quotationDetails) {
		this.quotationDetails = quotationDetails;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public CustomerQuotationVO getCustomerQuotationVO() {
		return customerQuotationVO;
	}

	public void setCustomerQuotationVO(CustomerQuotationVO customerQuotationVO) {
		this.customerQuotationVO = customerQuotationVO;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}

}
