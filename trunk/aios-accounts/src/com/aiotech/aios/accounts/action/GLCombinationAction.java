package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.COATemplate;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CombinationTemplate;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationTemplateVO;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationTreeVO;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationVO;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentVO;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.GLCombinationService;
import com.aiotech.aios.accounts.service.bl.AccountBL;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.to.GLCombinationMasterTO;
import com.aiotech.aios.accounts.to.converter.GLCombinationTOConverter;
import com.aiotech.aios.common.to.Constants.Accounts.Segments;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GLCombinationAction extends ActionSupport {

	private static final long serialVersionUID = 4494265948039320331L;

	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(GLCombinationAction.class);

	// Common Variable Declarations
	private int commonCode;
	private int codeId;
	private int accessId;
	private long segmentId;
	private String companyCode;
	private String companyCodeDesc;
	private String costCode;
	private String costCodeDesc;
	private String naturalCode;
	private String naturalCodeDesc;
	private String analyisCode;
	private String analyisCodeDesc;
	private String bufferCode1;
	private String bufferCode1Desc;
	private String bufferCode2;
	private String bufferCode2Desc;
	private String description;
	private String coaHeaderId;
	private String coaName;
	private long revenueId;
	private long depositAccountId;
	private long feeAccountId;
	private long otherChargesId;
	private long releaseDamageId;
	private long pdcReceived;
	private long ownersEquityId;
	private long accountsPayableId;
	private String trnValue;
	private String tempLineId;
	private int actualLineId;
	private String showPage;
	private String sqlReturnMessage;
	private String accountBalance;
	private long companyId;
	private long costAccountId;
	private long naturalAccountId;
	private long analyisAccountId;
	private long bufferAccountId1;
	private long bufferAccountId2;
	private long combinationId;
	private long combinationCopyId;
	private long copySegmentId;
	private int getFlag;
	private String accountCodes;
	private long accountId;
	private Integer id;

	private long unearnedRevenue;
	private long pdcIssued;
	private long clearingAccount;
	private String combinationTemplateIds;
	List<String> codeCombinations = new ArrayList<String>();
	int level = -1;

	private long purchaseDiscountControlId;
	private long salesPenaltyControlId;
	private long purchasePenaltyControlId;
	private long accountsReceivableControlId;
	private long productionExpenseControlId;
	private long productionRevenueControlId;
	private long accumulatedDeprControlId;
	private long cashAccountDPControlId;
	private long salesDiscountControlId;
	private long pettyCashControlId;
	private long bankClearingControlId;
	private Long recordId;
	private Long messageId;
	private Integer processFlag;

	// Common Object Declarations
	private CombinationBL combinationBL;
	private GLCombinationService combinationService;
	private GLChartOfAccountService accountService;
	private GLCombinationMasterTO combinationTo = null;
	private List<GLCombinationMasterTO> combinationList = null;
	private List<Object> aaData;
	private AccountBL accountBL;
	private SystemService systemService;

	private Implementation implementation = null;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	/** To redirect the action success page **/
	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess");// +Thread.currentThread().getStackTrace()[1].getMethodName());
		ServletActionContext.getRequest().setAttribute("rowId", id);
		List<Implementation> implementations = null;
		try {
			recordId = null;
			getImplementId();
			implementations = systemService.getAllImplementation();
			ServletActionContext.getRequest().setAttribute("AllImplementation",
					implementations);
			ServletActionContext.getRequest()
					.setAttribute("newImplementationId",
							implementation.getImplementationId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String showCombinationList() {
		LOGGER.info("Module: Accounts : Method: showCombinationList");
		try {

			combinationList = new ArrayList<GLCombinationMasterTO>();
			combinationTo = new GLCombinationMasterTO();

			getImplementId();
			List<Combination> codeList = combinationService
					.getAllCombination(implementation);
			combinationList = GLCombinationTOConverter
					.convertToTOList(codeList);
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();

			if (null != combinationList && combinationList.size() > 0) {
				for (GLCombinationMasterTO list : combinationList) {
					JSONArray array = new JSONArray();
					array.add(list.getCombinationId() + "");
					array.add(list.getCompanyId() + "");
					array.add(list.getCompanyCode() + "["
							+ list.getCompanyCodeDesc() + "]");
					array.add(list.getCostAccountId() + "");
					array.add(list.getCostCode() + "[" + list.getCostCodeDesc()
							+ "]");
					array.add(list.getNaturalAccountId() + "");
					array.add(list.getNaturalCode() + "["
							+ list.getNaturalCodeDesc() + "]");
					array.add(list.getAnalyisAccountId() + "");
					array.add(list.getAnalyisCode() + "["
							+ list.getAnalyisCodeDesc() + "]");
					array.add(list.getBufferAccountId1() + "");
					array.add(list.getBufferCode1() + "["
							+ list.getBufferCode1Desc() + "]");
					array.add(list.getBufferAccountId2() + "");
					array.add(list.getBufferCode2() + "["
							+ list.getBufferCode2Desc() + "]");
					data.add(array);
				}
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showCombinationList: Action Success");
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get entry view
	public String showAddEntryPage() {
		LOGGER.info("Inside Module: Accounts : Method: showAddEntryPage: ");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			ServletActionContext.getRequest().setAttribute("page", showPage);
			getImplementId();
			List<Segment> segment = accountService.getAllSegment();
			combinationList = new ArrayList<GLCombinationMasterTO>();
			GLCombinationMasterTO accountCodes = null;
			List<Account> account = null;
			for (Segment list : segment) {
				account = accountService.getAllAccountCode(implementation,
						Long.parseLong(list.getSegmentId().toString()));
				switch (Integer.parseInt(list.getSegmentId().toString())) {
				case 1:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setCompanyList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"COMPANY_ACCOUNT", accountCodes);
					break;
				case 2:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setCostList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"COST_ACCOUNT", accountCodes);
					break;
				case 3:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setNaturalList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"NATURAL_ACCOUNT", accountCodes);
					break;
				case 4:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setAnalysisList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"ANALYSIS_ACCOUNT", accountCodes);
					break;
				case 5:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setBufferList1(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"BUFFER1_ACCOUNT", accountCodes);
					break;
				case 6:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setBufferList1(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"BUFFER2_ACCOUNT", accountCodes);
					break;
				}
			}
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			session.removeAttribute("COMBINATION_INFO_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: showAddEntryPage: action success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAddEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// show add row
	public String showAddRowLine() {
		LOGGER.info("Inside Module: Accounts : Method: showAddRowLine: ");
		try {
			getImplementId();
			List<Segment> segment = accountService.getAllSegment();
			combinationList = new ArrayList<GLCombinationMasterTO>();
			GLCombinationMasterTO accountCodes = null;
			List<Account> account = null;
			for (Segment list : segment) {
				account = accountService.getAllAccountCode(implementation,
						Long.parseLong(list.getSegmentId().toString()));
				switch (Integer.parseInt(list.getSegmentId().toString())) {
				case 1:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setCompanyList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"COMPANY_ACCOUNT", accountCodes);
					break;
				case 2:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setCostList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"COST_ACCOUNT", accountCodes);
					break;
				case 3:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setNaturalList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"NATURAL_ACCOUNT", accountCodes);
					break;
				case 4:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setAnalysisList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"ANALYSIS_ACCOUNT", accountCodes);
					break;
				case 5:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setBufferList1(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"BUFFER1_ACCOUNT", accountCodes);
					break;
				case 6:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(account);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setBufferList1(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"BUFFER2_ACCOUNT", accountCodes);
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("rowId", id);
			LOGGER.info("Module: Accounts : Method: showAddRowLine: action success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAddRowLine: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get line entry view
	public String showLineEntryPage() {
		LOGGER.info("Inside Module: Accounts : Method: showLineEntryPage: ");
		try {
			ServletActionContext.getRequest().setAttribute("page", showPage);
			ServletActionContext.getRequest().setAttribute("rowId", id);
			if (getFlag == 1) {
				segmentId = 5;
				Account account1 = combinationService.getDefaultAccountCodes(
						implementation, segmentId);
				Account account2 = combinationService.getDefaultAccountCodes(
						implementation, ++segmentId);
				ServletActionContext.getRequest().setAttribute("account1",
						account1);
				ServletActionContext.getRequest().setAttribute("account2",
						account2);
			}
			LOGGER.info("Module: Accounts : Method: showLineEntryPage: action success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showLineEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get account codes
	public String getAccountCodes() {
		LOGGER.info("Inside Module: Accounts : Method: getAccountCodes: ");
		try {
			List<Account> accountList = new ArrayList<Account>();
			combinationList = new ArrayList<GLCombinationMasterTO>();
			getImplementId();
			accountList = combinationService.getAllAccountCode(implementation,
					segmentId);
			combinationList = GLCombinationTOConverter
					.convertAccountToTOList(accountList);
			ServletActionContext.getRequest().setAttribute("accountcodeList",
					combinationList);
			LOGGER.info("Module: Accounts : Method: getAccountCodes: action success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: getAccountCodes: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save the add add entry
	@SuppressWarnings("unchecked")
	public String combinationAddAddSave() {
		LOGGER.info("Module: Accounts : Method: combinationAddAddSave");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Combination combination = new Combination();
			List<Combination> combinationList = null;
			Account account = new Account();
			account.setAccountId(companyId);
			combination.setAccountByCompanyAccountId(account);
			account.setAccountId(costAccountId);
			combination.setAccountByCostcenterAccountId(account);
			account.setAccountId(naturalAccountId);
			combination.setAccountByNaturalAccountId(account);
			if (analyisAccountId > 0) {
				account.setAccountId(analyisAccountId);
				combination.setAccountByAnalysisAccountId(account);
			} else
				combination.setAccountByAnalysisAccountId(null);
			if (bufferAccountId1 > 0) {
				account.setAccountId(bufferAccountId1);
				combination.setAccountByBuffer1AccountId(account);
			} else
				combination.setAccountByBuffer1AccountId(null);
			if (bufferAccountId2 > 0) {
				account.setAccountId(bufferAccountId2);
				combination.setAccountByBuffer2AccountId(account);
			} else
				combination.setAccountByBuffer2AccountId(null);

			if (session.getAttribute("COMBINATION_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				combinationList = new ArrayList<Combination>();
			else
				combinationList = (List<Combination>) session
						.getAttribute("COMBINATION_INFO_"
								+ sessionObj.get("jSessionId"));
			combinationList.add(combination);
			session.setAttribute(
					"COMBINATION_INFO_" + sessionObj.get("jSessionId"),
					combinationList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: combinationAddAddSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: combinationAddAddSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to edit the line entry in add
	@SuppressWarnings("unchecked")
	public String combinationAddEditSave() {
		LOGGER.info("Module: Accounts : Method: combinationAddEditSave");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Combination combination = new Combination();
			List<Combination> combinationList = null;
			Account account = new Account();
			account.setAccountId(companyId);
			combination.setAccountByCompanyAccountId(account);
			account.setAccountId(costAccountId);
			combination.setAccountByCostcenterAccountId(account);
			account.setAccountId(naturalAccountId);
			combination.setAccountByNaturalAccountId(account);
			if (analyisAccountId > 0) {
				account.setAccountId(analyisAccountId);
				combination.setAccountByAnalysisAccountId(account);
			} else
				combination.setAccountByAnalysisAccountId(null);
			if (bufferAccountId1 > 0) {
				account.setAccountId(bufferAccountId1);
				combination.setAccountByBuffer1AccountId(account);
			} else
				combination.setAccountByBuffer1AccountId(null);
			if (bufferAccountId2 > 0) {
				account.setAccountId(bufferAccountId2);
				combination.setAccountByBuffer2AccountId(account);
			} else
				combination.setAccountByBuffer2AccountId(null);
			combinationList = (List<Combination>) session
					.getAttribute("COMBINATION_INFO_"
							+ sessionObj.get("jSessionId"));
			combinationList.set(id - 1, combination);
			session.setAttribute(
					"COMBINATION_INFO_" + sessionObj.get("jSessionId"),
					combinationList);
			ServletActionContext.getRequest().setAttribute("Success",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: combinationAddEditSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: combinationAddEditSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to delete the record from session
	@SuppressWarnings("unchecked")
	public String combinationAddDelete() {
		LOGGER.info("Module: Accounts : Method: combinationAddDelete");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<Combination> combinationList = null;
			combinationList = (List<Combination>) session
					.getAttribute("COMBINATION_INFO_"
							+ sessionObj.get("jSessionId"));
			combinationList.remove(id - 1);
			session.setAttribute(
					"COMBINATION_INFO_" + sessionObj.get("jSessionId"),
					combinationList);
			ServletActionContext.getRequest().setAttribute("Success",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: combinationAddDelete Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: combinationAddDelete: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save ledger list details
	public void saveLedgerList(List<Combination> combinationList)
			throws Exception {
		List<Ledger> ledgerList = new ArrayList<Ledger>();
		for (Combination list : combinationList) {
			Ledger ledger = new Ledger();
			ledger.setCombination(list);
			ledger.setSide(true);
			ledgerList.add(ledger);
		}
		combinationService.saveLedger(ledgerList);
	}

	// to save ledger details
	public void saveLedger(Combination combination) throws Exception {
		Ledger ledger = new Ledger();
		ledger.setCombination(combination);
		ledger.setSide(true);
		combinationService.saveLedger(ledger);
	}

	// to save add lines entries
	public String combinationAddSave() {
		LOGGER.info("Module: Accounts : Method: combinationAddSave");
		try {
			List<Combination> combinationList = null;
			String delimeter = "@@";
			String accountArray[] = splitArrayValues(accountCodes, delimeter);
			List<String> receiveList = Arrays.asList(accountArray);
			for (int i = 0; i < receiveList.size(); i++) {
				combinationList = GLCombinationTOConverter
						.convertToStringList(receiveList);
			}
			combinationService.saveAccountCode(combinationList);
			saveLedgerList(combinationList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute("sqlReturnMessage",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: combinationAddSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: combinationAddSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public static String[] splitArrayValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// to discard combination entries
	public String combinationDiscard() {
		LOGGER.info("Module: Accounts : Method: combinationDiscard");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("COMBINATION_INFO_"
					+ sessionObj.get("jSessionId"));
			ServletActionContext.getRequest().setAttribute("Success",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: combinationDiscard Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: combinationDiscard: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get edit view
	public String showEditEntryPage() {
		LOGGER.info("Module: Accounts : Method: showEditEntryPage");
		try {
			combinationList = new ArrayList<GLCombinationMasterTO>();
			combinationTo = new GLCombinationMasterTO();
			Account account = new Account();

			Combination combination = combinationService
					.getCombination(combinationId);
			combinationTo = GLCombinationTOConverter.convertToTO(combination);
			account = combinationService.getAllAccount(combinationTo
					.getCompanyId());
			combinationTo.setCompanyCode(account.getCode());
			combinationTo.setCompanyCodeDesc(account.getAccount());
			account = combinationService.getAllAccount(combinationTo
					.getCostAccountId());
			combinationTo.setCostCode(account.getCode());
			combinationTo.setCostCodeDesc(account.getAccount());
			account = combinationService.getAllAccount(combinationTo
					.getNaturalAccountId());
			combinationTo.setNaturalCode(account.getCode());
			combinationTo.setNaturalCodeDesc(account.getAccount());
			if (combinationTo.getAnalyisAccountId() > 0) {
				account = combinationService.getAllAccount(combinationTo
						.getAnalyisAccountId());
				combinationTo.setAnalyisCode(account.getCode());
				combinationTo.setAnalyisCodeDesc(account.getAccount());
			}
			if (combinationTo.getBufferAccountId1() > 0) {
				account = combinationService.getAllAccount(combinationTo
						.getBufferAccountId1());
				combinationTo.setBufferCode1(account.getCode());
				combinationTo.setBufferCode1Desc(account.getAccount());
			}
			if (combinationTo.getBufferAccountId2() > 0) {
				account = combinationService.getAllAccount(combinationTo
						.getBufferAccountId2());
				combinationTo.setBufferCode2(account.getCode());
				combinationTo.setBufferCode2Desc(account.getAccount());
			}
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_DETAILS", combinationTo);
			ServletActionContext.getRequest().setAttribute("page", showPage);

			getImplementId();
			List<Segment> segment = accountService.getAllSegment();
			combinationList = new ArrayList<GLCombinationMasterTO>();
			GLCombinationMasterTO accountCodes = null;
			List<Account> accountList = null;
			for (Segment list : segment) {
				accountList = accountService.getAllAccountCode(implementation,
						Long.parseLong(list.getSegmentId().toString()));
				switch (Integer.parseInt(list.getSegmentId().toString())) {
				case 1:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(accountList);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setCompanyList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"COMPANY_ACCOUNT", accountCodes);
					break;
				case 2:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(accountList);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setCostList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"COST_ACCOUNT", accountCodes);
					break;
				case 3:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(accountList);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setNaturalList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"NATURAL_ACCOUNT", accountCodes);
					break;
				case 4:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(accountList);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setAnalysisList(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"ANALYSIS_ACCOUNT", accountCodes);
					break;
				case 5:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(accountList);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setBufferList1(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"BUFFER1_ACCOUNT", accountCodes);
					break;
				case 6:
					combinationList = GLCombinationTOConverter
							.convertAccountToList(accountList);
					accountCodes = new GLCombinationMasterTO();
					accountCodes.setBufferList1(combinationList);
					ServletActionContext.getRequest().setAttribute(
							"BUFFER2_ACCOUNT", accountCodes);
					break;
				}
			}
			LOGGER.info("Module: Accounts : Method: showEditEntryPage Action Success");
			return SUCCESS;

		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showEditEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to do nothing method
	public String combinationUpdateLines() {
		ServletActionContext.getRequest().setAttribute("success",
				sqlReturnMessage);
		LOGGER.info("Module: Accounts : Method: combinationUpdateLines: Action Suceess ");
		return SUCCESS;
	}

	// to update combination entry
	public String combinationUpdate() {
		LOGGER.info("Module: Accounts : Method: combinationUpdate");
		try {
			Combination combination = new Combination();
			combination.setCombinationId(combinationId);
			Account account = new Account();
			account.setAccountId(companyId);
			combination.setAccountByCompanyAccountId(account);
			account.setAccountId(costAccountId);
			combination.setAccountByCostcenterAccountId(account);
			account.setAccountId(naturalAccountId);
			combination.setAccountByNaturalAccountId(account);
			if (analyisAccountId > 0) {
				account.setAccountId(analyisAccountId);
				combination.setAccountByAnalysisAccountId(account);
			} else
				combination.setAccountByAnalysisAccountId(null);
			if (bufferAccountId1 > 0) {
				account.setAccountId(bufferAccountId1);
				combination.setAccountByBuffer1AccountId(account);
			} else
				combination.setAccountByBuffer1AccountId(null);
			if (bufferAccountId2 > 0) {
				account.setAccountId(bufferAccountId2);
				combination.setAccountByBuffer2AccountId(account);
			} else
				combination.setAccountByBuffer2AccountId(null);
			combinationService.updateCombination(combination);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: combinationUpdate Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: combinationUpdate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Combination
	public String deleteCombinationLedger() {
		try {
			LOGGER.info("Module: Accounts : Method: deleteCombinationLedger");
			Combination combination = combinationBL.getCombinationService()
					.getCombinationWithLedger(combinationId);
			Ledger ledger = null;
			if (null != combination.getLedgers()
					&& combination.getLedgers().size() > 0) {
				ledger = new ArrayList<Ledger>(combination.getLedgers()).get(0);
				if (null == ledger.getBalance()
						&& (null == combination.getTransactionDetails() || combination
								.getTransactionDetails().size() <= 0)) {
					combinationBL.deleteCombinationLedger(combination, ledger);
					sqlReturnMessage = "SUCCESS";
				} else
					sqlReturnMessage = "Combination in use, can't be delete.";
			} else {
				combinationBL.deleteCombinationLedger(combination, ledger);
				sqlReturnMessage = "SUCCESS";
			}
			LOGGER.info("Module: Accounts : Method: deleteCombinationLedger Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Combination in use, can't be delete.";
			LOGGER.info("Module: Accounts : Method: deleteCombinationLedger: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCustomiseGeneralAccounts() {
		try {
			LOGGER.info("Module: Accounts : Method: showCustomiseGeneralAccounts");
			showUnEarnedRevenueSetUp();
			showAccountPayableSetUp();
			showPointOfSaleSetUp();
			LOGGER.info("Module: Accounts : Method: showCustomiseGeneralAccounts Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCustomiseGeneralAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save release combination
	public String saveCustomiseGeneralAccounts() {
		LOGGER.info("Module: Accounts : Method: saveCustomiseGeneralAccounts");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation implementation = new Implementation();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			implementation
					.setUnearnedRevenue(unearnedRevenue > 0 ? unearnedRevenue
							: null);
			implementation.setPdcReceived(pdcReceived > 0 ? pdcReceived : null);
			implementation.setPdcIssued(pdcIssued > 0 ? pdcIssued : null);
			implementation
					.setAccountsReceivable(accountsReceivableControlId > 0 ? accountsReceivableControlId
							: null);
			implementation
					.setExpenseAccount(productionExpenseControlId > 0 ? productionExpenseControlId
							: null);
			implementation
					.setRevenueAccount(productionRevenueControlId > 0 ? productionRevenueControlId
							: null);
			implementation
					.setAccumulatedDepreciation(accumulatedDeprControlId > 0 ? accumulatedDeprControlId
							: null);
			implementation.setOwnersEquity(ownersEquityId > 0 ? ownersEquityId
					: null);
			implementation
					.setClearingAccount(clearingAccount > 0 ? clearingAccount
							: null);
			implementation
					.setAccountPayable(accountsPayableId > 0 ? accountsPayableId
							: null);
			implementation
					.setCashAccount(cashAccountDPControlId > 0 ? cashAccountDPControlId
							: null);
			implementation
					.setSalesDiscount(salesDiscountControlId > 0 ? salesDiscountControlId
							: null);
			implementation
					.setPettyCashAccount(pettyCashControlId > 0 ? pettyCashControlId
							: null);
			implementation
					.setCreditCardAccount(bankClearingControlId > 0 ? bankClearingControlId
							: null);
			combinationService.updateImplementationAccounts(implementation);
			sqlReturnMessage = "Success";
			LOGGER.info("Module: Accounts : Method: saveCustomiseGeneralAccounts Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Transacation Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveCustomiseGeneralAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Contract Setup
	public String showUnEarnedRevenueSetUp() {
		try {
			LOGGER.info("Module: Accounts : Method: showUnEarnedRevenueSetUp");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation unearnedRevenueImpl = null;
			if (session.getAttribute("THIS") != null)
				unearnedRevenueImpl = (Implementation) session
						.getAttribute("THIS");
			if (unearnedRevenueImpl != null
					&& null != unearnedRevenueImpl.getUnearnedRevenue()) {
				Combination unearnedRevenue = combinationBL
						.getCombinationService()
						.getCombinationAllAccountDetail(
								unearnedRevenueImpl.getUnearnedRevenue());
				ServletActionContext.getRequest().setAttribute(
						"UNEARNED_REVENUE", unearnedRevenue);
			}
			if (unearnedRevenueImpl != null
					&& null != unearnedRevenueImpl.getPdcIssued()) {
				Combination pdcIssued = combinationBL.getCombinationService()
						.getCombinationAllAccountDetail(
								unearnedRevenueImpl.getPdcIssued());
				ServletActionContext.getRequest().setAttribute("PDC_ISSUED",
						pdcIssued);
			}
			if (unearnedRevenueImpl != null
					&& null != unearnedRevenueImpl.getPdcReceived()) {
				Combination pdcReceived = combinationBL.getCombinationService()
						.getCombinationAllAccountDetail(
								unearnedRevenueImpl.getPdcReceived());
				ServletActionContext.getRequest().setAttribute("PDC_RECEIVED",
						pdcReceived);
			}
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showUnEarnedRevenueSetUp: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Contract Setup
	public String showAccountPayableSetUp() {
		try {
			LOGGER.info("Module: Accounts : Method: showAccountPayableSetUp");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation accountsPaybleImpl = null;
			if (session.getAttribute("THIS") != null)
				accountsPaybleImpl = (Implementation) session
						.getAttribute("THIS");
			if (accountsPaybleImpl != null) {
				if (null != accountsPaybleImpl.getClearingAccount()) {
					Combination clearningAccount = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									accountsPaybleImpl.getClearingAccount());
					ServletActionContext.getRequest().setAttribute(
							"CLEARNING_ACCOUNT", clearningAccount);
				}
				if (null != accountsPaybleImpl.getOwnersEquity()) {
					Combination ownersEquity = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									accountsPaybleImpl.getOwnersEquity());
					ServletActionContext.getRequest().setAttribute(
							"OWNERS_EQUITY", ownersEquity);
				}
				if (null != accountsPaybleImpl.getAccountPayable()) {
					Combination accountsPayble = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									accountsPaybleImpl.getAccountPayable());
					ServletActionContext.getRequest().setAttribute(
							"ACCOUNTS_PAYABLE", accountsPayble);
				}
				if (null != accountsPaybleImpl.getCashAccount()) {
					Combination cashAccountDp = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									accountsPaybleImpl.getCashAccount());
					ServletActionContext.getRequest().setAttribute(
							"CASH_ACCOUNTDP", cashAccountDp);
				}
				if (null != accountsPaybleImpl.getPettyCashAccount()) {
					Combination pettyCashAccount = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									accountsPaybleImpl.getPettyCashAccount());
					ServletActionContext.getRequest().setAttribute(
							"PETTYCASH_ACCOUNT", pettyCashAccount);
				}
			}

			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAccountPayableSetUp: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Contract Setup
	public String showPointOfSaleSetUp() {
		try {
			LOGGER.info("Module: Accounts : Method: showPointOfSaleSetUp");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation pointOfSaleImpl = null;
			if (session.getAttribute("THIS") != null)
				pointOfSaleImpl = (Implementation) session.getAttribute("THIS");
			if (null != pointOfSaleImpl) {
				if (null != pointOfSaleImpl.getAccountsReceivable()) {
					Combination accountsReceivable = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									pointOfSaleImpl.getAccountsReceivable());
					ServletActionContext.getRequest().setAttribute(
							"ACCOUNTS_RECEIVABLE", accountsReceivable);
				}
				if (null != pointOfSaleImpl.getExpenseAccount()) {
					Combination expenseAccount = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									pointOfSaleImpl.getExpenseAccount());
					ServletActionContext.getRequest().setAttribute(
							"PRODUCT_EXPENSE", expenseAccount);
				}
				if (null != pointOfSaleImpl.getRevenueAccount()) {
					Combination revenueAccount = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									pointOfSaleImpl.getRevenueAccount());
					ServletActionContext.getRequest().setAttribute(
							"PRODUCT_REVENUE", revenueAccount);
				}
				if (null != pointOfSaleImpl.getAccumulatedDepreciation()) {
					Combination accumulatedDepreciation = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									pointOfSaleImpl
											.getAccumulatedDepreciation());
					ServletActionContext.getRequest().setAttribute(
							"ACCUMULATED_DEPR", accumulatedDepreciation);
				}
				if (null != pointOfSaleImpl.getSalesDiscount()) {
					Combination salesDiscount = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									pointOfSaleImpl.getSalesDiscount());
					ServletActionContext.getRequest().setAttribute(
							"SALESDISCOUNT_ACCOUNT", salesDiscount);
				}
				if (null != pointOfSaleImpl.getCreditCardAccount()) {
					Combination creditCardAccount = combinationBL
							.getCombinationService()
							.getCombinationAllAccountDetail(
									pointOfSaleImpl.getCreditCardAccount());
					ServletActionContext.getRequest().setAttribute(
							"BANKCLEARING_ACCOUNT", creditCardAccount);
				}
			}
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPointOfSaleSetUp: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save release combination
	public String saveUnEarnedRevenue() {
		LOGGER.info("Module: Accounts : Method: saveUnEarnedRevenue");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation unEarnedImpl = new Implementation();
			if (session.getAttribute("THIS") != null) {
				unEarnedImpl = (Implementation) session.getAttribute("THIS");
			}
			unEarnedImpl.setUnearnedRevenue(unearnedRevenue);
			unEarnedImpl.setPdcReceived(pdcReceived);
			unEarnedImpl.setPdcIssued(pdcIssued);
			combinationService.updateImplementationAccounts(unEarnedImpl);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveReleaseCombination Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Transacation Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveReleaseCombination: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save pointofsale control
	public String savePointOfSaleControl() {
		LOGGER.info("Module: Accounts : Method: savePointOfSaleControl");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation impl = new Implementation();
			if (session.getAttribute("THIS") != null) {
				impl = (Implementation) session.getAttribute("THIS");
			}
			impl.setAccountsReceivable(accountsReceivableControlId);
			impl.setExpenseAccount(productionExpenseControlId);
			impl.setRevenueAccount(productionRevenueControlId);
			impl.setAccumulatedDepreciation(accumulatedDeprControlId);
			combinationService.updateImplementationAccounts(impl);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: savePointOfSaleControl Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Transacation Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: savePointOfSaleControl: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save account payable control
	public String saveAccountsPayableControl() {
		LOGGER.info("Module: Accounts : Method: saveAccountsPayableControl");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Implementation payableImpl = new Implementation();
			if (session.getAttribute("THIS") != null) {
				payableImpl = (Implementation) session.getAttribute("THIS");
			}
			payableImpl.setOwnersEquity(ownersEquityId);
			payableImpl.setClearingAccount(clearingAccount);
			payableImpl.setAccountPayable(accountsPayableId);
			combinationService.updateImplementationAccounts(payableImpl);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveReleaseCombination Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Transacation Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveAccountsPayableControl: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings({ "unchecked" })
	public String showCombinationTemplate() {
		LOGGER.info("Module: Accounts : Method: showCombinationTemplate");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<CombinationTemplateVO> combinationTemplateVOList = this
					.getCombinationBL().generateCombinationTemplateTree();
			ServletActionContext.getRequest()
					.setAttribute("COMBINATION_TEMPLATE_TREE_VIEW",
							combinationTemplateVOList);
			if (null != session.getAttribute("SESSION_COMBINATION_IDS_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_IDS_"
									+ sessionObj.get("jSessionId")).equals("")) {
				List<Long> combinationIds = (ArrayList<Long>) session
						.getAttribute("SESSION_COMBINATION_IDS_"
								+ sessionObj.get("jSessionId"));
				session.removeAttribute("SESSION_COMBINATION_IDS_"
						+ sessionObj.get("jSessionId"));
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_ID", combinationIds);
			}
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			LOGGER.info("Module: Accounts : Method: showCombinationTemplate Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationTemplate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String showCoaTemplate() {
		LOGGER.info("Module: Accounts : Method: showCoaTemplate");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, CombinationTemplate> sessionCombinationTemplate = new HashMap<Long, CombinationTemplate>();
			if (null != session.getAttribute("SESSION_COMBINATION_TEMPLATE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TEMPLATE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				sessionCombinationTemplate = (HashMap<Long, CombinationTemplate>) session
						.getAttribute("SESSION_COMBINATION_TEMPLATE_"
								+ sessionObj.get("jSessionId"));
			}
			String[] lineDetailArray = splitValues(combinationTemplateIds, "#@");
			List<COATemplate> templateCOAList = new ArrayList<COATemplate>();
			COATemplate templateCOA = null;
			CombinationTemplate combinationTemplate = null;
			List<Long> combinationIds = new ArrayList<Long>();
			for (String string : lineDetailArray) {
				templateCOA = new COATemplate();
				combinationTemplate = new CombinationTemplate();
				combinationTemplate = sessionCombinationTemplate.get(Long
						.valueOf(string));
				combinationIds.add(combinationTemplate.getCombinationId());
				if (null != combinationTemplate
						.getCOATemplateByBuffer2AccountId()
						&& !("").equals(combinationTemplate
								.getCOATemplateByBuffer2AccountId())) {
					templateCOA.setAccount(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getAccount()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByAnalysisAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByBuffer1AccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByBuffer2AccountId()
									.getAccount()));
					templateCOA.setCode(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getCode()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByAnalysisAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByBuffer1AccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByBuffer2AccountId()
									.getCode()));
					templateCOA.setSegment(combinationTemplate
							.getCOATemplateByBuffer2AccountId().getSegment());
				} else if (null != combinationTemplate
						.getCOATemplateByBuffer1AccountId()
						&& !("").equals(combinationTemplate
								.getCOATemplateByBuffer1AccountId())) {
					templateCOA.setAccount(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getAccount()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByAnalysisAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByBuffer1AccountId()
									.getAccount()));
					templateCOA.setCode(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getCode()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByAnalysisAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByBuffer1AccountId()
									.getCode()));
					templateCOA.setSegment(combinationTemplate
							.getCOATemplateByBuffer1AccountId().getSegment());
				} else if (null != combinationTemplate
						.getCOATemplateByAnalysisAccountId()
						&& !("").equals(combinationTemplate
								.getCOATemplateByAnalysisAccountId())) {
					templateCOA.setAccount(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getAccount()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByAnalysisAccountId()
									.getAccount()));
					templateCOA.setCode(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getCode()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByAnalysisAccountId()
									.getCode()));
					templateCOA.setSegment(combinationTemplate
							.getCOATemplateByAnalysisAccountId().getSegment());
				} else if (null != combinationTemplate
						.getCOATemplateByNaturalAccountId()
						&& !("").equals(combinationTemplate
								.getCOATemplateByNaturalAccountId())) {
					templateCOA.setAccount(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getAccount()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getAccount())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getAccount()));
					templateCOA.setCode(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getCode()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getCode())
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByNaturalAccountId()
									.getCode()));
					templateCOA.setAccountType(combinationTemplate
							.getCOATemplateByNaturalAccountId()
							.getAccountType());
					templateCOA.setSegment(combinationTemplate
							.getCOATemplateByNaturalAccountId().getSegment());
				} else if (null != combinationTemplate
						.getCOATemplateByCostcenterAccountId()
						&& !("").equals(combinationTemplate
								.getCOATemplateByCostcenterAccountId())) {
					templateCOA.setAccount(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getAccount()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getAccount()));
					templateCOA.setCode(combinationTemplate
							.getCOATemplateByCompanyAccountId()
							.getCode()
							.concat(".")
							.concat(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getCode()));
					templateCOA
							.setSegment(combinationTemplate
									.getCOATemplateByCostcenterAccountId()
									.getSegment());
				} else if (null != combinationTemplate
						.getCOATemplateByCompanyAccountId()
						&& !("").equals(combinationTemplate
								.getCOATemplateByCompanyAccountId())) {
					templateCOA.setAccount(combinationTemplate
							.getCOATemplateByCompanyAccountId().getAccount());
					templateCOA.setCode(combinationTemplate
							.getCOATemplateByCompanyAccountId().getCode());
					templateCOA.setSegment(combinationTemplate
							.getCOATemplateByCompanyAccountId().getSegment());
				}
				templateCOAList.add(templateCOA);
			}
			ServletActionContext.getRequest().setAttribute("COA_TEMPLATE",
					templateCOAList);
			session.setAttribute(
					"SESSION_COMBINATION_IDS_" + sessionObj.get("jSessionId"),
					combinationIds);
			LOGGER.info("Module: Accounts : Method: showCoaTemplate Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCoaTemplate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showNewCombinationTree() {
		try {
			LOGGER.info("Module: Accounts : Method: showNewCombinationTree");
			List<Combination> combinations = combinationBL
					.getCombinationService().getAllParentCombination(
							combinationBL.getAccountBL().getImplementation());
			aaData = combinationBL.generateCombinationTree(combinations);
			LOGGER.info("Module: Accounts : Method: showNewCombinationTree Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showNewCombinationTree: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unused")
	private List<CombinationTreeVO> getCombinationRecursiveChild(
			CombinationTreeVO combinationTreeVO,
			List<CombinationTreeVO> combinationTreeVOs) throws Exception {
		if (null != combinationTreeVO.getChildren()
				&& combinationTreeVO.getChildren().size() > 0) {
			for (CombinationTreeVO cb : combinationTreeVO.getChildren()) {
				combinationTreeVOs.add(cb);
				getCombinationRecursiveChild(cb, combinationTreeVOs);
			}
		}
		return combinationTreeVOs;
	}

	public String showCombinationEntry() {
		try {
			LOGGER.info("Module: Accounts : Method: showCombinationEntry");
			int accessId = 0;
			Segment parentSegment = null;
			Segment segment = null;
			if (segmentId > 0) {
				segment = combinationBL.getAccountBL().getSegmentBL()
						.getSegmentService().getSegmentChilds(segmentId);
				accessId = (segment.getAccessId() + 1);
				parentSegment = combinationBL.getAccountBL().getSegmentBL()
						.getSegmentService()
						.getParentSegmentSp(implementation, accessId);
			} else {
				parentSegment = new Segment();
				accessId += 1;
				segment = combinationBL
						.getAccountBL()
						.getSegmentBL()
						.getSegmentService()
						.getParentSegment(
								combinationBL.getAccountBL()
										.getImplementation(), accessId);
				segment.setSegments(null);
				BeanUtils.copyProperties(parentSegment, segment);
			}
			List<Account> accounts = accountService.getAllAccountCode(
					implementation, parentSegment.getSegmentId());
			ServletActionContext.getRequest().setAttribute("ACCOUNTS_LIST",
					accounts);
			SegmentVO segmentVO = new SegmentVO();
			segmentVO.setSegmentId(segment.getSegmentId());
			segmentVO.setSegmentName(segment.getSegmentName());
			segmentVO.setParentName(parentSegment.getSegmentName());
			segmentVO.setParentSegmentId(parentSegment.getSegmentId());
			segmentVO.setParentAccessId(parentSegment.getAccessId());
			segmentVO.setCombinationId(combinationId);
			segmentVO.setShowSubAccountBtn(null != segment.getSegments()
					&& segment.getSegments().size() > 0 ? true : false);
			ServletActionContext.getRequest().setAttribute("SEGMENT_DETAIL",
					segmentVO);
			LOGGER.info("Module: Accounts : Method: showCombinationEntry Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCombinationTree() {
		try {
			LOGGER.info("Module: Accounts : Method: showCombinationTree");
			getImplementId();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("SESSION_COMBINATION_TREE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TREE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_TREE_"
						+ sessionObj.get("jSessionId"));
			}
			List<CombinationVO> combinationVOList = combinationBL
					.generateCombinationTree(implementation, 0l);
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			ServletActionContext.getRequest().setAttribute("PAGE", showPage);
			if (null != combinationVOList && combinationVOList.size() > 0)
				session.setAttribute(
						"SESSION_COMBINATION_TREE_"
								+ sessionObj.get("jSessionId"),
						combinationVOList);
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			LOGGER.info("Module: Accounts : Method: showCombinationTree Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationTree: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCombinationRejectTree() {
		LOGGER.info("Module: Accounts : Method: showCombinationRejectTree");
		try {
			getImplementId();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("SESSION_COMBINATION_TREE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TREE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_TREE_"
						+ sessionObj.get("jSessionId"));
			}
			List<CombinationVO> combinationVOList = combinationBL
					.generateCombinationTree(implementation, recordId);
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			ServletActionContext.getRequest().setAttribute("PAGE", showPage);
			if (null != combinationVOList && combinationVOList.size() > 0)
				session.setAttribute(
						"SESSION_COMBINATION_TREE_"
								+ sessionObj.get("jSessionId"),
						combinationVOList);
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			CommentVO comment = new CommentVO();
			if (recordId != null && recordId > 0) {
				Combination combination = new Combination();
				combination.setCombinationId(recordId);
				comment.setRecordId(combination.getCombinationId());
				comment.setUseCase(Combination.class.getName());
				comment = combinationBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			recordId = null;
			LOGGER.info("Module: Accounts : Method: showCombinationRejectTree Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationRejectTree: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String combinationApproval() {
		LOGGER.info("Module: Accounts : Method: combinationApproval");
		try {
			getImplementId();
			Combination combination = combinationBL.getCombinationService()
					.getCombinationByCombinationId(recordId);
			List<CombinationVO> combinationVOList = combinationBL
					.generateCombinationTree(implementation, combination);
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			ServletActionContext.getRequest().setAttribute("COMBINATION_VIEW",
					combination);
			LOGGER.info("Module: Accounts : Method: combinationApproval Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: combinationApproval: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCostCenterCombinationTree() {
		LOGGER.info("Module: Accounts : Method: showCostCenterCombinationTree");
		try {
			getImplementId();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("SESSION_COSTCENTER_TREE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COSTCENTER_TREE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COSTCENTER_TREE_"
						+ sessionObj.get("jSessionId"));
			}
			List<CombinationVO> combinationVOList = combinationBL
					.generateCostCenterCombinationTree(implementation);
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			if (null != combinationVOList && combinationVOList.size() > 0)
				session.setAttribute(
						"SESSION_COSTCENTER_TREE_"
								+ sessionObj.get("jSessionId"),
						combinationVOList);
			LOGGER.info("Module: Accounts : Method: showCostCenterCombinationTree Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCostCenterCombinationTree: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCostCenterCombinationReloadTree() {
		LOGGER.info("Module: Accounts : Method: showCostCenterCombinationReloadTree");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			getImplementId();
			if (null != session.getAttribute("SESSION_COMBINATION_TREE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TREE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				List<CombinationVO> combinationVOList = combinationBL
						.generateCombinationTree(implementation, 0l);
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_TREE_VIEW", combinationVOList);
				LOGGER.info("Module: Accounts : Method: showCostCenterCombinationReloadTree Action Success");
				return SUCCESS;
			} else {
				List<CombinationVO> combinationVOList = combinationBL
						.generateCombinationTree(implementation, 0l);
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_TREE_VIEW", combinationVOList);
				LOGGER.info("Module: Accounts : Method: showCostCenterCombinationReloadTree Action Success");
				return SUCCESS;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCostCenterCombinationReloadTree: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCombinationReloadTree() {
		LOGGER.info("Module: Accounts : Method: showCombinationReloadTree");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			getImplementId();
			if (null != session.getAttribute("SESSION_COMBINATION_TREE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TREE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				List<CombinationVO> combinationVOList = combinationBL
						.generateCombinationTree(implementation, 0l);
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_TREE_VIEW", combinationVOList);
				LOGGER.info("Module: Accounts : Method: showCombinationReloadTree Action Success");
				return SUCCESS;
			} else {
				List<CombinationVO> combinationVOList = combinationBL
						.generateCombinationTree(implementation, 0l);
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_TREE_VIEW", combinationVOList);
				LOGGER.info("Module: Accounts : Method: showCombinationReloadTree Action Success");
				return SUCCESS;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationReloadTree: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAccountList() {
		LOGGER.info("Module: Accounts : Method: showAccountList");
		try {
			getImplementId();
			if (segmentId > 6) {
				sqlReturnMessage = "Combination can't added to root end.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", sqlReturnMessage);
				return ERROR;
			} else {
				List<Account> accountList = accountService.getAllAccountCode(
						implementation, segmentId);
				ServletActionContext.getRequest().setAttribute(
						"COMBINATION_ACCOUNTS_LIST", accountList);
				return SUCCESS;
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAccountList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveCombinationAccount() {
		try {
			LOGGER.info("Module: Accounts : Method: saveCombinationAccount");
			Combination combination = null;
			Segment segment = null;
			if (combinationId > 0)
				combination = getCombinationService().getCombinationAccount(
						combinationId);
			else
				combination = new Combination();
			Account account = new Account();
			account.setAccountId(accountId);
			if ((int) Segments.Company.getCode() == accessId || accessId == 0)
				combination.setAccountByCompanyAccountId(account);
			else if ((int) Segments.CostCenter.getCode() == accessId)
				combination.setAccountByCostcenterAccountId(account);
			else if ((int) Segments.Natural.getCode() == accessId)
				combination.setAccountByNaturalAccountId(account);
			else if ((int) Segments.Analysis.getCode() == accessId)
				combination.setAccountByAnalysisAccountId(account);
			else if ((int) Segments.Buffer1.getCode() == accessId)
				combination.setAccountByBuffer1AccountId(account);
			else if ((int) Segments.Buffer2.getCode() == accessId)
				combination.setAccountByBuffer2AccountId(account);
			List<Combination> combinationExits = combinationBL
					.getCombinationService().validateCombination(combination,
							combinationId);
			if (null == combinationExits || combinationExits.size() == 0) {
				combination.setCombinationId(null);
				if (segmentId > 0)
					segment = combinationBL.getAccountBL().getSegmentBL()
							.getSegmentService().getSegmentById(segmentId);
				if ((null != segment && null != segment.getSegment())
						|| null != combination.getCombination()) {
					Combination selfCombination = new Combination();
					selfCombination.setCombinationId(combinationId);
					combination.setCombination(selfCombination);
				} else
					combination.setCombination(null);
				combinationBL.saveCombination(combination);
				sqlReturnMessage = "SUCCESS";
			} else
				sqlReturnMessage = "Selected combination already exits.";
			LOGGER.info("Module: Accounts : Method: saveCombinationAccount Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "FAILURE";
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: saveCombinationAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteCombinationAccount() {
		try {
			LOGGER.info("Module: Accounts : Method: deleteCombinationAccount");
			Combination combination = combinationBL.getCombinationService()
					.getCombinationWithChildAccountsDetail(combinationId);
			if (null == combination.getCombinations()
					|| combination.getCombinations().size() == 0) {
				List<Combination> combinationtmp = combinationBL
						.getCombinationService().getCombinationInDirectChild(
								combination);
				boolean deleteFlag = true;
				if (null != combinationtmp && combinationtmp.size() > 0) {
					for (Combination cb : combinationtmp) {
						if ((long) combination.getCombinationId() != (long) cb
								.getCombinationId()) {
							deleteFlag = false;
							break;
						}
					}
				}
				if (deleteFlag) {
					combinationBL.deleteCombination(combination);
					sqlReturnMessage = "SUCCESS";
				} else
					sqlReturnMessage = "Combination can't be deleted.";
			} else
				sqlReturnMessage = "Combination can't be deleted.";
			LOGGER.info("Module: Accounts : Method: deleteCombinationAccount Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "FAILURE";
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: deleteCombinationAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Combination getCombinationRecursiveChild(Combination combination)
			throws Exception {
		List<Combination> combinations = combinationService
				.getCombinationChildDetailWithAccount(combination
						.getCombinationId());
		List<Combination> combinationtmp = null;
		if (null != combinations && combinations.size() > 0) {
			combinationtmp = new ArrayList<Combination>();
			for (Combination cb : combinations) {
				combinationtmp.add(cb);
				getCombinationRecursiveChild(cb);
			}
		}
		if (null != combinationtmp && combinationtmp.size() > 0)
			combination
					.setCombinations(new HashSet<Combination>(combinationtmp));
		return combination;
	}

	private List<Combination> getCombinationRecursiveChild(
			Combination combination, List<Combination> combinations)
			throws Exception {
		List<Combination> combinationtmps = combinationService
				.getCombinationChildDetailWithAccount(combination
						.getCombinationId());
		List<Combination> combinationtmp = null;
		if (null != combinationtmps && combinationtmps.size() > 0) {
			combinationtmp = new ArrayList<Combination>();
			for (Combination cb : combinations) {
				combinationtmp.add(cb);
				getCombinationRecursiveChild(cb, combinationtmp);
			}
		}
		if (null != combinationtmp && combinationtmp.size() > 0)
			combinations.addAll(combinationtmp);
		return combinations;
	}

	public String pasteCombinationAccount() {
		try {
			LOGGER.info("Module: Accounts : Method: pasteCombinationAccount");
			Segment segment = combinationBL.getAccountBL().getSegmentBL()
					.getSegmentService().getSegmentParentById(segmentId);
			Segment copySegment = combinationBL.getAccountBL().getSegmentBL()
					.getSegmentService().getSegmentById(copySegmentId);
			if ((int) segment.getAccessId() <= (int) copySegment.getAccessId()) {
				Combination combinationCopy = combinationBL
						.getCombinationService().getCombinationAccountsDetail(
								combinationCopyId);
				Combination combinationPaste = combinationBL
						.getCombinationService().getCombinationAccountsDetail(
								combinationId);
				List<Combination> combinationCopys = combinationBL
						.getCombinationService().getCombinationDetail(
								combinationCopy, segment.getAccessId());
				// Check source combination presented
				Combination sourceCombination = null;
				if (null != copySegment.getSegment()
						&& null == segment.getSegment()) {
					sourceCombination = combinationBL.getCombinationService()
							.getSourceCombination(combinationPaste,
									copySegment.getAccessId());
					if (null == sourceCombination) {
						sourceCombination = new Combination();
						BeanUtils.copyProperties(sourceCombination,
								combinationCopy);
						sourceCombination.setCombination(null);
						sourceCombination.setCombinations(null);
						if (null == combinationCopys
								|| combinationCopys.size() == 0)
							combinationCopys = new ArrayList<Combination>();
						combinationCopys.add(sourceCombination);
					}
				}
				List<Combination> combinationCopysSelf = combinationBL
						.getCombinationService()
						.getSelfCombinationChildDetailWithAccount(
								combinationCopy.getCombinationId(),
								sourceCombination);
				if (null != combinationCopysSelf
						&& combinationCopysSelf.size() > 0) {
					for (Combination combination : combinationCopysSelf) {
						combination.setCombination(null);
						combination.setCombinations(null);
					}
					if (null == combinationCopys
							|| combinationCopys.size() == 0)
						combinationCopys = new ArrayList<Combination>();
					combinationCopys.addAll(combinationCopysSelf);
				}
				if (null != combinationCopys && combinationCopys.size() > 0) {
					Collections.sort(combinationCopys,
							new Comparator<Combination>() {
								public int compare(Combination o1,
										Combination o2) {
									return o1.getCombinationId().compareTo(
											o2.getCombinationId());
								}
							});

					long accountId = getCombinationPasteAccount(
							combinationPaste, segment.getAccessId());

					List<Combination> combinationParentChild = new ArrayList<Combination>();
					for (Combination combination : combinationCopys)
						combinationParentChild.add(this
								.getCombinationRecursiveChild(combination));
					Map<Long, Combination> copyMap = new HashMap<Long, Combination>();
					long key = 0l;
					for (Combination combination : combinationCopys) {
						key = getCombinationKey(combination);
						copyMap.put(key, combination);
					}

					List<Combination> combinations = combinationBL
							.getCombinationService().getCombinationDetail(
									combinationPaste, segment.getAccessId());
					// Combination paste self parent
					List<Combination> combinationPasteSelf = combinationBL
							.getCombinationService()
							.getSelfCombinationChildDetailWithAccount(
									combinationPaste.getCombinationId(), null);
					if (null != combinationPasteSelf
							&& combinationPasteSelf.size() > 0) {
						if (null == combinations || combinations.size() == 0)
							combinations = new ArrayList<Combination>();
						for (Combination cb1 : combinationPasteSelf) {
							combinations.add(cb1);
							combinations.addAll(getCombinationRecursiveChild(
									cb1, new ArrayList<Combination>()));
						}

					}
					if (null != combinations && combinations.size() > 0) {
						for (Combination combination : combinations) {
							key = getCombinationKey(combination);
							if (copyMap.containsKey(key))
								copyMap.remove(key);
						}
					}

					if (null != copyMap && copyMap.size() > 0) {
						Combination pasteObj = new Combination();
						pasteObj.setCombinationId(combinationId);
						List<Combination> combinationDts = new ArrayList<Combination>();
						for (Entry<Long, Combination> entry : copyMap
								.entrySet())
							combinationDts.addAll(updateCombination(
									entry.getValue(), accountId,
									segment.getAccessId(), combinationDts,
									pasteObj));
						if (null != combinationPaste
								&& null != combinationPaste.getCombination()) {
							for (Combination combination : combinationDts) {
								if (null == combination.getCombination()) {
									getActualParentCombination(combinationDts,
											combination, pasteObj);
									if (null == combination.getCombination())
										combination.setCombination(pasteObj);
								}
							}
						}
						combinationBL.saveCombinations(combinationDts);
						sqlReturnMessage = "SUCCESS";
					} else
						sqlReturnMessage = "Copied combination already already exits.";
				} else
					sqlReturnMessage = "No record found for the copied combination.";
			} else
				sqlReturnMessage = "Invalid location to paste.";
			LOGGER.info("Module: Accounts : Method: pasteCombinationAccount Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: pasteCombinationAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private List<Combination> updateCombination(Combination combination,
			long accountId, int accessId, List<Combination> combinationDts,
			Combination pasteObj) {
		Account account = new Account();
		account.setAccountId(accountId);
		boolean prflag = false;
		List<Combination> combinations = new ArrayList<Combination>();
		if ((int) Segments.Company.getCode() == accessId || accessId == 0) {
			if (null != combination.getAccountByCostcenterAccountId())
				combination.setAccountByCompanyAccountId(account);
			else
				prflag = true;
		} else if ((int) Segments.CostCenter.getCode() == accessId) {
			if (null != combination.getAccountByNaturalAccountId())
				combination.setAccountByCostcenterAccountId(account);
			else
				prflag = true;
		} else if ((int) Segments.Natural.getCode() == accessId) {
			if (null != combination.getAccountByAnalysisAccountId())
				combination.setAccountByNaturalAccountId(account);
			else
				prflag = true;
		} else if ((int) Segments.Analysis.getCode() == accessId) {
			if (null != combination.getAccountByBuffer1AccountId())
				combination.setAccountByAnalysisAccountId(account);
			else
				prflag = true;
		} else if ((int) Segments.Buffer1.getCode() == accessId) {
			if (null != combination.getAccountByBuffer2AccountId())
				combination.setAccountByBuffer1AccountId(account);
			else
				prflag = true;
		} else if ((int) Segments.Buffer2.getCode() == accessId)
			prflag = true;

		if (!prflag) {
			if (null == combination.getCombination())
				combinations = updateParentCombination(combination,
						combinations);
		} else {
			if (null != combination.getCombinations()) {
				combinations = updateParentCombination(combination,
						combinations);
			}
			if (null == combination.getCombination())
				combination.setCombination(pasteObj);
			combination.setCombinationId(null);
		}
		combination.setIsApprove((byte) WorkflowConstants.Status.Published
				.getCode());
		combinations.add(combination);
		return combinations;
	}

	private Combination getActualParentCombination(
			List<Combination> combinationDts, Combination combination,
			Combination combinationPaste) {
		if (null != combination.getAccountByCompanyAccountId()
				&& null == combination.getAccountByCostcenterAccountId())
			combination.setCombination(combinationPaste);
		else
			getParentCombination(combination, combinationDts);
		return combination;
	}

	private Combination getParentCombination(Combination combination,
			List<Combination> combinationDts) {
		if (null != combination.getAccountByCompanyAccountId()
				&& null != combination.getAccountByCostcenterAccountId()
				&& null == combination.getAccountByNaturalAccountId()) {
			for (Combination slf : combinationDts) {
				if (null != slf.getAccountByCompanyAccountId()
						&& null == slf.getAccountByCostcenterAccountId()
						&& (long) combination.getAccountByCompanyAccountId()
								.getAccountId() == (long) slf
								.getAccountByCompanyAccountId().getAccountId())
					combination.setCombination(slf);
			}
		} else if (null != combination.getAccountByCompanyAccountId()
				&& null != combination.getAccountByCostcenterAccountId()
				&& null != combination.getAccountByNaturalAccountId()
				&& null == combination.getAccountByAnalysisAccountId()) {
			for (Combination slf : combinationDts) {
				if (null != slf.getAccountByCompanyAccountId()
						&& null != slf.getAccountByCostcenterAccountId()
						&& null == slf.getAccountByNaturalAccountId()
						&& (long) combination.getAccountByCompanyAccountId()
								.getAccountId() == (long) slf
								.getAccountByCompanyAccountId().getAccountId()
						&& (long) combination.getAccountByCostcenterAccountId()
								.getAccountId() == (long) slf
								.getAccountByCostcenterAccountId()
								.getAccountId())
					combination.setCombination(slf);
			}
		} else if (null != combination.getAccountByCompanyAccountId()
				&& null != combination.getAccountByCostcenterAccountId()
				&& null != combination.getAccountByNaturalAccountId()
				&& null == combination.getAccountByAnalysisAccountId()) {
			for (Combination slf : combinationDts) {
				if (null != slf.getAccountByCompanyAccountId()
						&& null != slf.getAccountByCostcenterAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null == slf.getAccountByNaturalAccountId()
						&& (long) combination.getAccountByCompanyAccountId()
								.getAccountId() == (long) slf
								.getAccountByCompanyAccountId().getAccountId()
						&& (long) combination.getAccountByCostcenterAccountId()
								.getAccountId() == (long) slf
								.getAccountByCostcenterAccountId()
								.getAccountId())
					combination.setCombination(slf);
			}
		} else if (null != combination.getAccountByCompanyAccountId()
				&& null != combination.getAccountByCostcenterAccountId()
				&& null != combination.getAccountByNaturalAccountId()
				&& null != combination.getAccountByAnalysisAccountId()
				&& null == combination.getAccountByBuffer1AccountId()) {
			for (Combination slf : combinationDts) {
				if (null != slf.getAccountByCompanyAccountId()
						&& null != slf.getAccountByCostcenterAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null == slf.getAccountByAnalysisAccountId()
						&& (long) combination.getAccountByCompanyAccountId()
								.getAccountId() == (long) slf
								.getAccountByCompanyAccountId().getAccountId()
						&& (long) combination.getAccountByCostcenterAccountId()
								.getAccountId() == (long) slf
								.getAccountByCostcenterAccountId()
								.getAccountId()
						&& (long) combination.getAccountByNaturalAccountId()
								.getAccountId() == (long) slf
								.getAccountByNaturalAccountId().getAccountId())
					combination.setCombination(slf);
			}
		} else if (null != combination.getAccountByCompanyAccountId()
				&& null != combination.getAccountByCostcenterAccountId()
				&& null != combination.getAccountByNaturalAccountId()
				&& null != combination.getAccountByAnalysisAccountId()
				&& null != combination.getAccountByBuffer1AccountId()) {
			for (Combination slf : combinationDts) {
				if (null != slf.getAccountByCompanyAccountId()
						&& null != slf.getAccountByCostcenterAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null != slf.getAccountByAnalysisAccountId()
						&& null == slf.getAccountByBuffer1AccountId()
						&& (long) combination.getAccountByCompanyAccountId()
								.getAccountId() == (long) slf
								.getAccountByCompanyAccountId().getAccountId()
						&& (long) combination.getAccountByCostcenterAccountId()
								.getAccountId() == (long) slf
								.getAccountByCostcenterAccountId()
								.getAccountId()
						&& (long) combination.getAccountByNaturalAccountId()
								.getAccountId() == (long) slf
								.getAccountByNaturalAccountId().getAccountId()
						&& (long) combination.getAccountByAnalysisAccountId()
								.getAccountId() == (long) slf
								.getAccountByAnalysisAccountId().getAccountId())
					combination.setCombination(slf);
			}
		} else if (null != combination.getAccountByCompanyAccountId()
				&& null != combination.getAccountByCostcenterAccountId()
				&& null != combination.getAccountByNaturalAccountId()
				&& null != combination.getAccountByAnalysisAccountId()
				&& null != combination.getAccountByBuffer1AccountId()
				&& null != combination.getAccountByBuffer2AccountId()) {
			for (Combination slf : combinationDts) {
				if (null != slf.getAccountByCompanyAccountId()
						&& null != slf.getAccountByCostcenterAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null != slf.getAccountByNaturalAccountId()
						&& null != slf.getAccountByAnalysisAccountId()
						&& null != slf.getAccountByBuffer1AccountId()
						&& null == slf.getAccountByBuffer2AccountId()
						&& (long) combination.getAccountByCompanyAccountId()
								.getAccountId() == (long) slf
								.getAccountByCompanyAccountId().getAccountId()
						&& (long) combination.getAccountByCostcenterAccountId()
								.getAccountId() == (long) slf
								.getAccountByCostcenterAccountId()
								.getAccountId()
						&& (long) combination.getAccountByNaturalAccountId()
								.getAccountId() == (long) slf
								.getAccountByNaturalAccountId().getAccountId()
						&& (long) combination.getAccountByAnalysisAccountId()
								.getAccountId() == (long) slf
								.getAccountByAnalysisAccountId().getAccountId()
						&& (long) combination.getAccountByBuffer1AccountId()
								.getAccountId() == (long) slf
								.getAccountByBuffer1AccountId().getAccountId())
					combination.setCombination(slf);
			}
		}
		return combination;
	}

	private List<Combination> updateParentCombination(Combination combination,
			List<Combination> combinations) {
		combination.setCombinationId(null);
		if (null != combination.getCombinations()
				&& combination.getCombinations().size() > 0) {
			for (Combination cb : combination.getCombinations()) {
				cb.setCombinationId(null);
				cb.setCombination(combination);
				cb.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
				combinations.add(cb);
				updateParentCombination(cb, combinations);
			}
		}
		combination.setCombinations(null);
		return combinations;
	}

	private long getCombinationPasteAccount(Combination combination,
			int accessId) {
		if ((int) Segments.Company.getCode() == accessId)
			return combination.getAccountByCompanyAccountId().getAccountId();
		else if ((int) Segments.CostCenter.getCode() == accessId)
			return combination.getAccountByCostcenterAccountId().getAccountId();
		else if ((int) Segments.Natural.getCode() == accessId)
			return combination.getAccountByNaturalAccountId().getAccountId();
		else if ((int) Segments.Analysis.getCode() == accessId)
			return combination.getAccountByAnalysisAccountId().getAccountId();
		else if ((int) Segments.Buffer1.getCode() == accessId)
			return combination.getAccountByBuffer1AccountId().getAccountId();
		else if ((int) Segments.Buffer2.getCode() == accessId)
			return combination.getAccountByBuffer2AccountId().getAccountId();
		return 0;
	}

	private Long getCombinationKey(Combination combination) throws Exception {
		long key = 0l;
		if (null != combination.getAccountByCompanyAccountId())
			key = combination.getAccountByCompanyAccountId().getAccountId();
		if (null != combination.getAccountByCostcenterAccountId())
			key = combination.getAccountByCostcenterAccountId().getAccountId();
		if (null != combination.getAccountByNaturalAccountId())
			key = combination.getAccountByNaturalAccountId().getAccountId();
		if (null != combination.getAccountByAnalysisAccountId())
			key = combination.getAccountByAnalysisAccountId().getAccountId();
		if (null != combination.getAccountByBuffer1AccountId())
			key = combination.getAccountByBuffer1AccountId().getAccountId();
		if (null != combination.getAccountByBuffer2AccountId())
			key = combination.getAccountByBuffer2AccountId().getAccountId();
		return key;
	}

	public String saveCombination() {
		try {
			LOGGER.info("Module: Accounts : Method: saveCombination");
			Combination combination = getCombinationService().getCombination(
					combinationId);
			Account account = null;
			boolean flag = true;
			if (null != combination) {
				if (null == combination.getAccountByCostcenterAccountId()
						|| combination.getAccountByCostcenterAccountId()
								.equals("")) {
					account = new Account();
					account.setAccountId(accountId);
					combination.setAccountByCostcenterAccountId(account);
					List<Combination> tempCombination = getCombinationService()
							.validateByCostAccount(
									combination.getAccountByCompanyAccountId(),
									combination
											.getAccountByCostcenterAccountId());
					if (null != tempCombination && tempCombination.size() > 0) {
						flag = false;
					}
				} else if (null == combination.getAccountByNaturalAccountId()
						|| combination.getAccountByNaturalAccountId()
								.equals("")) {
					account = new Account();
					account.setAccountId(accountId);
					combination.setAccountByNaturalAccountId(account);
					List<Combination> tempCombination = getCombinationService()
							.validateByNaturalAccount(
									combination.getAccountByCompanyAccountId(),
									combination
											.getAccountByCostcenterAccountId(),
									combination.getAccountByNaturalAccountId());
					if (null != tempCombination && tempCombination.size() > 0) {
						flag = false;
					}
				} else if (null == combination.getAccountByAnalysisAccountId()
						|| combination.getAccountByAnalysisAccountId().equals(
								"")) {
					account = new Account();
					account.setAccountId(accountId);
					combination.setAccountByAnalysisAccountId(account);
					List<Combination> tempCombination = getCombinationService()
							.validateByAnalysisAccount(
									combination.getAccountByCompanyAccountId(),
									combination
											.getAccountByCostcenterAccountId(),
									combination.getAccountByNaturalAccountId(),
									combination.getAccountByAnalysisAccountId());
					if (null != tempCombination && tempCombination.size() > 0) {
						flag = false;
					}
				} else if (null == combination.getAccountByBuffer1AccountId()
						|| combination.getAccountByBuffer1AccountId()
								.equals("")) {
					account = new Account();
					account.setAccountId(accountId);
					combination.setAccountByBuffer1AccountId(account);
					List<Combination> tempCombination = getCombinationService()
							.validateByBuffer1Account(
									combination.getAccountByCompanyAccountId(),
									combination
											.getAccountByCostcenterAccountId(),
									combination.getAccountByNaturalAccountId(),
									combination.getAccountByAnalysisAccountId(),
									combination.getAccountByBuffer1AccountId());
					if (null != tempCombination && tempCombination.size() > 0) {
						flag = false;
					}
				} else if (null == combination.getAccountByBuffer2AccountId()
						|| combination.getAccountByBuffer2AccountId()
								.equals("")) {
					account = new Account();
					account.setAccountId(accountId);
					combination.setAccountByBuffer2AccountId(account);
					List<Combination> tempCombination = getCombinationService()
							.validateByBuffer2Account(
									combination.getAccountByCompanyAccountId(),
									combination
											.getAccountByCostcenterAccountId(),
									combination.getAccountByNaturalAccountId(),
									combination.getAccountByAnalysisAccountId(),
									combination.getAccountByBuffer1AccountId(),
									combination.getAccountByBuffer2AccountId());
					if (null != tempCombination && tempCombination.size() > 0) {
						flag = false;
					}
				}
			} else {
				account = new Account();
				account.setAccountId(accountId);
				Combination comb = new Combination();
				comb.setAccountByCompanyAccountId(account);
				comb.setCombinationId(null);
				List<Combination> tempCombination = getCombinationService()
						.validateByCompanyAccount(
								comb.getAccountByCompanyAccountId());
				if (null != tempCombination && tempCombination.size() > 0) {
					sqlReturnMessage = "Selected combination already exits.";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", sqlReturnMessage);
					return ERROR;
				} else {
					combinationBL.saveCombination(comb);
					sqlReturnMessage = "Success";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", sqlReturnMessage);
					LOGGER.info("Module: Accounts : Method: saveCombination Action Success");
					return SUCCESS;
				}
			}
			if (flag == true && null != combination) {
				combination.setCombinationId(null);
				combinationBL.saveCombination(combination);
				LOGGER.info("Module: Accounts : Method: saveCombination Action Success");
				sqlReturnMessage = "Success";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", sqlReturnMessage);
				return SUCCESS;
			} else {
				sqlReturnMessage = "Selected combination already exits.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", sqlReturnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			sqlReturnMessage = "FAILURE";
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveCombination: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public Combination createCostCenter(String accountDesc, Boolean isSystem)
			throws Exception {
		Account account = accountBL.createCostCenterAccount(accountDesc,
				isSystem);
		if (null != account && !account.equals("")) {
			Combination combination = this
					.getCombinationService()
					.getCombinationByCompanyAccount(account.getImplementation());
			if (null != combination && !combination.equals("")) {
				Combination costCombination = new Combination();
				costCombination.setAccountByCompanyAccountId(combination
						.getAccountByCompanyAccountId());
				costCombination.setAccountByCostcenterAccountId(account);
				combinationBL.saveSystemCombination(costCombination);
				return costCombination;
			}
			return null;
		} else {
			return null;
		}
	}

	public Combination createNaturalAccount(Combination combination,
			String accountDesc, Integer accountType, Boolean isSystem)
			throws Exception {
		Account account = accountBL.createNaturalAccount(accountDesc,
				accountType, isSystem);
		if (null != account && !account.equals("")) {
			combination = this.getCombinationService().getCombination(
					Integer.valueOf(combination.getCombinationId().toString()));
			if (null != combination && !combination.equals("")) {
				Combination natural = new Combination();
				natural.setAccountByCompanyAccountId(combination
						.getAccountByCompanyAccountId());
				natural.setAccountByCostcenterAccountId(combination
						.getAccountByCostcenterAccountId());
				natural.setAccountByNaturalAccountId(account);
				combinationBL.saveSystemCombination(natural);
				return natural;
			}
			return null;
		} else {
			return null;
		}
	}

	public Combination createAnalysisCombination(Combination combination,
			String accountDesc, Boolean isSystem) throws Exception {
		Account account = accountBL.createAnalyisAccount(accountDesc, isSystem,
				combination);
		if (null != account && !account.equals("")) {
			combination = this.getCombinationService().getCombination(
					combination.getCombinationId());
			if (null != combination && !combination.equals("")) {
				Combination newCombiantion = new Combination();
				newCombiantion.setAccountByCompanyAccountId(combination
						.getAccountByCompanyAccountId());
				newCombiantion.setAccountByCostcenterAccountId(combination
						.getAccountByCostcenterAccountId());
				newCombiantion.setAccountByNaturalAccountId(combination
						.getAccountByNaturalAccountId());
				newCombiantion.setAccountByAnalysisAccountId(account);
				combinationBL.saveSystemCombination(newCombiantion);
				return newCombiantion;
			}
			return null;
		} else {
			return null;
		}
	}

	// Fetch all combination and insert into ledger
	public String resetLedger() {
		try {
			LOGGER.info("Module: Accounts : Method: resetLedger");
			List<Ledger> ledgers = new ArrayList<Ledger>();
			Ledger ledger = null;
			getImplementId();
			List<Combination> combinations = combinationBL
					.getCombinationService().getTransactionCombination(
							implementation);
			for (Combination combination : combinations) {
				ledger = new Ledger();
				ledger.setCombination(combination);
				ledger.setSide(true);
				ledgers.add(ledger);
			}
			combinationBL.getCombinationService().saveLedger(ledgers);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Internal error.");
			return ERROR;
		}
	}

	// Show Account Balance
	public String showAccountBalance() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAccountBalance");
			Ledger ledger = combinationBL.getCombinationService()
					.getLederCombinationById(combinationId);
			if (null != ledger) {
				if (null == ledger.getBalance()
						|| (double) ledger.getBalance() == 0)
					accountBalance = "0.00";
				else if (!ledger.getSide())
					accountBalance = "-".concat(AIOSCommons.formatAmount(ledger
							.getBalance()));
				else
					accountBalance = AIOSCommons.formatAmount(ledger
							.getBalance());
			} else
				accountBalance = "0.00";
			LOGGER.info("Module: Accounts : Method: showAccountBalance Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			accountBalance = "0.00";
			return ERROR;
		}
	}

	public String deleteLedger() {
		try {
			Map<Long, List<Ledger>> ledgerMap = new HashMap<Long, List<Ledger>>();
			getImplementId();
			List<Ledger> ledgers = combinationBL.getCombinationService()
					.getAllLedgers(implementation);
			if (null != ledgers && ledgers.size() > 0) {
				List<Ledger> tempLedgers = null;
				for (Ledger ledger : ledgers) {
					tempLedgers = new ArrayList<Ledger>();
					if (ledgerMap.containsKey(ledger.getCombination()
							.getCombinationId())) {
						tempLedgers.addAll(ledgerMap.get(ledger
								.getCombination().getCombinationId()));
					}
					tempLedgers.add(ledger);
					ledgerMap.put(ledger.getCombination().getCombinationId(),
							tempLedgers);
				}
				List<Ledger> deleteLedgers = new ArrayList<Ledger>();
				for (Entry<Long, List<Ledger>> entry : ledgerMap.entrySet()) {
					tempLedgers = new ArrayList<Ledger>(entry.getValue());
					if (tempLedgers.size() > 1) {
						for (Ledger ledger : tempLedgers) {
							if (null == ledger.getBalance()) {
								deleteLedgers.add(ledger);
							}
						}
					}
				}
				combinationBL.getCombinationService().deleteLedger(
						deleteLedgers);
			}
			LOGGER.info("Module: Accounts : Method: deleteLedger Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters&setters
	public int getCommonCode() {
		return commonCode;
	}

	public void setCommonCode(int commonCode) {
		this.commonCode = commonCode;
	}

	public int getCodeId() {
		return codeId;
	}

	public void setCodeId(int codeId) {
		this.codeId = codeId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyCodeDesc() {
		return companyCodeDesc;
	}

	public void setCompanyCodeDesc(String companyCodeDesc) {
		this.companyCodeDesc = companyCodeDesc;
	}

	public String getCostCode() {
		return costCode;
	}

	public void setCostCode(String costCode) {
		this.costCode = costCode;
	}

	public String getCostCodeDesc() {
		return costCodeDesc;
	}

	public void setCostCodeDesc(String costCodeDesc) {
		this.costCodeDesc = costCodeDesc;
	}

	public String getNaturalCode() {
		return naturalCode;
	}

	public void setNaturalCode(String naturalCode) {
		this.naturalCode = naturalCode;
	}

	public String getNaturalCodeDesc() {
		return naturalCodeDesc;
	}

	public void setNaturalCodeDesc(String naturalCodeDesc) {
		this.naturalCodeDesc = naturalCodeDesc;
	}

	public String getAnalyisCode() {
		return analyisCode;
	}

	public void setAnalyisCode(String analyisCode) {
		this.analyisCode = analyisCode;
	}

	public String getAnalyisCodeDesc() {
		return analyisCodeDesc;
	}

	public void setAnalyisCodeDesc(String analyisCodeDesc) {
		this.analyisCodeDesc = analyisCodeDesc;
	}

	public String getBufferCode1() {
		return bufferCode1;
	}

	public void setBufferCode1(String bufferCode1) {
		this.bufferCode1 = bufferCode1;
	}

	public String getBufferCode1Desc() {
		return bufferCode1Desc;
	}

	public void setBufferCode1Desc(String bufferCode1Desc) {
		this.bufferCode1Desc = bufferCode1Desc;
	}

	public String getBufferCode2() {
		return bufferCode2;
	}

	public void setBufferCode2(String bufferCode2) {
		this.bufferCode2 = bufferCode2;
	}

	public String getBufferCode2Desc() {
		return bufferCode2Desc;
	}

	public void setBufferCode2Desc(String bufferCode2Desc) {
		this.bufferCode2Desc = bufferCode2Desc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCoaHeaderId() {
		return coaHeaderId;
	}

	public void setCoaHeaderId(String coaHeaderId) {
		this.coaHeaderId = coaHeaderId;
	}

	public String getCoaName() {
		return coaName;
	}

	public void setCoaName(String coaName) {
		this.coaName = coaName;
	}

	public String getTrnValue() {
		return trnValue;
	}

	public void setTrnValue(String trnValue) {
		this.trnValue = trnValue;
	}

	public String getTempLineId() {
		return tempLineId;
	}

	public void setTempLineId(String tempLineId) {
		this.tempLineId = tempLineId;
	}

	public int getActualLineId() {
		return actualLineId;
	}

	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getCodeCombinations() {
		return codeCombinations;
	}

	public void setCodeCombinations(List<String> codeCombinations) {
		this.codeCombinations = codeCombinations;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public GLCombinationService getCombinationService() {
		return combinationService;
	}

	public void setCombinationService(GLCombinationService combinationService) {
		this.combinationService = combinationService;
	}

	public GLCombinationMasterTO getCombinationTo() {
		return combinationTo;
	}

	public void setCombinationTo(GLCombinationMasterTO combinationTo) {
		this.combinationTo = combinationTo;
	}

	public List<GLCombinationMasterTO> getCombinationList() {
		return combinationList;
	}

	public void setCombinationList(List<GLCombinationMasterTO> combinationList) {
		this.combinationList = combinationList;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getCostAccountId() {
		return costAccountId;
	}

	public void setCostAccountId(long costAccountId) {
		this.costAccountId = costAccountId;
	}

	public long getNaturalAccountId() {
		return naturalAccountId;
	}

	public void setNaturalAccountId(long naturalAccountId) {
		this.naturalAccountId = naturalAccountId;
	}

	public long getAnalyisAccountId() {
		return analyisAccountId;
	}

	public void setAnalyisAccountId(long analyisAccountId) {
		this.analyisAccountId = analyisAccountId;
	}

	public long getBufferAccountId1() {
		return bufferAccountId1;
	}

	public void setBufferAccountId1(long bufferAccountId1) {
		this.bufferAccountId1 = bufferAccountId1;
	}

	public long getBufferAccountId2() {
		return bufferAccountId2;
	}

	public void setBufferAccountId2(long bufferAccountId2) {
		this.bufferAccountId2 = bufferAccountId2;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public int getGetFlag() {
		return getFlag;
	}

	public void setGetFlag(int getFlag) {
		this.getFlag = getFlag;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public void setAccountCodes(String accountCodes) {
		this.accountCodes = accountCodes;
	}

	public long getRevenueId() {
		return revenueId;
	}

	public void setRevenueId(long revenueId) {
		this.revenueId = revenueId;
	}

	public long getDepositAccountId() {
		return depositAccountId;
	}

	public void setDepositAccountId(long depositAccountId) {
		this.depositAccountId = depositAccountId;
	}

	public long getFeeAccountId() {
		return feeAccountId;
	}

	public void setFeeAccountId(int feeAccountId) {
		this.feeAccountId = feeAccountId;
	}

	public long getOtherChargesId() {
		return otherChargesId;
	}

	public void setOtherChargesId(long otherChargesId) {
		this.otherChargesId = otherChargesId;
	}

	public long getReleaseDamageId() {
		return releaseDamageId;
	}

	public void setReleaseDamageId(long releaseDamageId) {
		this.releaseDamageId = releaseDamageId;
	}

	public long getUnearnedRevenue() {
		return unearnedRevenue;
	}

	public void setUnearnedRevenue(long unearnedRevenue) {
		this.unearnedRevenue = unearnedRevenue;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getPdcReceived() {
		return pdcReceived;
	}

	public void setPdcReceived(long pdcReceived) {
		this.pdcReceived = pdcReceived;
	}

	public long getOwnersEquityId() {
		return ownersEquityId;
	}

	public void setOwnersEquityId(long ownersEquityId) {
		this.ownersEquityId = ownersEquityId;
	}

	public long getPdcIssued() {
		return pdcIssued;
	}

	public void setPdcIssued(long pdcIssued) {
		this.pdcIssued = pdcIssued;
	}

	public String getCombinationTemplateIds() {
		return combinationTemplateIds;
	}

	public void setCombinationTemplateIds(String combinationTemplateIds) {
		this.combinationTemplateIds = combinationTemplateIds;
	}

	public long getClearingAccount() {
		return clearingAccount;
	}

	public void setClearingAccount(long clearingAccount) {
		this.clearingAccount = clearingAccount;
	}

	public String getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

	public void setFeeAccountId(long feeAccountId) {
		this.feeAccountId = feeAccountId;
	}

	public long getSalesDiscountControlId() {
		return salesDiscountControlId;
	}

	public void setSalesDiscountControlId(long salesDiscountControlId) {
		this.salesDiscountControlId = salesDiscountControlId;
	}

	public long getPurchaseDiscountControlId() {
		return purchaseDiscountControlId;
	}

	public void setPurchaseDiscountControlId(long purchaseDiscountControlId) {
		this.purchaseDiscountControlId = purchaseDiscountControlId;
	}

	public long getSalesPenaltyControlId() {
		return salesPenaltyControlId;
	}

	public void setSalesPenaltyControlId(long salesPenaltyControlId) {
		this.salesPenaltyControlId = salesPenaltyControlId;
	}

	public long getPurchasePenaltyControlId() {
		return purchasePenaltyControlId;
	}

	public void setPurchasePenaltyControlId(long purchasePenaltyControlId) {
		this.purchasePenaltyControlId = purchasePenaltyControlId;
	}

	public long getAccountsReceivableControlId() {
		return accountsReceivableControlId;
	}

	public void setAccountsReceivableControlId(long accountsReceivableControlId) {
		this.accountsReceivableControlId = accountsReceivableControlId;
	}

	public long getProductionExpenseControlId() {
		return productionExpenseControlId;
	}

	public void setProductionExpenseControlId(long productionExpenseControlId) {
		this.productionExpenseControlId = productionExpenseControlId;
	}

	public long getProductionRevenueControlId() {
		return productionRevenueControlId;
	}

	public void setProductionRevenueControlId(long productionRevenueControlId) {
		this.productionRevenueControlId = productionRevenueControlId;
	}

	public long getAccumulatedDeprControlId() {
		return accumulatedDeprControlId;
	}

	public void setAccumulatedDeprControlId(long accumulatedDeprControlId) {
		this.accumulatedDeprControlId = accumulatedDeprControlId;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public long getAccountsPayableId() {
		return accountsPayableId;
	}

	public void setAccountsPayableId(long accountsPayableId) {
		this.accountsPayableId = accountsPayableId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Integer getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Integer processFlag) {
		this.processFlag = processFlag;
	}

	public AccountBL getAccountBL() {
		return accountBL;
	}

	public void setAccountBL(AccountBL accountBL) {
		this.accountBL = accountBL;
	}

	public long getCashAccountDPControlId() {
		return cashAccountDPControlId;
	}

	public void setCashAccountDPControlId(long cashAccountDPControlId) {
		this.cashAccountDPControlId = cashAccountDPControlId;
	}

	public long getPettyCashControlId() {
		return pettyCashControlId;
	}

	public void setPettyCashControlId(long pettyCashControlId) {
		this.pettyCashControlId = pettyCashControlId;
	}

	public long getBankClearingControlId() {
		return bankClearingControlId;
	}

	public void setBankClearingControlId(long bankClearingControlId) {
		this.bankClearingControlId = bankClearingControlId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public int getAccessId() {
		return accessId;
	}

	public void setAccessId(int accessId) {
		this.accessId = accessId;
	}

	public long getCombinationCopyId() {
		return combinationCopyId;
	}

	public void setCombinationCopyId(long combinationCopyId) {
		this.combinationCopyId = combinationCopyId;
	}

	public long getCopySegmentId() {
		return copySegmentId;
	}

	public void setCopySegmentId(long copySegmentId) {
		this.copySegmentId = copySegmentId;
	}
}
