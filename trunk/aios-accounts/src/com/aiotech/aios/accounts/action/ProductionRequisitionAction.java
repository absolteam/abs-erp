package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessProduction;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialIdleMixDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionRequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.ProductionRequisitionBL;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionReqDetailStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProductionRequisitionStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ProductionRequisitionAction extends ActionSupport {

	private static final long serialVersionUID = -3523076273867888127L;

	private static final Logger log = Logger
			.getLogger(ProductionRequisitionAction.class);

	private ProductionRequisitionBL productionRequisitionBL;
	private long productionRequisitionId;
	private long personId;
	private int rowId;
	private byte requisitionStatus;
	private String referenceNumber;
	private String requistionDate;
	private String description;
	private String personName;
	private String requisitionDetails;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess Success");
		return SUCCESS;
	}

	public String showAllProductionRequisitions() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductionRequisitions");
			aaData = productionRequisitionBL.getAllProductionRequisitions();
			log.info("Module: Accounts : Method: showAllProductionRequisitions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductionRequisitions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductionRequisitionsList() {
		try {
			log.info("Inside Module: Accounts : Method: showProductionRequisitionsList");
			aaData = productionRequisitionBL.getProductionRequisitionsList();
			log.info("Module: Accounts : Method: showProductionRequisitionsList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductionRequisitionsList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductionRequisitionEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductionRequisitionEntry");
			ProductionRequisitionVO productionRequisitionVO = null;
			if (productionRequisitionId > 0) {
				productionRequisitionVO = new ProductionRequisitionVO();
				ProductionRequisition productionRequisition = productionRequisitionBL
						.getProductionRequisitionService()
						.getProductionRequisitionById(productionRequisitionId);
				BeanUtils.copyProperties(productionRequisitionVO,
						productionRequisition);
				productionRequisitionVO.setRequisitionDateStr(DateFormat
						.convertDateToString(productionRequisition
								.getRequisitionDate().toString()));
				List<ProductionRequisitionDetailVO> productionRequisitionDetailVOs = new ArrayList<ProductionRequisitionDetailVO>();
				ProductionRequisitionDetailVO productionRequisitionDetailVO = null;
				for (ProductionRequisitionDetail productionRequisitionDetail : productionRequisition
						.getProductionRequisitionDetails()) {
					productionRequisitionDetailVO = new ProductionRequisitionDetailVO();
					BeanUtils.copyProperties(productionRequisitionDetailVO,
							productionRequisitionDetail);
					if (null != productionRequisitionDetail.getShelf())
						productionRequisitionDetailVO
								.setStoreName(productionRequisitionDetail
										.getShelf().getShelf().getAisle()
										.getStore().getStoreName()
										+ ">>"
										+ productionRequisitionDetail
												.getShelf().getShelf()
												.getAisle().getSectionName()
										+ ">>"
										+ productionRequisitionDetail
												.getShelf().getShelf()
												.getName()
										+ ">>"
										+ productionRequisitionDetail
												.getShelf().getName());
					if (null != productionRequisitionDetailVO
							.getProductPackageDetail()) {
						productionRequisitionDetailVO
								.setPackageDetailId(productionRequisitionDetail
										.getProductPackageDetail()
										.getProductPackageDetailId());
						productionRequisitionDetailVO
								.setBaseUnitName(productionRequisitionDetail
										.getProduct()
										.getLookupDetailByProductUnit()
										.getDisplayName());
					} else {
						productionRequisitionDetailVO
								.setPackageUnit(productionRequisitionDetail
										.getQuantity());
						productionRequisitionDetailVO.setPackageDetailId(-1l);
					}
					productionRequisitionDetailVO.setBaseQuantity(AIOSCommons
							.convertExponential(productionRequisitionDetail
									.getQuantity()));
					productionRequisitionDetailVO
							.setProductPackageVOs(productionRequisitionBL
									.getMaterialIdleMixBL()
									.getPackageConversionBL()
									.getProductPackagingDetail(
											productionRequisitionDetail
													.getProduct()
													.getProductId()));
					productionRequisitionDetailVOs
							.add(productionRequisitionDetailVO);
				}
				Collections.sort(productionRequisitionDetailVOs,
						new Comparator<ProductionRequisitionDetailVO>() {
							public int compare(
									ProductionRequisitionDetailVO o1,
									ProductionRequisitionDetailVO o2) {
								return o1
										.getProductionRequisitionDetailId()
										.compareTo(
												o2.getProductionRequisitionDetailId());
							}
						});
				productionRequisitionVO
						.setProductionRequisitionDetailVOs(productionRequisitionDetailVOs);
			} else {
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				User user = (User) sessionObj.get("USER");
				referenceNumber = SystemBL.getReferenceStamp(
						ProductionRequisition.class.getName(),
						productionRequisitionBL.getImplementation());
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION_REQUISITION", productionRequisitionVO);
			log.info("Module: Accounts : Method: showProductionRequisitionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductionRequisitionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String productionRequisitionPrint() {
		try {
			log.info("Inside Module: Accounts : Method: productionRequisitionPrint");
			ProductionRequisitionVO productionRequisitionVO = null;
			productionRequisitionVO = new ProductionRequisitionVO();
			ProductionRequisition productionRequisition = productionRequisitionBL
					.getProductionRequisitionService()
					.getProductionRequisitionById(productionRequisitionId);
			BeanUtils.copyProperties(productionRequisitionVO,
					productionRequisition);
			productionRequisitionVO.setRequisitionDateStr(DateFormat
					.convertDateToString(productionRequisition
							.getRequisitionDate().toString()));
			List<ProductionRequisitionDetailVO> productionRequisitionDetailVOs = new ArrayList<ProductionRequisitionDetailVO>();
			ProductionRequisitionDetailVO productionRequisitionDetailVO = null;
			for (ProductionRequisitionDetail productionRequisitionDetail : productionRequisition
					.getProductionRequisitionDetails()) {
				productionRequisitionDetailVO = new ProductionRequisitionDetailVO();
				BeanUtils.copyProperties(productionRequisitionDetailVO,
						productionRequisitionDetail);
				if (null != productionRequisitionDetail.getShelf())
					productionRequisitionDetailVO
							.setStoreName(productionRequisitionDetail
									.getShelf().getShelf().getAisle()
									.getStore().getStoreName()
									+ ">>"
									+ productionRequisitionDetail.getShelf()
											.getShelf().getAisle()
											.getSectionName()
									+ ">>"
									+ productionRequisitionDetail.getShelf()
											.getShelf().getName()
									+ ">>"
									+ productionRequisitionDetail.getShelf()
											.getName());
				productionRequisitionDetailVOs
						.add(productionRequisitionDetailVO);
			}
			Collections.sort(productionRequisitionDetailVOs,
					new Comparator<ProductionRequisitionDetailVO>() {
						public int compare(ProductionRequisitionDetailVO o1,
								ProductionRequisitionDetailVO o2) {
							return o1
									.getProductionRequisitionDetailId()
									.compareTo(
											o2.getProductionRequisitionDetailId());
						}
					});
			productionRequisitionVO
					.setProductionRequisitionDetailVOs(productionRequisitionDetailVOs);
			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION_REQUISITION", productionRequisitionVO);
			log.info("Module: Accounts : Method: productionRequisitionPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: productionRequisitionPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductionRequisitionDetail() {
		try {
			log.info("Inside Module: Accounts : Method: showProductionRequisitionDetail");
			ProductionRequisition productionRequisition = productionRequisitionBL
					.getProductionRequisitionService()
					.getProductionRequisitionById(productionRequisitionId);
			List<ProductionRequisitionDetailVO> productionRequisitionDetailVOs = new ArrayList<ProductionRequisitionDetailVO>();
			ProductionRequisitionDetailVO productionRequisitionDetailVO = null;
			List<MaterialIdleMixDetailVO> materialIdleMixDetailVOs = null;
			MaterialIdleMixDetailVO materialIdleMixDetailVO = null;
			MaterialIdleMix materialIdleMix = null;
			List<WorkinProcessProduction> workinProcessProductions = null;
			double producedQuantity = 0;
			for (ProductionRequisitionDetail productionRequisitionDetail : productionRequisition
					.getProductionRequisitionDetails()) {
				producedQuantity = 0;
				workinProcessProductions = productionRequisitionBL
						.getWorkinProcessService()
						.getWorkinProductionByRequsitionDetailId(
								productionRequisitionDetail
										.getProductionRequisitionDetailId());
				if (null != workinProcessProductions
						&& workinProcessProductions.size() > 0) {
					for (WorkinProcessProduction workinProcessProduction : workinProcessProductions)
						producedQuantity += workinProcessProduction
								.getProductionQuantity();
				}
				if (producedQuantity < productionRequisitionDetail
						.getQuantity()) {
					productionRequisitionDetailVO = new ProductionRequisitionDetailVO();
					BeanUtils.copyProperties(productionRequisitionDetailVO,
							productionRequisitionDetail);
					productionRequisitionDetailVO
							.setProcessMode(ProductionReqDetailStatus.get(
									productionRequisitionDetail.getStatus())
									.name());
					productionRequisitionDetailVO
							.setQuantity(productionRequisitionDetail
									.getQuantity() - producedQuantity);
					if (null != productionRequisitionDetail
							.getProductPackageDetail())
						productionRequisitionDetailVO
								.setPackageUnit(productionRequisitionBL
										.getMaterialIdleMixBL()
										.getPackageConversionBL()
										.getPackageConversionQuantity(
												productionRequisitionDetail
														.getProductPackageDetail()
														.getProductPackageDetailId(),
												productionRequisitionDetailVO
														.getQuantity()));
					else
						productionRequisitionDetailVO
								.setPackageUnit(productionRequisitionDetailVO
										.getQuantity());
					if (null != productionRequisitionDetail.getShelf())
						productionRequisitionDetailVO
								.setStoreName(productionRequisitionDetail
										.getShelf().getShelf().getAisle()
										.getStore().getStoreName()
										+ ">>"
										+ productionRequisitionDetail
												.getShelf().getShelf()
												.getAisle().getSectionName()
										+ ">>"
										+ productionRequisitionDetail
												.getShelf().getShelf()
												.getName()
										+ ">>"
										+ productionRequisitionDetail
												.getShelf().getName());
					materialIdleMix = productionRequisitionBL
							.getMaterialIdleMixBL()
							.getMaterialIdleMixService()
							.getMaterialIdleMixbyProduct(
									productionRequisitionDetail.getProduct()
											.getProductId());
					if (null != materialIdleMix) {
						materialIdleMixDetailVOs = new ArrayList<MaterialIdleMixDetailVO>();
						List<StockVO> actualStockVOs = null;
						for (MaterialIdleMixDetail materialIdleMixDetail : materialIdleMix
								.getMaterialIdleMixDetails()) {
							materialIdleMixDetailVO = new MaterialIdleMixDetailVO();
							BeanUtils.copyProperties(materialIdleMixDetailVO,
									materialIdleMixDetail);
							materialIdleMixDetailVO
									.setProductionQuantity(AIOSCommons.roundDecimals(materialIdleMixDetail
											.getMaterialQuantity()
											* productionRequisitionDetailVO
													.getQuantity()));
							materialIdleMixDetailVO
									.setProductPackageVOs(productionRequisitionBL
											.getMaterialIdleMixBL()
											.getPackageConversionBL()
											.getProductPackagingDetail(
													materialIdleMixDetail
															.getProduct()
															.getProductId()));
							if (null != materialIdleMixDetail
									.getProductPackageDetail()) {
								materialIdleMixDetailVO
										.setPackageDetailId(materialIdleMixDetail
												.getProductPackageDetail()
												.getProductPackageDetailId());
								materialIdleMixDetailVO
										.setBaseUnitName(materialIdleMixDetail
												.getProduct()
												.getLookupDetailByProductUnit()
												.getDisplayName());
							} else {
								materialIdleMixDetailVO.setPackageDetailId(-1l);
								materialIdleMixDetailVO
										.setPackageUnit(materialIdleMixDetailVO
												.getProductionQuantity());
							}
							materialIdleMixDetailVO
									.setInsufficientBalance(true);
							List<StockVO> stockVOs = productionRequisitionBL
									.getMaterialIdleMixBL()
									.getStockBL()
									.getProductStockFullDetails(
											materialIdleMixDetailVO
													.getProduct()
													.getProductId());
							if (null != stockVOs && stockVOs.size() > 0) {
								actualStockVOs = new ArrayList<StockVO>();
								for (StockVO stockVO : stockVOs) {
									if (null != stockVO.getQuantity()
											&& (double) stockVO.getQuantity() > 0) {
										actualStockVOs.add(stockVO);
									}
								}
								if (null != actualStockVOs
										&& actualStockVOs.size() > 0
										&& actualStockVOs.size() == 1) {
									materialIdleMixDetailVO
											.setShelfId(null != actualStockVOs
													.get(0).getShelf() ? actualStockVOs
													.get(0).getShelf()
													.getShelfId()
													: null);
									materialIdleMixDetailVO
											.setStoreName(actualStockVOs.get(0)
													.getStoreName());
									if ((double) actualStockVOs.get(0)
											.getAvailableQuantity() < (double) materialIdleMixDetailVO
											.getProductionQuantity())
										materialIdleMixDetailVO
												.setInsufficientBalance(false);
								}
							}
							materialIdleMixDetailVOs
									.add(materialIdleMixDetailVO);
						}
						productionRequisitionDetailVO
								.setMaterialIdleMixDetailVOs(materialIdleMixDetailVOs);
					}
					productionRequisitionDetailVOs
							.add(productionRequisitionDetailVO);
				}
			}
			Collections.sort(productionRequisitionDetailVOs,
					new Comparator<ProductionRequisitionDetailVO>() {
						public int compare(ProductionRequisitionDetailVO o1,
								ProductionRequisitionDetailVO o2) {
							return o1
									.getProductionRequisitionDetailId()
									.compareTo(
											o2.getProductionRequisitionDetailId());
						}
					});
			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION_REQUISITION_DETAIL",
					productionRequisitionDetailVOs);
			log.info("Module: Accounts : Method: showProductionRequisitionDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductionRequisitionDetail Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveProductionRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductionRequisition");
			ProductionRequisition productionRequisition = new ProductionRequisition();
			productionRequisition.setRequisitionDate(DateFormat
					.convertStringToDate(requistionDate));
			productionRequisition.setDescription(description);
			Person person = new Person();
			person.setPersonId(personId);
			productionRequisition.setPerson(person);
			productionRequisition
					.setRequisitionStatus(ProductionRequisitionStatus.Open
							.getCode());
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(requisitionDetails);
			JSONArray object = (JSONArray) receiveObject;
			ProductionRequisitionDetail productionRequisitionDetail = null;
			Product product = null;
			Shelf shelf = null;
			List<ProductionRequisitionDetail> productionRequisitionDetails = new ArrayList<ProductionRequisitionDetail>();
			List<ProductionRequisitionDetail> deletedRequisitionDetails = null;
			ProductPackageDetail productPackageDetail = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("requisitionDetails");
				for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					productionRequisitionDetail = new ProductionRequisitionDetail();
					product = new Product();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"productId").toString()));
					productionRequisitionDetail.setProduct(product);
					productionRequisitionDetail.setQuantity(Double
							.parseDouble(jsonObjectDetail.get("quantity")
									.toString()));
					productionRequisitionDetail
							.setDescription(null != jsonObjectDetail
									.get("description") ? jsonObjectDetail.get(
									"description").toString() : null);
					if (null != jsonObjectDetail.get("shelfId")
							&& Integer.parseInt(jsonObjectDetail.get("shelfId")
									.toString()) > 0) {
						shelf = new Shelf();
						shelf.setShelfId(Integer.parseInt(jsonObjectDetail.get(
								"shelfId").toString()));
						productionRequisitionDetail.setShelf(shelf);
					}
					productionRequisitionDetail
							.setProductionRequisitionDetailId((null != jsonObjectDetail
									.get("productionRequisitionDetailId") && Long
									.parseLong(jsonObjectDetail.get(
											"productionRequisitionDetailId")
											.toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"productionRequisitionDetailId")
											.toString()) : null);
					if ((null != jsonObjectDetail.get("packageType") && Long
							.parseLong(jsonObjectDetail.get("packageType")
									.toString()) > 0)) {
						productPackageDetail = new ProductPackageDetail();
						productPackageDetail.setProductPackageDetailId(Long
								.parseLong(jsonObjectDetail.get("packageType")
										.toString()));
						productionRequisitionDetail
								.setProductPackageDetail(productPackageDetail);
					}
					productionRequisitionDetail.setPackageUnit(Double
							.parseDouble(jsonObjectDetail.get("packageUnit")
									.toString()));
					productionRequisitionDetail
							.setStatus(ProductionReqDetailStatus.Open.getCode());
				}
				productionRequisitionDetails.add(productionRequisitionDetail);
			}

			if (productionRequisitionId > 0) {
				productionRequisition
						.setProductionRequisitionId(productionRequisitionId);
				productionRequisition.setReferenceNumber(referenceNumber);
				deletedRequisitionDetails = getUserDeletedRequisitionDetails(productionRequisitionDetails);
			} else
				productionRequisition.setReferenceNumber(SystemBL
						.getReferenceStamp(
								ProductionRequisition.class.getName(),
								productionRequisitionBL.getImplementation()));
			productionRequisitionBL.saveProductionRequisition(
					productionRequisition, productionRequisitionDetails,
					deletedRequisitionDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: showProductionRequisitionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductionRequisitionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteProductionRequisition() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProductionRequisition");
			ProductionRequisition productionRequisition = productionRequisitionBL
					.getProductionRequisitionService()
					.getProductionRequisitionById(productionRequisitionId);
			productionRequisitionBL.deleteProductionRequisition(
					productionRequisition,
					new ArrayList<ProductionRequisitionDetail>(
							productionRequisition
									.getProductionRequisitionDetails()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProductionRequisition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: deleteProductionRequisition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<ProductionRequisitionDetail> getUserDeletedRequisitionDetails(
			List<ProductionRequisitionDetail> productionRequisitionDetails)
			throws Exception {
		List<ProductionRequisitionDetail> existingRequisitionDetails = productionRequisitionBL
				.getProductionRequisitionService()
				.getProductionRequistionDetail(productionRequisitionId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProductionRequisitionDetail> hashRequisitionDetail = new HashMap<Long, ProductionRequisitionDetail>();
		if (null != hashRequisitionDetail && hashRequisitionDetail.size() > 0) {
			for (ProductionRequisitionDetail list : existingRequisitionDetails) {
				listOne.add(list.getProductionRequisitionDetailId());
				hashRequisitionDetail.put(
						list.getProductionRequisitionDetailId(), list);
			}
			if (null != hashRequisitionDetail
					&& hashRequisitionDetail.size() > 0) {
				for (ProductionRequisitionDetail list : productionRequisitionDetails) {
					listTwo.add(list.getProductionRequisitionDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProductionRequisitionDetail> deletedRequisitionDetails = null;
		if (null != different && different.size() > 0) {
			ProductionRequisitionDetail tempRequisitionDetail = null;
			deletedRequisitionDetails = new ArrayList<ProductionRequisitionDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempRequisitionDetail = new ProductionRequisitionDetail();
					tempRequisitionDetail = hashRequisitionDetail.get(list);
					deletedRequisitionDetails.add(tempRequisitionDetail);
				}
			}
		}
		return deletedRequisitionDetails;
	}

	// Getters & Setters
	public ProductionRequisitionBL getProductionRequisitionBL() {
		return productionRequisitionBL;
	}

	public void setProductionRequisitionBL(
			ProductionRequisitionBL productionRequisitionBL) {
		this.productionRequisitionBL = productionRequisitionBL;
	}

	public long getProductionRequisitionId() {
		return productionRequisitionId;
	}

	public void setProductionRequisitionId(long productionRequisitionId) {
		this.productionRequisitionId = productionRequisitionId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRequistionDate() {
		return requistionDate;
	}

	public void setRequistionDate(String requistionDate) {
		this.requistionDate = requistionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getRequisitionDetails() {
		return requisitionDetails;
	}

	public void setRequisitionDetails(String requisitionDetails) {
		this.requisitionDetails = requisitionDetails;
	}

	public byte getRequisitionStatus() {
		return requisitionStatus;
	}

	public void setRequisitionStatus(byte requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

}
