package com.aiotech.aios.accounts.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DebitDetail;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.DebitBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.opensymphony.xwork2.ActionSupport;

public class DebitAction extends ActionSupport {

	private static final long serialVersionUID = 748396441642955276L;

	private String debitNumber;
	private long returnId;
	private String date;
	private long debitId;
	private String debitDetails;
	private String description;
	private String returnMessage;
	private boolean exchangeFlag;
	private StockVO stockVO;
	private List<StockVO> barcodeDetails = new ArrayList<StockVO>();
	private List<StockVO> stocks = new ArrayList<StockVO>();
	private DebitBL debitBL;

	private static final Logger LOGGER = LogManager
			.getLogger(DebitAction.class);

	public String returnSuccess() {
		LOGGER.info("Inside Account method: returnSuccess");
		return SUCCESS;
	}

	public String debitJsonList() {
		LOGGER.info("Module: Accounts : Method: debitJsonList");
		try {
			JSONObject jsonResponse = debitBL
					.getAllDebits(getImplementationId());
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: debitJsonList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: debitJsonList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showDebitEntry() {
		LOGGER.info("Module: Accounts : Method: showDebitEntry");
		try {
			debitNumber = getNumber();
			ServletActionContext.getRequest().setAttribute("DEBIT_NUMBER",
					debitNumber);
			LOGGER.info("Module: Accounts : Method: showDebitEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showDebitEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveDebitEntry() {
		LOGGER.info("Module: Accounts : Method: saveDebitEntry");
		try {
			Debit debit = new Debit();
			if (debitId > 0) {
				debit.setDebitId(debitId);
				debit.setDebitNumber(debitNumber);
			} else {
				debit.setDebitNumber(getNumber());
			}
			debit.setDate(DateFormat.convertStringToDate(date));
			GoodsReturn goodsReturn = debitBL.getPurchaseBL()
					.getGoodsReturnService().getGoodsReturnSupplier(returnId);
			debit.setDescription(description);
			debit.setGoodsReturn(goodsReturn);
			debit.setItemExchange(exchangeFlag ? exchangeFlag : false);
			debit.setImplementation(getImplementationId());
			List<DebitDetail> detailDetail = getDebitLines();
			debitBL.saveDebit(debit, detailDetail);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: showDebitEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveDebitEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteDebitEntry() {
		LOGGER.info("Module: Accounts : Method: deleteDebitEntry");
		try {
			Debit debit = debitBL.getDebitService()
					.getDebitNotesById(debitId);
			if(!debit.getItemExchange()){
				BankReceipts bankReceipts = debitBL.getBankReceiptsService().getBankReceiptsByRecordIdUsecase(debitId, Debit.class.getSimpleName());
				if(null!=bankReceipts){
					returnMessage = "Record Delete failure, Receipts done for this Debit Note"; 
					LOGGER.info("Module: Accounts : Method: deleteDebitEntry: Action Success");
					return SUCCESS;
				}
			}
			debitBL.deleteDebits(debit,
					new ArrayList<DebitDetail>(debit.getDebitDetails()));
			returnMessage = "SUCCESS"; 
			LOGGER.info("Module: Accounts : Method: deleteDebitEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Failure";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteDebitEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}
	
	public String debitNotesPDFPrintBarcode() {
		try {
			LOG.info("Inside Module: Accounts : Method: debitNotesPDFPrintBarcode");
			Debit debit = debitBL.getDebitService().getDebitNotesById(debitId); 
			stocks = new ArrayList<StockVO>();
			barcodeDetails = new ArrayList<StockVO>();
			stockVO = new StockVO();
			List<StockVO> tempStocks = new ArrayList<StockVO>();
			Shelf shelf = null;
			for (DebitDetail debitDetail : debit.getDebitDetails()) {
				if (null != debitDetail.getGoodsReturnDetail().getReceiveDetail().getShelf()) {
					shelf = debitBL.getPurchaseBL()
							.getStockBL()
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									debitDetail.getGoodsReturnDetail().getReceiveDetail().getShelf().getShelfId());
					StockVO storeDetail = new StockVO();
					storeDetail.setStoreName(shelf.getShelf().getAisle()
							.getStore().getStoreName()
							+ "->");
					storeDetail.setAisleName(shelf.getShelf().getAisle()
							.getSectionName()
							+ "->");
					storeDetail.setRackName(shelf.getShelf().getName() + "->");
					storeDetail.setShelfName(shelf.getName());
					tempStocks.add(storeDetail);
				}
				for (int i = 0; i < debitDetail.getGoodsReturnDetail().getReturnQty(); i++) {
					tempStocks
							.add(setProductDetails(debitDetail.getProduct()));
				}
			}
			barcodeDetails.addAll(tempStocks);
			stockVO.setBarCodeDetails(barcodeDetails);
			stocks.add(stockVO);
			LOG.info("Module: Accounts : Method: debitNotesPDFPrintBarcode Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: debitNotesPDFPrintBarcode: throws Exception "
					+ ex);
			return ERROR;
		}
	}
	
	private StockVO setProductDetails(Product product) throws Exception {
		StockVO stockVO = new StockVO();
		if (product.getProductName().length() > 20)
			stockVO.setProductName(product.getProductName().substring(0, 20));
		else
			stockVO.setProductName(product.getProductName());

		stockVO.setProductCode(product.getBarCode());
		BitMatrix bitMatrix = new Code128Writer().encode(product.getBarCode(),
				BarcodeFormat.CODE_128, 240, 70);
		BufferedImage bi = MatrixToImageWriter.toBufferedImage(bitMatrix);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "png", baos);
		baos.flush();
		stockVO.setImage(bi);
		baos.close();
		return stockVO;
	}

	public String showDebitUpdateEntry() {
		LOGGER.info("Module: Accounts : Method: showDebitUpdateEntry");
		try {
			Debit debit = this.getDebitBL().getDebitService()
					.getDebitNotesById(debitId);
			List<DebitDetail> debitList = this.getDebitBL().getDebitService()
					.getDebitDetailById(debitId);
			ServletActionContext.getRequest().setAttribute("DEBIT_INFO", debit);
			ServletActionContext.getRequest().setAttribute("DEBIT_DETAIL_INFO",
					debitList);
			LOGGER.info("Module: Accounts : Method: showDebitUpdateEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showDebitUpdateEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private final List<DebitDetail> getDebitLines() throws Exception {
		List<DebitDetail> detailList = new ArrayList<DebitDetail>();
		String[] debitValue = splitValues(debitDetails, "#@");
		DebitDetail detail = null;
		Product product = null;
		GoodsReturnDetail goodsReturnDetail = null;
		for (String string : debitValue) {
			detail = new DebitDetail();
			product = new Product();
			String[] debits = splitValues(string, "__");
			product.setProductId(Long.parseLong(debits[0]));
			detail.setProduct(product);
			detail.setReturnQty(Double.parseDouble(debits[1]));
			detail.setAmount(Double.parseDouble(debits[2]));
			if (Integer.parseInt(debits[3]) > 0) {
				detail.setDebitDetailId(Long.parseLong(debits[3]));
			}
			if (Long.parseLong(debits[4]) > 0) {
				goodsReturnDetail = debitBL
						.getPurchaseBL()
						.getGoodsReturnService()
						.getGoodsReturnDetailWithReceive(
								Long.parseLong(debits[4]));
				detail.setGoodsReturnDetail(goodsReturnDetail);
			}
			detailList.add(detail);
		}
		return detailList;
	}

	private String getNumber() throws Exception {
		return SystemBL.getReferenceStamp(Debit.class.getName(),
				getImplementationId());
	}

	private Implementation getImplementationId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Implementation implementation = null;
		if (null != session.getAttribute("THIS")) {
			implementation = new Implementation();
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public DebitBL getDebitBL() {
		return debitBL;
	}

	public void setDebitBL(DebitBL debitBL) {
		this.debitBL = debitBL;
	}

	public String getDebitNumber() {
		return debitNumber;
	}

	public void setDebitNumber(String debitNumber) {
		this.debitNumber = debitNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getDebitId() {
		return debitId;
	}

	public void setDebitId(long debitId) {
		this.debitId = debitId;
	}

	public String getDebitDetails() {
		return debitDetails;
	}

	public void setDebitDetails(String debitDetails) {
		this.debitDetails = debitDetails;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getReturnId() {
		return returnId;
	}

	public void setReturnId(long returnId) {
		this.returnId = returnId;
	}

	public boolean isExchangeFlag() {
		return exchangeFlag;
	}

	public void setExchangeFlag(boolean exchangeFlag) {
		this.exchangeFlag = exchangeFlag;
	}

	public StockVO getStockVO() {
		return stockVO;
	}

	public void setStockVO(StockVO stockVO) {
		this.stockVO = stockVO;
	}

	public List<StockVO> getBarcodeDetails() {
		return barcodeDetails;
	}

	public void setBarcodeDetails(List<StockVO> barcodeDetails) {
		this.barcodeDetails = barcodeDetails;
	}

	public List<StockVO> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockVO> stocks) {
		this.stocks = stocks;
	}
}
