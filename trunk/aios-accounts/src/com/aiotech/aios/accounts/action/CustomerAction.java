package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.service.bl.CustomerBL;
import com.aiotech.aios.common.to.Constants.Accounts.CustomerType;
import com.aiotech.aios.common.to.Constants.Accounts.MemberCardType;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class CustomerAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private long customerId;
	private Long recordId;
	private Integer rowId;
	private String returnMessage;
	private String pageInfo;
	private CustomerBL customerBL;
	private List<Object> aaData;
	private String fileUploadFileName;

	private static final Logger LOGGER = LogManager
			.getLogger(CustomerAction.class);

	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		recordId = null;
		return SUCCESS;
	}

	// for customer json list
	public String showJsonCustomerList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonCustomerList");
			JSONObject jsonList = customerBL.getCustomerList(customerBL
					.getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonCustomerList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonCustomerList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updateCustomerNumber() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: updateCustomerNumber");
			List<Customer> customers = customerBL.getCustomerService()
					.getAllCustomers(customerBL.getImplementationId());
			if (null != customers && customers.size() > 0) {
				int customerNumber = 999;
				Collections.sort(customers, new Comparator<Customer>() {
					public int compare(Customer o1, Customer o2) {
						return o1.getCustomerId().compareTo(o2.getCustomerId());
					}
				});
				for (Customer customer : customers)
					customer.setCustomerNumber(String.valueOf(++customerNumber));
				customerBL.getCustomerService().saveCustomer(customers);
			}
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: updateCustomerNumber: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: updateCustomerNumber Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show customer json list
	public String showAllCustomerDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllCustomerDetails");
			aaData = customerBL.getAllCustomers(pageInfo);
			LOGGER.info("Module: Accounts : Method: showAllCustomerDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllCustomerDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show customer json list
	public String showAllPOSCustomerDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllPOSCustomerDetails");
			aaData = customerBL.getAllPOSCustomers(pageInfo);
			LOGGER.info("Module: Accounts : Method: showAllPOSCustomerDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllPOSCustomerDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show all customers
	public String showAllCustomer() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllCustomer");
			List<CustomerVO> customerList = this.getCustomerBL()
					.getAllCustomers(customerBL.getImplementationId());
			ServletActionContext.getRequest().setAttribute("CUSTOMER_INFO",
					customerList);
			LOGGER.info("Module: Accounts : Method: showAllCustomer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllCustomer Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show all customers
	public String showCustomerByReceipts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAllCustomer");
			List<Customer> customerList = this.getCustomerBL()
					.getCustomerService()
					.getCustomersByReceipts(customerBL.getImplementationId());
			ServletActionContext.getRequest().setAttribute("CUSTOMER_INFO",
					customerList);
			LOGGER.info("Module: Accounts : Method: showAllCustomer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllCustomer Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCustomerApproval() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCustomerApproval");
			customerId = recordId;
			showCustomerEntry();
			LOGGER.info("Module: Accounts : Method: showCustomerApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCustomerApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCustomerRejectEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCustomerRejectEntry");
			customerId = recordId;
			showCustomerEntry();
			LOGGER.info("Module: Accounts : Method: showCustomerRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCustomerRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show customer entry for add/edit
	public String showCustomerEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCustomerEntry");
			Customer customer = null;
			if (customerId == 0) {
				List<Customer> customerList = this.getCustomerBL()
						.getCustomerService()
						.getAllCustomers(customerBL.getImplementationId());
				if (null != customerList && customerList.size() > 0) {
					List<Long> customerNoList = new ArrayList<Long>();
					for (Customer list : customerList) {
						customerNoList.add(Long.parseLong(list
								.getCustomerNumber()));
					}
					long customerNo = Collections.max(customerNoList);
					ServletActionContext.getRequest().setAttribute(
							"CUSTOMER_NUMBER", ++customerNo);
				} else {
					ServletActionContext.getRequest().setAttribute(
							"CUSTOMER_NUMBER", "1000");
				}
			} else {
				customer = customerBL.getCustomerService().getCustomerDetails(
						customerId);
				CommentVO comment = new CommentVO();
				if (recordId != null && customer != null
						&& customer.getCustomerId() != 0 && recordId > 0) {
					comment.setRecordId(customer.getCustomerId());
					comment.setUseCase(Customer.class.getName());
					comment = customerBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			Map<Integer, CustomerType> customerType = new HashMap<Integer, CustomerType>();
			for (CustomerType e : EnumSet.allOf(CustomerType.class)) {
				customerType.put(e.getCode(), e);
			}

			Map<Byte, MemberCardType> memberCardTypes = new HashMap<Byte, MemberCardType>();
			for (MemberCardType e : EnumSet.allOf(MemberCardType.class)) {
				memberCardTypes.put(e.getCode(), e);
			}
			List<CreditTerm> creditTermList = this.getCustomerBL()
					.getCreditTermBL().getCreditTermService()
					.getAllCreditTerms(customerBL.getImplementationId());
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_TYPE_LIST", customerType);
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_REFFERAL_SOURCE",
					customerBL.getCreditTermBL().getLookupMasterBL()
							.getActiveLookupDetails("REFFERAL_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_METHOD",
					customerBL.getCreditTermBL().getLookupMasterBL()
							.getActiveLookupDetails("SHIPPING_SOURCE", true));
			ServletActionContext.getRequest().setAttribute("CREDIT_TERM_LIST",
					creditTermList);
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM_INFORMATION", customer);
			ServletActionContext.getRequest().setAttribute("MEMBER_CARD_TYPES",
					memberCardTypes);
			LOGGER.info("Module: Accounts : Method: showCustomerEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCustomerEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show customer entry for add/edit
	public String showSpecialCustomerEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showSpecialCustomerEntry");
			List<Customer> customerList = customerBL.getCustomerService()
					.getAllCustomers(customerBL.getImplementationId());
			if (null != customerList && customerList.size() > 0) {
				List<Long> customerNoList = new ArrayList<Long>();
				for (Customer list : customerList) {
					customerNoList
							.add(Long.parseLong(list.getCustomerNumber()));
				}
				long customerNo = Collections.max(customerNoList);
				ServletActionContext.getRequest().setAttribute(
						"CUSTOMER_NUMBER", ++customerNo);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"CUSTOMER_NUMBER", "1000");
			}

			if (customerId > 0) {
				Customer customer = customerBL.getCustomerService()
						.getCustomerDetails(customerId);
				CustomerVO customerVO = customerBL.addPOSCustomerVO(customer);
				ServletActionContext.getRequest().setAttribute(
						"CUSTOMER_DETAIL", customerVO);
			}

			LOGGER.info("Module: Accounts : Method: showCustomerEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCustomerEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteCustomer() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: deleteCustomer");
			Customer customer = customerBL.getCustomerService()
					.getCustomerDetails(customerId);
			Combination combination = customer.getCombination();
			Ledger ledger = customerBL.getCombinationBL()
					.getCombinationService()
					.getLederCombinationById(combination.getCombinationId());
			if (null == ledger.getBalance()) {
				customerBL.deleteCustomer(customer);
				returnMessage = "SUCCESS";
			} else
				returnMessage = "Transaction has been posted already";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteCustomer: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: deleteCustomer Exception "
					+ ex);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"FAILURE");
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Customer Shipping Site
	public String showCustomerShippingSite() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCustomerShippingSite");
			List<ShippingDetail> shippingDetails = customerBL
					.getCustomerService().getCustomerShippingSite(customerId);
			aaData = new ArrayList<Object>();
			ShippingDetail shipDetail = null;
			for (ShippingDetail shippingDetail : shippingDetails) {
				shipDetail = new ShippingDetail();
				shipDetail.setShippingId(shippingDetail.getShippingId());
				shipDetail.setName(shippingDetail.getName());
				aaData.add(shipDetail);
			}
			LOGGER.info("Module: Accounts : Method: showCustomerShippingSite: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showCustomerShippingSite Exception "
					+ ex);
			return ERROR;
		}
	}

	// Upload customers
	public String updateCustomerFromFile() {
		try {
			List<String> custoemrList = customerBL
					.fileReader(fileUploadFileName);
			customerBL.updateCustomerFromFile(custoemrList);
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String customerAddRow() {
		LOGGER.info("Inside Module: Accounts : Method: customerAddRow");
		ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
		return SUCCESS;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(String pageInfo) {
		this.pageInfo = pageInfo;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

}
