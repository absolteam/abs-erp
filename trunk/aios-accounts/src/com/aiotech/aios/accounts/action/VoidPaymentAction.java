/**
 * Void Payment Initial Development 
 * Created on 03-April-2013
 */
package com.aiotech.aios.accounts.action;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.VoidPayment;
import com.aiotech.aios.accounts.service.bl.VoidPaymentBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class VoidPaymentAction extends ActionSupport {

	private static final long serialVersionUID = -3591166936733103778L;
	// Common variables
	private long paymentId;
	private long directPaymentId;
	private String description;
	private String returnMessage;
	private String referenceNumber;
	private String voidDate;
	private long voidPaymentId;
	private List<Object> aaData;
	private Long recordId;
	private VoidPaymentBL voidPaymentBL;
	private static final Logger LOGGER = LogManager
			.getLogger(VoidPaymentAction.class);

	// return success
	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess Success");
		return SUCCESS;
	}

	// list all void payment
	public String showJsonVoidPayment() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonVoidPayment");
			JSONObject jsonList = voidPaymentBL.getAllVoidPayment();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonVoidPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonVoidPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Void payment entry screen
	public String showVoidPaymentEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showVoidPaymentEntry");
			referenceNumber = "";
			if (recordId != null && recordId > 0) {
				voidPaymentId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (directPaymentId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = voidPaymentBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
 			if (voidPaymentId > 0) {
				VoidPayment voidPayment = voidPaymentBL.getVoidPaymentService()
						.getVoidPaymentById(voidPaymentId); 
				ServletActionContext.getRequest().setAttribute("VOID_PAYMENT",
						voidPaymentBL.convertVOObject(voidPayment));
			} else
				referenceNumber = SystemBL.getReferenceStamp(
						VoidPayment.class.getName(),
						voidPaymentBL.getImplementation());
			LOGGER.info("Module: Accounts : Method: showJsonVoidPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showVoidPaymentEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// get all cheque payments
	public String showAllChequePayments() {
		LOGGER.info("Inside Module: Accounts : Method: showAllChequePayments");
		try {
			aaData = voidPaymentBL.getChequePayments();
			LOGGER.info("Module: Accounts : Method: showAllChequePayments: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllChequePayments Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// save void payment
	public String saveVoidPayment() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveVoidPayment");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			VoidPayment voidPayment = new VoidPayment();
			voidPayment.setDescription(description);
			voidPayment.setImplementation(voidPaymentBL.getImplementation());
			if (voidPaymentId > 0) {
				VoidPayment voidPaymentEd = voidPaymentBL
						.getVoidPaymentService().findVoidPaymentById(
								voidPaymentId);
				voidPayment.setVoidPaymentId(voidPaymentId);
				voidPayment.setReferenceNumber(voidPaymentEd
						.getReferenceNumber());
				voidPayment.setPerson(voidPaymentEd.getPerson());
				voidPayment.setCreatedDate(voidPaymentEd.getCreatedDate());
			} else {
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				voidPayment.setPerson(person);
				voidPayment.setCreatedDate(Calendar.getInstance().getTime());
				voidPayment.setReferenceNumber(SystemBL.getReferenceStamp(
						VoidPayment.class.getName(),
						voidPaymentBL.getImplementation()));
			}
			voidPayment.setVoidDate(DateFormat.convertStringToDate(voidDate));
			if (directPaymentId > 0) {
				DirectPayment directPayment = voidPaymentBL
						.getDirectPaymentBL().getDirectPaymentService()
						.getDirectPaymentDetail(directPaymentId);
				voidPayment.setDirectPayment(directPayment);
			}
			voidPaymentBL.saveVoidPayment(voidPayment);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: saveVoidPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "System error ";
			LOGGER.info("Module: Accounts : Method: saveVoidPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// delete void payment
	public String deleteVoidPayment() {
		LOGGER.info("Inside Module: Accounts : Method: deleteVoidPayment");
		try {
			VoidPayment voidPayment = voidPaymentBL.getVoidPaymentService()
					.getVoidPaymentById(voidPaymentId);
			if (null != voidPayment.getDirectPayment()) {
				DirectPayment directPayment = voidPaymentBL
						.getDirectPaymentBL()
						.getDirectPaymentService()
						.getDirectPaymentDetail(
								voidPayment.getDirectPayment()
										.getDirectPaymentId());
				voidPayment.setDirectPayment(directPayment);
			}  
			voidPaymentBL.deleteVoidPayment(voidPayment);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveVoidPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveVoidPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public VoidPaymentBL getVoidPaymentBL() {
		return voidPaymentBL;
	}

	public void setVoidPaymentBL(VoidPaymentBL voidPaymentBL) {
		this.voidPaymentBL = voidPaymentBL;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public long getDirectPaymentId() {
		return directPaymentId;
	}

	public void setDirectPaymentId(long directPaymentId) {
		this.directPaymentId = directPaymentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getVoidPaymentId() {
		return voidPaymentId;
	}

	public void setVoidPaymentId(long voidPaymentId) {
		this.voidPaymentId = voidPaymentId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getVoidDate() {
		return voidDate;
	}

	public void setVoidDate(String voidDate) {
		this.voidDate = voidDate;
	}

}
