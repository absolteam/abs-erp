package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductionReceive;
import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.service.bl.ProductionReceiveBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.opensymphony.xwork2.ActionSupport;

public class ProductionReceiveAction extends ActionSupport {

	private static final long serialVersionUID = 2365291423269480906L;

	private static final Logger log = Logger
			.getLogger(ProductionReceiveAction.class);

	// Dependency
	private ProductionReceiveBL productionReceiveBL;

	// Variables
	private long productionReceiveId;
	private long receiveSource;
	private long receivePerson;
	private int rowId;
	private String referenceNumber;
	private String receiveDate;
	private String description;
	private String receiveDetails;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		return SUCCESS;
	}

	// Show All Production Receive
	public String showAllProductionReceive() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductionReceive");
			aaData = productionReceiveBL.showAllProductionReceive();
			log.info("Module: Accounts : Method: showAllProductionReceive: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductionReceive Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Receive Entry Screen
	public String showProductionReceiveEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductionReceiveEntry");
			ProductionReceive productionReceive = null;
			if (productionReceiveId > 0)
				productionReceive = productionReceiveBL
						.getProductionReceiveService()
						.getAllProductionReceiveById(productionReceiveId);
			else
				referenceNumber = SystemBL.getReferenceStamp(
						ProductionReceive.class.getName(),
						productionReceiveBL.getImplementation());

			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION_SOURCE",
					productionReceiveBL.getLookupMasterBL()
							.getActiveLookupDetails("PRODUCTION_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION_RECEIVES", productionReceive);
			log.info("Module: Accounts : Method: showProductionReceiveEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductionReceiveEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Production Receive
	public String saveProductionReceive() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductionReceive");
			ProductionReceive productionReceive = new ProductionReceive();
			productionReceive.setReceiveDate(DateFormat
					.convertStringToDate(receiveDate));
			productionReceive.setDescription(description);
			Person person = new Person();
			person.setPersonId(receivePerson);
			productionReceive.setPersonByReceivePerson(person);
			LookupDetail sourceDetail = new LookupDetail();
			sourceDetail.setLookupDetailId(receiveSource);
			productionReceive.setLookupDetail(sourceDetail);
 			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(receiveDetails);
			JSONArray object = (JSONArray) receiveObject;
			ProductionReceiveDetail productionReceiveDetail = null;
			Product product = null;
			Shelf shelf = null;
			List<ProductionReceiveDetail> productionReceiveDetails = new ArrayList<ProductionReceiveDetail>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("receiveDetails");
				for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					productionReceiveDetail = new ProductionReceiveDetail();
					product = new Product();
					shelf = new Shelf();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"productId").toString()));
					shelf.setShelfId(Integer.parseInt(jsonObjectDetail.get(
							"rackId").toString()));
					productionReceiveDetail.setProduct(product);
					productionReceiveDetail.setShelf(shelf);
					productionReceiveDetail.setQuantity(Double
							.parseDouble(jsonObjectDetail.get("productQty")
									.toString()));
					productionReceiveDetail.setUnitRate(Double
							.parseDouble(jsonObjectDetail.get("amount")
									.toString()));
					productionReceiveDetail
							.setDescription((null != jsonObjectDetail
									.get("linesDescription") && !("")
									.equals(jsonObjectDetail.get(
											"linesDescription").toString())) ? jsonObjectDetail
									.get("linesDescription").toString() : null);
					productionReceiveDetail
							.setProductionReceiveDetailId((null != jsonObjectDetail
									.get("productReceiveDetailId") && Long
									.parseLong(jsonObjectDetail.get(
											"productReceiveDetailId")
											.toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"productReceiveDetailId")
											.toString()) : null);
				}
				productionReceiveDetails.add(productionReceiveDetail);
			}
			if (productionReceiveId > 0) {
				productionReceive.setProductionReceiveId(productionReceiveId);
				productionReceive.setReferenceNumber(referenceNumber);
			} else
				productionReceive.setReferenceNumber(SystemBL
						.getReferenceStamp(ProductionReceive.class.getName(),
								productionReceiveBL.getImplementation()));
			productionReceiveBL.saveProductionReceive(productionReceive,
					productionReceiveDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductionReceive: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveProductionReceive Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Production Receive
	public String deleteProductionReceive() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductionReceive");
			ProductionReceive productionReceive = productionReceiveBL
					.getProductionReceiveService().getProductionReceiveById(
							productionReceiveId);
			boolean deleted = productionReceiveBL.deleteProductionReceive(
					productionReceive, new ArrayList<ProductionReceiveDetail>(
							productionReceive.getProductionReceiveDetails()));
			if (deleted)
				returnMessage = "SUCCESS";
			else
				returnMessage = "Stock is in use.";
			log.info("Module: Accounts : Method: saveProductionReceive: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveProductionReceive Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public ProductionReceiveBL getProductionReceiveBL() {
		return productionReceiveBL;
	}

	public void setProductionReceiveBL(ProductionReceiveBL productionReceiveBL) {
		this.productionReceiveBL = productionReceiveBL;
	}

	public long getProductionReceiveId() {
		return productionReceiveId;
	}

	public void setProductionReceiveId(long productionReceiveId) {
		this.productionReceiveId = productionReceiveId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public long getReceiveSource() {
		return receiveSource;
	}

	public void setReceiveSource(long receiveSource) {
		this.receiveSource = receiveSource;
	}

	public long getReceivePerson() {
		return receivePerson;
	}

	public void setReceivePerson(long receivePerson) {
		this.receivePerson = receivePerson;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReceiveDetails() {
		return receiveDetails;
	}

	public void setReceiveDetails(String receiveDetails) {
		this.receiveDetails = receiveDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
