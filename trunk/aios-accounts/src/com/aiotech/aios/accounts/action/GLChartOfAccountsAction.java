package com.aiotech.aios.accounts.action;

/*******************************************************************************************
 *
 * Create Log
 * -------------------------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 	Description
 * -------------------------------------------------------------------------------------------
 * 1.0		06 Mar 2012 	Saleem A	 		   		Initial Version
 *********************************************************************************************/

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentVO;
import com.aiotech.aios.accounts.service.GLChartOfAccountService;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.to.GLChartOfAccountsTO;
import com.aiotech.aios.accounts.to.converter.GLChartOfAccountsTOConverter;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.to.Constants.Accounts.Segments;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GLChartOfAccountsAction extends ActionSupport {

	private static final long serialVersionUID = -1126091960432924836L;

	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(GLChartOfAccountsAction.class);

	// Common Variable Declaration
	private long accountId;
	private long segmentId;
	private long combinationId;
	private int accountTypeId;
	private String accountType;
	private String accountCode;
	private String accountDescription;
	private String showPage;
	private String message;
	private String sqlReturnMessage;
	private Long subType;
	private String systemAccountCode;
	private Long recordId;
	private Long messageId;
	private Integer processFlag;
	// Grid Variables
	private Integer rows = 0;
	private Integer id;

	// Common Object Reference initialization
	private GLChartOfAccountService accountService;
	private List<GLChartOfAccountsTO> accountList = null;
	private GLChartOfAccountsTO accountTo = null;

	private CombinationBL combinationBL;

	private Segment segment;
	private Implementation implementation = null;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		recordId = null;
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	// Update COA
	public String updateCOAAccounts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: updateCOAAccounts");
			/*
			 * List<Account> accounts = combinationBL.getChartOfAccountsBL()
			 * .getAccountService().getAllAccounts();
			 * combinationBL.getChartOfAccountsBL().updateCOAAccounts(accounts);
			 * sqlReturnMessage = "SUCCESS";
			 */
			LOGGER.info("Module: Accounts : Method: updateCOAAccounts: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Records update failure.";
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: updateCOAAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showChartOfAccountsList() {
		try {
			LOGGER.info("Module: Accounts : Method: showChartOfAccountsList");
			getImplementId();
			List<Account> coaList = accountService
					.getAllAccounts(implementation);
			accountList = GLChartOfAccountsTOConverter.convertToTOList(coaList);
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			Constants.Accounts accountTypes = new Constants.Accounts();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			if (null != accountList && accountList.size() > 0) {
				for (GLChartOfAccountsTO list : accountList) {
					JSONArray array = new JSONArray();
					array.add(list.getAccountId());
					array.add(list.getSegmentId());
					array.add(list.getSegmentName());
					array.add(list.getAccountCode());
					array.add(list.getAccountDescription());
					if (list.getSegmentId() == (long) Segments.Natural
							.getCode()) {
						accountTypeId = list.getAccountTypeId();
						switch (accountTypeId) {
						case 1:
							accountType = accountTypes.asset;
							break;
						case 2:
							accountType = accountTypes.liabilites;
							break;
						case 3:
							accountType = accountTypes.revenue;
							break;
						case 4:
							accountType = accountTypes.expenses;
							break;
						case 5:
							accountType = accountTypes.ownersEquity;
							break;
						}
					} else
						accountType = "";
					array.add(accountType);
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showChartOfAccountsList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showChartOfAccountsList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCombinationSubAccounts() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showCombinationSubAccounts: ");
			Segment segment = combinationBL.getAccountBL().getSegmentBL()
					.getSegmentService().getAllSegmentChilds(segmentId);
			accountList = null;
			accountTypeId = 0;
			if (null != segment && null != segment.getSegments()
					&& segment.getSegments().size() > 0) {
				if ((int) segment.getAccessId() == (int) Segments.Natural
						.getCode()) {
					Combination combination = combinationBL
							.getCombinationService().getCombinationAccount(
									combinationId);
					Account account = combinationBL
							.getAccountBL()
							.getAccountService()
							.getAccountByAccountId(
									combination.getAccountByNaturalAccountId()
											.getAccountId());
					accountTypeId = account.getAccountType();
				}
				Set<Long> segmentIds = new HashSet<Long>();
				for (Segment sg : segment.getSegments())
					segmentIds.add(sg.getSegmentId());
				List<Account> accounts = combinationBL.getAccountBL()
						.getAccountService()
						.getAccountBySegmentIds(segmentIds, accountTypeId);
				if (null != accounts && accounts.size() > 0) {
					accountList = new ArrayList<GLChartOfAccountsTO>();
					GLChartOfAccountsTO accountsTO = null;
					for (Account account : accounts) {
						accountsTO = new GLChartOfAccountsTO();
						accountsTO.setAccountId(account.getAccountId());
						accountsTO.setAccountCode(account.getCode());
						accountsTO.setAccountDescription(account.getAccount());
						accountsTO.setSegmentId(account.getSegment()
								.getSegmentId());
						accountsTO.setAccessId(account.getSegment()
								.getAccessId());
						accountList.add(accountsTO);
					}
				}
			}
			LOGGER.info("Module: Accounts : Method: showCombinationSubAccounts: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationSubAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String viewChartOfAccount() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: viewChartOfAccount: ");
			getImplementId();
			List<Account> coaList = accountService
					.getAllAccounts(implementation);
			if (null != coaList && coaList.size() > 0)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", coaList.size());
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", 0);
			LOGGER.info("Module: Accounts : Method: viewChartOfAccount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: viewChartOfAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Natural account
	public String showNaturalAccount() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showNaturalAccount: ");
			getImplementId();
			List<Account> accounts = accountService.getCOAAccountByAccountType(
					accountTypeId, implementation);
			accountList = GLChartOfAccountsTOConverter
					.convertToTOList(accounts);
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();

			Constants.Accounts accountTypes = new Constants.Accounts();
			for (GLChartOfAccountsTO list : accountList) {
				JSONArray array = new JSONArray();
				array.add(list.getAccountId());
				array.add(list.getAccountCode());
				array.add(list.getAccountDescription());
				if (list.getSegmentId() == 3) {
					accountTypeId = list.getAccountTypeId();
					switch (accountTypeId) {
					case 1:
						accountType = accountTypes.asset;
						break;
					case 2:
						accountType = accountTypes.liabilites;
						break;
					case 3:
						accountType = accountTypes.revenue;
						break;
					case 4:
						accountType = accountTypes.expenses;
						break;
					case 5:
						accountType = accountTypes.ownersEquity;
						break;
					}
				} else
					accountType = "";
				array.add(list.getAccountTypeId());
				array.add(accountType);
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showNaturalAccount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showNaturalAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get coa entry page showEntryPage
	public String showAddEntryPage() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showEntryPage: ");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Segment> segments = accountService
					.getDefaultSegments(combinationBL.getAccountBL()
							.getImplementation());
			SegmentVO segmentVO = null;
			List<SegmentVO> childSegmentVOs = null;
			List<SegmentVO> segmentVOs = new ArrayList<SegmentVO>();
			for (Segment segment : segments) {
				segmentVO = new SegmentVO();
				childSegmentVOs = new ArrayList<SegmentVO>();
				segmentVO.setSegmentId(segment.getSegmentId());
				segmentVO.setSegmentName(segment.getSegmentName());
				segmentVO.setSegmentVOs(combinationBL.getAccountBL()
						.getSegmentBL()
						.getSegmentDirectChild(segment, childSegmentVOs));
				segmentVOs.add(segmentVO);
			}
			ServletActionContext.getRequest().setAttribute("SEGMENT_LIST",
					segmentVOs);
			ServletActionContext.getRequest().setAttribute("page", showPage);
			LOGGER.info("Module: Accounts : Method: showAddEntryPage: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAddEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to show line level entry
	public String showLineEntryAccount() {
		LOGGER.info("Module: Accounts : Method: showLineEntryAccount");
		try {
			ServletActionContext.getRequest().setAttribute("rowId", id);
			ServletActionContext.getRequest().setAttribute("page", showPage);
			if (segmentId > 0) {
				Segment segment = combinationBL.getAccountBL().getSegmentBL()
						.getSegmentService().getSegmentParentById(segmentId);
				if ((int) segment.getAccessId() == (int) Segments.Natural
						.getCode()) {
					List<LookupDetail> subTypes = combinationBL
							.getLookupMasterBL().getActiveLookupDetails(
									"ACCOUNT_SUB_TYPE", true);
					Map<Integer, AccountType> accountTypes = new HashMap<Integer, AccountType>();
					for (AccountType e : EnumSet.allOf(AccountType.class)) {
						accountTypes.put(e.getCode(), e);
					}
					ServletActionContext.getRequest().setAttribute("SUB_TYPES",
							subTypes);
					ServletActionContext.getRequest().setAttribute(
							"ACCOUNT_TYPES", accountTypes);
				}
			}
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showLineEntryAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveAccountCOATemplate() {
		LOGGER.info("Module: Accounts : Method: saveAccountCOATemplate");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			String[] lineDetailArray = splitValues(accountDescription, "#@");
			Account account = null;
			Segment segment = null;
			List<Account> accountList = new ArrayList<Account>();
			getImplementId();
			Combination combination = null;
			List<Combination> combinationList = new ArrayList<Combination>();
			int lastIndex = -1;
			for (String stringArray : lineDetailArray) {
				String[] lineDetails = splitValues(stringArray, "__");
				account = new Account();
				segment = new Segment();
				account.setCode(lineDetails[0]);
				if (lineDetails[0].lastIndexOf('.') != -1) {
					String[] accountCodes = lineDetails[0].split("\\.");
					account.setCode(accountCodes[accountCodes.length - 1]);
					lastIndex = accountCodes.length;
					accountCodes = lineDetails[1].split("\\.");
					account.setAccount(accountCodes[accountCodes.length - 1]);
				} else {
					lastIndex = -1;
					account.setCode(lineDetails[0]);
					account.setAccount(lineDetails[1]);
				}
				account.setImplementation(implementation);
				account.setAccountType(Integer.parseInt(lineDetails[2]));
				segment.setSegmentId(Long.parseLong(lineDetails[3]));
				account.setSegment(segment);
				account.setIsSystem(false);
				account.setAccountId(Long
						.parseLong(String
								.valueOf(
										implementation.getImplementationId()
												.toString().length())
								.concat(implementation.getImplementationId()
										.toString())
								.concat(String.valueOf(
										account.getCode().length()).concat(
										account.getCode()))));
				accountList.add(account);
				combination = new Combination();
				combination = createCombiantionCOAId(lastIndex, lineDetails[0]);
				combinationList.add(combination);
			}
			combinationBL.saveAccountCombinations(accountList, combinationList);
			List<Segment> segmentList = accountService.getAllSegment();
			ServletActionContext.getRequest().setAttribute("segmentList",
					segmentList);
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			if (null != session.getAttribute("SESSION_COMBINATION_IDS_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_IDS_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_IDS_"
						+ sessionObj.get("jSessionId"));
			}
			if (null != session.getAttribute("SESSION_COMBINATION_TEMPLATE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TEMPLATE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_TEMPLATE_"
						+ sessionObj.get("jSessionId"));
			}
			LOGGER.info("Module: Accounts : Method: saveAccountCOATemplate: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: saveAccountCOATemplate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String discardCOAViewTemplate() {
		try {
			LOGGER.info("Module: Accounts : Method: discardCOAViewTemplate");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("SESSION_COMBINATION_TEMPLATE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TEMPLATE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_TEMPLATE_"
						+ sessionObj.get("jSessionId"));
			}
			if (null != session.getAttribute("SESSION_COMBINATION_IDS_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_IDS_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_IDS_"
						+ sessionObj.get("jSessionId"));
			}
			LOGGER.info("Module: Accounts : Method: discardCOAViewTemplate: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: discardCOAViewTemplate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Combination createCombiantionCOAId(int lastIndex, String accountCode) {
		Combination combination = new Combination();
		Account account = null;
		if (lastIndex == 1 || lastIndex == -1) {
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(accountCode.length()).concat(
							accountCode))));
			combination.setAccountByCompanyAccountId(account);
		} else if (lastIndex == 2) {
			account = new Account();
			String[] codeArray = accountCode.split("\\.");
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[0].length()).concat(
							codeArray[0]))));
			combination.setAccountByCompanyAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[1].length()).concat(
							codeArray[1]))));
			combination.setAccountByCostcenterAccountId(account);
		} else if (lastIndex == 3) {
			account = new Account();
			String[] codeArray = accountCode.split("\\.");
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[0].length()).concat(
							codeArray[0]))));
			combination.setAccountByCompanyAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[1].length()).concat(
							codeArray[1]))));
			combination.setAccountByCostcenterAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[2].length()).concat(
							codeArray[2]))));
			combination.setAccountByNaturalAccountId(account);
		} else if (lastIndex == 4) {
			account = new Account();
			String[] codeArray = accountCode.split("\\.");
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[0].length()).concat(
							codeArray[0]))));
			combination.setAccountByCompanyAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[1].length()).concat(
							codeArray[1]))));
			combination.setAccountByCostcenterAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[2].length()).concat(
							codeArray[2]))));
			combination.setAccountByNaturalAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[3].length()).concat(
							codeArray[3]))));
			combination.setAccountByAnalysisAccountId(account);
		} else if (lastIndex == 5) {
			account = new Account();
			String[] codeArray = accountCode.split("\\.");
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[0].length()).concat(
							codeArray[0]))));
			combination.setAccountByCompanyAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[1].length()).concat(
							codeArray[1]))));
			combination.setAccountByCostcenterAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[2].length()).concat(
							codeArray[2]))));
			combination.setAccountByNaturalAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[3].length()).concat(
							codeArray[3]))));
			combination.setAccountByAnalysisAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[4].length()).concat(
							codeArray[4]))));
			combination.setAccountByBuffer1AccountId(account);
		} else if (lastIndex == 6) {
			account = new Account();
			String[] codeArray = accountCode.split("\\.");
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[0].length()).concat(
							codeArray[0]))));
			combination.setAccountByCompanyAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[1].length()).concat(
							codeArray[1]))));
			combination.setAccountByCostcenterAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[2].length()).concat(
							codeArray[2]))));
			combination.setAccountByNaturalAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[3].length()).concat(
							codeArray[3]))));
			combination.setAccountByAnalysisAccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[4].length()).concat(
							codeArray[4]))));
			combination.setAccountByBuffer1AccountId(account);
			account = new Account();
			account.setAccountId(Long.parseLong(String
					.valueOf(
							implementation.getImplementationId().toString()
									.length())
					.concat(implementation.getImplementationId().toString())
					.concat(String.valueOf(codeArray[5].length()).concat(
							codeArray[5]))));
			combination.setAccountByBuffer2AccountId(account);
		}
		return combination;
	}

	@SuppressWarnings("unchecked")
	public String getChartOfAccountCode() {
		try {
			LOGGER.info("Module: Accounts : Method: getChartOfAccountCode");
			getImplementId();
			Account account = accountService.getLastUpdatedAccount(
					implementation, segmentId);
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<Account> accounts = new ArrayList<Account>();
			if (session.getAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId")) != null) {
				List<Account> accounttmp = (List<Account>) session
						.getAttribute("ACCOUNT_INFO_"
								+ sessionObj.get("jSessionId"));
				if (null != accounttmp && accounttmp.size() > 0)
					accounts.addAll(accounttmp);
			}
			if (null != account)
				accounts.add(account);
			systemAccountCode = combinationBL.getAccountBL()
					.getSystemAccountCode(accounts, account, segmentId);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: getChartOfAccountCode: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String saveAccountAddLines() {
		try {
			LOGGER.info("Module: Accounts : Method: saveAccountAddLines");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Account account = new Account();
			List<Account> accounts = null;
			account.setCode(accountCode);
			account.setAccount(accountDescription);
			account.setAccountType(accountTypeId);
			Segment segment = new Segment();
			segment.setSegmentId(segmentId);
			account.setSegment(segment);
			if (subType != null && subType > 0) {
				LookupDetail ld = new LookupDetail();
				ld.setLookupDetailId(subType);
				account.setLookupDetail(ld);
			}
			account.setIsSystem(false);
			sqlReturnMessage = "";
			getImplementId();
			account.setImplementation(implementation);
			if (session.getAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId")) == null) {
				accounts = new ArrayList<Account>();
			} else {
				accounts = (List<Account>) session.getAttribute("ACCOUNT_INFO_"
						+ sessionObj.get("jSessionId"));
			}
			boolean flag = true;
			for (Account list : accounts) {
				if (flag) {
					if (accountCode.equalsIgnoreCase(list.getCode())) {
						sqlReturnMessage = "Account code already exits";
						flag = false;
					} else if (accountDescription.equalsIgnoreCase(list
							.getAccount())) {
						sqlReturnMessage = "Account Description already exits";
						flag = false;
					}
				}
			}
			if (flag) {
				List<Account> tempAccountList = accountService
						.validateAccountDetails(accountCode, implementation,
								segmentId);
				if (null != tempAccountList && tempAccountList.size() > 0) {
					for (Account list : tempAccountList) {
						if (flag) {
							if (accountCode.equalsIgnoreCase(list.getCode())) {
								sqlReturnMessage = "Account code already exits";
								flag = false;
							} else if (accountDescription.equalsIgnoreCase(list
									.getAccount())) {
								sqlReturnMessage = "Account Description already exits";
								flag = false;
							}
						}
					}
				}
			}
			if (flag) {
				accounts.add(account);
				session.setAttribute(
						"ACCOUNT_INFO_" + sessionObj.get("jSessionId"),
						accounts);
				sqlReturnMessage = "Success";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				LOGGER.info("Module: Accounts : Method: saveAccountAddLines Action Success");
				return SUCCESS;
			} else {
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				LOGGER.info("Module: Accounts : Method: saveAccountAddLines Action Error "
						+ sqlReturnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: saveAccountAddLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to update add lines coa saveAccountAddEditLines
	@SuppressWarnings("unchecked")
	public String saveAccountAddEditLines() {
		LOGGER.info("Module: Accounts : Method: saveAccountAddEditLines");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Account account = new Account();
			Segment segment = new Segment();
			List<Account> accounts = null;
			account.setCode(accountCode);
			account.setAccount(accountDescription);
			account.setAccountType(accountTypeId);
			segment.setSegmentId(segmentId);
			account.setSegment(segment);
			if (subType != null && subType > 0) {
				LookupDetail ld = new LookupDetail();
				ld.setLookupDetailId(subType);
				account.setLookupDetail(ld);
			}
			account.setIsSystem(false);
			getImplementId();
			account.setImplementation(implementation);
			accounts = (List<Account>) session.getAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			boolean flag = true;
			int i = 0;
			for (Account list : accounts) {
				if (flag) {
					if (accountCode.equalsIgnoreCase(list.getCode())
							&& id - 1 != i) {
						sqlReturnMessage = "Account code already exits";
						flag = false;
					} else if (accountDescription.equalsIgnoreCase(list
							.getAccount()) && id - 1 != i) {
						sqlReturnMessage = "Account Description already exits";
						flag = false;
					}
				}
				i++;
			}
			if (flag) {
				List<Account> tempAccountList = accountService
						.validateAccountDetails(accountCode, implementation,
								segmentId);
				if (null != tempAccountList && tempAccountList.size() > 0) {
					for (Account list : tempAccountList) {
						if (flag) {
							if (accountCode.equalsIgnoreCase(list.getCode())) {
								sqlReturnMessage = "Account code already exits";
								flag = false;
							} else if (accountDescription.equalsIgnoreCase(list
									.getAccount())) {
								sqlReturnMessage = "Account Description already exits";
								flag = false;
							}
						}
					}
				}
			}
			if (flag) {
				accounts.set(id - 1, account);
				session.setAttribute(
						"ACCOUNT_INFO_" + sessionObj.get("jSessionId"),
						accounts);
				sqlReturnMessage = "Success";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				LOGGER.info("Module: Accounts : Method: saveAccountAddEditLines Action Success");
				return SUCCESS;
			} else {
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				LOGGER.info("Module: Accounts : Method: saveAccountAddEditLines Action Error "
						+ sqlReturnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: saveAccountAddEditLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to delete line entry
	@SuppressWarnings("unchecked")
	public String deleteAccountLines() {
		LOGGER.info("Module: Accounts : Method: deleteAccountLines");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<Account> accounts = null;
			accounts = (List<Account>) session.getAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			accounts.remove(id - 1);
			session.setAttribute(
					"ACCOUNT_INFO_" + sessionObj.get("jSessionId"), accounts);
			ServletActionContext.getRequest().setAttribute("Success",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: deleteAccountLines Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: deleteAccountLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save the records from a session
	@SuppressWarnings("unchecked")
	public String saveAccounts() {
		LOGGER.info("Module: Accounts : Method: saveAccounts");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			String successMessage = "Success";
			List<Account> accounts = (List<Account>) session
					.getAttribute("ACCOUNT_INFO_"
							+ sessionObj.get("jSessionId"));
			Segment segment = null;
			Segment segmentLevel = null;
			boolean saveFlag = true;
			for (Account account : accounts) {
				segment = combinationBL
						.getAccountBL()
						.getSegmentBL()
						.getSegmentService()
						.getSegmentParentById(
								account.getSegment().getSegmentId());
				if (null != segment.getSegment()) {
					// Validate self segment account
					List<Account> accountstmp = combinationBL
							.getAccountBL()
							.getAccountService()
							.getAccountsBySegmentId(implementation,
									segment.getSegment().getSegmentId());
					if (null != accountstmp && accountstmp.size() > 0) {
						if ((int) account.getAccountType() != (int) accountstmp
								.get(0).getAccountType()) {
							saveFlag = false;
							successMessage = "Please check the account type.";
							break;
						}
					}
					// Validate account
					segmentLevel = combinationBL
							.getAccountBL()
							.getSegmentBL()
							.getSegmentService()
							.getSegmentParentById(
									segment.getSegment().getSegmentId());
					if (null != segmentLevel.getSegment()) {
						accountstmp = combinationBL
								.getAccountBL()
								.getAccountService()
								.getAccountsBySegmentId(
										implementation,
										segmentLevel.getSegment()
												.getSegmentId());
						if (null != accountstmp && accountstmp.size() > 0) {
							if ((int) account.getAccountType() != (int) accountstmp
									.get(0).getAccountType()) {
								saveFlag = false;
								successMessage = "Please check the account type.";
								break;
							}
						}
						// Self validate
						if (account.getAccountType() > 0) {
							for (Account self : accounts) {
								if ((int) self.getAccountType() > 0
										&& !self.getCode()
												.trim()
												.equals(account.getCode()
														.trim())) {
									if ((long) segmentLevel.getSegment()
											.getSegmentId() == self
											.getSegment().getSegmentId()) {
										if ((int) account.getAccountType() != (int) self
												.getAccountType()) {
											saveFlag = false;
											successMessage = "Please check the account type.";
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			if (saveFlag) {
				User user = (User) sessionObj.get("USER");
				if (accounts != null && accounts.size() > 0)
					combinationBL.getAccountBL().saveChartOfAccounts(accounts,
							user);
				else
					successMessage = "Enter all the required information";
				session.removeAttribute("ACCOUNT_INFO_"
						+ sessionObj.get("jSessionId"));
				ServletActionContext.getRequest().setAttribute("message",
						successMessage);
			} else
				ServletActionContext.getRequest().setAttribute("message",
						successMessage);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("message",
					"Enter all the required information");
			LOGGER.info("Module: Accounts : Method: saveAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to discard coa entry
	public String discardAccounts() {
		LOGGER.info("Module: Accounts : Method: discardAccounts");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			ServletActionContext.getRequest()
					.setAttribute("message", "Success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("message",
					"Discard Failure");
			LOGGER.info("Module: Accounts : Method: saveAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getChartOfAccountsApprovalScreen() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: getChartOfAccountsApprovalScreen: ");
			accountList = new ArrayList<GLChartOfAccountsTO>();
			accountTo = new GLChartOfAccountsTO();
			List<Segment> segmentList = accountService.getAllSegment();
			ServletActionContext.getRequest().setAttribute("segmentList",
					segmentList);
			Account account = accountService.getAccountByAccountId(recordId);
			accountTo = GLChartOfAccountsTOConverter
					.convertAccountToTO(account);
			ServletActionContext.getRequest().setAttribute("ACCOUNT_DETAILS",
					accountTo);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: getChartOfAccountsApprovalScreen: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to update the coa entry
	public String showUpdateEntryPage() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showEntryPage: ");
			GLChartOfAccountsTO accountTo = new GLChartOfAccountsTO();
			List<Segment> segments = combinationBL
					.getAccountBL()
					.getSegmentBL()
					.getSegmentService()
					.getAllSegments(
							combinationBL.getAccountBL().getImplementation());
			SegmentVO segmentVO = null;
			List<SegmentVO> childSegmentVOs = null;
			List<SegmentVO> segmentVOs = new ArrayList<SegmentVO>();
			for (Segment segment : segments) {
				segmentVO = new SegmentVO();
				childSegmentVOs = new ArrayList<SegmentVO>();
				segmentVO.setSegmentId(segment.getSegmentId());
				segmentVO.setSegmentName(segment.getSegmentName());
				segmentVO.setSegmentVOs(combinationBL.getAccountBL()
						.getSegmentBL()
						.getSegmentDirectChild(segment, childSegmentVOs));
				segmentVOs.add(segmentVO);
			}
			ServletActionContext.getRequest().setAttribute("SEGMENT_LIST",
					segmentVOs);
			ServletActionContext.getRequest().setAttribute("page", showPage);
			Account account = accountService.getAccountByAccountId(accountId);
			accountTo = GLChartOfAccountsTOConverter
					.convertAccountToTO(account);
			Constants.Accounts accountTypes = new Constants.Accounts();
			if ((int) accountTo.getAccessId() == (int) Segments.Natural
					.getCode()) {
				switch (accountTo.getAccountTypeId()) {
				case 1:
					accountType = accountTypes.asset;
					break;
				case 2:
					accountType = accountTypes.liabilites;
					break;
				case 3:
					accountType = accountTypes.revenue;
					break;
				case 4:
					accountType = accountTypes.expenses;
					break;
				case 5:
					accountType = accountTypes.ownersEquity;
					break;
				}
				accountTo.setAccountType(accountType);
			} else
				accountTo.setAccountType("");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			session.setAttribute(
					"ACCOUNT_INFO_" + sessionObj.get("jSessionId"), account);
			ServletActionContext.getRequest().setAttribute("ACCOUNT_DETAILS",
					accountTo);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to update the coa entry
	public String showRejectEntryPage() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showRejectEntryPage: ");
			accountList = new ArrayList<GLChartOfAccountsTO>();
			GLChartOfAccountsTO accountTo = new GLChartOfAccountsTO();
			List<Segment> segmentList = accountService.getAllSegment();
			ServletActionContext.getRequest().setAttribute("segmentList",
					segmentList);
			ServletActionContext.getRequest().setAttribute("page", "edit");
			showPage = "edit";
			Account account = accountService.getAccountByAccountId(recordId);
			accountTo = GLChartOfAccountsTOConverter
					.convertAccountToTO(account);
			Constants.Accounts accountTypes = new Constants.Accounts();
			if (accountTo.getSegmentId() == (long) Segments.Natural.getCode()) {
				switch (accountTo.getAccountTypeId()) {
				case 1:
					accountType = accountTypes.asset;
					break;
				case 2:
					accountType = accountTypes.liabilites;
					break;
				case 3:
					accountType = accountTypes.revenue;
					break;
				case 4:
					accountType = accountTypes.expenses;
					break;
				case 5:
					accountType = accountTypes.ownersEquity;
					break;
				}
				accountTo.setAccountType(accountType);
			} else {
				accountTo.setAccountType("");
			}
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			session.setAttribute(
					"ACCOUNT_INFO_" + sessionObj.get("jSessionId"), account);
			ServletActionContext.getRequest().setAttribute("ACCOUNT_DETAILS",
					accountTo);
			CommentVO comment = new CommentVO();
			comment.setRecordId(account.getAccountId());
			comment.setUseCase(Account.class.getName());
			comment = combinationBL.getAccountBL().getCommentBL()
					.getCommentInfo(comment);
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
					comment);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showRejectEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to nothing user line edit...
	public String editUpdateAccountLines() {
		LOGGER.info("Module: Accounts : Method: editUpdateAccountLines");
		try {
			LOGGER.info("Module: Accounts : Method: editUpdateAccountLines Action Success");
			Account tempAccount = accountService.getAccountDetails(accountId);
			boolean flag = true;
			boolean innerFlag = true;
			if (!tempAccount.getCode().equalsIgnoreCase(accountCode)) {
				flag = false;
			} else if (!tempAccount.getAccount().equalsIgnoreCase(
					accountDescription)) {
				flag = false;
			}
			getImplementId();
			if (!flag) {
				List<Account> accountList = accountService
						.validateAccountDetails(accountCode, implementation,
								segmentId);
				if (null != accountList && accountList.size() > 0) {
					for (Account list : accountList) {
						if (list.getAccountId() != accountId) {
							if (innerFlag) {
								if (accountCode
										.equalsIgnoreCase(list.getCode())) {
									sqlReturnMessage = "Account Code Exits";
									flag = false;
									innerFlag = false;
								} else if (accountDescription
										.equalsIgnoreCase(list.getAccount())) {
									sqlReturnMessage = "Account Description Exits";
									flag = false;
									innerFlag = false;
								}
							}
						}
					}
				}
			}
			if (innerFlag) {
				sqlReturnMessage = "Success";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				return SUCCESS;
			} else {
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: editUpdateAccountLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private boolean validateAccounts(String code, String account, Long segmentId)
			throws Exception {
		getImplementId();
		List<Account> accountList = accountService.validateAccountDetails(code,
				implementation, segmentId);
		if (null != accountList && accountList.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	private Long generateAccountId(Implementation implementation,
			String accountCode) throws Exception {
		Account tempAccount = combinationBL.getAccountBL().getAccountService()
				.getLastUpdatedAccount(implementation);
		long accountId = 0;
		if (null != tempAccount)
			accountId = tempAccount.getAccountId() + 1;
		else
			accountId = implementation.getAccountStartingId();
		return accountId;
	}

	public Account createAnalyisAccount(String accountDesc, Boolean isSystem)
			throws Exception {
		getImplementId();
		segmentId = Segments.Analysis.getCode();
		Account accounts = accountService.getLastUpdatedAnalysis(
				implementation, segmentId);
		if (validateAccounts(
				String.valueOf(new Integer(accounts.getCode().trim()) + 1),
				accountDesc, segmentId)) {
			Account newAccount = new Account();
			newAccount.setCode(String.valueOf(new Integer(accounts.getCode()
					.trim()) + 1));
			newAccount.setAccount(accountDesc);
			newAccount.setSegment(accounts.getSegment());
			newAccount.setImplementation(accounts.getImplementation());
			newAccount.setAccountType(accounts.getAccountType());
			newAccount.setIsSystem(isSystem);
			newAccount.setAccountId(generateAccountId(
					accounts.getImplementation(), newAccount.getCode()));
			accountService.updateAccount(newAccount);
			return newAccount;
		} else {
			return null;
		}
	}

	public Account createNaturalAccount(String accountDesc,
			Integer accountType, Boolean isSystem) throws Exception {
		getImplementId();
		segmentId = Segments.Natural.getCode();
		Account accounts = accountService.getLastUpdatedNatural(implementation,
				segmentId);
		if (validateAccounts(
				String.valueOf(new Integer(accounts.getCode().trim()) + 1),
				accountDesc, segmentId)) {
			Account naturalAccount = new Account();
			naturalAccount.setCode(String.valueOf(new Integer(accounts
					.getCode().trim()) + 1));
			naturalAccount.setAccount(accountDesc);
			naturalAccount.setSegment(accounts.getSegment());
			naturalAccount.setImplementation(accounts.getImplementation());
			naturalAccount.setAccountType(accountType);
			naturalAccount.setIsSystem(isSystem);
			naturalAccount.setAccountId(generateAccountId(
					accounts.getImplementation(), naturalAccount.getCode()));
			accountService.updateAccount(naturalAccount);
			return naturalAccount;
		} else {
			return null;
		}
	}

	public Account createCostCenterAccount(String accountDesc, Boolean isSystem)
			throws Exception {
		getImplementId();
		segmentId = 2;
		Account accounts = accountService.getLastUpdatedCostCenter(
				implementation, segmentId);
		if (validateAccounts(
				String.valueOf(new Integer(accounts.getCode().trim()) + 1),
				accountDesc, segmentId)) {
			Account newAccount = new Account();
			newAccount.setCode(String.valueOf(new Integer(accounts.getCode()
					.trim()) + 1));
			newAccount.setAccount(accountDesc);
			newAccount.setSegment(accounts.getSegment());
			newAccount.setImplementation(accounts.getImplementation());
			newAccount.setAccountType(accounts.getAccountType());
			newAccount.setIsSystem(isSystem);
			newAccount.setAccountId(generateAccountId(
					accounts.getImplementation(), newAccount.getCode()));
			accountService.updateAccount(newAccount);
			return newAccount;
		} else {
			return null;
		}
	}

	// to save edit lines accounts
	public String updateAccounts() {
		LOGGER.info("Module: Accounts : Method: updateAccounts");
		try {
			getImplementId();
			Account account = new Account();
			Segment segment = new Segment();
			account.setAccountId(accountId);
			account.setCode(accountCode);
			account.setAccount(accountDescription);
			account.setAccountType(accountTypeId);
			segment.setSegmentId(segmentId);
			account.setSegment(segment);
			if (subType != null && subType > 0) {
				LookupDetail ld = new LookupDetail();
				ld.setLookupDetailId(subType);
				account.setLookupDetail(ld);
			}
			account.setIsSystem(false);
			account.setImplementation(implementation);
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			combinationBL.getAccountBL().updateChartOfAccount(account, user);
			message = "Success";
			ServletActionContext.getRequest().setAttribute(message, message);
			return SUCCESS;
		} catch (Exception ex) {
			message = "Failure";
			ServletActionContext.getRequest().setAttribute(message, message);
			LOGGER.info("Module: Accounts : Method: updateAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showFilterAccount() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showFilterAccount");
			getImplementId();
			List<Account> accountList = this.getAccountService()
					.getFilterAccounts(implementation, accountTypeId);
			ServletActionContext.getRequest().setAttribute("FILTER_ACCOUNTS",
					accountList);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showFilterAccount: throws Exception "
					+ ex);
			return null;
		}
	}

	// to delete the record
	public String deleteAccounts() {
		LOGGER.info("Module: Accounts : Method: deleteAccounts");
		try {
			Account account = new Account();
			Segment segment = new Segment();
			account.setAccountId(accountId);
			segment.setSegmentId(segmentId);
			account.setSegment(segment);
			List<Combination> combinationList = combinationBL
					.getCombinationService().getCombiantionByAccount(accountId);
			if (combinationList != null && !combinationList.equals("")
					&& combinationList.size() > 0) {
				ServletActionContext.getRequest().setAttribute("error",
						"Account is in use, Deletion Failure.");
				return ERROR;
			} else {
				combinationBL.getAccountBL().deleteAccount(account);
				ServletActionContext.getRequest().setAttribute("success",
						"Record deleted.");
				return SUCCESS;
			}
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("error", "Failure");
			LOGGER.info("Module: Accounts : Method: saveAccounts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get the account type
	public String getAccountType(int accountTypeId) {
		LOGGER.info("Module: Accounts : Method: getAccountType");
		try {
			if (accountTypeId > 0) {
				Constants.Accounts accountTypes = new Constants.Accounts();
				switch (accountTo.getAccountTypeId()) {
				case 1:
					accountType = accountTypes.asset;
					break;
				case 2:
					accountType = accountTypes.liabilites;
					break;
				case 3:
					accountType = accountTypes.revenue;
					break;
				case 4:
					accountType = accountTypes.expenses;
					break;
				case 5:
					accountType = accountTypes.ownersEquity;
					break;
				}
			} else {
				accountType = "";
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showEntryPage: throws Exception "
					+ ex);
			return ERROR;
		}
		return accountType;
	}

	public Combination createBankAccountAnalysis(Combination combination,
			Implementation implementation, String accountName) throws Exception {
		Segment segment = combinationBL.getAccountBL().getSegmentBL()
				.getParentSegment(implementation, Segments.Analysis.getCode());
		Account account = accountService.getLastUpdatedAnalysis(implementation,
				segment.getSegmentId());
		account.setImplementation(implementation);
		account = updateAccount(account, segment, implementation, accountName);
		combination = combinationBL.getCombinationService().getCombination(
				Integer.parseInt(combination.getCombinationId().toString()));
		combination.setAccountByAnalysisAccountId(account);
		combination.setCombinationId(null);
		createCombination(combination);
		combinationBL.saveLedger(combination);
		return combination;
	}

	public Combination createCombination(String accountName, int accountType,
			Implementation implementation) {
		try {
			List<Combination> combinationList = combinationBL
					.getCombinationService().getAllCombination(implementation);
			Combination combination = new Combination();
			boolean flag = true;
			for (Combination list : combinationList) {
				if (null != list.getAccountByCostcenterAccountId()
						&& !list.getAccountByCostcenterAccountId().equals("")
						&& flag == true) {
					combination.setCombinationId(list.getCombinationId());
					combination.setAccountByCompanyAccountId(list
							.getAccountByCompanyAccountId());
					combination.setAccountByCostcenterAccountId(list
							.getAccountByCostcenterAccountId());
					combination.setAccountByNaturalAccountId(list
							.getAccountByNaturalAccountId());
					combination.setAccountByAnalysisAccountId(list
							.getAccountByAnalysisAccountId());
					combination.setAccountByBuffer1AccountId(list
							.getAccountByBuffer1AccountId());
					combination.setAccountByBuffer2AccountId(list
							.getAccountByBuffer2AccountId());
					flag = false;
				}
			}
			Account account = null;
			if (null != combination) {
				Segment segment = combinationBL
						.getAccountBL()
						.getSegmentBL()
						.getParentSegment(implementation,
								Segments.Natural.getCode());
				account = accountService.getLastUpdatedAnalysis(implementation,
						segment.getSegmentId());
				account.setAccountType(accountType);
				account = updateAccount(account, segment, implementation,
						accountName);
				combination.setAccountByNaturalAccountId(account);
			}
			combination.setCombinationId(null);
			combination = createCombination(combination);
			combinationBL.saveLedger(combination);
			return combination;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showEntryPage: throws Exception "
					+ ex);
			return null;
		}
	}

	private Account updateAccount(Account account, Segment segment,
			Implementation implementation, String accountCode) throws Exception {
		account.setAccount(accountCode);
		account.setImplementation(implementation);
		Integer code = Integer.parseInt(account.getCode().trim());
		code += 1;
		account.setCode(code.toString());
		account.setAccountId(generateAccountId(implementation,
				account.getCode()));
		account.setSegment(segment);
		accountService.updateAccount(account);
		return account;
	}

	public Account createAccount(Account account, Segment segment,
			Implementation implementation, String accountCode) throws Exception {
		account.setAccount(accountCode);
		account.setImplementation(implementation);
		Integer code = Integer.parseInt(account.getCode().trim());
		code += 1;
		account.setCode(code.toString());
		account.setAccountId(generateAccountId(implementation,
				account.getCode()));
		account.setSegment(segment);
		accountService.updateAccount(account);
		return account;
	}

	private Combination createCombination(Combination combination)
			throws Exception {
		combinationBL.getCombinationService().updateCombination(combination);
		return combination;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters and Setters
	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	public int getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public List<GLChartOfAccountsTO> getAccountList() {
		return accountList;
	}

	public void setAccountList(List<GLChartOfAccountsTO> accountList) {
		this.accountList = accountList;
	}

	public GLChartOfAccountsTO getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(GLChartOfAccountsTO accountTo) {
		this.accountTo = accountTo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public GLChartOfAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(GLChartOfAccountService accountService) {
		this.accountService = accountService;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public Long getSubType() {
		return subType;
	}

	public void setSubType(Long subType) {
		this.subType = subType;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Integer getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Integer processFlag) {
		this.processFlag = processFlag;
	}

	public String getSystemAccountCode() {
		return systemAccountCode;
	}

	public void setSystemAccountCode(String systemAccountCode) {
		this.systemAccountCode = systemAccountCode;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}
}
