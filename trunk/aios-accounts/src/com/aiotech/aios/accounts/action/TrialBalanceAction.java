package com.aiotech.aios.accounts.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.accounts.domain.entity.COATemplate;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.to.GLJournalVoucherTO;
import com.mysql.jdbc.Connection;

public class TrialBalanceAction {

	@SuppressWarnings("unchecked")
	public static void insertIntoCOATemplate() {
		Connection conn = null;
		try {
			List<COATemplate> templateList = null;
			conn = getConnection();
			QueryRunner queryRunner = new QueryRunner();
			String query = " SELECT  segment_id as accountId, account_type as accountType,"
					+ " account as account, code as code"
					+ " FROM ac_coa_template c";
			templateList = (List<COATemplate>) queryRunner.query(conn, query,
					new BeanListHandler(COATemplate.class));
			for (COATemplate list : templateList) {
				String query1 = " INSERT INTO ac_account(account_id, implementation_id, segment_id, account_type, account, code, is_system) "
						+ "values(?,?,?, ?,?,?,?)";
				Object[] params = {
						Long.parseLong(String
								.valueOf(2)
								.concat("14")
								.concat(String.valueOf(list.getCode().length())
										.concat(list.getCode()))), 10,
						list.getAccountId(), list.getAccountType(),
						list.getAccount(), list.getCode(), false };
				queryRunner.update(conn, query1, params);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void performTrialBalance() {
		Connection conn = null;
		try {
			List<Transaction> transactionList = null;
			conn = getConnection();
			QueryRunner queryRunner = new QueryRunner();
			String query = " SELECT transaction_id as transactionId"
					+ " FROM ac_transaction t"
					+ " WHERE t.transaction_time>= '2013-05-01'"
					+ " AND t.transaction_time<= '2013-05-31'"
					+ " AND implementation_id=" + 6
					+ " ORDER BY t.transaction_id";
			transactionList = (List<Transaction>) queryRunner.query(conn,
					query, new BeanListHandler(Transaction.class));
			List<GLJournalVoucherTO> transactionDetailList = null;
			closeConnection(conn);
			String detailQuery = "";
			for (Transaction list : transactionList) {
				conn = getConnection();
				detailQuery = " SELECT transaction_detail_id AS transactionDetailId,"
						+ " td.combination_id AS combinationId,"
						+ " td.amount AS balance,"
						+ " td.is_debit AS isDebit,"
						+ " na.account_type AS accountType,"
						+ " cl.calendar_id AS calendarId,"
						+ " p.period_id AS periodId,"
						+ " ld.ledger_id AS ledgerId"
						+ " FROM ac_transaction_detail td"
						+ " LEFT JOIN ac_combination cb"
						+ " ON cb.combination_id=td.combination_id"
						+ " LEFT JOIN ac_account na"
						+ " ON cb.natural_account_id=na.account_id"
						+ " LEFT JOIN ac_transaction tr"
						+ " ON td.transaction_id=tr.transaction_id"
						+ " LEFT JOIN ac_period p"
						+ " ON tr.period_id=p.period_id"
						+ " LEFT JOIN ac_calendar cl"
						+ " ON cl.calendar_id=p.calendar_id"
						+ " LEFT JOIN ac_ledger ld"
						+ " ON ld.combination_id=cb.combination_id"
						+ " WHERE td.transaction_id=" + list.getTransactionId();
				queryRunner = new QueryRunner();
				transactionDetailList = (List<GLJournalVoucherTO>) queryRunner
						.query(conn, detailQuery, new BeanListHandler(
								GLJournalVoucherTO.class));
				updateLedgerFiscal(transactionDetailList, conn);
				closeConnection(conn);
			}
		} catch (Exception e) {
			closeConnection(conn);
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked" })
	public static void updateLedger() {
		Connection connection = null;
		try {
			connection = getConnection();
			QueryRunner queryRunner = new QueryRunner();
			String query = " SELECT combination_id AS combinationId FROM ac_combination"
					+ " WHERE natural_account_id IN (SELECT account_id FROM ac_account)";
			List<GLJournalVoucherTO> combiantions = (List<GLJournalVoucherTO>) queryRunner
					.query(connection, query, new BeanListHandler(
							GLJournalVoucherTO.class));
			if (null != combiantions && combiantions.size() > 0) {
				for (GLJournalVoucherTO combiantion : combiantions) {
					queryRunner = new QueryRunner();
					String ledgerQuery = " SELECT ledger_id as ledgerId FROM ac_ledger WHERE combination_id = "
							+ combiantion.getCombinationId();
					GLJournalVoucherTO ledgerCombination = (GLJournalVoucherTO) queryRunner
							.query(connection, ledgerQuery, new BeanHandler(
									GLJournalVoucherTO.class));
					if (null == ledgerCombination) {
						queryRunner = new QueryRunner();
						String insertQuery = " INSERT INTO ac_ledger(ledger_id, combination_id, balance, side) values(?,?,?,?)";
						Object[] params = { null,
								combiantion.getCombinationId(), null, true };
						queryRunner.update(connection, insertQuery, params);
					}
				}
			}
			System.out.println("Done!!!");
		} catch (Exception ex) {
			closeConnection(connection);
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void getInventory() {
		Connection conn = null;
		try {
			conn = getConnection();
			QueryRunner queryRunner = new QueryRunner();
			String query = " SELECT product_combination_id as productCombinationId"
					+ " FROM ac_product WHERE implementation_id = "
					+ 6
					+ " AND item_type = 'I'";
			List<GLJournalVoucherTO> products = (List<GLJournalVoucherTO>) queryRunner
					.query(conn, query, new BeanListHandler(
							GLJournalVoucherTO.class));
			closeConnection(conn);
			Set<Long> productSet = new HashSet<Long>();
			for (GLJournalVoucherTO product : products) {
				productSet.add(product.getProductCombinationId());
			}
			conn = getConnection();
			query = " SELECT transaction_id as transactionId"
					+ " FROM ac_transaction t"
					+ " WHERE t.transaction_time>= '2013-04-01'"
					+ " AND t.transaction_time<= '2013-04-31'"
					+ " AND implementation_id=" + 6
					+ " ORDER BY t.transaction_id";
			List<Transaction> transactionList = (List<Transaction>) queryRunner
					.query(conn, query, new BeanListHandler(Transaction.class));
			List<GLJournalVoucherTO> transactionDetailList = null;
			closeConnection(conn);
			String detailQuery = "";
			for (Transaction transaction : transactionList) {
				conn = getConnection();
				detailQuery = " SELECT transaction_detail_id AS transactionDetailId,"
						+ " td.combination_id AS combinationId,"
						+ " td.amount AS balance,"
						+ " td.is_debit AS isDebit,"
						+ " na.account_type AS accountType,"
						+ " cl.calendar_id AS calendarId,"
						+ " p.period_id AS periodId,"
						+ " ld.ledger_id AS ledgerId"
						+ " FROM ac_transaction_detail td"
						+ " LEFT JOIN ac_combination cb"
						+ " ON cb.combination_id=td.combination_id"
						+ " LEFT JOIN ac_account na"
						+ " ON cb.natural_account_id=na.account_id"
						+ " LEFT JOIN ac_transaction tr"
						+ " ON td.transaction_id=tr.transaction_id"
						+ " LEFT JOIN ac_period p"
						+ " ON tr.period_id=p.period_id"
						+ " LEFT JOIN ac_calendar cl"
						+ " ON cl.calendar_id=p.calendar_id"
						+ " LEFT JOIN ac_ledger ld"
						+ " ON ld.combination_id=cb.combination_id"
						+ " WHERE td.transaction_id = "
						+ transaction.getTransactionId();
				queryRunner = new QueryRunner();
				transactionDetailList = (List<GLJournalVoucherTO>) queryRunner
						.query(conn, detailQuery, new BeanListHandler(
								GLJournalVoucherTO.class));
				List<GLJournalVoucherTO> productTransactions = new ArrayList<GLJournalVoucherTO>();
				for (GLJournalVoucherTO detail : transactionDetailList) {
					if (productSet.contains(detail.getCombinationId()))
						productTransactions.add(detail);
				}
				if (null != productTransactions
						&& productTransactions.size() > 0)
					updateLedgerFiscal(productTransactions, conn);
				closeConnection(conn);
			}
		} catch (Exception e) {
			closeConnection(conn);
			e.printStackTrace();
		}
	}

	public static void updateLedgerFiscal(
			List<GLJournalVoucherTO> transactionDetailList,
			Connection connection) throws Exception {
		LedgerFiscal ledgerFiscal = null;
		Combination combination = null;
		Period period = null;
		String ledgerQuery = "";
		Ledger ledger = null;
		QueryRunner queryRunner = new QueryRunner();
		for (GLJournalVoucherTO detail : transactionDetailList) {
			ledgerFiscal = new LedgerFiscal();
			combination = new Combination();
			period = new Period();
			ledger = new Ledger();
			combination
					.setCombinationId(Long.valueOf(detail.getCombinationId()));
			ledger.setLedgerId(detail.getLedgerId());
			period.setPeriodId(Long.valueOf(detail.getPeriodId()));
			ledgerQuery = " SELECT ledger_fiscal_Id AS ledgerFiscalId, side AS side, balance AS balance FROM ac_ledger_fiscal WHERE combination_id="
					+ combination.getCombinationId()
					+ " AND period_id="
					+ period.getPeriodId();
			queryRunner = new QueryRunner();
			ledgerFiscal = (LedgerFiscal) queryRunner.query(connection,
					ledgerQuery, new BeanHandler(LedgerFiscal.class));
			if (null == ledgerFiscal || ("").equals(ledgerFiscal)) {
				queryRunner = new QueryRunner();
				String query = " INSERT INTO ac_ledger_fiscal(ledger_fiscal_id, combination_id, period_id, balance, side) values(?,?,?, ?,?)";
				Object[] params = { null, ledger.getLedgerId(),
						combination.getCombinationId(), period.getPeriodId(),
						0d, true };
				queryRunner.update(connection, query, params);
				ledgerQuery = " SELECT ledger_fiscal_Id AS ledgerFiscalId FROM ac_ledger_fiscal WHERE combination_id="
						+ combination.getCombinationId()
						+ " AND period_id="
						+ period.getPeriodId();
				queryRunner = new QueryRunner();
				ledgerFiscal = (LedgerFiscal) queryRunner.query(connection,
						ledgerQuery, new BeanHandler(LedgerFiscal.class));
				ledgerFiscal.setBalance(0d);
				ledgerFiscal.setSide(true);
			} else {
				ledgerFiscal.setBalance(0d);
				ledgerFiscal.setSide(true);
			}
			ledgerFiscal = processUpdateLedgerFiscal(ledgerFiscal,
					detail.getAccountType(), detail);
			queryRunner = new QueryRunner();
			String updateFiscalQuery = " UPDATE ac_ledger_fiscal SET ledger_id="
					+ ledger.getLedgerId()
					+ ", combination_id="
					+ combination.getCombinationId()
					+ ", period_id="
					+ period.getPeriodId()
					+ ", balance="
					+ ledgerFiscal.getBalance()
					+ ", side="
					+ ledgerFiscal.getSide()
					+ " WHERE ledger_fiscal_id="
					+ ledgerFiscal.getLedgerFiscalId();
			queryRunner.update(connection, updateFiscalQuery);
		}
	}

	// Process Ledger Fiscal
	private static LedgerFiscal processUpdateLedgerFiscal(
			LedgerFiscal ledgerFiscal, int accountType,
			GLJournalVoucherTO transactionDetail) throws Exception {
		double ledgerBalance = ledgerFiscal.getSide() ? (-1 * ledgerFiscal
				.getBalance()) : ledgerFiscal.getBalance();
		double transactionAmount = transactionDetail.getIsDebit() ? (-1 * transactionDetail
				.getBalance()) : transactionDetail.getBalance();
		double calculationAmount = 0.0;
		if (accountType == 1 || accountType == 4) {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				}
			}
		} else {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount <= 0 ? true : false));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							- Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledgerFiscal.getBalance())
							+ Math.abs(transactionDetail.getBalance());
					ledgerFiscal.setBalance(Math.abs(calculationAmount));
					ledgerFiscal
							.setSide((calculationAmount >= 0 ? true : false));
				}
			}
		}
		return ledgerFiscal;
	}

	@SuppressWarnings("unused")
	private static void testConnection() {
		Connection connection = null;
		try {
			connection = getConnection();
			System.out.println("Connection sucess");
		} catch (Exception e) {
			System.out.println("Connection failure");
			e.printStackTrace();
			closeConnection(connection);
		} finally {
			closeConnection(connection);
		}
	}

	public static Connection getConnection() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection connect = null;
		try {
			connect = (Connection) DriverManager
					.getConnection("jdbc:mysql://erp.aio.ae/aios?"
							+ "user=root&password=D@a!o922#");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connect;
	}

	public static void closeConnection(Connection conn) {
		if (conn != null)
			DbUtils.closeQuietly(conn);
	}

	private int[] theArray = new int[50];
	private int arraySize = 10;

	public void generateRandomArray() {

		for (int i = 0; i < arraySize; i++) {
			theArray[i] = (int) (Math.random() * 10) + 10;
		}
	}

	public void printArray() {

		System.out.println("----------");

		for (int i = 0; i < arraySize; i++) {

			System.out.println("| " + i + " | " + theArray[i] + " |");

			System.out.println("----------");

		}
	}

	public int getValueAtIndex(int index) {

		if (index < arraySize)
			return theArray[index];
		return 0;
	}

	public static void main(String args[]) throws Exception {

		TrialBalanceAction tba = new TrialBalanceAction();

		// tba.generateRandomArray();
		// tba.printArray();
		// System.out.println("Index(4) " + tba.getValueAtIndex(4));
		// / testConnection();
		// TrialBalanceAction.performTrialBalance();
		// TrialBalanceAction.getInventory();
		// TrialBalanceAction.insertIntoCOATemplate();
		// TrialBalanceAction.updateLedger();
		// TrialBalanceAction.testConnection();

		// TrialBalanceAction.testProgram();
		// TrialBalanceAction.checkDate();

		/*
		 * List<String> products =
		 * TrialBalanceAction.validateProductDuplicates(); Map<String,
		 * ProductVO> productMap = new HashMap<String, ProductVO>(); ProductVO
		 * productVO = null; String noCode = "NO CODE"; for (String string :
		 * products) { String[] prdDetail = splitValues(string, ","); productVO
		 * = new ProductVO(); if (!("").equals(prdDetail) &&
		 * (noCode).equalsIgnoreCase(prdDetail[1]) &&
		 * Double.parseDouble(prdDetail[2]) > 0) { if
		 * (!productMap.containsKey(prdDetail[0])) {
		 * productVO.setProductName(prdDetail[0]);
		 * productVO.setDisplayPrice(prdDetail[2]);
		 * productMap.put(productVO.getProductName(), productVO); } }
		 * 
		 * }
		 * 
		 * FileWriter writer = new FileWriter("D:/test/Accessoriescsv1.csv");
		 * for (ProductVO prdVO : productMap.values()) {
		 * writer.append(prdVO.getProductName() + "," +
		 * prdVO.getDisplayPrice()); writer.append('\n'); } writer.flush();
		 * writer.close();
		 */

		/*
		 * List<String> customers = TrialBalanceAction.readWriteCustomers();
		 * CustomerVO customerVO = null; Map<String, CustomerVO> customerHash =
		 * new HashMap<String, CustomerVO>(); for (String string : customers) {
		 * String[] cstDetail = splitValues(string, ","); customerVO = new
		 * CustomerVO(); if (!("").equals(cstDetail) && cstDetail.length == 2 &&
		 * !cstDetail[0].trim().equals("") && !cstDetail[1].trim().equals("") &&
		 * !cstDetail[0].contains("Cancelled") &&
		 * !cstDetail[0].contains("CANCELLED")) { if
		 * (!customerHash.containsKey(cstDetail[1])) {
		 * customerVO.setCustomerName(cstDetail[0].trim());
		 * customerVO.setMobileNumber(cstDetail[1].trim());
		 * customerHash.put(customerVO.getMobileNumber(), customerVO); } } }
		 * FileWriter writer = new FileWriter("D:/test/ccustomercsv.csv"); for
		 * (CustomerVO cstVO : customerHash.values()) {
		 * writer.append(cstVO.getCustomerName() + "," +
		 * cstVO.getMobileNumber()); writer.append('\n'); } writer.flush();
		 * writer.close();
		 */
		tba.fact();
	}

	@SuppressWarnings("unused")
	private static List<String> validateProductDuplicates() {
		File file = new File("D://test//AccessoriesVasesXl.csv");
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static List<String> readWriteCustomers() {
		File file = new File("D://test//Customers.csv");
		List<String> files = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			boolean exists = file.exists();
			if (exists) {
				for (String line1 = br.readLine(); line1 != null; line1 = br
						.readLine()) {
					files.add(line1);
				}
			}
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	@SuppressWarnings("unused")
	private static void checkDate() {
		DateTimeFormatter formatter = DateTimeFormat
				.forPattern("dd/MM/yyyy HH:mm:ss");
		DateTime fromDate = formatter.parseDateTime("01/01/2014 00:00:00");
		DateTime endDate = formatter.parseDateTime("31/01/2014 00:00:00");
		int diff = Years.yearsBetween(fromDate, endDate).getYears();
		System.out.println(diff);
	}

	public List<String> getCombination(String text, int position,
			List<String> result) throws Exception {
		List<String> tempresult = new ArrayList<String>();
		char charArray[] = new char[text.length()];
		for (int i = 0; i < text.length(); i++)
			charArray[i] = text.charAt(i);
		int temppos = position;
		// forward
		result.add(String.valueOf(charArray[position]));
		for (int i = 0; i < charArray.length; i++) {
			if (temppos == (charArray.length - 1))
				break;
			temppos += 1;
			tempresult.addAll(addTextRecursivly(charArray, position, temppos,
					new ArrayList<String>()));
		}

		// reverse
		/*
		 * for (int i = 0; i < position; i++){
		 * tempresult.addAll(addReverseTextRecursivly(charArray, position, new
		 * ArrayList<String>())); }
		 */

		result.addAll(tempresult);
		if ((charArray.length - 1) != position)
			return getCombination(text, ++position, result);
		return result;
	}

	@SuppressWarnings("unused")
	private List<String> addReverseTextRecursivly(char[] charArray,
			int position, List<String> result) {
		String cn = "";
		for (int i = position; i < charArray.length; i++) {
			cn += charArray[i];
			result.add(String.valueOf(charArray[position]) + "" + cn);
		}
		position += 1;
		if (position < charArray.length)
			return addReverseTextRecursivly(charArray, position, result);
		return result;
	}

	private List<String> addTextRecursivly(char[] charArray, int position,
			int location, List<String> result) {
		String cn = "";
		for (int i = location; i < charArray.length; i++) {
			cn += charArray[i];
			result.add(String.valueOf(charArray[position]) + "" + cn);
		}
		location += 1;
		if (location < charArray.length)
			return addTextRecursivly(charArray, position, location, result);
		return result;
	}

	void fact() {
		int fact = fact(5);
		System.out.println(fact);
	}

	int fact(int n) {
		if (n <= 1)
			return 1;
		else
			return n * fact(n - 1);
	}

	void permute(String input) {
		int inputLength = input.length();
		boolean[] used = new boolean[inputLength];
		StringBuffer outputString = new StringBuffer();
		char[] in = input.toCharArray();

		doPermute(in, outputString, used, inputLength, 0);

	}

	void doPermute(char[] in, StringBuffer outputString, boolean[] used,
			int inputLength, int level) {
		if (level == inputLength) {
			System.out.println(outputString.toString());
			return;
		}

		for (int i = 0; i < inputLength; ++i) {

			if (used[i])
				continue;

			outputString.append(in[i]);
			used[i] = true;
			doPermute(in, outputString, used, inputLength, level + 1);
			used[i] = false;
			outputString.setLength(outputString.length() - 1);
		}
	}

	@SuppressWarnings("unused")
	private static void testProgram() {
		long total = 0;
		for (int i = 2;; i++) {
			long fib = fib(i);
			if (fib > 4000000)
				break;
			if (fib % 2 == 0)
				total += fib;
		}
		System.out.println(total);
	}

	public static long fib(int n) {
		if (n <= 1)
			return n;
		else
			return fib(n - 1) + fib(n - 2);
	}
}