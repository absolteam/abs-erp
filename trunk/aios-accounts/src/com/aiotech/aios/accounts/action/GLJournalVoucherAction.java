package com.aiotech.aios.accounts.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.domain.entity.TransactionTempDetail;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.accounts.to.GLJournalVoucherTO;
import com.aiotech.aios.accounts.to.converter.GLJournalVoucherTOConverter;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.common.WorkflowBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GLJournalVoucherAction extends ActionSupport {

	private static final long serialVersionUID = 7600431435439332099L;

	// Common Variables
	private String journalId;
	private String tempJournalId;
	private long currencyId;
	private int periodId;
	private int calendarId;

	private String journalDate;
	private String entryDate;
	private String description;
	private String combinationAccountList;
	private double totalAmount;
	private double linesAmount;
	private int journalLineId;
	private int combinationId;
	private String linesDecription;
	private Boolean isDebit;
	private int transactionFlag;
	private int accountTypeId;
	private String accountType;
	private String showPage;
	private String sqlReturnMessage;
	private long categoryId;
	private String transactionDetail;
	private String journalNumber;
	private String recordId;
	private String fromDate;
	private String toDate;
	private Integer id;
	private String rowId;
	private String processFlag;
	private long alertId;
	private long transactionId;

	// Common Object
	private TransactionBL transactionBL;
	private List<GLJournalVoucherTO> journalList = null;
	private GLJournalVoucherTO journalTo;
	private Implementation implementation;

	private WorkflowEnterpriseService workflowEnterpriseService;
	private WorkflowBL workflowBL;
	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(GLJournalVoucherAction.class);

	/** To redirect the action success page **/
	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		recordId = null;
		ServletActionContext.getRequest().setAttribute("rowId", rowId);
		return SUCCESS;
	}

	public String getCombinationView() {
		LOGGER.info("Module: Accounts : Method: getCombinationView");
		ServletActionContext.getRequest().setAttribute("accountType",
				accountTypeId);
		return SUCCESS;
	}

	public String showAllTransaction() {
		LOGGER.info("Module: Accounts : Method: showAllTransaction");
		try {
			JSONObject jsonResponse = new JSONObject();
			List<Transaction> transactionList = fetchTransactionList();

			JSONArray data = new JSONArray();
			if (transactionList != null && transactionList.size() > 0) {
				JSONArray array = null;
				for (Transaction list : transactionList) {
					array = new JSONArray();
					array.add(list.getTransactionId());
					array.add(list.getJournalNumber());
					array.add(DateFormat.convertDateToString(list
							.getTransactionTime().toString()));

					String debit = "";
					String credit = "";
					if (list.getTransactionDetails() != null
							&& list.getTransactionDetails().size() > 30) {

						for (TransactionDetail transactionDetail : list
								.getTransactionDetails()) {
							if (transactionDetail.getIsDebit())
								debit = transactionDetail.getAmount() + "...";
							else
								credit = transactionDetail.getAmount() + "...";
						}

					} else if (list.getTransactionDetails() != null
							&& list.getTransactionDetails().size() > 2) {

						for (TransactionDetail transactionDetail : list
								.getTransactionDetails()) {
							if (transactionDetail.getIsDebit())
								debit += transactionDetail.getAmount() + " | ";
							else
								credit += transactionDetail.getAmount() + " | ";
						}

					} else {

						for (TransactionDetail transactionDetail : list
								.getTransactionDetails()) {
							if (transactionDetail.getIsDebit())
								debit = transactionDetail.getAmount() + "";
							else
								credit = transactionDetail.getAmount() + "";
						}

					}
					array.add(debit);
					array.add(credit);
					array.add(list.getCategory().getName());
					array.add(DateFormat.convertDateToString(list
							.getEntryTime().toString()));
					array.add(list.getDescription());
					data.add(array);
				}

			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showAllTransaction: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllTransaction: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private List<Transaction> fetchTransactionList() throws Exception {
		Long combinationId = 0L;
		Date fromDatee = null;
		Date toDatee = null;
		getImplementId();
		if (ServletActionContext.getRequest().getParameter("categoryId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("categoryId")) > 0)
			categoryId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("categoryId"));
		if (ServletActionContext.getRequest().getParameter("combinationId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("combinationId")) > 0)
			combinationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("combinationId"));
		if (ServletActionContext.getRequest().getParameter("fromDate") != null)
			fromDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("fromDate").toString());

		if (ServletActionContext.getRequest().getParameter("toDate") != null)
			toDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("toDate").toString());

		List<Transaction> transactionDetails = transactionBL
				.getTransactionService().getTransactionFilter(implementation,
						categoryId, combinationId, fromDatee, toDatee);
		return transactionDetails;
	}

	public String filterTransaction() {
		LOGGER.info("Module: Accounts : Method: filterTransaction");
		try {
			JSONObject jsonResponse = new JSONObject();
			getImplementId();
			List<Transaction> transactionList = null;
			String[] dataArray = null;
			String queryStr = " WHERE t.implementation.implementationId="
					+ implementation.getImplementationId();
			if (null != combinationAccountList
					&& !("").equals(combinationAccountList)) {
				dataArray = new String[combinationAccountList.split(",").length];
				dataArray = combinationAccountList.split(",");
			}
			if (dataArray != null && dataArray.length > 1)
				queryStr += " AND td.combination.combinationId in ("
						+ combinationAccountList + ")";
			else if (dataArray != null && dataArray.length == 1)
				queryStr += " AND td.combination.combinationId=" + dataArray[0];
			if (categoryId > 0)
				queryStr += " AND t.category.categoryId =" + categoryId;

			if (null != fromDate && !("").equals(fromDate))
				queryStr += " AND t.transactionTime >='" + fromDate + "'";

			if (null != toDate && !("").equals(toDate))
				queryStr += " AND t.transactionTime <='" + toDate + "'";

			transactionList = this.getTransactionBL().getTransactionService()
					.getFilterJournalEntries(queryStr);
			JSONArray data = new JSONArray();
			if (transactionList != null && transactionList.size() > 0) {
				JSONArray array = null;
				for (Transaction list : transactionList) {
					array = new JSONArray();
					array.add(list.getTransactionId());
					array.add(list.getJournalNumber());
					array.add(DateFormat.convertDateToString(list
							.getTransactionTime().toString()));
					array.add(list.getPeriod().getName());
					array.add(list.getCurrency().getCurrencyPool().getCode());
					array.add(list.getCategory().getName());
					array.add(DateFormat.convertDateToString(list
							.getEntryTime().toString()));
					array.add(list.getDescription());
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showAllTransaction: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllTransaction: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showJournalVoucherList() {
		try {
			LOGGER.info("Module: Accounts : Method: showJournalVoucherList");
			JSONObject jsonResponse = new JSONObject();
			getImplementId();
			List<Transaction> transactionList = transactionBL
					.getTransactionService().getTransactionsWithOutPos(
							implementation, "PointOfSale");
			JSONArray data = new JSONArray();
			JSONArray array = null;
			if (transactionList != null && transactionList.size() > 0) {
				for (Transaction list : transactionList) {
					array = new JSONArray();
					array.add(list.getTransactionId());
					array.add(0);
					array.add(list.getJournalNumber());
					array.add(DateFormat.convertDateToString(list
							.getTransactionTime().toString()));
					String debit = "";
					String credit = "";
					if (list.getTransactionDetails() != null
							&& list.getTransactionDetails().size() > 30) {

						for (TransactionDetail transactionDetail : list
								.getTransactionDetails()) {
							if (transactionDetail.getIsDebit())
								debit = AIOSCommons
										.formatAmount(transactionDetail
												.getAmount())
										+ "...";
							else
								credit = AIOSCommons
										.formatAmount(transactionDetail
												.getAmount())
										+ "...";
						}

					} else if (list.getTransactionDetails() != null
							&& list.getTransactionDetails().size() > 2) {

						for (TransactionDetail transactionDetail : list
								.getTransactionDetails()) {
							if (transactionDetail.getIsDebit())
								debit += AIOSCommons
										.formatAmount(transactionDetail
												.getAmount())
										+ " | ";
							else
								credit += AIOSCommons
										.formatAmount(transactionDetail
												.getAmount())
										+ " | ";
						}

					} else {

						for (TransactionDetail transactionDetail : list
								.getTransactionDetails()) {
							if (transactionDetail.getIsDebit())
								debit = AIOSCommons
										.formatAmount(transactionDetail
												.getAmount());
							else
								credit = AIOSCommons
										.formatAmount(transactionDetail
												.getAmount());
						}

					}
					array.add(debit);
					array.add(credit);
					array.add(list.getCategory().getName());
					array.add(DateFormat.convertDateToString(list
							.getEntryTime().toString()));
					array.add(list.getDescription());
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showJournalVoucherList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showJournalVoucherList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to show journal entry page
	public String showJournalVoucherAddEntry() {
		LOGGER.info("Module: Accounts : Method: showJournalVoucherAddEntry");
		try {
			getImplementId();
			List<Currency> currencyList = this.getTransactionBL()
					.getCurrencyService().getAllCurrency(implementation);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
				}
			}
			List<Category> categories = transactionBL.getCategoryBL()
					.getCategoryService()
					.getAllActiveCategories(implementation);
			ServletActionContext.getRequest().setAttribute("CATEGORY_INFO",
					categories);
			totalAmount = 0;
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			LOGGER.info("Module: Accounts : Method: showJournalVoucherAddEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJournalVoucherAddEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get combination details
	@SuppressWarnings("unused")
	public String showCombinationDetails() {
		LOGGER.info("Module: Accounts : Method: showCombinationDetails");
		try {
			int i = 0;
			journalList = new ArrayList<GLJournalVoucherTO>();
			List<Combination> combinationList = new ArrayList<Combination>();
			getImplementId();
			if (accountTypeId > 0) {
				combinationList = this
						.getTransactionBL()
						.getTransactionService()
						.getFilterCombinations(
								Long.parseLong(accountTypeId + ""),
								implementation);
			} else {
				combinationList = this.getTransactionBL()
						.getTransactionService()
						.getCombinations(implementation);
			}
			journalList = GLJournalVoucherTOConverter
					.convertCodeToToList(combinationList);

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();

			if (null != journalList && journalList.size() > 0) {
				JSONArray array = null;
				for (GLJournalVoucherTO list : journalList) {

					array = new JSONArray();
					array.add(list.getCombinationId());
					array.add(list.getCompanyCode() + "["
							+ list.getCompanyDesc() + "]");
					array.add(list.getCostCode() + "[" + list.getCostDesc()
							+ "]");
					array.add(list.getNaturalCode() + "["
							+ list.getNaturalDesc() + "]");
					if (list.getAnalyisCode() != null
							&& !list.getAnalyisCode().equals(""))
						array.add(list.getAnalyisCode() + "["
								+ list.getAnalyisDesc() + "]");
					else
						array.add("");
					if (list.getBuffer1Code() != null
							&& !list.getBuffer1Code().equals(""))
						array.add(list.getBuffer1Code() + "["
								+ list.getBuffer1Desc() + "]");
					else
						array.add("");
					if (list.getBuffer2Code() != null
							&& !list.getBuffer2Code().equals(""))
						array.add(list.getBuffer2Code() + "["
								+ list.getBuffer2Desc() + "]");
					else
						array.add("");
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showCombinationDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showCombinationDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save add add lines entry
	@SuppressWarnings("unchecked")
	public String journalSaveAddAdd() {
		LOGGER.info("Module: Accounts : Method: journalSaveAddAdd");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			TransactionDetail transaction = new TransactionDetail();
			Combination combination = new Combination();
			List<TransactionDetail> transactionList = null;
			combination.setCombinationId(Long.parseLong(combinationId + ""));
			transaction.setCombination(combination);
			transaction.setAmount(linesAmount);
			transaction.setDescription(linesDecription);
			if (transactionFlag > 0)
				transaction.setIsDebit(TransactionType.Debit.getCode());
			else
				transaction.setIsDebit(TransactionType.Credit.getCode());
			transaction.setIsreceipt(null);
			if (session.getAttribute("JOURNALLINES_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				transactionList = new ArrayList<TransactionDetail>();
			else
				transactionList = (List<TransactionDetail>) session
						.getAttribute("JOURNALLINES_INFO_"
								+ sessionObj.get("jSessionId"));
			transactionList.add(transaction);
			session.setAttribute(
					"JOURNALLINES_INFO_" + sessionObj.get("jSessionId"),
					transactionList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalSaveAddAdd Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: journalSaveAddAdd: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to add edit the journal entry
	@SuppressWarnings("unchecked")
	public String journalSaveAddEdit() {
		LOGGER.info("Module: Accounts : Method: journalSaveAddEdit");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			TransactionDetail transaction = new TransactionDetail();
			Combination combination = new Combination();
			List<TransactionDetail> transactionList = null;
			combination.setCombinationId(Long.parseLong(combinationId + ""));
			transaction.setCombination(combination);
			transaction.setAmount(linesAmount);
			transaction.setDescription(linesDecription);
			if (transactionFlag > 0)
				transaction.setIsDebit(TransactionType.Debit.getCode());
			else
				transaction.setIsDebit(TransactionType.Credit.getCode());
			transaction.setIsreceipt(null);
			transactionList = (List<TransactionDetail>) session
					.getAttribute("JOURNALLINES_INFO_"
							+ sessionObj.get("jSessionId"));
			transactionList.set(id - 1, transaction);
			session.setAttribute(
					"JOURNALLINES_INFO_" + sessionObj.get("jSessionId"),
					transactionList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalSaveAddEdit Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: journalSaveAddEdit: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to add delete entry
	@SuppressWarnings("unchecked")
	public String journalSaveAddDelete() {
		LOGGER.info("Module: Accounts : Method: journalSaveAddDelete");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<TransactionDetail> transactionList = null;
			transactionList = (List<TransactionDetail>) session
					.getAttribute("JOURNALLINES_INFO_"
							+ sessionObj.get("jSessionId"));
			transactionList.remove(id - 1);
			session.setAttribute(
					"JOURNALLINES_INFO_" + sessionObj.get("jSessionId"),
					transactionList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalSaveAddDelete Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: journalSaveAddDelete: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save journal entry
	public String journalVoucherSave() {
		LOGGER.info("Module: Accounts : Method: journalVoucherSave");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Transaction transaction = new Transaction();  
			Currency defaultCurrency = transactionBL.getCurrency();
			if (null != defaultCurrency) {
				if ((long) defaultCurrency.getCurrencyId() != currencyId) {
					Currency currency = transactionBL.getCurrencyService()
							.getCurrency(currencyId);
					CurrencyConversion currencyConversion = transactionBL
							.getCurrencyConversionBL()
							.getCurrencyConversionService()
							.getCurrencyConversionByCurrency(currencyId);
					transaction.setCurrency(currency);
					transaction
							.setExchangeRate(null != currencyConversion ? currencyConversion
									.getConversionRate() : 1.0);
				} else {
					transaction.setCurrency(defaultCurrency);
					transaction.setExchangeRate(1.0);
				}
			}
			Period period = new Period();
			period.setPeriodId(Long.valueOf(periodId));
			getImplementId();
			transaction.setImplementation(implementation);
			transaction.setTransactionTime(DateFormat
					.convertStringToDate(journalDate));
			transaction.setEntryTime(Calendar.getInstance().getTime());
			if (categoryId > 0) {
				Category category = new Category();
				category.setCategoryId(categoryId);
				transaction.setCategory(category);
			}
			transaction.setDescription(description);
			transaction.setPeriod(period);
			transaction.setPerson(person);
			transaction.setCreatedDate(new Date());
			transaction.setJournalNumber(SystemBL.getReferenceStamp(
					Transaction.class.getName(), implementation));
			List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
			TransactionDetail transactionDetail1 = null;
			Combination combination = null;
			String[] transactionDetailList = splitValues(transactionDetail,
					"#@");
			try {
				for (String string : transactionDetailList) {
					String[] detailString = splitValues(string, "__");
					transactionDetail1 = new TransactionDetail();
					combination = new Combination();
					combination.setCombinationId(Long
							.parseLong(detailString[0]));
					transactionDetail1.setCombination(combination);
					if (detailString[1].equals("1"))
						transactionDetail1.setIsDebit(TransactionType.Debit
								.getCode());
					else
						transactionDetail1.setIsDebit(TransactionType.Credit
								.getCode());
					transactionDetail1.setAmount(Double.parseDouble(AIOSCommons
							.formatAmount(Double.parseDouble(detailString[2]))
							.replaceAll("\\,", "")));
					if (!detailString[4].equalsIgnoreCase("##"))
						transactionDetail1.setDescription(detailString[4]);
					transactionDetails.add(transactionDetail1);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			transactionBL.saveTransaction(transaction, transactionDetails,
					null, alertId, null);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalVoucherSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalVoucherSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to discard journal entries
	public String journalVoucherDiscard() {
		LOGGER.info("Module: Accounts : Method: journalVoucherDiscard");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("JOURNALLINES_INFO_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: journalVoucherDiscard Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: journalVoucherSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get journal details
	public String getJournalDetails() {
		LOGGER.info("Module: Accounts : Method: getJournalDetails");
		try {
			getImplementId();
			List<Currency> currencyList = this.getTransactionBL()
					.getCurrencyService().getAllCurrency(implementation);
			List<Category> categoryList = this.getTransactionBL()
					.getCategoryBL().getCategoryService()
					.getAllActiveCategories(implementation);
			Transaction transaction = null;
			CommentVO comment = null;
			List<TransactionDetail> transactionDetail = null;
			if (null != journalId && !("").equals(journalId)
					&& !("0").equals(journalId)) {
				recordId = "0";
				transaction = transactionBL.getTransactionService()
						.getTransactionById(Long.parseLong(journalId));
				transactionDetail = transactionBL.getTransactionService()
						.getJournalLinesById(Long.parseLong(journalId));
			} else {
				if (null != recordId && !("").equals(recordId)
						&& !("0").equals(recordId)) {
					comment = new CommentVO();
					comment.setRecordId(Long.parseLong(recordId));
					comment.setUseCase(Transaction.class.getName());
					comment = transactionBL.getCommentBL().getCommentInfo(
							comment);
					journalId = recordId;
				}
				transaction = transactionBL.getTransactionService()
						.getTransactionById(Long.parseLong(journalId));
				transactionDetail = new ArrayList<TransactionDetail>(
						transaction.getTransactionDetails());
			}
			totalAmount = 0;
			for (TransactionDetail list : transactionDetail) {
				if (list.getIsDebit())
					totalAmount += list.getAmount();
			}
			Collections.sort(transactionDetail,
					new Comparator<TransactionDetail>() {
						public int compare(TransactionDetail m1,
								TransactionDetail m2) {
							return m1.getTransactionDetailId().compareTo(
									m2.getTransactionDetailId());
						}
					});
			setRecordId("");
			ServletActionContext.getRequest().getSession()
					.setAttribute("COMMENT_INFO", comment);

			ServletActionContext.getRequest().setAttribute("JOURNAL_INFO",
					transaction);
			ServletActionContext.getRequest().setAttribute("JOURNAL_LINE_INFO",
					transactionDetail);
			ServletActionContext.getRequest().setAttribute("JOURNAL_LINE_SIZE",
					transactionDetail.size());
			ServletActionContext.getRequest().setAttribute("CATEGORY_INFO",
					categoryList);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);

			ServletActionContext.getRequest().setAttribute(
					"PERIOD_LIST",
					transactionBL.getCalendarBL().showSelectedDatePeriod(true,
							new DateTime(transaction.getTransactionTime())));
			LOGGER.info("Module: Accounts : Method: getJournalDetails Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: getJournalDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showTransactionApproval() {
		LOGGER.info("Module: Accounts : Method: showTransactionApproval");
		try {
			getImplementId();
			Transaction transaction = transactionBL.getTransactionService()
					.getTransactionById(Long.parseLong(recordId));
			List<TransactionDetail> transactionDetail = null;
			if (transaction != null) {
				transactionDetail = new ArrayList<TransactionDetail>(
						transaction.getTransactionDetails());
			}
			Collections.sort(transactionDetail,
					new Comparator<TransactionDetail>() {
						public int compare(TransactionDetail o1,
								TransactionDetail o2) {
							return o1.getTransactionDetailId().compareTo(
									o2.getTransactionDetailId());
						}
					});
			totalAmount = 0;
			for (TransactionDetail tempDetail : transactionDetail) {
				if (tempDetail.getIsDebit())
					totalAmount += tempDetail.getAmount();
			}
			ServletActionContext.getRequest().setAttribute("JOURNAL_INFO",
					transaction);
			ServletActionContext.getRequest().setAttribute("JOURNAL_LINE_INFO",
					transactionDetail);
			LOGGER.info("Module: Accounts : Method: showTransactionApproval Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showTransactionApproval: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String saveUpdateTransctionLedger() {
		LOGGER.info("Module: Accounts : Method: saveUpdateTransctionLedger()");
		try {
			getImplementId();
			TransactionTemp transactionTemp = this.getTransactionBL()
					.getTransactionTempService()
					.getJournalByTempId(Long.parseLong(recordId));
			List<TransactionTempDetail> transactionTempDetail = new ArrayList<TransactionTempDetail>(
					transactionTemp.getTransactionTempDetails());
			Transaction referenceTransaction = new Transaction();
			referenceTransaction = transactionTemp.getTransaction();
			if (null != transactionTemp.getIsDelete()
					&& !transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("1").equals(processFlag)) {
				Transaction transaction = new Transaction();
				TransactionDetail detailEntity = new TransactionDetail();
				List<TransactionDetail> transactionDetail = new ArrayList<TransactionDetail>();
				List<TransactionDetail> transactionProcessDetail = null;
				transaction.setPeriod(transactionTemp.getPeriod());
				transaction.setCategory(transactionTemp.getCategory());
				transaction.setCurrency(transactionTemp.getCurrency());
				transaction.setDescription(transactionTemp.getDescription());
				transaction.setTransactionTime(transactionTemp
						.getTransactionTime());
				transaction.setEntryTime(transactionTemp.getEntryTime());
				transaction.setPerson(transactionTemp.getPerson());
				transaction.setCreatedDate(transactionTemp.getCreatedDate());
				if (null != referenceTransaction
						&& !("").equals(referenceTransaction)) {
					transaction.setJournalNumber(referenceTransaction
							.getJournalNumber());
				} else {
					transaction.setJournalNumber(SystemBL.saveReferenceStamp(
							Transaction.class.getName(), implementation));
				}
				transaction.setImplementation(transactionTemp
						.getImplementation());
				for (TransactionTempDetail list : transactionTempDetail) {
					detailEntity = new TransactionDetail();
					detailEntity.setCombination(list.getCombination());
					detailEntity.setDescription(list.getDescription());
					detailEntity.setAmount(list.getAmount());
					detailEntity.setIsDebit(list.getIsDebit());
					detailEntity.setIsreceipt(list.getIsreceipt());
					transactionDetail.add(detailEntity);
				}
				if (null != transactionTemp.getTransaction()
						&& !("").equals(transactionTemp.getTransaction())) {
					transaction.setTransactionId(transactionTemp
							.getTransaction().getTransactionId());
					transactionProcessDetail = this
							.getTransactionBL()
							.getTransactionService()
							.getJournalLinesById(transaction.getTransactionId());

				}
				transactionBL.updateTransaction(transaction, transactionDetail,
						transactionProcessDetail, transactionTemp,
						transactionTempDetail);
			} else if (null != transactionTemp.getIsDelete()
					&& !transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("2").equals(processFlag)) {
				transactionTemp.setIsApprove(Byte.parseByte(String
						.valueOf(Constants.RealEstate.Status.Deny.getCode())));
				transactionBL.getTransactionTempService().saveTempTransaction(
						transactionTemp, transactionTempDetail);
			} else if (null != transactionTemp.getIsDelete()
					&& transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("1").equals(processFlag)) {
				journalId = String.valueOf(transactionTemp.getTransaction()
						.getTransactionId());
				Transaction transaction = this.getTransactionBL()
						.getTransactionService()
						.getJournalById(Long.parseLong(journalId));
				List<TransactionDetail> transactionDetails = this
						.getTransactionBL().getTransactionService()
						.getJournalLinesById(transaction.getTransactionId());
				transactionBL.deleteUpdateTransaction(transaction,
						transactionDetails, transactionTemp,
						transactionTempDetail);
			} else if (null != transactionTemp.getIsDelete()
					&& transactionTemp.getIsDelete() && null != processFlag
					&& !("").equals(processFlag) && ("2").equals(processFlag)) {
				transactionTemp.setIsApprove(Byte.parseByte(String
						.valueOf(Constants.RealEstate.Status.Deny.getCode())));
				transactionBL.getTransactionTempService().saveTempTransaction(
						transactionTemp, transactionTempDetail);
			}
			LOGGER.info("Module: Accounts : Method: saveUpdateTransctionLedger() Action Success");
			sqlReturnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					sqlReturnMessage);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveUpdateTransctionLedger() throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save journal entry edit-add
	@SuppressWarnings("unchecked")
	public String journalEditAddSave() {
		LOGGER.info("Module: Accounts : Method: journalEditAddSave");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			TransactionDetail transaction = new TransactionDetail();
			Transaction trans = new Transaction();
			Combination combination = new Combination();
			List<TransactionDetail> transactionList = null;
			combination.setCombinationId(Long.parseLong(combinationId + ""));
			transaction.setCombination(combination);
			transaction.setDescription(linesDecription);
			transaction.setAmount(linesAmount);
			trans.setTransactionId(Long.parseLong(journalId));
			transaction.setTransaction(trans);
			if (transactionFlag > 0)
				transaction.setIsDebit(TransactionType.Debit.getCode());
			else
				transaction.setIsDebit(TransactionType.Credit.getCode());
			transaction.setIsreceipt(null);
			if (session.getAttribute("JOURNALLINES_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				transactionList = new ArrayList<TransactionDetail>();
			else
				transactionList = (List<TransactionDetail>) session
						.getAttribute("JOURNALLINES_INFO_"
								+ sessionObj.get("jSessionId"));
			transactionList.add(transaction);
			session.setAttribute(
					"JOURNALLINES_INFO_" + sessionObj.get("jSessionId"),
					transactionList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalEditAddSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: journalEditAddSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save journal edit-edit entry
	@SuppressWarnings("unchecked")
	public String journalEditEditSave() {
		LOGGER.info("Module: Accounts : Method: journalEditEditSave");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			TransactionDetail transaction = new TransactionDetail();
			Transaction trans = new Transaction();
			Combination combination = new Combination();
			List<TransactionDetail> transactionList = null;
			combination.setCombinationId(Long.parseLong(combinationId + ""));
			transaction.setCombination(combination);
			transaction.setDescription(linesDecription);
			transaction.setAmount(linesAmount);
			transaction.setTransactionDetailId(Long.parseLong(journalLineId
					+ ""));
			trans.setTransactionId(Long.parseLong(journalId));
			transaction.setTransaction(trans);
			if (transactionFlag > 0)
				transaction.setIsDebit(TransactionType.Debit.getCode());
			else
				transaction.setIsDebit(TransactionType.Credit.getCode());
			transaction.setIsreceipt(null);
			transactionList = (List<TransactionDetail>) session
					.getAttribute("JOURNALLINES_INFO_"
							+ sessionObj.get("jSessionId"));
			transactionList.set(id - 1, transaction);
			session.setAttribute(
					"JOURNALLINES_INFO_" + sessionObj.get("jSessionId"),
					transactionList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalEditEditSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: journalEditEditSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save journal entry
	public String journalVoucherUpdate() {
		LOGGER.info("Module: Accounts : Method: journalVoucherUpdate");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Transaction transaction = new Transaction();
			Currency defaultCurrency = transactionBL.getCurrency();
			if (null != defaultCurrency) {
				if ((long) defaultCurrency.getCurrencyId() != currencyId) {
					Currency currency = transactionBL.getCurrencyService()
							.getCurrency(currencyId);
					CurrencyConversion currencyConversion = transactionBL
							.getCurrencyConversionBL()
							.getCurrencyConversionService()
							.getCurrencyConversionByCurrency(currencyId);
					transaction.setCurrency(currency);
					transaction
							.setExchangeRate(null != currencyConversion ? currencyConversion
									.getConversionRate() : 1.0);
				} else {
					transaction.setCurrency(defaultCurrency);
					transaction.setExchangeRate(1.0);
				}
			}
			Period period = new Period(); 
			period.setPeriodId(Long.valueOf(periodId));
			Category category = new Category();
			category.setCategoryId(categoryId);
			transaction.setCategory(category);
			getImplementId();
			transaction.setImplementation(implementation);
			transaction.setTransactionTime(DateFormat
					.convertStringToDate(journalDate));
			java.text.DateFormat dateFormat = new SimpleDateFormat(
					"yyyy/MM/dd HH:mm:ss");
			Date dateTime = new Date();
			dateFormat.format(dateTime);
			transaction.setEntryTime(dateTime);
			transaction.setDescription(description);
 			transaction.setPeriod(period);
			transaction.setJournalNumber(journalNumber);

			List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
			List<TransactionDetail> transactionDetailsDelete = new ArrayList<TransactionDetail>();
			List<TransactionDetail> reverseTransactionDetails = null;

			TransactionDetail transactionDetail1 = null;
			Combination combination = null;
			String[] transactionDetailList = splitValues(transactionDetail,
					"#@");
			try {
				for (String string : transactionDetailList) {
					String[] detailString = splitValues(string, "__");
					transactionDetail1 = new TransactionDetail();
					combination = new Combination();
					combination.setCombinationId(Long
							.parseLong(detailString[0]));
					transactionDetail1.setCombination(combination);
					if (detailString[1].equals("1"))
						transactionDetail1.setIsDebit(TransactionType.Debit
								.getCode());
					else
						transactionDetail1.setIsDebit(TransactionType.Credit
								.getCode());
					transactionDetail1.setAmount(Double.parseDouble(AIOSCommons
							.formatAmount(Double.parseDouble(detailString[2]))
							.replaceAll("\\,", "")));
					if (!detailString[4].equalsIgnoreCase("##"))
						transactionDetail1.setDescription(detailString[4]);
					if (Long.parseLong(detailString[3]) > 0)
						transactionDetail1.setTransactionDetailId(Long
								.parseLong(detailString[3]));
					transactionDetails.add(transactionDetail1);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (journalId != null && !("").equals(journalId)
					&& !("0").equals(journalId)) {
				transaction.setTransactionId(Long.parseLong(journalId));
				reverseTransactionDetails = transactionBL
						.getTransactionService().getJournalLinesById(
								transaction.getTransactionId());

				transactionDetailsDelete = userDeletedTransactionDetails(
						transactionDetails, reverseTransactionDetails);
			}
			transactionBL.saveTransaction(transaction, transactionDetails,
					transactionDetailsDelete, alertId,
					reverseTransactionDetails);

			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalVoucherUpdate Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalVoucherUpdate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private List<TransactionDetail> userDeletedTransactionDetails(
			List<TransactionDetail> transactionDetails,
			List<TransactionDetail> reverseTransactionDetails) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, TransactionDetail> hashTransactionDetails = new HashMap<Long, TransactionDetail>();
		if (null != reverseTransactionDetails
				&& reverseTransactionDetails.size() > 0) {
			for (TransactionDetail list : reverseTransactionDetails) {
				listOne.add(list.getTransactionDetailId());
				hashTransactionDetails.put(list.getTransactionDetailId(), list);
			}
			for (TransactionDetail list : transactionDetails) {
				listTwo.add(list.getTransactionDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
		if (null != different && different.size() > 0) {
			TransactionDetail tempTransactionDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempTransactionDetail = new TransactionDetail();
					tempTransactionDetail = hashTransactionDetails.get(list);
					transactionDetailList.add(tempTransactionDetail);
				}
			}
		}
		return transactionDetailList;
	}

	public String ledgerReset() {
		LOGGER.info("Module: Accounts : Method: ledgerReset");
		try {
			getImplementId();
			List<Transaction> transactionList = transactionBL
					.getTransactionService().getJournalEntries(implementation);
			transactionBL.resetLedger(transactionList);
			sqlReturnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: ledgerReset Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure " + ex.getMessage();
			LOGGER.info("Module: Accounts : Method: ledgerReset: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String calendarLedgerReset() {
		LOGGER.info("Module: Accounts : Method: calendarLedgerReset");
		try {
			getImplementId();
			List<Transaction> transactions = transactionBL
					.getTransactionService().getJournalEntriesByCalendar(
							implementation, calendarId);
			transactionBL.resetLedger(transactions);
			sqlReturnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: calendarLedgerReset Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure " + ex.getMessage();
			LOGGER.info("Module: Accounts : Method: calendarLedgerReset: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String calendarBigLedgerReset() {
		try {
			LOGGER.info("Module: Accounts : Method: calendarBigLedgerReset Inside");
			getImplementId();
			List<Transaction> transactions = transactionBL
					.getTransactionService().getBigJournalEntriesByCalendar(
							implementation, calendarId, transactionId);
			if (null != transactions && transactions.size() > 0) {
				transactionId = transactions.get(transactions.size() - 1)
						.getTransactionId();
				transactionBL.resetLedger(transactions);
			}
			LOGGER.info("Module: Accounts : Method: calendarBigLedgerReset Success");
			return SUCCESS;
		} catch (Exception ex) {
			transactionId = -1;
			LOGGER.info("Module: Accounts : Method: calendarBigLedgerReset Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String closePeriodTransaction() {
		try {
			LOGGER.info("Inside method closePeriodTransaction");
			String[] transactionDetailList = splitValues(transactionDetail,
					"#@");
			String propertyDetails = "";
			getImplementId();
			List<TransactionDetail> transactionDetails = null;
			TransactionDetail transactionDetail = null;
			List<Category> categoryList = this.getTransactionBL()
					.getCategoryBL().getCategoryService()
					.getAllActiveCategories(implementation);
			Category rentRevenueCategory = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Rent Revenue")) {
						rentRevenueCategory.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == rentRevenueCategory || rentRevenueCategory.equals("")) {
				rentRevenueCategory = this.getTransactionBL().getCategoryBL()
						.createCategory(implementation, "Rent Revenue");
			} else if (null == rentRevenueCategory.getCategoryId()) {
				rentRevenueCategory = this.getTransactionBL().getCategoryBL()
						.createCategory(implementation, "Rent Revenue");
			}
			try {
				for (String detail : transactionDetailList) {
					transactionDetails = new ArrayList<TransactionDetail>();
					String[] detailString = splitValues(detail, "__");
					propertyDetails = "Rent Revenue Credited";
					transactionDetail = this.getTransactionBL()
							.createTransactionDetail(
									Double.parseDouble(detailString[1]),
									Long.parseLong(detailString[0]), false,
									propertyDetails, null, null);
					transactionDetails.add(transactionDetail);
					propertyDetails = "UnEarned Revenue Debited";
					transactionDetail = this.getTransactionBL()
							.createTransactionDetail(
									Double.parseDouble(detailString[1]),
									implementation.getUnearnedRevenue(), true,
									propertyDetails, null, null);
					transactionDetails.add(transactionDetail);
					propertyDetails = "Rent Revenue";
					Transaction transaction = this
							.getTransactionBL()
							.createTransaction(
									transactionBL.getCurrency().getCurrencyId(),
									propertyDetails, new Date(),
									rentRevenueCategory, null, null);
					this.getTransactionBL().saveTransaction(transaction,
							transactionDetails);
				}
				this.getTransactionBL().closeAlert(alertId);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			sqlReturnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: closePeriodTransaction Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: closePeriodTransaction: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to delete the journal entry
	public String journalVoucherDelete() {
		LOGGER.info("Module: Accounts : Method: journalVoucherDelete");
		try {
			Transaction transaction = transactionBL.getTransactionService()
					.getTransactionById(Long.parseLong(journalId));
			transactionBL.deleteTransaction(transaction);
			sqlReturnMessage = "Record Deleted.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: journalVoucherDelete Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: journalVoucherDelete: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String directJVTransactionFromXLS() {
		try {
			sqlReturnMessage = "ERROR";
			boolean returnStatus = false;
			String[] filesNames = null;
			if (showPage != null && !showPage.equals(""))
				filesNames = splitValues(showPage, "#");
			for (String str : filesNames)
				returnStatus = transactionBL.autoJVMakingSet(transactionBL
						.fileReader(str));
			if (returnStatus) {
				sqlReturnMessage = "SUCCESS";
			}

			/*
			 * List<Transaction> transactions = new ArrayList<Transaction>();
			 * List<Period> periods = transactionBL.getCalendarBL()
			 * .getCalendarService() .findPeriodByUsingDateFromCalendar((long)
			 * 23); for (Period period : periods) {
			 * transactions.addAll(transactionBL.getTransactionService()
			 * .getTransactionsWithTransactionDetailsByPeriod
			 * (period.getPeriodId())); }
			 * transactionBL.directDeleteJV(transactions);
			 */
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			sqlReturnMessage = "ERROR";
			return SUCCESS;
		}
	}

	public String deleteLedger() {
		try {
			List<Ledger> naturalAccounts = transactionBL
					.getTransactionService().getNaturalLedgerList(
							transactionBL.getImplemenationId());
			Map<Long, List<Ledger>> ledgerMap = new HashMap<Long, List<Ledger>>();
			List<Ledger> tempLedgers = null;
			if (null != naturalAccounts && naturalAccounts.size() > 0) {
				for (Ledger ledger : naturalAccounts) {
					tempLedgers = new ArrayList<Ledger>();
					if (ledgerMap.containsKey(ledger.getCombination()
							.getCombinationId()))
						tempLedgers.addAll(ledgerMap.get(ledger
								.getCombination().getCombinationId()));
					tempLedgers.add(ledger);
					ledgerMap.put(ledger.getCombination().getCombinationId(),
							tempLedgers);
				}
			}

			List<Ledger> deletedLedgers = new ArrayList<Ledger>();
			for (Entry<Long, List<Ledger>> entry : ledgerMap.entrySet()) {
				tempLedgers = new ArrayList<Ledger>();
				tempLedgers.addAll(entry.getValue());
				if (tempLedgers.size() > 1) {
					int counter = 0;
					for (Ledger ledger : tempLedgers) {
						if (counter == 0)
							counter += 1;
						else {
							deletedLedgers.add(ledger);
							counter += 1;
						}
					}
				}
			}
			transactionBL.deleteLedgers(deletedLedgers);

			ledgerMap = new HashMap<Long, List<Ledger>>();
			deletedLedgers = new ArrayList<Ledger>();
			List<Ledger> analysisAccounts = transactionBL
					.getTransactionService().getAnalysisLedgerList(
							transactionBL.getImplemenationId());
			if (null != analysisAccounts && analysisAccounts.size() > 0) {
				for (Ledger ledger1 : analysisAccounts) {
					tempLedgers = new ArrayList<Ledger>();
					if (ledgerMap.containsKey(ledger1.getCombination()
							.getCombinationId()))
						tempLedgers.addAll(ledgerMap.get(ledger1
								.getCombination().getCombinationId()));
					tempLedgers.add(ledger1);
					ledgerMap.put(ledger1.getCombination().getCombinationId(),
							tempLedgers);
				}
			}

			transactionBL.deleteLedgers(deletedLedgers);
			sqlReturnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: deleteLedger Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "ERROR";
			return SUCCESS;
		}
	}

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	private String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getter&setters
	public String getJournalId() {
		return journalId;
	}

	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public int getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(int calendarId) {
		this.calendarId = calendarId;
	}

	public String getJournalDate() {
		return journalDate;
	}

	public void setJournalDate(String journalDate) {
		this.journalDate = journalDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getJournalLineId() {
		return journalLineId;
	}

	public void setJournalLineId(int journalLineId) {
		this.journalLineId = journalLineId;
	}

	public int getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(int combinationId) {
		this.combinationId = combinationId;
	}

	public String getLinesDecription() {
		return linesDecription;
	}

	public void setLinesDecription(String linesDecription) {
		this.linesDecription = linesDecription;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<GLJournalVoucherTO> getJournalList() {
		return journalList;
	}

	public void setJournalList(List<GLJournalVoucherTO> journalList) {
		this.journalList = journalList;
	}

	public GLJournalVoucherTO getJournalTo() {
		return journalTo;
	}

	public void setJournalTo(GLJournalVoucherTO journalTo) {
		this.journalTo = journalTo;
	}

	public Boolean isDebit() {
		return isDebit;
	}

	public void setDebit(Boolean isDebit) {
		this.isDebit = isDebit;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}

	public int getTransactionFlag() {
		return transactionFlag;
	}

	public void setTransactionFlag(int transactionFlag) {
		this.transactionFlag = transactionFlag;
	}

	public double getLinesAmount() {
		return linesAmount;
	}

	public void setLinesAmount(double linesAmount) {
		this.linesAmount = linesAmount;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public Boolean getIsDebit() {
		return isDebit;
	}

	public void setIsDebit(Boolean isDebit) {
		this.isDebit = isDebit;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public int getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(String transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public String getJournalNumber() {
		return journalNumber;
	}

	public void setJournalNumber(String journalNumber) {
		this.journalNumber = journalNumber;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public String getTempJournalId() {
		return tempJournalId;
	}

	public void setTempJournalId(String tempJournalId) {
		this.tempJournalId = tempJournalId;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}

	public String getCombinationAccountList() {
		return combinationAccountList;
	}

	public void setCombinationAccountList(String combinationAccountList) {
		this.combinationAccountList = combinationAccountList;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public WorkflowBL getWorkflowBL() {
		return workflowBL;
	}

	public void setWorkflowBL(WorkflowBL workflowBL) {
		this.workflowBL = workflowBL;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
