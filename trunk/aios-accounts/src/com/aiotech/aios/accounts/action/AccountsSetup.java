package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationVO;
import com.aiotech.aios.accounts.domain.entity.vo.SegmentVO;
import com.aiotech.aios.accounts.service.bl.CalendarBL;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.to.GlCalendarMasterTO;
import com.aiotech.aios.accounts.to.converter.GlCalendarMasterTOConverter;
import com.aiotech.aios.common.to.Constants.Accounts.PeriodStatus;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AccountsSetup extends ActionSupport {

	private static final long serialVersionUID = 4584071221652920306L;

	private static final Logger LOG = Logger.getLogger(AccountsSetup.class);

	private CalendarBL calendarBL;
	private CombinationBL combinationBL;
	private SystemService systemService;

	private String calendarId;

	public String showSetupWizard() {
		try {
			LOG.info("Method: showSetupWizard() inside");
			List<Implementation> implementations = systemService
					.getAllImplementation();
			ServletActionContext.getRequest().setAttribute("AllImplementation",
					implementations);
			ServletActionContext.getRequest().setAttribute(
					"newImplementationId",
					calendarBL.getImplementation().getImplementationId());
			LOG.info("Method: showSetupWizard() success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Method: showSetupWizard() Exception" + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAccountsSetup() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAccountsSetup: ");
			this.financialYearTabContent();
			this.chartOfAccountsTabContent();
			this.showCombinationTree();
			LOG.info("Module: Accounts : Method: showAddEntryPage: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Method: showAccountsSetup() Exception" + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String financialYearTabContent() {
		try {
			LOG.info("Inside method: financialYearTabContent");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Calendar> calendarList = calendarBL.getCalendarService()
					.getAllCalendar(calendarBL.getImplementation());
			Calendar calendar = null;
			List<Period> periodList = null;
			List<GlCalendarMasterTO> periodDetail = null;
			boolean flag = true;
			for (Calendar list : calendarList) {
				if (flag) {
					if (list.getStatus() == 1) {
						calendarId = String.valueOf(list.getCalendarId());
						flag = false;
					}
				}
			}
			if (!flag) {
				calendar = calendarBL.getCalendarService().getCalendarDetails(
						new Long(calendarId));
				periodList = this.getCalendarBL().getCalendarService()
						.getPeriodDetails(calendar.getCalendarId());
				Collections.sort(periodList, new Comparator<Period>() {
					public int compare(Period o1, Period o2) {
						return o1.getStartTime().compareTo(o2.getEndTime());
					}
				});
				GlCalendarMasterTO calenderMasterto = GlCalendarMasterTOConverter
						.convertToTO(calendar);
				if (periodList != null && periodList.size() > 0) {
					session.setAttribute(
							"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"),
							periodList);
					periodDetail = new ArrayList<GlCalendarMasterTO>();
					for (Period period : periodList) {
						GlCalendarMasterTO calendarPeriod = GlCalendarMasterTOConverter
								.convertPeroidToTO(period);
						periodDetail.add(calendarPeriod);
					}
				}
				Collections.sort(periodDetail,
						new Comparator<GlCalendarMasterTO>() {
							public int compare(GlCalendarMasterTO m1,
									GlCalendarMasterTO m2) {
								return m1.getPeriodStartDate().compareTo(
										m2.getPeriodStartDate());
							}
						});
				ServletActionContext.getRequest().setAttribute(
						"CALENDAR_DETAILS", calenderMasterto);
				ServletActionContext.getRequest().setAttribute(
						"PERIOD_DETAILS", periodDetail);
			}
			Map<Byte, String> periodStatus = new HashMap<Byte, String>();
			for (PeriodStatus status : EnumSet.allOf(PeriodStatus.class))
				periodStatus.put(status.getCode(),
						status.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("PERIOD_STATUS",
					periodStatus);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_STATUS",
					flag);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_YEAR",
					calendarList);
			LOG.info("Method: financialYearTabContent success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Method: financialYearTabContent Exception" + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private void chartOfAccountsTabContent() {
		try {
			LOG.info("Inside Module: Accounts : Method: showEntryPage: ");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ACCOUNT_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Segment> segments = combinationBL
					.getAccountBL()
					.getSegmentBL()
					.getSegmentService()
					.getDefaultSegments(
							combinationBL.getAccountBL().getImplementation());
			SegmentVO segmentVO = null;
			List<SegmentVO> childSegmentVOs = null;
			List<SegmentVO> segmentVOs = new ArrayList<SegmentVO>();
			for (Segment segment : segments) {
				segmentVO = new SegmentVO();
				childSegmentVOs = new ArrayList<SegmentVO>();
				segmentVO.setSegmentId(segment.getSegmentId());
				segmentVO.setSegmentName(segment.getSegmentName());
				segmentVO.setSegmentVOs(combinationBL.getAccountBL()
						.getSegmentBL()
						.getSegmentDirectChild(segment, childSegmentVOs));
				segmentVOs.add(segmentVO);
			}
			ServletActionContext.getRequest().setAttribute("SEGMENT_LIST",
					segmentVOs);
			LOG.info("Module: Accounts : Method: showAddEntryPage: Action Success");
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAddEntryPage: throws Exception "
					+ ex);
		}
	}

	public void showCombinationTree() {
		try {
			LOG.info("Module: Accounts : Method: showCombinationTree");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("SESSION_COMBINATION_TREE_"
					+ sessionObj.get("jSessionId"))
					&& !session.getAttribute(
							"SESSION_COMBINATION_TREE_"
									+ sessionObj.get("jSessionId")).equals("")) {
				session.removeAttribute("SESSION_COMBINATION_TREE_"
						+ sessionObj.get("jSessionId"));
			}
			List<CombinationVO> combinationVOList = combinationBL
					.generateCombinationTree(calendarBL.getImplementation(), 0l);
			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			if (null != combinationVOList && combinationVOList.size() > 0)
				session.setAttribute(
						"SESSION_COMBINATION_TREE_"
								+ sessionObj.get("jSessionId"),
						combinationVOList);
			LOG.info("Module: Accounts : Method: showCombinationTree Action Success");
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showCombinationTree: throws Exception "
					+ ex);
		}
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public CalendarBL getCalendarBL() {
		return calendarBL;
	}

	public void setCalendarBL(CalendarBL calendarBL) {
		this.calendarBL = calendarBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
}
