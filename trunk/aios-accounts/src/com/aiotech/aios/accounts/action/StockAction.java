package com.aiotech.aios.accounts.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.AisleVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.ShelfVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.domain.entity.vo.StoreVO;
import com.aiotech.aios.accounts.service.bl.StockBL;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author Saleem
 */
public class StockAction extends ActionSupport {

	private static final long serialVersionUID = -2720344217941242717L;

	// Logger
	private static final Logger LOG = Logger.getLogger(StockAction.class);

	// Variables
	private long stockId;
	private long productId;
	private long storeId;
	private int sectionId;
	private int rackId;
	private int shelfId;
	private int rowId;
	private int quantity;
	private long purchaseId;
	private boolean noPrint;
	private String itemType;
	private double minimumQuantity;
	private double maximumQuantity;
	private double unitRate;
	private String showPage;
	private StockVO stockVO;
	private List<Object> aaData;
	private String storeName;
	private String sqlReturnMessage;
	List<StockVO> barcodeDetails = new ArrayList<StockVO>();
	List<StockVO> stocks = new ArrayList<StockVO>();
	private String month;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	// Dependency
	private StockBL stockBL;

	public String returnSuccess() {
		return SUCCESS;
	}

	public String getStockCheckup() {
		String[] nameOfMonth = new DateFormatSymbols().getMonths();
		ServletActionContext.getRequest().setAttribute("MONTH_LIST",
				nameOfMonth);
		return SUCCESS;
	}

	// Show all stores
	public String showAllStores() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllStores");
			List<Store> stores = stockBL.getStoreBL().getStoreService()
					.getAllStores(stockBL.getImplementation());
			List<Product> products = stockBL.getProductPricingBL()
					.getProductBL().getProductService()
					.getActiveProduct(stockBL.getImplementation());
			ServletActionContext.getRequest()
					.setAttribute("STORE_INFO", stores);
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			LOG.info("Module: Accounts : Method: showAllStores Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showAllStores: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show all sections
	public String showStoreSection() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStoreSection");
			List<Aisle> aisles = stockBL.getStoreBL().getStoreService()
					.getAsileDetails(storeId);
			aaData = new ArrayList<Object>();
			if (null != aisles) {
				AisleVO aisleVO = null;
				for (Aisle aisle : aisles) {
					aisleVO = new AisleVO();
					aisleVO.setAisleId(aisle.getAisleId());
					aisleVO.setSectionName(aisle.getSectionName());
					aaData.add(aisleVO);
				}
			}
			LOG.info("Module: Accounts : Method: showStoreSection Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStoreSection: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductCostingPrice() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductCostingPrice");
			Product product = stockBL.getProductPricingBL().getProductBL()
					.getProductService().getBasicProductById(productId);
			unitRate = stockBL.getProductCostingPrice(product);
			LOG.info("Module: Accounts : Method: showProductCostingPrice Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductCostingPrice: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show all racks
	public String showStoreRacks() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStoreRacks");
			List<Shelf> shelfs = stockBL.getStoreBL().getStoreService()
					.getRacksBySectionId(sectionId);
			aaData = new ArrayList<Object>();
			if (null != shelfs) {
				ShelfVO shelfVO = null;
				for (Shelf shelf : shelfs) {
					shelfVO = new ShelfVO();
					shelfVO.setShelfId(shelf.getShelfId());
					shelfVO.setName(shelf.getName());
					aaData.add(shelfVO);
				}
			}
			LOG.info("Module: Accounts : Method: showStoreRacks Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStoreRacks: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductDefaultStore() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductDefaultStore");
			storeName = "";
			Product product = stockBL.getProductPricingBL().getProductBL()
					.getProductService().getProductById(productId);
			if (null != product.getShelf()) {
				storeName = product.getShelf().getShelf().getAisle().getStore()
						.getStoreName()
						+ ">>"
						+ product.getShelf().getShelf().getAisle()
								.getSectionName()
						+ ">>"
						+ product.getShelf().getShelf().getName()
						+ ">>"
						+ product.getShelf().getName();
			}
			LOG.info("Module: Accounts : Method: showStoreRacks Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductDefaultStore: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductStockTable() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductStockTable");
			List<StockVO> stockVOs = stockBL
					.getProductStockFullDetails(productId);
			if (null != stockVOs && stockVOs.size() > 0) {
				Shelf shelf = null;
				for (StockVO stockVO : stockVOs) {
					shelf = stockBL.getStoreBL().getStoreService()
							.getStoreByShelfId(stockVO.getShelf().getShelfId());
					stockVO.setShelf(shelf);
				}
			}
			ServletActionContext.getRequest().setAttribute("STOCK_DETAILS",
					stockVOs);
			LOG.info("Module: Accounts : Method: showStoreRacks Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductStockTable: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Generate barcode
	public String pdfPrintBarcode() {
		try {
			LOG.info("Inside Module: Accounts : Method: pdfPrintBarcode");
			stockVO = new StockVO();
			stocks = new ArrayList<StockVO>();
			barcodeDetails = new ArrayList<StockVO>();
			if (productId > 0 && quantity > 0 && storeId == 0) {
				List<StockVO> tempStocks = new ArrayList<StockVO>();
				Product product = stockBL.getProductPricingBL().getProductBL()
						.getProductService().getProductById(productId);
				StockVO storeDetail = new StockVO();
				storeDetail.setStoreName(product.getShelf().getShelf()
						.getAisle().getStore().getStoreName()
						+ "->");
				storeDetail.setAisleName(product.getShelf().getShelf()
						.getAisle().getSectionName()
						+ "->");
				storeDetail.setRackName(product.getShelf().getShelf().getName()
						+ "->");
				storeDetail.setShelfName(product.getShelf().getName());
				tempStocks.add(storeDetail);
				for (int i = 0; i < quantity; i++)
					tempStocks.add(setProductDetails(productId));
				barcodeDetails.addAll(tempStocks);
			} else if (productId > 0 && quantity > 0) {
				List<StockVO> tempStocks = null;
				Shelf shelf = stockBL.getStoreBL().getStoreService()
						.getStoreByShelfId(shelfId);
				tempStocks = new ArrayList<StockVO>();
				StockVO storeDetail = new StockVO();
				storeDetail.setStoreName(shelf.getShelf().getAisle().getStore()
						.getStoreName()
						+ "->");
				storeDetail.setAisleName(shelf.getShelf().getAisle()
						.getSectionName()
						+ "->");
				storeDetail.setRackName(shelf.getShelf().getName() + "->");
				storeDetail.setShelfName(shelf.getName());
				tempStocks.add(storeDetail);
				for (int i = 0; i < quantity; i++)
					tempStocks.add(setProductDetails(productId));
				barcodeDetails.addAll(tempStocks);
			} else {
				List<Store> storeList = stockBL
						.getStoreBL()
						.getStoreService()
						.getFilteredStoreDetails(stockBL.getImplementation(),
								storeId, productId);
				List<StoreVO> storeVOs = new ArrayList<StoreVO>();
				for (Store store : storeList) {
					StoreVO storeVO = new StoreVO(store);
					List<StockVO> stockVOs = new ArrayList<StockVO>();
					Map<Long, Long> productMap = new HashMap<Long, Long>();
					for (Stock stock : store.getStocks()) {
						if (!productMap.containsKey(stock.getProduct()
								.getProductId())) {
							List<StockVO> availStock = null;
							if (sectionId > 0 || rackId > 0 || shelfId > 0)
								availStock = stockBL.getStockDetails(stock
										.getProduct().getProductId(), store
										.getStoreId(), sectionId, rackId,
										shelfId);
							else
								availStock = stockBL.getStockByStoreDetails(
										stock.getProduct().getProductId(),
										store.getStoreId());
							if (availStock != null)
								stockVOs.addAll(availStock);
						}
						productMap.put(stock.getProduct().getProductId(),
								store.getStoreId());
					}
					storeVO.setStockVOs(stockVOs);
					storeVOs.add(storeVO);
				}

				Map<Integer, List<StockVO>> stockVOMap = new HashMap<Integer, List<StockVO>>();
				List<StockVO> tempStocks = null;
				for (StoreVO storeVO : storeVOs) {
					for (StockVO stockVO2 : storeVO.getStockVOs()) {
						if (null != stockVOMap && stockVOMap.size() > 0) {
							if (stockVOMap.containsKey(stockVO2.getShelf()
									.getShelfId())) {
								tempStocks = new ArrayList<StockVO>(
										stockVOMap.get(stockVO2.getShelf()
												.getShelfId()));
								if (noPrint)
									tempStocks.add(setProductDetails(stockVO2));
								else
									for (int i = 0; i < Math.round(stockVO2
											.getQuantity()); i++)
										tempStocks
												.add(setProductDetails(stockVO2));
							} else {
								Shelf shelf = stockBL
										.getStoreBL()
										.getStoreService()
										.getStoreByShelfId(
												stockVO2.getShelf()
														.getShelfId());
								tempStocks = new ArrayList<StockVO>();
								StockVO storeDetail = new StockVO();
								storeDetail.setStoreName(shelf.getShelf()
										.getAisle().getStore().getStoreName()
										+ "->");
								storeDetail.setAisleName(shelf.getShelf()
										.getAisle().getSectionName()
										+ "->");
								storeDetail.setRackName(shelf.getShelf()
										.getName() + "->");
								storeDetail.setShelfName(shelf.getName());
								tempStocks.add(storeDetail);
								if (noPrint)
									tempStocks.add(setProductDetails(stockVO2));
								else
									for (int i = 0; i < Math.round(stockVO2
											.getQuantity()); i++)
										tempStocks
												.add(setProductDetails(stockVO2));
							}
						} else {
							tempStocks = new ArrayList<StockVO>();
							Shelf shelf = stockBL
									.getStoreBL()
									.getStoreService()
									.getStoreByShelfId(
											stockVO2.getShelf().getShelfId());
							StockVO storeDetail = new StockVO();
							storeDetail.setStoreName(shelf.getShelf()
									.getAisle().getStore().getStoreName()
									+ "->");
							storeDetail.setAisleName(shelf.getShelf()
									.getAisle().getSectionName()
									+ "->");
							storeDetail.setRackName(shelf.getShelf().getName()
									+ "->");
							storeDetail.setShelfName(shelf.getName());
							tempStocks.add(storeDetail);
							if (noPrint)
								tempStocks.add(setProductDetails(stockVO2));
							else
								for (int i = 0; i < Math.round(stockVO2
										.getQuantity()); i++)
									tempStocks.add(setProductDetails(stockVO2));
						}
						stockVOMap.put(stockVO2.getShelf().getShelfId(),
								tempStocks);
					}
				}
				for (Entry<Integer, List<StockVO>> stockVO2 : stockVOMap
						.entrySet())
					barcodeDetails.addAll(stockVO2.getValue());
			}
			stockVO.setBarCodeDetails(barcodeDetails);
			stocks.add(stockVO);
			LOG.info("Module: Accounts : Method: pdfPrintBarcode Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: pdfPrintBarcode: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private StockVO setProductDetails(StockVO stockVO2) throws WriterException,
			IOException {
		StockVO stockVO = new StockVO();
		if (stockVO2.getProduct().getProductName().length() > 20)
			stockVO.setProductName(stockVO2.getProduct().getProductName()
					.substring(0, 20));
		else
			stockVO.setProductName(stockVO2.getProduct().getProductName());

		stockVO.setProductCode(stockVO2.getProduct().getBarCode());
		BitMatrix bitMatrix = new Code128Writer().encode(stockVO2.getProduct()
				.getBarCode(), BarcodeFormat.CODE_128, 240, 70);
		BufferedImage bi = MatrixToImageWriter.toBufferedImage(bitMatrix);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "png", baos);
		baos.flush();
		stockVO.setImage(bi);
		baos.close();
		return stockVO;
	}

	private StockVO setProductDetails(long productId) throws Exception {
		StockVO stockVO = new StockVO();
		Product product = stockBL.getProductPricingBL().getProductBL()
				.getProductService().getBasicProductById(productId);
		if (product.getProductName().length() > 20)
			stockVO.setProductName(product.getProductName().substring(0, 20));
		else
			stockVO.setProductName(product.getProductName());

		stockVO.setProductCode(product.getBarCode());
		BitMatrix bitMatrix = new Code128Writer().encode(product.getBarCode(),
				BarcodeFormat.CODE_128, 240, 70);
		BufferedImage bi = MatrixToImageWriter.toBufferedImage(bitMatrix);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "png", baos);
		baos.flush();
		stockVO.setImage(bi);
		baos.close();
		return stockVO;
	}

	// Show all racks
	public String showStoreShelfs() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStoreShelfs");
			List<Shelf> shelfs = stockBL.getStoreBL().getStoreService()
					.getShelfsByRackId(rackId);
			aaData = new ArrayList<Object>();
			if (null != shelfs) {
				ShelfVO shelfVO = null;
				for (Shelf shelf : shelfs) {
					shelfVO = new ShelfVO();
					shelfVO.setShelfId(shelf.getShelfId());
					shelfVO.setName(shelf.getName());
					aaData.add(shelfVO);
				}
			}
			LOG.info("Module: Accounts : Method: showStoreShelfs Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStoreShelfs: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show all racks
	public String showStoreShelfProducts() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStoreShelfProducts");
			List<Stock> stocks = stockBL.getStockService()
					.getProductsByShelfId(shelfId);
			aaData = new ArrayList<Object>();
			if (null != stocks) {
				Map<Long, ProductVO> productHash = new HashMap<Long, ProductVO>();
				ProductVO productVO = null;
				for (Stock stock : stocks) {
					if (!productHash.containsKey(stock.getProduct()
							.getProductId())) {
						productVO = new ProductVO();
						productVO.setProductId(stock.getProduct()
								.getProductId());
						productVO.setProductName(stock.getProduct()
								.getProductName());
						productHash.put(productVO.getProductId(), productVO);
						aaData.add(productVO);
					}
				}
			}
			LOG.info("Module: Accounts : Method: showStoreShelfProducts Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStoreShelfProducts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Stock Detail
	public String showStockDetails() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStockDetails");
			List<Stock> stocks = stockBL.getStockService()
					.getStockByStoreDetails(productId, storeId);
			if (null != stocks && stocks.size() > 0) {
				stockVO = stockBL.validateStocks(stocks);
				LOG.info("Module: Accounts : Method: showStockDetails Success");
				return SUCCESS;
			} else {
				stockVO = null;
				LOG.info("Module: Accounts : Method: showStockDetails expected result falied");
				return SUCCESS;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStockDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Stock Detail
	public String showStockDetailByProduct() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStockDetailByProduct");
			List<Stock> stocks = stockBL.getStockService()
					.getStockFullDetailsByProduct(productId);
			if (null != stocks && stocks.size() > 0) {
				stockVO = stockBL.validateStocks(stocks);
				LOG.info("Module: Accounts : Method: showStockDetailByProduct Success");
				return SUCCESS;
			} else {
				stockVO = null;
				LOG.info("Module: Accounts : Method: showStockDetailByProduct expected result falied");
				return SUCCESS;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStockDetailByProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Product Stock Details
	public String showProductStockDetails() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductStockDetails");
			if (itemType != null && !("").equals(itemType.trim()))
				aaData = stockBL.showProductStockDetail(itemType.charAt(0));
			else
				aaData = stockBL.showProductStockDetail();
			LOG.info("Module: Accounts : Method: showProductStockDetails Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductStockDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Product Stock Details
	public String showNonCraftProductStockDetails() {
		try {
			LOG.info("Inside Module: Accounts : Method: showNonCraftProductStockDetails");
			if (itemType != null && !("").equals(itemType.trim()))
				aaData = stockBL.showNonCraftProductStockDetails(itemType
						.charAt(0));
			else
				aaData = stockBL.showNonCraftProductStockDetails();
			LOG.info("Module: Accounts : Method: showNonCraftProductStockDetails Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showNonCraftProductStockDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Stock Reordering
	@SuppressWarnings("unchecked")
	public String showProductStockReorderList() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductStockReorderList");
			aaData = (List<Object>) stockBL.getProductReorder();
			LOG.info("Module: Accounts : Method: showProductStockReorderList Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductStockReorderList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Stock Reordering
	@SuppressWarnings("unchecked")
	public String showProductStockHtmlReport() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductStockHtmlReport");
			List<StockVO> stockVOs = (List<StockVO>) stockBL
					.getProductReorder();
			ServletActionContext.getRequest().setAttribute("REORDER_STOCKS",
					stockVOs);
			LOG.info("Module: Accounts : Method: showProductStockHtmlReport Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductStockHtmlReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Stock Product Details
	public String showStockByProduct() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStockByProduct");
			aaData = stockBL.showProductStockDetail(productId);
			LOG.info("Module: Accounts : Method: showStockByProduct Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStockByProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Product Stock Details
	public String showProductStockDetailsForCheckup() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductStockDetailsForCheckup");
			aaData = stockBL.showProductStockDetailByProduct();
			LOG.info("Module: Accounts : Method: showProductStockDetailsForCheckup Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductStockDetailsForCheckup: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Product Stock Details
	public String showCommonProductStockDetails() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCommonProductStockDetails");
			aaData = stockBL.showProductStockDetail();
			LOG.info("Module: Accounts : Method: showCommonProductStockDetails Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showCommonProductStockDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Product Stock Details
	public String showProductStoreStockDetails() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductStoreStockDetails");
			aaData = stockBL.showProductStockDetail(productId);
			LOG.info("Module: Accounts : Method: showProductStoreStockDetails Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showProductStoreStockDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show stock JSON list
	@SuppressWarnings("unchecked")
	public String showStockJsonList() {
		try {
			LOG.info("Inside Module: Accounts : Method: showStockJsonList");
			aaData = (List<Object>) stockBL.getStockReportVO(stockBL
					.getStockService().getStockReport(
							stockBL.getImplementation(), storeId, productId));
			List<Object> tempObjects = null;
			if (minimumQuantity > 0) {
				tempObjects = new ArrayList<Object>();
				StockVO stockVO = null;
				for (Object object : aaData) {
					stockVO = new StockVO();
					stockVO = (StockVO) object;
					if (stockVO.getAvailableQuantity() <= minimumQuantity) {
						tempObjects.add(stockVO);
					}
				}
				aaData.clear();
				aaData.addAll(tempObjects);
			}
			if (maximumQuantity > 0) {
				StockVO stockVO = null;
				tempObjects = new ArrayList<Object>();
				for (Object object : aaData) {
					stockVO = new StockVO();
					stockVO = (StockVO) object;
					if (stockVO.getAvailableQuantity() >= maximumQuantity) {
						tempObjects.add(stockVO);
					}
				}
				aaData.clear();
				aaData.addAll(tempObjects);
			}
			LOG.info("Module: Accounts : Method: showStockJsonList Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showStockJsonList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Upload stock Product
	public String uploadStockProducts() {
		try {
			LOG.info("Module: Accounts : Method: uploadStockProducts: Inside Action");
			List<Stock> deleteStocks = stockBL.getStockService()
					.getStockReport(stockBL.getImplementation(), 0, 0);
			stockBL.getStockService().deleteStocks(deleteStocks);

			List<Stock> stocks = stockBL
					.getProductPricingBL()
					.getProductBL()
					.addStockProducts(
							stockBL.getProductPricingBL().getProductBL()
									.productFileReader(showPage));
			stockBL.updateBatchStockDetails(stocks);
			sqlReturnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: uploadStockProducts Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure";
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: uploadStockProducts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Upload stock Product
	public String uploadStockProductsWithPeriod() {
		try {
			LOG.info("Module: Accounts : Method: uploadStockProductsWithPeriod: Inside Action");
			if (month == null || month.equals("")) {
				sqlReturnMessage = "Choose the month to upload";
				return SUCCESS;
			}
			List<Stock> stocks = stockBL
					.getProductPricingBL()
					.getProductBL()
					.addStockProducts(
							stockBL.getProductPricingBL().getProductBL()
									.productFileReader(showPage));
			stockBL.updatePeriodicStocks(month, stocks);
			sqlReturnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: uploadStockProductsWithPeriod Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure";
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: uploadStockProductsWithPeriod: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Upload stock Product
	public String updatePeriodicStock() {
		try {
			LOG.info("Module: Accounts : Method: updatePeriodicStock: Inside Action");
			if (month == null || month.equals("")) {
				LocalDate date = new LocalDate(new Date());
				LocalDate previousMonth = date.minus(Period.months(1));
				month = previousMonth.toString("MMMM,yyyy");
			}
			stockBL.updatePeriodicStock(month);
			sqlReturnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: updatePeriodicStock Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure";
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: updatePeriodicStock: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Getters and Setters
	public long getStockId() {
		return stockId;
	}

	public void setStockId(long stockId) {
		this.stockId = stockId;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	@JSON(name = "stockVO")
	public StockVO getStockVO() {
		return stockVO;
	}

	public void setStockVO(StockVO stockVO) {
		this.stockVO = stockVO;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public double getMinimumQuantity() {
		return minimumQuantity;
	}

	public void setMinimumQuantity(double minimumQuantity) {
		this.minimumQuantity = minimumQuantity;
	}

	public double getMaximumQuantity() {
		return maximumQuantity;
	}

	public void setMaximumQuantity(double maximumQuantity) {
		this.maximumQuantity = maximumQuantity;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public int getSectionId() {
		return sectionId;
	}

	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}

	public int getRackId() {
		return rackId;
	}

	public void setRackId(int rackId) {
		this.rackId = rackId;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public List<StockVO> getBarcodeDetails() {
		return barcodeDetails;
	}

	public void setBarcodeDetails(List<StockVO> barcodeDetails) {
		this.barcodeDetails = barcodeDetails;
	}

	public List<StockVO> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockVO> stocks) {
		this.stocks = stocks;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public boolean isNoPrint() {
		return noPrint;
	}

	public void setNoPrint(boolean noPrint) {
		this.noPrint = noPrint;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(double unitRate) {
		this.unitRate = unitRate;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
}
