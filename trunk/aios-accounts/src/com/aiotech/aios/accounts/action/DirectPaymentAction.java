package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.ChequeBook;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PaymentRequestDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.DirectPaymentVO;
import com.aiotech.aios.accounts.domain.entity.vo.TransactionDetailVO;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.common.to.Constants.Accounts.InstitutionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class DirectPaymentAction extends ActionSupport {

	private static final long serialVersionUID = -1028438198270554215L;
	private long directPaymentId;
	private long directPaymentDetailId;
	private long supplierId;
	private long cashCombinationId;
	private Long currencyId;
	private Integer id;
	private String description;
	private String paymentNumber;
	private String returnMessage;
	private String vendorName;
	private long cardAccountId;
	private String paymentDate;
	private String chequeDate;
	private String paymentLineDetails;
	private String payeeType;
	private long bankAccountId;
	private long chequeBookId;
	private double amount;
	private String invoiceNumber;
	private Integer chequeNumber;
	private String chequeFlag;
	private String linesDescription;
	private byte paymentMode;
	private Long recordId;
	private String useCase;
	private Long alertId;
	private long transferAccountId;
	private long paymentRequestId;
	private List<Object> aaData;
	private Implementation implementation;
	private DirectPaymentBL directPaymentBL;
	private Long messageId;
	private int processType;
	private boolean viewFromDirectPayment;
	private String workflowReturnMessage;
	private String printableContent;

	private static final Logger LOGGER = LogManager
			.getLogger(DirectPaymentAction.class);

	public String returnSuccess() {
		alertId = null;
		messageId = null;
		recordId = null;
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showDirectPaymentList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showDirectPaymentList");
			JSONObject jsonList = directPaymentBL
					.getDirectPaymentList(getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showDirectPaymentList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showDirectPaymentList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all Direct Payment Details
	public String showAllDirectPaymentDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showDirectPaymentList");
			aaData = directPaymentBL
					.getDirectPaymentDetails(getImplementationId());
			LOGGER.info("Module: Accounts : Method: showDirectPaymentList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showDirectPaymentList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showDirectPayment() {
		LOGGER.info("Inside Module: Accounts : Method: showDirectPayment");
		try {
			List<Currency> currencyList = directPaymentBL.getTransactionBL()
					.getCurrencyService().getAllCurrency(getImplementationId());
			paymentNumber = null;
			if (recordId != null && recordId > 0) {
				directPaymentId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (directPaymentId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = directPaymentBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}

			if (directPaymentId > 0) {
				DirectPayment directPayment = directPaymentBL
						.getDirectPaymentService().getDirectPaymentDetail(
								directPaymentId);
				ServletActionContext.getRequest().setAttribute(
						"DIRECT_PAYMENT", directPayment);
				ServletActionContext.getRequest().setAttribute(
						"DIRECT_PAYMENT_DETAIL",
						directPayment.getDirectPaymentDetails());

			} else
				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(),
						directPaymentBL.getImplementationId());
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}
			Combination combination = null;
			if (null != directPaymentBL.getImplementationId()
					.getCashAccount()) {
				combination = directPaymentBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								directPaymentBL.getImplementationId()
										.getCashAccount());
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("CASH_ACCOUNTS",
					combination);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
			viewFromDirectPayment = true;
			LOGGER.info("Module: Accounts : Method: showDirectPayment SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showDirectPaymentToView() {
		LOGGER.info("Inside Module: Accounts : Method: showDirectPayment");
		try {
			List<Currency> currencyList = directPaymentBL.getTransactionBL()
					.getCurrencyService().getAllCurrency(getImplementationId());
			directPaymentId = recordId;
			if (directPaymentId > 0) {
				DirectPayment directPayment = directPaymentBL
						.getDirectPaymentService().getDirectPaymentById(
								directPaymentId);
				List<DirectPaymentDetail> directPaymentList = this
						.getDirectPaymentBL().getDirectPaymentService()
						.getPaymentDetail(directPayment);

				ServletActionContext.getRequest().setAttribute(
						"DIRECT_PAYMENT", directPayment);
				ServletActionContext.getRequest().setAttribute(
						"DIRECT_PAYMENT_DETAIL", directPaymentList);
			} else
				paymentNumber = SystemBL.getReferenceStamp(
						DirectPayment.class.getName(),
						directPaymentBL.getImplementationId());
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteDirectPayment() {
		LOGGER.info("Module: Accounts : Method: deleteDirectPayment");
		try {
			DirectPayment directPayment = directPaymentBL
					.getDirectPaymentService().getDirectPaymentById(
							directPaymentId);
			if (null != directPayment.getVoidPayments()
					&& directPayment.getVoidPayments().size() > 0) {
				returnMessage = "Direct Payment marked as void, can't be delete.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: deleteDirectPayment Error");
				return ERROR;
			}
			directPaymentBL.deleteDirectPayment(directPayment);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteDirectPayment Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Internal Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteDirectPayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBankAccount() {
		LOGGER.info("Module: Accounts : Method: showBankAccount");
		try {
			List<Bank> bankList = directPaymentBL.getBankService().getAllBanks(
					getImplementationId(), InstitutionType.Bank.getCode());
			List<BankAccount> bankAccountList = directPaymentBL
					.getBankService().getAllBankAccounts(getImplementationId());
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			ServletActionContext.getRequest().setAttribute(
					"BANK_ACCOUNT_DETAILS", bankAccountList);
			ServletActionContext.getRequest().setAttribute("ROW_ID", id);
			LOGGER.info("Module: Accounts : Method: showBankAccount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Internal Error.";
			LOGGER.info("Module: Accounts : Method: showBankAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Direct Payment
	public String saveDirectPayment() {
		LOGGER.info("Module: Accounts : Method: saveDirectPayment");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			// Validate Period
			boolean openPeriod = directPaymentBL.getTransactionBL()
					.getCalendarBL().transactionPostingPeriod(paymentDate);

			if (!openPeriod) {
				returnMessage = "Period not open for the payment date.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveDirectPayment: (Error : Period not opened)");
				return SUCCESS;
			}

			DirectPayment directPayment = new DirectPayment();
			BankAccount bankAccount = null;
			Combination combination = null;
			Combination lineCombination = null;
			DirectPaymentDetail directPaymentDetail = null;
			List<DirectPaymentDetail> paymentDetailList = new ArrayList<DirectPaymentDetail>();
			List<DirectPaymentDetail> deletePaymentDetailList = null;
			List<DirectPaymentDetail> paymentsList = null;
			DirectPayment directPaymentDB = null;
			ChequeBook chequeBook = null;
			if (directPaymentId == 0 && recordId > 0 && null != useCase
					&& !("").equals(useCase)) {
				directPayment.setRecordId(recordId);
				directPayment.setUseCase(useCase);
				DirectPayment existingPayment = directPaymentBL
						.getDirectPaymentService()
						.getDirectPaymentByRecordIdAndUseCase(recordId, useCase);
				if (null != existingPayment && !("").equals(existingPayment))
					directPaymentId = existingPayment.getDirectPaymentId();
			}
			if (directPaymentId > 0) {
				directPayment.setDirectPaymentId(directPaymentId);
				directPaymentDB = directPaymentBL.getDirectPaymentService()
						.getDirectPaymentDetail(directPaymentId);
				directPayment.setPaymentNumber(directPaymentDB
						.getPaymentNumber());
				directPayment.setCurrency(directPaymentDB.getCurrency());
				directPayment
						.setExchangeRate(directPaymentDB.getExchangeRate());
			} else
				directPayment.setPaymentNumber(SystemBL.getReferenceStamp(
						DirectPayment.class.getName(),
						directPaymentBL.getImplementationId()));
			directPayment.setPaymentDate(DateFormat
					.convertStringToDate(paymentDate));
			if (null != chequeDate && !("").equals(chequeDate))
				directPayment.setChequeDate(DateFormat
						.convertStringToDate(chequeDate));

			if (directPaymentId == 0
					|| (null != directPaymentDB && (long) directPaymentDB
							.getCurrency().getCurrencyId() != currencyId)) {
				Currency defaultCurrency = directPaymentBL.getTransactionBL()
						.getCurrency();
				if (null != defaultCurrency) {
					if ((long) defaultCurrency.getCurrencyId() != currencyId) {
						Currency currency = directPaymentBL.getTransactionBL()
								.getCurrencyService().getCurrency(currencyId);
						CurrencyConversion currencyConversion = directPaymentBL
								.getTransactionBL().getCurrencyConversionBL()
								.getCurrencyConversionService()
								.getCurrencyConversionByCurrency(currencyId);
						directPayment.setCurrency(currency);
						directPayment
								.setExchangeRate(null != currencyConversion ? currencyConversion
										.getConversionRate() : 1.0);
					} else {
						directPayment.setCurrency(defaultCurrency);
						directPayment.setExchangeRate(1.0);
					}
				}
			}

			if (paymentRequestId > 0) {
				PaymentRequest paymentRequest = new PaymentRequest();
				paymentRequest.setPaymentRequestId(paymentRequestId);
				directPayment.setPaymentRequest(paymentRequest);
			}
			if (null != payeeType && !("").equals(payeeType)
					&& !("OT").equalsIgnoreCase(payeeType)) {
				if (("SPL").equalsIgnoreCase(payeeType)) {
					Supplier supplier = new Supplier();
					supplier.setSupplierId(supplierId);
					directPayment.setSupplier(supplier);
				} else if (("EMP").equalsIgnoreCase(payeeType)) {
					Person person = new Person();
					person.setPersonId(supplierId);
					directPayment.setPersonByPersonId(person);
				} else if (("CST").equalsIgnoreCase(payeeType)) {
					Customer customer = new Customer();
					customer.setCustomerId(supplierId);
					directPayment.setCustomer(customer);
				}
			} else
				directPayment.setOthers(vendorName);
			directPayment.setPaymentMode(paymentMode);
			if (bankAccountId > 0 && transferAccountId == 0
					&& cashCombinationId == 0) {
				bankAccount = directPaymentBL.getBankService()
						.getBankAccountInfo(bankAccountId);
				chequeBook = directPaymentBL.getChequeBookBL()
						.getChequeBookService()
						.findChequeBookById(chequeBookId);
				directPayment.setChequeBook(chequeBook);
				directPayment.setChequeNumber(chequeNumber);
				directPayment.setCombination(bankAccount.getCombination());
				directPayment.setBankAccount(bankAccount);
				if (directPaymentId > 0 || recordId > 0)
					directPayment.setChequeNumber(chequeNumber);
			} else if (cardAccountId > 0) {
				bankAccount = directPaymentBL.getBankService()
						.getBankAccountInfo(cardAccountId);
				directPayment.setBankAccount(bankAccount);
				directPayment.setCombination(bankAccount.getCombination());
			} else if (transferAccountId > 0) {
				bankAccount = directPaymentBL.getBankService()
						.getBankAccountInfo(transferAccountId);
				directPayment.setBankAccount(bankAccount);
				directPayment.setCombination(bankAccount.getCombination());
			} else {
				combination = new Combination();
				combination.setCombinationId(cashCombinationId);
				directPayment.setCombination(combination);
			}
			directPayment.setDescription(description);
			directPayment.setImplementation(getImplementationId());
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			directPayment.setPersonByCreatedBy(person);
			directPayment.setCreatedDate(new Date());

			String[] lineDetailArray = splitValues(paymentLineDetails, "@@");
			for (String string : lineDetailArray) {
				String[] detailList = splitValues(string, "__");
				directPaymentDetail = new DirectPaymentDetail();
				lineCombination = new Combination();
				lineCombination.setCombinationId(Long.parseLong(detailList[0]));
				directPaymentDetail.setCombination(lineCombination);
				directPaymentDetail
						.setAmount(Double.parseDouble(detailList[1]));
				if (null != detailList[2] && !detailList[2].equals("##"))
					directPaymentDetail.setDescription(detailList[2]);
				if (Long.parseLong(detailList[3]) > 0)
					directPaymentDetail.setDirectPaymentDetailId(Long
							.parseLong(detailList[3]));
				if (null != detailList[4] && !detailList[4].equals("##"))
					directPaymentDetail.setInvoiceNumber(detailList[4]);
				if (null != detailList[5] && Long.parseLong(detailList[5]) > 0) {
					PaymentRequestDetail requestDetail = new PaymentRequestDetail();
					requestDetail.setPaymentRequestDetailId(Long
							.parseLong(detailList[5]));
					directPaymentDetail.setPaymentRequestDetail(requestDetail);
				}
				if (null != detailList[7] && Long.parseLong(detailList[7]) > 0)
					directPaymentDetail.setRecordId(Long
							.parseLong(detailList[7]));
				if (null != detailList[6] && !detailList[6].equals("##"))
					directPaymentDetail.setUseCase(detailList[6]);
				paymentDetailList.add(directPaymentDetail);
			}
			if (directPaymentId > 0) {
				try {
					paymentsList = directPaymentBL.getDirectPaymentService()
							.getPaymentDetail(directPayment);
					if (null != paymentsList && paymentsList.size() > 0) {
						Collection<Long> listOne = new ArrayList<Long>();
						Collection<Long> listTwo = new ArrayList<Long>();
						Map<Long, DirectPaymentDetail> hashPayment = new HashMap<Long, DirectPaymentDetail>();
						for (DirectPaymentDetail list : paymentsList) {
							listOne.add(list.getDirectPaymentDetailId());
							hashPayment.put(list.getDirectPaymentDetailId(),
									list);
						}
						if (null != hashPayment && hashPayment.size() > 0) {
							for (DirectPaymentDetail list : paymentDetailList) {
								listTwo.add(list.getDirectPaymentDetailId());
							}
						}
						Collection<Long> similar = new HashSet<Long>(listOne);
						Collection<Long> different = new HashSet<Long>();
						different.addAll(listOne);
						different.addAll(listTwo);
						similar.retainAll(listTwo);
						different.removeAll(similar);
						if (null != different && different.size() > 0) {
							DirectPaymentDetail tempPaymentDetail = null;
							deletePaymentDetailList = new ArrayList<DirectPaymentDetail>();
							for (Long list : different) {
								if (null != list && !list.equals(0)) {
									tempPaymentDetail = new DirectPaymentDetail();
									tempPaymentDetail = hashPayment.get(list);
									deletePaymentDetailList
											.add(tempPaymentDetail);
								}
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					returnMessage = "Internal Error.";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					LOGGER.info("Module: Accounts : Method: saveDirectPayment: throws Exception "
							+ ex);
					return ERROR;
				}
			}

			// Workflow Params
			DirectPaymentVO directPaymentVO = new DirectPaymentVO();
			directPaymentVO.setMessageId(messageId);
			directPaymentVO.setProcessType(processType);
			directPaymentBL.saveDirectPayments(directPayment, directPaymentDB,
					paymentDetailList, paymentsList, chequeBook,
					deletePaymentDetailList, directPaymentVO);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			directPaymentId = 0;
			returnMessage = "Internal Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveDirectPayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String directPaymentPDCClearance() { 
		try {
			LOGGER.info("Module: Accounts : Method: directPaymentPDCClearance");
			DirectPayment directPayment = directPaymentBL
					.getDirectPaymentService().getDirectPaymentDetail(
							directPaymentId);
			directPaymentBL.postTransactionAgainstPDCCheque(directPayment);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: directPaymentPDCClearance: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Internal Error."; 
			LOGGER.info("Module: Accounts : Method: directPaymentPDCClearance: throws Exception "
					+ ex);
			return ERROR;
		}
	}
	
	// create transaction and update print status
	public String printDirectPayment() {
		LOGGER.info("Module: Accounts : Method: printDirectPayment");
		try {
			DirectPayment directPayment = directPaymentBL
					.getDirectPaymentService().getDirectPaymentDetail(
							directPaymentId);
			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAILS",
					directPayment.getDirectPaymentDetails());

			LOGGER.info("Module: Accounts : Method: printDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Internal Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: printDirectPayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Direct Payment Print
	public String printViewDirectPayment() {
		LOGGER.info("Module: Accounts : Method: printViewDirectPayment");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			// Removing Sessions
			session.removeAttribute("DIRECT_PAYMENT_"
					+ sessionObj.get("jSessionId"));
			DirectPayment directPayments = directPaymentBL
					.getDirectPaymentService().getDirectPaymentDetail(
							directPaymentId);
			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayments);
			List<Transaction> transactions = directPaymentBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionByRecordIdAndUseCaseDetail(directPaymentId,
							DirectPayment.class.getSimpleName());
			List<TransactionDetailVO> transationDetailVOs = new ArrayList<TransactionDetailVO>();
			TransactionDetailVO transactionDetailVO = null;
			for (Transaction transaction : transactions) {
				for (TransactionDetail transactionDetail : transaction
						.getTransactionDetails()) {
					transactionDetailVO = new TransactionDetailVO();
					BeanUtils.copyProperties(transactionDetailVO,
							transactionDetail);
					transactionDetailVO
							.setAccountDescription(transactionDetail
									.getCombination()
									.getAccountByCompanyAccountId()
									.getAccount()
									+ "."
									+ transactionDetail.getCombination()
											.getAccountByCostcenterAccountId()
											.getAccount()
									+ "."
									+ transactionDetail.getCombination()
											.getAccountByNaturalAccountId()
											.getAccount()
									+ ((null != transactionDetail
											.getCombination()
											.getAccountByAnalysisAccountId() ? "."
											+ transactionDetail
													.getCombination()
													.getAccountByAnalysisAccountId()
													.getAccount()
											+ (null != transactionDetail
													.getCombination()
													.getAccountByBuffer1AccountId() ? "."
													+ transactionDetail
															.getCombination()
															.getAccountByBuffer1AccountId()
															.getAccount()
													+ (null != transactionDetail
															.getCombination()
															.getAccountByBuffer2AccountId() ? "."
															+ transactionDetail
																	.getCombination()
																	.getAccountByBuffer2AccountId()
																	.getAccount()
															: "")
													: "")
											: "")));
					transactionDetailVO.setDebitAmount(transactionDetail
							.getIsDebit() ? AIOSCommons
							.formatAmount(transactionDetail.getAmount()) : "");
					transactionDetailVO.setCreditAmount(!transactionDetail
							.getIsDebit() ? AIOSCommons
							.formatAmount(transactionDetail.getAmount()) : "");
					transationDetailVOs.add(transactionDetailVO);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"DIRECTPAYMENT_TRANSACTIONS", transationDetailVOs);

			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAILS",
					directPayments.getDirectPaymentDetails());

			session.setAttribute(
					"DIRECT_PAYMENT_" + sessionObj.get("jSessionId"),
					directPayments);

			LOGGER.info("Module: Accounts : Method: printViewDirectPayment: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Internal Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: printViewDirectPayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public String showDirectPaymentAddRow() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showDirectPaymentAddRow");
			ServletActionContext.getRequest().setAttribute("ROW_ID", id);
			LOGGER.info("Module: Accounts : Method: showDirectPaymentAddRow Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showDirectPaymentAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updateDirectPaymentTransaction() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: updateDirectPaymentTransaction");
			List<DirectPayment> directPayments = directPaymentBL
					.getDirectPaymentService().getAllDirectPaymentsOnly(
							directPaymentBL.getImplementationId());
			if (null != directPayments && directPayments.size() > 0)
				directPaymentBL.updateDirectPaymentTransaction(directPayments);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: showDirectPaymentAddRow Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: updateDirectPaymentTransaction Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String chequePaymentVoucherPrint() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: chequePaymentVoucherPrint");
			DirectPayment directPayment = directPaymentBL
					.getDirectPaymentService().getDirectPaymentById(
							directPaymentId);

			Configuration config = new Configuration();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ directPaymentBL.getImplementationId()
							.getImplementationId() + "/cashbank/cheque/"));

			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			// includes leading forward slash
			String contextPath = ServletActionContext.getRequest()
					.getContextPath();

			String urlPath = scheme + "://" + host + contextPath;

			Template template = null;
			if (directPayment.getBankAccount().getAccountNumber()
					.equals("1014453387801"))
				template = config.getTemplate("cheque-voucher.ftl");
			else
				template = config.getTemplate("cheque-leaf.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();
			rootMap.put("refNumber", directPayment.getPaymentNumber());
			rootMap.put("voucherDate", DateFormat
					.convertDateToString(directPayment.getPaymentDate()
							.toString()));

			if (null != directPayment.getSupplier()) {
				if (null != directPayment.getSupplier().getPerson())
					rootMap.put(
							"payeeName",
							directPayment
									.getSupplier()
									.getPerson()
									.getFirstName()
									.concat(" ")
									.concat(directPayment.getSupplier()
											.getPerson().getLastName()));
				else
					rootMap.put("payeeName", null != directPayment
							.getSupplier().getCompany() ? directPayment
							.getSupplier().getCompany().getCompanyName()
							: directPayment.getSupplier().getCmpDeptLocation()
									.getCompany().getCompanyName());
			} else if (null != directPayment.getCustomer()) {
				if (null != directPayment.getCustomer().getPersonByPersonId())
					rootMap.put(
							"payeeName",
							directPayment
									.getCustomer()
									.getPersonByPersonId()
									.getFirstName()
									.concat(" ")
									.concat(directPayment.getCustomer()
											.getPersonByPersonId()
											.getLastName()));
				else
					rootMap.put("payeeName", null != directPayment
							.getCustomer().getCompany() ? directPayment
							.getCustomer().getCompany().getCompanyName()
							: directPayment.getCustomer().getCmpDeptLocation()
									.getCompany().getCompanyName());
			} else if (null != directPayment.getPersonByPersonId()) {
				rootMap.put(
						"payeeName",
						directPayment
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(directPayment.getPersonByPersonId()
										.getLastName()));
			} else
				rootMap.put("payeeName", directPayment.getOthers());

			double totalAmount = 0;
			for (DirectPaymentDetail payDetail : directPayment
					.getDirectPaymentDetails())
				totalAmount += payDetail.getAmount();
			rootMap.put("chequeAmount", AIOSCommons.formatAmount(totalAmount));
			rootMap.put("chequeDate", DateFormat
					.convertDateToString(directPayment.getChequeDate()
							.toString()));
			rootMap.put("paymentDescription", directPayment.getDescription());
			rootMap.put("totalAmount", AIOSCommons.formatAmount(totalAmount));

			String decimalAmount = String.valueOf(AIOSCommons
					.formatAmount(totalAmount));
			decimalAmount = decimalAmount.substring(decimalAmount
					.lastIndexOf('.') + 1);
			String amountInWords = AIOSCommons
					.convertAmountToWords(totalAmount);
			if (Double.parseDouble(decimalAmount) > 0) {
				amountInWords = amountInWords
						.concat(" and ")
						.concat(AIOSCommons.convertAmountToWords(decimalAmount))
						.concat(" Fils ");
				String replaceWord = "Only";
				amountInWords = amountInWords.replaceAll(replaceWord, "");
				amountInWords = amountInWords.concat(" " + replaceWord);
			}
			rootMap.put("amountInWords", amountInWords);
			rootMap.put("urlPath", urlPath);
			Writer out = new StringWriter();
			template.process(rootMap, out);
			printableContent = out.toString();
			LOGGER.info("Module: Accounts : Method: chequePaymentVoucherPrint Success");
			return "template";
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: chequePaymentVoucherPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public long getDirectPaymentId() {
		return directPaymentId;
	}

	public void setDirectPaymentId(long directPaymentId) {
		this.directPaymentId = directPaymentId;
	}

	public long getDirectPaymentDetailId() {
		return directPaymentDetailId;
	}

	public void setDirectPaymentDetailId(long directPaymentDetailId) {
		this.directPaymentDetailId = directPaymentDetailId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Integer getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(Integer chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getLinesDescription() {
		return linesDescription;
	}

	public void setLinesDescription(String linesDescription) {
		this.linesDescription = linesDescription;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentLineDetails() {
		return paymentLineDetails;
	}

	public void setPaymentLineDetails(String paymentLineDetails) {
		this.paymentLineDetails = paymentLineDetails;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public long getCashCombinationId() {
		return cashCombinationId;
	}

	public void setCashCombinationId(long cashCombinationId) {
		this.cashCombinationId = cashCombinationId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public String getChequeFlag() {
		return chequeFlag;
	}

	public void setChequeFlag(String chequeFlag) {
		this.chequeFlag = chequeFlag;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public long getCardAccountId() {
		return cardAccountId;
	}

	public void setCardAccountId(long cardAccountId) {
		this.cardAccountId = cardAccountId;
	}

	public String getPayeeType() {
		return payeeType;
	}

	public void setPayeeType(String payeeType) {
		this.payeeType = payeeType;
	}

	public long getChequeBookId() {
		return chequeBookId;
	}

	public void setChequeBookId(long chequeBookId) {
		this.chequeBookId = chequeBookId;
	}

	public long getTransferAccountId() {
		return transferAccountId;
	}

	public void setTransferAccountId(long transferAccountId) {
		this.transferAccountId = transferAccountId;
	}

	public long getPaymentRequestId() {
		return paymentRequestId;
	}

	public void setPaymentRequestId(long paymentRequestId) {
		this.paymentRequestId = paymentRequestId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public int getProcessType() {
		return processType;
	}

	public void setProcessType(int processType) {
		this.processType = processType;
	}

	public boolean isViewFromDirectPayment() {
		return viewFromDirectPayment;
	}

	public void setViewFromDirectPayment(boolean viewFromDirectPayment) {
		this.viewFromDirectPayment = viewFromDirectPayment;
	}

	public String getWorkflowReturnMessage() {
		return workflowReturnMessage;
	}

	public void setWorkflowReturnMessage(String workflowReturnMessage) {
		this.workflowReturnMessage = workflowReturnMessage;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
