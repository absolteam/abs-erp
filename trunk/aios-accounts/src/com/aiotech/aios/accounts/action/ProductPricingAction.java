/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingCalcVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingVO;
import com.aiotech.aios.accounts.service.bl.ProductPricingBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationSubType;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author Saleem
 */
public class ProductPricingAction extends ActionSupport {

	private static final long serialVersionUID = 1464579394884211105L;

	// Dependency
	private ProductPricingBL productPricingBL;

	// Logger
	private static final Logger LOG = Logger
			.getLogger(ProductPricingAction.class);

	// Variables
	private long productPricingId;
	private long pricingType;
	private long productId;
	private long storeId;
	private long customerId;
	private long productPricingDetailId;
	private double priceValue;
	private double basicCostPrice;
	private double standardPrice;
	private double suggestedRetailPrice;
	private double sellingPrice;
	private byte calculationType;
	private byte calculationSubType;
	private long currencyId;
	private int priceDetailMirrorId;
	private int parentId;
	private int rowId;
	private String startDate;
	private String endDate;
	private String calculatedPrice;
	private String calculationDetails;
	private String description;
	private String pricingTitle;
	private String returnMessage;
	private String pageInfo;
	private String pricingDetailIds;
	private List<Object> aaData;
	private Long recordId;

	// Return to listing page
	public String returnSuccess() {
		try {
			LOG.info("Inside Module: Accounts : Method: returnSuccess");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("PRODUCT_PRICING_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PRICING_DELETED_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			recordId = null;
			LOG.info("Module: Accounts : Method: returnSuccess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: returnSuccess Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all Product Pricing
	public String showAllProductPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllProductPricing");
			aaData = productPricingBL.getAllProductPricing();
			LOG.info("Module: Accounts : Method: showAllProductPricing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllProductPricing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all Product Pricing
	public String showAllProductDetailPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllProductDetailPricing");
			aaData = productPricingBL.getProductPricingDetail();
			LOG.info("Module: Accounts : Method: showAllProductDetailPricing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllProductDetailPricing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showProductPricingApproval(){
		try {
			LOG.info("Inside Module: Accounts : Method: showProductPricingApproval");
			productPricingId = recordId;
			showProductPricingEntry();
			LOG.info("Module: Accounts : Method: showProductPricingApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductPricingApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showProductPricingRejectEntry(){
		try {
			LOG.info("Inside Module: Accounts : Method: showProductPricingRejectEntry");
			productPricingId = recordId;
			showProductPricingEntry();
			LOG.info("Module: Accounts : Method: showProductPricingRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductPricingRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Pricing Entry
	public String showProductPricingEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductPricingEntry");
			ProductPricingVO productPricingVO = null;
			if (productPricingId > 0) {
				ProductPricing productPricing = productPricingBL
						.getProductPricingService().getProductPricing(
								productPricingId);
				productPricingVO = new ProductPricingVO();
				BeanUtils.copyProperties(productPricingVO, productPricing);
				List<ProductPricingDetailVO> productPricingDetailVOs = new ArrayList<ProductPricingDetailVO>();
				ProductPricingDetailVO productPricingDetailVO = null;
				for (ProductPricingDetail pricingDetail : productPricing
						.getProductPricingDetails()) {
					productPricingDetailVO = new ProductPricingDetailVO();
					BeanUtils.copyProperties(productPricingDetailVO,
							pricingDetail);
					productPricingDetailVOs.add(productPricingDetailVO);
				}

				if (null != productPricingDetailVOs) {
					Collections.sort(productPricingDetailVOs,
							new Comparator<ProductPricingDetailVO>() {
								public int compare(ProductPricingDetailVO p1,
										ProductPricingDetailVO p2) {
									return p1
											.getProductPricingDetailId()
											.compareTo(
													p2.getProductPricingDetailId());
								}
							});
				}
				productPricingVO
						.setProductPricingDetailVOs(productPricingDetailVOs);
				
				CommentVO comment = new CommentVO();
				if (recordId != null && productPricing != null
						&& productPricing.getProductPricingId() != 0 && recordId > 0) {
					comment.setRecordId(productPricing.getProductPricingId());
					comment.setUseCase(ProductPricing.class.getName());
					comment = productPricingBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			List<Currency> currencies = productPricingBL.getCurrencyService()
					.getAllCurrency(productPricingBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_ALL",
					currencies);
			ServletActionContext.getRequest().setAttribute("PRODUCT_PRICING",
					productPricingVO);
			LOG.info("Module: Accounts : Method: showProductPricingEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductPricingEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Pricing Calculation
	@SuppressWarnings("unchecked")
	public String showPricingCalculation() {
		try {
			LOG.info("Inside Module: Accounts : Method: showPricingCalculation");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Integer, ProductPricingDetail> pricingDetailSession = (Map<Integer, ProductPricingDetail>) (session
					.getAttribute("PRODUCT_PRICING_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			List<ProductPricingCalcVO> productPricingCalcVOs = null;
			List<ProductPricingCalcVO> productPricingCalcFinalVOs = new ArrayList<ProductPricingCalcVO>();
			if (productPricingDetailId > 0
					&& (null == pricingDetailSession
							|| pricingDetailSession.size() == 0 || !pricingDetailSession
							.containsKey(parentId))) {
				List<ProductPricingCalc> productPricingCalc = productPricingBL
						.getProductPricingService()
						.getActiveProductPricingCalcByDetailId(
								productPricingDetailId);
				productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
				productPricingCalcVOs
						.addAll(addPricingCalcList(productPricingCalc));
				productPricingCalcFinalVOs.addAll(productPricingCalcVOs);
			} else {
				Set<ProductPricingCalc> productPricingCalcs = null;
				if (null != pricingDetailSession
						&& pricingDetailSession.size() > 0) {
					if (pricingDetailSession.containsKey(parentId)) {
						productPricingCalcs = new HashSet<ProductPricingCalc>();
						ProductPricingDetail productPricingDetail = pricingDetailSession
								.get(parentId);
						productPricingCalcs = productPricingDetail
								.getProductPricingCalcs();
						productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
						productPricingCalcVOs
								.addAll(addPricingCalcList(productPricingCalcs));
					}
				}
				if (null != productPricingCalcVOs
						&& productPricingCalcVOs.size() > 0) {
					double salesPrice = 0;
					double salesAmount = 0;
					for (ProductPricingCalcVO productPricingCalcVO : productPricingCalcVOs) {
						if (null != productPricingCalcVO.getComboType()
								&& (boolean) !productPricingCalcVO
										.getComboType()) {
							if ((byte) productPricingCalcVO
									.getCalculationType() == (byte) ProductCalculationType.Fixed_Price_Margin
									.getCode()
									|| (byte) productPricingCalcVO
											.getCalculationType() == (byte) ProductCalculationType.Fixed_Price_Margin
											.getCode()) {
								salesPrice = productPricingCalcVO
										.getSalePrice();
								salesAmount = productPricingCalcVO
										.getSalePriceAmount();
								productPricingCalcVO.setSalePrice(salesAmount);
								productPricingCalcVO
										.setSalePriceAmount(salesPrice);
							}
							productPricingCalcFinalVOs
									.add(productPricingCalcVO);
						}
					}
				}
			}
			Map<Byte, String> productCalculationType = new HashMap<Byte, String>();
			for (ProductCalculationType e : EnumSet
					.allOf(ProductCalculationType.class)) {
				productCalculationType.put(e.getCode(), e.name().toString()
						.replaceAll("_", " "));
			}
			Map<Byte, ProductCalculationSubType> productCalculationSubType = new HashMap<Byte, ProductCalculationSubType>();
			for (ProductCalculationSubType e : EnumSet
					.allOf(ProductCalculationSubType.class)) {
				productCalculationSubType.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("CALCULATION_TYPE",
					productCalculationType);
			ServletActionContext.getRequest().setAttribute(
					"CALCULATION_SUB_TYPE", productCalculationSubType);
			ServletActionContext.getRequest().setAttribute(
					"PRICING_TYPE",
					productPricingBL
							.getProductBL()
							.getLookupMasterBL()
							.getActiveLookupDetails("PRODUCT_PRICING_LEVEL",
									true));
			ServletActionContext.getRequest().setAttribute(
					"PRICING_CALCULATIONS", productPricingCalcFinalVOs);
			LOG.info("Module: Accounts : Method: showPricingCalculation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showPricingCalculation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<ProductPricingCalcVO> addPricingCalcList(
			Collection<? extends ProductPricingCalc> productPricingCalc)
			throws Exception {
		List<ProductPricingCalcVO> productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
		ProductPricingCalcVO productPricingCalcVO = null;
		for (ProductPricingCalc pricingCalc : productPricingCalc) {
			if (null != pricingCalc.getLookupDetail()) {
				productPricingCalcVO = new ProductPricingCalcVO();
				BeanUtils.copyProperties(productPricingCalcVO, pricingCalc);
				if (null != pricingCalc.getStore()
						&& null != pricingCalc.getStore().getStoreId()) {
					Store store = productPricingBL.getProductBL().getStoreBL()
							.getStoreService()
							.getStoreById(pricingCalc.getStore().getStoreId());
					productPricingCalcVO.setStoreName(store.getStoreName());
				}
				if (null != pricingCalc.getCustomer()
						&& null != pricingCalc.getCustomer().getCustomerId()) {
					Customer customer = productPricingBL
							.getCustomerBL()
							.getCustomerService()
							.getCustomerDetails(
									pricingCalc.getCustomer().getCustomerId());
					String customerName = "";
					if (null != customer.getPersonByPersonId()) {
						customerName = customer.getPersonByPersonId()
								.getFirstName()
								+ " "
								+ customer.getPersonByPersonId().getLastName();
					} else if (null != customer.getCmpDeptLocation()) {
						customerName = customer.getCmpDeptLocation()
								.getCompany().getCompanyName();
					} else if (null != customer.getCompany()) {
						customerName = customer.getCompany().getCompanyName();
					}
					productPricingCalcVO.setCustomerName(customerName);
				}
				productPricingCalcVOs.add(productPricingCalcVO);
			}
		}
		return productPricingCalcVOs;
	}

	public String getCustomerSalesPrice() {
		try {
			LOG.info("Inside Module: Accounts : Method: getCustomerSalesPrice");
			sellingPrice = 0;
			ProductPricingCalc productPricingCalc = productPricingBL
					.getCustomerSalesPrice(productId, customerId);
			if (null != productPricingCalc) {
				if (Constants.Accounts.ProductCalculationType.get(
						productPricingCalc.getCalculationType()).equals(
						Constants.Accounts.ProductCalculationType.Margin)
						|| Constants.Accounts.ProductCalculationType
								.get(productPricingCalc.getCalculationType())
								.equals(Constants.Accounts.ProductCalculationType.Markup))
					sellingPrice = productPricingCalc.getSalePrice();
				else
					sellingPrice = productPricingCalc.getSalePriceAmount();
			}
			LOG.info("Module: Accounts : Method: getCustomerSalesPrice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: getCustomerSalesPrice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getCustomerStoreSalesPrice() {
		try {
			LOG.info("Inside Module: Accounts : Method: getCustomerStoreSalesPrice");
			ProductPricingCalc productPricingCalc = productPricingBL
					.getCustomerStoreSalesPrice(productId, customerId, storeId);
			if (null != productPricingCalc) {
				if (Constants.Accounts.ProductCalculationType.get(
						productPricingCalc.getCalculationType()).equals(
						Constants.Accounts.ProductCalculationType.Margin)
						|| Constants.Accounts.ProductCalculationType
								.get(productPricingCalc.getCalculationType())
								.equals(Constants.Accounts.ProductCalculationType.Markup))
					sellingPrice = productPricingCalc.getSalePrice();
				else
					sellingPrice = productPricingCalc.getSalePriceAmount();
			}
			LOG.info("Module: Accounts : Method: getCustomerStoreSalesPrice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: getCustomerStoreSalesPrice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Calculated Price Value
	public ProductPricingCalc calculateProductPrice(
			ProductPricingDetail productPricingDetail,
			ProductPricingCalc productPricingCalc) throws Exception {

		LOG.info("Inside Module: Accounts : Method: calculateProductPrice");
		ProductCalculationType productCalculationType = ProductCalculationType
				.get(productPricingCalc.getCalculationType());
		double priceDetail = 0;
		if (productCalculationType.name().equalsIgnoreCase("markup")) {
			priceDetail = (productPricingDetail.getBasicCostPrice() * productPricingCalc
					.getSalePrice()) / 100;
			productPricingCalc.setSalePriceAmount(productPricingDetail
					.getBasicCostPrice() + priceDetail);
		} else if (productCalculationType.name().equalsIgnoreCase("margin")) {
			priceDetail = (productPricingDetail.getStandardPrice() / productPricingDetail
					.getBasicCostPrice())
					* productPricingCalc.getSalePrice()
					/ 100;
			priceDetail = (productPricingDetail.getStandardPrice() * (priceDetail * 100)) / 100;
			productPricingCalc.setSalePriceAmount(productPricingDetail
					.getStandardPrice() + priceDetail);
		} else if (productCalculationType.name().equalsIgnoreCase(
				"fixed_price_markup")) {
			priceDetail = (productPricingCalc.getSalePriceAmount() - productPricingDetail
					.getBasicCostPrice())
					/ productPricingDetail.getBasicCostPrice();
			productPricingCalc.setSalePrice(productPricingCalc
					.getSalePriceAmount());
			productPricingCalc.setSalePriceAmount(AIOSCommons
					.roundDecimals(priceDetail *= 100));
		} else if (productCalculationType.name().equalsIgnoreCase(
				"fixed_price_margin")) {
			priceDetail = (productPricingCalc.getSalePriceAmount() - productPricingDetail
					.getBasicCostPrice())
					/ productPricingDetail.getBasicCostPrice();
			productPricingCalc.setSalePrice(productPricingCalc
					.getSalePriceAmount());
			productPricingCalc.setSalePriceAmount(AIOSCommons
					.roundDecimals(priceDetail *= 100));
		} else if (productCalculationType.name()
				.equalsIgnoreCase("fixed_price")) {
			ProductCalculationSubType productCalculationSubType = ProductCalculationSubType
					.get(productPricingCalc.getCalculationSubType());
			if (productCalculationSubType.name().equalsIgnoreCase("percentage")) {
				priceDetail = (productPricingDetail.getBasicCostPrice() * productPricingCalc
						.getSalePrice()) / 100;
				productPricingCalc.setSalePriceAmount(productPricingDetail
						.getBasicCostPrice() + priceDetail);
			} else {
				productPricingCalc.setSalePriceAmount(productPricingCalc
						.getSalePriceAmount());
			}
		}
		LOG.info("Module: Accounts : Method: calculateProductPrice: Action Success");
		return productPricingCalc;
	}

	// Show Calculated Price Value
	public String calculateProductPrice() {
		try {
			LOG.info("Inside Module: Accounts : Method: calculateProductPrice");
			ProductCalculationType productCalculationType = ProductCalculationType
					.get(calculationType);
			double priceDetail = 0;
			if (productCalculationType.name().equalsIgnoreCase("markup")) {
				priceDetail = (basicCostPrice * priceValue) / 100;
				calculatedPrice = AIOSCommons.formatAmount(priceDetail
						+ basicCostPrice);
			} else if (productCalculationType.name().equalsIgnoreCase("margin")) {
				priceDetail = (standardPrice / basicCostPrice) * priceValue
						/ 100;
				priceDetail = (standardPrice * (priceDetail * 100)) / 100;
				calculatedPrice = AIOSCommons.formatAmount(priceDetail
						+ standardPrice);
			} else if (productCalculationType.name().equalsIgnoreCase(
					"fixed_price_markup")) {

				priceDetail = (priceValue - basicCostPrice) / basicCostPrice;
				calculatedPrice = AIOSCommons.formatAmount(priceDetail *= 100);
				if (priceDetail < 0) {
					calculatedPrice = calculatedPrice.replace("$", "-")
							.substring(0, calculatedPrice.length() - 1) + "%";
				} else {
					calculatedPrice += "%";
				}
			} else if (productCalculationType.name().equalsIgnoreCase(
					"fixed_price_margin")) {

				priceDetail = (priceValue - basicCostPrice) / basicCostPrice;
				calculatedPrice = AIOSCommons.formatAmount(priceDetail *= 100);
				if (priceDetail < 0) {
					calculatedPrice = calculatedPrice.replace("$", "-")
							.substring(0, calculatedPrice.length() - 1) + "%";
				} else {
					calculatedPrice += "%";
				}
			} else if (productCalculationType.name().equalsIgnoreCase(
					"fixed_price")) {
				ProductCalculationSubType productCalculationSubType = ProductCalculationSubType
						.get(calculationSubType);
				if (productCalculationSubType.name().equalsIgnoreCase(
						"percentage")) {
					priceDetail = (basicCostPrice * priceValue) / 100;
					if (priceDetail < 0) {
						calculatedPrice = AIOSCommons
								.formatAmount(basicCostPrice
										- Math.abs(priceDetail));
					} else {
						calculatedPrice = AIOSCommons
								.formatAmount(basicCostPrice + priceDetail);
					}
				} else {
					calculatedPrice = AIOSCommons.formatAmount(priceValue);
				}
			}
			LOG.info("Module: Accounts : Method: calculateProductPrice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: calculateProductPrice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Product Pricing Add Rows
	public String productPricingAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: productPricingAddRow");
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: productPricingCalcAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: productPricingAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Product Pricing Calculation Add Rows
	public String productPricingCalcAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: productPricingCalcAddRow");
			Map<Byte, String> productCalculationType = new HashMap<Byte, String>();
			for (ProductCalculationType e : EnumSet
					.allOf(ProductCalculationType.class)) {
				productCalculationType.put(e.getCode(), e.name().toString()
						.replaceAll("_", " "));
			}
			Map<Byte, ProductCalculationSubType> productCalculationSubType = new HashMap<Byte, ProductCalculationSubType>();
			for (ProductCalculationSubType e : EnumSet
					.allOf(ProductCalculationSubType.class)) {
				productCalculationSubType.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("CALCULATION_TYPE",
					productCalculationType);
			ServletActionContext.getRequest().setAttribute(
					"CALCULATION_SUB_TYPE", productCalculationSubType);
			ServletActionContext.getRequest().setAttribute(
					"PRICING_TYPE",
					productPricingBL
							.getProductBL()
							.getLookupMasterBL()
							.getActiveLookupDetails("PRODUCT_PRICING_LEVEL",
									true));
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: productPricingCalcAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: productPricingCalcAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Update Product Pricing Session Details
	@SuppressWarnings("unchecked")
	public String updateSessionProductPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: updateSessionProductPricing");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			ProductPricingDetail productPricingDetail = new ProductPricingDetail();
			Product product = new Product();
			product.setProductId(productId);
			productPricingDetail.setProduct(product);
			productPricingDetail.setBasicCostPrice(basicCostPrice);
			productPricingDetail.setDescription(description);
			productPricingDetail.setSuggestedRetailPrice(suggestedRetailPrice);
			productPricingDetail.setStandardPrice(standardPrice);
			productPricingDetail.setSellingPrice(sellingPrice);
			productPricingDetail.setStatus(true);
			if(storeId > 0){
				Store store = new Store();
				store.setStoreId(storeId);
				productPricingDetail.setStore(store);
			}
			if (productPricingDetailId > 0)
				productPricingDetail
						.setProductPricingDetailId(productPricingDetailId);
			if (null != calculationDetails && !("").equals(calculationDetails))
				productPricingDetail
						.setProductPricingCalcs(getPricingCalculations(calculationDetails));

			Map<Integer, ProductPricingDetail> pricingDetailSession = (Map<Integer, ProductPricingDetail>) (session
					.getAttribute("PRODUCT_PRICING_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			if (null != pricingDetailSession && pricingDetailSession.size() > 0) {
				pricingDetailSession.put(priceDetailMirrorId,
						productPricingDetail);
			} else {
				pricingDetailSession = new HashMap<Integer, ProductPricingDetail>();
				pricingDetailSession.put(priceDetailMirrorId,
						productPricingDetail);
			}
			session.setAttribute(
					"PRODUCT_PRICING_SESSION_INFO_"
							+ sessionObj.get("jSessionId"),
					pricingDetailSession);
			LOG.info("Module: Accounts : Method: updateSessionProductPricing: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: updateSessionProductPricing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Product Pricing Detail from Session
	@SuppressWarnings("unchecked")
	public String deleteSessionProductPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteSessionProductPricing");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Integer, ProductPricingDetail> pricingDetailSession = (Map<Integer, ProductPricingDetail>) (session
					.getAttribute("PRODUCT_PRICING_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			if (productPricingDetailId > 0) {
				Set<Long> pricingDeleteSession = (HashSet<Long>) (session
						.getAttribute("PRICING_DELETED_SESSION_INFO_"
								+ sessionObj.get("jSessionId")));
				if (null == pricingDeleteSession)
					pricingDeleteSession = new HashSet<Long>();
				pricingDeleteSession.add(productPricingDetailId);
				session.setAttribute("PRICING_DELETED_SESSION_INFO_"
						+ sessionObj.get("jSessionId"), pricingDeleteSession);
			}
			if (null != pricingDetailSession
					&& pricingDetailSession.containsKey(parentId)) {
				pricingDetailSession.remove(parentId);
				session.removeAttribute("PRODUCT_PRICING_SESSION_INFO_"
						+ sessionObj.get("jSessionId"));
				session.setAttribute("PRODUCT_PRICING_SESSION_INFO_"
						+ sessionObj.get("jSessionId"), pricingDetailSession);
			}

			LOG.info("Module: Accounts : Method: deleteSessionProductPricing: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: deleteSessionProductPricing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Product Pricing
	@SuppressWarnings("unchecked")
	public String saveProductPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveProductPricing");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			ProductPricing productPricing = new ProductPricing();
			Currency currency = new Currency();
			currency.setCurrencyId(currencyId);
			productPricing.setCurrency(currency);
			productPricing.setStartDate(DateFormat
					.convertStringToDate(startDate));
			productPricing.setPricingTitle(pricingTitle);
			productPricing.setEndDate(DateFormat.convertStringToDate(endDate));
			productPricing.setDescription(description);

			Map<Integer, ProductPricingDetail> pricingDetailSession = (Map<Integer, ProductPricingDetail>) (session
					.getAttribute("PRODUCT_PRICING_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			Set<Long> pricingDeletedSession = (HashSet<Long>) (session
					.getAttribute("PRICING_DELETED_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			List<ProductPricingDetail> productPricingDetailsOld = new ArrayList<ProductPricingDetail>();
			List<ProductPricingCalc> productPricingCalcOldAll = new ArrayList<ProductPricingCalc>();
			List<ProductPricingCalc> productPricingCalcnew = new ArrayList<ProductPricingCalc>();
			List<ProductPricingDetail> productPricingDetails = null;
			boolean updateFlag = true;
			if (null != pricingDetailSession && pricingDetailSession.size() > 0) {
				productPricingDetails = (List<ProductPricingDetail>) new ArrayList<ProductPricingDetail>(
						pricingDetailSession.values());
				updateFlag = validatePricingDetails(productPricing,
						productPricingDetails);
				List<ProductPricingCalc> tempPricingCalcs = null;
				for (ProductPricingDetail productPricingDetail : productPricingDetails) {
					if (null != productPricingDetail.getProductPricingCalcs()
							&& productPricingDetail.getProductPricingCalcs()
									.size() > 0) {
						if (productPricingDetailId > 0) {
							tempPricingCalcs = userDeletedPricings(
									new ArrayList<ProductPricingCalc>(
											productPricingDetail
													.getProductPricingCalcs()),
									productPricingBL.getProductPricingService()
											.getProductPricingCalcByDetailId(
													productPricingDetailId));
							if (null != tempPricingCalcs
									&& tempPricingCalcs.size() > 0) {
								productPricingCalcOldAll
										.addAll(tempPricingCalcs);
							}
						}
						for (ProductPricingCalc pricingCalc : productPricingDetail
								.getProductPricingCalcs()) {
							pricingCalc
									.setProductPricingDetail(productPricingDetail);
							productPricingCalcnew.add(pricingCalc);
						}
					}
				}
				productPricing
						.setProductPricingId(productPricingId > 0 ? productPricingId
								: null);
			} else if (null != pricingDeletedSession
					&& pricingDeletedSession.size() > 0) {
				productPricing.setProductPricingId(productPricingId);
				productPricingDetailsOld = new ArrayList<ProductPricingDetail>();
				for (Long productPricingDetailId : pricingDeletedSession) {
					ProductPricingDetail productPricingDetail = productPricingBL
							.getProductPricingService()
							.getProductPricingDetailByPricingDetailId(
									productPricingDetailId);
					if (null != productPricingDetail.getProductPricingCalcs()
							&& productPricingDetail.getProductPricingCalcs()
									.size() > 0) {
						productPricingCalcOldAll
								.addAll(new ArrayList<ProductPricingCalc>(
										productPricingDetail
												.getProductPricingCalcs()));
					}
					productPricingDetailsOld.add(productPricingDetail);
				}
			} else {
				productPricing.setImplementation(productPricingBL
						.getImplementation());
				productPricing
						.setProductPricingId(productPricingId > 0 ? productPricingId
								: null);
			}
			if (null != productPricingCalcnew
					&& productPricingCalcnew.size() > 0) {
				for (ProductPricingCalc productPricingCalc : productPricingCalcnew) {
					if (null != productPricingCalc.getProductPricingCalcId()
							&& productPricingCalc.getProductPricingCalcId() == 0)
						productPricingCalc.setProductPricingCalcId(null);
				}
			}
			if (updateFlag) {
				productPricingBL.saveProductPricing(productPricing,
						productPricingDetails, productPricingDetailsOld,
						productPricingCalcOldAll, productPricingCalcnew);
				session.removeAttribute("PRODUCT_PRICING_SESSION_INFO_"
						+ sessionObj.get("jSessionId"));
				session.removeAttribute("PRICING_DELETED_SESSION_INFO_"
						+ sessionObj.get("jSessionId"));
				LOG.info("Module: Accounts : Method: saveProductPricing: Action Success");
				returnMessage = "SUCCESS";
			} else {
				LOG.info("Module: Accounts : Method: saveProductPricing: Action error");
				returnMessage = "Some products have pricing for the given date.";
				return SUCCESS;
			}
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: saveProductPricing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private boolean validatePricingDetails(ProductPricing productPricing,
			List<ProductPricingDetail> productPricingDetails) throws Exception {
		boolean checkFlag = true;
		List<ProductPricingDetail> checkProductPricingDetails = productPricingBL
				.getProductPricingService().getProductPricingDetailByDate(
						productPricing.getStartDate(),
						productPricing.getEndDate(),
						productPricingBL.getImplementation());
		if (null != checkProductPricingDetails
				&& checkProductPricingDetails.size() > 0) {
			Map<String, ProductPricingDetail> productMap = new HashMap<String, ProductPricingDetail>();
			String key = "";
			for (ProductPricingDetail productPricingDetail : checkProductPricingDetails) {
				if (null != productPricingDetail.getStore())
					key = String.valueOf(
							productPricingDetail.getProduct().getProductId())
							.concat("-"
									+ productPricingDetail.getStore()
											.getStoreId());
				else
					key = String.valueOf(productPricingDetail.getProduct()
							.getProductId());
				productMap.put(key, productPricingDetail);
			} 
			ProductPricingDetail tempPricingDetail = null;
			for (ProductPricingDetail productPricingDetail : productPricingDetails) {
				key = "";
				if (null != productPricingDetail.getStore())
					key = String.valueOf(
							productPricingDetail.getProduct().getProductId())
							.concat("-"
									+ productPricingDetail.getStore()
											.getStoreId());
				else
					key = String.valueOf(productPricingDetail.getProduct()
							.getProductId());
				// check product exists
				if (productMap.containsKey(key)) {
					// check if same record
					tempPricingDetail = productMap.get(key);
					if (null == productPricingDetail
							.getProductPricingDetailId()
							|| productPricingDetail.getProductPricingDetailId() == 0) {
						checkFlag = false;
					} else if ((long) tempPricingDetail
							.getProductPricingDetailId() != (long) productPricingDetail
							.getProductPricingDetailId()) {
						checkFlag = false;
					}
				}
			}
		}
		return checkFlag;
	}

	// Delete Product Pricing
	public String deleteProductPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteProductPricing");
			ProductPricing productPricing = productPricingBL
					.getProductPricingService().getProductPricingAll(
							productPricingId);
			productPricingBL.deleteProductPricing(productPricing);
			LOG.info("Module: Accounts : Method: deleteProductPricing: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: deleteProductPricing Exception "
					+ ex);
			returnMessage = "Failure";
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Product Pricing Detail
	public String deleteProductPricingDetail() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteProductPricingDetail");
			ProductPricingDetail productPricingDetail = productPricingBL
					.getProductPricingService()
					.getProductPricingDetailByPricingDetailId(
							productPricingDetailId);
			productPricingBL.deleteProductPricingDetail(productPricingDetail);
			LOG.info("Module: Accounts : Method: deleteProductPricingDetail: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: deleteProductPricingDetail Exception "
					+ ex);
			returnMessage = "Failure";
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Pricing Detail
	public String showCommonProductPricing() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCommonProductPricing");
			@SuppressWarnings("unchecked")
			List<ProductPricingCalcVO> productPricingCalcVOs = (List<ProductPricingCalcVO>) productPricingBL
					.manipulateProductPricing(productId, storeId, customerId);
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_PRICING_INFO", productPricingCalcVOs);
			LOG.info("Module: Accounts : Method: showCommonProductPricing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCommonProductPricing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Pricing Detail
	@SuppressWarnings("unchecked")
	public String showProductPricingCalc() {
		try {
			LOG.info("Inside Module: Accounts : Method: showProductPricingCalc");
			aaData = (List<Object>) productPricingBL.manipulateProductPricing(
					productId, storeId, customerId);
			LOG.info("Module: Accounts : Method: showProductPricingCalc: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showProductPricingCalc Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Set the values to Pricing Calculation
	private Set<ProductPricingCalc> getPricingCalculations(
			String calculationDetail) throws Exception {
		String[] calculationDetails = splitValues(calculationDetail, "@#");
		ProductPricingCalc productPricingCalc = null;
		LookupDetail lookupDetail = null;
		List<ProductPricingCalc> productPricingCalcs = new ArrayList<ProductPricingCalc>();
		for (String detail : calculationDetails) {
			String[] details = splitValues(detail, "__");
			productPricingCalc = new ProductPricingCalc();
			lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(Long.parseLong(details[0]));
			productPricingCalc.setLookupDetail(lookupDetail);
			productPricingCalc.setCalculationType(Byte.valueOf(details[1]));
			productPricingCalc.setComboType(false);
			if (!("##").equals(details[2]))
				productPricingCalc.setCalculationSubType(Byte
						.valueOf(details[2]));
			if (!("##").equals(details[3]))
				productPricingCalc.setDescription(details[3]);
			ProductCalculationType productCalculationType = ProductCalculationType
					.get(productPricingCalc.getCalculationType());
			if (productCalculationType.name().equalsIgnoreCase(
					"fixed_price_markup")
					|| productCalculationType.name().equalsIgnoreCase(
							"fixed_price_margin")) {
				productPricingCalc
						.setSalePrice(AIOSCommons
								.formatAmountToDouble((details[5].replaceAll(
										"%", ""))));
				productPricingCalc.setSalePriceAmount(AIOSCommons
						.formatAmountToDouble(details[4]));
			} else {
				productPricingCalc.setSalePrice(AIOSCommons
						.formatAmountToDouble(details[4]));
				productPricingCalc.setSalePriceAmount((AIOSCommons
						.formatAmountToDouble(details[5])));
			}
			productPricingCalc.setIsDefault(true);
			if (Long.parseLong(details[7]) > 0)
				productPricingCalc.setProductPricingCalcId(Long
						.parseLong(details[7]));
			if (Long.parseLong(details[9]) > 0) {
				Store store = new Store();
				store.setStoreId(Long.parseLong(details[9]));
				productPricingCalc.setStore(store);
			}
			productPricingCalc.setStatus(true);
			if (!("##").equals(details[8])) {
				String[] customerDetails = splitValues(details[8], ",");
				for (String customer : customerDetails) {
					productPricingCalcs.add(addCustomerPricing(
							Long.parseLong(customer), productPricingCalc));
				}
			} else {
				productPricingCalcs.add(productPricingCalc);
			}
		}
		return new HashSet<ProductPricingCalc>(productPricingCalcs);
	}

	// Set the values to Pricing Calculation
	@SuppressWarnings("unused")
	private Set<ProductPricingCalc> addPricingCalculations() throws Exception {
		List<ProductPricingCalc> productPricingCalcs = new ArrayList<ProductPricingCalc>();
		List<ProductPricingCalc> productPricingCalcOld = new ArrayList<ProductPricingCalc>();

		if (productPricingDetailId > 0) {
			productPricingCalcOld = productPricingBL.getProductPricingService()
					.getProductPricingCalcByDetailId(productPricingDetailId);
			if (null != productPricingCalcOld
					&& productPricingCalcOld.size() > 0) {
				for (ProductPricingCalc productPricingCalc : productPricingCalcOld) {
					if (null == productPricingCalc.getLookupDetail()
							&& null != productPricingCalc.getIsDefault()
							&& (boolean) productPricingCalc.getIsDefault()) {
						productPricingCalc.setIsDefault(false);
					}
				}
			}
		}
		ProductPricingCalc productPricingCalc = new ProductPricingCalc();

		productPricingCalc
				.setCalculationType(ProductCalculationType.Fixed_Price
						.getCode());
		productPricingCalc.setComboType(false);
		productPricingCalc.setCalculationSubType(Byte
				.valueOf(ProductCalculationSubType.Amount.getCode()));
		productPricingCalc.setSalePrice(AIOSCommons
				.formatAmountToDouble(sellingPrice));
		productPricingCalc.setSalePriceAmount((AIOSCommons
				.formatAmountToDouble(sellingPrice)));
		productPricingCalc.setIsDefault(true);
		productPricingCalc.setStatus(true);
		productPricingCalcs.add(productPricingCalc);
		productPricingCalcs.addAll(productPricingCalcOld);
		return new HashSet<ProductPricingCalc>(productPricingCalcs);
	}

	private List<ProductPricingCalc> userDeletedPricings(
			List<ProductPricingCalc> productPricingCalcs,
			List<ProductPricingCalc> productPricingCalcOld) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProductPricingCalc> hashPrices = new HashMap<Long, ProductPricingCalc>();
		if (null != productPricingCalcOld && productPricingCalcOld.size() > 0) {
			for (ProductPricingCalc list : productPricingCalcOld) {
				listOne.add(list.getProductPricingCalcId());
				hashPrices.put(list.getProductPricingCalcId(), list);
			}
			for (ProductPricingCalc list : productPricingCalcs) {
				listTwo.add(list.getProductPricingCalcId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> differents = new HashSet<Long>();
		differents.addAll(listOne);
		differents.addAll(listTwo);
		similar.retainAll(listTwo);
		differents.removeAll(similar);
		List<ProductPricingCalc> pricingList = new ArrayList<ProductPricingCalc>();
		if (null != differents && differents.size() > 0) {
			ProductPricingCalc tempPricing = null;
			for (Long list : differents) {
				if (null != list && !list.equals(0l)) {
					tempPricing = new ProductPricingCalc();
					tempPricing = hashPrices.get(list);
					pricingList.add(tempPricing);
				}
			}
		}
		return pricingList;
	}

	private ProductPricingCalc addCustomerPricing(long customerId,
			ProductPricingCalc productPricingCalc)
			throws IllegalAccessException, InvocationTargetException {
		ProductPricingCalc customerPricing = new ProductPricingCalc();
		BeanUtils.copyProperties(customerPricing, productPricingCalc);
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		customerPricing.setCustomer(customer);
		return customerPricing;
	}

	// Split values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters and Setter
	public ProductPricingBL getProductPricingBL() {
		return productPricingBL;
	}

	public void setProductPricingBL(ProductPricingBL productPricingBL) {
		this.productPricingBL = productPricingBL;
	}

	public long getProductPricingId() {
		return productPricingId;
	}

	public void setProductPricingId(long productPricingId) {
		this.productPricingId = productPricingId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getCalculatedPrice() {
		return calculatedPrice;
	}

	public void setCalculatedPrice(String calculatedPrice) {
		this.calculatedPrice = calculatedPrice;
	}

	public double getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(double priceValue) {
		this.priceValue = priceValue;
	}

	public long getPricingType() {
		return pricingType;
	}

	public void setPricingType(long pricingType) {
		this.pricingType = pricingType;
	}

	public byte getCalculationType() {
		return calculationType;
	}

	public void setCalculationType(byte calculationType) {
		this.calculationType = calculationType;
	}

	public byte getCalculationSubType() {
		return calculationSubType;
	}

	public void setCalculationSubType(byte calculationSubType) {
		this.calculationSubType = calculationSubType;
	}

	public double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public int getPriceDetailMirrorId() {
		return priceDetailMirrorId;
	}

	public void setPriceDetailMirrorId(int priceDetailMirrorId) {
		this.priceDetailMirrorId = priceDetailMirrorId;
	}

	public double getBasicCostPrice() {
		return basicCostPrice;
	}

	public void setBasicCostPrice(double basicCostPrice) {
		this.basicCostPrice = basicCostPrice;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public long getProductPricingDetailId() {
		return productPricingDetailId;
	}

	public void setProductPricingDetailId(long productPricingDetailId) {
		this.productPricingDetailId = productPricingDetailId;
	}

	public double getSuggestedRetailPrice() {
		return suggestedRetailPrice;
	}

	public void setSuggestedRetailPrice(double suggestedRetailPrice) {
		this.suggestedRetailPrice = suggestedRetailPrice;
	}

	public String getCalculationDetails() {
		return calculationDetails;
	}

	public void setCalculationDetails(String calculationDetails) {
		this.calculationDetails = calculationDetails;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(String pageInfo) {
		this.pageInfo = pageInfo;
	}

	public String getPricingTitle() {
		return pricingTitle;
	}

	public void setPricingTitle(String pricingTitle) {
		this.pricingTitle = pricingTitle;
	}

	public String getPricingDetailIds() {
		return pricingDetailIds;
	}

	public void setPricingDetailIds(String pricingDetailIds) {
		this.pricingDetailIds = pricingDetailIds;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
