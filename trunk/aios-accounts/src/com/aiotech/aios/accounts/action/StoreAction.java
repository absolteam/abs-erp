package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.ShelfVO;
import com.aiotech.aios.accounts.service.bl.StoreBL;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class StoreAction extends ActionSupport {

	private static final long serialVersionUID = 908374610009859712L;
	private StoreBL storeBL;
	private long storeId;
	private String storeNumber;
	private String storeName;
	private Implementation implementation;
	private Integer rowId;
	private String returnMessage;
	private long personId;
	private String personName;
	private String address;
	private String aisleDetails;
	private long storageType;
	private long storageSection;
	private long binType;
	private Integer sectionReferenceId;
	private Integer binReferenceId;
	private int aisleId;
	private int rackLineId;
	private int referenceNumber;
	private String pageInfo;
	private String searchCriteria;
	private String binDetails;
	private String rackDetails;
	private boolean confirmSave;
	private long locationId;
	private Long recordId;
	private List<Object> aaData;

	private static final Logger LOGGER = LogManager
			.getLogger(StoreAction.class);

	public String returnSuccess() {
		LOGGER.info("Inside Module: Accounts, Method :returnSucces");
		recordId = null;
		return SUCCESS;
	}

	public String showJsonStoreList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonStoreList");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("BIN_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			JSONObject jsonList = getStoreBL().getStoreList(
					getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showStoreEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show All Store Details
	public String showStoreReportList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonStoreList");
			aaData = storeBL.getStoreDetails(getImplementationId(), storeId,
					personId);
			LOG.info("Module: Accounts : Method: showAllProductPricing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreReportList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show All Store Details
	public String showStoreDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonStoreList");
			aaData = storeBL.showStoreDetails(getImplementationId());
			LOG.info("Module: Accounts : Method: showAllProductPricing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreReportList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Entry Screen
	public String showStoreEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showStoreEntry");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			session.removeAttribute("BIN_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			storeNumber = showStoreNumber();
			personId = user.getPersonId();
			personName = (String) sessionObj.get("PERSON_NAME");
			ServletActionContext.getRequest().setAttribute("STORE_NUMBER",
					storeNumber);
			ServletActionContext.getRequest().setAttribute(
					"STORAGE_TYPES",
					storeBL.getLookupMasterBL().getActiveLookupDetails(
							"STORAGE_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"STORAGE_SECTIONS",
					storeBL.getLookupMasterBL().getActiveLookupDetails(
							"STORAGE_SECTION", true));
			LOGGER.info("Module: Accounts : Method: showStoreEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Bin Entry Screen
	@SuppressWarnings("unchecked")
	public String showStoreBinEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showStoreBinEntry");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("BIN_SESSION_INFO_"
					+ sessionObj.get("jSessionId"))) {
				Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
						.getAttribute("BIN_SESSION_INFO_"
								+ sessionObj.get("jSessionId"));
				if (null != shelfVOs && shelfVOs.size() > 0) {
					if (shelfVOs.containsKey(sectionReferenceId)) {
						Map<Integer, ShelfVO> shelfVOMap = shelfVOs
								.get(sectionReferenceId);
						ServletActionContext.getRequest().setAttribute(
								"BIN_DETAILS",
								new ArrayList<ShelfVO>(shelfVOMap.values()));
					}
				}
			}
			ServletActionContext.getRequest().setAttribute("BIN_TYPES",
					storeBL.getStoreBinTypes());
			LOGGER.info("Module: Accounts : Method: showStoreBinEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreBinEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Bin Add Row
	public String storeBinAddRows() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: storeBinAddRows");
			ServletActionContext.getRequest().setAttribute("BIN_TYPES",
					storeBL.getStoreBinTypes());
			LOGGER.info("Module: Accounts : Method: storeBinAddRows: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: storeBinAddRows: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Rack Add Row
	public String storeRackAddRows() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: storeRackAddRows");
			ServletActionContext.getRequest().setAttribute("RACK_TYPES",
					storeBL.getStoreBinTypes());
			LOGGER.info("Module: Accounts : Method: storeRackAddRows: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: storeRackAddRows: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Rack Entry Screen
	@SuppressWarnings("unchecked")
	public String showBinRackEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBinRackEntry");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			saveBinDetails();
			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null != shelfVOs && !shelfVOs.equals("")) {
				Map<Integer, ShelfVO> shelfVOMap = shelfVOs
						.get(sectionReferenceId);
				if (shelfVOMap.containsKey(binReferenceId)) {
					ShelfVO rackDetail = shelfVOMap.get(binReferenceId);
					ServletActionContext.getRequest().setAttribute(
							"RACK_DETAILS", rackDetail.getRackDetails());
				}
			}
			ServletActionContext.getRequest().setAttribute("RACK_TYPES",
					storeBL.getStoreBinTypes());
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: showBinRackEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: showBinRackEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Detail Report
	public String showStoreDetailReport() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showStoreDetailReport");
			Store store = storeBL.getStoreService()
					.getStoreInfoDetails(storeId);
			ServletActionContext.getRequest().setAttribute("STORE_DETAILS",
					store);
			LOGGER.info("Module: Accounts : Method: showStoreDetailReport: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreDetailReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showStoreUpdate() {
		LOGGER.info("Inside Module: Accounts : Method: showStoreUpdate");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("BIN_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));

			Store store = getStoreBL().getStoreService().getStoreDetails(
					storeId);
			List<Aisle> aisles = getStoreBL().getStoreService()
					.getAsileDetails(storeId);

			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = new HashMap<Integer, Map<Integer, ShelfVO>>();

			if (null != aisles && !aisles.equals("")) {
				List<ShelfVO> shelfVOList = null;
				Map<Integer, ShelfVO> asileDetail = null;
				for (Aisle aisle : aisles) {
					int i = 0;
					shelfVOList = new ArrayList<ShelfVO>();
					asileDetail = new HashMap<Integer, ShelfVO>();
					for (Shelf shelf : aisle.getShelfs()) {
						if (null == shelf.getShelf())
							shelfVOList.add(addBinDetails(shelf, ++i));
					}
					for (ShelfVO shelfVO : shelfVOList)
						asileDetail.put(shelfVO.getReferenceNumber(), shelfVO);
					shelfVOs.put(aisle.getAisleId(), asileDetail);
				}
			}
			session.setAttribute(
					"BIN_SESSION_INFO_" + sessionObj.get("jSessionId"),
					shelfVOs);
			ServletActionContext.getRequest().setAttribute("STORE_INFO", store);
			ServletActionContext.getRequest().setAttribute("AISLE_LINE_INFO",
					aisles);

			ServletActionContext.getRequest().setAttribute(
					"STORAGE_TYPES",
					storeBL.getLookupMasterBL().getActiveLookupDetails(
							"STORAGE_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"STORAGE_SECTIONS",
					storeBL.getLookupMasterBL().getActiveLookupDetails(
							"STORAGE_SECTION", true));
			ServletActionContext.getRequest().setAttribute("BIN_TYPES",
					storeBL.getStoreBinTypes());
			CommentVO comment = new CommentVO();
			if (recordId != null && store != null
					&& store.getStoreId() != 0 && recordId > 0) {
				comment.setRecordId(store.getStoreId());
				comment.setUseCase(Store.class.getName());
				comment = storeBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			LOGGER.info("Module: Accounts : Method: showStoreUpdate: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreUpdate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String addRows() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: addRows");
			ServletActionContext.getRequest().setAttribute(
					"STORAGE_SECTIONS",
					storeBL.getLookupMasterBL().getActiveLookupDetails(
							"STORAGE_SECTION", true));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: addRows: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPersons() {
		LOGGER.info("Inside Module: Accounts : Method: showPersons");
		try {
			List<Person> personList = getStoreBL().getPersonDAO()
					.findByNamedQuery("getAllPerson", getImplementationId());
			ServletActionContext.getRequest().setAttribute("PERSON_INFO",
					personList);
			LOGGER.info("Module: Accounts : Method: showPersons: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPersons: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Store Info
	public String showStore() {
		LOGGER.info("Inside Module: Accounts : Method: showStore");
		try {
			List<Store> stores = storeBL.getStoreService().getAllStores(
					getImplementationId());
			ServletActionContext.getRequest().setAttribute("STORE_DETAILS",
					stores);
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOGGER.info("Module: Accounts : Method: showStore: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStore: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Store Details
	@SuppressWarnings("unchecked")
	public String saveStore() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveStore");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Store store = new Store();
			Person person = new Person();
			store.setStoreName(storeName);
			person.setPersonId(personId);
			store.setPerson(person);
			store.setAddress(address);
			CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
			cmpDeptLocation.setCmpDeptLocId(locationId);
			store.setCmpDeptLocation(cmpDeptLocation);
			if (storageType > 0) {
				LookupDetail storageTypeLookup = new LookupDetail();
				storageTypeLookup.setLookupDetailId(storageType);
				store.setLookupDetail(storageTypeLookup);
			}

			String[] aisleDetailArray = splitValues(aisleDetails, "#@");
			List<Aisle> aisles = addAisleDetails(aisleDetailArray);
			store.setImplementation(getImplementationId());
			if (storeId > 0) {
				store.setStoreId(storeId);
				store.setStoreNumber(storeNumber);
			} else {
				store.setStoreNumber(showStoreNumber());
			}
			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null != shelfVOs && shelfVOs.size() > 0) {
				// Check for Delete Racks
				List<Shelf> existingRacks = new ArrayList<Shelf>();
				for (Aisle aisle : aisles) {
					if (null != aisle.getAisleId() && aisle.getAisleId() > 0) {
						List<Shelf> tempShelfs = storeBL.getStoreService()
								.getRacksBySectionId(aisle.getAisleId());
						if (null != tempShelfs && tempShelfs.size() > 0)
							existingRacks.addAll(tempShelfs);
					}
				}
				List<Shelf> deletedShelfs = new ArrayList<Shelf>();
				List<Shelf> deletedRacks = null;
				if (null != existingRacks && existingRacks.size() > 0) {
					// Get Deleted Racks
					deletedRacks = getUserDeletedRacks(existingRacks, shelfVOs);
					if(null!=deletedRacks && deletedRacks.size()>0){
						for (Shelf shelf : deletedRacks) {
							deletedShelfs.addAll(storeBL.getStoreService()
									.getShelfsByRackId(shelf.getShelfId()));
						}
					} 
				}
				if (storeId > 0) {
					// Check for Delete Shelfs
					List<Shelf> shelfDetails = new ArrayList<Shelf>();
					Map<Integer, ShelfVO> shelfVOMap = null;
					for (Entry<Integer, Map<Integer, ShelfVO>> shelfVOsMap : shelfVOs
							.entrySet()) {
						shelfVOMap = shelfVOsMap.getValue();
						for (Entry<Integer, ShelfVO> shelfVO : shelfVOMap
								.entrySet()) {
							shelfDetails.add(storeBL.addShelfDetails(shelfVO
									.getValue()));
						}
					}

					for (Shelf rack : shelfDetails) {
						if (null != rack.getShelfId() && rack.getShelfId() > 0) {
							List<Shelf> tempShelfs = storeBL.getStoreService()
									.getShelfsByRackId(rack.getShelfId());
							if (null != tempShelfs && tempShelfs.size() > 0) {
								List<Shelf> shelfRacks = storeBL
										.addShelfDetails(new ArrayList<Shelf>(
												rack.getShelfs()));
								// Get Deleted Shelfs
								List<Shelf> tempDeletedShelfs = getUserDeletedShelfs(
										tempShelfs, shelfRacks);
								if (null != tempDeletedShelfs
										&& tempDeletedShelfs.size() > 0)
									deletedShelfs.addAll(tempDeletedShelfs);
							}
						}
					}
				}
				storeBL.saveStore(store, aisles, shelfVOs, deletedRacks,
						deletedShelfs);
				returnMessage = "SUCCESS";
				session.removeAttribute("BIN_SESSION_INFO_"
						+ sessionObj.get("jSessionId"));
			} else {
				returnMessage = "Please enter Bin Details.";
				LOGGER.info("Module: Accounts : Method: saveStore: Action Error");
				return ERROR;
			}
			LOGGER.info("Module: Accounts : Method: saveStore: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveStore: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private List<Shelf> getUserDeletedShelfs(List<Shelf> existingShelfs,
			List<Shelf> shelfRacks) {
		Collection<Integer> listOne = new ArrayList<Integer>();
		Collection<Integer> listTwo = new ArrayList<Integer>();
		Map<Integer, Shelf> hashShelfDetail = new HashMap<Integer, Shelf>();
		if (null != existingShelfs && existingShelfs.size() > 0) {
			for (Shelf list : existingShelfs) {
				listOne.add(list.getShelfId());
				hashShelfDetail.put(list.getShelfId(), list);
			}
			if (null != hashShelfDetail && hashShelfDetail.size() > 0) {
				for (Shelf list : shelfRacks) {
					listTwo.add(list.getShelfId());
				}
			}
		}
		Collection<Integer> similar = new HashSet<Integer>(listOne);
		Collection<Integer> different = new HashSet<Integer>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<Shelf> deleteShelfs = null;
		if (null != different && different.size() > 0) {
			Shelf tempShelf = null;
			deleteShelfs = new ArrayList<Shelf>();
			for (Integer list : different) {
				if (null != list && !list.equals(0)) {
					tempShelf = new Shelf();
					tempShelf = hashShelfDetail.get(list);
					deleteShelfs.add(tempShelf);
				}
			}
		}

		return deleteShelfs;
	}

	private List<Shelf> getUserDeletedRacks(List<Shelf> existingShelfs,
			Map<Integer, Map<Integer, ShelfVO>> shelfVOs) throws Exception {
		Collection<Integer> listOne = new ArrayList<Integer>();
		Collection<Integer> listTwo = new ArrayList<Integer>();
		Map<Integer, Shelf> hashShelfDetail = new HashMap<Integer, Shelf>();
		if (null != existingShelfs && existingShelfs.size() > 0) {
			for (Shelf list : existingShelfs) {
				listOne.add(list.getShelfId());
				hashShelfDetail.put(list.getShelfId(), list);
			}
			if (null != hashShelfDetail && hashShelfDetail.size() > 0) {
				Map<Integer, ShelfVO> shelfVOMap = null;
				List<Shelf> shelfDetails = new ArrayList<Shelf>();
				for (Entry<Integer, Map<Integer, ShelfVO>> shelfVOsMap : shelfVOs
						.entrySet()) {
					shelfVOMap = shelfVOsMap.getValue();
					for (Entry<Integer, ShelfVO> shelfVO : shelfVOMap
							.entrySet()) {
						shelfDetails.add(storeBL.addShelfDetails(shelfVO
								.getValue()));
					}
				}
				for (Shelf list : shelfDetails) {
					listTwo.add(list.getShelfId());
				}
			}
		}
		Collection<Integer> similar = new HashSet<Integer>(listOne);
		Collection<Integer> different = new HashSet<Integer>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<Shelf> deleteShelfs = null;
		List<Shelf> deleteRackShelfs = null;
		if (null != different && different.size() > 0) {
			Shelf tempShelf = null;
			deleteShelfs = new ArrayList<Shelf>();
			for (Integer list : different) {
				if (null != list && !list.equals(0)) {
					tempShelf = new Shelf();
					tempShelf = hashShelfDetail.get(list);
					deleteRackShelfs = storeBL.getStoreService()
							.getShelfsByRackId(tempShelf.getShelfId());
					if (null != deleteRackShelfs && deleteRackShelfs.size() > 0)
						tempShelf
								.setShelfs(new HashSet<Shelf>(deleteRackShelfs));
					deleteShelfs.add(tempShelf);
				}
			}
		}
		return deleteShelfs;
	}

	@SuppressWarnings("unchecked")
	// Add Aisle Details
	private List<Aisle> addAisleDetails(String[] aisleDetails) throws Exception {

		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();

		Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
				.getAttribute("BIN_SESSION_INFO_"
						+ sessionObj.get("jSessionId"));

		List<Aisle> aisles = new ArrayList<Aisle>();
		if (null != shelfVOs && shelfVOs.size() > 0) {
			Aisle aisle = null;
			LookupDetail sectionType = null;

			for (String detail : aisleDetails) {
				String[] details = splitValues(detail, "__");
				aisle = new Aisle();
				sectionType = new LookupDetail();
				aisle.setAisleNumber(details[1]);
				aisle.setDimension(details[2]);
				sectionType.setLookupDetailId(Long.parseLong(details[3]));
				aisle.setLookupDetail(sectionType);
				if (Integer.parseInt(details[4]) > 0)
					aisle.setAisleId(Integer.parseInt(details[4]));
				if (shelfVOs.containsKey(Integer.parseInt(details[0]))) {
					Map<Integer, ShelfVO> shelfVOMap = shelfVOs.get(Integer
							.parseInt(details[0]));
					for (ShelfVO shelfVO : shelfVOMap.values()) {
						shelfVO.setAisle(aisle);
					}
					shelfVOs.put(Integer.parseInt(details[0]), shelfVOMap);
				}
				aisle.setSectionName(details[5]);
				aisles.add(aisle);
			}
		}
		session.setAttribute(
				"BIN_SESSION_INFO_" + sessionObj.get("jSessionId"), shelfVOs);
		return aisles;
	}

	// Save Bin Details
	@SuppressWarnings("unchecked")
	public String saveBinDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveBinDetails");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null == shelfVOs) {
				shelfVOs = new HashMap<Integer, Map<Integer, ShelfVO>>();
			}
			if (null != binDetails && !binDetails.equals("")) {
				String[] binDetailsArray = splitValues(binDetails, "#@");
				List<ShelfVO> shelfVOList = addBinDetails(binDetailsArray,
						shelfVOs.get(sectionReferenceId));
				Map<Integer, ShelfVO> asileDetail = new HashMap<Integer, ShelfVO>();

				for (ShelfVO shelfVO : shelfVOList) {

					asileDetail.put(shelfVO.getReferenceNumber(), shelfVO);
				}
				shelfVOs.put(sectionReferenceId, asileDetail);
			} else {
				if (shelfVOs.containsKey(sectionReferenceId))
					shelfVOs.remove(sectionReferenceId);
				else
					shelfVOs.put(sectionReferenceId, null);
			}
			session.setAttribute(
					"BIN_SESSION_INFO_" + sessionObj.get("jSessionId"),
					shelfVOs);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: saveBinDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: saveBinDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Discard Bin Details
	@SuppressWarnings("unchecked")
	public String discardBinDetail() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: discardBinDetail");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null != shelfVOs && shelfVOs.size() > 0) {
				Map<Integer, ShelfVO> shelfVOMap = shelfVOs
						.get(sectionReferenceId);
				Map<Integer, ShelfVO> updatedShelf = new HashMap<Integer, ShelfVO>();

				if (null != shelfVOMap && shelfVOMap.size() > 0) {
					for (Entry<Integer, ShelfVO> shelfVO : shelfVOMap
							.entrySet()) {
						ShelfVO shelf = shelfVO.getValue();
						if (shelf.isConfirmSave()) {
							updatedShelf.put(shelfVO.getKey(), shelf);
						}
					}
					if (null != updatedShelf && updatedShelf.size() > 0) {
						shelfVOs.put(sectionReferenceId, updatedShelf);
					} else {
						shelfVOs.remove(sectionReferenceId);
					}
				}
			}
			session.setAttribute(
					"BIN_SESSION_INFO_" + sessionObj.get("jSessionId"),
					shelfVOs);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: discardBinDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: discardBinDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Bin Detail
	@SuppressWarnings("unchecked")
	public String deleteBinDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: deleteBinDetails");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null != shelfVOs && shelfVOs.size() > 0) {

				Map<Integer, ShelfVO> shelfVOMap = shelfVOs
						.get(sectionReferenceId);
				if (shelfVOMap.containsKey(referenceNumber)) {
					shelfVOMap.remove(referenceNumber);
					shelfVOs.put(sectionReferenceId, shelfVOMap);
				}
			}
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: deleteBinDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: deleteBinDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save Rack Details
	@SuppressWarnings("unchecked")
	public String saveRackDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveRackDetails");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null != rackDetails && !rackDetails.equals("")) {
				String[] rackDetailsArray = splitValues(rackDetails, "#@");
				Map<Integer, ShelfVO> shelfVOMap = shelfVOs
						.get(sectionReferenceId);
				ShelfVO shelfVO = shelfVOMap.get(binReferenceId);
				shelfVO.setRackDetails(addRackDetails(rackDetailsArray));
				shelfVOMap.put(binReferenceId, shelfVO);
				shelfVOs.put(sectionReferenceId, shelfVOMap);
				session.setAttribute(
						"BIN_SESSION_INFO_" + sessionObj.get("jSessionId"),
						shelfVOs);
			}
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: saveRackDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: saveRackDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Rack Detail
	@SuppressWarnings("unchecked")
	public String deleteRackDetail() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: deleteRackDetail");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<Integer, Map<Integer, ShelfVO>> shelfVOs = (Map<Integer, Map<Integer, ShelfVO>>) session
					.getAttribute("BIN_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			if (null != shelfVOs && shelfVOs.size() > 0) {

				Map<Integer, ShelfVO> shelfVOMap = shelfVOs
						.get(sectionReferenceId);
				ShelfVO shelfVO = shelfVOMap.get(binReferenceId);
				int deleteIndex = --rackLineId;
				if (null != shelfVO.getRackDetails()
						&& shelfVO.getRackDetails().size() >= deleteIndex) {
					shelfVO.getRackDetails().remove(--rackLineId);
					shelfVOMap.put(binReferenceId, shelfVO);
					shelfVOs.put(sectionReferenceId, shelfVOMap);
					session.setAttribute(
							"BIN_SESSION_INFO_" + sessionObj.get("jSessionId"),
							shelfVOs);
				}
			}
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: deleteRackDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: deleteRackDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Add Bin Details
	private List<ShelfVO> addBinDetails(String[] binDetailsArray,
			Map<Integer, ShelfVO> shelfVOMap) {
		ShelfVO shelfVO = null;
		Aisle aisle = new Aisle();
		List<ShelfVO> shelfVOs = new ArrayList<ShelfVO>();
		for (String binDetail : binDetailsArray) {
			String[] details = splitValues(binDetail, "__");
			shelfVO = new ShelfVO();
			shelfVO.setShelfType(Byte.parseByte(details[0]));
			shelfVO.setDimension(details[1]);
			if (details[2].equalsIgnoreCase("true"))
				shelfVO.setConfirmSave(true);
			else
				shelfVO.setConfirmSave(confirmSave);
			shelfVO.setReferenceNumber(Integer.parseInt(details[3]));
			if (Integer.parseInt(details[4]) > 0)
				shelfVO.setShelfId(Integer.parseInt(details[4]));
			if (null != shelfVOMap
					&& shelfVOMap.containsKey(shelfVO.getReferenceNumber())) {
				ShelfVO tempShelfVO = shelfVOMap.get(shelfVO
						.getReferenceNumber());
				shelfVO.setRackDetails(tempShelfVO.getRackDetails());
			}
			if (Integer.parseInt(details[5]) > 0) {
				aisle.setAisleId(Integer.parseInt(details[5]));
				shelfVO.setAisle(aisle);
			}
			shelfVO.setName(details[6]);
			shelfVOs.add(shelfVO);
		}
		return shelfVOs;
	}

	// Add Bin Details
	private ShelfVO addBinDetails(Shelf shelf, int reference) throws Exception {
		ShelfVO shelfVO = null;
		shelfVO = new ShelfVO();
		shelfVO.setShelfType(shelf.getShelfType());
		shelfVO.setDimension(shelf.getDimension());
		shelfVO.setName(shelf.getName());
		shelfVO.setConfirmSave(true);
		shelfVO.setReferenceNumber(reference);
		shelfVO.setShelfId(shelf.getShelfId());
		shelfVO.setAisle(shelf.getAisle());
		List<ShelfVO> rackVOs = new ArrayList<ShelfVO>();
		if (null != shelf.getShelfs() && shelf.getShelfs().size() > 0) {
			rackVOs.addAll(addRackDetails(shelf.getShelfs()));
			shelfVO.setRackDetails(rackVOs);
		}
		return shelfVO;
	}

	// Add Rack Details
	private List<ShelfVO> addRackDetails(String[] rackDetailsArray) {
		ShelfVO shelfVO = null;
		Aisle aisle = new Aisle();
		List<ShelfVO> shelfVOs = new ArrayList<ShelfVO>();
		for (String rackDetail : rackDetailsArray) {
			String[] details = splitValues(rackDetail, "__");
			shelfVO = new ShelfVO();
			shelfVO.setShelfType(Byte.parseByte(details[0]));
			shelfVO.setDimension(details[1]);
			if (Integer.parseInt(details[2]) > 0)
				shelfVO.setShelfId(Integer.parseInt(details[2]));
			if (Integer.parseInt(details[3]) > 0) {
				aisle.setAisleId(Integer.parseInt(details[3]));
				shelfVO.setAisle(aisle);
			}
			shelfVO.setName(details[4]);
			shelfVOs.add(shelfVO);
		}
		return shelfVOs;
	}

	// Add Rack Details
	private List<ShelfVO> addRackDetails(Set<Shelf> racks) throws Exception {
		ShelfVO rackVO = null;
		List<ShelfVO> shelfVOs = new ArrayList<ShelfVO>();
		int i = 0;
		for (Shelf rack : racks) {
			rackVO = new ShelfVO();
			rackVO.setShelfType(rack.getShelfType());
			rackVO.setName(rack.getName());
			rackVO.setConfirmSave(true);
			rackVO.setDimension(rack.getDimension());
			rackVO.setShelfId(rack.getShelfId());
			rackVO.setAisle(rack.getAisle());
			rackVO.setShelf(rack.getShelf());
			rackVO.setReferenceNumber(++i);
			shelfVOs.add(rackVO);
		}
		return shelfVOs;
	}

	public String showStoreApproval(){
		try {
			LOG.info("Inside Module: Accounts : Method: showStoreApproval");
			storeId = recordId;
			showStoreUpdate();
			LOG.info("Module: Accounts : Method: showStoreApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showStoreApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showStoreRejectEntry(){
		try {
			LOG.info("Inside Module: Accounts : Method: showStoreRejectEntry");
			storeId = recordId;
			showStoreUpdate();
			LOG.info("Module: Accounts : Method: showStoreRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showStoreRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String deleteStore() {
		try {
			Store store = storeBL.getStoreService().getStoreDetailByStoreId(
					storeId);
			storeBL.deleteStore(store);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: deleteStore: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteStore: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private String showStoreNumber() throws Exception {
		List<Store> storeList = getStoreBL().getStoreService().getAllStores(
				getImplementationId());
		if (null != storeList && storeList.size() > 0) {
			int storeSize = storeList.size() - 1;
			long number = Long.parseLong(storeList.get(storeSize)
					.getStoreNumber());
			storeNumber = String.valueOf(number += 1);
		} else {
			storeNumber = "1000";
		}
		return storeNumber;
	}

	// Show Store Shelf Details
	public String showStoreShelfDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showStoreShelfDetails");
			ServletActionContext.getRequest().setAttribute(
					"STORE_INFO",
					storeBL.addStoreVos(storeBL.getStoreService().getAllStores(
							getImplementationId())));
			LOGGER.info("Module: Accounts : Method: showStoreShelfDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: showStoreShelfDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Implementation getImplementationId() throws NullPointerException {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (null != session.getAttribute("THIS")
				&& !session.getAttribute("THIS").equals("")) {
			implementation = new Implementation();
			implementation = (Implementation) session.getAttribute("THIS");
			return implementation;
		} else
			return null;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public StoreBL getStoreBL() {
		return storeBL;
	}

	public void setStoreBL(StoreBL storeBL) {
		this.storeBL = storeBL;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public String getStoreNumber() {
		return storeNumber;
	}

	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAisleDetails() {
		return aisleDetails;
	}

	public void setAisleDetails(String aisleDetails) {
		this.aisleDetails = aisleDetails;
	}

	public long getStorageType() {
		return storageType;
	}

	public void setStorageType(long storageType) {
		this.storageType = storageType;
	}

	public long getStorageSection() {
		return storageSection;
	}

	public void setStorageSection(long storageSection) {
		this.storageSection = storageSection;
	}

	public long getBinType() {
		return binType;
	}

	public void setBinType(long binType) {
		this.binType = binType;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(String pageInfo) {
		this.pageInfo = pageInfo;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public Integer getBinReferenceId() {
		return binReferenceId;
	}

	public void setBinReferenceId(Integer binReferenceId) {
		this.binReferenceId = binReferenceId;
	}

	public Integer getSectionReferenceId() {
		return sectionReferenceId;
	}

	public void setSectionReferenceId(Integer sectionReferenceId) {
		this.sectionReferenceId = sectionReferenceId;
	}

	public String getBinDetails() {
		return binDetails;
	}

	public void setBinDetails(String binDetails) {
		this.binDetails = binDetails;
	}

	public boolean isConfirmSave() {
		return confirmSave;
	}

	public void setConfirmSave(boolean confirmSave) {
		this.confirmSave = confirmSave;
	}

	public int getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(int referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRackDetails() {
		return rackDetails;
	}

	public void setRackDetails(String rackDetails) {
		this.rackDetails = rackDetails;
	}

	public int getRackLineId() {
		return rackLineId;
	}

	public void setRackLineId(int rackLineId) {
		this.rackLineId = rackLineId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public int getAisleId() {
		return aisleId;
	}

	public void setAisleId(int aisleId) {
		this.aisleId = aisleId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
