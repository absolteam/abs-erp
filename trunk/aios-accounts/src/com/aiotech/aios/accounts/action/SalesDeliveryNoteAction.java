package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryCharge;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryPack;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.SalesDeliveryNoteBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * Sales Delivery Note
 * 
 * @author Saleem
 */
public class SalesDeliveryNoteAction extends ActionSupport {

	private static final long serialVersionUID = 2081297618806701968L;

	// Dependency
	private SalesDeliveryNoteBL salesDeliveryNoteBL;

	private static final Logger LOG = Logger
			.getLogger(SalesDeliveryNoteAction.class);

	// Variables
	private List<Object> aaData;
	private long salesDeliveryNoteId;
	private long salesPersonId;
	private long shippingTerm;
	private long shippingMethod;
	private byte status;
	private long customerId;
	private long salesOrderId;
	private byte paymentMode;
	private long salesInvoiceId;
	private long shippingDetailId;
	private long currencyId;
	private Long recordId;
	private int rowId;
	private int currentRowId;
	private String deliveryDate;
	private String description;
	private String referenceNumber;
	private String packingDetails;
	private String chargesDetails;
	private String salesDeliveryDetails;
	private String returnMessage;
	private String personName;
	private boolean continuousSales;
	private SalesDeliveryNoteVO salesDeliveryNoteVO;
	private Long alertId;
	private long creditTermId;
	private String reference1;
	private String reference2;
	private String printableContent;

	public String returnSuccess() {
		LOG.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show all Sales Delivery Note json list
	public String showAllDeliveryNotes() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllDeliveryNotes");
			aaData = salesDeliveryNoteBL.getAllDeliveryNotes();
			LOG.info("Module: Accounts : Method: showAllDeliveryNotes: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllDeliveryNotes Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSalesDeliveryApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesDeliveryApproval");
			salesDeliveryNoteId = recordId;
			showDeliveryNoteEntry();
			LOG.info("Module: Accounts : Method: showSalesDeliveryApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesDeliveryApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSalesDeliveryRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesDeliveryRejectEntry");
			salesDeliveryNoteId = recordId;
			showDeliveryNoteEntry();
			LOG.info("Module: Accounts : Method: showSalesDeliveryRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showSalesDeliveryRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Sales Delivery Note
	public String showDeliveryNoteEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showDeliveryNoteEntry");
			SalesDeliveryNoteVO salesDeliveryNoteVO = null;
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			if (salesDeliveryNoteId > 0) {
				salesDeliveryNoteVO = new SalesDeliveryNoteVO();
				SalesDeliveryNote salesDeliveryNote = salesDeliveryNoteBL
						.getSalesDeliveryNoteService().getSalesDeliveryNote(
								salesDeliveryNoteId);
				BeanUtils
						.copyProperties(salesDeliveryNoteVO, salesDeliveryNote);
				List<SalesDeliveryDetailVO> salesDeliveryDetails = new ArrayList<SalesDeliveryDetailVO>();
				SalesDeliveryDetailVO salesDeliveryDetailVO = null;
				double discount = 0;
				double totalSales = 0;
				double availableQuantity = 0;
				List<StockVO> stockVOs = null;
				for (SalesDeliveryDetail deliveryDetail : salesDeliveryNote
						.getSalesDeliveryDetails()) {
					salesDeliveryDetailVO = new SalesDeliveryDetailVO();
					BeanUtils.copyProperties(salesDeliveryDetailVO,
							deliveryDetail);
					if (null != deliveryDetail.getProductExpiry())
						salesDeliveryDetailVO.setExpiryDate(DateFormat
								.convertDateToString(deliveryDetail
										.getProductExpiry().toString()));
					if (null != deliveryDetail.getProductPackageDetail()) {
						salesDeliveryDetailVO.setPackageDetailId(deliveryDetail
								.getProductPackageDetail()
								.getProductPackageDetailId());
						salesDeliveryDetailVO.setBaseUnitName(deliveryDetail
								.getProduct().getLookupDetailByProductUnit()
								.getDisplayName()); 
					} else
						salesDeliveryDetailVO.setPackageDetailId(-1l);
					salesDeliveryDetailVO.setBaseQuantity(AIOSCommons
							.convertExponential(deliveryDetail
									.getDeliveryQuantity()));
					salesDeliveryDetailVO
							.setProductPackageVOs(salesDeliveryNoteBL
									.getSalesOrderBL()
									.getCustomerQuotationBL()
									.getPackageConversionBL()
									.getProductPackagingDetail(
											deliveryDetail.getProduct()
													.getProductId()));
					salesDeliveryDetailVO.setBasePrice(salesDeliveryNoteBL
							.getStockBL().getProductCostPrice(
									deliveryDetail.getProduct()
											.getProductPricingDetails()));
					salesDeliveryDetailVO.setStandardPrice(salesDeliveryNoteBL
							.getStockBL().getProductStandardPrice(
									deliveryDetail.getProduct()
											.getProductPricingDetails()));
					discount = 0;
					totalSales = deliveryDetail.getPackageUnit()
							* deliveryDetail.getUnitRate();
					availableQuantity = deliveryDetail.getDeliveryQuantity();
					if (null != deliveryDetail.getIsPercentage()
							&& null != deliveryDetail.getDiscount()
							&& deliveryDetail.getDiscount() > 0) {
						if (deliveryDetail.getIsPercentage()) {
							discount = ((totalSales * deliveryDetail
									.getDiscount()) / 100);
						} else {
							discount = deliveryDetail.getDiscount();
						}
						totalSales -= discount;
					}
					salesDeliveryDetailVO.setTotal(AIOSCommons
							.roundDecimals(totalSales));
					if (null != deliveryDetail.getShelf()) {
						stockVOs = salesDeliveryNoteBL.getStockBL()
								.getStockDetails(
										deliveryDetail.getProduct()
												.getProductId(),
										deliveryDetail.getShelf().getShelfId());
						if (null != stockVOs && stockVOs.size() > 0) {
							for (StockVO stockVO : stockVOs)
								availableQuantity += stockVO
										.getAvailableQuantity();
						}
					}
					salesDeliveryDetailVO
							.setStockAvailableQuantity(availableQuantity);
					salesDeliveryDetails.add(salesDeliveryDetailVO);
				}
				Collections.sort(salesDeliveryDetails,
						new Comparator<SalesDeliveryDetailVO>() {
							public int compare(SalesDeliveryDetailVO o1,
									SalesDeliveryDetailVO o2) {
								return o1.getSalesDeliveryDetailId().compareTo(
										o2.getSalesDeliveryDetailId());
							}
						});
				salesDeliveryNoteVO
						.setSalesDeliveryDetailVOs(salesDeliveryDetails);
				CommentVO comment = new CommentVO();
				if (recordId != null && salesDeliveryNote != null
						&& salesDeliveryNote.getSalesDeliveryNoteId() != 0
						&& recordId > 0) {
					comment.setRecordId(salesDeliveryNote
							.getSalesDeliveryNoteId());
					comment.setUseCase(SalesDeliveryNote.class.getName());
					comment = salesDeliveryNoteBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else {
				referenceNumber = generateReferenceNumber();
				salesPersonId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			List<Currency> currencies = salesDeliveryNoteBL
					.getSalesOrderBL()
					.getCurrencyService()
					.getAllCurrency(
							salesDeliveryNoteBL.getSalesOrderBL()
									.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_ALL",
					currencies);
			ServletActionContext.getRequest().setAttribute("DELIVERY_NOTE",
					salesDeliveryNoteVO);
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_METHOD",
					salesDeliveryNoteBL.getSalesOrderBL().getLookupMasterBL()
							.getActiveLookupDetails("SHIPPING_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_TERMS",
					salesDeliveryNoteBL.getSalesOrderBL().getLookupMasterBL()
							.getActiveLookupDetails("SHIPPING_TERMS", true));
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					salesDeliveryNoteBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					salesDeliveryNoteBL.getSalesOrderBL().getLookupMasterBL()
							.getActiveLookupDetails("CHARGES_TYPE", true));
			ServletActionContext.getRequest().setAttribute("DELIVERY_STATUS",
					salesDeliveryNoteBL.getO2CProcessStatus());
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_UNIT",
					salesDeliveryNoteBL.getProductBL().getLookupMasterBL()
							.getActiveLookupDetails("PRODUCT_UNITNAME", true));
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM",
					salesDeliveryNoteBL
							.getCreditTermBL()
							.getCreditTermService()
							.getAllCustomerCreditTerms(
									salesDeliveryNoteBL.getImplementation()));
			LOG.info("Module: Accounts : Method: showDeliveryNoteEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showDeliveryNoteEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Sales Order
	public String saveDeliveryNote() {
		try {
			LOG.info("Inside Module: Accounts : Method: saveDeliveryNote");
			// Validate Period
			boolean openPeriod = salesDeliveryNoteBL.getTransactionBL()
					.getCalendarBL().transactionPostingPeriod(deliveryDate);

			if (!openPeriod) {
				returnMessage = "Period not open for the payment date.";
				LOG.info("Module: Accounts : Method: saveDeliveryNote: (Error : Period not opened)");
				return SUCCESS;
			}
			List<SalesDeliveryDetail> salesDeliveryDetail = new ArrayList<SalesDeliveryDetail>();
			String[] salesDeliveryDetails = splitValues(
					this.salesDeliveryDetails, "@#");
			for (String detail : salesDeliveryDetails)
				salesDeliveryDetail.add(addSalesDeliveryDetail(detail));
			if (salesDeliveryNoteBL.validateStockQuantity(salesDeliveryDetail)) {
				SalesDeliveryNote salesDeliveryNote = new SalesDeliveryNote();
				salesDeliveryNote.setDeliveryDate(DateFormat
						.convertStringToDate(deliveryDate));
				salesDeliveryNote.setDescription(description);

				salesDeliveryNote.setReference1(reference1);
				salesDeliveryNote.setReference2(reference2);

				Customer customer = new Customer();
				customer.setCustomerId(customerId);
				salesDeliveryNote.setCustomer(customer);
				
				if(shippingDetailId > 0){
					ShippingDetail shippingDetail = new ShippingDetail();
					shippingDetail.setShippingId(shippingDetailId);
					salesDeliveryNote.setShippingDetail(shippingDetail);
				} 
				
				LookupDetail lookupDetail = null;
				salesDeliveryNote
						.setModeOfPayment(paymentMode > 0 ? paymentMode : null);
				salesDeliveryNote.setStatus(status > 0 ? status : null);
				if (shippingMethod > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(shippingMethod);
					salesDeliveryNote
							.setLookupDetailByShippingMethod(lookupDetail);
				}
				if (shippingTerm > 0) {
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(shippingTerm);
					salesDeliveryNote
							.setLookupDetailByShippingTerm(lookupDetail);
				}
				if (salesPersonId > 0) {
					Person person = new Person();
					person.setPersonId(salesPersonId);
					salesDeliveryNote.setPersonBySalesPersonId(person);
				}

				if (salesOrderId > 0) {
					SalesOrder salesOrder = new SalesOrder();
					salesOrder.setSalesOrderId(salesOrderId);
					salesDeliveryNote.setSalesOrder(salesOrder);
				}
				if (creditTermId > 0) {
					CreditTerm creditTerm = new CreditTerm();
					creditTerm.setCreditTermId(creditTermId);
					salesDeliveryNote.setCreditTerm(creditTerm);
				}
				List<SalesDeliveryDetail> deleteDeliveryDetails = null;

				List<SalesDeliveryCharge> salesDeliveryCharge = null;
				List<SalesDeliveryCharge> deleteDeliveryCharges = null;

				List<SalesDeliveryPack> salesDeliveryPack = null;
				List<SalesDeliveryPack> deleteDeliveryPack = null;

				if (null != chargesDetails && !("").equals(chargesDetails)) {
					String[] chargesDetails = splitValues(this.chargesDetails,
							"@#");
					salesDeliveryCharge = new ArrayList<SalesDeliveryCharge>();
					for (String detail : chargesDetails) {
						salesDeliveryCharge
								.add(addSalesDeliveryCharges(detail));
					}
				}
				if (null != packingDetails && !("").equals(packingDetails)) {
					String[] packingDetails = splitValues(this.packingDetails,
							"@#");
					salesDeliveryPack = new ArrayList<SalesDeliveryPack>();
					for (String detail : packingDetails) {
						salesDeliveryPack.add(addSalesDeliveryPacks(detail));
					}
				}
				List<SalesDeliveryDetail> exitingSalesDeliveryDetails = null;
				List<SalesDeliveryCharge> exitingSalesDeliveryCharges = null;
				SalesDeliveryNote salesDeliveryNoteDB = null;
				if (salesDeliveryNoteId > 0) {
					salesDeliveryNote
							.setSalesDeliveryNoteId(salesDeliveryNoteId);
					salesDeliveryNoteDB = salesDeliveryNoteBL
							.getSalesDeliveryNoteService()
							.getBasicSalesDeliveryNote(salesDeliveryNoteId);
					salesDeliveryNote.setReferenceNumber(salesDeliveryNoteDB
							.getReferenceNumber());
					salesDeliveryNote.setCurrency(salesDeliveryNoteDB
							.getCurrency());
					salesDeliveryNote.setExchangeRate(salesDeliveryNoteDB
							.getExchangeRate());
					exitingSalesDeliveryDetails = salesDeliveryNoteBL
							.getSalesDeliveryNoteService()
							.getSalesDeliveryDetailByDeliveryId(
									salesDeliveryNoteId);
					exitingSalesDeliveryCharges = salesDeliveryNoteBL
							.getSalesDeliveryNoteService()
							.getSalesDeliveryChargesByDeliveryId(
									salesDeliveryNoteId);
					deleteDeliveryDetails = userDeletedSalesDeliveryDetails(
							salesDeliveryDetail, exitingSalesDeliveryDetails);
					deleteDeliveryCharges = userDeletedSalesDeliveryCharges(
							salesDeliveryCharge, exitingSalesDeliveryCharges);
					deleteDeliveryPack = userDeletedSalesDeliveryPacks(
							salesDeliveryPack,
							salesDeliveryNoteBL.getSalesDeliveryNoteService()
									.getSalesDeliveryPackByDeliveryId(
											salesDeliveryNoteId));
				} else
					salesDeliveryNote
							.setReferenceNumber(generateReferenceNumber());

				salesDeliveryNote.setStatusNote((byte) 1);
				SalesOrder salesOrder = null;
				if (salesOrderId > 0) {
					salesOrder = salesDeliveryNoteBL.getSalesOrderBL()
							.getSalesOrderService()
							.getBasicSalesOrder(salesOrderId);
					salesDeliveryNote.setCurrency(salesOrder.getCurrency());
					salesDeliveryNote.setExchangeRate(salesOrder
							.getExchangeRate());
					if (null != salesOrder) {
						if (null != salesOrder.getContinuousSales()
								&& (boolean) salesOrder.getContinuousSales() == continuousSales) {
							salesOrder = null;
						} else if (null == salesOrder.getContinuousSales()
								&& continuousSales == false) {
							salesOrder = null;
						} else
							salesOrder.setContinuousSales(continuousSales);
					}
				}

				if (salesOrderId == 0
						&& salesDeliveryNoteId == 0
						|| (null != salesDeliveryNoteDB && (long) salesDeliveryNoteDB
								.getCurrency().getCurrencyId() != currencyId)) {
					Currency defaultCurrency = salesDeliveryNoteBL
							.getTransactionBL().getCurrency();
					if (null != defaultCurrency) {
						if ((long) defaultCurrency.getCurrencyId() != currencyId) {
							Currency currency = salesDeliveryNoteBL
									.getTransactionBL().getCurrencyService()
									.getCurrency(currencyId);
							CurrencyConversion currencyConversion = salesDeliveryNoteBL
									.getTransactionBL()
									.getCurrencyConversionBL()
									.getCurrencyConversionService()
									.getCurrencyConversionByCurrency(currencyId);
							salesDeliveryNote.setCurrency(currency);
							salesDeliveryNote
									.setExchangeRate(null != currencyConversion ? currencyConversion
											.getConversionRate() : 1.0);
						} else {
							salesDeliveryNote.setCurrency(defaultCurrency);
							salesDeliveryNote.setExchangeRate(1.0);
						}
					}
				}

				SalesDeliveryNoteVO vo = new SalesDeliveryNoteVO();
				vo.setAlertId(alertId);
				salesDeliveryNoteBL.saveDeliveryNote(salesDeliveryNote,
						salesDeliveryDetail, salesDeliveryCharge,
						salesDeliveryPack, deleteDeliveryDetails,
						deleteDeliveryCharges, deleteDeliveryPack,
						exitingSalesDeliveryDetails,
						exitingSalesDeliveryCharges, salesOrder, vo);
				returnMessage = "SUCCESS";
				LOG.info("Module: Accounts : Method: saveDeliveryNote: Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Insufficient stock balance.";
				LOG.info("Module: Accounts : Method: saveDeliveryNote: Action Error");
				return ERROR;
			}
		} catch (Exception ex) {
			returnMessage = "System internal error.";
			LOG.info("Module: Accounts : Method: saveDeliveryNote Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Sales Order
	public String deleteSalesDeliveryNote() {
		try {
			LOG.info("Inside Module: Accounts : Method: deleteSalesDeliveryNote");
			SalesDeliveryNote salesDeliveryNote = salesDeliveryNoteBL
					.getSalesDeliveryNoteService().getSalesDeliveryNoteInfo(
							salesDeliveryNoteId);
			salesDeliveryNoteBL.deleteSalesDeliveryNote(
					salesDeliveryNote,
					new ArrayList<SalesDeliveryDetail>(salesDeliveryNote
							.getSalesDeliveryDetails()),
					new ArrayList<SalesDeliveryCharge>(salesDeliveryNote
							.getSalesDeliveryCharges()),
					new ArrayList<SalesDeliveryPack>(salesDeliveryNote
							.getSalesDeliveryPacks()));
			returnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: deleteSalesDeliveryNote: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "System internal error.";
			LOG.info("Module: Accounts : Method: deleteSalesDeliveryNote Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Add Sales Delivery Pack
	private SalesDeliveryPack addSalesDeliveryPacks(String deliveryPack) {
		SalesDeliveryPack salesDeliveryPack = new SalesDeliveryPack();
		String deliveryPacks[] = splitValues(deliveryPack, "__");
		LookupDetail productUnit = new LookupDetail();
		if (null != deliveryPacks[0] && !("##").equals(deliveryPacks[0]))
			salesDeliveryPack.setReferenceNumber(deliveryPacks[0]);
		productUnit.setLookupDetailId(Long.parseLong(deliveryPacks[1]));
		salesDeliveryPack.setLookupDetail(productUnit);
		if (null != deliveryPacks[2]
				&& Double.parseDouble(deliveryPacks[2]) > 0)
			salesDeliveryPack
					.setNoOfPacks(Double.parseDouble(deliveryPacks[2]));
		if (null != deliveryPacks[3]
				&& Double.parseDouble(deliveryPacks[3]) > 0)
			salesDeliveryPack.setGrossWeight(Double
					.parseDouble(deliveryPacks[3]));
		if (null != deliveryPacks[4] && !("##").equals(deliveryPacks[4]))
			salesDeliveryPack.setDispatcher(deliveryPacks[4]);
		if (null != deliveryPacks[5] && !("##").equals(deliveryPacks[5]))
			salesDeliveryPack.setDescription(deliveryPacks[5]);
		if (null != deliveryPacks[6] && Long.parseLong(deliveryPacks[6]) > 0)
			salesDeliveryPack.setSalesDeliveryPackId(Long
					.parseLong(deliveryPacks[6]));
		return salesDeliveryPack;
	}

	// Add Sales Delivery Charge
	private SalesDeliveryCharge addSalesDeliveryCharges(String deliveryCharge) {
		SalesDeliveryCharge salesDeliveryCharge = new SalesDeliveryCharge();
		String deliveryCharges[] = splitValues(deliveryCharge, "__");
		LookupDetail charges = new LookupDetail();
		charges.setLookupDetailId(Long.parseLong(deliveryCharges[0]));
		salesDeliveryCharge.setLookupDetail(charges);
		if (("true").equals(deliveryCharges[1]))
			salesDeliveryCharge.setIsPercentage(true);
		else
			salesDeliveryCharge.setIsPercentage(false);
		salesDeliveryCharge.setCharges(Double.parseDouble(deliveryCharges[2]));
		if (null != deliveryCharges[3] && !("##").equals(deliveryCharges[3]))
			salesDeliveryCharge.setDescription(deliveryCharges[3]);
		if (null != deliveryCharges[5]
				&& Long.parseLong(deliveryCharges[5]) > 0)
			salesDeliveryCharge.setSalesDeliveryChargeId(Long
					.parseLong(deliveryCharges[5]));
		return salesDeliveryCharge;
	}

	// Add Sales Delivery Detail
	private SalesDeliveryDetail addSalesDeliveryDetail(String deliveryDetail) {
		SalesDeliveryDetail salesDeliveryDetail = new SalesDeliveryDetail();
		String deliveryDetails[] = splitValues(deliveryDetail, "__");
		Product product = new Product();
		product.setProductId(Long.parseLong(deliveryDetails[0]));
		salesDeliveryDetail.setProduct(product);
		if (Long.parseLong(deliveryDetails[1]) > 0l) {
			Shelf shelf = new Shelf();
			shelf.setShelfId(Integer.parseInt(deliveryDetails[1]));
			salesDeliveryDetail.setShelf(shelf);
		}
		if (Double.parseDouble(deliveryDetails[2]) > 0) {
			salesDeliveryDetail.setOrderQuantity(Double
					.parseDouble(deliveryDetails[2]));
		}
		salesDeliveryDetail.setDeliveryQuantity(Double
				.parseDouble(deliveryDetails[3]));
		salesDeliveryDetail.setUnitRate(Double.parseDouble(deliveryDetails[4]));
		if (null != deliveryDetails[5] && !("##").equals(deliveryDetails[5])) {
			if (("true").equals(deliveryDetails[5]))
				salesDeliveryDetail.setIsPercentage(true);
			else
				salesDeliveryDetail.setIsPercentage(false);
			salesDeliveryDetail.setDiscount(Double
					.parseDouble(deliveryDetails[6]));
		}
		if (null != deliveryDetails[7]
				&& Long.parseLong(deliveryDetails[7]) > 0)
			salesDeliveryDetail.setSalesDeliveryDetailId(Long
					.parseLong(deliveryDetails[7]));
		if (null != deliveryDetails[8]
				&& Long.parseLong(deliveryDetails[8]) > 0) {
			SalesOrderDetail orderDetail = new SalesOrderDetail();
			orderDetail.setSalesOrderDetailId(Long
					.parseLong(deliveryDetails[8]));
			salesDeliveryDetail.setSalesOrderDetail(orderDetail);
		}
		if (null != deliveryDetails[9] && !("##").equals(deliveryDetails[9]))
			salesDeliveryDetail.setBatchNumber(deliveryDetails[9]);
		if (null != deliveryDetails[10] && !("##").equals(deliveryDetails[10]))
			salesDeliveryDetail.setProductExpiry(DateFormat
					.convertStringToDate(deliveryDetails[10]));
		if (null != deliveryDetails[11]
				&& Long.parseLong(deliveryDetails[11]) > 0) {
			ProductPackageDetail productPackageDetail = new ProductPackageDetail();
			productPackageDetail.setProductPackageDetailId(Long
					.parseLong(deliveryDetails[11]));
			salesDeliveryDetail.setProductPackageDetail(productPackageDetail);
		}
		salesDeliveryDetail.setPackageUnit(Double
				.parseDouble(deliveryDetails[12]));
		return salesDeliveryDetail;
	}

	// Compare exiting Sales Delivery Pack with updated Delivery Pack
	private List<SalesDeliveryPack> userDeletedSalesDeliveryPacks(
			List<SalesDeliveryPack> salesDeliveryPack,
			List<SalesDeliveryPack> salesDeliveryPackOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, SalesDeliveryPack> hashSalesDeliveryPack = new HashMap<Long, SalesDeliveryPack>();
		if (null != salesDeliveryPackOld && salesDeliveryPackOld.size() > 0) {
			for (SalesDeliveryPack list : salesDeliveryPackOld) {
				listOne.add(list.getSalesDeliveryPackId());
				hashSalesDeliveryPack.put(list.getSalesDeliveryPackId(), list);
			}
			for (SalesDeliveryPack list : salesDeliveryPack) {
				listTwo.add(list.getSalesDeliveryPackId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<SalesDeliveryPack> salesDeliveryPackList = null;
		if (null != different && different.size() > 0) {
			SalesDeliveryPack tempSalesDeliveryPack = null;
			salesDeliveryPackList = new ArrayList<SalesDeliveryPack>();
			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempSalesDeliveryPack = new SalesDeliveryPack();
					tempSalesDeliveryPack = hashSalesDeliveryPack.get(list);
					salesDeliveryPackList.add(tempSalesDeliveryPack);
				}
			}
		}
		return salesDeliveryPackList;
	}

	// Compare exiting Sales Delivery Charges with updated Delivery Charges
	private List<SalesDeliveryCharge> userDeletedSalesDeliveryCharges(
			List<SalesDeliveryCharge> salesDeliveryCharge,
			List<SalesDeliveryCharge> salesDeliveryChargesByOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, SalesDeliveryCharge> hashSalesOrderCharges = new HashMap<Long, SalesDeliveryCharge>();
		if (null != salesDeliveryChargesByOld
				&& salesDeliveryChargesByOld.size() > 0) {
			for (SalesDeliveryCharge list : salesDeliveryChargesByOld) {
				listOne.add(list.getSalesDeliveryChargeId());
				hashSalesOrderCharges
						.put(list.getSalesDeliveryChargeId(), list);
			}
			if (null != salesDeliveryCharge && salesDeliveryCharge.size() > 0) {
				for (SalesDeliveryCharge list : salesDeliveryCharge) {
					listTwo.add(list.getSalesDeliveryChargeId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<SalesDeliveryCharge> salesDeliveryChargeList = null;
		if (null != different && different.size() > 0) {
			SalesDeliveryCharge tempSalesDeliveryCharge = null;
			salesDeliveryChargeList = new ArrayList<SalesDeliveryCharge>();
			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempSalesDeliveryCharge = new SalesDeliveryCharge();
					tempSalesDeliveryCharge = hashSalesOrderCharges.get(list);
					salesDeliveryChargeList.add(tempSalesDeliveryCharge);
				}
			}
		}
		return salesDeliveryChargeList;
	}

	// Compare exiting Sales Delivery Details with updated Delivery Details
	private List<SalesDeliveryDetail> userDeletedSalesDeliveryDetails(
			List<SalesDeliveryDetail> salesDeliveryDetail,
			List<SalesDeliveryDetail> salesDeliveryDetailOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, SalesDeliveryDetail> hashSalesCharges = new HashMap<Long, SalesDeliveryDetail>();
		if (null != salesDeliveryDetailOld && salesDeliveryDetailOld.size() > 0) {
			for (SalesDeliveryDetail list : salesDeliveryDetailOld) {
				listOne.add(list.getSalesDeliveryDetailId());
				hashSalesCharges.put(list.getSalesDeliveryDetailId(), list);
			}
			for (SalesDeliveryDetail list : salesDeliveryDetail) {
				listTwo.add(list.getSalesDeliveryDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<SalesDeliveryDetail> salesDetailList = new ArrayList<SalesDeliveryDetail>();
		if (null != different && different.size() > 0) {
			SalesDeliveryDetail tempSalesDeliveryDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempSalesDeliveryDetail = new SalesDeliveryDetail();
					tempSalesDeliveryDetail = hashSalesCharges.get(list);
					salesDetailList.add(tempSalesDeliveryDetail);
				}
			}
		}
		return salesDetailList;
	}

	// Delivery Note Order Add Row
	public String deliveryNoteDetailAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: deliveryNoteDetailAddRow");
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: salesOrderDetailAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: deliveryNoteDetailAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delivery Note Charges Add Row
	public String deliveryNoteChargesAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: deliveryNoteChargesAddRow");
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					salesDeliveryNoteBL.getSalesOrderBL().getLookupMasterBL()
							.getActiveLookupDetails("CHARGES_TYPE", true));
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: deliveryNoteChargesAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: deliveryNoteChargesAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Sales Order Packs Add Row
	public String deliveryNotePacksAddRow() {
		try {
			LOG.info("Inside Module: Accounts : Method: deliveryNotePacksAddRow");
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_UNIT",
					salesDeliveryNoteBL.getProductBL().getLookupMasterBL()
							.getActiveLookupDetails("PRODUCT_UNITNAME", true));
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			LOG.info("Module: Accounts : Method: deliveryNotePacksAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: deliveryNotePacksAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get Active Delivery Notes by customer id
	public String showActiveDeliveryNote() {
		try {
			LOG.info("Inside Module: Accounts : Method: showActiveDeliveryNote");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("DELIVERY_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<SalesDeliveryDetail> customerNotes = salesDeliveryNoteBL
					.getSalesDeliveryNoteService().getNonInvoicedDeliveryNotes(
							customerId, currencyId);
			if (null != customerNotes && customerNotes.size() > 0) {
				List<SalesDeliveryNoteVO> customerActiveNotes = salesDeliveryNoteBL
						.validateSalesDeliveryInvoice(customerNotes);
				if (null != customerActiveNotes
						&& customerActiveNotes.size() > 0) {
					Map<Long, SalesDeliveryNoteVO> deliverySession = new HashMap<Long, SalesDeliveryNoteVO>();
					for (SalesDeliveryNoteVO list : customerActiveNotes) {
						deliverySession
								.put(list.getSalesDeliveryNoteId(), list);
					}
					Collections.sort(customerActiveNotes,
							new Comparator<SalesDeliveryNoteVO>() {
								public int compare(SalesDeliveryNoteVO s1,
										SalesDeliveryNoteVO s2) {
									return s2
											.getSalesDeliveryNoteId()
											.compareTo(
													s1.getSalesDeliveryNoteId());
								}
							});
					session.setAttribute("DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId"), deliverySession);
					ServletActionContext.getRequest().setAttribute(
							"DELIVERY_NOTE_INFO", customerActiveNotes);
				}

				LOG.info("Module: Accounts : Method: showActiveDeliveryNote Success");
				return SUCCESS;
			} else {
				LOG.info("Module: Accounts : Method: showActiveDeliveryNote failure");
				return ERROR;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showActiveDeliveryNote: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Active Delivery Notes by customer id
	public String showActiveDeliveryNoteEdit() {
		try {
			LOG.info("Inside Module: Accounts : Method: showActiveDeliveryNote");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("DELIVERY_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<SalesDeliveryDetail> customerNotes = salesDeliveryNoteBL
					.getSalesDeliveryNoteService()
					.getNonInvoicedDeliveryNotesEdit(customerId,
							salesInvoiceId, currencyId);
			List<SalesDeliveryDetail> tempDeliveryDetails = salesDeliveryNoteBL
					.getSalesDeliveryNoteService()
					.getSalesDeliveryDetailBySalesInvoice(salesInvoiceId);
			Map<Long, SalesDeliveryNote> deliveryNoteSession = new HashMap<Long, SalesDeliveryNote>();
			for (SalesDeliveryDetail salesDeliveryDetail : tempDeliveryDetails) {
				deliveryNoteSession.put(salesDeliveryDetail
						.getSalesDeliveryNote().getSalesDeliveryNoteId(),
						salesDeliveryDetail.getSalesDeliveryNote());
			}
			if (null != customerNotes && customerNotes.size() > 0) {
				List<SalesDeliveryNoteVO> customerActiveNotes = salesDeliveryNoteBL
						.validateSalesDeliveryInvoice(customerNotes);
				if (null != customerActiveNotes
						&& customerActiveNotes.size() > 0) {
					Map<Long, SalesDeliveryNoteVO> deliverySession = new HashMap<Long, SalesDeliveryNoteVO>();
					for (SalesDeliveryNoteVO list : customerActiveNotes) {
						if (list.getSalesDeliveryNoteId() != null
								&& deliveryNoteSession.containsKey(list
										.getSalesDeliveryNoteId()))
							list.setEditFlag(true);
						else
							list.setEditFlag(false);

						deliverySession
								.put(list.getSalesDeliveryNoteId(), list);
					}
					Collections.sort(customerActiveNotes,
							new Comparator<SalesDeliveryNoteVO>() {
								public int compare(SalesDeliveryNoteVO s1,
										SalesDeliveryNoteVO s2) {
									return s2
											.getSalesDeliveryNoteId()
											.compareTo(
													s1.getSalesDeliveryNoteId());
								}
							});
					session.setAttribute("DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId"), deliverySession);
					ServletActionContext.getRequest().setAttribute(
							"DELIVERY_NOTE_INFO", customerActiveNotes);
				}

				LOG.info("Module: Accounts : Method: showActiveDeliveryNote Success");
				return SUCCESS;
			} else {
				LOG.info("Module: Accounts : Method: showActiveDeliveryNote failure");
				return ERROR;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showActiveDeliveryNote: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Fetch Sales Delivery Note Info from session by delivery id
	public String showSessionDeliveryNoteInfo() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSessionDeliveryNoteInfo");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, SalesDeliveryNoteVO> salesDeliveryNoteVOs = (Map<Long, SalesDeliveryNoteVO>) (session
					.getAttribute("DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			SalesDeliveryNoteVO salesDeliveryNoteVO = (SalesDeliveryNoteVO) salesDeliveryNoteVOs
					.get(salesDeliveryNoteId);
			ServletActionContext.getRequest().setAttribute("rowId", rowId);
			ServletActionContext.getRequest().setAttribute(
					"DELIVERY_NOTE_BYID", salesDeliveryNoteVO);
			LOG.info("Module: Accounts : Method: showSessionDeliveryNoteInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showSessionDeliveryNoteInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Fetch Delivery Detail Info from session by Delivery Note Id
	public String showSessionDeliveryDetailInfo() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSessionDeliveryDetailInfo");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, SalesDeliveryNoteVO> salesDeliveryNoteVOs = (Map<Long, SalesDeliveryNoteVO>) (session
					.getAttribute("DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			SalesDeliveryNoteVO salesDeliveryNoteVO = (SalesDeliveryNoteVO) salesDeliveryNoteVOs
					.get(salesDeliveryNoteId);
			ServletActionContext.getRequest().setAttribute(
					"SALESDELIVERY_DETAIL_BYID",
					salesDeliveryNoteVO.getSalesDeliveryDetailVOs());
			LOG.info("Module: Accounts : Method: showSessionDeliveryDetailInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showSessionDeliveryDetailInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Delivery Detail Session by Delivery Note Id
	public String updateDeliveryNoteSession() {
		try {
			LOG.info("Inside Module: Accounts : Method: updateDeliveryNoteSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, SalesDeliveryNoteVO> salesDeliveryNoteVOs = (Map<Long, SalesDeliveryNoteVO>) (session
					.getAttribute("DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			SalesDeliveryNoteVO salesDeliveryNoteVO = (SalesDeliveryNoteVO) salesDeliveryNoteVOs
					.get(salesDeliveryNoteId);
			String salesDeliveryInfo[] = splitValues(salesDeliveryDetails, "#@");
			Map<Long, List<SalesDeliveryDetailVO>> detailVOMap = new HashMap<Long, List<SalesDeliveryDetailVO>>();
			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = null;
			for (SalesDeliveryDetailVO list : salesDeliveryNoteVO
					.getSalesDeliveryDetailVOs()) {
				salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
				if (detailVOMap.containsKey(list.getSalesDeliveryDetailId()))
					salesDeliveryDetailVOs.addAll(detailVOMap.get(list
							.getSalesDeliveryDetailId()));
				salesDeliveryDetailVOs.add(list);
				detailVOMap.put(list.getSalesDeliveryDetailId(),
						salesDeliveryDetailVOs);
			}
			List<SalesDeliveryDetailVO> salesDeliveryDetailVOList = new ArrayList<SalesDeliveryDetailVO>();
			for (String deliveryInfo : salesDeliveryInfo) {
				String deliveryLine[] = splitValues(deliveryInfo, "__");
				if (detailVOMap.containsKey(Long.parseLong(deliveryLine[1]))) {
					List<SalesDeliveryDetailVO> tempDeliveryDetails = detailVOMap
							.get(Long.parseLong(deliveryLine[1]));
					for (SalesDeliveryDetailVO salesDeliveryDetailVO : tempDeliveryDetails) {
						if (null != salesDeliveryDetailVO
								.getSalesInvoiceDetailId()) {
							if (Long.parseLong(deliveryLine[3]) == salesDeliveryDetailVO
									.getSalesInvoiceDetailId()) {
								if (Double.parseDouble(deliveryLine[0]) > 0) {
									salesDeliveryDetailVO
											.setInvoiceQuantity(Double
													.parseDouble(deliveryLine[0]));
									salesDeliveryDetailVO
											.setInvoiceUnitRate(Double
													.parseDouble(deliveryLine[2]));
									salesDeliveryDetailVO.setUnitRate(Double
											.parseDouble(deliveryLine[2]));
									salesDeliveryDetailVOList
											.add(salesDeliveryDetailVO);
									break;
								} else {
									returnMessage = "Invoice Quantity should be greater than zero.";
									LOG.info("Module: Accounts : Method: updateDeliveryNoteSession Error");
									return ERROR;
								}
							}
						} else {
							if (Double.parseDouble(deliveryLine[0]) > 0) {
								salesDeliveryDetailVO.setInvoiceQuantity(Double
										.parseDouble(deliveryLine[0]));
								salesDeliveryDetailVO.setUnitRate(Double
										.parseDouble(deliveryLine[2]));
								salesDeliveryDetailVO.setInvoiceUnitRate(Double
										.parseDouble(deliveryLine[2]));
								salesDeliveryDetailVOList
										.add(salesDeliveryDetailVO);
								break;
							} else {
								returnMessage = "Invoice Quantity should be greater than zero.";
								LOG.info("Module: Accounts : Method: updateDeliveryNoteSession Error");
								return ERROR;
							}
						}
					}
				}
			}
			salesDeliveryNoteVO
					.setSalesDeliveryDetailVOs(salesDeliveryDetailVOList);
			salesDeliveryNoteVOs.put(
					salesDeliveryNoteVO.getSalesDeliveryNoteId(),
					salesDeliveryNoteVO);
			session.setAttribute(
					"DELIVERY_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId"),
					salesDeliveryNoteVOs);
			returnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: updateDeliveryNoteSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "System, Internal error.";
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: updateDeliveryNoteSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unused")
	public String printSalesDeliveryNote() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_salesdelivery_template") != null ? ((String) session
					.getAttribute("print_salesdelivery_template"))
					.equalsIgnoreCase("true") : false;
			SalesDeliveryNoteVO vo = null;
			List<SalesDeliveryNoteVO> salesDeliveryList = null;
			SalesDeliveryNote note = salesDeliveryNoteBL
					.getSalesDeliveryNoteService().getSalesDeliveryNoteInfo(
							salesDeliveryNoteId);
			if (!isTemplatePrintEnabled) {
				salesDeliveryList = salesDeliveryNoteBL
						.fetchSalesDeliveryReportData();
				for (SalesDeliveryNoteVO salesDeliveryNoteVO : salesDeliveryList) {
					Collections.sort(
							salesDeliveryNoteVO.getSalesDeliveryDetailVOs(),
							new Comparator<SalesDeliveryDetailVO>() {
								public int compare(SalesDeliveryDetailVO s1,
										SalesDeliveryDetailVO s2) {
									return s1
											.getSalesDeliveryDetailId()
											.compareTo(
													s2.getSalesDeliveryDetailId());
								}
							});
				}
				ServletActionContext.getRequest().setAttribute(
						"SALES_DELIVERY", salesDeliveryList);
				return "default";

			} else {

				vo = salesDeliveryNoteBL.convertEntityTOVO(note);

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ salesDeliveryNoteBL.getImplementation()
								.getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-sales-delivery-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (salesDeliveryNoteBL.getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							salesDeliveryNoteBL.getImplementation()
									.getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put(
						"deliveryDate",
						note.getDeliveryDate() != null ? DateFormat
								.convertDateToString(note.getDeliveryDate()
										+ "") : DateFormat
								.convertSystemDateToString(note
										.getDeliveryDate()));

				rootMap.put("note", vo);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				return "template";
			}

		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	@SuppressWarnings("unused")
	public String printSalesDeliveryAdvice() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_salesdelivery_template") != null ? ((String) session
					.getAttribute("print_salesdelivery_template"))
					.equalsIgnoreCase("true") : false;
			SalesDeliveryNoteVO vo = null;
			List<SalesDeliveryNoteVO> salesDeliveryList = null;
			SalesDeliveryNote note = salesDeliveryNoteBL
					.getSalesDeliveryNoteService().getSalesDeliveryNoteInfo(
							salesDeliveryNoteId);
			if (!isTemplatePrintEnabled) {
				salesDeliveryList = salesDeliveryNoteBL
						.fetchSalesDeliveryReportData();
				for (SalesDeliveryNoteVO salesDeliveryNoteVO : salesDeliveryList) {
					Collections.sort(
							salesDeliveryNoteVO.getSalesDeliveryDetailVOs(),
							new Comparator<SalesDeliveryDetailVO>() {
								public int compare(SalesDeliveryDetailVO s1,
										SalesDeliveryDetailVO s2) {
									return s1
											.getSalesDeliveryDetailId()
											.compareTo(
													s2.getSalesDeliveryDetailId());
								}
							});
				}
				ServletActionContext.getRequest().setAttribute(
						"SALES_DELIVERY", salesDeliveryList);
				return SUCCESS;

			} else {

				vo = salesDeliveryNoteBL.convertEntityTOVO(note);

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ salesDeliveryNoteBL.getImplementation()
								.getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-sales-delivery-advice-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (salesDeliveryNoteBL.getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							salesDeliveryNoteBL.getImplementation()
									.getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put(
						"deliveryDate",
						note.getDeliveryDate() != null ? DateFormat
								.convertDateToString(note.getDeliveryDate()
										+ "") : DateFormat
								.convertSystemDateToString(note
										.getDeliveryDate()));

				rootMap.put("note", vo);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				return "template";
			}

		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	// Show all customer delivery note
	public String showAllCustomerDeliveryNote() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllCustomerDeliveryNote");
			List<SalesDeliveryNote> salesDeliveryNotes = salesDeliveryNoteBL
					.getSalesDeliveryNoteService().getAllDeliveryNotes(
							salesDeliveryNoteBL.getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"SALES_DELIVERY_NOTE", salesDeliveryNotes);
			LOG.info("Module: Accounts : Method: showAllCustomerDeliveryNote Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showAllCustomerDeliveryNote: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Customer Sales Delivery Detail
	public String showSalesDeliveryNoteDetail() {
		try {
			LOG.info("Inside Module: Accounts : Method: showSalesDeliveryNoteDetail");
			salesDeliveryNoteVO = salesDeliveryNoteBL
					.manipulateDeliverNote(salesDeliveryNoteBL
							.getSalesDeliveryNoteService()
							.getSalesDeliveryWithInvoiceDetails(
									salesDeliveryNoteId));
			LOG.info("Module: Accounts : Method: showSalesDeliveryNoteDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: showSalesDeliveryNoteDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Generate Reference Number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(SalesDeliveryNote.class.getName(),
				salesDeliveryNoteBL.getImplementation());
	}

	// Split the value
	private static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public SalesDeliveryNoteBL getSalesDeliveryNoteBL() {
		return salesDeliveryNoteBL;
	}

	public void setSalesDeliveryNoteBL(SalesDeliveryNoteBL salesDeliveryNoteBL) {
		this.salesDeliveryNoteBL = salesDeliveryNoteBL;
	}

	public long getSalesDeliveryNoteId() {
		return salesDeliveryNoteId;
	}

	public void setSalesDeliveryNoteId(long salesDeliveryNoteId) {
		this.salesDeliveryNoteId = salesDeliveryNoteId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getSalesPersonId() {
		return salesPersonId;
	}

	public void setSalesPersonId(long salesPersonId) {
		this.salesPersonId = salesPersonId;
	}

	public long getShippingTerm() {
		return shippingTerm;
	}

	public void setShippingTerm(long shippingTerm) {
		this.shippingTerm = shippingTerm;
	}

	public long getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(long shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(long salesOrderId) {
		this.salesOrderId = salesOrderId;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPackingDetails() {
		return packingDetails;
	}

	public void setPackingDetails(String packingDetails) {
		this.packingDetails = packingDetails;
	}

	public String getChargesDetails() {
		return chargesDetails;
	}

	public void setChargesDetails(String chargesDetails) {
		this.chargesDetails = chargesDetails;
	}

	public String getSalesDeliveryDetails() {
		return salesDeliveryDetails;
	}

	public void setSalesDeliveryDetails(String salesDeliveryDetails) {
		this.salesDeliveryDetails = salesDeliveryDetails;
	}

	public long getShippingDetailId() {
		return shippingDetailId;
	}

	public void setShippingDetailId(long shippingDetailId) {
		this.shippingDetailId = shippingDetailId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public SalesDeliveryNoteVO getSalesDeliveryNoteVO() {
		return salesDeliveryNoteVO;
	}

	public void setSalesDeliveryNoteVO(SalesDeliveryNoteVO salesDeliveryNoteVO) {
		this.salesDeliveryNoteVO = salesDeliveryNoteVO;
	}

	public long getSalesInvoiceId() {
		return salesInvoiceId;
	}

	public void setSalesInvoiceId(long salesInvoiceId) {
		this.salesInvoiceId = salesInvoiceId;
	}

	public int getCurrentRowId() {
		return currentRowId;
	}

	public void setCurrentRowId(int currentRowId) {
		this.currentRowId = currentRowId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public boolean isContinuousSales() {
		return continuousSales;
	}

	public void setContinuousSales(boolean continuousSales) {
		this.continuousSales = continuousSales;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getReference2() {
		return reference2;
	}

	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
