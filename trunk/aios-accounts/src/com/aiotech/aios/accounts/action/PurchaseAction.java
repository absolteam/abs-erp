package com.aiotech.aios.accounts.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.accounts.domain.entity.vo.QuotationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.PurchaseBL;
import com.aiotech.aios.accounts.to.PurchaseOrderTO;
import com.aiotech.aios.accounts.to.SupplierTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.POStatus;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PurchaseAction extends ActionSupport {

	private static final long serialVersionUID = 5170954676341891294L;
	// Variable Declaration
	private long purchaseId;
	private String purchaseNumber;
	private long quotationId;
	private long requisitionId;
	private char itemNature;
	private String rowId;
	private String purchaseDetails;
	private long currencyId;
	private String purchaseDate;
	private String expiryDate;
	private long paymentTermId;
	private long supplierId;
	private long siteId;
	private String returnMessage;
	private String description;
	private String contactPerson;
	private String deliveryDate;
	private String deliveryAddress;
	private String pageInfo;
	private String batchNumber;
	private String duplicateProductIds;
	private Integer status;
	private long purchaseManagerId;
	private long purchaseDetailId;
	private long shippingTermId;
	private byte paymentModeId;
	private InputStream fileInputStream;
	private Long alertId;
	private Long recordId;
	private Long messageId;
	private double unitRate;
	private double batchReceiveQuantity;
	private long productId;
	private boolean continuousPurchase;
	private StockVO stockVO;
	private List<StockVO> barcodeDetails = new ArrayList<StockVO>();
	private List<StockVO> stocks = new ArrayList<StockVO>();
	private String received;
	private String supplier;

	private List<PurchaseOrderTO> purchaseOrderDS;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	// Common Object
	private Implementation implementation = null;
	private PurchaseBL purchaseBL;

	private List<SupplierTO> suppliersList;
	private String printableContent;

	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(PurchaseAction.class);

	public String returnSuccess() {
		LOGGER.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;

	}

	public String loadPurchaseOrderReportCriteria() {
		try {
			suppliersList = new ArrayList<SupplierTO>();
			suppliersList = convertListTO(purchaseBL.getSupplierService()
					.getAllSuppliers(getImplementation()));
			List<Product> products = purchaseBL.getProductBL()
					.getProductService().getOnlyProducts(getImplementation());

			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String showJsonPurchaseList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonPurchaseList");
			net.sf.json.JSONObject jsonList = purchaseBL
					.getPurchaselist(getImplementationId());
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonPurchaseList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showJsonPurchaseList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String purchaseOrderFilterSearchReport() {
		try {
			Purchase purchase = purchaseBL.getPurchaseService()
					.getPurchaseByIdWithDifferentChilds(purchaseId, true, true,
							true, true, true, true, true, true, true);

			ServletActionContext.getRequest().setAttribute("PURCHASE_INFO",
					purchase);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: purchaseOrderFilterSearchReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showJsonPurchaseReportList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showJsonPurchaseReportList");
			PurchaseVO vo = new PurchaseVO();
			if (!purchaseDate.equalsIgnoreCase("undefined")
					&& !purchaseDate.equalsIgnoreCase("null")) {
				vo.setDate(DateFormat.convertStringToDate(purchaseDate));
			}
			if (!expiryDate.equalsIgnoreCase("undefined")
					&& !expiryDate.equalsIgnoreCase("null")) {
				vo.setExpiryDate(DateFormat.convertStringToDate(expiryDate));
			}

			if (supplier != null && !supplier.equalsIgnoreCase("undefined")
					&& !supplier.equalsIgnoreCase("null")) {
				vo.setSupplierId(Long.valueOf(supplier));
			}

			if (productId > 0) {
				vo.setProductId(productId);
			}

			if (received != null && !received.equalsIgnoreCase("undefined")
					&& !received.equalsIgnoreCase("null")) {
				vo.setStatus((received.equalsIgnoreCase("true") ? 1 : 0));
			}

			if (ServletActionContext.getRequest().getParameter("departmentId") != null
					&& Long.parseLong(ServletActionContext.getRequest()
							.getParameter("departmentId")) > 0) {
				vo.setDepartmentId(Long.parseLong(ServletActionContext
						.getRequest().getParameter("departmentId")));
			}

			List<PurchaseVO> vos = purchaseBL.getPurchaseAllOrderByFilter(vo);
			net.sf.json.JSONObject jsonResponse = new net.sf.json.JSONObject();
			net.sf.json.JSONArray data = new net.sf.json.JSONArray();
			net.sf.json.JSONArray array = null;
			for (PurchaseVO purchaseVO : vos) {
				for (PurchaseDetailVO tempDetailVO : purchaseVO
						.getPurchaseDetailVO()) {
					array = new net.sf.json.JSONArray();
					array.add(purchaseVO.getPurchaseId());
					array.add(purchaseVO.getPurchaseNumber());
					array.add(purchaseVO.getFromDate());
					array.add(purchaseVO.getRequisitionNumber() + "");
					array.add(purchaseVO.getReceiveNumber() + "");
					array.add(purchaseVO.getSupplierName());
					array.add(tempDetailVO.getProduct().getCode());
					array.add(tempDetailVO.getProduct().getProductName());
					array.add(tempDetailVO.getProduct()
							.getLookupDetailByProductUnit().getDisplayName());
					array.add(tempDetailVO.getUnitRate());
					array.add(tempDetailVO.getQuantity());
					array.add(tempDetailVO.getUnitRate()
							* tempDetailVO.getQuantity());
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);

			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showJsonPurchaseReportList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showJsonPurchaseReportList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String purchaseOrdersPrintoutReport() {
		try {
			PurchaseVO vo = new PurchaseVO();
			if (!purchaseDate.equalsIgnoreCase("undefined")
					&& !purchaseDate.equalsIgnoreCase("null")) {
				vo.setDate(DateFormat.convertStringToDate(purchaseDate));
			}
			if (!expiryDate.equalsIgnoreCase("undefined")
					&& !expiryDate.equalsIgnoreCase("null")) {
				vo.setExpiryDate(DateFormat.convertStringToDate(expiryDate));
			}

			if (supplier != null && !supplier.equalsIgnoreCase("undefined")
					&& !supplier.equalsIgnoreCase("null")) {
				vo.setSupplierId(Long.valueOf(supplier));
			}

			if (productId > 0) {
				vo.setProductId(productId);
			}

			if (received != null && !received.equalsIgnoreCase("undefined")
					&& !received.equalsIgnoreCase("null")) {
				vo.setStatus((received.equalsIgnoreCase("true") ? 1 : 0));
			}

			if (ServletActionContext.getRequest().getParameter("departmentId") != null
					&& Long.parseLong(ServletActionContext.getRequest()
							.getParameter("departmentId")) > 0) {
				vo.setDepartmentId(Long.parseLong(ServletActionContext
						.getRequest().getParameter("departmentId")));
			}

			List<PurchaseDetailVO> detailVOs = new ArrayList<PurchaseDetailVO>();
			List<PurchaseVO> vos = purchaseBL.getPurchaseAllOrderByFilter(vo);
			PurchaseVO tempVO = null;
			Double total = 0.0;
			for (PurchaseVO purchaseVO : vos) {
				for (PurchaseDetailVO tempDetailVO : purchaseVO
						.getPurchaseDetailVO()) {
					tempVO = new PurchaseVO();
					BeanUtils.copyProperties(tempVO, purchaseVO);
					tempDetailVO.setPurchaseVO(tempVO);
					total += tempDetailVO.getTotalRate();
					detailVOs.add(tempDetailVO);
				}
			}

			ServletActionContext.getRequest().setAttribute("PURCHASE_LIST",
					detailVOs);
			ServletActionContext.getRequest().setAttribute("TOTAL_PURCHASE",
					AIOSCommons.roundTwoDecimals(total));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: purchaseOrderFilterSearchReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Purchase Order filter search for report as XLS
	public String purchaseOrderFilterSearchReportXLS() {
		try {

			Purchase purchase = purchaseBL.getPurchaseService()
					.getPurchaseByIdWithDifferentChilds(purchaseId, true, true,
							true, true, true, true, true, true, true);

			String str = "";

			String fromDatee = "-NA-";
			String toDatee = "-NA-";
			String isQuoted = "-NA-";
			String product = "-NA-";

			if (!purchaseDate.equalsIgnoreCase("undefined")
					&& !purchaseDate.equalsIgnoreCase("null")) {
				fromDatee = purchaseDate;
			}
			if (!expiryDate.equalsIgnoreCase("undefined")
					&& !expiryDate.equalsIgnoreCase("null")) {
				toDatee = expiryDate;
			}
			if (!received.equalsIgnoreCase("undefined")
					&& !received.equalsIgnoreCase("null")) {
				isQuoted = received;
			}
			if (supplier != null && !supplier.equalsIgnoreCase("undefined")
					&& !supplier.equalsIgnoreCase("null")) {
				product = supplier;
			}

			str = "Purchase Details\n\n";
			str += "Filter Condition\n";
			str += "From Date,To Date,Supplier,Is Received\n";
			str += fromDatee + "," + toDatee + "," + product + "," + isQuoted
					+ "\n\n";
			if (purchase.getPurchaseDetails() != null
					&& purchase.getPurchaseDetails().size() > 0) {

				str += "\nPurchase Number, " + purchase.getPurchaseNumber()
						+ ", Supplier Number, "
						+ purchase.getSupplier().getSupplierNumber();

				str += "\nQuotation, ";
				if (purchase.getQuotation() != null) {
					str += purchase.getQuotation().getQuotationNumber();
				}
				str += ", Supplier Name, ";
				if (purchase.getSupplier() != null
						&& purchase.getSupplier().getPerson() != null) {
					str += purchase.getSupplier().getPerson().getFirstName()
							+ " "
							+ purchase.getSupplier().getPerson().getLastName();
				}
				str += "\nDate, "
						+ (DateFormat.convertDateToString(purchase.getDate()
								.toString())) + ", Payment Terms, ";
				if (purchase.getCreditTerm() != null) {
					str += purchase.getCreditTerm().getName();
				}
				str += "\nExpiry Date , "
						+ (DateFormat.convertDateToString(purchase
								.getExpiryDate().toString())) + ", Currency, "
						+ purchase.getCurrency().getCurrencyPool().getCode();
				str += "\nStatus, " + purchase.getStatus() + ", Description, "
						+ purchase.getDescription();

				str += "\n\nItem Type, Product, UOM, Unit Rate, Quantity, Total Amount, Description";
				str += "\n";
				for (PurchaseDetail purchaseDetail : purchase
						.getPurchaseDetails()) {
					str += purchaseDetail.getProduct().getItemType()
							+ ","
							+ purchaseDetail.getProduct().getProductName()
							+ ","
							+ purchaseDetail.getProduct()
									.getLookupDetailByProductUnit()
									.getDisplayName() + ","
							+ purchaseDetail.getUnitRate() + ","
							+ purchaseDetail.getQuantity() + ","
							+ purchaseDetail.getUnitRate()
							* purchaseDetail.getQuantity() + ","
							+ purchaseDetail.getDescription();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String populatePurchaseOrdersJasperReportData() {
		try {
			purchaseOrderDS = new ArrayList<PurchaseOrderTO>();
			PurchaseOrderTO to = new PurchaseOrderTO();

			Purchase purchase = purchaseBL.getPurchaseService()
					.getPurchaseByIdWithDifferentChilds(purchaseId, true, true,
							true, true, true, true, true, true, true);

			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			to.convertToPurchaseOrderTO(purchase);

			to.setCreatedDate(DateFormat.convertSystemDateToString(new Date()));

			purchaseOrderDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			/*
			 * if (contractDS != null && contractDS.size() == 0) {
			 * contractDS.add(to); } else { contractDS.set(0, to); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String showPurchaseEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showPurchaseEntry");
		try {
			List<CreditTerm> paymentList = purchaseBL.getCreditTermBL()
					.getCreditTermService()
					.getAllSupplierCreditTerms(getImplementationId());
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_NUMBER",
					SystemBL.getReferenceStamp(Purchase.class.getName(),
							getImplementationId()));
			List<Currency> currencyList = getPurchaseBL().getCurrencyList(
					getImplementationId());
			for (Currency curList : currencyList) {
				if (curList.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", curList.getCurrencyId());
				}
			}
			Map<Byte, String> modeOfPayment = new HashMap<Byte, String>();
			for (ModeOfPayment e : EnumSet.allOf(ModeOfPayment.class)) {
				modeOfPayment.put(e.getCode(), e.name().replaceAll("_", " "));
			}
			Map<Byte, POStatus> poStatus = new HashMap<Byte, POStatus>();
			for (POStatus e : EnumSet.allOf(POStatus.class)) {
				poStatus.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_TERMS",
					purchaseBL.getLookupMasterBL().getActiveLookupDetails(
							"SHIPPING_TERMS", true));
			ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
					modeOfPayment);
			ServletActionContext.getRequest().setAttribute("PO_STATUS",
					poStatus);
			ServletActionContext.getRequest().setAttribute("CURRENCY_DETAIL",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_DETAIL",
					paymentList);
			LOGGER.info("Module: Accounts : Method: showPurchaseEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPurchaseUpdate() {
		LOGGER.info("Inside Module: Accounts : Method: showPurchaseUpdate");
		try {
			if (recordId != null && recordId > 0) {
				purchaseId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (purchaseId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = purchaseBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			Purchase purchase = getPurchaseBL().getPurchaseService()
					.getPurchaseOrder(purchaseId);
			List<PurchaseDetail> detailList = purchaseBL.getPurchaseService()
					.getPurchaseDetailOrderById(purchaseId);
			List<PurchaseDetailVO> purchaseDetailVOs = new ArrayList<PurchaseDetailVO>();
			PurchaseDetailVO purchaseDetailVO = null;
			for (PurchaseDetail purchaseDetail : detailList) {
				purchaseDetailVO = new PurchaseDetailVO();
				BeanUtils.copyProperties(purchaseDetailVO, purchaseDetail);
				if (null != purchaseDetail.getProductPackageDetail()) {
					purchaseDetailVO.setPackageDetailId(purchaseDetail
							.getProductPackageDetail()
							.getProductPackageDetailId());
					purchaseDetailVO.setBaseUnitName(purchaseDetail
							.getProduct().getLookupDetailByProductUnit()
							.getDisplayName());
				} else {
					purchaseDetailVO.setPackageUnit(purchaseDetail
							.getQuantity());
					purchaseDetailVO.setPackageDetailId(-1l);
				}
				purchaseDetailVO.setBaseQuantity(AIOSCommons
						.convertExponential(purchaseDetail.getQuantity()));
				purchaseDetailVO.setProductPackageVOs(purchaseBL
						.getPackageConversionBL().getProductPackagingDetail(
								purchaseDetail.getProduct().getProductId()));
				purchaseDetailVO.setExpiryDate(null != purchaseDetail
						.getProductExpiry() ? DateFormat
						.convertDateToString(purchaseDetail.getProductExpiry()
								.toString()) : null);
				purchaseDetailVOs.add(purchaseDetailVO);
			}
			Collections.sort(purchaseDetailVOs,
					new Comparator<PurchaseDetail>() {
						public int compare(PurchaseDetail o1, PurchaseDetail o2) {
							return o1.getPurchaseDetailId().compareTo(
									o2.getPurchaseDetailId());
						}
					});
			List<CreditTerm> paymentList = purchaseBL.getCreditTermBL()
					.getCreditTermService()
					.getAllSupplierCreditTerms(getImplementationId());
			List<Currency> currencyList = getPurchaseBL().getCurrencyList(
					getImplementationId());
			Map<Byte, String> modeOfPayment = new HashMap<Byte, String>();
			for (ModeOfPayment e : EnumSet.allOf(ModeOfPayment.class)) {
				modeOfPayment.put(e.getCode(), e.name().replaceAll("_", " "));
			}
			Map<Byte, POStatus> poStatus = new HashMap<Byte, POStatus>();
			for (POStatus e : EnumSet.allOf(POStatus.class)) {
				poStatus.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute(
					"SHIPPING_TERMS",
					purchaseBL.getLookupMasterBL().getActiveLookupDetails(
							"SHIPPING_TERMS", true));
			ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
					modeOfPayment);
			ServletActionContext.getRequest().setAttribute("PO_STATUS",
					poStatus);
			ServletActionContext.getRequest().setAttribute("CURRENCY_DETAIL",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_DETAIL",
					paymentList);
			ServletActionContext.getRequest().setAttribute("PURCHASE_INFO",
					purchase);
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_DETAIL_INFO", purchaseDetailVOs);
			LOGGER.info("Module: Accounts : Method: showPurchaseUpdate: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseUpdate: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showQuotation() {
		LOGGER.info("Inside Module: Accounts : Method: showQuotation");
		try {
			List<Quotation> quotationList = getPurchaseBL().getQuotationBL()
					.getQuotationService()
					.getUnPurchasedQuotation(getImplementationId());
			ServletActionContext.getRequest().setAttribute("QUOTATION_DETAIL",
					quotationList);
			LOGGER.info("Module: Accounts : Method: showQuotation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showQuotation: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showQuotationDetail() {
		LOGGER.info("Inside Module: Accounts : Method: showQuotationDetail");
		try {
			List<QuotationDetail> quotationDetailList = purchaseBL
					.getQuotationBL().getQuotationService()
					.getQuotationDetail(quotationId);
			Collections.sort(quotationDetailList,
					new Comparator<QuotationDetail>() {
						public int compare(QuotationDetail o1,
								QuotationDetail o2) {
							return o1.getQuotationDetailId().compareTo(
									o2.getQuotationDetailId());
						}
					});
			List<QuotationDetailVO> quotationDetailVOs = new ArrayList<QuotationDetailVO>();
			QuotationDetailVO quotationDetailVO = null;
			for (QuotationDetail quotationDetail : quotationDetailList) {
				quotationDetailVO = new QuotationDetailVO();
				BeanUtils.copyProperties(quotationDetailVO, quotationDetail);
				quotationDetailVO.setItemTypeStr(ItemType.get(
						quotationDetail.getProduct().getItemType()).name());
				quotationDetailVO.setProductPackageVOs(purchaseBL
						.getPackageConversionBL().getProductPackagingDetail(
								quotationDetail.getProduct().getProductId()));
				quotationDetailVO.setPackageUnit(quotationDetail
						.getTotalUnits());
				quotationDetailVOs.add(quotationDetailVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"QUOTATION_LINE_DETAIL", quotationDetailVOs);
			LOGGER.info("Module: Accounts : Method: showQuotationDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showQuotationDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductDetail() {
		LOGGER.info("Inside Module: Accounts : Method: showProductDetail");
		try {
			List<Product> productList = getPurchaseBL().getProductBL()
					.getProductService()
					.getActiveProduct(getImplementationId(), itemNature);
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAIL",
					productList);
			LOGGER.info("Module: Accounts : Method: showProductDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showProductDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String savePurchaseOrder() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: savePurchaseOrder");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			duplicateProductIds = "";
			List<String> productDuplicateIds = purchaseBL
					.validatePurchaseOrder(purchaseDetails);
			if (null == productDuplicateIds || productDuplicateIds.size() == 0) {
				Purchase purchase = new Purchase();
				Requisition requisition = null;
				purchase.setDate(DateFormat.convertStringToDate(purchaseDate));
				purchase.setExpiryDate(DateFormat
						.convertStringToDate(expiryDate));
				if (supplierId > 0) {
					Supplier supplier = new Supplier();
					supplier.setSupplierId(supplierId);
					purchase.setSupplier(supplier);
				}
				purchase.setDescription(description);
				purchase.setContinuousPurchase(continuousPurchase);
				purchase.setDeliveryDate(deliveryDate != null
						&& !deliveryDate.equals("") ? DateFormat
						.convertStringToDate(deliveryDate) : null);
				purchase.setDeliveryAddress(deliveryAddress);
				purchase.setStatus(status);
				if (purchaseId > 0)
					purchase.setPurchaseId(purchaseId);
				if (quotationId > 0) {
					Quotation quotation = new Quotation();
					quotation.setQuotationId(quotationId);
					purchase.setQuotation(quotation);
				}

				if (requisitionId > 0) {
					requisition = purchaseBL.getQuotationBL()
							.getRequisitionService()
							.getRequisitionById(requisitionId);
					requisition.setStatus(RequisitionProcessStatus.InProgress
							.getCode());
					purchase.setRequisition(requisition);
				}
				if (paymentTermId > 0) {
					CreditTerm creditTerm = new CreditTerm();
					creditTerm.setCreditTermId(paymentTermId);
					purchase.setCreditTerm(creditTerm);
				}
				if (purchaseManagerId > 0) {
					Person person = new Person();
					person.setPersonId(purchaseManagerId);
					purchase.setPersonByPurchaseManager(person);
				}
				if (shippingTermId > 0) {
					LookupDetail lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(shippingTermId);
					purchase.setLookupDetail(lookupDetail);
				}
				purchase.setModeOfPayment(paymentModeId > 0 ? paymentModeId
						: null);
				purchase.setImplementation(getImplementationId());
				if (purchaseId == 0)
					purchase.setPurchaseNumber(SystemBL.getReferenceStamp(
							Purchase.class.getName(), getImplementationId()));

				if (ServletActionContext.getRequest().getParameter("useCase") != null) {
					purchase.setUseCase(ServletActionContext.getRequest()
							.getParameter("useCase").toString());
					purchase.setRecordId(recordId);
				}

				List<PurchaseDetail> purchaseDetailList = new ArrayList<PurchaseDetail>();
				PurchaseDetail detail = null;
				Product product = null;
				RequisitionDetail requisitionDetail = null;
				ProductPackageDetail productPackageDetail = null;
				Shelf shelf = null;
				JSONParser parser = new JSONParser();
				Object purchaseDetailObject = parser.parse(purchaseDetails);
				JSONArray object = (JSONArray) purchaseDetailObject;
				for (Iterator<?> iterator = object.iterator(); iterator
						.hasNext();) {
					JSONObject jsonObject = (JSONObject) iterator.next();
					JSONArray detailArray = (JSONArray) jsonObject
							.get("purchsaeDetailJS");
					JSONObject jsonObjectDetail = (JSONObject) detailArray
							.get(0);

					detail = new PurchaseDetail();
					product = new Product();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"productId").toString()));
					detail.setProduct(product);
					detail.setQuantity(Double.parseDouble(jsonObjectDetail.get(
							"productQty").toString()));
					detail.setUnitRate(Double.parseDouble(jsonObjectDetail.get(
							"unitRate").toString()));
					detail.setDescription(null != jsonObjectDetail
							.get("linesDescription") ? jsonObjectDetail.get(
							"linesDescription").toString() : null);
					detail.setBatchNumber((null != jsonObjectDetail
							.get("batchNumber") && !("")
							.equals(jsonObjectDetail.get("batchNumber")
									.toString().trim())) ? jsonObjectDetail
							.get("batchNumber").toString() : null);
					detail.setProductExpiry(null != jsonObjectDetail
							.get("productExpiry") ? DateFormat
							.convertStringToDate(jsonObjectDetail.get(
									"productExpiry").toString()) : null);
					if (null != jsonObjectDetail.get("requisitionDetailId")
							&& Long.parseLong(jsonObjectDetail.get(
									"requisitionDetailId").toString()) > 0) {
						requisitionDetail = new RequisitionDetail();
						requisitionDetail.setRequisitionDetailId(Long
								.parseLong(jsonObjectDetail.get(
										"requisitionDetailId").toString()));
						detail.setRequisitionDetail(requisitionDetail);
					}
					detail.setPackageUnit(Double.parseDouble(jsonObjectDetail
							.get("packageUnit").toString()));
					if (null != jsonObjectDetail.get("packageDetailId")
							&& Long.parseLong(jsonObjectDetail.get(
									"packageDetailId").toString()) > 0) {
						productPackageDetail = new ProductPackageDetail();
						productPackageDetail.setProductPackageDetailId(Long
								.parseLong(jsonObjectDetail.get(
										"packageDetailId").toString()));
						detail.setProductPackageDetail(productPackageDetail);
					}
					if (null != jsonObjectDetail.get("shelfId")
							&& Integer.parseInt(jsonObjectDetail.get("shelfId")
									.toString()) > 0) {
						shelf = new Shelf();
						shelf.setShelfId(Integer.parseInt(jsonObjectDetail.get(
								"shelfId").toString()));
						detail.setShelf(shelf);
					}
					detail.setPurchaseDetailId((null != jsonObjectDetail
							.get("purchaseDetailId") && Long
							.parseLong(jsonObjectDetail.get("purchaseDetailId")
									.toString()) > 0) ? Long
							.parseLong(jsonObjectDetail.get("purchaseDetailId")
									.toString()) : null);
					purchaseDetailList.add(detail);
				}

				List<PurchaseDetail> deletePurchaseDetails = null;
				List<PurchaseDetail> purchaseDList = null;
				if (purchaseId > 0) {
					try {
						purchaseDList = getPurchaseBL().getPurchaseService()
								.getPurchaseDetailOrderById(purchaseId);
						Collection<Long> listOne = new ArrayList<Long>();
						Collection<Long> listTwo = new ArrayList<Long>();
						Map<Long, PurchaseDetail> hashPurchaseDetail = new HashMap<Long, PurchaseDetail>();
						if (null != purchaseDList && purchaseDList.size() > 0) {
							for (PurchaseDetail list : purchaseDList) {
								listOne.add(list.getPurchaseDetailId());
								hashPurchaseDetail.put(
										list.getPurchaseDetailId(), list);
							}
							if (null != hashPurchaseDetail
									&& hashPurchaseDetail.size() > 0) {
								for (PurchaseDetail list : purchaseDetailList) {
									listTwo.add(list.getPurchaseDetailId());
								}
							}
						}
						Collection<Long> similar = new HashSet<Long>(listOne);
						Collection<Long> different = new HashSet<Long>();
						different.addAll(listOne);
						different.addAll(listTwo);
						similar.retainAll(listTwo);
						different.removeAll(similar);
						if (null != different && different.size() > 0) {
							PurchaseDetail tempPurchaseDetail = null;
							deletePurchaseDetails = new ArrayList<PurchaseDetail>();
							for (Long list : different) {
								if (null != list && !list.equals(0)) {
									tempPurchaseDetail = new PurchaseDetail();
									tempPurchaseDetail = hashPurchaseDetail
											.get(list);
									deletePurchaseDetails
											.add(tempPurchaseDetail);
								}
							}
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				List<Stock> stocks = new ArrayList<Stock>();
				Purchase purchaseOld = null;
				if (purchaseId > 0) {
					purchaseOld = purchaseBL.getPurchaseService()
							.getPurchaseOrderSupplier(purchaseId);
					purchase.setPurchaseNumber(purchaseOld.getPurchaseNumber());
					purchase.setCreatedDate(purchaseOld.getCreatedDate());
					purchase.setPersonByCreatedBy(purchaseOld
							.getPersonByCreatedBy());
					purchase.setCurrency(purchaseOld.getCurrency());
					purchase.setExchangeRate(purchaseOld.getExchangeRate());

					Stock stock = null;
					if (null == purchaseOld.getSupplier()
							&& null != purchase.getSupplier()) {
						// Stock update
						for (PurchaseDetail purchaseDetail : purchaseOld
								.getPurchaseDetails()) {
							stock = new Stock();
							shelf = purchaseBL
									.getStockBL()
									.getStoreBL()
									.getStoreService()
									.getStoreByShelfId(
											purchaseDetail.getShelf()
													.getShelfId());
							stock.setProduct(purchaseDetail.getProduct());
							stock.setQuantity(purchaseDetail.getQuantity());
							stock.setShelf(shelf);
							stock.setStore(shelf.getShelf().getAisle()
									.getStore());
							stock.setUnitRate(purchaseDetail.getUnitRate());
							stock.setImplementation(purchaseBL
									.getImplemenationId());
							stock.setBatchNumber(purchaseDetail
									.getBatchNumber());
							stock.setProductExpiry(purchaseDetail
									.getProductExpiry());
							stocks.add(stock);
						}
					}
				}

				if (purchaseId == 0
						|| (null != purchaseOld && (long) purchaseOld
								.getCurrency().getCurrencyId() != currencyId)) {
					Currency defaultCurrency = purchaseBL.getTransactionBL()
							.getCurrency();
					if (null != defaultCurrency) {
						if ((long) defaultCurrency.getCurrencyId() != currencyId) {
							Currency currency = purchaseBL.getTransactionBL()
									.getCurrencyService()
									.getCurrency(currencyId);
							CurrencyConversion currencyConversion = purchaseBL
									.getTransactionBL()
									.getCurrencyConversionBL()
									.getCurrencyConversionService()
									.getCurrencyConversionByCurrency(currencyId);
							purchase.setCurrency(currency);
							purchase.setExchangeRate(null != currencyConversion ? currencyConversion
									.getConversionRate() : 1.0);
						} else {
							purchase.setCurrency(defaultCurrency);
							purchase.setExchangeRate(1.0);
						}
					}
				}
				String unifiedPrice = "";
				if (session.getAttribute("unified_price") != null)
					unifiedPrice = session.getAttribute("unified_price")
							.toString();
				if (purchase.getSupplier() != null
						&& purchase.getSupplier().getSupplierId() != null) {
					purchaseBL.savePurchaseOrder(purchase, purchaseDetailList,
							deletePurchaseDetails, stocks, unifiedPrice,
							requisition);
				} else {
					purchaseBL.savePurchaseOrderWithOutSupplier(purchase,
							purchaseDetailList, deletePurchaseDetails,
							purchaseDList, unifiedPrice, requisition);
				}
				returnMessage = "SUCCESS";
				LOGGER.info("Module: Accounts : Method: savePurchaseOrder: Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Product Duplicated, Please Check.";
				for (String dup : productDuplicateIds)
					duplicateProductIds += dup + ",";
				LOGGER.info("Module: Accounts : Method: savePurchaseOrder: Action failure "
						+ returnMessage);
				return SUCCESS;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			duplicateProductIds = "";
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: savePurchaseOrder: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deletePurchaseOrder() {
		LOGGER.info("Inside Module: Accounts : Method: deletePurchaseOrder");
		try {
			Purchase purchase = getPurchaseBL().getPurchaseService()
					.getPurchaseOrderBasicById(purchaseId);
			purchaseBL
					.deletePurchaseOrder(
							purchase,
							new ArrayList<PurchaseDetail>(purchase
									.getPurchaseDetails()));
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: deletePurchaseOrder: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			LOGGER.info("Module: Accounts : Method: deletePurchaseOrder: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPurchaseOrderPrint() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPurchaseOrderPrint");

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isLPOTemplatePrintEnabled = session
					.getAttribute("print_lpo_template") != null ? ((String) session
					.getAttribute("print_lpo_template"))
					.equalsIgnoreCase("true") : false;

			Purchase purchase = purchaseBL.getPurchaseService()
					.getPurchaseOrderPrint(purchaseId);
			List<PurchaseDetailVO> purchaseDetailVOs = new ArrayList<PurchaseDetailVO>();
			PurchaseDetailVO purchaseDetailVO = null;
			double totalAmount = 0;
			for (PurchaseDetail purchaseDetail : purchase.getPurchaseDetails()) {
				purchaseDetailVO = new PurchaseDetailVO();
				BeanUtils.copyProperties(purchaseDetailVO, purchaseDetail);
				if (null != purchaseDetail.getProductPackageDetail()) {
					purchaseDetailVO.setUnitCode(purchaseDetail
							.getProductPackageDetail().getLookupDetail()
							.getDisplayName());
				} else
					purchaseDetailVO.setUnitCode(purchaseDetail.getProduct()
							.getLookupDetailByProductUnit().getDisplayName());
				purchaseDetailVO.setQuantity(null != purchaseDetail
						.getPackageUnit() ? purchaseDetail.getPackageUnit()
						: purchaseDetail.getQuantity());
				if ((double) purchaseDetail.getUnitRate() > 1)
					purchaseDetailVO.setUnitRateStr(AIOSCommons
							.formatAmount(purchaseDetail.getUnitRate()));
				else
					purchaseDetailVO.setUnitRateStr(String
							.valueOf(purchaseDetail.getUnitRate()));
				purchaseDetailVO.setTotalRateStr(AIOSCommons
						.formatAmount(purchaseDetailVO.getQuantity()
								* purchaseDetail.getUnitRate()));
				totalAmount += purchaseDetailVO.getQuantity()
						* purchaseDetail.getUnitRate();
				purchaseDetailVOs.add(purchaseDetailVO);
			}
			PurchaseVO purchaseVO = new PurchaseVO();
			BeanUtils.copyProperties(purchaseVO, purchase);
			purchaseVO.setTotalPurchaseAmountStr(AIOSCommons
					.formatAmount(totalAmount));

			String amountInWords = AIOSCommons
					.convertAmountToWords(totalAmount);
			String decimalAmount = AIOSCommons.formatAmount(totalAmount);
			decimalAmount = decimalAmount.substring(decimalAmount
					.lastIndexOf('.') + 1);

			if (Double.parseDouble(decimalAmount) > 0) {
				amountInWords = amountInWords
						.concat(" and ")
						.concat(AIOSCommons.convertAmountToWords(decimalAmount))
						.concat(" Fils ");
				String replaceWord = "Only";
				amountInWords = amountInWords.replaceAll(replaceWord, "");
				amountInWords = amountInWords.concat(" " + replaceWord);
			}
			purchaseVO.setAmountInWords(amountInWords);
			Collections.sort(purchaseDetailVOs,
					new Comparator<PurchaseDetailVO>() {
						public int compare(PurchaseDetailVO o1,
								PurchaseDetailVO o2) {
							return o1.getPurchaseDetailId().compareTo(
									o2.getPurchaseDetailId());
						}
					});

			if (!isLPOTemplatePrintEnabled) {

				ServletActionContext.getRequest().setAttribute(
						"PURCHASE_ORDER_PRINT", purchaseVO);
				ServletActionContext.getRequest().setAttribute(
						"PURCHASE_DETAIL_PRINT", purchaseDetailVOs);
				LOGGER.info("Module: Accounts : Method: showPurchaseOrderPrint: Action Success");
				return "default";
			} else {

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/lpo/"
						+ getImplementationId().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config.getTemplate("html-lpo-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementationId().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementationId().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put(
						"datetime",
						purchase.getDate() != null ? DateFormat
								.convertSystemDateToString(purchase.getDate())
								: "");

				rootMap.put("referenceNumber", purchase.getPurchaseNumber());

				rootMap.put("paymentterm",
						purchase.getCreditTerm() != null ? purchase
								.getCreditTerm().getName() : "");

				if (purchase.getSupplier() != null) {

					try {
						if (purchase.getSupplier().getCompany() != null) {

							String phone = purchase.getSupplier().getCompany()
									.getCompanyPhone();
							String address = purchase.getSupplier()
									.getCompany().getCompanyAddress();
							rootMap.put(
									"supplier",
									purchase.getSupplier().getCompany()
											.getCompanyName()
											+ ((phone != null && phone.trim()
													.length() != 0) ? " [Contact: "
													+ purchase.getSupplier()
															.getCompany()
															.getCompanyPhone()
													+ "]"
													: "")
											+ ((address != null && address
													.length() != 0) ? " [Address: "
													+ purchase
															.getSupplier()
															.getCompany()
															.getCompanyAddress()
													+ "]"
													: ""));

						} else if (purchase.getSupplier().getPerson() != null) {

							String phone = purchase.getSupplier().getPerson()
									.getMobile();
							String address = purchase.getSupplier().getPerson()
									.getPermanentAddress();
							rootMap.put(
									"supplier",
									purchase.getSupplier().getPerson()
											.getFirstName()
											+ " "
											+ ((purchase.getSupplier()
													.getPerson().getLastName() != null) ? purchase
													.getSupplier().getPerson()
													.getLastName()
													: "")
											+ ((phone != null && phone.trim()
													.length() != 0) ? " [Contact: "
													+ phone + "]"
													: "")
											+ ((address != null && address
													.trim().length() != 0) ? " [Address: "
													+ address + "]"
													: ""));
						}
					} catch (Exception e) {
						e.printStackTrace();
						rootMap.put("supplier", "N/A");
					}

				} else {

					rootMap.put("supplier", "");
				}

				rootMap.put("PURCHASE_ORDER", purchase);
				rootMap.put("PURCHASE_DETAILS", purchaseDetailVOs);

				rootMap.put("totalAmount",
						AIOSCommons.formatAmountToDouble(totalAmount));
				rootMap.put("amountInWords", amountInWords);
				rootMap.put("urlPath", urlPath);
				rootMap.put(
						"extraLines",
						purchaseDetailVOs.size() < 23 ? 23 - purchaseDetailVOs
								.size() : null);

				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseOrderPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showSupplier() {
		LOGGER.info("Inside Module: Accounts : Method: showSupplier");
		try {
			List<Supplier> supplierList = getPurchaseBL().getSupplierService()
					.getAllSuppliers(getImplementationId());
			List<SupplierTO> supplierToList = convertListTO(supplierList);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INFO",
					supplierToList);
			ServletActionContext.getRequest().setAttribute("PAGE_INFO",
					pageInfo);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSupplier: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public static List<SupplierTO> convertListTO(List<Supplier> supplierList)
			throws Exception {
		List<SupplierTO> supplierTOList = new ArrayList<SupplierTO>();
		for (Supplier list : supplierList) {
			supplierTOList.add(convertTO(list));
		}
		return supplierTOList;
	}

	public static SupplierTO convertTO(Supplier supplier) {
		SupplierTO supplierTO = new SupplierTO();
		supplierTO.setSupplierId(supplier.getSupplierId());
		supplierTO.setSupplierNumber(supplier.getSupplierNumber());
		supplierTO.setCombinationId(supplier.getCombination()
				.getCombinationId());
		if (null != supplier.getPerson())
			supplierTO.setSupplierName(supplier.getPerson().getFirstName()
					+ " " + supplier.getPerson().getLastName());
		else
			supplierTO
					.setSupplierName(null != supplier.getCmpDeptLocation() ? supplier
							.getCmpDeptLocation().getCompany().getCompanyName()
							: supplier.getCompany().getCompanyName());
		if (supplier.getSupplierType() != null)
			supplierTO.setType(Constants.Accounts.SupplierType.get(
					supplier.getSupplierType()).toString());

		if (null != supplier.getCreditTerm())
			supplierTO.setPaymentTermId(supplier.getCreditTerm()
					.getCreditTermId());
		return supplierTO;

	}

	public String purchasePDFPrintBarcode() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: purchasePDFPrintBarcode");
			Purchase purchase = purchaseBL.getPurchaseService()
					.getPurchaseOrderDetailByPurchaseId(purchaseId);
			stocks = new ArrayList<StockVO>();
			barcodeDetails = new ArrayList<StockVO>();
			stockVO = new StockVO();
			List<StockVO> tempStocks = new ArrayList<StockVO>();
			Shelf shelf = null;
			for (PurchaseDetail purchaseDetail : purchase.getPurchaseDetails()) {
				if (null != purchaseDetail.getShelf()) {
					shelf = purchaseBL
							.getStockBL()
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									purchaseDetail.getShelf().getShelfId());
					StockVO storeDetail = new StockVO();
					storeDetail.setStoreName(shelf.getShelf().getAisle()
							.getStore().getStoreName()
							+ "->");
					storeDetail.setAisleName(shelf.getShelf().getAisle()
							.getSectionName()
							+ "->");
					storeDetail.setRackName(shelf.getShelf().getName() + "->");
					storeDetail.setShelfName(shelf.getName());
					tempStocks.add(storeDetail);
				}
				for (int i = 0; i < purchaseDetail.getQuantity(); i++) {
					tempStocks.add(setProductDetails(purchaseDetail
							.getProduct()));
				}
			}
			barcodeDetails.addAll(tempStocks);
			stockVO.setBarCodeDetails(barcodeDetails);
			stocks.add(stockVO);
			LOGGER.info("Module: Accounts : Method: purchasePDFPrintBarcode Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: purchasePDFPrintBarcode: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private StockVO setProductDetails(Product product) throws Exception {
		StockVO stockVO = new StockVO();
		if (product.getProductName().length() > 20)
			stockVO.setProductName(product.getProductName().substring(0, 20));
		else
			stockVO.setProductName(product.getProductName());

		stockVO.setProductCode(product.getBarCode());
		BitMatrix bitMatrix = new Code128Writer().encode(product.getBarCode(),
				BarcodeFormat.CODE_128, 240, 70);
		BufferedImage bi = MatrixToImageWriter.toBufferedImage(bitMatrix);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "png", baos);
		baos.flush();
		stockVO.setImage(bi);
		baos.close();
		return stockVO;
	}

	public String purchaseOrderAddRow() {
		LOGGER.info("Inside Module: Accounts : Method: purchaseOrderAddRow");
		ServletActionContext.getRequest().setAttribute("rowId",
				new Integer(rowId));
		return SUCCESS;
	}

	public String purchaseDirectPaymentEntry() {
		try {
			Purchase purchase = null;
			if (recordId != null && recordId > 0) {
				PurchaseVO vo = new PurchaseVO();
				purchase = purchaseBL.getPurchaseService().getPurchaseOrder(
						recordId);
				vo.setMessageId(alertId);
				vo.setObject(purchase);
				vo.setRecordId(recordId);
				vo.setUseCase(Purchase.class.getSimpleName());
				messageId = alertId;
				purchaseBL.purchaseDirectPaymentEntry(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private List<PurchaseDetailVO> getPurchaseBatchDetail() throws Exception {
		JSONParser parser = new JSONParser();
		Object purchaseDetailObject = parser.parse(purchaseDetails);
		JSONArray object = (JSONArray) purchaseDetailObject;
		PurchaseDetailVO purchaseDetailVO = null;
		List<PurchaseDetailVO> purchaseDetailVOs = new ArrayList<PurchaseDetailVO>();
		Product product = null;
		Shelf shelf = null;
		for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
			JSONObject jsonObject = (JSONObject) iterator.next();
			JSONArray detailArray = (JSONArray) jsonObject
					.get("purchsaeDetailJS");
			JSONObject jsonObjectDetail = (JSONObject) detailArray.get(0);

			purchaseDetailVO = new PurchaseDetailVO();
			product = purchaseBL
					.getProductBL()
					.getProductService()
					.getSimpleProductById(
							Long.parseLong(jsonObjectDetail.get("productId")
									.toString()));
			shelf = ((null != jsonObjectDetail.get("shelfId") && Integer
					.parseInt(jsonObjectDetail.get("shelfId").toString()) > 0) ? purchaseBL
					.getStockBL()
					.getStoreBL()
					.getStoreService()
					.getStoreByShelfId(
							Integer.parseInt(jsonObjectDetail.get("shelfId")
									.toString())) : null);
			purchaseDetailVO.setProduct(product);
			if (null != shelf) {
				purchaseDetailVO.setStoreId(shelf.getShelf().getAisle()
						.getStore().getStoreId());
				purchaseDetailVO.setShelfId(shelf.getShelfId());
				purchaseDetailVO.setStoreName(shelf.getShelf().getAisle()
						.getStore().getStoreName()
						+ ">>"
						+ shelf.getShelf().getAisle().getSectionName()
						+ ">>"
						+ shelf.getShelf().getName()
						+ ">>"
						+ shelf.getName());
			}
			purchaseDetailVO.setQuantity(Double.parseDouble(jsonObjectDetail
					.get("productQty").toString()));
			purchaseDetailVO.setBaseQuantity(AIOSCommons
					.convertExponential(purchaseDetailVO.getQuantity()));
			purchaseDetailVO.setPackageUnit(Double.parseDouble(jsonObjectDetail
					.get("packageUnit").toString()));
			purchaseDetailVO.setPackageDetailId(Long.parseLong(jsonObjectDetail
					.get("packageDetailId").toString()));
			purchaseDetailVO.setProductPackageVOs(purchaseBL
					.getPackageConversionBL().getProductPackagingDetail(
							product.getProductId()));
			if (purchaseDetailVO.getPackageDetailId() > 0) {
				purchaseDetailVO.setProductPackageDetailVO(purchaseBL
						.getPackageConversionBL().getBaseConversionUnit(
								purchaseDetailVO.getPackageDetailId(),
								purchaseDetailVO.getQuantity(), 0));
			}
			purchaseDetailVO.setUnitRate(Double.parseDouble(jsonObjectDetail
					.get("unitRate").toString()));
			purchaseDetailVO.setDescription(null != jsonObjectDetail
					.get("linesDescription") ? jsonObjectDetail.get(
					"linesDescription").toString() : null);
			purchaseDetailVO.setBatchNumber(null != jsonObjectDetail
					.get("batchNumber") ? jsonObjectDetail.get("batchNumber")
					.toString() : null);
			purchaseDetailVO.setExpiryDate(null != jsonObjectDetail
					.get("productExpiry") ? jsonObjectDetail.get(
					"productExpiry").toString() : null);
			purchaseDetailVO.setRequisitionDetailId((null != jsonObjectDetail
					.get("requisitionDetailId") && Long
					.parseLong(jsonObjectDetail.get("requisitionDetailId")
							.toString()) > 0) ? Long.parseLong(jsonObjectDetail
					.get("requisitionDetailId").toString()) : null);
			purchaseDetailVO.setPurchaseDetailId((null != jsonObjectDetail
					.get("purchaseDetailId") && Long.parseLong(jsonObjectDetail
					.get("purchaseDetailId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("purchaseDetailId")
							.toString()) : null);
			purchaseDetailVOs.add(purchaseDetailVO);
		}
		return purchaseDetailVOs;
	}

	public String showPurchaseDetailMoreOption() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPurchaseDetailMoreOption");
			List<PurchaseDetailVO> purchaseDetailVOs = getPurchaseBatchDetail();
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_DETAILINFO", purchaseDetailVOs);
			LOGGER.info("Module: Accounts : Method: showPurchaseDetailMoreOption Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseDetailMoreOption: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private List<PurchaseDetailVO> getPurchaseReceiveBatchDetail(
			List<PurchaseDetailVO> sessionPurchaseDetailVOs) throws Exception {
		JSONParser parser = new JSONParser();
		Object purchaseDetailObject = parser.parse(purchaseDetails);
		JSONArray object = (JSONArray) purchaseDetailObject;
		PurchaseDetailVO purchaseDetailVO = null;
		Product product = null;
		Shelf shelf = null;
		List<PurchaseDetailVO> purchaseDetailVOs = new ArrayList<PurchaseDetailVO>();
		Map<Long, PurchaseDetailVO> sessionPODetailMap = new HashMap<Long, PurchaseDetailVO>();
		for (PurchaseDetailVO sessionDtVO : sessionPurchaseDetailVOs)
			sessionPODetailMap.put(sessionDtVO.getPurchaseDetailId(),
					sessionDtVO);
		for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
			JSONObject jsonObject = (JSONObject) iterator.next();
			JSONArray detailArray = (JSONArray) jsonObject
					.get("purchsaeDetailJS");
			JSONObject jsonObjectDetail = (JSONObject) detailArray.get(0);

			purchaseDetailVO = new PurchaseDetailVO();
			product = purchaseBL
					.getProductBL()
					.getProductService()
					.getSimpleProductById(
							Long.parseLong(jsonObjectDetail.get("productId")
									.toString()));
			shelf = ((null != jsonObjectDetail.get("shelfId") && Integer
					.parseInt(jsonObjectDetail.get("shelfId").toString()) > 0) ? purchaseBL
					.getStockBL()
					.getStoreBL()
					.getStoreService()
					.getStoreByShelfId(
							Integer.parseInt(jsonObjectDetail.get("shelfId")
									.toString())) : null);
			purchaseDetailVO.setProduct(product);
			if (null != shelf) {
				purchaseDetailVO.setStoreId(shelf.getShelf().getAisle()
						.getStore().getStoreId());
				purchaseDetailVO.setShelfId(shelf.getShelfId());
				purchaseDetailVO.setStoreName(shelf.getShelf().getAisle()
						.getStore().getStoreName()
						+ ">>"
						+ shelf.getShelf().getAisle().getSectionName()
						+ ">>"
						+ shelf.getShelf().getName()
						+ ">>"
						+ shelf.getName());
			}
			purchaseDetailVO.setUnitCode(product.getLookupDetailByProductUnit()
					.getAccessCode());
			purchaseDetailVO.setPurchaseQty(Double.parseDouble(jsonObjectDetail
					.get("poQty").toString()));
			purchaseDetailVO.setReceivedQty(Double.parseDouble(jsonObjectDetail
					.get("receivedQty").toString()));
			purchaseDetailVO.setReturnedQty(Double.parseDouble(jsonObjectDetail
					.get("returnedQty").toString()));
			purchaseDetailVO.setQuantity(Double.parseDouble(jsonObjectDetail
					.get("receiveQty").toString()));
			purchaseDetailVO.setUnitRate(Double.parseDouble(jsonObjectDetail
					.get("unitRate").toString()));
			purchaseDetailVO.setReturnQty(Double.parseDouble(jsonObjectDetail
					.get("returnQty").toString()));
			purchaseDetailVO.setDescription(null != jsonObjectDetail
					.get("linesDescription") ? jsonObjectDetail.get(
					"linesDescription").toString() : null);
			purchaseDetailVO.setBatchNumber(null != jsonObjectDetail
					.get("batchNumber") ? jsonObjectDetail.get("batchNumber")
					.toString() : null);
			purchaseDetailVO.setExpiryDate(null != jsonObjectDetail
					.get("productExpiry") ? jsonObjectDetail.get(
					"productExpiry").toString() : null);
			purchaseDetailVO.setReceiveDetailId((null != jsonObjectDetail
					.get("receiveDetailId") && Long.parseLong(jsonObjectDetail
					.get("receiveDetailId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("receiveDetailId")
							.toString()) : null);
			purchaseDetailVO.setPurchaseDetailId((null != jsonObjectDetail
					.get("purchaseDetailId") && Long.parseLong(jsonObjectDetail
					.get("purchaseDetailId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("purchaseDetailId")
							.toString()) : null);
			purchaseDetailVO.setPurchaseId((null != jsonObjectDetail
					.get("purchaseId") && Long.parseLong(jsonObjectDetail.get(
					"purchaseId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("purchaseId").toString())
					: null);
			if (sessionPODetailMap.containsKey(purchaseDetailVO
					.getPurchaseDetailId())) {
				purchaseDetailVO.setProductPackageDetail(sessionPODetailMap
						.get(purchaseDetailVO.getPurchaseDetailId())
						.getProductPackageDetail());
				purchaseDetailVO.setProductPackageVOs(sessionPODetailMap.get(
						purchaseDetailVO.getPurchaseDetailId())
						.getProductPackageVOs());
				if (null != sessionPODetailMap.get(
						purchaseDetailVO.getPurchaseDetailId())
						.getPackageDetailId()
						&& sessionPODetailMap.get(
								purchaseDetailVO.getPurchaseDetailId())
								.getPackageDetailId() > 0) {
					purchaseDetailVO.setPackageDetailId(sessionPODetailMap.get(
							purchaseDetailVO.getPurchaseDetailId())
							.getPackageDetailId());
					purchaseDetailVO.setPackageUnit(purchaseDetailVO
							.getQuantity());
					purchaseDetailVO.setBaseQuantity(AIOSCommons
							.convertExponential(purchaseBL
									.getPackageConversionBL()
									.getPackageBaseQuantity(
											purchaseDetailVO
													.getPackageDetailId(),
											purchaseDetailVO.getQuantity())));
					purchaseDetailVO.setBaseUnitName(sessionPODetailMap.get(
							purchaseDetailVO.getPurchaseDetailId())
							.getBaseUnitName());
				} else {
					purchaseDetailVO.setPackageUnit(purchaseDetailVO
							.getQuantity());
					purchaseDetailVO.setPackageDetailId(-1l);
				}
			}
			purchaseDetailVOs.add(purchaseDetailVO);
		}
		return purchaseDetailVOs;
	}

	public String showReceiveDetailMoreOption() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showReceiveDetailMoreOption");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseMap = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseMap.get(purchaseId);

			if (null != purchaseVO.getPurchaseDetailBatchVO()
					&& purchaseVO.getPurchaseDetailBatchVO().size() > 0) {
				ServletActionContext.getRequest().setAttribute(
						"RECEIVE_DETAILINFO",
						purchaseVO.getPurchaseDetailBatchVO());
			} else {
				List<PurchaseDetailVO> purchaseDetailVOs = getPurchaseReceiveBatchDetail(purchaseVO
						.getPurchaseDetailVO());
				purchaseVO.setPurchaseDetailVO(purchaseDetailVOs);
				ServletActionContext.getRequest().setAttribute(
						"RECEIVE_DETAILINFO", purchaseDetailVOs);
			}
			purchaseVO.setContinuousPurchase(continuousPurchase);
			purchaseMap.put(purchaseVO.getPurchaseId(), purchaseVO);
			session.setAttribute(
					"PURCHASE_SESSION_INFO_" + sessionObj.get("jSessionId"),
					purchaseMap);
			LOGGER.info("Module: Accounts : Method: showReceiveDetailMoreOption Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showReceiveDetailMoreOption: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String validateBatchPendingReceiveDetail() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: validateBatchPendingReceiveDetail");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseMap = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			JSONParser parser = new JSONParser();
			Object purchaseDetailObject = parser.parse(purchaseDetails);
			JSONArray object = (JSONArray) purchaseDetailObject;

			double receivedQty = 0;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray detailArray = (JSONArray) jsonObject
						.get("purchsaeDetailJS");
				JSONObject jsonObjectDetail = (JSONObject) detailArray.get(0);

				if ((long) Long.parseLong(jsonObjectDetail.get(
						"purchaseDetailId").toString()) == purchaseDetailId) {
					receivedQty += Double.parseDouble(jsonObjectDetail.get(
							"receiveQty").toString());
				}
			}
			double poReceiveQty = 0;
			PurchaseVO purchaseVO = (PurchaseVO) purchaseMap.get(purchaseId);
			for (PurchaseDetailVO detailVO : purchaseVO.getPurchaseDetailVO()) {
				if ((long) detailVO.getPurchaseDetailId() == purchaseDetailId) {
					poReceiveQty = detailVO.getQuantity();
					break;
				}
			}
			if (receivedQty <= poReceiveQty)
				returnMessage = "SUCCESS";
			else
				returnMessage = "ReceiveQty should be lesser than PO Qty";
			LOGGER.info("Module: Accounts : Method: validateBatchPendingReceiveDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Receive Qty should be lt or eq PO qty";
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: validateBatchPendingReceiveDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showBatchPendingReceiveDetail() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showBatchPendingReceiveDetail");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseMap = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));

			JSONParser parser = new JSONParser();
			Object purchaseDetailObject = parser.parse(purchaseDetails);
			JSONArray detailArray = (JSONArray) purchaseDetailObject;
			JSONObject jsonObjectDetail = (JSONObject) detailArray.get(0);

			PurchaseDetailVO purchaseDetailVO = null;
			Product product = null;
			Shelf shelf = null;

			purchaseDetailVO = new PurchaseDetailVO();
			product = purchaseBL
					.getProductBL()
					.getProductService()
					.getBasicProductById(
							Long.parseLong(jsonObjectDetail.get("productId")
									.toString()));
			shelf = ((null != jsonObjectDetail.get("shelfId") && Integer
					.parseInt(jsonObjectDetail.get("shelfId").toString()) > 0) ? purchaseBL
					.getStockBL()
					.getStoreBL()
					.getStoreService()
					.getStoreByShelfId(
							Integer.parseInt(jsonObjectDetail.get("shelfId")
									.toString())) : null);
			purchaseDetailVO.setProduct(product);
			if (null != shelf) {
				purchaseDetailVO.setStoreId(shelf.getShelf().getAisle()
						.getStore().getStoreId());
				purchaseDetailVO.setShelfId(shelf.getShelfId());
				purchaseDetailVO.setStoreName(shelf.getShelf().getAisle()
						.getStore().getStoreName()
						+ ">>"
						+ shelf.getShelf().getAisle().getSectionName()
						+ ">>"
						+ shelf.getShelf().getName()
						+ ">>"
						+ shelf.getName());
			}
			purchaseDetailVO.setPurchaseQty(Double.parseDouble(jsonObjectDetail
					.get("poQty").toString()));
			purchaseDetailVO.setReceivedQty(Double.parseDouble(jsonObjectDetail
					.get("receivedQty").toString()));
			purchaseDetailVO.setReturnedQty(Double.parseDouble(jsonObjectDetail
					.get("returnedQty").toString()));
			purchaseDetailVO.setQuantity(Double.parseDouble(jsonObjectDetail
					.get("receiveHiddenQty").toString())
					- Double.parseDouble(jsonObjectDetail.get("receiveQty")
							.toString()));
			purchaseDetailVO.setUnitRate(Double.parseDouble(jsonObjectDetail
					.get("unitRate").toString()));
			purchaseDetailVO.setDescription(null != jsonObjectDetail
					.get("linesDescription") ? jsonObjectDetail.get(
					"linesDescription").toString() : null);
			purchaseDetailVO.setBatchNumber(null != jsonObjectDetail
					.get("batchNumber") ? jsonObjectDetail.get("batchNumber")
					.toString() : null);
			purchaseDetailVO.setExpiryDate(null != jsonObjectDetail
					.get("productExpiry") ? jsonObjectDetail.get(
					"productExpiry").toString() : null);
			purchaseDetailVO.setReceiveDetailId((null != jsonObjectDetail
					.get("receiveDetailId") && Long.parseLong(jsonObjectDetail
					.get("receiveDetailId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("receiveDetailId")
							.toString()) : null);
			purchaseDetailVO.setPurchaseDetailId((null != jsonObjectDetail
					.get("purchaseDetailId") && Long.parseLong(jsonObjectDetail
					.get("purchaseDetailId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("purchaseDetailId")
							.toString()) : null);

			purchaseDetailVO.setPurchaseId((null != jsonObjectDetail
					.get("purchaseId") && Long.parseLong(jsonObjectDetail.get(
					"purchaseId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("purchaseId").toString())
					: null);
			long packageDetailId = ((null != jsonObjectDetail
					.get("packageDetailId") && Long.parseLong(jsonObjectDetail
					.get("packageDetailId").toString()) > 0) ? Long
					.parseLong(jsonObjectDetail.get("packageDetailId")
							.toString()) : -1);
			PurchaseVO purchaseVO = (PurchaseVO) purchaseMap
					.get(purchaseDetailVO.getPurchaseId());
			for (PurchaseDetailVO purchaseDetail : purchaseVO
					.getPurchaseDetailVO()) {
				if ((long) purchaseDetail.getPurchaseDetailId() == (long) purchaseDetailVO
						.getPurchaseDetailId()) {
					purchaseDetailVO.setPackageDetailId(packageDetailId);
					purchaseDetailVO.setProductPackageDetail(purchaseDetail
							.getProductPackageDetail());
					purchaseDetailVO.setProductPackageVOs(purchaseDetail
							.getProductPackageVOs());
					purchaseDetailVO.setPackageUnit(purchaseDetailVO
							.getQuantity());
					if (packageDetailId > 0)
						purchaseDetailVO
								.setBaseQuantity(AIOSCommons
										.convertExponential(purchaseBL
												.getPackageConversionBL()
												.getPackageBaseQuantity(
														packageDetailId,
														purchaseDetailVO
																.getQuantity())));
					purchaseDetailVO.setBaseUnitName(purchaseDetail
							.getBaseUnitName());
				}
			}
			rowId = jsonObjectDetail.get("lastRowId").toString();
			ServletActionContext.getRequest().setAttribute(
					"RECEIVE_DETAILPENDING", purchaseDetailVO);
			LOGGER.info("Module: Accounts : Method: showBatchPendingReceiveDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBatchPendingReceiveDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}

	public long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public long getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(long quotationId) {
		this.quotationId = quotationId;
	}

	public char getItemNature() {
		return itemNature;
	}

	public void setItemNature(char itemNature) {
		this.itemNature = itemNature;
	}

	public String getPurchaseDetails() {
		return purchaseDetails;
	}

	public void setPurchaseDetails(String purchaseDetails) {
		this.purchaseDetails = purchaseDetails;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public long getSiteId() {
		return siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(String pageInfo) {
		this.pageInfo = pageInfo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public List<SupplierTO> getSuppliersList() {
		return suppliersList;
	}

	public void setSuppliersList(List<SupplierTO> suppliersList) {
		this.suppliersList = suppliersList;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getReceived() {
		return received;
	}

	public void setReceived(String received) {
		this.received = received;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public List<PurchaseOrderTO> getPurchaseOrderDS() {
		return purchaseOrderDS;
	}

	public void setPurchaseOrderDS(List<PurchaseOrderTO> purchaseOrderDS) {
		this.purchaseOrderDS = purchaseOrderDS;
	}

	public long getPurchaseManagerId() {
		return purchaseManagerId;
	}

	public void setPurchaseManagerId(long purchaseManagerId) {
		this.purchaseManagerId = purchaseManagerId;
	}

	public long getShippingTermId() {
		return shippingTermId;
	}

	public void setShippingTermId(long shippingTermId) {
		this.shippingTermId = shippingTermId;
	}

	public byte getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(byte paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public StockVO getStockVO() {
		return stockVO;
	}

	public void setStockVO(StockVO stockVO) {
		this.stockVO = stockVO;
	}

	public List<StockVO> getBarcodeDetails() {
		return barcodeDetails;
	}

	public void setBarcodeDetails(List<StockVO> barcodeDetails) {
		this.barcodeDetails = barcodeDetails;
	}

	public List<StockVO> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockVO> stocks) {
		this.stocks = stocks;
	}

	public boolean isContinuousPurchase() {
		return continuousPurchase;
	}

	public void setContinuousPurchase(boolean continuousPurchase) {
		this.continuousPurchase = continuousPurchase;
	}

	public double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(double unitRate) {
		this.unitRate = unitRate;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public long getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(long requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getDuplicateProductIds() {
		return duplicateProductIds;
	}

	public void setDuplicateProductIds(String duplicateProductIds) {
		this.duplicateProductIds = duplicateProductIds;
	}

	public long getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(long purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId;
	}

	public double getBatchReceiveQuantity() {
		return batchReceiveQuantity;
	}

	public void setBatchReceiveQuantity(double batchReceiveQuantity) {
		this.batchReceiveQuantity = batchReceiveQuantity;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
}
