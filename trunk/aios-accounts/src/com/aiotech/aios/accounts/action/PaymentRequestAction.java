/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PaymentRequestDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PaymentRequestDetailVO;
import com.aiotech.aios.accounts.service.bl.PaymentRequestBL;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentRequestMode;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentRequestStatus;
import com.aiotech.aios.common.to.Constants.Accounts.RequestDetailStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PaymentRequestAction extends ActionSupport {

	private static final long serialVersionUID = -4730902555250768662L;
	private long paymentRequestId;
	private String paymentRequestNo;
	private String requestDate;
	private String description;
	private Byte status;
	private String requestDetail;
	private Integer processFlag;
	private Integer rowId;
	private String returnMessage;
	private Long recordId;
	private Long alertId;
	private Long messageId;
	private long personId;
	private String personName;
	private byte paymentModeId;
	private double totalAmount;
	private List<Object> aaData;
	private PaymentRequestBL paymentRequestBL;
	private String printableContent;

	private static final Logger LOGGER = LogManager
			.getLogger(PaymentRequestAction.class);

	public String returnSuccess() {
		LOGGER.info("Inside Method returnSuccess");
		return SUCCESS;
	}

	// To List all Payment Request
	public String showJsonPaymentRequest() {
		LOGGER.info("Inside Module: Accounts : Method: showJsonPaymentRequest");
		try {
			JSONObject jsonList = paymentRequestBL.getAllPaymentRequest();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showJsonPaymentRequest: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJsonPaymentRequest Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showPaymentRequestPrint() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPaymentRequestPrint");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean pettyCashTemplatePrintEnabled = session
					.getAttribute("print_payreq_template") != null ? ((String) session
					.getAttribute("print_payreq_template"))
					.equalsIgnoreCase("true") : false;
			PaymentRequest paymentRequest = paymentRequestBL
					.getPaymentRequestService().getPaymentRequestById(
							paymentRequestId);
			List<PaymentRequestDetailVO> paymentRequestDetailVOs = new ArrayList<PaymentRequestDetailVO>();
			PaymentRequestDetailVO paymentRequestDetailVO = null;
			double totalamount = 0;
			for (PaymentRequestDetail requestDetail : paymentRequest
					.getPaymentRequestDetails()) {
				paymentRequestDetailVO = new PaymentRequestDetailVO();
				BeanUtils.copyProperties(paymentRequestDetailVO, requestDetail);
				paymentRequestDetailVO.setAmountStr(AIOSCommons
						.formatAmount(requestDetail.getAmount()));
				paymentRequestDetailVO.setPaymentModeStr(ModeOfPayment.get(
						requestDetail.getPaymentMode()).name());
				paymentRequestDetailVO.setCategoryName(requestDetail
						.getLookupDetail().getDisplayName());
				paymentRequestDetailVOs.add(paymentRequestDetailVO);
				totalamount += requestDetail.getAmount();
			}
			Collections.sort(paymentRequestDetailVOs,
					new Comparator<PaymentRequestDetailVO>() {
						public int compare(PaymentRequestDetailVO o1,
								PaymentRequestDetailVO o2) {
							return o1.getPaymentRequestDetailId().compareTo(
									o2.getPaymentRequestDetailId());
						}
					});
			if (pettyCashTemplatePrintEnabled) {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/templates/implementation_"
						+ paymentRequestBL.getImplementation()
								.getImplementationId() + "/paymentreq"));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-paymentreq-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();
				rootMap.put("datetime", DateFormat
						.convertDateToString(paymentRequest.getRequestDate()
								.toString()));
				personName = "";
				if (null != paymentRequest.getPersonByPersonId())
					personName = paymentRequest.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ paymentRequest.getPersonByPersonId()
									.getLastName();
				else
					personName = paymentRequest.getPersonName();
				rootMap.put("requestBy", personName);
				rootMap.put("referenceNumber", paymentRequest.getPaymentReqNo());
				String amountInWords = AIOSCommons
						.convertAmountToWords(totalamount);
				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(totalamount));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("amountInWords", amountInWords);
				rootMap.put("totalAmount",
						AIOSCommons.formatAmount(totalamount));
				rootMap.put("paymentDetails", paymentRequestDetailVOs);
				rootMap.put("urlPath", urlPath);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
			}
			LOGGER.info("Module: Accounts : Method: showPaymentRequestPrint: Action Success");
			return "template";
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPaymentRequestPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Payment request entry ADD/EDIT
	public String showPaymentRequestEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showPaymentRequestEntry");
		try {
			Map<Byte, PaymentRequestStatus> requestStatus = new HashMap<Byte, PaymentRequestStatus>();
			for (PaymentRequestStatus e : EnumSet
					.allOf(PaymentRequestStatus.class)) {
				requestStatus.put(e.getCode(), e);
			}
			List<Currency> currencyList = paymentRequestBL.getCurrencyService()
					.getAllCurrency(paymentRequestBL.getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_REQUEST_STATUS", requestStatus);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					paymentRequestBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute(
					"REQUEST_CATEGORY",
					paymentRequestBL.getLookupMasterBL()
							.getActiveLookupDetails("REQUEST_CATEGORY", true));
			PaymentRequest paymentRequest = null;
			personId = 0;
			personName = null;
			paymentRequestId = null != recordId && recordId > 0 ? recordId
					: paymentRequestId;
			totalAmount = 0;
			if (paymentRequestId > 0) {
				paymentRequest = paymentRequestBL.getPaymentRequestService()
						.getPaymentRequestById(paymentRequestId);
				List<PaymentRequestDetailVO> paymentRequestDetailVOs = new ArrayList<PaymentRequestDetailVO>();
				PaymentRequestDetailVO paymentRequestDetailVO = null;
				for (PaymentRequestDetail paymentRequestDetail : paymentRequest
						.getPaymentRequestDetails()) {
					paymentRequestDetailVO = new PaymentRequestDetailVO();
					BeanUtils.copyProperties(paymentRequestDetailVO,
							paymentRequestDetail);
					totalAmount += paymentRequestDetail.getAmount();
					paymentRequestDetailVOs.add(paymentRequestDetailVO);
				}
				Collections.sort(paymentRequestDetailVOs,
						new Comparator<PaymentRequestDetailVO>() {
							public int compare(PaymentRequestDetailVO o1,
									PaymentRequestDetailVO o2) {
								return o1.getPaymentRequestDetailId()
										.compareTo(
												o2.getPaymentRequestDetailId());
							}
						});
				ServletActionContext.getRequest().setAttribute(
						"PAYMENT_REQUEST", paymentRequest);
				ServletActionContext.getRequest().setAttribute(
						"PAYMENT_REQUEST_DETAIL", paymentRequestDetailVOs);
			} else
				paymentRequestNo = SystemBL.getReferenceStamp(
						PaymentRequest.class.getName(),
						paymentRequestBL.getImplementation());

			for (Currency currency : currencyList) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency);
					break;
				}
			}
			CommentVO comment = new CommentVO();
			if (paymentRequest != null
					&& paymentRequest.getPaymentRequestId() != 0
					&& recordId > 0) {
				comment.setRecordId(paymentRequest.getPaymentRequestId());
				comment.setUseCase(PaymentRequest.class.getName());
				comment = paymentRequestBL.getCommentBL().getCommentInfo(
						comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			LOGGER.info("Module: Accounts : Method: showPaymentRequestEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPaymentRequestEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Payment request Detail Add Row
	public String showPayementRequestAddRow() {
		LOGGER.info("Inside Module: Accounts : Method: showPayementRequestAddRow");
		try {
			List<Currency> currencyList = paymentRequestBL.getCurrencyService()
					.getAllCurrency(paymentRequestBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					paymentRequestBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			ServletActionContext.getRequest().setAttribute(
					"REQUEST_CATEGORY",
					paymentRequestBL.getLookupMasterBL()
							.getActiveLookupDetails("REQUEST_CATEGORY", true));
			LOGGER.info("Module: Accounts : Method: showPayementRequestAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPayementRequestAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// save or update payment request
	public String savePaymentRequest() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: savePaymentRequest");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			PaymentRequest paymentRequest = new PaymentRequest();
			paymentRequest.setRequestDate(DateFormat
					.convertStringToDate(requestDate));
			paymentRequest.setStatus(status);
			paymentRequest.setDescription(description);
			paymentRequest.setImplementation(paymentRequestBL
					.getImplementation());
			if (personId > 0) {
				Person requestPerson = new Person();
				requestPerson.setPersonId(personId);
				paymentRequest.setPersonByPersonId(requestPerson);
			} else
				paymentRequest.setPersonName(personName);
			List<PaymentRequestDetail> requestDetailList = new ArrayList<PaymentRequestDetail>();
			List<PaymentRequestDetail> deleteRequestDetailList = new ArrayList<PaymentRequestDetail>();
			PaymentRequestDetail requestDetail = null;
			LookupDetail lookupDetail = null;
			String[] lineDetailArray = splitValues(this.requestDetail, "#@");
			for (String string : lineDetailArray) {
				String[] detailString = splitValues(string, "__");
				requestDetail = new PaymentRequestDetail();
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(Long.parseLong(detailString[0]));
				requestDetail.setLookupDetail(lookupDetail);
				requestDetail.setPaymentMode(Byte.parseByte(detailString[1]));
				requestDetail.setAmount(Double.parseDouble(detailString[2]));
				if (null != detailString[3]
						&& !detailString[3].equalsIgnoreCase("##"))
					requestDetail.setDescription(detailString[3]);
				if (Long.parseLong(detailString[4]) > 0)
					requestDetail.setPaymentRequestDetailId(Long
							.parseLong(detailString[4]));
				requestDetailList.add(requestDetail);
			}
			if (paymentRequestId > 0) {
				PaymentRequest paymentRequestEdit = paymentRequestBL
						.getPaymentRequestService().getPaymentRequestById(
								paymentRequestId);
				paymentRequest.setPersonByCreatedBy(paymentRequestEdit
						.getPersonByCreatedBy());
				paymentRequest.setCreatedDate(paymentRequestEdit
						.getCreatedDate());
				paymentRequest.setPaymentRequestId(paymentRequestId);
				paymentRequest.setPaymentReqNo(paymentRequestNo);
				try {
					List<PaymentRequestDetail> requestDList = this
							.getPaymentRequestBL().getPaymentRequestService()
							.getRequestDetailById(paymentRequestId);
					Collection<Long> listOne = new ArrayList<Long>();
					Collection<Long> listTwo = new ArrayList<Long>();
					Map<Long, PaymentRequestDetail> hashRequestDetail = new HashMap<Long, PaymentRequestDetail>();
					if (null != requestDList && requestDList.size() > 0) {
						for (PaymentRequestDetail list : requestDList) {
							listOne.add(list.getPaymentRequestDetailId());
							hashRequestDetail.put(
									list.getPaymentRequestDetailId(), list);
						}
						if (null != hashRequestDetail
								&& hashRequestDetail.size() > 0) {
							for (PaymentRequestDetail list : requestDetailList) {
								listTwo.add(list.getPaymentRequestDetailId());
							}
						}
					}
					Collection<Long> similar = new HashSet<Long>(listOne);
					Collection<Long> different = new HashSet<Long>();
					different.addAll(listOne);
					different.addAll(listTwo);
					similar.retainAll(listTwo);
					different.removeAll(similar);
					if (null != different && different.size() > 0) {
						for (Long list : different) {
							if (null != list && !list.equals(0)) {
								deleteRequestDetailList.add(hashRequestDetail
										.get(list));
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					returnMessage = "Failure";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					LOGGER.info("Module: Accounts : Method: savePaymentRequest: throws Exception "
							+ ex);
					return ERROR;
				}
			} else {
				paymentRequest.setPaymentReqNo(SystemBL.getReferenceStamp(
						PaymentRequest.class.getName(),
						paymentRequestBL.getImplementation()));
				paymentRequest.setCreatedDate(new Date());
				User user = (User) sessionObj.get("USER");
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				paymentRequest.setPersonByCreatedBy(person);
			}
			paymentRequestBL.savePaymentRequest(paymentRequest,
					requestDetailList, deleteRequestDetailList, alertId);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: savePaymentRequest: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: savePaymentRequest: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete payment request
	public String deletePaymentRequest() {
		LOGGER.info("Inside Module: Accounts : Method: deletePaymentRequest");
		try {
			PaymentRequest paymentRequest = paymentRequestBL
					.getPaymentRequestService().getPaymentRequestById(
							paymentRequestId);
			paymentRequestBL.deletePaymentRequest(paymentRequest, messageId);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deletePaymentRequest: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error ";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deletePaymentRequest: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get View Payment Request for approval process
	public String showPaymentRequestApproval() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPaymentRequestApproval");
			PaymentRequest paymentRequest = paymentRequestBL
					.getPaymentRequestService().getPaymentRequestById(recordId);
			List<PaymentRequestDetailVO> paymentRequestDetailVOs = new ArrayList<PaymentRequestDetailVO>();
			PaymentRequestDetailVO paymentRequestDetailVO = null;
			for (PaymentRequestDetail paymentRequestDetail : paymentRequest
					.getPaymentRequestDetails()) {
				paymentRequestDetailVO = new PaymentRequestDetailVO();
				BeanUtils.copyProperties(paymentRequestDetailVO,
						paymentRequestDetail);
				paymentRequestDetailVO.setPaymentModeStr(ModeOfPayment
						.get(paymentRequestDetail.getPaymentMode()).toString()
						.replaceAll("_", " "));
				paymentRequestDetailVOs.add(paymentRequestDetailVO);
			}
			Collections.sort(paymentRequestDetailVOs,
					new Comparator<PaymentRequestDetailVO>() {
						public int compare(PaymentRequestDetailVO o1,
								PaymentRequestDetailVO o2) {
							return o1.getPaymentRequestDetailId().compareTo(
									o2.getPaymentRequestDetailId());
						}
					});
			ServletActionContext.getRequest().setAttribute("PAYMENT_REQUEST",
					paymentRequest);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_REQUEST_DETAIL", paymentRequestDetailVOs);
			Map<Byte, PaymentRequestStatus> requestStatus = new HashMap<Byte, PaymentRequestStatus>();
			for (PaymentRequestStatus e : EnumSet
					.allOf(PaymentRequestStatus.class)) {
				requestStatus.put(e.getCode(), e);
			}
			Map<Byte, String> paymentMode = new HashMap<Byte, String>();
			for (PaymentRequestMode e : EnumSet.allOf(PaymentRequestMode.class)) {
				paymentMode.put(e.getCode(), e.toString()
						.replaceAll("\\_", " "));
			}

			Map<Byte, RequestDetailStatus> requestDetailStatus = new HashMap<Byte, RequestDetailStatus>();
			for (RequestDetailStatus e : EnumSet
					.allOf(RequestDetailStatus.class)) {
				requestDetailStatus.put(e.getCode(), e);
			}
			List<Currency> currencyList = paymentRequestBL.getCurrencyService()
					.getAllCurrency(paymentRequestBL.getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_REQUEST_STATUS", requestStatus);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					paymentMode);
			ServletActionContext.getRequest().setAttribute(
					"REQUEST_DETAIL_STATUS", requestDetailStatus);
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			LOGGER.info("Module: Accounts : Method: showPaymentRequestApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentRequestApproval: Action Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show all approved payment request for direct payment entry
	public String showApprovedPaymentRequest() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showApprovedPaymentRequest");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("PAYMENT_REQUEST_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<PaymentRequest> paymentRequests = paymentRequestBL
					.getPaymentRequestService()
					.findAllApprovedPaymentRequest(
							paymentRequestBL.getImplementation(),
							paymentModeId,
							(byte) WorkflowConstants.Status.Published.getCode());
			Map<Long, Set<PaymentRequestDetail>> requestMap = new HashMap<Long, Set<PaymentRequestDetail>>();
			for (PaymentRequest list : paymentRequests) {
				requestMap.put(list.getPaymentRequestId(),
						list.getPaymentRequestDetails());
			}
			aaData = paymentRequestBL
					.convertPaymentRequestObject(paymentRequests);
			ServletActionContext.getRequest().setAttribute("PAYMENT_REQUESTS",
					paymentRequests);
			session.setAttribute(
					"PAYMENT_REQUEST_SESSION_INFO_"
							+ sessionObj.get("jSessionId"), requestMap);
			LOGGER.info("Module: Accounts : Method: showApprovedPaymentRequest: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showApprovedPaymentRequest: Action Exception "
					+ ex);
			return ERROR;
		}
	}

	// show payment request detail info in direct payment detail
	@SuppressWarnings("unchecked")
	public String showPaymentRequestDetail() {
		LOGGER.info("Inside Module: Accounts : Method: showPaymentRequestDetail");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, Set<PaymentRequestDetail>> requestDetailMap = (Map<Long, Set<PaymentRequestDetail>>) session
					.getAttribute("PAYMENT_REQUEST_SESSION_INFO_"
							+ sessionObj.get("jSessionId"));
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_REQUEST_DETAILS",
					requestDetailMap.get(paymentRequestId));
			session.removeAttribute("PAYMENT_REQUEST_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: showPaymentRequestDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPaymentRequestDetail: Action Exception "
					+ ex);
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public PaymentRequestBL getPaymentRequestBL() {
		return paymentRequestBL;
	}

	public void setPaymentRequestBL(PaymentRequestBL paymentRequestBL) {
		this.paymentRequestBL = paymentRequestBL;
	}

	public long getPaymentRequestId() {
		return paymentRequestId;
	}

	public void setPaymentRequestId(long paymentRequestId) {
		this.paymentRequestId = paymentRequestId;
	}

	public String getPaymentRequestNo() {
		return paymentRequestNo;
	}

	public void setPaymentRequestNo(String paymentRequestNo) {
		this.paymentRequestNo = paymentRequestNo;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getRequestDetail() {
		return requestDetail;
	}

	public void setRequestDetail(String requestDetail) {
		this.requestDetail = requestDetail;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Integer getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Integer processFlag) {
		this.processFlag = processFlag;
	}

	public byte getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(byte paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
}
