package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Promotion;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.PromotionOption;
import com.aiotech.aios.accounts.service.bl.PromotionBL;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountOptions;
import com.aiotech.aios.common.to.Constants.Accounts.PACType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class PromotionAction extends ActionSupport {

	private static final long serialVersionUID = 5804904340000914835L;

	private static final Logger log = Logger.getLogger(PromotionAction.class);

	// Dependency
	private PromotionBL promotionBL;

	// Variables
	private long promotionId;
	private Long recordId;
	private int rowId;
	private int status;
	private char minimumSales;
	private long discountOnPrice;
	private byte rewardType;
	private byte promotionOption;
	private double salesValue;
	private String fromDate;
	private String toDate;
	private String description;
	private String promotionMethods;
	private String returnMessage;
	private String promotionName;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		log.info("Inside Module: Accounts : Method: returnSuccess");
		recordId = null;
		return SUCCESS;
	}

	// Show all Product points
	public String showAllPromotions() {
		try {
			log.info("Inside Module: Accounts : Method: showAllPromotions");
			aaData = promotionBL.showAllPromotions();
			log.info("Module: Accounts : Method: showAllPromotions: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllPromotions Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	
	public String showPromotionApproval(){
		try {
			LOG.info("Inside Module: Accounts : Method: showPromotionApproval");
			promotionId = recordId;
			showPromotionEntry();
			LOG.info("Module: Accounts : Method: showPromotionApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showPromotionApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showPromotionRejectEntry(){
		try {
			LOG.info("Inside Module: Accounts : Method: showPromotionRejectEntry");
			promotionId = recordId;
			showPromotionEntry();
			LOG.info("Module: Accounts : Method: showPromotionRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showPromotionRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	// Show Promotion Entry Screen
	public String showPromotionEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showPromotionEntry"); 
			if (promotionId > 0) {
				Promotion promotion = promotionBL.getPromotionService()
						.getPromotionbyId(promotionId);
				ServletActionContext.getRequest().setAttribute(
						"PROMOTION_INFO", promotion);
				CommentVO comment = new CommentVO();
				if (recordId != null && promotion != null
						&& promotion.getPromotionId() != 0 && recordId > 0) {
					comment.setRecordId(promotion.getPromotionId());
					comment.setUseCase(Promotion.class.getName());
					comment = promotionBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			Map<Byte, String> promotionRewardTypes = new HashMap<Byte, String>();
			for (PACType e : EnumSet.allOf(PACType.class))
				promotionRewardTypes.put(e.getCode(),
						e.name().replaceAll("_", " "));
			Map<Byte, String> discountOptions = new HashMap<Byte, String>();
			for (DiscountOptions e : EnumSet.allOf(DiscountOptions.class))
				discountOptions.put(e.getCode(), e.name().replaceAll("_", " "));

			ServletActionContext.getRequest().setAttribute(
					"PROMOTION_REWARD_TYPES", promotionRewardTypes);
			ServletActionContext.getRequest().setAttribute("PROMOTION_OPTIONS",
					discountOptions);
			ServletActionContext.getRequest().setAttribute(
					"DISCOUNT_ON_PRICE",
					promotionBL.getLookupMasterBL().getActiveLookupDetails(
							"PRODUCT_PRICING_LEVEL", true));
			log.info("Module: Accounts : Method: showPromotionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showPromotionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Add Row
	public String showProductAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showProductAddRow");
			Map<Byte, PACType> packTypes = new HashMap<Byte, PACType>();
			for (PACType e : EnumSet.allOf(PACType.class))
				packTypes.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PACK_TYPES",
					packTypes);
			log.info("Module: Accounts : Method: showProductAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Promotion
	public String savePromotion() {
		try {
			log.info("Inside Module: Accounts : Method: savePromotion");
			Promotion promotion = new Promotion();
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(discountOnPrice);
			promotion.setLookupDetail(lookupDetail);
			promotion.setPromotionName(promotionName);
			promotion.setRewardType(rewardType);
			promotion.setStatus(status > 0 ? true : false);
			promotion.setFromDate(DateFormat.convertStringToDate(fromDate));
			promotion.setToDate(DateFormat.convertStringToDate(toDate));
			promotion.setPromotionOption(promotionOption);
			promotion.setMiniumSales(minimumSales == '\u0000' ? minimumSales : null);
 			promotion.setSalesValue(salesValue);
			promotion.setDescription(description);
			List<PromotionOption> promotionOptions = new ArrayList<PromotionOption>();
			List<PromotionOption> deletedPromotionOptions = null;
			JSONObject jsonObject = new JSONObject();
			for (Object object : aaData)
				jsonObject.put("promotionNature", object);

			try {
				String promotionNature = jsonObject
						.getString("promotionNature");
				JSONObject productObject = new JSONObject(promotionNature);
				JSONArray recordArray = productObject.getJSONArray("record");
				PromotionOption promotionOption = null;
				Product product = null;
				Customer customer = null;
				for (int i = 0; i < recordArray.length(); i++) {
					JSONObject jsonobj = recordArray.getJSONObject(i);
					promotionOption = new PromotionOption();
					if (jsonobj.getString("entityName").equalsIgnoreCase(
							"product")) {
						product = new Product();
						product.setProductId(jsonobj.getLong("recordId"));
						promotionOption.setProduct(product);
					} else if (jsonobj.getString("entityName")
							.equalsIgnoreCase("customer")) {
						customer = new Customer();
						customer.setCustomerId(jsonobj.getLong("recordId"));
						promotionOption.setCustomer(customer);
					}
					promotionOptions.add(promotionOption);
				}
			} catch (Exception e) {
				returnMessage = "Promotion option failure.";
				e.printStackTrace();
				return ERROR;
			}

			List<PromotionMethod> promotionDetails = new ArrayList<PromotionMethod>();
			List<PromotionMethod> deletedPromotionDetails = null;

			String[] promotionMethodArray = splitValues(promotionMethods, "#@");
			for (String promotionDetail : promotionMethodArray) {
				promotionDetails.add(addPromotionDetail(promotionDetail));
			}

			if (promotionId > 0) {
				promotion.setPromotionId(promotionId);
				deletedPromotionDetails = userDeletedPromotionMethods(
						promotionDetails, promotionBL.getPromotionService()
								.getPromotionMethodByPromotionId(promotionId));
				deletedPromotionOptions = userDeletedPromotionOptions(
						promotionOptions, promotionBL.getPromotionService()
								.getPromotionOptionByPromotionId(promotionId));
			}
			promotionBL.savePromotion(promotion, promotionDetails,
					deletedPromotionDetails, promotionOptions,
					deletedPromotionOptions);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: savePromotion: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: savePromotion: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Promotion
	public String deletePromotion() {
		try {
			log.info("Inside Module: Accounts : Method: deletePromotion");
			Promotion promotion = promotionBL.getPromotionService()
					.getPromotionbyId(promotionId);
			promotionBL.deletePromotion(promotion);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deletePromotion: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deletePromotion: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get User Deleted Promotion Methods
	private List<PromotionMethod> userDeletedPromotionMethods(
			List<PromotionMethod> promotionDetails,
			List<PromotionMethod> existingPromotionMethods) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, PromotionMethod> hashPromotionDetails = new HashMap<Long, PromotionMethod>();
		if (null != existingPromotionMethods
				&& existingPromotionMethods.size() > 0) {
			for (PromotionMethod list : existingPromotionMethods) {
				listOne.add(list.getPromotionMethodId());
				hashPromotionDetails.put(list.getPromotionMethodId(), list);
			}
			for (PromotionMethod list : promotionDetails) {
				listTwo.add(list.getPromotionMethodId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<PromotionMethod> promotionDetailsFinal = new ArrayList<PromotionMethod>();
		if (null != different && different.size() > 0) {
			PromotionMethod tempPromotionMethod = null;

			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempPromotionMethod = new PromotionMethod();
					tempPromotionMethod = hashPromotionDetails.get(list);
					promotionDetailsFinal.add(tempPromotionMethod);
				}
			}
		}
		return promotionDetailsFinal;
	}

	// Get User Deleted Promotion Options
	private List<PromotionOption> userDeletedPromotionOptions(
			List<PromotionOption> promotionOptions,
			List<PromotionOption> existingPromotionOptions) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, PromotionOption> hashPromotionOptions = new HashMap<Long, PromotionOption>();
		if (null != existingPromotionOptions
				&& existingPromotionOptions.size() > 0) {
			for (PromotionOption list : existingPromotionOptions) {
				listOne.add(list.getPromotionOptionId());
				hashPromotionOptions.put(list.getPromotionOptionId(), list);
			}
			for (PromotionOption list : promotionOptions) {
				listTwo.add(list.getPromotionOptionId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<PromotionOption> promotionOptionsFinal = new ArrayList<PromotionOption>();
		if (null != different && different.size() > 0) {
			PromotionOption tempPromotionOption = null;

			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempPromotionOption = new PromotionOption();
					tempPromotionOption = hashPromotionOptions.get(list);
					promotionOptionsFinal.add(tempPromotionOption);
				}
			}
		}
		return promotionOptionsFinal;
	}

	// Add to Promotion Method
	private PromotionMethod addPromotionDetail(String promotionDetail) {
		PromotionMethod promotionMethod = new PromotionMethod();
		String detailArray[] = splitValues(promotionDetail, "__");
		promotionMethod.setPacType(null != detailArray[0]
				&& !("0").equals(detailArray[0].trim()) ? Byte
				.parseByte(detailArray[0]) : null);
		promotionMethod.setCalculationType(null != detailArray[1]
				&& !("##").equals(detailArray[1].trim()) ? detailArray[1]
				.charAt(0) : null);

		if ((byte) promotionMethod.getPacType() != (byte) PACType.Coupon
				.getCode()) {
			if ((byte) promotionMethod.getPacType() == (byte) PACType.Promotion_Points
					.getCode()) {
				promotionMethod.setProductPoint(null != detailArray[2]
						&& !("0").equals(detailArray[2].trim()) ? Double
						.parseDouble(detailArray[2]) : null);
			} else {
				promotionMethod.setPromotionAmount(null != detailArray[2]
						&& !("0").equals(detailArray[2].trim()) ? Double
						.parseDouble(detailArray[2]) : null);
			}
		}else{
			if (null != detailArray[3] && Integer.parseInt(detailArray[3]) > 0) {
				Coupon coupon = new Coupon();
				coupon.setCouponId(Long.parseLong(detailArray[3]));
				promotionMethod.setCoupon(coupon);
			}
		} 
		promotionMethod.setPromotionMethodId(null != detailArray[4]
				&& !("0").equals(detailArray[4].trim()) ? Long
				.parseLong(detailArray[4]) : null);
		return promotionMethod;
	}

	// Split Array Values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public PromotionBL getPromotionBL() {
		return promotionBL;
	}

	public void setPromotionBL(PromotionBL promotionBL) {
		this.promotionBL = promotionBL;
	}

	public long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(long promotionId) {
		this.promotionId = promotionId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public char getMinimumSales() {
		return minimumSales;
	}

	public void setMinimumSales(char minimumSales) {
		this.minimumSales = minimumSales;
	}

	public byte getRewardType() {
		return rewardType;
	}

	public void setRewardType(byte rewardType) {
		this.rewardType = rewardType;
	}

	public double getSalesValue() {
		return salesValue;
	}

	public void setSalesValue(double salesValue) {
		this.salesValue = salesValue;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPromotionMethods() {
		return promotionMethods;
	}

	public void setPromotionMethods(String promotionMethods) {
		this.promotionMethods = promotionMethods;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public byte getPromotionOption() {
		return promotionOption;
	}

	public void setPromotionOption(byte promotionOption) {
		this.promotionOption = promotionOption;
	}

	public long getDiscountOnPrice() {
		return discountOnPrice;
	}

	public void setDiscountOnPrice(long discountOnPrice) {
		this.discountOnPrice = discountOnPrice;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
