package com.aiotech.aios.accounts.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.domain.entity.vo.StoreVO;
import com.aiotech.aios.accounts.service.bl.ReceiveBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class ReceiveNotesAction extends ActionSupport {

	private static final long serialVersionUID = 1020513023570145975L;
	// Common variable declaration
	private long receiveId;
	private String receiveNumber;
	private String receiveDate;
	private long receiveDetailId;
	private double receiveQty;
	private long productId;
	private String productName;
	private Integer id;
	private String showPage;
	private String sqlReturnMessage;
	private String itemNature;
	private String receiveLines;
	private String receiveDetails;
	private String productDetails;
	private long purchaseId;
	private long storeId;
	private String purchaseReturns;
	private String description;
	private String referenceNo;
	private long supplierId;
	private Integer rowId;
	private String purchaseDetail;
	private long combinationId;
	private String supplierName;
	private String receiveDetail;
	private double invoiceAmount;
	private String receiveAmount;
	private boolean continuousPurchase;
	private long invoiceId;
	private Long recordId;
	private long currencyId;
	private String deliveryNo;
	private String printableContent;
	private String purchaseDetails;
	private boolean batchProduct;
	private String reference1;
	private String reference2;
	private String fromDate;
	private String toDate;
	// Common Object
	private List<Receive> receiveList;
	private List<ReceiveDetail> detailList;
	private ReceiveBL receiveBL;
	private List<Object> aaData;
	private StockVO stockVO;
	private List<StockVO> barcodeDetails = new ArrayList<StockVO>();
	private List<StockVO> stocks = new ArrayList<StockVO>();

	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(ReceiveNotesAction.class);

	/** To redirect the action success page **/
	public String returnSuccess() {
		try {
			ServletActionContext.getRequest().setAttribute("rowId", id);
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("PURCHASE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"))) {
				session.removeAttribute("STORE_INFO_"
						+ sessionObj.get("jSessionId"));
				session.removeAttribute("PURCHASE_SESSION_INFO_"
						+ sessionObj.get("jSessionId"));
			}
			receiveId = 0l;
			LOGGER.info("Module: Accounts : Method: returnSuccess");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: returnSuccess: throws Exception "
					+ ex);
			return ERROR;
		}

	}

	public String showClosedReceiveNotes() {
		return SUCCESS;
	}

	public String showClosedGRNList() {
		try {
			LOGGER.info("Module: Accounts : Method: showClosedGRNList");
			aaData = receiveBL.getClosedGRNList();
			LOGGER.info("Module: Accounts : Method: showClosedGRNList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showClosedGRNList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showReceiveNotesDetails() {
		try {
			LOGGER.info("Module: Accounts : Method: showReceiveNotesDetails");
			List<ReceiveDetail> receiveDetails = receiveBL.getReceiveService()
					.getClosedReceiveDetails(receiveId);
			List<ReceiveDetailVO> receiveDetailVOs = new ArrayList<ReceiveDetailVO>();
			ReceiveDetailVO receiveDetailVO = null;
			for (ReceiveDetail receiveDetail : receiveDetails) {
				double returnedQty = 0;
				if (null != receiveDetail.getGoodsReturnDetails()
						&& receiveDetail.getGoodsReturnDetails().size() > 0) {
					for (GoodsReturnDetail goodsReturnDetail : receiveDetail
							.getGoodsReturnDetails()) {
						returnedQty += goodsReturnDetail.getPackageUnit();
					}
				}
				if (receiveDetail.getPackageUnit() > returnedQty) {
					receiveDetailVO = new ReceiveDetailVO();
					BeanUtils.copyProperties(receiveDetailVO, receiveDetail);
					receiveDetailVO.setProductPackageVOs(receiveBL
							.getPurchaseBL()
							.getPackageConversionBL()
							.getProductPackagingDetail(
									receiveDetail.getProduct().getProductId()));
					if (null != receiveDetail.getProductPackageDetail()) {
						receiveDetailVO.setPackageDetailId(receiveDetail
								.getProductPackageDetail()
								.getProductPackageDetailId());
						boolean iflag = true;
						for (ProductPackageVO packagevo : receiveDetailVO
								.getProductPackageVOs()) {
							if (iflag) {
								for (ProductPackageDetailVO packageDtvo : packagevo
										.getProductPackageDetailVOs()) {
									if ((long) packageDtvo
											.getProductPackageDetailId() == (long) receiveDetail
											.getProductPackageDetail()
											.getProductPackageDetailId()) {
										receiveDetailVO
												.setConversionUnitName(packageDtvo
														.getConversionUnitName());
										iflag = true;
										break;
									}
								}
							} else
								break;
						}
						receiveDetailVO.setBaseQuantity(AIOSCommons
								.convertExponential(receiveBL
										.getPurchaseBL()
										.getPackageConversionBL()
										.getPackageBaseQuantity(
												receiveDetailVO
														.getPackageDetailId(),
												returnedQty)));
						receiveDetailVO.setBaseUnitName(receiveDetail
								.getProduct().getLookupDetailByProductUnit()
								.getDisplayName());
					} else {
						receiveDetailVO.setPackageDetailId(-1l);
						receiveDetailVO.setConversionUnitName("");
						receiveDetailVO.setBaseQuantity("");
					}
					receiveDetailVO.setReceiveQty(receiveDetailVO
							.getPackageUnit());
					receiveDetailVO.setReturnedQty(returnedQty);
					receiveDetailVOs.add(receiveDetailVO);
				}
			}
			ServletActionContext.getRequest().setAttribute("RECEIVE_DETAILS",
					receiveDetailVOs);
			LOGGER.info("Module: Accounts : Method: showReceiveNotesDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showReceiveNotesDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showReceiveList() {
		try {
			LOGGER.info("Module: Accounts : Method: showReceiveList");
			receiveList = receiveBL.getReceiveService().getAllReceiveNotes(
					receiveBL.getPurchaseBL().getImplemenationId());
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			if (receiveList != null && receiveList.size() > 0) {
				double totalReceive = 0;
				for (Receive list : receiveList) {
					JSONArray array = new JSONArray();
					array.add(list.getReceiveId());
					array.add(list.getReceiveNumber());
					array.add(DateFormat.convertDateToString(list
							.getReceiveDate().toString()));
					List<ReceiveDetail> receiveDetail = new ArrayList<ReceiveDetail>(
							list.getReceiveDetails());
					totalReceive = 0;
					boolean invoiceFlag = false;
					String status = "Pending";
					Set<String> purchaseNoSet = new HashSet<String>();
					for (ReceiveDetail list2 : receiveDetail) {
						totalReceive += list2.getUnitRate()
								* list2.getReceiveQty();
						purchaseNoSet.add(list2.getPurchase()
								.getPurchaseNumber());
						if (list2.getInvoiceDetails() != null
								&& list2.getInvoiceDetails().size() > 0
								&& !invoiceFlag) {
							invoiceFlag = true;
							status = "Invoiced";
						}
					}
					StringBuilder purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(" ,");

					array.add(purchaseNumber.toString());
					array.add(list.getReferenceNo());
					array.add(list.getDeliveryNo());
					array.add(AIOSCommons.formatAmount(totalReceive));
					array.add(invoiceFlag);
					array.add(list.getSupplier().getSupplierNumber());
					if (null != list.getSupplier().getPerson()
							&& !("").equals(list.getSupplier().getPerson()))
						array.add(list
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat(" ")
								.concat(list.getSupplier().getPerson()
										.getLastName()));
					else
						array.add(null != list.getSupplier()
								.getCmpDeptLocation() ? list.getSupplier()
								.getCmpDeptLocation().getCompany()
								.getCompanyName() : list.getSupplier()
								.getCompany().getCompanyName());

					array.add(status);
					array.add(null != list.getPerson() ? list.getPerson()
							.getFirstName()
							+ " "
							+ list.getPerson().getLastName() : "");
					data.add(array);
				}
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showReceiveList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show GRN
	public String showReceiveEntry() {
		try {
			LOGGER.info("Module: Accounts : Method: showReceiveEntry");
			receiveNumber = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("PURCHASE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			if (receiveId > 0) {
				ReceiveVO receiveVO = new ReceiveVO();
				Receive receive = receiveBL.getReceiveService()
						.getReceiptsById(receiveId);
				BeanUtils.copyProperties(receiveVO, receive);
				receiveVO
						.setSupplierName(null != receive.getSupplier()
								.getPerson() ? receive
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat("")
								.concat(receive.getSupplier().getPerson()
										.getLastName()) : (null != receive
								.getSupplier().getCompany() ? receive
								.getSupplier().getCompany().getCompanyName()
								: receive.getSupplier().getCmpDeptLocation()
										.getCompany().getCompanyName()));
				receiveVO.setStrReceiveDate(DateFormat
						.convertDateToString(receive.getReceiveDate()
								.toString()));
				ServletActionContext.getRequest().setAttribute("RECEIVE_INFO",
						receiveVO);
				Map<Long, PurchaseVO> standingPOMap = getStandingPOForSupplier(receive
						.getSupplier().getSupplierId());
				Map<Long, PurchaseVO> editPOReceiveMap = new HashMap<Long, PurchaseVO>();
				Set<Long> purchaseIds = new HashSet<Long>();
				for (ReceiveDetail receiveDetail : receive.getReceiveDetails()) {
					if (null != standingPOMap
							&& standingPOMap.containsKey(receiveDetail
									.getPurchase().getPurchaseId())) {
						editPOReceiveMap.put(receiveDetail.getPurchase()
								.getPurchaseId(), standingPOMap
								.get(receiveDetail.getPurchase()
										.getPurchaseId()));
						standingPOMap.remove(receiveDetail.getPurchase()
								.getPurchaseId());
					}
					purchaseIds
							.add(receiveDetail.getPurchase().getPurchaseId());
				}
				List<Purchase> purchases = receiveBL.getPurchaseBL()
						.getPurchaseService()
						.getReceivePurchaseOrders(purchaseIds);
				List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>();
				for (Purchase purchase : purchases)
					purchaseDetails.addAll(new ArrayList<PurchaseDetail>(
							purchase.getPurchaseDetails()));
				List<PurchaseVO> currentReceivePOs = receiveBL.getPurchaseBL()
						.validatePurchaseReceive(purchaseDetails);

				Map<Long, PurchaseVO> receivePOMap = new HashMap<Long, PurchaseVO>();
				for (PurchaseVO lpo : currentReceivePOs)
					receivePOMap.put(lpo.getPurchaseId(), lpo);
				PurchaseVO purchaseVOtmp = null;
				Map<Long, List<PurchaseDetailVO>> bacthPurchasePOs = new HashMap<Long, List<PurchaseDetailVO>>();
				List<PurchaseDetailVO> bacthPurchasePOtmp = null;
				PurchaseDetailVO purchaseDetailVOtmp = null;
				List<PurchaseDetailVO> purchaseDetailVOstmp = null;
				Map<String, ReceiveDetailVO> productQtyMap = new HashMap<String, ReceiveDetailVO>();
				double productQty = 0;
				ReceiveDetailVO rcvDetailVO = null;
				String key = "";
				for (ReceiveDetail receiveDetail : receive.getReceiveDetails()) {
					productQty = 0;
					rcvDetailVO = new ReceiveDetailVO();
					key = receiveDetail.getProduct().getProductId() + "-"
							+ receiveDetail.getPurchase().getPurchaseId();
					if (productQtyMap.containsKey(key)) {
						rcvDetailVO = productQtyMap.get(key);
						productQty += rcvDetailVO.getEditReceiveQty();
					}
					receiveDetail.setReceiveQty(receiveDetail.getPackageUnit());
					productQty += receiveDetail.getReceiveQty();
					rcvDetailVO.setEditReceiveQty(productQty);
					rcvDetailVO.setShelf(receiveDetail.getShelf());
					productQtyMap.put(key, rcvDetailVO);
					purchaseVOtmp = receivePOMap.get(receiveDetail
							.getPurchase().getPurchaseId());
					purchaseDetailVOstmp = new ArrayList<PurchaseDetailVO>();
					for (PurchaseDetailVO purchaseDtVO : purchaseVOtmp
							.getPurchaseDetailVO()) {
						if ((long) purchaseDtVO.getProduct().getProductId() == (long) receiveDetail
								.getProduct().getProductId())
							purchaseDetailVOstmp.add(purchaseDtVO);
					}
					double returnQty = 0;
					if (null != receiveDetail.getGoodsReturnDetails()
							&& receiveDetail.getGoodsReturnDetails().size() > 0) {
						for (GoodsReturnDetail returnDetail : receiveDetail
								.getGoodsReturnDetails())
							returnQty += returnDetail.getPackageUnit();
					}
					if (returnQty > 0) {
						rcvDetailVO = productQtyMap.get(key);
						rcvDetailVO.setEditReturnQty(returnQty);
						productQtyMap.put(key, rcvDetailVO);
					}
					purchaseDetailVOtmp = new PurchaseDetailVO();
					for (PurchaseDetailVO purchaseDtVO : purchaseDetailVOstmp) {
						purchaseDtVO.setReceivedQty(purchaseDtVO
								.getReceivedQty()
								- receiveDetail.getReceiveQty());
						purchaseDtVO.setReturnedQty((null != purchaseDtVO
								.getReturnedQty() ? (purchaseDtVO
								.getReturnedQty() - returnQty) : 0));
						purchaseDtVO.setReceiveQty((null != purchaseDtVO
								.getReceiveQty() ? purchaseDtVO.getReceiveQty()
								: 0)
								+ returnQty + receiveDetail.getReceiveQty());
						purchaseDtVO.setQuantity(purchaseDtVO.getReceiveQty());
						BeanUtils.copyProperties(purchaseDetailVOtmp,
								purchaseDtVO);
					}
					purchaseDetailVOtmp
							.setPackageDetailId(null != receiveDetail
									.getProductPackageDetail() ? receiveDetail
									.getProductPackageDetail()
									.getProductPackageDetailId() : null);
					purchaseDetailVOtmp.setPackageUnit(receiveDetail
							.getPackageUnit());
					purchaseDetailVOtmp.setQuantity(receiveDetail
							.getReceiveQty());
					purchaseDetailVOtmp.setReceiveQty(receiveDetail
							.getReceiveQty());
					purchaseDetailVOtmp.setBatchNumber(receiveDetail
							.getBatchNumber());
					purchaseDetailVOtmp.setExpiryDate(null != receiveDetail
							.getProductExpiry() ? DateFormat
							.convertDateToString(receiveDetail
									.getProductExpiry().toString()) : "");
					purchaseDetailVOtmp.setShelfId(receiveDetail.getShelf()
							.getShelfId());
					purchaseDetailVOtmp.setStoreName(receiveDetail.getShelf()
							.getShelf().getAisle().getStore().getStoreName()
							+ ">>"
							+ receiveDetail.getShelf().getShelf().getAisle()
									.getSectionName()
							+ ">>"
							+ receiveDetail.getShelf().getShelf().getName()
							+ ">>" + receiveDetail.getShelf().getName());
					purchaseDetailVOtmp.setReceivedQty(0d);
					purchaseDetailVOtmp.setReturnedQty(0d);
					purchaseDetailVOtmp.setReceiveDetailId(receiveDetail
							.getReceiveDetailId());

					bacthPurchasePOtmp = new ArrayList<PurchaseDetailVO>();
					if (bacthPurchasePOs.containsKey(receiveDetail
							.getPurchase().getPurchaseId()))
						bacthPurchasePOtmp.addAll(bacthPurchasePOs
								.get(receiveDetail.getPurchase()
										.getPurchaseId()));
					bacthPurchasePOtmp.add(purchaseDetailVOtmp);
					bacthPurchasePOs.put(receiveDetail.getPurchase()
							.getPurchaseId(), bacthPurchasePOtmp);
				}
				key = "";
				if (null != receivePOMap && receivePOMap.size() > 0) {
					for (Entry<Long, PurchaseVO> entry : receivePOMap
							.entrySet()) {
						for (PurchaseDetailVO purchaseDetailVO : entry
								.getValue().getPurchaseDetailVO()) {
							key = purchaseDetailVO.getProduct().getProductId()
									+ "-" + entry.getKey();
							if (productQtyMap.containsKey(key)) {
								rcvDetailVO = new ReceiveDetailVO();
								rcvDetailVO = productQtyMap.get(key);
								purchaseDetailVO.setQuantity(rcvDetailVO
										.getEditReceiveQty());
								purchaseDetailVO.setReturnQty(rcvDetailVO
										.getEditReturnQty());
								purchaseDetailVO.setShelfId(rcvDetailVO
										.getShelf().getShelfId());
								purchaseDetailVO.setStoreName(rcvDetailVO
										.getShelf().getShelf().getAisle()
										.getStore().getStoreName()
										+ ">>"
										+ rcvDetailVO.getShelf().getShelf()
												.getAisle().getSectionName()
										+ ">>"
										+ rcvDetailVO.getShelf().getShelf()
												.getName()
										+ ">>"
										+ rcvDetailVO.getShelf().getName());
							}
						}
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"PURCHASE_EDITINFO", receivePOMap.values());
				List<PurchaseVO> purchaseVOs = new ArrayList<PurchaseVO>();
				for (PurchaseVO purchaseVO : receivePOMap.values()) {
					purchaseVOtmp = new PurchaseVO();
					purchaseVOtmp = (PurchaseVO) receivePOMap.get(purchaseVO
							.getPurchaseId());
					purchaseVOs.add(purchaseVOtmp);
				}
				ServletActionContext.getRequest().setAttribute(
						"PURCHASE_BYIDS", purchaseVOs);
				if (null == standingPOMap || standingPOMap.size() == 0)
					standingPOMap = new HashMap<Long, PurchaseVO>();
				else {
					editPOReceiveMap = new HashMap<Long, PurchaseVO>();
					editPOReceiveMap.putAll(standingPOMap);
					ServletActionContext.getRequest().setAttribute(
							"PURCHASE_INFO", editPOReceiveMap.values());
				}
				standingPOMap.putAll(receivePOMap);
				Map<Long, PurchaseDetailVO> purchaseDetailVOMap = null;
				for (Entry<Long, PurchaseVO> poEntry : standingPOMap.entrySet()) {
					purchaseIds = new HashSet<Long>();
					purchaseVOtmp = standingPOMap.get(poEntry.getKey());
					purchaseDetailVOMap = new HashMap<Long, PurchaseDetailVO>();
					for (PurchaseDetailVO vo : purchaseVOtmp
							.getPurchaseDetailVO())
						purchaseDetailVOMap.put(vo.getPurchaseDetailId(), vo);
					bacthPurchasePOtmp = bacthPurchasePOs.get(poEntry.getKey());
					if (null != bacthPurchasePOtmp
							&& bacthPurchasePOtmp.size() > 0) {
						for (PurchaseDetailVO purchaseDetailVO : bacthPurchasePOtmp) {
							if (!purchaseIds.contains(purchaseDetailVO
									.getProduct().getProductId())) {
								purchaseDetailVOtmp = purchaseDetailVOMap
										.get(purchaseDetailVO
												.getPurchaseDetailId());
								purchaseDetailVO
										.setReceivedQty(purchaseDetailVOtmp
												.getReceivedQty());
								purchaseDetailVO
										.setReturnedQty(purchaseDetailVOtmp
												.getReturnedQty());
								purchaseIds.add(purchaseDetailVO.getProduct()
										.getProductId());
							}
						}
						Collections.sort(bacthPurchasePOtmp,
								new Comparator<PurchaseDetailVO>() {
									public int compare(PurchaseDetailVO o1,
											PurchaseDetailVO o2) {
										return o1
												.getProduct()
												.getProductId()
												.compareTo(
														o2.getProduct()
																.getProductId());
									}
								});
						purchaseVOtmp
								.setPurchaseDetailBatchVO(bacthPurchasePOtmp);
					}

					standingPOMap.put(poEntry.getKey(), purchaseVOtmp);
				}
				session.setAttribute(
						"PURCHASE_SESSION_INFO_" + sessionObj.get("jSessionId"),
						standingPOMap);
			} else {
				receiveNumber = SystemBL.getReferenceStamp(Receive.class
						.getName(), receiveBL.getPurchaseBL()
						.getImplemenationId());
				ServletActionContext.getRequest().setAttribute(
						"RECEIVE_NUMBER", receiveNumber);
			}
			LOGGER.info("Module: Accounts : Method: showReceiveEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showReceiveEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Map<Long, PurchaseVO> getStandingPOForSupplier(long supplierId)
			throws Exception {
		List<Purchase> purchases = receiveBL.getPurchaseBL()
				.getSupplierActivePurchaseOrder(supplierId);
		Map<Long, PurchaseVO> standingPOMap = null;
		if (null != purchases && purchases.size() > 0) {
			List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>();
			for (Purchase purchase : purchases)
				purchaseDetails.addAll(new ArrayList<PurchaseDetail>(purchase
						.getPurchaseDetails()));
			List<PurchaseVO> supplierActivePO = receiveBL.getPurchaseBL()
					.validatePurchaseReceive(purchaseDetails);

			standingPOMap = new HashMap<Long, PurchaseVO>();
			if (null != supplierActivePO && supplierActivePO.size() > 0) {
				for (PurchaseVO list : supplierActivePO)
					standingPOMap.put(list.getPurchaseId(), list);
			}
		}
		return standingPOMap;
	}

	public String editReceiveEntry() {
		try {
			LOGGER.info("Module: Accounts : Method: editReceiveEntry");
			receiveNumber = null;
			if (recordId != null && recordId > 0) {
				receiveId = recordId;
				CommentVO comment = new CommentVO();
				if (receiveId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = receiveBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			ReceiveVO receiveVO = null;
			if (receiveId > 0) {
				receiveVO = new ReceiveVO();
				Receive receive = receiveBL.getReceiveService()
						.getReceiptsById(receiveId);
				BeanUtils.copyProperties(receiveVO, receive);
				ServletActionContext.getRequest().setAttribute("RECEIVE_INFO",
						receiveVO);
			}
			LOGGER.info("Module: Accounts : Method: editReceiveEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: editReceiveEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show Purchase Asset Detail
	public String showAssetReceiveDetail() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showAssetReceiveDetail");
			aaData = receiveBL.getAssetReceiveDetail(receiveBL.getPurchaseBL()
					.getImplemenationId());
			LOGGER.info("Module: Accounts : Method: showAssetReceiveDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAssetReceiveDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// show active purchase order for an selected supplier
	@SuppressWarnings("unchecked")
	public String supplierActivePurchaseOrder() {
		LOGGER.info("Inside Module: Accounts : Method: supplierActivePurchaseOrder");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("PURCHASE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Purchase> purchases = receiveBL.getPurchaseBL()
					.getSupplierActivePurchaseOrder(supplierId);
			List<PurchaseVO> supplierActivePO = null;
			if (null != purchases && purchases.size() > 0) {
				List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>();
				for (Purchase purchase : purchases)
					purchaseDetails.addAll(new ArrayList<PurchaseDetail>(
							purchase.getPurchaseDetails()));
				supplierActivePO = receiveBL.getPurchaseBL()
						.validatePurchaseReceive(purchaseDetails);

				Map<Long, PurchaseVO> purchaseSession = new HashMap<Long, PurchaseVO>();
				if (null != supplierActivePO && supplierActivePO.size() > 0) {
					Collections.sort(supplierActivePO,
							new Comparator<PurchaseVO>() {
								public int compare(PurchaseVO p1, PurchaseVO p2) {
									return p2.getPurchaseId().compareTo(
											p1.getPurchaseId());
								}
							});
					for (PurchaseVO list : supplierActivePO) {
						purchaseSession.put(list.getPurchaseId(), list);
					}
				}
				session.setAttribute(
						"PURCHASE_SESSION_INFO_" + sessionObj.get("jSessionId"),
						purchaseSession);
				if (null == session.getAttribute("STORE_INFO_"
						+ sessionObj.get("jSessionId"))
						|| ((List<StoreVO>) session.getAttribute("STORE_INFO_"
								+ sessionObj.get("jSessionId"))).size() > 0) {
					List<StoreVO> storeVOs = receiveBL
							.getStoreBL()
							.addStoreVos(
									getReceiveBL()
											.getStoreBL()
											.getStoreService()
											.getAllStores(
													receiveBL
															.getPurchaseBL()
															.getImplemenationId()));
					session.setAttribute(
							"STORE_INFO_" + sessionObj.get("jSessionId"),
							storeVOs);
				}
			}
			ServletActionContext.getRequest().setAttribute("PURCHASE_INFO",
					supplierActivePO);
			LOGGER.info("Module: Accounts : Method: supplierActivePurchaseOrder Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: supplierActivePurchaseOrder: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String supplierActivePurchaseOrderEdit() {
		LOGGER.info("Inside Module: Accounts : Method: supplierActivePurchaseOrderEdit");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("PURCHASE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			// Pending PO items
			List<Purchase> supplierListTemp = receiveBL.getPurchaseBL()
					.getSupplierActivePurchaseOrder(supplierId);
			List<PurchaseDetail> purchaseDetails = new ArrayList<PurchaseDetail>();
			for (Purchase purchase : supplierListTemp)
				purchaseDetails.addAll(new ArrayList<PurchaseDetail>(purchase
						.getPurchaseDetails()));
			List<PurchaseVO> supplierActivePOTemp = receiveBL.getPurchaseBL()
					.validatePurchaseReceive(purchaseDetails);

			// Current Receive
			List<Purchase> supplierList = receiveBL.getPurchaseBL()
					.getSupplierActivePurchaseOrderAndCurrentReceive(receiveId);

			List<PurchaseVO> supplierActivePO = receiveBL.getPurchaseBL()
					.validatePurchaseReceiveEdit(supplierList, null);

			// Other Receives
			supplierList = receiveBL.getPurchaseBL().getPurchaseService()
					.supplierPOByOtherReceives(receiveId, supplierList);

			supplierActivePO = receiveBL
					.getPurchaseBL()
					.validatePurchaseReceiveEdit(supplierList, supplierActivePO);

			supplierActivePO.addAll(supplierActivePOTemp);

			Map<Long, PurchaseVO> purchaseSession = new HashMap<Long, PurchaseVO>();
			if (null != supplierActivePO && supplierActivePO.size() > 0) {

				for (PurchaseVO list : supplierActivePO) {
					if (!purchaseSession.containsKey(list.getPurchaseId())) {
						purchaseSession.put(list.getPurchaseId(), list);
					}
				}
				supplierActivePO = new ArrayList<PurchaseVO>(
						purchaseSession.values());
				Collections.sort(supplierActivePO,
						new Comparator<PurchaseVO>() {
							public int compare(PurchaseVO p1, PurchaseVO p2) {
								return p2.getPurchaseId().compareTo(
										p1.getPurchaseId());
							}
						});
			}
			session.setAttribute(
					"PURCHASE_SESSION_INFO_" + sessionObj.get("jSessionId"),
					purchaseSession);
			if (null == session.getAttribute("STORE_INFO_"
					+ sessionObj.get("jSessionId"))
					|| ((List<StoreVO>) session.getAttribute("STORE_INFO_"
							+ sessionObj.get("jSessionId"))).size() > 0) {
				List<StoreVO> storeVOs = receiveBL.getStoreBL().addStoreVos(
						getReceiveBL()
								.getStoreBL()
								.getStoreService()
								.getAllStores(
										receiveBL.getPurchaseBL()
												.getImplemenationId()));
				session.setAttribute(
						"STORE_INFO_" + sessionObj.get("jSessionId"), storeVOs);
			}

			ServletActionContext.getRequest().setAttribute("PURCHASE_INFO",
					supplierActivePO);
			LOGGER.info("Module: Accounts : Method: supplierActivePurchaseOrderEdit Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: supplierActivePurchaseOrderEdit: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get purchase info from session by purchase id
	public String showSessionPurchaseInfo() {
		LOGGER.info("Inside Module: Accounts : Method: showSessionPurchaseInfo");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseList = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseList.get(purchaseId);
			ServletActionContext.getRequest().setAttribute("rowId", rowId);
			ServletActionContext.getRequest().setAttribute("PURCHASE_BYID",
					purchaseVO);
			LOGGER.info("Module: Accounts : Method: showSessionPurchaseInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionPurchaseInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get purchase detail stored in session by purchase id
	@SuppressWarnings("unchecked")
	public String showSessionPurchaseDetail() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showSessionPurchaseDetail");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, PurchaseVO> purchaseList = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseList.get(purchaseId);
			ServletActionContext.getRequest().setAttribute("PURCHASE_SESINFO",
					purchaseVO);
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_DETAIL_BYID", purchaseVO.getPurchaseDetailVO());
			LOGGER.info("Module: Accounts : Method: showSessionPurchaseDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionPurchaseDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get purchase detail stored in session by purchase id
	@SuppressWarnings("unchecked")
	public String showSessionPurchaseDetailEdit() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showSessionPurchaseDetail");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, PurchaseVO> purchaseList = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseList.get(purchaseId);
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_DETAIL_BYID", purchaseVO.getPurchaseDetailVO());
			LOGGER.info("Module: Accounts : Method: showSessionPurchaseDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionPurchaseDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	// Show Store from Session
	public String showStoreSession() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showStoreSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<StoreVO> storeVOs = (List<StoreVO>) (session
					.getAttribute("STORE_INFO_" + sessionObj.get("jSessionId")));
			if (null == storeVOs || storeVOs.size() == 0)
				storeVOs = receiveBL.getStoreBL().addStoreVos(
						getReceiveBL()
								.getStoreBL()
								.getStoreService()
								.getAllStores(
										receiveBL.getPurchaseBL()
												.getImplemenationId()));

			ServletActionContext.getRequest().setAttribute("STORE_INFO",
					storeVOs);
			LOGGER.info("Module: Accounts : Method: showStoreSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showStoreDetails() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showStoreDetails");
			List<StoreVO> storeVOs = receiveBL.getStoreBL().addStoreVos(
					getReceiveBL()
							.getStoreBL()
							.getStoreService()
							.getAllStores(
									receiveBL.getPurchaseBL()
											.getImplemenationId()));
			List<StoreVO> finalStoreVOs = new ArrayList<StoreVO>();
			if (storeId > 0) {
				for (StoreVO storeVO : storeVOs) {
					if ((long) storeVO.getStoreId() == storeId) {
						finalStoreVOs.add(storeVO);
						break;
					}
				}
				storeId = 0;
			} else
				finalStoreVOs.addAll(storeVOs);
			ServletActionContext.getRequest().setAttribute("STORE_INFO",
					finalStoreVOs);
			LOGGER.info("Module: Accounts : Method: showStoreDetails Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String checkReceivePurchaseBatch() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: checkReceivePurchaseBatch");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseMap = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseMap.get(purchaseId);
			if (null != purchaseVO.getPurchaseDetailBatchVO()
					&& purchaseVO.getPurchaseDetailBatchVO().size() > 0)
				sqlReturnMessage = "true";
			else
				sqlReturnMessage = "false";
			LOGGER.info("Module: Accounts : Method: checkReceivePurchaseBatch Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: checkReceivePurchaseBatch: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteReceivePurchaseBatchSession() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: deleteReceivePurchaseBatchSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseMap = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseMap.get(purchaseId);
			if (null != purchaseVO.getPurchaseDetailBatchVO()
					&& purchaseVO.getPurchaseDetailBatchVO().size() > 0) {
				purchaseVO.setPurchaseDetailBatchVO(null);
			}
			purchaseMap.put(purchaseId, purchaseVO);
			session.setAttribute(
					"PURCHASE_SESSION_INFO_" + sessionObj.get("jSessionId"),
					purchaseMap);
			sqlReturnMessage = "true";
			LOGGER.info("Module: Accounts : Method: deleteReceivePurchaseBatchSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: deleteReceivePurchaseBatchSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String updatePurchaseSession() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: updatePurchaseSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, PurchaseVO> purchaseList = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			PurchaseVO purchaseVO = (PurchaseVO) purchaseList.get(purchaseId);
			PurchaseDetailVO purchaseDetailVO = null;
			List<PurchaseDetailVO> detailList = new ArrayList<PurchaseDetailVO>();
			Map<Long, PurchaseDetailVO> detailVOMap = new HashMap<Long, PurchaseDetailVO>();
			for (PurchaseDetailVO list : purchaseVO.getPurchaseDetailVO()) {
				detailVOMap.put(list.getPurchaseDetailId(), list);
			}
			Map<String, PurchaseDetailVO> batchProductMap = null;
			PurchaseDetailVO purchaseDetailVOBatch = null;
			List<PurchaseDetailVO> purchaseDetailBatchVOs = null;
			if (batchProduct) {
				batchProductMap = new HashMap<String, PurchaseDetailVO>();
				purchaseDetailBatchVOs = new ArrayList<PurchaseDetailVO>();
			}
			double poAmount = 0;
			JSONParser parser = new JSONParser();
			Object purchaseDetailObject = parser.parse(purchaseDetails);
			org.json.simple.JSONArray object = (org.json.simple.JSONArray) purchaseDetailObject;
			Shelf shelf = null;
			String key = "";
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				purchaseDetailVO = new PurchaseDetailVO();
				org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) iterator
						.next();
				org.json.simple.JSONArray detailArray = (org.json.simple.JSONArray) jsonObject
						.get("purchsaeDetailJS");
				org.json.simple.JSONObject jsonObjectDetail = (org.json.simple.JSONObject) detailArray
						.get(0);
				purchaseDetailVO = detailVOMap.get(Long
						.parseLong(jsonObjectDetail.get("purchaseDetailId")
								.toString()));
				if (batchProduct) {
					purchaseDetailVOBatch = new PurchaseDetailVO();
					BeanUtils.copyProperties(purchaseDetailVOBatch,
							purchaseDetailVO);
					key = (purchaseDetailVOBatch
							.getPurchaseDetailId()
							.toString()
							.concat((null != jsonObjectDetail
									.get("batchNumber") && !("")
									.equals(jsonObjectDetail.get("batchNumber"))) ? "-"
									+ jsonObjectDetail.get("batchNumber")
											.toString()
									: "")
							.concat((null != jsonObjectDetail
									.get("productExpiry")
									&& !("").equals(jsonObjectDetail
											.get("productExpiry")) ? "-"
									+ jsonObjectDetail.get("productExpiry")
											.toString() : ""))
							.concat((null != jsonObjectDetail
									.get("packageDetailId")
									&& !("").equals(jsonObjectDetail
											.get("packageDetailId")) ? "-"
									+ jsonObjectDetail.get("packageDetailId")
											.toString() : ""))).trim();
					if (batchProductMap.containsKey(key)) {
						purchaseDetailVOBatch = batchProductMap.get(key);
						purchaseDetailVOBatch.setQuantity(purchaseDetailVOBatch
								.getQuantity()
								+ Double.parseDouble(jsonObjectDetail.get(
										"receiveQty").toString()));
						batchProductMap.put(key, purchaseDetailVOBatch);
					} else {
						purchaseDetailVOBatch
								.setBatchNumber(null != jsonObjectDetail
										.get("batchNumber") ? jsonObjectDetail
										.get("batchNumber").toString() : null);
						purchaseDetailVOBatch
								.setExpiryDate(null != jsonObjectDetail
										.get("productExpiry") ? jsonObjectDetail
										.get("productExpiry").toString() : null);
						purchaseDetailVOBatch.setQuantity(Double
								.parseDouble(jsonObjectDetail.get("receiveQty")
										.toString()));
						purchaseDetailVOBatch.setShelfId(Integer
								.parseInt(jsonObjectDetail.get("shelfId")
										.toString()));
						if (purchaseDetailVOBatch.getShelfId() > 0) {
							shelf = receiveBL
									.getStoreBL()
									.getStoreService()
									.getShelfByShelfId(
											purchaseDetailVOBatch.getShelfId());
							purchaseDetailVOBatch
									.setStoreName(null != shelf ? shelf
											.getName() : "");
						}
						purchaseDetailVOBatch.setReturnQty(Double
								.parseDouble(jsonObjectDetail.get("returnQty")
										.toString()));
						purchaseDetailVOBatch.setUnitRate(Double
								.parseDouble(jsonObjectDetail.get("unitRate")
										.toString()));
						purchaseDetailVOBatch
								.setDescription(null != jsonObjectDetail
										.get("linesDescription") ? jsonObjectDetail
										.get("linesDescription").toString()
										: null);
						purchaseDetailVOBatch
								.setReceiveDetailId((null != jsonObjectDetail
										.get("receiveDetailId") ? Long
										.parseLong(jsonObjectDetail.get(
												"receiveDetailId").toString())
										: null));
						purchaseDetailVOBatch.setPackageDetailId(Long
								.parseLong(jsonObjectDetail.get(
										"packageDetailId").toString()));
						purchaseDetailVOBatch.setPackageUnit(Double
								.parseDouble(jsonObjectDetail
										.get("packageUnit").toString()));
						poAmount += purchaseDetailVOBatch.getUnitRate()
								* purchaseDetailVOBatch.getQuantity();
						purchaseDetailBatchVOs.add(purchaseDetailVOBatch);
						batchProductMap.put(key, purchaseDetailVOBatch);
					}
				} else {
					purchaseDetailVO.setBatchNumber(null != jsonObjectDetail
							.get("batchNumber") ? jsonObjectDetail.get(
							"batchNumber").toString() : null);
					purchaseDetailVO.setExpiryDate(null != jsonObjectDetail
							.get("productExpiry") ? jsonObjectDetail.get(
							"productExpiry").toString() : null);
					purchaseDetailVO.setQuantity(Double
							.parseDouble(jsonObjectDetail.get("receiveQty")
									.toString()));
					purchaseDetailVO.setShelfId(Integer
							.parseInt(jsonObjectDetail.get("shelfId")
									.toString()));
					if (purchaseDetailVO.getShelfId() > 0) {
						shelf = receiveBL
								.getStoreBL()
								.getStoreService()
								.getShelfByShelfId(
										purchaseDetailVO.getShelfId());
						purchaseDetailVO.setStoreName(null != shelf ? shelf
								.getName() : "");
					}
					purchaseDetailVO.setReturnQty(Double
							.parseDouble(jsonObjectDetail.get("returnQty")
									.toString()));
					purchaseDetailVO.setUnitRate(Double
							.parseDouble(jsonObjectDetail.get("unitRate")
									.toString()));

					purchaseDetailVO.setDescription(null != jsonObjectDetail
							.get("linesDescription") ? jsonObjectDetail.get(
							"linesDescription").toString() : null);
					poAmount += purchaseDetailVO.getUnitRate()
							* purchaseDetailVO.getQuantity();
					detailList.add(purchaseDetailVO);
				}
			}
			receiveAmount = AIOSCommons.formatAmount(poAmount);
			purchaseVO.setBatchProduct(batchProduct);
			if (!batchProduct) {
				purchaseVO.setContinuousPurchase(continuousPurchase);
				purchaseVO.setPurchaseDetailVO(detailList);
			} else
				purchaseVO.setPurchaseDetailBatchVO(purchaseDetailBatchVOs);
			purchaseList.put(purchaseVO.getPurchaseId(), purchaseVO);
			LOGGER.info("Module: Accounts : Method: updatePurchaseSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: updatePurchaseSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Active Receive Info by supplier id to process Invoice
	public String showActiveGRN() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("RECEIVE_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<ReceiveDetail> supplierGRN = receiveBL.getReceiveService()
					.getSupplierActiveGRN(supplierId);
			if (null != supplierGRN && supplierGRN.size() > 0) {
				List<ReceiveVO> supplierActiveGRN = receiveBL
						.validateReceiveDetailInvoice(supplierGRN);
				Map<Long, ReceiveVO> receiveSession = new HashMap<Long, ReceiveVO>();
				for (ReceiveVO list : supplierActiveGRN)
					receiveSession.put(list.getReceiveId(), list);
				session.setAttribute(
						"RECEIVE_NOTES_SESSION_INFO_"
								+ sessionObj.get("jSessionId"), receiveSession);
				ServletActionContext.getRequest().setAttribute(
						"GOODS_RECEIVE_INFO", supplierActiveGRN);
				LOGGER.info("Module: Accounts : Method: showActiveGRN Success");
				return SUCCESS;
			} else {
				LOGGER.info("Module: Accounts : Method: showActiveGRN failure");
				return ERROR;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showActiveGRN: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Active Receive Info by supplier id to process Invoice
	public String showActiveGRNEdit() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("RECEIVE_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<ReceiveDetail> supplierGRN = receiveBL.getReceiveService()
					.getSupplierActiveGRNEdit(supplierId, invoiceId);

			if (null != supplierGRN && supplierGRN.size() > 0) {
				List<ReceiveVO> supplierActiveGRN = receiveBL
						.validateReceiveDetailInvoiceEdit(supplierGRN);
				Map<Long, ReceiveVO> receiveSession = new HashMap<Long, ReceiveVO>();
				for (ReceiveVO list : supplierActiveGRN) {
					receiveSession.put(list.getReceiveId(), list);
				}
				session.setAttribute(
						"RECEIVE_NOTES_SESSION_INFO_"
								+ sessionObj.get("jSessionId"), receiveSession);
				ServletActionContext.getRequest().setAttribute(
						"GOODS_RECEIVE_INFO", supplierActiveGRN);
				LOGGER.info("Module: Accounts : Method: showActiveGRN Success");
				return SUCCESS;
			} else {
				LOGGER.info("Module: Accounts : Method: showActiveGRN failure");
				return ERROR;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showActiveGRN: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Fetch Receive Info from session thru receive_id
	public String showSessionReceiveInfo() {
		LOGGER.info("Inside Module: Accounts : Method: showSessionReceiveInfo");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, ReceiveVO> receiveHash = (Map<Long, ReceiveVO>) (session
					.getAttribute("RECEIVE_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			ReceiveVO receiveVO = (ReceiveVO) receiveHash.get(receiveId);
			double totalInvoice = 0;
			Purchase purchase = null;
			for (ReceiveDetailVO receiveDtVO : receiveVO.getReceiveDetailsVO()) {
				purchase = receiveBL
						.getPurchaseBL()
						.getPurchaseService()
						.getPurchaseAndCurrencyById(receiveDtVO.getPurchaseId());
				totalInvoice += receiveBL
						.getTransactionBL()
						.getCurrencyConversionBL()
						.getTransactionValue(
								purchase.getCurrency().getCurrencyId(),
								currencyId, receiveDtVO.getInvoiceQty(),
								receiveDtVO.getUnitRate(),
								purchase.getExchangeRate());
			}
			receiveVO.setTotalReceiveAmount(AIOSCommons
					.roundDecimals(totalInvoice));
			ServletActionContext.getRequest().setAttribute("rowId", rowId);
			ServletActionContext.getRequest().setAttribute(
					"GOODS_RECEIVE_BYID", receiveVO);
			LOGGER.info("Module: Accounts : Method: showSessionReceiveInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionReceiveInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Fetch Receive Detail Info from session thru receive_id
	@SuppressWarnings("unchecked")
	public String showSessionReceiveDetailInfo() {
		LOGGER.info("Inside Module: Accounts : Method: showSessionReceiveDetailInfo");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, ReceiveVO> receiveHash = (Map<Long, ReceiveVO>) (session
					.getAttribute("RECEIVE_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			ReceiveVO receiveVO = (ReceiveVO) receiveHash.get(receiveId);
			ServletActionContext.getRequest().setAttribute(
					"RECEIVE_DETAIL_BYID", receiveVO.getReceiveDetailsVO());
			LOGGER.info("Module: Accounts : Method: showSessionReceiveDetailInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionReceiveDetailInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Receive Detail Session id by receive_id
	public String updateReceiveSession() {
		LOGGER.info("Inside Module: Accounts : Method: updateReceiveSession");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, ReceiveVO> receiveHash = (Map<Long, ReceiveVO>) (session
					.getAttribute("RECEIVE_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			ReceiveVO receiveVO = (ReceiveVO) receiveHash.get(receiveId);
			String receiveInfo[] = splitArrayValues(receiveDetail, "#@");
			ReceiveDetailVO receiveDetailVO = null;
			List<ReceiveDetailVO> detailList = new ArrayList<ReceiveDetailVO>();
			Map<Long, ReceiveDetailVO> detailVOMap = new HashMap<Long, ReceiveDetailVO>();
			for (ReceiveDetailVO list : receiveVO.getReceiveDetailsVO()) {
				detailVOMap.put(list.getReceiveDetailId(), list);
			}
			for (String receive : receiveInfo) {
				String receiveLine[] = splitArrayValues(receive, "__");
				receiveDetailVO = new ReceiveDetailVO();
				if (detailVOMap.containsKey(Long.parseLong(receiveLine[2]))) {
					receiveVO.setInvoiceStatus(true);
					receiveDetailVO = detailVOMap.get(Long
							.parseLong(receiveLine[2]));
					receiveDetailVO.setInvoiceQty(Double
							.parseDouble(receiveLine[0]));
					if (!("##").equals(receiveLine[1]))
						receiveDetailVO.setDescription(receiveLine[1]);
					detailList.add(receiveDetailVO);
				}
			}
			receiveVO.setReceiveDetailsVO(detailList);
			receiveHash.put(receiveVO.getReceiveId(), receiveVO);
			invoiceAmount = 0;
			for (Entry<Long, ReceiveVO> receive : receiveHash.entrySet()) {
				ReceiveVO tempReceiveVO = (ReceiveVO) receiveHash.get(receive
						.getKey());
				if (tempReceiveVO.isInvoiceStatus()) {
					for (ReceiveDetailVO receiveDetailVO2 : tempReceiveVO
							.getReceiveDetailsVO())
						invoiceAmount += receiveDetailVO2.getInvoiceQty()
								* receiveDetailVO2.getUnitRate();
				}
			}
			LOGGER.info("Module: Accounts : Method: updateReceiveSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: updateReceiveSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Receive Detail Session id by receive_id
	public String updateReceiveSessionEdit() {
		LOGGER.info("Inside Module: Accounts : Method: updateReceiveSession");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, ReceiveVO> receiveHash = (Map<Long, ReceiveVO>) (session
					.getAttribute("RECEIVE_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			ReceiveVO receiveVO = (ReceiveVO) receiveHash.get(receiveId);
			String receiveInfo[] = splitArrayValues(receiveDetail, "#@");
			ReceiveDetailVO receiveDetailVO = null;
			List<ReceiveDetailVO> detailList = new ArrayList<ReceiveDetailVO>();
			Map<Long, ReceiveDetailVO> detailVOMap = new HashMap<Long, ReceiveDetailVO>();
			for (ReceiveDetailVO list : receiveVO.getReceiveDetailsVO()) {
				detailVOMap.put(list.getReceiveDetailId(), list);
			}
			for (String receive : receiveInfo) {
				String receiveLine[] = splitArrayValues(receive, "__");
				receiveDetailVO = new ReceiveDetailVO();
				if (detailVOMap.containsKey(Long.parseLong(receiveLine[2]))) {
					receiveVO.setInvoiceStatus(true);
					receiveDetailVO = detailVOMap.get(Long
							.parseLong(receiveLine[2]));
					receiveDetailVO.setInvoiceQty(Double
							.parseDouble(receiveLine[0]));
					if (!("##").equals(receiveLine[1]))
						receiveDetailVO.setDescription(receiveLine[1]);

					receiveDetailVO.setInvoicedFlag(Boolean
							.valueOf(receiveLine[3]));
					detailList.add(receiveDetailVO);
				}
			}
			receiveVO.setReceiveDetailsVO(detailList);
			receiveHash.put(receiveVO.getReceiveId(), receiveVO);
			invoiceAmount = 0;
			for (Entry<Long, ReceiveVO> receive : receiveHash.entrySet()) {
				ReceiveVO tempReceiveVO = (ReceiveVO) receiveHash.get(receive
						.getKey());
				if (tempReceiveVO.isInvoiceStatus()) {
					for (ReceiveDetailVO receiveDetailVO2 : tempReceiveVO
							.getReceiveDetailsVO())
						if (receiveDetailVO2.isInvoicedFlag())
							invoiceAmount += receiveDetailVO2.getInvoiceQty()
									* receiveDetailVO2.getUnitRate();
				}
			}

			session.setAttribute(
					"RECEIVE_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId"), receiveHash);
			LOGGER.info("Module: Accounts : Method: updateReceiveSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: updateReceiveSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String printReceiveNote() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_grn_template") != null ? ((String) session
					.getAttribute("print_grn_template"))
					.equalsIgnoreCase("true") : false;

			if (recordId != null && recordId > 0) {
				receiveId = recordId;
			}
			Receive receive = receiveBL.getReceiveService().getReceiveDetails(
					receiveId);
			ReceiveVO receiveVO = receiveBL.convertReceiveToVO(receive);

			Collections.sort(receiveVO.getReceiveDetailsVO(),
					new Comparator<ReceiveDetailVO>() {
						public int compare(ReceiveDetailVO t1,
								ReceiveDetailVO t2) {
							return t1.getReceiveDetailId().compareTo(
									t2.getReceiveDetailId());
						}
					});

			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute("RECEIVE_INFO",
						receiveVO);
				ServletActionContext.getRequest()
						.setAttribute("RECEIVE_DETAILS_INFO",
								receiveVO.getReceiveDetailsVO());
				ServletActionContext.getRequest().setAttribute(
						"PURCHASE_NUMBER", receiveVO.getPurchaseNumber());
				return "default";
			} else {

				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/accounts/"
						+ receiveBL.getPurchaseBL().getImplemenationId()
								.getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config.getTemplate("html-grn-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (receiveBL.getPurchaseBL().getImplemenationId()
						.getMainLogo() != null) {

					byte[] bFile = FileUtil
							.getBytes(new File(receiveBL.getPurchaseBL()
									.getImplemenationId().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put(
						"datetime",
						receive.getReceiveDate() != null ? DateFormat
								.convertDateToString(receive.getReceiveDate()
										+ "") : DateFormat
								.convertSystemDateToString(receive
										.getCreatedDate()));

				rootMap.put("referenceNumber", receiveVO.getReceiveNumber());
				rootMap.put("purchaseNumber", receiveVO.getPurchaseNumber());
				rootMap.put("invoiceNumber", receiveVO.getReferenceNo());
				rootMap.put("requisitionNumber",
						receiveVO.getRequisitionNumber());
				rootMap.put("deliveryNumber", receiveVO.getDeliveryNo());
				rootMap.put("supplier", receiveVO.getSupplierName()
						.toUpperCase());
				rootMap.put("totalAmount", receiveVO.getDisplayTotalAmount());
				rootMap.put("totalAmountInWords",
						receiveVO.getDisplayTotalAmountInWords());
				rootMap.put("receiveDetails", receiveVO.getReceiveDetailsVO());
				rootMap.put("urlPath", urlPath);
				rootMap.put("extraLines", receiveVO.getReceiveDetailsVO()
						.size() < 28 ? 28 - receiveVO.getReceiveDetailsVO()
						.size() : null);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				LOGGER.info("Module: Accounts : Method: showReceiveEntry: Action Success");
				return "template";
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to show add rows
	public String showAddRow() {
		LOGGER.info("Module: Accounts : Method: showAddRow");
		try {
			List<Product> productList = receiveBL
					.getPurchaseBL()
					.getProductBL()
					.getProductService()
					.getActiveProduct(
							receiveBL.getPurchaseBL().getImplemenationId());
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					productList);
			LOGGER.info("Module: Accounts : Method: showAddRow: Action Success");
			ServletActionContext.getRequest().setAttribute("rowId", id);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAddRow: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showReceives() {
		LOGGER.info("Module: Accounts : Method: showReceives");
		try {
			List<Receive> receiveList = this
					.getReceiveBL()
					.getReceiveService()
					.getAllReceiveNotes(
							receiveBL.getPurchaseBL().getImplemenationId());
			ServletActionContext.getRequest().setAttribute("RECEIVE_LIST_INFO",
					receiveList);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceives: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showReceiveDetail() {
		LOGGER.info("Module: Accounts : Method: showReceiveDetail");
		try {
			List<ReceiveDetail> receiveDetailList = this.getReceiveBL()
					.getReceiveService().getReceiveLineDetails(receiveId);
			ServletActionContext.getRequest().setAttribute(
					"RECEIVE_DETAIL_INFO", receiveDetailList);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get receive detail entry
	public String showReceiveLineEntry() {
		LOGGER.info("Module: Accounts : Method: showReceiveLineEntry");
		try {
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			ServletActionContext.getRequest().setAttribute("rowId", id);
			LOGGER.info("Module: Accounts : Method: showReceiveLineEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveLineEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get lpo
	public String showPurchaseOrders() {
		LOGGER.info("Module: Accounts : Method: showPurchaseOrders");
		try {
			List<Purchase> purchaseList = getReceiveBL()
					.getPurchaseBL()
					.getPurchaseService()
					.getPurchaseOrders(
							receiveBL.getPurchaseBL().getImplemenationId());
			ServletActionContext.getRequest().setAttribute("PURCHASE_INFO",
					purchaseList);
			LOGGER.info("Module: Accounts : Method: showPurchaseOrders: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseOrders: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPurchaseDetail() {
		LOGGER.info("Module: Accounts : Method: showPurchaseDetail");
		try {
			List<PurchaseDetail> purchaseDetail = getReceiveBL()
					.getPurchaseBL().getPurchaseService()
					.getPurchaseDetailOrderById(purchaseId);
			List<Store> storeList = getReceiveBL()
					.getStoreBL()
					.getStoreService()
					.getAllStores(
							receiveBL.getPurchaseBL().getImplemenationId());
			List<Aisle> aisleList = new ArrayList<Aisle>();
			for (Store list : storeList) {
				List<Aisle> tempAisleList = getReceiveBL().getStoreBL()
						.getStoreService().getAsileDetails(list.getStoreId());
				aisleList.addAll(tempAisleList);
			}
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_DETAIL_INFO", purchaseDetail);
			ServletActionContext.getRequest().setAttribute("STORE_INFO",
					storeList);
			ServletActionContext.getRequest().setAttribute("AISLE_INFO",
					aisleList);
			LOGGER.info("Module: Accounts : Method: showPurchaseDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPurchaseReturnDetail() {
		LOGGER.info("Module: Accounts : Method: showPurchaseReturnDetail");
		try {
			List<PurchaseDetail> purchaseDetail = getReceiveBL()
					.getPurchaseBL().getPurchaseService()
					.getPurchaseDetailOrderById(purchaseId);
			List<PurchaseDetailVO> purchaseDetailVO = convertListVo(purchaseDetail);
			String purchaseArray[] = splitArrayValues(purchaseReturns, "@@");
			PurchaseDetailVO purchaseLine = null;
			List<PurchaseDetailVO> tempPurchaseList = new ArrayList<PurchaseDetailVO>();
			for (String purchase : purchaseArray) {
				String purchaseValue[] = splitArrayValues(purchase, "&&");
				purchaseLine = new PurchaseDetailVO();
				purchaseLine.setPurchaseDetailId(Long
						.parseLong(purchaseValue[0]));
				purchaseLine.setQuantity(Double.parseDouble(purchaseValue[3]));
				tempPurchaseList.add(purchaseLine);
			}
			for (PurchaseDetailVO list : purchaseDetailVO) {
				for (PurchaseDetail list2 : tempPurchaseList) {
					if (list.getPurchaseDetailId().equals(
							list2.getPurchaseDetailId())) {
						list.setReturnQty(list2.getQuantity());
						list.setReceiveQty(list.getQuantity()
								- list2.getQuantity());
						list.setTotalRate(list.getReceiveQty()
								* list.getUnitRate());
					} else {
						list.setReceiveQty(list.getQuantity());
						list.setTotalRate(list.getReceiveQty()
								* list.getUnitRate());
					}
				}
			}
			List<Store> storeList = getReceiveBL()
					.getStoreBL()
					.getStoreService()
					.getAllStores(
							receiveBL.getPurchaseBL().getImplemenationId());
			List<Aisle> aisleList = new ArrayList<Aisle>();
			for (Store list : storeList) {
				List<Aisle> tempAisleList = getReceiveBL().getStoreBL()
						.getStoreService().getAsileDetails(list.getStoreId());
				aisleList.addAll(tempAisleList);
			}
			receiveNumber = SystemBL.getReferenceStamp(GoodsReturn.class
					.getName(), receiveBL.getPurchaseBL().getImplemenationId());
			ServletActionContext.getRequest().setAttribute("RETURN_NUMBER",
					receiveNumber);
			ServletActionContext.getRequest().setAttribute(
					"PURCHASE_DETAILVO_INFO", purchaseDetailVO);
			ServletActionContext.getRequest().setAttribute("STORE_INFO",
					storeList);
			ServletActionContext.getRequest().setAttribute("AISLE_INFO",
					aisleList);
			LOGGER.info("Module: Accounts : Method: showPurchaseReturnDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showPurchaseReturnDetail: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private static List<PurchaseDetailVO> convertListVo(
			List<PurchaseDetail> detalList) {
		List<PurchaseDetailVO> detailListVo = new ArrayList<PurchaseDetailVO>();
		for (PurchaseDetail list : detalList) {
			detailListVo.add(convertyEntity(list));
		}
		return detailListVo;
	}

	private static PurchaseDetailVO convertyEntity(PurchaseDetail detail) {
		PurchaseDetailVO detailVO = new PurchaseDetailVO();
		detailVO.setPurchaseDetailId(detail.getPurchaseDetailId());
		detailVO.setQuantity(detail.getQuantity());
		detailVO.setUnitRate(detail.getUnitRate());
		detailVO.setProduct(detail.getProduct());
		return detailVO;
	}

	// to get product details
	public String showProductDetails() {
		LOGGER.info("Module: Accounts : Method: showProductDetails");
		try {
			List<Product> productList = receiveBL
					.getPurchaseBL()
					.getProductBL()
					.getProductService()
					.getActiveProduct(
							receiveBL.getPurchaseBL().getImplemenationId(),
							itemNature.charAt(0));
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					productList);
			LOGGER.info("Module: Accounts : Method: showProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to store session product line add entry
	@SuppressWarnings("unchecked")
	public String saveReceiptAddLines() {
		LOGGER.info("Module: Accounts : Method: saveReceiptAddLines");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ReceiveDetail detail = new ReceiveDetail();
			Product product = new Product();
			product.setProductId(productId);
			detail.setReceiveQty(receiveQty);
			detail.setProduct(product);
			if (receiveId > 0) {
				Receive receive = new Receive();
				receive.setReceiveId(receiveId);
				if (receiveDetailId > 0)
					detail.setReceiveDetailId(receiveDetailId);
				else
					detail.setReceiveDetailId(null);
				detail.setReceive(receive);
			} else {
				detail.setReceiveDetailId(null);
			}
			if (session.getAttribute("RECEIVE_LINES_"
					+ sessionObj.get("jSessionId")) == null)
				detailList = new ArrayList<ReceiveDetail>();
			else
				detailList = (List<ReceiveDetail>) session
						.getAttribute("RECEIVE_LINES_"
								+ sessionObj.get("jSessionId"));
			detailList.add(detail);
			session.setAttribute(
					"RECEIVE_LINES_" + sessionObj.get("jSessionId"), detailList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute("sqlReturnMessage",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveReceiptAddLines: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: saveReceiptAddLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to update session store values
	@SuppressWarnings("unchecked")
	public String saveReceiptAddupdateLines() {
		LOGGER.info("Module: Accounts : Method: saveReceiptAddupdateLines");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ReceiveDetail detail = new ReceiveDetail();
			Product product = new Product();
			product.setProductId(productId);
			detail.setReceiveQty(receiveQty);
			detail.setProduct(product);
			if (receiveId > 0) {
				Receive receive = new Receive();
				receive.setReceiveId(receiveId);
				detail.setReceiveDetailId(receiveDetailId);
				detail.setReceive(receive);
			}
			detailList = (List<ReceiveDetail>) session
					.getAttribute("RECEIVE_LINES_"
							+ sessionObj.get("jSessionId"));
			detailList.set(id - 1, detail);
			session.setAttribute(
					"RECEIVE_LINES_" + sessionObj.get("jSessionId"), detailList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute("sqlReturnMessage",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveReceiptAddupdateLines: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: saveReceiptAddupdateLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to delete receipt lines
	@SuppressWarnings("unchecked")
	public String deleteReceiptLines() {
		LOGGER.info("Module: Accounts : Method: deleteReceiptLines");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			detailList = (List<ReceiveDetail>) session
					.getAttribute("RECEIVE_LINES_"
							+ sessionObj.get("jSessionId"));
			detailList.remove(id - 1);
			session.setAttribute(
					"RECEIVE_LINES_" + sessionObj.get("jSessionId"), detailList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute("sqlReturnMessage",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: deleteReceiptLines: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: deleteReceiptLines: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save GRN
	@SuppressWarnings("unchecked")
	public String saveReceipts() {
		try {
			LOGGER.info("Module: Accounts : Method: saveReceipts");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			// Validate Period
			boolean openPeriod = receiveBL.getTransactionBL().getCalendarBL()
					.transactionPostingPeriod(receiveDate);

			if (!openPeriod) {
				sqlReturnMessage = "Period not open for the Receive date.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", sqlReturnMessage);
				LOGGER.info("Module: Accounts : Method: saveReceipts: (Error : Period not opened)");
				return SUCCESS;
			}
			Receive receiveEdit = null;
			if (receiveId > 0)
				receiveEdit = receiveBL.getReceiveService().getReceiveInfoById(
						receiveId);
			Map<Long, PurchaseVO> purchaseList = (Map<Long, PurchaseVO>) (session
					.getAttribute("PURCHASE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			String purchaseInfo[] = splitArrayValues(purchaseDetail, "#@");
			List<PurchaseVO> purchaseVOList = new ArrayList<PurchaseVO>();
			for (String purchaseLine : purchaseInfo) {
				if (purchaseList.containsKey(Long.parseLong(purchaseLine))) {
					PurchaseVO purchaseVO = (PurchaseVO) purchaseList.get(Long
							.parseLong(purchaseLine));
					purchaseVOList.add(purchaseVO);
				}
			}
			ReceiveDetail receiveDetail = null;
			GoodsReturnDetail goodsReturnDetail = null;
			Shelf shelf = null;
			Receive receive = createReceiveNotes();
			if (null != receiveEdit) {
				receive.setCreatedDate(receiveEdit.getCreatedDate());
				receive.setPerson(receiveEdit.getPerson());
				receive.setReceiveNumber(receiveEdit.getReceiveNumber());
			} else {
				User user = (User) sessionObj.get("USER");
				receive.setCreatedDate(Calendar.getInstance().getTime());
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				receive.setPerson(person);
			}
			List<Transaction> transactions = new ArrayList<Transaction>();
			Set<ReceiveDetail> receiveDetailHash = new HashSet<ReceiveDetail>();
			List<Purchase> purchases = new ArrayList<Purchase>();
			PurchaseDetail purchaseDetail = null;
			List<PurchaseDetailVO> purchaseDetailsVOs = null;
			Map<Long, PurchaseDetailVO> purchaseDetailMap = null;
			List<GoodsReturnDetail> goodsReturnDetails = new ArrayList<GoodsReturnDetail>();
			Map<Long, GoodsReturnDetail> returnDetailMap = new HashMap<Long, GoodsReturnDetail>();
			ProductPackageDetail productPackageDetail = null;
			List<ReceiveDetail> detailList = null;
			for (PurchaseVO list : purchaseVOList) {
				detailList = new ArrayList<ReceiveDetail>();
				double totalAvailedQty = 0;
				double totalPurchasedQty = 0;
				if (list.isBatchProduct()
						&& null != list.getPurchaseDetailBatchVO()
						&& list.getPurchaseDetailBatchVO().size() > 0) {
					purchaseDetailsVOs = new ArrayList<PurchaseDetailVO>(
							list.getPurchaseDetailBatchVO());
					purchaseDetailMap = new HashMap<Long, PurchaseDetailVO>();
					for (PurchaseDetailVO vopd : list.getPurchaseDetailVO()) {
						totalPurchasedQty += vopd.getPurchaseQty();
						totalAvailedQty += ((null != vopd.getReceivedQty() ? vopd
								.getReceivedQty() : 0) + (null != vopd
								.getReturnedQty() ? vopd.getReturnedQty() : 0));
						purchaseDetailMap.put(vopd.getPurchaseDetailId(), vopd);
					}
				} else {
					purchaseDetailsVOs = new ArrayList<PurchaseDetailVO>(
							list.getPurchaseDetailVO());
					for (PurchaseDetailVO vopd : list.getPurchaseDetailVO()) {
						totalPurchasedQty += vopd.getPurchaseQty();
						totalAvailedQty += ((null != vopd.getReceivedQty() ? vopd
								.getReceivedQty() : 0) + (null != vopd
								.getReturnedQty() ? vopd.getReturnedQty() : 0));
					}
				}
				for (PurchaseDetailVO detail : purchaseDetailsVOs) {
					purchaseDetail = new PurchaseDetail();
					BeanUtils.copyProperties(purchaseDetail, detail);
					receiveDetail = new ReceiveDetail();
					receiveDetail.setPurchase(list);
					shelf = new Shelf();
					shelf.setShelfId(detail.getShelfId());
					receiveDetail.setShelf(shelf);
					receiveDetail.setProduct(detail.getProduct());
					receiveDetail.setUnitRate(detail.getUnitRate());
					receiveDetail.setDescription(detail.getDescription());
					receiveDetail.setReceiveQty(detail.getQuantity());
					receiveDetail.setPackageUnit(detail.getQuantity());
					receiveDetail.setBatchNumber(null != detail
							.getBatchNumber()
							&& !("").equals(detail.getBatchNumber()) ? detail
							.getBatchNumber() : null);
					if (null != detail.getPackageDetailId()
							&& detail.getPackageDetailId() > 0) {
						productPackageDetail = new ProductPackageDetail();
						productPackageDetail.setProductPackageDetailId(detail
								.getPackageDetailId());
						receiveDetail
								.setProductPackageDetail(productPackageDetail);
						receiveDetail.setPackageUnit(detail.getQuantity());
						receiveDetail.setReceiveQty(receiveBL
								.getPurchaseBL()
								.getPackageConversionBL()
								.getPackageBaseQuantity(
										detail.getPackageDetailId(),
										detail.getQuantity()));
					}
					receiveDetail.setPurchaseDetail(purchaseDetail);
					receiveDetail
							.setProductExpiry((null != detail.getExpiryDate() && !("")
									.equals(detail.getExpiryDate())) ? DateFormat
									.convertStringToDate(detail.getExpiryDate())
									: null);
					if (list.isBatchProduct()) {
						if (!returnDetailMap.containsKey(detail
								.getPurchaseDetailId())) {
							PurchaseDetailVO tempDetailVO = new PurchaseDetailVO();
							tempDetailVO = purchaseDetailMap.get(detail
									.getPurchaseDetailId());
							if (null != tempDetailVO.getReturnQty()
									&& !("").equals(tempDetailVO.getReturnQty())
									&& tempDetailVO.getReturnQty() > 0) {
								goodsReturnDetail = new GoodsReturnDetail();
								goodsReturnDetail.setPurchase(list);
								if (receiveDetail.getReceiveQty() > 0) {
									goodsReturnDetail
											.setReceiveDetail(receiveDetail);
									goodsReturnDetail
											.setBatchNumber(receiveDetail
													.getBatchNumber());
									goodsReturnDetail
											.setProductExpiry(receiveDetail
													.getProductExpiry());
								} else {
									goodsReturnDetail
											.setPurchaseDetail(purchaseDetail);
									goodsReturnDetail
											.setBatchNumber(purchaseDetail
													.getBatchNumber());
									goodsReturnDetail
											.setProductExpiry(purchaseDetail
													.getProductExpiry());
								}
								goodsReturnDetail.setPackageUnit(detail
										.getReturnQty());
								if (null != detail.getPackageDetailId()
										&& detail.getPackageDetailId() > 0) {
									goodsReturnDetail.setPackageUnit(detail
											.getReturnQty());
									goodsReturnDetail
											.setReturnQty(receiveBL
													.getPurchaseBL()
													.getPackageConversionBL()
													.getPackageBaseQuantity(
															detail.getPackageDetailId(),
															detail.getReturnQty()));
									goodsReturnDetail
											.setProductPackageDetail(productPackageDetail);
								} else {
									goodsReturnDetail.setReturnQty(detail
											.getReturnQty());
									goodsReturnDetail.setPackageUnit(detail
											.getReturnQty());
								}
								goodsReturnDetail.setProduct(detail
										.getProduct());
								goodsReturnDetails.add(goodsReturnDetail);
								totalAvailedQty += detail.getPackageUnit();
								returnDetailMap.put(
										detail.getPurchaseDetailId(),
										goodsReturnDetail);
							}
						}
					} else {
						if (null != detail.getReturnQty()
								&& !("").equals(detail.getReturnQty())
								&& detail.getReturnQty() > 0) {
							goodsReturnDetail = new GoodsReturnDetail();
							goodsReturnDetail.setPurchase(list);
							if (receiveDetail.getReceiveQty() > 0) {
								goodsReturnDetail
										.setReceiveDetail(receiveDetail);
								goodsReturnDetail.setBatchNumber(receiveDetail
										.getBatchNumber());
								goodsReturnDetail
										.setProductExpiry(receiveDetail
												.getProductExpiry());
							} else {
								goodsReturnDetail
										.setPurchaseDetail(purchaseDetail);
								goodsReturnDetail.setBatchNumber(purchaseDetail
										.getBatchNumber());
								goodsReturnDetail
										.setProductExpiry(purchaseDetail
												.getProductExpiry());
							}
							if (null != detail.getPackageDetailId()
									&& detail.getPackageDetailId() > 0) {
								goodsReturnDetail.setPackageUnit(detail
										.getReturnQty());
								goodsReturnDetail.setReturnQty(receiveBL
										.getPurchaseBL()
										.getPackageConversionBL()
										.getPackageBaseQuantity(
												detail.getPackageDetailId(),
												detail.getReturnQty()));
								goodsReturnDetail
										.setProductPackageDetail(productPackageDetail);
							} else {
								goodsReturnDetail.setReturnQty(detail
										.getReturnQty());
								goodsReturnDetail.setPackageUnit(detail
										.getReturnQty());
							}
							goodsReturnDetail.setProduct(detail.getProduct());
							goodsReturnDetails.add(goodsReturnDetail);
							totalAvailedQty += detail.getReturnQty();
						}
					}
					detailList.add(receiveDetail);
					totalAvailedQty += receiveDetail.getPackageUnit();
				}
				List<ReceiveDetail> receiveDetails = new ArrayList<ReceiveDetail>(
						detailList);
				for (ReceiveDetail detail : detailList) {
					if (detail.getReceiveQty() <= 0)
						receiveDetails.remove(detail);
				}
				boolean saveReceipt = true;
				if (null != receiveDetails && receiveDetails.size() > 0) {
					for (ReceiveDetail rt : receiveDetails) {
						if (null == rt.getShelf()
								|| null == rt.getShelf().getShelfId()
								|| (int) rt.getShelf().getShelfId() == 0) {
							saveReceipt = false;
							break;
						}
					}
				}
				if (!saveReceipt) {
					sqlReturnMessage = "Please select store for receive.";
					LOGGER.info("Module: Accounts : Method: saveReceipts: Action error due to store not select");
					return SUCCESS;
				}
				receiveDetailHash.addAll(receiveDetails);
				transactions.addAll(receiveBL.createGRNTransaction(receive,
						receiveDetails, receiveBL.getTransactionBL()
								.getCategory("Good Receiving Note (GRN)")));
				if (totalPurchasedQty == totalAvailedQty) {
					Purchase purchase = new Purchase();
					BeanUtils.copyProperties(purchase, list);
					purchase.setPurchaseId(list.getPurchaseId());
					purchase.setPurchaseNumber(list.getPurchaseNumber());
					purchase.setCurrency(list.getCurrency());
					purchase.setDate(list.getDate());
					purchase.setDescription(list.getDescription());
					purchase.setExpiryDate(list.getExpiryDate());
					purchase.setSupplier(list.getSupplier());
					purchase.setQuotation(list.getQuotation());
					purchase.setContinuousPurchase(list.getContinuousPurchase());
					purchase.setImplementation(receive.getImplementation());
					purchase.setRequisition(list.getRequisition());
					purchase.setExchangeRate(list.getExchangeRate());
					purchase.setStatus(2);
					purchase.setIsApprove(list.getIsApprove());
					purchases.add(purchase);
				} else {
					Purchase purchase = new Purchase();
					purchase.setPurchaseId(list.getPurchaseId());
					purchase.setPurchaseNumber(list.getPurchaseNumber());
					purchase.setCurrency(list.getCurrency());
					purchase.setDate(list.getDate());
					purchase.setDescription(list.getDescription());
					purchase.setExpiryDate(list.getExpiryDate());
					purchase.setSupplier(list.getSupplier());
					purchase.setQuotation(list.getQuotation());
					purchase.setContinuousPurchase(list.getContinuousPurchase());
					purchase.setImplementation(receive.getImplementation());
					purchase.setRequisition(list.getRequisition());
					purchase.setExchangeRate(list.getExchangeRate());
					purchase.setStatus(1);
					purchase.setIsApprove(list.getIsApprove());
					purchases.add(purchase);
				}
			}
			if (null != receiveDetailHash && receiveDetailHash.size() > 0) {
				String unifiedPrice = "";
				if (session.getAttribute("unified_price") != null)
					unifiedPrice = session.getAttribute("unified_price")
							.toString();
				receive.setReceiveDetails(receiveDetailHash);
				if (null != goodsReturnDetails && goodsReturnDetails.size() > 0)
					receiveBL.saveReceiptDetails(
							receive,
							createGoodsReturn(goodsReturnDetails,
									receive.getSupplier(), receive),
							transactions, purchases, unifiedPrice, receiveEdit);

				else
					receiveBL.saveReceiptDetails(receive, null, transactions,
							purchases, unifiedPrice, receiveEdit);

				// Receive stock update process for update the stock column in
				// receive detail
				receiveEdit = receiveBL.getReceiveService().getReceiveInfoById(
						receive.getReceiveId());
				List<Stock> stocks = receiveBL
						.getStockBL()
						.getStockService()
						.getStockReport(
								receiveBL.getPurchaseBL().getImplemenationId(),
								0, 0);
				Map<String, Stock> stockMaps = new HashMap<String, Stock>();
				for (Stock stock : stocks) {
					if (stock.getShelf() != null)
						stockMaps.put(stock.getProduct().getProductId() + "_"
								+ stock.getShelf().getShelfId(), stock);
				}
				Stock stock = null;
				for (ReceiveDetail receiveDetail2 : receiveEdit
						.getReceiveDetails()) {
					stock = stockMaps.get(receiveDetail2.getProduct()
							.getProductId()
							+ "_"
							+ receiveDetail2.getShelf().getShelfId());
					if (stock != null)
						receiveDetail2.setStock(stock.getQuantity());
				}
				receiveBL.getReceiveService().saveReceipts(
						receiveEdit,
						new ArrayList<ReceiveDetail>(receiveEdit
								.getReceiveDetails()));
				// Receive detail stock update ends here
			} else if (null != goodsReturnDetails
					&& goodsReturnDetails.size() > 0) {
				receiveBL.getGoodsReturnBL().saveReturns(
						createGoodsReturn(goodsReturnDetails,
								receive.getSupplier(), null), purchases);
			}
			session.removeAttribute("STORE_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PURCHASE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));

			sqlReturnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: saveReceipts: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "Failure";
			LOGGER.info("Module: Accounts : Method: saveReceipts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private GoodsReturn createGoodsReturn(
			List<GoodsReturnDetail> goodsReturnList, Supplier supplier,
			Receive receive) throws Exception {
		GoodsReturn goodsReturn = new GoodsReturn();
		goodsReturn.setReturnNumber(SystemBL.getReferenceStamp(
				GoodsReturn.class.getName(), receiveBL.getPurchaseBL()
						.getImplemenationId()));
		goodsReturn.setDate(DateFormat.convertStringToDate(receiveDate));
		goodsReturn.setSupplier(supplier);
		goodsReturn.setGoodsReturnDetails(new HashSet<GoodsReturnDetail>(
				goodsReturnList));
		goodsReturn.setImplementation(receiveBL.getPurchaseBL()
				.getImplemenationId());
		goodsReturn.setReceive(receive);
		goodsReturn.setIsDebit(false);
		return goodsReturn;
	}

	private Receive createReceiveNotes() throws Exception {
		Receive receive = new Receive();
		receive.setDescription(description);
		receive.setReferenceNo(referenceNo);
		receive.setDeliveryNo(deliveryNo);
		receive.setReference1(reference1);
		receive.setReference2(reference2);
		if (receiveId == 0)
			receive.setReceiveNumber(SystemBL.getReferenceStamp(Receive.class
					.getName(), receiveBL.getPurchaseBL().getImplemenationId()));
		Supplier supplier = new Supplier();
		Combination combination = new Combination();
		combination.setCombinationId(combinationId);
		supplier.setSupplierId(supplierId);
		supplier.setCombination(combination);
		receive.setSupplier(supplier);
		receive.setReceiveDate(DateFormat.convertStringToDate(receiveDate));
		receive.setStatus((byte) 1);
		receive.setImplementation(receiveBL.getPurchaseBL()
				.getImplemenationId());
		return receive;
	}

	private static String[] splitArrayValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public List<ReceiveDetail> receiveToConverter(List<String> receiveList) {
		List<ReceiveDetail> detailList = new ArrayList<ReceiveDetail>();
		for (int i = 0; i < receiveList.size(); i++) {
			detailList.add(convertList(receiveList.get(i)));
		}
		return detailList;
	}

	public ReceiveDetail convertList(String receiveDetail) {
		ReceiveDetail detail = new ReceiveDetail();
		Product prod = new Product();
		String delimeter = ",";
		String receiveArray[] = splitArrayValues(receiveDetail, delimeter);
		prod.setProductId(Long.parseLong(receiveArray[0]));
		detail.setReceiveQty(Double.parseDouble(receiveArray[1]));
		detail.setProduct(prod);
		return detail;
	}

	// to delete receive details
	public String deleteReceive() {
		LOGGER.info("Module: Accounts : Method: deleteReceive");
		try {
			Receive receive = receiveBL.getReceiveService().getReceiveInfoById(
					receiveId);
			Map<Long, Purchase> purchaseMap = new HashMap<Long, Purchase>();
			for (ReceiveDetail rcDt : receive.getReceiveDetails())
				purchaseMap.put(rcDt.getPurchase().getPurchaseId(),
						rcDt.getPurchase());

			List<ReceiveDetail> receiveDetailTmp = receiveBL
					.getReceiveService().getPurchaseDetailWithNonReceive(
							purchaseMap.keySet(), receiveId);
			if (null != receiveDetailTmp && receiveDetailTmp.size() > 0) {
				for (ReceiveDetail rcDt : receiveDetailTmp) {
					if (purchaseMap.containsKey(rcDt.getPurchase()
							.getPurchaseId()))
						purchaseMap.remove(rcDt.getPurchase().getPurchaseId());
				}
			}
			List<Purchase> purchases = null;
			if (null != purchaseMap && purchaseMap.size() > 0) {
				purchases = new ArrayList<Purchase>();
				for (Entry<Long, Purchase> lpo : purchaseMap.entrySet())
					lpo.getValue().setStatus((int) 1);
				purchases.addAll(purchaseMap.values());
			}
			receiveBL.deleteReceiveFullDetails(receive, purchases);
			LOGGER.info("Module: Accounts : Method: deleteReceive: Action Success");
			ServletActionContext.getRequest()
					.setAttribute("success", "success");
			return SUCCESS;
		} catch (Exception ex) {
			ServletActionContext.getRequest().setAttribute("error", "error");
			LOGGER.info("Module: Accounts : Method: deleteReceive: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String receieNotesPDFPrintBarcode() {
		try {
			LOG.info("Inside Module: Accounts : Method: receieNotesPDFPrintBarcode");
			Receive receive = receiveBL.getReceiveService()
					.getReceiveDetailsBarcode(receiveId);
			stocks = new ArrayList<StockVO>();
			barcodeDetails = new ArrayList<StockVO>();
			stockVO = new StockVO();
			List<StockVO> tempStocks = new ArrayList<StockVO>();
			Shelf shelf = null;
			for (ReceiveDetail receiveDetail : receive.getReceiveDetails()) {
				if (null != receiveDetail.getShelf()) {
					shelf = receiveBL
							.getStockBL()
							.getStoreBL()
							.getStoreService()
							.getStoreByShelfId(
									receiveDetail.getShelf().getShelfId());
					StockVO storeDetail = new StockVO();
					storeDetail.setStoreName(shelf.getShelf().getAisle()
							.getStore().getStoreName()
							+ "->");
					storeDetail.setAisleName(shelf.getShelf().getAisle()
							.getSectionName()
							+ "->");
					storeDetail.setRackName(shelf.getShelf().getName() + "->");
					storeDetail.setShelfName(shelf.getName());
					tempStocks.add(storeDetail);
				}
				for (int i = 0; i < receiveDetail.getReceiveQty(); i++) {
					tempStocks
							.add(setProductDetails(receiveDetail.getProduct()));
				}
			}
			barcodeDetails.addAll(tempStocks);
			stockVO.setBarCodeDetails(barcodeDetails);
			stocks.add(stockVO);
			LOG.info("Module: Accounts : Method: receieNotesPDFPrintBarcode Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: receieNotesPDFPrintBarcode: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private StockVO setProductDetails(Product product) throws Exception {
		StockVO stockVO = new StockVO();
		if (product.getProductName().length() > 20)
			stockVO.setProductName(product.getProductName().substring(0, 20));
		else
			stockVO.setProductName(product.getProductName());

		stockVO.setProductCode(product.getBarCode());
		BitMatrix bitMatrix = new Code128Writer().encode(product.getBarCode(),
				BarcodeFormat.CODE_128, 240, 70);
		BufferedImage bi = MatrixToImageWriter.toBufferedImage(bitMatrix);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "png", baos);
		baos.flush();
		stockVO.setImage(bi);
		baos.close();
		return stockVO;
	}

	// to discard receipts
	public String discardReceipts() {
		LOGGER.info("Module: Accounts : Method: Module");
		try {
			LOGGER.info("Module: Accounts : Method: discardReceipts: Action Success");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("STORE_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("PURCHASE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: Module: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showAllReceives() {
		LOGGER.info("Module: Accounts : Method: showAllReceives");
		try {
			receiveList = this
					.getReceiveBL()
					.getReceiveService()
					.getAllReceiveNotes(
							receiveBL.getPurchaseBL().getImplemenationId());
			ServletActionContext.getRequest().setAttribute("RECEIVE_ALL",
					receiveList);
			ServletActionContext.getRequest().setAttribute("PAGE", showPage);
			LOGGER.info("Module: Accounts : Method: showAllReceives: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: Module: showAllReceives throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showReceiveDetailList() {
		try {
			LOGGER.info("Module: Accounts : Method: showReceiveDetailList");
			detailList = this.getReceiveBL().getReceiveService()
					.getReceiveLineDetails(receiveId);
			ServletActionContext.getRequest().setAttribute(
					"RECEIVE_DETAIL_LIST", detailList);
			LOGGER.info("Module: Accounts : Method: showReceiveDetailList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: Module: showReceiveDetailList throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String updateProductStock() {
		try {
			LOGGER.info("Module: Accounts : Method: updateProductStock");
			List<ReceiveDetail> receiveDetails = receiveBL.getReceiveService()
					.getAllReceiveDetails(
							receiveBL.getPurchaseBL().getImplemenationId());
			if (null != receiveDetails && receiveDetails.size() > 0) {
				Stock stock = null;
				Map<String, Stock> stockMap = new HashMap<String, Stock>();
				String key = "";
				for (ReceiveDetail receiveDetail : receiveDetails) {
					stock = new Stock();
					key = receiveDetail
							.getProduct()
							.getProductId()
							.toString()
							.concat("-")
							.concat(receiveDetail.getShelf().getShelfId()
									.toString());
					if (stockMap.containsKey(key)) {
						stock = stockMap.get(key);
						stock.setQuantity(stock.getQuantity()
								+ receiveDetail.getReceiveQty());
					} else {
						stock.setProduct(receiveDetail.getProduct());
						stock.setQuantity(receiveDetail.getReceiveQty());
						stock.setShelf(receiveDetail.getShelf());
						stock.setUnitRate(receiveDetail.getUnitRate());
						stock.setImplementation(receiveBL.getPurchaseBL()
								.getImplemenationId());
						stock.setStore(receiveDetail.getShelf().getShelf()
								.getAisle().getStore());
					}
					stockMap.put(key, stock);
				}
				receiveBL.getStockBL().updateStockDetails(
						new ArrayList<Stock>(stockMap.values()), null);
			}
			sqlReturnMessage = "Stock updated successfully.";
			LOGGER.info("Module: Accounts : Method: updateProductStock: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: Module: updateProductStock throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// updateStockBasedOnTheReceivedPeriod
	public String updateStockBasedOnTheReceivedPeriod() {
		try {
			LOG.info("Module: Accounts : Method: updateStockBasedOnTheReceivedPeriod: Inside Action");
			Date fromDatee = null;
			Date toDatee = null;
			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}
			List<Receive> receives = receiveBL.getReceiveService()
					.getFilteredReceiveNotes(
							receiveBL.getPurchaseBL().getImplemenationId(),
							toDatee, fromDatee, null, null, null);
			Map<String, Stock> stockMap = new HashMap<String, Stock>();
			for (Receive receive : receives) {

				List<ReceiveDetail> receiveDetails = receiveBL
						.getReceiveService().getReceiveLineDetails(
								receive.getReceiveId());
				if (null != receiveDetails && receiveDetails.size() > 0) {
					Stock stock = null;
					String key = "";
					for (ReceiveDetail receiveDetail : receiveDetails) {
						stock = new Stock();
						key = receiveDetail
								.getProduct()
								.getProductId()
								.toString()
								.concat("-")
								.concat(receiveDetail.getShelf().getShelfId()
										.toString());
						if (stockMap.containsKey(key)) {
							stock = stockMap.get(key);
							stock.setQuantity(stock.getQuantity()
									+ receiveDetail.getReceiveQty());
						} else {
							stock.setProduct(receiveDetail.getProduct());
							stock.setQuantity(receiveDetail.getReceiveQty());
							stock.setShelf(receiveDetail.getShelf());
							stock.setUnitRate(receiveDetail.getUnitRate());
							stock.setImplementation(receiveBL.getPurchaseBL()
									.getImplemenationId());
							stock.setStore(receiveDetail.getShelf().getShelf()
									.getAisle().getStore());
						}
						stockMap.put(key, stock);
					}

				}
			}

			receiveBL.getStockBL().updateStockDetails(
					new ArrayList<Stock>(stockMap.values()), null);
			sqlReturnMessage = "SUCCESS";
			LOG.info("Module: Accounts : Method: updateStockBasedOnTheReceivedPeriod Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure";
			ex.printStackTrace();
			LOG.info("Module: Accounts : Method: updateStockBasedOnTheReceivedPeriod: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Getters & setters
	public long getReceiveId() {
		return receiveId;
	}

	public void setReceiveId(long receiveId) {
		this.receiveId = receiveId;
	}

	public String getReceiveNumber() {
		return receiveNumber;
	}

	public void setReceiveNumber(String receiveNumber) {
		this.receiveNumber = receiveNumber;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public long getReceiveDetailId() {
		return receiveDetailId;
	}

	public void setReceiveDetailId(long receiveDetailId) {
		this.receiveDetailId = receiveDetailId;
	}

	public double getReceiveQty() {
		return receiveQty;
	}

	public void setReceiveQty(double receiveQty) {
		this.receiveQty = receiveQty;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Receive> getReceiveList() {
		return receiveList;
	}

	public void setReceiveList(List<Receive> receiveList) {
		this.receiveList = receiveList;
	}

	public List<ReceiveDetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ReceiveDetail> detailList) {
		this.detailList = detailList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public String getItemNature() {
		return itemNature;
	}

	public void setItemNature(String itemNature) {
		this.itemNature = itemNature;
	}

	public ReceiveBL getReceiveBL() {
		return receiveBL;
	}

	public void setReceiveBL(ReceiveBL receiveBL) {
		this.receiveBL = receiveBL;
	}

	public String getReceiveLines() {
		return receiveLines;
	}

	public void setReceiveLines(String receiveLines) {
		this.receiveLines = receiveLines;
	}

	public String getReceiveDetails() {
		return receiveDetails;
	}

	public void setReceiveDetails(String receiveDetails) {
		this.receiveDetails = receiveDetails;
	}

	public String getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}

	public long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getPurchaseReturns() {
		return purchaseReturns;
	}

	public void setPurchaseReturns(String purchaseReturns) {
		this.purchaseReturns = purchaseReturns;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getPurchaseDetail() {
		return purchaseDetail;
	}

	public void setPurchaseDetail(String purchaseDetail) {
		this.purchaseDetail = purchaseDetail;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getReceiveDetail() {
		return receiveDetail;
	}

	public void setReceiveDetail(String receiveDetail) {
		this.receiveDetail = receiveDetail;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public StockVO getStockVO() {
		return stockVO;
	}

	public void setStockVO(StockVO stockVO) {
		this.stockVO = stockVO;
	}

	public List<StockVO> getBarcodeDetails() {
		return barcodeDetails;
	}

	public void setBarcodeDetails(List<StockVO> barcodeDetails) {
		this.barcodeDetails = barcodeDetails;
	}

	public List<StockVO> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockVO> stocks) {
		this.stocks = stocks;
	}

	public String getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(String receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public boolean isContinuousPurchase() {
		return continuousPurchase;
	}

	public void setContinuousPurchase(boolean continuousPurchase) {
		this.continuousPurchase = continuousPurchase;
	}

	public long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public String getDeliveryNo() {
		return deliveryNo;
	}

	public void setDeliveryNo(String deliveryNo) {
		this.deliveryNo = deliveryNo;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getPurchaseDetails() {
		return purchaseDetails;
	}

	public void setPurchaseDetails(String purchaseDetails) {
		this.purchaseDetails = purchaseDetails;
	}

	public boolean isBatchProduct() {
		return batchProduct;
	}

	public void setBatchProduct(boolean batchProduct) {
		this.batchProduct = batchProduct;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getReference2() {
		return reference2;
	}

	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
}
