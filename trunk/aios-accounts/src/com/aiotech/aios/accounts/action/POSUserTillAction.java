package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.POSUserTillVO;
import com.aiotech.aios.accounts.service.bl.POSUserTillBL;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class POSUserTillAction extends ActionSupport {

	private static final long serialVersionUID = 987734309835636945L;
	private static final Logger log = LogManager
			.getLogger(POSUserTillAction.class);
	private List<Object> aaData;
	private POSUserTillBL pOSUserTillBL;
	@SuppressWarnings("unused")
	private Implementation implementation;
	private String posUserTillJson;

	private Long posUserTillId;
	private Long personId;
	private Long storeId;
	private String tillNumber;
	private String isActive;
	private String returnMessage;
	private String kitchenPrinter;
	private String barPrinter;
	private POSUserTillVO pOSUserTillVOJson;
	private Long supervisorId;
	private Long recordId;

	public String returnSuccess() {
		try {
			log.info("Inside Method: returnSuccess() Action Success");
			CommentVO comment = new CommentVO();
			if (recordId != null && recordId > 0 && posUserTillId > 0) {
				POSUserTill posUserTill = pOSUserTillBL.getpOSUserTillService()
						.getPosUserTillInformation(posUserTillId);
				comment.setRecordId(posUserTill.getPosUserTillId());
				comment.setUseCase(POSUserTill.class.getName());
				comment = pOSUserTillBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			} else {
				posUserTillId = 0l;
			}
			recordId = null;
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPosUserTillList() {
		try {
			aaData = new ArrayList<Object>();
			recordId = null;
			List<POSUserTill> posuserTills = pOSUserTillBL
					.getpOSUserTillService().getAllPosUserTill(
							getImplementation());
			if (null != posuserTills && posuserTills.size() > 0) {
				POSUserTillVO vO = null;
				for (POSUserTill list : posuserTills) {
					vO = new POSUserTillVO();
					vO.setPosUserTillId(list.getPosUserTillId());
					if (list.getPersonByPersonId() != null) {
						vO.setPersonName(list.getPersonByPersonId()
								.getFirstName()
								+ " "
								+ list.getPersonByPersonId().getLastName()
								+ " ["
								+ list.getPersonByPersonId().getPersonNumber()
								+ "]");
						vO.setPersonNumber(list.getPersonByPersonId()
								.getPersonNumber());
					}

					if (list.getPersonBySupervisor() != null) {
						vO.setSupervisorName(list.getPersonBySupervisor()
								.getFirstName()
								+ " "
								+ list.getPersonBySupervisor().getLastName()
								+ " ["
								+ list.getPersonBySupervisor()
										.getPersonNumber() + "]");
						vO.setSupervisorNumber(list.getPersonBySupervisor()
								.getPersonNumber());

						vO.setSupervisorId(list.getPersonBySupervisor()
								.getPersonId());
					}
					if (list.getStore() != null) {
						vO.setStoreName(list.getStore().getStoreName());
					}
					vO.setTillNumber(list.getTillNumber());
					if (list.getIsActive() != null
							&& list.getIsActive() == true)
						vO.setStatusView("YES");
					else
						vO.setStatusView("NO");
					aaData.add(vO);
				}
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String showPOSuserApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showPOSuserApproval");
			posUserTillId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showPOSuserApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showPOSuserApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showPOSuserRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showPOSuserRejectEntry");
			posUserTillId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showPOSuserRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showPOSuserRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showPOSUsertTill() {
		try {
			POSUserTill posUserTill = null;
			if (null != posUserTillId) {
				posUserTill = pOSUserTillBL.getpOSUserTillService()
						.getPosUserTillInformation(posUserTillId);

				POSUserTillVO vO = null;
				vO = new POSUserTillVO();
				vO.setPosUserTillId(posUserTill.getPosUserTillId());
				if (posUserTill.getPersonByPersonId() != null) {
					vO.setPersonName(posUserTill.getPersonByPersonId()
							.getFirstName()
							+ " "
							+ posUserTill.getPersonByPersonId().getLastName()
							+ " ["
							+ posUserTill.getPersonByPersonId()
									.getPersonNumber() + "]");
					vO.setPersonNumber(posUserTill.getPersonByPersonId()
							.getPersonNumber());

					vO.setPersonId(posUserTill.getPersonByPersonId()
							.getPersonId());
				}

				if (posUserTill.getPersonBySupervisor() != null) {
					vO.setSupervisorName(posUserTill.getPersonBySupervisor()
							.getFirstName()
							+ " "
							+ posUserTill.getPersonBySupervisor().getLastName()
							+ " ["
							+ posUserTill.getPersonBySupervisor()
									.getPersonNumber() + "]");
					vO.setSupervisorNumber(posUserTill.getPersonBySupervisor()
							.getPersonNumber());

					vO.setSupervisorId(posUserTill.getPersonBySupervisor()
							.getPersonId());
				}
				vO.setBarPrinter(posUserTill.getBarPrinter());
				vO.setKitchenPrinter(posUserTill.getKitchenPrinter());

				if (posUserTill.getStore() != null) {
					vO.setStoreName(posUserTill.getStore().getStoreName());
					vO.setStoreId(posUserTill.getStore().getStoreId());
				}
				vO.setTillNumber(posUserTill.getTillNumber());
				if (posUserTill.getIsActive() != null
						&& posUserTill.getIsActive() == true)
					vO.setStatusView("YES");
				else
					vO.setStatusView("NO");

				vO.setIsActive(posUserTill.getIsActive());

				pOSUserTillVOJson = vO;

				CommentVO comment = new CommentVO();
				if (recordId != null && posUserTill != null
						&& posUserTill.getPosUserTillId() != 0 && recordId > 0) {
					comment.setRecordId(posUserTill.getPosUserTillId());
					comment.setUseCase(POSUserTill.class.getName());
					comment = pOSUserTillBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String savePOSUsertTill() {
		returnMessage = "SUCCESS";
		try {
			POSUserTill posUserTill = new POSUserTill();
			if (posUserTillId != null && posUserTillId > 0)
				posUserTill.setPosUserTillId(posUserTillId);

			if (personId != null) {
				Person person = new Person();
				person.setPersonId(personId);
				posUserTill.setPersonByPersonId(person);

			}

			if (supervisorId != null) {
				Person person = new Person();
				person.setPersonId(supervisorId);
				posUserTill.setPersonBySupervisor(person);

			}

			if (storeId != null) {
				Store store = new Store();
				store.setStoreId(storeId);
				posUserTill.setStore(store);

			}
			posUserTill.setKitchenPrinter(null != kitchenPrinter
					&& !("").equals(kitchenPrinter.trim()) ? kitchenPrinter
					.trim() : null);
			posUserTill.setBarPrinter(null != barPrinter
					&& !("").equals(barPrinter.trim()) ? barPrinter.trim()
					: null);
			posUserTill.setTillNumber(tillNumber);
			if (isActive != null && isActive.equalsIgnoreCase("true"))
				posUserTill.setIsActive(true);
			else
				posUserTill.setIsActive(false);
			posUserTill.setImplementation(getImplementation());
			pOSUserTillBL.savePOSUserTill(posUserTill);
		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Save failured";
		}
		return SUCCESS;
	}

	public String deletePOSUsertTill() {
		returnMessage = "SUCCESS";
		try {
			POSUserTill posUserTill = null;
			if (posUserTillId != null) {
				posUserTill = pOSUserTillBL.getpOSUserTillService()
						.getPosUserTillInformation(posUserTillId);
				pOSUserTillBL.deletePOSUserTill(posUserTill);

			}

		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Delete failured";
		}
		return SUCCESS;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public POSUserTillBL getpOSUserTillBL() {
		return pOSUserTillBL;
	}

	public void setpOSUserTillBL(POSUserTillBL pOSUserTillBL) {
		this.pOSUserTillBL = pOSUserTillBL;
	}

	public String getPosUserTillJson() {
		return posUserTillJson;
	}

	public void setPosUserTillJson(String posUserTillJson) {
		this.posUserTillJson = posUserTillJson;
	}

	public Long getPosUserTillId() {
		return posUserTillId;
	}

	public void setPosUserTillId(Long posUserTillId) {
		this.posUserTillId = posUserTillId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getTillNumber() {
		return tillNumber;
	}

	public void setTillNumber(String tillNumber) {
		this.tillNumber = tillNumber;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public POSUserTillVO getpOSUserTillVOJson() {
		return pOSUserTillVOJson;
	}

	public void setpOSUserTillVOJson(POSUserTillVO pOSUserTillVOJson) {
		this.pOSUserTillVOJson = pOSUserTillVOJson;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Long getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Long supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getKitchenPrinter() {
		return kitchenPrinter;
	}

	public void setKitchenPrinter(String kitchenPrinter) {
		this.kitchenPrinter = kitchenPrinter;
	}

	public String getBarPrinter() {
		return barPrinter;
	}

	public void setBarPrinter(String barPrinter) {
		this.barPrinter = barPrinter;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
