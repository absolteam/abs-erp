package com.aiotech.aios.accounts.action;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.vo.AssetVO;
import com.aiotech.aios.accounts.service.bl.AssetCreationBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.AssetCategory;
import com.aiotech.aios.common.to.Constants.Accounts.AssetColor;
import com.aiotech.aios.common.to.Constants.Accounts.AssetCondition;
import com.aiotech.aios.common.to.Constants.Accounts.DepreciationConvention;
import com.aiotech.aios.common.to.Constants.Accounts.DepreciationMethod;
import com.aiotech.aios.common.to.Constants.Accounts.PlateType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class AssetCreationAction extends ActionSupport {

	private static final long serialVersionUID = -3418714257360366327L;

	private static final Logger log = Logger
			.getLogger(AssetCreationAction.class);

	// Dependency
	private AssetCreationBL assetCreationBL;

	// Variables
	private long assetCreationId;
	private long productId;
	private long assetSubClass;
	private long assetType;
	private long category;
	private long ownerId;
	private double scrapValue;
	private double decliningVariance;
	private Integer rowId;
	private int usefulLife;
	private byte assetClass;
	private byte assetColor;
	private byte assetCondition;
	private byte plateType;
	private byte plateColor;
	private byte depreciationMethod;
	private byte depreciationConvention;
	private String ownerType;
	private String assetNumber;
	private String assetName;
	private String manufacturer;
	private String brand;
	private String assetMake;
	private String assetModel;
	private String serialNumber;
	private String description;
	private String yearMake;
	private String assetVin;
	private String plateNumber;
	private String tcNumber;
	private String assetDetails;
	private String commenceDate;
	private String returnMessage;
	private List<Object> aaData;
	private Object assetValueDetail;
	private Long recordId;

	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show All Asset JSON list
	public String showAssetJsonList() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetJsonList");
			aaData = assetCreationBL.showAssetJsonList();
			log.info("Module: Accounts : Method: showAssetJsonList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetJsonList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show All Asset JSON list
	public String showAssetCheckOutList() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckOutList");
			aaData = assetCreationBL.showAssetCheckOutList();
			log.info("Module: Accounts : Method: showAssetCheckOutList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckOutList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Sub Class
	public String showAssetSubClass() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetSubClass");
			String code = "";
			if (("Tangible").equalsIgnoreCase(Constants.Accounts.AssetCategory
					.get(assetClass).name()))
				code = "SUB_CLASS_TANAGIBLE";
			else
				code = "SUB_CLASS_INTANAGIBLE";
			List<LookupDetail> assetSubClass = assetCreationBL
					.getLookupMasterBL().getActiveLookupDetails(code, true);
			LookupDetail lookupDetail = null;
			aaData = new ArrayList<Object>();
			for (LookupDetail list : assetSubClass) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(list.getLookupDetailId());
				lookupDetail.setDisplayName(list.getDisplayName());
				aaData.add(lookupDetail);
			}
			log.info("Module: Accounts : Method: showAssetSubClass: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetSubClass Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetCreationApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCreationApproval");
			assetCreationId = recordId;
			showAssetCreationEntry();
			log.info("Module: Accounts : Method: showAssetCreationApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCreationApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetCreationRejectEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCreationApproval");
			assetCreationId = recordId;
			showAssetCreationEntry();
			log.info("Module: Accounts : Method: showAssetCreationRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCreationRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Creation Entry Screen
	public String showAssetCreationEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCreationEntry");
			if (assetCreationId > 0) {
				AssetCreation assetCreation = assetCreationBL
						.getAssetCreationService().getAssetInformation(
								assetCreationId);
				AssetVO assetVO = convertEntityVO(assetCreation);
				ServletActionContext.getRequest().setAttribute(
						"ASSET_CREATION", assetVO);
				String code = "";
				if (("Tangible")
						.equalsIgnoreCase(Constants.Accounts.AssetCategory.get(
								assetCreation.getAssetClass()).name()))
					code = "SUB_CLASS_TANAGIBLE";
				else
					code = "SUB_CLASS_INTANAGIBLE";
				List<LookupDetail> assetSubClass = assetCreationBL
						.getLookupMasterBL()
						.getActiveLookupDetails(code, false);
				ServletActionContext.getRequest().setAttribute(
						"ASSET_SUB_CLASS", assetSubClass);
				CommentVO comment = new CommentVO();
				if (recordId != null && assetCreation != null
						&& assetCreation.getAssetCreationId() != 0
						&& recordId > 0) {
					comment.setRecordId(assetCreation.getAssetCreationId());
					comment.setUseCase(AssetCreation.class.getName());
					comment = assetCreationBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else {
				assetNumber = assetCreationBL.generateAssetNumber();
			}
			List<LookupDetail> assetType = assetCreationBL.getLookupMasterBL()
					.getActiveLookupDetails("ASSET_TYPE", true);
			List<LookupDetail> costTypes = assetCreationBL.getLookupMasterBL()
					.getActiveLookupDetails("ASSET_COST_TYPE", true);
			List<LookupDetail> assetCategory = assetCreationBL
					.getLookupMasterBL().getActiveLookupDetails(
							"ASSET_CATEGORY", true);

			Map<Byte, AssetCategory> assetClass = new HashMap<Byte, AssetCategory>();
			for (AssetCategory e : EnumSet.allOf(AssetCategory.class)) {
				assetClass.put(e.getCode(), e);
			}
			Map<Byte, AssetColor> assetColor = new HashMap<Byte, AssetColor>();
			for (AssetColor e : EnumSet.allOf(AssetColor.class)) {
				assetColor.put(e.getCode(), e);
			}
			Map<Byte, AssetCondition> assetCondition = new HashMap<Byte, AssetCondition>();
			for (AssetCondition e : EnumSet.allOf(AssetCondition.class)) {
				assetCondition.put(e.getCode(), e);
			}
			Map<Byte, PlateType> plateType = new HashMap<Byte, PlateType>();
			for (PlateType e : EnumSet.allOf(PlateType.class)) {
				plateType.put(e.getCode(), e);
			}
			Map<Byte, String> depreciationMethod = new HashMap<Byte, String>();
			for (DepreciationMethod e : EnumSet.allOf(DepreciationMethod.class)) {
				depreciationMethod
						.put(e.getCode(),
								e.name().equalsIgnoreCase(
										"Declinig_Balance_Percentage") ? "Declinig Balance%"
										: e.name().replaceAll("_", " "));
			}
			Map<Byte, String> convention = new HashMap<Byte, String>();
			for (DepreciationConvention e : EnumSet
					.allOf(DepreciationConvention.class)) {
				convention.put(e.getCode(), e.name().replaceAll("_", " "));
			}
			ServletActionContext.getRequest().setAttribute("COST_TYPE",
					costTypes);
			ServletActionContext.getRequest().setAttribute("ASSET_CLASS",
					assetClass);
			ServletActionContext.getRequest().setAttribute("ASSET_TYPE",
					assetType);
			ServletActionContext.getRequest().setAttribute("ASSET_CATEGORY",
					assetCategory);
			ServletActionContext.getRequest().setAttribute("ASSET_COLOR",
					assetColor);
			ServletActionContext.getRequest().setAttribute("ASSET_CONDITION",
					assetCondition);
			ServletActionContext.getRequest().setAttribute("PLATE_TYPE",
					plateType);
			ServletActionContext.getRequest().setAttribute(
					"DEPRECIATION_METHOD", depreciationMethod);
			ServletActionContext.getRequest().setAttribute("CONVENTION",
					convention);
			log.info("Module: Accounts : Method: showAssetCreationEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCreationEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Value Detail
	public String showAssetValueDetail() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetValueDetail");
			assetValueDetail = assetCreationBL.getAssetValueDetail(
					assetCreationBL.getAssetCreationService()
							.getAssetDetailedInformation(assetCreationId),
					commenceDate);
			log.info("Module: Accounts : Method: showAssetValueDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetValueDetail Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Detail list
	public String showAssetDetailList() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetDetailList");
			List<AssetCreation> assetCreation = assetCreationBL
					.getAssetCreationService().getAllAssets(
							assetCreationBL.getImplementation());
			List<AssetVO> assetVOs = convertEntityVOs(assetCreation);
			ServletActionContext.getRequest().setAttribute("ASSET_INFORMATION",
					assetVOs);
			log.info("Module: Accounts : Method: showAssetDetailList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCreationEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Creation
	public String saveAssetCreation() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetCreation");
			AssetCreation assetCreation = new AssetCreation();
			boolean updateFlag = false;
			assetCreation.setAssetClass(assetClass);
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(assetSubClass);
			assetCreation.setLookupDetailByAssetSubclass(lookupDetail);
			lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(assetType);
			assetCreation.setLookupDetailByAssetType(lookupDetail);
			if (category > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(category);
				assetCreation.setLookupDetailByCategory(lookupDetail);
			}

			if (productId > 0) {
				Product product = new Product();
				product.setProductId(productId);
				assetCreation.setProduct(product);
			}

			assetCreation.setAssetColor(assetColor > 0 ? assetColor : null);
			assetCreation.setAssetCondition(assetCondition > 0 ? assetCondition
					: null);
			assetCreation.setAssetMake(assetMake);
			assetCreation.setAssetModel(assetModel);
			assetCreation.setAssetName(assetName);
			assetCreation.setAssetVin(assetVin);
			assetCreation.setBrand(brand);
			assetCreation.setDescription(description);
			assetCreation.setManufacturer(manufacturer);
			assetCreation.setPlateNumber(plateNumber);
			assetCreation.setSerialNumber(serialNumber);
			assetCreation.setYearMake(yearMake);
			assetCreation.setTcNumber(tcNumber);
			assetCreation.setPlateColor(plateColor > 0 ? plateColor : null);
			assetCreation.setPlateType(plateType > 0 ? plateType : null);
			assetCreation.setDepreciationMethod(depreciationMethod);
			assetCreation
					.setDecliningVariance(decliningVariance > 0 ? decliningVariance
							: null);
			assetCreation.setUsefulLife(usefulLife);
			assetCreation.setConvention(depreciationConvention);
			assetCreation.setScrapValue(scrapValue > 0 ? scrapValue : null);
			assetCreation.setCommenceDate(DateFormat
					.convertStringToDate(commenceDate));

			if (null != ownerType && ownerType.equals("P") && ownerId > 0) {
				Person person = new Person();
				person.setPersonId(ownerId);
				assetCreation.setPerson(person);
			} else if (null != ownerType && ownerType.equals("C")
					&& ownerId > 0) {
				Company company = new Company();
				company.setCompanyId(ownerId);
				assetCreation.setCompany(company);
			}
			List<AssetDetail> assetDetailList = null;
			List<AssetDetail> deleteAssetDetails = null;
			if (null != assetDetails && !("").equals(assetDetails)) {
				assetDetailList = new ArrayList<AssetDetail>();
				String[] assetCreationDetails = splitValues(assetDetails, "#@");
				for (String creationDetail : assetCreationDetails) {
					assetDetailList.add(addAssetCreationDetail(creationDetail));
				}
			}

			if (assetCreationId > 0) {
				updateFlag = true;
				assetCreation.setAssetCreationId(assetCreationId);
				assetCreation.setAssetNumber(assetNumber);
				deleteAssetDetails = userDeletedAssetDetails(
						assetDetailList,
						assetCreationBL.getAssetCreationService()
								.getAssetDetailByAssetCreationId(
										assetCreationId));
			} else
				assetCreation.setAssetNumber(assetCreationBL
						.generateAssetNumber());
			assetCreationBL.saveAssetCreation(assetCreation, assetDetailList,
					deleteAssetDetails, updateFlag);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetCreation Success ");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure.";
			log.info("Module: Accounts : Method: saveAssetCreation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Asset Creation
	public String deleteAssetCreation() {
		try {
			AssetCreation asssetCreation = assetCreationBL
					.getAssetCreationService().getAssetDetailInfo(
							assetCreationId);
			assetCreationBL.deleteAssetCreation(asssetCreation);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAssetCreation Success ");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Record creation failure.";
			log.info("Module: Accounts : Method: deleteAssetCreation Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Add Asset Detail
	private AssetDetail addAssetCreationDetail(String assetDetail) {
		AssetDetail assetCreationDetail = new AssetDetail();
		String assetDetails[] = splitValues(assetDetail, "__");
		LookupDetail costType = new LookupDetail();
		DirectPaymentDetail directPaymentDetail = new DirectPaymentDetail();
		costType.setLookupDetailId(Long.parseLong(assetDetails[0]));
		assetCreationDetail.setLookupDetail(costType);
		directPaymentDetail.setDirectPaymentDetailId(Long
				.parseLong(assetDetails[1]));
		assetCreationDetail.setDirectPaymentDetail(directPaymentDetail);
		if (null != assetDetails[2] && !("##").equals(assetDetails[2]))
			assetCreationDetail.setDescription(assetDetails[2]);
		if (Long.parseLong(assetDetails[3]) > 0l) {
			assetCreationDetail.setAssetDetailId(Long
					.parseLong(assetDetails[3]));
		}
		return assetCreationDetail;
	}

	// Get user deleted Asset Details
	private List<AssetDetail> userDeletedAssetDetails(
			List<AssetDetail> assetDetails, List<AssetDetail> assetDetailsOld) {

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, AssetDetail> hashAssetDetails = new HashMap<Long, AssetDetail>();
		if (null != assetDetailsOld && assetDetailsOld.size() > 0) {
			for (AssetDetail list : assetDetailsOld) {
				listOne.add(list.getAssetDetailId());
				hashAssetDetails.put(list.getAssetDetailId(), list);
			}
			for (AssetDetail list : assetDetails) {
				listTwo.add(list.getAssetDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<AssetDetail> assetDetailList = new ArrayList<AssetDetail>();
		if (null != different && different.size() > 0) {
			AssetDetail tempAssetDetail = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempAssetDetail = new AssetDetail();
					tempAssetDetail = hashAssetDetails.get(list);
					assetDetailList.add(tempAssetDetail);
				}
			}
		}
		return assetDetailList;
	}

	// Add Row
	public String assetCreationAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: assetCreationAddRow");
			List<LookupDetail> costTypes = assetCreationBL.getLookupMasterBL()
					.getActiveLookupDetails("ASSET_COST_TYPE", true);
			ServletActionContext.getRequest().setAttribute("COST_TYPE",
					costTypes);
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: assetCreationAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}

	}

	// Convert Entity
	private List<AssetVO> convertEntityVOs(List<AssetCreation> assetCreations)
			throws Exception {
		List<AssetVO> assetVOs = new ArrayList<AssetVO>();
		for (AssetCreation assetCreation : assetCreations) {
			assetVOs.add(convertEntityVO(assetCreation));
		}
		return assetVOs;
	}

	// Convert Entity
	private AssetVO convertEntityVO(AssetCreation assetCreation)
			throws IllegalAccessException, InvocationTargetException {
		AssetVO assetVO = new AssetVO();
		BeanUtils.copyProperties(assetVO, assetCreation);
		assetVO.setAssetClassCategory(Constants.Accounts.AssetCategory.get(
				assetCreation.getAssetClass()).name());
		assetVO.setAssetSubClass(assetCreation.getLookupDetailByAssetSubclass()
				.getDisplayName());
		assetVO.setAssetType(assetCreation.getLookupDetailByAssetType()
				.getDisplayName());
		assetVO.setDepreciationDate(DateFormat
				.convertDateToString(assetCreation.getCommenceDate().toString()));
		if (null != assetCreation.getPerson()
				&& !("").equals(assetCreation.getPerson())) {
			assetVO.setOwnerName(assetCreation.getPerson().getFirstName()
					.concat(" ")
					.concat(assetCreation.getPerson().getLastName()));
			assetVO.setOwnerType('P');
			assetVO.setOwnerId(assetCreation.getPerson().getPersonId());
		} else if (null != assetCreation.getCompany()
				&& !("").equals(assetCreation.getCompany())) {
			assetVO.setOwnerName(assetCreation.getCompany().getCompanyName());
			assetVO.setOwnerId(assetCreation.getCompany().getCompanyId());
			assetVO.setOwnerType('C');
		}
		return assetVO;
	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public AssetCreationBL getAssetCreationBL() {
		return assetCreationBL;
	}

	public void setAssetCreationBL(AssetCreationBL assetCreationBL) {
		this.assetCreationBL = assetCreationBL;
	}

	public String getAssetNumber() {
		return assetNumber;
	}

	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}

	public long getAssetCreationId() {
		return assetCreationId;
	}

	public void setAssetCreationId(long assetCreationId) {
		this.assetCreationId = assetCreationId;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public byte getAssetClass() {
		return assetClass;
	}

	public void setAssetClass(byte assetClass) {
		this.assetClass = assetClass;
	}

	public long getAssetSubClass() {
		return assetSubClass;
	}

	public void setAssetSubClass(long assetSubClass) {
		this.assetSubClass = assetSubClass;
	}

	public long getAssetType() {
		return assetType;
	}

	public void setAssetType(long assetType) {
		this.assetType = assetType;
	}

	public long getCategory() {
		return category;
	}

	public void setCategory(long category) {
		this.category = category;
	}

	public byte getAssetColor() {
		return assetColor;
	}

	public void setAssetColor(byte assetColor) {
		this.assetColor = assetColor;
	}

	public byte getAssetCondition() {
		return assetCondition;
	}

	public void setAssetCondition(byte assetCondition) {
		this.assetCondition = assetCondition;
	}

	public byte getPlateType() {
		return plateType;
	}

	public void setPlateType(byte plateType) {
		this.plateType = plateType;
	}

	public byte getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(byte plateColor) {
		this.plateColor = plateColor;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getAssetMake() {
		return assetMake;
	}

	public void setAssetMake(String assetMake) {
		this.assetMake = assetMake;
	}

	public String getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getYearMake() {
		return yearMake;
	}

	public void setYearMake(String yearMake) {
		this.yearMake = yearMake;
	}

	public String getAssetVin() {
		return assetVin;
	}

	public void setAssetVin(String assetVin) {
		this.assetVin = assetVin;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public String getTcNumber() {
		return tcNumber;
	}

	public void setTcNumber(String tcNumber) {
		this.tcNumber = tcNumber;
	}

	public String getAssetDetails() {
		return assetDetails;
	}

	public void setAssetDetails(String assetDetails) {
		this.assetDetails = assetDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public double getScrapValue() {
		return scrapValue;
	}

	public void setScrapValue(double scrapValue) {
		this.scrapValue = scrapValue;
	}

	public byte getDepreciationMethod() {
		return depreciationMethod;
	}

	public void setDepreciationMethod(byte depreciationMethod) {
		this.depreciationMethod = depreciationMethod;
	}

	public byte getDepreciationConvention() {
		return depreciationConvention;
	}

	public void setDepreciationConvention(byte depreciationConvention) {
		this.depreciationConvention = depreciationConvention;
	}

	public String getCommenceDate() {
		return commenceDate;
	}

	public void setCommenceDate(String commenceDate) {
		this.commenceDate = commenceDate;
	}

	public double getDecliningVariance() {
		return decliningVariance;
	}

	public void setDecliningVariance(double decliningVariance) {
		this.decliningVariance = decliningVariance;
	}

	public int getUsefulLife() {
		return usefulLife;
	}

	public void setUsefulLife(int usefulLife) {
		this.usefulLife = usefulLife;
	}

	@JSON(name = "assetValueDetail")
	public Object getAssetValueDetail() {
		return assetValueDetail;
	}

	public void setAssetValueDetail(Object assetValueDetail) {
		this.assetValueDetail = assetValueDetail;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
