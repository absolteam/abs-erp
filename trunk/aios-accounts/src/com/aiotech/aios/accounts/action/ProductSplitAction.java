package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductSplit;
import com.aiotech.aios.accounts.domain.entity.ProductSplitDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.vo.ProductSplitVO;
import com.aiotech.aios.accounts.service.bl.ProductSplitBL;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ProductSplitAction extends ActionSupport{

	private static final long serialVersionUID = 2365291423269480906L;

	private static final Logger log = Logger
			.getLogger(ProductSplitAction.class);

	// Dependency
	private ProductSplitBL productSplitBL;

	// Variables
	private long productSplitId;
	private long receiveSource;
	private long receivePerson;
	private int rowId;
	private String referenceNumber;
	private String receiveDate;
	private String description;
	private String receiveDetails;
	private String returnMessage;
	private List<Object> aaData;
	private String productSplits;
	private Long productId;
	private int storeId;
	private Integer shelfId;
	private String itemType;
	// return success
	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		itemType=null;
		return SUCCESS;
	}

	// Show All Product Split Receive
	public String showProductSplitList() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductSplit");
			aaData = productSplitBL.showAllProductSplit();
			log.info("Module: Accounts : Method: showAllProductSplit: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductSplit Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Receive Entry Screen
	public String showProductSplitEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductSplitEntry");
			ProductSplit productSplit = null;
			ProductSplitVO productSplitVO=null;
			if (productSplitId > 0){
				productSplit = productSplitBL.getProductSplitService().getProductSplitById(productSplitId);
				productSplitVO=productSplitBL.convertProductSplitToVO(productSplit);
				ServletActionContext.getRequest().setAttribute(
						"PRODUCT_SPLIT_INFO", productSplitVO);
				
				ServletActionContext.getRequest().setAttribute(
						"PRODUCT_SPLIT_DETAIL", productSplitVO.getSplitDetailVO());
			}else{
				referenceNumber = SystemBL.getReferenceStamp(
						ProductSplit.class.getName(),
						productSplitBL.getImplementation());
			}
			
			
			log.info("Module: Accounts : Method: showProductSplitEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductSplitEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Production Receive
	public String saveProductSplit() {
		try {
			log.info("Inside Module: Accounts : Method: saveProject");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			ProductSplit productSplit = new ProductSplit();
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			
			Product product=new Product();
			product.setProductId(productId);
			productSplit.setProduct(product);
			Shelf store=new Shelf();
			store=productSplitBL.getStoreBL().getStoreService().getStoreByShelfId(storeId);
			productSplit.setShelf(store);

			productSplit.setDescription(description);
			

			// Inventories Coding Starts
			JSONParser parser = new JSONParser();
			Object inventoryObject = parser.parse(productSplits);
			JSONArray object = (JSONArray) inventoryObject;
			ProductSplitDetail projectInventory = null;
			List<ProductSplitDetail> inventories = new ArrayList<ProductSplitDetail>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("productSplits");
				if (null != scheduleArray && scheduleArray.size() > 0) {
					for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
							.hasNext();) {
						JSONObject resourceDetail = (JSONObject) iteratorObject
								.next();
						projectInventory = new ProductSplitDetail();
						product=new Product();
						product.setProductId(Long
								.valueOf(resourceDetail.get("productId")
										.toString()));
						projectInventory.setQuantity(1.0);
						projectInventory.setUnitRate(Double
								.valueOf(resourceDetail.get("amount").toString()));
						projectInventory.setProduct(product);
						store=productSplitBL.getStoreBL().getStoreService().getStoreByShelfId(Integer
								.valueOf(resourceDetail.get("shelfId")
										.toString()));
						projectInventory.setStore(store.getShelf().getAisle().getStore());
						projectInventory.setShelf(store);
						projectInventory.setDescription(resourceDetail
								.get("description").toString());
						long productSplitDetailId = 0;
						if (resourceDetail.get("productSplitDetailId") != null
								&& !resourceDetail.get("productSplitDetailId")
										.toString().equals(""))
							productSplitDetailId = Long.parseLong(resourceDetail
									.get("productSplitDetailId").toString());
						if (productSplitDetailId > 0) {
							projectInventory
									.setProductSplitDetailId(productSplitDetailId);

						}

						
						inventories.add(projectInventory);
					}
				}
			}
			List<ProductSplitDetail> deleteInvenotries = null;
			List<ProductSplitDetail> editProductSplits=null;
			ProductSplit editSplit=null;
			if (productSplitId > 0) {
				editSplit=productSplitBL.getProductSplitService().getProductSplitById(productSplitId);
				editProductSplits=productSplitBL.getProductSplitService().getProductSplitDetailBySplitId(productSplitId);
				deleteInvenotries = userDeletedInventories(
						inventories,
						editProductSplits);
				ProductSplit tempSplit=productSplitBL.getProductSplitService().getProductSplitById(productSplitId);		
				productSplit.setProductSplitId(productSplitId);
				productSplit.setCreatedDate(tempSplit.getCreatedDate());
				productSplit.setPerson(tempSplit.getPerson());
				productSplit.setReferenceNumber(tempSplit.getReferenceNumber());
			}else{
				productSplit.setCreatedDate(new Date());
				productSplit.setReferenceNumber(SystemBL.getReferenceStamp(
						ProductSplit.class.getName(),
						productSplitBL.getImplementation()));
				productSplit.setPerson(person);
			}

			
			productSplitBL.saveProductSplit(productSplit, inventories,
					deleteInvenotries,editProductSplits,editSplit);
			productSplitId=productSplit.getProductSplitId();
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	private List<ProductSplitDetail> userDeletedInventories(
			List<ProductSplitDetail> projectResources,
			List<ProductSplitDetail> projectResourcOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProductSplitDetail> hashResources = new HashMap<Long, ProductSplitDetail>();
		if (null != projectResourcOld && projectResourcOld.size() > 0) {
			for (ProductSplitDetail list : projectResourcOld) {
				listOne.add(list.getProductSplitDetailId());
				hashResources.put(list.getProductSplitDetailId(), list);
			}
			for (ProductSplitDetail list : projectResources) {
				listTwo.add(list.getProductSplitDetailId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProductSplitDetail> resourceList = new ArrayList<ProductSplitDetail>();
		if (null != different && different.size() > 0) {
			ProductSplitDetail tempResource = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempResource = new ProductSplitDetail();
					tempResource = hashResources.get(list);
					resourceList.add(tempResource);
				}
			}
		}
		return resourceList;
	}


	// Delete Production Receive
	public String deleteProductSplit() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProductSplit");
			ProductSplit productSplit = productSplitBL.getProductSplitService().getProductSplitById(productSplitId);
					
			boolean deleted = productSplitBL.deleteProductSplit(productSplit);
			if (deleted)
				returnMessage = "SUCCESS";
			else
				returnMessage = "Information can not be deleted it is in use.";
			log.info("Module: Accounts : Method: deleteProductSplit: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteProductSplit Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	
	
	//Getter & Setter
	public ProductSplitBL getProductSplitBL() {
		return productSplitBL;
	}
	public void setProductSplitBL(ProductSplitBL productSplitBL) {
		this.productSplitBL = productSplitBL;
	}
	public long getProductSplitId() {
		return productSplitId;
	}
	public void setProductSplitId(long productSplitId) {
		this.productSplitId = productSplitId;
	}
	public long getReceiveSource() {
		return receiveSource;
	}
	public void setReceiveSource(long receiveSource) {
		this.receiveSource = receiveSource;
	}
	public long getReceivePerson() {
		return receivePerson;
	}
	public void setReceivePerson(long receivePerson) {
		this.receivePerson = receivePerson;
	}
	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReceiveDetails() {
		return receiveDetails;
	}
	public void setReceiveDetails(String receiveDetails) {
		this.receiveDetails = receiveDetails;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	public List<Object> getAaData() {
		return aaData;
	}
	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getProductSplits() {
		return productSplits;
	}

	public void setProductSplits(String productSplits) {
		this.productSplits = productSplits;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
}
