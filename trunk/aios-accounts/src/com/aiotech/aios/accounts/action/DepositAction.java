package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.DepositService;
import com.aiotech.aios.accounts.service.bl.DepositBL;
import com.aiotech.aios.accounts.to.DepositTO;
import com.aiotech.aios.accounts.to.converter.GLJournalVoucherTOConverter;  
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation; 
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class DepositAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	//Logger Property
	private static final Logger LOGGER =LogManager.getLogger(DepositAction.class); 
	private DepositService depositService;
	private DepositBL depositBL;
	private List<DepositTO> depositList;
	private DepositTO depositTO;
	private Implementation implementation=null; 
	 
	private String creditDetails; 
	private String debitDetails;
	private long contractId;
	private String accountDetails;
	private String accountName;
	private double transactionAmount;
	private long combinationId;
	private String postDetails;
	private String propertyName;
	private AlertBL alertBL;
	private SystemService systemService;
	private String receiptEntry;
	private long alertId;
	public String returnSuccess(){
		return SUCCESS;
	}
	
	public String showDepositJsonList(){
		LOGGER.info("Module: Accounts : Method: showDepositJsonList");
		try{ 
			HttpSession session=ServletActionContext.getRequest().getSession(); 
			Map<String,Object> sessionObj= ActionContext.getContext().getSession();
			depositList=new ArrayList<DepositTO>();
			depositTO=new DepositTO(); 
			getImplementId();  
			List<TransactionDetail>transactionList=depositService.getTransactionBL().getTransactionService().getJournalLines(implementation);
			session.setAttribute("TRANSACTION_LIST_"+sessionObj.get("jSessionId"), transactionList);
			depositList=GLJournalVoucherTOConverter.convertToDepositList(transactionList);  
		 
			if (depositList != null && depositList.size() > 0) {
				LOGGER.info("Module: Accounts : Method: showDepositJsonList, "+
						" Account List size() "+ transactionList.size());
			} 
			
			JSONObject jsonResponse = new JSONObject();  
			JSONArray data= new JSONArray();   
			int j=0;
			for(TransactionDetail list :transactionList){ 
				JSONArray array= new JSONArray();  
				array.add(list.getTransactionDetailId());
				array.add(list.getCombination().getCombinationId());
				array.add("<span class=\"selectcode\" id=selectcode_"+ ++j+"><input type=\"checkbox\" value=\""+list.getTransactionDetailId()+"\" id=\"selectedDetailId_"+j+"\"></span>");
				if(null!=list.getCombination().getAccountByAnalysisAccountId() && !list.getCombination().getAccountByAnalysisAccountId().equals(""))
					array.add(list.getCombination().getAccountByNaturalAccountId().getAccount()+"."+list.getCombination().getAccountByAnalysisAccountId().getAccount());
				else
					array.add(list.getCombination().getAccountByNaturalAccountId().getAccount());
				array.add(list.getAmount()); 
				if(list.getIsDebit())
					array.add("Debit");
				else
					array.add("Credit");
				array.add(list.getDescription());
				array.add(list.getTransaction().getTransactionId());
				data.add(array); 
			}
			jsonResponse.put("aaData", data);  
			ServletActionContext.getRequest().setAttribute("jsonlist", jsonResponse);  
			LOGGER.info("Module: Accounts : Method: showDepositJsonList: Action Success"); 
			return SUCCESS;
		} 
		catch (Exception ex){
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showDepositJsonList: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showBankTransaction(){
		try { 
			getImplementId();  
			depositList=new ArrayList<DepositTO>(); 
			List<Bank> bankList=depositBL.getBankList(implementation); 
			depositList=depositBL.getBankTransactionEntries(implementation);   
			ServletActionContext.getRequest().setAttribute("BANKS", bankList);  
			ServletActionContext.getRequest().setAttribute("BANK_ACCOUNT_ENTRIES", depositList);  
			LOGGER.info("Module: Accounts : Method: showBankTransaction: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBankTransaction: throws Exception "+ex);  
			return ERROR;
		}
	}
	
 	public String saveDepositTransaction(){
		try{
			/*HttpSession session=ServletActionContext.getRequest().getSession(); 
			Map<String,Object> sessionObj= ActionContext.getContext().getSession();
			getImplementId();  
			long transactionDetailId=0;
			String debitSide[]=splitValues(debitDetails, ","); 
			Transaction transaction=null;
			TransactionDetail transactionDetail=null; 
			List<TransactionDetail>transactionList=new ArrayList<TransactionDetail>();
			List<TransactionDetail>detailList=new ArrayList<TransactionDetail>();
			List<TransactionDetail> sessionList=(List<TransactionDetail>) session.getAttribute("TRANSACTION_LIST_"+sessionObj.get("jSessionId"));
			boolean flag=false; 
			for(int i=0;i<debitSide.length;i++){ 
				transactionDetailId=Long.parseLong(debitSide[i]);
				for(TransactionDetail list: sessionList){
					if(transactionDetailId==list.getTransactionDetailId()){
						list.setIsreceipt(null);
						detailList.add(list);
						flag=true;
					} 
					if(flag==true){
						flag=false;
						break;
					}
				}
			}
			depositBL.updateTransaction(detailList); 
			for(TransactionDetail list: detailList){
				transaction=new Transaction();
				list.setTransactionDetailId(null);
				list.setIsreceipt(null); 
				transaction=depositBL.createTransaction(list.getAmount(),implementation,"for deposit");  
				transactionDetail=depositBL.createDepositCredit(list.getAmount(),combinationId); 
				transactionDetail.setIsreceipt(null); 
				transactionList.add(transactionDetail); 
				list.setIsDebit(true);
				list.setDescription("amount d/r");
				transactionList.add(list);
				depositBL.saveTransaction(transaction,transactionList); 
			}*/
			LOGGER.info("Module: Accounts : Method: saveDepositTransaction: Action Success"); 
			return SUCCESS;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: saveDepositTransaction: throws Exception "+ex);  
			return ERROR;
		}
	} 
	 
	public void getImplementId(){
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation=new Implementation();
		if(session.getAttribute("THIS")!=null){
		  implementation=(Implementation)session.getAttribute("THIS"); 
		}
	}
	
	public void saveReceiptTransactions() {
		LOGGER.info("Module: Accounts : Inside Method: saveReceiptTransaction()");
		try {
			getImplementId();
			Account account = this.getDepositBL().getTransactionBL()
					.getCombinationService().getAccountDetail(accountName, implementation);
			Combination customerCombination = this.getDepositBL()
					.getTransactionBL().getCombinationService()
					.getCombinationByAccount(account.getAccountId());
			List<TransactionDetail> transactionDetailList;
			Transaction transaction = null;
			String[] recepitDetailList = splitValues(receiptEntry, "#@");
			for (String string : recepitDetailList) {
				String[] detailString = splitValues(string, "__");
				if (null != detailString && !detailString.equals("")) {
					transactionDetailList = this.getDepositBL()
							.createReceiptTransactionDetail(
									customerCombination, detailString,
									propertyName, implementation);
					if (null != transactionDetailList
							&& transactionDetailList.size() > 0) {
						transaction = this.getDepositBL().createReceiptTransaction(
								transactionDetailList.get(0).getAmount(),
								implementation);
						this.getDepositBL()
								.getTransactionBL()
								.saveTransaction(transaction, transactionDetailList);
					} 
				}
			} 
			Alert alert = this.getAlertBL().getAlertService().getAlertInfo(alertId);
			alert.setIsActive(false);
			alertBL.commonSaveAlert(alert);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: saveReceiptTransaction(): throws Exception "
					+ ex);
		}
	}
	
	//to split the values
	public static String[] splitValues(String spiltValue, String delimeter){
		return spiltValue.split(delimeter+"(?=([^\"]*\"[^\"]*\")*[^\"]*$)"); 
	} 
	
	public DepositService getDepositService() {
		return depositService;
	}
	public void setDepositService(DepositService depositService) {
		this.depositService = depositService;
	}
	public DepositBL getDepositBL() {
		return depositBL;
	}
	public void setDepositBL(DepositBL depositBL) {
		this.depositBL = depositBL;
	}

	public List<DepositTO> getDepositList() {
		return depositList;
	}

	public void setDepositList(List<DepositTO> depositList) {
		this.depositList = depositList;
	}

	public DepositTO getDepositTO() {
		return depositTO;
	}

	public void setDepositTO(DepositTO depositTO) {
		this.depositTO = depositTO;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getCreditDetails() {
		return creditDetails;
	}

	public void setCreditDetails(String creditDetails) {
		this.creditDetails = creditDetails;
	}

	public String getDebitDetails() {
		return debitDetails;
	}

	public void setDebitDetails(String debitDetails) {
		this.debitDetails = debitDetails;
	}

	public long getContractId() {
		return contractId;
	}

	public void setContractId(long contractId) {
		this.contractId = contractId;
	}

	public String getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(String accountDetails) {
		this.accountDetails = accountDetails;
	} 

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getPostDetails() {
		return postDetails;
	}

	public void setPostDetails(String postDetails) {
		this.postDetails = postDetails;
	}

	public String getReceiptEntry() {
		return receiptEntry;
	}

	public void setReceiptEntry(String receiptEntry) {
		this.receiptEntry = receiptEntry;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	} 
}
