package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialIdleMixDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialIdleMixVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.MaterialIdleMixBL;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MaterialIdleMixAction extends ActionSupport {

	private static final long serialVersionUID = 5898319851436664035L;

	private static final Logger log = Logger
			.getLogger(MaterialIdleMixAction.class);

	private long materialIdleMixId;
	private long craftProductId;
	private long personId;
	private int rowId;
	private String referenceNumber;
	private String createdDate;
	private String personName;
	private String description;
	private String idleMixDetails;
	private String returnMessage;
	private List<Object> aaData;

	private MaterialIdleMixBL materialIdleMixBL;

	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showAllMaterialIdleMix() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMaterialIdleMix");
			aaData = materialIdleMixBL.getAllMaterialIdleMix();
			log.info("Module: Accounts : Method: showAllMaterialIdleMix: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllMaterialIdleMix Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Entry
	public String showMaterialIdleMixEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAllMaterialIdleMix");
			MaterialIdleMixVO materialIdleMixVO = null;
			if (materialIdleMixId > 0) {
				MaterialIdleMix materialIdleMix = materialIdleMixBL
						.getMaterialIdleMixService().getMaterialIdleMixbyId(
								materialIdleMixId);
				materialIdleMixVO = new MaterialIdleMixVO();
				BeanUtils.copyProperties(materialIdleMixVO, materialIdleMix);
				materialIdleMixVO.setDate(DateFormat
						.convertDateToString(materialIdleMix.getCreatedDate()
								.toString()));
				MaterialIdleMixDetailVO materialIdleMixDetailVO = null;
				List<MaterialIdleMixDetailVO> materialIdleMixDetailVOs = new ArrayList<MaterialIdleMixDetailVO>();
				for (MaterialIdleMixDetail mixDetail : materialIdleMix
						.getMaterialIdleMixDetails()) {
					materialIdleMixDetailVO = new MaterialIdleMixDetailVO();
					BeanUtils
							.copyProperties(materialIdleMixDetailVO, mixDetail);
					materialIdleMixDetailVO
							.setProductPackageVOs(materialIdleMixBL
									.getPackageConversionBL()
									.getProductPackagingDetail(
											mixDetail.getProduct()
													.getProductId()));
					if (null != mixDetail.getProductPackageDetail()) {
						materialIdleMixDetailVO.setPackageDetailId(mixDetail
								.getProductPackageDetail()
								.getProductPackageDetailId());
						materialIdleMixDetailVO.setBaseUnitName(mixDetail
								.getProduct().getLookupDetailByProductUnit()
								.getDisplayName());
					} else {
						materialIdleMixDetailVO.setPackageDetailId(-1l);
						if (null == materialIdleMixDetailVO.getPackageUnit()
								|| materialIdleMixDetailVO.getPackageUnit() <= 0)
							materialIdleMixDetailVO.setPackageUnit(mixDetail
									.getMaterialQuantity());
					}
					materialIdleMixDetailVO
							.setBaseQuantity(AIOSCommons
									.convertExponential(mixDetail
											.getMaterialQuantity()));
					materialIdleMixDetailVOs.add(materialIdleMixDetailVO);
				}
				Collections.sort(materialIdleMixDetailVOs,
						new Comparator<MaterialIdleMixDetail>() {
							public int compare(MaterialIdleMixDetail o1,
									MaterialIdleMixDetail o2) {
								return o1
										.getMaterialIdleMixDetailId()
										.compareTo(
												o2.getMaterialIdleMixDetailId());
							}
						});
				materialIdleMixVO
						.setMaterialMixDetailVOs(materialIdleMixDetailVOs);
			} else {
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				User user = (User) sessionObj.get("USER");
				referenceNumber = SystemBL.getReferenceStamp(
						MaterialIdleMix.class.getName(),
						materialIdleMixBL.getImplementation());
				personId = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			ServletActionContext.getRequest().setAttribute("MATERIAL_DETAIL",
					materialIdleMixVO);
			log.info("Module: Accounts : Method: showMaterialIdleMixEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialIdleMixEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showMaterialIdleMixDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showMaterialIdleMixDetails");
			MaterialIdleMix materialIdleMix = materialIdleMixBL
					.getMaterialIdleMixService().getMaterialIdleMixbyProduct(
							craftProductId);
			List<MaterialIdleMixDetailVO> materialIdleMixDetailVOs = null;
			MaterialIdleMixDetailVO materialIdleMixDetailVO = null;
			if (null != materialIdleMix
					&& null != materialIdleMix.getMaterialIdleMixDetails()
					&& materialIdleMix.getMaterialIdleMixDetails().size() > 0) {
				materialIdleMixDetailVOs = new ArrayList<MaterialIdleMixDetailVO>();
				List<StockVO> actualStockVOs = null;
				for (MaterialIdleMixDetail mixDetail : materialIdleMix
						.getMaterialIdleMixDetails()) {
					materialIdleMixDetailVO = new MaterialIdleMixDetailVO();
					BeanUtils
							.copyProperties(materialIdleMixDetailVO, mixDetail);
					materialIdleMixDetailVO.setInsufficientBalance(true);
					List<StockVO> stockVOs = materialIdleMixBL.getStockBL()
							.getProductStockFullDetails(
									mixDetail.getProduct().getProductId());
					if (null != stockVOs && stockVOs.size() > 0) {
						actualStockVOs = new ArrayList<StockVO>();
						for (StockVO stockVO : stockVOs) {
							if (null != stockVO.getQuantity()
									&& (double) stockVO.getQuantity() > 0) {
								actualStockVOs.add(stockVO);
							}
						}
						if (null != actualStockVOs && actualStockVOs.size() > 0
								&& actualStockVOs.size() == 1) {
							materialIdleMixDetailVO
									.setShelfId(null != actualStockVOs.get(0)
											.getShelf() ? actualStockVOs.get(0)
											.getShelf().getShelfId() : null);
							materialIdleMixDetailVO.setStoreName(actualStockVOs
									.get(0).getStoreName());
							if ((double) actualStockVOs.get(0)
									.getAvailableQuantity() < (double) mixDetail
									.getMaterialQuantity())
								materialIdleMixDetailVO
										.setInsufficientBalance(false);
						}
					}
					materialIdleMixDetailVO
							.setProductPackageVOs(materialIdleMixBL
									.getPackageConversionBL()
									.getProductPackagingDetail(
											mixDetail.getProduct()
													.getProductId()));
					if (null != mixDetail.getProductPackageDetail()) {
						materialIdleMixDetailVO.setPackageDetailId(mixDetail
								.getProductPackageDetail()
								.getProductPackageDetailId());
						materialIdleMixDetailVO.setBaseUnitName(mixDetail
								.getProduct().getLookupDetailByProductUnit()
								.getDisplayName());
					} else {
						materialIdleMixDetailVO.setPackageDetailId(-1l);
						if (null == materialIdleMixDetailVO.getPackageUnit()
								|| materialIdleMixDetailVO.getPackageUnit() <= 0)
							materialIdleMixDetailVO.setPackageUnit(mixDetail
									.getMaterialQuantity());
					}
					materialIdleMixDetailVO
							.setBaseQuantity(AIOSCommons
									.convertExponential(mixDetail
											.getMaterialQuantity()));
					materialIdleMixDetailVOs.add(materialIdleMixDetailVO);
				}
				Collections.sort(materialIdleMixDetailVOs,
						new Comparator<MaterialIdleMixDetailVO>() {
							public int compare(MaterialIdleMixDetailVO o1,
									MaterialIdleMixDetailVO o2) {
								return o1
										.getMaterialIdleMixDetailId()
										.compareTo(
												o2.getMaterialIdleMixDetailId());
							}
						});
			}
			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_IDLEMIX_DETAILS", materialIdleMixDetailVOs);
			log.info("Module: Accounts : Method: showMaterialIdleMixDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showMaterialIdleMixDetails Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// save or update
	public String saveMaterialIdleMix() {
		try {
			log.info("Inside Module: Accounts : Method: saveMaterialIdleMix");
			MaterialIdleMix materialIdleMix = new MaterialIdleMix();
			materialIdleMix.setDescription(description);
			Person person = new Person();
			person.setPersonId(personId);
			materialIdleMix.setPerson(person);
			Product craftProduct = new Product();
			craftProduct.setProductId(craftProductId);
			materialIdleMix.setProduct(craftProduct);
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(idleMixDetails);
			JSONArray object = (JSONArray) receiveObject;
			MaterialIdleMixDetail materialIdleMixDetail = null;
			Product product = null;
			List<MaterialIdleMixDetail> materialIdleMixDetails = new ArrayList<MaterialIdleMixDetail>();
			List<MaterialIdleMixDetail> deletedMaterialMixDetails = null;
			ProductPackageDetail productPackageDetail = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("idleMixDetails");
				for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					materialIdleMixDetail = new MaterialIdleMixDetail();
					product = new Product();
					product.setProductId(Long.parseLong(jsonObjectDetail.get(
							"productId").toString()));
					materialIdleMixDetail.setProduct(product);
					materialIdleMixDetail.setPackageUnit(Double
							.parseDouble(jsonObjectDetail.get("packageUnit")
									.toString()));
					if (null != jsonObjectDetail.get("packageDetailId")
							&& Long.parseLong(jsonObjectDetail.get(
									"packageDetailId").toString()) > 0) {
						productPackageDetail = new ProductPackageDetail();
						productPackageDetail.setProductPackageDetailId(Long
								.parseLong(jsonObjectDetail.get(
										"packageDetailId").toString()));
						materialIdleMixDetail
								.setProductPackageDetail(productPackageDetail);
					}
					materialIdleMixDetail.setMaterialQuantity(Double
							.parseDouble(jsonObjectDetail.get(
									"materialQuantity").toString()));
					materialIdleMixDetail
							.setTypicalWastage((null != jsonObjectDetail
									.get("typicalWastage") && Double
									.parseDouble(jsonObjectDetail.get(
											"typicalWastage").toString()) > 0) ? Double
									.parseDouble(jsonObjectDetail.get(
											"typicalWastage").toString())
									: null);
					materialIdleMixDetail
							.setMaterialIdleMixDetailId((null != jsonObjectDetail
									.get("materialIdleMixDetailId") && Long
									.parseLong(jsonObjectDetail.get(
											"materialIdleMixDetailId")
											.toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"materialIdleMixDetailId")
											.toString()) : null);
				}
				materialIdleMixDetails.add(materialIdleMixDetail);
			}
			if (materialIdleMixId > 0) {
				materialIdleMix.setMaterialIdleMixId(materialIdleMixId);
				materialIdleMix.setReferenceNumber(referenceNumber);
				materialIdleMix.setCreatedDate(DateFormat
						.convertStringToDate(createdDate));
				deletedMaterialMixDetails = getUserDeletedMixDetails(materialIdleMixDetails);
			} else {
				materialIdleMix.setReferenceNumber(SystemBL.getReferenceStamp(
						MaterialIdleMix.class.getName(),
						materialIdleMixBL.getImplementation()));
				materialIdleMix
						.setCreatedDate(Calendar.getInstance().getTime());
			}
			materialIdleMixBL.saveMaterialIdleMix(materialIdleMix,
					materialIdleMixDetails, deletedMaterialMixDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveMaterialIdleMix: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveMaterialIdleMix Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// delete
	public String deleteMaterialIdleMix() {
		try {
			log.info("Inside Module: Accounts : Method: saveMaterialIdleMix");
			MaterialIdleMix materialIdleMix = materialIdleMixBL
					.getMaterialIdleMixService().getMaterialIdleMixbyId(
							materialIdleMixId);
			materialIdleMixBL.deleteMaterialIdleMix(
					materialIdleMix,
					new ArrayList<MaterialIdleMixDetail>(materialIdleMix
							.getMaterialIdleMixDetails()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteMaterialIdleMix: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteMaterialIdleMix Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<MaterialIdleMixDetail> getUserDeletedMixDetails(
			List<MaterialIdleMixDetail> materialIdleMixDetails)
			throws Exception {
		List<MaterialIdleMixDetail> existingMixDetails = materialIdleMixBL
				.getMaterialIdleMixService().getMaterialIdleMixDetails(
						materialIdleMixId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, MaterialIdleMixDetail> hashPurchaseDetail = new HashMap<Long, MaterialIdleMixDetail>();
		if (null != existingMixDetails && existingMixDetails.size() > 0) {
			for (MaterialIdleMixDetail list : existingMixDetails) {
				listOne.add(list.getMaterialIdleMixDetailId());
				hashPurchaseDetail.put(list.getMaterialIdleMixDetailId(), list);
			}
			if (null != hashPurchaseDetail && hashPurchaseDetail.size() > 0) {
				for (MaterialIdleMixDetail list : materialIdleMixDetails) {
					listTwo.add(list.getMaterialIdleMixDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<MaterialIdleMixDetail> deletedMixDetails = null;
		if (null != different && different.size() > 0) {
			MaterialIdleMixDetail tempMixDetail = null;
			deletedMixDetails = new ArrayList<MaterialIdleMixDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempMixDetail = new MaterialIdleMixDetail();
					tempMixDetail = hashPurchaseDetail.get(list);
					deletedMixDetails.add(tempMixDetail);
				}
			}
		}
		return deletedMixDetails;
	}

	// Getters & Setters
	public long getMaterialIdleMixId() {
		return materialIdleMixId;
	}

	public void setMaterialIdleMixId(long materialIdleMixId) {
		this.materialIdleMixId = materialIdleMixId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public MaterialIdleMixBL getMaterialIdleMixBL() {
		return materialIdleMixBL;
	}

	public void setMaterialIdleMixBL(MaterialIdleMixBL materialIdleMixBL) {
		this.materialIdleMixBL = materialIdleMixBL;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getIdleMixDetails() {
		return idleMixDetails;
	}

	public void setIdleMixDetails(String idleMixDetails) {
		this.idleMixDetails = idleMixDetails;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public long getCraftProductId() {
		return craftProductId;
	}

	public void setCraftProductId(long craftProductId) {
		this.craftProductId = craftProductId;
	}
}
