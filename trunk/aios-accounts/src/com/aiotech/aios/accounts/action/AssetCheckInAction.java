package com.aiotech.aios.accounts.action;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetCheckIn;
import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.accounts.domain.entity.vo.AssetCheckInVO;
import com.aiotech.aios.accounts.service.bl.AssetCheckInBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AssetCheckInAction extends ActionSupport {

	private static final long serialVersionUID = 690964545291717860L;

	private static final Logger log = Logger
			.getLogger(AssetCheckInAction.class);

	private AssetCheckInBL assetCheckInBL;
	private List<Object> aaData;
	private long assetCheckInId;
	private long assetCheckOutId;
	private long checkOutBy;
	private Long recordId;
	private String checkInNumber;
	private String checkInDate;
	private String description;
	private String returnMessage;
	private String personName;

	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		recordId = null;
		return SUCCESS;
	}

	// Show All Asset Check In JSON list
	public String showCheckInJsonList() {
		try {
			log.info("Inside Module: Accounts : Method: showCheckInJsonList");
			aaData = assetCheckInBL.showCheckInJsonList();
			log.info("Module: Accounts : Method: showCheckInJsonList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCheckInJsonList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showAssetCheckInApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckInApproval");
			assetCheckInId = recordId;
			showAssetCheckInEntry();
			log.info("Module: Accounts : Method: showAssetCheckInApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckOutApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetCheckInReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckInReject");
			assetCheckInId = recordId;
			showAssetCheckInEntry();
			log.info("Module: Accounts : Method: showAssetCheckInReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckInReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Check In Screen entry
	public String showAssetCheckInEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetCheckInEntry");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			assetCheckOutId = 0l;
			personName = null;
			if (assetCheckInId > 0) {
				AssetCheckIn assetCheckIn = assetCheckInBL
						.getAssetCheckInService().getAssetCheckInDetails(
								assetCheckInId);
				AssetCheckInVO assetCheckInVO = convertCheckIn(assetCheckIn);
				ServletActionContext.getRequest().setAttribute("CHECK_IN",
						assetCheckInVO);
				CommentVO comment = new CommentVO();
				if (recordId != null && assetCheckIn != null
						&& assetCheckIn.getAssetCheckInId() != 0 && recordId > 0) {
					comment.setRecordId(assetCheckIn.getAssetCheckInId());
					comment.setUseCase(AssetCheckIn.class.getName());
					comment = assetCheckInBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			} else {
				checkInNumber = getAssetCheckInReference();
				checkOutBy = user.getPersonId();
				personName = (String) sessionObj.get("PERSON_NAME");
			}
			log.info("Module: Accounts : Method: showAssetCheckInEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetCheckInEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Check In
	public String saveAssetCheckIn() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetCheckIn");
			AssetCheckIn assetCheckIn = new AssetCheckIn();
			if (assetCheckInId > 0) {
				assetCheckIn.setAssetCheckInId(assetCheckInId);
				assetCheckIn.setCheckInNumber(checkInNumber);
			} else
				assetCheckIn.setCheckInNumber(getAssetCheckInReference());
			assetCheckIn.setCheckInDate(DateFormat
					.convertStringToDate(checkInDate));
			Person person = new Person();
			person.setPersonId(checkOutBy);
			assetCheckIn.setPerson(person);
			assetCheckIn.setDescription(description);
			AssetCheckOut assetCheckOut = assetCheckInBL.getAssetCheckOutBL()
					.getAssetCheckOutService()
					.getAssetCheckOutById(assetCheckOutId);
			assetCheckOut.setStatus(false);
			assetCheckIn.setAssetCheckOut(assetCheckOut);
			assetCheckInBL.saveAssetCheckIn(assetCheckIn, assetCheckOut);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetCheckIn: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Save failure.";
			log.info("Module: Accounts : Method: saveAssetCheckIn Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Asset Check In
	public String deleteAssetCheckIn() {
		try {
			log.info("Inside Module: Accounts : Method: saveAssetCheckIn");
			AssetCheckIn assetCheckIn = assetCheckInBL.getAssetCheckInService()
					.getAssetCheckInDetails(assetCheckInId);
			assetCheckInBL.deleteAssetCheckIn(assetCheckIn);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAssetCheckIn: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Save failure.";
			log.info("Module: Accounts : Method: deleteAssetCheckIn Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private AssetCheckInVO convertCheckIn(AssetCheckIn assetCheckIn)
			throws Exception {
		AssetCheckInVO assetCheckOInVO = new AssetCheckInVO();
		BeanUtils.copyProperties(assetCheckOInVO, assetCheckIn);
		assetCheckOInVO.setInDate(DateFormat.convertDateToString(assetCheckIn
				.getCheckInDate().toString()));
		return assetCheckOInVO;
	}

	// Generate Asset Check In Reference
	public String getAssetCheckInReference() throws Exception {
		return SystemBL.getReferenceStamp(AssetCheckIn.class.getName(),
				assetCheckInBL.getImplementation());
	}

	// Getters & Setters
	public long getAssetCheckInId() {
		return assetCheckInId;
	}

	public void setAssetCheckInId(long assetCheckInId) {
		this.assetCheckInId = assetCheckInId;
	}

	public AssetCheckInBL getAssetCheckInBL() {
		return assetCheckInBL;
	}

	public void setAssetCheckInBL(AssetCheckInBL assetCheckInBL) {
		this.assetCheckInBL = assetCheckInBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getAssetCheckOutId() {
		return assetCheckOutId;
	}

	public void setAssetCheckOutId(long assetCheckOutId) {
		this.assetCheckOutId = assetCheckOutId;
	}

	public String getCheckInNumber() {
		return checkInNumber;
	}

	public void setCheckInNumber(String checkInNumber) {
		this.checkInNumber = checkInNumber;
	}

	public long getCheckOutBy() {
		return checkOutBy;
	}

	public void setCheckOutBy(long checkOutBy) {
		this.checkOutBy = checkOutBy;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}
}
