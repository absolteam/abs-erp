package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.vo.CurrencyConversionVO;
import com.aiotech.aios.accounts.domain.entity.vo.CurrencyVO;
import com.aiotech.aios.accounts.service.bl.CurrencyConversionBL;
import com.opensymphony.xwork2.ActionSupport;

public class CurrencyConversionAction extends ActionSupport {

	private static final long serialVersionUID = -8518030589503993110L;

	private static final Logger log = Logger
			.getLogger(CurrencyConversionAction.class.getName());

	private long currencyConversionId;
	private long conversionCurrency;
	private long oldCurrency;
	private double conversionRate;
	private double transactionValue;
	private String returnMessage;
	private String currencyConversions;
	private Integer rowId;
	private List<Object> aaData;
	private CurrencyConversionBL currencyConversionBL;

	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showAllCurrencyConversion() {
		try {
			log.info("Inside Module: Accounts : Method: showAllCurrencyConversion");
			aaData = currencyConversionBL.getAllCurrencyConversion();
			log.info("Module: Accounts : Method: showAllCurrencyConversion: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllCurrencyConversion Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCurrencyConversionAdd() {
		try {
			log.info("Inside Module: Accounts : Method: showCurrencyConversionAdd");
			Currency currency = currencyConversionBL.getCurrencyBL()
					.getDefaultCurrency();
			if (null != currency) {
				CurrencyVO currencyVO = currencyConversionBL.getCurrencyBL()
						.addCurrencyVO(currency);
				ServletActionContext.getRequest().setAttribute(
						"DEFAULT_CURRENCY", currencyVO);
			}
			List<CurrencyVO> nonFnCurrencieVOs = getNonFunctionalCurrency();
			ServletActionContext.getRequest().setAttribute(
					"NON_DEFAULT_CURRENCY", nonFnCurrencieVOs);
			log.info("Module: Accounts : Method: showCurrencyConversionAdd: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCurrencyConversionAdd Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCurrencyConversionEdit() {
		try {
			log.info("Inside Module: Accounts : Method: showCurrencyConversionEdit");
			Currency currency = currencyConversionBL.getCurrencyBL()
					.getDefaultCurrency();
			if (null != currency) {
				CurrencyVO currencyVO = currencyConversionBL.getCurrencyBL()
						.addCurrencyVO(currency);
				ServletActionContext.getRequest().setAttribute(
						"DEFAULT_CURRENCY", currencyVO);
			}
			CurrencyConversion currencyConversion = currencyConversionBL
					.getCurrencyConversionService().getCurrencyConversionById(
							currencyConversionId);
			CurrencyConversionVO currencyConversionVO = new CurrencyConversionVO();
			BeanUtils.copyProperties(currencyConversionVO, currencyConversion);
			currencyConversionVO.setCountryName(currencyConversion
					.getCurrency().getCurrencyPool().getCountry()
					.getCountryName());
			currencyConversionVO.setConversionCurrency(currencyConversion
					.getCurrency().getCurrencyPool().getCode());
			ServletActionContext.getRequest().setAttribute(
					"NON_DEFAULT_CURRENCY", currencyConversionVO);
			log.info("Module: Accounts : Method: showCurrencyConversionEdit: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCurrencyConversionEdit Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<CurrencyVO> getNonFunctionalCurrency() throws Exception {
		List<Currency> nonFnCurrencies = currencyConversionBL.getCurrencyBL()
				.getCurrencyService()
				.getNonFnCurrencies(currencyConversionBL.getImplementation());
		List<CurrencyVO> nonFnCurrencieVOs = null;
		if (null != nonFnCurrencies && nonFnCurrencies.size() > 0) {
			nonFnCurrencieVOs = new ArrayList<CurrencyVO>();
			for (Currency cy : nonFnCurrencies)
				nonFnCurrencieVOs.add(currencyConversionBL.getCurrencyBL()
						.addCurrencyVO(cy));
		}
		return nonFnCurrencieVOs;
	}

	public String currencyConversionAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: currencyConversionAddRow");
			List<CurrencyVO> nonFnCurrencieVOs = getNonFunctionalCurrency();
			ServletActionContext.getRequest().setAttribute(
					"NON_DEFAULT_CURRENCY", nonFnCurrencieVOs);
			log.info("Module: Accounts : Method: currencyConversionAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: currencyConversionAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveCurrencyConversion() {
		try {
			log.info("Inside Module: Accounts : Method: saveCurrencyConversion");
			JSONParser parser = new JSONParser();
			Object packageObject = parser.parse(currencyConversions);
			JSONArray object = (JSONArray) packageObject;
			CurrencyConversion currencyConversion = null;
			Currency currency = null;
			List<CurrencyConversion> currencyConversionList = new ArrayList<CurrencyConversion>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				currencyConversion = new CurrencyConversion();
				currency = new Currency();
				JSONObject jsonObject = (JSONObject) iterator.next();
				currencyConversion.setConversionRate(Double
						.parseDouble(jsonObject.get("conversionRate")
								.toString()));
				currency.setCurrencyId(Long.parseLong(jsonObject.get(
						"conversionCurrency").toString()));
				currencyConversion.setCurrency(currency);
				currencyConversion.setImplementation(currencyConversionBL
						.getImplementation());
				if (null != jsonObject.get("currencyConversionId")
						&& Long.parseLong(jsonObject
								.get("currencyConversionId").toString()) > 0)
					currencyConversion.setCurrencyConversionId(Long
							.parseLong(jsonObject.get("currencyConversionId")
									.toString()));
				currencyConversionList.add(currencyConversion);
			}
			currencyConversionBL
					.saveCurrencyConversions(currencyConversionList);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveCurrencyConversion: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveCurrencyConversion Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteCurrencyConversion() {
		try {
			log.info("Inside Module: Accounts : Method: saveCurrencyConversion");
			CurrencyConversion currencyConversion = currencyConversionBL
					.getCurrencyConversionService().getCurrencyConversionById(
							currencyConversionId);
			currencyConversionBL.deleteCurrencyConversion(currencyConversion);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveCurrencyConversion: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveCurrencyConversion Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getCurrencyConversionValue() {
		try {
			log.info("Inside Module: Accounts : Method: getCurrencyConversionValue");
			transactionValue = currencyConversionBL.getCurrencyConversionValue(
					oldCurrency, conversionCurrency, transactionValue);
			log.info("Module: Accounts : Method: getCurrencyConversionValue: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: getCurrencyConversionValue Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public long getCurrencyConversionId() {
		return currencyConversionId;
	}

	public void setCurrencyConversionId(long currencyConversionId) {
		this.currencyConversionId = currencyConversionId;
	}

	public long getConversionCurrency() {
		return conversionCurrency;
	}

	public void setConversionCurrency(long conversionCurrency) {
		this.conversionCurrency = conversionCurrency;
	}

	public double getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(double conversionRate) {
		this.conversionRate = conversionRate;
	}

	public CurrencyConversionBL getCurrencyConversionBL() {
		return currencyConversionBL;
	}

	public void setCurrencyConversionBL(
			CurrencyConversionBL currencyConversionBL) {
		this.currencyConversionBL = currencyConversionBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getCurrencyConversions() {
		return currencyConversions;
	}

	public void setCurrencyConversions(String currencyConversions) {
		this.currencyConversions = currencyConversions;
	}

	public double getTransactionValue() {
		return transactionValue;
	}

	public void setTransactionValue(double transactionValue) {
		this.transactionValue = transactionValue;
	}

	public long getOldCurrency() {
		return oldCurrency;
	}

	public void setOldCurrency(long oldCurrency) {
		this.oldCurrency = oldCurrency;
	}
}
