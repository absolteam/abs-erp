package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Emi;
import com.aiotech.aios.accounts.domain.entity.EmiDetail;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.service.bl.EmiDetailBL;
import com.aiotech.aios.accounts.to.EmiDetailTO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

public class EmiDetailAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	//Variable Declarations
	private long emiId;
	private long loanId;
	private String showPage;
	private int varianceDays;
	private int yearlyNoDate;
	private Double addtionalCharges;
	private String sheduleBy;
	private String emiLines;
	private Integer id;
	private String returnMessage;
	private Double interestRate;
	private long recordId;
	private String attribute1;
	private String dueDate;
	//Object Declarations
	private EmiDetailBL emiDetailBL;
	
	//Logger Property
	private static final Logger LOGGER =LogManager.getLogger(EmiDetailAction.class);
	
	public String returnSuccess(){
		if(null==id)
			id=0;
		LOGGER.info("Module: Accounts : Method: returnSuccess: Action Success"); 
		ServletActionContext.getRequest().setAttribute("rowId", id);  
		return SUCCESS;
	} 
	
	public String emiAddRow(){
		try {
			Loan loan=getEmiDetailBL().getLoanService().getLoanById(loanId);
			ServletActionContext.getRequest().setAttribute("LOAN_DETAILS_EMI", loan);   
			ServletActionContext.getRequest().setAttribute("rowId", id);  
			LOGGER.info("Module: Accounts : Method: returnSuccess: Action Success");  
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showEMIJsonList: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showEMIJsonList(){
		LOGGER.info("Module: Accounts : Method: showEMIJsonList");
		try{
			JSONObject jsonList=getEmiDetailBL().getEMIlist(getImplementaionId()); 
			ServletActionContext.getRequest().setAttribute("jsonlist", jsonList);  
			LOGGER.info("Module: Accounts : Method: showEMIJsonList: Action Success"); 
			return SUCCESS;
		}
		catch(Exception ex){
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showEMIJsonList: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showEMIEntry(){
		LOGGER.info("Module: Accounts : Method: showEMIEntry");
		try {
			ServletActionContext.getRequest().setAttribute("showPage", showPage);  
			LOGGER.info("Module: Accounts : Method: showEMIEntry: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();  
			LOGGER.info("Module: Accounts : Method: showEMIEntry: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String buildLoanSchedule(){
		LOGGER.info("Module: Accounts : Method: buildLoanSchedule");
		try {
			getEmiDetailBL().setEmiTOList(null);
			Loan loan=getEmiDetailBL().getLoanService().getLoanById(loanId);
			List<Emi> detailList = getEmiDetailBL().getEmiService().getEMISchedule(loanId);
			getEmiDetailBL().setChargeList(getEmiDetailBL().getLoanService().getLoanChargesById(loan.getLoanId()));
			List<EmiDetailTO> emiList=new ArrayList<EmiDetailTO>();  
			List<EmiDetailTO> emiSchedule= new ArrayList<EmiDetailTO>(); 
			if(null!=detailList && detailList.size()>0){
				emiList=getEmiDetailBL().convertListEntity(detailList);  
				getEmiDetailBL().setSheduleBy("S");   
				emiSchedule=getEmiDetailBL().buildWithEMI(loan,emiList);
			}
			else{ 
				emiSchedule= getEmiDetailBL().buildSchedule(loan);  
			}   
			ServletActionContext.getRequest().setAttribute("LOAN_CHARGES_DETAIL", getEmiDetailBL().getChargeList()); 
			ServletActionContext.getRequest().setAttribute("ALERT_LOAN_DETAILS", loan);   
			if(null!=emiList && !emiList.equals("") && emiList.size()>0){
				for(int i=0;i<emiSchedule.size();i++){
					for(EmiDetailTO list: emiList){
						if(list.getEmiDueDate().equalsIgnoreCase(emiSchedule.get(i).getEmiDueDate())){
							emiSchedule.remove(i);
						}
					}  
				}
			}
			ServletActionContext.getRequest().setAttribute("LOAN_SCHEDULE", emiSchedule); 
			ServletActionContext.getRequest().setAttribute("EMI_DETAIL_PAYMENT", emiList);
			int maxSize=0;  
			if(null!=emiList && !emiList.equals("") && emiList.size()>0){
				for(EmiDetailTO list: emiList){
					int temp=list.getEmiPaymentList().size();
					if(temp>maxSize){
						ServletActionContext.getRequest().setAttribute("EMI_DETAIL_HEAD", list.getEmiPaymentList());
					}
				}
			}
			LOGGER.info("Module: Accounts : Method: buildLoanSchedule: Action Success"); 
			if(loan.getEmiType().equals("P"))
				return SUCCESS;
			else
				return "UNPREDICT";
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: buildLoanSchedule: throws Exception "+ex);  
			return ERROR;
		}
	} 
	
	public String performEmiSchedule(){
		LOGGER.info("Module: Accounts : Method: performEmiSchedule");
		try {
			loanId=recordId;
			String[] message=splitValues(attribute1," "); 
			getEmiDetailBL().setEmiTOList(null);
			Loan loan=getEmiDetailBL().getLoanService().getLoanById(loanId); 
			List<Emi> detailList = getEmiDetailBL().getEmiService().getEMISchedule(loanId);
			getEmiDetailBL().setChargeList(getEmiDetailBL().getLoanService().getLoanChargesById(loan.getLoanId()));
			List<EmiDetailTO> emiList=new ArrayList<EmiDetailTO>(); 
			List<EmiDetailTO> emiSchedule= new ArrayList<EmiDetailTO>(); 
			if(null!=detailList && detailList.size()>0){
				emiList=getEmiDetailBL().convertListEntity(detailList);  
				getEmiDetailBL().setSheduleBy("S");  
				getEmiDetailBL().setCurrentPaymentDate(DateFormat.convertStringToDate(message[6])); 
				emiSchedule=getEmiDetailBL().buildWithEMI(loan,emiList);
			}
			else{
				getEmiDetailBL().setCurrentPaymentDate(DateFormat.convertStringToDate(message[6])); 
				emiSchedule= getEmiDetailBL().buildSchedule(loan);  
			}  
			ServletActionContext.getRequest().setAttribute("LOAN_CHARGES_DETAIL", getEmiDetailBL().getChargeList()); 
			ServletActionContext.getRequest().setAttribute("ALERT_LOAN_DETAILS", loan);   
			for(int i=0;i<emiSchedule.size();i++){
				for(EmiDetailTO list: emiList){
					if(list.getEmiDueDate().equalsIgnoreCase(emiSchedule.get(i).getEmiDueDate())){
						emiSchedule.remove(i);
					}
				}  
			}
			ServletActionContext.getRequest().setAttribute("LOAN_SCHEDULE", emiSchedule); 
			ServletActionContext.getRequest().setAttribute("EMI_DETAIL_PAYMENT", emiList);
			int maxSize=0;  
			for(EmiDetailTO list: emiList){
				int temp=list.getEmiPaymentList().size();
				if(temp>maxSize){
					ServletActionContext.getRequest().setAttribute("EMI_DETAIL_HEAD", list.getEmiPaymentList());
				}
			}
			LOGGER.info("Module: Accounts : Method: performEmiSchedule: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: performEmiSchedule: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String showEmiPayment(){
		LOGGER.info("Module: Accounts : Method: showEmiPayment");
		try {
			Loan loan=getEmiDetailBL().getLoanService().getLoanById(loanId); 
			loan.setEmiFirstDuedate(DateFormat.convertStringToDate(dueDate));
			String[] emiPay=splitValues(attribute1, "##@");
			EmiDetail emiDetail=null;
			List<EmiDetail> detailList=new ArrayList<EmiDetail>();
			for(String string: emiPay){
				String[] emiPayments=splitValues(string,"#@");
				emiDetail=new EmiDetail();
				emiDetail.setName(emiPayments[0]);
				emiDetail.setAmount(Double.parseDouble(emiPayments[1])); 
				detailList.add(emiDetail); 
			}
			ServletActionContext.getRequest().setAttribute("ALERT_LOAN_DETAILS", loan);
			ServletActionContext.getRequest().setAttribute("EMI_DETAILS", detailList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showEmiPayment: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String calculateSchedule(){
		LOGGER.info("Module: Accounts : Method: calculateSchedule");
		try {
			EmiDetailTO detail=null; 
			List<EmiDetailTO>detailList=new ArrayList<EmiDetailTO>();
			Loan loan=getEmiDetailBL().getLoanService().getLoanById(loanId);
			int i=0;
			if(null!=emiLines && !emiLines.equalsIgnoreCase("")){
				String[] lineDetails = splitValues(emiLines,"↕↕");
				for(String line: lineDetails){
					detail=new EmiDetailTO(); 
					String[] emiLine = splitValues(line,"↕");
					detail.setLineNumber(++i);
					detail.setEmiDueDate(emiLine[0]);
					/*detail.setInstalment(new Double(emiLine[1]));
					if(null!=emiLine[2] && !emiLine[2].equalsIgnoreCase("") && !emiLine[2].equalsIgnoreCase("0"))
						detail.setInterest(new Double(emiLine[2]));
					else
						detail.setInterest(0d);
					if(null!=emiLine[3] && !emiLine[3].equalsIgnoreCase("") && !emiLine[3].equalsIgnoreCase("0"))
						detail.setInsurance(new Double(emiLine[3]));
					else
						detail.setInsurance(0d);
					if(null!=emiLine[4] && !emiLine[4].equalsIgnoreCase("") && !emiLine[4].equalsIgnoreCase("0"))
						detail.setOtherCharges(new Double(emiLine[4]));
					else
						detail.setOtherCharges(0d);
					if(null!=emiLine[5] && !emiLine[5].equalsIgnoreCase("") && !emiLine[5].equalsIgnoreCase("0"))
						detail.setEiborAmount(new Double(emiLine[5]));
					else
						detail.setEiborAmount(0d);
					if(null!=emiLine[6] && !emiLine[6].equalsIgnoreCase("") && !emiLine[6].equalsIgnoreCase("null"))
						detail.setDescription(emiLine[6]);*/
					detailList.add(detail); 
				}
				List<EmiDetailTO> emiSchedule= getEmiDetailBL().calculateSchedule(detailList,loan);
				ServletActionContext.getRequest().setAttribute("LOAN_SCHEDULE", emiSchedule);   
			}
			else{
				ServletActionContext.getRequest().setAttribute("LOAN_SCHEDULE", null);   
			}
			LOGGER.info("Module: Accounts : Method: calculateSchedule: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: calculateSchedule: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String saveEMIPayments(){
		LOGGER.info("Module: Accounts : Method: saveEmiDetails");
		try {
			Loan loan=new Loan();
			loan.setLoanId(loanId);
			Emi emi=new Emi();
			emi.setLoan(loan);
			emi.setDate(DateFormat.convertStringToDate(dueDate));
			emi.setStatus(true); 
			String[] emiPay=splitValues(attribute1, "##@");
			EmiDetail emiDetail=null;
			Combination combination=null;
			List<EmiDetail> detailList=new ArrayList<EmiDetail>();
			for(String string: emiPay){
				String[] emiPayments=splitValues(string,"#@");
				emiDetail=new EmiDetail();
				emiDetail.setName(emiPayments[0]);
				emiDetail.setAmount(Double.parseDouble(emiPayments[1])); 
				combination=new Combination();
				if(Long.parseLong(emiPayments[2])>0){
					combination.setCombinationId(Long.parseLong(emiPayments[2])); 
					emiDetail.setCombination(combination);
				}
				else
					emiDetail.setCombination(null); 
				detailList.add(emiDetail); 
			} 
			getEmiDetailBL().saveEMIDetails(emi,detailList);
			getEmiDetailBL().saveEMIImages(emi,false);
			getEmiDetailBL().saveEMIDocuments(emi,false);
			returnMessage="SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: saveEmiDetails: Action Success"); 
			return SUCCESS;
			
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage="FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: calculateSchedule: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String saveEmiDetails(){
		LOGGER.info("Module: Accounts : Method: saveEmiDetails");
		try {
			/*EmiDetail detail=null;
			Loan loan=null; 
			String[] lineDetails = splitValues(emiLines,"↕↕");
			List<EmiDetail>detailList=new ArrayList<EmiDetail>();
			for(String line: lineDetails){
				detail=new EmiDetail();
				loan=new Loan(); 
				String[] emiLine = splitValues(line,"↕");
				loan.setLoanId(loanId);
				detail.setLoan(loan);
				detail.setDate(DateFormat.convertStringToDate(emiLine[0]));
				detail.setInstalment(new Double(emiLine[1]));
				if(null!=emiLine[2] && !emiLine[2].equalsIgnoreCase("") && !emiLine[2].equalsIgnoreCase("0"))
					detail.setInterest(new Double(emiLine[2]));
				if(null!=emiLine[3] && !emiLine[3].equalsIgnoreCase("") && !emiLine[3].equalsIgnoreCase("0"))
					detail.setInsurance(new Double(emiLine[3]));
				if(null!=emiLine[4] && !emiLine[4].equalsIgnoreCase("") && !emiLine[4].equalsIgnoreCase("0"))
					detail.setOtherCharges(new Double(emiLine[4]));
				if(null!=emiLine[5] && !emiLine[5].equalsIgnoreCase("") && !emiLine[5].equalsIgnoreCase("0"))
					detail.setEiborAmount(new Double(emiLine[5]));
				if(null!=emiLine[6] && !emiLine[6].equalsIgnoreCase("") && !emiLine[6].equalsIgnoreCase("null"))
					detail.setDescription(emiLine[6]);
				detailList.add(detail);
			}
			getEmiDetailBL().getEmiService().saveEMI(detailList);*/
			returnMessage="SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: saveEmiDetails: Action Success"); 
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage="FAILURE";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE", returnMessage); 
			LOGGER.info("Module: Accounts : Method: saveEmiDetails: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public String getEMISchedule(){
		LOGGER.info("Module: Accounts : Method: getEMISchedule");
		try {
			List<Emi> detailList = getEmiDetailBL().getEmiService().getEMISchedule(loanId);
			if(null!=detailList && detailList.size()>0){
				List<EmiDetailTO> emiList=getEmiDetailBL().convertEntity(detailList);
				ServletActionContext.getRequest().setAttribute("EMI_DETAIL_LIST", emiList); 
				/*if(detailList.get(0).getLoan().getRate()!=null)
					interestRate=detailList.get(0).getLoan().getRate();
				else if(detailList.get(0).getLoan().getFinalRate()!=null)
					interestRate=detailList.get(0).getLoan().getFinalRate();
				else
					interestRate=0d;
				ServletActionContext.getRequest().setAttribute("RATE", interestRate); 
				ServletActionContext.getRequest().setAttribute("LOAN_TYPE", detailList.get(0).getLoan().getRateType()); */
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();  
			LOGGER.info("Module: Accounts : Method: saveEmiDetails: throws Exception "+ex);  
			return ERROR;
		}
	}
	
	public Implementation getImplementaionId()throws Exception{
		HttpSession session=ServletActionContext.getRequest().getSession();
		Implementation implementation=new Implementation();
		if(null!=session.getAttribute("THIS"))
			implementation=(Implementation)session.getAttribute("THIS");
		return implementation;
	}
	
	public static String[] splitValues(String spiltValue, String delimeter){
		return spiltValue.split(delimeter+"(?=([^\"]*\"[^\"]*\")*[^\"]*$)"); 
	} 
	
	public long getEmiId() {
		return emiId;
	}

	public void setEmiId(long emiId) {
		this.emiId = emiId;
	}

	public long getLoanId() {
		return loanId;
	}

	public void setLoanId(long loanId) {
		this.loanId = loanId;
	}

	public EmiDetailBL getEmiDetailBL() {
		return emiDetailBL;
	}

	public void setEmiDetailBL(EmiDetailBL emiDetailBL) {
		this.emiDetailBL = emiDetailBL;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public int getVarianceDays() {
		return varianceDays;
	}

	public void setVarianceDays(int varianceDays) {
		this.varianceDays = varianceDays;
	}

	public int getYearlyNoDate() {
		return yearlyNoDate;
	}

	public void setYearlyNoDate(int yearlyNoDate) {
		this.yearlyNoDate = yearlyNoDate;
	}

	public Double getAddtionalCharges() {
		return addtionalCharges;
	}

	public void setAddtionalCharges(Double addtionalCharges) {
		this.addtionalCharges = addtionalCharges;
	}

	public String getSheduleBy() {
		return sheduleBy;
	}

	public void setSheduleBy(String sheduleBy) {
		this.sheduleBy = sheduleBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmiLines() {
		return emiLines;
	}

	public void setEmiLines(String emiLines) {
		this.emiLines = emiLines;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	} 
}
