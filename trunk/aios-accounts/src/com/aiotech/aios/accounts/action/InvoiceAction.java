package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveVO;
import com.aiotech.aios.accounts.service.bl.InvoiceBL;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class InvoiceAction extends ActionSupport {

	private static final long serialVersionUID = -939064056143135047L;
	private static final Logger LOGGER = LogManager
			.getLogger(InvoiceAction.class);

	private long invoiceId;
	private long continousInvoiceId;
	private long currencyId;
	private Long recordId;
	private Long alertId;
	private Long messageId;
	private String invoiceNumber;
	private String tempInvoiceNumber;
	private String date;
	private String description;
	private String invoiceLineDetails;
	private String returnMessage;
	private InvoiceBL invoiceBL;
	private String receiveDetail;
	private String supplierId;
	private Integer rowId;
	private String invoiceDetail;
	private double totalInvoice;
	private double invoiceAmount;
	private Byte status;
	private List<Object> aaData;
	private InvoiceVO invoiceVO;
	private Long parentInvoiceId;

	public String returnSuccess() {
		try {
			LOGGER.info("Inside Account returnSuccess()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("RECEIVE_NOTES_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: returnSuccess Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showInvoiceList() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showInvoiceList");
			List<Invoice> invoices = invoiceBL.getInvoiceService()
					.getAllNonContinuosPurchaseInvoice(
							invoiceBL.getImplementationId());
			JSONObject jsonList = invoiceBL.getInvoiceList(invoices);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: Accounts : Method: showInvoiceList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showInvoiceList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all Purchase Invoice Continuous
	public String showContinuousPurchaseInvoice() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showContinuousPurchaseInvoice");
			aaData = invoiceBL.getAllContinuousPurchaseInvoice();
			LOGGER.info("Module: Accounts : Method: showContinuousPurchaseInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showContinuousPurchaseInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public InvoiceVO addPurchaseInvoiceVO(Invoice invoice) throws Exception {
		InvoiceVO invoiceVO = new InvoiceVO();
		BeanUtils.copyProperties(invoiceVO, invoice);
		invoiceVO
				.setSupplierName(null != invoice.getSupplier().getPerson() ? invoice
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat("")
						.concat(invoice.getSupplier().getPerson().getLastName())
						: (null != invoice.getSupplier().getCompany() ? invoice
								.getSupplier().getCompany().getCompanyName()
								: invoice.getSupplier().getCmpDeptLocation()
										.getCompany().getCompanyName()));
		List<InvoiceDetail> invoiceDetails = null;
		if (null != invoice.getInvoice())
			invoiceDetails = new ArrayList<InvoiceDetail>(invoice.getInvoice()
					.getInvoiceDetails());
		else
			invoiceDetails = new ArrayList<InvoiceDetail>(
					invoice.getInvoiceDetails());
		ReceiveDetail receiveDetail = null;
		ReceiveVO receiveVO = null;
		Map<Long, ReceiveVO> receiveVOMap = new HashMap<Long, ReceiveVO>();
		for (InvoiceDetail invoiceDetail : invoiceDetails) {
			receiveDetail = invoiceDetail.getReceiveDetail();
			receiveVO = new ReceiveVO();
			if (null != receiveVOMap
					&& receiveVOMap.containsKey(receiveDetail.getReceive()
							.getReceiveId())) {
				receiveVO = receiveVOMap.get(receiveDetail.getReceive()
						.getReceiveId());
				receiveVO.setTotalReceiveAmount(null != receiveVO
						.getTotalReceiveAmount() ? receiveVO
						.getTotalReceiveAmount()
						+ (receiveDetail.getUnitRate() * receiveDetail
								.getReceiveQty()) : (receiveDetail
						.getUnitRate() * receiveDetail.getReceiveQty()));
				receiveVO.setTotalReceiveQty(null != receiveVO
						.getTotalReceiveQty() ? receiveVO.getTotalReceiveQty()
						+ receiveDetail.getReceiveQty() : receiveDetail
						.getReceiveQty());
			} else {
				receiveVO
						.setTotalReceiveAmount((receiveDetail.getUnitRate() * receiveDetail
								.getReceiveQty()));
				receiveVO.setTotalReceiveQty(receiveDetail.getReceiveQty());
				receiveVO.setReceiveNumber(receiveDetail.getReceive()
						.getReceiveNumber());
				receiveVO.setReferenceNo(receiveDetail.getReceive()
						.getReferenceNo());
				receiveVO.setStrReceiveDate(DateFormat
						.convertDateToString(receiveDetail.getReceive()
								.getReceiveDate().toString()));
			}
			receiveVOMap.put(receiveDetail.getReceive().getReceiveId(),
					receiveVO);
		}
		invoiceVO.setReceiveVOList(new ArrayList<ReceiveVO>(receiveVOMap
				.values()));
		double invoiceAmount = 0;
		if (null != invoice.getInvoices() && invoice.getInvoices().size() > 0) {
			for (Invoice inv : invoice.getInvoices()) {
				invoiceAmount = null != inv.getInvoiceAmount() ? inv
						.getInvoiceAmount() + invoiceAmount : invoiceAmount;
			}
		}
		invoiceVO.setInvoiceAmount(invoice.getInvoiceAmount() + invoiceAmount);
		invoiceVO.setTotalInvoice(invoice.getTotalInvoice());
		invoiceVO.setPendingInvoice(AIOSCommons.roundDecimals(invoiceVO
				.getTotalInvoice() - invoiceVO.getInvoiceAmount()));
		return invoiceVO;
	}

	// Show purchase invoice alert
	public String showPurchaseInvoiceAlert() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showPurchaseInvoiceAlert");
			if (recordId != null && recordId > 0) {
				InvoiceVO vo = new InvoiceVO();
				Invoice invoice = invoiceBL.getInvoiceService().getInvoiceById(
						recordId);
				vo.setMessageId(alertId);
				vo.setObject(invoice);
				vo.setRecordId(recordId);
				vo.setUseCase(Invoice.class.getSimpleName());
				messageId = alertId;
				invoiceBL.invoiceDirectPaymentEntry(vo);
			}
			LOGGER.info("Module: Accounts : Method: showPurchaseInvoiceAlert: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showPurchaseInvoiceAlert Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Continuous Purchase invoice entry
	public String showContinuousPurchaseInvoiceEntry() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showContinuousPurchaseInvoiceEntry");

			Map<Byte, InvoiceStatus> invoiceStatus = new HashMap<Byte, InvoiceStatus>();
			for (InvoiceStatus e : EnumSet.allOf(InvoiceStatus.class)) {
				if (!("done").equalsIgnoreCase(e.name()))
					invoiceStatus.put(e.getCode(), e);
			}
			invoiceNumber = SystemBL.getReferenceStamp(Invoice.class.getName(),
					invoiceBL.getImplementationId());
			Invoice invoice = invoiceBL.getInvoiceService().getInvoiceInfoById(
					invoiceId);
			ServletActionContext.getRequest().setAttribute("INVOICE_INFO",
					addPurchaseInvoiceVO(invoice));
			ServletActionContext.getRequest().setAttribute("INVOICE_STATUS",
					invoiceStatus);
			List<Currency> currencies = invoiceBL.getReceiveBL()
					.getTransactionBL().getCurrencyService()
					.getAllCurrency(invoiceBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencies);
			LOGGER.info("Module: Accounts : Method: showContinuousPurchaseInvoiceEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showContinuousPurchaseInvoiceEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveContinuousPurchaseInvoice() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveContinuousPurchaseInvoice");
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Invoice invoice = new Invoice();
			invoice.setDate(DateFormat.convertStringToDate(date));
			Supplier supplier = new Supplier();
			supplier.setSupplierId(Long.parseLong(supplierId));
			invoice.setSupplier(supplier);
			invoice.setStatus(status);
			invoice.setInvoiceAmount(invoiceAmount);
			invoice.setProcessStatus(totalInvoice == invoiceAmount ? false
					: true);
			invoice.setImplementation(invoiceBL.getImplementation());
			Invoice parentInvoice = invoiceBL.getInvoiceService()
					.getInvoiceById(continousInvoiceId);
			invoice.setInvoice(parentInvoice);
			invoice.setCurrency(parentInvoice.getCurrency());
			invoice.setExchangeRate(parentInvoice.getExchangeRate());
			invoice.setInvoiceNumber(SystemBL.getReferenceStamp(
					Invoice.class.getName(), invoiceBL.getImplementation()));
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			invoice.setPerson(person);
			invoice.setCreatedDate(Calendar.getInstance().getTime());

			invoiceBL.saveInvoice(invoice);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: Accounts : Method: saveContinuousPurchaseInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: saveContinuousPurchaseInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showInvoiceAddEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showInvoiceAddEntry");
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		session.removeAttribute("RECEIVE_NOTES_SESSION_INFO_"
				+ sessionObj.get("jSessionId"));
		try {
			invoiceNumber = null;
			if (recordId != null && recordId > 0) {
				invoiceId = recordId;
			}
			if (invoiceId > 0) {
				Invoice invoice = invoiceBL.getInvoiceService()
						.getInvoiceInfoById(invoiceId);
				List<ReceiveDetail> supplierGRN = invoiceBL.getReceiveBL()
						.getReceiveService()
						.getSupplierGRN(Long.parseLong(supplierId));
				List<ReceiveVO> supplierActiveGRN = invoiceBL.getReceiveBL()
						.validateReceiveDetailInvoice(supplierGRN);
				Map<Long, ReceiveVO> receiveSession = new HashMap<Long, ReceiveVO>();
				for (ReceiveVO list : supplierActiveGRN) {
					receiveSession.put(list.getReceiveId(), list);
				}
				Map<Long, ReceiveVO> receiveVOList = new HashMap<Long, ReceiveVO>();
				ReceiveVO receiveVO = null;
				Set<Long> receiveIdSet = new HashSet<Long>();
				for (InvoiceDetail detail : invoice.getInvoiceDetails()) {
					receiveVO = receiveSession.get(detail.getReceiveDetail()
							.getReceive().getReceiveId());
					for (ReceiveDetailVO detailVO : receiveVO
							.getReceiveDetailsVO()) {
						if (detailVO.getReceiveDetailId().equals(
								detail.getReceiveDetail().getReceiveDetailId())) {
							detailVO.setSelectedInvQty(detail.getInvoiceQty());
							detailVO.setInvoiceQty(detail.getInvoiceQty());
							detailVO.setInvoiceDetailId(detail
									.getInvoiceDetailId());
						}
					}
					receiveVO.setReceiveDetailsVO(receiveVO
							.getReceiveDetailsVO());
					receiveSession.put(detail.getReceiveDetail().getReceive()
							.getReceiveId(), receiveVO);
					receiveVOList.put(
							detail.getReceiveDetail().getReceive()
									.getReceiveId(),
							receiveSession.get(detail.getReceiveDetail()
									.getReceive().getReceiveId()));
					receiveIdSet.add(detail.getReceiveDetail().getReceive()
							.getReceiveId());
				}
				if (receiveSession.size() > receiveIdSet.size()) {
					Collection<Long> similar = new HashSet<Long>(receiveIdSet);
					Collection<Long> different = new HashSet<Long>();
					different.addAll(receiveIdSet);
					different.addAll(receiveSession.keySet());
					similar.retainAll(receiveSession.keySet());
					different.removeAll(similar);
					for (Long nonReceiveId : different) {
						boolean removeFlag = false;
						if (receiveSession.containsKey(nonReceiveId)) {
							receiveVO = receiveSession.get(nonReceiveId);
							for (ReceiveDetailVO detailVO : receiveVO
									.getReceiveDetailsVO()) {
								if (detailVO.getInvoiceQty() == 0) {
									removeFlag = true;
								} else {
									removeFlag = false;
									break;
								}
							}
							if (removeFlag) {
								receiveSession.remove(nonReceiveId);
							}
						}
					}
				}
				supplierActiveGRN = new ArrayList<ReceiveVO>();
				for (ReceiveVO receives : receiveSession.values()) {
					supplierActiveGRN.add(receives);
				}
				session.setAttribute(
						"RECEIVE_NOTES_SESSION_INFO_"
								+ sessionObj.get("jSessionId"), receiveSession);
				ServletActionContext.getRequest().setAttribute("INVOICE_INFO",
						invoice);
				ServletActionContext.getRequest().setAttribute(
						"GOODS_RECEIVE_INFO", supplierActiveGRN);
				ServletActionContext.getRequest().setAttribute(
						"INVOICE_RECEIVE_DETAIL", receiveVOList.values());
			} else
				invoiceNumber = SystemBL.getReferenceStamp(
						Invoice.class.getName(),
						invoiceBL.getImplementationId());
			Map<Byte, InvoiceStatus> invoiceStatus = new HashMap<Byte, InvoiceStatus>();
			for (InvoiceStatus e : EnumSet.allOf(InvoiceStatus.class)) {
				if (!("done").equalsIgnoreCase(e.name()))
					invoiceStatus.put(e.getCode(), e);
			}
			List<Currency> currencies = invoiceBL.getReceiveBL()
					.getTransactionBL().getCurrencyService()
					.getAllCurrency(invoiceBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencies);
			ServletActionContext.getRequest().setAttribute("INVOICE_STATUS",
					invoiceStatus);
			LOGGER.info("Module: Accounts : Method: showInvoiceAddEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showInvoiceAddEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showInvoiceEditEntry() {
		LOGGER.info("Inside Module: Accounts : Method: showInvoiceAddEntry");
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		session.removeAttribute("RECEIVE_NOTES_SESSION_INFO_"
				+ sessionObj.get("jSessionId"));
		try {
			invoiceNumber = null;
			if (recordId != null && recordId > 0) {
				invoiceId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (invoiceId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = invoiceBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (invoiceId > 0) {
				Invoice invoiceEdit = invoiceBL.getInvoiceService()
						.getInvoiceInfoById(invoiceId);
				InvoiceVO invoice = invoiceBL
						.converttInvoiceEntityIntoVO(invoiceEdit);

				List<ReceiveDetail> supplierGRN = invoiceBL.getReceiveBL()
						.getReceiveService()
						.getSupplierGRN(invoice.getSupplier().getSupplierId());
				List<ReceiveVO> supplierActiveGRN = invoiceBL.getReceiveBL()
						.validateReceiveDetailInvoice(supplierGRN);
				Map<Long, ReceiveVO> receiveSession = new HashMap<Long, ReceiveVO>();
				for (ReceiveVO list : supplierActiveGRN) {
					receiveSession.put(list.getReceiveId(), list);
				}
				Map<Long, ReceiveVO> receiveVOList = new HashMap<Long, ReceiveVO>();
				ReceiveVO receiveVO = null;
				Set<Long> receiveIdSet = new HashSet<Long>();
				for (InvoiceDetail detail : invoice.getInvoiceDetails()) {
					receiveVO = receiveSession.get(detail.getReceiveDetail()
							.getReceive().getReceiveId());
					for (ReceiveDetailVO detailVO : receiveVO
							.getReceiveDetailsVO()) {
						if (detailVO.getReceiveDetailId().equals(
								detail.getReceiveDetail().getReceiveDetailId())) {
							detailVO.setSelectedInvQty(detail.getInvoiceQty());
							detailVO.setInvoiceQty(detail.getInvoiceQty());
							detailVO.setInvoiceDetailId(detail
									.getInvoiceDetailId());
						}
					}
					receiveVO.setReceiveDetailsVO(receiveVO
							.getReceiveDetailsVO());
					receiveSession.put(detail.getReceiveDetail().getReceive()
							.getReceiveId(), receiveVO);
					receiveVOList.put(
							detail.getReceiveDetail().getReceive()
									.getReceiveId(),
							receiveSession.get(detail.getReceiveDetail()
									.getReceive().getReceiveId()));
					receiveIdSet.add(detail.getReceiveDetail().getReceive()
							.getReceiveId());
				}
				if (receiveSession.size() > receiveIdSet.size()) {
					Collection<Long> similar = new HashSet<Long>(receiveIdSet);
					Collection<Long> different = new HashSet<Long>();
					different.addAll(receiveIdSet);
					different.addAll(receiveSession.keySet());
					similar.retainAll(receiveSession.keySet());
					different.removeAll(similar);
					for (Long nonReceiveId : different) {
						boolean removeFlag = false;
						if (receiveSession.containsKey(nonReceiveId)) {
							receiveVO = receiveSession.get(nonReceiveId);
							for (ReceiveDetailVO detailVO : receiveVO
									.getReceiveDetailsVO()) {
								if (detailVO.getInvoiceQty() == 0) {
									removeFlag = true;
								} else {
									removeFlag = false;
									break;
								}
							}
							if (removeFlag) {
								receiveSession.remove(nonReceiveId);
							}
						}
					}
				}
				supplierActiveGRN = new ArrayList<ReceiveVO>();
				for (ReceiveVO receives : receiveSession.values()) {
					supplierActiveGRN.add(receives);
				}
				session.setAttribute(
						"RECEIVE_NOTES_SESSION_INFO_"
								+ sessionObj.get("jSessionId"), receiveSession);
				ServletActionContext.getRequest().setAttribute("INVOICE_INFO",
						invoice);
				ServletActionContext.getRequest().setAttribute(
						"GOODS_RECEIVE_INFO", supplierActiveGRN);
				ServletActionContext.getRequest().setAttribute(
						"INVOICE_RECEIVE_DETAIL", receiveVOList.values());
			} else
				invoiceNumber = SystemBL.getReferenceStamp(
						Invoice.class.getName(),
						invoiceBL.getImplementationId());
			Map<Byte, InvoiceStatus> invoiceStatus = new HashMap<Byte, InvoiceStatus>();
			for (InvoiceStatus e : EnumSet.allOf(InvoiceStatus.class)) {
				if (!("done").equalsIgnoreCase(e.name()))
					invoiceStatus.put(e.getCode(), e);
			}
			List<Currency> currencies = invoiceBL.getReceiveBL()
					.getTransactionBL().getCurrencyService()
					.getAllCurrency(invoiceBL.getImplementation());
			for (Currency currency : currencies) {
				if (currency.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", currency.getCurrencyId());
					break;
				}
			}
			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencies);
			ServletActionContext.getRequest().setAttribute("INVOICE_STATUS",
					invoiceStatus);
			LOGGER.info("Module: Accounts : Method: showInvoiceAddEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showInvoiceAddEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showInvoicePrint() {
		LOGGER.info("Inside Module: Accounts : Method: showInvoicePrint");
		try {
			if (invoiceId > 0) {
				Invoice invoice = invoiceBL.getInvoiceService().getInvoiceById(
						invoiceId);
				List<InvoiceDetailVO> invoiceDetailVOs = new ArrayList<InvoiceDetailVO>();
				InvoiceDetailVO invoiceDetailVO = null;
				for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
					invoiceDetailVO = new InvoiceDetailVO();
					BeanUtils.copyProperties(invoiceDetailVO, invoiceDetail);
					invoiceDetailVOs.add(invoiceDetailVO);
				}

				Set<String> receiveNumbers = new HashSet<String>();
				Set<String> refNumbers = new HashSet<String>();
				for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
					receiveNumbers.add(invoiceDetail.getReceiveDetail()
							.getReceive().getReceiveNumber());
					if (null != invoiceDetail.getReceiveDetail().getReceive()
							.getReferenceNo())
						refNumbers.add(invoiceDetail.getReceiveDetail()
								.getReceive().getReferenceNo());
				}

				StringBuilder receiveNumber = new StringBuilder();
				for (String receiveNum : receiveNumbers)
					receiveNumber.append(receiveNum).append(" ,");

				StringBuilder refNumber = new StringBuilder();
				for (String refNum : refNumbers)
					refNumber.append(refNum).append(" ,");

				Collections.sort(invoiceDetailVOs,
						new Comparator<InvoiceDetail>() {
							public int compare(InvoiceDetail s1,
									InvoiceDetail s2) {
								return s1.getInvoiceDetailId().compareTo(
										s2.getInvoiceDetailId());
							}
						});
				ServletActionContext.getRequest().setAttribute(
						"INVOICE_INFO_PRINT", invoice);
				ServletActionContext.getRequest().setAttribute(
						"INVOICE_DETAIL_INFO_PRINT", invoiceDetailVOs);
				ServletActionContext.getRequest().setAttribute(
						"RECEIVE_NUMBER", receiveNumber);
				ServletActionContext.getRequest().setAttribute(
						"REFERENCE_NUMBER", refNumber);
			}
			LOGGER.info("Module: Accounts : Method: showInvoicePrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showInvoicePrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showContiniousInvoicePrint() {
		LOGGER.info("Inside Module: Accounts : Method: showContiniousInvoicePrint");
		try {
			if (invoiceId > 0) {
				Invoice invoice = invoiceBL.getInvoiceService()
						.getInvoiceFindById(invoiceId);
				StringBuilder receiveNumber = new StringBuilder();
				StringBuilder refNumber = new StringBuilder();
				List<Invoice> invoices = invoiceBL.getInvoiceService()
						.getChildInvoiceById(invoiceId);
				if (null != invoices && invoices.size() > 0) {
					Collections.sort(invoices, new Comparator<Invoice>() {
						public int compare(Invoice i1, Invoice i2) {
							return i2.getInvoiceNumber().compareTo(
									i1.getInvoiceNumber());
						}
					});
					invoice = invoiceBL.getInvoiceService()
							.getContinuousInvoiceById(
									invoices.get(0).getInvoiceId());
					invoice.setInvoiceDetails(new HashSet<InvoiceDetail>(
							invoice.getInvoice().getInvoiceDetails()));
					for (InvoiceDetail invoiceDetail : invoice
							.getInvoiceDetails()) {
						if (null != invoiceDetail.getReceiveDetail()) {
							receiveNumber.append(
									invoiceDetail.getReceiveDetail()
											.getReceive().getReceiveNumber())
									.append(", ");
							refNumber.append(null != invoiceDetail
									.getReceiveDetail().getReceive()
									.getReferenceNo() ? (invoiceDetail
									.getReceiveDetail().getReceive()
									.getReferenceNo() + (", ")) : "");
						}
					}
				} else {
					invoice = invoiceBL.getInvoiceService().getInvoiceById(
							invoiceId);
					for (InvoiceDetail invoiceDetail : invoice
							.getInvoiceDetails()) {
						if (null != invoiceDetail.getReceiveDetail()) {
							receiveNumber.append(
									invoiceDetail.getReceiveDetail()
											.getReceive().getReceiveNumber())
									.append(", ");
							refNumber.append(null != invoiceDetail
									.getReceiveDetail().getReceive()
									.getReferenceNo() ? (invoiceDetail
									.getReceiveDetail().getReceive()
									.getReferenceNo() + (", ")) : "");
						}
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"INVOICE_INFO_PRINT", invoice);
				ServletActionContext.getRequest().setAttribute(
						"RECEIVE_NUMBER", receiveNumber);
				ServletActionContext.getRequest().setAttribute(
						"REFERENCE_NUMBER", refNumber);
			}
			LOGGER.info("Module: Accounts : Method: showContiniousInvoicePrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showContiniousInvoicePrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Validate same invoice no exits already
	@SuppressWarnings("unused")
	private Boolean validateInvoiceNumber(String invoiceNumber,
			String tempInvoiceNumber, List<Invoice> invoiceList, long invoiceId)
			throws Exception {
		for (Invoice list : invoiceList) {
			if (invoiceId > 0) {
				if (!tempInvoiceNumber.equalsIgnoreCase(invoiceNumber)) {
					if (invoiceNumber.equalsIgnoreCase(list.getInvoiceNumber())) {
						return false;
					}
				} else {
					return true;
				}
			} else {
				if ((invoiceNumber).equalsIgnoreCase(list.getInvoiceNumber())) {
					return false;
				}
			}
		}
		return true;
	}

	public String saveInvoice() {
		LOGGER.info("Inside Module: Accounts : Method: saveInvoice");
		try {
			Invoice invoice = new Invoice();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (parentInvoiceId != null && parentInvoiceId > 0) {
				Invoice invoiceEdit = invoiceBL.getInvoiceService()
						.getInvoiceInfoById(invoiceId);
				invoiceEdit.setInvoiceAmount(invoiceAmount);
				invoiceBL.getInvoiceService().saveInvoice(invoiceEdit);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveInvoice: Action Success");
				return SUCCESS;
			}

			@SuppressWarnings("unchecked")
			Map<Long, ReceiveVO> receiveHash = (Map<Long, ReceiveVO>) (session
					.getAttribute("RECEIVE_NOTES_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			String[] receiveIdArray = splitValues(receiveDetail, "@#");
			List<ReceiveVO> receiveVOList = new ArrayList<ReceiveVO>();
			for (String receiveLine : receiveIdArray) {
				if (receiveHash.containsKey(Long.parseLong(receiveLine))) {
					ReceiveVO receiveVO = (ReceiveVO) receiveHash.get(Long
							.parseLong(receiveLine));
					receiveVOList.add(receiveVO);
				}
			}
			InvoiceDetail invoiceDetail = null;
			List<Receive> receiveList = new ArrayList<Receive>();
			Receive receive = null;
			Invoice invoiceEdit = null;
			if (invoiceId > 0) {
				invoiceEdit = invoiceBL.getInvoiceService().getInvoiceInfoById(
						invoiceId);
				invoice.setInvoiceNumber(invoiceEdit.getInvoiceNumber());
				invoice.setCreatedDate(invoiceEdit.getCreatedDate());
				invoice.setPerson(invoiceEdit.getPerson());
				invoice.setCurrency(invoiceEdit.getCurrency());
				invoice.setExchangeRate(invoiceEdit.getExchangeRate());
			} else {
				invoice.setInvoiceNumber(SystemBL.getReferenceStamp(
						Invoice.class.getName(),
						invoiceBL.getImplementationId()));
				invoice.setCreatedDate(Calendar.getInstance().getTime());
				User user = (User) sessionObj.get("USER");
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				invoice.setPerson(person);
			}
			if (invoiceId == 0
					|| (null != invoiceEdit && (long) invoiceEdit.getCurrency()
							.getCurrencyId() != currencyId)) {
				Currency defaultCurrency = invoiceBL.getReceiveBL()
						.getTransactionBL().getCurrency();
				if (null != defaultCurrency) {
					if ((long) defaultCurrency.getCurrencyId() != currencyId) {
						Currency currency = invoiceBL.getReceiveBL()
								.getTransactionBL().getCurrencyService()
								.getCurrency(currencyId);
						CurrencyConversion currencyConversion = invoiceBL
								.getReceiveBL().getTransactionBL()
								.getCurrencyConversionBL()
								.getCurrencyConversionService()
								.getCurrencyConversionByCurrency(currencyId);
						invoice.setCurrency(currency);
						invoice.setExchangeRate(null != currencyConversion ? currencyConversion
								.getConversionRate() : 1.0);
					} else {
						invoice.setCurrency(defaultCurrency);
						invoice.setExchangeRate(1.0);
					}
				}
			}
			invoice.setDate(DateFormat.convertStringToDate(date));
			invoice.setDescription(description);
			invoice.setStatus(status);
			invoice.setImplementation(invoiceBL.getImplementationId());
			if (parentInvoiceId != null && parentInvoiceId > 0) {
				Invoice parentInvoice = new Invoice();
				parentInvoice.setInvoiceId(parentInvoiceId);
				invoice.setInvoice(parentInvoice);
			} else {
				invoice.setTotalInvoice(totalInvoice);
			}
			invoice.setInvoiceAmount(invoiceAmount);
			if (totalInvoice > invoiceAmount)
				invoice.setProcessStatus(true);
			else
				invoice.setProcessStatus(false);
			List<InvoiceDetail> invoiceDetailList = null;
			Set<InvoiceDetail> invoiceDetailHash = new HashSet<InvoiceDetail>();
			List<InvoiceDetail> deleteInvoiceDetailList = null;
			for (ReceiveVO list : receiveVOList) {
				invoiceDetailList = new ArrayList<InvoiceDetail>();
				double totalAvailedQty = 0;
				double totalReceivedQty = list.getTotalReceiveQty();
				for (ReceiveDetailVO detail : list.getReceiveDetailsVO()) {

					invoiceDetail = new InvoiceDetail();
					invoiceDetail.setProduct(detail.getProduct());
					invoiceDetail.setInvoiceQty(detail.getReceiveQty());
					invoiceDetail.setDescription(detail.getDescription());
					invoiceDetail.setReceiveDetail(detail);
					/*
					 * if (invoiceId > 0)
					 * invoiceDetail.setInvoiceDetailId(detail
					 * .getInvoiceDetailId());
					 */
					invoiceDetailList.add(invoiceDetail);
					totalAvailedQty += (invoiceDetail.getInvoiceQty() + (null != detail
							.getInvoicedQty() ? detail.getInvoicedQty() : 0));
				}
				List<InvoiceDetail> invoiceDetails = new ArrayList<InvoiceDetail>(
						invoiceDetailList);
				for (InvoiceDetail detail : invoiceDetailList) {
					if (detail.getInvoiceQty() <= 0) {
						invoiceDetails.remove(detail);
					}
				}
				invoiceDetailHash.addAll(invoiceDetails);

				if (null != invoiceDetails
						&& invoiceDetails.size() > 0
						&& (totalReceivedQty == totalAvailedQty || totalInvoice > invoiceAmount)) {
					receive = new Receive();
					receive.setReceiveId(list.getReceiveId());
					receive.setReceiveNumber(list.getReceiveNumber());
					receive.setReferenceNo(list.getReferenceNo());
					receive.setReceiveDate(list.getReceiveDate());
					receive.setSupplier(list.getSupplier());
					receive.setImplementation(invoice.getImplementation());
					receive.setDescription(list.getDescription());
					receive.setStatus((byte) 0);
					receive.setIsApprove(list.getIsApprove());
					receiveList.add(receive);
				}
				invoice.setSupplier(list.getSupplier());
			}
			if (invoiceId > 0) {
				try {
					List<InvoiceDetail> invocieDetailList = this.getInvoiceBL()
							.getInvoiceService().getInvoiceDetails(invoiceId);
					Collection<Long> listOne = new ArrayList<Long>();
					Collection<Long> listTwo = new ArrayList<Long>();
					Map<Long, InvoiceDetail> hashInvoiceDetail = new HashMap<Long, InvoiceDetail>();
					if (null != invocieDetailList
							&& invocieDetailList.size() > 0) {
						for (InvoiceDetail list : invocieDetailList) {
							listOne.add(list.getInvoiceDetailId());
							hashInvoiceDetail.put(list.getInvoiceDetailId(),
									list);
						}
						if (null != hashInvoiceDetail
								&& hashInvoiceDetail.size() > 0) {
							for (InvoiceDetail list : invoiceDetailHash) {
								listTwo.add(list.getInvoiceDetailId());
							}
						}
					}
					Collection<Long> similar = new HashSet<Long>(listOne);
					Collection<Long> different = new HashSet<Long>();
					different.addAll(listOne);
					different.addAll(listTwo);
					similar.retainAll(listTwo);
					different.removeAll(similar);
					if (null != different && different.size() > 0) {
						InvoiceDetail tempInvoiceDetail = null;
						deleteInvoiceDetailList = new ArrayList<InvoiceDetail>();
						for (Long list : different) {
							if (null != list && !list.equals(0)) {
								tempInvoiceDetail = new InvoiceDetail();
								tempInvoiceDetail = hashInvoiceDetail.get(list);
								deleteInvoiceDetailList.add(tempInvoiceDetail);
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			List<Receive> receivesList = new ArrayList<Receive>(receiveList);
			if (null != deleteInvoiceDetailList
					&& deleteInvoiceDetailList.size() > 0) {
				for (InvoiceDetail invoiceDetails : deleteInvoiceDetailList) {
					if (null != receivesList && receivesList.size() > 0) {
						for (Receive receives : receivesList) {
							if (!receives.getReceiveId().equals(
									invoiceDetails.getReceiveDetail()
											.getReceive().getReceiveId())) {
								receives.setStatus((byte) 1);
								receiveList.add(receives);
							}
						}
					} else {
						invoiceDetails.getReceiveDetail().getReceive()
								.setStatus((byte) 1);
						receiveList.add(invoiceDetails.getReceiveDetail()
								.getReceive());
					}
				}
			}
			if (null != invoiceDetailHash && invoiceDetailHash.size() > 0) {
				invoice.setInvoiceDetails(invoiceDetailHash);
				invoiceBL.saveInvoice(invoice, receiveList,
						deleteInvoiceDetailList, invoiceEdit);
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveInvoice: Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Invoice Qty should be > 0";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: Accounts : Method: saveInvoice: Action Failure");
				return ERROR;
			}
		} catch (Exception ex) {
			returnMessage = "Internal Error";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteInvoice() {
		LOGGER.info("Inside Module: Accounts : Method: deleteInvoice");
		try {
			Invoice invoice = invoiceBL.getInvoiceService().getInvoiceById(
					invoiceId);
			Map<Long, Receive> receiveDeails = new HashMap<Long, Receive>();
			for (InvoiceDetail detail : invoice.getInvoiceDetails()) {
				detail.getReceiveDetail().getReceive().setStatus((byte) 1);
				receiveDeails
						.put(detail.getReceiveDetail().getReceive()
								.getReceiveId(), detail.getReceiveDetail()
								.getReceive());
			}
			invoiceBL.deleteInvoice(invoice, new ArrayList<Receive>(
					receiveDeails.values()));
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Internal Error";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: deleteInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show active invoice's for supplier payments by passing supplier id
	public String getSupplierActiveInvoice() {
		LOGGER.info("Inside Module: Accounts : Method: getSupplierActiveInvoice");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("INVOICE_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			List<InvoiceDetail> supplierInvoice = invoiceBL.getInvoiceService()
					.getSupplierActiveInvoice(Long.parseLong(supplierId));
			if (null != supplierInvoice && supplierInvoice.size() > 0) {
				List<InvoiceVO> supplierActiveInvoice = invoiceBL
						.validateInvoiceDetail(supplierInvoice);
				Map<Long, InvoiceVO> invoiceSession = new HashMap<Long, InvoiceVO>();
				for (InvoiceVO list : supplierActiveInvoice) {
					invoiceSession.put(list.getInvoiceId(), list);
				}
				session.setAttribute(
						"INVOICE_SESSION_INFO_" + sessionObj.get("jSessionId"),
						invoiceSession);
				ServletActionContext.getRequest().setAttribute("INVOICE_NOTES",
						supplierActiveInvoice);
				LOGGER.info("Module: Accounts : Method: getSupplierActiveInvoice Success");
				return SUCCESS;
			} else {
				LOGGER.info("Module: Accounts : Method: getSupplierActiveInvoice Error");
				return ERROR;
			}
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: getSupplierActiveInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Fetch Invoice Info from session thru invoice_id
	public String showSessionInvoiceInfo() {
		LOGGER.info("Inside Module: Accounts : Method: showSessionInvoiceInfo");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, InvoiceVO> invoiceHash = (Map<Long, InvoiceVO>) (session
					.getAttribute("INVOICE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			InvoiceVO receiveVO = (InvoiceVO) invoiceHash.get(invoiceId);
			ServletActionContext.getRequest().setAttribute("rowId", rowId);
			ServletActionContext.getRequest().setAttribute("INVOICE_NOTE_BYID",
					receiveVO);
			LOGGER.info("Module: Accounts : Method: showSessionInvoiceInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionInvoiceInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Fetch Receive Detail Info from session thru receive_id
	@SuppressWarnings("unchecked")
	public String showSessionInvoiceDetailInfo() {
		LOGGER.info("Inside Module: Accounts : Method: showSessionInvoiceDetailInfo");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<Long, InvoiceVO> invoiceHash = (Map<Long, InvoiceVO>) (session
					.getAttribute("INVOICE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			InvoiceVO invoiceVO = (InvoiceVO) invoiceHash.get(invoiceId);
			ServletActionContext.getRequest().setAttribute(
					"INVOICE_DETAIL_BYID", invoiceVO.getInvoiceDetailVOList());
			LOGGER.info("Module: Accounts : Method: showSessionInvoiceDetailInfo Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSessionInvoiceDetailInfo: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Update Invoice Detail Session id by invoice_id
	public String updateInvoiceSession() {
		LOGGER.info("Inside Module: Accounts : Method: updateInvoiceSession");
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			Map<Long, InvoiceVO> invoiceHash = (Map<Long, InvoiceVO>) (session
					.getAttribute("INVOICE_SESSION_INFO_"
							+ sessionObj.get("jSessionId")));
			InvoiceVO invoiceVO = (InvoiceVO) invoiceHash.get(invoiceId);
			String invoiceInfo[] = splitValues(invoiceDetail, "#@");
			InvoiceDetailVO invoiceDetailVO = null;
			List<InvoiceDetailVO> detailList = new ArrayList<InvoiceDetailVO>();
			Map<Long, InvoiceDetailVO> detailVOMap = new HashMap<Long, InvoiceDetailVO>();
			for (InvoiceDetailVO list : invoiceVO.getInvoiceDetailVOList()) {
				detailVOMap.put(list.getInvoiceDetailId(), list);
			}
			for (String invoice : invoiceInfo) {
				String invoiceLine[] = splitValues(invoice, "__");
				invoiceDetailVO = new InvoiceDetailVO();
				if (detailVOMap.containsKey(Long.parseLong(invoiceLine[1]))) {
					invoiceDetailVO = detailVOMap.get(Long
							.parseLong(invoiceLine[1]));
					if (!("##").equals(invoiceLine[0]))
						invoiceDetailVO.setDescription(invoiceLine[0]);
					detailList.add(invoiceDetailVO);
				}
			}
			invoiceVO.setInvoiceDetailVOList(detailList);
			invoiceHash.put(invoiceVO.getInvoiceId(), invoiceVO);
			LOGGER.info("Module: Accounts : Method: updateInvoiceSession Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: updateInvoiceSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showSupplierActiveInvoices() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showSupplierActiveInvoices");
			List<Invoice> invoices = invoiceBL.getInvoiceService()
					.getSupplierActiveInvoices(Long.parseLong(supplierId),
							currencyId);
			List<InvoiceVO> invoiceVOs = null;
			if (null != invoices && invoices.size() > 0) {
				invoiceVOs = new ArrayList<InvoiceVO>();
				InvoiceVO invoiceVO = null;
				Set<String> strset = null;
 				for (Invoice invoice : invoices) {
					invoiceVO = new InvoiceVO();
					strset = new HashSet<String>();
					BeanUtils.copyProperties(invoiceVO, invoice);
					if (null != invoice.getInvoiceDetails()
							&& invoice.getInvoiceDetails().size() > 0) {
						for (InvoiceDetail invDT : invoice.getInvoiceDetails())
							strset.add(
									invDT.getReceiveDetail().getReceive()
											.getReceiveNumber());
					} else {
						Invoice invoicePr = getParentInvoiceDetail(invoice
								.getInvoiceId());
						for (InvoiceDetail invDT : invoicePr
								.getInvoiceDetails())
							strset.add(
									invDT.getReceiveDetail().getReceive()
											.getReceiveNumber());
					}
					String str = " ("+StringUtils.join(strset, ',')+")"; 
					
					invoiceVO.setInvoiceReference(invoiceVO.getInvoiceNumber()
							.concat(str.toString()));
					invoiceVOs.add(invoiceVO);
				}
				java.util.Collections.sort(invoiceVOs,
						new Comparator<InvoiceVO>() {
							public int compare(InvoiceVO o1, InvoiceVO o2) {
								return o2.getInvoiceId().compareTo(
										o1.getInvoiceId());
							}
						});
			}
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INVOICE",
					invoiceVOs);
			LOGGER.info("Module: Accounts : Method: showSupplierActiveInvoices Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSupplierActiveInvoices: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Invoice getParentInvoiceDetail(long invoiceId) throws Exception {
		Invoice invoice = invoiceBL.getInvoiceService()
				.getBaseParentInvoiceById(invoiceId);
		if (null != invoice.getInvoice())
			return getParentInvoiceDetail(invoice.getInvoice().getInvoiceId());
		return invoice;
	}

	public String showInvoiceDataPayment() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: showInvoiceDataPayment");
			Invoice invoice = invoiceBL.getInvoiceService().getInvoiceFindById(
					invoiceId);
			invoiceVO = new InvoiceVO();
			invoiceVO.setInvoiceAmount(invoice.getInvoiceAmount());
			invoiceVO.setDescription(invoice.getDescription());
			invoiceVO.setInvoiceNumber(invoice.getInvoiceNumber());
			invoiceVO.setInvoiceId(invoice.getInvoiceId());
			invoiceVO.setUseCase(Invoice.class.getSimpleName());
			Supplier supplier = invoiceBL.getReceiveBL().getPurchaseBL()
					.getSupplierService()
					.getSupplierCombination(Long.parseLong(supplierId));
			invoiceVO.setCombinationId(supplier.getCombination()
					.getCombinationId());
			invoiceVO.setCombinationStr(supplier.getCombination()
					.getAccountByNaturalAccountId().getAccount()
					+ ""
					+ (null != supplier.getCombination()
							.getAccountByAnalysisAccountId() ? supplier
							.getCombination().getAccountByAnalysisAccountId()
							.getAccount() : ""));
			LOGGER.info("Module: Accounts : Method: showInvoiceDataPayment Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showInvoiceDataPayment: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public InvoiceBL getInvoiceBL() {
		return invoiceBL;
	}

	public void setInvoiceBL(InvoiceBL invoiceBL) {
		this.invoiceBL = invoiceBL;
	}

	public long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInvoiceLineDetails() {
		return invoiceLineDetails;
	}

	public void setInvoiceLineDetails(String invoiceLineDetails) {
		this.invoiceLineDetails = invoiceLineDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getTempInvoiceNumber() {
		return tempInvoiceNumber;
	}

	public void setTempInvoiceNumber(String tempInvoiceNumber) {
		this.tempInvoiceNumber = tempInvoiceNumber;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getReceiveDetail() {
		return receiveDetail;
	}

	public void setReceiveDetail(String receiveDetail) {
		this.receiveDetail = receiveDetail;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getInvoiceDetail() {
		return invoiceDetail;
	}

	public void setInvoiceDetail(String invoiceDetail) {
		this.invoiceDetail = invoiceDetail;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public double getTotalInvoice() {
		return totalInvoice;
	}

	public void setTotalInvoice(double totalInvoice) {
		this.totalInvoice = totalInvoice;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getContinousInvoiceId() {
		return continousInvoiceId;
	}

	public void setContinousInvoiceId(long continousInvoiceId) {
		this.continousInvoiceId = continousInvoiceId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getParentInvoiceId() {
		return parentInvoiceId;
	}

	public void setParentInvoiceId(Long parentInvoiceId) {
		this.parentInvoiceId = parentInvoiceId;
	}

	public InvoiceVO getInvoiceVO() {
		return invoiceVO;
	}

	public void setInvoiceVO(InvoiceVO invoiceVO) {
		this.invoiceVO = invoiceVO;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
}
