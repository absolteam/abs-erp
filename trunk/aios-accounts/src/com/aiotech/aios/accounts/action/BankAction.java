package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.service.bl.BankBL;
import com.aiotech.aios.common.to.Constants.Accounts.BankCardType;
import com.aiotech.aios.common.to.Constants.Accounts.InstitutionType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.CompanyVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BankAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(BankAction.class);

	private String id;
	Implementation implementation = null;
	private int rowId;
	private long recordId;

	private long bankId;
	private String bankName;
	private String bankNumber;
	private long combinationId;
	private String iban;
	private String routingNumber;
	private long bankContact;
	private String contactPerson;
	private String addEditFlag;
	private Long bankAccountId;
	private Bank bank;
	private String accountNumber;
	private byte accountType;
	private String accountPurpose;
	private Long branchContact;
	private String branchName;
	private String address;
	private Date createdDate;
	private Long createdBy;
	private String rountingNumber;
	private String bankAccountString;
	private Long currencyId;
	private Long signatory2;
	private Long signatory1;
	private Long modeOfOperation;
	private String swiftCode;
	private String holderId;
	private byte institutionType;
	private String returnMessage;
	private byte cardType;
	private String validFrom;
	private String validTo;
	private Integer csvNumber;
	private Integer pinNumber;
	private Double cardLimit;
	private String cardNumber;
	private String description;
	private Long issuingEntity;
	private String cardTypeName;
	private double creditLimit;
	private BankBL bankBL;
	private String showPage;
	private String cardTypeInfo;

	// redirect to success
	public String returnSuccess() {
		return SUCCESS;
	}

	// Show bank detail for add/update
	public String getBankListRedirect() {
		try {
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			List<LookupDetail> operationMode = this.getBankBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("MODE_OF_OPERATION", true);
			List<LookupDetail> signatories = this.getBankBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("SIGNATORIES", true);
			List<Currency> currencyList = this.getBankBL().getCurrencyService()
					.getAllCurrency(getImplementation());
			ServletActionContext.getRequest().setAttribute("MODE_OF_OPERATION",
					operationMode);
			ServletActionContext.getRequest().setAttribute("ACCOUNT_TYPES",
					bankBL.getBankAccountType());
			ServletActionContext.getRequest().setAttribute("SIGNATORIES",
					signatories);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// to get card info
	public String getCardDetails() {
		try {
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			List<LookupDetail> issuingEntities = this.getBankBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("CARD_ISSUING_ENTITIES", true);
			ServletActionContext.getRequest().setAttribute(
					"CARD_ISSUING_ENTITIES", issuingEntities);
			ServletActionContext.getRequest().setAttribute("BANK_CARD_TYPES",
					bankBL.getBankCardType());
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("AddEditFlag",
					addEditFlag);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// to get all person & company for account holder
	public String showAllPersonCompany() {
		LOGGER.info("Module: Accounts : Method: showAllPersonCompany");
		try {
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			List<Person> personList = this.getBankBL().getAllPersons(
					implementation);
			List<Company> companyList = this.getBankBL().getAllCompany(
					implementation);
			List<CompanyVO> companyVOList = this.getBankBL().listPersonCompany(
					personList, companyList);
			ServletActionContext.getRequest().setAttribute("COMPANY_PERSON",
					companyVOList);
			LOGGER.info("Module: Accounts : Method: showAllPersonCompany: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showAllPersonCompany: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// list all bank info
	public String getBankList() {
		try {
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			List<Bank> banks = bankBL.getBankService().getAllInsitutions(
					getImplementation());
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Bank list : banks) {
				array = new JSONArray();
				array.add(list.getBankId());
				array.add(InstitutionType.get(list.getInstitutionType()).name()
						.replaceAll("_", " "));
				array.add(list.getBankName());
				array.add(list.getPerson().getFirstName() + " "
						+ list.getPerson().getLastName());
				array.add(DateFormat.convertDateToString(list.getCreatedDate()
						.toString()));
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getBankInfo() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			// Removing Sessions
			session.removeAttribute("BANK_ACCOUNT_"
					+ sessionObj.get("jSessionId"));

			if (recordId > 0) {
				// Comment Information --Added by rafiq
				bankId = recordId;

				CommentVO comment = new CommentVO();
				if (bankId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(Bank.class.getName());
					comment = bankBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			Bank bank = null;
			List<BankAccount> bankAccounts = null;
			if (bankId != 0) {
				bank = bankBL.getBankService().getBankInfo(bankId);
				bankAccounts = new ArrayList<BankAccount>(
						bank.getBankAccounts());
			}
			ServletActionContext.getRequest().setAttribute("INSTITUTION_TYPES",
					bankBL.getInstitutionType());
			session.setAttribute(
					"BANK_ACCOUNT_" + sessionObj.get("jSessionId"),
					bankAccounts);
			ServletActionContext.getRequest()
					.setAttribute("BANK_DETAILS", bank);
			ServletActionContext.getRequest().setAttribute(
					"BANK_ACCOUNT_DETAILS", bankAccounts);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	// Get all bank Accounts
	public String showBankAccount() {
		LOGGER.info("Module: Accounts : Method: showBankAccount");
		try {
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			List<Bank> bankList = this
					.getBankBL()
					.getBankService()
					.getAllBanks(getImplementation(),
							InstitutionType.Bank.getCode());
			List<BankAccount> bankAccountList = this.getBankBL()
					.getBankService().getAllBankAccounts(getImplementation());
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			ServletActionContext.getRequest().setAttribute(
					"BANK_ACCOUNT_DETAILS", bankAccountList);
			ServletActionContext.getRequest().setAttribute("PAGE_VALUE",
					showPage);
			LOGGER.info("Module: Accounts : Method: showBankAccount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showBankAccount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// show all debit/credit card info
	public String showDebitCreditCard() {
		LOGGER.info("Module: Accounts : Method: showBankAccount");
		try {
			if (cardTypeInfo.equalsIgnoreCase("ccd"))
				cardType = BankCardType.Credit.getCode();
			else
				cardType = BankCardType.Debit.getCode();
			List<Bank> bankList = bankBL.getBankService()
					.getAllDebitCreditCard(
							bankBL.getTransactionBL().getImplemenationId(),
							InstitutionType.Credit_Unions.getCode(), cardType);
			ServletActionContext.getRequest().setAttribute("BANK_DETAILS",
					bankList);
			ServletActionContext.getRequest().setAttribute("PAGE_VALUE",
					showPage);
			LOGGER.info("Module: Accounts : Method: showDebitCreditCard: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showDebitCreditCard: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// save or update bank account
	@SuppressWarnings("unchecked")
	public String saveBankAccount() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankAccount bankaccount = new BankAccount();
			List<BankAccount> bankaccounts = null;
			Bank bank = new Bank();
			if (bankId != 0) {
				bank.setBankId(bankId);
				bankaccount.setBank(bank);
			}
			if (bankAccountId != null && bankAccountId > 0) {
				bankaccount.setBankAccountId(bankAccountId);
				BankAccount bankAccountToEdit = bankBL.getBankService()
						.getBankAccountInfo(bankAccountId);
				bankaccount.setCombination(bankAccountToEdit.getCombination());
				bankaccount.setCombinationByClearingAccount(bankAccountToEdit
						.getCombinationByClearingAccount());
			}
			bankaccount.setAccountNumber(accountNumber);
			bankaccount.setCreditLimit(creditLimit);
			bankaccount.setBankAccountHolder(holderId);
			bankaccount.setAccountPurpose(accountPurpose);
			bankaccount.setAccountType(accountType);
			bankaccount.setBranchName(branchName);
			bankaccount.setRountingNumber(rountingNumber);
			bankaccount.setIban(iban);
			bankaccount.setSwiftCode(swiftCode);
			LookupDetail lookupDetail = null;
			if (null != modeOfOperation && modeOfOperation > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(modeOfOperation);
				bankaccount.setLookupDetailByOperationMode(lookupDetail);
			}
			if (null != signatory1 && signatory1 > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(signatory1);
				bankaccount.setLookupDetailBySignatory1LookupId(lookupDetail);
			}

			if (null != signatory2 && signatory2 > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(signatory2);
				bankaccount.setLookupDetailBySignatory2LookupId(lookupDetail);
			}
			if (null != currencyId && currencyId > 0) {
				Currency currency = new Currency();
				currency.setCurrencyId(currencyId);
				bankaccount.setCurrency(currency);
			}
			bankaccount.setBranchContact(branchContact);
			bankaccount.setContactPerson(contactPerson);
			bankaccount.setAddress(address);
			if (session.getAttribute("BANK_ACCOUNT_"
					+ sessionObj.get("jSessionId")) == null) {
				bankaccounts = new ArrayList<BankAccount>();
			} else {
				bankaccounts = (List<BankAccount>) session
						.getAttribute("BANK_ACCOUNT_"
								+ sessionObj.get("jSessionId"));
			}
			if (null != addEditFlag && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				bankaccounts.add(bankaccount);
			} else if (null != addEditFlag && addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				bankaccounts.set(Integer.parseInt(id) - 1, bankaccount);
			} else if (null != addEditFlag && addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				if (null != bankaccounts
						&& bankaccounts.size() >= Integer.parseInt(id)) {
					bankaccounts.remove(Integer.parseInt(id) - 1);
					bankBL.getBankService().deleteBankAccount(bankaccount);
				}
			}
			session.setAttribute(
					"BANK_ACCOUNT_" + sessionObj.get("jSessionId"),
					bankaccounts);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	// save or update debit or credit card
	@SuppressWarnings("unchecked")
	public String saveBankDebitCredit() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			BankAccount bankaccount = new BankAccount();
			List<BankAccount> bankaccounts = null;
			Bank bank = new Bank();
			if (bankId != 0) {
				bank.setBankId(bankId);
				bankaccount.setBank(bank);
			}
			if (bankAccountId != null && bankAccountId > 0) {
				bankaccount.setBankAccountId(bankAccountId);
				BankAccount bankAccountToEdit = bankBL.getBankService()
						.getBankAccountInfo(bankAccountId);
				bankaccount.setCombination(bankAccountToEdit.getCombination());
				bankaccount.setCombinationByClearingAccount(bankAccountToEdit
						.getCombinationByClearingAccount());
			}
			bankaccount.setAccountNumber(accountNumber);
			bankaccount.setBankAccountHolder(holderId);
			bankaccount.setCreditLimit(creditLimit);
			bankaccount.setAccountPurpose(accountPurpose);
			bankaccount.setCardType(cardType);
			bankaccount.setValidFrom(DateFormat.convertStringToDate(validFrom));
			bankaccount.setValidTo(DateFormat.convertStringToDate(validTo));
			bankaccount.setCsvNumber(csvNumber);
			bankaccount.setPinNumber(pinNumber);
			bankaccount.setCardLimit(cardLimit);
			bankaccount.setCardNumber(cardNumber);
			bankaccount.setDescription(description);
			LookupDetail lookupDetail = null;
			if (issuingEntity > 0) {
				lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(issuingEntity);
				bankaccount.setLookupDetailByCardIssuingEntity(lookupDetail);
			}
			bankaccount.setBranchContact(branchContact);
			bankaccount.setContactPerson(contactPerson);
			if (session.getAttribute("BANK_ACCOUNT_"
					+ sessionObj.get("jSessionId")) == null) {
				bankaccounts = new ArrayList<BankAccount>();
			} else {
				bankaccounts = (List<BankAccount>) session
						.getAttribute("BANK_ACCOUNT_"
								+ sessionObj.get("jSessionId"));
			}
			if (null != addEditFlag && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				bankaccounts.add(bankaccount);
			} else if (null != addEditFlag && addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				bankaccounts.set(Integer.parseInt(id) - 1, bankaccount);
			} else if (null != addEditFlag && addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				if (null != bankaccounts
						&& bankaccounts.size() >= Integer.parseInt(id)) {
					bankaccounts.remove(Integer.parseInt(id) - 1);
					bankBL.getBankService().deleteBankAccount(bankaccount);
				}
			}
			session.setAttribute(
					"BANK_ACCOUNT_" + sessionObj.get("jSessionId"),
					bankaccounts);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String saveBank() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: saveBank: Action");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<BankAccount> bankaccounts = null;
			Bank bankToEdit = new Bank();
			Bank bank = new Bank();
			Combination combination = new Combination();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			bankaccounts = (ArrayList<BankAccount>) session
					.getAttribute("BANK_ACCOUNT_"
							+ sessionObj.get("jSessionId"));
			if (bankId != 0) {
				bank.setBankId(bankId);
				bankToEdit = bankBL.getBankService().getBankInfo(bankId);
				bank.setCreatedDate(bankToEdit.getCreatedDate());
				bank.setPerson(bankToEdit.getPerson());
			} else {
				bank.setCreatedDate(new Date());
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				bank.setPerson(person);
			}

			if (combinationId != 0) {
				combination.setCombinationId(combinationId);
				bank.setCombination(combination);
			}
			bank.setInstitutionType(institutionType);
			bank.setBankName(bankName);
			bank.setImplementation(getImplementation());
			bankBL.saveBank(bank, bankaccounts);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			session.removeAttribute("BANK_ACCOUNT_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: saveBank: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal Error.";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: Accounts : Method: saveBank: throws exception "
					+ ex);
			return ERROR;
		}
	}

	public String deleteBank() {
		try {
			bank = bankBL.getBankService().getBankInfo(bankId);
			bankBL.deleteBank(bank);
			ServletActionContext.getRequest().setAttribute("succMsg",
					"Record deleted.");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
					"Error occured in deletion.");
			return ERROR;
		}
	}

	public BankBL getBankBL() {
		return bankBL;
	}

	public void setBankBL(BankBL bankBL) {
		this.bankBL = bankBL;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public long getBankContact() {
		return bankContact;
	}

	public void setBankContact(long bankContact) {
		this.bankContact = bankContact;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountPurpose() {
		return accountPurpose;
	}

	public void setAccountPurpose(String accountPurpose) {
		this.accountPurpose = accountPurpose;
	}

	public Long getBranchContact() {
		return branchContact;
	}

	public void setBranchContact(Long branchContact) {
		this.branchContact = branchContact;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getRountingNumber() {
		return rountingNumber;
	}

	public void setRountingNumber(String rountingNumber) {
		this.rountingNumber = rountingNumber;
	}

	public String getBankAccountString() {
		return bankAccountString;
	}

	public void setBankAccountString(String bankAccountString) {
		this.bankAccountString = bankAccountString;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getSignatory2() {
		return signatory2;
	}

	public void setSignatory2(Long signatory2) {
		this.signatory2 = signatory2;
	}

	public Long getSignatory1() {
		return signatory1;
	}

	public void setSignatory1(Long signatory1) {
		this.signatory1 = signatory1;
	}

	public Long getModeOfOperation() {
		return modeOfOperation;
	}

	public void setModeOfOperation(Long modeOfOperation) {
		this.modeOfOperation = modeOfOperation;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getHolderId() {
		return holderId;
	}

	public void setHolderId(String holderId) {
		this.holderId = holderId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Double getCardLimit() {
		return cardLimit;
	}

	public void setCardLimit(Double cardLimit) {
		this.cardLimit = cardLimit;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIssuingEntity() {
		return issuingEntity;
	}

	public void setIssuingEntity(Long issuingEntity) {
		this.issuingEntity = issuingEntity;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getCardTypeInfo() {
		return cardTypeInfo;
	}

	public void setCardTypeInfo(String cardTypeInfo) {
		this.cardTypeInfo = cardTypeInfo;
	}

	public Integer getCsvNumber() {
		return csvNumber;
	}

	public void setCsvNumber(Integer csvNumber) {
		this.csvNumber = csvNumber;
	}

	public Integer getPinNumber() {
		return pinNumber;
	}

	public void setPinNumber(Integer pinNumber) {
		this.pinNumber = pinNumber;
	}

	public byte getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(byte institutionType) {
		this.institutionType = institutionType;
	}

	public byte getAccountType() {
		return accountType;
	}

	public void setAccountType(byte accountType) {
		this.accountType = accountType;
	}

	public void setCardType(byte cardType) {
		this.cardType = cardType;
	}

	public byte getCardType() {
		return cardType;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
}
