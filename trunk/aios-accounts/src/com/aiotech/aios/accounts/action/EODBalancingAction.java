package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.EODBalancing;
import com.aiotech.aios.accounts.domain.entity.EODBalancingDetail;
import com.aiotech.aios.accounts.domain.entity.EODProductionDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialWastageDetail;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.EODBalancingDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.EODBalancingVO;
import com.aiotech.aios.accounts.domain.entity.vo.EODProductionDetailVO;
import com.aiotech.aios.accounts.service.bl.EODBalancingBL;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class EODBalancingAction extends ActionSupport {

	private static final long serialVersionUID = -4251151905523050681L;

	private static final Logger log = Logger
			.getLogger(EODBalancingAction.class);

	// Dependency
	private EODBalancingBL eodBalancingBL;

	// Variables
	private String referenceNumber;
	private String returnMessage;
	private String currentDate;
	private String description;
	private String eodDetails;
	private String salesDate;
	private int rowId;
	private long posStoreId;
	private long eodBalancingId;
	private long posUserTillId;
	private long implementationId;
	private List<Object> aaData;

	// Return Success
	public String returnSuccess() {
		log.info("Inside Module: Accounts : Method: returnSuccess() : Action Success");
		return SUCCESS;
	}

	// Show All EOD Balancing
	public String showAllEODBalancing() {
		try {
			log.info("Inside Module: Accounts : Method: showAllEODBalancing");
			aaData = eodBalancingBL.showAllEODBalancing();
			log.info("Module: Accounts : Method: showAllEODBalancing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllEODBalancing Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show EOD Balancing Entry
	public String showEODBalancingEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showEODBalancingEntry()");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			referenceNumber = SystemBL.getReferenceStamp(
					EODBalancing.class.getName(),
					eodBalancingBL.getImplementation());
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			salesDate = DateFormat.convertSystemDateToString(Calendar
					.getInstance().getTime());
			List<Store> stores = null;
			if (null != posUserSession)
				posUserTillId = posUserSession.getPosUserTillId();
			else {
				posUserTillId = 0;
				stores = eodBalancingBL.getPointOfSaleBL().getStoreBL()
						.getStoreService()
						.getAllStores(eodBalancingBL.getImplementation());
			}
			ServletActionContext.getRequest().setAttribute("STORE_DETAILS",
					stores);
			implementationId = eodBalancingBL.getImplementation()
					.getImplementationId();
			log.info("Module: Accounts : Method: showEODBalancingEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showEODBalancingEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String eodBalancingPrint() {
		try {
			log.info("Inside Module: Accounts : Method: eodBalancingPrint()");
			EODBalancing eodBalancing = eodBalancingBL.getEodBalancingService()
					.getEODBalancingById(eodBalancingId);
			EODBalancingVO eodBalancingVO = new EODBalancingVO();
			BeanUtils.copyProperties(eodBalancingVO, eodBalancing);
			eodBalancingVO.setEodDate(DateFormat
					.convertDateToString(eodBalancing.getDate().toString())); 
			eodBalancingVO.setPosUserName(eodBalancing.getPerson()
					.getFirstName().concat(" ")
					+ eodBalancing.getPerson().getLastName());
			eodBalancingVO.setStoreName(eodBalancing.getStore()
					.getStoreName()); 
			List<EODBalancingDetailVO> eodBalancingDetails = new ArrayList<EODBalancingDetailVO>();
			List<EODProductionDetailVO> eodProductionDetailVOs = new ArrayList<EODProductionDetailVO>();
			EODBalancingDetailVO eodBalancingDetailVo = null;
			EODProductionDetailVO eodProductionDetailVO = null;
			for (EODBalancingDetail eodDetail : eodBalancing
					.getEODBalancingDetails()) {
				eodBalancingDetailVo = new EODBalancingDetailVO();
				BeanUtils.copyProperties(eodBalancingDetailVo, eodDetail);
				for (EODProductionDetail productionDetail : eodDetail
						.getEODProductionDetails()) {
					eodProductionDetailVO = new EODProductionDetailVO();
					BeanUtils.copyProperties(eodProductionDetailVO,
							productionDetail);
					eodProductionDetailVOs.add(eodProductionDetailVO);
				}
				eodBalancingDetailVo
						.setEodProductionDetailVOs(eodProductionDetailVOs);
				eodBalancingDetails.add(eodBalancingDetailVo);
			}
			eodBalancingVO.setEodBalancingDetailVOs(eodBalancingDetails);
			ServletActionContext.getRequest().setAttribute("EOD_DETAILS",
					eodBalancingVO);
			log.info("Module: Accounts : Method: eodBalancingPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: eodBalancingPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Save EOD Balancing
	public String performEODBalancing() {
		try {
			log.info("Inside Module: Accounts : Method: performEODBalancing");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			Store store = null;
			List<PointOfSale> pointOfSales = null;
			if (null != posUserSession) {
				store = posUserSession.getStore();
				pointOfSales = eodBalancingBL
						.getPointOfSaleBL()
						.getPointOfSaleService()
						.getPointOfSalesByStore(
								eodBalancingBL.getImplementation(),
								store.getStoreId(),
								InventorySubCategory.Craft0Manufacturer
										.getCode());
			} else {
				store = new Store();
				store.setStoreId(posStoreId);
			}
			if (null != pointOfSales && pointOfSales.size() > 0) {
				Map<Long, List<PointOfSaleDetail>> craftMap = new HashMap<Long, List<PointOfSaleDetail>>();
				List<PointOfSaleDetail> tempSalesDetails = null;
				for (PointOfSale pointOfSale : pointOfSales) {
					if (null != pointOfSale.getPointOfSaleDetails()
							&& pointOfSale.getPointOfSaleDetails().size() > 0) {
						for (PointOfSaleDetail saleDetail : pointOfSale
								.getPointOfSaleDetails()) {
							tempSalesDetails = new ArrayList<PointOfSaleDetail>();
							if (craftMap.containsKey(saleDetail
									.getProductByProductId().getProductId())) {
								tempSalesDetails.addAll(craftMap
										.get(saleDetail.getProductByProductId()
												.getProductId()));
							}
							tempSalesDetails.add(saleDetail);
							craftMap.put(saleDetail.getProductByProductId()
									.getProductId(), tempSalesDetails);
						}
					}
				}
				if (null != craftMap && craftMap.size() > 0) {
					// Create EOD
					EODBalancing eodBalancing = new EODBalancing();
					eodBalancing.setDate(Calendar.getInstance().getTime()); 
					eodBalancing.setImplementation(eodBalancingBL
							.getImplementation());
					eodBalancing.setReferenceNumber(SystemBL.getReferenceStamp(
							EODBalancing.class.getName(),
							eodBalancingBL.getImplementation()));

					// Create EOD Detail
					double craftQuantity = 0;
					double productionQuantity = 0;
					double acceptedWastage = 0;
					double productionWastage = 0;
					List<EODBalancingDetail> eodBalancingDetails = new ArrayList<EODBalancingDetail>();
					List<EODProductionDetail> eodDProductionDetails = new ArrayList<EODProductionDetail>();
					List<MaterialWastageDetail> wastageDetails = new ArrayList<MaterialWastageDetail>();
					List<MaterialWastageDetail> materialWastageDetails = eodBalancingBL
							.getMaterialWastageBL()
							.getMaterialWastageService()
							.getMaterialWastageDetailsByStore(
									store.getStoreId());
					Map<Long, List<MaterialWastageDetail>> wastageMap = null;
					if (null != materialWastageDetails
							&& materialWastageDetails.size() > 0) {
						List<MaterialWastageDetail> tempMaterialWastageDetails = null;
						wastageMap = new HashMap<Long, List<MaterialWastageDetail>>();
						for (MaterialWastageDetail materialWastageDetail : materialWastageDetails) {
							tempMaterialWastageDetails = new ArrayList<MaterialWastageDetail>();
							if (wastageMap.containsKey(materialWastageDetail
									.getProduct().getProductId()))
								tempMaterialWastageDetails.addAll(wastageMap
										.get(materialWastageDetail.getProduct()
												.getProductId()));
							tempMaterialWastageDetails
									.add(materialWastageDetail);
							wastageMap
									.put(materialWastageDetail.getProduct()
											.getProductId(),
											tempMaterialWastageDetails);
						}
					}
					EODBalancingDetail eodBalancingDetail = null;
					EODProductionDetail eodDProductionDetail = null;
					Product product = null;
					MaterialIdleMix materialIdleMix = null;
					for (Entry<Long, List<PointOfSaleDetail>> saleentry : craftMap
							.entrySet()) {
						craftQuantity = 0;
						tempSalesDetails = new ArrayList<PointOfSaleDetail>(
								saleentry.getValue());
						for (PointOfSaleDetail pointOfSaleDetail : tempSalesDetails) {
							craftQuantity += pointOfSaleDetail.getQuantity();
						}
						eodBalancingDetail = new EODBalancingDetail();
						product = new Product();
						product.setProductId(saleentry.getKey());
						eodBalancingDetail.setProduct(product);
						eodBalancingDetail.setProductQuantity(craftQuantity);
						eodBalancingDetail.setEODBalancing(eodBalancing);
						eodBalancingDetails.add(eodBalancingDetail);
						// Create EOD Production Detail
						materialIdleMix = eodBalancingBL
								.getMaterialIdleMixBL()
								.getMaterialIdleMixService()
								.getMaterialIdleMixbyProduct(saleentry.getKey());
						if (null != materialIdleMix) {
							for (MaterialIdleMixDetail mixDetail : materialIdleMix
									.getMaterialIdleMixDetails()) {
								productionWastage = 0;
								materialWastageDetails = new ArrayList<MaterialWastageDetail>();
								productionQuantity = mixDetail
										.getMaterialQuantity()
										* eodBalancingDetail
												.getProductQuantity();
								eodDProductionDetail = new EODProductionDetail();
								eodDProductionDetail
										.setEODBalancingDetail(eodBalancingDetail);
								eodDProductionDetail.setProduct(mixDetail
										.getProduct());
								acceptedWastage = ((mixDetail
										.getTypicalWastage() / mixDetail
										.getMaterialQuantity()) * productionQuantity);
								acceptedWastage = ((acceptedWastage * productionQuantity) / 100);
								eodDProductionDetail
										.setAcceptedWastage(AIOSCommons
												.roundDecimals(acceptedWastage));
								eodDProductionDetail
										.setProductQuantity(productionQuantity);
								// Check Production Wastage
								if (null != wastageMap && wastageMap.size() > 0) {
									materialWastageDetails = wastageMap
											.get(mixDetail.getProduct()
													.getProductId());
									if (null != materialWastageDetails
											&& materialWastageDetails.size() > 0) {
										for (MaterialWastageDetail materialWastageDetail : materialWastageDetails) {
											productionWastage += materialWastageDetail
													.getWastageQuantity();
										}
										wastageDetails
												.addAll(materialWastageDetails);
									}
								}
								eodDProductionDetail
										.setProductionWastage(productionWastage);
								eodDProductionDetails.add(eodDProductionDetail);
							}
						}
					}
					eodBalancingBL.saveEODProduction(eodBalancing,
							eodBalancingDetails, eodDProductionDetails,
							wastageDetails, pointOfSales, store);
				}
			}
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: performEODBalancing: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: performEODBalancing: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Getters & Setters
	public EODBalancingBL getEodBalancingBL() {
		return eodBalancingBL;
	}

	public void setEodBalancingBL(EODBalancingBL eodBalancingBL) {
		this.eodBalancingBL = eodBalancingBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getEodDetails() {
		return eodDetails;
	}

	public void setEodDetails(String eodDetails) {
		this.eodDetails = eodDetails;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getEodBalancingId() {
		return eodBalancingId;
	}

	public void setEodBalancingId(long eodBalancingId) {
		this.eodBalancingId = eodBalancingId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}

	public long getPosUserTillId() {
		return posUserTillId;
	}

	public void setPosUserTillId(long posUserTillId) {
		this.posUserTillId = posUserTillId;
	}

	public long getImplementationId() {
		return implementationId;
	}

	public void setImplementationId(long implementationId) {
		this.implementationId = implementationId;
	}

	public long getPosStoreId() {
		return posStoreId;
	}

	public void setPosStoreId(long posStoreId) {
		this.posStoreId = posStoreId;
	}
}
