package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.vo.CalendarVO;
import com.aiotech.aios.accounts.domain.entity.vo.PeriodVO;
import com.aiotech.aios.accounts.service.bl.CalendarBL;
import com.aiotech.aios.accounts.to.GlCalendarMasterTO;
import com.aiotech.aios.accounts.to.converter.GlCalendarMasterTOConverter;
import com.aiotech.aios.common.to.Constants.Accounts.PeriodStatus;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GlCalendarMasterAction extends ActionSupport {

	private static final long serialVersionUID = -8881537176278360483L;

	private static final Logger LOGGER = LogManager
			.getLogger(GlCalendarMasterAction.class);
	// Variable declaration

	private CalendarBL calendarBL;
	private String periodId;
	private String calendarId;
	private String name;
	private String startTime;
	private String endTime;
	private String extention;
	private String addEditFlag;
	private byte periodStatus;
	private String returnMessage;
	String jsonResult;
	String json;
	private String showPage;
	private byte status;
	private String periodDate;
	// result List
	private List<GlCalendarMasterTO> calenderList;
	private GlCalendarMasterTO calenderMasterto = null;
	private int rowId;
	private Long recordId;
	private Long messageId;
	private Integer processFlag;
	private long alertId;
	private List<Object> aaData;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("rowid", rowId);
		ServletActionContext.getRequest().setAttribute("AddEditFlag",
				addEditFlag);
		Map<Byte, String> periodStatus = new HashMap<Byte, String>();
		for (PeriodStatus status : EnumSet.allOf(PeriodStatus.class))
			periodStatus.put(status.getCode(),
					status.name().replaceAll("_", " "));
		ServletActionContext.getRequest().setAttribute("PERIOD_STATUS",
				periodStatus);
		recordId = null;
		return SUCCESS;
	}

	public String execute() {
		try {
			calenderList = new ArrayList<GlCalendarMasterTO>();
			calenderMasterto = new GlCalendarMasterTO();
			// Call Implementation
			List<Calendar> calendar = calendarBL.getCalendarService()
					.getAllCalendar(calendarBL.getImplementation());
			calenderList = GlCalendarMasterTOConverter
					.convertToTOList(calendar);
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (GlCalendarMasterTO list : calenderList) {
				array = new JSONArray();
				array.add(list.getCalendarId());
				array.add(list.getName());
				array.add(list.getStartTime());
				array.add(list.getEndTime());
				array.add(list.getCalendarStatus());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Show Selected Date Period
	@SuppressWarnings("unchecked")
	public String showSelectedDatePeriod() {
		try {
			LOGGER.info("Module: Accounts :  Method showSelectedDatePeriod() inside");
			DateTimeFormatter formatter = DateTimeFormat
					.forPattern("dd-MMM-yyyy");
			aaData = (List<Object>) calendarBL.showSelectedDatePeriod(true,
					new DateTime(formatter.parseDateTime(periodDate)));
			if (null != aaData && aaData.size() > 0) {
				LOGGER.info("Module: Accounts :  Method showSelectedDatePeriod() Action Success");
				return SUCCESS;
			}
			returnMessage = "No access for this period.";
			LOGGER.info("Module: Accounts :  Method showSelectedDatePeriod() Action Error");
			return ERROR;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showSelectedDatePeriod: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showCalendar() {
		try {
			LOGGER.info("Inside method: showCalendar");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Calendar> calendarList = this.getCalendarBL()
					.getCalendarService()
					.getAllApprovedCalendar(calendarBL.getImplementation());
			Calendar calendar = null;
			List<Period> periodList = null;
			List<GlCalendarMasterTO> periodDetail = null;
			boolean flag = true;
			for (Calendar list : calendarList) {
				if (flag) {
					if (list.getStatus() == 1) {
						calendarId = String.valueOf(list.getCalendarId());
						flag = false;
					}
				}
			}
			if (!flag) {
				calendar = this.getCalendarBL().getCalendarService()
						.getCalendarDetails(new Long(calendarId));
				periodList = calendarBL.getCalendarService().getPeriodDetails(
						calendar.getCalendarId());
				calenderMasterto = GlCalendarMasterTOConverter
						.convertToTO(calendar);
				if (periodList != null && periodList.size() > 0) {
					Collections.sort(periodList, new Comparator<Period>() {
						public int compare(Period o1, Period o2) {
							return o1.getStartTime().compareTo(o2.getEndTime());
						}
					});
					session.setAttribute(
							"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"),
							periodList);
					periodDetail = new ArrayList<GlCalendarMasterTO>();
					for (Period period : periodList) {
						GlCalendarMasterTO calendarPeriod = GlCalendarMasterTOConverter
								.convertPeroidToTO(period);
						periodDetail.add(calendarPeriod);
					}
				}
				if (periodDetail != null)
					Collections.sort(periodDetail,
							new Comparator<GlCalendarMasterTO>() {
								public int compare(GlCalendarMasterTO m1,
										GlCalendarMasterTO m2) {
									return m1.getPeriodStartDate().compareTo(
											m2.getPeriodStartDate());
								}
							});
				ServletActionContext.getRequest().setAttribute(
						"CALENDAR_DETAILS", calenderMasterto);
				ServletActionContext.getRequest().setAttribute(
						"PERIOD_DETAILS", periodDetail);
			}
			Map<Byte, String> periodStatus = new HashMap<Byte, String>();
			for (PeriodStatus status : EnumSet.allOf(PeriodStatus.class))
				periodStatus.put(status.getCode(),
						status.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("FINANCIAL_STATUS",
					flag);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_YEAR",
					calendarList);
			ServletActionContext.getRequest().setAttribute("PERIOD_STATUS",
					periodStatus);
			showPage = "add";
			ServletActionContext.getRequest().setAttribute(showPage, showPage);
			LOGGER.info("Method: showCalendar success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Method: showCalendar Exception" + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCalendarRejectEntry() {
		try {
			LOGGER.info("Inside method: showCalendarRejectEntry");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Calendar> calendarList = this.getCalendarBL()
					.getCalendarService()
					.getAllCalendar(calendarBL.getImplementation());
			Calendar calendar = null;
			List<Period> periodList = null;
			List<GlCalendarMasterTO> periodDetail = null;
			if (null != recordId) {
				calendar = calendarBL.getCalendarService()
						.getCalendarDetails(recordId);
				periodList = calendarBL.getCalendarService().getPeriodDetails(
						calendar.getCalendarId());
				calenderMasterto = GlCalendarMasterTOConverter
						.convertToTO(calendar);
				if (periodList != null && periodList.size() > 0) {
					Collections.sort(periodList, new Comparator<Period>() {
						public int compare(Period o1, Period o2) {
							return o1.getStartTime().compareTo(o2.getEndTime());
						}
					});
					session.setAttribute(
							"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"),
							periodList);
					periodDetail = new ArrayList<GlCalendarMasterTO>();
					for (Period period : periodList) {
						GlCalendarMasterTO calendarPeriod = GlCalendarMasterTOConverter
								.convertPeroidToTO(period);
						periodDetail.add(calendarPeriod);
					}
				}
				if (periodDetail != null)
					Collections.sort(periodDetail,
							new Comparator<GlCalendarMasterTO>() {
								public int compare(GlCalendarMasterTO m1,
										GlCalendarMasterTO m2) {
									return m1.getPeriodStartDate().compareTo(
											m2.getPeriodStartDate());
								}
							});
				ServletActionContext.getRequest().setAttribute(
						"CALENDAR_DETAILS", calenderMasterto);
				ServletActionContext.getRequest().setAttribute(
						"PERIOD_DETAILS", periodDetail);
			}
			Map<Byte, String> periodStatus = new HashMap<Byte, String>();
			for (PeriodStatus status : EnumSet.allOf(PeriodStatus.class))
				periodStatus.put(status.getCode(),
						status.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("FINANCIAL_STATUS",
					true);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_YEAR",
					calendarList);
			ServletActionContext.getRequest().setAttribute("PERIOD_STATUS",
					periodStatus);
			CommentVO comment = new CommentVO();
			if (recordId != null && calendar != null
					&& calendar.getCalendarId() != 0 && recordId > 0) {
				comment.setRecordId(calendar.getCalendarId());
				comment.setUseCase(Calendar.class.getName());
				comment = calendarBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			showPage = "add";
			ServletActionContext.getRequest().setAttribute(showPage, showPage);
			LOGGER.info("Method: showCalendarRejectEntry success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Method: showCalendarRejectEntry Exception" + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCustomiseCalendar() {
		try {
			LOGGER.info("Inside method: showCustomiseCalendar");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Calendar> calendarList = calendarBL.getCalendarService()
					.getAllCalendar(calendarBL.getImplementation());
			Calendar calendar = null;
			List<Period> periodList = null;
			List<GlCalendarMasterTO> periodDetail = null;
			boolean flag = true;
			for (Calendar list : calendarList) {
				if (flag) {
					if (list.getStatus() == 1) {
						calendarId = String.valueOf(list.getCalendarId());
						flag = false;
					}
				}
			}
			if (!flag) {
				calendar = calendarBL.getCalendarService().getCalendarDetails(
						new Long(calendarId));
				periodList = this.getCalendarBL().getCalendarService()
						.getPeriodDetails(calendar.getCalendarId());
				Collections.sort(periodList, new Comparator<Period>() {
					public int compare(Period o1, Period o2) {
						return o1.getStartTime().compareTo(o2.getEndTime());
					}
				});
				calenderMasterto = GlCalendarMasterTOConverter
						.convertToTO(calendar);
				if (periodList != null && periodList.size() > 0) {
					session.setAttribute(
							"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"),
							periodList);
					periodDetail = new ArrayList<GlCalendarMasterTO>();
					for (Period period : periodList) {
						GlCalendarMasterTO calendarPeriod = GlCalendarMasterTOConverter
								.convertPeroidToTO(period);
						periodDetail.add(calendarPeriod);
					}
				}
				Collections.sort(periodDetail,
						new Comparator<GlCalendarMasterTO>() {
							public int compare(GlCalendarMasterTO m1,
									GlCalendarMasterTO m2) {
								return m1.getPeriodStartDate().compareTo(
										m2.getPeriodStartDate());
							}
						});
				ServletActionContext.getRequest().setAttribute(
						"CALENDAR_DETAILS", calenderMasterto);
				ServletActionContext.getRequest().setAttribute(
						"PERIOD_DETAILS", periodDetail);
			}
			Map<Byte, String> periodStatus = new HashMap<Byte, String>();
			for (PeriodStatus status : EnumSet.allOf(PeriodStatus.class))
				periodStatus.put(status.getCode(),
						status.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("PERIOD_STATUS",
					periodStatus);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_STATUS",
					flag);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_YEAR",
					calendarList);
			showPage = "add-customise";
			ServletActionContext.getRequest().setAttribute(showPage, showPage);
			LOGGER.info("Method: showCustomiseCalendar success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Method: showCustomiseCalendar Exception" + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get the calendar & period details for add & edit
	public String getCalendarDetails() {
		calenderMasterto = new GlCalendarMasterTO();
		List<GlCalendarMasterTO> calenderMasterList = null;
		try {
			List<Period> periods = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			if (calendarId != null && !calendarId.equalsIgnoreCase("")) {
				Calendar calendarDetails = this.getCalendarBL()
						.getCalendarService()
						.getCalendarDetails(Long.parseLong(calendarId));
				calenderMasterto = GlCalendarMasterTOConverter
						.convertToTO(calendarDetails);
				ServletActionContext.getRequest().setAttribute(
						"CALENDAR_DETAILS", calenderMasterto);
				periods = this.getCalendarBL().getCalendarService()
						.getPeriodDetails(Long.parseLong(calendarId));
				session.setAttribute(
						"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"),
						periods);
				if (periods != null && periods.size() > 0) {
					calenderMasterList = new ArrayList<GlCalendarMasterTO>();
					for (Period period : periods) {
						calenderMasterto = GlCalendarMasterTOConverter
								.convertPeroidToTO(period);
						calenderMasterList.add(calenderMasterto);
					}
				}
				Collections.sort(calenderMasterList,
						new Comparator<GlCalendarMasterTO>() {
							public int compare(GlCalendarMasterTO m1,
									GlCalendarMasterTO m2) {
								return m1.getPeriodStartDate().compareTo(
										m2.getPeriodStartDate());
							}
						});
				ServletActionContext.getRequest().setAttribute(
						"PERIOD_DETAILS", calenderMasterList);
			}
			Map<Byte, String> periodStatus = new HashMap<Byte, String>();
			for (PeriodStatus status : EnumSet.allOf(PeriodStatus.class))
				periodStatus.put(status.getCode(),
						status.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("PERIOD_STATUS",
					periodStatus);
			ServletActionContext.getRequest().setAttribute("PAGE-VALUE",
					showPage);

			List<Calendar> calendarList = calendarBL.getCalendarService()
					.getAllCalendar(calendarBL.getImplementation());
			boolean flag = true;
			for (Calendar list : calendarList) {
				if (flag) {
					if (list.getStatus() == 1) {
						flag = false;
					}
				}
			}
			ServletActionContext.getRequest().setAttribute("FINANCIAL_STATUS",
					flag);
			ServletActionContext.getRequest().setAttribute("FINANCIAL_YEAR",
					calendarList);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg1",
					"Calendar Add Failure");
			return SUCCESS;
		}

	}

	// Save the period on session add
	@SuppressWarnings("unchecked")
	public String savePeriodAdd() {
		rowId = 0;
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			calenderMasterto = new GlCalendarMasterTO();
			List<Period> periods = null;
			Period period = new Period();
			period.setName(name);
			period.setStartTime(DateFormat.convertStringToDate(startTime));
			period.setEndTime(DateFormat.convertStringToDate(endTime));
			period.setStatus(periodStatus);
			if (!extention.equals(""))
				period.setExtention(Integer.parseInt(extention));

			if (session.getAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId")) == null) {
				periods = new ArrayList<Period>();
			} else {
				periods = (List<Period>) session.getAttribute("GL_PERIOD_INFO_"
						+ sessionObj.get("jSessionId"));
			}
			periods.add(period);
			session.setAttribute(
					"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"), periods);
			LOGGER.info("period success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save the period on session add
	@SuppressWarnings("unchecked")
	public String savePeriodEdit() {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			calenderMasterto = new GlCalendarMasterTO();
			List<Period> periods = null;
			Period period = new Period();

			if (periodId != null && !periodId.trim().equals("")
					&& !periodId.trim().equals("0"))
				period.setPeriodId(Long.parseLong(periodId));

			period.setName(name);
			period.setStartTime(DateFormat.convertStringToDate(startTime));
			period.setEndTime(DateFormat.convertStringToDate(endTime));
			period.setStatus(periodStatus);
			if (!extention.equals(""))
				period.setExtention(Integer.parseInt(extention));

			periods = (List<Period>) session.getAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			if (null != periods && periods.size() > 0) {
				for (Period periodTemp : periods) {
					if ((String.valueOf(periodTemp.getPeriodId())
							.equals(periodId))) {
						BeanUtils.copyProperties(periodTemp, period);
						break;
					}
				}
			}
			session.setAttribute(
					"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"), periods);
			LOGGER.info("period edit success");
			ServletActionContext.getRequest().setAttribute("Success",
					"Period Edit success");
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("Error",
					"Period Edit failure");
			return ERROR;
		}
	}

	public String periodDateValidation() {
		LOGGER.info("Inside method periodDateValidation");
		try {
			if (null != periodDate && !periodDate.equals("")) {
				Period period = calendarBL.getCalendarService()
						.getPeriodFromDate(calendarBL.getImplementation(),
								DateFormat.convertStringToDate(periodDate));
				LOGGER.info("Date val "
						+ DateFormat.convertStringToDate(periodDate));
				if (null != period && !period.equals("")) {
					returnMessage = "SUCCESS " + "#@" + period.getName()
							+ " #@" + period.getPeriodId();
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return SUCCESS;
				} else {
					returnMessage = "Selected period not accessible.";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return ERROR;
				}
			} else {
				returnMessage = "Date shoudn't be void.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry!Internal error. " + ex;
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	// delete the period on session
	@SuppressWarnings("unchecked")
	public String deletePeriod() {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<Period> periods = null;
			periods = (List<Period>) session.getAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			if (periods != null) {
				int index = rowId - 1;
				periods.remove(index);
			}
			session.setAttribute(
					"GL_PERIOD_INFO_" + sessionObj.get("jSessionId"), periods);
			LOGGER.info("period delete success");
			ServletActionContext.getRequest().setAttribute("Success",
					"Period delete success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("Error",
					"Period delete failure");
			return ERROR;
		}
	}

	// add/edit the calendar

	public String saveCalendarAdd() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Calendar calendar = new Calendar();
			List<Period> periodList = null;

			if (calendarId != null && !calendarId.trim().equals("")
					&& !calendarId.trim().equals("0")) {
				calendar.setCalendarId(Long.parseLong(calendarId));
				periodList = calendarBL.getCalendarService().getPeriodDetails(
						Long.parseLong(calendarId));
			}
			calendar.setName(name);
			calendar.setStartTime(DateFormat.convertStringToDate(startTime));
			calendar.setEndTime(DateFormat.convertStringToDate(endTime));
			calendar.setImplementation(calendarBL.getImplementation());
			calendar.setStatus(status); 
			@SuppressWarnings("unchecked")
			List<Period> periods = (ArrayList<Period>) session
					.getAttribute("GL_PERIOD_INFO_"
							+ sessionObj.get("jSessionId"));
			if (status == (byte) PeriodStatus.Close.getCode()) {
				for (Period period : periods) {
					if ((byte) period.getStatus() != (byte) PeriodStatus.Close
							.getCode()) {
						returnMessage = "All periods should be closed to inactivate the calendar.(Failure)";
						ServletActionContext.getRequest().setAttribute(
								"RETURN_MESSAGE", returnMessage);
						return SUCCESS;
					}
				}
			}
			List<Period> deletePeriodList = null;
			if (null != calendar.getCalendarId()) {
				deletePeriodList = new ArrayList<Period>();
				Collection<Long> listOne = new ArrayList<Long>();
				Collection<Long> listTwo = new ArrayList<Long>();
				Map<Long, Period> hashPeriod = new HashMap<Long, Period>();
				if (null != periodList && periodList.size() > 0) {
					for (Period list : periodList) {
						listOne.add(list.getPeriodId());
						hashPeriod.put(list.getPeriodId(), list);
					}
					if (null != periods && periods.size() > 0) {
						for (Period list : periods) {
							listTwo.add(list.getPeriodId());
						}
					}
				}
				Collection<Long> similar = new HashSet<Long>(listOne);
				Collection<Long> different = new HashSet<Long>();
				different.addAll(listOne);
				different.addAll(listTwo);
				similar.retainAll(listTwo);
				different.removeAll(similar);
				if (null != different && different.size() > 0) {
					Period tempPeriod = null;

					for (Long list : different) {
						if (null != list && !list.equals(0)) {
							tempPeriod = new Period();
							tempPeriod = hashPeriod.get(list);
							deletePeriodList.add(tempPeriod);
						}
					}
				}
			}
			List<Period> openPeriods = new ArrayList<Period>();
			List<Period> closePeriods = new ArrayList<Period>();
			List<Period> tempClosePeriods = new ArrayList<Period>();
			if (null != calendarId && !calendarId.trim().equals("")
					&& !calendarId.trim().equals("0")) {
				Map<Long, Period> periodMap = new HashMap<Long, Period>();
				if (null != periodList && periodList.size() > 0) {
					for (Period list : periodList) {
						periodMap.put(list.getPeriodId(), list);
					}
				}
				Period tempPeriod = null;
				for (Period sessionList : periods) {
					if (periodMap.containsKey(sessionList.getPeriodId())) {
						tempPeriod = periodMap.get(sessionList.getPeriodId());
						if ((byte) tempPeriod.getStatus() != (byte) sessionList
								.getStatus()
								&& (byte) sessionList.getStatus() == (byte) PeriodStatus.Close
										.getCode()) {
							closePeriods.add(tempPeriod);
						}
						if ((byte) tempPeriod.getStatus() != (byte) sessionList
								.getStatus()
								&& (byte) sessionList.getStatus() == (byte) PeriodStatus.Temporary_Closed
										.getCode()) {
							tempClosePeriods.add(tempPeriod);
						}
						if ((byte) sessionList.getStatus() == (byte) PeriodStatus.Open
								.getCode()) {
							openPeriods.add(tempPeriod);
						}
					}
				}
			}
			if (null != closePeriods && closePeriods.size() > 0
					&& null != calendar.getCalendarId()) {
				Map<Long, Period> closedPeriodMap = new HashMap<Long, Period>();
				for (Period period : closePeriods) {
					if (null != period.getPeriodId())
						closedPeriodMap.put(period.getPeriodId(), period);
				}
				for (Entry<Long, Period> closeEntry : closedPeriodMap
						.entrySet()) {
					Period period = closeEntry.getValue();
					List<Period> fiscalPeriods = calendarBL
							.getCalendarService().getNonClosedPeriodDetails(
									calendar.getCalendarId(),
									period.getStartTime(), period.getEndTime());
					for (Period fiscalPeriod : fiscalPeriods) {
						if (!closedPeriodMap.containsKey(fiscalPeriod
								.getPeriodId())) {
							returnMessage = "Period should be closed in order.(Failure)";
							ServletActionContext.getRequest().setAttribute(
									"RETURN_MESSAGE", returnMessage);
							return ERROR;
						}
					}
				}
			}
			User user = (User) sessionObj.get("USER"); 
			calendarBL.saveCalendar(calendar, periods, openPeriods,
					closePeriods, deletePeriodList, user, tempClosePeriods);
			session.removeAttribute("GL_PERIOD_INFO_"
					+ sessionObj.get("jSessionId"));
			if (calendarId != null && !calendarId.trim().equals("")
					&& !calendarId.trim().equals("0"))
				returnMessage = "Record updated.(Success)";
			else
				returnMessage = "Record created.(Success)";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Failure";
			returnMessage = "Sorry!Internal error " + ex + " (Failure)";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}

	}

	// Calendar Approval Screen showCalendarApprovalScreen
	public String showCalendarApprovalScreen() {
		try {
			Calendar calendar = calendarBL.getCalendarService()
					.getFinancialYearById(recordId);
			CalendarVO calendarVO = new CalendarVO();
			BeanUtils.copyProperties(calendarVO, calendar);
			calendarVO.setStatusStr((byte) calendar.getStatus() == 1 ? "Active"
					: "Inactive");
			calendarVO.setStart(DateFormat.convertDateToString(calendar
					.getStartTime().toString()));
			calendarVO.setEnd(DateFormat.convertDateToString(calendar
					.getEndTime().toString()));
			PeriodVO periodVO = null;
			List<PeriodVO> periodVOs = new ArrayList<PeriodVO>();
			for (Period period : calendar.getPeriods()) {
				periodVO = new PeriodVO();
				BeanUtils.copyProperties(periodVO, period);
				periodVO.setStart(DateFormat.convertDateToString(period
						.getStartTime().toString()));
				periodVO.setEnd(DateFormat.convertDateToString(period
						.getEndTime().toString()));
				periodVO.setStatusStr(PeriodStatus.get(period.getStatus())
						.name());
				periodVOs.add(periodVO);
			}
			Collections.sort(periodVOs, new Comparator<PeriodVO>() {
				public int compare(PeriodVO o1, PeriodVO o2) {
					return o1.getStartTime().compareTo(o2.getEndTime());
				}
			});
			calendarVO.setPeriodVOs(periodVOs);
			ServletActionContext.getRequest().setAttribute("CALENDAR_DETAILS",
					calendarVO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// delete the calendar
	public String deleteCalendar() {
		try {
			LOGGER.info("Inside method deleteCalendar");
			if (calendarId != null && !calendarId.equals("")
					&& new Long(calendarId) > 0) {
				Calendar calendar = this.getCalendarBL().getCalendarService()
						.getCalendarDetails(Long.parseLong(calendarId));
				List<Period> periodList = this.getCalendarBL()
						.getCalendarService()
						.getPeriodDetails(Long.parseLong(calendarId));
				if (null != periodList && periodList.size() > 0) {
					returnMessage = "Financial Year have periods, can't be deleted.(Failure)";
					LOGGER.info("Calendar Deletion Failure! " + returnMessage);
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return ERROR;
				} else {
					this.getCalendarBL().getCalendarService()
							.deleteCalendar(calendar);
					returnMessage = "Financial Year Deleted.(Success)";
					LOGGER.info("Calendar Deleted Successfully!");
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					return SUCCESS;
				}
			} else {
				returnMessage = "Select a financial year to delete.(Failure)";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Calendar Deletion Failure! " + returnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error " + ex + " (Failure)";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	// validate calendar
	public String validateCalendar() {
		try {
			LOGGER.info("Inside method validateCalendar");
			List<Calendar> calendar = calendarBL.getCalendarService()
					.getAllCalendar(calendarBL.getImplementation());
			if (null != calendar && calendar.size() > 0) {
				returnMessage = "Financial Year already exits.";
				LOGGER.info("Validate Calendar process Failure! "
						+ returnMessage);
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				return ERROR;
			} else {
				returnMessage = "success";
				LOGGER.info("Validate Calendar process success!");
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				return SUCCESS;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnMessage = "Sorry! Internal error " + ex;
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	// cancel the calendar
	public String calendarCancel() {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		HttpSession session = ServletActionContext.getRequest().getSession();
		ServletActionContext.getRequest().setAttribute("succMsg1",
				"No changes made");
		session.removeAttribute("GL_PERIOD_INFO_"
				+ sessionObj.get("jSessionId"));
		return SUCCESS;
	}

	public List<GlCalendarMasterTO> getCalenderList() {
		return calenderList;
	}

	public String getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}

	public String getJson() {
		json = jsonResult;
		return json;
	}

	public String getPeriodId() {
		return periodId;
	}

	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getExtention() {
		return extention;
	}

	public void setExtention(String extention) {
		this.extention = extention;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public byte getPeriodStatus() {
		return periodStatus;
	}

	public void setPeriodStatus(byte periodStatus) {
		this.periodStatus = periodStatus;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(String periodDate) {
		this.periodDate = periodDate;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public CalendarBL getCalendarBL() {
		return calendarBL;
	}

	public void setCalendarBL(CalendarBL calendarBL) {
		this.calendarBL = calendarBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Integer getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Integer processFlag) {
		this.processFlag = processFlag;
	}
}
