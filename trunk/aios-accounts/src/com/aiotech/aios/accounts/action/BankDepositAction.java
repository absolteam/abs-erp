package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.BankDepositVO;
import com.aiotech.aios.accounts.service.bl.BankDepositBL;
import com.aiotech.aios.common.to.Constants.Accounts.ReceiptType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BankDepositAction extends ActionSupport {

	private static final long serialVersionUID = 5745589918071509826L;
	private static final Logger LOGGER = LogManager
			.getLogger(BankDepositAction.class);
	private BankDepositBL bankDepositBL;
	private String paymentFrom;
	private Long bankDepositId;
	private String bankDepositNumber;
	private String date;
	private String description;
	private String bankDepositDetails;
	private String returnMessage;
	private Long recordId;
	private String useCase;
	private long alertId;
	private long depositerId;
	private long bankAccountId;

	public String returnSuccess() {
		try {
			LOGGER.info("Inside Method returnSuccess");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("BANK_PAID_RECEIPTS_SESSION_"
					+ sessionObj.get("jSessionId"));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showAllBankReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all bank deposits
	public String bankDepositList() {
		try {
			JSONObject jsonResponse = new JSONObject();
			List<BankDeposit> bankDeposits = bankDepositBL
					.getBankDepositService().getAllBankDeposit(
							getImplementation());
			JSONArray data = new JSONArray();
			if (null != bankDeposits && bankDeposits.size() > 0) {
				for (BankDeposit list : bankDeposits) {
					JSONArray array = new JSONArray();
					array.add(list.getBankDepositId());
					array.add(list.getBankDepositNumber());
					array.add(DateFormat.convertDateToString(list.getDate()
							.toString()));
					array.add(list
							.getPersonByDepositerId()
							.getFirstName()
							.concat(" ")
							.concat(list.getPersonByDepositerId().getLastName()));
					array.add(list.getDescription());
					array.add(list.getPersonByCreatedBy().getFirstName()
							.concat(" ")
							.concat(list.getPersonByCreatedBy().getLastName()));
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info(ex);
			return ERROR;
		}
	}

	// Bank Deposit Entry Show
	public String showBankDeposit() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			bankDepositNumber = SystemBL.getReferenceStamp(
					BankDeposit.class.getName(),
					bankDepositBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
					bankDepositBL.getBankReceiptsBL().getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("POSRECEIPT_TYPES",
					bankDepositBL.getBankReceiptsBL().getPOSReceiptType());
			List<Store> stores = bankDepositBL.getBankReceiptsBL()
					.getSalesInvoiceBL().getSalesDeliveryNoteBL().getStockBL()
					.getStoreBL().getStoreService()
					.getAllStores(bankDepositBL.getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"RECEIPT_TYPES",
					bankDepositBL.getLookupMasterBL().getActiveLookupDetails(
							"Receipt_Types", true));
			ServletActionContext.getRequest().setAttribute("RECEIPT_SOURCE",
					bankDepositBL.getBankReceiptsBL().getReceiptSource());
			ServletActionContext.getRequest().setAttribute("STORE_DETAILS",
					stores);
			Map<Byte, String> receiptTypes = new HashMap<Byte, String>();
			for (ReceiptType e : EnumSet.allOf(ReceiptType.class)) {
				receiptTypes.put(e.getCode(), e.name());
			}
			ServletActionContext.getRequest().setAttribute("RECEIPT_TYPES",
					receiptTypes);
			long tempid = -1;
			AIOSCommons.removeUploaderSession(session, "depositDocs", tempid,
					"BankDeposit");
			if (null != recordId && recordId > 0) {
				ServletActionContext.getRequest().setAttribute("RECORD_ID",
						recordId);
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Bank Deposits
	public String saveBankDeposit() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			BankDeposit bankDeposit = new BankDeposit();
			bankDeposit.setBankDepositNumber(SystemBL.getReferenceStamp(
					BankDeposit.class.getName(),
					bankDepositBL.getImplementation()));
			bankDeposit.setDate(DateFormat.convertStringToDate(date));
			bankDeposit.setDescription(description);
			Person depositer = new Person();
			depositer.setPersonId(depositerId);
			bankDeposit.setPersonByDepositerId(depositer);
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			bankDeposit.setPersonByCreatedBy(person);
			bankDeposit.setCreatedDate(Calendar.getInstance().getTime());
			BankAccount bankAccount = bankDepositBL.getBankBL()
					.getBankService().getBankAccountDetails(bankAccountId);
			bankDeposit.setBankAccount(bankAccount);
			bankDeposit.setImplementation(getImplementation());
			BankDepositDetail bankDepositDetail = null;
			BankReceiptsDetail bankReceiptsDetail = null;
			List<BankDepositDetail> bankDepositDetailList = new ArrayList<BankDepositDetail>();
			String[] lineDetailArray = splitValues(bankDepositDetails, "@@");
			List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
			PointOfSaleReceipt pointOfSaleReceipt = null;
			List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
			for (String detail : lineDetailArray) {
				String[] detailArray = splitValues(detail, "__");
				bankDepositDetail = new BankDepositDetail();
				bankAccount = new BankAccount();
				bankDepositDetail.setAmount(AIOSCommons
						.formatAmountToDouble(detailArray[0]));
				if (null != detailArray[1] && !("##").equals(detailArray[1]))
					bankDepositDetail.setDescription(detailArray[1]);
				if (null != detailArray[2]
						&& Long.parseLong(detailArray[2]) > 0) {
					bankReceiptsDetail = bankDepositBL
							.getBankReceiptsBL()
							.getBankReceiptsService()
							.getBankReceiptsDetailById(
									Long.parseLong(detailArray[2]));
					bankReceiptsDetail.setIsDeposited(true);
					bankReceiptsDetails.add(bankReceiptsDetail);
					bankDepositDetail.setBankReceiptsDetail(bankReceiptsDetail);
					bankDepositDetail.setReceiptType(bankReceiptsDetail
							.getPaymentMode());
				}
				if (null != detailArray[3] && !("##").equals(detailArray[3]))
					bankDepositDetail.setUseCase(detailArray[3]);
				if (null != detailArray[4]
						&& Long.parseLong(detailArray[4]) > 0)
					bankDepositDetail.setRecordId(Long
							.parseLong(detailArray[4]));
				if (null != bankDepositDetail.getUseCase()
						&& ("PointOfSaleReceipt").equals(bankDepositDetail
								.getUseCase())) {
					pointOfSaleReceipt = bankDepositBL
							.getPointOfSaleBL()
							.getPointOfSaleService()
							.getPointOfSaleReceiptById(
									bankDepositDetail.getRecordId());
					bankDepositDetail.setReceiptType(pointOfSaleReceipt
							.getReceiptType());
					pointOfSaleReceipt.setIsDeposited(true);
					pointOfSaleReceipts.add(pointOfSaleReceipt);
				}
				bankDepositDetailList.add(bankDepositDetail);
			}
			bankDepositBL.saveBankDepositTransaction(bankDeposit,
					bankDepositDetailList, alertId, bankReceiptsDetails,
					pointOfSaleReceipts);
			session.removeAttribute("BANK_PAID_RECEIPTS_SESSION_"
					+ sessionObj.get("jSessionId"));
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Internal error.");
			return ERROR;
		}
	}

	// Delete Bank Deposit
	public String deleteBankDeposit() {
		try {
			BankDeposit bankDeposit = this.getBankDepositBL()
					.getBankDepositService().getBankDepositInfo(bankDepositId);
			bankDepositBL.deleteBankDeposit(bankDeposit);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Internal error.");
			return ERROR;
		}
	}

	public String viewBankDeposit() throws Exception {
		if (recordId != null && recordId > 0) {
			bankDepositId = recordId;
			// Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if (bankDepositId > 0) {
				comment.setRecordId(recordId);
				comment.setUseCase(DirectPayment.class.getName());
				comment = bankDepositBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
		}
		List<BankDepositVO> bankDepositVOs = new ArrayList<BankDepositVO>();
		BankDeposit bankDeposit = bankDepositBL.getBankDepositService()
				.getBankDepositInfo(bankDepositId);
		if (bankDeposit != null && bankDeposit.getBankDepositDetails() != null) {
			for (BankDepositDetail bankDepositDetail : bankDeposit
					.getBankDepositDetails()) {
				BankDepositVO bdVo = new BankDepositVO();
				bdVo.setBankDepositDetail(bankDepositDetail);
				bankDepositVOs.add(bdVo);
			}
		}
		ServletActionContext.getRequest().setAttribute("BANK_DEPOSIT",
				bankDeposit);
		ServletActionContext.getRequest().setAttribute("BANK_DEPOSIT_DETAIL",
				bankDepositVOs);

		return SUCCESS;
	}

	// Split the values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public BankDepositBL getBankDepositBL() {
		return bankDepositBL;
	}

	public void setBankDepositBL(BankDepositBL bankDepositBL) {
		this.bankDepositBL = bankDepositBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public String getPaymentFrom() {
		return paymentFrom;
	}

	public void setPaymentFrom(String paymentFrom) {
		this.paymentFrom = paymentFrom;
	}

	public Long getBankDepositId() {
		return bankDepositId;
	}

	public void setBankDepositId(Long bankDepositId) {
		this.bankDepositId = bankDepositId;
	}

	public String getBankDepositNumber() {
		return bankDepositNumber;
	}

	public void setBankDepositNumber(String bankDepositNumber) {
		this.bankDepositNumber = bankDepositNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBankDepositDetails() {
		return bankDepositDetails;
	}

	public void setBankDepositDetails(String bankDepositDetails) {
		this.bankDepositDetails = bankDepositDetails;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public long getDepositerId() {
		return depositerId;
	}

	public void setDepositerId(long depositerId) {
		this.depositerId = depositerId;
	}

	public long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
}
