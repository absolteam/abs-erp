﻿package com.aiotech.aios.accounts.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.TenderDetail;
import com.aiotech.aios.accounts.service.bl.TenderBL;
import com.aiotech.aios.accounts.to.TenderTO;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class TenderAction extends ActionSupport {

	private static final long serialVersionUID = -3456225431711595397L;
	private TenderBL tenderBL;
	private Long tenderId;
	private long currencyId;
	private String tenderNumber;
	private String tenderDate;
	private String tenderExpiry;
	private String tenderDetails;
	private Integer rowId = 0;
	private String description;
	private String returnMessage;

	private String fromDate;
	private String toDate;
	private String quoted;
	private String productId;
	private InputStream fileInputStream;
	private List<Product> productList;
	private Long recordId;

	private List<TenderTO> tenderDS;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	public String returnSuccess() {
		return SUCCESS;
	}

	public String execute() {
		try {
			List<Tender> tenders = tenderBL.getTenderService()
					.getUnQuotedTenders(getImplementation());
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Tender tender : tenders) {
				array = new JSONArray();
				array.add(tender.getTenderId());
				array.add(tender.getTenderNumber());
				array.add(DateFormat.convertDateToString(tender.getTenderDate()
						.toString()));
				array.add(DateFormat.convertDateToString(tender
						.getTenderExpiry().toString()));
				array.add(tender.getCurrency().getCurrencyId());
				array.add(tender.getCurrency().getCurrencyPool().getCode());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveTender() {
		try {
			Tender tender = new Tender();
			Currency currency = new Currency();
			if (this.getTenderBL().validateTender(tenderDetails)) {
				if (tenderId == null || tenderId == 0) {
					tenderId = null;
					tenderNumber = SystemBL.getReferenceStamp(
							Tender.class.getName(), getImplementation());
				}
				tender.setTenderId(tenderId);
				tender.setTenderDate(DateFormat.convertStringToDate(tenderDate));
				tender.setTenderExpiry(DateFormat
						.convertStringToDate(tenderExpiry));
				tender.setTenderNumber(tenderNumber);
				tender.setDescription(description);
				currency.setCurrencyId(currencyId);
				tender.setCurrency(currency);
				tender.setImplementation(getImplementation());
				List<TenderDetail> tenderDetailList = new ArrayList<TenderDetail>();
				TenderDetail tenderDetail;
				Product product;
				String[] purchaseDetailList = splitValues(tenderDetails, "#@");
				try {
					for (String string : purchaseDetailList) {
						String[] detailString = splitValues(string, "__");
						tenderDetail = new TenderDetail();
						product = new Product();
						product.setProductId(Long.parseLong(detailString[0]));
						tenderDetail.setProduct(product);
						tenderDetail.setUnitRate(detailString[1]);
						tenderDetail.setQuantity(detailString[2]);
						if (Long.parseLong(detailString[3]) > 0l)
							tenderDetail.setTenderDetailId(Long
									.parseLong(detailString[3]));
						else
							tenderDetail.setTenderDetailId(null);
						if (!detailString[4].equalsIgnoreCase("##"))
							tenderDetail.setDescription(detailString[4]);
						tenderDetailList.add(tenderDetail);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				tender.setTenderDetails(new HashSet<TenderDetail>(
						tenderDetailList));
				tenderBL.saveTender(tender);
				returnMessage = "SUCCESS";
				return SUCCESS;
			} else {
				returnMessage = "Product Duplicated, Please Check.";
				return ERROR;
			}
		} catch (Exception e) {
			returnMessage = "Failure";
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getTenderList() {

		try {
			ServletActionContext.getRequest().setAttribute(
					"TENDERS_LIST",
					tenderBL.getTenderService().getUnQuotedTenders(
							getImplementation()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String showTenderDetail() {
		try {
			List<TenderDetail> tenderDetails = tenderBL.getTenderService()
					.getTenderDetails(tenderId);
			ServletActionContext.getRequest().setAttribute(
					"TENDERS_DETAIL_LIST", tenderDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public String getTenderForEdit() {

		try {
			if (recordId != null && recordId > 0) {
				tenderId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (tenderId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = tenderBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			List<Currency> currencies = tenderBL.getCurrencyService()
					.getAllCurrency(getImplementation());
			ServletActionContext.getRequest().setAttribute("TENDER_CURRENCY",
					currencies);
			for (Currency curList : currencies) {
				if (curList.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", curList.getCurrencyId());
				}
			}
			if (null == tenderId || tenderId == 0) {
				ServletActionContext.getRequest().setAttribute(
						"TENDER_NUMBER",
						SystemBL.getReferenceStamp(Tender.class.getName(),
								getImplementation()));
				return SUCCESS;
			}

			Tender tender = tenderBL.getTenderService().getTenderById(tenderId);
			List<TenderDetail> tenderDetails = tenderBL.getTenderService()
					.getTenderDetails(tender.getTenderId());
			currencyId = tender.getCurrency().getCurrencyId();
			ServletActionContext.getRequest().setAttribute("TENDER", tender);
			ServletActionContext.getRequest().setAttribute(
					"TENDER_tenderDate",
					DateFormat.convertDateToString(tender.getTenderDate()
							.toString()));
			ServletActionContext.getRequest().setAttribute(
					"TENDER_tenderExpiry",
					DateFormat.convertDateToString(tender.getTenderExpiry()
							.toString()));
			ServletActionContext.getRequest().setAttribute(
					"TENDER_DETAIL_INFO", tenderDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String deleteTender() {
		try {
			if (tenderId != null && tenderId != 0) {
				Tender tender = tenderBL.getTenderService().getTenderBasicById(
						tenderId);
				tenderBL.deleteTender(tender, new ArrayList<TenderDetail>(
						tender.getTenderDetails()));
				returnMessage = "SUCCESS";
			}
		} catch (Exception e) {
			returnMessage = "FAILURE";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String tenderOrderAddRow() {
		ServletActionContext.getRequest().setAttribute("rowId", rowId);
		return SUCCESS;
	}

	public String showTenderReportPage() {

		try {
			productList = tenderBL.getProductService().getAllProduct(
					(Implementation) (ServletActionContext.getRequest()
							.getSession().getAttribute("THIS")));
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// tender filter search
	public String tenderFilterSearch() {
		try {

			Date fromDatee = null;
			Date toDatee = null;
			Boolean isQuoted = null;
			Long product = null;

			if (!fromDate.equalsIgnoreCase("undefined")
					&& !fromDate.equalsIgnoreCase("null")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (!toDate.equalsIgnoreCase("undefined")
					&& !toDate.equalsIgnoreCase("null")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}
			if (!quoted.equalsIgnoreCase("undefined")
					&& !quoted.equalsIgnoreCase("null")) {
				isQuoted = Boolean.parseBoolean(quoted);
			}
			if (!productId.equalsIgnoreCase("undefined")
					&& !productId.equalsIgnoreCase("null")) {
				product = Long.parseLong(productId);
			}

			JSONObject jsonResponse = new JSONObject();

			List<Tender> tenders = tenderBL.getTenderService()
					.getTendersByFilterValues(fromDatee, toDatee, product,
							isQuoted, getImplementation());

			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Tender tender : tenders) {
				array = new JSONArray();
				array.add(tender.getTenderId());
				array.add(tender.getTenderNumber());
				array.add(tender.getTenderDate().toString());
				array.add(tender.getTenderExpiry().toString());
				array.add("$");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String tenderFilterSearchReport() {

		Tender tender = tenderBL.getTenderService()
				.getTenderByIdWithDifferentChilds(tenderId, true, true, false);

		currencyId = tender.getCurrency().getCurrencyId();

		ServletActionContext.getRequest().setAttribute(
				"TENDER_tenderDate",
				DateFormat.convertDateToString(tender.getTenderDate()
						.toString()));
		ServletActionContext.getRequest().setAttribute(
				"TENDER_tenderExpiry",
				DateFormat.convertDateToString(tender.getTenderExpiry()
						.toString()));

		ServletActionContext.getRequest().setAttribute("TENDER_INFO", tender);
		return SUCCESS;
	}

	// Tender filter search for report as XLS
	public String tenderFilterSearchReportXLS() {
		try {

			Tender tender = tenderBL.getTenderService()
					.getTenderByIdWithDifferentChilds(tenderId, true, true,
							false); // tenderService.getTenderById(tenderId);
			/*
			 * List<TenderDetail> tenderDetails = tenderService
			 * .getTenderDetails(tender);
			 */

			String str = "";

			String fromDatee = "-NA-";
			String toDatee = "-NA-";
			String isQuoted = "-NA-";
			String product = "-NA-";

			if (!fromDate.equalsIgnoreCase("undefined")
					&& !fromDate.equalsIgnoreCase("null")) {
				fromDatee = fromDate;
			}
			if (!toDate.equalsIgnoreCase("undefined")
					&& !toDate.equalsIgnoreCase("null")) {
				toDatee = toDate;
			}
			if (!quoted.equalsIgnoreCase("undefined")
					&& !quoted.equalsIgnoreCase("null")) {
				isQuoted = quoted;
			}
			if (!productId.equalsIgnoreCase("undefined")
					&& !productId.equalsIgnoreCase("null")) {
				product = productId;
			}

			str = "Tender Details\n\n";
			str += "Filter Condition\n";
			str += "From Date,To Date,Product,Is Quoted\n";
			str += fromDatee + "," + toDatee + "," + product + "," + isQuoted
					+ "\n\n";

			str += "\n Tender No., " + tender.getTenderNumber();
			str += "\n Date, "
					+ DateFormat.convertDateToString(tender.getTenderDate()
							.toString())
					+ ", Expiry, "
					+ DateFormat.convertDateToString(tender.getTenderExpiry()
							.toString());
			str += "\n Currency, "
					+ tender.getCurrency().getCurrencyPool().getCode()
					+ ", Description, " + tender.getDescription();

			if (tender.getTenderDetails() != null
					&& tender.getTenderDetails().size() > 0) {
				str += "\n\n Item Type, Product, UOM , Unit Rate, Quantity, Total Amount, Description";
				str += "\n";
				for (TenderDetail tenderDetail : tender.getTenderDetails()) {
					str += ItemType
							.get(tenderDetail.getProduct().getItemType())
							.name()
							+ ","
							+ tenderDetail.getProduct().getProductName()
							+ ","
							+ tenderDetail.getProduct()
									.getLookupDetailByProductUnit()
									.getDisplayName()
							+ ","
							+ tenderDetail.getUnitRate()
							+ ","
							+ tenderDetail.getQuantity()
							+ ","
							+ Double.parseDouble(tenderDetail.getUnitRate())
							* Double.parseDouble(tenderDetail.getQuantity())
							+ ",";
					if (tenderDetail.getDescription() != null) {
						str += tenderDetail.getDescription() + ",";
					} else {
						str += ",";
					}

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public String returnSuccessReport() {
		try {

			/*
			 * ContractFeeType[] feeTypes = Constants.RealEstate.ContractFeeType
			 * .values(); ServletActionContext.getRequest()
			 * .setAttribute("FEETYPES", feeTypes);
			 * 
			 * ServletActionContext.getRequest().setAttribute("rowid", id);
			 * ServletActionContext.getRequest().setAttribute("addEditFlag",
			 * addEditFlag);
			 * ServletActionContext.getRequest().setAttribute("LEGAL_PROCESS",
			 * legalProcess);
			 */
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String populateTenderJasperReportData() {
		try {
			tenderDS = new ArrayList<TenderTO>();
			TenderTO to = new TenderTO();

			Tender tender = tenderBL.getTenderService()
					.getTenderByIdWithDifferentChilds(tenderId, true, true,
							false);

			/*
			 * setImplementation(((Implementation) (ServletActionContext
			 * .getRequest().getSession().getAttribute("THIS"))));
			 */

			to.convertToTenderTO(tender);

			tenderDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public TenderBL getTenderBL() {
		return tenderBL;
	}

	public void setTenderBL(TenderBL tenderBL) {
		this.tenderBL = tenderBL;
	}

	public Long getTenderId() {
		return tenderId;
	}

	public String getTenderNumber() {
		return tenderNumber;
	}

	public String getTenderDate() {
		return tenderDate;
	}

	public String getTenderExpiry() {
		return tenderExpiry;
	}

	public void setTenderId(Long tenderId) {
		this.tenderId = tenderId;
	}

	public void setTenderNumber(String tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public void setTenderDate(String tenderDate) {
		this.tenderDate = tenderDate;
	}

	public void setTenderExpiry(String tenderExpiry) {
		this.tenderExpiry = tenderExpiry;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getTenderDetails() {
		return tenderDetails;
	}

	public void setTenderDetails(String tenderDetails) {
		this.tenderDetails = tenderDetails;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getQuoted() {
		return quoted;
	}

	public void setQuoted(String quoted) {
		this.quoted = quoted;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public List<TenderTO> getTenderDS() {
		return tenderDS;
	}

	public void setTenderDS(List<TenderTO> tenderDS) {
		this.tenderDS = tenderDS;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
