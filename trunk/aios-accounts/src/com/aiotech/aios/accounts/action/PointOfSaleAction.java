package com.aiotech.aios.accounts.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.RewardPolicy;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleAuditVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.service.bl.PointOfSaleBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceSalesType;
import com.aiotech.aios.common.to.Constants.Accounts.POSPaymentType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PointOfSaleAction extends ActionSupport {

	private static final long serialVersionUID = 7796952878688879955L;

	private static final Logger log = Logger.getLogger(PointOfSaleAction.class);

	// Dependency
	private PointOfSaleBL pointOfSaleBL;
	// Variables
	private long pointOfSaleId;
	private long pointOfSaleTempId;
	private long pointOfSalePrintId;
	private long productId;
	private long specialProductId;
	private long customerId;
	private long driverId;
	private long employeeId;
	private long storeId;
	private long personId;
	private int posStoreId;
	private long pointOfSaleDetailId;
	private int rowId;
	private int itemOrder;
	private int comboItemOrder;
	private int couponNumber;
	private int customerCoupon;
	private byte discountMode;
	private byte salesType;
	private byte paymentMode;
	private byte[] receiptData;
	private double quantity;
	private double discount;
	private double productUnitPrice;
	private double receiveAmount;
	private double dispatchDiscount;
	private double dispatchCost;
	private double deliveryCharges;
	private double receiveCashAmount;
	private String productTotalPrice;
	private String productPreview;
	private String salesDate;
	private String salesRefNumber;
	private String returnMessage;
	private String additionalCharges;
	private String storeDetails;
	private String customerName;
	private String shippingAddress;
	private String customerDiscount;
	private String totalDues;
	private String exchangeReference;
	private String transactionNumber;
	private String printers;
	private String kitchenOrders;
	private String additionalCost;
	private String barOrders;
	private String posPrintReciptContent;
	private String posSessionId;
	private String deliveryOptionName;
	private String password;
	private String driverMobile;
	private String employeeName;
	private String customerMobile;
	private String customerLocation;
	private String dateFrom;
	private String dateTo;
	private boolean freeDelivery;
	private boolean comboType;
	private boolean printerFlag;
	private boolean showPinNumber;
	private boolean deleteFlag;
	private Long deliveryOption;
	private Long categoryId;
	private PointOfSaleVO pointOfSaleVO;
	private List<Object> previewObjects;
	private List<Object> aaData;

	// Return Success
	public String returnSuccess() {
		log.info("Inside Module: Accounts : Method: returnSuccess() : Action Success");
		return SUCCESS;
	}

	/**
	 * Show all PointOfSales for POPUP
	 */
	public String showAllPointOfSales() {
		try {
			log.info("Inside Module: Accounts : Method: showAllPointOfSales");
			aaData = pointOfSaleBL.showAllPointOfSales(pointOfSaleId,
					salesType, salesDate, customerId);
			log.info("Module: Accounts : Method: showAllPointOfSales: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showAllPointOfSales: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show all Dispatched orders
	public String showDispatchedOrders() {
		try {
			log.info("Inside Module: Accounts : Method: showDispatchedOrders");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			aaData = pointOfSaleBL.showDispatchedOrders(
					posUserSession.getPosUserTillId(), false);
			log.info("Module: Accounts : Method: showDispatchedOrders: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showDispatchedOrders: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String savePosOrder() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (pointOfSaleId == 0)
				commonStoreOrderCall(true);
			session.removeAttribute("POS_PRODUCT_PREVIEW_"
					+ sessionObj.get("jSessionId"));
			posSessionId = null;
			commonPosDashboard();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: savePosOrder: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getSavedPosOrder() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (pointOfSaleId > 0) {
				PointOfSale pointOfSale = pointOfSaleBL.getPointOfSaleService()
						.getAllPointOfDetailByOrderId(pointOfSaleId);
				boolean orderPrinted = null != pointOfSale.getOrderPrinted() ? pointOfSale
						.getOrderPrinted() : false;
				PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
				pointOfSaleVO.setPointOfSaleId(pointOfSale.getPointOfSaleId());
				pointOfSaleVO.setOrderPrinted(orderPrinted);
				pointOfSaleVO
						.setCustomerId(null != pointOfSale.getCustomer() ? pointOfSale
								.getCustomer().getCustomerId() : 0);
				pointOfSaleVO
						.setCustomerName(null != pointOfSale.getCustomer() ? (null != pointOfSale
								.getCustomer().getPersonByPersonId() ? pointOfSale
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat(" "
										+ pointOfSale.getCustomer()
												.getPersonByPersonId()
												.getLastName())
								: pointOfSale.getCustomer().getCompany()
										.getCompanyName())
								: "");
				pointOfSaleVO.setCustomerNumber(null != pointOfSale
						.getCustomer() ? pointOfSale.getCustomer()
						.getCustomerNumber() : "");
				pointOfSaleVO.setDeliveryOption(null != pointOfSale
						.getLookupDetail() ? pointOfSale.getLookupDetail()
						.getLookupDetailId() : null);
				pointOfSaleVO.setSaleType(pointOfSale.getSaleType());
				pointOfSaleVO.setPosSalesDate(DateFormat
						.convertDateToString(pointOfSale.getSalesDate()
								.toString()));
				List<ProductVO> productPreviews = new ArrayList<ProductVO>();
				ProductVO product = null;
				Map<String, ProductVO> previewsMap = new HashMap<String, ProductVO>();
				session.removeAttribute("POS_PRODUCT_PREVIEW_"
						+ sessionObj.get("jSessionId"));
				List<ProductVO> comboProducts = new ArrayList<ProductVO>();
				int itemOrder = 0;
				List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>(
						pointOfSale.getPointOfSaleDetails());
				Collections.sort(pointOfSaleDetails,
						new Comparator<PointOfSaleDetail>() {
							public int compare(PointOfSaleDetail o1,
									PointOfSaleDetail o2) {
								return o1.getItemOrder().compareTo(
										o2.getItemOrder());
							}
						});
				for (PointOfSaleDetail saleDetail : pointOfSaleDetails) {
					product = new ProductVO();
					product.setQuantity(saleDetail.getQuantity());
					product.setProductId(saleDetail.getProductByProductId()
							.getProductId());
					product.setProductName(saleDetail.getProductByProductId()
							.getProductName());
					product.setTotalPrice(saleDetail.getQuantity()
							* saleDetail.getUnitRate());
					product.setDisplayPrice(AIOSCommons.formatAmount(product
							.getTotalPrice()));
					product.setFinalUnitPrice(saleDetail.getUnitRate());
					product.setDisplayUnitPrice(AIOSCommons
							.formatAmount(product.getFinalUnitPrice()));

					// HERE
					product.setOrderPrinted(orderPrinted);
					product.setPointOfSaleDetailId(saleDetail
							.getPointOfSaleDetailId());
					product.setSpecialPos(saleDetail.getSpecialProduct());
					if (null != saleDetail.getComboType()
							&& (boolean) saleDetail.getComboType()) {
						product.setComboProductId(saleDetail
								.getProductByComboProductId().getProductId());
						product.setSpecialPos(false);
						product.setComboType(true);
						product.setComboItemOrder(saleDetail
								.getComboItemOrder());
						comboProducts.add(product);
					} else {
						product.setComboType(false);
						product.setComboItemOrder(saleDetail
								.getComboItemOrder());
					}
					product.setItemOrder(++itemOrder);
					productPreviews.add(product);
					if (null != saleDetail.getSpecialProduct()
							&& (boolean) saleDetail.getSpecialProduct())
						previewsMap.put(String.valueOf(product.getProductId())
								.concat("-" + saleDetail.getItemOrder()),
								product);
					else if ((null == saleDetail.getSpecialProduct() || (boolean) !saleDetail
							.getSpecialProduct())
							&& (null == saleDetail.getComboType() || (boolean) !saleDetail
									.getComboType()))
						previewsMap.put(String.valueOf(product.getProductId()),
								product);
				}
				for (ProductVO productVO : comboProducts) {
					String key = String.valueOf(productVO.getComboProductId())
							.concat("-" + productVO.getComboItemOrder());
					if (previewsMap.containsKey(key)) {
						ProductVO tempProduct = previewsMap.get(key);
						Map<Long, ProductVO> comboProduct = new HashMap<Long, ProductVO>();
						comboProduct.put(productVO.getProductId(), productVO);
						if (null != tempProduct.getProductComboVOs()
								&& tempProduct.getProductComboVOs().size() > 0)
							comboProduct.putAll(tempProduct
									.getProductComboVOs());
						tempProduct.setProductComboVOs(comboProduct);
						previewsMap.put(key, tempProduct);
					}
				}
				ServletActionContext.getRequest().setAttribute("POINT_OF_SALE",
						pointOfSaleVO);
				ServletActionContext.getRequest().setAttribute(
						"PRODUCT_PREVIEW", productPreviews);
				session.setAttribute(
						"POS_PRODUCT_PREVIEW_" + sessionObj.get("jSessionId"),
						previewsMap);
			} else {
				Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) session
						.getAttribute("POS_PRODUCT_PREVIEW_"
								+ sessionObj.get("jSessionId"));
				List<ProductVO> productPreviews = new ArrayList<ProductVO>();
				List<ProductVO> productVOs = new ArrayList<ProductVO>(
						previewsMap.values());
				for (ProductVO productVO : productVOs) {
					productPreviews.add(productVO);
					if (null != productVO.getProductComboVOs())
						productPreviews.addAll(productVO.getProductComboVOs()
								.values());
				}
				Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) session
						.getAttribute("SECURED_SALES_SESSION_"
								+ sessionObj.get("jSessionId"));
				PointOfSaleVO pointOfSaleVO = securedSalesSession
						.get(posSessionId);
				if (null != pointOfSaleVO.getOrderPrinted()
						&& (boolean) pointOfSaleVO.getOrderPrinted()) {
					for (ProductVO productVO : productPreviews)
						productVO.setOrderPrinted(true);
				}
				Collections.sort(productPreviews, new Comparator<Object>() {
					public int compare(Object p1, Object p2) {
						return ((ProductVO) p1).getItemOrder().compareTo(
								((ProductVO) p2).getItemOrder());
					}
				});
				ServletActionContext.getRequest().setAttribute("POINT_OF_SALE",
						pointOfSaleVO);
				ServletActionContext.getRequest().setAttribute(
						"PRODUCT_PREVIEW", productPreviews);
			}
			commonPosDashboard();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String showAllSavedPosOrder() {
		try {
			log.info("Inside Method: showAllSavedPosOrder");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getPointOfSaleBySession(
							pointOfSaleBL.getImplementation(),
							posUserSession.getPosUserTillId());
			ServletActionContext.getRequest().setAttribute(
					"CONSOLIDATED_SESSION_LIST", pointOfSales);
			log.info("Module: Accounts : Method: showAllSavedPosOrder: Action Success");
		} catch (Exception e) {
			log.info("Module: Accounts : Method: showAllSavedPosOrder: Action Exception");
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String deleteSavedPosOrder() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (pointOfSaleId > 0) {
				pointOfSaleBL.deleteSessionPOSOrder(pointOfSaleBL
						.getPointOfSaleService().getPointOfDetailByOrderId(
								pointOfSaleId));
			} else {
				Map<String, PointOfSaleVO> securedPointOfSales = (Map<String, PointOfSaleVO>) (session
						.getAttribute("SECURED_SALES_SESSION_"
								+ sessionObj.get("jSessionId")));
				PointOfSaleVO pointOfSaleVO = securedPointOfSales
						.get(posSessionId);
				if (null != pointOfSaleVO
						&& null != pointOfSaleVO.getPointOfSaleId()
						&& deleteFlag) {
					pointOfSaleBL.deleteSessionPOSOrder(pointOfSaleBL
							.getPointOfSaleService().getPointOfDetailByOrderId(
									pointOfSaleVO.getPointOfSaleId()));
				}
			}
			pointOfSaleId = 0;
			posSessionId = null;
			session.removeAttribute("SECURED_SALES_SESSION_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("POS_PRODUCT_PREVIEW_"
					+ sessionObj.get("jSessionId"));
			resetValues(session, sessionObj);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		pointOfSaleId = 0;
		commonPosDashboard();
		return SUCCESS;
	}

	/**
	 * Show Point Of Sales DashBard Initial Screen With SubCategory & Product
	 * Display
	 * 
	 * @return
	 */
	public String showPOSDashBoard() {
		commonPosDashboard();
		return SUCCESS;

	}

	public String discardPOSSales() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("POS_PRODUCT_PREVIEW_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("SECURED_SALES_SESSION_"
					+ sessionObj.get("jSessionId"));
			resetValues(session, sessionObj);
			commonPosDashboard();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return SUCCESS;
	}

	public String initializeOrderingMenuConfig() {
		try {
			HttpSession httpSession = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			httpSession.removeAttribute("POS_PRODUCT_PREVIEW_"
					+ sessionObj.get("jSessionId"));
			httpSession.removeAttribute("SECURED_SALES_SESSION_"
					+ sessionObj.get("jSessionId"));
			resetValues(httpSession, sessionObj);

			Implementation implementation = pointOfSaleBL.getImplementation();

			if (implementation == null)
				throw new Exception("COMPANY KEY UNKNOWN :: POSAction");

			User orderingUser = pointOfSaleBL.getSystemService().getUser(
					"online.ordering_" + implementation.getImplementationId());

			List<POSUserTill> posUserSession = pointOfSaleBL
					.getSystemService()
					.getpOSUserTillDAO()
					.findByNamedQuery("getPosUserTillByPersonId",
							orderingUser.getPersonId());

			if (null != posUserSession && posUserSession.size() > 0)
				httpSession.setAttribute(
						"POS_USERTILL_SESSION" + sessionObj.get("jSessionId"),
						posUserSession.get(0));

			commonPosDashboard();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public void commonPosDashboard() {
		try {
			log.info("Inside Module: Accounts : Method: showPOSDashBoard");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			resetValues(session, sessionObj);
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			if (null == posUserSession || ("").equals(posUserSession)) {
				if (null == posUserSession || ("").equals(posUserSession))
					returnMessage = "Till is not configured.";
			} else {

				ServletActionContext.getRequest().setAttribute(
						"SALES_SOTRE_NAME",
						posUserSession.getStore().getStoreName());

			}

			previewObjects = new ArrayList<Object>();
			if (null != posUserSession) {
				List<PointOfSale> pointOfSaleSessionAll = pointOfSaleBL
						.getPointOfSaleService().getAllPointOfSaleBySession(
								pointOfSaleBL.getImplementation(),
								posUserSession.getPosUserTillId());
				List<PointOfSale> pointOfSaleSessions = null;
				boolean deliverySesssion = false;
				if (null != pointOfSaleSessionAll
						&& pointOfSaleSessionAll.size() > 0) {
					pointOfSaleSessions = new ArrayList<PointOfSale>();
					for (PointOfSale pointOfSale : pointOfSaleSessionAll) {
						if (null == pointOfSale.getDeliveryStatus())
							pointOfSaleSessions.add(pointOfSale);
						else
							deliverySesssion = true;
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"DISPATCH_ORDER", deliverySesssion);
				ServletActionContext.getRequest().setAttribute(
						"POS_ORDERS_SESSION", pointOfSaleSessions);
			}
			List<ProductCategoryVO> productCategoryVOs = pointOfSaleBL
					.getProductPricingBL().getProductBL()
					.getProductCategoryBL().getAllPOSProductCategories();
			if (null == productCategoryVOs)
				productCategoryVOs = new ArrayList<ProductCategoryVO>();
			List<Product> specialProducts = pointOfSaleBL.getProductPricingBL()
					.getProductBL().getAllActiveInventorySpecialProducts();
			if (null != specialProducts && specialProducts.size() > 0) {
				ProductCategoryVO productCategoryVO = null;
				for (Product product : specialProducts) {
					productCategoryVO = new ProductCategoryVO();
					productCategoryVO.setCategoryName(product.getProductName());
					productCategoryVO.setProductCategoryId(product
							.getProductId());
					productCategoryVO.setSpecialProduct(true);
					productCategoryVOs.add(productCategoryVO);
				}
			}

			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_CATEGORIES", productCategoryVOs);

			Map<Byte, String> salesTypes = new HashMap<Byte, String>();
			for (InvoiceSalesType e : EnumSet.allOf(InvoiceSalesType.class))
				salesTypes.put(e.getCode(), e.name().replaceAll("_", " "));
			ServletActionContext.getRequest().setAttribute("SALES_TYPE",
					salesTypes);
			// Delivery Options
			List<LookupDetail> deliveryOptions = pointOfSaleBL
					.getLookupMasterBL().getActiveLookupDetails(
							"DELIVERY_OPTION", true);
			ServletActionContext.getRequest().setAttribute("DELIVERY_OPTION",
					deliveryOptions);

			List<Product> products = (List<Product>) (session
					.getAttribute("POSPRODUCT_DETAILS_"
							+ sessionObj.get("jSessionId")));
			if (null == products || products.size() == 0) {
				products = pointOfSaleBL.getProductPricingBL().getProductBL()
						.getAllProducts();
				session.setAttribute(
						"POSPRODUCT_DETAILS_" + sessionObj.get("jSessionId"),
						products);
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAILS",
					products);
			log.info("Module: Accounts : Method: showPOSDashBoard: Action Success");
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showPOSDashBoard: throws Exception "
					+ ex);
		}
	}

	public String showSubCategory() {
		try {
			List<ProductCategoryVO> productSubCategoryVOs = pointOfSaleBL
					.getProductPricingBL().getProductBL()
					.getProductCategoryBL()
					.getProductSubCategoriesByCategory(categoryId);
			List<Product> products = null;
			if (null != productSubCategoryVOs
					&& productSubCategoryVOs.size() > 0) {
				String categoryIds = "";
				for (int i = 0; i < productSubCategoryVOs.size(); i++) {
					if (i < (productSubCategoryVOs.size() - 1))
						categoryIds += productSubCategoryVOs.get(i)
								.getProductCategoryId() + "__";
					else
						categoryIds += productSubCategoryVOs.get(i)
								.getProductCategoryId();
				}
				String[] categoryArray = splitValues(categoryIds, "__");
				Long[] categoryIdArray = new Long[categoryArray.length];
				for (int i = 0; i < categoryArray.length; i++)
					categoryIdArray[i] = Long.parseLong(categoryArray[i]);

				products = pointOfSaleBL.getProductPricingBL().getProductBL()
						.getProductByCategories(categoryIdArray);
			}
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_SUB_CATEGORIES", productSubCategoryVOs);
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAILS",
					products);
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showSubCategory: throws Exception "
					+ ex);
		}

		return SUCCESS;
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	/**
	 * Show POS Detail by Reference
	 */
	public String showPointOfDetailByReference() {
		try {
			log.info("Inside Module: Accounts : Method: showPointOfDetailByReference");
			PointOfSaleVO pointOfSaleVO = pointOfSaleBL
					.showPointOfDetailByReference(salesRefNumber);
			if (null != pointOfSaleVO) {
				ServletActionContext.getRequest().setAttribute("POINT_OF_SALE",
						pointOfSaleVO);
				salesDate = DateFormat.convertSystemDateToString(Calendar
						.getInstance().getTime());
				salesRefNumber = SystemBL.getReferenceStamp(
						MerchandiseExchange.class.getName(),
						pointOfSaleBL.getImplementation());
				log.info("Module: Accounts : Method: showPointOfDetailByReference: Action Success");
				return SUCCESS;
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Nil");
				log.info("Module: Accounts : Method: showPointOfDetailByReference: Action Error");
				return ERROR;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure ");
			log.info("Module: Accounts : Method: showPointOfDetailByReference: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private Map<Long, ProductVO> addComboProduct(POSUserTill posUserSession,
			JSONObject jsonObject, int itemOrder, ProductVO productTemp)
			throws Exception {
		ProductVO product = new ProductVO();
		product.setProductId((Long) jsonObject.get("productId"));
		product.setProductName((String) jsonObject.get("productName"));
		product.setFinalUnitPrice(0d);
		product.setDisplayUnitPrice(AIOSCommons.formatAmount(product
				.getFinalUnitPrice()));
		product.setTotalPrice(product.getFinalUnitPrice());
		product.setStoreId(posUserSession.getStore().getStoreId());
		product.setDisplayPrice(AIOSCommons.formatAmount(product
				.getTotalPrice()));
		product.setQuantity(1);
		product.setItemOrder(itemOrder);
		product.setComboProductId(productTemp.getProductId());
		product.setComboType(true);
		product.setComboItemOrder(productTemp.getItemOrder());
		Map<Long, ProductVO> productComboVOs = new HashMap<Long, ProductVO>();
		productComboVOs.put(product.getProductId(), product);
		return productComboVOs;
	}

	/**
	 * Store Product values in session when user clicks on product in POS
	 * Dashboard
	 * 
	 * @return JSON Object
	 */
	@SuppressWarnings("unchecked")
	public String saveProductPreviewSession() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductPreviewSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			JSONParser parser = new JSONParser();
			Object previewObject = parser.parse(productPreview);
			JSONObject jsonObject = (JSONObject) previewObject;
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			ProductVO product = null;
			if (null == previewsMap || previewsMap.size() == 0) {
				previewsMap = new HashMap<String, ProductVO>();
				previewObjects = new ArrayList<Object>();
			}
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			boolean comboType = (Boolean) jsonObject.get("comboType");

			long productId = (Long) jsonObject.get("productId");
			String productTempId = String.valueOf(productId);
			List<ProductVO> comboProductVOs = null;
			long productComboTempId = 0;
			if (comboType) {
				String currentComboPId = "";
				if (itemOrder > 0 && productId > 0)
					currentComboPId = String.valueOf(specialProductId).concat(
							"-" + itemOrder);
				else
					currentComboPId = (String) session
							.getAttribute("CURRENT_COMBO_"
									+ sessionObj.get("jSessionId"));
				if (null != previewsMap && previewsMap.size() > 0) {
					if (previewsMap.containsKey(currentComboPId)) {
						product = previewsMap.get(currentComboPId);
						if (null != product.getProductComboVOs()
								&& product.getProductComboVOs().size() > 0) {
							Map<Long, ProductVO> comboProducts = new HashMap<Long, ProductVO>(
									product.getProductComboVOs());
							if (comboProducts.containsKey((Long) jsonObject
									.get("productId"))) {
								ProductVO comboProduct = comboProducts
										.get((Long) jsonObject.get("productId"));
								comboProduct.setQuantity(comboProduct
										.getQuantity() + 1);
								comboProduct.setTotalPrice(comboProduct
										.getQuantity()
										* comboProduct.getFinalUnitPrice());
								comboProduct.setDisplayPrice(AIOSCommons
										.formatAmount(comboProduct
												.getTotalPrice()));
								comboProduct.setComboItemOrder(product
										.getItemOrder());
								comboProduct.setComboType(true);
								comboProduct.setComboProductId(product
										.getProductId());
								comboProducts.put(
										(Long) jsonObject.get("productId"),
										comboProduct);
								product.setProductComboVOs(comboProducts);
								previewsMap.put(currentComboPId, product);
								comboProductVOs = new ArrayList<ProductVO>();
								for (ProductVO productVO : previewsMap.values()) {
									if (null != productVO.getProductComboVOs()
											&& productVO.getProductComboVOs()
													.size() > 0)
										comboProductVOs.addAll(productVO
												.getProductComboVOs().values());
								}
							} else {
								int itemOrder = (product.getItemOrder() + product
										.getProductComboVOs().size());
								Map<Long, ProductVO> productComboVOs = addComboProduct(
										posUserSession, jsonObject,
										++itemOrder, product);
								productComboVOs.putAll(product
										.getProductComboVOs());
								product.setProductComboVOs(productComboVOs);
								previewsMap.put(currentComboPId, product);
								comboProductVOs = new ArrayList<ProductVO>();
								for (ProductVO productVO : previewsMap.values()) {
									if (null != productVO.getProductComboVOs()
											&& productVO.getProductComboVOs()
													.size() > 0)
										comboProductVOs.addAll(productVO
												.getProductComboVOs().values());
								}
							}
						} else {
							int itemOrder = product.getItemOrder();
							Map<Long, ProductVO> productComboVOs = addComboProduct(
									posUserSession, jsonObject, ++itemOrder,
									product);
							product.setProductComboVOs(productComboVOs);
							previewsMap.put(currentComboPId, product);
							comboProductVOs = new ArrayList<ProductVO>();
							for (ProductVO productVO : previewsMap.values()) {
								if (null != productVO.getProductComboVOs()
										&& productVO.getProductComboVOs()
												.size() > 0)
									comboProductVOs.addAll(productVO
											.getProductComboVOs().values());
							}
						}
					}
				}
			} else {
				boolean specialProduct = (Boolean) jsonObject
						.get("specialProduct");
				if (specialProduct) {
					productComboTempId = (Long) jsonObject.get("productId");
					product = addProductToView(jsonObject, posUserSession);
					if (null != product) {
						int itemOrder = previewsMap.size() + 1;
						for (ProductVO tempProductVO : previewsMap.values()) {
							if (null != tempProductVO.getProductComboVOs()
									&& tempProductVO.getProductComboVOs()
											.size() > 0) {
								itemOrder += tempProductVO.getProductComboVOs()
										.size();
							}
						}
						product.setItemOrder(itemOrder);
						product.setSpecialPos(true);
						product.setComboType(false);
						long productCId = (Long) jsonObject.get("productId");
						previewsMap.put(
								String.valueOf(productCId).concat(
										"-" + product.getItemOrder()), product);
						comboProductVOs = new ArrayList<ProductVO>();
						for (ProductVO productVO : previewsMap.values()) {
							if (null != productVO.getProductComboVOs()
									&& productVO.getProductComboVOs().size() > 0)
								comboProductVOs.addAll(productVO
										.getProductComboVOs().values());
						}
					}
				}

				else if (null != previewsMap && previewsMap.size() > 0
						&& previewsMap.containsKey(productTempId)) {
					product = previewsMap.get(productTempId);
					product.setQuantity(product.getQuantity() + 1);
					product.setComboType(false);
					product.setTotalPrice(product.getQuantity()
							* product.getFinalUnitPrice());
					product.setDisplayPrice(AIOSCommons.formatAmount(product
							.getTotalPrice()));
					previewsMap.put(productTempId, product);
					comboProductVOs = new ArrayList<ProductVO>();
					for (ProductVO productVO : previewsMap.values()) {
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0)
							comboProductVOs.addAll(productVO
									.getProductComboVOs().values());
					}
				} else {
					product = addProductToView(jsonObject, posUserSession);
					if (null != product) {
						int itemOrder = previewsMap.size() + 1;
						for (ProductVO tempProductVO : previewsMap.values()) {
							if (null != tempProductVO.getProductComboVOs()
									&& tempProductVO.getProductComboVOs()
											.size() > 0) {
								itemOrder += tempProductVO.getProductComboVOs()
										.size();
							}
						}
						product.setItemOrder(itemOrder);
						previewsMap.put(productTempId, product);
						comboProductVOs = new ArrayList<ProductVO>();
						for (ProductVO productVO : previewsMap.values()) {
							if (null != productVO.getProductComboVOs()
									&& productVO.getProductComboVOs().size() > 0)
								comboProductVOs.addAll(productVO
										.getProductComboVOs().values());
						}
					}
				}
			}

			for (ProductVO productVO : previewsMap.values()) {
				StockVO stockVO = pointOfSaleBL.getStockBL()
						.getStoreAndStockByProduct(productVO.getProductId(),
								posUserSession.getStore().getStoreId());
				productVO.setStoreName((stockVO != null ? stockVO
						.getStoreName() : ""));
			}
			previewObjects = new ArrayList<Object>(previewsMap.values());
			Collections.sort(previewObjects, new Comparator<Object>() {
				public int compare(Object p1, Object p2) {
					return ((ProductVO) p1).getItemOrder().compareTo(
							((ProductVO) p2).getItemOrder());
				}
			});
			if (productComboTempId > 0) {
				int size = previewObjects.size()
						+ (null != comboProductVOs
								&& comboProductVOs.size() > 0 ? comboProductVOs
								.size() : 0);
				session.setAttribute(
						"CURRENT_COMBO_" + sessionObj.get("jSessionId"), String
								.valueOf(productComboTempId).concat("-" + size));
			}
			createProductSalesSession(comboProductVOs);
			log.info("Module: Accounts : Method: saveProductPreviewSession: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: saveProductPreviewSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private void createProductSalesSession(List<ProductVO> comboProductVOs)
			throws Exception {
		int currentOrder = 0;
		itemOrder = 0;
		productId = 0;
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		Map<String, ProductVO> productSessionMap = new HashMap<String, ProductVO>();
		for (Object object : previewObjects) {
			ProductVO productVO = (ProductVO) object;
			productVO.setItemOrder(++currentOrder);
			if (null != productVO.getProductComboVOs()
					&& productVO.getProductComboVOs().size() > 0) {
				for (ProductVO comboItem : productVO.getProductComboVOs()
						.values()) {
					comboItem.setItemOrder(++currentOrder);
					comboItem.setComboItemOrder(productVO.getItemOrder());
				}
			}
			if (null != productVO.getSpecialPos()
					&& (boolean) productVO.getSpecialPos()) {
				productSessionMap.put(String.valueOf(productVO.getProductId())
						.concat("-" + productVO.getItemOrder()), productVO);
			} else
				productSessionMap.put(String.valueOf(productVO.getProductId()),
						productVO);
		}
		if (null != comboProductVOs && comboProductVOs.size() > 0)
			previewObjects.addAll(comboProductVOs);
		Collections.sort(previewObjects, new Comparator<Object>() {
			public int compare(Object p1, Object p2) {
				return ((ProductVO) p1).getItemOrder().compareTo(
						((ProductVO) p2).getItemOrder());
			}
		});
		session.setAttribute(
				"POS_PRODUCT_PREVIEW_" + sessionObj.get("jSessionId"),
				productSessionMap);
	}

	// Add Product to the table view
	private ProductVO addProductToView(JSONObject jsonObject,
			POSUserTill posUserSession) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		String unifiedPrice = "";
		if (session.getAttribute("unified_price") != null)
			unifiedPrice = session.getAttribute("unified_price").toString();
		com.aiotech.aios.accounts.domain.entity.ProductPricingDetail productPricingDetail = null;
		if (!unifiedPrice.equals("false")) {
			productPricingDetail = pointOfSaleBL
					.getDefalutProductPricingDetail((Long) jsonObject
							.get("productId"));
		} else {
			productPricingDetail = pointOfSaleBL
					.getNonUnfiedProductPricingDetail((Long) jsonObject
							.get("productId"), posUserSession.getStore()
							.getStoreId());
			if (null == productPricingDetail) {
				productPricingDetail = pointOfSaleBL
						.getDefalutProductPricingDetail((Long) jsonObject
								.get("productId"));
				productPricingDetail.setProductPricingCalcs(null);
			}
		}
		ProductVO product = null;
		if (null != productPricingDetail) {
			product = new ProductVO();
			if ((null != posUserSession.getBarPrinter() && !("")
					.equals(posUserSession.getBarPrinter()))
					|| (null != posUserSession.getKitchenPrinter() && !("")
							.equals(posUserSession.getKitchenPrinter()))) {
				ProductCategory productCategory = pointOfSaleBL
						.getProductPricingBL()
						.getProductBL()
						.getProductCategoryBL()
						.getProductSubCategoriesByProduct(
								(Long) jsonObject.get("productId"));
				if (null != productCategory) {
					if (null != productCategory.getBarPrint()
							&& (boolean) productCategory.getBarPrint() == true)
						product.setBarPrinter(true);
					else if (null != productCategory.getKitchenPrint()
							&& (boolean) productCategory.getKitchenPrint() == true)
						product.setKitchenPrinter(true);
				}
			}
			product.setProductId((Long) jsonObject.get("productId"));
			product.setProductName((String) jsonObject.get("productName"));
			product.setFinalUnitPrice(productPricingDetail.getSellingPrice());
			product.setDisplayUnitPrice(AIOSCommons.formatAmount(product
					.getFinalUnitPrice()));
			product.setTotalPrice(product.getFinalUnitPrice());
			product.setStoreId(posUserSession.getStore().getStoreId());
			product.setDisplayPrice(AIOSCommons.formatAmount(product
					.getTotalPrice()));
			product.setQuantity(1);
			product.setComboType(false);
			product.setBasePrice(productPricingDetail.getBasicCostPrice());
			product.setStandardPrice(productPricingDetail.getStandardPrice());
		}
		return product;
	}

	/**
	 * Delete Product values in session when user clicks on delete option
	 * 
	 * @return
	 */
	public String getApproverUserInformation() {
		try {
			log.info("Inside Module: Accounts : Method: getApproverUserInformation");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			if (posUserSession != null
					&& posUserSession.getPersonBySupervisor() != null) {
				returnMessage = "SUCCESS";
				User user = pointOfSaleBL.getSystemService()
						.getActiveUserByPersonId(
								posUserSession.getPersonBySupervisor()
										.getPersonId());
				ServletActionContext.getRequest().setAttribute("USER_INFO",
						user);
				ServletActionContext.getRequest().setAttribute("PERSON_INFO",
						posUserSession.getPersonBySupervisor());

			}
			log.info("Module: Accounts : Method: getApproverUserInformation: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: getApproverUserInformation: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String checkPOSPrintOrderStatus() {
		try {
			log.info("Inside Module: Accounts : Method: checkPOSPrintOrderStatus");
			printerFlag = false;
			PointOfSale pos = null;
			if (pointOfSaleId > 0)
				pos = pointOfSaleBL.getPointOfSaleService()
						.findByPointOfSaleId(Long.valueOf(pointOfSaleId));
			else
				pos = pointOfSaleBL.getPointOfSaleService()
						.findByPointOfSaleBySessionValue(posSessionId);

			if (null != pos && null != pos.getOrderPrinted())
				printerFlag = pos.getOrderPrinted();

			log.info("Module: Accounts : Method: checkPOSPrintOrderStatus: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			printerFlag = false;
			ex.printStackTrace();
			log.info("Module: Accounts : Method: checkPOSPrintOrderStatus: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Delete Product values in session when user clicks on delete option
	 * 
	 * @return
	 */
	public String verifyPassword() {
		try {
			log.info("Inside Module: Accounts : Method: verifyPassword");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			if (posUserSession != null
					&& posUserSession.getPersonBySupervisor() != null) {
				User user = pointOfSaleBL.getSystemService()
						.getActiveUserByPersonId(
								posUserSession.getPersonBySupervisor()
										.getPersonId());

				if ((AIOSCommons.encrypt(password)).equals(user.getPassword())) {
					returnMessage = "SUCCESS";
					PointOfSale pos = pointOfSaleBL.getPointOfSaleService()
							.findByPointOfSaleId(Long.valueOf(pointOfSaleId));
					pos.setOrderPrinted(false);
					pointOfSaleBL.getPointOfSaleService().savePointOfSale(pos);
				} else {
					returnMessage = "ERROR";
				}

			}
			log.info("Module: Accounts : Method: verifyPassword: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: verifyPassword: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Delete Product values in session when user clicks on delete option
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String deleteProductPreviewSession() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProductPreviewSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			List<ProductVO> comboProductVOs = null;
			if (specialProductId > 0) {
				String productTempId = String.valueOf(specialProductId).concat(
						"-" + comboItemOrder);
				if (null != previewsMap
						&& previewsMap.containsKey(productTempId)) {
					ProductVO product = previewsMap.get(productTempId);
					Map<Long, ProductVO> comboProducts = new HashMap<Long, ProductVO>(
							product.getProductComboVOs());
					if (null != comboProducts
							&& comboProducts.containsKey(productId)) {
						comboProducts.remove(productId);
						product.setProductComboVOs(comboProducts);
						previewsMap.put(productTempId, product);
						previewObjects = new ArrayList<Object>(
								previewsMap.values());
						comboProductVOs = new ArrayList<ProductVO>();
						for (ProductVO productVO : previewsMap.values()) {
							if (null != productVO.getProductComboVOs()
									&& productVO.getProductComboVOs().size() > 0) {
								comboProductVOs.addAll(productVO
										.getProductComboVOs().values());
							}
						}
					}
				}
			} else {
				String productTempId = String.valueOf(productId);
				if (comboType)
					productTempId = String.valueOf(productId).concat(
							"-" + itemOrder);
				previewsMap.remove(String.valueOf(productTempId));
				if (null != previewsMap && previewsMap.size() > 0) {
					previewObjects = new ArrayList<Object>(previewsMap.values());
					comboProductVOs = new ArrayList<ProductVO>();
					for (ProductVO productVO : previewsMap.values()) {
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							comboProductVOs.addAll(productVO
									.getProductComboVOs().values());
						}
					}
				} else
					previewObjects = new ArrayList<Object>();
			}
			if (null != previewObjects && previewObjects.size() > 0) {
				Collections.sort(previewObjects, new Comparator<Object>() {
					public int compare(Object p1, Object p2) {
						return ((ProductVO) p1).getItemOrder().compareTo(
								((ProductVO) p2).getItemOrder());
					}
				});
			}
			createProductSalesSession(comboProductVOs);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProductPreviewSession: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: deleteProductPreviewSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Update Product values in session when user change product quantity
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String updateProductPreviewSession() {
		try {
			log.info("Inside Module: Accounts : Method: updateProductPreviewSession");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			if (specialProductId > 0) {
				String productTempId = String.valueOf(specialProductId).concat(
						"-" + comboItemOrder);
				if (null != previewsMap
						&& previewsMap.containsKey(productTempId)) {
					ProductVO product = previewsMap.get(productTempId);
					Map<Long, ProductVO> comboProducts = new HashMap<Long, ProductVO>(
							product.getProductComboVOs());
					if (null != comboProducts
							&& comboProducts.containsKey(productId)) {
						ProductVO comboProduct = comboProducts.get(productId);
						comboProduct.setQuantity(quantity);
						comboProducts.put(productId, comboProduct);
						product.setProductComboVOs(comboProducts);
						previewsMap.put(productTempId, product);
					}
				}
			} else {
				String productTempId = String.valueOf(productId);
				if (comboType)
					productTempId = String.valueOf(productId).concat(
							"-" + itemOrder);
				if (null != previewsMap
						&& previewsMap.containsKey(productTempId)) {
					ProductVO product = previewsMap.get(productTempId);
					product.setQuantity(quantity);
					product.setTotalPrice(quantity
							* product.getFinalUnitPrice());
					product.setDisplayPrice(AIOSCommons.formatAmount(product
							.getTotalPrice()));
					if (null != product.getProductComboVOs()
							&& product.getProductComboVOs().size() > 0) {
						Map<Long, ProductVO> comboProducts = new HashMap<Long, ProductVO>(
								product.getProductComboVOs());
						if (null != comboProducts && comboProducts.size() > 0) {
							for (ProductVO comboProduct : comboProducts
									.values()) {
								comboProduct.setQuantity(quantity);
							}
							product.setProductComboVOs(comboProducts);
						}
					}
					previewsMap.put(productTempId, product);
				}
			}
			previewObjects = new ArrayList<Object>(previewsMap.values());
			List<ProductVO> comboProductVOs = new ArrayList<ProductVO>();
			for (ProductVO productVO : previewsMap.values()) {
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					comboProductVOs.addAll(productVO.getProductComboVOs()
							.values());
				}
			}
			Collections.sort(previewObjects, new Comparator<Object>() {
				public int compare(Object p1, Object p2) {
					return ((ProductVO) p1).getItemOrder().compareTo(
							((ProductVO) p2).getItemOrder());
				}
			});
			createProductSalesSession(comboProductVOs);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updateProductPreviewSession: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: updateProductPreviewSession: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Generate POS Invoice on user click generate-invoice id
	 * 
	 * @return
	 */
	public String generatePOSInvoice() {
		boolean validateStock = validateStocks();
		if (validateStock) {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			commonGeneratePosInvoice();
			showPinNumber = session.getAttribute("show_pos_secretpin") != null ? ((String) session
					.getAttribute("show_pos_secretpin"))
					.equalsIgnoreCase("true") : false;
			return SUCCESS;
		} else {
			return "stockerror";
		}
	}

	@SuppressWarnings("unchecked")
	private boolean validateStocks() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			boolean stockFlag = true;
			// Validate Stock
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			List<ProductVO> tempstockProducts = new ArrayList<ProductVO>(
					previewsMap.values());
			List<ProductVO> stockProducts = new ArrayList<ProductVO>();
			for (ProductVO productVO : tempstockProducts) {
				Product product = pointOfSaleBL.getProductPricingBL()
						.getProductBL().getProductService()
						.getBasicProductById(productVO.getProductId());
				if ((byte) product.getItemSubType() != (byte) InventorySubCategory.Services
						.getCode()
						&& (byte) product.getItemSubType() != (byte) InventorySubCategory.Craft0Manufacturer
								.getCode()) {
					stockProducts.add(productVO);
				}
			}

			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			if (null != stockProducts && stockProducts.size() > 0) {
				ProductVO tempProductVO = null;
				Set<Long> productIds = new HashSet<Long>();
				Map<Long, ProductVO> productMap = new HashMap<Long, ProductVO>();
				for (ProductVO productVO : stockProducts) {
					tempProductVO = new ProductVO();
					if (productMap.containsKey(productVO.getProductId())) {
						tempProductVO = productMap
								.get(productVO.getProductId());
					}
					tempProductVO.setQuantity(tempProductVO.getQuantity()
							+ productVO.getQuantity());
					productMap.put(productVO.getProductId(), productVO);
				}
				List<StockVO> stockVOs = pointOfSaleBL.getStockBL()
						.validateStocks(productMap.keySet(),
								posUserSession.getStore().getStoreId());

				if (null != stockVOs && stockVOs.size() > 0) {
					productIds = new HashSet<Long>(productMap.keySet());
					for (StockVO stockVO : stockVOs) {
						if (productMap.containsKey(stockVO.getProduct()
								.getProductId())) {
							productIds.remove(stockVO.getProduct()
									.getProductId());
						}
					}

					for (StockVO stockVO : stockVOs) {
						if (productMap.containsKey(stockVO.getProduct()
								.getProductId())) {
							tempProductVO = new ProductVO();
							tempProductVO = productMap.get(stockVO.getProduct()
									.getProductId());
							if (tempProductVO.getQuantity() > stockVO
									.getAvailableQuantity()) {
								productIds.add(stockVO.getProduct()
										.getProductId());
							}
						}
					}
					ServletActionContext.getRequest().setAttribute(
							"NON_STOCK_PRODUCTS", productIds);
				} else {
					productIds = new HashSet<Long>(productMap.keySet());
					ServletActionContext.getRequest().setAttribute(
							"NON_STOCK_PRODUCTS", productIds);
				}
				if (null != productIds && productIds.size() > 0)
					stockFlag = false;
				else
					stockFlag = true;
			}
			return stockFlag;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	public void commonGeneratePosInvoice() {
		try {
			log.info("Inside Module: Accounts : Method: generatePOSInvoice");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			resetValues(session, sessionObj);
			Map<String, PointOfSaleVO> securedSalesSession = new HashMap<String, PointOfSaleVO>();
			PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
			printerFlag = false;
			// Check for Product Points
			List<ProductVO> productPoints = pointOfSaleBL
					.getProductPoints(new ArrayList<ProductVO>(previewsMap
							.values()));
			// Check for promotions
			List<ProductVO> salesPromotions = pointOfSaleBL
					.getPromotionDetails(productPoints, customerId);
			// Check for Discounts
			List<ProductVO> salesInvoice = pointOfSaleBL.getDiscountDetails(
					salesPromotions, customerId);
			if (null == salesDate || ("").equals(salesDate))
				salesDate = DateFormat.convertSystemDateToString(Calendar
						.getInstance().getTime());

			if (pointOfSaleId > 0) {
				PointOfSale pointOfSaletemp = pointOfSaleBL
						.getPointOfSaleService().findByPointOfSaleId(
								pointOfSaleId);
				salesRefNumber = pointOfSaletemp.getReferenceNumber();
			} else {
				salesRefNumber = SystemBL.getReferenceStamp(
						PointOfSale.class.getName(),
						pointOfSaleBL.getImplementation());
			}
			pointOfSaleVO.setSalesDate(null != salesDate
					&& !("").equals(salesDate) ? DateFormat
					.convertStringToDate(salesDate) : Calendar.getInstance()
					.getTime());

			pointOfSaleVO.setReferenceNumber(salesRefNumber);
			pointOfSaleVO.setSaleType(salesType);
			pointOfSaleVO.setProductVOs(salesInvoice);
			ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
					salesInvoice);
			Map<Byte, POSPaymentType> paymentType = new HashMap<Byte, POSPaymentType>();
			for (POSPaymentType e : EnumSet.allOf(POSPaymentType.class))
				paymentType.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
					paymentType);
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					pointOfSaleBL.getPromotionBL().getLookupMasterBL()
							.getActiveLookupDetails("CHARGES_TYPE", true));
			double totalSales = 0;
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			double customerDiscounts = 0;
			for (ProductVO productVO : productVOs) {
				totalSales += productVO.getTotalPrice();
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO tempproductVO : productVO
							.getProductComboVOs().values())
						totalSales += tempproductVO.getTotalPrice();
				}
				customerDiscounts += productVO.getCustomerDiscount();
				if (customerId == 0) {
					if (productVO.isPromotionFlag())
						customerCoupon = productVO.getNumberOfCoupons();
				}
				if (productVO.isBarPrinter() || productVO.isKitchenPrinter())
					printerFlag = true;
			}

			if (customerId > 0) {
				ProductVO tempProductVO = productVOs.get(0);
				pointOfSaleVO.setCustomerId(customerId);
				pointOfSaleVO.setCustomerName(customerName);
				if (tempProductVO.isPromotionFlag()) {
					customerCoupon = tempProductVO.getCustomerCoupons();
					if (tempProductVO.isCustomerPromotion()) {
						PromotionMethod promotionMethod = new PromotionMethod();
						promotionMethod.setPromotionMethodId(tempProductVO
								.getPromotionMethodId());
						pointOfSaleVO.setPromotionMethod(promotionMethod);
					}
				} else {
					if (tempProductVO.isCustomerPromotion()) {
						DiscountMethod discountMethod = new DiscountMethod();
						discountMethod.setDiscountMethodId(tempProductVO
								.getDiscountMethodId());
						pointOfSaleVO.setDiscountMethod(discountMethod);
					}
				}
			}

			productTotalPrice = AIOSCommons.formatAmount(totalSales);
			if (customerDiscounts > 0)
				totalDues = AIOSCommons.formatAmount(totalSales
						- customerDiscounts);
			else
				totalDues = productTotalPrice;
			receiveCashAmount = AIOSCommons.formatAmountToDouble(totalDues);
			customerDiscount = AIOSCommons.formatAmount(customerDiscounts);
			pointOfSaleVO.setOutStanding(productTotalPrice);
			pointOfSaleVO.setTotalDue(totalDues);
			String sessionId = (deliveryOption != null && deliveryOption > 0 ? deliveryOptionName
					.trim() + "_"
					: "")
					+ ""
					+ DateFormat.convertSystemDateToString(new Date())
					+ "_" + DateFormat.getTimeOnlyWithSeconds(new Date());
			if (deliveryOption != null && deliveryOption > 0) {
				pointOfSaleVO.setDeliveryOption(deliveryOption);
				pointOfSaleVO.setDeliveryOptionName(deliveryOptionName);
			}
			posSessionId = sessionId;
			if (pointOfSaleId > 0) {
				pointOfSaleTempId = pointOfSaleId;
				pointOfSaleVO.setPointOfSaleId(pointOfSaleId);
			}
			pointOfSaleVO.setDeliveryOptionName(posSessionId);
			pointOfSaleVO.setCustomerDiscount(customerDiscount);
			pointOfSaleVO.setSaleType(salesType);
			securedSalesSession.put(posSessionId, pointOfSaleVO);
			session.setAttribute(
					"SECURED_SALES_SESSION_" + sessionObj.get("jSessionId"),
					securedSalesSession);
			ServletActionContext.getRequest().setAttribute("SALES_SOTRE_NAME",
					posUserSession.getStore().getStoreName());
			pointOfSaleId = 0;
			log.info("Module: Accounts : Method: commonGeneratePosInvoice: Action Success");
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: commonGeneratePosInvoice: throws Exception "
					+ ex);
		}
	}

	@SuppressWarnings("unchecked")
	public String employeeOrderPointofsale() {
		try {
			log.info("Inside Module: Accounts : Method: employeeOrderPointofsale");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			Map<String, Object> employeeMealDetails = new HashMap<String, Object>();
			resetValues(session, sessionObj);
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			double totalDue = 0;
			for (ProductVO productVO : productVOs) {
				pointOfSaleDetails
						.add(addPointOfSaleDetailEmployeeServe(productVO));
				totalDue += productVO.getQuantity() * productVO.getTotalPrice();
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO tempproductVO : productVO
							.getProductComboVOs().values()) {
						totalDue += tempproductVO.getQuantity()
								* tempproductVO.getTotalPrice();
						pointOfSaleDetails.add(addPointOfSaleDetail(
								tempproductVO, true));
					}
				}
			}
			PointOfSale pointOfSale = new PointOfSale();
			pointOfSale.setSalesDate(Calendar.getInstance().getTime());
			pointOfSale.setOfflineEntry(true);
			pointOfSale.setProcessedSession((byte) 1);
			Person person = new Person();
			person.setPersonId(employeeId);
			pointOfSale.setPersonByEmployeeId(person);
			pointOfSale.setSaleType(salesType);
			pointOfSale.setReferenceNumber(SystemBL.getReferenceStamp(
					PointOfSale.class.getName(),
					pointOfSaleBL.getImplementation()));
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			pointOfSale.setPOSUserTill(posUserSession);
			if (deliveryOption > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(deliveryOption);
				pointOfSale.setLookupDetail(lookupDetail);
			}
			pointOfSaleBL.saveEmployeePOSOrder(pointOfSale, pointOfSaleDetails);
			pointOfSaleVO = new PointOfSaleVO();
			BeanUtils.copyProperties(pointOfSaleVO, pointOfSale);
			pointOfSaleVO.setEmployeeName(employeeName);
			pointOfSaleVO.setTotalDue(AIOSCommons.formatAmount(totalDue));
			pointOfSaleVO.setSaleType(salesType);
			employeeMealDetails.put("SALES_SUMMARY_PREVIEW", pointOfSaleVO);
			employeeMealDetails.put("POS_PRODUCT_PREVIEW", previewsMap);
			session.setAttribute(
					"EMPLOYEE_MEAL_DETAIL_" + sessionObj.get("jSessionId"),
					employeeMealDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: employeeOrderPointofsale: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: employeeOrderPointofsale: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String dispatchPOSInvoice() {
		try {
			log.info("Inside Module: Accounts : Method: dispatchPOSInvoice");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			PointOfSale pointOfSale = pointOfSaleBL.getPointOfSaleService()
					.getDispacthPointOfDetailByOrderId(pointOfSaleId);

			showPinNumber = session.getAttribute("show_pos_secretpin") != null ? ((String) session
					.getAttribute("show_pos_secretpin"))
					.equalsIgnoreCase("true") : false;

			Map<String, ProductVO> previewsMap = new HashMap<String, ProductVO>();

			resetValues(session, sessionObj);
			Map<String, PointOfSaleVO> securedSalesSession = new HashMap<String, PointOfSaleVO>();

			ProductVO productVO = null;
			List<ProductVO> salesInvoice = new ArrayList<ProductVO>();
			double totalSales = 0;
			double customerDiscounts = 0;
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSale
					.getPointOfSaleDetails()) {
				productVO = new ProductVO();
				productVO.setProductId(pointOfSaleDetail
						.getProductByProductId().getProductId());
				productVO.setProductName(pointOfSaleDetail
						.getProductByProductId().getProductName());
				productVO.setQuantity(pointOfSaleDetail.getQuantity());
				productVO.setTotalPrice(pointOfSaleDetail.getQuantity()
						* pointOfSaleDetail.getUnitRate());
				productVO.setDisplayPrice(AIOSCommons.formatAmount(productVO
						.getTotalPrice()));
				productVO.setFinalUnitPrice(productVO.getTotalPrice());
				productVO
						.setTotalPoints(pointOfSaleDetail.getPromotionPoints());
				productVO.setItemOrder(pointOfSaleDetail.getItemOrder());

				totalSales += pointOfSaleDetail.getUnitRate()
						* pointOfSaleDetail.getQuantity();
				salesInvoice.add(productVO);
				previewsMap.put(String.valueOf(productVO.getProductId()) + "-"
						+ pointOfSaleDetail.getItemOrder(), productVO);
			}

			double addtionalCharges = 0;
			if (null != pointOfSale.getPointOfSaleCharges()
					&& pointOfSale.getPointOfSaleCharges().size() > 0) {
				for (PointOfSaleCharge saleCharges : pointOfSale
						.getPointOfSaleCharges()) {
					addtionalCharges += saleCharges.getCharges();
				}
			}

			PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();

			salesDate = DateFormat.convertDateToString(pointOfSale
					.getSalesDate().toString());
			salesRefNumber = pointOfSale.getReferenceNumber();
			pointOfSaleTempId = pointOfSale.getPointOfSaleId();
			pointOfSaleVO.setSalesDate(pointOfSale.getSalesDate());
			pointOfSaleVO.setReferenceNumber(salesRefNumber);
			pointOfSaleVO.setSaleType(pointOfSale.getSaleType());
			pointOfSaleVO.setProductVOs(salesInvoice);
			ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
					salesInvoice);
			Map<Byte, POSPaymentType> paymentType = new HashMap<Byte, POSPaymentType>();
			for (POSPaymentType e : EnumSet.allOf(POSPaymentType.class))
				paymentType.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
					paymentType);

			if (null != pointOfSale.getCustomer()) {
				pointOfSaleVO.setCustomerId(pointOfSale.getCustomer()
						.getCustomerId());
				pointOfSaleVO.setCustomerName(null != pointOfSale.getCustomer()
						.getPersonByPersonId() ? pointOfSale
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(pointOfSale.getCustomer().getPersonByPersonId()
								.getLastName()) : (null != pointOfSale
						.getCustomer().getCompany() ? pointOfSale.getCustomer()
						.getCompany().getCompanyName() : pointOfSale
						.getCustomer().getCmpDeptLocation().getCompany()
						.getCompanyName()));

				pointOfSaleVO.setCustomerNumber(null != pointOfSale
						.getCustomer() ? pointOfSale.getCustomer()
						.getCustomerNumber() : "");

				customerName = pointOfSaleVO.getCustomerName();
				customerId = pointOfSaleVO.getCustomerId();
			}

			if (null != pointOfSale.getTotalDiscount()
					&& (double) pointOfSale.getTotalDiscount() > 0) {
				customerDiscounts = pointOfSale.getTotalDiscount();
			}

			if (addtionalCharges > 0) {
				totalSales += addtionalCharges;
				additionalCost = AIOSCommons.formatAmount(addtionalCharges);
			}

			productTotalPrice = AIOSCommons.formatAmount(totalSales);

			if (customerDiscounts > 0) {
				totalDues = AIOSCommons.formatAmount(totalSales
						- customerDiscounts);
				salesInvoice.get(0).setCustomerDiscount(customerDiscounts);
				previewsMap.put(
						String.valueOf(salesInvoice.get(0).getProductId()),
						salesInvoice.get(0));
			} else
				totalDues = productTotalPrice;

			customerDiscount = AIOSCommons.formatAmount(customerDiscounts);
			pointOfSaleVO.setOutStanding(productTotalPrice);
			pointOfSaleVO.setTotalDue(totalDues);
			pointOfSaleVO.setCustomerDiscount(customerDiscount);
			securedSalesSession.put(
					String.valueOf(pointOfSale.getPointOfSaleId()),
					pointOfSaleVO);
			session.setAttribute(
					"SECURED_SALES_SESSION_" + sessionObj.get("jSessionId"),
					securedSalesSession);
			session.setAttribute(
					"POS_PRODUCT_PREVIEW_" + sessionObj.get("jSessionId"),
					previewsMap);
			log.info("Module: Accounts : Method: dispatchPOSInvoice: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: dispatchPOSInvoice: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public void commonStoreOrderCall(boolean deleteSession) {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			PointOfSale pointOfSale = new PointOfSale();
			if (customerId > 0) {
				Customer customer = new Customer();
				customer.setCustomerId(customerId);
				pointOfSale.setCustomer(customer);
			}
			if (null != deliveryOption && deliveryOption > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(deliveryOption);
				pointOfSale.setLookupDetail(lookupDetail);
			}
			pointOfSale.setSalesDate(Calendar.getInstance().getTime());
			pointOfSale.setPOSUserTill((POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId"))));
			String sessionId = (deliveryOption != null && deliveryOption > 0 ? deliveryOptionName
					.trim() + "_"
					: "")
					+ ""
					+ DateFormat.convertSystemDateToString(new Date())
					+ "_" + DateFormat.getTimeOnlyWithSeconds(new Date());
			posSessionId = sessionId;
			pointOfSale.setReferenceNumber(posSessionId);
			pointOfSale.setProcessedSession((byte) 0);
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			for (ProductVO productVO : productVOs) {
				pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
						productVOs.get(0).isCustomerPromotion()));
			}
			pointOfSale = pointOfSaleBL.savePOSOrderSession(pointOfSale,
					pointOfSaleDetails);
			pointOfSaleId = pointOfSale.getPointOfSaleId();
			if (deleteSession) {
				session.removeAttribute("POS_PRODUCT_PREVIEW_"
						+ sessionObj.get("jSessionId"));
			} else {
				List<PointOfSaleDetail> pointOfSaleDetailTemp = pointOfSaleBL
						.getPointOfSaleService()
						.getAllPointOfSaleDetailsByOrderId(pointOfSaleId);
				for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetailTemp) {
					if (previewsMap.containsKey(pointOfSaleDetail
							.getProductByProductId().getProductId())) {
						ProductVO productVO = previewsMap.get(pointOfSaleDetail
								.getProductByProductId().getProductId());
						productVO.setPointOfSaleDetailId(pointOfSaleDetail
								.getPointOfSaleDetailId());
						previewsMap.put(String.valueOf(pointOfSaleDetail
								.getProductByProductId().getProductId()),
								productVO);
					}
				}
				session.setAttribute(
						"POS_PRODUCT_PREVIEW_" + sessionObj.get("jSessionId"),
						previewsMap);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: commonStoreOrderCall: throws Exception "
					+ ex);
		}
	}

	// Reset values
	private void resetValues(HttpSession session, Map<String, Object> sessionObj) {
		setProductUnitPrice(0);
		setTotalDues("");
		setCustomerDiscount("");
		setCustomerCoupon(0);
		session.removeAttribute("SALES_SUMMARY_PREVIEW_"
				+ sessionObj.get("jSessionId"));
		session.removeAttribute("OFFER_SUMMARY_PREVIEW_"
				+ sessionObj.get("jSessionId"));
		session.removeAttribute("EXCHANGE_SUMMARY_PREVIEW_"
				+ sessionObj.get("jSessionId"));
		session.removeAttribute("POINT_OF_SALE_" + sessionObj.get("jSessionId"));
		session.removeAttribute("EMPLOYEE_MEAL_DETAIL_"
				+ sessionObj.get("jSessionId"));
	}

	/**
	 * Calculate inline discount
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String calculateInlineDiscount() {
		try {
			log.info("Inside Module: Accounts : Method: calculateInlineDiscount");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			String currentComboPId = String.valueOf(productId);
			productTotalPrice = "";
			if (comboType)
				currentComboPId = currentComboPId.concat("-" + itemOrder);
			if (null != previewsMap && previewsMap.containsKey(currentComboPId)) {
				ProductVO productVO = previewsMap.get(String
						.valueOf(currentComboPId));
				if (discountMode == (byte) 0 && discount > 0) {
					productTotalPrice = AIOSCommons.formatAmount((productVO
							.getTotalPrice() - discount));
					productVO.setDiscountMode(false);
					productVO.setDiscount(discount);
					productVO.setDiscountedValue(discount);
					previewsMap.put(currentComboPId, productVO);
				} else if (discountMode == (byte) 1 && discount > 0) {
					double discountedPrice = ((productVO.getTotalPrice() * discount) / 100);
					productTotalPrice = AIOSCommons.formatAmount((productVO
							.getTotalPrice() - discountedPrice));
					productVO.setDiscountMode(true);
					productVO.setDiscount(discountedPrice);
					productVO.setDiscountedValue(discount);
					previewsMap.put(currentComboPId, productVO);
				} else {
					productVO.setDiscountMode(false);
					productVO.setDiscount(0);
					productVO.setDiscountedValue(0);
					productTotalPrice = AIOSCommons.formatAmount(productVO
							.getDisplayPrice());
					previewsMap.put(currentComboPId, productVO);
				}
			}
			log.info("Module: Accounts : Method: calculateInlineDiscount: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: calculateInlineDiscount: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Show POS Payment Summary
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String showPaymentSummary() {
		try {
			log.info("Inside Module: Accounts : Method: showPaymentSummary");
			if (paymentMode > (byte) 0) {
				HttpSession session = ServletActionContext.getRequest()
						.getSession();
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();
				Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
						.getAttribute("POS_PRODUCT_PREVIEW_"
								+ sessionObj.get("jSessionId")));
				PointOfSaleVO salesSummarySession = (PointOfSaleVO) (session
						.getAttribute("SALES_SUMMARY_PREVIEW_"
								+ sessionObj.get("jSessionId")));
				pointOfSaleVO = new PointOfSaleVO();
				double totalSales = 0;
				double additionalCost = 0;
				List<ProductVO> productVOs = new ArrayList<ProductVO>(
						previewsMap.values());
				double specialDiscount = 0;
				if (dispatchDiscount > 0 || dispatchCost > 0) {
					additionalCost = dispatchCost;
					pointOfSaleVO
							.setAdditionalCost(dispatchCost > 0 ? AIOSCommons
									.formatAmount(dispatchCost) : null);
					pointOfSaleVO
							.setCustomerDiscount(dispatchDiscount > 0 ? AIOSCommons
									.formatAmount(dispatchDiscount) : null);
					specialDiscount = dispatchDiscount;
					for (ProductVO productVO : productVOs) {
						totalSales += productVO.getTotalPrice();
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							for (ProductVO tempproductVO : productVO
									.getProductComboVOs().values()) {
								totalSales += tempproductVO.getTotalPrice();
							}
						}
					}
				} else {
					for (ProductVO productVO : productVOs) {
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							for (ProductVO tempproductVO : productVO
									.getProductComboVOs().values())
								totalSales += tempproductVO.getTotalPrice();
						}
						/*
						 * if (productVO.getDiscount() > 0) { totalSales +=
						 * productVO.getTotalPrice() - productVO.getDiscount();
						 * } else totalSales += productVO.getTotalPrice();
						 */
						if (productVO.getCustomerDiscount() > 0) {
							specialDiscount += productVO.getCustomerDiscount();
						}

						specialDiscount += productVO.getDiscount();
						totalSales += productVO.getTotalPrice();
					}
					pointOfSaleVO
							.setCustomerDiscount(specialDiscount > 0 ? AIOSCommons
									.formatAmount(specialDiscount) : null);
					if (null != additionalCharges
							&& !("").equals(additionalCharges)
							&& !("[]").equals(additionalCharges)) {
						JSONParser parser = new JSONParser();
						Object summaryDetail = parser.parse(additionalCharges);
						if (null != summaryDetail
								&& !("").equals(summaryDetail)) {
							JSONArray jsonArray = (JSONArray) summaryDetail;
							for (Object jsonObject : jsonArray) {
								JSONObject object = (JSONObject) jsonObject;
								additionalCost += Double.parseDouble(object
										.get("charges").toString());
							}
							pointOfSaleVO.setAdditionalCost(AIOSCommons
									.formatAmount(additionalCost));
						}
					}
				}
				pointOfSaleVO.setSalesAmount(AIOSCommons
						.formatAmount(totalSales));
				pointOfSaleVO.setTotalDue(AIOSCommons
						.formatAmount((totalSales - specialDiscount)
								+ additionalCost));

				if (null != salesSummarySession
						&& !("").equals(salesSummarySession)) {
					pointOfSaleVO.setCashReceived(null != salesSummarySession
							.getCashReceived()
							&& !("").equals(salesSummarySession
									.getCashReceived()) ? salesSummarySession
							.getCashReceived() : null);
					pointOfSaleVO
							.setTransactionNumber(null != salesSummarySession
									.getTransactionNumber()
									&& !("").equals(salesSummarySession
											.getTransactionNumber()) ? salesSummarySession
									.getTransactionNumber() : null);
					pointOfSaleVO.setCardReceived(null != salesSummarySession
							.getCardReceived()
							&& !("").equals(salesSummarySession
									.getCardReceived()) ? salesSummarySession
							.getCardReceived() : null);
					pointOfSaleVO.setChequeReceived(null != salesSummarySession
							.getChequeReceived()
							&& !("").equals(salesSummarySession
									.getChequeReceived()) ? salesSummarySession
							.getChequeReceived() : null);
					pointOfSaleVO.setCouponReceived(null != salesSummarySession
							.getCouponReceived()
							&& !("").equals(salesSummarySession
									.getCouponReceived()) ? salesSummarySession
							.getCouponReceived() : null);
					pointOfSaleVO
							.setExchangeReceived(null != salesSummarySession
									.getExchangeReceived()
									&& !("").equals(salesSummarySession
											.getExchangeReceived()) ? salesSummarySession
									.getExchangeReceived() : null);

					// To understand whether order has been printed to submit to
					// customer

				}
				switch (paymentMode) {
				case 1:
					pointOfSaleVO.setCashReceived(AIOSCommons
							.formatAmount(receiveAmount));
					break;
				case 2:
					pointOfSaleVO.setChequeReceived(AIOSCommons
							.formatAmount(null != pointOfSaleVO
									.getChequeReceived()
									&& !("").equals(pointOfSaleVO
											.getChequeReceived()) ? AIOSCommons
									.formatAmountToDouble(pointOfSaleVO
											.getChequeReceived())
									+ receiveAmount : receiveAmount));
					pointOfSaleVO.setTransactionNumber(null != pointOfSaleVO
							.getTransactionNumber()
							&& !("").equals(pointOfSaleVO
									.getTransactionNumber()) ? (pointOfSaleVO
							.getTransactionNumber()) + "," + transactionNumber
							: transactionNumber);
					break;
				case 3:
					pointOfSaleVO.setCardReceived(AIOSCommons
							.formatAmount(null != pointOfSaleVO
									.getCardReceived()
									&& !("").equals(pointOfSaleVO
											.getCardReceived()) ? AIOSCommons
									.formatAmountToDouble(pointOfSaleVO
											.getCardReceived())
									+ receiveAmount : receiveAmount));
					pointOfSaleVO.setTransactionNumber(null != pointOfSaleVO
							.getTransactionNumber()
							&& !("").equals(pointOfSaleVO
									.getTransactionNumber()) ? (pointOfSaleVO
							.getTransactionNumber()) + "," + transactionNumber
							: transactionNumber);
					break;
				case 4:
					List<PointOfSaleVO> offerSummary = (List<PointOfSaleVO>) (session
							.getAttribute("OFFER_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId")));
					RewardPolicy rewardPolicy = pointOfSaleBL
							.calculateCouponReward(couponNumber, customerId);
					if (null != rewardPolicy) {
						PointOfSaleVO offerSaleVO = new PointOfSaleVO();
						offerSaleVO.setCouponId(rewardPolicy.getCoupon()
								.getCouponId());
						offerSaleVO.setRewardPolicyId(rewardPolicy
								.getRewardPolicyId());
						double rewardAmount = 0;
						if (rewardPolicy.getIsPercentage())
							rewardAmount = totalSales
									* rewardPolicy.getReward() / 100;
						else
							rewardAmount = rewardPolicy.getReward();
						pointOfSaleVO
								.setCouponReceived(AIOSCommons.formatAmount(null != pointOfSaleVO
										.getCouponReceived()
										&& !("").equals(pointOfSaleVO
												.getCouponReceived()) ? AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getCouponReceived())
										+ rewardAmount : rewardAmount));
						offerSaleVO.setOfferAmount(rewardAmount);
						offerSaleVO.setCouponNumber(couponNumber);
						if (null == offerSummary)
							offerSummary = new ArrayList<PointOfSaleVO>();
						offerSummary.add(offerSaleVO);
						session.setAttribute("OFFER_SUMMARY_PREVIEW_"
								+ sessionObj.get("jSessionId"), offerSummary);
					} else {
						returnMessage = "No reward found for this coupon number "
								+ couponNumber;
						log.info("Module: Accounts : Method: showPaymentSummary: Action Error");
						return ERROR;
					}
					break;
				case 5:
					Map<String, Object> exchangeSummary = (HashMap<String, Object>) (session
							.getAttribute("EXCHANGE_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId")));
					if (null != exchangeSummary && exchangeSummary.size() > 0
							&& exchangeSummary.containsKey(exchangeReference)) {
						returnMessage = "Exchange bill " + exchangeReference
								+ " already recorded.";
						log.info("Module: Accounts : Method: showPaymentSummary: Action Error");
						return ERROR;
					}
					/*
					 * MerchandiseExchange merchandiseExchange = pointOfSaleBL
					 * .getMerchandiseExchangeBL()
					 * .getMerchandiseExchangeByReference( exchangeReference);
					 */
					CreditNoteVO merchandiseExchange = pointOfSaleBL
							.getCreditBL().getExchangeInformation(
									exchangeReference);
					if (null != merchandiseExchange
							&& !("").equals(merchandiseExchange)) {

						pointOfSaleVO.setExchangeReceived(merchandiseExchange
								.getTotalReturn() + "");
						/*
						 * double totalPrice = 0; for (MerchandiseExchangeDetail
						 * merchandiseExchangeDetail : merchandiseExchange
						 * .getMerchandiseExchangeDetails()) { double
						 * totalPerPrice = merchandiseExchangeDetail
						 * .getPointOfSaleDetail().getUnitRate()
						 * merchandiseExchangeDetail .getReturnQuantity();
						 * double discountAmount = 0; if (null !=
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getIsPercentageDiscount() && null !=
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getDiscountValue() && merchandiseExchangeDetail
						 * .getPointOfSaleDetail() .getDiscountValue() > 0) { if
						 * (merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getIsPercentageDiscount()) discountAmount =
						 * (totalPerPrice * merchandiseExchangeDetail
						 * .getPointOfSaleDetail() .getDiscountValue()) / 100;
						 * else discountAmount = (totalPerPrice -
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getDiscountValue()); } if (null !=
						 * merchandiseExchangeDetail
						 * .getPointOfSaleDetail().getDiscountMethod()) {
						 * discountAmount += pointOfSaleBL .getDiscountAmount(
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getDiscountMethod() .getDiscount(),
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getDiscountMethod(), (merchandiseExchangeDetail
						 * .getPointOfSaleDetail() .getUnitRate() *
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getQuantity())); } else if (null !=
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getPromotionMethod()) { discountAmount +=
						 * pointOfSaleBL .getPromotionAmount(
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getPromotionMethod(), (merchandiseExchangeDetail
						 * .getPointOfSaleDetail() .getUnitRate() *
						 * merchandiseExchangeDetail .getPointOfSaleDetail()
						 * .getQuantity())); } totalPerPrice -= discountAmount;
						 * 
						 * totalPrice += totalPerPrice; pointOfSaleVO
						 * .setExchangeReceived(AIOSCommons.formatAmount(null !=
						 * pointOfSaleVO .getExchangeReceived() &&
						 * !("").equals(pointOfSaleVO .getExchangeReceived()) ?
						 * AIOSCommons .formatAmountToDouble(pointOfSaleVO
						 * .getExchangeReceived()) + totalPrice : totalPrice));
						 * if (null == exchangeSummary) exchangeSummary = new
						 * HashMap<String, Object>();
						 * exchangeSummary.put(exchangeReference,
						 * merchandiseExchange .getMerchandiseExchangeId());
						 * session.setAttribute("EXCHANGE_SUMMARY_PREVIEW_" +
						 * sessionObj.get("jSessionId"), exchangeSummary); }
						 */} else {
						returnMessage = "No Result found for Exchange Bill "
								+ exchangeReference;
						log.info("Module: Accounts : Method: showPaymentSummary: Action Error");
						return ERROR;
					}
					break;
				}
				pointOfSaleVO
						.setReceivedTotal(AIOSCommons.formatAmount((null != pointOfSaleVO
								.getCashReceived()
								&& !("").equals(pointOfSaleVO.getCashReceived()) ? AIOSCommons
								.formatAmountToDouble(pointOfSaleVO
										.getCashReceived()) : 0)
								+ (null != pointOfSaleVO.getCardReceived()
										&& !("").equals(pointOfSaleVO
												.getCardReceived()) ? AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getCardReceived()) : 0)
								+ (null != pointOfSaleVO.getChequeReceived()
										&& !("").equals(pointOfSaleVO
												.getChequeReceived()) ? AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getChequeReceived()) : 0)
								+ (null != pointOfSaleVO.getCouponReceived()
										&& !("").equals(pointOfSaleVO
												.getCouponReceived()) ? AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getCouponReceived()) : 0)
								+ (null != pointOfSaleVO.getExchangeReceived()
										&& !("").equals(pointOfSaleVO
												.getExchangeReceived()) ? AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getExchangeReceived()) : 0)
								+ 0));
				if ((null != pointOfSaleVO.getReceivedTotal() && !("")
						.equals(pointOfSaleVO.getReceivedTotal()))) {
					if (AIOSCommons.formatAmountToDouble(pointOfSaleVO
							.getTotalDue())
							- AIOSCommons.formatAmountToDouble(pointOfSaleVO
									.getReceivedTotal()) < 0) {
						pointOfSaleVO
								.setBalanceDue(AIOSCommons.formatAmount((Math.abs(AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getTotalDue())
										- AIOSCommons
												.formatAmountToDouble(pointOfSaleVO
														.getReceivedTotal())))));
					} else {
						pointOfSaleVO.setBalanceDue(null);
					}
					if ((AIOSCommons.formatAmountToDouble(pointOfSaleVO
							.getTotalDue()) - AIOSCommons
							.formatAmountToDouble(pointOfSaleVO
									.getReceivedTotal())) > 0)
						pointOfSaleVO
								.setOutStanding(AIOSCommons.formatAmount(AIOSCommons
										.formatAmountToDouble(pointOfSaleVO
												.getTotalDue())
										- AIOSCommons
												.formatAmountToDouble(pointOfSaleVO
														.getReceivedTotal())));
					else
						pointOfSaleVO.setOutStanding(null);
				}
				session.setAttribute(
						"SALES_SUMMARY_PREVIEW_" + sessionObj.get("jSessionId"),
						pointOfSaleVO);
			}
			log.info("Module: Accounts : Method: showPaymentSummary: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showPaymentSummary: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Save Point Of Sale
	 */
	@SuppressWarnings("unchecked")
	public String savePointOfSale() {
		try {
			log.info("Inside Module: Accounts : Method: savePointOfSale");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			PointOfSaleVO salesSummary = (PointOfSaleVO) session
					.getAttribute("SALES_SUMMARY_PREVIEW_"
							+ sessionObj.get("jSessionId"));
			if (null != salesSummary) {
				Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) session
						.getAttribute("SECURED_SALES_SESSION_"
								+ sessionObj.get("jSessionId"));

				if (null == salesSummary.getOutStanding()
						|| ("").equals(salesSummary.getOutStanding())
						|| AIOSCommons.formatAmountToDouble(salesSummary
								.getOutStanding()) <= 0) {
					POSUserTill posUserSession = (POSUserTill) (session
							.getAttribute("POS_USERTILL_SESSION"
									+ sessionObj.get("jSessionId")));
					PointOfSale pointOfSale = createPointOfSale(securedSalesSession
							.get(posSessionId));
					if (null == pointOfSale.getPersonByPersonId())
						pointOfSale.setPersonByPersonId(posUserSession
								.getPersonByPersonId());
					boolean customerPromotion = false;
					if (null != pointOfSale.getPromotionMethod()
							|| null != pointOfSale.getDiscountMethod())
						customerPromotion = true;
					List<PointOfSaleDetail> pointOfSalesDetailTemp = null;

					if (null != pointOfSale.getPointOfSaleId()
							&& pointOfSale.getPointOfSaleId() > 0) {
						PointOfSale pointOfSaleTemp = pointOfSaleBL
								.getPointOfSaleService()
								.getAllPointOfDetailByOrderId(
										pointOfSale.getPointOfSaleId());
						pointOfSalesDetailTemp = new ArrayList<PointOfSaleDetail>(
								pointOfSaleTemp.getPointOfSaleDetails());
					} else
						pointOfSale.setReferenceNumber(SystemBL
								.getReferenceStamp(PointOfSale.class.getName(),
										pointOfSaleBL.getImplementation()));

					pointOfSale.setPOSUserTill(posUserSession);
					if (null != pointOfSale.getCustomer()
							&& null != pointOfSale.getCustomer()
									.getCustomerId()) {
						Customer customer = pointOfSaleBL
								.getCustomerBL()
								.getCustomerService()
								.getCustomerDetails(
										pointOfSale.getCustomer()
												.getCustomerId());

						salesSummary
								.setCustomerName(null != customer
										.getPersonByPersonId() ? (null != customer
										.getPersonByPersonId().getPrefix() ? customer
										.getPersonByPersonId().getPrefix() : "")
										.concat(" "
												+ customer
														.getPersonByPersonId()
														.getFirstName()
														.concat(" ")
														.concat(customer
																.getPersonByPersonId()
																.getLastName()))
										: (null != customer.getCompany() ? customer
												.getCompany().getCompanyName()
												: customer.getCmpDeptLocation()
														.getCompany()
														.getCompanyName()));

						salesSummary
								.setCustomerNumber(null != customer ? customer
										.getCustomerNumber() : "");
					}
					salesSummary.setSaleType(pointOfSale.getSaleType());
					salesSummary.setReferenceNumber(pointOfSale
							.getReferenceNumber());
					session.setAttribute(
							"SALES_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId"),
							salesSummary);

					Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
							.getAttribute("POS_PRODUCT_PREVIEW_"
									+ sessionObj.get("jSessionId")));
					pointOfSale.setIsEodBalanced(false);
					pointOfSale.setProcessedSession((byte) 1);

					List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
					List<ProductVO> productVOs = new ArrayList<ProductVO>(
							previewsMap.values());
					double totalDiscount = null != pointOfSale
							.getTotalDiscount() ? pointOfSale
							.getTotalDiscount() : 0;
					for (ProductVO productVO : productVOs) {
						totalDiscount += productVO.getDiscount();
						pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
								customerPromotion));
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							for (ProductVO tempproductVO : productVO
									.getProductComboVOs().values()) {
								totalDiscount += tempproductVO.getDiscount();
								pointOfSaleDetails.add(addPointOfSaleDetail(
										tempproductVO, true));
							}
						}
					}
					pointOfSale.setTotalDiscount(totalDiscount);
					List<PointOfSaleCharge> pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();

					if (null != additionalCharges
							&& !("").equals(additionalCharges)
							&& !("[]").equals(additionalCharges)) {
						JSONParser parser = new JSONParser();
						Object summaryDetail = parser.parse(additionalCharges);
						if (null != summaryDetail
								&& !("").equals(summaryDetail)) {
							PointOfSaleCharge pointOfSaleCharge = null;
							LookupDetail lookupDetail = null;
							JSONArray jsonArray = (JSONArray) summaryDetail;
							for (Object jsonObject : jsonArray) {
								JSONObject object = (JSONObject) jsonObject;
								pointOfSaleCharge = new PointOfSaleCharge();
								lookupDetail = new LookupDetail();
								lookupDetail.setLookupDetailId(Long
										.parseLong(object.get("chargesType")
												.toString()));
								pointOfSaleCharge.setCharges(Double
										.parseDouble(object.get("charges")
												.toString()));
								pointOfSaleCharge.setDescription(object.get(
										"description").toString());
								pointOfSaleCharge.setLookupDetail(lookupDetail);
								pointOfSaleCharges.add(pointOfSaleCharge);
							}
						}
					}
					List<PointOfSaleReceipt> pointOfSaleReceipts = addSalesReceipts((PointOfSaleVO) (session
							.getAttribute("SALES_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId"))));
					List<PointOfSaleOffer> pointOfSaleOffers = addPointOfSaleOffer((List<PointOfSaleVO>) (session
							.getAttribute("OFFER_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId"))));

					// Sat null
					List<PointOfSaleVO> returnProductDetails = null;

					pointOfSaleBL.savePointOfSale(pointOfSale,
							pointOfSalesDetailTemp, pointOfSaleDetails,
							pointOfSaleCharges, pointOfSaleReceipts,
							pointOfSaleOffers, returnProductDetails,
							posUserSession);

					session.setAttribute(
							"POINT_OF_SALE_" + sessionObj.get("jSessionId"),
							pointOfSale);
					log.info("Module: Accounts : Method: savePointOfSale: Action Success");
					returnMessage = "SUCCESS";
					return SUCCESS;
				}
			}
			returnMessage = "Payment is not fully setteled.";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: savePointOfSale: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String processPrintPointOfSaleTemp() {
		try {
			log.info("Inside Module: Accounts : Method: processPrintPointOfSaleTemp");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			PointOfSaleVO salesSummary = (PointOfSaleVO) session
					.getAttribute("SALES_SUMMARY_PREVIEW_"
							+ sessionObj.get("jSessionId"));
			if (null != salesSummary
					&& (null == salesSummary.getOutStanding()
							|| ("").equals(salesSummary.getOutStanding()) || AIOSCommons
							.formatAmountToDouble(salesSummary.getOutStanding()) <= 0)) {
				returnMessage = "SUCCESS";
				return SUCCESS;
			}
			returnMessage = "Payment is not fully setteled.";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: processPrintPointOfSaleTemp: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String processPrintPointOfSale() {
		try {
			log.info("Inside Module: Accounts : Method: processPrintPointOfSale");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			PointOfSaleVO salesSummary = (PointOfSaleVO) session
					.getAttribute("SALES_SUMMARY_PREVIEW_"
							+ sessionObj.get("jSessionId"));
			if (null != salesSummary) {
				Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) session
						.getAttribute("SECURED_SALES_SESSION_"
								+ sessionObj.get("jSessionId"));

				if (null == salesSummary.getOutStanding()
						|| ("").equals(salesSummary.getOutStanding())
						|| AIOSCommons.formatAmountToDouble(salesSummary
								.getOutStanding()) <= 0) {
					POSUserTill posUserSession = (POSUserTill) (session
							.getAttribute("POS_USERTILL_SESSION"
									+ sessionObj.get("jSessionId")));
					PointOfSaleVO sessionPointOfSaleVO = securedSalesSession
							.get(posSessionId);
					if (personId > 0) {
						Person pr = pointOfSaleBL.getCustomerBL()
								.getCommentBL().getPersonDAO()
								.findById(personId);
						sessionPointOfSaleVO.setPersonByPersonId(pr);
					} else
						sessionPointOfSaleVO.setPersonByPersonId(posUserSession
								.getPersonByPersonId());
					securedSalesSession.put(posSessionId, sessionPointOfSaleVO);
					PointOfSale pointOfSale = createPointOfSale(sessionPointOfSaleVO);
					boolean customerPromotion = false;
					if (null != pointOfSale.getPromotionMethod()
							|| null != pointOfSale.getDiscountMethod())
						customerPromotion = true;
					pointOfSale.setPOSUserTill(posUserSession);
					if (null != pointOfSale.getCustomer()
							&& null != pointOfSale.getCustomer()
									.getCustomerId()) {
						Customer customer = pointOfSaleBL
								.getCustomerBL()
								.getCustomerService()
								.getCustomerDetails(
										pointOfSale.getCustomer()
												.getCustomerId());

						salesSummary
								.setCustomerName(null != customer
										.getPersonByPersonId() ? (null != customer
										.getPersonByPersonId().getPrefix() ? customer
										.getPersonByPersonId().getPrefix() : "")
										.concat(" "
												+ customer
														.getPersonByPersonId()
														.getFirstName()
														.concat(" ")
														.concat(customer
																.getPersonByPersonId()
																.getLastName()))
										: (null != customer.getCompany() ? customer
												.getCompany().getCompanyName()
												: customer.getCmpDeptLocation()
														.getCompany()
														.getCompanyName()));

						salesSummary
								.setCustomerNumber(null != customer ? customer
										.getCustomerNumber() : "");
					}
					salesSummary.setSaleType(pointOfSale.getSaleType());
					salesSummary.setReferenceNumber(pointOfSale
							.getReferenceNumber());
					salesSummary.setPersonByPersonId(sessionPointOfSaleVO
							.getPersonByPersonId());
					session.setAttribute(
							"SALES_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId"),
							salesSummary);

					Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
							.getAttribute("POS_PRODUCT_PREVIEW_"
									+ sessionObj.get("jSessionId")));
					pointOfSale.setIsEodBalanced(false);
					pointOfSale.setProcessedSession((byte) 1);

					List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
					List<ProductVO> productVOs = new ArrayList<ProductVO>(
							previewsMap.values());
					double totalDiscount = null != pointOfSale
							.getTotalDiscount() ? pointOfSale
							.getTotalDiscount() : 0;
					for (ProductVO productVO : productVOs) {
						totalDiscount += productVO.getDiscount();
						pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
								customerPromotion));
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							for (ProductVO tempproductVO : productVO
									.getProductComboVOs().values()) {
								totalDiscount += tempproductVO.getDiscount();
								pointOfSaleDetails.add(addPointOfSaleDetail(
										tempproductVO, true));
							}
						}
					}
					pointOfSale.setTotalDiscount(totalDiscount);
					List<PointOfSaleCharge> pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();

					if (null != additionalCharges
							&& !("").equals(additionalCharges)
							&& !("[]").equals(additionalCharges)) {
						JSONParser parser = new JSONParser();
						Object summaryDetail = parser.parse(additionalCharges);
						if (null != summaryDetail
								&& !("").equals(summaryDetail)) {
							PointOfSaleCharge pointOfSaleCharge = null;
							LookupDetail lookupDetail = null;
							JSONArray jsonArray = (JSONArray) summaryDetail;
							for (Object jsonObject : jsonArray) {
								JSONObject object = (JSONObject) jsonObject;
								pointOfSaleCharge = new PointOfSaleCharge();
								lookupDetail = new LookupDetail();
								lookupDetail.setLookupDetailId(Long
										.parseLong(object.get("chargesType")
												.toString()));
								pointOfSaleCharge.setCharges(Double
										.parseDouble(object.get("charges")
												.toString()));
								pointOfSaleCharge.setDescription(object.get(
										"description").toString());
								pointOfSaleCharge.setLookupDetail(lookupDetail);
								pointOfSaleCharges.add(pointOfSaleCharge);
							}
						}
					}
					session.setAttribute(
							"POINT_OF_SALE_" + sessionObj.get("jSessionId"),
							pointOfSale);
					log.info("Module: Accounts : Method: processPrintPointOfSale: Action Success");
					returnMessage = "SUCCESS";
					return SUCCESS;
				}
			}
			returnMessage = "Payment is not fully setteled.";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: processPrintPointOfSale: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Update Dispatch Point Of Sale
	 */
	public String updateDispatchPointOfSale() {
		try {
			log.info("Inside Module: Accounts : Method: updateDispatchPointOfSale");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			PointOfSaleVO salesSummary = (PointOfSaleVO) session
					.getAttribute("SALES_SUMMARY_PREVIEW_"
							+ sessionObj.get("jSessionId"));
			if (null != salesSummary) {

				if (null == salesSummary.getOutStanding()
						|| ("").equals(salesSummary.getOutStanding())
						|| Double.parseDouble(salesSummary.getOutStanding()) <= 0) {
					PointOfSale pointOfSale = pointOfSaleBL
							.getPointOfSaleService().getPointOfDetailByOrderId(
									pointOfSaleId);

					if (personId > 0) {
						Person pr = pointOfSaleBL.getCustomerBL()
								.getCommentBL().getPersonDAO()
								.findById(personId);
						pointOfSale.setPersonByPersonId(pr);
					}

					if (null != pointOfSale.getCustomer()
							&& null != pointOfSale.getCustomer()
									.getCustomerId()) {
						Customer customer = pointOfSaleBL
								.getCustomerBL()
								.getCustomerService()
								.getCustomerDetails(
										pointOfSale.getCustomer()
												.getCustomerId());

						salesSummary
								.setCustomerName(null != customer
										.getPersonByPersonId() ? (null != customer
										.getPersonByPersonId().getPrefix() ? customer
										.getPersonByPersonId().getPrefix() : "")
										.concat(" "
												+ customer
														.getPersonByPersonId()
														.getFirstName()
														.concat(" ")
														.concat(customer
																.getPersonByPersonId()
																.getLastName()))
										: (null != customer.getCompany() ? customer
												.getCompany().getCompanyName()
												: customer.getCmpDeptLocation()
														.getCompany()
														.getCompanyName()));

						salesSummary
								.setCustomerNumber(null != customer ? customer
										.getCustomerNumber() : "");
					}
					salesSummary.setReferenceNumber(pointOfSale
							.getReferenceNumber());
					salesSummary.setSaleType(InvoiceSalesType.Delivery
							.getCode());
					salesSummary.setPersonByPersonId(pointOfSale
							.getPersonByPersonId());
					session.setAttribute(
							"SALES_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId"),
							salesSummary);

					pointOfSale.setIsEodBalanced(true);
					pointOfSale.setProcessedSession((byte) 1);
					pointOfSale.setDeliveryStatus(true);

					List<PointOfSaleReceipt> pointOfSaleReceipts = addSalesReceipts((PointOfSaleVO) (session
							.getAttribute("SALES_SUMMARY_PREVIEW_"
									+ sessionObj.get("jSessionId"))));

					// Set Null
					List<PointOfSaleVO> returnProductDetails = null;
					pointOfSaleBL.updateDispatchPointOfSale(
							pointOfSale,
							pointOfSaleReceipts,
							returnProductDetails,
							new ArrayList<PointOfSaleDetail>(pointOfSale
									.getPointOfSaleDetails()), pointOfSale
									.getPOSUserTill());

					session.setAttribute(
							"POINT_OF_SALE_" + sessionObj.get("jSessionId"),
							pointOfSale);
					log.info("Module: Accounts : Method: updateDispatchPointOfSale: Action Success");
					returnMessage = "SUCCESS";
					return SUCCESS;
				}
			}
			returnMessage = "Payment is not fully setteled.";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: updateDispatchPointOfSale: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Dispatch Point Of Sale
	 */
	@SuppressWarnings("unchecked")
	public String dispatchPointOfSale() {
		try {
			log.info("Inside Module: Accounts : Method: dispatchPointOfSale");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) session
					.getAttribute("SECURED_SALES_SESSION_"
							+ sessionObj.get("jSessionId"));
			pointOfSaleVO = securedSalesSession.get(posSessionId);
			pointOfSaleVO.setOrderPrinted(true);
			securedSalesSession.put(posSessionId, pointOfSaleVO);
			session.setAttribute(
					"SECURED_SALES_SESSION_" + sessionObj.get("jSessionId"),
					securedSalesSession);
			PointOfSale pointOfSale = createPointOfSale(securedSalesSession
					.get(posSessionId));
			boolean customerPromotion = false;
			if (null != pointOfSale.getPromotionMethod()
					|| null != pointOfSale.getDiscountMethod())
				customerPromotion = true;
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			pointOfSale.setPOSUserTill(posUserSession);
			List<PointOfSaleDetail> pointOfSalesDetailTemp = null;
			if (null != pointOfSale.getPointOfSaleId()
					&& pointOfSale.getPointOfSaleId() > 0) {
				PointOfSale pointOfSaleTemp = pointOfSaleBL
						.getPointOfSaleService().getAllPointOfDetailByOrderId(
								pointOfSale.getPointOfSaleId());
				pointOfSalesDetailTemp = new ArrayList<PointOfSaleDetail>(
						pointOfSaleTemp.getPointOfSaleDetails());
				pointOfSale.setReferenceNumber(pointOfSaleTemp
						.getReferenceNumber());
			} else
				pointOfSale.setReferenceNumber(SystemBL.getReferenceStamp(
						PointOfSale.class.getName(),
						pointOfSaleBL.getImplementation()));

			if (null != pointOfSale.getCustomer()
					&& null != pointOfSale.getCustomer().getCustomerId()) {
				Customer customer = pointOfSaleBL
						.getCustomerBL()
						.getCustomerService()
						.getCustomerDetails(
								pointOfSale.getCustomer().getCustomerId());

				pointOfSaleVO.setCustomerName(null != customer
						.getPersonByPersonId() ? (null != customer
						.getPersonByPersonId().getPrefix() ? customer
						.getPersonByPersonId().getPrefix() : "").concat(" "
						+ customer
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(customer.getPersonByPersonId()
										.getLastName())) : (null != customer
						.getCompany() ? customer.getCompany().getCompanyName()
						: customer.getCmpDeptLocation().getCompany()
								.getCompanyName()));
				pointOfSaleVO.setCustomerNumber((null != customer) ? customer
						.getCustomerNumber() : "");
				pointOfSaleVO.setCustomerMobile(customerMobile);
				pointOfSaleVO.setCustomerLocation(customerLocation);
			}
			pointOfSaleVO.setReferenceNumber(pointOfSale.getReferenceNumber());

			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			pointOfSale.setProcessedSession((byte) 0);
			pointOfSale.setDeliveryStatus(false);
			pointOfSale.setOfflineEntry(false);
			pointOfSale.setOrderPrinted(true);
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			double totalDiscount = null != pointOfSale.getTotalDiscount() ? pointOfSale
					.getTotalDiscount() : 0;
			for (ProductVO productVO : productVOs) {
				totalDiscount += productVO.getDiscount();
				pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
						customerPromotion));
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO tempproductVO : productVO
							.getProductComboVOs().values()) {
						totalDiscount += tempproductVO.getDiscount();
						pointOfSaleDetails.add(addPointOfSaleDetail(
								tempproductVO, true));
					}
				}
			}
			List<PointOfSaleCharge> pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();

			pointOfSale.setDriverNumber(driverMobile);
			pointOfSale.setTotalDiscount(totalDiscount);
			Person person = new Person();
			person.setPersonId(driverId);
			pointOfSale.setPersonByDriverId(person);
			double addtionalCharges = 0;
			if (!freeDelivery) {
				PointOfSaleCharge pointOfSaleCharge = new PointOfSaleCharge();
				LookupDetail lookupDetail = pointOfSaleBL.getLookupMasterBL()
						.getActiveLookupDetails("CHARGES_TYPE", true).get(0);
				pointOfSaleCharge.setCharges(deliveryCharges);
				pointOfSaleCharge.setLookupDetail(lookupDetail);
				pointOfSaleCharge.setDescription("Delivery Charges");
				pointOfSaleCharges.add(pointOfSaleCharge);
				pointOfSaleVO.setDeliveryCharges(deliveryCharges);
			} else
				pointOfSaleVO.setDeliveryCharges(0);

			if (null != additionalCharges && !("").equals(additionalCharges)
					&& !("[]").equals(additionalCharges)) {
				JSONParser parser = new JSONParser();
				Object summaryDetail = parser.parse(additionalCharges);
				if (null != summaryDetail && !("").equals(summaryDetail)) {
					PointOfSaleCharge pointOfSaleCharge = null;
					LookupDetail lookupDetail = null;
					JSONArray jsonArray = (JSONArray) summaryDetail;
					for (Object jsonObject : jsonArray) {
						JSONObject object = (JSONObject) jsonObject;
						pointOfSaleCharge = new PointOfSaleCharge();
						lookupDetail = new LookupDetail();
						lookupDetail.setLookupDetailId(Long.parseLong(object
								.get("chargesType").toString()));
						pointOfSaleCharge.setCharges(Double.parseDouble(object
								.get("charges").toString()));
						pointOfSaleCharge.setDescription(object.get(
								"description").toString());
						pointOfSaleCharge.setLookupDetail(lookupDetail);
						addtionalCharges += pointOfSaleCharge.getCharges();
						pointOfSaleCharges.add(pointOfSaleCharge);
					}
				}
			}
			pointOfSaleVO.setAdditionalCost(addtionalCharges > 0 ? AIOSCommons
					.formatAmount(addtionalCharges) : null);
			double totalDues = (AIOSCommons.formatAmountToDouble((pointOfSaleVO
					.getTotalDue())) + addtionalCharges + deliveryCharges)
					- totalDiscount;
			pointOfSaleVO.setTotalDue(AIOSCommons.formatAmount(totalDues));
			pointOfSaleVO.setSaleType(salesType);
			pointOfSaleBL.dispatchPointOfSale(pointOfSale,
					pointOfSalesDetailTemp, pointOfSaleDetails,
					pointOfSaleCharges);

			session.setAttribute(
					"SALES_SUMMARY_PREVIEW_" + sessionObj.get("jSessionId"),
					pointOfSaleVO);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: dispatchPointOfSale: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: dispatchPointOfSale: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Cheque Or Card Details
	public String getPOSReceiptDetails() {
		try {
			log.info("Inside Module: Accounts : Method: getPOSReceiptDetails");
			List<PointOfSaleReceipt> pointOfSaleReceipts = pointOfSaleBL
					.getPointOfSaleService()
					.getPointOfSaleReceiptsByCurrencyMode(
							pointOfSaleBL.getImplementation(),
							DateFormat.getFromDate(Calendar.getInstance()
									.getTime()),
							DateFormat.getToDate(Calendar.getInstance()
									.getTime()), paymentMode);
			pointOfSaleVO = new PointOfSaleVO();
			if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
				double totalSales = 0;
				for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
					totalSales += pointOfSaleReceipt.getReceipt();
				pointOfSaleVO.setSalesAmount(AIOSCommons
						.formatAmount(totalSales));
				pointOfSaleVO.setBalanceDue(String.valueOf(totalSales));
				pointOfSaleVO.setTotalDue(String.valueOf(pointOfSaleReceipts
						.size()));
			}
			log.info("Module: Accounts : Method: getPOSReceiptDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			productTotalPrice = "0";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: getPOSReceiptDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Add PointOfSale Offer
	private List<PointOfSaleOffer> addPointOfSaleOffer(
			List<PointOfSaleVO> pointOfSaleVOs) {
		if (null != pointOfSaleVOs && pointOfSaleVOs.size() > 0) {
			PointOfSaleOffer pointOfSaleOffer = null;
			List<PointOfSaleOffer> pointOfSaleOffers = new ArrayList<PointOfSaleOffer>();
			for (PointOfSaleVO pointOfSaleVO : pointOfSaleVOs) {
				pointOfSaleOffer = new PointOfSaleOffer();
				if (pointOfSaleVO.getCouponId() > 0) {
					Coupon coupon = new Coupon();
					coupon.setCouponId(pointOfSaleVO.getCouponId());
					pointOfSaleOffer.setCoupon(coupon);
					pointOfSaleOffer.setCouponNumber(pointOfSaleVO
							.getCouponNumber());
				}
				if (pointOfSaleVO.getRewardPolicyId() > 0) {
					RewardPolicy rewardPolicy = new RewardPolicy();
					rewardPolicy.setRewardPolicyId(pointOfSaleVO
							.getRewardPolicyId());
					pointOfSaleOffer.setRewardPolicy(rewardPolicy);
				}
				pointOfSaleOffer.setOfferAmount(pointOfSaleVO.getOfferAmount());
				pointOfSaleOffers.add(pointOfSaleOffer);
			}
			return pointOfSaleOffers;
		}
		return null;
	}

	// Add Point Of Sale Receipt
	private List<PointOfSaleReceipt> addSalesReceipts(
			PointOfSaleVO pointOfSaleVO) {
		PointOfSaleReceipt pointOfSaleReceipt = null;
		List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
		if (pointOfSaleVO == null)
			return null;

		if (null != pointOfSaleVO.getCashReceived()
				&& !("").equals(pointOfSaleVO.getCashReceived())) {
			pointOfSaleReceipt = new PointOfSaleReceipt();
			pointOfSaleReceipt
					.setReceiptType(Constants.Accounts.POSPaymentType.Cash
							.getCode());
			pointOfSaleReceipt.setReceipt(AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getCashReceived()));
			pointOfSaleReceipt.setBalanceDue(null != pointOfSaleVO
					.getBalanceDue() ? (AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getBalanceDue())) : 0);
			// pointOfSaleReceipt.setBalanceDue(null);
			pointOfSaleReceipts.add(pointOfSaleReceipt);
		}
		if (null != pointOfSaleVO.getCardReceived()
				&& !("").equals(pointOfSaleVO.getCardReceived())) {
			pointOfSaleReceipt = new PointOfSaleReceipt();
			pointOfSaleReceipt
					.setReceiptType(Constants.Accounts.POSPaymentType.Card
							.getCode());
			pointOfSaleReceipt.setReceipt(AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getCardReceived()));
			pointOfSaleReceipt.setTransactionNumber(pointOfSaleVO
					.getTransactionNumber());
			pointOfSaleReceipts.add(pointOfSaleReceipt);
		}
		if (null != pointOfSaleVO.getChequeReceived()
				&& !("").equals(pointOfSaleVO.getChequeReceived())) {
			pointOfSaleReceipt = new PointOfSaleReceipt();
			pointOfSaleReceipt
					.setReceiptType(Constants.Accounts.POSPaymentType.Cheque
							.getCode());
			pointOfSaleReceipt.setReceipt(AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getChequeReceived()));
			pointOfSaleReceipt.setTransactionNumber(pointOfSaleVO
					.getTransactionNumber());
			pointOfSaleReceipts.add(pointOfSaleReceipt);
		}
		if (null != pointOfSaleVO.getCouponReceived()
				&& !("").equals(pointOfSaleVO.getCouponReceived())) {
			pointOfSaleReceipt = new PointOfSaleReceipt();
			pointOfSaleReceipt
					.setReceiptType(Constants.Accounts.POSPaymentType.Coupon
							.getCode());
			pointOfSaleReceipt.setReceipt(AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getCouponReceived()));
			pointOfSaleReceipts.add(pointOfSaleReceipt);
		}
		if (null != pointOfSaleVO.getExchangeReceived()
				&& !("").equals(pointOfSaleVO.getExchangeReceived())) {
			pointOfSaleReceipt = new PointOfSaleReceipt();
			pointOfSaleReceipt
					.setReceiptType(Constants.Accounts.POSPaymentType.Exchange
							.getCode());
			pointOfSaleReceipt.setReceipt(AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getExchangeReceived()));
			pointOfSaleReceipt.setBalanceDue(null != pointOfSaleVO
					.getBalanceDue() ? (AIOSCommons
					.formatAmountToDouble(pointOfSaleVO.getBalanceDue())) : 0);
			pointOfSaleReceipts.add(pointOfSaleReceipt);
		}
		return pointOfSaleReceipts;
	}

	// Add Point Of Sale Detail
	private PointOfSaleDetail addPointOfSaleDetail(ProductVO productVO,
			boolean customerPromotion) throws Exception {
		PointOfSaleDetail pointOfSaleDetail = new PointOfSaleDetail();
		Product product = new Product();
		product.setProductId(productVO.getProductId());
		pointOfSaleDetail.setProductByProductId(product);
		if (productVO.isPromotionFlag() && !customerPromotion) {
			if (productVO.getPromotionMethodId() > 0) {
				PromotionMethod promotionMethod = new PromotionMethod();
				promotionMethod.setPromotionMethodId(productVO
						.getPromotionMethodId());
				pointOfSaleDetail.setPromotionMethod(promotionMethod);
			}
		}
		if (productVO.isDiscountFlag() && !customerPromotion) {
			if (productVO.getDiscountMethodId() > 0) {
				DiscountMethod discountMethod = new DiscountMethod();
				discountMethod.setDiscountMethodId(productVO
						.getDiscountMethodId());
				pointOfSaleDetail.setDiscountMethod(discountMethod);
			}
		}
		pointOfSaleDetail.setQuantity(productVO.getQuantity());
		pointOfSaleDetail.setUnitRate(productVO.getFinalUnitPrice());
		if (productVO.isDiscountMode()) {
			pointOfSaleDetail.setIsPercentageDiscount(productVO
					.isDiscountMode());
			pointOfSaleDetail.setDiscountValue(productVO.getDiscountedValue());
		} else {
			if (productVO.getDiscount() > 0) {
				pointOfSaleDetail.setIsPercentageDiscount(false);
				pointOfSaleDetail.setDiscountValue(productVO
						.getDiscountedValue());
			}
		}
		pointOfSaleDetail.setActualPoints(productVO.getProductPurchasePoints());
		pointOfSaleDetail.setPromotionPoints(productVO.getPromotionPoints());
		pointOfSaleDetail.setSpecialProduct(productVO.getSpecialPos());
		if (null != productVO.getComboType()
				&& (boolean) productVO.getComboType()) {
			pointOfSaleDetail.setComboType(productVO.getComboType());
			Product comboProduct = new Product();
			comboProduct.setProductId(productVO.getComboProductId());
			pointOfSaleDetail.setProductByComboProductId(comboProduct);
		}
		pointOfSaleDetail.setComboItemOrder(productVO.getComboItemOrder());
		pointOfSaleDetail.setItemOrder(productVO.getItemOrder());
		return pointOfSaleDetail;
	}

	// Add Point Of Sale Detail
	private PointOfSaleDetail addPointOfSaleDetailEmployeeServe(
			ProductVO productVO) throws Exception {
		PointOfSaleDetail pointOfSaleDetail = new PointOfSaleDetail();
		Product product = new Product();
		product.setProductId(productVO.getProductId());
		pointOfSaleDetail.setProductByProductId(product);
		pointOfSaleDetail.setQuantity(productVO.getQuantity());
		pointOfSaleDetail.setUnitRate(productVO.getQuantity()
				* productVO.getBasePrice());
		pointOfSaleDetail.setSpecialProduct(productVO.getSpecialPos());
		if (null != productVO.getComboType()
				&& (boolean) productVO.getComboType()) {
			pointOfSaleDetail.setComboType(productVO.getComboType());
			Product comboProduct = new Product();
			comboProduct.setProductId(productVO.getComboProductId());
			pointOfSaleDetail.setProductByComboProductId(comboProduct);
		}
		pointOfSaleDetail.setComboItemOrder(productVO.getComboItemOrder());
		pointOfSaleDetail.setItemOrder(productVO.getItemOrder());
		return pointOfSaleDetail;
	}

	/**
	 * Add charges details
	 * 
	 * @return
	 */
	public String chargesAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: chargesAddRow");
			ServletActionContext.getRequest().setAttribute(
					"CHARGES_TYPE",
					pointOfSaleBL.getPromotionBL().getLookupMasterBL()
							.getActiveLookupDetails("CHARGES_TYPE", true));
			log.info("Module: Accounts : Method: chargesAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: chargesAddRow: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String customerReceiptPrintBeforePayment() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) (session
					.getAttribute("SECURED_SALES_SESSION_"
							+ sessionObj.get("jSessionId")));
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			pointOfSaleVO = securedSalesSession.get(posSessionId);
			PointOfSale pointOfSale = createPointOfSale(pointOfSaleVO);
			boolean customerPromotion = false;
			if (null != pointOfSaleVO.getPromotionMethod()
					|| null != pointOfSaleVO.getDiscountMethod())
				customerPromotion = true;

			pointOfSale.setPOSUserTill(posUserSession);
			PointOfSale pointOfSaleTemp = null;
			List<PointOfSaleDetail> pointOfSaleDetailsTemp = null;
			if (null != pointOfSale.getPointOfSaleId()
					&& pointOfSale.getPointOfSaleId() > 0) {
				pointOfSaleTemp = pointOfSaleBL.getPointOfSaleService()
						.getPointOfDetailByOrderId(
								pointOfSale.getPointOfSaleId());
				pointOfSaleDetailsTemp = new ArrayList<PointOfSaleDetail>(
						pointOfSaleTemp.getPointOfSaleDetails());
			}

			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			for (ProductVO productVO : productVOs) {
				pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
						customerPromotion));
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO comboProduct : productVO
							.getProductComboVOs().values()) {
						pointOfSaleDetails.add(addPointOfSaleDetail(
								comboProduct, true));
					}
				}
			}

			pointOfSale.setOrderPrinted(true);
			pointOfSaleBL.updatePointOfSale(pointOfSale,
					pointOfSaleDetailsTemp, pointOfSaleDetails);
			pointOfSaleVO.setPointOfSaleId(pointOfSale.getPointOfSaleId());
			pointOfSaleVO.setQuotation("Quotation");
			securedSalesSession.put(posSessionId, pointOfSaleVO);
			session.setAttribute(
					"SECURED_SALES_SESSION_" + sessionObj.get("jSessionId"),
					securedSalesSession);
			customerReceiptPrint();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: customerReceiptPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String customerReceiptPrint() {
		try {
			log.info("Inside Module: Accounts : Method: customerReceiptPrint");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) (session
					.getAttribute("SECURED_SALES_SESSION_"
							+ sessionObj.get("jSessionId")));
			pointOfSaleVO = securedSalesSession.get(posSessionId);
			pointOfSaleVO.setOrderPrinted(true);
			boolean customerPromotion = false;
			if (null != pointOfSaleVO.getPromotionMethod()
					|| null != pointOfSaleVO.getDiscountMethod())
				customerPromotion = true;
			securedSalesSession.put(posSessionId, pointOfSaleVO);
			session.setAttribute(
					"SECURED_SALES_SESSION_" + sessionObj.get("jSessionId"),
					securedSalesSession);
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			for (ProductVO productVO : productVOs) {
				pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
						customerPromotion));
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO comboProduct : productVO
							.getProductComboVOs().values()) {
						pointOfSaleDetails.add(addPointOfSaleDetail(
								comboProduct, true));
					}
				}
			}
			double additionalCost = 0;
			if (null != additionalCharges && !("").equals(additionalCharges)
					&& !("[]").equals(additionalCharges)) {
				JSONParser parser = new JSONParser();
				Object summaryDetail = parser.parse(additionalCharges);
				if (null != summaryDetail && !("").equals(summaryDetail)) {
					JSONArray jsonArray = (JSONArray) summaryDetail;
					for (Object jsonObject : jsonArray) {
						JSONObject object = (JSONObject) jsonObject;
						additionalCost += Double.parseDouble(object.get(
								"charges").toString());
					}
				}
			}
			double totalSales = 0;
			double totalDiscount = 0;
			for (ProductVO productVO : productVOs) {
				totalDiscount += productVO.getDiscount();
				totalDiscount += productVO.getCustomerDiscount();
				totalSales += productVO.getTotalPrice();
			}
			totalSales -= totalDiscount;
			totalSales += additionalCost;
			pointOfSaleVO.setTotalDue(AIOSCommons.formatAmount(totalSales));
			pointOfSaleVO.setOrderPrinted(true);
			if (additionalCost > 0)
				pointOfSaleVO.setAdditionalCost(AIOSCommons
						.formatAmount(additionalCost));
			if (null != pointOfSaleVO.getCustomer()
					&& null != pointOfSaleVO.getCustomer().getCustomerId()) {
				Customer customer = pointOfSaleBL
						.getCustomerBL()
						.getCustomerService()
						.getCustomerDetails(
								pointOfSaleVO.getCustomer().getCustomerId());

				pointOfSaleVO.setCustomerName(null != customer
						.getPersonByPersonId() ? customer
						.getPersonByPersonId()
						.getPrefix()
						.concat(" "
								+ customer
										.getPersonByPersonId()
										.getFirstName()
										.concat(" ")
										.concat(customer.getPersonByPersonId()
												.getLastName()))
						: (null != customer.getCompany() ? customer
								.getCompany().getCompanyName() : customer
								.getCmpDeptLocation().getCompany()
								.getCompanyName()));

				pointOfSaleVO.setCustomerNumber(null != customer ? customer
						.getCustomerNumber() : "");
			}
			session.setAttribute(
					"SALES_SUMMARY_PREVIEW_" + sessionObj.get("jSessionId"),
					pointOfSaleVO);
			log.info("Module: Accounts : Method: customerReceiptPrint: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: customerReceiptPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Save Point Of Sale
	 */
	@SuppressWarnings("unchecked")
	public String savePointOfSaleTemprory() {
		try {
			log.info("Inside Module: Accounts : Method: savePointOfSaleTemprory");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			Map<String, PointOfSaleVO> securedSalesSession = (Map<String, PointOfSaleVO>) (session
					.getAttribute("SECURED_SALES_SESSION_"
							+ sessionObj.get("jSessionId")));
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			pointOfSaleVO = securedSalesSession.get(posSessionId);
			PointOfSale pointOfSale = createPointOfSale(pointOfSaleVO);
			boolean customerPromotion = false;
			if (null != pointOfSaleVO.getPromotionMethod()
					|| null != pointOfSaleVO.getDiscountMethod())
				customerPromotion = true;

			pointOfSale.setPOSUserTill(posUserSession);
			PointOfSale pointOfSaleTemp = null;
			List<PointOfSaleDetail> pointOfSaleDetailsTemp = null;
			if (null != pointOfSale.getPointOfSaleId()
					&& pointOfSale.getPointOfSaleId() > 0) {
				pointOfSaleTemp = pointOfSaleBL.getPointOfSaleService()
						.getPointOfDetailByOrderId(
								pointOfSale.getPointOfSaleId());
				pointOfSaleDetailsTemp = new ArrayList<PointOfSaleDetail>(
						pointOfSaleTemp.getPointOfSaleDetails());
			}

			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			for (ProductVO productVO : productVOs) {
				pointOfSaleDetails.add(addPointOfSaleDetail(productVO,
						customerPromotion));
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO comboProduct : productVO
							.getProductComboVOs().values()) {
						pointOfSaleDetails.add(addPointOfSaleDetail(
								comboProduct, true));
					}
				}
			}
			pointOfSaleBL.updatePointOfSale(pointOfSale,
					pointOfSaleDetailsTemp, pointOfSaleDetails);
			session.removeAttribute("POS_PRODUCT_PREVIEW_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("SECURED_SALES_SESSION_"
					+ sessionObj.get("jSessionId"));
			log.info("Module: Accounts : Method: savePointOfSaleTemprory: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: savePointOfSaleTemprory: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private PointOfSale createPointOfSale(PointOfSaleVO pointOfSaleVO) {
		PointOfSale pointOfSale = new PointOfSale();
		if (pointOfSaleVO.getCustomerId() > 0) {
			Customer customer = new Customer();
			customer.setCustomerId(pointOfSaleVO.getCustomerId());
			pointOfSale.setCustomer(customer);
			if (null != pointOfSaleVO.getShippingId()
					&& pointOfSaleVO.getShippingId() > 0) {
				ShippingDetail shippingDetail = new ShippingDetail();
				shippingDetail.setShippingId(pointOfSaleVO.getShippingId());
			}
		}
		if (null != pointOfSaleVO.getDeliveryOption()
				&& pointOfSaleVO.getDeliveryOption() > 0) {
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(pointOfSaleVO.getDeliveryOption());
			pointOfSale.setLookupDetail(lookupDetail);
		}
		pointOfSale.setSaleType(pointOfSaleVO.getSaleType());
		pointOfSale.setIsEodBalanced(null);
		pointOfSale.setReferenceNumber(pointOfSaleVO.getReferenceNumber());
		pointOfSale.setSalesDate(pointOfSaleVO.getSalesDate());
		pointOfSale.setOfflineEntry(false);
		pointOfSale.setOrderPrinted(pointOfSaleVO.getOrderPrinted());
		pointOfSale.setProcessedSession((byte) 0);
		pointOfSale.setSaleType(pointOfSaleVO.getSaleType());
		pointOfSale
				.setDeliveryOptionName(pointOfSaleVO.getDeliveryOptionName());
		pointOfSale.setPointOfSaleId(pointOfSaleVO.getPointOfSaleId());
		pointOfSale.setPromotionMethod(pointOfSaleVO.getPromotionMethod());
		pointOfSale.setDiscountMethod(pointOfSaleVO.getDiscountMethod());
		pointOfSale
				.setTotalDiscount(null != pointOfSaleVO.getCustomerDiscount()
						&& !("").equals(pointOfSaleVO.getCustomerDiscount()) ? AIOSCommons
						.formatAmountToDouble(pointOfSaleVO
								.getCustomerDiscount()) : null);
		pointOfSale.setPersonByPersonId(pointOfSaleVO.getPersonByPersonId());
		return pointOfSale;
	}

	@SuppressWarnings("unchecked")
	public String sendOrderPrinters() {
		try {
			log.info("Inside Module: Accounts : Method: sendOrderPrinters");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			if (null != previewsMap && previewsMap.size() > 0) {
				List<ProductVO> productVOTemps = new ArrayList<ProductVO>(
						previewsMap.values());
				List<ProductVO> kitchenPrinters = new ArrayList<ProductVO>();
				List<ProductVO> barPrinters = new ArrayList<ProductVO>();
				for (ProductVO productVO : productVOTemps) {
					if ((boolean) productVO.isBarPrinter() == true) {
						barPrinters.add(productVO);
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							barPrinters.addAll(productVO.getProductComboVOs()
									.values());
						}
					} else if ((boolean) productVO.isKitchenPrinter() == true) {
						kitchenPrinters.add(productVO);
						if (null != productVO.getProductComboVOs()
								&& productVO.getProductComboVOs().size() > 0) {
							kitchenPrinters.addAll(productVO
									.getProductComboVOs().values());
						}
					}
				}
				POSUserTill posUserSession = (POSUserTill) (session
						.getAttribute("POS_USERTILL_SESSION"
								+ sessionObj.get("jSessionId")));
				printers = null;
				barOrders = null;
				kitchenOrders = null;

				if (null != kitchenPrinters && kitchenPrinters.size() > 0) {
					printers = posUserSession.getKitchenPrinter();
					kitchenOrders = createReciptPrintContent(kitchenPrinters);
				}
				if (null != barPrinters && barPrinters.size() > 0) {
					printers = null != printers && !("").equals(printers) ? printers
							.concat(",").concat(posUserSession.getBarPrinter())
							: posUserSession.getBarPrinter();
					barOrders = createReciptPrintContent(barPrinters);
				}
			}
			log.info("Module: Accounts : Method: sendOrderPrinters: Action Success");
			returnMessage = "SUCCESS";
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			ex.printStackTrace();
			log.info("Module: Accounts : Method: sendOrderPrinters: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String createReciptPrintContent(List<ProductVO> productVOs) {
		try {
			log.info("Inside Module: Accounts : Method: createReciptPrintContent");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configuration config = new Configuration();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/pos/"
					+ pointOfSaleBL.getImplementation().getImplementationId()));
			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			String contextPath = ServletActionContext.getRequest()
					.getContextPath(); // includes leading forward slash

			String urlPath = scheme + "://" + host + contextPath;

			Template template = config.getTemplate("pos-order-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			rootMap.put("logo", urlPath);
			rootMap.put("title", posUserSession.getStore().getCmpDeptLocation()
					.getCompany().getCompanyName());
			DateTime dt = new DateTime();
			rootMap.put("datetime",
					DateFormat.convertSystemDateToString(new Date()) + " "
							+ dt.toLocalTime().getHourOfDay() + ":"
							+ dt.toLocalTime().getMinuteOfHour());
			rootMap.put("telephone",
					posUserSession.getStore().getCmpDeptLocation().getCompany()
							.getCompanyPhone() != null ? posUserSession
							.getStore().getCmpDeptLocation().getCompany()
							.getCompanyPhone() : "");

			rootMap.put("servedBy", (posUserSession.getPersonByPersonId()
					.getPrefix() != null ? posUserSession.getPersonByPersonId()
					.getPrefix() + " " : " ")
					+ posUserSession.getPersonByPersonId().getFirstName()
					+ " "
					+ posUserSession.getPersonByPersonId().getLastName());

			rootMap.put("referenceNumber", salesRefNumber);
			rootMap.put("tillNumber", posUserSession.getTillNumber());
			rootMap.put(
					"branchContact",
					posUserSession.getStore().getCmpDeptLocation()
							.getLocation().getContactNumber() != null ? posUserSession
							.getStore().getCmpDeptLocation().getLocation()
							.getContactNumber()
							: "");
			rootMap.put("location", posUserSession.getStore()
					.getCmpDeptLocation().getLocation().getLocationName());
			rootMap.put("itemLists", productVOs);
			if (deliveryOptionName != null && !("").equals(deliveryOptionName)
					&& !("select").equalsIgnoreCase(deliveryOptionName.trim()))
				rootMap.put("deliveryOption", deliveryOptionName.trim());
			if (productVOs != null) {
				int quantemp = 0;
				for (ProductVO pvo : productVOs)
					quantemp += pvo.getQuantity();
				rootMap.put("totalQuantity", quantemp);
			}

			Writer out = new StringWriter();
			template.process(rootMap, out);
			posPrintReciptContent = out.toString();
			log.info("Module: Accounts : Method: createReciptPrintContent: Action Success");
			return posPrintReciptContent;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: createReciptPrintContent: throws Exception "
					+ ex);
			return posPrintReciptContent;
		}
	}

	Integer itemsPrintStart = null;
	Integer itemsAllowedPerPage = null;
	Integer totalItemsToPrint = null;

	@SuppressWarnings("unchecked")
	public String getReceiptContentListSize() {

		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));

			totalItemsToPrint = previewsMap.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getReciptPrintContent() {
		try {
			log.info("Inside Module: Accounts : Method: getReciptPrintContent");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) (session
					.getAttribute("POS_PRODUCT_PREVIEW_"
							+ sessionObj.get("jSessionId")));
			PointOfSaleVO posVo = (PointOfSaleVO) session
					.getAttribute("SALES_SUMMARY_PREVIEW_"
							+ sessionObj.get("jSessionId"));

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configuration config = new Configuration();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/pos/"
					+ pointOfSaleBL.getImplementation().getImplementationId()));
			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			String contextPath = ServletActionContext.getRequest()
					.getContextPath(); // includes leading forward slash

			String urlPath = scheme + "://" + host + contextPath;

			Template template = config.getTemplate("html-pos-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			if (pointOfSaleBL.getImplementation().getMainLogo() != null) {
				byte[] bFile = FileUtil.getBytes(new File(pointOfSaleBL
						.getImplementation().getMainLogo()));
				bFile = Base64.encodeBase64(bFile);

				rootMap.put("logo", new String(bFile));
			} else {
				rootMap.put("logo", "");
			}
			rootMap.put("logo", urlPath);
			rootMap.put("title", posUserSession.getStore().getCmpDeptLocation()
					.getCompany().getCompanyName());
			DateTime dt = new DateTime();
			rootMap.put("datetime",
					DateFormat.convertSystemDateToString(new Date()) + " "
							+ dt.toLocalTime().getHourOfDay() + ":"
							+ dt.toLocalTime().getMinuteOfHour());
			rootMap.put("salesType", InvoiceSalesType.get(posVo.getSaleType())
					.name().replaceAll("_", " "));
			rootMap.put("telephone",
					posUserSession.getStore().getCmpDeptLocation().getCompany()
							.getCompanyPhone() != null ? posUserSession
							.getStore().getCmpDeptLocation().getCompany()
							.getCompanyPhone() : "");
			rootMap.put(
					"branchContact",
					posUserSession.getStore().getCmpDeptLocation()
							.getLocation().getContactNumber() != null ? posUserSession
							.getStore().getCmpDeptLocation().getLocation()
							.getContactNumber()
							: "");
			rootMap.put("cashReceived", posVo.getCashReceived());
			rootMap.put("couponReceived", posVo.getCouponReceived());
			rootMap.put("cardReceived", posVo.getCardReceived());
			rootMap.put("chequeReceived", posVo.getChequeReceived());
			rootMap.put("exchangeReceived", posVo.getExchangeReceived());
			if (posVo.getLookupDetail() != null)
				rootMap.put("deliveryOption", posVo.getLookupDetail()
						.getDisplayName());
			else if (deliveryOptionName != null
					&& !("").equals(deliveryOptionName)
					&& !("select").equalsIgnoreCase(deliveryOptionName.trim()))
				rootMap.put("deliveryOption", deliveryOptionName.trim());

			rootMap.put("balanceDue", posVo.getBalanceDue());
			if ((double) posVo.getDeliveryCharges() > 0)
				rootMap.put("deliveryCharges", posVo.getDeliveryCharges());
			else
				rootMap.put("deliveryCharges", null);
			rootMap.put("addtionalCharges", posVo.getAdditionalCost());

			rootMap.put("address", posUserSession.getStore()
					.getCmpDeptLocation().getLocation().getAddress());
			rootMap.put("location", posUserSession.getStore()
					.getCmpDeptLocation().getLocation().getLocationName());
			if (null != posVo.getPersonByPersonId()
					&& null != posVo.getPersonByPersonId().getPersonId()) {
				rootMap.put("servedBy", (posVo.getPersonByPersonId()
						.getPrefix() != null ? posVo.getPersonByPersonId()
						.getPrefix() + " " : " ")
						+ posVo.getPersonByPersonId().getFirstName()
						+ " "
						+ posVo.getPersonByPersonId().getLastName());
			} else
				rootMap.put("servedBy", (posUserSession.getPersonByPersonId()
						.getPrefix() != null ? posUserSession
						.getPersonByPersonId().getPrefix() + " " : " ")
						+ posUserSession.getPersonByPersonId().getFirstName()
						+ " "
						+ posUserSession.getPersonByPersonId().getLastName());

			rootMap.put("referenceNumber", posVo.getReferenceNumber());
			rootMap.put("tillNumber", posUserSession.getTillNumber());
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			List<ProductVO> productVOList = new ArrayList<ProductVO>();

			for (ProductVO productVO : productVOs)
				productVOList.add(productVO);

			// Sort with item order for printing
			Collections.sort(productVOList);

			Boolean endReached = true;

			if (itemsPrintStart != null && itemsPrintStart != -1) {

				endReached = false;
				Integer itemPrintPerPage = Integer.parseInt((String) session
						.getAttribute("pos_items_per_page"));
				Integer allowedItemsInThisPrint = itemsPrintStart
						+ itemPrintPerPage;
				List<ProductVO> productVOPagedList = new ArrayList<ProductVO>();

				for (int i = itemsPrintStart; (i < productVOList.size())
						&& (i < allowedItemsInThisPrint); i++) {

					productVOPagedList.add(productVOList.get(i));

					if (allowedItemsInThisPrint >= productVOList.size())
						endReached = true;
				}

				rootMap.put("itemLists", productVOPagedList);
			} else {

				rootMap.put("itemLists", productVOList);
			}

			rootMap.put("customerName", posVo.getCustomerName());
			rootMap.put("customerNumber", posVo.getCustomerNumber());
			rootMap.put("customerMobile", posVo.getCustomerMobile());
			rootMap.put("customerLocation", posVo.getCustomerLocation());
			rootMap.put("quotation", posVo.getQuotation());

			if (endReached) {
				rootMap.put("totalDue", posVo.getTotalDue());
				rootMap.put("totalBeforeDiscount", posVo.getSalesAmount());

				if (previewsMap.values() != null) {
					int quantemp = 0;
					double totalDiscount = 0.0;
					for (ProductVO pvo : previewsMap.values()) {
						quantemp += pvo.getQuantity();
						totalDiscount += pvo.getDiscount();
						totalDiscount += pvo.getCustomerDiscount();
					}
					rootMap.put("totalQuantity", quantemp);
					if (totalDiscount > 0)
						rootMap.put("totalDiscount",
								AIOSCommons.formatAmount(totalDiscount));
					else
						rootMap.put("totalDiscount", "-N/A-");
				}
			} else {
				// put empty string in case pagination is active and last page
				// is not reached
				rootMap.put("totalDue", "");
				rootMap.put("totalBeforeDiscount", "");
				rootMap.put("totalQuantity", "");
				rootMap.put("totalDiscount", "");
			}
			Writer out = new StringWriter();
			template.process(rootMap, out);
			posPrintReciptContent = out.toString();
			// System.out.println(posPrintReciptContent);
			log.info("Module: Accounts : Method: getReciptPrintContent: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: getReciptPrintContent: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getEmployeeReciptPrintContent() {
		try {
			log.info("Inside Module: Accounts : Method: getEmployeeReciptPrintContent");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Map<String, Object> employeeMealDetail = (Map<String, Object>) (session
					.getAttribute("EMPLOYEE_MEAL_DETAIL_"
							+ sessionObj.get("jSessionId")));

			Map<String, ProductVO> previewsMap = (Map<String, ProductVO>) employeeMealDetail
					.get("POS_PRODUCT_PREVIEW");
			PointOfSaleVO posVo = (PointOfSaleVO) employeeMealDetail
					.get("SALES_SUMMARY_PREVIEW");

			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			Configuration config = new Configuration();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/pos/"
					+ pointOfSaleBL.getImplementation().getImplementationId()));
			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			String contextPath = ServletActionContext.getRequest()
					.getContextPath(); // includes leading forward slash

			String urlPath = scheme + "://" + host + contextPath;

			Template template = config.getTemplate("pos-employee-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			if (pointOfSaleBL.getImplementation().getMainLogo() != null) {
				byte[] bFile = FileUtil.getBytes(new File(pointOfSaleBL
						.getImplementation().getMainLogo()));
				bFile = Base64.encodeBase64(bFile);

				rootMap.put("logo", new String(bFile));
			} else {
				rootMap.put("logo", "");
			}
			rootMap.put("logo", urlPath);
			rootMap.put("title", posUserSession.getStore().getCmpDeptLocation()
					.getCompany().getCompanyName());
			DateTime dt = new DateTime();
			rootMap.put("datetime",
					DateFormat.convertSystemDateToString(new Date()) + " "
							+ dt.toLocalTime().getHourOfDay() + ":"
							+ dt.toLocalTime().getMinuteOfHour());
			rootMap.put("salesType", InvoiceSalesType.get(posVo.getSaleType())
					.name().replaceAll("_", " "));
			rootMap.put("telephone",
					posUserSession.getStore().getCmpDeptLocation().getCompany()
							.getCompanyPhone() != null ? posUserSession
							.getStore().getCmpDeptLocation().getCompany()
							.getCompanyPhone() : "");
			rootMap.put(
					"branchContact",
					posUserSession.getStore().getCmpDeptLocation()
							.getLocation().getContactNumber() != null ? posUserSession
							.getStore().getCmpDeptLocation().getLocation()
							.getContactNumber()
							: "");

			if (posVo.getLookupDetail() != null)
				rootMap.put("deliveryOption", posVo.getLookupDetail()
						.getDisplayName());

			rootMap.put("address", posUserSession.getStore()
					.getCmpDeptLocation().getLocation().getAddress());
			rootMap.put("location", posUserSession.getStore()
					.getCmpDeptLocation().getLocation().getLocationName());
			rootMap.put("servedBy", (posUserSession.getPersonByPersonId()
					.getPrefix() != null ? posUserSession.getPersonByPersonId()
					.getPrefix() + " " : " ")
					+ posUserSession.getPersonByPersonId().getFirstName()
					+ " "
					+ posUserSession.getPersonByPersonId().getLastName());
			rootMap.put("referenceNumber", posVo.getReferenceNumber());
			rootMap.put("tillNumber", posUserSession.getTillNumber());
			rootMap.put("totalDue", posVo.getTotalDue());
			List<ProductVO> productVOs = new ArrayList<ProductVO>(
					previewsMap.values());
			List<ProductVO> productVOList = new ArrayList<ProductVO>();
			for (ProductVO productVO : productVOs) {
				productVOList.add(productVO);
				if (null != productVO.getProductComboVOs()
						&& productVO.getProductComboVOs().size() > 0) {
					for (ProductVO tempproductVO : productVO
							.getProductComboVOs().values())
						productVOList.add(tempproductVO);
				}
			}
			rootMap.put("itemLists", productVOList);
			rootMap.put("employeeName", posVo.getEmployeeName());
			rootMap.put(
					"supervisorName",
					posUserSession
							.getPersonBySupervisor()
							.getFirstName()
							.concat(null != posVo.getPersonByEmployeeId()
									.getLastName() ? " "
									+ posVo.getPersonByEmployeeId()
											.getLastName() : ""));
			if (previewsMap.values() != null) {
				int quantemp = 0;
				for (ProductVO pvo : previewsMap.values()) {
					quantemp += pvo.getQuantity();
					if (null != pvo.getProductComboVOs()
							&& pvo.getProductComboVOs().size() > 0) {
						for (ProductVO psvo : pvo.getProductComboVOs().values()) {
							quantemp += psvo.getQuantity();
						}
					}
				}
				rootMap.put("totalQuantity", quantemp);
			}
			Writer out = new StringWriter();
			template.process(rootMap, out);
			posPrintReciptContent = out.toString();
			log.info("Module: Accounts : Method: getEmployeeReciptPrintContent: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: getEmployeeReciptPrintContent: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show EOD POS details
	public String showEodPosDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showEodPosDetails");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));

			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getEODPointOfSaleBySalesDate(posUserSession.getStore()
							.getStoreId(), salesDate);

			List<PointOfSaleVO> pointOfSaleVOs = pointOfSaleBL
					.getPointOfSaleReceiptsBySalesDate(pointOfSales);
			List<PointOfSaleVO> pointOfSaleEmployeeVOs = pointOfSaleBL
					.getPointOfSaleByEmployeeService(posUserSession.getStore()
							.getStoreId(), salesDate);
			ServletActionContext.getRequest().setAttribute("POS_RECEIPTS",
					pointOfSaleVOs);
			ServletActionContext.getRequest().setAttribute(
					"POS_EMPLOYEE_SERVICE", pointOfSaleEmployeeVOs);
			log.info("Module: Accounts : Method: showEodPosDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showEodPosDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show EOD POS details
	public String showStoreEodPosDetails() {
		try {
			log.info("Inside Module: Accounts : Method: showStoreEodPosDetails");
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getEODPointOfSaleBySalesDate(posStoreId, salesDate);
			List<PointOfSaleVO> pointOfSaleVOs = pointOfSaleBL
					.getPointOfSaleReceiptsBySalesDate(pointOfSales);
			List<PointOfSaleVO> pointOfSaleEmployeeVOs = pointOfSaleBL
					.getPointOfSaleByEmployeeService(posStoreId, salesDate);
			ServletActionContext.getRequest().setAttribute("POS_RECEIPTS",
					pointOfSaleVOs);
			ServletActionContext.getRequest().setAttribute(
					"POS_EMPLOYEE_SERVICE", pointOfSaleEmployeeVOs);
			log.info("Module: Accounts : Method: showStoreEodPosDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showStoreEodPosDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String deletePointOfSale() {
		try {
			log.info("Inside Module: Accounts : Method: deletePointOfSale");

			if (pointOfSaleId > 0) {
				pointOfSaleBL.deleteSessionPOSOrder(pointOfSaleBL
						.getPointOfSaleService().getPointOfDetailByOrderId(
								pointOfSaleId));
				returnMessage = "SUCCESS";
			}
			log.info("Module: Accounts : Method: deletePointOfSale: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: deletePointOfSale: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPOSReceiptsByReceipt() {
		try {
			log.info("Inside Module: Accounts : Method: showPOSReceiptsByReceipt");
			List<PointOfSaleReceipt> pointOfSaleReceipts = pointOfSaleBL
					.getPointOfSaleService().getNonDepositedReceipts(
							POSPaymentType.Cash.getCode(),
							POSPaymentType.Cheque.getCode(),
							POSPaymentType.Card.getCode(),
							pointOfSaleBL.getImplementation(), dateFrom,
							dateTo, storeId, paymentMode);
			List<BankReceiptsDetailVO> receiptVOs = null;
			if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
				BankReceiptsDetailVO receiptVO = null;
				receiptVOs = new ArrayList<BankReceiptsDetailVO>();
				for (PointOfSaleReceipt receipt : pointOfSaleReceipts) {
					receiptVO = new BankReceiptsDetailVO();
					if (null != receipt.getBalanceDue()
							&& (double) receipt.getBalanceDue() > 0) {
						receiptVO.setAmount(receipt.getReceipt()
								- receipt.getBalanceDue());
					} else
						receiptVO.setAmount(receipt.getReceipt());
					if ((double) receipt.getReceipt() > 0) {
						receiptVO.setBankRefNo(receipt.getTransactionNumber());
						receiptVO.setPaymentModeName(POSPaymentType.get(
								receipt.getReceiptType()).name());
						receiptVO.setReferenceNo(receipt.getPointOfSale()
								.getReferenceNumber());
						receiptVO.setUseCase(PointOfSaleReceipt.class
								.getSimpleName());
						receiptVO
								.setRecordId(receipt.getPointOfSaleReceiptId());
						receiptVOs.add(receiptVO);
					}
				}

				Collections.sort(receiptVOs,
						new Comparator<BankReceiptsDetailVO>() {
							public int compare(BankReceiptsDetailVO o1,
									BankReceiptsDetailVO o2) {
								return o1.getPaymentModeName().compareTo(
										o2.getPaymentModeName());
							}
						});
			}
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_DETAIL", receiptVOs);
			log.info("Module: Accounts : Method: showPOSReceiptsByReceipt: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showPOSReceiptsByReceipt: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPointOfSaleEOD() {
		try {
			log.info("Inside Module: Accounts : Method: showPointOfSaleEOD");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			List<Store> stores = null;
			if (null != posUserSession) {
				stores = new ArrayList<Store>();
				Store store = pointOfSaleBL
						.getStoreBL()
						.getStoreService()
						.getBasicStoreByStoreId(
								posUserSession.getStore().getStoreId());
				stores.add(store);
				ServletActionContext.getRequest().setAttribute("STORE_ID",
						store.getStoreId());
			} else {
				stores = pointOfSaleBL.getStoreBL().getStoreService()
						.getAllStores(pointOfSaleBL.getImplementation());
				ServletActionContext.getRequest().setAttribute("STORE_ID", -1);
			}
			ServletActionContext.getRequest().setAttribute("STORE_DETAIL",
					stores);
			log.info("Module: Accounts : Method: 	: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showPointOfSaleEOD: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String addAddtionalCharges() {
		try {
			log.info("Module: Accounts : Method: addAddtionalCharges Inside");
			rowId = 0;
			updatepointOfSalesAddtionalCharges(0l, rowId);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: addAddtionalCharges Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: addAddtionalCharges Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private void updatepointOfSalesAddtionalCharges(long pointOfSaleId,
			int batchCount) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleBL.getPointOfSaleService()
				.getAllPointOfSalesData(pointOfSaleId,
						pointOfSaleBL.getImplementation());
		batchCount += 1;
		if (null != pointOfSales && pointOfSales.size() > 0) {
			Collections.sort(pointOfSales, new Comparator<PointOfSale>() {
				public int compare(PointOfSale o1, PointOfSale o2) {
					return o1.getPointOfSaleId().compareTo(
							o2.getPointOfSaleId());
				}
			});
			pointOfSaleId = pointOfSales.get(pointOfSales.size() - 1)
					.getPointOfSaleId();
			pointOfSaleBL.updatepointOfSalesAddtionalCharges(pointOfSales);
			log.info(batchCount
					+ " updatepointOfSalesAddtionalCharges Batch Success");
			updatepointOfSalesAddtionalCharges(pointOfSaleId, batchCount);
		}
	}

	public String updatePointOfSalesTransaction() {
		try {
			log.info("Module: Accounts : Method: updatePointOfSalesTransaction Inside");
			rowId = 0;
			updatepointOfSalesTransaction(rowId);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updatePointOfSalesTransaction Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: updatePointOfSalesTransaction Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updatePointOfSalesTransactionBySalesDate() {
		try {
			log.info("Module: Accounts : Method: updatePointOfSalesTransactionBySalesDate Inside");
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getAllPointOfSalesByDateRange(
							pointOfSaleBL.getImplementation(),
							DateFormat.convertStringToDate(dateFrom),
							DateFormat.convertStringToDate(dateTo));
			Collections.sort(pointOfSales, new Comparator<PointOfSale>() {
				public int compare(PointOfSale o1, PointOfSale o2) {
					return o1.getPointOfSaleId().compareTo(
							o2.getPointOfSaleId());
				}
			});
			pointOfSaleBL.createPointOfSalesTransactionOnly(pointOfSales, null);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updatePointOfSalesTransactionBySalesDate Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: updatePointOfSalesTransactionBySalesDate Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private void updatepointOfSalesTransaction(int batchCount) throws Exception {
		List<PointOfSale> pointOfSales = pointOfSaleBL.getPointOfSaleService()
				.getAllPointOfSalesData(pointOfSaleId,
						pointOfSaleBL.getImplementation());
		batchCount += 1;
		if (null != pointOfSales && pointOfSales.size() > 0) {
			Collections.sort(pointOfSales, new Comparator<PointOfSale>() {
				public int compare(PointOfSale o1, PointOfSale o2) {
					return o1.getPointOfSaleId().compareTo(
							o2.getPointOfSaleId());
				}
			});
			pointOfSaleId = pointOfSales.get(pointOfSales.size() - 1)
					.getPointOfSaleId();
			pointOfSaleBL.updatePointOfSalesTransaction(pointOfSales, null);
			log.info(batchCount
					+ " updatepointOfSalesTransaction Batch Success");
			// updatepointOfSalesTransaction(pointOfSaleId, batchCount);
		}
	}

	public String deletePointOfSaleDuplicates() {
		try {
			log.info("Module: Accounts : Method: deletePointOfSaleDuplicates Inside");
			java.util.Calendar date1 = java.util.Calendar.getInstance();
			date1.set(2015, 3, 15, 0, 0, 0);
			java.util.Calendar date2 = java.util.Calendar.getInstance();
			date2.set(2015, 3, 16, 0, 0, 0);
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getAllPointOfSalesByDate(
							pointOfSaleBL.getImplementation(), date1.getTime(),
							date2.getTime());
			Map<String, List<PointOfSale>> salesMap = new HashMap<String, List<PointOfSale>>();
			List<PointOfSale> tempPointOfSaleTemp = null;
			if (null != pointOfSales && pointOfSales.size() > 0) {
				for (PointOfSale pointOfSale : pointOfSales) {
					tempPointOfSaleTemp = new ArrayList<PointOfSale>();
					if (salesMap.containsKey(pointOfSale.getReferenceNumber()))
						tempPointOfSaleTemp.addAll(salesMap.get(pointOfSale
								.getReferenceNumber()));
					tempPointOfSaleTemp.add(pointOfSale);
					salesMap.put(pointOfSale.getReferenceNumber(),
							tempPointOfSaleTemp);
				}
			}
			List<PointOfSale> deletedPointOfSales = new ArrayList<PointOfSale>();
			for (Entry<String, List<PointOfSale>> entry : salesMap.entrySet()) {
				tempPointOfSaleTemp = new ArrayList<PointOfSale>();
				tempPointOfSaleTemp.addAll(entry.getValue());
				if (tempPointOfSaleTemp.size() > 1) {
					int counter = 0;
					for (PointOfSale pointOfSale : tempPointOfSaleTemp) {
						if (counter == 0)
							counter += 1;
						else {
							deletedPointOfSales.add(pointOfSale);
							counter += 1;
						}
					}
				}
			}
			pointOfSaleBL.deletePointOfSale(deletedPointOfSales);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deletePointOfSaleDuplicates Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: deletePointOfSaleDuplicates Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String auditPointOfSaleTransaction() {
		try {
			log.info("Module: Accounts : Method: auditPointOfSaleTransaction Inside");
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getAllPointOfSalesByDateRange(
							pointOfSaleBL.getImplementation(),
							DateFormat.convertStringToDate(dateFrom),
							DateFormat.convertStringToDate(dateTo));
			Map<Long, Double> salesMap = new HashMap<Long, Double>();
			double salesAmount = 0;
			for (PointOfSale pointOfSale : pointOfSales) {
				for (PointOfSaleReceipt receipt : pointOfSale
						.getPointOfSaleReceipts()) {
					salesAmount = 0;
					if (salesMap.containsKey(pointOfSale.getPointOfSaleId()))
						salesAmount += salesMap.get(pointOfSale
								.getPointOfSaleId());
					salesAmount += Math.abs(receipt.getReceipt()
							- (null != receipt.getBalanceDue() ? receipt
									.getBalanceDue() : 0));
					salesMap.put(pointOfSale.getPointOfSaleId(), salesAmount);
				}
			}
			List<Transaction> transactions = pointOfSaleBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailByIds(
							salesMap.keySet(),
							PointOfSale.class.getSimpleName(),
							pointOfSaleBL.getImplementation()
									.getRevenueAccount(),
							pointOfSaleBL.getImplementation()
									.getSalesDiscount());
			PointOfSaleAuditVO pointOfSaleAuditVO = null;
			previewObjects = new ArrayList<Object>();
			for (Transaction transaction : transactions) {
				if (salesMap.containsKey(transaction.getRecordId())) {
					salesAmount = 0;
					for (TransactionDetail transactionDT : transaction
							.getTransactionDetails()) {
						if ((long) transactionDT.getCombination()
								.getCombinationId() == (long) pointOfSaleBL
								.getImplementation().getRevenueAccount())
							salesAmount += transactionDT.getAmount();
						else if ((long) transactionDT.getCombination()
								.getCombinationId() == (long) pointOfSaleBL
								.getImplementation().getSalesDiscount())
							salesAmount -= transactionDT.getAmount();
					}
					if (salesAmount != (double) salesMap.get(transaction
							.getRecordId())) {
						pointOfSaleAuditVO = new PointOfSaleAuditVO();
						pointOfSaleAuditVO.setRecordId(transaction
								.getRecordId());
						pointOfSaleAuditVO.setSalesAmount(salesMap
								.get(transaction.getRecordId()));
						pointOfSaleAuditVO.setTransactionAmount(salesAmount);
						pointOfSaleAuditVO.setTransactionId(transaction
								.getTransactionId());
						pointOfSaleAuditVO.setReason("Amount Differs.");
						previewObjects.add(pointOfSaleAuditVO);
					}
				} else {
					pointOfSaleAuditVO = new PointOfSaleAuditVO();
					pointOfSaleAuditVO.setRecordId(transaction.getRecordId());
					pointOfSaleAuditVO.setReason("Transaction not found.");
					previewObjects.add(pointOfSaleAuditVO);
				}
			}
			log.info("Module: Accounts : Method: auditPointOfSaleTransaction Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: auditPointOfSaleTransaction Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String auditPointOfSaleReceipts() {
		try {
			log.info("Module: Accounts : Method: auditPointOfSaleReceipts Inside");
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getAllPointOfSalesByDateRange(
							pointOfSaleBL.getImplementation(),
							DateFormat.convertStringToDate(dateFrom),
							DateFormat.convertStringToDate(dateTo));
			double salesAmount = 0;
			double unitAmount = 0;
			PointOfSaleAuditVO pointOfSaleAuditVO = null;
			previewObjects = new ArrayList<Object>();
			for (PointOfSale pointOfSale : pointOfSales) {
				for (PointOfSaleReceipt receipt : pointOfSale
						.getPointOfSaleReceipts()) {
					salesAmount = 0;
					unitAmount = 0;
					salesAmount += Math.abs(receipt.getReceipt()
							- (null != receipt.getBalanceDue() ? receipt
									.getBalanceDue() : 0));
				}
				for (PointOfSaleDetail saleDetail : pointOfSale
						.getPointOfSaleDetails())
					unitAmount += (saleDetail.getQuantity() * saleDetail
							.getUnitRate());
				for (PointOfSaleCharge saleCharges : pointOfSale
						.getPointOfSaleCharges())
					unitAmount += saleCharges.getCharges();
				if (null != pointOfSale.getTotalDiscount()
						&& pointOfSale.getTotalDiscount() > 0)
					unitAmount -= pointOfSale.getTotalDiscount();
				if (salesAmount != unitAmount) {
					pointOfSaleAuditVO = new PointOfSaleAuditVO();
					pointOfSaleAuditVO.setRecordId(pointOfSale
							.getPointOfSaleId());
					pointOfSaleAuditVO.setSalesAmount(salesAmount);
					pointOfSaleAuditVO.setTransactionAmount(unitAmount);
					pointOfSaleAuditVO.setReason("Amount Differs.");
					previewObjects.add(pointOfSaleAuditVO);
				}
			}
			log.info("Module: Accounts : Method: auditPointOfSaleReceipts Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: auditPointOfSaleReceipts Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteDuplicatePointOfSales() {
		try {
			log.info("Module: Accounts : Method: deleteDuplicatePointOfSales Inside");
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getAllPointOfSalesByDateRange(
							pointOfSaleBL.getImplementation(),
							DateFormat.convertStringToDate(dateFrom),
							DateFormat.convertStringToDate(dateTo));
			Map<String, Map<Long, PointOfSale>> salesMap = new HashMap<String, Map<Long, PointOfSale>>();
			Map<Long, PointOfSale> tempSalesMap = null;
			for (PointOfSale pointOfSale : pointOfSales) {
				tempSalesMap = new HashMap<Long, PointOfSale>();
				if (salesMap.containsKey(pointOfSale.getReferenceNumber()))
					tempSalesMap.putAll(salesMap.get(pointOfSale
							.getReferenceNumber()));
				tempSalesMap.put(pointOfSale.getPointOfSaleId(), pointOfSale);
				salesMap.put(pointOfSale.getReferenceNumber(), tempSalesMap);
			}
			PointOfSale pointOfSale = null;
			PointOfSale pointOfSaletmp = null;
			String detailMapKey = "";
			String detailMapKeytmp = "";
			Map<Long, PointOfSale> deleteSalesMap = new HashMap<Long, PointOfSale>();
			List<PointOfSaleDetail> pointOfSaleDetails = null;
			List<PointOfSaleDetail> pointOfSaleDetailstmp = null;
			for (Entry<String, Map<Long, PointOfSale>> salesEntry : salesMap
					.entrySet()) {
				tempSalesMap = new HashMap<Long, PointOfSale>(
						salesEntry.getValue());
				if (tempSalesMap.size() > 1) {
					for (Entry<Long, PointOfSale> entry : tempSalesMap
							.entrySet()) {
						if (!deleteSalesMap.containsKey(entry.getKey())) {
							pointOfSale = new PointOfSale();
							BeanUtils.copyProperties(pointOfSale,
									entry.getValue());
							if (pointOfSale.getReferenceNumber().equals(
									"16-DE-2-1259"))
								System.out.println("for known duplicate.");
							detailMapKey = "";
							pointOfSaleDetails = new ArrayList<PointOfSaleDetail>(
									pointOfSale.getPointOfSaleDetails());
							Collections.sort(pointOfSaleDetails,
									new Comparator<PointOfSaleDetail>() {
										public int compare(
												PointOfSaleDetail o1,
												PointOfSaleDetail o2) {
											return o1
													.getProductByProductId()
													.getProductId()
													.compareTo(
															o2.getProductByProductId()
																	.getProductId());
										}
									});
							for (PointOfSaleDetail saleDetail : pointOfSaleDetails)
								detailMapKey += saleDetail
										.getProductByProductId().getProductId()
										+ "@";
							detailMapKey = detailMapKey.substring(0,
									(detailMapKey.length() - 1));
							for (Entry<Long, PointOfSale> tmpentry : tempSalesMap
									.entrySet()) {
								pointOfSaletmp = new PointOfSale();
								detailMapKeytmp = "";
								BeanUtils.copyProperties(pointOfSaletmp,
										tmpentry.getValue());
								if ((long) pointOfSale.getPointOfSaleId() != (long) pointOfSaletmp
										.getPointOfSaleId()) {
									pointOfSaleDetailstmp = new ArrayList<PointOfSaleDetail>(
											pointOfSaletmp
													.getPointOfSaleDetails());
									Collections
											.sort(pointOfSaleDetailstmp,
													new Comparator<PointOfSaleDetail>() {
														public int compare(
																PointOfSaleDetail o1,
																PointOfSaleDetail o2) {
															return o1
																	.getProductByProductId()
																	.getProductId()
																	.compareTo(
																			o2.getProductByProductId()
																					.getProductId());
														}
													});
									for (PointOfSaleDetail saleDetailtmp : pointOfSaleDetailstmp)
										detailMapKeytmp += saleDetailtmp
												.getProductByProductId()
												.getProductId()
												+ "@";
									detailMapKeytmp = detailMapKeytmp
											.substring(
													0,
													(detailMapKeytmp.length() - 1));
									if (detailMapKey.equals(detailMapKeytmp))
										deleteSalesMap.put(tmpentry.getKey(),
												tmpentry.getValue());
								}
							}
						}
					}
				}
			}
			pointOfSaleBL.deletePointOfSale(new ArrayList<PointOfSale>(
					deleteSalesMap.values()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteDuplicatePointOfSales Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteDuplicatePointOfSales Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String updatePointOfSaleReference() {
		try {
			log.info("Module: Accounts : Method: updatePointOfSaleReference Inside");
			List<PointOfSale> pointOfSales = pointOfSaleBL
					.getPointOfSaleService().getAllPointOfSalesByDateRange(
							pointOfSaleBL.getImplementation(),
							DateFormat.convertStringToDate(dateFrom),
							DateFormat.convertStringToDate(dateTo));
			Map<String, List<PointOfSale>> salesMap = new HashMap<String, List<PointOfSale>>();
			List<PointOfSale> tempPointOfSaleTemp = null;
			if (null != pointOfSales && pointOfSales.size() > 0) {
				for (PointOfSale pointOfSale : pointOfSales) {
					tempPointOfSaleTemp = new ArrayList<PointOfSale>();
					if (salesMap.containsKey(pointOfSale.getReferenceNumber()))
						tempPointOfSaleTemp.addAll(salesMap.get(pointOfSale
								.getReferenceNumber()));
					tempPointOfSaleTemp.add(pointOfSale);
					salesMap.put(pointOfSale.getReferenceNumber(),
							tempPointOfSaleTemp);

				}
			}
			List<PointOfSale> updatePointOfSales = new ArrayList<PointOfSale>();
			for (Entry<String, List<PointOfSale>> entry : salesMap.entrySet()) {
				tempPointOfSaleTemp = new ArrayList<PointOfSale>();
				tempPointOfSaleTemp.addAll(entry.getValue());
				if (tempPointOfSaleTemp.size() > 1) {
					String value = "A";
					int counter = 0;
					for (PointOfSale pointOfSale : tempPointOfSaleTemp) {
						if (counter == 0)
							counter += 1;
						else {
							pointOfSale.setReferenceNumber(pointOfSale
									.getReferenceNumber().concat("-" + value));
							updatePointOfSales.add(pointOfSale);
							int charValue = value.charAt(0);
							value = String.valueOf((char) (charValue + 1));
							counter += 1;
						}
					}
				}
			}
			pointOfSaleBL.getPointOfSaleService().savePointOfSale(
					updatePointOfSales);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updatePointOfSaleReference Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: updatePointOfSaleReference Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public String getProductPreview() {
		return productPreview;
	}

	public void setProductPreview(String productPreview) {
		this.productPreview = productPreview;
	}

	@JSON(name = "previewObjects")
	public List<Object> getPreviewObjects() {
		return previewObjects;
	}

	public void setPreviewObjects(List<Object> previewObjects) {
		this.previewObjects = previewObjects;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}

	public String getSalesRefNumber() {
		return salesRefNumber;
	}

	public void setSalesRefNumber(String salesRefNumber) {
		this.salesRefNumber = salesRefNumber;
	}

	public byte getDiscountMode() {
		return discountMode;
	}

	public void setDiscountMode(byte discountMode) {
		this.discountMode = discountMode;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getProductUnitPrice() {
		return productUnitPrice;
	}

	public void setProductUnitPrice(double productUnitPrice) {
		this.productUnitPrice = productUnitPrice;
	}

	public String getProductTotalPrice() {
		return productTotalPrice;
	}

	public void setProductTotalPrice(String productTotalPrice) {
		this.productTotalPrice = productTotalPrice;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getAdditionalCharges() {
		return additionalCharges;
	}

	public void setAdditionalCharges(String additionalCharges) {
		this.additionalCharges = additionalCharges;
	}

	public PointOfSaleVO getPointOfSaleVO() {
		return pointOfSaleVO;
	}

	public void setPointOfSaleVO(PointOfSaleVO pointOfSaleVO) {
		this.pointOfSaleVO = pointOfSaleVO;
	}

	public byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public double getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public int getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(int couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getCustomerDiscount() {
		return customerDiscount;
	}

	public void setCustomerDiscount(String customerDiscount) {
		this.customerDiscount = customerDiscount;
	}

	public String getTotalDues() {
		return totalDues;
	}

	public void setTotalDues(String totalDues) {
		this.totalDues = totalDues;
	}

	public int getCustomerCoupon() {
		return customerCoupon;
	}

	public void setCustomerCoupon(int customerCoupon) {
		this.customerCoupon = customerCoupon;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getPointOfSaleId() {
		return pointOfSaleId;
	}

	public void setPointOfSaleId(long pointOfSaleId) {
		this.pointOfSaleId = pointOfSaleId;
	}

	public byte getSalesType() {
		return salesType;
	}

	public void setSalesType(byte salesType) {
		this.salesType = salesType;
	}

	public String getExchangeReference() {
		return exchangeReference;
	}

	public void setExchangeReference(String exchangeReference) {
		this.exchangeReference = exchangeReference;
	}

	public String getPosPrintReciptContent() {
		return posPrintReciptContent;
	}

	public void setPosPrintReciptContent(String posPrintReciptContent) {
		this.posPrintReciptContent = posPrintReciptContent;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public String getStoreDetails() {
		return storeDetails;
	}

	public void setStoreDetails(String storeDetails) {
		this.storeDetails = storeDetails;
	}

	public Long getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(Long deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getPosSessionId() {
		return posSessionId;
	}

	public void setPosSessionId(String posSessionId) {
		this.posSessionId = posSessionId;
	}

	public String getDeliveryOptionName() {
		return deliveryOptionName;
	}

	public void setDeliveryOptionName(String deliveryOptionName) {
		this.deliveryOptionName = deliveryOptionName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getPointOfSaleDetailId() {
		return pointOfSaleDetailId;
	}

	public void setPointOfSaleDetailId(long pointOfSaleDetailId) {
		this.pointOfSaleDetailId = pointOfSaleDetailId;
	}

	public long getPointOfSaleTempId() {
		return pointOfSaleTempId;
	}

	public void setPointOfSaleTempId(long pointOfSaleTempId) {
		this.pointOfSaleTempId = pointOfSaleTempId;
	}

	public long getPointOfSalePrintId() {
		return pointOfSalePrintId;
	}

	public void setPointOfSalePrintId(long pointOfSalePrintId) {
		this.pointOfSalePrintId = pointOfSalePrintId;
	}

	public boolean isFreeDelivery() {
		return freeDelivery;
	}

	public void setFreeDelivery(boolean freeDelivery) {
		this.freeDelivery = freeDelivery;
	}

	public String getDriverMobile() {
		return driverMobile;
	}

	public void setDriverMobile(String driverMobile) {
		this.driverMobile = driverMobile;
	}

	public long getDriverId() {
		return driverId;
	}

	public void setDriverId(long driverId) {
		this.driverId = driverId;
	}

	public double getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(double deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	public long getSpecialProductId() {
		return specialProductId;
	}

	public void setSpecialProductId(long specialProductId) {
		this.specialProductId = specialProductId;
	}

	public int getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(int itemOrder) {
		this.itemOrder = itemOrder;
	}

	public boolean isComboType() {
		return comboType;
	}

	public void setComboType(boolean comboType) {
		this.comboType = comboType;
	}

	public String getPrinters() {
		return printers;
	}

	public void setPrinters(String printers) {
		this.printers = printers;
	}

	public String getKitchenOrders() {
		return kitchenOrders;
	}

	public void setKitchenOrders(String kitchenOrders) {
		this.kitchenOrders = kitchenOrders;
	}

	public String getBarOrders() {
		return barOrders;
	}

	public void setBarOrders(String barOrders) {
		this.barOrders = barOrders;
	}

	public int getComboItemOrder() {
		return comboItemOrder;
	}

	public void setComboItemOrder(int comboItemOrder) {
		this.comboItemOrder = comboItemOrder;
	}

	public String getAdditionalCost() {
		return additionalCost;
	}

	public void setAdditionalCost(String additionalCost) {
		this.additionalCost = additionalCost;
	}

	public double getDispatchDiscount() {
		return dispatchDiscount;
	}

	public void setDispatchDiscount(double dispatchDiscount) {
		this.dispatchDiscount = dispatchDiscount;
	}

	public double getDispatchCost() {
		return dispatchCost;
	}

	public void setDispatchCost(double dispatchCost) {
		this.dispatchCost = dispatchCost;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public boolean isPrinterFlag() {
		return printerFlag;
	}

	public void setPrinterFlag(boolean printerFlag) {
		this.printerFlag = printerFlag;
	}

	public byte[] getReceiptData() {
		return receiptData;
	}

	public void setReceiptData(byte[] receiptData) {
		this.receiptData = receiptData;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerLocation() {
		return customerLocation;
	}

	public void setCustomerLocation(String customerLocation) {
		this.customerLocation = customerLocation;
	}

	public int getPosStoreId() {
		return posStoreId;
	}

	public void setPosStoreId(int posStoreId) {
		this.posStoreId = posStoreId;
	}

	public double getReceiveCashAmount() {
		return receiveCashAmount;
	}

	public void setReceiveCashAmount(double receiveCashAmount) {
		this.receiveCashAmount = receiveCashAmount;
	}

	public Integer getItemsPrintStart() {
		return itemsPrintStart;
	}

	public void setItemsPrintStart(Integer itemsPrintStart) {
		this.itemsPrintStart = itemsPrintStart;
	}

	public Integer getItemsAllowedPerPage() {
		return itemsAllowedPerPage;
	}

	public void setItemsAllowedPerPage(Integer itemsAllowedPerPage) {
		this.itemsAllowedPerPage = itemsAllowedPerPage;
	}

	@JSON(name = "totalItemsToPrint")
	public Integer getTotalItemsToPrint() {
		return totalItemsToPrint;
	}

	public void setTotalItemsToPrint(Integer totalItemsToPrint) {
		this.totalItemsToPrint = totalItemsToPrint;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public boolean isShowPinNumber() {
		return showPinNumber;
	}

	public void setShowPinNumber(boolean showPinNumber) {
		this.showPinNumber = showPinNumber;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
}
