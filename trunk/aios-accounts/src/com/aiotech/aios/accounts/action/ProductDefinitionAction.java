package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.ProductDefinition;
import com.aiotech.aios.accounts.domain.entity.vo.ProductDefinitionVO;
import com.aiotech.aios.accounts.service.bl.ProductDefinitionBL;
import com.aiotech.aios.common.to.Constants.Accounts.ProductPricingDetail;
import com.opensymphony.xwork2.ActionSupport;

public class ProductDefinitionAction extends ActionSupport {

	private static final long serialVersionUID = -8008873267072750529L;
	private static final Logger log = Logger
			.getLogger(ProductDefinitionAction.class);

	private ProductDefinitionBL productDefinitionBL;

	private long productDefinitionId;
	private String definitionLabel;
	private long productDefinitionParentId;
	private long productId;
	private long specialProductId;
	private int itemOrder;
	private boolean defaultPricing;
	private String productName;
	private String returnMessage;
	private List<Object> aaData;

	// return success
	public String returnSuccess() {
		return SUCCESS;
	}

	// show all definitions
	public String showAllProductDefinitionsList() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductDefinitionsList");
			aaData = productDefinitionBL.showAllProductDefinitionsList();
			log.info("Module: Accounts : Method: showAllProductDefinitionsList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductDefinitionsList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show all definitions
	public String showProductDefinitionParents() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDefinitionParents");
			aaData = productDefinitionBL
					.showProductDefinitionParents(productId);
			log.info("Module: Accounts : Method: showProductDefinitionParents: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDefinitionParents Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show product definition sub category
	public String showProductDefinitionSubCategory() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDefinitionSubCategory");
			aaData = productDefinitionBL
					.showProductDefinitionSubCategory(productId);
			log.info("Module: Accounts : Method: showProductDefinitionSubCategory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDefinitionSubCategory Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showSimpleDefinitionProduct() {
		try {
			log.info("Inside Module: Accounts : Method: showSimpleDefinitionProduct");
			ProductDefinition productDefintion = productDefinitionBL
					.getProductDefinitionService()
					.getProductDefinitionByDefinitionId(productDefinitionId);
			List<ProductDefinition> productDefinitions = productDefinitionBL
					.getProductDefinitionService().getProductDefinitionByGroup(
							productDefintion.getProductByProductId()
									.getProductId(),
							productDefintion.getDefinitionLabel());
			List<Product> products = new ArrayList<Product>();
			for (ProductDefinition productDefinition : productDefinitions)
				products.add(productDefinition.getProductBySpecialProductId());
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAILS",
					products);
			log.info("Module: Accounts : Method: showSimpleDefinitionProduct: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showSimpleDefinitionProduct Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Definition Product sub category
	public String showDefinitionProductSubCategory() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDefinitionSubCategory");
			ProductDefinition productDefintion = productDefinitionBL
					.getProductDefinitionService()
					.getProductDefinitionByDefinitionId(productDefinitionId);
			List<ProductDefinition> productDefinitionViews = null;
			List<ProductDefinition> productDefinitions = null;
			if (null != productDefintion) {
				if (null != productDefintion.getProductBySpecialProductId()) {
					if (null != productDefintion.getProductDefinition()) {
						productDefinitions = productDefinitionBL
								.getProductDefinitionService()
								.getProductDefinitionByGroupByDefinition(
										productDefintion
												.getProductByProductId()
												.getProductId(),
										productDefintion.getDefinitionLabel(),
										productDefintion.getProductDefinition()
												.getProductDefinitionId());
						productDefinitionViews = productDefinitionBL
								.getProductDefinitionService()
								.getChildProductDefinitions(
										productDefintion.getProductDefinition()
												.getProductDefinitionId());

					} else {
						productDefinitions = productDefinitionBL
								.getProductDefinitionService()
								.getProductDefinitionByGroup(
										productDefintion
												.getProductByProductId()
												.getProductId(),
										productDefintion.getDefinitionLabel());
						productDefinitionViews = productDefinitionBL
								.getProductDefinitionService()
								.getParentProductDefinitionByGroup(
										productDefinitionBL.getImplementation(),
										productDefintion
												.getProductByProductId()
												.getProductId());
						if (null != productDefintion.getProductByProductId()
								.getProductCategory()) {
							// Get SubCategory
							ProductCategory productCategory = productDefinitionBL
									.getProductCategoryBL()
									.getProductSubCategoriesByProduct(productId);
							ServletActionContext.getRequest().setAttribute(
									"PRODUCT_SUBCATEGORY", productCategory);
						}
					}
					if (null != productDefinitions
							&& productDefinitions.size() > 0) {
						List<Product> products = new ArrayList<Product>();
						for (ProductDefinition productDefinition : productDefinitions)
							products.add(productDefinition
									.getProductBySpecialProductId());
						ServletActionContext.getRequest().setAttribute(
								"PRODUCT_DETAILS", products);
					}
				} else {
					if (null != productDefintion.getProductDefinitions())
						productDefinitionViews = new ArrayList<ProductDefinition>(
								productDefintion.getProductDefinitions());
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_DEFINITIONS", productDefinitionViews);
			log.info("Module: Accounts : Method: showDefinitionProductSubCategory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showDefinitionProductSubCategory Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all definition labels
	public String showAllDefinitionLabels() {
		try {
			log.info("Inside Module: Accounts : Method: showAllDefinitionLabels");
			aaData = productDefinitionBL.showAllDefinitionLabels(productId);
			log.info("Module: Accounts : Method: showAllDefinitionLabels: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllDefinitionLabels Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String filterProductDefinition() {
		try {
			log.info("Inside Module: Accounts : Method: filterProductDefinition");
			List<ProductDefinition> productDefinitions = productDefinitionBL
					.getProductDefinitionService()
					.getParentProductDefinitionByGroup(
							productDefinitionBL.getImplementation(), productId);
			// Get SubCategory
			ProductCategory productCategory = productDefinitionBL
					.getProductCategoryBL().getProductSubCategoriesByProduct(
							productId);
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_SUBCATEGORY", productCategory);
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_DEFINITIONS", productDefinitions);
			log.info("Module: Accounts : Method: showProductDefinitionParents: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: filterProductDefinition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Definition Entry Screen
	public String showProductDefinitionEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDefinitionEntry");
			List<Product> products = productDefinitionBL.getProductBL()
					.getAllInventorySpecialProducts();
			ServletActionContext.getRequest().setAttribute("SPECIAL_PRODUCTS",
					products);
			log.info("Module: Accounts : Method: showProductDefinitionEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDefinitionEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductDefinitionTree() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDefinitionTree");
			List<ProductDefinitionVO> productDefinitionVOs = productDefinitionBL
					.getProductDefinitionTree(productId);
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_DEFINITION_LIST", productDefinitionVOs);
			log.info("Module: Accounts : Method: showProductDefinitionTree: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDefinitionTree Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductDefinitionSimpleTree() {
		try {
			log.info("Inside Module: Accounts : Method: showProductDefinitionTree");
			List<ProductDefinitionVO> productDefinitionVOs = productDefinitionBL
					.getProductDefinitionSimpleTree(productId);
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_DEFINITIONS", productDefinitionVOs);
			log.info("Module: Accounts : Method: showProductDefinitionTree: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDefinitionTree Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Product Definition
	public String saveProductDefinition() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductDefinition");
			Product product = new Product();
			product.setProductId(productId);
			JSONObject jsonObject = new JSONObject();
			for (Object object : aaData)
				jsonObject.put("specialProducts", object);
			List<ProductDefinition> productDefinitions = new ArrayList<ProductDefinition>();

			String specialTempProduct = jsonObject.getString("specialProducts");
			JSONObject productObject = new JSONObject(specialTempProduct);
			JSONArray recordArray = productObject.getJSONArray("record");
			ProductDefinition specialDefinition = null;
			Product specialProduct = null;
			for (int i = 0; i < recordArray.length(); i++) {
				JSONObject jsonobj = recordArray.getJSONObject(i);
				specialDefinition = new ProductDefinition();
				specialProduct = new Product();
				specialProduct.setProductId(jsonobj.getLong("recordId"));
				specialDefinition.setProductBySpecialProductId(specialProduct);
				productDefinitions.add(specialDefinition);
			}
			ProductDefinition tempDefinition = null;
			for (ProductDefinition definition : productDefinitions) {
				definition.setProductByProductId(product);
				definition.setDefinitionLabel(definitionLabel.trim());
				definition.setImplementation(productDefinitionBL
						.getImplementation());
				definition.setPricingDetail(ProductPricingDetail.Combo
						.getCode());
				tempDefinition = productDefinitionBL
						.getProductDefinitionService()
						.getProductDefinitionByProductAndLabel(
								definition.getProductByProductId()
										.getProductId(),
								definition.getDefinitionLabel().trim());
				if (null != tempDefinition
						&& null != tempDefinition.getProductDefinitionId()) {
					definition.setProductDefinitionId(tempDefinition
							.getProductDefinitionId());
					definition.setPricingDetail(tempDefinition
							.getPricingDetail());
				}

				definition.setStatus(true);
			}
			productDefinitionBL.saveProductDefinition(productDefinitions);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductDefinition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveProductDefinition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete product definition
	public String deleteProductDefinition() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProductDefinition");
			ProductDefinition productDefinition = productDefinitionBL
					.getProductDefinitionService()
					.getProductDefinitionByDefinitionId(productDefinitionId);
			productDefinitionBL.deleteProductDefinition(productDefinition);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProductDefinition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteProductDefinition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Update product definition
	public String updateProductDefinition() {
		try {
			log.info("Inside Module: Accounts : Method: updateProductDefinition");
			ProductDefinition productDefinition = productDefinitionBL
					.getProductDefinitionService()
					.getProductDefinitionByDefinitionId(productDefinitionId);
			productDefinition
					.setPricingDetail(defaultPricing ? ProductPricingDetail.Default
							.getCode() : ProductPricingDetail.Combo.getCode());
			productDefinitionBL.saveProductDefinition(productDefinition);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: updateProductDefinition: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: updateProductDefinition Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public ProductDefinitionBL getProductDefinitionBL() {
		return productDefinitionBL;
	}

	public void setProductDefinitionBL(ProductDefinitionBL productDefinitionBL) {
		this.productDefinitionBL = productDefinitionBL;
	}

	public long getProductDefinitionId() {
		return productDefinitionId;
	}

	public void setProductDefinitionId(long productDefinitionId) {
		this.productDefinitionId = productDefinitionId;
	}

	public String getDefinitionLabel() {
		return definitionLabel;
	}

	public void setDefinitionLabel(String definitionLabel) {
		this.definitionLabel = definitionLabel;
	}

	public long getProductDefinitionParentId() {
		return productDefinitionParentId;
	}

	public void setProductDefinitionParentId(long productDefinitionParentId) {
		this.productDefinitionParentId = productDefinitionParentId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getSpecialProductId() {
		return specialProductId;
	}

	public void setSpecialProductId(long specialProductId) {
		this.specialProductId = specialProductId;
	}

	public int getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(int itemOrder) {
		this.itemOrder = itemOrder;
	}

	public boolean isDefaultPricing() {
		return defaultPricing;
	}

	public void setDefaultPricing(boolean defaultPricing) {
		this.defaultPricing = defaultPricing;
	}
}
