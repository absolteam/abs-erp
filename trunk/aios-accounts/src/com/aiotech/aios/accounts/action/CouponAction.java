package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.service.bl.CouponBL;
import com.aiotech.aios.common.to.Constants.Accounts.CouponType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class CouponAction extends ActionSupport {

	private static final long serialVersionUID = -6497244604133892743L;

	private static final Logger log = Logger.getLogger(CouponAction.class);

	// Dependency
	private CouponBL couponBL;

	// Variables
	private long couponId;
	private String couponName;
	private byte couponType;
	private int startDigit;
	private int endDigit;
	private String fromDate;
	private String toDate;
	private String status;
	private String description;
	private String returnMessage;
	private List<Object> aaData;
	private Object couponVO;
	private int rowId;
	private Long recordId;

	// return success
	public String returnSuccess() {
		try {
			log.info("Inside Module: Accounts : Method: returnSuccess");
			Map<Byte, String> couponTypes = new HashMap<Byte, String>();
			for (CouponType e : EnumSet.allOf(CouponType.class)) {
				couponTypes.put(e.getCode(), e.name().replaceAll("_", " "));
			}
			ServletActionContext.getRequest().setAttribute("COUPON_TYPES",
					couponTypes);
			CommentVO comment = new CommentVO();
			if (recordId != null && recordId > 0 && couponId > 0) {
				Coupon coupon = couponBL.getCouponService()
						.getCouponByCouponId(couponId);
				comment.setRecordId(coupon.getCouponId());
				comment.setUseCase(Coupon.class.getName());
				comment = couponBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}else{
				couponId = 0l;
			}
			recordId = null;
			log.info("Module: Accounts : Method: returnSuccess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: returnSuccess Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Active Coupons
	public String showAllPopupCoupon() {
		try {
			log.info("Inside Module: Accounts : Method: showAllPopupCoupon");
			aaData = couponBL.showAllPopupCoupon();
			recordId = null;
			ServletActionContext.getRequest().setAttribute("COUPON_INFO",
					aaData);
			log.info("Module: Accounts : Method: showAllPopupCoupon: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllPopupCoupon Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all commission rule json data
	public String showAllCoupons() {
		try {
			log.info("Inside Module: Accounts : Method: showAllCoupons");
			aaData = couponBL.showAllCoupons();
			log.info("Module: Accounts : Method: showAllCoupons: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllCoupons Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCouponApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCouponApproval");
			couponId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showCouponApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCouponApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showCouponReject() {
		try {
			LOG.info("Inside Module: Accounts : Method: showCouponReject");
			couponId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showCouponReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showCouponReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Coupon entry screen
	public String showCouponEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showCouponEntry");
			Coupon coupon = couponBL.getCouponService().getCouponByCouponId(
					couponId);
			couponVO = couponBL.addCoupontVO(coupon);
			log.info("Module: Accounts : Method: showCouponEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCouponEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Coupon
	public String saveCoupon() {
		try {
			log.info("Inside Module: Accounts : Method: saveCoupon");
			Coupon coupon = new Coupon();
			coupon.setCouponId(couponId > 0 ? couponId : null);
			coupon.setCouponName(couponName);
			coupon.setCouponType(couponType);
			coupon.setFromDate(DateFormat.convertStringToDate(fromDate));
			coupon.setToDate(null != toDate && !("").equals(toDate) ? DateFormat
					.convertStringToDate(toDate) : null);
			coupon.setStartDigit(startDigit);
			coupon.setEndDigit(endDigit);
			coupon.setStatus(status.equalsIgnoreCase("true") ? true : false);
			coupon.setDescription(description);
			couponBL.saveCoupon(coupon);
			recordId = null;
			couponId = 0l;
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveCoupon: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveCoupon Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Coupon
	public String deleteCoupon() {
		try {
			log.info("Inside Module: Accounts : Method: deleteCoupon");
			Coupon coupon = couponBL.getCouponService().getCouponByCouponId(
					couponId);
			couponBL.deleteCoupon(coupon);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteCoupon: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteCoupon Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public CouponBL getCouponBL() {
		return couponBL;
	}

	public void setCouponBL(CouponBL couponBL) {
		this.couponBL = couponBL;
	}

	public long getCouponId() {
		return couponId;
	}

	public void setCouponId(long couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public byte getCouponType() {
		return couponType;
	}

	public void setCouponType(byte couponType) {
		this.couponType = couponType;
	}

	public int getStartDigit() {
		return startDigit;
	}

	public void setStartDigit(int startDigit) {
		this.startDigit = startDigit;
	}

	public int getEndDigit() {
		return endDigit;
	}

	public void setEndDigit(int endDigit) {
		this.endDigit = endDigit;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String isStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Object getCouponVO() {
		return couponVO;
	}

	public void setCouponVO(Object couponVO) {
		this.couponVO = couponVO;
	}

	public String getStatus() {
		return status;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
