package com.aiotech.aios.accounts.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts2.ServletActionContext;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.vo.QuotationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.QuotationVO;
import com.aiotech.aios.accounts.service.bl.QuotationBL;
import com.aiotech.aios.accounts.to.QuotationTO;
import com.aiotech.aios.accounts.to.SupplierTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionProcessStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class QuotationAction extends ActionSupport {

	private static final long serialVersionUID = -9139398396430256832L;

	private QuotationBL quotationBL;
	private Long quotationId;
	private Long currencyId;
	private Long tenderId;
	private Long requisitionId;
	private String quotationNumber;
	private String quotationDate;
	private String expiryDate;
	private Long supplierId;
	private Long supplierSiteId;
	private String description;
	private String returnMessage;
	private Integer rowId = 0;
	private String quotationStringDetail;
	private InputStream fileInputStream;
	private Long recordId;
	private long termId;

	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private List<QuotationTO> quotationDS;

	private List<SupplierTO> suppliersList = new ArrayList<SupplierTO>();
	String supplier = null;
	String purchased = null;

	public String returnSuccess() {
		return SUCCESS;
	}

	public String showUnPurchasedQuotation() {
		try {
			List<Quotation> quotations = quotationBL.getQuotationService()
					.getUnPurchasedQuotation(getImplementation());
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			if (null != quotations && quotations.size() > 0) {
				for (Quotation quotation : quotations) {
					array = new JSONArray();
					array.add(quotation.getQuotationId());
					array.add(quotation.getCurrency().getCurrencyId());
					array.add(quotation.getQuotationNumber());
					array.add(null != quotation.getTender() ? quotation
							.getTender().getTenderNumber() : "");
					array.add(null != quotation.getRequisition() ? quotation
							.getRequisition().getReferenceNumber() : "");
					array.add(DateFormat.convertDateToString(quotation
							.getQuotationDate().toString()));
					array.add(DateFormat.convertDateToString(quotation
							.getExpiryDate().toString()));
					if (null != quotation.getSupplier().getPerson())
						array.add(quotation.getSupplier().getPerson()
								.getFirstName()
								+ " "
								+ quotation.getSupplier().getPerson()
										.getLastName());
					else
						array.add(null != quotation.getSupplier()
								.getCmpDeptLocation() ? quotation.getSupplier()
								.getCmpDeptLocation().getCompany()
								.getCompanyName() : quotation.getSupplier()
								.getCompany().getCompanyName());
					array.add(quotation.getSupplier().getSupplierId());
					if(quotation.getCreditTerm()!=null)
						array.add(quotation.getCreditTerm().getCreditTermId());
					else
						array.add(0);
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String execute() {
		try {
			List<Quotation> quotations = quotationBL.getQuotationService()
					.getAllQuotation(getImplementation());
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			if (null != quotations && quotations.size() > 0) {
				for (Quotation quotation : quotations) {
					array = new JSONArray();
					array.add(quotation.getQuotationId());
					array.add(quotation.getCurrency().getCurrencyId());
					array.add(quotation.getQuotationNumber());
					array.add(null != quotation.getTender() ? quotation
							.getTender().getTenderNumber() : "");
					array.add(null != quotation.getRequisition() ? quotation
							.getRequisition().getReferenceNumber() : "");
					array.add(DateFormat.convertDateToString(quotation
							.getQuotationDate().toString()));
					array.add(DateFormat.convertDateToString(quotation
							.getExpiryDate().toString()));
					if (null != quotation.getSupplier().getPerson())
						array.add(quotation.getSupplier().getPerson()
								.getFirstName()
								+ " "
								+ quotation.getSupplier().getPerson()
										.getLastName());
					else
						array.add(null != quotation.getSupplier()
								.getCmpDeptLocation() ? quotation.getSupplier()
								.getCmpDeptLocation().getCompany()
								.getCompanyName() : quotation.getSupplier()
								.getCompany().getCompanyName());
					array.add(quotation.getSupplier().getSupplierId());
					if (null != quotation.getPurchases()
							&& quotation.getPurchases().size() > 0) {
						array.add("Purchased");
						array.add(true);
					} else {
						array.add("Pending");
						array.add(false);
					}
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Generate Quotation Number
	private String generateQuotationNumber() throws Exception {
		return SystemBL.getReferenceStamp(Quotation.class.getName(),
				getImplementation());
	}

	// Save Quotation Details
	public String saveQuotation() {
		try {
			Quotation quotation = new Quotation();
			Currency currency = new Currency();
			Requisition requisition = null;
			if (tenderId > 0) {
				Tender tender = new Tender();
				tender.setTenderId(tenderId);
				quotation.setTender(tender);
			}
			if (requisitionId > 0) {
				requisition = quotationBL.getRequisitionService()
						.getRequisitionById(requisitionId);
				requisition.setStatus(RequisitionProcessStatus.InProgress
						.getCode());
				quotation.setRequisition(requisition);
			}
			
			if(termId>0){
				CreditTerm ct=new CreditTerm();
				ct.setCreditTermId(termId);
				quotation.setCreditTerm(ct);
			}
			quotation.setQuotationDate(DateFormat
					.convertStringToDate(quotationDate));
			quotation.setExpiryDate(DateFormat.convertStringToDate(expiryDate));
			quotation.setDescription(description);
			if (currencyId > 0) {
				currency.setCurrencyId(currencyId);
				quotation.setCurrency(currency);
			} else {
				quotation.setCurrency(quotationBL.getCurrencyBL()
						.getDefaultCurrency());
			}

			quotation.setImplementation(getImplementation());
			Supplier supplier = new Supplier();
			supplier.setSupplierId(supplierId);
			quotation.setSupplier(supplier);

			List<QuotationDetail> quotationDetails = new ArrayList<QuotationDetail>();
			QuotationDetail quotationDetail;
			Product product;
			String[] quotationDetailList = splitValues(quotationStringDetail,
					"#@");
			RequisitionDetail requisitionDetail = null;
			try {
				for (String string : quotationDetailList) {
					String[] detailString = splitValues(string, "__");
					quotationDetail = new QuotationDetail();
					product = new Product();
					product.setProductId(Long.parseLong(detailString[0]));
					quotationDetail.setProduct(product);
					quotationDetail.setUnitRate(Double
							.parseDouble(detailString[1]));
					quotationDetail.setTotalUnits(Double
							.parseDouble(detailString[2]));
					if (Long.parseLong(detailString[3]) > 0l)
						quotationDetail.setQuotationDetailId(Long
								.parseLong(detailString[3]));
					else
						quotationDetail.setQuotationDetailId(null);
					if (!detailString[4].equalsIgnoreCase("##"))
						quotationDetail.setDescription(detailString[4]);
					if (Long.parseLong(detailString[5]) > 0l) {
						requisitionDetail = new RequisitionDetail();
						requisitionDetail.setRequisitionDetailId(Long
								.parseLong(detailString[5]));
						quotationDetail.setRequisitionDetail(requisitionDetail);
					}

					quotationDetails.add(quotationDetail);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<QuotationDetail> deleteQuotationDetailList = null;
			if (quotationId > 0) {
				quotation.setQuotationNumber(quotationNumber);
				quotation.setQuotationId(quotationId);
				try {
					List<QuotationDetail> quotationDList = this
							.getQuotationBL().getQuotationService()
							.getQuotationDetail(quotationId);
					Collection<Long> listOne = new ArrayList<Long>();
					Collection<Long> listTwo = new ArrayList<Long>();
					Map<Long, QuotationDetail> hashQuotation = new HashMap<Long, QuotationDetail>();
					if (null != quotationDList && quotationDList.size() > 0) {
						for (QuotationDetail list : quotationDList) {
							listOne.add(list.getQuotationDetailId());
							hashQuotation
									.put(list.getQuotationDetailId(), list);
						}
						if (null != hashQuotation && hashQuotation.size() > 0) {
							for (QuotationDetail list : quotationDetails) {
								listTwo.add(list.getQuotationDetailId());
							}
						}
					}
					Collection<Long> similar = new HashSet<Long>(listOne);
					Collection<Long> different = new HashSet<Long>();
					different.addAll(listOne);
					different.addAll(listTwo);
					similar.retainAll(listTwo);
					different.removeAll(similar);
					if (null != different && different.size() > 0) {
						QuotationDetail tempQuotationDetail = null;
						deleteQuotationDetailList = new ArrayList<QuotationDetail>();
						for (Long list : different) {
							if (null != list && !list.equals(0)) {
								tempQuotationDetail = new QuotationDetail();
								tempQuotationDetail = hashQuotation.get(list);
								deleteQuotationDetailList
										.add(tempQuotationDetail);
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				quotation.setQuotationNumber(generateQuotationNumber());
			}
			quotation.setQuotationDetails(new HashSet<QuotationDetail>(
					quotationDetails));
			quotationBL.saveQuotation(quotation, deleteQuotationDetailList,
					requisition);
			returnMessage = "SUCCESS";
		} catch (Exception e) {
			returnMessage = "FAILURE";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public String getQuotationForEdit() {
		try {
			List<CreditTerm> paymentList = quotationBL.getCreditTermBL()
			.getCreditTermService()
			.getAllSupplierCreditTerms(getImplementation());
			ServletActionContext.getRequest().setAttribute("PAYMENT_DETAIL",
					paymentList);
			if (recordId != null && recordId > 0) {
				quotationId = recordId;
				CommentVO comment = new CommentVO();
				if (quotationId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(DirectPayment.class.getName());
					comment = quotationBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			List<Currency> currencies = getQuotationBL().getCurrencyBL()
					.getCurrencyService().getAllCurrency(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"QUOTATION_CURRENCY", currencies);
			if (null == quotationId || quotationId == 0) {
				ServletActionContext.getRequest().setAttribute(
						"QUOTATION_NUMBER",
						SystemBL.getReferenceStamp(Quotation.class.getName(),
								getImplementation()));
				return SUCCESS;
			}

			Quotation quotation = quotationBL.getQuotationService()
					.getQuotationById(quotationId).get(0);
			List<QuotationDetail> quotationDetails = quotationBL
					.getQuotationService().getQuotationDetail(
							quotation.getQuotationId());
			Collections.sort(quotationDetails,
					new Comparator<QuotationDetail>() {
						public int compare(QuotationDetail o1,
								QuotationDetail o2) {
							return o1.getQuotationDetailId().compareTo(
									o2.getQuotationDetailId());
						}
					});
			List<QuotationDetailVO> quotationDetailVOs = new ArrayList<QuotationDetailVO>();
			QuotationDetailVO quotationDetailVO = null;
			for (QuotationDetail quotationDetail : quotationDetails) {
				quotationDetailVO = new QuotationDetailVO();
				BeanUtils.copyProperties(quotationDetailVO, quotationDetail);
				quotationDetailVO.setItemTypeStr(ItemType.get(
						quotationDetail.getProduct().getItemType()).name());
				quotationDetailVOs.add(quotationDetailVO);

			}
			currencyId = quotation.getCurrency().getCurrencyId();
			ServletActionContext.getRequest().setAttribute("QUOTATION",
					quotation);
			ServletActionContext.getRequest().setAttribute(
					"QUOTATION_quotationDate",
					DateFormat.convertDateToString(quotation.getQuotationDate()
							.toString()));
			ServletActionContext.getRequest().setAttribute(
					"QUOTATION_expiryDate",
					DateFormat.convertDateToString(quotation.getExpiryDate()
							.toString()));
			ServletActionContext.getRequest().setAttribute(
					"QUOTATION_DETAIL_INFO", quotationDetailVOs);
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String deleteQuotation() {

		try {
			if (quotationId != null && quotationId != 0) {
				Quotation quotation = quotationBL.getQuotationService()
						.getQuotationBasicById(quotationId);
				quotationBL.deleteQuotation(
						quotation,
						new ArrayList<QuotationDetail>(quotation
								.getQuotationDetails()));
				returnMessage = "SUCCESS";
			}
		} catch (Exception e) {
			returnMessage = "FAILURE";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public String loadQuotationReportCriteria() {
		try {

			suppliersList = new ArrayList<SupplierTO>();

			suppliersList = convertListTO(quotationBL.getSupplierService()
					.getAllSuppliers(getImplementation()));

			List<Product> products = quotationBL.getProductService()
					.getActiveProduct(getImplementation());
			ServletActionContext.getRequest().setAttribute("PRODUCT_LIST",
					products);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;

	}

	public static List<SupplierTO> convertListTO(List<Supplier> supplierList)
			throws Exception {
		List<SupplierTO> supplierTOList = new ArrayList<SupplierTO>();
		for (Supplier list : supplierList) {
			supplierTOList.add(convertTO(list));
		}
		return supplierTOList;
	}

	public static SupplierTO convertTO(Supplier supplier) {
		SupplierTO supplierTO = new SupplierTO();
		supplierTO.setSupplierId(supplier.getSupplierId());
		supplierTO.setSupplierNumber(supplier.getSupplierNumber());
		supplierTO.setCombinationId(supplier.getCombination()
				.getCombinationId());
		if (null != supplier.getPerson())
			supplierTO.setSupplierName(supplier.getPerson().getFirstName()
					+ " " + supplier.getPerson().getLastName());
		else
			supplierTO
					.setSupplierName(null != supplier.getCmpDeptLocation() ? supplier
							.getCmpDeptLocation().getCompany().getCompanyName()
							: supplier.getCompany().getCompanyName());
		supplierTO.setType(Constants.Accounts.SupplierType.get(
				supplier.getSupplierType()).toString());
		if (null != supplier.getCreditTerm())
			supplierTO.setPaymentTermId(supplier.getCreditTerm()
					.getCreditTermId());
		return supplierTO;

	}

	public String quotationFilterReportHtm() {
		try {
			List<Quotation> quotations = fetchQuotationDetails();
			List<QuotationDetailVO> quotationDetailVOs = null;
			QuotationDetailVO quotationDetailVO = null;
			QuotationVO quotationVO = null;
			if (null != quotations && quotations.size() > 0) {
				quotationDetailVOs = new ArrayList<QuotationDetailVO>();
				for (Quotation quotation : quotations) {
					quotationVO = new QuotationVO();
					BeanUtils.copyProperties(quotationVO, quotation);
					if (null != quotation.getSupplier().getPerson())
						quotationVO.setSupplierName(quotation.getSupplier()
								.getPerson().getFirstName()
								+ " "
								+ quotation.getSupplier().getPerson()
										.getLastName());
					else
						quotationVO.setSupplierName(null != quotation
								.getSupplier().getCmpDeptLocation() ? quotation
								.getSupplier().getCmpDeptLocation()
								.getCompany().getCompanyName() : quotation
								.getSupplier().getCompany().getCompanyName());
					quotationVO.setQuotationDateStr(DateFormat
							.convertDateToString(quotation.getQuotationDate()
									.toString()));
					for (QuotationDetail quotationDetail : quotation
							.getQuotationDetails()) {
						quotationDetailVO = new QuotationDetailVO();
						BeanUtils.copyProperties(quotationDetailVO,
								quotationDetail);
						quotationDetailVO.setQuotationVO(quotationVO);
						quotationDetailVO.setTotalRate(quotationDetail
								.getTotalUnits()
								* quotationDetail.getUnitRate());
						quotationDetailVO.setDisplayUnitRate(AIOSCommons
								.formatAmount(quotationDetail.getUnitRate()));
						quotationDetailVO.setDisplayTotalAmount(AIOSCommons
								.formatAmount(quotationDetail.getUnitRate()
										* quotationDetail.getUnitRate()));
						quotationDetailVO.setDisplayQuantity(AIOSCommons
								.formatAmount(quotationDetail.getTotalUnits()));
						quotationDetailVOs.add(quotationDetailVO);
					}
				}
				if (null != quotationDetailVOs && quotationDetailVOs.size() > 0) {
					Collections.sort(quotationDetailVOs,
							new Comparator<QuotationDetailVO>() {
								public int compare(QuotationDetailVO o1,
										QuotationDetailVO o2) {
									return o1
											.getQuotationVO()
											.getQuotationNumber()
											.compareTo(
													o2.getQuotationVO()
															.getQuotationNumber());
								}
							});
					quotationVO = new QuotationVO();
					double totalQuantity = 0;
					double totalUnitRate = 0;
					double totalAmount = 0;
					for (QuotationDetailVO detailVO : quotationDetailVOs) {
						totalQuantity += detailVO.getTotalUnits();
						totalUnitRate += detailVO.getUnitRate();
						totalAmount += detailVO.getTotalRate();
					}
					quotationVO.setDisplayQuantity(AIOSCommons
							.formatAmount(totalQuantity));
					quotationVO.setDisplayUnitRate(AIOSCommons
							.formatAmount(totalUnitRate));
					quotationVO.setDisplayTotalAmount(AIOSCommons
							.formatAmount(totalAmount));
				}
			}
			ServletActionContext.getRequest().setAttribute("QUOTATION_DETAIL",
					quotationDetailVOs);
			ServletActionContext.getRequest().setAttribute("QUOTATION_INFO",
					quotationVO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String quotationReportCriteriaSearch() {
		try {
			List<Quotation> quotations = fetchQuotationDetails();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			if (null != quotations && quotations.size() > 0) {
				for (Quotation quotation : quotations) {
					for (QuotationDetail quotationDetail : quotation
							.getQuotationDetails()) {
						array = new JSONArray();
						array.add(quotation.getQuotationId());
						array.add(quotation.getCurrency().getCurrencyId());
						array.add(quotation.getQuotationNumber());
						if (null != quotation.getTender())
							array.add(quotation.getTender().getTenderNumber());
						else
							array.add("");
						if (null != quotation.getRequisition())
							array.add(quotation.getRequisition()
									.getReferenceNumber());
						else
							array.add("");
						array.add(DateFormat.convertDateToString(quotation
								.getQuotationDate().toString()));
						array.add(DateFormat.convertDateToString(quotation
								.getExpiryDate().toString()));
						if (null != quotation.getSupplier().getPerson())
							array.add(quotation.getSupplier().getPerson()
									.getFirstName()
									+ " "
									+ quotation.getSupplier().getPerson()
											.getLastName());
						else
							array.add(null != quotation.getSupplier()
									.getCmpDeptLocation() ? quotation
									.getSupplier().getCmpDeptLocation()
									.getCompany().getCompanyName() : quotation
									.getSupplier().getCompany()
									.getCompanyName());
						array.add(quotation.getSupplier().getSupplierNumber());
						if (null != quotation.getSupplier().getPerson())
							array.add(quotation.getSupplier().getPerson()
									.getFirstName()
									+ " "
									+ quotation.getSupplier().getPerson()
											.getLastName());
						else
							array.add(null != quotation.getSupplier()
									.getCmpDeptLocation() ? quotation
									.getSupplier().getCmpDeptLocation()
									.getCompany().getCompanyName() : quotation
									.getSupplier().getCompany()
									.getCompanyName());
						array.add(quotationDetail.getProduct().getCode() + "--"
								+ quotationDetail.getProduct().getProductName());
						data.add(array);
					}
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}

	private List<Quotation> fetchQuotationDetails() throws Exception {
		Date fromDate = null;
		Date toDate = null;
		supplierId = null;
		Long productId = null;
		if (!quotationDate.equalsIgnoreCase("undefined")
				&& !quotationDate.equalsIgnoreCase("null")) {
			fromDate = DateFormat.convertStringToDate(quotationDate);
		}
		if (!expiryDate.equalsIgnoreCase("undefined")
				&& !expiryDate.equalsIgnoreCase("null")) {
			toDate = DateFormat.convertStringToDate(expiryDate);
		}

		if (!supplier.equalsIgnoreCase("undefined")
				&& !supplier.equalsIgnoreCase("null")) {
			supplierId = Long.parseLong(supplier);
		}
		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}
		List<Quotation> quotations = quotationBL.getQuotationService()
				.getQuotationsByDifferentValues(fromDate, toDate, supplierId,
						productId, getImplementation());
		return quotations;
	}

	public String populateQuotationJasperReportData() {
		try {
			quotationDS = new ArrayList<QuotationTO>();
			QuotationTO to = new QuotationTO();

			Quotation quotation = quotationBL.getQuotationService()
					.getQuotationByIdWithDifferentChilds(quotationId, true,
							true, true, false, true);

			/*
			 * setImplementation(((Implementation) (ServletActionContext
			 * .getRequest().getSession().getAttribute("THIS"))));
			 */

			to.convertToQuotationTO(quotation);

			quotationDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (quotationDS != null && quotationDS.size() == 0) {
				quotationDS.add(to);
			} else {
				quotationDS.set(0, to);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String quotationFilterSearchReport() {
		try {

			Quotation quotation = quotationBL.getQuotationService()
					.getQuotationByIdWithDifferentChilds(quotationId, true,
							true, true, false, true); /*
													 * quotationService
													 * .getQuotationById
													 * (quotationId);
													 */
			/*
			 * List<QuotationDetail> quotationDetails = quotationService
			 * .getQuotationDetail(quotationId);
			 */

			ServletActionContext.getRequest().setAttribute("QUOTATION_INFO",
					quotation);
			/*
			 * ServletActionContext.getRequest().setAttribute(
			 * "QUOTATION_DETAIL_INFO", quotationDetails);
			 */

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Quotation Order filter search for report as XLS
	public String quotationFilterSearchReportXLS() {
		try {

			Quotation quotation = quotationBL.getQuotationService()
					.getQuotationByIdWithDifferentChilds(quotationId, true,
							true, true, false, true);

			/*
			 * List<Quotation> quotations = quotationService
			 * .getQuotationById(quotationId); List<QuotationDetail>
			 * quotationDetails = quotationService
			 * .getQuotationDetail(quotationId);
			 */

			String str = "";

			String fromDatee = "-NA-";
			String toDatee = "-NA-";
			String isQuoted = "-NA-";
			String product = "-NA-";

			if (!quotationDate.equalsIgnoreCase("undefined")
					&& !quotationDate.equalsIgnoreCase("null")) {
				fromDatee = quotationDate;
			}
			if (!expiryDate.equalsIgnoreCase("undefined")
					&& !expiryDate.equalsIgnoreCase("null")) {
				toDatee = expiryDate;
			}
			if (!purchased.equalsIgnoreCase("undefined")
					&& !purchased.equalsIgnoreCase("null")) {
				isQuoted = purchased;
			}
			if (!supplier.equalsIgnoreCase("undefined")
					&& !supplier.equalsIgnoreCase("null")) {
				product = supplier;
			}

			str = "Purchase Details\n\n";
			str += "Filter Condition\n";
			str += "From Date,To Date,Supplier,Is Purchased\n";
			str += fromDatee + "," + toDatee + "," + product + "," + isQuoted
					+ "\n\n";
			if (quotation.getQuotationDetails() != null
					&& quotation.getQuotationDetails().size() > 0) {

				str += "\nTender No. , "
						+ quotation.getTender().getTenderNumber()
						+ " , Number, "
						+ quotation.getSupplier().getSupplierNumber();
				str += "\nQuotation Number , " + quotation.getQuotationNumber()
						+ " , Supplier Name, ";
				if (quotation.getSupplier() != null
						&& quotation.getSupplier().getPerson() != null) {
					str += quotation.getSupplier().getPerson().getFirstName()
							+ quotation.getSupplier().getPerson().getLastName();
				}
				str += "\nCurrency , "
						+ quotation.getCurrency().getCurrencyPool().getCode()
						+ " , Description, " + quotation.getDescription();
				str += "\nQuotation Date , "
						+ DateFormat.convertDateToString(quotation
								.getQuotationDate().toString());
				str += "\nQuotation Expiry , "
						+ DateFormat.convertDateToString(quotation
								.getExpiryDate().toString());

				str += "\n\nItem Type, Product, UOM, Unit Rate, Quantity, Total Amount, Description";
				str += "\n";
				for (QuotationDetail quotatonDetail : quotation
						.getQuotationDetails()) {
					str += quotatonDetail.getProduct().getItemType()
							+ ","
							+ quotatonDetail.getProduct().getProductName()
							+ ","
							+ quotatonDetail.getProduct()
									.getLookupDetailByProductUnit()
									.getDisplayName() + ","
							+ quotatonDetail.getUnitRate() + ","
							+ quotatonDetail.getTotalUnits() + ","
							+ quotatonDetail.getUnitRate()
							* quotatonDetail.getTotalUnits() + ","
							+ quotatonDetail.getDescription();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public QuotationBL getQuotationBL() {
		return quotationBL;
	}

	public void setQuotationBL(QuotationBL quotationBL) {
		this.quotationBL = quotationBL;
	}

	public Long getQuotationId() {
		return quotationId;
	}

	public Long getTenderId() {
		return tenderId;
	}

	public String getQuotationNumber() {
		return quotationNumber;
	}

	public String getQuotationDate() {
		return quotationDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public Long getSupplierSiteId() {
		return supplierSiteId;
	}

	public String getDescription() {
		return description;
	}

	public void setQuotationId(Long quotationId) {
		this.quotationId = quotationId;
	}

	public void setTenderId(Long tenderId) {
		this.tenderId = tenderId;
	}

	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}

	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public void setSupplierSiteId(Long supplierSiteId) {
		this.supplierSiteId = supplierSiteId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getQuotationStringDetail() {
		return quotationStringDetail;
	}

	public void setQuotationStringDetail(String quotationStringDetail) {
		this.quotationStringDetail = quotationStringDetail;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public List<SupplierTO> getSuppliersList() {
		return suppliersList;
	}

	public void setSuppliersList(List<SupplierTO> suppliersList) {
		this.suppliersList = suppliersList;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getPurchased() {
		return purchased;
	}

	public void setPurchased(String purchased) {
		this.purchased = purchased;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public List<QuotationTO> getQuotationDS() {
		return quotationDS;
	}

	public void setQuotationDS(List<QuotationTO> quotationDS) {
		this.quotationDS = quotationDS;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(Long requisitionId) {
		this.requisitionId = requisitionId;
	}

	public long getTermId() {
		return termId;
	}

	public void setTermId(long termId) {
		this.termId = termId;
	}

}
