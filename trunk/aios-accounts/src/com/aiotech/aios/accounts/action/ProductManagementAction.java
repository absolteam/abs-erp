package com.aiotech.aios.accounts.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.service.bl.ProductBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.AssetCategory;
import com.aiotech.aios.common.to.Constants.Accounts.CostingType;
import com.aiotech.aios.common.to.Constants.Accounts.ExpenseCategory;
import com.aiotech.aios.common.to.Constants.Accounts.InventoryCategory;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationSubType;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationType;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ProductManagementAction extends ActionSupport {

	private static final long serialVersionUID = 987734309835636945L;
	// Common Variables
	private long productId;
	private String productName;
	private long unitId;
	private String unitName;
	private String itemType;
	private long itemNature;
	private boolean status;
	private long productCategory;
	private long inventoryAccount;
	private long expenseAccount;
	private byte costingType;
	private int id;
	private String showPage;
	private List<Product> productList;
	private String sqlReturnMessage;
	private String productCode;
	private long categoryId;
	private byte subItemType;
	private boolean reOrdering;
	private int maxLeadTime;
	private int minLeadTime;
	private int maxUsage;
	private int minUsage;
	private double reOrderQuantity;
	private int shelfId;
	private String fromHours;
	private String toHours;
	private String rowId;
	private String barCode;
	private String publicName;
	private String description;
	private boolean specialPos;
	private Map<Byte, String> itemSubTypes;
	private List<Object> aaData;
	private Object productDetail;

	private Long recordId;
	private Long messageId;
	private Integer processFlag;

	private String imgPath;
	private Integer requiredImageNumber;
	private String hsCode;

	// Common Object
	private ProductBL productBL;

	// Logger Property
	private static final Logger log = LogManager
			.getLogger(ProductManagementAction.class);

	/** To redirect the action success page **/
	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess");
		recordId = null;
		return SUCCESS;
	}

	// to list the grid values
	public String showProductDetails() {
		try {
			log.info("Module: Accounts : Method: showProductDetails");
			aaData = productBL.showProductDetails(itemType);
			log.info("Module: Accounts : Method: showProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showRequisitionProductDetails() {
		try {
			log.info("Module: Accounts : Method: showRequisitionProductDetails");
			aaData = productBL.showRequisitionProductDetails();
			log.info("Module: Accounts : Method: showRequisitionProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showRequisitionProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showSalesRequisitionProductDetails() {
		try {
			log.info("Module: Accounts : Method: showSalesRequisitionProductDetails");
			aaData = productBL.showSalesRequisitionProductDetails();
			log.info("Module: Accounts : Method: showSalesRequisitionProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showSalesRequisitionProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// get all inventory products
	public String productInventoryMakingPrint() {
		try {
			log.info("Module: Accounts : Method: productInventoryMakingPrint");
			List<ProductCategoryVO> products = productBL
					.getInventoryProduct("I");
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAILS",
					products);
			log.info("Module: Accounts : Method: productInventoryMakingPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: productInventoryMakingPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showCraftProductDetails() {
		try {
			log.info("Module: Accounts : Method: showCraftProductDetails");
			aaData = productBL.showCraftProductDetails();
			log.info("Module: Accounts : Method: showCraftProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCraftProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showFinishedProductDetails() {
		try {
			log.info("Module: Accounts : Method: showFinishedProductDetails");
			aaData = productBL.showFinishedProductDetails();
			log.info("Module: Accounts : Method: showFinishedProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showFinishedProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showNonCraftServiceProductDetails() {
		try {
			log.info("Module: Accounts : Method: showNonCraftServiceProductDetails");
			aaData = productBL.showNonCraftServiceProductDetails();
			log.info("Module: Accounts : Method: showNonCraftServiceProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showNonCraftServiceProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showPurchaseProductDetails() {
		try {
			log.info("Module: Accounts : Method: showPurchaseProductDetails");
			aaData = productBL.showPurchaseProductDetails(itemType);
			log.info("Module: Accounts : Method: showPurchaseProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showPurchaseProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showSalesProductDetails() {
		try {
			log.info("Module: Accounts : Method: showSalesProductDetails");
			aaData = productBL.showSalesProductDetails(itemType);
			log.info("Module: Accounts : Method: showSalesProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showSalesProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showSpecailProductDetails() {
		try {
			log.info("Module: Accounts : Method: showSpecailProductDetails");
			aaData = productBL.getInventorySpecialProducts();
			log.info("Module: Accounts : Method: showSpecailProductDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showSpecailProductDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showCategoryProduct() {
		try {
			log.info("Module: Accounts : Method: showCategoryProduct");
			List<Product> products = productBL.getProductByCategory(categoryId);
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAILS",
					products);
			log.info("Module: Accounts : Method: showCategoryProduct: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCategoryProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showCategoryPOSProduct() {
		try {
			log.info("Module: Accounts : Method: showCategoryPOSProduct");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			POSUserTill posUserSession = (POSUserTill) (session
					.getAttribute("POS_USERTILL_SESSION"
							+ sessionObj.get("jSessionId")));
			List<Product> products = productBL.getPOSProductByCategory(
					categoryId, posUserSession.getStore().getStoreId());
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAILS",
					products);
			log.info("Module: Accounts : Method: showCategoryPOSProduct: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showCategoryPOSProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductByBarCode() {
		try {
			log.info("Module: Accounts : Method: showProductByBarCode");
			Product product = productBL.getProductService()
					.getProductCodeByBarCode(productBL.getImplementation(),
							barCode);
			if (null != product)
				productDetail = productBL.addProductObject(product);
			log.info("Module: Accounts : Method: showProductByBarCode: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductByBarCode: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to list the grid values
	public String showProductList() {
		try {
			log.info("Module: Accounts : Method: showProductList");
			productList = productBL.getProductService().getAllProduct(
					productBL.getImplementation());

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			String type = "";
			String subtype = "";
			if (null != productList && productList.size() > 0) {
				for (Product product : productList) {
					array = new JSONArray();
					array.add(product.getProductId());
					array.add(product.getCode());
					array.add(product.getProductName());
					array.add(product.getLookupDetailByProductUnit()
							.getDisplayName());
					array.add(ItemType.get(product.getItemType()).name());
					type = InventorySubCategory.get(product.getItemSubType())
							.name().replaceAll("_", " ");
					subtype = type.replaceAll("0", "/");
					array.add(subtype);
					array.add(null != product.getProductCategory() ? product
							.getProductCategory().getCategoryName() : "");
					array.add(product.getStatus() ? "Active" : "In Active");
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			log.info("Module: Accounts : Method: showProductList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductApproval() {
		try {
			log.info("Module: Accounts : Method: showProductApproval");
			productId = recordId;
			showProductEntry();
			log.info("Module: Accounts : Method: showProductApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductApproval: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductRejectEntry() {
		try {
			log.info("Module: Accounts : Method: showProductRejectEntry");
			productId = recordId;
			showProductEntry();
			showPage = "edit";
			log.info("Module: Accounts : Method: showProductRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductRejectEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductSalesHistory() {
		try {
			log.info("Module: Accounts : Method: showProductSalesHistory");

			aaData = productBL.getProductSales(productId);

			log.info("Module: Accounts : Method: showProductSalesHistory: Action Success");
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductSalesHistory: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showProductPurchasesHistory() {
		try {
			log.info("Module: Accounts : Method: showProductPurchasesHistory");

			aaData = productBL.getProductPurchases(productId);

			log.info("Module: Accounts : Method: showProductPurchasesHistory: Action Success");
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductPurchasesHistory: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to create product
	public String showProductEntry() {
		try {
			log.info("Module: Accounts : Method: showProductEntry");
			itemSubTypes = new HashMap<Byte, String>();
			if (productId > 0) {
				Product productEntity = productBL.getProductService()
						.getProductById(productId);
				ProductVO productVO = new ProductVO();
				BeanUtils.copyProperties(productVO, productEntity);
				if (null != productEntity.getShelf()) {
					productVO.setStoreName(productEntity
							.getShelf()
							.getShelf()
							.getAisle()
							.getStore()
							.getStoreName()
							.concat("-->")
							.concat(productEntity
									.getShelf()
									.getShelf()
									.getAisle()
									.getSectionName()
									.concat("-->")
									.concat(productEntity
											.getShelf()
											.getShelf()
											.getName()
											.concat("-->")
											.concat(productEntity.getShelf()
													.getName()))));
				}
				List<Image> images = productBL
						.getImageBL()
						.getImageService()
						.getImagesByRecordId(productEntity.getProductId(),
								Product.class.getSimpleName());
				if (null != images && images.size() > 0) {
					byte[] encoded = productBL.getImageBL().showImage(
							images.get(0));
					if (null != encoded) {
						String pmg = null;
						encoded = Base64.encodeBase64(encoded);
						pmg = new String(encoded);
						productVO.setProductPic(pmg);
					}
				}
				productVO.setItemTypeName(ItemType.get(productVO.getItemType())
						.name());
				productVO.setSubItemTypeName(InventorySubCategory
						.get(productVO.getCostingType()).name()
						.replaceAll("_", " "));
				productVO.setCostingMethod(CostingType.get(
						productVO.getCostingType()).name());
				productVO.setStatusStr(productVO.getStatus() ? "Active"
						: "Inactive");
				productVO.setItemNatureName(null != productVO
						.getLookupDetailByItemNature() ? productVO
						.getLookupDetailByItemNature().getDisplayName() : null);
				ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
						productVO);
				if (productEntity.getItemType() == 'I')
					for (InventoryCategory e : EnumSet
							.allOf(InventoryCategory.class))
						itemSubTypes.put(e.getCode(),
								e.name().replaceAll("_", " "));
				else if (productEntity.getItemType() == 'E')
					for (ExpenseCategory e : EnumSet
							.allOf(ExpenseCategory.class))
						itemSubTypes.put(e.getCode(),
								e.name().replaceAll("_", " "));
				else if (productEntity.getItemType() == 'A')
					for (AssetCategory e : EnumSet.allOf(AssetCategory.class))
						itemSubTypes.put(e.getCode(),
								e.name().replaceAll("_", " "));
				for (Entry<Byte, String> subType : itemSubTypes.entrySet()) {
					itemSubTypes.put(subType.getKey(), subType.getValue()
							.replaceAll("0", "/"));
				}

				CommentVO comment = new CommentVO();
				if (recordId != null && productEntity != null
						&& productEntity.getProductId() != 0 && recordId > 0) {
					comment.setRecordId(productEntity.getProductId());
					comment.setUseCase(Product.class.getName());
					comment = productBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			List<LookupDetail> itemNatures = productBL.getLookupMasterBL()
					.getActiveLookupDetails("ITEM_NATURE",
							productBL.getImplementation());
			List<LookupDetail> productUnits = productBL.getLookupMasterBL()
					.getActiveLookupDetails("PRODUCT_UNITNAME",
							productBL.getImplementation());
			List<ProductCategoryVO> productCategoryVOs = productBL
					.getProductCategoryBL().getProductCategory();
			Map<Character, ItemType> itemTypes = new HashMap<Character, ItemType>();
			for (ItemType e : EnumSet.allOf(ItemType.class)) {
				itemTypes.put(e.getCode(), e);
			}
			Map<Byte, CostingType> costingTypes = new HashMap<Byte, CostingType>();
			for (CostingType e : EnumSet.allOf(CostingType.class)) {
				costingTypes.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("ITEM_TYPES",
					itemTypes);
			ServletActionContext.getRequest().setAttribute("COSTING_TYPES",
					costingTypes);
			List<Product> products = productBL.getProductService()
					.getOnlyProducts(productBL.getImplementation());
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			ServletActionContext.getRequest().setAttribute("PRODUCTS_LIST",
					products);
			ServletActionContext.getRequest().setAttribute("UNITLIST",
					productUnits);
			ServletActionContext.getRequest().setAttribute("PRODUCT_CATEGORY",
					productCategoryVOs);
			ServletActionContext.getRequest().setAttribute("ITEM_NATURE",
					itemNatures);

			log.info("Module: Accounts : Method: showProductEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String savePopupProduct() {
		try {
			log.info("Module: Accounts : Method: savePopupProduct");
			this.saveProduct();
			log.info("Module: Accounts : Method: savePopupProduct: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: savePopupProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save product details
	public String saveProduct() {
		try {
			log.info("Module: Accounts : Method: saveProduct");
			boolean flag = true;
			productList = productBL.getProductService().getAllProduct(
					productBL.getImplementation());
			if (null != productList && productList.size() > 0) {
				for (Product list : productList) {
					if (!list.getCode().equalsIgnoreCase(productCode)
							&& !list.getProductName().equalsIgnoreCase(
									productName) && flag) {
						flag = true;
					} else {
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				Product productEntity = new Product();
				productEntity.setProductName(productName);
				productEntity.setItemType(itemType.charAt(0));
				if (itemNature > 0) {
					LookupDetail lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(itemNature);
					productEntity.setLookupDetailByItemNature(lookupDetail);
				}
				productEntity.setStatus(status);
				productEntity.setCode(productCode);
				productEntity.setBarCode(barCode);
				productEntity.setDescription(description);
				if (productCategory > 0) {
					ProductCategory category = new ProductCategory();
					category.setProductCategoryId(productCategory);
					productEntity.setProductCategory(category);
				}
				productEntity.setItemSubType(subItemType);
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(unitId);
				productEntity.setLookupDetailByProductUnit(lookupDetail);
				productEntity.setImplementation(productBL.getImplementation());

				Combination combination = new Combination();
				if (inventoryAccount > 0) {
					combination.setCombinationId(inventoryAccount);
					Combination analysisCombination = null;
					analysisCombination = productBL.getCombinationBL()
							.createAnalysisCombination(combination,
									productEntity.getCode(), true);
					productEntity
							.setCombinationByInventoryAccount(analysisCombination);
				} else {
					combination.setCombinationId(expenseAccount);
					Combination analysisCombination = null;
					analysisCombination = productBL.getCombinationBL()
							.createAnalysisCombination(combination,
									productEntity.getCode(), true);
					productEntity
							.setCombinationByExpenseAccount(analysisCombination);
				}
				productEntity.setCostingType(costingType);
				productEntity.setReOdering(reOrdering);
				productEntity.setMinUsage(minUsage > 0 ? minUsage : null);
				productEntity.setMaxLeadTime(maxLeadTime > 0 ? maxLeadTime
						: null);
				productEntity.setMinLeadTime(minLeadTime > 0 ? minLeadTime
						: null);
				productEntity.setMaxUsage(maxUsage > 0 ? maxUsage : null);

				productEntity
						.setReOrderQuantity(reOrderQuantity > 0 ? reOrderQuantity
								: null);
				productEntity.setFromHours(fromHours);
				productEntity.setToHours(toHours);
				productEntity.setSpecialPos(specialPos);

				productEntity.setPublicName(publicName);
				productEntity.setHsCode(hsCode);
				if (shelfId > 0) {
					Shelf shelf = new Shelf();
					shelf.setShelfId(shelfId);
					productEntity.setShelf(shelf);
				}
				productBL.saveProductDetails(productEntity);
				sqlReturnMessage = "Success";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				log.info("Module: Accounts : Method: saveProduct: Action Success");
				return SUCCESS;
			} else {
				sqlReturnMessage = "Duplicating Product code or Description";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				log.info("Module: Accounts : Method: saveProduct: Action Failure "
						+ sqlReturnMessage);
				return ERROR;
			}

		} catch (Exception ex) {
			sqlReturnMessage = "Error in product creation.";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			log.info("Module: Accounts : Method: saveProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to update product
	public String updateProduct() {
		try {
			log.info("Module: Accounts : Method: updateProduct");
			boolean flag = true;
			List<Product> productList = productBL.getProductService()
					.getAllProduct(productBL.getImplementation());
			if (null != productList && productList.size() > 0) {
				for (Product list : productList) {
					if (flag) {
						if (!list.getCode().equalsIgnoreCase(productCode)
								&& !list.getProductName().equalsIgnoreCase(
										productName)
								|| list.getProductId().equals(productId)) {
							flag = true;
						} else {
							flag = false;
							break;
						}
					}
				}
			}
			if (flag) {
				Product productEntity = new Product();
				LookupDetail unit = new LookupDetail();
				productEntity.setProductId(productId);
				productEntity.setProductName(productName);
				productEntity.setDescription(description);
				productEntity.setItemType(itemType.charAt(0));
				if (itemNature > 0) {
					LookupDetail lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(itemNature);
					productEntity.setLookupDetailByItemNature(lookupDetail);
				}
				productEntity.setStatus(status);
				productEntity.setItemSubType(subItemType);
				productEntity.setCostingType(costingType);
				if (productCategory > 0) {
					ProductCategory category = new ProductCategory();
					category.setProductCategoryId(productCategory);
					productEntity.setProductCategory(category);
				}
				unit.setLookupDetailId(unitId);
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(unitId);
				productEntity.setLookupDetailByProductUnit(lookupDetail);
				productEntity.setCode(productCode);
				productEntity.setBarCode(barCode);
				productEntity.setImplementation(productBL.getImplementation());
				Combination combination = new Combination();
				if (inventoryAccount > 0) {
					Combination invcombination = productBL.getCombinationBL()
							.getCombinationService()
							.getCombinationAccountDetail(inventoryAccount);
					if (null == invcombination.getAccountByAnalysisAccountId()) {
						combination.setCombinationId(inventoryAccount);
						Combination analysisCombination = null;
						analysisCombination = productBL.getCombinationBL()
								.createAnalysisCombination(combination,
										productEntity.getCode(), true);
						productEntity
								.setCombinationByInventoryAccount(analysisCombination);
					} else {
						combination.setCombinationId(inventoryAccount);
						productEntity
								.setCombinationByInventoryAccount(combination);
					}
				} else {
					Combination expcombination = productBL.getCombinationBL()
							.getCombinationService()
							.getCombinationAccountDetail(expenseAccount);
					if (null == expcombination.getAccountByAnalysisAccountId()) {
						combination.setCombinationId(expenseAccount);
						Combination analysisCombination = null;
						analysisCombination = productBL.getCombinationBL()
								.createAnalysisCombination(combination,
										productEntity.getCode(), true);
						productEntity
								.setCombinationByExpenseAccount(analysisCombination);
					} else {
						combination.setCombinationId(expenseAccount);
						productEntity
								.setCombinationByExpenseAccount(combination);
					}
				}
				productEntity.setReOdering(reOrdering);
				productEntity.setMaxLeadTime(maxLeadTime > 0 ? maxLeadTime
						: null);
				productEntity.setMinLeadTime(minLeadTime > 0 ? minLeadTime
						: null);
				productEntity.setMaxUsage(maxUsage > 0 ? maxUsage : null);
				productEntity.setMinUsage(minUsage > 0 ? maxUsage : null);
				productEntity
						.setReOrderQuantity(reOrderQuantity > 0 ? reOrderQuantity
								: null);
				productEntity.setFromHours(fromHours);
				productEntity.setToHours(toHours);
				productEntity.setSpecialPos(specialPos);
				productEntity.setPublicName(publicName);
				productEntity.setHsCode(hsCode);
				if (shelfId > 0) {
					Shelf shelf = new Shelf();
					shelf.setShelfId(shelfId);
					productEntity.setShelf(shelf);
				}
				productBL.saveProductDetails(productEntity);
				sqlReturnMessage = "Success";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				log.info("Module: Accounts : Method: updateProduct: Action Success");
				return SUCCESS;
			} else {
				sqlReturnMessage = "Duplicating Product code or Description";
				ServletActionContext.getRequest().setAttribute(
						sqlReturnMessage, sqlReturnMessage);
				log.info("Module: Accounts : Method: saveProduct: Action Failure "
						+ sqlReturnMessage);
				return ERROR;
			}
		} catch (Exception ex) {
			sqlReturnMessage = "Sorry! Internal error.";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			log.info("Module: Accounts : Method: updateProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to delete product entry
	public String deleteProduct() {
		log.info("Module: Accounts : Method: deleteProduct");
		try {
			Product product = productBL.getProductService()
					.getBasicProductById(productId);
			productBL.deleteProduct(product);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			log.info("Module: Accounts : Method: updateProduct: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Product Deletion failed";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			log.info("Module: Accounts : Method: deleteProduct: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to generate product code based on the sub category
	public String generateProductCode() {
		try {
			if (subItemType != ' ') {
				List<Product> products = productBL.getProductService()
						.getProductCodeByType(productBL.getImplementation(),
								itemType.charAt(0), subItemType);
				if (products != null && products.size() > 0) {
					Collections.sort(products, new Comparator<Product>() {
						public int compare(Product p1, Product p2) {
							return p2.getCode().compareTo(p1.getCode());
						}
					});
					String subItemType = productBL
							.getProductCodeByItemType(this.subItemType);
					String s = products.get(0).getCode();
					Long tempCode = Long.valueOf(s.substring(2, s.length()));
					tempCode += 1;
					productCode = subItemType + tempCode;
				} else {
					String categoryTyp = productBL
							.getProductCodeByItemType(subItemType);
					productCode = categoryTyp + 100000;
				}
			}
			return SUCCESS;
		} catch (Exception ex) {
			return ERROR;
		}
	}

	// Get Item Sub Type
	public String showItemSubType() {
		try {
			if (null != itemType && !("").equals(itemType)) {
				itemSubTypes = new HashMap<Byte, String>();
				if (("I").equalsIgnoreCase(itemType))
					for (InventoryCategory e : EnumSet
							.allOf(InventoryCategory.class)) {
						itemSubTypes.put(e.getCode(),
								e.name().replaceAll("_", " "));
					}
				else if (("E").equalsIgnoreCase(itemType))
					for (ExpenseCategory e : EnumSet
							.allOf(ExpenseCategory.class))
						itemSubTypes.put(e.getCode(),
								e.name().replaceAll("_", " "));
				else if (("A").equalsIgnoreCase(itemType))
					for (AssetCategory e : EnumSet.allOf(AssetCategory.class))
						itemSubTypes.put(e.getCode(),
								e.name().replaceAll("_", " "));
			}
			for (Entry<Byte, String> subType : itemSubTypes.entrySet()) {
				itemSubTypes.put(subType.getKey(), subType.getValue()
						.replaceAll("0", "/"));
			}
			return SUCCESS;
		} catch (Exception ex) {
			return ERROR;
		}
	}

	// Product list for Asset usage common screen
	public String getProductListForAssetUsage() {
		try {
			log.info("Module: Accounts : Method: showProductList");
			productList = this
					.getProductBL()
					.getProductService()
					.getProductsByItemType(productBL.getImplementation(),
							new Character('A'));

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			if (null != productList && productList.size() > 0) {
				JSONArray array = null;
				for (Product list : productList) {
					array = new JSONArray();
					array.add(list.getProductId());
					array.add(list.getCode());
					array.add(list.getProductName());
					array.add(list.getLookupDetailByProductUnit()
							.getDisplayName());
					array.add(Constants.Accounts.ItemType.get(
							list.getItemType()).name());
					array.add(null != list.getLookupDetailByItemNature() ? list
							.getLookupDetailByItemNature().getDisplayName()
							: "");
					array.add(list.getStatus() ? "Active" : "In Active");
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			log.info("Module: Accounts : Method: showProductList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Show all Products
	public String showAllProducts() {
		try {
			log.info("Module: Accounts : Method: showProductList");
			List<Product> products = productBL.getProductService()
					.getActiveProduct(productBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("PRODUCT_DETAIL",
					products);
			log.info("Module: Accounts : Method: showProductList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: showProductList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Product Print
	public String productPrintReport() {
		try {
			log.info("Module: Accounts : Method: productPrintReport");
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_PRINT_LIST",
					productBL.getProductService().getActiveProduct(
							productBL.getImplementation()));
			log.info("Module: Accounts : Method: productPrintReport: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: productPrintReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Get Product Value by Costing Type
	public String showStockValue() {
		try {
			log.info("Module: Accounts : Method: showStockValue: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showStockValue: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Upload products
	public String uploadProductInventory() {
		try {
			log.info("Module: Accounts : Method: uploadProductInventory: Inside Action");
			ProductCategoryVO productCategoryVO = productBL
					.addProductInventory(productBL.productFileReader(showPage));
			ProductCategory category = null;
			ProductCategory subCategory = null;
			List<ProductCategory> productCategories = new ArrayList<ProductCategory>();
			Map<String, ProductCategory> productCategoryMap = new HashMap<String, ProductCategory>();
			for (ProductCategoryVO categoryVO : productCategoryVO
					.getSubCategories()) {
				category = new ProductCategory();
				BeanUtils.copyProperties(category, categoryVO);
				category.setProductCategoryId(null);
				productCategories.add(category);
				for (ProductCategoryVO subVO : categoryVO.getSubCategorySet()) {
					subCategory = new ProductCategory();
					BeanUtils.copyProperties(subCategory, subVO);
					subCategory.setProductCategoryId(null);
					subCategory.setProductCategory(category);
					productCategoryMap.put(subCategory.getCategoryName(),
							subCategory);
					productCategories.add(subCategory);
				}
			}
			ProductPricing productPricing = new ProductPricing();
			productPricing.setPricingTitle(productBL.getImplementation()
					.getCompanyName().concat(" Pricing"));
			productPricing.setStartDate(Calendar.getInstance().getTime());
			LocalDate currentDate = DateTime.now().toLocalDate();
			currentDate = currentDate.plusYears(5);
			productPricing.setEndDate(currentDate.toDate());
			productPricing.setCurrency(productBL.getCurrencyService()
					.getDefaultCurrency(productBL.getImplementation()));
			productPricing.setImplementation(productBL.getImplementation());
			ProductPricingDetail pricingDetail = null;
			List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>();

			List<Product> products = new ArrayList<Product>();
			Product product = null;
			Stock stock = null;
			Shelf shelf = new Shelf();
			Store store = new Store();
			shelf.setShelfId(21);
			store.setStoreId(16l);
			List<Stock> stocks = new ArrayList<Stock>();
			for (ProductVO productVO : productCategoryVO.getProductVOs()) {
				product = new Product();
				stock = new Stock();
				BeanUtils.copyProperties(product, productVO);
				product.setProductId(null);
				subCategory = productCategoryMap
						.get(productVO.getSubCategory());
				product.setProductCategory(subCategory);
				if (productVO.getQuantity() > 0) {
					stock.setShelf(shelf);
					stock.setStore(store);
					stock.setProduct(product);
					stock.setQuantity(productVO.getQuantity());
					stock.setUnitRate(productVO.getBasePrice());
					stock.setImplementation(productBL.getImplementation());
					stocks.add(stock);
				}

				if (productVO.getBasePrice() > 0) {
					pricingDetail = new ProductPricingDetail();
					pricingDetail.setProduct(product);
					pricingDetail.setBasicCostPrice(productVO.getBasePrice());
					pricingDetail.setStandardPrice(productVO.getBasePrice());
					pricingDetail.setSuggestedRetailPrice(productVO
							.getBasePrice());
					pricingDetail.setSellingPrice(productVO.getBasePrice());
					pricingDetail.setStatus(true);
					productPricingDetails.add(pricingDetail);
				}

				products.add(product);
			}
			productBL.saveProductInventory(productCategories, products, stocks,
					productPricing, productPricingDetails);
			sqlReturnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: uploadProductInventory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: uploadProductInventory: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Upload products
	public String uploadProducts() {
		try {
			log.info("Module: Accounts : Method: uploadProducts: Inside Action");
			ProductCategoryVO productCategoryVO = productBL
					.addProducts(productBL.productFileReader(showPage));
			ProductCategory category = null;
			ProductCategory subCategory = null;
			List<ProductCategory> productCategories = new ArrayList<ProductCategory>();
			Map<String, ProductCategory> productCategoryMap = new HashMap<String, ProductCategory>();
			for (ProductCategoryVO categoryVO : productCategoryVO
					.getSubCategories()) {
				category = new ProductCategory();
				BeanUtils.copyProperties(category, categoryVO);
				category.setProductCategoryId(null);
				productCategories.add(category);
				for (ProductCategoryVO subVO : categoryVO.getSubCategorySet()) {
					subCategory = new ProductCategory();
					BeanUtils.copyProperties(subCategory, subVO);
					subCategory.setProductCategoryId(null);
					subCategory.setProductCategory(category);
					productCategoryMap.put(subCategory.getCategoryName(),
							subCategory);
					subCategory
							.setIsApprove((byte) WorkflowConstants.Status.Published
									.getCode());
					productCategories.add(subCategory);
				}
			}
			List<Product> products = new ArrayList<Product>();
			Product product = null;
			ProductPricing productPricing = new ProductPricing();
			productPricing.setPricingTitle(productBL.getImplementation()
					.getCompanyName().concat(" Pricing"));
			productPricing.setStartDate(Calendar.getInstance().getTime());
			LocalDate currentDate = DateTime.now().toLocalDate();
			currentDate = currentDate.plusYears(5);
			productPricing.setEndDate(currentDate.toDate());
			productPricing.setCurrency(productBL.getCurrencyService()
					.getDefaultCurrency(productBL.getImplementation()));
			productPricing
					.setIsApprove((byte) WorkflowConstants.Status.Published
							.getCode());
			productPricing.setImplementation(productBL.getImplementation());
			ProductPricingDetail pricingDetail = null;
			List<ProductPricingDetail> productPricingDetails = new ArrayList<ProductPricingDetail>();
			List<ProductPricingCalc> productPricingCalcs = new ArrayList<ProductPricingCalc>();
			// ProductPricingCalc productPricingCalc = null;
			List<Stock> stocks = new ArrayList<Stock>();
			Stock stock = null;
			for (ProductVO productVO : productCategoryVO.getProductVOs()) {
				product = new Product();
				pricingDetail = new ProductPricingDetail();
				// productPricingCalc = new ProductPricingCalc();
				BeanUtils.copyProperties(product, productVO);
				product.setProductId(null);
				subCategory = productCategoryMap
						.get(productVO.getSubCategory());
				product.setProductCategory(subCategory);
				pricingDetail.setProduct(product);
				pricingDetail.setBasicCostPrice(productVO.getBasePrice());
				pricingDetail.setStandardPrice(productVO.getStandardPrice());
				pricingDetail.setSuggestedRetailPrice(productVO
						.getSuggestedPrice());
				// pricingDetail
				// .setSellingPrice(pricingDetail.getBasicCostPrice() * 3);
				pricingDetail.setSellingPrice(productVO.getSellingPrice());
				pricingDetail.setStatus(true);
				pricingDetail.setProductPricing(productPricing);

				/*
				 * productPricingCalc.setStatus(true); productPricingCalc
				 * .setCalculationType(ProductCalculationType.Fixed_Price
				 * .getCode()); productPricingCalc
				 * .setCalculationSubType(ProductCalculationSubType.Amount
				 * .getCode()); productPricingCalc.setSalePrice(pricingDetail
				 * .getBasicCostPrice() * 3);
				 * productPricingCalc.setSalePriceAmount(productPricingCalc
				 * .getSalePrice()); productPricingCalc.setComboType(false);
				 * productPricingCalc.setIsDefault(true);
				 * productPricingCalc.setProductPricingDetail(pricingDetail);
				 * productPricingCalcs.add(productPricingCalc);
				 */
				productPricingDetails.add(pricingDetail);

				if (productVO.getQuantity() > 0) {
					stock = new Stock();
					stock.setShelf(productVO.getShelf());
					if (productVO.getShelf().getShelf() != null)
						stock.setStore(productVO.getShelf().getShelf()
								.getAisle().getStore());
					stock.setProduct(product);
					stock.setQuantity(productVO.getQuantity());
					stock.setUnitRate(productVO.getBasePrice());
					stock.setImplementation(productBL.getImplementation());
					stocks.add(stock);
				}
				product.setIsApprove((byte) WorkflowConstants.Status.Published
						.getCode());
				product.setLookupDetailByProductUnit(productVO
						.getLookupDetailByProductUnit());
				products.add(product);
			}
			productBL.saveProducts(productCategories, products, productPricing,
					productPricingDetails, productPricingCalcs, stocks);
			sqlReturnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: uploadProducts: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("Module: Accounts : Method: uploadProducts: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String returnProductImagePath() {

		imgPath = "none";
		final String PRODUCTS = "products";
		final String webappsImageFolder = "images/ordering-menu/company/";

		String finalImgPath = productBL.getImplementation().getCompanyKey()
				+ "/" + PRODUCTS + "/" + productCode + "_"
				+ requiredImageNumber + ".png";

		String webContentProductsImageFolder = ServletActionContext
				.getServletContext().getRealPath(
						webappsImageFolder + finalImgPath);

		if (new File(webContentProductsImageFolder).exists()) {
			imgPath = finalImgPath;
		} else {
			imgPath = "none";
		}

		return SUCCESS;
	}

	// Getters & Setters
	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public ProductBL getProductBL() {
		return productBL;
	}

	public void setProductBL(ProductBL productBL) {
		this.productBL = productBL;
	}

	public byte getCostingType() {
		return costingType;
	}

	public void setCostingType(byte costingType) {
		this.costingType = costingType;
	}

	public long getItemNature() {
		return itemNature;
	}

	public void setItemNature(long itemNature) {
		this.itemNature = itemNature;
	}

	public long getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(long productCategory) {
		this.productCategory = productCategory;
	}

	public Map<Byte, String> getItemSubTypes() {
		return itemSubTypes;
	}

	public void setItemSubTypes(Map<Byte, String> itemSubTypes) {
		this.itemSubTypes = itemSubTypes;
	}

	public byte getSubItemType() {
		return subItemType;
	}

	public void setSubItemType(byte subItemType) {
		this.subItemType = subItemType;
	}

	public boolean isReOrdering() {
		return reOrdering;
	}

	public void setReOrdering(boolean reOrdering) {
		this.reOrdering = reOrdering;
	}

	public int getMaxLeadTime() {
		return maxLeadTime;
	}

	public void setMaxLeadTime(int maxLeadTime) {
		this.maxLeadTime = maxLeadTime;
	}

	public int getMinLeadTime() {
		return minLeadTime;
	}

	public void setMinLeadTime(int minLeadTime) {
		this.minLeadTime = minLeadTime;
	}

	public int getMaxUsage() {
		return maxUsage;
	}

	public void setMaxUsage(int maxUsage) {
		this.maxUsage = maxUsage;
	}

	public int getMinUsage() {
		return minUsage;
	}

	public void setMinUsage(int minUsage) {
		this.minUsage = minUsage;
	}

	public double getReOrderQuantity() {
		return reOrderQuantity;
	}

	public void setReOrderQuantity(double reOrderQuantity) {
		this.reOrderQuantity = reOrderQuantity;
	}

	public String getFromHours() {
		return fromHours;
	}

	public void setFromHours(String fromHours) {
		this.fromHours = fromHours;
	}

	public String getToHours() {
		return toHours;
	}

	public void setToHours(String toHours) {
		this.toHours = toHours;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public Object getProductDetail() {
		return productDetail;
	}

	public void setProductDetail(Object productDetail) {
		this.productDetail = productDetail;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getInventoryAccount() {
		return inventoryAccount;
	}

	public void setInventoryAccount(long inventoryAccount) {
		this.inventoryAccount = inventoryAccount;
	}

	public long getExpenseAccount() {
		return expenseAccount;
	}

	public void setExpenseAccount(long expenseAccount) {
		this.expenseAccount = expenseAccount;
	}

	public boolean isSpecialPos() {
		return specialPos;
	}

	public void setSpecialPos(boolean specialPos) {
		this.specialPos = specialPos;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Integer getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Integer processFlag) {
		this.processFlag = processFlag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Integer getRequiredImageNumber() {
		return requiredImageNumber;
	}

	public void setRequiredImageNumber(Integer requiredImageNumber) {
		this.requiredImageNumber = requiredImageNumber;
	}

	public String getHsCode() {
		return hsCode;
	}

	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}
}
