package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackage;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPackageVO;
import com.aiotech.aios.accounts.service.bl.PackageConversionBL;
import com.aiotech.aios.accounts.service.bl.ProductPackageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.opensymphony.xwork2.ActionSupport;

public class ProductPackageAction extends ActionSupport {

	private static final long serialVersionUID = -2035101590724093017L;

	private static final Logger log = Logger
			.getLogger(ProductPackageAction.class);

	private long productPackageId;
	private long productPackageDetailId;
	private long productId;
	private long recordId;
	private String packageName;
	private double length;
	private double width;
	private double height;
	private String referenceNumber;
	private String barcode;
	private double netWeight;
	private double grossWeight;
	private double packageQuantity;
	private double basePrice;
	private String packagingDetails;
	private String returnMessage;
	private Integer rowId;
	private List<Object> aaData;
	private List<ProductPackageVO> productPackageVOs;
	private ProductPackageDetailVO productPackagingDetailVO;
	private ProductPackageBL productPackageBL;
	private PackageConversionBL packageConversionBL;

	public String returnSuccess() {
		log.info("Inside Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String showAllJsonProductPackaging() {
		try {
			log.info("Inside Module: Accounts : Method: showAllJsonProductPackaging");
			aaData = (List<Object>) productPackageBL.getAllProductPackagings();
			log.info("Module: Accounts : Method: showAllJsonProductPackaging: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllJsonProductPackaging Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductPackageEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductPackageEntry");
			ProductPackageVO productPackageVO = null;
			if (productPackageId > 0) {
				ProductPackage productPackage = productPackageBL
						.getProductPackageService().getProductPackagingbyId(
								productPackageId);
				productPackageVO = new ProductPackageVO();
				BeanUtils.copyProperties(productPackageVO, productPackage);
				List<ProductPackageDetailVO> productPackageDetailVOs = new ArrayList<ProductPackageDetailVO>();
				ProductPackageDetailVO productPackageDetailVO = null;
				for (ProductPackageDetail productPackageDetail : productPackage
						.getProductPackageDetails()) {
					productPackageDetailVO = new ProductPackageDetailVO();
					BeanUtils.copyProperties(productPackageDetailVO,
							productPackageDetail);
					productPackageDetailVOs.add(productPackageDetailVO);
				}
				Collections.sort(productPackageDetailVOs,
						new Comparator<ProductPackageDetailVO>() {
							public int compare(ProductPackageDetailVO o1,
									ProductPackageDetailVO o2) {
								return o1.getProductPackageDetailId()
										.compareTo(
												o2.getProductPackageDetailId());
							}
						});
				productPackageVO
						.setProductPackageDetailVOs(productPackageDetailVOs);
			} else
				referenceNumber = SystemBL.getReferenceStamp(
						ProductPackage.class.getName(),
						productPackageBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("PACKAGE_INFO",
					productPackageVO);
			List<LookupDetail> productUnits = productPackageBL
					.getLookupMasterBL().getActiveLookupDetails(
							"PRODUCT_UNITNAME",
							productPackageBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("PRODUCT_UNITS",
					productUnits);
			log.info("Module: Accounts : Method: showProductPackageEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductPackageEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showProductPackageAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showProductPackageAddRow");
			List<LookupDetail> productUnits = productPackageBL
					.getLookupMasterBL().getActiveLookupDetails(
							"PRODUCT_UNITNAME",
							productPackageBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("PRODUCT_UNITS",
					productUnits);
			log.info("Module: Accounts : Method: showProductPackageAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductPackageAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveProductPackage() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductPackage");
			ProductPackage productPackage = new ProductPackage();
			Product product = new Product();
			product.setProductId(productId);
			productPackage.setProduct(product);
			productPackage.setBarcode(barcode);
			productPackage.setGrossWeight(grossWeight > 0 ? grossWeight : null);
			productPackage.setNetWeight(netWeight > 0 ? netWeight : null);
			productPackage.setHeight(height > 0 ? height : null);
			productPackage.setWidth(width > 0 ? width : null);
			productPackage.setLength(length > 0 ? length : null);
			productPackage.setPackageName(packageName);

			JSONParser parser = new JSONParser();
			Object packageObject = parser.parse(packagingDetails);
			JSONArray object = (JSONArray) packageObject;
			ProductPackageDetail productPackageDetail = null;
			LookupDetail lookupDetail = null;
			List<ProductPackageDetail> productPackageDetailList = new ArrayList<ProductPackageDetail>();
			List<ProductPackageDetail> deletedPackageDetails = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("packagingDetails");
				for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
						.hasNext();) {
					JSONObject jsonObjectDetail = (JSONObject) iteratorObject
							.next();
					productPackageDetail = new ProductPackageDetail();
					lookupDetail = new LookupDetail();
					lookupDetail.setLookupDetailId(Long
							.parseLong(jsonObjectDetail.get("productUnitId")
									.toString()));
					productPackageDetail.setLookupDetail(lookupDetail);
					productPackageDetail.setUnitQuantity(Double
							.parseDouble(jsonObjectDetail.get("quantity")
									.toString()));
					productPackageDetail
							.setProductPackageDetailId((null != jsonObjectDetail
									.get("productPackageDetailId") && Long
									.parseLong(jsonObjectDetail.get(
											"productPackageDetailId")
											.toString()) > 0) ? Long
									.parseLong(jsonObjectDetail.get(
											"productPackageDetailId")
											.toString()) : null);
				}
				productPackageDetailList.add(productPackageDetail);
			}
			if (productPackageId > 0) {
				productPackage.setProductPackageId(productPackageId);
				productPackage.setReferenceNumber(referenceNumber);
				deletedPackageDetails = getUserDeletedPackageDetails(productPackageDetailList);
			} else
				productPackage.setReferenceNumber(SystemBL.getReferenceStamp(
						ProductPackage.class.getName(),
						productPackageBL.getImplementation()));
			productPackageBL.saveProductPackage(productPackage,
					productPackageDetailList, deletedPackageDetails);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductPackage: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveProductPackage Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteProductPackage() {
		try {
			log.info("Inside Module: Accounts : Method: deleteProductPackage");
			ProductPackage productPackage = productPackageBL
					.getProductPackageService().getProductPackagingbyId(
							productPackageId);
			productPackageBL.deleteProductPackage(productPackage);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProductPackage: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteProductPackage Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<ProductPackageDetail> getUserDeletedPackageDetails(
			List<ProductPackageDetail> productPackageDetailList)
			throws Exception {
		List<ProductPackageDetail> existingProductPackageDetails = productPackageBL
				.getProductPackageService().getProductPackageDetailsById(
						productPackageId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, ProductPackageDetail> hashProductPackageDetail = new HashMap<Long, ProductPackageDetail>();
		if (null != existingProductPackageDetails
				&& existingProductPackageDetails.size() > 0) {
			for (ProductPackageDetail list : existingProductPackageDetails) {
				listOne.add(list.getProductPackageDetailId());
				hashProductPackageDetail.put(list.getProductPackageDetailId(),
						list);
			}
			if (null != hashProductPackageDetail
					&& hashProductPackageDetail.size() > 0) {
				for (ProductPackageDetail list : productPackageDetailList) {
					listTwo.add(list.getProductPackageDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<ProductPackageDetail> deletedProductPackageDetails = null;
		if (null != different && different.size() > 0) {
			ProductPackageDetail tempProductPackageDetail = null;
			deletedProductPackageDetails = new ArrayList<ProductPackageDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempProductPackageDetail = new ProductPackageDetail();
					tempProductPackageDetail = hashProductPackageDetail
							.get(list);
					deletedProductPackageDetails.add(tempProductPackageDetail);
				}
			}
		}
		return deletedProductPackageDetails;
	}

	public String getProductPackagingDetail() {
		try {
			log.info("Inside Module: Accounts : Method: getProductPackagingDetail");
			productPackageVOs = packageConversionBL
					.getProductPackagingDetail(productId);
			log.info("Module: Accounts : Method: getProductPackagingDetail: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: getProductPackagingDetail Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showBaseConversionUnit() {
		try {
			log.info("Inside Module: Accounts : Method: showBaseConversionUnit");
			productPackagingDetailVO = packageConversionBL
					.getBaseConversionUnit(productPackageDetailId,
							packageQuantity, basePrice);
			log.info("Module: Accounts : Method: showBaseConversionUnit: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showBaseConversionUnit Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showPackageConversionUnit() {
		try {
			log.info("Inside Module: Accounts : Method: showPackageConversionUnit");
			productPackagingDetailVO = packageConversionBL
					.getPackageConversionUnit(productPackageDetailId,
							packageQuantity);
			log.info("Module: Accounts : Method: showPackageConversionUnit: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showPackageConversionUnit Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public long getProductPackageId() {
		return productPackageId;
	}

	public void setProductPackageId(long productPackageId) {
		this.productPackageId = productPackageId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(double netWeight) {
		this.netWeight = netWeight;
	}

	public double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public String getPackagingDetails() {
		return packagingDetails;
	}

	public void setPackagingDetails(String packagingDetails) {
		this.packagingDetails = packagingDetails;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public ProductPackageBL getProductPackageBL() {
		return productPackageBL;
	}

	public void setProductPackageBL(ProductPackageBL productPackageBL) {
		this.productPackageBL = productPackageBL;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public long getProductPackageDetailId() {
		return productPackageDetailId;
	}

	public void setProductPackageDetailId(long productPackageDetailId) {
		this.productPackageDetailId = productPackageDetailId;
	}

	public double getPackageQuantity() {
		return packageQuantity;
	}

	public void setPackageQuantity(double packageQuantity) {
		this.packageQuantity = packageQuantity;
	}

	public PackageConversionBL getPackageConversionBL() {
		return packageConversionBL;
	}

	public void setPackageConversionBL(PackageConversionBL packageConversionBL) {
		this.packageConversionBL = packageConversionBL;
	}

	public ProductPackageDetailVO getProductPackagingDetailVO() {
		return productPackagingDetailVO;
	}

	public void setProductPackagingDetailVO(
			ProductPackageDetailVO productPackagingDetailVO) {
		this.productPackagingDetailVO = productPackagingDetailVO;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
}
