package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AssetServiceCharge;
import com.aiotech.aios.accounts.domain.entity.AssetServiceDetail;
import com.aiotech.aios.accounts.domain.entity.vo.AssetServiceChargeVO;
import com.aiotech.aios.accounts.service.bl.AssetServiceChargeBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class AssetServiceChargeAction extends ActionSupport {

	private static final long serialVersionUID = 5498034781412544693L;

	private static final Logger log = Logger
			.getLogger(AssetServiceChargeAction.class);

	private AssetServiceChargeBL assetServiceChargeBL;
	private List<Object> aaData;

	private long assetServiceChargeId;
	private long assetServiceDetailId;
	private Long recordId;
	private String returnMessage;
	private String serviceCharges;
	private String page;
	private Integer rowId;

	public String returnSuccess() {
		log.info("Inside Method: returnSuccess()");
		return SUCCESS;
	}

	// Show Asset Service Charge list
	public String showServiceChargeList() {
		try {
			log.info("Inside Module: Accounts : Method: showserviceChargeList");
			aaData = assetServiceChargeBL.showServiceChargeList();
			log.info("Module: Accounts : Method: showserviceChargeList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showserviceChargeList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetServiceChargeApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceChargeApproval");
			assetServiceChargeId = recordId;
			AssetServiceCharge assetServiceCharge = assetServiceChargeBL
					.getAssetServiceChargeService()
					.getAssetServiceChargeByChargeId(assetServiceChargeId);
			assetServiceDetailId = assetServiceCharge.getAssetServiceDetail()
					.getAssetServiceDetailId();
			showServiceChargeEntry();
			log.info("Module: Accounts : Method: showAssetServiceChargeApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceChargeApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showAssetServiceChargeReject() {
		try {
			log.info("Inside Module: Accounts : Method: showAssetServiceChargeReject");
			assetServiceChargeId = recordId;
			AssetServiceCharge assetServiceCharge = assetServiceChargeBL
					.getAssetServiceChargeService()
					.getAssetServiceChargeByChargeId(assetServiceChargeId);
			assetServiceDetailId = assetServiceCharge.getAssetServiceDetail()
					.getAssetServiceDetailId();
			showServiceChargeEntry();
			log.info("Module: Accounts : Method: showAssetServiceChargeReject: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAssetServiceChargeReject Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Asset Service Charge Screen Entry
	public String showServiceChargeEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showServiceChargeEntry");
			if (assetServiceDetailId > 0) {
				List<AssetServiceCharge> assetServiceCharges = assetServiceChargeBL
						.getAssetServiceChargeService()
						.getAssetServiceChargeById(assetServiceDetailId);
				AssetServiceChargeVO assetServiceChargeVO = convertChargesEntity(assetServiceCharges);
				ServletActionContext.getRequest().setAttribute(
						"SERVICE_CHARGE", assetServiceChargeVO);
				page = "edit";
				CommentVO comment = new CommentVO();
				if (recordId != null && recordId > 0) {
					AssetServiceCharge assetServiceCharge = assetServiceChargeBL
							.getAssetServiceChargeService()
							.getAssetServiceChargeByChargeId(recordId);
					comment.setRecordId(assetServiceCharge
							.getAssetServiceChargeId());
					comment.setUseCase(AssetServiceCharge.class.getName());
					comment = assetServiceChargeBL.getCommentBL()
							.getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else
				page = "add";
			List<LookupDetail> costTypes = assetServiceChargeBL
					.getAssetServiceBL().getLookupMasterBL()
					.getActiveLookupDetails("ASSET_SERVICE_COST_TYPE", true);

			ServletActionContext.getRequest().setAttribute("COST_TYPES",
					costTypes);
			log.info("Module: Accounts : Method: showServiceChargeEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showServiceChargeEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Asset Service Charge
	public String saveAssetServiceCharge() {
		try {
			log.info("Inside Module: Accounts : Method: showServiceChargeEntry");
			List<AssetServiceCharge> assetServiceCharges = new ArrayList<AssetServiceCharge>();
			String[] serviceCharge = splitValues(serviceCharges, "#@");
			for (String charge : serviceCharge) {
				assetServiceCharges.add(addAssetServiceCharges(charge));
			}
			List<AssetServiceCharge> deleteAssetServiceCharges = null;
			if (page.equalsIgnoreCase("edit")) {
				deleteAssetServiceCharges = userDeletedAssetServiceCharges(
						assetServiceCharges,
						assetServiceChargeBL.getAssetServiceChargeService()
								.getAssetServiceChargeByDetailId(
										assetServiceDetailId));
			}
			AssetServiceDetail assetServiceDetail = new AssetServiceDetail();
			assetServiceDetail.setAssetServiceDetailId(assetServiceDetailId);
			for (AssetServiceCharge assetServiceCharge : assetServiceCharges) {
				assetServiceCharge.setAssetServiceDetail(assetServiceDetail);
			}
			assetServiceChargeBL.saveAssetServiceCharge(assetServiceCharges,
					deleteAssetServiceCharges);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: showServiceChargeEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Save failure";
			log.info("Module: Accounts : Method: saveAssetServiceCharge Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Asset Service Charge
	public String deleteAssetServiceCharge() {
		try {
			log.info("Inside Module: Accounts : Method: showServiceChargeEntry");
			List<AssetServiceCharge> assetServiceCharges = assetServiceChargeBL
					.getAssetServiceChargeService().getAssetServiceChargeById(
							assetServiceDetailId);
			assetServiceChargeBL.deleteAssetServiceCharge(assetServiceCharges);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: showServiceChargeEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Delete failure";
			log.info("Module: Accounts : Method: saveAssetServiceCharge Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Add Asset Service Charge
	private AssetServiceCharge addAssetServiceCharges(String serviceCharges) {
		AssetServiceCharge assetServiceCharge = new AssetServiceCharge();
		String serviceCharge[] = splitValues(serviceCharges, "__");
		LookupDetail lookupDetail = new LookupDetail();
		lookupDetail.setLookupDetailId(Long.parseLong(serviceCharge[0]));
		assetServiceCharge.setLookupDetail(lookupDetail);
		assetServiceCharge.setAmount(Double.parseDouble(serviceCharge[1]));
		if (null != serviceCharge[2] && !("##").equals(serviceCharge[2]))
			assetServiceCharge.setDescription(serviceCharge[2]);
		if (Long.parseLong(serviceCharge[3]) > 0l) {
			assetServiceCharge.setAssetServiceChargeId(Long
					.parseLong(serviceCharge[3]));
		}
		return assetServiceCharge;
	}

	// Get user deleted Asset Service Details
	private List<AssetServiceCharge> userDeletedAssetServiceCharges(
			List<AssetServiceCharge> assetServiceCharges,
			List<AssetServiceCharge> assetServiceChargesOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, AssetServiceCharge> hashAssetDetails = new HashMap<Long, AssetServiceCharge>();
		if (null != assetServiceChargesOld && assetServiceChargesOld.size() > 0) {
			for (AssetServiceCharge list : assetServiceChargesOld) {
				listOne.add(list.getAssetServiceChargeId());
				hashAssetDetails.put(list.getAssetServiceChargeId(), list);
			}
			for (AssetServiceCharge list : assetServiceCharges) {
				listTwo.add(list.getAssetServiceChargeId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<AssetServiceCharge> assetServiceChargesFinal = new ArrayList<AssetServiceCharge>();
		if (null != different && different.size() > 0) {
			AssetServiceCharge tempAssetServiceCharge = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempAssetServiceCharge = new AssetServiceCharge();
					tempAssetServiceCharge = hashAssetDetails.get(list);
					assetServiceChargesFinal.add(tempAssetServiceCharge);
				}
			}
		}
		return assetServiceChargesFinal;
	}

	private AssetServiceChargeVO convertChargesEntity(
			List<AssetServiceCharge> assetServiceCharges) {
		AssetServiceChargeVO assetServiceChargeVO = new AssetServiceChargeVO();
		assetServiceChargeVO.setAssetName(assetServiceCharges.get(0)
				.getAssetServiceDetail().getAssetService().getAssetCreation()
				.getAssetName());
		assetServiceChargeVO.setServiceNumber(assetServiceCharges.get(0)
				.getAssetServiceDetail().getAssetService().getServiceNumber());
		assetServiceChargeVO.setServiceDate(DateFormat
				.convertDateToString(assetServiceCharges.get(0)
						.getAssetServiceDetail().getServiceDate().toString()));
		assetServiceChargeVO.setDescription(assetServiceCharges.get(0)
				.getAssetServiceDetail().getDescription());
		assetServiceChargeVO.setAssetServiceDetailId(assetServiceCharges.get(0)
				.getAssetServiceDetail().getAssetServiceDetailId());
		assetServiceChargeVO.setAssetServiceCharges(assetServiceCharges);
		return assetServiceChargeVO;
	}

	// Add Row
	public String assetServiceChargeAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: assetServiceChargeAddRow");
			List<LookupDetail> costTypes = assetServiceChargeBL
					.getAssetServiceBL().getLookupMasterBL()
					.getActiveLookupDetails("ASSET_SERVICE_COST_TYPE", true);
			ServletActionContext.getRequest().setAttribute("COST_TYPES",
					costTypes);
			ServletActionContext.getRequest().setAttribute("ROW_ID", rowId);
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: assetServiceChargeAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}

	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters & Setters
	public AssetServiceChargeBL getAssetServiceChargeBL() {
		return assetServiceChargeBL;
	}

	public void setAssetServiceChargeBL(
			AssetServiceChargeBL assetServiceChargeBL) {
		this.assetServiceChargeBL = assetServiceChargeBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getAssetServiceChargeId() {
		return assetServiceChargeId;
	}

	public void setAssetServiceChargeId(long assetServiceChargeId) {
		this.assetServiceChargeId = assetServiceChargeId;
	}

	public long getAssetServiceDetailId() {
		return assetServiceDetailId;
	}

	public void setAssetServiceDetailId(long assetServiceDetailId) {
		this.assetServiceDetailId = assetServiceDetailId;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getServiceCharges() {
		return serviceCharges;
	}

	public void setServiceCharges(String serviceCharges) {
		this.serviceCharges = serviceCharges;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
