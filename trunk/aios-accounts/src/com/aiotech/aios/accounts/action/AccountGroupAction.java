package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AccountGroupDetail;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.vo.AccountGroupVO;
import com.aiotech.aios.accounts.service.bl.AccountGroupBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionSupport;

public class AccountGroupAction extends ActionSupport {

	private static final long serialVersionUID = 9130282613188403642L;

	private static final Logger log = Logger
			.getLogger(AccountGroupAction.class);

	private long accountGroupId;
	private int rowId;
	private String referenceNumber;
	private String wastageDate;
	private String description;
	private String returnMessage;
	private String groupDetails;
	private String code;
	private int accountType;
	private String title;
	private Long combinationId;
	private List<Object> aaData;

	private AccountGroupBL accountGroupBL;

	public String returnSuccess() {
		log.info("Module: Accounts : Method: returnSuccess");
		return SUCCESS;
	}

	public String showAllAccountGroups() {
		try {
			log.info("Inside Module: Accounts : Method: showAllAccountGroups");
			aaData = accountGroupBL.getAllAccountGroups();
			log.info("Module: Accounts : Method: showAllAccountGroups: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllAccountGroups Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// entry
	public String showAccountGroupEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showAccountGroupEntry");
			AccountGroupVO accountGroupVO = null;
			List<AccountGroupVO> details = null;
			if (accountGroupId > 0) {
				AccountGroup accountGroup = accountGroupBL
						.getAccountGroupService().getAccountGroupbyId(
								accountGroupId);
				accountGroupVO = new AccountGroupVO();
				BeanUtils.copyProperties(accountGroupVO, accountGroup);
				details = accountGroupBL
						.detailsConversion(new ArrayList<AccountGroupDetail>(
								accountGroup.getAccountGroupDetails()));

			} else {
				accountGroupVO = new AccountGroupVO();
				String value = SystemBL.getReferenceStamp(
						AccountGroup.class.getName(),
						accountGroupBL.getImplementation());
				accountGroupVO.setCode(value);
			}
			ServletActionContext.getRequest().setAttribute("ACCOUNT_GROUP",
					accountGroupVO);
			ServletActionContext.getRequest().setAttribute(
					"ACCOUNT_GROUP_DETAIL", details);
			log.info("Module: Accounts : Method: showAccountGroupEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAccountGroupEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveAccountGroup() {
		try {
			log.info("Inside Module: Accounts : Method: saveAccountGroup");
			AccountGroup accountGroup = new AccountGroup();
			accountGroup.setDescription(description);
			accountGroup.setTitle(title);
			accountGroup.setAccountType(accountType);
			JSONParser parser = new JSONParser();
			Object receiveObject = parser.parse(groupDetails);
			JSONArray object = (JSONArray) receiveObject;
			AccountGroupDetail accountGroupDetail = null;
			Combination combination = null;
			List<AccountGroupDetail> accountGroupDetails = new ArrayList<AccountGroupDetail>();
			List<AccountGroupDetail> deletedAccountGroupDetails = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				accountGroupDetail = new AccountGroupDetail();
				combination = new Combination();
				combination.setCombinationId(Long.parseLong(jsonObject.get(
						"combinationId").toString()));
				accountGroupDetail.setCombination(combination);
				if (jsonObject.get("accountGroupDetailId").toString() != null
						&& Long.parseLong(jsonObject
								.get("accountGroupDetailId").toString()) > 0)
					accountGroupDetail.setAccountGroupDetailId(Long
							.parseLong(jsonObject.get("accountGroupDetailId")
									.toString()));

				accountGroupDetails.add(accountGroupDetail);
			}
			List<AccountGroupDetail> existingAccountGroups = null;
			if (accountGroupId > 0) {
				AccountGroup accountGroupTemp = accountGroupBL
						.getAccountGroupService().getAccountGroupbyId(
								accountGroupId);
				existingAccountGroups = accountGroupBL.getAccountGroupService()
						.getAccountGroupDetails(accountGroupId);
				deletedAccountGroupDetails = getUserDeletedAccountGroups(
						existingAccountGroups, accountGroupDetails);
				accountGroup.setCode(accountGroupTemp.getCode());
				accountGroup.setAccountGroupId(accountGroupId);
			} else {
				accountGroup.setCode(code);
			}
			accountGroupBL.saveAccountGroup(accountGroup, accountGroupDetails,
					deletedAccountGroupDetails, existingAccountGroups);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveAccountGroup: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: saveAccountGroup Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteAccountGroup() {
		try {
			log.info("Inside Module: Accounts : Method: deleteAccountGroup");
			AccountGroup accountGroup = accountGroupBL.getAccountGroupService()
					.getAccountGroupbyId(accountGroupId);
			accountGroupBL.deleteAccountGroup(
					accountGroup,
					new ArrayList<AccountGroupDetail>(accountGroup
							.getAccountGroupDetails()));
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteAccountGroup: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: deleteAccountGroup Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String validateAccountType() {
		try {
			log.info("Inside Module: Accounts : Method: validateAccountType");
			Combination combination = accountGroupBL.getCombinationBL()
					.getCombinationService()
					.getCombinationAccounts(combinationId);
			Account account = null;
			if (combination.getAccountByBuffer1AccountId() != null)
				account = combination.getAccountByBuffer1AccountId();
			else if (combination.getAccountByAnalysisAccountId() != null)
				account = combination.getAccountByAnalysisAccountId();
			else if (combination.getAccountByNaturalAccountId() != null)
				account = combination.getAccountByNaturalAccountId();
			else if (combination.getAccountByCostcenterAccountId() != null)
				account = combination.getAccountByCostcenterAccountId();

			if (account.getAccountType() != null
					&& (int) account.getAccountType() == (int) accountType)
				returnMessage = "SUCCESS";
			else
				returnMessage = "ERROR";
			log.info("Module: Accounts : Method: validateAccountType: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: validateAccountType Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<AccountGroupDetail> getUserDeletedAccountGroups(
			List<AccountGroupDetail> existingAccountGroupDetails,
			List<AccountGroupDetail> accountGroupDetails) throws Exception {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, AccountGroupDetail> hashPurchaseDetail = new HashMap<Long, AccountGroupDetail>();
		if (null != existingAccountGroupDetails
				&& existingAccountGroupDetails.size() > 0) {
			for (AccountGroupDetail list : existingAccountGroupDetails) {
				listOne.add(list.getAccountGroupDetailId());
				hashPurchaseDetail.put(list.getAccountGroupDetailId(), list);
			}
			if (null != hashPurchaseDetail && hashPurchaseDetail.size() > 0) {
				for (AccountGroupDetail list : accountGroupDetails) {
					listTwo.add(list.getAccountGroupDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<AccountGroupDetail> deletedAccountGroupDetails = null;
		if (null != different && different.size() > 0) {
			AccountGroupDetail tempAccountGroupDetail = null;
			deletedAccountGroupDetails = new ArrayList<AccountGroupDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempAccountGroupDetail = new AccountGroupDetail();
					tempAccountGroupDetail = hashPurchaseDetail.get(list);
					deletedAccountGroupDetails.add(tempAccountGroupDetail);
				}
			}
		}
		return deletedAccountGroupDetails;
	}

	public String showAccountGroupAddRow() {
		try {
			log.info("Inside Module: Accounts : Method: showAccountGroupAddRow");

			log.info("Module: Accounts : Method: showAccountGroupAddRow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAccountGroupAddRow Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getWastageDate() {
		return wastageDate;
	}

	public void setWastageDate(String wastageDate) {
		this.wastageDate = wastageDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getAccountGroupId() {
		return accountGroupId;
	}

	public void setAccountGroupId(long accountGroupId) {
		this.accountGroupId = accountGroupId;
	}

	public String getGroupDetails() {
		return groupDetails;
	}

	public void setGroupDetails(String groupDetails) {
		this.groupDetails = groupDetails;
	}

	public AccountGroupBL getAccountGroupBL() {
		return accountGroupBL;
	}

	public void setAccountGroupBL(AccountGroupBL accountGroupBL) {
		this.accountGroupBL = accountGroupBL;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}
}
