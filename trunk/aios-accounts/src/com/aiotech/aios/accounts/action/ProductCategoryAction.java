package com.aiotech.aios.accounts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryVO;
import com.aiotech.aios.accounts.service.bl.ProductCategoryBL;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ProductCategoryAction extends ActionSupport {

	private static final long serialVersionUID = 1787407270209655426L;

	private static final Logger log = Logger
			.getLogger(ProductCategoryAction.class);

	private ProductCategoryBL productCategoryBL;

	private long productCategoryId;
	private long parentCategory;
	private String categoryName;
	private String description;
	private String returnMessage;
	private boolean specialPos;
	private boolean kitchenPrint;
	private boolean barPrint;
	private boolean activeStatus;
	private String publicName;
 	private Long recordId;
	private ProductCategoryVO productCategoryVO;
	private List<Object> aaData;

	public String returnSuccess() {
		try {
			log.info("Inside Module: Accounts : Method: returnSuccess");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ProductCategoryPicture_"
					+ sessionObj.get("jSessionId"));
			recordId = null;
			log.info("Module: Accounts : Method: returnSuccess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: returnSuccess Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all Product Categories
	public String showAllProductCategory() {
		try {
			log.info("Inside Module: Accounts : Method: showAllProductCategory");
			aaData = productCategoryBL.showAllProductCategory();
			log.info("Module: Accounts : Method: showAllProductCategory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllProductCategory Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showProductCategoryApproval() {
		try {
			log.info("Inside Module: Accounts : Method: showProductCategoryApproval");
			productCategoryId = recordId;
			showUpdateProductCategory();
			log.info("Module: Accounts : Method: showProductCategoryApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductCategoryApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showProductCategoryRejectEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showProductCategoryRejectEntry");
			productCategoryId = recordId;
			showUpdateProductCategory();
			log.info("Module: Accounts : Method: showProductCategoryRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showProductCategoryRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Product Category update
	public String showUpdateProductCategory() {
		try {
			log.info("Inside Module: Accounts : Method: showUpdateProductCategory"); 
			if (productCategoryId > 0) {
				ProductCategoryVO productCategoryVO = productCategoryBL
						.showUpdateProductCategory(productCategoryBL
								.getProductCategoryService()
								.getProductCategoryDetails(productCategoryId));
				ServletActionContext.getRequest().setAttribute(
						"PRODUCT_CATEGORY", productCategoryVO); 
				CommentVO comment = new CommentVO();
				if (recordId != null && productCategoryVO != null
						&& productCategoryVO.getProductCategoryId() != 0 && recordId > 0) {
					comment.setRecordId(productCategoryVO.getProductCategoryId());
					comment.setUseCase(ProductCategory.class.getName());
					comment = productCategoryBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			List<ProductCategoryVO> productCategories = productCategoryBL
					.getProductCategory();
			ServletActionContext.getRequest().setAttribute("PARENT_CATEGORY",
					productCategories); 
			log.info("Module: Accounts : Method: showUpdateProductCategory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showUpdateProductCategory Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Product Category
	public String saveProductCategory() {
		try {
			log.info("Inside Module: Accounts : Method: saveProductCategory"); 
			ProductCategory productCategory = new ProductCategory();
			productCategory.setCategoryName(categoryName);
			if (parentCategory > 0) {
				ProductCategory parentProductCategory = new ProductCategory();
				parentProductCategory.setProductCategoryId(parentCategory);
				productCategory.setProductCategory(parentProductCategory);
			}
			productCategory
					.setProductCategoryId(productCategoryId > 0 ? productCategoryId
							: null);
			productCategory.setStatus(activeStatus);
			productCategory.setSpecialPos(specialPos);
			productCategory.setPublicName(publicName);
			if (specialPos) { 	 
				productCategory.setKitchenPrint(kitchenPrint);
				productCategory.setBarPrint(barPrint);
			}
			productCategory.setDescription(description);
			productCategoryBL.saveProductCategory(productCategory); 
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveProductCategory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: saveProductCategory Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Product Category
	public String deleteProductCategory() {
		try {
			ProductCategory productCategory = productCategoryBL
					.getProductCategoryService().getProductCategoryDetails(
							productCategoryId);
			if ((null == productCategory.getProductCategory() && (null != productCategory
					.getProductCategories() && productCategory
					.getProductCategories().size() > 0))
					|| (null != productCategory.getProducts() && productCategory
							.getProducts().size() > 0)) {
				returnMessage = "Category in use, can't be delete.";
				return SUCCESS;
			} else
				productCategoryBL.deleteProductCategory(productCategory);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteProductCategory: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			log.info("Module: Accounts : Method: deleteProductCategory Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public ProductCategoryBL getProductCategoryBL() {
		return productCategoryBL;
	}

	public void setProductCategoryBL(ProductCategoryBL productCategoryBL) {
		this.productCategoryBL = productCategoryBL;
	}

	public long getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public long getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(long parentCategory) {
		this.parentCategory = parentCategory;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public ProductCategoryVO getProductCategoryVO() {
		return productCategoryVO;
	}

	public void setProductCategoryVO(ProductCategoryVO productCategoryVO) {
		this.productCategoryVO = productCategoryVO;
	}

	public boolean isSpecialPos() {
		return specialPos;
	}

	public void setSpecialPos(boolean specialPos) {
		this.specialPos = specialPos;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public boolean isKitchenPrint() {
		return kitchenPrint;
	}

	public void setKitchenPrint(boolean kitchenPrint) {
		this.kitchenPrint = kitchenPrint;
	}

	public boolean isBarPrint() {
		return barPrint;
	}

	public void setBarPrint(boolean barPrint) {
		this.barPrint = barPrint;
	}

	public boolean isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
