package com.aiotech.aios.accounts.action;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.domain.entity.RewardPolicy;
import com.aiotech.aios.accounts.service.bl.RewardPolicyBL;
import com.aiotech.aios.common.to.Constants.Accounts.RewardType;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class RewardPolicyAction extends ActionSupport {

	private static final long serialVersionUID = -1588028764331080222L;

	private static final Logger log = Logger
			.getLogger(RewardPolicyAction.class);

	// Dependency
	private RewardPolicyBL rewardPolicyBL;

	// Variables
	private long rewardPolicyId;
	private long couponId;
	private Long recordId;
	private byte rewardType;
	private double reward;
	private String policyName;
	private String percentage;
	private String description;
	private String returnMessage;
	private List<Object> aaData;
	private Object rewardPolicyVO;

	// return success
	public String returnSuccess() {
		try {
			log.info("Inside Module: Accounts : Method: returnSuccess");
			Map<Byte, RewardType> rewardTypes = new HashMap<Byte, RewardType>();
			for (RewardType e : EnumSet.allOf(RewardType.class)) {
				rewardTypes.put(e.getCode(), e);
			}
			CommentVO comment = new CommentVO();
			if (recordId != null && recordId > 0 && rewardPolicyId > 0) {
				RewardPolicy rewardPolicy = rewardPolicyBL.getRewardPolicyService()
				.getRewardPolicyByPolicyId(rewardPolicyId);
				comment.setRecordId(rewardPolicy.getRewardPolicyId());
				comment.setUseCase(RewardPolicy.class.getName());
				comment = rewardPolicyBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}else{
				rewardPolicyId = 0l;
			}
			recordId = null;
			ServletActionContext.getRequest().setAttribute("REWARD_TYPES",
					rewardTypes);
			log.info("Module: Accounts : Method: returnSuccess: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: returnSuccess Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}
	
	public String showRewardPolicyApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showRewardPolicyApproval");
			rewardPolicyId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showRewardPolicyApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showRewardPolicyApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showRewardPolicyRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showRewardPolicyRejectEntry");
			rewardPolicyId = recordId;
			returnSuccess();
			LOG.info("Module: Accounts : Method: showRewardPolicyRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showRewardPolicyRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show all reward policy json data
	public String showAllRewardPolicy() {
		try {
			log.info("Inside Module: Accounts : Method: showAllRewardPolicy");
			aaData = rewardPolicyBL.showAllRewardPolicy();
			recordId = null;
			log.info("Module: Accounts : Method: showAllRewardPolicy: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showAllRewardPolicy Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Reward Policy entry screen
	public String showRewardPolicyEntry() {
		try {
			log.info("Inside Module: Accounts : Method: showRewardPolicyEntry");
			RewardPolicy rewardPolicy = rewardPolicyBL.getRewardPolicyService()
					.getRewardPolicyByPolicyId(rewardPolicyId);
			rewardPolicyVO = rewardPolicyBL.addRewardPolicyVO(rewardPolicy);
			log.info("Module: Accounts : Method: showRewardPolicyEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			log.info("Module: Accounts : Method: showRewardPolicyEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Reward Policy
	public String saveRewardPolicy() {
		try {
			log.info("Inside Module: Accounts : Method: saveRewardPolicy");
			RewardPolicy rewardPolicy = new RewardPolicy();
			rewardPolicy.setRewardPolicyId(rewardPolicyId > 0 ? rewardPolicyId
					: null);
			rewardPolicy.setRewardType(rewardType);
			rewardPolicy.setPolicyName(policyName);
			rewardPolicy.setIsPercentage(null != percentage
					&& !("false").equalsIgnoreCase(percentage) ? true : false);
			rewardPolicy.setReward(reward);
			rewardPolicy.setDescription(description);
			Coupon coupon = new Coupon();
			coupon.setCouponId(couponId);
			rewardPolicy.setCoupon(coupon);
			rewardPolicyBL.saveRewardPolicy(rewardPolicy);
			rewardPolicyId = 0l;
			recordId = null;
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: saveRewardPolicy: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: saveRewardPolicy Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Delete Reward Policy
	public String deleteRewardPolicy() {
		try {
			log.info("Inside Module: Accounts : Method: deleteRewardPolicy");
			RewardPolicy rewardPolicy = rewardPolicyBL.getRewardPolicyService()
					.getRewardPolicyByPolicyId(rewardPolicyId);
			rewardPolicyBL.deleteRewardPolicy(rewardPolicy);
			returnMessage = "SUCCESS";
			log.info("Module: Accounts : Method: deleteRewardPolicy: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "FAILURE";
			log.info("Module: Accounts : Method: deleteRewardPolicy Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getters & Setters
	public RewardPolicyBL getRewardPolicyBL() {
		return rewardPolicyBL;
	}

	public void setRewardPolicyBL(RewardPolicyBL rewardPolicyBL) {
		this.rewardPolicyBL = rewardPolicyBL;
	}

	public long getRewardPolicyId() {
		return rewardPolicyId;
	}

	public void setRewardPolicyId(long rewardPolicyId) {
		this.rewardPolicyId = rewardPolicyId;
	}

	@JSON(name = "aaData")
	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	@JSON(name = "rewardPolicyVO")
	public Object getRewardPolicyVO() {
		return rewardPolicyVO;
	}

	public void setRewardPolicyVO(Object rewardPolicyVO) {
		this.rewardPolicyVO = rewardPolicyVO;
	}

	public long getCouponId() {
		return couponId;
	}

	public void setCouponId(long couponId) {
		this.couponId = couponId;
	}

	public byte getRewardType() {
		return rewardType;
	}

	public void setRewardType(byte rewardType) {
		this.rewardType = rewardType;
	}

	public double getReward() {
		return reward;
	}

	public void setReward(double reward) {
		this.reward = reward;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
