package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyPool;
import com.aiotech.aios.accounts.service.bl.CurrencyBL;
import com.aiotech.aios.accounts.to.GlCurrencyMasterTO;
import com.aiotech.aios.accounts.to.converter.GlCurrencyMasterTOConverter;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GlCurrencyMasterAction extends ActionSupport {

	private static final long serialVersionUID = 2733513817069739207L;
	private static final Logger LOGGER = LogManager
			.getLogger(GlCurrencyMasterAction.class);
	// Variable declaration
	private String currencyCode;
	private String currencyDesc;
	private String currencyCountry;
	private String currencyStatus;
	private long currencyId;
	private int usageFlag;
	private Long recordId;
	private boolean defalutFlag;
	private Boolean defaultCurrency;
 
  	Implementation implementation = null;

	List<Currency> currency = null;

	private List<GlCurrencyMasterTO> currencyList;
	private String manipulationFlag;
	private String id;
	private CurrencyBL currencyBL;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	int insertStatus = 0;

	// Currency master method's starting here

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	public String execute() {
		recordId = null;
		return SUCCESS;
	}

	public String currencyMasterDetails() {

		try {
 			currencyList = new ArrayList<GlCurrencyMasterTO>();
			// call Implementation
			getImplementId();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("GL_CURRENCY_INFO_"
					+ sessionObj.get("jSessionId"));
			List<Currency> currency = currencyBL.getCurrencyService()
					.getAllCurrency(implementation);

			currencyList = GlCurrencyMasterTOConverter
					.convertToTOList(currency);

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			for (GlCurrencyMasterTO list : currencyList) {
				JSONArray array = new JSONArray();
				array.add(list.getCurrencyId());
				array.add(list.getCurrencyCode());
				array.add(list.getCurrencyDesc());
				array.add(list.getCurrencyCountry());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("val", jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String currencyAddLinesRetrieve() {
		try {
			currencyList = new ArrayList<GlCurrencyMasterTO>();
			List<CurrencyPool> currencyPool = currencyBL.getCurrencyService()
					.getCurrencyPool(implementation);
			currencyList = GlCurrencyMasterTOConverter
					.convertPoolToTOList(currencyPool); 
			ServletActionContext.getServletContext().setAttribute(
					"CURRENCYINFO", currencyList);
			List<Currency> curList = currencyBL.getCurrencyService()
					.getAllCurrency(implementation);
			for (Currency currency : curList) {
				if (currency.getDefaultCurrency() == true) {
					ServletActionContext.getServletContext().setAttribute(
							"DEFAULT_CURRENCY", true);
					return SUCCESS;
				}
			}
			ServletActionContext.getServletContext().setAttribute(
					"DEFAULT_CURRENCY", false);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getServletContext().setAttribute("errMsg1",
					"Error");
			return ERROR;
		}
	}

	public String getCurrencyApprovalScreen() {
		try {
			currencyId = recordId;
			editCurrencyReterive();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getCurrencyRejectEntry() {
		try {
			currencyId = recordId;
			editCurrencyReterive();
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Edit in separate page
	public String editCurrencyReterive() {
		try {
			GlCurrencyMasterTO currencyTO = new GlCurrencyMasterTO();
			// fetch Pool Information
			currencyList = new ArrayList<GlCurrencyMasterTO>();
			List<CurrencyPool> currencyPool = currencyBL.getCurrencyService()
					.getAllCurrencyPool();
			currencyList = GlCurrencyMasterTOConverter
					.convertPoolToTOList(currencyPool); 
			getImplementId();
			// fetch currency information
			Currency currency = new Currency();
			currency = currencyBL.getCurrencyService().getCurrency(currencyId);
			currencyTO = GlCurrencyMasterTOConverter
					.convertCurrecyToTO(currency);
			ServletActionContext.getServletContext().setAttribute(
					"CURRENCYPOOLINFO", currencyList);
			ServletActionContext.getServletContext().setAttribute(
					"CURRENCYINFO", currencyTO);
			List<Currency> currencyList = currencyBL.getCurrencyService()
					.getAllCurrency(implementation);
			boolean flag = false;
			for (Currency curList : currencyList) {
				if (!flag) {
					if (curList.getDefaultCurrency()
							&& !currency.getCurrencyId().equals(
									curList.getCurrencyId())) {
						flag = true;
					}
				}
			}
			ServletActionContext.getServletContext().setAttribute(
					"DEFAULT_CURRENCY", flag);
			CommentVO comment = new CommentVO();
			if (recordId != null && currency != null
					&& currency.getCurrencyId() != 0 && recordId > 0) {
				comment.setRecordId(currency.getCurrencyId());
				comment.setUseCase(Currency.class.getName());
				comment = currencyBL.getCommentBL().getCommentInfo(comment);
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String saveCurrency() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			getImplementId();
			List<Currency> currencys = null;
			Currency currency = new Currency();
			CurrencyPool currencyPool = new CurrencyPool();
			currencyPool.setCurrencyPoolId(currencyId);
			currency.setCurrencyPool(currencyPool);
			currency.setImplementation(implementation);
			currency.setDefaultCurrency(defaultCurrency);
			if (currencyStatus != null && currencyStatus.equals("true"))
				currency.setStatus(true);
			else
				currency.setStatus(false);

			if (session.getAttribute("GL_CURRENCY_INFO_"
					+ sessionObj.get("jSessionId")) == null) {
				currencys = new ArrayList<Currency>();
			} else {
				currencys = (List<Currency>) session
						.getAttribute("GL_CURRENCY_INFO_"
								+ sessionObj.get("jSessionId"));
			}
			if (manipulationFlag != null && manipulationFlag.equals("A")) {
				LOGGER.info("Add Process");
				currencys.add(currency);
			} else if (manipulationFlag.equals("E")) {
				LOGGER.info("Edit Process");
				currencys.set(Integer.parseInt(id) - 1, currency);
			} else if (manipulationFlag.equals("D")) {
				LOGGER.info("Delete Process");
				currencys.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"GL_CURRENCY_INFO_" + sessionObj.get("jSessionId"),
					currencys);

			// currencyService.addCurrency(currency);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	public String finalSaveCurrency() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			if (null != session.getAttribute("GL_CURRENCY_INFO_"
					+ sessionObj.get("jSessionId"))
					&& !("").equals(session.getAttribute("GL_CURRENCY_INFO_"
							+ sessionObj.get("jSessionId")))) {
				@SuppressWarnings("unchecked")
				List<Currency> currencies = (ArrayList<Currency>) session
						.getAttribute("GL_CURRENCY_INFO_"
								+ sessionObj.get("jSessionId"));
				if (null != currencies && !("").equals(currencies)) {
					User user = (User) sessionObj.get("USER");
					currencyBL.saveCurrency(currencies, user);
					session.removeAttribute("GL_CURRENCY_INFO_"
							+ sessionObj.get("jSessionId"));
					ServletActionContext.getRequest().setAttribute("succMsg1",
							"Record created.");
				} else {
					ServletActionContext.getRequest().setAttribute("errMsg1",
							"Record creation failure.");
				}
				return SUCCESS;
			} else {
				ServletActionContext.getRequest().setAttribute("errMsg1",
						"No Currency Added");
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg1",
					"Currency is in use already.");
			return ERROR;
		}
	}

	public String currencyDelete() {

		try {
			Currency currency = new Currency();
			currency.setCurrencyId(currencyId);
			currencyBL.deleteCurrency(currency);
			ServletActionContext.getRequest().setAttribute("succMsg1",
					"Currency Deleted.");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg1",
					"Currency is in use already.");
			return ERROR;
		}
	}

	public String currencyAddDelete() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			@SuppressWarnings("unchecked")
			List<Currency> currency = (List<Currency>) session
					.getAttribute("GL_CURRENCY_INFO_"
							+ sessionObj.get("jSessionId"));
			if (currency != null) {
				currency.remove(new Integer(id) - 1);
			}
			session.setAttribute(
					"GL_CURRENCY_INFO_" + sessionObj.get("jSessionId"),
					currency);
			LOGGER.info("currency delete success");
			ServletActionContext.getRequest().setAttribute("Success",
					"Currency delete Success");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg1", "Error");
			return ERROR;
		}
	}

	public String editSaveCurrency() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			getImplementId();
			Currency currency = new Currency();
			CurrencyPool pool = new CurrencyPool();
			pool.setCurrencyPoolId(Long.parseLong(currencyCode));
			currency.setCurrencyPool(pool);
			currency.setCurrencyId(currencyId);
			currency.setImplementation(implementation);
			currency.setDefaultCurrency(defaultCurrency);
			if (currencyStatus != null && currencyStatus.equals("true"))
				currency.setStatus(true);
			else
				currency.setStatus(false);
			User user = (User) sessionObj.get("USER");
			currencyBL.saveCurrency(currency, user);
			ServletActionContext.getRequest().setAttribute("succMsg1",
					"Record updated..");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg1", "Error");
			return ERROR;
		}
	}

	// Generate get & set methods for the variables
	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public String getCurrencyCountry() {
		return currencyCountry;
	}

	public void setCurrencyCountry(String currencyCountry) {
		this.currencyCountry = currencyCountry;
	}

	public String getCurrencyStatus() {
		return currencyStatus;
	}

	public void setCurrencyStatus(String currencyStatus) {
		this.currencyStatus = currencyStatus;
	}

	public int getUsageFlag() {
		return usageFlag;
	}

	public void setUsageFlag(int usageFlag) {
		this.usageFlag = usageFlag;
	}

	String jsonResult;
	String json;

	public String getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}

	public String getJson() {
		json = jsonResult;
		System.out.println(json);
		return json;
	}

	/**
	 * @return an collection that contains the actual data
	 */
	public List<GlCurrencyMasterTO> getCurrecnyList() {
		return currencyList;
	}

	public List<Currency> getCurrency() {
		return currency;
	}

	public void setCurrency(List<Currency> currency) {
		this.currency = currency;
	}

	public String getManipulationFlag() {
		return manipulationFlag;
	}

	public void setManipulationFlag(String manipulationFlag) {
		this.manipulationFlag = manipulationFlag;
	}

	public boolean isDefalutFlag() {
		return defalutFlag;
	}

	public void setDefalutFlag(boolean defalutFlag) {
		this.defalutFlag = defalutFlag;
	}

	public Boolean getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(Boolean defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public CurrencyBL getCurrencyBL() {
		return currencyBL;
	}

	public void setCurrencyBL(CurrencyBL currencyBL) {
		this.currencyBL = currencyBL;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
