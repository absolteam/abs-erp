/**
 * 
 */
package com.aiotech.aios.accounts.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.IssueReturn;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferVO;
import com.aiotech.aios.accounts.service.bl.IssueReturnBL;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author Saleem
 */
public class IssueReturnAction extends ActionSupport {

	private static final long serialVersionUID = 5087099037224000462L;

	private static final Logger LOG = Logger.getLogger(IssueReturnAction.class);// LOGGER

	// Dependency
	private IssueReturnBL issueReturnBL;

	// Variables
	private long issueReturnId;
	private long cmpDeptLocationId;
	private long storeId;
	private Long recordId;
	private String returnDate;
	private String referenceNumber;
	private String description;
	private String returnDetail;

	public String returnSuccess() {
		try {
			LOG.info("Inside Method : returnSuccess()");
			recordId = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("ISSUE_LOCATION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: returnSuccess Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get all Issue Returns
	public String showAllIssueReturn() {
		try {
			LOG.info("Inside Module: Accounts : Method: showAllIssueReturn");
			JSONObject jsonObject = issueReturnBL.getAllIssueReturn();
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonObject);
			LOG.info("Module: Accounts : Method: showAllIssueReturn: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showAllIssueReturn Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showIssueReturnApproval() {
		try {
			LOG.info("Inside Module: Accounts : Method: showIssueReturnApproval");
			issueReturnId = recordId;
			showIssueReturnEntry();
			LOG.info("Module: Accounts : Method: showIssueReturnApproval: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showIssueReturnApproval Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showIssueReturnRejectEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showIssueReturnRejectEntry");
			issueReturnId = recordId;
			showIssueReturnEntry();
			LOG.info("Module: Accounts : Method: showIssueReturnRejectEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showIssueReturnRejectEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Issue Return entry screen
	public String showIssueReturnEntry() {
		try {
			LOG.info("Inside Module: Accounts : Method: showIssueReturnEntry");
			IssueReturn issueReturn = null;
			if (issueReturnId > 0) {
				issueReturn = issueReturnBL.getIssueReturnService()
						.getIssueReturnById(issueReturnId);
				CommentVO comment = new CommentVO();
				if (recordId != null && issueReturn != null
						&& issueReturn.getIssueReturnId() != 0 && recordId > 0) {
					comment.setRecordId(issueReturn.getIssueReturnId());
					comment.setUseCase(IssueReturn.class.getName());
					comment = issueReturnBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			} else {
				referenceNumber = generateReferenceNumber();
			}
			List<Store> stores = issueReturnBL.getIssueRequistionBL()
					.getStockBL().getStoreBL().getStoreService()
					.getStores(issueReturnBL.getImplementation());
			ServletActionContext.getRequest().setAttribute("STORE_DETAILS",
					stores);
			ServletActionContext.getRequest().setAttribute("ISSUE_RETURN",
					issueReturn);
			LOG.info("Module: Accounts : Method: showIssueReturnEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOG.info("Module: Accounts : Method: showIssueReturnEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Issue Returns
	@SuppressWarnings("unchecked")
	public String saveIssueReturns() {
		try {
			LOG.info("Module: Accounts : Method: saveIssueReturns");
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			IssueReturn issueReturn = new IssueReturn();
			issueReturn.setReturnDate(DateFormat
					.convertStringToDate(returnDate));
			issueReturn.setDescription(description);
			List<IssueReturnDetail> issueReturnDetails = new ArrayList<IssueReturnDetail>();
			IssueReturnDetail returnDetail = null;
			if (cmpDeptLocationId > 0) {
				CmpDeptLoc cmpDeptLocation = new CmpDeptLoc();
				cmpDeptLocation.setCmpDeptLocId(cmpDeptLocationId);
				issueReturn.setCmpDeptLocation(cmpDeptLocation);
				List<IssueRequistionVO> requistionVOList = new ArrayList<IssueRequistionVO>();
				Map<Long, IssueRequistionVO> issueRequistionVOs = (Map<Long, IssueRequistionVO>) (session
						.getAttribute("ISSUE_LOCATION_SESSION_INFO_"
								+ sessionObj.get("jSessionId")));
				String requistionInfo[] = splitValues(this.returnDetail, "#@");
				for (String requistionLine : requistionInfo) {
					if (issueRequistionVOs.containsKey(Long
							.parseLong(requistionLine))) {
						IssueRequistionVO issueRequistionVO = (IssueRequistionVO) issueRequistionVOs
								.get(Long.parseLong(requistionLine));
						requistionVOList.add(issueRequistionVO);
					}
				}
				for (IssueRequistionVO requistionVO : requistionVOList) {
					for (IssueRequistionDetailVO requistionDetailVO : requistionVO
							.getIssueRequistionDetailVO()) {
						returnDetail = new IssueReturnDetail();
						returnDetail
								.setIssueRequistionDetail(requistionDetailVO);
						returnDetail.setReturnQuantity(requistionDetailVO
								.getReturnQty());
						returnDetail
								.setProduct(requistionDetailVO.getProduct());
						returnDetail.setDescription(requistionDetailVO
								.getDescription());
						LookupDetail materialCondition = null;
						if (null != requistionDetailVO.getMaterialCondition()
								&& requistionDetailVO.getMaterialCondition() > 0) {
							materialCondition = new LookupDetail();
							materialCondition
									.setLookupDetailId(requistionDetailVO
											.getMaterialCondition());
						}
						returnDetail.setLookupDetail(materialCondition);
						issueReturnDetails.add(returnDetail);
					}
				}
			} else {
				Store store = new Store();
				store.setStoreId(storeId);
				issueReturn.setStore(store);
				Map<Long, MaterialTransferVO> materialTransferVOs = (Map<Long, MaterialTransferVO>) (session
						.getAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
								+ sessionObj.get("jSessionId")));
				String requistionInfo[] = splitValues(this.returnDetail, "#@");
				List<MaterialTransferVO> transferVOList = new ArrayList<MaterialTransferVO>();
				for (String requistionLine : requistionInfo) {
					if (materialTransferVOs.containsKey(Long
							.parseLong(requistionLine))) {
						MaterialTransferVO materialTransferVO = (MaterialTransferVO) materialTransferVOs
								.get(Long.parseLong(requistionLine));
						transferVOList.add(materialTransferVO);
					}
				}
				for (MaterialTransferVO transferVO : transferVOList) {
					for (MaterialTransferDetailVO transferDetailVO : transferVO
							.getMaterialTransferDetailVOs()) {
						returnDetail = new IssueReturnDetail();
						returnDetail
								.setMaterialTransferDetail(transferDetailVO);
						returnDetail.setReturnQuantity(transferDetailVO
								.getReturnQty());
						returnDetail.setProduct(transferDetailVO.getProduct());
						returnDetail.setDescription(transferDetailVO
								.getDescription());
						LookupDetail materialCondition = null;
						if (null != transferDetailVO.getMaterialCondition()
								&& transferDetailVO.getMaterialCondition() > 0) {
							materialCondition = new LookupDetail();
							materialCondition
									.setLookupDetailId(transferDetailVO
											.getMaterialCondition());
						}
						returnDetail.setLookupDetail(materialCondition);
						issueReturnDetails.add(returnDetail);
					}
				}
			}
			if (issueReturnId > 0l) {
				issueReturn.setIssueReturnId(issueReturnId);
				issueReturn.setReferenceNumber(referenceNumber);
			} else {
				issueReturn.setReferenceNumber(generateReferenceNumber());
			}
			issueReturnBL.saveIssueReturns(issueReturn, issueReturnDetails);
			session.removeAttribute("ISSUE_LOCATION_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("MATERIAL_TRANSFER_SESSION_INFO_"
					+ sessionObj.get("jSessionId"));
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: saveIssueReturns: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: saveIssueReturns: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Delete Issue Returns
	public String deleteIssueReturns() {
		LOG.info("Module: Accounts : Method: deleteIssueReturns");
		try {
			IssueReturn issueReturn = issueReturnBL.getIssueReturnService()
					.getIssueReturns(issueReturnId);
			issueReturnBL.deleteIssueReturns(
					issueReturn,
					new ArrayList<IssueReturnDetail>(issueReturn
							.getIssueReturnDetails()));
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOG.info("Module: Accounts : Method: deleteIssueReturns: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"System internal error.");
			LOG.info("Module: Accounts : Method: deleteIssueReturns: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// Generate Reference Number
	private String generateReferenceNumber() throws Exception {
		return SystemBL.getReferenceStamp(IssueReturn.class.getName(),
				issueReturnBL.getImplementation());
	}

	// Split the value
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Getters and Setters
	public IssueReturnBL getIssueReturnBL() {
		return issueReturnBL;
	}

	public void setIssueReturnBL(IssueReturnBL issueReturnBL) {
		this.issueReturnBL = issueReturnBL;
	}

	public long getIssueReturnId() {
		return issueReturnId;
	}

	public void setIssueReturnId(long issueReturnId) {
		this.issueReturnId = issueReturnId;
	}

	public long getCmpDeptLocationId() {
		return cmpDeptLocationId;
	}

	public void setCmpDeptLocationId(long cmpDeptLocationId) {
		this.cmpDeptLocationId = cmpDeptLocationId;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnDetail() {
		return returnDetail;
	}

	public void setReturnDetail(String returnDetail) {
		this.returnDetail = returnDetail;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}
}
