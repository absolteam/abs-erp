package com.aiotech.aios.accounts.to;

/*******************************************************************************
*
* Initial Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		30 Oct 2010 	A. Abdul Saleem.	     Initial Version
******************************************************************************/

import java.io.Serializable; 

import com.aiotech.aios.accounts.domain.entity.CurrencyPool;

public class GlCurrencyMasterTO implements Serializable {
 
	private static final long serialVersionUID = 1L;
	private String currencyCode;
	private String currencyDesc;
	private String currencyCountry;
	private String currencyPrecision;
	private String currencyStatus;
	private long currencyId;
	private int usageFlag; 

	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id; 
	private long implementationId;
	private CurrencyPool currencyPool;
	private long currencyPoolId;
	private boolean defalutFlag;
	private Boolean defaultCurrency;
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCurrencyDesc() {
		return currencyDesc;
	}
	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}
	public String getCurrencyCountry() {
		return currencyCountry;
	}
	public void setCurrencyCountry(String currencyCountry) {
		this.currencyCountry = currencyCountry;
	}
	public String getCurrencyPrecision() {
		return currencyPrecision;
	}
	public void setCurrencyPrecision(String currencyPrecision) {
		this.currencyPrecision = currencyPrecision;
	}
	public String getCurrencyStatus() {
		return currencyStatus;
	}
	public void setCurrencyStatus(String currencyStatus) {
		this.currencyStatus = currencyStatus;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	} 
	
	public int getUsageFlag() {
		return usageFlag;
	}
	public void setUsageFlag(int usageFlag) {
		this.usageFlag = usageFlag;
	}
	public long getImplementationId() {
		return implementationId;
	}
	public void setImplementationId(long implementationId) {
		this.implementationId = implementationId;
	}
	public CurrencyPool getCurrencyPool() {
		return currencyPool;
	}
	public void setCurrencyPool(CurrencyPool currencyPool) {
		this.currencyPool = currencyPool;
	}
	public long getCurrencyPoolId() {
		return currencyPoolId;
	}
	public void setCurrencyPoolId(long currencyPoolId) {
		this.currencyPoolId = currencyPoolId;
	}
	public boolean isDefalutFlag() {
		return defalutFlag;
	}
	public void setDefalutFlag(boolean defalutFlag) {
		this.defalutFlag = defalutFlag;
	}
	public Boolean getDefaultCurrency() {
		return defaultCurrency;
	}
	public void setDefaultCurrency(Boolean defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	} 
}
