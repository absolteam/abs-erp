package com.aiotech.aios.accounts.to;

import java.io.Serializable;
import java.util.List;

public class GLCombinationMasterTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long combinationId;
	private int companyId;
	private int costAccountId;
	private int naturalAccountId;
	private int analyisAccountId;
	private int bufferAccountId1;
	private int bufferAccountId2;
	
	private String companyCode;
	private String companyCodeDesc;
	private String costCode;
	private String costCodeDesc;
	private String naturalCode;
	private String naturalCodeDesc;
	private String analyisCode;
	private String analyisCodeDesc;
	private String bufferCode1;
	private String bufferCode1Desc;
	private String bufferCode2;
	private String bufferCode2Desc;
	private String description;
	private String coaHeaderId;
	private String coaName;
	
	private int accountId;
	private String account;
	private String code;
	
	private Integer trnValue;
	private Integer tempLineId;
	private int actualLineId;
	
	private String sqlReturnMessage;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	//Grid Variables
	private Integer gridFrom; 
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	 
	private List<GLCombinationMasterTO> companyList;
	private List<GLCombinationMasterTO> costList;
	private List<GLCombinationMasterTO> naturalList;
	private List<GLCombinationMasterTO> analysisList;
	private List<GLCombinationMasterTO> bufferList1;
	private List<GLCombinationMasterTO> bufferList2;
	
	//Getters & setters
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getCostAccountId() {
		return costAccountId;
	}
	public void setCostAccountId(int costAccountId) {
		this.costAccountId = costAccountId;
	}
	public int getNaturalAccountId() {
		return naturalAccountId;
	}
	public void setNaturalAccountId(int naturalAccountId) {
		this.naturalAccountId = naturalAccountId;
	}
	public int getAnalyisAccountId() {
		return analyisAccountId;
	}
	public void setAnalyisAccountId(int analyisAccountId) {
		this.analyisAccountId = analyisAccountId;
	}
	public int getBufferAccountId1() {
		return bufferAccountId1;
	}
	public void setBufferAccountId1(int bufferAccountId1) {
		this.bufferAccountId1 = bufferAccountId1;
	}
	public int getBufferAccountId2() {
		return bufferAccountId2;
	}
	public void setBufferAccountId2(int bufferAccountId2) {
		this.bufferAccountId2 = bufferAccountId2;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyCodeDesc() {
		return companyCodeDesc;
	}
	public void setCompanyCodeDesc(String companyCodeDesc) {
		this.companyCodeDesc = companyCodeDesc;
	}
	public String getCostCode() {
		return costCode;
	}
	public void setCostCode(String costCode) {
		this.costCode = costCode;
	}
	public String getCostCodeDesc() {
		return costCodeDesc;
	}
	public void setCostCodeDesc(String costCodeDesc) {
		this.costCodeDesc = costCodeDesc;
	}
	public String getNaturalCode() {
		return naturalCode;
	}
	public void setNaturalCode(String naturalCode) {
		this.naturalCode = naturalCode;
	}
	public String getNaturalCodeDesc() {
		return naturalCodeDesc;
	}
	public void setNaturalCodeDesc(String naturalCodeDesc) {
		this.naturalCodeDesc = naturalCodeDesc;
	}
	public String getAnalyisCode() {
		return analyisCode;
	}
	public void setAnalyisCode(String analyisCode) {
		this.analyisCode = analyisCode;
	}
	public String getAnalyisCodeDesc() {
		return analyisCodeDesc;
	}
	public void setAnalyisCodeDesc(String analyisCodeDesc) {
		this.analyisCodeDesc = analyisCodeDesc;
	}
	public String getBufferCode1() {
		return bufferCode1;
	}
	public void setBufferCode1(String bufferCode1) {
		this.bufferCode1 = bufferCode1;
	}
	public String getBufferCode1Desc() {
		return bufferCode1Desc;
	}
	public void setBufferCode1Desc(String bufferCode1Desc) {
		this.bufferCode1Desc = bufferCode1Desc;
	}
	public String getBufferCode2() {
		return bufferCode2;
	}
	public void setBufferCode2(String bufferCode2) {
		this.bufferCode2 = bufferCode2;
	}
	public String getBufferCode2Desc() {
		return bufferCode2Desc;
	}
	public void setBufferCode2Desc(String bufferCode2Desc) {
		this.bufferCode2Desc = bufferCode2Desc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCoaHeaderId() {
		return coaHeaderId;
	}
	public void setCoaHeaderId(String coaHeaderId) {
		this.coaHeaderId = coaHeaderId;
	}
	public String getCoaName() {
		return coaName;
	}
	public void setCoaName(String coaName) {
		this.coaName = coaName;
	}
	public Integer getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	public int getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}
	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}
	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	 
	public long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}
	public List<GLCombinationMasterTO> getCompanyList() {
		return companyList;
	}
	public void setCompanyList(List<GLCombinationMasterTO> companyList) {
		this.companyList = companyList;
	}
	public List<GLCombinationMasterTO> getCostList() {
		return costList;
	}
	public void setCostList(List<GLCombinationMasterTO> costList) {
		this.costList = costList;
	}
	public List<GLCombinationMasterTO> getNaturalList() {
		return naturalList;
	}
	public void setNaturalList(List<GLCombinationMasterTO> naturalList) {
		this.naturalList = naturalList;
	}
	public List<GLCombinationMasterTO> getAnalysisList() {
		return analysisList;
	}
	public void setAnalysisList(List<GLCombinationMasterTO> analysisList) {
		this.analysisList = analysisList;
	}
	public List<GLCombinationMasterTO> getBufferList1() {
		return bufferList1;
	}
	public void setBufferList1(List<GLCombinationMasterTO> bufferList1) {
		this.bufferList1 = bufferList1;
	}
	public List<GLCombinationMasterTO> getBufferList2() {
		return bufferList2;
	}
	public void setBufferList2(List<GLCombinationMasterTO> bufferList2) {
		this.bufferList2 = bufferList2;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	 
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

}
