package com.aiotech.aios.accounts.to;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.TenderDetail;

/**
 * @author Usman Anwar
 * 
 */
public class TenderTO {

	Long tenderId;
	String currency;
	String tenderDate;
	String tenderExpiryDate;
	String tenderNumber;
	String tenderDescription;

	List<TenderTO> tenderDetails;
	// tender details

	String productName;
	String quantity;
	String unitRate;
	String model;
	String description;
	Double totalAmount;
	String amount;
	String uom;
	String itemType;

	public void convertToTenderTO(Tender tender) {
		try {

			setTenderTOValues(this, tender);

			if (tender.getTenderDetails() != null) {
				this.tenderDetails = new ArrayList<TenderTO>();
				Double total = 0.0;
				for (TenderDetail tenderDetail : tender.getTenderDetails()) {

					TenderTO to = new TenderTO();
					to.setModel(tenderDetail.getModel());
					to.setProductName(tenderDetail.getProduct()
							.getProductName());
					to.setQuantity(tenderDetail.getQuantity());
					to.setUnitRate(tenderDetail.getUnitRate());
					to.setItemType(tenderDetail.getProduct().getItemType()
							.toString());
					to.setUom(tenderDetail.getProduct().getLookupDetailByProductUnit().getDisplayName());
					to.setDescription(tenderDetail.getDescription());

					to.setTotalAmount((Double.parseDouble(tenderDetail
							.getQuantity()) * Double.parseDouble(tenderDetail
							.getUnitRate())));

					total = total
							+ (Double.parseDouble(tenderDetail.getQuantity()) * Double
									.parseDouble(tenderDetail.getUnitRate()));
					amount = total.toString();
					this.tenderDetails.add(to);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTenderTOValues(TenderTO tenderTO, Tender tender) {

		try {

			tenderTO.setTenderId(tender.getTenderId());
			tenderTO.setTenderNumber(tender.getTenderNumber());
			tenderTO.setTenderDate(tender.getTenderDate().toString());
			tenderTO.setTenderExpiryDate(tender.getTenderExpiry().toString());
			tenderTO.setTenderDescription(tender.getDescription());

			if (tender.getCurrency() != null) {
				tenderTO.setCurrency(tender.getCurrency().getCurrencyPool()
						.getCode());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// ============Getters Setters================
	public Long getTenderId() {
		return tenderId;
	}

	public void setTenderId(Long tenderId) {
		this.tenderId = tenderId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<TenderTO> getTenderDetails() {
		return tenderDetails;
	}

	public void setTenderDetails(List<TenderTO> tenderDetails) {
		this.tenderDetails = tenderDetails;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(String unitRate) {
		this.unitRate = unitRate;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTenderDate() {
		return tenderDate;
	}

	public void setTenderDate(String tenderDate) {
		this.tenderDate = tenderDate;
	}

	public String getTenderExpiryDate() {
		return tenderExpiryDate;
	}

	public void setTenderExpiryDate(String tenderExpiryDate) {
		this.tenderExpiryDate = tenderExpiryDate;
	}

	public String getTenderNumber() {
		return tenderNumber;
	}

	public void setTenderNumber(String tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public String getTenderDescription() {
		return tenderDescription;
	}

	public void setTenderDescription(String tenderDescription) {
		this.tenderDescription = tenderDescription;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
