package com.aiotech.aios.accounts.to;

import java.util.Date;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Transaction;

public class TransactionTO extends Transaction implements java.io.Serializable{

	private static final long serialVersionUID = 414166898959436614L;
	
	private String description;
	private Long combinationId;
	private Double amount;
	private boolean isDebit;
	private Combination combination;
	private Date date;
	private String accountCode;
	private String refFromXL;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public boolean isDebit() {
		return isDebit;
	}
	public void setDebit(boolean isDebit) {
		this.isDebit = isDebit;
	}
	public Combination getCombination() {
		return combination;
	}
	public void setCombination(Combination combination) {
		this.combination = combination;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getRefFromXL() {
		return refFromXL;
	}
	public void setRefFromXL(String refFromXL) {
		this.refFromXL = refFromXL;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
}
