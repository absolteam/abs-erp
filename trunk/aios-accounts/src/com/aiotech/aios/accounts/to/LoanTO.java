package com.aiotech.aios.accounts.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.LoanDetail;

public class LoanTO extends LoanDetail implements Serializable{

	private static final long serialVersionUID = 1L;
	private String linkLabel;
	private String anchorExpression;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private String detailDate;
	private long otherChargeId;
	private String chargeName;
	private double chargeValue;
	private String chargeType;
	private String accountNumber; 
	private String sancationDate;
	private String maturityDate;
	private String loanNumber;
	private double loanAmount;
	private double interest;
	private double finalInterest;
	private String currency;
	private String loanType;
	private String interestType;
	private String interestMethod; 
	private String description;
	private String emiType;
	
	private List<LoanTO> loanDetailList=new ArrayList<LoanTO>();
	private List<LoanTO> loanChargeList=new ArrayList<LoanTO>();
	
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public List<LoanTO> getLoanDetailList() {
		return loanDetailList;
	}
	public void setLoanDetailList(List<LoanTO> loanDetailList) {
		this.loanDetailList = loanDetailList;
	}
	public List<LoanTO> getLoanChargeList() {
		return loanChargeList;
	}
	public void setLoanChargeList(List<LoanTO> loanChargeList) {
		this.loanChargeList = loanChargeList;
	}
	public String getDetailDate() {
		return detailDate;
	}
	public void setDetailDate(String detailDate) {
		this.detailDate = detailDate;
	}
	public long getOtherChargeId() {
		return otherChargeId;
	}
	public void setOtherChargeId(long otherChargeId) {
		this.otherChargeId = otherChargeId;
	}
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	public double getChargeValue() {
		return chargeValue;
	}
	public void setChargeValue(double chargeValue) {
		this.chargeValue = chargeValue;
	}
	public String getChargeType() {
		return chargeType;
	}
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public String getSancationDate() {
		return sancationDate;
	}
	public void setSancationDate(String sancationDate) {
		this.sancationDate = sancationDate;
	}
	public String getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public double getFinalInterest() {
		return finalInterest;
	}
	public void setFinalInterest(double finalInterest) {
		this.finalInterest = finalInterest;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInterestType() {
		return interestType;
	}
	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}
	public String getInterestMethod() {
		return interestMethod;
	}
	public void setInterestMethod(String interestMethod) {
		this.interestMethod = interestMethod;
	}
	public String getEmiType() {
		return emiType;
	}
	public void setEmiType(String emiType) {
		this.emiType = emiType;
	} 
}
