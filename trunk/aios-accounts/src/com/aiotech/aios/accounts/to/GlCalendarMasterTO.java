package com.aiotech.aios.accounts.to;

/*******************************************************************************
*
* Initial Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		06 Oct 2010 	Saleem	 		   
******************************************************************************/


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet; 
import java.util.Set;

import com.aiotech.aios.accounts.domain.entity.Period;


public class GlCalendarMasterTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long periodId;
	private long calendarId;
	private String name;
	private String startTime;
	private String endTime;
	private int extention; 
	private long implementationId;
	private String calendarStatus;
	private Set<Period> periods = new HashSet<Period>(0);
	private byte status; 
	private Date periodStartDate;
	public long getPeriodId() {
		return periodId;
	}
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}
	public long getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(long calendarId) {
		this.calendarId = calendarId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getExtention() {
		return extention;
	}
	public void setExtention(int extention) {
		this.extention = extention;
	}
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id; 
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	} 
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Set<Period> getPeriods() {
		return periods;
	}
	public void setPeriods(Set<Period> periods) {
		this.periods = periods;
	}
	public long getImplementationId() {
		return implementationId;
	}
	public void setImplementationId(long implementationId) {
		this.implementationId = implementationId;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public String getCalendarStatus() {
		return calendarStatus;
	}
	public void setCalendarStatus(String calendarStatus) {
		this.calendarStatus = calendarStatus;
	}
	public Date getPeriodStartDate() {
		return periodStartDate;
	}
	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	} 
}
