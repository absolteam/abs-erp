package com.aiotech.aios.accounts.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.Supplier;

public class SupplierTO extends Supplier implements Serializable {

	private static final long serialVersionUID = 1L;

	private String supplierName;
	private String type;
	private String paymentTermName;
	private long paymentTermId;
	private long combinationId;
	private long creditTermId;
	private List<SupplierTO> details;
	private String total;

	// payment
	private String paymentDate;
	private String invoiceNumber;
	private String paymentDescription;
	private String chequeNumber;
	private String discountReceived;
	private String chequeDate;

	// paymentDetails
	private String productName;
	private String quantity;
	private String unitRate;
	private String paymentDetailDescription;

	// directPayment
	private String directPaymentNumber;
	private String directPaymentDate;
	private String directPaymentChequeDate;
	private String isPdc;
	private String others;

	// directPayment Details
	private String directPaymentDetailInvoiceNumber;
	private String directPaymentAmount;
	private String directPaymentDetailDescription;

	private List<SupplierTO> directPaymentsList;
	private List<SupplierTO> paymentsList;

	private List<SupplierTO> directPaymentsDetailsList;
	private List<SupplierTO> paymentsDetailsList;

	private List<Receive> receivesList;
	private List<GoodsReturn> returnsList;

	private String rNumber;
	private String rDate;
	private String description;
	
		
	private List<SupplierTO> receiveTOs;
	private List<SupplierTO> returnTs;

	public SupplierTO() {
	}

	public SupplierTO(Supplier supplier) throws Exception {

		this.setSupplierId(supplier.getSupplierId());

		if (supplier.getPerson() != null) {
			this.supplierName = supplier.getPerson().getFirstName() + " "
					+ supplier.getPerson().getLastName();
			this.type = "Person";
		}

		if (supplier.getCmpDeptLocation() != null) {
			if (supplier.getCmpDeptLocation().getCompany() != null) {
				this.supplierName = supplier.getCmpDeptLocation().getCompany()
						.getCompanyName();
				this.type = "Company";
			}
		}

		if (supplier.getCreditTerm() != null) {
			this.paymentTermName = supplier.getCreditTerm().getName();
		} else {
			this.paymentTermName = "";
		}

		this.setSupplierNumber(supplier.getSupplierNumber());
		if(supplier.getCmpDeptLocation() != null) {
			this.setBranchName(supplier.getCmpDeptLocation().getLocation()
				.getLocationName());
		}

		if (supplier.getPayments() != null && supplier.getPayments().size() > 0) {
			List<Payment> list = new ArrayList<Payment>(supplier.getPayments());

			Collections.sort(list, new Comparator<Payment>() {
				public int compare(Payment m1, Payment m2) {
					return m1.getDate().compareTo(m2.getDate());
				}
			});

			this.paymentsList = preparePaymentsList(list);

		}

		if (supplier.getDirectPayments() != null
				&& supplier.getDirectPayments().size() > 0) {
			List<DirectPayment> list = new ArrayList<DirectPayment>(
					supplier.getDirectPayments());

			Collections.sort(list, new Comparator<DirectPayment>() {
				public int compare(DirectPayment m1, DirectPayment m2) {
					return m1.getPaymentDate().compareTo(m2.getPaymentDate());
				}
			});
			this.directPaymentsList = prepareDirectPaymentsList(list);
		}

	}

	private List<SupplierTO> prepareDirectPaymentsList(
			List<DirectPayment> payments) throws Exception {

		List<SupplierTO> paymentsList = new ArrayList<SupplierTO>();

		for (DirectPayment payment : payments) {
			SupplierTO supplierTO = new SupplierTO();

			supplierTO
					.setDirectPaymentDate(payment.getPaymentDate() != null ? payment
							.getPaymentDate().toString() : "");
			supplierTO
					.setDirectPaymentNumber(payment.getPaymentNumber() != null ? payment
							.getPaymentNumber().toString() : "");
			supplierTO
					.setDirectPaymentChequeDate(payment.getChequeDate() != null ? payment
							.getChequeDate().toString() : "");
			supplierTO.setIsPdc(payment.getIsPdc() != null ? payment.getIsPdc()
					.toString() : "");
			supplierTO.setOthers(payment.getOthers());

			if (payment.getDirectPaymentDetails() != null
					&& payment.getDirectPaymentDetails().size() > 0) {

				List<DirectPaymentDetail> details = new ArrayList<DirectPaymentDetail>(
						payment.getDirectPaymentDetails());
				supplierTO
						.setDirectPaymentsDetailsList(prepareDirectPaymentsDetailsList(details));
			}
			paymentsList.add(supplierTO);
		}
		return paymentsList;

	}

	private List<SupplierTO> prepareDirectPaymentsDetailsList(
			List<DirectPaymentDetail> details) throws Exception {

		List<SupplierTO> directPaymentsDetailsList = new ArrayList<SupplierTO>();

		for (DirectPaymentDetail directPaymentDetail : details) {
			SupplierTO supplierTO = new SupplierTO();

			supplierTO.setDirectPaymentDetailDescription(directPaymentDetail
					.getDescription());
			supplierTO.setDirectPaymentAmount(directPaymentDetail.getAmount()
					.toString());
			supplierTO.setDirectPaymentDetailInvoiceNumber(directPaymentDetail
					.getInvoiceNumber());
			directPaymentsDetailsList.add(supplierTO);
		}

		return directPaymentsDetailsList;
	}

	private List<SupplierTO> preparePaymentsList(List<Payment> payments)
			throws Exception {

		List<SupplierTO> paymentsList = new ArrayList<SupplierTO>();

		for (Payment payment : payments) {
			SupplierTO supplierTO = new SupplierTO();

			supplierTO.setPaymentDate(payment.getDate().toString());
			supplierTO
					.setInvoiceNumber(payment.getInvoiceNumber() != null ? payment
							.getInvoiceNumber().toString() : "");
			supplierTO
					.setPaymentDescription(payment.getDescription() != null ? payment
							.getDescription().toString() : "");
			supplierTO.setChequeDate(payment.getChequeDate() != null ? payment
					.getChequeDate().toString() : "");
			supplierTO
					.setChequeNumber(payment.getChequeNumber() != null ? payment
							.getChequeNumber().toString() : "");
			supplierTO
					.setDiscountReceived(payment.getDiscountReceived() != null ? payment
							.getDiscountReceived().toString() : ""

					);

			if (payment.getPaymentDetails() != null
					&& payment.getPaymentDetails().size() > 0) {

				List<PaymentDetail> details = new ArrayList<PaymentDetail>(
						payment.getPaymentDetails());
				supplierTO
						.setPaymentsDetailsList(preparePaymentsDetailsList(details));
			}
			paymentsList.add(supplierTO);
		}
		return paymentsList;

	}

	private List<SupplierTO> preparePaymentsDetailsList(
			List<PaymentDetail> details) throws Exception {

		List<SupplierTO> paymentsDetailsList = new ArrayList<SupplierTO>();

		for (PaymentDetail paymentDetail : details) {
			SupplierTO supplierTO = new SupplierTO();

			supplierTO.setProductName(paymentDetail.getProduct() != null && 
					paymentDetail.getProduct().getProductName() != null ? paymentDetail.getProduct()
					.getProductName().toString() : ""

			);
			if(paymentDetail.getQuantity() != null) {
				supplierTO.setQuantity(paymentDetail.getQuantity().toString());
			}
			
			if(paymentDetail.getUnitRate() != null) {
				supplierTO.setUnitRate(paymentDetail.getUnitRate().toString());
			}
			
			if(paymentDetail.getDescription() != null) {
				supplierTO.setPaymentDescription(paymentDetail.getDescription());
			}
			paymentsDetailsList.add(supplierTO);
		}

		return paymentsDetailsList;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getPaymentTermName() {
		return paymentTermName;
	}

	public void setPaymentTermName(String paymentTermName) {
		this.paymentTermName = paymentTermName;
	}

	public long getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public List<SupplierTO> getDetails() {
		return details;
	}

	public void setDetails(List<SupplierTO> details) {
		this.details = details;
	}

	public List<SupplierTO> getDirectPaymentsList() {
		return directPaymentsList;
	}

	public void setDirectPaymentsList(List<SupplierTO> directPaymentsList) {
		this.directPaymentsList = directPaymentsList;
	}

	public List<SupplierTO> getPaymentsList() {
		return paymentsList;
	}

	public void setPaymentsList(List<SupplierTO> paymentsList) {
		this.paymentsList = paymentsList;
	}

	public List<SupplierTO> getDirectPaymentsDetailsList() {
		return directPaymentsDetailsList;
	}

	public void setDirectPaymentsDetailsList(
			List<SupplierTO> directPaymentsDetailsList) {
		this.directPaymentsDetailsList = directPaymentsDetailsList;
	}

	public List<SupplierTO> getPaymentsDetailsList() {
		return paymentsDetailsList;
	}

	public void setPaymentsDetailsList(List<SupplierTO> paymentsDetailsList) {
		this.paymentsDetailsList = paymentsDetailsList;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getDiscountReceived() {
		return discountReceived;
	}

	public void setDiscountReceived(String discountReceived) {
		this.discountReceived = discountReceived;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(String unitRate) {
		this.unitRate = unitRate;
	}

	public String getPaymentDetailDescription() {
		return paymentDetailDescription;
	}

	public void setPaymentDetailDescription(String paymentDetailDescription) {
		this.paymentDetailDescription = paymentDetailDescription;
	}

	public String getDirectPaymentNumber() {
		return directPaymentNumber;
	}

	public void setDirectPaymentNumber(String directPaymentNumber) {
		this.directPaymentNumber = directPaymentNumber;
	}

	public String getDirectPaymentDate() {
		return directPaymentDate;
	}

	public void setDirectPaymentDate(String directPaymentDate) {
		this.directPaymentDate = directPaymentDate;
	}

	public String getDirectPaymentChequeDate() {
		return directPaymentChequeDate;
	}

	public void setDirectPaymentChequeDate(String directPaymentChequeDate) {
		this.directPaymentChequeDate = directPaymentChequeDate;
	}

	public String getIsPdc() {
		return isPdc;
	}

	public void setIsPdc(String isPdc) {
		this.isPdc = isPdc;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getDirectPaymentDetailInvoiceNumber() {
		return directPaymentDetailInvoiceNumber;
	}

	public void setDirectPaymentDetailInvoiceNumber(
			String directPaymentDetailInvoiceNumber) {
		this.directPaymentDetailInvoiceNumber = directPaymentDetailInvoiceNumber;
	}

	public String getDirectPaymentAmount() {
		return directPaymentAmount;
	}

	public void setDirectPaymentAmount(String directPaymentAmount) {
		this.directPaymentAmount = directPaymentAmount;
	}

	public String getDirectPaymentDetailDescription() {
		return directPaymentDetailDescription;
	}

	public void setDirectPaymentDetailDescription(
			String directPaymentDetailDescription) {
		this.directPaymentDetailDescription = directPaymentDetailDescription;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<Receive> getReceivesList() {
		return receivesList;
	}

	public void setReceivesList(List<Receive> receivesList) {
		this.receivesList = receivesList;
	}

	public List<GoodsReturn> getReturnsList() {
		return returnsList;
	}

	public void setReturnsList(List<GoodsReturn> returnsList) {
		this.returnsList = returnsList;
	}

	public String getrNumber() {
		return rNumber;
	}

	public void setrNumber(String rNumber) {
		this.rNumber = rNumber;
	}

	public String getrDate() {
		return rDate;
	}

	public void setrDate(String rDate) {
		this.rDate = rDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<SupplierTO> getReceiveTOs() {
		return receiveTOs;
	}

	public void setReceiveTOs(List<SupplierTO> receiveTOs) {
		this.receiveTOs = receiveTOs;
	}

	public List<SupplierTO> getReturnTs() {
		return returnTs;
	}

	public void setReturnTs(List<SupplierTO> returnTs) {
		this.returnTs = returnTs;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}
	

}
