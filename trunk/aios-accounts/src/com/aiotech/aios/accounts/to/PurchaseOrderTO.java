/**
 * 
 */
package com.aiotech.aios.accounts.to;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;

/**
 * @author Usman Anwar
 * 
 */
public class PurchaseOrderTO implements java.io.Serializable {

	private static final long serialVersionUID = -7689740980780805702L;
	
	private Long purchaseId;
	private String currency;
	private String paymentTerm;
	private Long quotationId;
	private String quotationNumber = "";
	private String supplierNumber = "";
	private String supplierName = "";
	private String purchaseNumber;
	private String purchaseDate;
	private String expiryDate;
	private Integer status;
	private String purchaseDescription = "";

	private String createdDate;
	private String anchorExpression;
	private String linkLabel;

	// purchase detail

	private String productName;
	private Double quantity;
	private Double unitRate;
	private String purchaseDetailDescription = "";
	private String itemName;
	private String itemType;
	private String uom;
	private Double amount;
	private Double totalAmount;

	private List<PurchaseOrderTO> purchaseDetails;

	public void convertToPurchaseOrderTO(Purchase purchase) {
		try {

			setPurchaseOrderTOValues(this, purchase);

			totalAmount = 0.0;
			if (purchase.getPurchaseDetails() != null) {
				this.purchaseDetails = new ArrayList<PurchaseOrderTO>();
				for (PurchaseDetail detail : purchase.getPurchaseDetails()) {

					PurchaseOrderTO detailTO = new PurchaseOrderTO();

					detailTO.setProductName(detail.getProduct()
							.getProductName());
					detailTO.setQuantity(detail.getQuantity());
					detailTO.setUnitRate(detail.getUnitRate());

					detailTO.setAmount(detail.getQuantity()
							* detail.getUnitRate());

					detailTO.setPurchaseDetailDescription(detail
							.getDescription());
					detailTO.setItemType(detail.getProduct().getItemType()
							.toString());
					detailTO.setUom(detail.getProduct().getLookupDetailByProductUnit().getDisplayName());
					if (detail.getDescription() != null) {
						detailTO.setPurchaseDetailDescription(detail
								.getDescription());
					} else {
						detailTO.setPurchaseDetailDescription("");
					}

					this.totalAmount = totalAmount + detailTO.getAmount();

					this.purchaseDetails.add(detailTO);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setPurchaseOrderTOValues(PurchaseOrderTO purchaseOrderTO,
			Purchase purchase) {

		try {

			purchaseOrderTO.purchaseId = purchase.getPurchaseId();
			purchaseOrderTO.purchaseNumber = purchase.getPurchaseNumber();
			purchaseOrderTO.setPurchaseDate(purchase.getDate().toString());
			purchaseOrderTO.setExpiryDate(purchase.getExpiryDate().toString());
			purchaseOrderTO.setStatus(purchase.getStatus());
			purchaseOrderTO.setPurchaseDescription(purchase.getDescription());

			if (purchase.getQuotation() != null
					&& purchase.getQuotation().getQuotationNumber() != null) {
				purchaseOrderTO.setQuotationNumber(purchase.getQuotation()
						.getQuotationNumber());
			}

			if (purchase.getCurrency() != null) {
				purchaseOrderTO.setCurrency(purchase.getCurrency()
						.getCurrencyPool().getCode());
			}

			if (purchase.getCreditTerm() != null) {
				purchaseOrderTO.setPaymentTerm(purchase.getCreditTerm()
						.getName());
			}

			if (purchase.getQuotation() != null) {
				purchaseOrderTO.setQuotationId(purchase.getQuotation()
						.getQuotationId());
			}

			if (purchase.getSupplier() != null) {
				purchaseOrderTO.setSupplierNumber(purchase.getSupplier()
						.getSupplierNumber());
				if (purchase.getSupplier().getPerson() != null) {
					purchaseOrderTO
							.setSupplierName(purchase.getSupplier().getPerson()
									.getFirstName()
									+ " "
									+ purchase.getSupplier().getPerson()
											.getFirstName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// ==========Getters Setters====================
	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public Long getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(Long quotationId) {
		this.quotationId = quotationId;
	}

	public String getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(String supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPurchaseDescription() {
		return purchaseDescription;
	}

	public void setPurchaseDescription(String purchaseDescription) {
		this.purchaseDescription = purchaseDescription;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public String getPurchaseDetailDescription() {
		return purchaseDetailDescription;
	}

	public void setPurchaseDetailDescription(String purchaseDetailDescription) {
		this.purchaseDetailDescription = purchaseDetailDescription;
	}

	public List<PurchaseOrderTO> getPurchaseDetails() {
		return purchaseDetails;
	}

	public void setPurchaseDetails(List<PurchaseOrderTO> purchaseDetails) {
		this.purchaseDetails = purchaseDetails;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getQuotationNumber() {
		return quotationNumber;
	}

	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
