package com.aiotech.aios.accounts.to;

import java.io.Serializable; 

public class GLChartOfAccountsTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//Common Variable Declaration
	private long accountId;
	private long implementationId;
	private long segmentId;
	private int accessId;
	private int accountTypeId;
	private String segmentName; 
	private String accountType;
	private String accountCode; 
	private String accountDescription;
	
	//Grid variable Declarations
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;  
	
	//Sql Return Types
	private String sqlReturnMessage; 
	private int sqlReturnStatus;
	private String sqlProgramName;
	private Long lookupDetailId;
	private String lookupDisplayName;

	//Default Constructor
	public GLChartOfAccountsTO(){
		
	}

	//Getters and Setters
	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	public int getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public Integer getGridFrom() {
		return gridFrom;
	}

	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}

	public Integer getGridTo() {
		return gridTo;
	}

	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}

	public String getGridSord() {
		return gridSord;
	}

	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	} 

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}

	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}

	public String getSqlProgramName() {
		return sqlProgramName;
	}

	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	} 
	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public long getImplementationId() {
		return implementationId;
	}

	public void setImplementationId(long implementationId) {
		this.implementationId = implementationId;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}

	public String getLookupDisplayName() {
		return lookupDisplayName;
	}

	public void setLookupDisplayName(String lookupDisplayName) {
		this.lookupDisplayName = lookupDisplayName;
	}

	public int getAccessId() {
		return accessId;
	}

	public void setAccessId(int accessId) {
		this.accessId = accessId;
	} 
}
