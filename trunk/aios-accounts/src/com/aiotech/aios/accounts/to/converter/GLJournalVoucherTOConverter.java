package com.aiotech.aios.accounts.to.converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.to.DepositTO;
import com.aiotech.aios.accounts.to.GLJournalVoucherTO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.AIOSCommons;

public class GLJournalVoucherTOConverter {

	public static List<GLJournalVoucherTO> convertToTOList(
			List<TransactionTemp> transactionList) {
		List<GLJournalVoucherTO> tosList = new ArrayList<GLJournalVoucherTO>();
		for (TransactionTemp transcation : transactionList) {
			tosList.add(convertToTO(transcation));
		}
		return tosList;
	}

	public static List<DepositTO> convertToDepositList(
			List<TransactionDetail> transactionDetail) {
		List<DepositTO> tosList = new ArrayList<DepositTO>();
		for (TransactionDetail transcation : transactionDetail) {
			tosList.add(convertToDepositTO(transcation));
		}
		return tosList;
	}

	public static List<DepositTO> convertBankDepositList(
			List<BankAccount> accountList) {
		List<DepositTO> tosList = new ArrayList<DepositTO>();
		for (BankAccount account : accountList) {
			tosList.add(convertBankAccountDepositTO(account));
		}
		return tosList;
	}

	public static List<GLJournalVoucherTO> convertCurrencyToTOList(
			List<Currency> currencyList) {
		List<GLJournalVoucherTO> tosList = new ArrayList<GLJournalVoucherTO>();
		for (Currency currency : currencyList) {
			tosList.add(convertCurrencyToTO(currency));
		}
		return tosList;
	}

	public static List<GLJournalVoucherTO> convertPeriodToTOList(
			List<Period> periodList) {
		List<GLJournalVoucherTO> tosList = new ArrayList<GLJournalVoucherTO>();
		for (Period period : periodList) {
			tosList.add(convertPeriodToTO(period));
		}
		return tosList;
	}

	public static List<GLJournalVoucherTO> convertDetailToTOList(
			List<TransactionDetail> transactionDetail) {
		List<GLJournalVoucherTO> tosList = new ArrayList<GLJournalVoucherTO>();
		for (TransactionDetail detail : transactionDetail) {
			tosList.add(convertDetailToTO(detail));
		}
		return tosList;
	}

	public static List<GLJournalVoucherTO> convertCodeToToList(
			List<Combination> combinationList) {

		List<GLJournalVoucherTO> tosList = new ArrayList<GLJournalVoucherTO>();

		for (Combination combination : combinationList) {
			tosList.add(convertCodeToTO(combination));
		}
		return tosList;
	}

	public static GLJournalVoucherTO convertToTO(TransactionTemp transaction) {
		GLJournalVoucherTO transactionTO = new GLJournalVoucherTO();
		transactionTO.setJournalId(transaction.getTransaction()
				.getTransactionId());
		transactionTO.setCurrencyId(transaction.getCurrency().getCurrencyId());
		transactionTO.setPeriodId(transaction.getPeriod().getPeriodId()
				.intValue());
		transactionTO.setJournalDate(DateFormat.convertDateToString(transaction
				.getTransactionTime().toString()));
		transactionTO.setEntryDate(DateFormat.convertDateToDate(transaction
				.getEntryTime().toString()));
		transactionTO.setPeriodName(transaction.getPeriod().getName());
		transactionTO.setCurrencyCode(transaction.getCurrency()
				.getCurrencyPool().getCode());
		transactionTO.setDescription(transaction.getDescription()); 
		if (null != transaction.getCategory()) {
			transactionTO.setCategoryName(transaction.getCategory().getName());
		}
		return transactionTO;
	}

	public static GLJournalVoucherTO convertToTOEdit(Transaction transaction) {
		GLJournalVoucherTO transactionTO = new GLJournalVoucherTO();
		transactionTO.setJournalId(transaction.getTransactionId());
		transactionTO.setCurrencyId(transaction.getCurrency().getCurrencyId());
		transactionTO.setPeriodId(transaction.getPeriod().getPeriodId()
				.intValue());
		transactionTO.setPeriodName(transaction.getPeriod().getName());
		transactionTO.setJournalDate(DateFormat.convertDateToString(transaction
				.getTransactionTime().toString()));
		transactionTO.setDescription(transaction.getDescription());
		if (null != transaction.getCategory()) {
			transactionTO.setCategoryId(transaction.getCategory()
					.getCategoryId());
		}
		transactionTO.setJournalNumber(transaction.getJournalNumber());
		return transactionTO;
	}

	public static GLJournalVoucherTO convertCurrencyToTO(Currency currency) {
		GLJournalVoucherTO transactionTO = new GLJournalVoucherTO();
		transactionTO.setCurrencyId(currency.getCurrencyId());
		transactionTO.setCurrencyCode(currency.getCurrencyPool().getCode());
		return transactionTO;
	}

	public static GLJournalVoucherTO convertPeriodToTO(Period period) {
		GLJournalVoucherTO transactionTO = new GLJournalVoucherTO();
		transactionTO.setPeriodId(period.getPeriodId().intValue());
		transactionTO.setPeriodName(period.getName());
		return transactionTO;
	}

	public static GLJournalVoucherTO convertCodeToTO(Combination combination) {
		GLJournalVoucherTO transactionTO = new GLJournalVoucherTO();
		transactionTO.setCombinationId(combination.getCombinationId());
		transactionTO.setCompanyCode(combination.getAccountByCompanyAccountId()
				.getCode());
		transactionTO.setCompanyDesc(combination.getAccountByCompanyAccountId()
				.getAccount());
		transactionTO.setCostCode(combination.getAccountByCostcenterAccountId()
				.getCode());
		transactionTO.setCostDesc(combination.getAccountByCostcenterAccountId()
				.getAccount());
		transactionTO.setNaturalCode(combination.getAccountByNaturalAccountId()
				.getCode());
		transactionTO.setNaturalDesc(combination.getAccountByNaturalAccountId()
				.getAccount());
		if (null != combination.getAccountByAnalysisAccountId()) {
			transactionTO.setAnalyisCode(combination
					.getAccountByAnalysisAccountId().getCode());
			transactionTO.setAnalyisDesc(combination
					.getAccountByAnalysisAccountId().getAccount());
		} else {
			transactionTO.setAnalyisCode("");
			transactionTO.setAnalyisDesc("");
		}
		if (null != combination.getAccountByBuffer1AccountId()) {
			transactionTO.setBuffer1Code(combination
					.getAccountByBuffer1AccountId().getCode());
			transactionTO.setBuffer1Desc(combination
					.getAccountByBuffer1AccountId().getAccount());
		} else {
			transactionTO.setBuffer1Code("");
			transactionTO.setBuffer1Desc("");
		}
		if (null != combination.getAccountByBuffer2AccountId()) {
			transactionTO.setBuffer2Code(combination
					.getAccountByBuffer2AccountId().getCode());
			transactionTO.setBuffer2Desc(combination
					.getAccountByBuffer2AccountId().getAccount());
		} else {
			transactionTO.setBuffer2Code("");
			transactionTO.setBuffer2Desc("");
		}
		return transactionTO;
	}

	public static GLJournalVoucherTO convertDetailToTO(TransactionDetail detail) {
		GLJournalVoucherTO transactionTO = new GLJournalVoucherTO();
		transactionTO.setCombinationId(detail.getCombination()
				.getCombinationId());
		transactionTO.setJournalLineId(detail.getTransactionDetailId()
				.intValue());
		transactionTO.setLinesDecription(detail.getDescription());
		transactionTO.setLinesAmount(detail.getAmount());
		transactionTO.setAmount(AIOSCommons.formatAmount(BigDecimal
				.valueOf(detail.getAmount())));
		transactionTO.setIsDebit(detail.getIsDebit());
		transactionTO.setCompanyCode(detail.getCombination()
				.getAccountByCompanyAccountId().getCode());
		transactionTO.setCompanyDesc(detail.getCombination()
				.getAccountByCompanyAccountId().getAccount());
		transactionTO.setCostCode(detail.getCombination()
				.getAccountByCostcenterAccountId().getCode());
		transactionTO.setCostDesc(detail.getCombination()
				.getAccountByCostcenterAccountId().getAccount());

		transactionTO.setNaturalCode(detail.getCombination()
				.getAccountByNaturalAccountId().getCode());
		transactionTO.setNaturalDesc(detail.getCombination()
				.getAccountByNaturalAccountId().getAccount());

		if (null != detail.getCombination().getAccountByAnalysisAccountId()) {
			transactionTO.setAnalyisCode(detail.getCombination()
					.getAccountByAnalysisAccountId().getCode());
			transactionTO.setAnalyisDesc(detail.getCombination()
					.getAccountByAnalysisAccountId().getAccount());
		} else {
			transactionTO.setAnalyisCode("");
			transactionTO.setAnalyisDesc("");
		}
		if (null != detail.getCombination().getAccountByBuffer1AccountId()) {
			transactionTO.setBuffer1Code(detail.getCombination()
					.getAccountByBuffer1AccountId().getCode());
			transactionTO.setBuffer1Desc(detail.getCombination()
					.getAccountByBuffer1AccountId().getAccount());
		} else {
			transactionTO.setBuffer1Code("");
			transactionTO.setBuffer1Desc("");
		}
		if (null != detail.getCombination().getAccountByBuffer2AccountId()) {
			transactionTO.setBuffer2Code(detail.getCombination()
					.getAccountByBuffer2AccountId().getCode());
			transactionTO.setBuffer2Desc(detail.getCombination()
					.getAccountByBuffer2AccountId().getAccount());
		} else {
			transactionTO.setBuffer2Code("");
			transactionTO.setBuffer2Desc("");
		}
		return transactionTO;
	}

	public static DepositTO convertToDepositTO(TransactionDetail detail) {
		DepositTO depositTO = new DepositTO();
		depositTO.setTransactionLineId(detail.getTransactionDetailId());
		depositTO.setCombinationId(detail.getCombination().getCombinationId());
		depositTO.setLinesDecription(detail.getDescription());
		depositTO.setIsDebit(detail.getIsDebit());
		depositTO.setLinesAmount(detail.getAmount());
		depositTO.setTransactionId(detail.getTransaction().getTransactionId());

		depositTO.setCompanyCode(detail.getCombination()
				.getAccountByCompanyAccountId().getCode());
		depositTO.setCompanyDesc(detail.getCombination()
				.getAccountByCompanyAccountId().getAccount());
		depositTO.setCostCode(detail.getCombination()
				.getAccountByCostcenterAccountId().getCode());
		depositTO.setCostDesc(detail.getCombination()
				.getAccountByCostcenterAccountId().getAccount());

		depositTO.setNaturalCode(detail.getCombination()
				.getAccountByNaturalAccountId().getCode());
		depositTO.setNaturalDesc(detail.getCombination()
				.getAccountByNaturalAccountId().getAccount());

		if (null != detail.getCombination().getAccountByAnalysisAccountId()) {
			depositTO.setAnalyisCode(detail.getCombination()
					.getAccountByAnalysisAccountId().getCode());
			depositTO.setAnalyisDesc(detail.getCombination()
					.getAccountByAnalysisAccountId().getAccount());
		} else {
			depositTO.setAnalyisCode("");
			depositTO.setAnalyisDesc("");
		}
		if (null != detail.getCombination().getAccountByBuffer1AccountId()) {
			depositTO.setBuffer1Code(detail.getCombination()
					.getAccountByBuffer1AccountId().getCode());
			depositTO.setBuffer1Desc(detail.getCombination()
					.getAccountByBuffer1AccountId().getAccount());
		} else {
			depositTO.setBuffer1Code("");
			depositTO.setBuffer1Desc("");
		}
		if (null != detail.getCombination().getAccountByBuffer2AccountId()) {
			depositTO.setBuffer2Code(detail.getCombination()
					.getAccountByBuffer2AccountId().getCode());
			depositTO.setBuffer2Desc(detail.getCombination()
					.getAccountByBuffer2AccountId().getAccount());
		} else {
			depositTO.setBuffer2Code("");
			depositTO.setBuffer2Desc("");
		}
		return depositTO;
	}

	public static DepositTO convertBankAccountDepositTO(BankAccount account) {
		DepositTO depositTO = new DepositTO();
		depositTO.setBankName(account.getBank().getBankName());
		depositTO.setBankAccount(account.getAccountNumber());
		depositTO.setBankId(account.getBank().getBankId());
		depositTO.setCombinationId(account.getCombination().getCombinationId());
		return depositTO;
	}
}
