package com.aiotech.aios.accounts.to;

import java.io.Serializable; 

public class GLJournalVoucherTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	//Common Variables
	private long journalId;
	private long currencyId;
	private int calendarId;
	private int implementationId;
	private String journalDate;
	private String description;
	private double totalAmount;
	private double linesAmount;
	private int journalLineId;
	private Long combinationId;
	private String linesDecription;
	private String amount;
	private Boolean isDebit;
	private String currencyCode;
	private String calendarName;
	private int periodId;
	private String EntryDate;
	private String periodName;
	private long categoryId;
	private long transactionDetailId;  
	private String categoryName;
	
	private int accountType;
	private long naturalAccountId;
	private long analysisAccountId;
	private long buffer1AccountId;
	private long buffer2AccountId;
	private long ledgerId; 
	private double balance;
	private Long productCombinationId;
	
	private String companyCode;
	private String companyDesc;
	private String costCode;
	private String costDesc;
	private String naturalCode;
	private String naturalDesc;
	private String analyisCode;
	private String analyisDesc;
	private String buffer1Code;
	private String buffer1Desc;
	private String buffer2Code;
	private String buffer2Desc;  
	
	//Grid variable Declarations
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id; 
	
	//Sql Return Types
	private String sqlReturnMessage; 
	private int sqlReturnStatus;
	private String sqlProgramName;
	private String journalNumber;
	
	//Getters&setters
	public long getJournalId() {
		return journalId;
	}
	public void setJournalId(long journalId) {
		this.journalId = journalId;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public int getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(int calendarId) {
		this.calendarId = calendarId;
	}
	public int getImplementationId() {
		return implementationId;
	}
	public void setImplementationId(int implementationId) {
		this.implementationId = implementationId;
	}
	public String getJournalDate() {
		return journalDate;
	}
	public void setJournalDate(String journalDate) {
		this.journalDate = journalDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getJournalLineId() {
		return journalLineId;
	}
	public void setJournalLineId(int journalLineId) {
		this.journalLineId = journalLineId;
	}
	public Long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}
	public String getLinesDecription() {
		return linesDecription;
	}
	public void setLinesDecription(String linesDecription) {
		this.linesDecription = linesDecription;
	} 
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	 
	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}
	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public int getPeriodId() {
		return periodId;
	}
	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}
	public String getPeriodName() {
		return periodName;
	}
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyDesc() {
		return companyDesc;
	}
	public void setCompanyDesc(String companyDesc) {
		this.companyDesc = companyDesc;
	}
	public String getCostCode() {
		return costCode;
	}
	public void setCostCode(String costCode) {
		this.costCode = costCode;
	}
	public String getCostDesc() {
		return costDesc;
	}
	public void setCostDesc(String costDesc) {
		this.costDesc = costDesc;
	}
	public String getNaturalCode() {
		return naturalCode;
	}
	public void setNaturalCode(String naturalCode) {
		this.naturalCode = naturalCode;
	}
	public String getNaturalDesc() {
		return naturalDesc;
	}
	public void setNaturalDesc(String naturalDesc) {
		this.naturalDesc = naturalDesc;
	}
	public String getAnalyisCode() {
		return analyisCode;
	}
	public void setAnalyisCode(String analyisCode) {
		this.analyisCode = analyisCode;
	}
	public String getAnalyisDesc() {
		return analyisDesc;
	}
	public void setAnalyisDesc(String analyisDesc) {
		this.analyisDesc = analyisDesc;
	}
	public String getBuffer1Code() {
		return buffer1Code;
	}
	public void setBuffer1Code(String buffer1Code) {
		this.buffer1Code = buffer1Code;
	}
	public String getBuffer1Desc() {
		return buffer1Desc;
	}
	public void setBuffer1Desc(String buffer1Desc) {
		this.buffer1Desc = buffer1Desc;
	}
	public String getBuffer2Code() {
		return buffer2Code;
	}
	public void setBuffer2Code(String buffer2Code) {
		this.buffer2Code = buffer2Code;
	}
	public String getBuffer2Desc() {
		return buffer2Desc;
	}
	public void setBuffer2Desc(String buffer2Desc) {
		this.buffer2Desc = buffer2Desc;
	}
	public Boolean getIsDebit() {
		return isDebit;
	}
	public void setIsDebit(Boolean isDebit) {
		this.isDebit = isDebit;
	}
	public double getLinesAmount() {
		return linesAmount;
	}
	public void setLinesAmount(double linesAmount) {
		this.linesAmount = linesAmount;
	}
	public String getEntryDate() {
		return EntryDate;
	}
	public void setEntryDate(String entryDate) {
		EntryDate = entryDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getJournalNumber() {
		return journalNumber;
	}
	public void setJournalNumber(String journalNumber) {
		this.journalNumber = journalNumber;
	}
	public long getTransactionDetailId() {
		return transactionDetailId;
	}
	public void setTransactionDetailId(long transactionDetailId) {
		this.transactionDetailId = transactionDetailId;
	}
	public long getNaturalAccountId() {
		return naturalAccountId;
	}
	public void setNaturalAccountId(long naturalAccountId) {
		this.naturalAccountId = naturalAccountId;
	}
	public long getAnalysisAccountId() {
		return analysisAccountId;
	}
	public void setAnalysisAccountId(long analysisAccountId) {
		this.analysisAccountId = analysisAccountId;
	}
	public long getBuffer1AccountId() {
		return buffer1AccountId;
	}
	public void setBuffer1AccountId(long buffer1AccountId) {
		this.buffer1AccountId = buffer1AccountId;
	}
	public long getBuffer2AccountId() {
		return buffer2AccountId;
	}
	public void setBuffer2AccountId(long buffer2AccountId) {
		this.buffer2AccountId = buffer2AccountId;
	}
	public int getAccountType() {
		return accountType;
	}
	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}
	public long getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(long ledgerId) {
		this.ledgerId = ledgerId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public Long getProductCombinationId() {
		return productCombinationId;
	}
	public void setProductCombinationId(Long productCombinationId) {
		this.productCombinationId = productCombinationId;
	} 
}
