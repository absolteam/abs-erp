package com.aiotech.aios.accounts.to.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.to.GlCalendarMasterTO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.system.domain.entity.Implementation;


public class GlCalendarMasterTOConverter {
	
	
	public static List<GlCalendarMasterTO> convertToTOList(
			List<Calendar> calendar) throws ParseException { 
		List<GlCalendarMasterTO> tosList = new ArrayList<GlCalendarMasterTO>(); 
		for (Calendar cals : calendar) {
			tosList.add(convertToTO(cals));
		} 
		return tosList;
	}

	public static GlCalendarMasterTO convertToTO(Calendar calendar) throws ParseException {
		GlCalendarMasterTO infoTO = new GlCalendarMasterTO();
		infoTO.setPeriods(calendar.getPeriods());
		infoTO.setCalendarId(calendar.getCalendarId());
		infoTO.setName(calendar.getName());
		infoTO.setStartTime(DateFormat.convertDateToString(calendar.getStartTime().toString()));
		infoTO.setEndTime(DateFormat.convertDateToString(calendar.getEndTime().toString()));
		infoTO.setCalendarStatus((calendar.getStatus()==1)?"Active":"Inactive");
		infoTO.setImplementationId(calendar.getImplementation().getImplementationId());
		if(calendar.getStatus()!=null)
		infoTO.setStatus(calendar.getStatus());
		return infoTO;
	}

	public static Calendar convertToEntity(GlCalendarMasterTO infoTO) throws Exception {

		Calendar calendar = new Calendar();
		Implementation implementation=new Implementation();
		calendar.setPeriods(infoTO.getPeriods());
		
		calendar.setCalendarId(infoTO.getCalendarId());
		calendar.setName(infoTO.getName());
		calendar.setStartTime(DateFormat.convertStringToDate(infoTO.getStartTime()));
		calendar.setEndTime(DateFormat.convertStringToDate(infoTO.getEndTime()));
		implementation.setImplementationId(infoTO.getImplementationId());
		calendar.setImplementation(implementation);
		calendar.setStatus(infoTO.getStatus());
		return calendar;
	}
	
	public static GlCalendarMasterTO convertPeroidToTO(Period period) throws ParseException {
		GlCalendarMasterTO infoTO = new GlCalendarMasterTO();
		
		infoTO.setPeriodId(period.getPeriodId());
		infoTO.setName(period.getName());
		infoTO.setStartTime(DateFormat.convertDateToString(period.getStartTime().toString()));
		infoTO.setEndTime(DateFormat.convertDateToString(period.getEndTime().toString()));
		if(period.getExtention()!=null)
		infoTO.setExtention(period.getExtention());
		infoTO.setStatus(period.getStatus());
		infoTO.setPeriodStartDate(period.getStartTime());
		return infoTO;
	}
	
	public static Period convertPeroidToEntity(GlCalendarMasterTO infoTO) throws Exception {

		Period period = new Period();
		
		period.setName(infoTO.getName());
		period.setStartTime(DateFormat.convertStringToDate(infoTO.getStartTime()));
		period.setEndTime(DateFormat.convertStringToDate(infoTO.getEndTime()));
		period.setExtention(infoTO.getExtention());

		return period;
	}
}
