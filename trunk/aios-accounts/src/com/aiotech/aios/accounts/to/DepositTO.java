package com.aiotech.aios.accounts.to;

import java.io.Serializable;

public class DepositTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String accountCode;
	private String account;
	private String companyCode;
	private String CompanyDesc;
	private String CostCode;
	private String CostDesc;
	private String naturalCode;
	private String naturalDesc;
	private String analyisCode;
	private String analyisDesc;
	private String buffer1Code;
	private String buffer1Desc;
	private String buffer2Code;
	private String buffer2Desc;
	private Boolean isDebit;
	private Boolean bankFlag;
	private Boolean creditFlag;
	private long combinationId;
	private String linesDecription;
	private double linesAmount;
	private long transactionLineId;
	private long transactionId;
	private String bankName;
	private String bankAccount;
	private long bankId;
	
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	 
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyDesc() {
		return CompanyDesc;
	}
	public void setCompanyDesc(String companyDesc) {
		CompanyDesc = companyDesc;
	}
	public String getCostCode() {
		return CostCode;
	}
	public void setCostCode(String costCode) {
		CostCode = costCode;
	}
	public String getCostDesc() {
		return CostDesc;
	}
	public void setCostDesc(String costDesc) {
		CostDesc = costDesc;
	}
	public String getNaturalCode() {
		return naturalCode;
	}
	public void setNaturalCode(String naturalCode) {
		this.naturalCode = naturalCode;
	}
	public String getNaturalDesc() {
		return naturalDesc;
	}
	public void setNaturalDesc(String naturalDesc) {
		this.naturalDesc = naturalDesc;
	}
	public String getAnalyisCode() {
		return analyisCode;
	}
	public void setAnalyisCode(String analyisCode) {
		this.analyisCode = analyisCode;
	}
	public String getAnalyisDesc() {
		return analyisDesc;
	}
	public void setAnalyisDesc(String analyisDesc) {
		this.analyisDesc = analyisDesc;
	}
	public String getBuffer1Code() {
		return buffer1Code;
	}
	public void setBuffer1Code(String buffer1Code) {
		this.buffer1Code = buffer1Code;
	}
	public String getBuffer1Desc() {
		return buffer1Desc;
	}
	public void setBuffer1Desc(String buffer1Desc) {
		this.buffer1Desc = buffer1Desc;
	}
	public String getBuffer2Code() {
		return buffer2Code;
	}
	public void setBuffer2Code(String buffer2Code) {
		this.buffer2Code = buffer2Code;
	}
	public String getBuffer2Desc() {
		return buffer2Desc;
	}
	public void setBuffer2Desc(String buffer2Desc) {
		this.buffer2Desc = buffer2Desc;
	}
	public Boolean getIsDebit() {
		return isDebit;
	}
	public void setIsDebit(Boolean isDebit) {
		this.isDebit = isDebit;
	}
	public Boolean getBankFlag() {
		return bankFlag;
	}
	public void setBankFlag(Boolean bankFlag) {
		this.bankFlag = bankFlag;
	}
	public Boolean getCreditFlag() {
		return creditFlag;
	}
	public void setCreditFlag(Boolean creditFlag) {
		this.creditFlag = creditFlag;
	}
	public long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}
	public String getLinesDecription() {
		return linesDecription;
	}
	public void setLinesDecription(String linesDecription) {
		this.linesDecription = linesDecription;
	}
	public double getLinesAmount() {
		return linesAmount;
	}
	public void setLinesAmount(double linesAmount) {
		this.linesAmount = linesAmount;
	}
	public long getTransactionLineId() {
		return transactionLineId;
	}
	public void setTransactionLineId(long transactionLineId) {
		this.transactionLineId = transactionLineId;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public long getBankId() {
		return bankId;
	}
	public void setBankId(long bankId) {
		this.bankId = bankId;
	} 
}
