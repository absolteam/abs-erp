/**
 * 
 */
package com.aiotech.aios.accounts.to;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;

/**
 * @author Usman Anwar
 * 
 */
public class QuotationTO {

	Long quotationId;
	String currency;
	String tenderNumber;
	String tenderDate;
	String tednderExpiryDate;

	String supplierNumber;
	String supplierName;

	String quotationNumber;
	String quotationDate;
	String quotationExpiryDate;
	String quotationDescription;

	String productName;
	Double unitRate;
	Double totalUnits;
	String quotationDetailDescription;
	String itemType;
	String uom;
	String quantity;
	String description;

	Double amount;
	Double totalAmount;

	List<QuotationTO> quotationDetails;

	public void convertToQuotationTO(Quotation quotation) {
		try {
			setQuotationProperties(this, quotation);
			totalAmount = 0.0;
			if (quotation.getQuotationDetails() != null) {
				this.quotationDetails = new ArrayList<QuotationTO>();
				for (QuotationDetail detail : quotation.getQuotationDetails()) {

					QuotationTO detailTO = new QuotationTO();

					detailTO.setProductName(detail.getProduct()
							.getProductName());
					detailTO.setTotalUnits(detail.getTotalUnits());
					detailTO.setUnitRate(detail.getUnitRate());
					detailTO.setQuotationDetailDescription(detail
							.getDescription());
					detailTO.setQuantity(detail.getTotalUnits().toString());

					detailTO.setAmount(detail.getTotalUnits()
							* detail.getUnitRate());
					detailTO.setItemType(detail.getProduct().getItemType()
							.toString());
					detailTO.setUom(detail.getProduct().getLookupDetailByProductUnit().getDisplayName());

					if (detail.getDescription() != null) {
						detailTO.setDescription(detail.getDescription());
					} else {
						detailTO.setDescription("");
					}

					this.quotationDetails.add(detailTO);
					
					totalAmount = totalAmount + detailTO.getAmount();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setQuotationProperties(QuotationTO to, Quotation quotation)
			throws Exception {

		if (quotation.getQuotationId() != null) {
			to.setQuotationId(quotation.getQuotationId());
		}
		to.setQuotationNumber(quotation.getQuotationNumber());
		to.setQuotationDate(quotation.getQuotationDate().toString());
		to.setQuotationExpiryDate(quotation.getExpiryDate().toString());
		if (quotation.getCurrency() != null) {
			to.setCurrency(quotation.getCurrency().getCurrencyPool().getCode());
		}

		if (quotation.getTender() != null) {
			to.setTenderNumber(quotation.getTender().getTenderNumber());
			to.setQuotationDate(quotation.getTender().getTenderDate()
					.toString());
			to.setQuotationExpiryDate(quotation.getTender().getTenderExpiry()
					.toString());
		}

		if (quotation.getSupplier() != null) {
			to.setSupplierName(quotation.getSupplier().getSupplierNumber());
		}
	}

	// ==========Getters Setters==============

	public Long getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(Long quotationId) {
		this.quotationId = quotationId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTenderNumber() {
		return tenderNumber;
	}

	public void setTenderNumber(String tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public String getTenderDate() {
		return tenderDate;
	}

	public void setTenderDate(String tenderDate) {
		this.tenderDate = tenderDate;
	}

	public String getTednderExpiryDate() {
		return tednderExpiryDate;
	}

	public void setTednderExpiryDate(String tednderExpiryDate) {
		this.tednderExpiryDate = tednderExpiryDate;
	}

	public String getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(String supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getQuotationNumber() {
		return quotationNumber;
	}

	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}

	public String getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate;
	}

	public String getQuotationExpiryDate() {
		return quotationExpiryDate;
	}

	public void setQuotationExpiryDate(String quotationExpiryDate) {
		this.quotationExpiryDate = quotationExpiryDate;
	}

	public String getQuotationDescription() {
		return quotationDescription;
	}

	public void setQuotationDescription(String quotationDescription) {
		this.quotationDescription = quotationDescription;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Double getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(Double totalUnits) {
		this.totalUnits = totalUnits;
	}

	public String getQuotationDetailDescription() {
		return quotationDetailDescription;
	}

	public void setQuotationDetailDescription(String quotationDetailDescription) {
		this.quotationDetailDescription = quotationDetailDescription;
	}

	public List<QuotationTO> getQuotationDetails() {
		return quotationDetails;
	}

	public void setQuotationDetails(List<QuotationTO> quotationDetails) {
		this.quotationDetails = quotationDetails;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
