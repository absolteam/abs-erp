package com.aiotech.aios.accounts.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyPool;
import com.aiotech.aios.accounts.service.CurrencyService;
import com.aiotech.aios.accounts.to.GlCurrencyMasterTO;

public class GlCurrencyMasterTOConverter {
	 private static CurrencyService currencyService;
	public static List<GlCurrencyMasterTO> convertToTOList(
			List<Currency> currency) throws Exception {

		List<GlCurrencyMasterTO> tosList = new ArrayList<GlCurrencyMasterTO>();

		for (Currency cals : currency) {
			tosList.add(convertCurrecyToTO(cals));
		}

		return tosList;
	}
	public static List<GlCurrencyMasterTO> convertPoolToTOList(
			List<CurrencyPool> pool) throws Exception {
			List<GlCurrencyMasterTO> tosList = new ArrayList<GlCurrencyMasterTO>();
			for (CurrencyPool currencyPool : pool) {
				tosList.add(convertPoolToTO(currencyPool));
			}

		return tosList;
	}
	public static GlCurrencyMasterTO convertCurrecyToTO(Currency currency) throws Exception {
		GlCurrencyMasterTO infoTO = new GlCurrencyMasterTO();

		infoTO.setCurrencyId(currency.getCurrencyId()); 
		if(currency.getCurrencyPool()!=null){
			infoTO.setCurrencyDesc(currency.getCurrencyPool().getName());
			infoTO.setCurrencyCode(currency.getCurrencyPool().getCode());
			infoTO.setCurrencyCountry(currency.getCurrencyPool().getCountry().getCountryName());
			infoTO.setCurrencyPoolId(currency.getCurrencyPool().getCurrencyPoolId()); 
		}
		infoTO.setDefalutFlag(currency.getStatus());
		infoTO.setDefaultCurrency(currency.getDefaultCurrency());
		return infoTO;
	}
	
	public static GlCurrencyMasterTO convertPoolToTO(CurrencyPool currencyPool) throws Exception {
		GlCurrencyMasterTO infoTO = new GlCurrencyMasterTO();
		infoTO.setCurrencyId(currencyPool.getCurrencyPoolId()); 
		infoTO.setCurrencyDesc(currencyPool.getName());
		infoTO.setCurrencyCode(currencyPool.getCode());
		infoTO.setCurrencyCountry(currencyPool.getCountry().getCountryName()); 
		return infoTO;
	}
	public static CurrencyService getCurrencyService() {
		return currencyService;
	}
	public static void setCurrencyService(CurrencyService currencyService) {
		GlCurrencyMasterTOConverter.currencyService = currencyService;
	}
}
