package com.aiotech.aios.accounts.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.EmiDetail; 
import com.aiotech.aios.accounts.domain.entity.vo.LoanChargeVO;


public class EmiDetailTO  extends EmiDetail implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String emiDueDate;
	private int lineNumber;
	private double emiDepreciation;
	private double emiCumulative;   
	private double instalment;
	private double interest;
	private double eiborAmount;
	private double loanCharge;
	private String description;
	private Boolean status;
	private byte paidStatus;
	private List<LoanChargeVO> loanCharges=new ArrayList<LoanChargeVO>();
	private List<EmiDetailTO> emiPaymentList=new ArrayList<EmiDetailTO>();
	public String getEmiDueDate() {
		return emiDueDate;
	}
	public void setEmiDueDate(String emiDueDate) {
		this.emiDueDate = emiDueDate;
	}
	public int getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	public double getEmiDepreciation() {
		return emiDepreciation;
	}
	public void setEmiDepreciation(double emiDepreciation) {
		this.emiDepreciation = emiDepreciation;
	}
	public double getEmiCumulative() {
		return emiCumulative;
	}
	public void setEmiCumulative(double emiCumulative) {
		this.emiCumulative = emiCumulative;
	}  
	public List<LoanChargeVO> getLoanCharges() {
		return loanCharges;
	}
	public void setLoanCharges(List<LoanChargeVO> loanCharges) {
		this.loanCharges = loanCharges;
	}
	public double getInstalment() {
		return instalment;
	}
	public void setInstalment(double instalment) {
		this.instalment = instalment;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public double getEiborAmount() {
		return eiborAmount;
	}
	public void setEiborAmount(double eiborAmount) {
		this.eiborAmount = eiborAmount;
	}
	public double getLoanCharge() {
		return loanCharge;
	}
	public void setLoanCharge(double loanCharge) {
		this.loanCharge = loanCharge;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public byte getPaidStatus() {
		return paidStatus;
	}
	public void setPaidStatus(byte paidStatus) {
		this.paidStatus = paidStatus;
	}
	public List<EmiDetailTO> getEmiPaymentList() {
		return emiPaymentList;
	}
	public void setEmiPaymentList(List<EmiDetailTO> emiPaymentList) {
		this.emiPaymentList = emiPaymentList;
	} 
}
