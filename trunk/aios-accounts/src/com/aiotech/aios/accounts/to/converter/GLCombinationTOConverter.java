package com.aiotech.aios.accounts.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.action.GLCombinationAction;
import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.to.GLCombinationMasterTO;


public class GLCombinationTOConverter {
	
	public static List<GLCombinationMasterTO> convertToTOList(List<Combination> combinationList) {

		List<GLCombinationMasterTO> tosList = new ArrayList<GLCombinationMasterTO>();

		for (Combination combination : combinationList) {
			tosList.add(convertToTO(combination));
		} 
		return tosList;
	}
	
	public static List<GLCombinationMasterTO> convertAccountToTOList(List<Account> accountList) { 
		List<GLCombinationMasterTO> tosList = new ArrayList<GLCombinationMasterTO>(); 
		for (Account account : accountList) {
			tosList.add(convertToTOAccount(account));
		} 
		return tosList;
	}
	
	public static List<GLCombinationMasterTO> convertAccountToList(List<Account> accountList) { 
		List<GLCombinationMasterTO> tosList = new ArrayList<GLCombinationMasterTO>(); 
		for (Account account : accountList) {
			tosList.add(convertToAccount(account));
		} 
		return tosList;
	}
	
	public static List<Combination> convertToStringList(List<String> accountList) { 
		List<Combination> toEntity = new ArrayList<Combination>();

		for (String account : accountList) {
			toEntity.add(convertStringEntity(account));
		} 
		return toEntity;
	}
	
	public static GLCombinationMasterTO convertToTO(Combination combination) {
		GLCombinationMasterTO combinationTo = new GLCombinationMasterTO(); 
		combinationTo.setCombinationId(combination.getCombinationId().longValue()); 
		combinationTo.setCompanyId(combination.getAccountByCompanyAccountId().getAccountId().intValue());
		combinationTo.setCompanyCode(combination.getAccountByCompanyAccountId().getCode());
		combinationTo.setCompanyCode(combination.getAccountByCompanyAccountId().getAccount());
		
		combinationTo.setCostAccountId(combination.getAccountByCostcenterAccountId().getAccountId().intValue());
		combinationTo.setCostCode(combination.getAccountByCostcenterAccountId().getCode());
		combinationTo.setCostCodeDesc(combination.getAccountByCostcenterAccountId().getAccount());
		
		combinationTo.setNaturalAccountId(combination.getAccountByNaturalAccountId().getAccountId().intValue());
		combinationTo.setNaturalCode(combination.getAccountByNaturalAccountId().getCode());
		combinationTo.setNaturalCodeDesc(combination.getAccountByNaturalAccountId().getAccount());
		
		if(combination.getAccountByAnalysisAccountId()!=null){
			combinationTo.setAnalyisAccountId(combination.getAccountByAnalysisAccountId().getAccountId().intValue());
			combinationTo.setAnalyisCode(combination.getAccountByAnalysisAccountId().getCode());
			combinationTo.setAnalyisCodeDesc(combination.getAccountByAnalysisAccountId().getAccount());
		}
		else{
			combinationTo.setAnalyisAccountId(0);
			combinationTo.setAnalyisCode("");
			combinationTo.setAnalyisCodeDesc("");
		}
		if(combination.getAccountByBuffer1AccountId()!=null){
			combinationTo.setBufferAccountId1(combination.getAccountByBuffer1AccountId().getAccountId().intValue());
			combinationTo.setBufferCode1(combination.getAccountByBuffer1AccountId().getCode());
			combinationTo.setBufferCode1Desc(combination.getAccountByBuffer1AccountId().getAccount());
		}
		else{
			combinationTo.setBufferAccountId1(0);
			combinationTo.setBufferCode1("");
			combinationTo.setBufferCode1Desc("");
		}
		if(combination.getAccountByBuffer2AccountId()!=null) {
			combinationTo.setBufferAccountId2(combination.getAccountByBuffer2AccountId().getAccountId().intValue());
			combinationTo.setBufferCode2(combination.getAccountByBuffer2AccountId().getCode());
			combinationTo.setBufferCode2Desc(combination.getAccountByBuffer2AccountId().getAccount());
		}
		else{
			combinationTo.setBufferAccountId2(0);
			combinationTo.setBufferCode2("");
			combinationTo.setBufferCode2Desc("");
		} 
		return combinationTo;
	}
	
	public static GLCombinationMasterTO convertToTOAccount(Account account) {
		GLCombinationMasterTO combinationTo = new GLCombinationMasterTO(); 
		combinationTo.setCompanyId(account.getAccountId().intValue());
		combinationTo.setCompanyCode(account.getCode());
		combinationTo.setDescription(account.getAccount()); 
		return combinationTo;
	}
	
	public static GLCombinationMasterTO convertToAccount(Account account){
		GLCombinationMasterTO combinationTo = new GLCombinationMasterTO(); 
		combinationTo.setAccountId(account.getAccountId().intValue());
		combinationTo.setCode(account.getCode());
		combinationTo.setAccount(account.getAccount());
		return combinationTo;
	}
	
	public static Combination convertStringEntity(String accountId){
		Combination combination= new Combination(); 
		String delimeter=","; 
		String accountArray[]=GLCombinationAction.splitArrayValues(accountId, delimeter);  
		Account account=new Account();
		account.setAccountId(Long.parseLong(accountArray[0]));
		combination.setAccountByCompanyAccountId(account);
		account=new Account();
		account.setAccountId(Long.parseLong(accountArray[1]));
		combination.setAccountByCostcenterAccountId(account);
		account=new Account();
		account.setAccountId(Long.parseLong(accountArray[2]));
		combination.setAccountByNaturalAccountId(account); 
		account=new Account();
		account.setAccountId(Long.parseLong(accountArray[3]));
		combination.setAccountByAnalysisAccountId(account);
		account=new Account();
		account.setAccountId(Long.parseLong(accountArray[4]));
		combination.setAccountByBuffer1AccountId(account);
		account=new Account();
		account.setAccountId(Long.parseLong(accountArray[5]));
		combination.setAccountByBuffer2AccountId(account);
		account=new Account();
		if(combination.getAccountByAnalysisAccountId().getAccountId()==0)
			combination.setAccountByAnalysisAccountId(null);  
		if(combination.getAccountByBuffer1AccountId().getAccountId()==0)
			combination.setAccountByBuffer1AccountId(null);
		if(combination.getAccountByBuffer2AccountId().getAccountId()==0)
			combination.setAccountByBuffer2AccountId(null);
		return combination;
	}
}
