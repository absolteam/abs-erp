package com.aiotech.aios.accounts.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Segment;
import com.aiotech.aios.accounts.to.GLChartOfAccountsTO;

public class GLChartOfAccountsTOConverter {

	public static List<GLChartOfAccountsTO> convertToTOList(
			List<Account> accountList) {

		List<GLChartOfAccountsTO> tosList = new ArrayList<GLChartOfAccountsTO>();

		for (Account account : accountList) {
			tosList.add(convertToTO(account));
		}
		return tosList;
	}

	public static GLChartOfAccountsTO convertToTO(Account account) {
		GLChartOfAccountsTO accountTo = new GLChartOfAccountsTO();
		accountTo.setSegmentId(account.getSegment().getSegmentId());
		accountTo.setSegmentName(account.getSegment().getSegmentName());
		accountTo.setAccountId(account.getAccountId());
		accountTo.setAccountCode(account.getCode());
		accountTo.setAccountDescription(account.getAccount().trim());
		if (account.getAccountType() != null)
			accountTo.setAccountTypeId(account.getAccountType());
		return accountTo;
	}

	public static Account convertToEntity(GLChartOfAccountsTO accountTo)
			throws Exception {
		Account account = new Account();
		account.getSegment().setSegmentName(accountTo.getSegmentName());
		account.getSegment().setSegmentId(accountTo.getSegmentId());
		account.setCode(accountTo.getAccountCode());
		account.setAccount(accountTo.getAccountDescription());
		account.setAccountType(accountTo.getAccountTypeId());
		return account;
	}

	// Check without segment
	public static GLChartOfAccountsTO convertAccountToTO(Account account) {
		GLChartOfAccountsTO accountTo = new GLChartOfAccountsTO();
		accountTo.setSegmentId(account.getSegment().getSegmentId());
		accountTo.setSegmentName(account.getSegment().getSegmentName());
		accountTo.setAccessId(account.getSegment().getAccessId());
		accountTo.setAccountId(account.getAccountId());
		accountTo.setAccountCode(account.getCode());
		accountTo.setAccountDescription(account.getAccount().trim());
		if (null != account.getAccountType() && account.getAccountType() > 0)
			accountTo.setAccountTypeId(account.getAccountType());

		if (account.getLookupDetail() != null) {
			accountTo.setLookupDetailId(account.getLookupDetail()
					.getLookupDetailId());
			accountTo.setLookupDisplayName(account.getLookupDetail()
					.getDisplayName());
		}
		return accountTo;
	}

	// For segments
	public static List<GLChartOfAccountsTO> convertSegmentToTOList(
			List<Segment> segmentList) {

		List<GLChartOfAccountsTO> tosList = new ArrayList<GLChartOfAccountsTO>();

		for (Segment segment : segmentList) {
			tosList.add(convertSegmentToTO(segment));
		}
		return tosList;
	}

	public static GLChartOfAccountsTO convertSegmentToTO(Segment segment) {
		GLChartOfAccountsTO segmentTo = new GLChartOfAccountsTO();
		segmentTo.setSegmentId(segment.getSegmentId());
		segmentTo.setSegmentName(segment.getSegmentName());
		return segmentTo;
	}
}
