 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!--[if lt IE 7]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->

<html class='no-js' lang='en'>
	<!--<![endif]-->
	
	<head>
		<meta charset='utf-8' />
		<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
		<title>AIO Photo Slider</title>
				
		<meta name="language" content="en" />
		<meta content='width=device-width, initial-scale=1.0' name='viewport' />
		
		<link rel="stylesheet" href="resources/css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="resources/css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
		
		<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		
		<style type="text/css" media="screen">			
			
			#holder {
				overflow: hidden;
				margin: 0 auto;
			}
			 
			#maximage {
				position:relative !important;
			}
			
			body {
				background: none repeat scroll 0% 0% rgb(248, 248, 248);
				font-family: wf_segoe-ui_normal,'Segoe UI',Segoe,'Segoe WP',Tahoma,"Trebuchet MS", Arial, Helvetica, sans-serif;
			}
			
			.notify {
				margin: 0 auto;
				display: block;
				width: 300px;
				padding-top: 10%;
				color: #888;
				font-size: 18px;
				font-weight: 300;
				text-align: center;
				text-transform: uppercase;
			}
			
			.loading {
				width: 100%;
				height: 100%;
				display: block;
				z-index: 999;
				background: #FFF;
				position: absolute;
			}
			.workingImage {
				position: fixed;
				top: 11%;
				left: 34%;
				background: #fff;
			}
		</style>
		
		<!--[if IE 6]>
			<style type="text/css" media="screen">
				#gradient {display:none;}
			</style>
		<![endif]-->
	</head>
	<body>
		<!-- <div class="loading">
			&nbsp;
			<img class="workingImage" src="resources/images/loading_dots.gif" title="Syncing images from Server" />			
		</div> -->
		<s:if test="imagesToRender != null">
			<s:if test="%{imagesToRender.isEmpty()}">
				<p class="notify">
					<img src="resources/images/error.png" width="200" />
					<br/><br/>
					<span style="color: rgb(255, 26, 26); border-bottom: 1px solid #ddd; ">
						No image found
					</span> 
					<br/>
					<span style="font-size: 11px; text-transform: initial;" >
						Please make sure images are uploaded and configured
					</span>
				</p>						
			</s:if>
			<s:else>
				<div id="holder">
					<div id="maximage">			
						<s:iterator value="imagesToRender" var="imageName">
							<img src="resources/images/implementation/${companyKey}/${display}/${imageName}" alt="" />
						</s:iterator>					
					</div>	
				</div>
			</s:else>
		</s:if>
		<s:else>
			<p class="notify">
				<img src="resources/images/error.png" width="200" />
				<br/><br/>
				<span style="color: rgb(255, 26, 26); border-bottom: 1px solid #ddd; ">
					Company key or access type seems missing
				</span> 
				<br/>
				<span style="font-size: 11px; text-transform: initial;" >
					Please make sure images are configured and slider path in address bar is correct
				</span>
			</p>
		</s:else>		
		
		<script src='resources/js/jquery-1.8.3.js'></script>
		<script src="resources/js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
		<script src="resources/js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
		
		<script type="text/javascript" charset="utf-8">
			$(function() {
				    
			 	if(window.location.href.indexOf("display=v") > -1) {
	            
					/* var width = window.innerWidth;					
					var tweakWidth_H = 0.55;
					
					$("#holder").width(width - (width * tweakWidth_H)); */
															
					jQuery('#maximage').maximage({
						fillElement: '#holder',
						backgroundSize: 'contain',
						cssBackgroundSize: false,
					});	
					
         		} else {
         			
         			var height = window.innerHeight;					
					var tweakHeight = 0.01;
					
					//$("#holder").width("98%");
					//$("#holder").css("margin-top", "1%");
															
					jQuery('#maximage').maximage({
						fillElement: '#holder',
						backgroundSize: 'contain',
						cssBackgroundSize: false,
					});	
         			//jQuery('#maximage').maximage({ });	
         		}
				
				//setTimeout('location.reload(true);', 300000);
			});
			
			/* $(window).load(function() {
				$(".loading").fadeOut();
			}); */
		</script>
  </body>
</html>