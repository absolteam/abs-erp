package com.aiotech.aios.photoslider.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.ServletActionContext;

public class ImageSync {

	final static String getImagesNamesGetTag = "liveImagesNamesGetAction";
	final static String liveImageGetTag = "liveImagesURL";

	public static Boolean syncImagesFromServer(String localImageStorePath,
			String companyKey, String display, Properties props)
			throws Exception {

		String serverImageNames = getCurrentImagesNamesFromServer(props
				.getProperty(getImagesNamesGetTag)
				+ "?companyKey="
				+ companyKey + "&display=" + display);
		String[] imageNamesGotFromServer = splitValues(serverImageNames, "#@");

		List<String> liveImages = new ArrayList<String>(
				Arrays.asList(imageNamesGotFromServer));

		File imagesFolder = new File(localImageStorePath);
		File[] listOfImages = imagesFolder.listFiles();

		File toDelete;

		List<String> localImages = new ArrayList<String>();

		for (int i = 0; listOfImages != null && i < listOfImages.length; i++) {

			if (listOfImages[i].isFile()) {

				localImages.add(listOfImages[i].getName());
			}
		}

		// delete all local images which are available in local but not
		// available on live
		for (String localImage : localImages) {
			if (!liveImages.contains(localImage)) {
				toDelete = new File(imagesFolder + File.separator + localImage);
				if (toDelete.exists())
					toDelete.delete();
			}
		}

		// down load all new images from server i.e. that are available in live
		// but not in local
		for (String liveImage : liveImages) {
			if (!localImages.contains(liveImage)) {
				downloadImageFromServer(props.getProperty(liveImageGetTag)
						+ companyKey + "/" + display + "/" + liveImage,
						imagesFolder + File.separator + liveImage);
			}
		}

		return true;
	}

	static String getCurrentImagesNamesFromServer(String serverSliderURL)
			throws Exception {

		URL url = new URL(serverSliderURL);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		return IOUtils.toString(in, encoding);
	}

	static void downloadImageFromServer(String imageURL, String saveLocation)
			throws Exception {

		URL url = new URL(imageURL.replace(" ", "%20"));
		URLConnection connection = url.openConnection();
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		InputStream inputStream = connection.getInputStream();

		BufferedImage bufferedImage = ImageIO.read(inputStream);
		ImageIO.write(bufferedImage, "PNG", new File(saveLocation));
	}

	private static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}
}
