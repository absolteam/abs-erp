package com.aiotech.aios.photoslider.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.omg.PortableInterceptor.SUCCESSFUL;

import com.aiotech.aios.photoslider.util.ImageSync;

public class SliderAction {

	final String sliderImageFolder = "aios/slider/";
	final String webappsImageFolder = "resources/images/implementation/";
	final String confFile = "slider.conf";
	final String driveTag = "file.drive";
	final String appNameTag = "war.name";
	final String WEBINF = "WEB-INF";
	final String ENV = "env";
	public String display = "h";
	List<String> imagesToRender;
	String companyKey;
	String access = "live";
	String imageFileNames;

	public String execute() throws Exception {

		String reqURI = ServletActionContext.getRequest().getRequestURI();
		Properties props = new Properties();
		props.load(ServletActionContext.getServletContext()
				.getResourceAsStream(
						File.separator + WEBINF + File.separator + confFile));
		String deployedAppName = props.getProperty(appNameTag).trim();

		if (reqURI.contains(deployedAppName)) {

			String[] param = reqURI.split(deployedAppName + "/");

			if (param == null || param.length <= 1
					|| access.equalsIgnoreCase("live"))
				return "success";
			else
				companyKey = param[1];

			String imagesPath = props.getProperty(driveTag).trim()
					.concat(sliderImageFolder)
					.concat(companyKey + File.separator + display);

			try {
				if (props.getProperty(ENV).equalsIgnoreCase("local"))
					ImageSync.syncImagesFromServer(imagesPath, companyKey,
							display, props);
			} catch (Exception e) {
				System.out.println("Syncing images from live server FAILED.");
				e.printStackTrace();
			}

			String webContentCompanyImageFolder = ServletActionContext
					.getServletContext().getRealPath(
							webappsImageFolder + companyKey + File.separator
									+ display);

			File imagesFolder = new File(imagesPath);
			File[] listOfImages = imagesFolder.listFiles();

			imagesToRender = new ArrayList<String>();

			for (int i = 0; listOfImages != null && i < listOfImages.length; i++) {

				if (listOfImages[i].isFile()) {

					if (new File(webContentCompanyImageFolder + File.separator
							+ listOfImages[i].getName()).exists()) {

						imagesToRender.add(listOfImages[i].getName());
					} else {

						File imageToSave = new File(
								webContentCompanyImageFolder + File.separator
										+ listOfImages[i].getName());

						FileUtils.copyFile(listOfImages[i], imageToSave);

						imagesToRender.add(imageToSave.getName());
					}
				}
			}

			if (listOfImages != null && imagesToRender != null
					&& listOfImages.length > 0) {

				handleDeletedImages(imagesToRender,
						webContentCompanyImageFolder);
			}

		}

		return "success";
	}

	private void handleDeletedImages(List<String> validImages,
			String localImagesPath) {

		try {

			File localImages = new File(localImagesPath);
			File[] allLocalImages = null;

			if (localImages.exists() && localImages.isDirectory()) {

				allLocalImages = localImages.listFiles();

				if (validImages.size() < allLocalImages.length) {

					for (File img : allLocalImages) {

						if (img.isFile()
								&& !validImages.contains(img.getName()))
							img.delete();
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String retrieveImagesNames() throws IOException {

		Properties props = new Properties();
		props.load(ServletActionContext.getServletContext()
				.getResourceAsStream(
						File.separator + WEBINF + File.separator + confFile));
		String imagesPath = props.getProperty(driveTag).trim()
				.concat(sliderImageFolder)
				.concat(companyKey + File.separator + display);

		File imagesFolder = new File(imagesPath);
		File[] listOfImages = imagesFolder.listFiles();

		imageFileNames = "";

		for (int i = 0; listOfImages != null && i < listOfImages.length; i++) {
			if (listOfImages[i].isFile()) {
				imageFileNames += listOfImages[i].getName() + "#@";
			}
		}

		return "success";
	}

	public List<String> getImagesToRender() {
		return imagesToRender;
	}

	public void setImagesToRender(List<String> imagesToRender) {
		this.imagesToRender = imagesToRender;
	}

	public String getCompanyKey() {
		return companyKey;
	}

	public void setCompanyKey(String companyKey) {
		this.companyKey = companyKey;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getImageFileNames() {
		return imageFileNames;
	}

	public void setImageFileNames(String imageFileNames) {
		this.imageFileNames = imageFileNames;
	}
}
