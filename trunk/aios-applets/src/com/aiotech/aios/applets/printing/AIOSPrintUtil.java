package com.aiotech.aios.applets.printing;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Random;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

@SuppressWarnings("serial")
public class AIOSPrintUtil extends Applet {

	String nortification = "Printing the requested document...";
	String error_ = "";
	String fileReadError = "";
	String fileWriteError = "";
	final String PROGRAMFILES = System.getenv("programfiles");
	final String WINWORD = "\\Microsoft Office\\Office12\\WINWORD.exe";
	final String COMMANDCODE = "\"/q\" \"/n\" \"/mFilePrintDefault\" \"/mFileExit\"";
	String filePath = "C:\\";
	Image image;
	MediaTracker mediaTracker;

	public void paint(Graphics g) {

		g.drawString(nortification, 10, 20);
		g.drawString(fileWriteError, 10, 40);
		g.drawString(fileReadError, 10, 60);
		g.drawString(error_, 10, 80);
	}

	public void init() {

		setLayout(null);
		setBackground(Color.white);
		setForeground(Color.gray);
		Font font = new Font("calibri", Font.PLAIN, 14);
		setFont(font);
		repaint();
		try {
			
			printDocument();
		} catch (Exception e) {
			e.printStackTrace();
			error_ = "Error: " + e.getMessage();
			repaint();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void printDocument() throws Exception {

		try {
			String caller = this.getParameter("caller");
			String template = this.getParameter("template");
			String printAction = this.getParameter("serverAddress");
			if (!PROGRAMFILES.isEmpty())
				filePath = PROGRAMFILES.substring(0, 3);
			filePath = filePath
					.concat("temp-doc-".concat(getRandom() + ".doc"));
			URL printableDoc = new URL(printAction + "?caller=" + caller
					+ "&template=" + template);

			InputStream inputStream = printableDoc.openConnection()
					.getInputStream();
			POIFSFileSystem fileSystem = new POIFSFileSystem(inputStream);
			HWPFDocument doc = new HWPFDocument(fileSystem);

			OutputStream out = (OutputStream) AccessController
					.doPrivileged(new PrivilegedAction() {
						public Object run() {
							try {
								return new FileOutputStream(new File(filePath));
							} catch (FileNotFoundException e) {
								e.printStackTrace();
								fileWriteError = "fileWriteError: "
										+ e.getMessage();
								repaint();
							}
							return null;
						}
					});

			doc.write(out);
			inputStream.close();
			out.flush();
			out.close();

			System.out.println("Document Received Successfully");
			nortification = "Document formulated, printing in progress...";
			repaint();

			try {
				String[] command = new String[] {
						"\"" + PROGRAMFILES + WINWORD + "\"",
						"\"" + filePath + "\"", COMMANDCODE };
				/*
				 * String temp=""; for(String str:command){
				 * temp=temp.concat(str); }
				 * System.out.println("Document string--"+temp);
				 */

				ProcessBuilder pb = new ProcessBuilder(command);
				Process process = pb.start();// Runtime.getRuntime().exec(pb);
				process.waitFor();

				nortification = "Document printed successfully.";
				System.out.println("Document Printed Successfully");

			} catch (Exception e) {
				e.printStackTrace();
				nortification = "Failed (MS-Word 2007 is required for printing): "
						+ e.getMessage();
			}

		} catch (Exception e) {
			e.printStackTrace();
			nortification = "Template not found on server.";
			System.out.println("Document Printing Failed. " + e.getMessage());
		} finally {
			repaint();
			deleteTempFile(filePath);
		}
	}

	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void deleteTempFile(final String filePath) {

		AccessController.doPrivileged(new PrivilegedAction() {
			public Object run() {
				try {
					new File(filePath).delete();
				} catch (Exception e) {
					e.printStackTrace();
					fileReadError = "fileReadError (delete): " + e.getMessage();
					repaint();
				}
				return null;
			}
		});
	}

	private int getRandom() {
		Random random = new Random();
		return random.nextInt(1100);
	}

	public static void main(String arg) {

	}
}
