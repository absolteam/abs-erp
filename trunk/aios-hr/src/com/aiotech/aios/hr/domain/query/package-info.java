@org.hibernate.annotations.NamedQueries({

		@org.hibernate.annotations.NamedQuery(name = "getAllPerson", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " WHERE p.implementation=? and p.status=1 AND p.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPersonWithoutStatus", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " WHERE p.implementation=? "),

		@org.hibernate.annotations.NamedQuery(name = "getPersonalDetails", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.identities id"
				+ " LEFT JOIN FETCH p.dependents dep"
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " LEFT JOIN FETCH p.experienceses exp"
				+ " LEFT JOIN FETCH p.academicQualificationses aq"
				+ " LEFT JOIN FETCH p.skillses sk"
				+ " LEFT JOIN FETCH id.lookupDetail ild"
				+ " LEFT JOIN FETCH aq.lookupDetail aqld"
				+ " LEFT JOIN FETCH sk.lookupDetail skld"
				+ " LEFT JOIN FETCH p.implementation imp"
				+ " LEFT JOIN FETCH p.jobAssignments j"
				+ " LEFT JOIN FETCH p.customersForPersonId cstm"
				+ " LEFT JOIN FETCH cstm.combination cstcb"
				+ " LEFT JOIN FETCH p.suppliers splr"
				+ " LEFT JOIN FETCH splr.combination spltcb"
				+ " LEFT JOIN FETCH j.job job" + " WHERE p.personId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllEmployee", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " WHERE ld.dataId = 1 AND p.implementation=? AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllEmployeeForJob", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " LEFT JOIN FETCH p.jobAssignments j"
				+ " LEFT JOIN FETCH j.job job"
				+ " WHERE ld.dataId = 1 AND p.implementation=? "
				+ "AND (j.isActive!=1 OR job.status!=1) AND p.status=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getAllEmployeeBasedOnJob", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " JOIN FETCH p.jobAssignments j"
				+ " WHERE ld.dataId = 1 AND p.implementation=? AND j.isActive=1 AND p.status=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getAllEmployeeBasedOnCompany", query = "SELECT Distinct(p) FROM Person p "
				+ " JOIN p.jobAssignments jp"
				+ " JOIN jp.cmpDeptLocation cdl"
				+ " JOIN cdl.company c"
				+ " WHERE c.companyId=? and jp.isActive=1 AND p.status=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getAllTenants", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " WHERE ld.dataId = 2 AND p.implementation=? AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCompanys", query = "SELECT Distinct(c) FROM Company c "
				+ " LEFT JOIN FETCH c.identities ids"
				+ " LEFT JOIN FETCH ids.lookupDetail ldi"
				+ " LEFT JOIN FETCH c.companyTypeAllocations cts"
				+ " LEFT JOIN FETCH cts.lookupDetail ld"
				+ " WHERE c.implementation=? AND c.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCompanyByCustomerId", query = "SELECT Distinct(c) FROM Company c "
				+ " WHERE c.companyId=(SELECT cst.company.companyId FROM Customer cst WHERE cst.customerId = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getPersonByCustomerId", query = "SELECT Distinct(p) FROM Person p "
				+ " WHERE p.personId=(SELECT cst.personByPersonId.personId FROM Customer cst WHERE cst.customerId = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getPersonBySecretPin", query = "SELECT Distinct(p) FROM Person p "
				+ " WHERE p.implementation = ? AND p.personNumber = ? AND p.secretPin = ?)"),

		@org.hibernate.annotations.NamedQuery(name = "getOnlyCompanies", query = "SELECT Distinct(c) FROM Company c "
				+ " LEFT JOIN FETCH c.identities ids"
				+ " LEFT JOIN FETCH ids.lookupDetail ldi"
				+ " LEFT JOIN FETCH c.companyTypeAllocations cts"
				+ " LEFT JOIN FETCH cts.lookupDetail ld"
				+ " WHERE c.implementation=? AND (ld.dataId != 8 AND ld.dataId !=6 AND ld.dataId!=2)"),

		/*@org.hibernate.annotations.NamedQuery(name = "getAllPersonAddress", query = "SELECT pa FROM PersonAddress pa "
			+ " WHERE pa.person=?"),
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllIdentity", query = "SELECT Distinct(i) FROM Identity i "
				+ " LEFT JOIN FETCH i.lookupDetail ld "
				+ " LEFT JOIN FETCH i.person p "
				+ " LEFT JOIN FETCH p.implementation impp "
				+ " WHERE i.person=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAcademicQualifications", query = "SELECT Distinct(aq) FROM AcademicQualifications aq "
				+ " LEFT JOIN FETCH aq.lookupDetail ld "
				+ " LEFT JOIN FETCH aq.person p "
				+ " LEFT JOIN FETCH aq.implementation impp "
				+ " WHERE aq.person=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllExperiences", query = "SELECT Distinct(exp) FROM Experiences exp "
				+ " LEFT JOIN FETCH exp.person p " + " WHERE exp.person=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSkills", query = "SELECT Distinct(sk) FROM Skills sk "
				+ " LEFT JOIN FETCH sk.lookupDetail ld "
				+ " LEFT JOIN FETCH sk.person p "
				+ " LEFT JOIN FETCH sk.implementation impp "
				+ " WHERE sk.person=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllIdentityOnlyByImplementation", query = "SELECT Distinct(i) FROM Identity i "
				+ " LEFT JOIN FETCH i.lookupDetail ld "
				+ " LEFT JOIN FETCH i.person p "
				+ " LEFT JOIN FETCH p.implementation impp " + " WHERE impp=?"),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityExpiry", query = "SELECT Distinct(i) FROM Identity i "
				+ " LEFT JOIN FETCH i.lookupDetail ld "
				+ " LEFT JOIN FETCH i.person p "
				+ " LEFT JOIN FETCH i.company c "
				+ " LEFT JOIN FETCH p.implementation impp "
				+ " LEFT JOIN FETCH c.implementation impc "
				+ " WHERE (i.expireDate>=? AND i.expireDate<=?) "),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityExpectedReturn", query = "SELECT Distinct(ia) FROM IdentityAvailability ia "
				+ " LEFT JOIN FETCH ia.identity i "
				+ " LEFT JOIN FETCH ia.implementation impp "
				+ " LEFT JOIN FETCH i.lookupDetail ld "
				+ " LEFT JOIN FETCH i.person p "
				+ " LEFT JOIN FETCH i.company c "
				+ " WHERE (ia.expectedReturnDate IS NOT NULL AND ia.actualReturnDate IS NULL) "
				+ " AND (ia.expectedReturnDate>=? AND ia.expectedReturnDate<=?) "),

		@org.hibernate.annotations.NamedQuery(name = "getAllDependent", query = "SELECT d FROM Dependent d "
				+ " WHERE d.person=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJob", query = "SELECT Distinct(j) FROM Job j "
				+ " LEFT JOIN FETCH j.designation des"
				+ " LEFT JOIN FETCH des.grade grd"
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpe"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH j.jobShifts js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " WHERE j.status=1 AND j.implementation=? AND j.isApprove=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobWithoutStatus", query = "SELECT Distinct(j) FROM Job j "
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpe"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH j.jobShifts js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " WHERE j.implementation=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobBasedOnCompany", query = "SELECT Distinct(j) FROM Job j "
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpe"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH j.jobShifts js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " WHERE c.companyId=? AND ((ja IS NULL OR ja.isActive=1 ) AND (jpe IS NULL OR jpe.isActive=1))"),

		@org.hibernate.annotations.NamedQuery(name = "getJob", query = "SELECT Distinct(j) FROM Job j "
				+ " LEFT JOIN FETCH j.designation des"
				+ " LEFT JOIN FETCH des.grade grd"
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpe"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH j.jobShifts js"
				+ " LEFT JOIN FETCH js.workingShift ws" + " WHERE j.jobId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getJobForDelete", query = "SELECT Distinct(j) FROM Job j "
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpe"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH j.jobShifts js"
				+ " LEFT JOIN FETCH js.workingShift ws" + " WHERE j.jobId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobAssignment", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " JOIN FETCH ja.person p"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " WHERE j.implementation=? AND j.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllActiveJobAssignment", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " JOIN FETCH ja.person p"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.resignationTerminations termin"
				+ " LEFT JOIN FETCH ja.leaveProcesses leaveProcess"
				+ " LEFT JOIN FETCH ja.employeeCalendars empCal"
				+ " LEFT JOIN FETCH empCal.employeeCalendarPeriods periods"
				+ " WHERE j.implementation=? AND j.status=1 AND ja.isActive=1 AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getJobAssignmentByPerson", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " JOIN FETCH ja.person p"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " WHERE p.personId=? and ja.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getJobAssignmentInfo", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.leave leave"
				+ " LEFT JOIN FETCH leave.lookupDetail ld"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH g.leavePolicy lp"
				+ " LEFT JOIN FETCH g.attendancePolicy ap"
				+ " JOIN FETCH ja.person p"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.leaveProcesses leaveProcess"
				+ " LEFT JOIN FETCH ja.employeeCalendars empCal"
				+ " LEFT JOIN FETCH empCal.employeeCalendarPeriods periods"
				+ " WHERE ja.jobAssignmentId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobAssignmentBasedOnCompanyWPS", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " WHERE c.companyId=? and ja.isActive=1 and j.status=1 and ja.payMode='WPS' AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobAssignmentBasedOnCompany", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH ja.leaveReconciliations lr"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " WHERE c.companyId=? and ja.isActive=1 and j.status=1 AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getJobAssignmentAgainstSwipeId", query = "SELECT ja FROM JobAssignment ja "
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH ja.person p" + " WHERE ja.swipeId=? and ja.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayrollElement", query = "SELECT Distinct(pe) FROM PayrollElement pe "
				+ " LEFT JOIN FETCH pe.combination c"
				+ " LEFT JOIN FETCH pe.payrollElement pereverse"
				+ " WHERE pe.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollElementByCode", query = "SELECT Distinct(pe) FROM PayrollElement pe "
				+ " LEFT JOIN FETCH pe.combination c"
				+ " LEFT JOIN FETCH pe.payrollElement pereverse"
				+ " WHERE pe.implementation=? and pe.elementCode=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDesignation", query = "SELECT d FROM Designation d "
				+ " JOIN FETCH d.grade g"
				+ " JOIN FETCH d.company c"
				+ " LEFT JOIN FETCH d.designation des"
				+ " WHERE d.implementation=? AND d.isActive=1 AND d.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCompanyDesignation", query = "SELECT d FROM Designation d "
				+ " JOIN FETCH d.grade g"
				+ " JOIN FETCH d.company c"
				+ " LEFT JOIN FETCH d.designation des"
				+ " WHERE c.companyId=? AND d.isActive=1 AND d.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllGrade", query = "SELECT g FROM Grade g "
				+ " WHERE g.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDesignationInfo", query = "SELECT d FROM Designation d "
				+ " JOIN FETCH d.grade g"
				+ " JOIN FETCH d.company c"
				+ " LEFT JOIN FETCH d.designation des"
				+ " WHERE d.designationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLocation", query = "SELECT Distinct(cdl) FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.companyTypeAllocations type "
				+ " LEFT JOIN FETCH type.lookupDetail ld "
				+ " WHERE c.implementation=? AND ( ld.dataId!=6 AND ld.dataId!=8 )"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSharedLocation", query = "SELECT Distinct(cdl) FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.companyTypeAllocations type "
				+ " LEFT JOIN FETCH type.lookupDetail ld "
				+ " WHERE c.implementation!=? AND cdl.dataSharing=1 AND ( ld.dataId!=6 AND ld.dataId!=8 )"),

		@org.hibernate.annotations.NamedQuery(name = "getSetupByCombinationId", query = "SELECT Distinct(cdl) FROM CmpDeptLoc cdl "
				+ " LEFT JOIN FETCH cdl.combination c "
				+ " WHERE c.combinationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getBranchAndLocationInfo", query = "SELECT Distinct(cdl) FROM CmpDeptLoc cdl "
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.companyTypeAllocations type "
				+ " LEFT JOIN FETCH type.lookupDetail ld "
				+ " WHERE cdl.cmpDeptLocId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLeave", query = "SELECT l FROM Leave l "
				+ " WHERE l.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLeaveBasedOnJob", query = "SELECT Distinct(l) FROM Leave l "
				+ " LEFT JOIN FETCH l.jobLeaves jl"
				+ " LEFT JOIN FETCH jl.job j" + " WHERE j=? and jl.isActive=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getAllShift", query = "SELECT Distinct(ws) FROM WorkingShift ws "
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ws.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllShiftWithEmployee", query = "SELECT Distinct(ws) FROM WorkingShift ws "
				+ " LEFT JOIN FETCH ws.jobShifts js"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH js.jobAssignment jaT"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH jaT.person per"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ws.implementation=? AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllActiveShift", query = "SELECT Distinct(ws) FROM WorkingShift ws "
				+ " LEFT JOIN FETCH ws.jobShifts js"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH js.jobAssignment jaT"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH jaT.person per"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ws.implementation=? AND ws.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getShiftWithEmployee", query = "SELECT Distinct(ws) FROM WorkingShift ws "
				+ " LEFT JOIN FETCH ws.jobShifts js"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH js.jobAssignment jaT"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH jaT.person per"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ws.workingShiftId=? AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getJobWithWorkingShift", query = "SELECT Distinct(j) FROM Job j "
				+ " LEFT JOIN FETCH j.jobShifts js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.person p"
				+ " WHERE j.implementation=? AND (j.status=1)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobPayrollElement", query = "SELECT jp FROM JobPayrollElement jp "
				+ " JOIN FETCH jp.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jp.payrollElementByPercentageElement percentage"
				+ " WHERE jp.job=? and jp.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobLeave", query = "SELECT jl FROM JobLeave jl "
				+ " JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " WHERE jl.isActive=1 and jl.job=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCasualJobLeave", query = "SELECT jl FROM JobLeave jl "
				+ " JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " WHERE jl.job=? and jl.isActive=1 and ld.accessCode='CL'"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShift", query = "SELECT Distinct(js) FROM JobShift js "
				+ " JOIN FETCH js.workingShift ws"
				+ " WHERE js.job=? and js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftWithDate", query = "SELECT Distinct(js) FROM JobShift js "
				+ " JOIN FETCH js.workingShift ws"
				+ " WHERE js.job=? and js.isActive=1 and js.effectiveStartDate <= ? and js.effectiveEndDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftWithLocationAndDate", query = "SELECT Distinct(js) FROM JobShift js "
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH js.job job"
				+ " LEFT JOIN FETCH job.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ja.designation desig"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.location loc1"
				+ " WHERE loc1.locationId=? AND ja.isActive=1 AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftWithLocationAndDateByAssignment", query = "SELECT Distinct(js) FROM JobShift js "
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH js.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ja.designation desig"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.location loc1"
				+ " WHERE loc1.locationId=? AND ja.isActive=1 AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftByJobAndJobAssignment", query = "SELECT Distinct(js) FROM JobShift js "
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH js.job job"
				+ " LEFT JOIN FETCH job.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ja.designation desig"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.location loc1"
				+ " WHERE ja.jobAssignmentId=? AND ja.isActive=1 AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftByJobAssignment", query = "SELECT Distinct(js) FROM JobShift js "
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH js.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ja.designation desig"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.location loc1"
				+ " WHERE ja.jobAssignmentId=? AND ja.isActive=1 AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftWithDateByAssignment", query = "SELECT js FROM JobShift js "
				+ " JOIN FETCH js.workingShift ws"
				+ " WHERE js.jobAssignment=? and js.isActive=1 and js.effectiveStartDate <= ? and js.effectiveEndDate >= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftWithOutDate", query = "SELECT js FROM JobShift js "
				+ " JOIN FETCH js.workingShift ws"
				+ " WHERE js.job=? AND js.effectiveStartDate IS NULL AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobShiftWithOutDateByAssignment", query = "SELECT js FROM JobShift js "
				+ " JOIN FETCH js.workingShift ws"
				+ " WHERE js.jobAssignment=? AND js.effectiveStartDate IS NULL AND js.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobLeaveByPerson", query = "SELECT jl FROM JobLeave jl "
				+ " JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " JOIN FETCH jl.job j"
				+ " JOIN FETCH j.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH g.leavePolicy lpolicy"
				+ " WHERE ja.person=? and ja.isActive=1 and jl.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLeaveProcessList", query = "SELECT lp FROM LeaveProcess lp "
				+ " JOIN FETCH lp.jobLeave jl"
				+ " JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " JOIN FETCH jl.job j"
				+ " JOIN FETCH lp.jobAssignment ja"
				+ " JOIN FETCH ja.person person"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH g.leavePolicy lpolicy"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.location loc"
				+ " JOIN FETCH lp.person crby"
				+ " WHERE lp.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLeaveProcessByTransactionRefrence", query = "SELECT lp FROM LeaveProcess lp "
				+ " JOIN FETCH lp.jobLeave jl"
				+ " JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " JOIN FETCH jl.job j"
				+ " WHERE lp.transactionReference=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllLeaveProcessBasedOnPerson", query = "SELECT Distinct(lp) FROM LeaveProcess lp "
				+ " LEFT JOIN FETCH lp.jobLeave jl"
				+ " LEFT JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH jl.job j"
				+ " LEFT JOIN FETCH lp.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH g.leavePolicy lpolicy"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH lp.person crby"
				+ " WHERE person.personNumber=? AND person.implementation=? and lp.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getApprovedBasedOnPersonForParticularDate", query = "SELECT lp FROM LeaveProcess lp "
				+ " LEFT JOIN FETCH lp.jobLeave jl"
				+ " LEFT JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " JOIN FETCH jl.job j"
				+ " JOIN FETCH lp.jobAssignment ja"
				+ " JOIN FETCH ja.person person"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH g.leavePolicy lpolicy"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.location loc"
				+ " JOIN FETCH lp.person crby"
				+ " WHERE person.personNumber=? "
				+ " AND person.implementation=? and lp.fromDate <= ? and lp.toDate >= ? AND lp.isApprove=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLeaveProcess", query = "SELECT lp FROM LeaveProcess lp "
				+ " LEFT JOIN FETCH lp.jobLeave jl"
				+ " LEFT JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " JOIN FETCH jl.job j"
				+ " JOIN FETCH lp.jobAssignment ja"
				+ " JOIN FETCH ja.person person"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH g.leavePolicy lpolicy"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department dept"
				+ " JOIN FETCH cdl.location loc"
				+ " JOIN FETCH lp.person crby"
				+ " WHERE lp.leaveProcessId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getJobLeaveInfo", query = "SELECT jl FROM JobLeave jl "
				+ " LEFT JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH jl.job j" + " WHERE jl.jobLeaveId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSwipeInOut", query = "SELECT sio FROM SwipeInOut sio "
				+ " WHERE sio.swipeId=? and sio.implementation=? and status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSwipeInOutWithDate", query = "SELECT sio FROM SwipeInOut sio "
				+ " WHERE sio.swipeId=? and sio.implementation=? and sio.attendanceDate=? and status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllSwipeInOutWithDateReader", query = "SELECT sio FROM SwipeInOut sio "
				+ " WHERE sio.swipeId=? and sio.implementation=? and sio.attendanceDate=? and sio.timeInOutType=? and status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAttendancePolicyInfo", query = "SELECT Distinct(ap) FROM AttendancePolicy ap "
				+ " LEFT JOIN FETCH ap.grades grd"
				+ " WHERE ap.implementation=? and grd.gradeId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAttendanceInfo", query = "SELECT a FROM Attendance a "
				+ " LEFT JOIN FETCH a.jobShift js"
				+ " LEFT JOIN FETCH  js.job j "
				+ " LEFT JOIN FETCH a.jobAssignment ja "
				+ " WHERE a.attendanceDate=? and ja.person.personId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAttendanceBasedOnPerson", query = "SELECT a FROM Attendance a "
				+ " WHERE a.person=? order by a.attendanceDate desc"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAttendance", query = "SELECT a FROM Attendance a "
				+ " LEFT JOIN FETCH a.jobShift js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ja.person=p and a.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAttendancePunchReport", query = "SELECT a FROM Attendance a "
				+ " LEFT JOIN FETCH a.jobShift js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ja.person.personId=? and a.attendanceDate>=? and a.attendanceDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCompanyAttendancePunchReport", query = "SELECT a FROM Attendance a "
				+ " LEFT JOIN FETCH a.jobShift js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE c.companyId=? and a.attendanceDate>=? and a.attendanceDate <= ? order by p.personId"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAttendancePunchReportBasedPeriod", query = "SELECT a FROM Attendance a "
				+ " LEFT JOIN FETCH a.jobShift js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE a.implementation=? and a.attendanceDate>=? and a.attendanceDate <= ? order by p.personId"),

		@org.hibernate.annotations.NamedQuery(name = "getAttendanceInfoForDevationProcess", query = "SELECT Distinct(a) FROM Attendance a "
				+ " LEFT JOIN FETCH a.jobShift js"
				+ " LEFT JOIN FETCH js.workingShift ws"
				+ " LEFT JOIN FETCH a.person p"
				+ " LEFT JOIN FETCH js.job j"
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE a.attendanceId=?"),
		@org.hibernate.annotations.NamedQuery(name = "getEmployeeCalendarPeriods", query = "SELECT ecp FROM EmployeeCalendarPeriod ecp "
				+ " JOIN FETCH ecp.employeeCalendar ec"
				//+ " JOIN FETCH ec.jobCalendars jc"
				//+ " JOIN FETCH jc.jobAssignment ja"
				+ " WHERE ecp.fromDate>=? and ecp.toDate <= ? AND ec.employeeCalendarId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getEmployeeCalendar", query = "SELECT Distinct(ec) FROM EmployeeCalendar ec "
				+ " LEFT JOIN FETCH ec.employeeCalendarPeriods ecp"
				+ " LEFT JOIN FETCH ec.location jc"
				+ " LEFT JOIN FETCH ec.jobAssignment ja"
				+ " WHERE ec.implementation=? order by ja"),

		@org.hibernate.annotations.NamedQuery(name = "getLossOfPays", query = "SELECT Distinct(lp) FROM LeaveProcess lp "
				+ " LEFT JOIN FETCH lp.jobLeave jl"
				+ " LEFT JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH jl.job j"
				+ " LEFT JOIN FETCH lp.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH lp.person crby"
				+ " WHERE person.personId=? AND person.implementation=?  "
				+ " and (lp.isApprove=1)"),

		@org.hibernate.annotations.NamedQuery(name = "getLeaveHistory", query = "SELECT Distinct(lp) FROM LeaveProcess lp "
				+ " JOIN FETCH lp.jobLeave jl"
				+ " JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " JOIN FETCH jl.job j"
				+ " JOIN FETCH lp.jobAssignment ja"
				+ " JOIN FETCH ja.person person"
				+ " JOIN FETCH lp.person crby"
				+ " WHERE person.personNumber=? AND person.implementation=? and lp.fromDate >= ?  "
				+ " and lp.toDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAttendanceDeviationForSalary", query = "SELECT a FROM Attendance a "
				+ " WHERE a.person=? and (a.attendanceDate>=? and a.attendanceDate<=?)"
				+ " AND (a.deductionType=2 or a.deductionType=3)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAttendanceForCompOff", query = "SELECT a FROM Attendance a "
				+ " WHERE a.person=? and (a.attendanceDate>=? and a.attendanceDate<=?)"
				+ " AND (a.isApprove=1)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayroll", query = "SELECT Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.jobAssignment ja"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " JOIN FETCH ja.person per"
				+ " WHERE p.implementation=? AND p.isApprove=? and p.isSalary=1 order by p.payrollId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayForAdjustment", query = "SELECT Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.jobAssignment ja"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " JOIN FETCH ja.person per"
				+ " WHERE p.implementation=? order by p.payrollId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollInfo", query = "SELECT Distinct(p) FROM Payroll p "
				+ " LEFT JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH p.payPeriodTransaction ppt"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH p.payrollTransactions pt"
				+ " LEFT JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.payrollId=? group by pe.elementCode order by pe.elementNature desc "),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollInfoPlain", query = "SELECT Distinct(p) FROM Payroll p "
				+ " LEFT JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH p.payPeriodTransaction ppt"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH p.payrollTransactions pt"
				+ " LEFT JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.payrollId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollDetail", query = "SELECT Distinct(p) FROM Payroll p "
				+ " LEFT JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH p.payPeriodTransaction ppt"
				+ " LEFT JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpes"
				+ " LEFT JOIN FETCH jpes.payrollElementByPayrollElementId pe1"
				+ " LEFT JOIN FETCH jpes.payrollElementByPercentageElement pe2"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH p.payrollTransactions pt"
				+ " LEFT JOIN FETCH pt.jobPayrollElement jpes1"
				+ " LEFT JOIN FETCH jpes1.payrollElementByPayrollElementId pe3"
				+ " LEFT JOIN FETCH jpes1.payrollElementByPercentageElement pe4"
				+ " LEFT JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.payrollId=? and pt.status!=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollDetailByUseCase", query = "SELECT Distinct(p) FROM Payroll p "
				+ " LEFT JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH p.payPeriodTransaction ppt"
				+ " LEFT JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobPayrollElements jpes"
				+ " LEFT JOIN FETCH jpes.payrollElementByPayrollElementId pe1"
				+ " LEFT JOIN FETCH jpes.payrollElementByPercentageElement pe2"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH d.grade g"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH p.payrollTransactions pt"
				+ " LEFT JOIN FETCH pt.jobPayrollElement jpes1"
				+ " LEFT JOIN FETCH jpes1.payrollElementByPayrollElementId pe3"
				+ " LEFT JOIN FETCH jpes1.payrollElementByPercentageElement pe4"
				+ " LEFT JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.recordId=? and p.useCase=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollTransactionList", query = "SELECT pt.payrollTransactionId,SUM(pt.amount),pt FROM PayrollTransaction pt "
				+ " JOIN FETCH pt.payroll p"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.payrollId=? and pt.status!=? group by pe.elementCode "
				+ "order by pe.elementNature desc "),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayrollToEmployee", query = "SELECT p FROM Payroll p "
				+ " JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " JOIN FETCH ja.person per"
				+ " WHERE p.person=? AND p.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayrollTransaction", query = "SELECT pt FROM PayrollTransaction pt "
				+ " JOIN  pt.payroll p"
				+ " JOIN  pt.payrollElement pe"
				+ " WHERE p.payrollId=?  " + ""),

		@org.hibernate.annotations.NamedQuery(name = "getPayTransactionByPayPeriodId", query = "SELECT Distinct(pt) FROM PayrollTransaction pt "
				+ " LEFT JOIN  pt.payroll p"
				+ " LEFT JOIN  pt.payrollElement pe"
				+ " LEFT JOIN  pt.payPeriodTransaction ppt"
				+ " WHERE p.jobAssignment=? AND ppt.payPeriodTransactionId=?  "),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollBasedonJob", query = "SELECT p FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " WHERE p.jobAssignment=? and p.endDate<? order by p.payrollId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollBasedonSameCombination", query = "SELECT  Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " JOIN FETCH p.jobAssignment ja"
				+ " JOIN FETCH ja.person per"
				+ " JOIN FETCH p.payPeriodTransaction ppt"
				+ " WHERE per.personId=? and ppt.payPeriodTransactionId=? and p.status!=? and p.isSalary=1"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollBasedonPayMonth", query = "SELECT  Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " JOIN FETCH p.jobAssignment ja"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " JOIN FETCH ja.person per"
				+ " JOIN FETCH p.payPeriodTransaction ppt"
				+ " WHERE per.personId=? AND p.payMonth=? and p.isSalary=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayrollBasedonPayMonth", query = "SELECT  Distinct(p) FROM Payroll p "
				+ " LEFT JOIN FETCH p.payrollTransactions pt"
				+ " LEFT JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH p.payPeriodTransaction ppt"
				+ " WHERE p.implementation=? and p.payMonth=? and p.isSalary=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayrollToWPS", query = "SELECT Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH per.identities ids"
				+ " JOIN FETCH p.payPeriodTransaction ppt"
				+ " WHERE p.jobAssignment=? AND p.payMonth=? and ja.payMode='WPS' and p.isSalary=1  "),

		@org.hibernate.annotations.NamedQuery(name = "getAllCommittedPayrolls", query = "SELECT Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.personBank pb"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH per.identities ids"
				+ " JOIN FETCH p.payPeriodTransaction ppt"
				+ " WHERE p.implementation=? and p.status=? and p.isSalary=1  "),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayrollForMonth", query = "SELECT Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobPayrollElements jp"
				+ " LEFT JOIN FETCH jp.payrollElementByPayrollElementId pe1"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " LEFT JOIN FETCH pe.combination combin"
				+ " JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH per.identities ids"
				+ " WHERE p.implementation=? And p.payMonth=? AND p.isApprove=? "),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollBasedonPerson", query = "SELECT Distinct(p) FROM Payroll p "
				+ " JOIN FETCH p.payrollTransactions pt"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobPayrollElements jp"
				+ " LEFT JOIN FETCH jp.payrollElementByPayrollElementId pe1"
				+ " JOIN FETCH ja.designation d"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " LEFT JOIN FETCH pe.combination combin"
				+ " JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH per.identities ids"
				+ " WHERE per.personId=? AND p.isSalary=1 order by p.payrollId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getPayrollBasedonPersonAndMonth", query = "SELECT Distinct(p) FROM Payroll p "
				+ " LEFT JOIN FETCH p.payrollTransactions pt"
				+ " LEFT JOIN FETCH pt.payrollElement pe"
				+ " LEFT JOIN FETCH p.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.job j"
				+ " LEFT JOIN FETCH j.jobPayrollElements jp"
				+ " LEFT JOIN FETCH jp.payrollElementByPayrollElementId pe1"
				+ " LEFT JOIN FETCH ja.designation d"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " LEFT JOIN FETCH pe.combination combin"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH per.identities ids"
				+ " WHERE per.personId=? AND p.payMonth=? AND p.isSalary=1  order by p.payrollId desc"),

		@org.hibernate.annotations.NamedQuery(name = "getLOPPayrollTransaction", query = "SELECT pt FROM PayrollTransaction pt "
				+ " JOIN FETCH pt.payroll p"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.payrollId=? and pe.elementCode='LOP' " + ""),

		@org.hibernate.annotations.NamedQuery(name = "getOTPayrollTransaction", query = "SELECT pt FROM PayrollTransaction pt "
				+ " JOIN FETCH pt.payroll p"
				+ " JOIN FETCH pt.payrollElement pe"
				+ " WHERE p.payrollId=? and pe.elementCode='OT' " + ""),

		@org.hibernate.annotations.NamedQuery(name = "getAllDepartments", query = "SELECT d FROM Department d "
				+ " WHERE d.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllDepartmentsBasedOnCompany", query = "SELECT d FROM Department d "
				+ " JOIN FETCH d.company c"
				+ " WHERE c.companyId=? group by d "),

		@org.hibernate.annotations.NamedQuery(name = "getAllLocations", query = "SELECT Distinct(l)  FROM Location l "
				+ " LEFT JOIN FETCH l.state s"
				+ " LEFT JOIN FETCH l.country c"
				+ " LEFT JOIN FETCH s.cities cy"
				+ " LEFT JOIN FETCH l.city cty" + " WHERE l.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCompanySetup", query = "SELECT cdl FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " WHERE c=? and cdl.isActive=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getAllLocationBasedOnCompany", query = "SELECT Distinct(l) FROM Location l "
				+ " LEFT JOIN FETCH l.state s"
				+ " LEFT JOIN FETCH l.country c"
				+ " LEFT JOIN FETCH s.cities cy"
				+ " LEFT JOIN FETCH l.company c"
				+ " JOIN FETCH l.city cty"
				+ " WHERE  c.companyId=? AND cy.cityId=cty.cityId  group by l "),

		@org.hibernate.annotations.NamedQuery(name = "getSetupCDLBased", query = "SELECT cdl FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.department d"
				+ " JOIN FETCH cdl.location l"
				+ " WHERE c.companyId =? AND d.departmentId=? AND l.locationId=? AND cdl.isActive=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getSetupCLBased", query = "SELECT cdl FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " WHERE c.companyId =? AND cdl.department.departmentId is null AND l.locationId=? AND cdl.isActive=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getSetupDepartmentBased", query = "SELECT cdl FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " JOIN FETCH cdl.department d" + " WHERE d.departmentId =? "),

		@org.hibernate.annotations.NamedQuery(name = "getSetupLocationBased", query = "SELECT cdl FROM CmpDeptLoc cdl "
				+ " JOIN FETCH cdl.company c"
				+ " JOIN FETCH cdl.location l"
				+ " JOIN FETCH cdl.department d" + " WHERE l.locationId =? "),

		@org.hibernate.annotations.NamedQuery(name = "getCompanyInfo", query = "SELECT Distinct(c) FROM Company c "
				+ " LEFT JOIN FETCH c.bankAccount ba"
				+ " LEFT JOIN FETCH c.identities ids"
				+ " LEFT JOIN FETCH ids.lookupDetail ldi"
				+ " LEFT JOIN FETCH c.companyTypeAllocations cts"
				+ " LEFT JOIN FETCH cts.lookupDetail ld"
				+ " LEFT JOIN FETCH c.customers cstm"
				+ " LEFT JOIN FETCH cstm.combination cstcb"
				+ " LEFT JOIN FETCH c.suppliers splr"
				+ " LEFT JOIN FETCH splr.combination spltcb"
				+ " WHERE  c.companyId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getEmployeeCalendarList", query = "SELECT Distinct(ec) FROM EmployeeCalendar ec "
				+ " LEFT JOIN FETCH ec.employeeCalendarPeriods ecp"
				+ " LEFT JOIN FETCH ec.jobAssignment ja"
				+ " LEFT JOIN FETCH ec.location l"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " WHERE  ec.implementation=? "),

		@org.hibernate.annotations.NamedQuery(name = "getEmployeeCalendarInfo", query = "SELECT Distinct(ec) FROM EmployeeCalendar ec "
				+ " LEFT JOIN FETCH ec.employeeCalendarPeriods ecp"
				+ " LEFT JOIN FETCH ec.jobAssignment ja"
				+ " LEFT JOIN FETCH ec.location l"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " WHERE  ec.employeeCalendarId=? order by ecp.fromDate "),

		@org.hibernate.annotations.NamedQuery(name = "getEmployeeCalendarByEmployee", query = "SELECT Distinct(ec) FROM EmployeeCalendar ec "
				+ " LEFT JOIN FETCH ec.employeeCalendarPeriods ecp"
				+ " LEFT JOIN FETCH ec.jobAssignment ja"
				+ " LEFT JOIN FETCH ec.location l"
				+ " LEFT JOIN FETCH ja.person per"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " WHERE per.personId=? order by ecp.fromDate "),

		@org.hibernate.annotations.NamedQuery(name = "getAllLocationList", query = "SELECT Distinct(l) FROM Location l "
				+ " JOIN FETCH l.state s"
				+ " JOIN FETCH l.country c"
				+ " JOIN FETCH s.cities cy"
				+ " JOIN FETCH l.company c"
				+ " JOIN FETCH l.city cty"
				+ " WHERE  l.implementation=? AND cy.cityId=cty.cityId "),

		@org.hibernate.annotations.NamedQuery(name = "getLeaveReconciliation", query = "SELECT Distinct(lc) FROM LeaveReconciliation lc "
				+ " LEFT JOIN FETCH lc.jobLeave jl"
				+ " LEFT JOIN FETCH jl.leave l"
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH lc.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person per"
				+ " WHERE  per.personNumber=? AND lc.implementation=? and lc.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPersonTypeAllocation", query = "SELECT Distinct(pta) FROM PersonTypeAllocation pta "
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " LEFT JOIN FETCH pta.person p" + " WHERE  p=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCompanyTypeAllocation", query = "SELECT Distinct(cta) FROM CompanyTypeAllocation cta "
				+ " LEFT JOIN FETCH cta.lookupDetail ld"
				+ " LEFT JOIN FETCH cta.company c" + " WHERE  c=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetUsageList", query = "SELECT Distinct(asst) FROM AssetUsage asst "
				+ " LEFT JOIN FETCH asst.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH asst.product prod"
				+ " LEFT JOIN FETCH asst.personByPersonId person"
				+ " LEFT JOIN FETCH asst.personByCreatedBy createdby"
				+ " LEFT JOIN FETCH asst.lookupDetailByAssetColor colr"
				+ " LEFT JOIN FETCH asst.lookupDetailByAssetType type"
				+ " LEFT JOIN FETCH asst.assetCreation assmaster"
				+ " WHERE  asst.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAssetUsageInfo", query = "SELECT Distinct(asst) FROM AssetUsage asst "
				+ " LEFT JOIN FETCH asst.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH asst.product prod"
				+ " LEFT JOIN FETCH asst.personByPersonId person"
				+ " LEFT JOIN FETCH asst.personByCreatedBy createdby"
				+ " LEFT JOIN FETCH asst.lookupDetailByAssetColor colr"
				+ " LEFT JOIN FETCH asst.lookupDetailByAssetType type"
				+ " LEFT JOIN FETCH asst.assetCreation assmaster"
				+ " WHERE  asst.assetUsageId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPersonBanksByPerson", query = "SELECT Distinct(pb) FROM PersonBank pb "
				+ " LEFT JOIN FETCH pb.person p"
				+ " WHERE  p.personId=? AND pb.isActive=1 AND p.status=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPersonBank", query = "SELECT Distinct(pb) FROM PersonBank pb "
				+ " LEFT JOIN FETCH pb.person p"
				+ " WHERE  pb.implementation=? AND pb.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOwnerPersonBanks", query = "SELECT Distinct(pb) FROM PersonBank pb "
				+ " LEFT JOIN FETCH pb.person p"
				+ " LEFT JOIN FETCH pb.company c"
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH c.companyTypeAllocations cta"
				+ " LEFT JOIN FETCH pta.lookupDetail pld"
				+ " LEFT JOIN FETCH cta.lookupDetail cld"
				+ " LEFT JOIN FETCH pld.lookupMaster plm"
				+ " LEFT JOIN FETCH cld.lookupMaster clm"
				+ " WHERE  pb.implementation=? "
				+ " AND (plm.accessCode='PERSON_TYPE' OR clm.accessCode='COMPANY_TYPE')"
				+ " AND (pld.accessCode='OW' OR cld.accessCode='OW') "),

		@org.hibernate.annotations.NamedQuery(name = "getPersonBankInfo", query = "SELECT Distinct(pb) FROM PersonBank pb "
				+ " LEFT JOIN FETCH pb.person p" + " WHERE  pb.personBankId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMemoWarningList", query = "SELECT Distinct(mw) FROM MemoWarning mw "
				+ " LEFT JOIN FETCH mw.memoWarningPersons mwp"
				+ " LEFT JOIN FETCH mw.person p"
				+ " LEFT JOIN FETCH p.jobAssignments jaa"
				+ " LEFT JOIN FETCH jaa.designation desa"
				+ " LEFT JOIN FETCH jaa.cmpDeptLocation cdla"
				+ " LEFT JOIN FETCH cdla.company ca"
				+ " LEFT JOIN FETCH cdla.department da"
				+ " LEFT JOIN FETCH cdla.location la"
				+ " LEFT JOIN FETCH mwp.person per"
				+ " LEFT JOIN FETCH per.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH mw.lookupDetail ld"
				+ " WHERE mw.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCompanyBasedDepartment", query = "SELECT Distinct(d) FROM Department d "
				+ " LEFT JOIN FETCH d.company c"
				+ " WHERE c.companyId=? and d.status=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getCommonPersonList", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.jobAssignments js"
				+ " LEFT JOIN FETCH js.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH p.implementation i" + " WHERE p.status=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getPersonListForReport", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.jobAssignments js"
				+ " LEFT JOIN FETCH js.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH p.implementation i"
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH p.academicQualificationses aq"
				+ " LEFT JOIN FETCH aq.lookupDetail ldac"
				+ " LEFT JOIN FETCH p.skillses sk"
				+ " LEFT JOIN FETCH p.experienceses sk"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"),

		@org.hibernate.annotations.NamedQuery(name = "getMemoWarningInfo", query = "SELECT Distinct(mw) FROM MemoWarning mw "
				+ " LEFT JOIN FETCH mw.memoWarningPersons mwp"
				+ " LEFT JOIN FETCH mw.person p"
				+ " LEFT JOIN FETCH p.jobAssignments jaa"
				+ " LEFT JOIN FETCH jaa.designation desa"
				+ " LEFT JOIN FETCH jaa.cmpDeptLocation cdla"
				+ " LEFT JOIN FETCH cdla.company ca"
				+ " LEFT JOIN FETCH cdla.department da"
				+ " LEFT JOIN FETCH cdla.location la"
				+ " LEFT JOIN FETCH mwp.person per"
				+ " LEFT JOIN FETCH per.jobAssignments ja"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company c"
				+ " LEFT JOIN FETCH cdl.department d"
				+ " LEFT JOIN FETCH cdl.location l"
				+ " LEFT JOIN FETCH mw.lookupDetail ld"
				+ " WHERE mw.memoWarningId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getGradeList", query = "SELECT Distinct(g) FROM Grade g "
				+ " LEFT JOIN FETCH g.attendancePolicy ap"
				+ " LEFT JOIN FETCH g.leavePolicy l"
				+ " WHERE g.implementation=? and g.isActive=1 AND g.isApprove=? "),

		@org.hibernate.annotations.NamedQuery(name = "getGradeInfo", query = "SELECT Distinct(g) FROM Grade g "
				+ " LEFT JOIN FETCH g.lookupDetail insurance"
				+ " LEFT JOIN FETCH g.attendancePolicy ap"
				+ " LEFT JOIN FETCH g.leavePolicy l" + " WHERE g.gradeId=? "),

		@org.hibernate.annotations.NamedQuery(name = "findGradeByJobAssignmentId", query = "SELECT Distinct(g) FROM Grade g "
				+ " LEFT JOIN FETCH g.lookupDetail insurance"
				+ " LEFT JOIN FETCH g.attendancePolicy ap"
				+ " LEFT JOIN FETCH g.leavePolicy l"
				+ " LEFT JOIN FETCH g.designations des"
				+ " LEFT JOIN FETCH des.jobAssignments ja"
				+ " WHERE ja.jobAssignmentId=? AND ja.isActive=1 AND ja.isPrimary=1 "),

		@org.hibernate.annotations.NamedQuery(name = "findGradeByOpenPositionId", query = "SELECT Distinct(g) FROM Grade g "
				+ " LEFT JOIN FETCH g.lookupDetail insurance"
				+ " LEFT JOIN FETCH g.attendancePolicy ap"
				+ " LEFT JOIN FETCH g.leavePolicy l"
				+ " LEFT JOIN FETCH g.designations des"
				+ " LEFT JOIN FETCH des.openPositions op"
				+ " LEFT JOIN FETCH des.jobAssignments ja"
				+ " WHERE op.openPositionId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLeavePolicyList", query = "SELECT Distinct(lp) FROM LeavePolicy lp "
				+ " LEFT JOIN FETCH lp.grades g" + " WHERE lp.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAttendancePolicyList", query = "SELECT Distinct(ap) FROM AttendancePolicy ap "
				+ " LEFT JOIN FETCH ap.grades g"
				+ " WHERE ap.implementation=? AND ap.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAttendancePolicyDetail", query = "SELECT Distinct(ap) FROM AttendancePolicy ap "
				+ " LEFT JOIN FETCH ap.grades g"
				+ " WHERE ap.attendancePolicyId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLeavePolicyInfo", query = "SELECT Distinct(lp) FROM LeavePolicy lp "
				+ " LEFT JOIN FETCH lp.grades g" + " WHERE lp.leavePolicyId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLeaveList", query = "SELECT Distinct(l) FROM Leave l "
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH l.personByCreatedBy pc"
				+ " LEFT JOIN FETCH l.personByUpdatedBy pu"
				+ " WHERE l.implementation=? AND l.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveLeaveList", query = "SELECT Distinct(l) FROM Leave l "
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH l.personByCreatedBy pc"
				+ " LEFT JOIN FETCH l.personByUpdatedBy pu"
				+ " WHERE l.implementation=? and l.isActive=1 AND l.isApprove=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLeaveInfo", query = "SELECT Distinct(l) FROM Leave l "
				+ " LEFT JOIN FETCH l.lookupDetail ld"
				+ " LEFT JOIN FETCH l.personByCreatedBy pc"
				+ " LEFT JOIN FETCH l.personByUpdatedBy pu"
				+ " WHERE l.leaveId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityAvailabilityList", query = "SELECT Distinct(ia) FROM IdentityAvailability ia "
				+ " LEFT JOIN FETCH ia.identity i"
				+ " LEFT JOIN FETCH i.person per"
				+ " LEFT JOIN FETCH i.company company"
				+ " LEFT JOIN FETCH i.lookupDetail ld"
				+ " WHERE ia.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityAvailabilityMasterList", query = "SELECT Distinct(iam) FROM IdentityAvailabilityMaster iam "
				+ " LEFT JOIN FETCH iam.identityAvailabilities ia"
				+ " LEFT JOIN FETCH ia.identity i"
				+ " LEFT JOIN FETCH i.person per"
				+ " LEFT JOIN FETCH i.company company"
				+ " LEFT JOIN FETCH i.lookupDetail ld"
				+ " LEFT JOIN FETCH iam.personByCreatedBy createdBy"
				+ " LEFT JOIN FETCH iam.personByHandoverTo personByHandoverTo"
				+ " WHERE iam.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityAvailabilityMasterInfo", query = "SELECT Distinct(iam) FROM IdentityAvailabilityMaster iam "
				+ " LEFT JOIN FETCH iam.identityAvailabilities ia"
				+ " LEFT JOIN FETCH ia.identity i"
				+ " LEFT JOIN FETCH i.person per"
				+ " LEFT JOIN FETCH i.company company"
				+ " LEFT JOIN FETCH i.lookupDetail ld"
				+ " LEFT JOIN FETCH iam.personByCreatedBy createdBy"
				+ " LEFT JOIN FETCH iam.personByHandoverTo personByHandoverTo"
				+ " WHERE iam.identityAvailabilityMasterId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityAvailabilityInfo", query = "SELECT Distinct(ia) FROM IdentityAvailability ia "
				+ " LEFT JOIN FETCH ia.identity i"
				+ " LEFT JOIN FETCH i.person per"
				+ " LEFT JOIN FETCH i.company company"
				+ " LEFT JOIN FETCH i.lookupDetail ld"
				+ " WHERE ia.identityAvailabilityId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getIdentityList", query = "SELECT Distinct(i) FROM Identity i "
				+ " LEFT JOIN FETCH i.person per"
				+ " LEFT JOIN FETCH i.company company"
				+ " LEFT JOIN FETCH i.lookupDetail ld"
				+ " WHERE i.isActive=1 AND (per.implementation=? or company.implementation=?) "),

		@org.hibernate.annotations.NamedQuery(name = "getWorkingShiftList", query = "SELECT Distinct(ws) FROM WorkingShift ws "
				+ " LEFT JOIN FETCH ws.jobShifts js"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ws.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkingShiftInfo", query = "SELECT Distinct(ws) FROM WorkingShift ws "
				+ " LEFT JOIN FETCH ws.jobShifts js"
				+ " LEFT JOIN FETCH ws.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " WHERE ws.workingShiftId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllEmployeeLoan", query = "SELECT Distinct(el) FROM EmployeeLoan el "
				+ " LEFT JOIN FETCH el.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH el.personBySanctionedBy sanction"
				+ " LEFT JOIN FETCH el.personByCreatedBy crea"
				+ " LEFT JOIN FETCH el.employeeLoanRepayments elr"
				+ " WHERE el.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getEmployeeLoanDetail", query = "SELECT Distinct(el) FROM EmployeeLoan el "
				+ " LEFT JOIN FETCH el.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH ja.person p"
				+ " LEFT JOIN FETCH el.personBySanctionedBy sanction"
				+ " LEFT JOIN FETCH el.personByCreatedBy crea"
				+ " LEFT JOIN FETCH el.employeeLoanRepayments elr"
				+ " WHERE el.employeeLoanId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOpenPosition", query = "SELECT Distinct(op) FROM OpenPosition op "
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH op.person per"
				+ " WHERE op.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getOpenPositionSameCombination", query = "SELECT Distinct(op) FROM OpenPosition op "
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH op.person per"
				+ " WHERE des.designationId=? AND cdl.cmpDeptLocId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getOpenPositionById", query = "SELECT Distinct(op) FROM OpenPosition op "
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH op.person per"
				+ " WHERE op.openPositionId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobAssignmentForOpenPoistion", query = "SELECT Distinct(ja) FROM JobAssignment ja "
				+ " JOIN FETCH ja.job j"
				+ " JOIN FETCH ja.cmpDeptLocation cdl"
				+ " JOIN FETCH ja.designation d"
				+ " WHERE d=? AND cdl=? AND ja.isPrimary=1 AND ja.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllRecruitmentSource", query = "SELECT Distinct(rs) FROM RecruitmentSource rs "
				+ " LEFT JOIN FETCH rs.person per"
				+ " LEFT JOIN FETCH rs.location loc"
				+ " LEFT JOIN FETCH rs.lookupDetail look"
				+ " LEFT JOIN FETCH rs.contactses contas"
				+ " WHERE rs.implementation=?  AND rs.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getLocationDetail", query = "SELECT Distinct(l) FROM Location l "
				+ " LEFT JOIN FETCH l.company cmp"
				+ " LEFT JOIN FETCH l.country cnt"
				+ " LEFT JOIN FETCH l.state st"
				+ " LEFT JOIN FETCH l.city cty"
				+ " WHERE l.locationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getRecruitmentSourceById", query = "SELECT Distinct(rs) FROM RecruitmentSource rs "
				+ " LEFT JOIN FETCH rs.lookupDetail look"
				+ " LEFT JOIN FETCH rs.person per"
				+ " LEFT JOIN FETCH rs.location loc"
				+ " LEFT JOIN FETCH rs.contactses contas"
				+ " WHERE rs.recruitmentSourceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAdvertisingList", query = "SELECT Distinct(ap) FROM AdvertisingPanel ap "
				+ " LEFT JOIN FETCH ap.personByPostedBy postedBy"
				+ " LEFT JOIN FETCH ap.recruitmentSource rs"
				+ " LEFT JOIN FETCH rs.lookupDetail look"
				+ " LEFT JOIN FETCH ap.personByApprovedBy approvedBy"
				+ " LEFT JOIN FETCH ap.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE ap.implementation=? AND ap.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAdvertisingListWithPosition", query = "SELECT Distinct(ap) FROM AdvertisingPanel ap "
				+ " LEFT JOIN FETCH ap.personByPostedBy postedBy"
				+ " LEFT JOIN FETCH ap.recruitmentSource rs"
				+ " LEFT JOIN FETCH rs.lookupDetail look"
				+ " LEFT JOIN FETCH ap.personByApprovedBy approvedBy"
				+ " LEFT JOIN FETCH ap.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE op.openPositionId=? and rs.recruitmentSourceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAdvertisingPanelById", query = "SELECT Distinct(ap) FROM AdvertisingPanel ap "
				+ " LEFT JOIN FETCH ap.personByPostedBy postedBy"
				+ " LEFT JOIN FETCH ap.recruitmentSource rs"
				+ " LEFT JOIN FETCH rs.lookupDetail look"
				+ " LEFT JOIN FETCH ap.personByApprovedBy approvedBy"
				+ " LEFT JOIN FETCH ap.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE ap.advertisingPanelId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllCandidate", query = "SELECT Distinct(can) FROM Candidate can "
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH can.lookupDetail look"
				+ " LEFT JOIN FETCH rs.lookupDetail look1"
				+ " LEFT JOIN FETCH can.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE can.implementation=? AND can.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCandidateByStatus", query = "SELECT Distinct(can) FROM Candidate can "
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH can.lookupDetail look"
				+ " LEFT JOIN FETCH rs.lookupDetail look1"
				+ " LEFT JOIN FETCH can.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH can.interviewProcesses interpro"
				+ " LEFT JOIN FETCH interpro.interview int"
				+ " WHERE can.implementation=? and can.status=? "),

		@org.hibernate.annotations.NamedQuery(name = "getCandidateOfferByStatus", query = "SELECT Distinct(canoff) FROM CandidateOffer canoff "
				+ " JOIN FETCH canoff.candidate can"
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH can.lookupDetail look"
				+ " LEFT JOIN FETCH rs.lookupDetail look1"
				+ " LEFT JOIN FETCH can.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH can.interviewProcesses interpro"
				+ " LEFT JOIN FETCH interpro.interview int"
				+ " WHERE canoff.implementation=? and can.status=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCandidateOfferByStatuses", query = "SELECT Distinct(canoff) FROM CandidateOffer canoff "
				+ " JOIN FETCH canoff.candidate can"
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH can.lookupDetail look"
				+ " LEFT JOIN FETCH rs.lookupDetail look1"
				+ " LEFT JOIN FETCH can.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH can.interviewProcesses interpro"
				+ " LEFT JOIN FETCH interpro.interview int"
				+ " WHERE canoff.implementation=? and (can.status =? OR can.status =?) "),

		@org.hibernate.annotations.NamedQuery(name = "getCandiateById", query = "SELECT Distinct(can) FROM Candidate can "
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH rs.lookupDetail look1"
				+ " LEFT JOIN FETCH can.lookupDetail look"
				+ " LEFT JOIN FETCH can.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE can.candidateId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getCandiateOfferById", query = "SELECT Distinct(canoff) FROM CandidateOffer canoff "
				+ " JOIN FETCH canoff.candidate can"
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH rs.lookupDetail look1"
				+ " LEFT JOIN FETCH can.lookupDetail look"
				+ " LEFT JOIN FETCH can.openPosition op"
				+ " LEFT JOIN FETCH op.lookupDetailByJobType jobType"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " LEFT JOIN FETCH canoff.job job"
				+ " WHERE canoff.candidateOfferId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllJobAssignmentsWhichHasAttendenceByCompanyId", query = "SELECT Distinct(ja) FROM JobAssignment ja"
				+ " JOIN FETCH ja.person p"
				+ " JOIN ja.cmpDeptLocation cdl "
				+ " JOIN cdl.company c "
				+ " JOIN ja.attendances at "
				+ " WHERE c.companyId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllQuestionBank", query = "SELECT Distinct(qb) FROM QuestionBank qb "
				+ " LEFT JOIN FETCH qb.designation des"
				+ " LEFT JOIN FETCH des.grade grd"
				+ " LEFT JOIN FETCH qb.questionInfos qinfo"
				+ " LEFT JOIN FETCH qb.interviews int"
				+ " WHERE qb.implementation=? AND qb.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllActiveQuestionBank", query = "SELECT Distinct(qb) FROM QuestionBank qb "
				+ " LEFT JOIN FETCH qb.designation des"
				+ " LEFT JOIN FETCH des.grade grd"
				+ " LEFT JOIN FETCH qb.questionInfos qinfo"
				+ " LEFT JOIN FETCH qb.interviews int"
				+ " WHERE qb.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllValidQuestionBank", query = "SELECT Distinct(qb) FROM QuestionBank qb "
				+ " LEFT JOIN FETCH qb.designation des"
				+ " LEFT JOIN FETCH des.grade grd"
				+ " LEFT JOIN FETCH qb.questionInfos qinfo"
				+ " LEFT JOIN FETCH qb.interviews int"
				+ " WHERE des.designationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getQuestionBankById", query = "SELECT Distinct(qb) FROM QuestionBank qb "
				+ " LEFT JOIN FETCH qb.designation des"
				+ " LEFT JOIN FETCH des.grade grd"
				+ " LEFT JOIN FETCH qb.questionInfos qinfo"
				+ " LEFT JOIN FETCH qb.interviews int"
				+ " WHERE qb.questionBankId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllInterview", query = "SELECT Distinct(i) FROM Interview i "
				+ " LEFT JOIN FETCH i.questionBank qb"
				+ " LEFT JOIN FETCH i.lookupDetail look"
				+ " LEFT JOIN FETCH i.location location"
				+ " LEFT JOIN FETCH i.interviewProcesses ip"
				+ " LEFT JOIN FETCH i.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE i.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getInterviewById", query = "SELECT Distinct(i) FROM Interview i "
				+ " LEFT JOIN FETCH i.questionBank qb"
				+ " LEFT JOIN FETCH i.lookupDetail look"
				+ " LEFT JOIN FETCH i.location location"
				+ " LEFT JOIN FETCH i.interviewProcesses ip"
				+ " LEFT JOIN FETCH i.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH des.grade grad" + " WHERE i.interviewId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getInterviewProcessById", query = "SELECT Distinct(ip) FROM InterviewProcess ip "
				+ " LEFT JOIN FETCH ip.interview i"
				+ " LEFT JOIN FETCH ip.candidate can"
				+ " LEFT JOIN FETCH ip.person per"
				+ " LEFT JOIN FETCH ip.jobAssignment ja"
				+ " LEFT JOIN FETCH can.personByPersonId person"
				+ " LEFT JOIN FETCH can.recruitmentSource rs"
				+ " LEFT JOIN FETCH can.advertisingPanel ap"
				+ " LEFT JOIN FETCH i.questionBank qb"
				+ " LEFT JOIN FETCH i.lookupDetail look"
				+ " LEFT JOIN FETCH i.location location"
				+ " LEFT JOIN FETCH i.openPosition op"
				+ " LEFT JOIN FETCH op.designation des"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"

				+ " LEFT JOIN FETCH des.grade grad"
				+ " WHERE ip.interviewProcessId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPersonsByAssignment", query = "SELECT Distinct(p) FROM Person p "
				+ " LEFT JOIN FETCH p.personTypeAllocations pta"
				+ " LEFT JOIN FETCH pta.lookupDetail ld"
				+ " LEFT JOIN FETCH p.jobAssignments ja"
				+ " WHERE ja.jobAssignmentId IN (:jobAssignmentIds)"),

		@org.hibernate.annotations.NamedQuery(name = "getSwipeInOutsByJobAssignmentIds", query = "SELECT s FROM SwipeInOut s "
				+ " WHERE s.swipeId IN (select j.swipeId FROM JobAssignment j where j.jobAssignmentId IN (:jobAssignmentIds))"),

		@org.hibernate.annotations.NamedQuery(name = "getPersonsByIdWithDesignation", query = "SELECT p FROM Person p "
				+ " LEFT JOIN FETCH p.jobAssignments j"
				+ " LEFT JOIN FETCH j.designation d WHERE p.personId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayPeriod", query = "SELECT Distinct(pp) FROM PayPeriod pp "
				+ " LEFT JOIN FETCH pp.payPeriodTransactions ppt"
				+ " WHERE pp.implementation=? AND pp.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayPeriodById", query = "SELECT Distinct(pp) FROM PayPeriod pp "
				+ " LEFT JOIN FETCH pp.payPeriodTransactions ppt"
				+ " WHERE pp.payPeriodId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getActivePayPeriod", query = "SELECT Distinct(pp) FROM PayPeriod pp "
				+ " LEFT JOIN FETCH pp.payPeriodTransactions ppt"
				+ " WHERE pp.implementation=? and pp.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getHouseRentAllowanceById", query = "SELECT Distinct(hra) FROM HouseRentAllowance hra "
				+ " LEFT JOIN FETCH hra.jobAssignment ja"
				+ " LEFT JOIN FETCH hra.lookupDetail ld"
				+ " LEFT JOIN FETCH hra.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH hra.location loc"
				+ " LEFT JOIN FETCH hra.hraContractPayments conts"
				+ " LEFT JOIN FETCH hra.person per"
				+ " WHERE hra.houseRentAllowanceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllHouseRentAllowance", query = "SELECT Distinct(hra) FROM HouseRentAllowance hra "
				+ " LEFT JOIN FETCH hra.jobAssignment ja"
				+ " LEFT JOIN FETCH hra.lookupDetail ld"
				+ " LEFT JOIN FETCH hra.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH hra.location loc"
				+ " LEFT JOIN FETCH hra.hraContractPayments conts"
				+ " LEFT JOIN FETCH hra.person per"
				+ " WHERE hra.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPhoneAllowanceById", query = "SELECT Distinct(pa) FROM PhoneAllowance pa "
				+ " LEFT JOIN FETCH pa.jobAssignment ja"
				+ " LEFT JOIN FETCH pa.lookupDetailBySubscriptionType sType"
				+ " LEFT JOIN FETCH pa.lookupDetailByNetworkProvider nProvide"
				+ " LEFT JOIN FETCH pa.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH pa.person per"
				+ " WHERE pa.phoneAllowanceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPhoneAllowance", query = "SELECT Distinct(pa) FROM PhoneAllowance pa "
				+ " LEFT JOIN FETCH pa.jobAssignment ja"
				+ " LEFT JOIN FETCH pa.lookupDetailBySubscriptionType sType"
				+ " LEFT JOIN FETCH pa.lookupDetailByNetworkProvider nProvide"
				+ " LEFT JOIN FETCH pa.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH pa.person per"
				+ " WHERE pa.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getJobPayrollElementById", query = "SELECT Distinct(jp) FROM JobPayrollElement jp "
				+ " JOIN FETCH jp.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jp.payrollElementByPercentageElement pe"
				+ " LEFT JOIN FETCH jp.houseRentAllowances hras"
				+ " LEFT JOIN FETCH jp.parkingAllowances park"
				+ " LEFT JOIN FETCH jp.dependentAllowances dependent"
				+ " LEFT JOIN FETCH jp.phoneAllowances phone"
				+ " LEFT JOIN FETCH jp.fuelAllowances fuel"
				+ " LEFT JOIN FETCH jp.medicalAllowances medical"
				+ " LEFT JOIN FETCH jp.ticketAllowances ticket"
				+ " WHERE jp.jobPayrollElementId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllowanceById", query = "SELECT Distinct(a) FROM Allowance a "
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH a.lookupDetailByAllowanceType ldType"
				+ " LEFT JOIN FETCH a.lookupDetailByAllowanceSubType subType"
				+ " LEFT JOIN FETCH a.allowanceTransactions at"
				+ " LEFT JOIN FETCH a.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH a.person per" + " WHERE a.allowanceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllAllowance", query = "SELECT Distinct(a) FROM Allowance a "
				+ " LEFT JOIN FETCH a.jobAssignment ja"
				+ " LEFT JOIN FETCH a.lookupDetailByAllowanceType ldType"
				+ " LEFT JOIN FETCH a.lookupDetailByAllowanceSubType subType"
				+ " LEFT JOIN FETCH a.allowanceTransactions at"
				+ " LEFT JOIN FETCH a.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH a.person per" + " WHERE a.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getFuelAllowanceById", query = "SELECT Distinct(fa) FROM FuelAllowance fa "
				+ " LEFT JOIN FETCH fa.jobAssignment ja"
				+ " LEFT JOIN FETCH fa.lookupDetailByVehicleNumber vechi"
				+ " LEFT JOIN FETCH fa.lookupDetailByFuelCompany company"
				+ " LEFT JOIN FETCH fa.lookupDetailByCardType card"
				+ " LEFT JOIN FETCH fa.mileageLogs at"
				+ " LEFT JOIN FETCH fa.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH fa.person per"
				+ " WHERE fa.fuelAllowanceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllFuelAllowance", query = "SELECT Distinct(fa) FROM FuelAllowance fa "
				+ " LEFT JOIN FETCH fa.jobAssignment ja"
				+ " LEFT JOIN FETCH fa.lookupDetailByVehicleNumber vechi"
				+ " LEFT JOIN FETCH fa.lookupDetailByFuelCompany company"
				+ " LEFT JOIN FETCH fa.lookupDetailByCardType card"
				+ " LEFT JOIN FETCH fa.mileageLogs at"
				+ " LEFT JOIN FETCH fa.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH fa.person per"
				+ " WHERE fa.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMileageLogById", query = "SELECT Distinct(ml) FROM MileageLog ml "
				+ " LEFT JOIN FETCH ml.lookupDetailByVehicleNumber vehi"
				+ " LEFT JOIN FETCH ml.lookupDetailBySource froms"
				+ " LEFT JOIN FETCH ml.lookupDetailByDestination tos"
				+ " LEFT JOIN FETCH ml.fuelAllowance fa"
				+ " LEFT JOIN FETCH ml.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ml.personByPersonId per"
				+ " WHERE ml.mileageLogId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllMileageLog", query = "SELECT Distinct(ml) FROM MileageLog ml "
				+ " LEFT JOIN FETCH ml.lookupDetailByVehicleNumber vehi"
				+ " LEFT JOIN FETCH ml.lookupDetailBySource froms"
				+ " LEFT JOIN FETCH ml.lookupDetailByDestination tos"
				+ " LEFT JOIN FETCH ml.fuelAllowance fa"
				+ " LEFT JOIN FETCH ml.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.person person"
				+ " LEFT JOIN FETCH ml.personByPersonId per"
				+ " WHERE ml.implementation=? AND ml.isApprove=?"),

		@org.hibernate.annotations.NamedQuery(name = "getParkingAllowanceById", query = "SELECT Distinct(pa) FROM ParkingAllowance pa "
				+ " LEFT JOIN FETCH pa.jobAssignment ja"
				+ " LEFT JOIN FETCH pa.lookupDetailByParkingType sType"
				+ " LEFT JOIN FETCH pa.lookupDetailByCardCompany nProvide"
				+ " LEFT JOIN FETCH pa.lookupDetailByCardType cardType"
				+ " LEFT JOIN FETCH pa.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH pa.person per"
				+ " WHERE pa.parkingAllowanceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllParkingAllowance", query = "SELECT Distinct(pa) FROM ParkingAllowance pa "
				+ " LEFT JOIN FETCH pa.jobAssignment ja"
				+ " LEFT JOIN FETCH pa.lookupDetailByParkingType sType"
				+ " LEFT JOIN FETCH pa.lookupDetailByCardCompany nProvide"
				+ " LEFT JOIN FETCH pa.lookupDetailByCardType cardType"
				+ " LEFT JOIN FETCH pa.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH pa.person per"
				+ " WHERE pa.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getTicketAllowanceById", query = "SELECT Distinct(ta) FROM TicketAllowance ta "
				+ " LEFT JOIN FETCH ta.jobAssignment ja"
				+ " LEFT JOIN FETCH ta.lookupDetailByToDestination toDest"
				+ " LEFT JOIN FETCH ta.lookupDetailByFromDestination fromDest"
				+ " LEFT JOIN FETCH ta.lookupDetailByAirline airline"
				+ " LEFT JOIN FETCH ta.lookupDetailByAgency agency"
				+ " LEFT JOIN FETCH ta.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH ta.person per"
				+ " WHERE ta.ticketAllowanceId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllTicketAllowance", query = "SELECT Distinct(ta) FROM TicketAllowance ta "
				+ " LEFT JOIN FETCH ta.jobAssignment ja"
				+ " LEFT JOIN FETCH ta.lookupDetailByToDestination toDest"
				+ " LEFT JOIN FETCH ta.lookupDetailByFromDestination fromDest"
				+ " LEFT JOIN FETCH ta.lookupDetailByAirline airline"
				+ " LEFT JOIN FETCH ta.lookupDetailByAgency agency"
				+ " LEFT JOIN FETCH ta.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH ta.person per"
				+ " WHERE ta.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getPayByCountById", query = "SELECT Distinct(pc) FROM PayByCount pc "
				+ " LEFT JOIN FETCH pc.jobAssignment ja"
				+ " LEFT JOIN FETCH pc.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH pc.payrollElement payrollElement"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH pc.personByCreatedBy per"
				+ " LEFT JOIN FETCH pc.personByPersonId perId"
				+ " WHERE pc.payByCountId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPayByCount", query = "SELECT Distinct(pc) FROM PayByCount pc "
				+ " LEFT JOIN FETCH pc.jobAssignment ja"
				+ " LEFT JOIN FETCH pc.jobPayrollElement jpe"
				+ " LEFT JOIN FETCH pc.payrollElement payrollElement"
				+ " LEFT JOIN FETCH jpe.payrollElementByPayrollElementId pe"
				+ " LEFT JOIN FETCH jpe.payrollElementByPercentageElement pepercent"
				+ " LEFT JOIN FETCH pc.personByCreatedBy per"
				+ " LEFT JOIN FETCH pc.personByPersonId perId"
				+ " WHERE pc.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPromotionDemotion", query = "SELECT Distinct(pd) FROM PromotionDemotion pd "
				+ " LEFT JOIN FETCH pd.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.job job"
				+ " LEFT JOIN FETCH pd.job job1"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH pd.personByRequestedBy jpe"
				+ " LEFT JOIN FETCH pd.openPosition op"
				+ " LEFT JOIN FETCH pd.personByCreatedBy per"
				+ " LEFT JOIN FETCH ja.person perId"
				+ " LEFT JOIN FETCH op.designation des1"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.company cmp"
				+ " LEFT JOIN FETCH cdl1.department dept"
				+ " LEFT JOIN FETCH cdl1.location loc"
				+ " WHERE pd.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllPromotionDemotionToActivate", query = "SELECT Distinct(pd) FROM PromotionDemotion pd "
				+ " LEFT JOIN FETCH pd.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.job job"
				+ " LEFT JOIN FETCH pd.job job1"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH pd.personByRequestedBy jpe"
				+ " LEFT JOIN FETCH pd.openPosition op"
				+ " LEFT JOIN FETCH pd.personByCreatedBy per"
				+ " LEFT JOIN FETCH ja.person perId"
				+ " LEFT JOIN FETCH op.designation des1"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.company cmp1"
				+ " LEFT JOIN FETCH cdl1.department dept1"
				+ " LEFT JOIN FETCH cdl1.location loc1"
				+ " WHERE pd.implementation=? AND pd.isApprove=1 AND pd.status!=1"),

		@org.hibernate.annotations.NamedQuery(name = "getPromotionDemotionById", query = "SELECT Distinct(pd) FROM PromotionDemotion pd "
				+ " LEFT JOIN FETCH pd.jobAssignment ja"
				+ " LEFT JOIN FETCH ja.job job"
				+ " LEFT JOIN FETCH pd.job job1"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH pd.personByRequestedBy jpe"
				+ " LEFT JOIN FETCH pd.openPosition op"
				+ " LEFT JOIN FETCH pd.personByCreatedBy per"
				+ " LEFT JOIN FETCH ja.person perId"
				+ " LEFT JOIN FETCH op.designation des1"
				+ " LEFT JOIN FETCH op.cmpDeptLocation cdl1"
				+ " LEFT JOIN FETCH cdl1.company cmp1"
				+ " LEFT JOIN FETCH cdl1.department dept1"
				+ " LEFT JOIN FETCH cdl1.location loc1"
				+ " WHERE pd.promotionDemotionId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllResignationTermination", query = "SELECT Distinct(rt) FROM ResignationTermination rt "
				+ " LEFT JOIN FETCH rt.jobAssignment ja"
				+ " LEFT JOIN FETCH rt.personByRequestedBy jpe"
				+ " LEFT JOIN FETCH rt.personByCreatedBy per"
				+ " LEFT JOIN FETCH ja.job job"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ja.person perId"
				+ " WHERE rt.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllResignationTerminationToActivate", query = "SELECT Distinct(rt) FROM ResignationTermination rt "
				+ " LEFT JOIN FETCH rt.jobAssignment ja"
				+ " LEFT JOIN FETCH rt.personByRequestedBy jpe"
				+ " LEFT JOIN FETCH rt.personByCreatedBy per"
				+ " LEFT JOIN FETCH ja.job job"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ja.person perId"
				+ " WHERE rt.implementation=? AND rt.isApprove=1"),

		@org.hibernate.annotations.NamedQuery(name = "getResignationTerminationById", query = "SELECT Distinct(rt) FROM ResignationTermination rt "
				+ " LEFT JOIN FETCH rt.jobAssignment ja"
				+ " LEFT JOIN FETCH rt.personByRequestedBy jpe"
				+ " LEFT JOIN FETCH rt.personByCreatedBy per"
				+ " LEFT JOIN FETCH ja.job job"
				+ " LEFT JOIN FETCH ja.designation des"
				+ " LEFT JOIN FETCH ja.cmpDeptLocation cdl"
				+ " LEFT JOIN FETCH cdl.company cmp"
				+ " LEFT JOIN FETCH cdl.department dept"
				+ " LEFT JOIN FETCH cdl.location loc"
				+ " LEFT JOIN FETCH ja.person perId"
				+ " WHERE rt.resignationTerminationId=?"),

})
package com.aiotech.aios.hr.domain.query;