package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.TicketAllowance;
import com.aiotech.aios.hr.domain.entity.vo.JobPayrollElementVO;
import com.aiotech.aios.hr.domain.entity.vo.PayPeriodVO;
import com.aiotech.aios.hr.domain.entity.vo.TicketAllowanceVO;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.hr.service.bl.TicketAllowanceBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class TicketAllowanceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(TicketAllowanceAction.class);
	private Implementation implementation;
	private TicketAllowanceBL ticketAllowanceBL;
	private PayrollBL payrollBL;
	private String transactionDate;
	private Integer id;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private List<Object> aaData;
	private Byte payPolicy;
	private Double amount;
	private String fromDate;
	private String toDate;
	private String receiptNumber;
	private String isFinanceImpact;
	private Long payPeriodTransactionId;
	private String description;

	private Long ticketAllowanceId;
	private Long source;
	private Long destination;
	private Long airline;
	private Long agency;
	private Character travelClass;
	private String ticketType;
	private Byte numberOfTicket;
	private Double ticketCost;
	private Double balanceAmount;
	private String purchaseDate;
	
	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("jobAssignmentId",
				jobAssignmentId);
		ServletActionContext.getRequest().setAttribute("jobPayrollElementId",
				jobPayrollElementId);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getTicketList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			Map<String,String> travelClasses = ticketAllowanceBL.travelClass();
			// Find the pay period of current transaction
			PayPeriodVO payPeriodVO = new PayPeriodVO();
			payPeriodVO.setJobAssignmentId(jobAssignmentId);
			payPeriodVO.setTransactionDate(DateFormat
					.convertStringToDate(transactionDate));
			payPeriodVO.setPayPolicy(payPolicy);
			payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

			PayPeriodTransaction payPeriodTransaction = ticketAllowanceBL
					.getPayPeriodBL().findPayPeriod(payPeriodVO);

			TicketAllowanceVO ticketAllowanceVO = new TicketAllowanceVO();
			if (payPeriodTransaction != null)
				ticketAllowanceVO
						.setPayPeriodTransaction(payPeriodTransaction);

			ticketAllowanceVO.setJobAssignmentId(jobAssignmentId);
			ticketAllowanceVO.setJobPayrollElementId(jobPayrollElementId);

			List<TicketAllowance> ticketAllowances = ticketAllowanceBL
					.getTicketAllowanceService()
					.getAllTicketAllowanceByCriteria(ticketAllowanceVO);

			if (ticketAllowances != null && ticketAllowances.size() > 0) {
				TicketAllowanceVO vo = null;
				for (TicketAllowance list : ticketAllowances) {
					vo = new TicketAllowanceVO();
					vo.setTicketAllowanceId(list.getTicketAllowanceId());
					if (list.getLookupDetailByFromDestination() != null)
						vo.setFrom(list.getLookupDetailByFromDestination()
								.getDisplayName());
					if (list.getLookupDetailByToDestination() != null)
						vo.setTo(list.getLookupDetailByToDestination()
								.getDisplayName());

					if (list.getLookupDetailByAirline() != null)
						vo.setAirline(list.getLookupDetailByAirline()
								.getDisplayName());
					
					if (list.getLookupDetailByAgency() != null)
						vo.setAgency(list.getLookupDetailByAgency()
								.getDisplayName());
					
					if(list.getTravelClass()!=null)
						vo.setTicketClass(travelClasses.get(list.getTravelClass().toString()));
					vo.setTicketType(list.getTicketType());
					vo.setNumberOfTicket(list.getNumberOfTicket());
					vo.setTicketCost(list.getTicketCost());
					vo.setFromDateView(DateFormat.convertDateToString(list
							.getFromDate().toString()));
					vo.setToDateView(DateFormat.convertDateToString(list
							.getToDate().toString()));
					vo.setAmount(list.getAmount());
					if (list.getIsFinanceImpact() != null
							&& list.getIsFinanceImpact() == true)
						vo.setIsFinanceView("YES");
					else
						vo.setIsFinanceView("NO");

					aaData.add(vo);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getTicketAllowance() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			TicketAllowanceVO ticketAllowanceVO = null;
			JobPayrollElementVO jobPayrollElementVO = null;
			if (ticketAllowanceId != null && ticketAllowanceId > 0) {
				TicketAllowance ticketAllowance = ticketAllowanceBL
						.getTicketAllowanceService().getTicketAllowanceById(
								ticketAllowanceId);
				if (ticketAllowance != null)
					ticketAllowanceVO = ticketAllowanceBL
							.convertEntityToVO(ticketAllowance);
			}

			// Find the Maximum provided & actual earned amount
			{
				// Find the pay period of current transaction
				PayPeriodVO payPeriodVO = new PayPeriodVO();
				payPeriodVO.setJobAssignmentId(jobAssignmentId);
				payPeriodVO.setTransactionDate(DateFormat
						.convertStringToDate(transactionDate));
				payPeriodVO.setPayPolicy(payPolicy);
				payPeriodVO.setPayPeriodTransactionId(payPeriodTransactionId);

				PayPeriodTransaction payPeriodTransaction = ticketAllowanceBL
						.getPayPeriodBL().findPayPeriod(payPeriodVO);
				TicketAllowanceVO ticketAllowanceTempVO = new TicketAllowanceVO();
				if (payPeriodTransaction != null)
					ticketAllowanceTempVO
							.setPayPeriodTransaction(payPeriodTransaction);

				ticketAllowanceTempVO.setJobAssignmentId(jobAssignmentId);
				ticketAllowanceTempVO
						.setJobPayrollElementId(jobPayrollElementId);

				List<TicketAllowance> ticketAllowances = ticketAllowanceBL
						.getTicketAllowanceService()
						.getAllTicketAllowanceByCriteria(
								ticketAllowanceTempVO);

				jobPayrollElementVO = payrollBL.getJobPayrollInfomration(
						jobPayrollElementId, new ArrayList<Object>(
								ticketAllowances), payPeriodTransaction);

				ServletActionContext.getRequest().setAttribute("JOBPAY_INFO",
						jobPayrollElementVO);
			}


			List<LookupDetail> port = ticketAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("PORT",
							true);

			List<LookupDetail> airlines = ticketAllowanceBL
					.getLookupMasterBL().getActiveLookupDetails("AIRLINE",
							true);
			
			List<LookupDetail> agencies = ticketAllowanceBL
			.getLookupMasterBL().getActiveLookupDetails("AGENCY",
					true);
			

			ServletActionContext.getRequest().setAttribute("PORT",
					port);

			ServletActionContext.getRequest().setAttribute("AIRLINE",
					airlines);
			
			ServletActionContext.getRequest().setAttribute("AGENCY",
					agencies);
			
			Map<String,String> travelClasses = ticketAllowanceBL.travelClass();
			ServletActionContext.getRequest().setAttribute("TRAVEL_CLASS",
					travelClasses);

			ServletActionContext.getRequest().setAttribute("ALLOWANCE",
					ticketAllowanceVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveTicketAllowance() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			TicketAllowance ticketAllowance = new TicketAllowance();
			if (ticketAllowanceId != null && ticketAllowanceId > 0) {
				ticketAllowance = ticketAllowanceBL.getTicketAllowanceService()
						.getTicketAllowanceById(ticketAllowanceId);

			} else {
				ticketAllowance.setCreatedDate(new Date());
				ticketAllowance.setPerson(person);
			}
			ticketAllowance.setAmount(amount);
			if (fromDate != null && !fromDate.equals(""))
				ticketAllowance.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			if (toDate != null && !toDate.equals(""))
				ticketAllowance.setToDate(DateFormat
						.convertStringToDate(toDate));

			if (fromDate != null && !fromDate.equals(""))
				ticketAllowance.setFromDate(DateFormat
						.convertStringToDate(fromDate));
			
			if (purchaseDate != null && !purchaseDate.equals(""))
				ticketAllowance.setPurchaseDate(DateFormat
						.convertStringToDate(purchaseDate));
			ticketAllowance.setImplementation(getImplementation());

			ticketAllowance.setReceiptNumber(receiptNumber);
			if (isFinanceImpact != null
					&& isFinanceImpact.equalsIgnoreCase("true"))
				ticketAllowance.setIsFinanceImpact(true);
			else
				ticketAllowance.setIsFinanceImpact(false);
			if (jobAssignmentId != null) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				ticketAllowance.setJobAssignment(jobAssignment);
			}
			if (jobPayrollElementId != null) {
				JobPayrollElement jobPayrollElement = new JobPayrollElement();
				jobPayrollElement.setJobPayrollElementId(jobPayrollElementId);
				ticketAllowance.setJobPayrollElement(jobPayrollElement);
			}
			if (source != null && source > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(source);
				ticketAllowance.setLookupDetailByFromDestination(lookupDetail);
			}
			if (destination != null && destination > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(destination);
				ticketAllowance.setLookupDetailByToDestination(lookupDetail);
			}
			if (airline != null && airline > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(airline);
				ticketAllowance.setLookupDetailByAirline(lookupDetail);
			}
			if (agency != null && agency > 0) {
				LookupDetail lookupDetail = new LookupDetail();
				lookupDetail.setLookupDetailId(agency);
				ticketAllowance.setLookupDetailByAgency(lookupDetail);
			}
			
			if (payPeriodTransactionId != null) {
				PayPeriodTransaction payPeriodTransaction = new PayPeriodTransaction();
				payPeriodTransaction.setPayPeriodTransactionId(payPeriodTransactionId);
				ticketAllowance.setPayPeriodTransaction(payPeriodTransaction);
			}

			ticketAllowance.setDescription(description);
			ticketAllowance.setTicketCost(ticketCost);
			ticketAllowance.setTicketType(ticketType);
			ticketAllowance.setTravelClass(travelClass);
			ticketAllowance.setNumberOfTicket(numberOfTicket);
			ticketAllowance.setBalanceAmount(balanceAmount);

			returnMessage = ticketAllowanceBL
					.saveTicketAllowance(ticketAllowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("saveTicketAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the PhoneAllowance");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (ticketAllowanceId != null && ticketAllowanceId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveTicketAllowance() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String deleteTicketAllowance() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnMessage = "SUCCESS";
			TicketAllowance ticketAllowance = new TicketAllowance();
			if (ticketAllowanceId != null && ticketAllowanceId > 0) {
				ticketAllowance = ticketAllowanceBL.getTicketAllowanceService()
						.getTicketAllowanceById(ticketAllowanceId);

			}
			returnMessage = ticketAllowanceBL
					.deleteTicketAllowance(ticketAllowance);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteTicketAllowance() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteTicketAllowance() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}

	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public Byte getPayPolicy() {
		return payPolicy;
	}

	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIsFinanceImpact() {
		return isFinanceImpact;
	}

	public void setIsFinanceImpact(String isFinanceImpact) {
		this.isFinanceImpact = isFinanceImpact;
	}

	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}

	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TicketAllowanceBL getTicketAllowanceBL() {
		return ticketAllowanceBL;
	}

	public void setTicketAllowanceBL(TicketAllowanceBL ticketAllowanceBL) {
		this.ticketAllowanceBL = ticketAllowanceBL;
	}

	public Long getTicketAllowanceId() {
		return ticketAllowanceId;
	}

	public void setTicketAllowanceId(Long ticketAllowanceId) {
		this.ticketAllowanceId = ticketAllowanceId;
	}


	public Long getAirline() {
		return airline;
	}

	public void setAirline(Long airline) {
		this.airline = airline;
	}

	public Long getAgency() {
		return agency;
	}

	public void setAgency(Long agency) {
		this.agency = agency;
	}

	public Character getTravelClass() {
		return travelClass;
	}

	public void setTravelClass(Character travelClass) {
		this.travelClass = travelClass;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public Byte getNumberOfTicket() {
		return numberOfTicket;
	}

	public void setNumberOfTicket(Byte numberOfTicket) {
		this.numberOfTicket = numberOfTicket;
	}

	public Double getTicketCost() {
		return ticketCost;
	}

	public void setTicketCost(Double ticketCost) {
		this.ticketCost = ticketCost;
	}

	public Double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}


	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}

	public Long getDestination() {
		return destination;
	}

	public void setDestination(Long destination) {
		this.destination = destination;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
}
