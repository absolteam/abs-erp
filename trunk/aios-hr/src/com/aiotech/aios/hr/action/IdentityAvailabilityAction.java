package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.hr.domain.entity.IdentityAvailabilityMaster;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.IdentityAvailabilityMasterVO;
import com.aiotech.aios.hr.domain.entity.vo.IdentityAvailabilityVO;
import com.aiotech.aios.hr.service.bl.IdentityAvailabilityBL;
import com.aiotech.aios.hr.service.bl.IdentityAvailabilityMasterBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class IdentityAvailabilityAction extends ActionSupport {
	private IdentityAvailabilityBL identityAvailabilityBL;
	private IdentityAvailabilityMasterBL identityAvailabilityMasterBL;
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(IdentityAvailabilityAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long recordId;
	private Long identityAvailabilityId;
	private Long identityAvailabilityMasterId;
	private Long identityId;
	private String handoverDate;
	private String expectedReturnDate;
	private String actualReturnDate;
	private String purpose;
	private String description;
	private Byte status;
	private Boolean isActive;
	private Long alertId;
	private Long personId;
	private Long messageId;
	private String printableContent;
	private String doucmentSplits;
	private String personName;
	private String department;
	private String rowId;

	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	@SuppressWarnings("unchecked")
	public String getIdentityAvailabilityList() {

		try {
			List<IdentityAvailabilityMaster> identityAvailabilityMasterList = identityAvailabilityMasterBL
					.getIdentityAvailabilityMasterService()
					.getIdentityAvailabilityMasterList(getImplementation());

			List<IdentityAvailabilityMasterVO> identityAvailabilityVOs = identityAvailabilityMasterBL
					.convertEntitiesTOVOs(identityAvailabilityMasterList);

			JSONObject jsonResponse = new JSONObject();
			if (identityAvailabilityMasterList != null
					&& identityAvailabilityMasterList.size() > 0) {
				jsonResponse.put("iTotalRecords",
						identityAvailabilityVOs.size());
				jsonResponse.put("iTotalDisplayRecords",
						identityAvailabilityVOs.size());
			}
			JSONArray data = new JSONArray();
			for (IdentityAvailabilityMasterVO list : identityAvailabilityVOs) {

				JSONArray array = new JSONArray();
				array.add(list.getIdentityAvailabilityMasterId() + "");
				array.add(list.getReferenceNumber() + "");
				array.add(list.getPersonName() + "");
				array.add(list.getDepartment() + "");
				array.add(list.getHandoverDateStr() + "");
				array.add(list.getStatusStr() + "");
				array.add(list.getDescription() + "");
				array.add(list.getCreatedPerson() + "");
				array.add(list.getCreatedDateStr() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("getIdentityAvailabilityList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("getIdentityAvailabilityList() unsuccessful Return");
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getIdentityList() {

		try {
			List<Identity> identityList = identityAvailabilityBL
					.getIdentityAvailabilityService().getIdentityList(
							getImplementation());

			JSONObject jsonResponse = new JSONObject();
			if (identityList != null && identityList.size() > 0) {
				jsonResponse.put("iTotalRecords", identityList.size());
				jsonResponse.put("iTotalDisplayRecords", identityList.size());
			}
			JSONArray data = new JSONArray();
			for (Identity list : identityList) {

				JSONArray array = new JSONArray();
				array.add(list.getIdentityId() + "");
				if (list.getLookupDetail() != null)
					array.add(list.getLookupDetail().getDisplayName() + "");
				else
					array.add("-NA-");
				if (list.getPerson() != null)
					array.add(list.getPerson().getFirstName() + " "
							+ list.getPerson().getLastName() + "["
							+ list.getPerson().getPersonNumber() + "]");
				else if (list.getCompany() != null)
					array.add(list.getCompany().getCompanyName() + "");
				else
					array.add("-NA-");

				array.add(list.getIdentityNumber() + "");
				array.add(list.getIdentityCode() + "");
				array.add(list.getExpireDate() + "");
				array.add(list.getIssuedPlace() + "");
				array.add(list.getDescription() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("getIdentityList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("getIdentityList() unsuccessful Return");
			return ERROR;
		}
	}

	public String getIdentityAvailabilityAdd() {
		try {
			CommentVO comment = new CommentVO();

			if (recordId != null && recordId > 0) {
				identityAvailabilityMasterId = recordId;
				comment.setRecordId(identityAvailabilityMasterId);
				comment.setUseCase(IdentityAvailabilityMaster.class.getName());
				comment = identityAvailabilityMasterBL.getCommentBL()
						.getCommentInfo(comment);
			}
			IdentityAvailabilityMaster identityAvailability = new IdentityAvailabilityMaster();
			IdentityAvailabilityMasterVO identityAvailabilityVO = new IdentityAvailabilityMasterVO();
			if (identityAvailabilityMasterId != null
					&& identityAvailabilityMasterId > 0) {
				identityAvailability = identityAvailabilityMasterBL
						.getIdentityAvailabilityMasterService()
						.getIdentityAvailabilityMasterInfo(
								identityAvailabilityMasterId);
				identityAvailabilityVO = identityAvailabilityMasterBL
						.convertEntityToVO(identityAvailability);
			} else {
				identityAvailabilityVO.setHandoverDateStr(DateFormat
						.convertSystemDateToString(new Date()));
				identityAvailabilityVO.setReferenceNumber(SystemBL
						.getReferenceStamp(IdentityAvailabilityMaster.class
								.getName(), identityAvailabilityMasterBL
								.getImplementation()));
			}
			ServletActionContext.getRequest().setAttribute("HANDOVER_INFO",
					identityAvailabilityVO);
			ServletActionContext.getRequest()
					.setAttribute("recordId", recordId);
			ServletActionContext.getRequest().setAttribute("alertId", alertId);
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
					comment);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String identityAvailabilityPrint() {
		try {
			IdentityAvailabilityMaster identityAvailability = new IdentityAvailabilityMaster();
			IdentityAvailabilityMasterVO identityAvailabilityVO = new IdentityAvailabilityMasterVO();
			if (identityAvailabilityMasterId != null
					&& identityAvailabilityMasterId > 0) {
				identityAvailability = identityAvailabilityMasterBL
						.getIdentityAvailabilityMasterService()
						.getIdentityAvailabilityMasterInfo(
								identityAvailabilityMasterId);
				identityAvailabilityVO = identityAvailabilityMasterBL
						.convertEntityToVO(identityAvailability);
			}

			ServletActionContext.getRequest().setAttribute("HANDOVER_INFO",
					identityAvailabilityVO);
			ServletActionContext.getRequest().setAttribute(
					"HANDOVER_LIST_INFO",
					identityAvailabilityVO.getIdentityAvailabilityVOs());
			ServletActionContext.getRequest()
					.setAttribute("recordId", recordId);

			Configuration config = new Configuration();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/hr/"
					+ getImplementation().getImplementationId()));

			String scheme = ServletActionContext.getRequest().getScheme();
			String host = ServletActionContext.getRequest().getHeader("Host");

			// includes leading forward slash
			String contextPath = ServletActionContext.getRequest()
					.getContextPath();

			String urlPath = scheme + "://" + host + contextPath;

			Template template = config
					.getTemplate("html-document-handover-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			if (getImplementation().getMainLogo() != null) {

				byte[] bFile = FileUtil.getBytes(new File(getImplementation()
						.getMainLogo()));
				bFile = Base64.encodeBase64(bFile);

				rootMap.put("logo", new String(bFile));
			} else {
				rootMap.put("logo", "");
			}

			rootMap.put("urlPath", urlPath);
			rootMap.put("HANDOVER_INFO", identityAvailabilityVO);
			rootMap.put("HANDOVER_LIST_INFO",
					identityAvailabilityVO.getIdentityAvailabilityVOs());
			Writer out = new StringWriter();
			template.process(rootMap, out);
			printableContent = out.toString();
			// System.out.println(printableContent);

			return "template";
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String saveIdentityAvailability() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			IdentityAvailabilityMaster editIdentityAvailabilityMaster = null;
			IdentityAvailabilityMaster identityAvailabilityMaster = new IdentityAvailabilityMaster();
			JSONParser parser = new JSONParser();
			Object inventoryObject = parser.parse(doucmentSplits);
			JSONArray object = (JSONArray) inventoryObject;
			IdentityAvailability identityAvailability = null;
			List<IdentityAvailability> identityAvailabilities = new ArrayList<IdentityAvailability>();
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				JSONArray scheduleArray = (JSONArray) jsonObject
						.get("documentSplits");
				if (null != scheduleArray && scheduleArray.size() > 0) {
					for (Iterator<?> iteratorObject = scheduleArray.iterator(); iteratorObject
							.hasNext();) {
						JSONObject resourceDetail = (JSONObject) iteratorObject
								.next();
						identityAvailability = new IdentityAvailability();
						if (resourceDetail.get("identityAvailabilityId") != null
								&& !resourceDetail
										.get("identityAvailabilityId")
										.toString().equals(""))
							identityAvailability.setIdentityAvailabilityId(Long
									.valueOf(resourceDetail.get(
											"identityAvailabilityId")
											.toString()));

						if (resourceDetail.get("identityId") != null
								&& !resourceDetail.get("identityId").toString()
										.equals("")) {
							Identity identity = new Identity();
							identity.setIdentityId(Long.valueOf(resourceDetail
									.get("identityId").toString()));
							identityAvailability.setIdentity(identity);
						}

						identityAvailability.setDocumentName(resourceDetail
								.get("documentName").toString());

						identityAvailability.setHandoverDate(DateFormat
								.convertStringToDate(resourceDetail.get(
										"handoverDate").toString()));

						if (resourceDetail.get("expectedReturnDate") != null)
							identityAvailability
									.setExpectedReturnDate(DateFormat
											.convertStringToDate(resourceDetail
													.get("expectedReturnDate")
													.toString()));

						if (resourceDetail.get("purpose") != null)
							identityAvailability.setPurpose(resourceDetail.get(
									"purpose").toString());

						if (resourceDetail.get("actualReturnDate") != null)
							identityAvailability.setActualReturnDate(DateFormat
									.convertStringToDate(resourceDetail.get(
											"actualReturnDate").toString()));

						identityAvailability
								.setImplementation(identityAvailabilityMasterBL
										.getImplementation());
						identityAvailabilities.add(identityAvailability);
					}
				}
			}

			identityAvailabilityMaster.setCreatedDate(new Date());
			identityAvailabilityMaster.setHandoverDate(DateFormat
					.convertStringToDate(handoverDate));
			identityAvailabilityMaster.setDepartment(department);
			identityAvailabilityMaster.setName(personName);
			identityAvailabilityMaster.setPurpose(purpose);
			identityAvailabilityMaster.setDescription(description);
			identityAvailabilityMaster.setStatus(status);
			if(personId!=null && personId>0){
				Person person = new Person();
				person.setPersonId(personId);
				identityAvailabilityMaster.setPersonByHandoverTo(person);
			}
			Person person1 = new Person();
			person1.setPersonId(user.getPersonId());
			identityAvailabilityMaster.setPersonByCreatedBy(person1);
			identityAvailabilityMaster
					.setImplementation(identityAvailabilityMasterBL
							.getImplementation());

			List<IdentityAvailability> deleteIdentityAvailabilities = null;
			List<IdentityAvailability> editIdentityAvailabilities = null;
			if (identityAvailabilityMasterId > 0) {
				editIdentityAvailabilityMaster = identityAvailabilityMasterBL
						.getIdentityAvailabilityMasterService()
						.getIdentityAvailabilityMasterInfo(
								identityAvailabilityMasterId);
				editIdentityAvailabilities = new ArrayList<IdentityAvailability>(
						editIdentityAvailabilityMaster
								.getIdentityAvailabilities());
				deleteIdentityAvailabilities = userDeletedIdentites(
						identityAvailabilities, editIdentityAvailabilities);
				identityAvailabilityMaster
						.setReferenceNumber(editIdentityAvailabilityMaster
								.getReferenceNumber());
				identityAvailabilityMaster
						.setIdentityAvailabilityMasterId(identityAvailabilityMasterId);

			} else {
				identityAvailabilityMaster.setReferenceNumber(SystemBL
						.getReferenceStamp(IdentityAvailabilityMaster.class
								.getName(), identityAvailabilityMasterBL
								.getImplementation()));

			}
			IdentityAvailabilityVO vo = new IdentityAvailabilityVO();
			vo.setAlertId(alertId);
			identityAvailabilityMasterBL.saveIdentityAvailability(
					identityAvailabilityMaster, identityAvailabilities,
					deleteIdentityAvailabilities, vo);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");

			LOGGER.info("saveIdentityAvailability() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (identityAvailabilityMasterId > 0)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveIdentityAvailability() Unsuccessful Return");
			return ERROR;
		}
	}

	private List<IdentityAvailability> userDeletedIdentites(
			List<IdentityAvailability> identityAvailabilities,
			List<IdentityAvailability> identityAvailabilitiesOld) {
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, IdentityAvailability> hashResources = new HashMap<Long, IdentityAvailability>();
		if (null != identityAvailabilitiesOld
				&& identityAvailabilitiesOld.size() > 0) {
			for (IdentityAvailability list : identityAvailabilitiesOld) {
				listOne.add(list.getIdentityAvailabilityId());
				hashResources.put(list.getIdentityAvailabilityId(), list);
			}
			for (IdentityAvailability list : identityAvailabilities) {
				listTwo.add(list.getIdentityAvailabilityId());
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		List<IdentityAvailability> resourceList = new ArrayList<IdentityAvailability>();
		if (null != different && different.size() > 0) {
			IdentityAvailability tempResource = null;

			for (Long list : different) {
				if (null != list && !list.equals(0l)) {
					tempResource = new IdentityAvailability();
					tempResource = hashResources.get(list);
					resourceList.add(tempResource);
				}
			}
		}
		return resourceList;
	}

	public String deleteIdentityAvailability() {
		try {
			if (identityAvailabilityMasterId != null
					&& identityAvailabilityMasterId > 0) {
				identityAvailabilityMasterBL
						.deleteIdentityAvailability(identityAvailabilityMasterId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteIdentityAvailability() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteIdentityAvailability() unsuccessful Return");
			return ERROR;
		}
	}

	public IdentityAvailabilityBL getIdentityAvailabilityBL() {
		return identityAvailabilityBL;
	}

	public void setIdentityAvailabilityBL(
			IdentityAvailabilityBL identityAvailabilityBL) {
		this.identityAvailabilityBL = identityAvailabilityBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getIdentityAvailabilityId() {
		return identityAvailabilityId;
	}

	public void setIdentityAvailabilityId(Long identityAvailabilityId) {
		this.identityAvailabilityId = identityAvailabilityId;
	}

	public Long getIdentityId() {
		return identityId;
	}

	public void setIdentityId(Long identityId) {
		this.identityId = identityId;
	}

	public String getHandoverDate() {
		return handoverDate;
	}

	public void setHandoverDate(String handoverDate) {
		this.handoverDate = handoverDate;
	}

	public String getExpectedReturnDate() {
		return expectedReturnDate;
	}

	public void setExpectedReturnDate(String expectedReturnDate) {
		this.expectedReturnDate = expectedReturnDate;
	}

	public String getActualReturnDate() {
		return actualReturnDate;
	}

	public void setActualReturnDate(String actualReturnDate) {
		this.actualReturnDate = actualReturnDate;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getDoucmentSplits() {
		return doucmentSplits;
	}

	public void setDoucmentSplits(String doucmentSplits) {
		this.doucmentSplits = doucmentSplits;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public IdentityAvailabilityMasterBL getIdentityAvailabilityMasterBL() {
		return identityAvailabilityMasterBL;
	}

	public void setIdentityAvailabilityMasterBL(
			IdentityAvailabilityMasterBL identityAvailabilityMasterBL) {
		this.identityAvailabilityMasterBL = identityAvailabilityMasterBL;
	}

	public Long getIdentityAvailabilityMasterId() {
		return identityAvailabilityMasterId;
	}

	public void setIdentityAvailabilityMasterId(
			Long identityAvailabilityMasterId) {
		this.identityAvailabilityMasterId = identityAvailabilityMasterId;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

}
