package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.MemoWarning;
import com.aiotech.aios.hr.domain.entity.MemoWarningPerson;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.MemoWarningVO;
import com.aiotech.aios.hr.service.bl.MemoWarningBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class MemoWarningAction extends ActionSupport {
	private MemoWarningBL memoWarningBL;
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(MemoWarningAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private int typeId;
	private Long companyId;
	private String departmentIds;
	private String referenceNumber;
	private Byte type;
	private int subType;
	private String title;
	private String subject;
	private String createdDate;
	private String notes;
	private String message;
	private String ccList;
	private String employeeList;
	private Long memoWarningId;
	private Long recordId;
	private Byte processType;
	private Long alertId;
	private Long messageId;
	private String printableContent;
	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	public String getMemoWarningList() {

		try {
			List<MemoWarning> memoWarnings = memoWarningBL
					.getMemoWarningService().getMemoWarningList(
							getImplementation());

			List<MemoWarningVO> memoWarningVOs = memoWarningBL
					.convertEntitiesTOVOs(memoWarnings);

			JSONObject jsonResponse = new JSONObject();
			if (memoWarnings != null && memoWarnings.size() > 0) {
				jsonResponse.put("iTotalRecords", memoWarningVOs.size());
				jsonResponse.put("iTotalDisplayRecords", memoWarningVOs.size());
			}
			JSONArray data = new JSONArray();
			for (MemoWarningVO list : memoWarningVOs) {

				JSONArray array = new JSONArray();
				array.add(list.getMemoWarningId() + "");
				array.add(list.getTitle() + "");
				array.add(list.getMemoType());
				array.add(list.getMemoSubType());
				array.add(list.getReferenceNumber() + "");
				if (list.getPerson() != null)
					array.add(list.getPerson().getFirstName() + " "
							+ list.getPerson().getLastName() + "");
				else
					array.add("-NA-");
				array.add(list.getCreatedDateStr() + "");
				if (list.getIsApprove() == 3) {
					array.add("APPROVAL PENDING");
				} else if (list.getIsApprove() == 1) {
					array.add("PUPLISHED");
				} else if (list.getIsApprove() == 4) {
					array.add("PUPLISHED");
				} else if (list.getIsApprove() == 2) {
					array.add("CANCELLED");
				} else {
					array.add("-NA-");
				}

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Memo Warning List Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Memo Warning List unsuccessful Return");
			return ERROR;
		}
	}

	public String getMemoWarningAdd() {
		try {
			MemoWarningVO memoWarningVO = new MemoWarningVO();
			MemoWarning memoWarning = new MemoWarning();
			CommentVO comment = new CommentVO();

			if (recordId != null && recordId > 0) {
				memoWarningId = recordId;
				comment.setRecordId(Long.parseLong(memoWarningId + ""));
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.MemoWarning");
				comment = memoWarningBL.getCommentBL().getCommentInfo(comment);
			}
			if (memoWarningId != null && memoWarningId > 0) {

				memoWarning = memoWarningBL.getMemoWarningService()
						.getMemoWarningInfo(memoWarningId);
				memoWarningVO = memoWarningBL.convertEntityToVO(memoWarning);

			} else {
				memoWarning.setReferenceNumber(memoWarningBL.getRefereceNum());
				memoWarningVO.setMemoWarning(memoWarning);
			}

			ServletActionContext.getRequest().setAttribute("MEMO_WARNING_INFO",
					memoWarningVO);
			ServletActionContext.getRequest()
					.setAttribute("recordId", recordId);
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
					comment);
			List<Company> companys = memoWarningBL.getCompanyBL()
					.commonCompanyList();
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companys);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String getMemoWarningSubType() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();

			session.removeAttribute("MEMO_SUBTYPE_"
					+ sessionObj.get("jSessionId"));

			List<LookupDetail> lookupDetails = new ArrayList<LookupDetail>();
			String code = "MEMO_SUBTYPE";
			if (typeId == 2)
				code = "WARNING_SUBTYPE";
			lookupDetails = memoWarningBL.getLookupMasterBL()
					.getActiveLookupDetails(code, true);
			ServletActionContext.getRequest().setAttribute("SUB_TYPE",
					lookupDetails);
			session.setAttribute(
					"MEMO_SUBTYPE_" + sessionObj.get("jSessionId"),
					lookupDetails);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Memo Warning Sub Type unsuccessful Return");
			return ERROR;
		}
	}

	public String getDepartmetnList() {
		try {
			List<Department> departments = new ArrayList<Department>();

			departments = memoWarningBL.getDepartmentBL().commonDepartmentList(
					companyId);
			ServletActionContext.getRequest().setAttribute("DEPARTMENT_LIST",
					departments);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Unsuccessful Return");
			return ERROR;
		}
	}

	public String getEmployeesList() {
		try {
			List<Person> persons = new ArrayList<Person>();
			if (companyId == null || companyId == 0)
				companyId = null;

			if (departmentIds == null || departmentIds.equals("")
					|| departmentIds.equals("0"))
				departmentIds = null;

			persons = memoWarningBL.getPersonBL().commonPersonList(companyId,
					departmentIds);

			ServletActionContext.getRequest().setAttribute("EMPLOYEE_LIST",
					persons);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Unsuccessful Return");
			return ERROR;
		}
	}

	public String saveMemoWarning() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			MemoWarning memoWarning = new MemoWarning();
			if (memoWarningId != null && memoWarningId > 0) {
				memoWarning = memoWarningBL.getMemoWarningService()
						.getMemoWarningInfo(memoWarningId);
			} else {
				memoWarning.setCreatedDate(new Date());
				memoWarning.setPerson(person);
				
					memoWarning.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.Published
									.getCode() + ""));
				
			}

			if (recordId != null && recordId > 0)
				memoWarning.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));

			memoWarning.setProcessType(processType);
			memoWarning.setTitle(title);
			memoWarning.setSubject(subject);
			memoWarning.setMessage(message);
			memoWarning.setNotes(notes);
			memoWarning.setType(type);
			memoWarning.setReferenceNumber(referenceNumber);
			memoWarning.setStatus(Byte.parseByte("1"));
			memoWarning.setImplementation(getImplementation());
			List<MemoWarningPerson> memoWarningPersons = new ArrayList<MemoWarningPerson>();
			MemoWarningPerson memoWarningPerson = null;
			Person personTemp = null;
			if (employeeList != null && !employeeList.equals("")) {
				String[] strings = memoWarningBL.splitValues(employeeList, ",");
				for (String string : strings) {
					memoWarningPerson = new MemoWarningPerson();
					personTemp = new Person();
					personTemp.setPersonId(Long.valueOf(string));
					memoWarningPerson.setPerson(personTemp);
					memoWarningPerson.setStatus(Byte.parseByte("1"));
					memoWarningPerson.setIsCcFlag(false);
					memoWarningPerson.setMemoWarning(memoWarning);
					memoWarningPersons.add(memoWarningPerson);
				}
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Please Select Employees");
				return SUCCESS;
			}

			if (ccList != null && !ccList.equals("")) {
				String[] strings = memoWarningBL.splitValues(ccList, ",");
				for (String string : strings) {
					memoWarningPerson = new MemoWarningPerson();
					person = new Person();
					person.setPersonId(Long.valueOf(string));
					memoWarningPerson.setPerson(person);
					memoWarningPerson.setStatus(Byte.parseByte("1"));
					memoWarningPerson.setIsCcFlag(true);
					memoWarningPerson.setMemoWarning(memoWarning);
					memoWarningPersons.add(memoWarningPerson);
				}
			}
			List<LookupDetail> lookupDetails = (List<LookupDetail>) session
					.getAttribute("MEMO_SUBTYPE_"
							+ sessionObj.get("jSessionId"));
			for (LookupDetail lookupDetail : lookupDetails) {
				if (lookupDetail.getDataId().equals(subType))
					memoWarning.setLookupDetail(lookupDetail);
			}

			memoWarning.setDepartment(companyId + "&#" + departmentIds);
			memoWarningBL.saveMemoWarning(memoWarning, memoWarningPersons,alertId);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			session.removeAttribute("MEMO_SUBTYPE_"
					+ sessionObj.get("jSessionId"));

			LOGGER.info("Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (memoWarningId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("Unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteMemoWarning() {
		try {
			if (memoWarningId != null && memoWarningId > 0) {
				memoWarningBL.deleteMemoWarning(memoWarningId,messageId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("Asset Usage Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("Asset Usage Info unsuccessful Return");
			return ERROR;
		}
	}

	public String getMemoWarningApproval() {
		try {
			MemoWarningVO memoWarningVO = new MemoWarningVO();
			MemoWarning memoWarning = new MemoWarning();
			if (recordId != null && recordId > 0)
				memoWarningId = recordId;

			if (memoWarningId != null && memoWarningId > 0) {

				memoWarning = memoWarningBL.getMemoWarningService()
						.getMemoWarningInfo(memoWarningId);
				memoWarningVO = memoWarningBL.convertEntityToVO(memoWarning);

			} else {
				memoWarning.setReferenceNumber(memoWarningBL.getRefereceNum());
				memoWarningVO.setMemoWarning(memoWarning);
			}

			ServletActionContext.getRequest().setAttribute("MEMO_WARNING_INFO",
					memoWarningVO);

			List<Company> companys = memoWarningBL.getCompanyBL()
					.commonCompanyList();
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companys);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return SUCCESS;

		}
	}

	public String viewMemoWarning() {
		try {
			MemoWarningVO memoWarningVO = new MemoWarningVO();
			MemoWarning memoWarning = new MemoWarning();

			if (memoWarningId != null && memoWarningId > 0) {

				memoWarning = memoWarningBL.getMemoWarningService()
						.getMemoWarningInfo(memoWarningId);
				memoWarningVO = memoWarningBL.convertEntityToVO(memoWarning);

			} else {
				memoWarning.setReferenceNumber(memoWarningBL.getRefereceNum());
				memoWarningVO.setMemoWarning(memoWarning);
			}

			ServletActionContext.getRequest().setAttribute("MEMO_WARNING_INFO",
					memoWarningVO);

			List<Company> companys = memoWarningBL.getCompanyBL()
					.commonCompanyList();
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companys);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return SUCCESS;

		}
	}
	
	public String printMemoWarning() {
		try {
			HttpSession session = ServletActionContext.getRequest()
			.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			MemoWarningVO memoWarningVO = new MemoWarningVO();
			MemoWarning memoWarning = new MemoWarning();

			if (memoWarningId != null && memoWarningId > 0) {

				memoWarning = memoWarningBL.getMemoWarningService()
						.getMemoWarningInfo(memoWarningId);
				memoWarningVO = memoWarningBL.convertEntityToVO(memoWarning);
				
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader("Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("memo-warning-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(getImplementation()
							.getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("memoType", memoWarningVO.getMemoType());
				rootMap.put("title", memoWarningVO.getTitle());
				rootMap.put("createdDate", memoWarningVO.getCreatedDateStr());
				rootMap.put("referenceNumber", memoWarningVO.getReferenceNumber());
				rootMap.put("subject", memoWarningVO.getSubject());
				rootMap.put("memoTo", memoWarningVO.getToDetails());
				rootMap.put("message", memoWarningVO.getMessage());
				rootMap.put("companyName", getImplementation().getCompanyName());
				rootMap.put("memoWarningVO", memoWarningVO);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}else{
				return "error";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}
	
	

	public String publishMemoWarning() {
		try {
			if (memoWarningId != null && memoWarningId > 0) {
				memoWarningBL.publishMemoWarning(memoWarningId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to publish");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("publishMemoWarning() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Publish Failure");
			LOGGER.error("publishMemoWarning() unsuccessful Return");
			return ERROR;
		}
	}

	public MemoWarningBL getMemoWarningBL() {
		return memoWarningBL;
	}

	public void setMemoWarningBL(MemoWarningBL memoWarningBL) {
		this.memoWarningBL = memoWarningBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getDepartmentIds() {
		return departmentIds;
	}

	public void setDepartmentIds(String departmentIds) {
		this.departmentIds = departmentIds;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public int getSubType() {
		return subType;
	}

	public void setSubType(int subType) {
		this.subType = subType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCcList() {
		return ccList;
	}

	public void setCcList(String ccList) {
		this.ccList = ccList;
	}

	public String getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(String employeeList) {
		this.employeeList = employeeList;
	}

	public Long getMemoWarningId() {
		return memoWarningId;
	}

	public void setMemoWarningId(Long memoWarningId) {
		this.memoWarningId = memoWarningId;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Byte getProcessType() {
		return processType;
	}

	public void setProcessType(Byte processType) {
		this.processType = processType;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

}
