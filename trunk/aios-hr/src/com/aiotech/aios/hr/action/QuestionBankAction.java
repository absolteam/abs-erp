package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.QuestionInfo;
import com.aiotech.aios.hr.domain.entity.vo.QuestionBankVO;
import com.aiotech.aios.hr.service.bl.QuestionBankBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class QuestionBankAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(JobAssignmentAction.class);
	private Implementation implementation;
	private QuestionBankBL questionBankBL;
	private List<Object> aaData;
	private QuestionBank questionBank;
	private Long questionBankId;
	private Integer id;
	private String childLineDetail;
	private Long designationId;
	private Long recordId;

	public String returnSuccess() {

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getQuestionBankList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<QuestionBank> questionBanks = questionBankBL
					.getQuestionBankService().getAllQuestionBank(
							getImplementation());

			if (questionBanks != null && questionBanks.size() > 0) {
				Map<Integer, String> patters = questionBankBL
						.questionPatterns();
				QuestionBankVO questionBankVO = null;
				for (QuestionBank list : questionBanks) {
					questionBankVO = new QuestionBankVO();
					questionBankVO.setQuestionBankId(list.getQuestionBankId());
					questionBankVO.setQuestionPatternView(patters.get(Integer
							.valueOf(list.getQuestionPattern())));
					questionBankVO.setQuestionType(list.getQuestionType());
					if (list.getDesignation() != null) {
						questionBankVO.setDesignationName(list.getDesignation()
								.getDesignationName());
						questionBankVO.setGradeName(list.getDesignation()
								.getGrade().getGradeName());
					} else {
						questionBankVO.setDesignationName("");
						questionBankVO.setGradeName("");
					}
					questionBankVO.setDescription(list.getDescription());
					aaData.add(questionBankVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getCommonQuestionBankList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();
			List<QuestionBank> questionBanks = null;
			if (designationId != null && designationId > 0)
				questionBanks = questionBankBL.getQuestionBankService()
						.getAllValidQuestionBank(designationId);
			else
				questionBanks = questionBankBL.getQuestionBankService()
						.getAllActiveQuestionBank(getImplementation());

			if (questionBanks != null && questionBanks.size() > 0) {
				Map<Integer, String> patters = questionBankBL
						.questionPatterns();
				QuestionBankVO questionBankVO = null;
				for (QuestionBank list : questionBanks) {
					questionBankVO = new QuestionBankVO();
					questionBankVO.setQuestionBankId(list.getQuestionBankId());
					questionBankVO.setQuestionPatternView(patters.get(Integer
							.valueOf(list.getQuestionPattern())));
					questionBankVO.setQuestionType(list.getQuestionType());
					if (list.getDesignation() != null) {
						questionBankVO.setDesignationName(list.getDesignation()
								.getDesignationName());
						questionBankVO.setGradeName(list.getDesignation()
								.getGrade().getGradeName());
					} else {
						questionBankVO.setDesignationName("");
						questionBankVO.setGradeName("");
					}
					questionBankVO.setDescription(list.getDescription());
					aaData.add(questionBankVO);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getQuestionBankAdd() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			AIOSCommons.removeUploaderSession(session, "uploadedDocs", -1L,
					"QuestionBank");

			QuestionBankVO questionBankVO = null;
			if (recordId != null && recordId > 0) {
				questionBankId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (questionBankId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = questionBankBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (questionBankId != null && questionBankId > 0) {
				questionBankVO = new QuestionBankVO();
				questionBank = questionBankBL.getQuestionBankService()
						.getQuestionBankById(questionBankId);

				// Apply the Conversion to set the Entity value into EntityVO
				questionBankVO = questionBankBL
						.convertEntityIntoVO(questionBank);

			}
			Map<Integer, String> patters = questionBankBL.questionPatterns();

			ServletActionContext.getRequest().setAttribute("QUESTION_PATTERNS",
					patters);

			ServletActionContext.getRequest().setAttribute("QUESTION_BANK",
					questionBankVO);
			ServletActionContext.getRequest().setAttribute(
					"QUESTION_INFO_LIST", questionBankVO.getQuestionInfos());
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String getAddRow() {
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	public String saveQuestionBank() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		long documentRecId = -1;
		String returnMessage = null;
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (questionBank.getQuestionBankId() != null
					&& questionBank.getQuestionBankId() > 0) {
			}

			if (questionBank.getDesignation() == null
					|| questionBank.getDesignation().getDesignationId() == null
					|| questionBank.getDesignation().getDesignationId() <= 0)
				questionBank.setDesignation(null);

			questionBank.setImplementation(getImplementation());
			QuestionBankVO questionBankVO = new QuestionBankVO();
			List<QuestionInfo> questionInfos = questionBankBL
					.getQuestionInfoLineDetail(childLineDetail);
			questionBankVO.setQuestionInfoList(questionInfos);
			questionBankBL.saveQuestionBank(questionBank, questionBankVO);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("saveQuestionBank() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (questionBank.getQuestionBankId() != null
					&& questionBank.getQuestionBankId() > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveQuestionBank() Info unsuccessful Return");
			return ERROR;
		} finally {
			if (returnMessage != null && returnMessage.equals("SUCCESS"))
				AIOSCommons.removeUploaderSession(session, "uploadedDocs",
						documentRecId, "QuestionBank");
		}
	}

	public String deleteQuestionBank() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			if (questionBankId != null && questionBankId > 0) {
				questionBank = questionBankBL.getQuestionBankService()
						.getQuestionBankById(questionBankId);
			}

			questionBankBL.deleteQuestionBank(questionBank);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteQuestionBank() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteQuestionBank() Info unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public QuestionBankBL getQuestionBankBL() {
		return questionBankBL;
	}

	public void setQuestionBankBL(QuestionBankBL questionBankBL) {
		this.questionBankBL = questionBankBL;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public QuestionBank getQuestionBank() {
		return questionBank;
	}

	public void setQuestionBank(QuestionBank questionBank) {
		this.questionBank = questionBank;
	}

	public Long getQuestionBankId() {
		return questionBankId;
	}

	public void setQuestionBankId(Long questionBankId) {
		this.questionBankId = questionBankId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChildLineDetail() {
		return childLineDetail;
	}

	public void setChildLineDetail(String childLineDetail) {
		this.childLineDetail = childLineDetail;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
