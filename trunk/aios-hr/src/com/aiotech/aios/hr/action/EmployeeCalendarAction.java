package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendarPeriod;
import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.hr.domain.entity.vo.EmployeeCalendarVO;
import com.aiotech.aios.hr.service.bl.EmployeeCalendarBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class EmployeeCalendarAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(EmployeeCalendarAction.class);
	EmployeeCalendarBL employeeCalendarBL;
	Implementation implementation;
	private Long employeeCalendarId;
	private Long jobAssignmentId;
	private String weekend;
	private String locationGroup;
	private String calendarLineDetail;
	private String calendarName;
	private Long locationId;
	private Integer id;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long recordId;

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("rowId", id);

		return SUCCESS;
	}

	// Listing JSON
	public String getEmployeeCalendarJson() {

		try {
			List<EmployeeCalendar> employeeCalendars = employeeCalendarBL
					.getEmployeeCalendarService().getEmployeeCalendarList(
							getImplementation());
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", employeeCalendars.size());
			jsonResponse.put("iTotalDisplayRecords", employeeCalendars.size());
			JSONArray data = new JSONArray();
			for (EmployeeCalendar list : employeeCalendars) {

				JSONArray array = new JSONArray();
				array.add(list.getEmployeeCalendarId());
				array.add(list.getCalendarNumber() + "");
				array.add(list.getCalendarName() + "");
				if (list.getLocation() != null) {
					array.add(list.getLocation().getLocationName() + "");
				} else {
					array.add("-NA-");
				}
				if (list.getJobAssignment() != null)
					array.add(list.getJobAssignment().getPerson()
							.getFirstName()
							+ " "
							+ list.getJobAssignment().getPerson().getLastName()
							+ "("
							+ list.getJobAssignment().getDesignation()
									.getDesignationName() + ")");
				else
					array.add("-NA-");

				if (list.getWeekend() != null
						&& !list.getWeekend().trim().equals(""))
					array.add(list.getWeekend() + "");
				else
					array.add("-NA-");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getEmployeeCalendar() {
		try {
			EmployeeCalendar employeeCalendar = new EmployeeCalendar();
			List<Integer> intelist = new ArrayList<Integer>();
			if (recordId != null && recordId > 0) {
				employeeCalendarId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (employeeCalendarId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = employeeCalendarBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (employeeCalendarId != null && employeeCalendarId > 0) {
				employeeCalendar = employeeCalendarBL
						.getEmployeeCalendarService().getEmployeeCalendarInfo(
								employeeCalendarId);

			} else {
				List<EmployeeCalendar> employeeCalendars = employeeCalendarBL
						.getEmployeeCalendarService().getEmployeeCalendarList(
								getImplementation());
				if (employeeCalendars != null && employeeCalendars.size() > 0) {
					for (int i = 0; i < employeeCalendars.size(); i++) {
						intelist.add(Integer.parseInt(employeeCalendars.get(i)
								.getCalendarNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					employeeCalendar.setCalendarNumber(temp + "");
				} else {
					employeeCalendar.setCalendarNumber(1000 + "");
				}
			}
			ServletActionContext.getRequest().setAttribute("EMPLOYEE_CALENDAR",
					employeeCalendar);
			List<EmployeeCalendarPeriod> periods = new ArrayList<EmployeeCalendarPeriod>(
					employeeCalendar.getEmployeeCalendarPeriods());
			Collections.sort(periods, new Comparator<EmployeeCalendarPeriod>() {
				public int compare(EmployeeCalendarPeriod m1,
						EmployeeCalendarPeriod m2) {
					return m2.getFromDate().compareTo(m1.getFromDate());
				}
			});
			ServletActionContext.getRequest().setAttribute(
					"EMPLOYEE_CALENDAR_LIST", periods);

			List<Location> locations = employeeCalendarBL.getLocationService()
					.getAllLocationList(getImplementation());
			ServletActionContext.getRequest().setAttribute("LOCATIONS",
					locations);

			List<JobAssignment> jobAssignments = employeeCalendarBL
					.getJobService().getAllJobAssignment(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"JOB_ASSIGNMENT_LIST", jobAssignments);

			List<EmployeeCalendarVO> holidayTypeList = employeeCalendarBL
					.getHolidayTypeList();
			ServletActionContext.getRequest().setAttribute("HOLIDAY_TYPE_LIST",
					holidayTypeList);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getAddRow() {
		List<EmployeeCalendarVO> holidayTypeList = employeeCalendarBL
				.getHolidayTypeList();
		ServletActionContext.getRequest().setAttribute("HOLIDAY_TYPE_LIST",
				holidayTypeList);
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	public String saveEmployeeCalendar() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<Integer> intelist = new ArrayList<Integer>();
			EmployeeCalendar employeeCalendar = new EmployeeCalendar();
			if (employeeCalendarId != null && employeeCalendarId > 0) {
				employeeCalendar = employeeCalendarBL
						.getEmployeeCalendarService().getEmployeeCalendarInfo(
								employeeCalendarId);
			} else {

				List<EmployeeCalendar> employeeCalendars = employeeCalendarBL
						.getEmployeeCalendarService().getEmployeeCalendarList(
								getImplementation());
				if (employeeCalendars != null && employeeCalendars.size() > 0) {
					for (int i = 0; i < employeeCalendars.size(); i++) {
						intelist.add(Integer.parseInt(employeeCalendars.get(i)
								.getCalendarNumber()));
					}

					int temp = Collections.max(intelist);
					temp += 1;
					employeeCalendar.setCalendarNumber(temp + "");
				} else {
					employeeCalendar.setCalendarNumber(1000 + "");
				}
			}
			employeeCalendar.setCalendarName(calendarName);
			employeeCalendar.setImplementation(getImplementation());
			Grade grade = new Grade();
			grade.setGradeId(Long.parseLong("2"));
			employeeCalendar.setGrade(grade);
			if (locationId != null && locationId > 0) {
				Location location = new Location();
				location.setLocationId(locationId);
				employeeCalendar.setLocation(location);
			} else {
				employeeCalendar.setLocation(null);
			}
			if (jobAssignmentId != null && jobAssignmentId > 0) {
				JobAssignment jobAssignment = new JobAssignment();
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				employeeCalendar.setJobAssignment(jobAssignment);
			} else {
				employeeCalendar.setJobAssignment(null);
			}
			if (weekend != null && !weekend.trim().equals(""))
				employeeCalendar.setWeekend(weekend.substring(0,
						weekend.length() - 1));
			else
				employeeCalendar.setWeekend(null);

			if (locationGroup != null && !locationGroup.trim().equals("")
					&& locationId != null && locationId > 0)
				employeeCalendar.setLocationGroup(locationGroup.substring(0,
						locationGroup.length() - 1));
			else
				employeeCalendar.setLocationGroup(null);

			returnMessage = employeeCalendarBL.saveCalendar(employeeCalendarBL
					.getCalendarLineDetail(calendarLineDetail),
					employeeCalendar);
			if (!returnMessage.equals("SUCCESS"))
				returnMessage = "Failure";

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			returnMessage = "Failure";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String deleteCalendar() {

		try {

			employeeCalendarBL.deleteEmployeeCalendar(employeeCalendarId);

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			returnMessage = "Can not delete, Information is in use already";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmployeeCalendarBL getEmployeeCalendarBL() {
		return employeeCalendarBL;
	}

	public void setEmployeeCalendarBL(EmployeeCalendarBL employeeCalendarBL) {
		this.employeeCalendarBL = employeeCalendarBL;
	}

	public Long getEmployeeCalendarId() {
		return employeeCalendarId;
	}

	public void setEmployeeCalendarId(Long employeeCalendarId) {
		this.employeeCalendarId = employeeCalendarId;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public String getLocationGroup() {
		return locationGroup;
	}

	public void setLocationGroup(String locationGroup) {
		this.locationGroup = locationGroup;
	}

	public String getWeekend() {
		return weekend;
	}

	public void setWeekend(String weekend) {
		this.weekend = weekend;
	}

	public String getCalendarLineDetail() {
		return calendarLineDetail;
	}

	public void setCalendarLineDetail(String calendarLineDetail) {
		this.calendarLineDetail = calendarLineDetail;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
