package com.aiotech.aios.hr.action;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.vo.AttendancePolicyVO;
import com.aiotech.aios.hr.service.bl.AttendancePolicyBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class AttendancePolicyAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(AttendancePolicyAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long attendancePolicyId;
	private Boolean allowLop;
	private Boolean allowOt;
	private Boolean allowCompoff;
	private String otHoursForHalfday;
	private String otHoursForFullday;
	private String compoffHoursForHalfday;
	private String compoffHoursForFullday;
	private String lopHoursForHalfday;
	private String lopHoursForFullday;
	private String processType;
	private String policyName;
	private Boolean isDefalut;
	private Byte attendanceType;
	private Double attendanceDays;
	private Double attendanceHours;
	private Boolean hourlyProcess;
	private AttendancePolicyBL attendancePolicyBL;
	private Long recordId;

	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	public String getAttendancePolicyList() {

		try {
			List<AttendancePolicy> attendancePolicies = attendancePolicyBL
					.getAttendancePolicyService().getAttendancePolicyList(
							getImplementation());

			JSONObject jsonResponse = new JSONObject();
			if (attendancePolicies != null && attendancePolicies.size() > 0) {
				jsonResponse.put("iTotalRecords", attendancePolicies.size());
				jsonResponse.put("iTotalDisplayRecords",
						attendancePolicies.size());
			}
			JSONArray data = new JSONArray();
			for (AttendancePolicy list : attendancePolicies) {

				JSONArray array = new JSONArray();
				array.add(list.getAttendancePolicyId() + "");
				array.add(list.getPolicyName() + "");
				if (list.getProcessType() != null
						&& list.getProcessType().equals("FLS"))
					array.add("First In & Last Out Swipe Only");
				else
					array.add("All Swipe");

				if (list.getAllowOt() != null && list.getAllowOt() == true
						&& list.getOtHoursForHalfday() != null
						&& list.getOtHoursForFullday() != null) {
					array.add("YES");
					array.add(DateFormat.convertTimeToString(list
							.getOtHoursForHalfday() + ""));
					array.add(DateFormat.convertTimeToString(list
							.getOtHoursForFullday() + ""));
				} else {
					array.add("NO");
					array.add("-NA-");
					array.add("-NA-");
				}
				if (list.getAllowCompoff() != null
						&& list.getAllowCompoff() == true
						&& list.getCompoffHoursForHalfday() != null
						&& list.getCompoffHoursForFullday() != null) {
					array.add("YES");
					array.add(DateFormat.convertTimeToString(list
							.getCompoffHoursForHalfday() + ""));
					array.add(DateFormat.convertTimeToString(list
							.getCompoffHoursForFullday() + ""));
				} else {
					array.add("NO");
					array.add("-NA-");
					array.add("-NA-");
				}
				if (list.getLopHoursForHalfday() != null)
					array.add(DateFormat.convertTimeToString(list
							.getLopHoursForHalfday() + ""));
				else
					array.add("");
				if (list.getLopHoursForFullday() != null)
					array.add(DateFormat.convertTimeToString(list
							.getLopHoursForFullday() + ""));
				else
					array.add("");
				if (list.getIsDefalut() != null && list.getIsDefalut() == true)
					array.add("YES");
				else
					array.add("NO");

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info(" getAttendancePolicyList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getAttendancePolicyList() unsuccessful Return");
			return ERROR;
		}
	}

	public String getAttendancePolicyAdd() {
		try {
			if (recordId != null && recordId > 0) {
				attendancePolicyId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (attendancePolicyId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = attendancePolicyBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}

			AttendancePolicy attendancePolicy = new AttendancePolicy();
			AttendancePolicyVO attendancePolicyVO = null;
			if (attendancePolicyId != null && attendancePolicyId > 0) {
				attendancePolicy = attendancePolicyBL
						.getAttendancePolicyService().getAttendancePolicyInfo(
								attendancePolicyId);
				attendancePolicyVO = attendancePolicyBL
						.convertEntityToVO(attendancePolicy);
			}

			ServletActionContext.getRequest().setAttribute("POLICY_INFO",
					attendancePolicyVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String saveAttendancePolicy() {
		try {

			// List<AttendancePolicy>
			// attendancePolicies=attendancePolicyBL.getAttendancePolicyService().getAttendancePolicyList(getImplementation());

			AttendancePolicy attendancePolicy = new AttendancePolicy();
			if (attendancePolicyId != null && attendancePolicyId > 0)
				attendancePolicy.setAttendancePolicyId(attendancePolicyId);

			attendancePolicy.setPolicyName(policyName);
			attendancePolicy.setProcessType(processType);
			if (allowOt != null)
				attendancePolicy.setAllowOt(allowOt);
			if (allowCompoff != null)
				attendancePolicy.setAllowCompoff(allowCompoff);

			if (allowLop != null)
				attendancePolicy.setAllowLop(allowLop);
			if (hourlyProcess != null)
				attendancePolicy.setHourlyProcess(hourlyProcess);
			// OT
			if (otHoursForHalfday != null && !otHoursForHalfday.equals(""))
				attendancePolicy.setOtHoursForHalfday(DateFormat
						.convertStringToTime(otHoursForHalfday));
			if (otHoursForFullday != null && !otHoursForFullday.equals(""))
				attendancePolicy.setOtHoursForFullday(DateFormat
						.convertStringToTime(otHoursForFullday));
			// COMPENSATION OFF
			if (compoffHoursForHalfday != null
					&& !compoffHoursForHalfday.equals(""))
				attendancePolicy.setCompoffHoursForHalfday(DateFormat
						.convertStringToTime(compoffHoursForHalfday));
			if (compoffHoursForFullday != null
					&& !compoffHoursForFullday.equals(""))
				attendancePolicy.setCompoffHoursForFullday(DateFormat
						.convertStringToTime(compoffHoursForFullday));
			// LOP
			if (lopHoursForHalfday != null && !lopHoursForHalfday.equals(""))
				attendancePolicy.setLopHoursForHalfday(DateFormat
						.convertStringToTime(lopHoursForHalfday));
			if (lopHoursForFullday != null && !lopHoursForFullday.equals(""))
				attendancePolicy.setLopHoursForFullday(DateFormat
						.convertStringToTime(lopHoursForFullday));

			attendancePolicy.setImplementation(getImplementation());
			if (isDefalut != null)
				attendancePolicy.setIsDefalut(isDefalut);

			attendancePolicy.setAttendanceType(attendanceType);
			if (attendanceDays != null && attendanceDays > 0)
				attendancePolicy.setAttendanceDays(attendanceDays);
			if (attendanceHours != null && attendanceHours > 0)
				attendancePolicy.setAttendanceHours(attendanceHours);

			attendancePolicyBL.saveAttendancePolicy(attendancePolicy);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");

			LOGGER.info("saveAttendancePolicy() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (attendancePolicyId > 0)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("saveAttendancePolicy() Unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteAttendancePolicy() {
		try {
			if (attendancePolicyId != null && attendancePolicyId > 0) {
				attendancePolicyBL.deleteAttendancePolicy(attendancePolicyId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteAttendancePolicy() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteAttendancePolicy() unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public AttendancePolicyBL getAttendancePolicyBL() {
		return attendancePolicyBL;
	}

	public void setAttendancePolicyBL(AttendancePolicyBL attendancePolicyBL) {
		this.attendancePolicyBL = attendancePolicyBL;
	}

	public Long getAttendancePolicyId() {
		return attendancePolicyId;
	}

	public void setAttendancePolicyId(Long attendancePolicyId) {
		this.attendancePolicyId = attendancePolicyId;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Boolean getAllowOt() {
		return allowOt;
	}

	public void setAllowOt(Boolean allowOt) {
		this.allowOt = allowOt;
	}

	public Boolean getAllowCompoff() {
		return allowCompoff;
	}

	public void setAllowCompoff(Boolean allowCompoff) {
		this.allowCompoff = allowCompoff;
	}

	public String getOtHoursForHalfday() {
		return otHoursForHalfday;
	}

	public void setOtHoursForHalfday(String otHoursForHalfday) {
		this.otHoursForHalfday = otHoursForHalfday;
	}

	public String getOtHoursForFullday() {
		return otHoursForFullday;
	}

	public void setOtHoursForFullday(String otHoursForFullday) {
		this.otHoursForFullday = otHoursForFullday;
	}

	public String getCompoffHoursForHalfday() {
		return compoffHoursForHalfday;
	}

	public void setCompoffHoursForHalfday(String compoffHoursForHalfday) {
		this.compoffHoursForHalfday = compoffHoursForHalfday;
	}

	public String getCompoffHoursForFullday() {
		return compoffHoursForFullday;
	}

	public void setCompoffHoursForFullday(String compoffHoursForFullday) {
		this.compoffHoursForFullday = compoffHoursForFullday;
	}

	public String getLopHoursForHalfday() {
		return lopHoursForHalfday;
	}

	public void setLopHoursForHalfday(String lopHoursForHalfday) {
		this.lopHoursForHalfday = lopHoursForHalfday;
	}

	public String getLopHoursForFullday() {
		return lopHoursForFullday;
	}

	public void setLopHoursForFullday(String lopHoursForFullday) {
		this.lopHoursForFullday = lopHoursForFullday;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public Boolean getIsDefalut() {
		return isDefalut;
	}

	public void setIsDefalut(Boolean isDefalut) {
		this.isDefalut = isDefalut;
	}

	public Double getAttendanceDays() {
		return attendanceDays;
	}

	public void setAttendanceDays(Double attendanceDays) {
		this.attendanceDays = attendanceDays;
	}

	public Double getAttendanceHours() {
		return attendanceHours;
	}

	public void setAttendanceHours(Double attendanceHours) {
		this.attendanceHours = attendanceHours;
	}

	public Byte getAttendanceType() {
		return attendanceType;
	}

	public void setAttendanceType(Byte attendanceType) {
		this.attendanceType = attendanceType;
	}

	public Boolean getAllowLop() {
		return allowLop;
	}

	public void setAllowLop(Boolean allowLop) {
		this.allowLop = allowLop;
	}

	public Boolean getHourlyProcess() {
		return hourlyProcess;
	}

	public void setHourlyProcess(Boolean hourlyProcess) {
		this.hourlyProcess = hourlyProcess;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
