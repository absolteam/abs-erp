package com.aiotech.aios.hr.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.hr.service.bl.DesignationBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionSupport;

public class DesignationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(DesignationAction.class);
	DesignationBL designationBL;
	Implementation implementation;

	private Long designationId;
	private Long companyId;
	private String designationName;
	private Long gradeId;
	private Long parentDesignationId;
	private String description;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long recordId;
	private String designationNameArabic;

	public String resturnSuccess() {
		return SUCCESS;
	}

	// Listing JSON
	public String getDesignationListJson() {

		try {

			List<Designation> designations = designationBL
					.getDesignationService().getAllDesignation(
							getImplementation());
			if (designations != null && designations.size() > 0) {

				iTotalRecords = designations.size();
				iTotalDisplayRecords = designations.size();
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (Designation list : designations) {

				JSONArray array = new JSONArray();
				array.add(list.getDesignationId() + "");
				array.add(list.getDesignationName() + "");
				array.add(list.getGrade().getGradeName() + "");
				if (list.getDesignation() != null
						&& list.getDesignation().getDesignationName() != null)
					array.add(list.getDesignation().getDesignationName() + "");
				else
					array.add("--");
				array.add(list.getCompany().getCompanyName() + "");
				array.add(list.getDescription() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String redirectDesignation() {
		List<Company> companyList = new ArrayList<Company>();
		List<Grade> gradeList = new ArrayList<Grade>();
		List<Designation> designationList = new ArrayList<Designation>();
		try {
			companyList = designationBL.getCompanyService().getOnlyCompanies(
					getImplementation());
			ServletActionContext.getRequest().setAttribute("COMPANY_LIST",
					companyList);

			gradeList = designationBL.getDesignationService().getAllGrade(
					getImplementation());
			ServletActionContext.getRequest().setAttribute("GRADE_LIST",
					gradeList);
			Designation designation = new Designation();
			if (recordId != null && recordId > 0) {
				designationId = recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (designationId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = designationBL.getCommentBL().getCommentInfo(
							comment);
					ServletActionContext.getRequest().setAttribute(
							"COMMENT_IFNO", comment);
				}
			}
			if (designationId > 0) {
				designation = designationBL.getDesignationService()
						.getDesignationInfo(designationId);
				designationList = designationBL.getDesignationService()
						.getAllCompanyDesignation(
								designation.getCompany().getCompanyId());

			}
			if (designation != null
					&& designation.getDesignationNameArabic() != null)
				ServletActionContext.getRequest().setAttribute(
						"DESIGNATION_NAME_ARABIC",
						AIOSCommons.bytesToUTFString(designation
								.getDesignationNameArabic()));
			ServletActionContext.getRequest().setAttribute("DESIGNATION",
					designation);
			ServletActionContext.getRequest().setAttribute("DESIGNATION_LIST",
					designationList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getParentDesignationList() {

		List<Designation> designationList = new ArrayList<Designation>();
		try {

			if (companyId != null && companyId > 0) {
				designationList = designationBL.getDesignationService()
						.getAllCompanyDesignation(companyId);
			}
			ServletActionContext.getRequest().setAttribute("DESIGNATION_LIST",
					designationList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String saveDesignation() {

		Designation designation = new Designation();
		try {

			if (designationId != null && designationId > 0) {
				designation = designationBL.getDesignationService()
						.getDesignationInfo(designationId);
			}
			designation.setDesignationName(designationName);
			if (gradeId != null && gradeId > 0) {
				Grade grade = new Grade();
				grade.setGradeId(gradeId);
				designation.setGrade(grade);
			}
			if (companyId != null && companyId > 0) {
				Company company = new Company();
				company.setCompanyId(companyId);
				designation.setCompany(company);
			}

			if (designationNameArabic != null
					&& !designationNameArabic.equals("")) {
				designation.setDesignationNameArabic(AIOSCommons
						.stringToUTFBytes(designationNameArabic));

			}
			if (parentDesignationId != null && parentDesignationId > 0) {
				Designation parentDesignation = new Designation();
				parentDesignation.setDesignationId(parentDesignationId);
				designation.setDesignation(parentDesignation);
			}
			designation.setDescription(description);
			designation.setImplementation(getImplementation());
			designation.setIsActive(true);
			returnMessage = designationBL.saveDesignation(designation);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String deleteDesignation() {

		try {

			designationBL.deleteDesignation(designationId);

			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			returnMessage = "Can not delete, Designation is in use already";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public DesignationBL getDesignationBL() {
		return designationBL;
	}

	public void setDesignationBL(DesignationBL designationBL) {
		this.designationBL = designationBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public Long getGradeId() {
		return gradeId;
	}

	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

	public Long getParentDesignationId() {
		return parentDesignationId;
	}

	public void setParentDesignationId(Long parentDesignationId) {
		this.parentDesignationId = parentDesignationId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getDesignationNameArabic() {
		return designationNameArabic;
	}

	public void setDesignationNameArabic(String designationNameArabic) {
		this.designationNameArabic = designationNameArabic;
	}
}
