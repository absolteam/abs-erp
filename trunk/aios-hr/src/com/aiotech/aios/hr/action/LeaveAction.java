package com.aiotech.aios.hr.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.LeavePolicy;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.LeaveVO;
import com.aiotech.aios.hr.service.bl.LeaveBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LeaveAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
	.getLogger(LeaveAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long leaveId;
	private String displayName;
	private Boolean isMonthly;
	private Boolean isYearly;
	private Boolean isService;
	private Boolean isWeekly;
	private Integer isServiceCount;
	private Double defaultDays;
	private Boolean isPaid;
	private Double fullPaidDays;
	private Double halfPaidDays;
	private Boolean isCarryForward;
	private Integer carryForwardCount;
	private Boolean canEncash;
	private Date createdDate;
	private Boolean isActive;
	private String leaveType;
	private Double monthlyDays;
	private Double yearlyDays;
	private String leaveLineDetail;
	private Long recordId;
	
	private LeaveBL leaveBL;
	public String returnSuccess() {
		if(null==id)
			id=0;
		ServletActionContext.getRequest().setAttribute("rowId", id);  
		return SUCCESS;
	}
	public String getLeavesRedirect() {
		List<LeavePolicy> leavePolicies=leaveBL.getLeavePolicyBL().getLeavePolicyService().getLeavePolicyList(getImplementation());
		ServletActionContext.getRequest().setAttribute("Leave_Policy", leavePolicies);

		return SUCCESS;
	}

	// Listing JSON
	public String getLeaveList() {

		try {		
			List<Leave> leaves=leaveBL.getLeaveService().getLeaveList(getImplementation());
			List<LeaveVO> leaveVOs=null;
			JSONObject jsonResponse = new JSONObject(); 
			if(leaves!=null && leaves.size()>0){
				leaveVOs=leaveBL.convertEntitiesTOVOs(leaves);
				jsonResponse.put("iTotalRecords", leaves.size());
				jsonResponse.put("iTotalDisplayRecords", leaves.size());  
			}
			JSONArray data= new JSONArray();   
			for(LeaveVO list :leaveVOs){ 
				
				JSONArray array= new JSONArray();  
				array.add(list.getLeaveId()+""); 
				array.add(list.getLeaveType()+"");
				array.add(list.getLeaveCode()+"");
				array.add(list.getDisplayName()+"");
				array.add(list.getDefaultDays()+"");
				/*array.add(list.getIsMonthlyStr()+"");
				array.add(list.getIsYearlyStr()+"");
				array.add(list.getIsServiceStr()+"");*/
				array.add(list.getIsPaidStr()+"");
				array.add(list.getFullPaidDays()+"");
				array.add(list.getHalfPaidDays()+"");
				array.add(list.getIsCarryForwardStr()+"");
				array.add(list.getCarryForwardCount()+"");
				array.add(list.getIsServiceCount()+"");

				data.add(array); 
			}  
			jsonResponse.put("aaData", data);  
		    ServletActionContext.getRequest().setAttribute("jsonlist", jsonResponse);
		    LOGGER.info(" getLeaveList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getLeaveList() unsuccessful Return");
			return ERROR;
		}
	}
	
	// Listing JSON
	public String getActiveLeaveList() {

		try {		
			List<Leave> leaves=leaveBL.getLeaveService().getActiveLeaveList(getImplementation());
			List<LeaveVO> leaveVOs=null;
			JSONObject jsonResponse = new JSONObject(); 
			if(leaves!=null && leaves.size()>0){
				leaveVOs=leaveBL.convertEntitiesTOVOs(leaves);
				jsonResponse.put("iTotalRecords", leaves.size());
				jsonResponse.put("iTotalDisplayRecords", leaves.size());  
			}
			JSONArray data= new JSONArray();   
			for(LeaveVO list :leaveVOs){ 
				
				JSONArray array= new JSONArray();  
				array.add(list.getLeaveId()+""); 
				array.add(list.getDisplayName()+"");
				array.add(list.getLeaveCode()+"");
				array.add(list.getLeaveType()+"");
				array.add(list.getDefaultDays()+"");
				array.add(list.getIsMonthlyStr()+"");
				array.add(list.getIsYearlyStr()+"");
				array.add(list.getIsServiceStr()+"");
				array.add(list.getIsServiceCount()+"");
				array.add(list.getIsPaidStr()+"");
				array.add(list.getFullPaidDays()+"");
				array.add(list.getHalfPaidDays()+"");
				array.add(list.getIsCarryForwardStr()+"");
				array.add(list.getCarryForwardCount()+"");
				
				data.add(array); 
			}  
			jsonResponse.put("aaData", data);  
		    ServletActionContext.getRequest().setAttribute("jsonlist", jsonResponse);
		    LOGGER.info(" getLeaveList() Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(" getLeaveList() unsuccessful Return");
			return ERROR;
		}
	}
	
	
	public String getLeaveAdd() {
		try{
			Map<String, Object> sessionObj = ActionContext.getContext()
			.getSession();
			HttpSession session = ServletActionContext.getRequest().getSession();

			session.removeAttribute("LEAVE_TYPE_LIST_"
					+ sessionObj.get("jSessionId"));
			if(recordId!=null && recordId>0){
				leaveId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (leaveId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(Leave.class.getName());
					comment = leaveBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			Leave leave=new Leave();
			LeaveVO leaveVO=null;
			if(leaveId!=null && leaveId>0){
				leave=leaveBL.getLeaveService().getLeaveInfo(leaveId);
				leaveVO=leaveBL.convertEntityToVO(leave);
			}
			List<LookupDetail> leaveTypeList=leaveBL.getLookupMasterBL().getActiveLookupDetails("LEAVE_TYPE_LIST",false);
			ServletActionContext.getRequest().setAttribute("LEAVE_TYPES", leaveTypeList);
			ServletActionContext.getRequest().setAttribute("LEAVE_INFO", leaveVO);
			session.setAttribute("LEAVE_TYPE_LIST_"
					+ sessionObj.get("jSessionId"), leaveTypeList);

			return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return ERROR;

		}
	}
	
	public String saveLeave() {
		try {	
			HttpSession session = ServletActionContext.getRequest()
			.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
			.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Leave leave=new Leave();
			if(leaveId!=null && leaveId>0){
				leave=leaveBL.getLeaveService().getLeaveInfo(leaveId);
				leave.setPersonByUpdatedBy(person);
				leave.setUpdatedDate(new Date());
			}else{
				leave.setPersonByCreatedBy(person);
				leave.setCreatedDate(new Date());

			}
				
			leave.setDisplayName(displayName);
			leave.setDefaultDays(defaultDays);
			leave.setIsMonthly(isMonthly);
			leave.setIsYearly(isYearly);
			leave.setIsService(isService);
			leave.setIsWeekly(isWeekly);
			leave.setIsServiceCount(isServiceCount);
			leave.setIsPaid(isPaid);
			leave.setFullPaidDays(fullPaidDays);
			leave.setHalfPaidDays(halfPaidDays);
			leave.setCarryForwardCount(carryForwardCount);
			leave.setCanEncash(canEncash);
			leave.setIsActive(isActive);
			
			List<LookupDetail> lookupDetails = (List<LookupDetail>) session
			.getAttribute("LEAVE_TYPE_LIST_"
					+ sessionObj.get("jSessionId"));
			for(LookupDetail lookupDetail:lookupDetails){
				if(lookupDetail.getAccessCode().equals(leaveType))
					leave.setLookupDetail(lookupDetail);
			}

		
			leave.setImplementation(getImplementation());
			
			
			leaveBL.saveLeave(leave);
			
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE","SUCCESS");
			session.removeAttribute("LEAVE_TYPE_LIST_"
					+ sessionObj.get("jSessionId"));
	
	
			LOGGER.info("saveLeave() Sucessfull Return");
			return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			if (leaveId > 0)
				ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Creation Failure");
			LOGGER.error("saveLeave() Unsuccessful Return");
			return ERROR;
		}
	}
	
	public String deleteLeave() {
		try {	
			if(leaveId!=null && leaveId>0){
				leaveBL.deleteLeave(leaveId);
			}else{
				ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
				"No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE","SUCCESS");
			LOGGER.info("deleteLeave() Sucessfull Return");
			return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
				ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure / Information is in use already.");
			LOGGER.error("deleteLeave() unsuccessful Return");
			return ERROR;
		}
	}
	
	public String saveLeavePolicy() {
		try {	
			if(leaveLineDetail!=null){
				List<LeavePolicy> deleteLeavePolicies = leaveBL
						.getLeavePolicyBL()
						.getJobLeaveSimiler(
								leaveBL.getLeavePolicyBL().getLeaveLineDetail(
										leaveLineDetail),
								leaveBL.getLeavePolicyBL()
										.getLeavePolicyService()
										.getLeavePolicyList(getImplementation()));
				leaveBL.getLeavePolicyBL().leavePoliciesSave(
						leaveBL.getLeavePolicyBL().getLeaveLineDetail(
								leaveLineDetail), deleteLeavePolicies);
			}else{
				ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
				"There is no required information in the leave policy");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE","SUCCESS");
			LOGGER.info("saveLeavePolicy() Sucessfull Return");
			return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
				ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Save Failure");
			LOGGER.error("saveLeavePolicy() unsuccessful Return");
			return ERROR;
		}
	}
	
	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}
	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}
	public LeaveBL getLeaveBL() {
		return leaveBL;
	}
	public void setLeaveBL(LeaveBL leaveBL) {
		this.leaveBL = leaveBL;
	}
	public Long getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Boolean getIsMonthly() {
		return isMonthly;
	}
	public void setIsMonthly(Boolean isMonthly) {
		this.isMonthly = isMonthly;
	}
	public Boolean getIsYearly() {
		return isYearly;
	}
	public void setIsYearly(Boolean isYearly) {
		this.isYearly = isYearly;
	}
	public Boolean getIsService() {
		return isService;
	}
	public void setIsService(Boolean isService) {
		this.isService = isService;
	}
	public Integer getIsServiceCount() {
		return isServiceCount;
	}
	public void setIsServiceCount(Integer isServiceCount) {
		this.isServiceCount = isServiceCount;
	}
	
	public Boolean getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}
	public Double getFullPaidDays() {
		return fullPaidDays;
	}
	public void setFullPaidDays(Double fullPaidDays) {
		this.fullPaidDays = fullPaidDays;
	}
	public Double getHalfPaidDays() {
		return halfPaidDays;
	}
	public void setHalfPaidDays(Double halfPaidDays) {
		this.halfPaidDays = halfPaidDays;
	}
	public Boolean getIsCarryForward() {
		return isCarryForward;
	}
	public void setIsCarryForward(Boolean isCarryForward) {
		this.isCarryForward = isCarryForward;
	}
	public Integer getCarryForwardCount() {
		return carryForwardCount;
	}
	public void setCarryForwardCount(Integer carryForwardCount) {
		this.carryForwardCount = carryForwardCount;
	}
	public Boolean getCanEncash() {
		return canEncash;
	}
	public void setCanEncash(Boolean canEncash) {
		this.canEncash = canEncash;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public Double getMonthlyDays() {
		return monthlyDays;
	}
	public void setMonthlyDays(Double monthlyDays) {
		this.monthlyDays = monthlyDays;
	}
	public Double getYearlyDays() {
		return yearlyDays;
	}
	public void setYearlyDays(Double yearlyDays) {
		this.yearlyDays = yearlyDays;
	}
	public String getLeaveLineDetail() {
		return leaveLineDetail;
	}
	public void setLeaveLineDetail(String leaveLineDetail) {
		this.leaveLineDetail = leaveLineDetail;
	}
	public Double getDefaultDays() {
		return defaultDays;
	}
	public void setDefaultDays(Double defaultDays) {
		this.defaultDays = defaultDays;
	}
	public Long getRecordId() {
		return recordId;
	}
	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
	public Boolean getIsWeekly() {
		return isWeekly;
	}
	public void setIsWeekly(Boolean isWeekly) {
		this.isWeekly = isWeekly;
	}
}
