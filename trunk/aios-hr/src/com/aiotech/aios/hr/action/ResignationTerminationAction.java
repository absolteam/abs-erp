package com.aiotech.aios.hr.action;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobLeave;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.ResignationTermination;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.ResignationTerminationVO;
import com.aiotech.aios.hr.service.bl.ResignationTerminationBL;
import com.aiotech.aios.hr.to.LeaveProcessTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class ResignationTerminationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(ResignationTerminationAction.class);
	private Implementation implementation;
	private ResignationTerminationBL resignationTerminationBL;
	private Long jobAssignmentId;
	private List<Object> aaData;
	private String description;
	private Byte status;
	private Long resignationTerminationId;
	private Boolean positionAutoRelease;
	private String reason;
	private Long requestedPerson;
	private Byte type;
	private String effectiveDate;
	private String requestedDate;
	private Long recordId;
	private Integer eligibleDays;
	private Integer givenDays;
	private Byte action;
	private Integer noticeDays;
	private String printableContent;
	private String payLineDetail;

	public String returnSuccess() {
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getResignationTerminationList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			aaData = new ArrayList<Object>();

			List<ResignationTermination> resignationTerminations = resignationTerminationBL
					.getResignationTerminationService()
					.getAllResignationTermination(getImplementation());

			if (resignationTerminations != null
					&& resignationTerminations.size() > 0) {
				ResignationTerminationVO vo = null;
				for (ResignationTermination list : resignationTerminations) {
					vo = new ResignationTerminationVO();
					vo.setResignationTerminationId(list
							.getResignationTerminationId());
					vo.setEmployeeName(list.getJobAssignment().getPerson()
							.getFirstName()
							+ " "
							+ list.getJobAssignment().getPerson().getLastName());
					vo.setJobNumber(list.getJobAssignment()
							.getJobAssignmentNumber());
					vo.setEffectiveDateView(DateFormat.convertDateToString(list
							.getEffectiveDate().toString()));
					vo.setNoticeDays(list.getNoticeDays());
					vo.setGivenDays(list.getGivenDays());
					vo.setTypeView(Constants.HR.ResignationType
							.get(list.getType()).name().replaceAll("\\d+", "")
							.replaceAll("(.)([A-Z])", "$1 $2"));
					vo.setCurrentPosition(list.getJobAssignment()
							.getDesignation().getDesignationName());
					vo.setActionView(Constants.HR.ResignationActions
							.get(list.getAction()).name()
							.replaceAll("\\d+", "")
							.replaceAll("(.)([A-Z])", "$1 $2"));
					vo.setRequestedBy(list.getPersonByRequestedBy()
							.getFirstName()
							+ " "
							+ list.getPersonByRequestedBy().getLastName());
					vo.setResignationStatus(Constants.HR.ResignationStatus
							.get(list.getStatus()).name()
							.replaceAll("\\d+", "")
							.replaceAll("(.)([A-Z])", "$1 $2"));
					aaData.add(vo);

				}
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getResignationTerminationAdd() {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			CommentVO comment = new CommentVO();

			if (recordId != null && recordId > 0) {
				resignationTerminationId = recordId;
				comment.setRecordId(Long.parseLong(resignationTerminationId
						+ ""));
				comment.setUseCase("com.aiotech.aios.hr.domain.entity.IdentityAvailability");
				comment = resignationTerminationBL.getCommentBL()
						.getCommentInfo(comment);
			}
			ResignationTerminationVO resignationTerminationVO = null;
			Payroll payroll = null;
			PayrollVO payrollVO=null;
			if (resignationTerminationId != null
					&& resignationTerminationId > 0) {
				ResignationTermination resignationTermination = resignationTerminationBL
						.getResignationTerminationService()
						.getResignationTerminationById(resignationTerminationId);
				if (resignationTermination != null)
					resignationTerminationVO = resignationTerminationBL
							.convertEntityToVO(resignationTermination);

				payroll = resignationTerminationBL
						.getPayrollBL()
						.getPayrollService()
						.getPayrollDetailByUseCase(
								resignationTermination
										.getResignationTerminationId(),
								resignationTermination.getClass()
										.getSimpleName());
				payrollVO=resignationTerminationBL
				.getPayrollBL().payrollToVOConverter(payroll);
			} else {
				resignationTerminationVO = new ResignationTerminationVO();
				resignationTerminationVO.setRequestedDateView(DateFormat
						.convertSystemDateToString(new Date()));
				resignationTerminationVO.setCreatedDateView(DateFormat
						.convertSystemDateToString(new Date()));
				resignationTerminationVO.setEffectiveDateView(DateFormat
						.convertSystemDateToString(new Date()));
			}

			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();

			jobAssignments = resignationTerminationBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(getImplementation());

			ServletActionContext.getRequest().setAttribute(
					"JOB_ASSIGNMENT_LIST", jobAssignments);
			Map<Byte, String> resignationTypes = resignationTerminationBL
					.resignationType();
			ServletActionContext.getRequest().setAttribute("RESIGNATION_TYPE",
					resignationTypes);
			Map<Byte, String> actions = resignationTerminationBL.actions();
			ServletActionContext.getRequest().setAttribute("ACTIONS", actions);
			ServletActionContext.getRequest().setAttribute("RESIGNATION",
					resignationTerminationVO);
		
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveResignationTermination() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			Payroll oldPayroll = null;
			ResignationTermination resignationTermination = new ResignationTermination();
			if (resignationTerminationId != null
					&& resignationTerminationId > 0) {
				resignationTermination = resignationTerminationBL
						.getResignationTerminationService()
						.getResignationTerminationById(resignationTerminationId);
				oldPayroll = resignationTerminationBL
						.getPayrollBL()
						.getPayrollService()
						.getPayrollDetailByUseCase(
								resignationTermination
										.getResignationTerminationId(),
								resignationTermination.getClass()
										.getSimpleName());

			} else {
				resignationTermination.setCreatedDate(new Date());
				resignationTermination.setPersonByCreatedBy(person);
			}
			JobAssignment jobAssignment = null;
			if (jobAssignmentId != null) {
				jobAssignment = resignationTerminationBL.getJobAssignmentBL()
						.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);
				jobAssignment.setJobAssignmentId(jobAssignmentId);
				resignationTermination.setJobAssignment(jobAssignment);
			}

			if (effectiveDate != null && !effectiveDate.equals(""))
				resignationTermination.setEffectiveDate(DateFormat
						.convertStringToDate(effectiveDate));

			if (requestedDate != null && !requestedDate.equals(""))
				resignationTermination.setRequestedDate(DateFormat
						.convertStringToDate(requestedDate));

			resignationTermination.setImplementation(getImplementation());

			if (requestedPerson != null) {
				Person reqPerson = new Person();
				reqPerson.setPersonId(requestedPerson);
				resignationTermination.setPersonByRequestedBy(reqPerson);
			}
			resignationTermination.setAction(action);
			resignationTermination.setReason(reason);
			resignationTermination.setDescription(description);
			resignationTermination.setType(type);
			resignationTermination.setEligibleDays(givenDays);
			resignationTermination.setGivenDays(givenDays);
			resignationTermination.setNoticeDays(noticeDays);
			resignationTermination
					.setStatus(Constants.HR.ResignationStatus.Requested
							.getCode());

			Payroll payroll = new Payroll();
			payroll.setUseCase(ResignationTermination.class.getSimpleName());
			payroll.setRecordId(resignationTermination
					.getResignationTerminationId());
			payroll.setImplementation(getImplementation());
			payroll.setPerson(jobAssignment.getPerson());
			payroll.setJobAssignment(jobAssignment);
			payroll.setStatus(Constants.HR.PayStatus.InProcess.getCode());

			payroll.setIsApprove(Byte
					.parseByte(WorkflowConstants.Status.Published.getCode()
							+ ""));
			payroll.setIsSalary(false);
			payroll.setCreatedDate(new Date());
			payroll.setStartDate(resignationTermination.getEffectiveDate());
			payroll.setEndDate(resignationTermination.getEffectiveDate());
			payroll.setIsLedgerTransaction(false);
			List<PayrollTransaction> transactions = getPayLineDetails(payroll,
					payLineDetail);

			returnMessage = resignationTerminationBL
					.saveResignationTermination(resignationTermination,
							payroll, transactions, oldPayroll);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("resignationTermination() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the resignationTermination");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			if (resignationTerminationId != null
					&& resignationTerminationId > 0)

				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Modification Failure");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Creation Failure");
			LOGGER.error("resignationTermination() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String deleteResignationTermination() {
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			String returnMessage = "SUCCESS";
			ResignationTermination resignationTermination = new ResignationTermination();
			if (resignationTerminationId != null
					&& resignationTerminationId > 0) {
				resignationTermination = resignationTerminationBL
						.getResignationTerminationService()
						.getResignationTerminationById(resignationTerminationId);

			}
			resignationTerminationBL
					.deleteResignationTermination(resignationTermination);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("deleteResignationTermination() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteResignationTermination() Info unsuccessful Return");
			return ERROR;
		}
	}

	public String approveResignationTermination() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		String returnMessage = "SUCCESS";
		try {

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			ResignationTermination resignationTermination = new ResignationTermination();
			if (resignationTerminationId != null
					&& resignationTerminationId > 0) {
				resignationTermination = resignationTerminationBL
						.getResignationTerminationService()
						.getResignationTerminationById(resignationTerminationId);
				resignationTermination
						.setStatus(Constants.HR.PromotionStatus.Approved
								.getCode());
			}
			/*
			 * returnMessage = resignationTerminationBL
			 * .saveResignationTermination(resignationTermination);
			 */

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("approveResignationTermination() Info Save Sucessfull Return");
			return SUCCESS;
		} catch (ArithmeticException ae) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Failure in creating the approveResignationTermination");
			return ERROR;
		} catch (Exception e) {
			e.printStackTrace();

			LOGGER.error("approveResignationTermination() Info unsuccessful Return");
			return ERROR;
		} finally {

		}
	}

	public String findServicePeriod() {
		try {

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			ResignationTerminationVO resignationTerminationVO = null;

			if (jobAssignmentId != null && jobAssignmentId > 0 && type != null
					&& effectiveDate != null) {

				resignationTerminationVO = new ResignationTerminationVO();
				resignationTerminationVO.setJobAssignmentId(jobAssignmentId);
				resignationTerminationVO.setType(type);
				resignationTerminationVO.setEffectiveDateView(effectiveDate);
				resignationTerminationVO = resignationTerminationBL
						.findServicePeriod(resignationTerminationVO);
			}

			ServletActionContext.getRequest().setAttribute("SERVICE_DETAILS",
					resignationTerminationVO);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String showEosPrintout() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_eos_template") != null ? ((String) session
					.getAttribute("print_eos_template"))
					.equalsIgnoreCase("true") : false;
			ResignationTerminationVO vo = null;

			if (resignationTerminationId != null
					&& resignationTerminationId > 0) {
				ResignationTermination resignationTermination = resignationTerminationBL
						.getResignationTerminationService()
						.getResignationTerminationById(resignationTerminationId);
				if (resignationTermination != null)
					vo = resignationTerminationBL
							.convertEntityToVO(resignationTermination);
				vo.setJobAssignmentId(vo.getJobAssignment()
						.getJobAssignmentId());

			}

			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute("EOS_INFO", vo);
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config.getTemplate("html-eos-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("urlPath", urlPath);
				rootMap.put("requestedDate", vo.getCreatedDateView());
				rootMap.put("employee", vo.getPersonName());
				rootMap.put("company", vo.getCompanyName());
				rootMap.put("department", vo.getDepartmentName());
				rootMap.put("designation", vo.getDesignationName());
				rootMap.put("location", vo.getLocationName());
				rootMap.put("employeeID", vo.getPersonNumber());
				vo = resignationTerminationBL.findServicePeriod(vo);
				rootMap.put("fromDate", vo.getFromDateView());
				rootMap.put("toDate", vo.getToDateView());
				rootMap.put("basic", vo.getBasicAmount());
				rootMap.put("gratuityDays", vo.getGratuityDays());
				rootMap.put("totalAmount", vo.getTotalAmountView());
				rootMap.put("days", vo.getTotalDays());
				rootMap.put("months", vo.getTotalMonths());
				rootMap.put("years", vo.getTotalYears());
				rootMap.put("policyDays", vo.getEosPolicy().getPayDays());
				rootMap.put("policyName", vo.getEosPolicy().getPolicyName());
				rootMap.put("remarks", vo.getReason());
				rootMap.put("comments", vo.getDescription());
				PayrollVO payrollVO = resignationTerminationBL
						.processFinalSettlement(vo);
				rootMap.put("payments", payrollVO.getPayrollTransactionVOs());
				double finalAmount = vo.getTotalAmount();
				for (PayrollTransactionVO transVo : payrollVO
						.getPayrollTransactionVOs()) {
					finalAmount += transVo.getAmount();
				}
				rootMap.put("finalTotalAmount", finalAmount);
				double netAmount = (Double.valueOf(finalAmount));
				String decimalAmount = String.valueOf(AIOSCommons
						.formatAmount(netAmount));
				decimalAmount = decimalAmount.substring(decimalAmount
						.lastIndexOf('.') + 1);
				String amountInWords = AIOSCommons
						.convertAmountToWords(netAmount);
				if (Double.parseDouble(decimalAmount) > 0) {
					amountInWords = amountInWords
							.concat(" and ")
							.concat(AIOSCommons
									.convertAmountToWords(decimalAmount))
							.concat(" Fils ");
					String replaceWord = "Only";
					amountInWords = amountInWords.replaceAll(replaceWord, "");
					amountInWords = amountInWords.concat(" " + replaceWord);
				}
				rootMap.put("finalTotalAmountInWords", amountInWords);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String resignationTerminationPrintout() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_resignation_template") != null ? ((String) session
					.getAttribute("print_resignation_template"))
					.equalsIgnoreCase("true") : false;
			ResignationTerminationVO vo = null;

			if (resignationTerminationId != null
					&& resignationTerminationId > 0) {
				ResignationTermination resignationTermination = resignationTerminationBL
						.getResignationTerminationService()
						.getResignationTerminationById(resignationTerminationId);
				if (resignationTermination != null)
					vo = resignationTerminationBL
							.convertEntityToVO(resignationTermination);
				vo.setJobAssignmentId(vo.getJobAssignment()
						.getJobAssignmentId());

			}

			if (!isTemplatePrintEnabled) {
				ServletActionContext.getRequest().setAttribute(
						"RESIGNATION_TERMINATION_INFO", vo);
				return "default";
			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-resignation-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(
							getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("urlPath", urlPath);
				rootMap.put("requestedDate", vo.getCreatedDateView());
				rootMap.put("employee", vo.getPersonName());
				rootMap.put("company", vo.getCompanyName());
				rootMap.put("department", vo.getDepartmentName());
				rootMap.put("designation", vo.getDesignationName());
				rootMap.put("location", vo.getLocationName());
				rootMap.put("employeeID", vo.getPersonNumber());
				rootMap.put("actionType", vo.getTypeView().toUpperCase());
				rootMap.put("fromDate", vo.getEffectiveDateView());

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				rootMap.put("orderNumber", vo.getJobAssignment()
						.getJobAssignmentNumber()
						+ "-"
						+ month
						+ ""
						+ day
						+ "/" + year);
				rootMap.put("remarks", vo.getReason());
				rootMap.put("comments", vo.getDescription());

				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public List<PayrollTransaction> getPayLineDetails(Payroll payroll,
			String payLineDetail) {
		List<PayrollTransaction> payrollTransactions = new ArrayList<PayrollTransaction>(
				payroll.getPayrollTransactions());
		Map<Long, PayrollTransaction> transMap = new HashMap<Long, PayrollTransaction>();

		for (PayrollTransaction payrollTransaction : payrollTransactions) {
			transMap.put(payrollTransaction.getPayrollTransactionId(),
					payrollTransaction);
		}

		if (payLineDetail != null && !payLineDetail.equals("")) {
			payrollTransactions = new ArrayList<PayrollTransaction>();
			String[] recordArray = new String[payLineDetail.split("##").length];
			recordArray = payLineDetail.split("##");
			for (String record : recordArray) {
				PayrollTransaction payrollTransaction = new PayrollTransaction();
				PayrollElement payrollElement = new PayrollElement();
				String[] dataArray = new String[record.split("@#").length];
				dataArray = record.split("@#");

				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals("")) {
					if (transMap.containsKey(Long.parseLong(dataArray[0])))
						payrollTransaction = transMap.get(Long
								.parseLong(dataArray[0]));
				} else {
					payrollTransaction.setCreatedDate(new Date());
					payrollTransaction
							.setStatus(Constants.HR.PayTransactionStatus.New
									.getCode());
					payrollTransaction.setPerson(payroll.getPerson());
					payrollTransaction.setImplementation(getImplementation());
				}
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals("")) {
					payrollElement.setPayrollElementId(Long
							.parseLong(dataArray[1]));
					payrollTransaction.setPayrollElement(payrollElement);
				}
				if (!dataArray[2].equals("-1")
						&& !dataArray[2].trim().equals(""))
					payrollTransaction.setAmount(Double.valueOf(dataArray[2]));

				if (!dataArray[3].equals("-1")
						&& !dataArray[3].trim().equals(""))
					payrollTransaction.setNote(dataArray[3]);
				
				payrollTransaction.setPayroll(payroll);

				payrollTransactions.add(payrollTransaction);
			}
		}
		return payrollTransactions;
	}

	public String processFinalSettlement() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			ResignationTerminationVO resignationTerminationVO = null;
			PayrollVO payrollVO = null;
			if (jobAssignmentId != null && jobAssignmentId > 0
					&& effectiveDate != null) {

				resignationTerminationVO = new ResignationTerminationVO();
				resignationTerminationVO.setJobAssignmentId(jobAssignmentId);
				resignationTerminationVO.setEffectiveDateView(effectiveDate);
				resignationTerminationVO.setResignationTerminationId(resignationTerminationId);
				resignationTerminationVO.setEffectiveDate(DateFormat
						.convertStringToDate(effectiveDate));
				payrollVO = resignationTerminationBL
						.processFinalSettlement(resignationTerminationVO);
				List<PayrollElement> payrollElements = resignationTerminationBL
						.getPayrollBL().getPayrollService()
						.getAllPayrollElement(getImplementation());

				ServletActionContext.getRequest().setAttribute(
						"PAYROLL_ELEMENTS", payrollElements);
			}

			ServletActionContext.getRequest().setAttribute("PAYROLL_PROCESS",
					payrollVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Boolean getPositionAutoRelease() {
		return positionAutoRelease;
	}

	public void setPositionAutoRelease(Boolean positionAutoRelease) {
		this.positionAutoRelease = positionAutoRelease;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getRequestedPerson() {
		return requestedPerson;
	}

	public void setRequestedPerson(Long requestedPerson) {
		this.requestedPerson = requestedPerson;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public ResignationTerminationBL getResignationTerminationBL() {
		return resignationTerminationBL;
	}

	public void setResignationTerminationBL(
			ResignationTerminationBL resignationTerminationBL) {
		this.resignationTerminationBL = resignationTerminationBL;
	}

	public Long getResignationTerminationId() {
		return resignationTerminationId;
	}

	public void setResignationTerminationId(Long resignationTerminationId) {
		this.resignationTerminationId = resignationTerminationId;
	}

	public Byte getAction() {
		return action;
	}

	public void setAction(Byte action) {
		this.action = action;
	}

	public Integer getEligibleDays() {
		return eligibleDays;
	}

	public void setEligibleDays(Integer eligibleDays) {
		this.eligibleDays = eligibleDays;
	}

	public Integer getGivenDays() {
		return givenDays;
	}

	public void setGivenDays(Integer givenDays) {
		this.givenDays = givenDays;
	}

	public Integer getNoticeDays() {
		return noticeDays;
	}

	public void setNoticeDays(Integer noticeDays) {
		this.noticeDays = noticeDays;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getPayLineDetail() {
		return payLineDetail;
	}

	public void setPayLineDetail(String payLineDetail) {
		this.payLineDetail = payLineDetail;
	}
}
