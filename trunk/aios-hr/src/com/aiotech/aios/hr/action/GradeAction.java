package com.aiotech.aios.hr.action;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.LeavePolicy;
import com.aiotech.aios.hr.domain.entity.PayPeriod;
import com.aiotech.aios.hr.service.bl.GradeBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class GradeAction extends ActionSupport {
	private GradeBL gradeBL;
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(GradeAction.class);
	private Implementation implementation;
	private Integer id;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String returnMessage;
	private Long gradeId = null;
	private Grade grade;
	private Long jobAssignmentId;
	private Long openPositionId;
	private Long recordId;

	public String returnSuccess() {

		return SUCCESS;
	}

	// Listing JSON
	public String getGradeList() {

		try {
			List<Grade> grades = gradeBL.getGradeService().getGradeList(
					getImplementation());

			JSONObject jsonResponse = new JSONObject();
			if (grades != null && grades.size() > 0) {
				jsonResponse.put("iTotalRecords", grades.size());
				jsonResponse.put("iTotalDisplayRecords", grades.size());
			}
			JSONArray data = new JSONArray();
			for (Grade list : grades) {

				JSONArray array = new JSONArray();
				array.add(list.getGradeId() + "");
				array.add(list.getGradeName() + "");
				array.add(list.getSequence());
				array.add(list.getMinSalary() + "");
				array.add(list.getMaxSalary() + "");
				if (list.getAttendancePolicy() != null
						&& list.getAttendancePolicy().getAttendancePolicyId() != null)
					array.add(list.getAttendancePolicy().getPolicyName() + "");
				else
					array.add("-NA-");
				if (list.getLeavePolicy() != null
						&& list.getLeavePolicy().getLeavePolicyId() != null)
					array.add(list.getLeavePolicy().getLeavePolicyName() + "");
				else
					array.add("-NA-");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Grade List Sucessfull Return");
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Grade List unsuccessful Return");
			return ERROR;
		}
	}

	public String getGradeAdd() {
		try {
			Grade grade = new Grade();

			Long leaveDefaultId = null;
			Long attendanceDefalutId = null;
			List<LeavePolicy> leavePolicies = gradeBL.getLeavePolicyBL()
					.getLeavePolicyService()
					.getLeavePolicyList(getImplementation());
			Map<String, String> evaluationPeriod = gradeBL.evaluationPeriod();
			Map<String, String> ticketClass = gradeBL.ticketClass();

			List<LookupDetail> insuranceType = gradeBL.getLookupMasterBL()
					.getActiveLookupDetails("INSURANCE_TYPE", true);

			ServletActionContext.getRequest().setAttribute("LEAVE_POLICIES",
					leavePolicies);

			List<AttendancePolicy> attendancePolicies = gradeBL
					.getAttendancePolicyBL().getAttendancePolicyService()
					.getAttendancePolicyList(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"ATTENDANCE_POLICIES", attendancePolicies);

			if(recordId!=null && recordId>0){
				gradeId=recordId;
				// Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (gradeId > 0) {
					comment.setRecordId(recordId);
					comment.setUseCase(AttendancePolicy.class.getName());
					comment = gradeBL.getCommentBL().getCommentInfo(comment);
					ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
							comment);
				}
			}
			
			if (gradeId != null && gradeId > 0) {
				grade = gradeBL.getGradeService().getGradeInfo(gradeId);
			} else {
				List<Grade> grades = gradeBL.getGradeService().getGradeList(
						getImplementation());
				if (grades != null && grades.size() > 0) {
					Collections.sort(grades, new Comparator<Grade>() {
						public int compare(Grade m1, Grade m2) {
							return m2.getSequence().compareTo(m1.getSequence());
						}
					});
					grade.setSequence(grades.get(0).getSequence() + 1);
				} else {
					grade.setSequence(1);
				}
				for (LeavePolicy leavePolicy : leavePolicies) {
					if (leavePolicy.getIsDefalut() != null
							&& leavePolicy.getIsDefalut())
						leaveDefaultId = leavePolicy.getLeavePolicyId();
				}

				for (AttendancePolicy attendancePolicy : attendancePolicies) {
					if (attendancePolicy.getIsDefalut() != null
							&& attendancePolicy.getIsDefalut())
						attendanceDefalutId = attendancePolicy
								.getAttendancePolicyId();
				}

			}

			List<PayPeriod> payPeriodList = gradeBL.getPayPeriodBL()
					.getPayPeriodService().getAllPayPeriod(getImplementation());
			ServletActionContext.getRequest().setAttribute("PAYPERIOD_LIST",
					payPeriodList);

			ServletActionContext.getRequest().setAttribute("GRADE_INFO", grade);
			ServletActionContext.getRequest().setAttribute("DEFAULT_LEAVE",
					leaveDefaultId);
			ServletActionContext.getRequest().setAttribute(
					"EVALUTATION_PERIOD", evaluationPeriod);
			ServletActionContext.getRequest().setAttribute("INCREMENT_PERIOD",
					evaluationPeriod);
			ServletActionContext.getRequest().setAttribute("TICKET_CLASS",
					ticketClass);
			ServletActionContext.getRequest().setAttribute("INSURANCE_TYPE",
					insuranceType);

			ServletActionContext.getRequest().setAttribute(
					"DEFAULT_ATTENDANCE", attendanceDefalutId);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String getGradeView() {
		try {
			Grade grade = new Grade();
			if (openPositionId != null && openPositionId > 0) {
				Grade gradeTemp = gradeBL.getGradeService()
						.findGradeByOpenPositionId(openPositionId);
				if (gradeTemp != null)
					gradeId = gradeTemp.getGradeId();
			}

			if (jobAssignmentId != null && jobAssignmentId > 0) {
				JobAssignment jobAssignment = gradeBL.getJobAssignmentBL()
						.getJobAssignmentService()
						.getJobAssignmentInfo(jobAssignmentId);
				if (jobAssignment != null
						&& jobAssignment.getDesignation() != null
						&& jobAssignment.getDesignation().getGrade() != null)
					gradeId = jobAssignment.getDesignation().getGrade()
							.getGradeId();
			}

			Long leaveDefaultId = null;
			Long attendanceDefalutId = null;
			List<LeavePolicy> leavePolicies = gradeBL.getLeavePolicyBL()
					.getLeavePolicyService()
					.getLeavePolicyList(getImplementation());
			Map<String, String> evaluationPeriod = gradeBL.evaluationPeriod();
			Map<String, String> ticketClass = gradeBL.ticketClass();

			List<LookupDetail> insuranceType = gradeBL.getLookupMasterBL()
					.getActiveLookupDetails("INSURANCE_TYPE", true);

			ServletActionContext.getRequest().setAttribute("LEAVE_POLICIES",
					leavePolicies);

			List<AttendancePolicy> attendancePolicies = gradeBL
					.getAttendancePolicyBL().getAttendancePolicyService()
					.getAttendancePolicyList(getImplementation());
			ServletActionContext.getRequest().setAttribute(
					"ATTENDANCE_POLICIES", attendancePolicies);

			if (gradeId != null && gradeId > 0) {
				grade = gradeBL.getGradeService().getGradeInfo(gradeId);
			} else if (jobAssignmentId != null && jobAssignmentId > 0) {
				grade = gradeBL.findGradeByJobAssignmentId(jobAssignmentId);
			} else {
				List<Grade> grades = gradeBL.getGradeService().getGradeList(
						getImplementation());
				if (grades != null && grades.size() > 0) {
					Collections.sort(grades, new Comparator<Grade>() {
						public int compare(Grade m1, Grade m2) {
							return m2.getSequence().compareTo(m1.getSequence());
						}
					});
					grade.setSequence(grades.get(0).getSequence() + 1);
				} else {
					grade.setSequence(1);
				}
				for (LeavePolicy leavePolicy : leavePolicies) {
					if (leavePolicy.getIsDefalut() != null
							&& leavePolicy.getIsDefalut())
						leaveDefaultId = leavePolicy.getLeavePolicyId();
				}

				for (AttendancePolicy attendancePolicy : attendancePolicies) {
					if (attendancePolicy.getIsDefalut() != null
							&& attendancePolicy.getIsDefalut())
						attendanceDefalutId = attendancePolicy
								.getAttendancePolicyId();
				}

			}

			List<PayPeriod> payPeriodList = gradeBL.getPayPeriodBL()
					.getPayPeriodService().getAllPayPeriod(getImplementation());
			ServletActionContext.getRequest().setAttribute("PAYPERIOD_LIST",
					payPeriodList);

			ServletActionContext.getRequest().setAttribute("GRADE_INFO", grade);
			ServletActionContext.getRequest().setAttribute("DEFAULT_LEAVE",
					leaveDefaultId);
			ServletActionContext.getRequest().setAttribute(
					"EVALUTATION_PERIOD", evaluationPeriod);
			ServletActionContext.getRequest().setAttribute("INCREMENT_PERIOD",
					evaluationPeriod);
			ServletActionContext.getRequest().setAttribute("TICKET_CLASS",
					ticketClass);
			ServletActionContext.getRequest().setAttribute("INSURANCE_TYPE",
					insuranceType);

			ServletActionContext.getRequest().setAttribute(
					"DEFAULT_ATTENDANCE", attendanceDefalutId);

			ServletActionContext.getRequest().setAttribute("VIEW_FLAG", true);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		}
	}

	public String saveGrade() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			grade.setImplementation(getImplementation());
			
			if(grade.getLookupDetail()==null || 
					grade.getLookupDetail().getLookupDetailId()==null || 
					grade.getLookupDetail().getLookupDetailId()<=0){
				grade.setLookupDetail(null);
			}

			grade.setIsActive(true);
			gradeBL.saveGrade(grade);

			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");

			LOGGER.info("Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			if (grade.getGradeId() > 0)
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Save Failured.");
			else
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "Save Failured.");
			LOGGER.error("Unsuccessful Return");
			return ERROR;
		}
	}

	public String deleteGrade() {
		try {
			if (gradeId != null && gradeId > 0) {
				gradeBL.deleteGrade(gradeId);
			} else {
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", "No record selected to delete");
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"SUCCESS");
			LOGGER.info("deleteGrade() Sucessfull Return");
			return SUCCESS;
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Delete Failure");
			LOGGER.error("deleteGrade() unsuccessful Return");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getGradeId() {
		return gradeId;
	}

	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

	public GradeBL getGradeBL() {
		return gradeBL;
	}

	public void setGradeBL(GradeBL gradeBL) {
		this.gradeBL = gradeBL;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Long getOpenPositionId() {
		return openPositionId;
	}

	public void setOpenPositionId(Long openPositionId) {
		this.openPositionId = openPositionId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

}
